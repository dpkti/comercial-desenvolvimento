VERSION 4.00
Begin VB.Form Form1 
   Caption         =   "GERA��O DE BASES PARA FRANQUIAS E PAAC"
   ClientHeight    =   3720
   ClientLeft      =   1650
   ClientTop       =   1725
   ClientWidth     =   7950
   Height          =   4125
   Icon            =   "fil320.frx":0000
   Left            =   1590
   LinkTopic       =   "Form1"
   ScaleHeight     =   3720
   ScaleWidth      =   7950
   Top             =   1380
   Width           =   8070
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   5970
      Picture         =   "fil320.frx":0442
      ScaleHeight     =   420
      ScaleWidth      =   540
      TabIndex        =   15
      Top             =   2280
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   5955
      Picture         =   "fil320.frx":0884
      ScaleHeight     =   420
      ScaleWidth      =   540
      TabIndex        =   14
      Top             =   1755
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6600
      Picture         =   "fil320.frx":0CC6
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   13
      Top             =   2130
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   6600
      Picture         =   "fil320.frx":0FD0
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   12
      Top             =   1635
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Finaliza"
      Height          =   495
      Left            =   6015
      TabIndex        =   7
      Top             =   855
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Gera Base Di�ria"
      Height          =   495
      Left            =   6030
      TabIndex        =   6
      Top             =   255
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   1230
      Left            =   3120
      TabIndex        =   3
      Top             =   135
      Width           =   2775
      Begin VB.OptionButton Option2 
         Caption         =   "Mensal"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   810
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Di�rio"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1200
      Left            =   105
      TabIndex        =   0
      Top             =   135
      Width           =   2775
      Begin VB.CheckBox Check2 
         Caption         =   "Completa"
         Height          =   195
         Left            =   135
         TabIndex        =   2
         Top             =   795
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Alterados"
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Value           =   1  'Checked
         Width           =   975
      End
   End
   Begin MSGrid.Grid Grid1 
      Height          =   2055
      Left            =   1200
      TabIndex        =   17
      Top             =   3720
      Visible         =   0   'False
      Width           =   5415
      _Version        =   65536
      _ExtentX        =   9551
      _ExtentY        =   3625
      _StockProps     =   77
      BackColor       =   16777215
      Rows            =   300
      Cols            =   3
   End
   Begin VB.Label Label7 
      Caption         =   "Gerando Base Gen�rica: "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   450
      Left            =   690
      TabIndex        =   16
      Top             =   1830
      Width           =   3015
   End
   Begin VB.Label Label3 
      Caption         =   "Franquia / PAAC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   270
      Left            =   3795
      TabIndex        =   11
      Top             =   2340
      Width           =   1980
   End
   Begin VB.Label Label1 
      Caption         =   "Gerando Base Espec�fica:"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   270
      Left            =   690
      TabIndex        =   10
      Top             =   2355
      Width           =   2955
   End
   Begin VB.Label Label4 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   960
      TabIndex        =   9
      Top             =   2760
      Width           =   6105
   End
   Begin VB.Label Label5 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   2280
      TabIndex        =   8
      Top             =   3240
      Width           =   3780
   End
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Sub COPIA_ARQUIVOS_GENERICOS()

On Error GoTo TRATA_ERRO

   'COPIA ARQUIVOS COM CAMPO COD_LOJA DO DIRETORIO ESPECIFICO CONFORME OS DEPOSITOS
   'PARA OS QUAIS A FRANQUIA EFETUA VENDA

    SQL = "SELECT tipo_loja "
    SQL = SQL & " FROM deposito_filial "
    SQL = SQL & " WHERE cod_filial = " & w_cod_filial
    
    Set MSnp = DBCONTROLE2.CreateSnapshot(SQL)
     
    If MSnp!tipo_loja = 1 Or MSnp.EOF Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR1\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
    
    ElseIf MSnp!tipo_loja = 2 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR2\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
         
    ElseIf MSnp!tipo_loja = 3 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR3\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
    
    ElseIf MSnp!tipo_loja = 4 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR4\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 5 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR5\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
    
    ElseIf MSnp!tipo_loja = 6 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR6\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 7 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR7\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 8 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR8\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 9 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR9\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 10 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR10\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 11 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR11\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 12 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR12\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 13 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR13\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 14 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR14\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 15 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR15\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 16 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR16\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 17 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR17\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 18 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR18\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 19 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR19\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 20 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR20\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
    
    ElseIf MSnp!tipo_loja = 21 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR21\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
       
    ElseIf MSnp!tipo_loja = 22 Then
    
        DIRETORIO_ORIGEM = "C:\DADOS\FILIAL\DIARIA\DIR22\"
        DIRETORIO_DESTINO = "C:\DADOS\FILIAL\DIARIA\"
        DoEvents
    
    End If
       
      
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\DEP_VISA.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\DEP_VISA.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\DEP_VISA.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ESTAT_IT.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\ESTAT_IT.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ESTAT_IT.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_ANA.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\ITEM_ANA.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_ANA.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_EST.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\ITEM_EST.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_EST.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_PRE.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\ITEM_PRE.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_PRE.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_GLO.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\ITEM_GLO.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\ITEM_GLO.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETE_EN.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\FRETE_EN.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETE_EN.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETE_UF.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\FRETE_UF.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETE_UF.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETEUFT.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\FRETEUFT.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\FRETEUFT.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\TAXA.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\TAXA.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\TAXA.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\LOJA.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\LOJA.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\LOJA.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\UF_DEP.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\UF_DEP.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\UF_DEP.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\CESTAV.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\CESTAV.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\CESTAV.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\CESTAI.DBF")
    Do While Guarda <> ""
        Kill "C:\DADOS\FILIAL\DIARIA\CESTAI.DBF"
        DoEvents
        Guarda = Dir("C:\DADOS\FILIAL\DIARIA\CESTAI.DBF")
    Loop
    Guarda = Dir("C:\DADOS\FILIAL\DIARIA\VDR_ITEM.DBF")
 '   Do While Guarda <> ""
 '       Kill "C:\DADOS\FILIAL\DIARIA\VDR_ITEM.DBF"
 '       DoEvents
 '       Guarda = Dir("C:\DADOS\FILIAL\DIARIA\VDR_ITEM.DBF")
 '   Loop
       
    FileCopy DIRETORIO_ORIGEM & "DEP_VISA.DBF", DIRETORIO_DESTINO & "DEP_VISA.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "ESTAT_IT.DBF", DIRETORIO_DESTINO & "ESTAT_IT.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "ITEM_ANA.DBF", DIRETORIO_DESTINO & "ITEM_ANA.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "ITEM_EST.DBF", DIRETORIO_DESTINO & "ITEM_EST.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "ITEM_PRE.DBF", DIRETORIO_DESTINO & "ITEM_PRE.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "ITEM_GLO.DBF", DIRETORIO_DESTINO & "ITEM_GLO.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "FRETE_EN.DBF", DIRETORIO_DESTINO & "FRETE_EN.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "FRETE_UF.DBF", DIRETORIO_DESTINO & "FRETE_UF.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "FRETEUFT.DBF", DIRETORIO_DESTINO & "FRETEUFT.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "TAXA.DBF", DIRETORIO_DESTINO & "TAXA.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "LOJA.DBF", DIRETORIO_DESTINO & "LOJA.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "UF_DEP.DBF", DIRETORIO_DESTINO & "UF_DEP.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "CESTAV.DBF", DIRETORIO_DESTINO & "CESTAV.DBF"
    DoEvents
    FileCopy DIRETORIO_ORIGEM & "CESTAI.DBF", DIRETORIO_DESTINO & "CESTAI.DBF"
    DoEvents
'    FileCopy DIRETORIO_ORIGEM & "VDR_ITEM.DBF", DIRETORIO_DESTINO & "VDR_ITEM.DBF"
'    DoEvents

Exit Sub

TRATA_ERRO:
If Err = 53 Then
    Resume Next
End If

End Sub

Private Sub Command1_Click()

On Error GoTo TrataErro
    Grid1.Row = 0
    linha = 1
    Grid1.ColWidth(1) = 1000
    Grid1.ColWidth(2) = 1000
    
    Screen.MousePointer = 11
     
    tempoini = Time
    
   'Verifica se j� executou o programa
    Call VERIF_EXECUCAO
    If w_termina Then
        Exit Sub
    End If
    
   'Verifica se algum form3.check foi escolhido
    Call VERIF_CHECK
    If w_termina Then
        Exit Sub
    End If
    
   'Abre o arquivo access de controle
    Call ABRE_CONTROLE
    
   'Verifica que tipos de bases ser�o geradas
   'Call VERIFICA_TIPO
    
    Call DIRETORIOS
    
    Call DESCOMPACTA_BASE_ZERADA
      
   'Verifica a exist�ncia do 0000.MDB, arquivo padr�o da base
    Call VERIF_0000MDB
    
   'Flega o in�cio da execu��o do programa
    w_execucao = True
    
   'Busca sequencia do EXTRACAO para gera��o de base diaria
    Call BUSCA_NUMFILIAL
     
   'Define os arquivos de trabalho atrav�z do w_num_filial
    Call DEFINE_NOME_FILIAL(1)
    
    Call REMOVE_MDB
    
   'Abre o(s) arquivo(s) access para leitura da base
    Call ABRE_OUTROS
    
   'Cria o .MDB geral para todos os gerentes no diretorio de trabalho
    Form1.label5.Caption = "BASE GEN�RICA"
    Form1.Refresh
    Call CRIA_TEMP
    
      
    Picture4.Visible = True
    
    Call APLICACAO
    
    Call BANCO
    
    Call CATEG_SINAL
    
    Call CIDADE
    
    Call CLIE_MENSAGEM_FILIAL
    
    Call DATAS_FILIAL
    
    Call DOLAR_DIARIO
    
    Call filial
    
    Call FORNECEDOR
    
    Call GRUPO
    
    Call ITEM_CADASTRO
    
   '*** ITEM_CUSTO N�O DEVE SER GERADO PARA PAAC�S
   'Call ITEM_CUSTO
    
    Call MONTADORA
    
    Call PLANO_PGTO
    
    Call SUBGRUPO
    linha = linha + 1
    
    Call SUBST_TRIBUTARIA
    
    Call TABELA_DESCPER
    
    Call TABELA_VENDA
    
    Call TIPO_CLIENTE
    
    Call TRANSPORTADORA
    
    Call UF
    
    Call UF_ORIGEM_DESTINO
    
    Call UF_DPK
    
    Call UF_TPCLIENTE
         
   'NOVAS TABELAS PARA FILIAL, REGIONAL E REPRESENTANTE
    Call FILIAL_FRANQUIA
    
    Call FRETE_UF_BLOQ
    
    Call NATUREZA_OPERACAO
    
    Call TIPO_CLIENTE_BLAU
    
    Call CANCEL_PEDNOTA
    
    Call R_REPVEN
    
    Call R_FILDEP
    
    Call EMBALADOR
    
    Call CLIENTE_CATEG_VDR
    
    Call FORNECEDOR_ESPECIFICO
    
    Call R_UF_DEPOSITO
    
   'Tabelas com registros para dele��o
   'Call DELETA_APLICACAO
    
    Call DELETA_TABELA_DESCPER
    
    Call DEL_FORNECEDOR_ESPECIFICO
    
    Call DELETA_R_UF_DEPOSITO
    
    Call DELETA_R_DPK_EQUIV
    
    Call R_DPK_EQUIV
    
   '*****************************************
   'TABELAS ESPECIFICAS POR DEPOSITO x FILIAL
    Call IDENTIFICA_DIRETORIO
    
   'GERA_TABELAS_DEPOSITO_ESPECIFICO
    
    Call DEPOSITO_VISAO
    
    Call ESTATISTICA_ITEM
    
    Call ITEM_ANALITICO
    
    Call ITEM_ESTOQUE
    
    Call ITEM_GLOBAL
    
    Call ITEM_PRECO
    
    Call TAXA
    
    Call UF_DEPOSITO
    
    Call FRETE_UF
    
    Call FRETE_UF_TRANSP
    
    Call LOJA
    
    Call FRETE_ENTREGA
    
    Call CLIENTE_CARACTERISTICA
    
    Call UF_CATEG
            
    Call CESTA_ITEM
    
    Call CESTA_VENDA
    
   '*****************************************
   'Copia o arquivo gen�rico para todos os gerentes cadastrados no controle_bases.gerente
    Call COPIA_ESPECIFICO_FILIAL
    
   'Remove o(s) arquivo(s) gen�rico(s) diario e completo
    Call REMOVE_TEMP
    
   'Call COPIA_BASE_PRONTA
   'DoEvents
    
    Picture4.Visible = False
    Picture1.Left = 6675
    Picture1.Visible = True
    
    '***********************
    'GERA BASES ESPECIFICAS
    '***********************
    If Form1.Check1.Value = 1 Then 'BASE DE ATUALIZA��O DIARIA
        
        msql = "select cod_filial,fl_vdr from filial "
        
        Set CON = dbcontrole.CreateSnapshot(msql)
        CON.MoveLast
        CON.MoveFirst
        
        Do While Not CON.EOF
            w_cod_filial = CON!cod_filial
            FL_VDR = CON!FL_VDR
         
            Call COPIA_ARQUIVOS_GENERICOS
            DoEvents
                   
            Picture5.Visible = True
            Form1.label5.Caption = "BASE FILIAL = " & w_cod_filial
            Form1.Refresh
            DoEvents
            Form1.Grid1.Col = 2
    
            Call REPRESENTANTE_FILIAL
            DoEvents
            
            Call REPR_END_CORRESP_FILIAL
            DoEvents
            
            Call REPRESENTACAO_FILIAL
            DoEvents
    
            Call R_REPCGC_FILIAL
            DoEvents
    
            Call R_REPVEN_FILIAL
            DoEvents
    
            Call CLIENTE_FILIAL
            DoEvents
         
            Call CLIE_CREDITO_FILIAL
            DoEvents
    
            Call CLIE_ENDERECO_FILIAL
            DoEvents
            
            Call CLIENTE_INTERNET
            DoEvents
         
            Call DELETA_REPRESENTANTE
            DoEvents
    
            Call DELETA_CLIENTE
            DoEvents
    
            Call SALDO_PEDIDOS_FILIAL
            DoEvents
    
            Call DUPLICATAS_FILIAL
            DoEvents
    
            Call PEDNOTA_VENDA("F")
            DoEvents
         
            Call ITPEDNOTA_VENDA("F")
            DoEvents
        
            Call R_PEDIDO_CONF("F")
            DoEvents
         
            Call ROMANEIO("F")
            DoEvents
         
            Call V_PEDLIQ_VENDA("F")
            DoEvents
    
            Call DELETA_R_CLIE_REPRES
            DoEvents
    
            Call R_CLIE_REPRES
            DoEvents
            
            If FL_VDR = "S" Then
                Call DELETA_VDR_R_CLIE_LINHA_PRODUTO
                DoEvents
            
                Call DELETA_CLIENTE_categ_vdr
                DoEvents
            
                Call VDR_CONTROLE_VDR
                DoEvents
                
                Call VDR_ITEM_PRECO
                DoEvents
                
                Call VDR_R_CLIE_LINHA_PRODUTO
                DoEvents
                
                Call VDR_DESCONTO_CATEG
                DoEvents
                
                Call VDR_TABELA_VENDA
                DoEvents
            
            End If
    
            Call DEPOSITO
            DBF.Close
           'DBF2.Close
            DoEvents
            DoEvents
            Form1.label5.Caption = "ZIPA BASES DI�RIAS"
            Form1.Refresh
            DoEvents
         
            Call ZIPA_BASES_FILIAL
            DoEvents
         
            Form1.label5.Caption = "GERA E_MAIL"
            Form1.Refresh
            DoEvents
         
            Call GERA_E_MAIL_FILIAL
            DoEvents
            Form1.Refresh
            Form1.label5.Caption = "DELETANDO BASES ESPEC�FICAS"
            DoEvents
    
            Call DEL_BASE_ESPECIFICA
            DoEvents
            
            Call DESCOMPACTA_BASE_ESPECIFICA
            DoEvents
         
            If CON.RecordCount > 0 Then
                CON.MoveNext
            End If
            DoEvents
            
            Set DBF = OpenDatabase("C:\DADOS\FILIAL\DIARIA\", False, False, "DBASE IV;")
            DoEvents
           
           '**********************************************************
           'DESCOMPACTA BASE ZERADA
            DIRETORIO_DEL = "C:\DADOS\FILIAL\DIARIA\"
           '**********************************************************
   
        Loop
    
    End If
    
   'Atualiza banco de controle CONTROLE.MDB
    Form1.label5.Caption = "BASE DE CONTROLE"
    Form1.Refresh
    Call NUM_FILIAL
    
    tempofim = Time
    Screen.MousePointer = 0
         
    MsgBox "Inicio: " & tempoini & "   Fim: " & tempofim, 0, "TEMPO EXECU��O"
    Form1.label5.Caption = "EXECU��O OK !!!, FINALIZAR"
    Form1.Label4.Caption = ""
              
Exit Sub
     
TrataErro:
    If Err = 3022 Then
        Resume Next
    ElseIf Err = 70 Then
        Resume Next
    Else
        MsgBox Str(Err) + "-" + Error$
    End If
    
    Resume Next

End Sub

Private Sub Command2_Click()
   'esta � a vers�o anterior
    If w_termina Then
        dbextracao.Close
        dbcontrole.Close
        DBF.Close
    End If
    
    ret = Shell("C:\com\saida\arquivos\copia.bat")
  
    End
End Sub

Private Sub Form_Load()

    dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'dir_controle = "C:\DADOS\CONTROLE\NOVA\CONTROLE.MDB"
    dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'dir_e_mail = "C:\COM\SAIDA\MSG\"
    dir_e_mail = "p:\out\"

   'zipa = "C:\DADOS\PTOBAT\NOVA\ZIPA.BAT "
   'zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "
    zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
    w_termina = False
    w_execucao = False
   
   'Define diretorio base do arquivo fixo 0000.MDB
    dir_base_filial = "C:\DADOS\FILIAL\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   'onde os "X" representam o num. do arquivo a ser gerado para o gerente
    dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   'onde os "Y" representam o c�digo do gerente
   'dir_diaria_filial = "C:\DADOS\FILIAL\DIARIA\NOVA"
    dir_diaria_filial = "C:\DADOS\FILIAL\DIARIA"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
    dir_completa_filial = "C:\DADOS\FILIAL\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   'dir_controle = "C:\DADOS\CONTROLE\NOVA\CONTROLE.MDB"
    dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
    dir_e_mail = "p:\out\"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail_arq_filial = "C:\COM\SAIDA\ARQUIVOS\"
    dir_e_mail_arq_filial = "p:\out\"

   'Define diretorio do .BAT de pkzip dos arquivos
   'zipa = "C:\DADOS\PTOBAT\NOVA\ZIPA.BAT "
    zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
    w_termina = False
    w_execucao = False
   
End Sub


