VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{FF817B01-5406-4F3D-9821-ABF53C70F145}#1.0#0"; "XPFRAMENOVO.OCX"
Begin VB.Form frmVDA015 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:  VDA015 - NOTAS RECEBIDAS  :."
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   Icon            =   "frmVDA015.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8205
   ScaleWidth      =   11910
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboLoja 
      Height          =   315
      Left            =   10080
      Style           =   2  'Dropdown List
      TabIndex        =   40
      Top             =   2250
      Width           =   1785
   End
   Begin XPFrameNovo.xFrame fraDigitacao 
      Height          =   2325
      Left            =   0
      TabIndex        =   17
      Top             =   1740
      Width           =   5745
      _ExtentX        =   10134
      _ExtentY        =   4101
      Caption         =   "Digitar Notas"
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      HeaderGradientBottom=   12611136
      Begin VB.TextBox txtObs 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   630
         Left            =   60
         MaxLength       =   300
         TabIndex        =   8
         Top             =   1650
         Width           =   5070
      End
      Begin Bot�o.cmd cmdBuscarFornecedor 
         Height          =   330
         Left            =   555
         TabIndex        =   19
         TabStop         =   0   'False
         ToolTipText     =   "Buscar fornecedor"
         Top             =   540
         Width           =   330
         _ExtentX        =   582
         _ExtentY        =   582
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA015.frx":08CA
         PICN            =   "frmVDA015.frx":08E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtNomeFornecedor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   915
         Locked          =   -1  'True
         MaxLength       =   33
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   540
         Width           =   4770
      End
      Begin VB.TextBox txtCodFornecedor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   60
         MaxLength       =   3
         TabIndex        =   0
         Top             =   540
         Width           =   465
      End
      Begin Bot�o.cmd cmdGravar 
         Height          =   540
         Left            =   5160
         TabIndex        =   9
         ToolTipText     =   "Gravar informa��es"
         Top             =   1740
         Width           =   525
         _ExtentX        =   926
         _ExtentY        =   953
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA015.frx":0C80
         PICN            =   "frmVDA015.frx":0C9C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtCGC 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         MaxLength       =   14
         TabIndex        =   3
         Top             =   1095
         Width           =   1830
      End
      Begin VB.TextBox txtQtdItens 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4740
         MaxLength       =   5
         TabIndex        =   7
         Top             =   1095
         Width           =   930
      End
      Begin VB.TextBox txtSituacao 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4290
         MaxLength       =   1
         TabIndex        =   6
         Top             =   1095
         Width           =   270
      End
      Begin VB.TextBox txtDifer 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3750
         MaxLength       =   1
         TabIndex        =   5
         Text            =   "0"
         Top             =   1095
         Width           =   360
      End
      Begin VB.TextBox txtSerie 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3090
         MaxLength       =   3
         TabIndex        =   4
         Top             =   1095
         Width           =   480
      End
      Begin VB.TextBox txtNumNota 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "999999"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   0
         EndProperty
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   90
         MaxLength       =   6
         TabIndex        =   2
         Top             =   1095
         Width           =   810
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Observa��es"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   34
         Top             =   1440
         Width           =   945
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1080
         TabIndex        =   21
         Top             =   915
         Width           =   315
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Qtd Itens"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   4740
         TabIndex        =   25
         Top             =   915
         Width           =   690
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sit"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   4290
         TabIndex        =   24
         Top             =   915
         Width           =   180
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Difer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   3750
         TabIndex        =   23
         Top             =   915
         Width           =   345
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "S�rie"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   3090
         TabIndex        =   22
         Top             =   915
         Width           =   360
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fornecedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   60
         TabIndex        =   18
         Top             =   330
         Width           =   825
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "N� da Nota"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   20
         Top             =   915
         Width           =   795
      End
   End
   Begin XPFrameNovo.xFrame FraConsultar 
      Height          =   1485
      Left            =   5820
      TabIndex        =   26
      Top             =   1740
      Width           =   6075
      _ExtentX        =   10716
      _ExtentY        =   2619
      Caption         =   "Consultar"
      Enabled         =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FontBold        =   0   'False
      FontItalic      =   0   'False
      FontSize        =   8.25
      FontStrikethru  =   0   'False
      FontUnderline   =   0   'False
      HeaderGradientBottom=   12611136
      Begin MSComCtl2.DTPicker txtDtConsultarFim 
         Height          =   345
         Left            =   2490
         TabIndex        =   37
         Top             =   510
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   609
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   51642369
         CurrentDate     =   39303
      End
      Begin MSComCtl2.DTPicker txtDtConsultar 
         Height          =   345
         Left            =   300
         TabIndex        =   11
         Top             =   510
         Width           =   1725
         _ExtentX        =   3043
         _ExtentY        =   609
         _Version        =   393216
         CheckBox        =   -1  'True
         Format          =   51642369
         CurrentDate     =   39303
      End
      Begin Bot�o.cmd cmdBuscarFornecedorCons 
         Height          =   330
         Left            =   510
         TabIndex        =   30
         TabStop         =   0   'False
         ToolTipText     =   "Buscar fornecedor"
         Top             =   1080
         Width           =   330
         _ExtentX        =   582
         _ExtentY        =   582
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA015.frx":1976
         PICN            =   "frmVDA015.frx":1992
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtNomeFornecedorCons 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   870
         Locked          =   -1  'True
         MaxLength       =   33
         TabIndex        =   29
         TabStop         =   0   'False
         Top             =   1080
         Width           =   3570
      End
      Begin VB.TextBox txtCodFornecedorCons 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   30
         MaxLength       =   3
         TabIndex        =   12
         Top             =   1080
         Width           =   465
      End
      Begin Bot�o.cmd cmdConsultar 
         Height          =   330
         Left            =   4470
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Buscar fornecedor"
         Top             =   1080
         Width           =   1560
         _ExtentX        =   2752
         _ExtentY        =   582
         BTYPE           =   3
         TX              =   "Consultar Notas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA015.frx":1D2C
         PICN            =   "frmVDA015.frx":1D48
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "De:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   30
         TabIndex        =   39
         Top             =   570
         Width           =   255
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "At�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2100
         TabIndex        =   38
         Top             =   570
         Width           =   315
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Fornecedor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   30
         TabIndex        =   31
         Top             =   885
         Width           =   825
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Data"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   60
         TabIndex        =   27
         Top             =   300
         Width           =   345
      End
   End
   Begin MSComctlLib.ListView lsvNotas 
      Height          =   3885
      Left            =   0
      TabIndex        =   28
      Top             =   4290
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   6853
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   0   'False
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   10
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Loja"
         Object.Width           =   3175
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Fornecedor"
         Object.Width           =   4939
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "N� Nota"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "CGC"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   4
         Text            =   "S�rie"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   5
         Text            =   "Difer"
         Object.Width           =   1058
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   6
         Text            =   "Situa��o"
         Object.Width           =   1499
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   7
         Text            =   "Qtd Itens"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Dt Digita��o"
         Object.Width           =   2999
      EndProperty
      BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   9
         Text            =   "Observa��es"
         Object.Width           =   2540
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   960
      Left            =   11205
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   180
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA015.frx":20E2
      PICN            =   "frmVDA015.frx":20FE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSobre 
      Height          =   960
      Left            =   10440
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Informa��es do sistema"
      Top             =   180
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Sobre"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA015.frx":2DD8
      PICN            =   "frmVDA015.frx":2DF4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   510
      Left            =   11280
      TabIndex        =   32
      TabStop         =   0   'False
      ToolTipText     =   "Excluir item selecionado"
      Top             =   3750
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   900
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA015.frx":3ACE
      PICN            =   "frmVDA015.frx":3AEA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   510
      Left            =   10650
      TabIndex        =   35
      TabStop         =   0   'False
      ToolTipText     =   "Gerar Arquivo Excel"
      Top             =   3750
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   900
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA015.frx":4084
      PICN            =   "frmVDA015.frx":40A0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdBlocoNotas 
      Height          =   510
      Left            =   10020
      TabIndex        =   36
      TabStop         =   0   'False
      ToolTipText     =   "Gerar arquivo texto"
      Top             =   3750
      Width           =   585
      _ExtentX        =   1032
      _ExtentY        =   900
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA015.frx":4D7A
      PICN            =   "frmVDA015.frx":4D96
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Notas digitadas:"
      Height          =   195
      Left            =   0
      TabIndex        =   33
      Top             =   4080
      Width           =   1140
   End
   Begin VB.Label lblDepositoConectado 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   11730
      TabIndex        =   16
      Top             =   1440
      Width           =   120
   End
   Begin VB.Label lblRazaoSocial 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   45
      TabIndex        =   15
      Top             =   1440
      Width           =   120
   End
   Begin VB.Image Image4 
      Appearance      =   0  'Flat
      Height          =   375
      Left            =   0
      Picture         =   "frmVDA015.frx":5510
      Stretch         =   -1  'True
      Top             =   1350
      Width           =   11940
   End
   Begin VB.Image Image3 
      Height          =   1380
      Left            =   0
      Picture         =   "frmVDA015.frx":5646
      Top             =   0
      Width           =   2265
   End
   Begin VB.Image Image1 
      Height          =   1365
      Left            =   0
      Picture         =   "frmVDA015.frx":6B95
      Stretch         =   -1  'True
      Top             =   0
      Width           =   11940
   End
   Begin VB.Menu mnuExcluir 
      Caption         =   "Excluir"
      Visible         =   0   'False
      Begin VB.Menu mnuExcluirItem 
         Caption         =   "Excluir Item Selecionado"
      End
   End
End
Attribute VB_Name = "FRMvda015"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBlocoNotas_Click()
    Dim vArq As Integer
    Dim vNomeArq As String
    
    vArq = FreeFile
    
    vNomeArq = "C:\Nota_Compra_Entrada_" & Format(Now, "YYYYMMDD_HHMMSS") & ".TXT"
    
    Open vNomeArq For Output As vArq
    
    Print #vArq, "Nota Compra Entrada - " & Now
    
    Print #vArq, "Loja;Nota;CGC;Serie;Difer;Situacao;Qtd Itens;Dt Entrada;Obs"
    
    For i = 1 To lsvNotas.ListItems.Count
        
        Print #vArq, lsvNotas.ListItems(i) & ";" & lsvNotas.ListItems(i).SubItems(1) & _
        ";" & lsvNotas.ListItems(i).SubItems(2) & ";" & lsvNotas.ListItems(i).SubItems(3) & _
        ";" & lsvNotas.ListItems(i).SubItems(4) & ";" & lsvNotas.ListItems(i).SubItems(5) & _
        ";" & lsvNotas.ListItems(i).SubItems(6) & ";" & lsvNotas.ListItems(i).SubItems(7) & _
        ";" & lsvNotas.ListItems(i).SubItems(8) & ";" & lsvNotas.ListItems(i).SubItems(9)
 
    Next
    
    Close #vArq

    MsgBox "Arquivo " & vNomeArq, vbInformation, "Aten��o"

End Sub

Private Sub cmdBuscarFornecedorCons_Click()

    vRetornoCodFornecedor = 0

    frmBuscarFornecedor.Show 1

    If vRetornoCodFornecedor <> 0 Then

        txtCodFornecedorCons = vRetornoCodFornecedor

    End If

End Sub


Private Sub cmdExcel_Click()
    
    Dim xl As Object
    Dim xlw As Object
    Dim xlp As Object
    Dim vNomeArq As String
    
    Set xl = CreateObject("excel.application")
    Set xlw = xl.Workbooks.Add
    Set xlp = xlw.Sheets(1)

    xlp.Cells(1, 1) = "Nota Compra Entrada - " & Now
    
    xl.Cells(2, 1) = "Loja"
    xl.Cells(2, 2) = "Nota"
    xl.Cells(2, 3) = "CGC"
    xl.Cells(2, 4) = "Serie"
    xl.Cells(2, 5) = "Difer"
    xl.Cells(2, 6) = "Situacao"
    xl.Cells(2, 7) = "Qtd Itens"
    xl.Cells(2, 8) = "Dt Entrada"
    xl.Cells(2, 9) = "Obs"
    
    For i = 1 To lsvNotas.ListItems.Count
        
        xl.Cells(i + 2, 1).Value = Me.lsvNotas.ListItems(i)
        xl.Cells(i + 2, 2).Value = Me.lsvNotas.ListItems(i).SubItems(1)
        xl.Cells(i + 2, 3).Value = Me.lsvNotas.ListItems(i).SubItems(2)
        xl.Cells(i + 2, 4).Value = Me.lsvNotas.ListItems(i).SubItems(3)
        xl.Cells(i + 2, 5).Value = Me.lsvNotas.ListItems(i).SubItems(4)
        xl.Cells(i + 2, 6).Value = Me.lsvNotas.ListItems(i).SubItems(5)
        xl.Cells(i + 2, 7).Value = Me.lsvNotas.ListItems(i).SubItems(6)
        xl.Cells(i + 2, 8).Value = Me.lsvNotas.ListItems(i).SubItems(7)
        xl.Cells(i + 2, 9).Value = Me.lsvNotas.ListItems(i).SubItems(8)
        xl.Cells(i + 2, 10).Value = Me.lsvNotas.ListItems(i).SubItems(9)
 
    Next
    
    vNomeArq = "C:\Nota_Compra_Entrada_" & Format(Now, "YYYYMMDD_HHMMSS")
    
    xlp.SaveAs vNomeArq
    
    xlw.Close True

    Set xlp = Nothing
    Set xlw = Nothing
    Set xl = Nothing

    MsgBox "Os dados foram enviados para o arquivo " & vNomeArq, vbInformation, "Aten��o"
    
End Sub

Private Sub cmdExcluir_Click()
    If Not lsvNotas.SelectedItem Is Nothing Then
        If Me.lsvNotas.SelectedItem <> "" Then
            If MsgBox("Confirma exclus�o do item selecionado ?", vbQuestion + vbYesNo) = vbYes Then
            
                vClsDPK001.ExcluiBind
                vBanco.Parameters.Add "PM_CODLOJA", Val(Me.lsvNotas.SelectedItem), 1
                vBanco.Parameters.Add "PM_NOTA", Val(lsvNotas.SelectedItem.SubItems(2)), 1
                vBanco.Parameters.Add "PM_CGC", lsvNotas.SelectedItem.SubItems(3), 1
                vBanco.Parameters.Add "PM_SERIE", lsvNotas.SelectedItem.SubItems(4), 1
                vBanco.Parameters.Add "PM_DIFER", Me.lsvNotas.SelectedItem.SubItems(5), 1
                vBanco.Parameters.Add "PM_SITUACAO", Me.lsvNotas.SelectedItem.SubItems(6), 1
                vBanco.Parameters.Add "PM_QTD", Me.lsvNotas.SelectedItem.SubItems(7), 1
                vBanco.Parameters.Add "PM_OBS", Me.lsvNotas.SelectedItem.SubItems(8), 1
                vBanco.Parameters.Add "PM_ACAO", 2, 1
                
                vClsDPK001.ExecutaPL vOwners(1) & ".PCK_VDA015.PR_INSERT_NOTA(:PM_CODLOJA, :PM_NOTA, :PM_CGC, :PM_SERIE, :PM_DIFER, :PM_SITUACAO, :PM_QTD, :PM_OBS, :PM_ACAO)"
        
                Preencher_Lsv_Load 0, "", ""
                   
            End If
        End If
    End If
End Sub

Private Sub Form_Activate()
    On Error Resume Next
    txtCodFornecedor.SetFocus
End Sub



Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Me.txtSerie.SetFocus
    
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtCodFornecedorCons_Change()

    txtNomeFornecedorCons = ""

    If txtCodFornecedorCons <> "" Then

        Set vObjOracle(0) = vClsTABELAS001.TabelaFornecedor(txtCodFornecedorCons)

        If Not vObjOracle(0).EOF Then

            txtNomeFornecedorCons = vObjOracle(0).Fields(4)

        End If

    End If
End Sub
Private Sub txtCodFornecedorCons_GotFocus()

    Call vClsDPK001.SelecionaCampo(txtCodFornecedorCons)

End Sub
Private Sub txtCodFornecedorcons_KeyPress(KeyAscii As Integer)

   Call vClsDPK001.CampoNumerico(KeyAscii, False)

End Sub

Private Sub cmdBuscarFornecedor_Click()

    vRetornoCodFornecedor = 0

    frmBuscarFornecedor.Show 1

    If vRetornoCodFornecedor <> 0 Then

        txtCodFornecedor = vRetornoCodFornecedor

    End If

End Sub


Private Sub cmdConsultar_Click()
    
    If Val(0 & txtDtConsultar.Day) > 0 And Val(0 & txtDtConsultarFim.Day) = 0 Then
        MsgBox "Verifique a data final e tente novamente.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    
    If vCD = 1 Then
        If Val(cboLoja.Text) = 0 Then
        
          If txtDtConsultar <> txtDtConsultarFim Then
          MsgBox "A op��o de consultar todos os CDs somente est� dispon�vel para um dia espec�fico.", vbInformation + vbOKOnly, "Aten��o"
            Exit Sub
          End If
        
        End If
    End If
    
    
    If vCD = 1 Then
    
        If Val(0 & txtDtConsultar.Day) > 0 And Val(txtCodFornecedorCons) = 0 Then
            Preencher_Lsv_Load_CDS 0, txtDtConsultar.Value, txtDtConsultarFim.Value, Val(cboLoja.Text)
        ElseIf Val(0 & txtDtConsultar.Day) = 0 And Val(txtCodFornecedorCons) > 0 Then
            Preencher_Lsv_Load_CDS txtCodFornecedorCons, "", "", Val(cboLoja.Text)
        ElseIf Val(0 & txtDtConsultar.Day) > 0 And Val(txtCodFornecedorCons) > 0 Then
            Preencher_Lsv_Load_CDS txtCodFornecedorCons, txtDtConsultar.Value, Me.txtDtConsultarFim.Value, Val(cboLoja.Text)
        End If
    Else
        If Val(0 & txtDtConsultar.Day) > 0 And Val(txtCodFornecedorCons) = 0 Then
            Preencher_Lsv_Load 0, txtDtConsultar.Value, txtDtConsultarFim.Value
        ElseIf Val(0 & txtDtConsultar.Day) = 0 And Val(txtCodFornecedorCons) > 0 Then
            Preencher_Lsv_Load txtCodFornecedorCons, "", ""
        ElseIf Val(0 & txtDtConsultar.Day) > 0 And Val(txtCodFornecedorCons) > 0 Then
            Preencher_Lsv_Load txtCodFornecedorCons, txtDtConsultar.Value, Me.txtDtConsultarFim.Value
        End If
    End If
End Sub

Private Sub txtCGC_LostFocus()

    If txtCGC <> "" Then
    
        Set vObjOracle(0) = vClsTABELAS001.TabelaFornecedor(, , , txtCGC)
        
        If vObjOracle(0).EOF Then
        
            Call vClsDPK001.Informar("CGC n�o encontrado.")
            
            txtCodFornecedor = ""
            txtCGC = ""
            txtCGC.SetFocus
            
            Exit Sub
        
        Else
        
            txtCodFornecedor = vObjOracle(0).Fields(0)
        
        End If
    
    End If

End Sub

Private Sub txtCodFornecedor_Change()

    txtNomeFornecedor = ""

    If txtCodFornecedor <> "" Then

        Set vObjOracle(0) = vClsTABELAS001.TabelaFornecedor(txtCodFornecedor)

        If Not vObjOracle(0).EOF Then

            txtNomeFornecedor = vObjOracle(0).Fields(4)

        End If

    End If

End Sub
Private Sub txtCodFornecedor_GotFocus()

    Call vClsDPK001.SelecionaCampo(txtCodFornecedor)

End Sub
Private Sub txtCodFornecedor_KeyPress(KeyAscii As Integer)

    Call vClsDPK001.CampoNumerico(KeyAscii, False)

End Sub

Private Sub cmdGravar_Click()
    If Val(Me.txtCodFornecedor) = 0 Then
        MsgBox "Informe o c�digo do Fornecedor.", vbInformation, "Aten��o"
        Me.txtCodFornecedor.SetFocus
        Exit Sub
    End If
    
    If Val(Me.txtNumNota) = 0 Then
        MsgBox "Informe o n�mero da nota.", vbInformation, "Aten��o"
        Me.txtNumNota.SetFocus
        Exit Sub
    End If
    
    If Trim(txtSerie) = "" Then
        MsgBox "Informe a s�rie.", vbInformation, "Aten��o"
        Me.txtSerie.SetFocus
        Exit Sub
    End If
    
    If Trim(txtDifer) = "" Then
        MsgBox "Informe o Difer.", vbInformation, "Aten��o"
        Me.txtDifer.SetFocus
        Exit Sub
    End If
    
    If Trim(txtSituacao) = "" Then
        MsgBox "Informe a Situa��o.", vbInformation, "Aten��o"
        Me.txtSituacao.SetFocus
        Exit Sub
    End If
    
    If Val(txtQtdItens) = 0 Then
        MsgBox "Informe a quantidade de itens.", vbInformation, "Aten��o"
        Me.txtQtdItens.SetFocus
        Exit Sub
    End If
        
    vClsDPK001.ExcluiBind
        
    vBanco.Parameters.Add "PM_LOJA", vCD, 1
    vBanco.Parameters.Add "PM_NOTA", txtNumNota, 1
    vBanco.Parameters.Add "PM_CGC", txtCGC, 1
    vBanco.Parameters.Add "PM_SERIE", txtSerie, 1
    vBanco.Parameters.Add "PM_DIFER", txtDifer, 1
    vBanco.Parameters.Add "PM_SITUACAO", txtSituacao, 1
    vBanco.Parameters.Add "PM_QTD", txtQtdItens, 1
    vBanco.Parameters.Add "PM_OBS", Trim(Me.txtObs), 1
    vBanco.Parameters.Add "PM_ACAO", 1, 1
    
    vClsDPK001.ExecutaPL (vOwners(1) & ".PCK_VDA015.PR_INSERT_NOTA(:PM_LOJA, :PM_NOTA, :PM_CGC, :PM_SERIE, :PM_DIFER, :PM_SITUACAO, :PM_QTD, :PM_OBS, :PM_ACAO)")
    
    Preencher_Lsv_Load 0, "", ""
    
    If vCD = 1 Then
        cboLoja.ListIndex = 1
    End If
    
    
    txtCodFornecedor = ""
    txtNumNota = ""
    txtCGC = ""
    txtSerie = ""
    txtDifer = "0"
    txtSituacao = ""
    txtQtdItens = ""
    txtObs = ""
    
    txtCodFornecedor.SetFocus
    
End Sub

Private Sub cmdSair_Click()

    If vClsDPK001.Perguntar("Confirma sa�da do sistema?") = vbYes Then

        Call vClsDPK001.SAIR

    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub
Private Sub Form_Load()

    On Error GoTo Erro
    
    txtDtConsultar.Value = Now - 1
    txtDtConsultarFim.Value = Now - 1

    'ARQUIVOS OPCIONAIS
    'F:\SISTEMAS\Padr�es\VB6\Forms\Form de Busca de Itens.frm
    'F:\SISTEMAS\Padr�es\VB6\Forms\Form de Busca de Clientes.frm
    'F:\SISTEMAS\Padr�es\VB6\Forms\Form de Busca de Fornecedores.frm
    'F:\SISTEMAS\Padr�es\VB6\Forms\Form de Busca de Transportadoras.frm
    'F:\SISTEMAS\Padr�es\VB6\M�dulos\M�dulo Excel.bas
    'F:\SISTEMAS\Padr�es\VB6\M�dulos\M�dulo OA.bas

    Call vClsDPK001.ConectarOracle("PRODUCAO", "vda015", , True, Me)


    If vTipoCDConectado = "U" Then

        vOwners(1) = "PRODUCAO"

    ElseIf vTipoCDConectado = "M" Then

        vOwners(1) = "DEP" & Format(vCD, "00")

    End If

    Me.Caption = ".:  " & UCase(vObjOracle(0).Fields(0)) & " - " & UCase(vObjOracle(0).Fields(1) & "   [" & vObjOracle(0).Fields(2) & "]") & "  :."

    Preencher_Lsv_Load 0, "", ""
    

    'Preencher combo--------------------------------------------------------------------
    If vCD = 1 Then
        Dim vRst As Object
        Set vRst = vBanco.CreateDynaset("SELECT Cod_loja, Nome_fantasia FROM LOJA where cod_loja in(SELECT Cod_loja FROM LOJA_ativa) order by Cod_loja", 0&)
        Me.cboLoja.AddItem "0 - TODOS"
        For i = 1 To vRst.RecordCount
            Me.cboLoja.AddItem vRst!Cod_loja & " - " & vRst!Nome_fantasia
            vRst.MoveNext
        Next
        Set vRst = Nothing
        
        For i = 0 To cboLoja.ListCount - 1
            If vCD = Val(cboLoja.List(i)) Then
                cboLoja.ListIndex = i
                Exit For
            End If
        Next
        cboLoja.Visible = True
    Else
        cboLoja.Visible = False
    End If
    '------------------------------------------------------------------------------------
    

    Exit Sub

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "Form_Load")

End Sub
Private Sub Form_Unload(Cancel As Integer)
    Cancel = 1
End Sub

Private Sub txtCodFornecedor_LostFocus()

    If txtCodFornecedor <> "" Then
    
        Set vObjOracle(0) = vClsTABELAS001.TabelaFornecedor(txtCodFornecedor)
        
        If vObjOracle(0).EOF Then
        
            Call vClsDPK001.Informar("Fornecedor n�o encontrado.")
            
            txtCGC = ""
            txtCodFornecedor = ""
            txtCodFornecedor.SetFocus
            
            Exit Sub
        
        Else
        
            txtCGC = vObjOracle(0).Fields(2)
        
        End If
    
    End If

End Sub


Private Sub txtDifer_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then txtSituacao.SetFocus
    
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDtConsultarFim_LostFocus()
    If Val(0 & txtDtConsultarFim.Value) = 0 Then
        MsgBox "Por favor preencher a data final.", vbInformation, "Aten��o"
        txtDtConsultarFim.SetFocus
        Exit Sub
    ElseIf CDate(txtDtConsultarFim.Value) < CDate(txtDtConsultar.Value) Then
        MsgBox "A data final n�o poder ser menor que a inicial.", vbInformation, "Aten��o"
        txtDtConsultarFim.SetFocus
        Exit Sub
    End If
End Sub

Private Sub txtNumNota_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then txtCGC.SetFocus
    
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub

Sub Preencher_lsv(pRst As Object)
    Dim lItem As ListItem
    
    lsvNotas.ListItems.Clear
    
    Do While pRst.EOF = False
        
        Set lItem = Me.lsvNotas.ListItems.Add
        lItem = pRst("Deposito")
        lItem.SubItems(1) = pRst("Fornecedor")
        lItem.SubItems(2) = pRst("NUM_NF")
        lItem.SubItems(3) = pRst("CGC")
        lItem.SubItems(4) = pRst("Serie")
        lItem.SubItems(5) = pRst("Difer")
        lItem.SubItems(6) = pRst("Situacao")
        lItem.SubItems(7) = pRst("Qtd_Itens")
        lItem.SubItems(8) = pRst("DT_ENTRADA")
        lItem.SubItems(9) = "" & pRst("OBSERVACOES")
        
        pRst.MoveNext
    Loop
    
End Sub

Sub Preencher_Lsv_Load(pCodFornecedor As Long, pData As String, pDataFim As String)
    
    vBanco.Parameters.Remove "PM_CURSOR"
    vBanco.Parameters.Add "PM_CURSOR", 0, ORAPARM_BOTH
    vBanco("PM_CURSOR").serverType = ORATYPE_CURSOR
    vBanco("PM_CURSOR").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco("PM_CURSOR").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODFORNECEDOR"
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Remove "PM_DATAFIM"
    
    vBanco.Parameters.Add "PM_CODFORNECEDOR", Val(pCodFornecedor), 1
    vBanco.Parameters.Add "PM_DATA", IIf(Len(pData) = 0, "0", Formata_Data(pData)), 1
    vBanco.Parameters.Add "PM_DATAFIM", IIf(Len(pDataFim) = 0, "0", Formata_Data(pDataFim)), 1
    
    vClsDPK001.ExecutaPL vOwners(1) & ".PCK_VDA015.PR_SELECT_NOTAS(:PM_CURSOR, :PM_CODFORNECEDOR, :PM_DATA, :PM_DATAFIM)"

    Set vObjOracle(0) = vBanco.Parameters("PM_CURSOR").Value

    If Not vObjOracle(0) Is Nothing Then
        Preencher_lsv vObjOracle(0)
    Else
        MsgBox "Informa��es n�o econtradas.", vbInformation, "Aten��o"
    End If
End Sub


Sub Preencher_Lsv_Load_CDS(pCodFornecedor As Long, pData As String, pDataFim As String, pCodLoja As Integer)
    
    vBanco.Parameters.Remove "PM_CURSOR"
    vBanco.Parameters.Add "PM_CURSOR", 0, ORAPARM_BOTH
    vBanco("PM_CURSOR").serverType = ORATYPE_CURSOR
    vBanco("PM_CURSOR").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco("PM_CURSOR").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "PM_CODFORNECEDOR"
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Remove "PM_DATAFIM"
    vBanco.Parameters.Remove "PM_COD_LOJA"
    
    vBanco.Parameters.Add "PM_CODFORNECEDOR", Val(pCodFornecedor), 1
    vBanco.Parameters.Add "PM_DATA", IIf(Len(pData) = 0, "0", Formata_Data(pData)), 1
    vBanco.Parameters.Add "PM_DATAFIM", IIf(Len(pDataFim) = 0, "0", Formata_Data(pDataFim)), 1
    vBanco.Parameters.Add "PM_COD_LOJA", Val(pCodLoja), 1
    
    If Val(pCodLoja) = 0 Then
        vClsDPK001.ExecutaPL vOwners(1) & ".PCK_VDA015.PR_SELECT_NOTAS_TODOS(:PM_CURSOR, :PM_CODFORNECEDOR, :PM_DATA, :PM_DATAFIM, :PM_COD_LOJA)"
    Else
        vClsDPK001.ExecutaPL vOwners(1) & ".PCK_VDA015.PR_SELECT_NOTAS_CDS(:PM_CURSOR, :PM_CODFORNECEDOR, :PM_DATA, :PM_DATAFIM, :PM_COD_LOJA)"
    End If
    
    

    Set vObjOracle(0) = vBanco.Parameters("PM_CURSOR").Value

    If Not vObjOracle(0) Is Nothing Then
        Preencher_lsv vObjOracle(0)
    Else
        MsgBox "Informa��es n�o econtradas.", vbInformation, "Aten��o"
    End If
End Sub

Function Formata_Data(pData As String) As String
    
    If pData = "" Then Exit Function
    
    Dim vMes As String
    
    Select Case Month(pData)
        Case 1
            vMes = "JAN"
        Case 2
            vMes = "FEB"
        Case 3
            vMes = "MAR"
        Case 4
            vMes = "APR"
        Case 5
            vMes = "MAY"
        Case 6
            vMes = "JUN"
        Case 7
            vMes = "JUL"
        Case 8
            vMes = "AUG"
        Case 9
            vMes = "SEP"
        Case 10
            vMes = "OCT"
        Case 11
            vMes = "NOV"
        Case 12
            vMes = "DEC"
    End Select
    
    Formata_Data = Format(Day(CDate(pData)), "00") & "-" & vMes & "-" & Format(Year(CDate(pData)), "00")
    
End Function

Private Sub txtObs_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtQtdItens_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdGravar.SetFocus
End Sub

Private Sub txtSerie_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then txtDifer.SetFocus
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtSituacao_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then Me.txtQtdItens.SetFocus
    
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub
