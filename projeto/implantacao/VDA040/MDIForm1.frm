VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "Consulta Item"
   ClientHeight    =   5940
   ClientLeft      =   945
   ClientTop       =   780
   ClientWidth     =   6690
   Icon            =   "MDIForm1.frx":0000
   LinkTopic       =   "MDIForm1"
   MouseIcon       =   "MDIForm1.frx":030A
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand sscmdSair 
         Height          =   495
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDIForm1.frx":0614
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   5445
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   873
      _StockProps     =   15
      Caption         =   "Para obter ajuda click ""F1"""
      ForeColor       =   255
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FloodColor      =   8388608
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub MDIForm_Load()
      On Error GoTo TrataErro

    Dim strLogin As String
    Dim ss As Object
    Dim strMsg As String
    
    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
      End
    End If
    
    
    Call Get_CD(Mid(App.Path, 1, 1))

   If strTp_banco = "U" Then
     strTabela_Banco = "PRODUCAO."
   Else
     strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
   End If


    fl_banco = 0
    'posicionar tela
    MDIForm1.Top = 0
    MDIForm1.Left = 0
    MDIForm1.Width = Screen.Width
    MDIForm1.Height = Screen.Height
        
    
    
    'path na rede
    strPath = "H:\ORACLE\DADOS\32BITS\"
    strPathUsuario = "G:\USR\" & sCOD_VEND & "\"
    'strPath = "F:\ORACLE\DADOS\32BITS\"
    'strPathUsuario = "F:\USR\" & sCOD_VEND & "\"
    
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA040/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("DESENV", "VDA040/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("DESENV", "LOJA/DPK", 0&)
    
        
    'carregar data de faturamento,uf origem, filial origem
          SQL = "select a.dt_faturamento, a.dt_real, "
    SQL = SQL & " to_char(b.cod_filial,'0009') || '-' || c.sigla filial, "
    SQL = SQL & " d.cod_uf cod_uf,"
    SQL = SQL & " to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default"
    SQL = SQL & " from datas a, " & strTabela_Banco & "deposito b, filial c,  cidade d, loja e "
    SQL = SQL & " where  b.cod_filial=c.cod_filial and "
    SQL = SQL & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
        
    If ss.EOF And ss.BOF Then
        Call Process_Line_Errors(SQL)
    Else
        Data_Faturamento = CDate(ss!DT_FATURAMENTO)
        data_real = CDate(ss!DT_REAL)
        FILIAL_PED = ss!filial
        uf_origem = ss!cod_uf
        deposito_default = ss!deposito_default
    End If
    

    frmGeral.Show
                
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
       Resume
    ElseIf Err = 13 Then
      strMsg = "PARA EXECUTAR ESTE PROGRAMA � PRECISO ESTAR LOGADO"
      strMsg = strMsg & " NA REDE COMO UM TLMKT (C�DIGO DE VENDA)."
      strMsg = strMsg & " VOC� EST� LOGADO COMO: '" & Environ("USER") & "'"
      MsgBox strMsg, vbExclamation, "ATEN��O"
      End
    Else
       Call Process_Line_Errors(SQL)
    End If
End Sub



Private Sub mnuSair_Click()
  Call sscmdSair_Click
End Sub



Private Sub mnuSobre_Click()
  frmSobre.Show 1
End Sub

Private Sub sscmdSair_Click()
  If frmGeral.txtCOD_FABRICA = "" Then
    End
  End If
End Sub



