VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmCD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informa��es CDs"
   ClientHeight    =   3510
   ClientLeft      =   285
   ClientTop       =   2385
   ClientWidth     =   8850
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3510
   ScaleWidth      =   8850
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   10080
      TabIndex        =   1
      Top             =   4080
      Width           =   1095
   End
   Begin MSGrid.Grid Grid1 
      Height          =   2595
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Visible         =   0   'False
      Width           =   8610
      _Version        =   65536
      _ExtentX        =   15187
      _ExtentY        =   4577
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
End
Attribute VB_Name = "frmCD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdSair_Click()
  Unload Me
End Sub

Private Sub Form_Activate()
  Dim ss As Object
  Screen.MousePointer = 11

  SQL = "select to_char(a.cod_loja,'00')||'-'||b.nome_fantasia loja,"
  SQL = SQL & " (a.qtd_atual - a.qtd_reserv - a.qtd_pendente) qtd_disponivel,"
  SQL = SQL & " c.class_abc, c.class_abcf, c.class_venda, "
  SQL = SQL & " TO_CHAR(a.vl_ult_compra,'999999990.00') vl_ult_compra,"
  SQL = SQL & " a.dt_ult_compra, TO_CHAR(a.cuema,'9999999990.000') cuema,"
  SQL = SQL & " a.dt_cuema, c.categoria,"
  SQL = SQL & " a.qtd_maxvda , "
  SQL = SQL & " a.qtd_atual,"
  SQL = SQL & " a.qtd_reserv,"
  SQL = SQL & " a.qtd_pendente,"
  SQL = SQL & " DECODE(a.situacao,0,'ATIVO',DECODE(a.situacao,8,'SUBSTITUIDO',"
  SQL = SQL & " DECODE(a.situacao,9,'DESATIVADO',''))) situacao"
  SQL = SQL & " from item_estoque a, loja b, item_analitico c"
  SQL = SQL & " Where"
  SQL = SQL & " C.cod_dpk = :DPK and"
  SQL = SQL & " a.cod_dpk = c.cod_dpk+0 and"
  SQL = SQL & " a.cod_loja = c.cod_loja and"
  SQL = SQL & " a.cod_loja = b.cod_loja"
  SQL = SQL & " Order by a.cod_loja"
  
  oradatabase.Parameters.Remove "DPK"
  oradatabase.Parameters.Add "DPK", frmGeral.txtCOD_DPK, 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.EOF And ss.BOF Then
    MsgBox "N�o h� informa��es por CD para este item", vbInformation, "ATEN��O"
    Screen.MousePointer = 0
    Unload Me
  End If
  With Grid1
        .Cols = 15
        .Rows = ss.recordcount + 1
        .ColWidth(0) = 1800
        .ColWidth(1) = 1200
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 1100
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        .ColWidth(9) = 1000
        .ColWidth(10) = 1000
        .ColWidth(11) = 1000
        .ColWidth(12) = 1000
        .ColWidth(13) = 1000
        .ColWidth(14) = 1200
        
         .Row = 0
         .Col = 0
         .Text = "CD"
         .Col = 1
         .Text = "Qtd.Disponivel"
         .Col = 2
         .Text = "Class.ABC"
         .Col = 3
         .Text = "Class.ABCF"
         .Col = 4
         .Text = "Class.Venda"
         .Col = 5
         .Text = "Vl.Ult.Compra"
         .Col = 6
         .Text = "DT.Ult.Compra"
         .Col = 7
         .Text = "Vl.Cuema"
         .Col = 8
         .Text = "Dt.Cuema"
         .Col = 9
         .Text = "Categoria"
         .Col = 10
         .Text = "Qtd.Max.Vda"
         .Col = 11
         .Text = "Qtd.Atual"
         .Col = 12
         .Text = "Qtd.Reserv."
         .Col = 13
         .Text = "Qtd.Pendente"
         .Col = 14
         .Text = "Situa��o"
        
        
         For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = IIf(IsNull(ss!loja), "", ss!loja)
            .Col = 1
            .ColAlignment(1) = 1
            .Text = IIf(IsNull(ss!qtd_disponivel), 0, ss!qtd_disponivel)
            .Col = 2
            .ColAlignment(2) = 2
            .Text = IIf(IsNull(ss!class_abc), "", ss!class_abc)
            .Col = 3
            .ColAlignment(3) = 2
            .Text = IIf(IsNull(ss!class_abcf), "", ss!class_abcf)
            .Col = 4
            .ColAlignment(4) = 1
            .Text = IIf(IsNull(ss!class_venda), "", ss!class_venda)
            .Col = 5
            .ColAlignment(5) = 1
            .Text = IIf(IsNull(ss!vl_ult_compra), "", ss!vl_ult_compra)
            .Col = 6
            .ColAlignment(6) = 2
            .Text = IIf(IsNull(ss!dt_ult_compra), "", ss!dt_ult_compra)
            .Col = 7
            .ColAlignment(7) = 1
            .Text = IIf(IsNull(ss!cuema), "", ss!cuema)
            .Col = 8
            .ColAlignment(8) = 2
            .Text = IIf(IsNull(ss!dt_cuema), "", ss!dt_cuema)
            .Col = 9
            .ColAlignment(9) = 1
            .Text = IIf(IsNull(ss!categoria), "", ss!categoria)
            .Col = 10
            .ColAlignment(10) = 1
            .Text = IIf(IsNull(ss!qtd_maxvda), "", ss!qtd_maxvda)
            .Col = 11
            .ColAlignment(11) = 1
            .Text = IIf(IsNull(ss!qtd_atual), "", ss!qtd_atual)
            .Col = 12
            .ColAlignment(12) = 1
            .Text = IIf(IsNull(ss!qtd_reserv), "", ss!qtd_reserv)
            .Col = 13
            .ColAlignment(13) = 1
            .Text = IIf(IsNull(ss!qtd_pendente), "", ss!qtd_pendente)
            .Col = 14
            .ColAlignment(14) = 2
            .Text = IIf(IsNull(ss!situacao), "", ss!situacao)

            ss.MoveNext
        Next
        
    End With
    Screen.MousePointer = 0
    Grid1.Visible = True
End Sub

