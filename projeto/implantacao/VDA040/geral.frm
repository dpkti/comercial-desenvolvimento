VERSION 5.00
Begin VB.Form frmGeral 
   BorderStyle     =   0  'None
   Caption         =   "Dados Gerais"
   ClientHeight    =   5130
   ClientLeft      =   60
   ClientTop       =   675
   ClientWidth     =   9285
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5130
   ScaleWidth      =   9285
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdCD 
      Caption         =   "INF. POR CD"
      Height          =   435
      Left            =   6240
      TabIndex        =   51
      Top             =   4620
      Width           =   1395
   End
   Begin VB.CommandButton cmdEquivalencia 
      Caption         =   "EQUIVAL�NCIA"
      Height          =   435
      Left            =   4200
      TabIndex        =   50
      Top             =   4620
      Width           =   1395
   End
   Begin VB.CommandButton cmdAplicacao 
      Caption         =   "APLICA��O"
      Height          =   435
      Left            =   2160
      TabIndex        =   49
      Top             =   4620
      Width           =   1395
   End
   Begin VB.CommandButton cmdSubst 
      Caption         =   "ITEM SUBSTITUIDO"
      Height          =   435
      Left            =   2160
      TabIndex        =   48
      Top             =   4020
      Width           =   1395
   End
   Begin VB.CommandButton cmdLimpar 
      Caption         =   "LIMPAR TELA"
      Height          =   375
      Left            =   7500
      TabIndex        =   47
      Top             =   1500
      Width           =   1395
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "BUSCAR"
      Height          =   375
      Left            =   5940
      TabIndex        =   18
      Top             =   1500
      Width           =   1395
   End
   Begin VB.TextBox txtEan 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   6960
      MaxLength       =   13
      TabIndex        =   17
      Top             =   900
      Width           =   1755
   End
   Begin VB.TextBox txtDescricao 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1020
      MaxLength       =   40
      TabIndex        =   15
      Top             =   1320
      Width           =   4455
   End
   Begin VB.TextBox txtDgite 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5700
      MaxLength       =   1
      TabIndex        =   13
      Top             =   840
      Width           =   255
   End
   Begin VB.TextBox txtNrite 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5340
      MaxLength       =   2
      TabIndex        =   12
      Top             =   840
      Width           =   315
   End
   Begin VB.TextBox txtSbgruite 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4980
      MaxLength       =   2
      TabIndex        =   11
      Top             =   840
      Width           =   315
   End
   Begin VB.TextBox txtGruite 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4620
      MaxLength       =   2
      TabIndex        =   10
      Top             =   840
      Width           =   315
   End
   Begin VB.TextBox txtOriginal 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1020
      MaxLength       =   20
      TabIndex        =   8
      Top             =   840
      Width           =   2535
   End
   Begin VB.TextBox txtCOD_DPK 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   8160
      MaxLength       =   9
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtCOD_FABRICA 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4380
      MaxLength       =   19
      TabIndex        =   4
      Top             =   180
      Width           =   3255
   End
   Begin VB.TextBox txtCOD_FORNECEDOR 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1080
      MaxLength       =   3
      TabIndex        =   1
      Top             =   240
      Width           =   375
   End
   Begin VB.Label Label22 
      AutoSize        =   -1  'True
      Caption         =   "Margem Lucro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1920
      TabIndex        =   53
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Label lblMg_Lucro 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1920
      TabIndex        =   52
      Top             =   2940
      Width           =   630
   End
   Begin VB.Label lblCadastro 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   46
      Top             =   4140
      Width           =   1110
   End
   Begin VB.Label Label21 
      AutoSize        =   -1  'True
      Caption         =   "Dt.Cadastro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   180
      TabIndex        =   45
      Top             =   3960
      Width           =   1020
   End
   Begin VB.Label lblQtd_MinForn 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7860
      TabIndex        =   44
      Top             =   3600
      Width           =   630
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      Caption         =   "Qtd.Min.Forn."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   7860
      TabIndex        =   43
      Top             =   3420
      Width           =   1170
   End
   Begin VB.Label lblQtd_minvda 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6480
      TabIndex        =   42
      Top             =   3600
      Width           =   570
   End
   Begin VB.Label Label18 
      AutoSize        =   -1  'True
      Caption         =   "Qtd.Min.Venda"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   6480
      TabIndex        =   41
      Top             =   3420
      Width           =   1275
   End
   Begin VB.Label lblPC_IPI 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5640
      TabIndex        =   40
      Top             =   3540
      Width           =   570
   End
   Begin VB.Label Label16 
      AutoSize        =   -1  'True
      Caption         =   "Aliq.IPI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5700
      TabIndex        =   39
      Top             =   3360
      Width           =   630
   End
   Begin VB.Label lblIPI 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3960
      TabIndex        =   38
      Top             =   3540
      Width           =   1530
   End
   Begin VB.Label Label17 
      AutoSize        =   -1  'True
      Caption         =   "Trib.IPI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4080
      TabIndex        =   37
      Top             =   3360
      Width           =   645
   End
   Begin VB.Label lblProcedencia 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   36
      Top             =   3540
      Width           =   3690
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      Caption         =   "Proced�ncia"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   180
      TabIndex        =   35
      Top             =   3360
      Width           =   1080
   End
   Begin VB.Label lblVolume 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6240
      TabIndex        =   34
      Top             =   2940
      Width           =   1110
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "Volume"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   6300
      TabIndex        =   33
      Top             =   2760
      Width           =   630
   End
   Begin VB.Label lblPeso 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5160
      TabIndex        =   32
      Top             =   2940
      Width           =   810
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      Caption         =   "Peso"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5220
      TabIndex        =   31
      Top             =   2760
      Width           =   435
   End
   Begin VB.Label lblEdi 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3180
      TabIndex        =   30
      Top             =   2940
      Width           =   1590
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Cod.F�brica EDI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3240
      TabIndex        =   29
      Top             =   2760
      Width           =   1410
   End
   Begin VB.Label lblClass_Fiscal 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   2940
      Width           =   1590
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "Class.Fiscal"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   180
      TabIndex        =   27
      Top             =   2760
      Width           =   1020
   End
   Begin VB.Label lblMascarado 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7500
      TabIndex        =   26
      Top             =   2340
      Width           =   1590
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "Mascarado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   7560
      TabIndex        =   25
      Top             =   2160
      Width           =   945
   End
   Begin VB.Label lblSubgrupo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5220
      TabIndex        =   24
      Top             =   2340
      Width           =   2190
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "Subgrupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5280
      TabIndex        =   23
      Top             =   2160
      Width           =   825
   End
   Begin VB.Label lblGrupo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2580
      TabIndex        =   22
      Top             =   2340
      Width           =   2550
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2640
      TabIndex        =   21
      Top             =   2160
      Width           =   525
   End
   Begin VB.Label lblLinha 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   20
      Top             =   2340
      Width           =   2370
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Linha"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   180
      TabIndex        =   19
      Top             =   2160
      Width           =   480
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      X1              =   60
      X2              =   11700
      Y1              =   2040
      Y2              =   2040
   End
   Begin VB.Label Label6 
      Caption         =   "EAN"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6420
      TabIndex        =   16
      Top             =   900
      Width           =   435
   End
   Begin VB.Label lblDescr 
      AutoSize        =   -1  'True
      Caption         =   "Descri��o"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   14
      Top             =   1380
      Width           =   870
   End
   Begin VB.Label Label5 
      Caption         =   "DPA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4020
      TabIndex        =   9
      Top             =   840
      Width           =   435
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Original"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   840
      Width           =   660
   End
   Begin VB.Label Label3 
      Caption         =   "DPK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7740
      TabIndex        =   5
      Top             =   180
      Width           =   435
   End
   Begin VB.Label Label2 
      Caption         =   "Cod.F�brica"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3300
      TabIndex        =   3
      Top             =   240
      Width           =   1095
   End
   Begin VB.Label lblSIGLA 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1500
      TabIndex        =   2
      Top             =   240
      Width           =   1710
   End
   Begin VB.Label Label1 
      Caption         =   "Fornecedor"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "frmGeral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub GetItem(lngCod_dpk As Long, iCOD_FORNECEDOR As Integer, strCOD_FABRICA As String)
    On Error GoTo TrataErro
    
    Dim ss2 As Object
    Dim ss3 As Object
    Static FechaConsultaItem As Boolean
    
    'monta SQL
    SQL = "select a.cod_dpk,a.cod_fornecedor,a.cod_fabrica,"
    SQL = SQL & "   a.desc_item, a.mascarado, a.class_fiscal,"
    SQL = SQL & "   a.peso,a.volume,"
    SQL = SQL & "   a.cod_procedencia,"
    SQL = SQL & "   a.cod_tributacao_ipi,"
    SQL = SQL & "   a.pc_ipi,a.qtd_minvda,"
    SQL = SQL & "   a.qtd_minforn,"
    SQL = SQL & "   to_char(a.dt_cadastramento,'dd/mm/rr') dt_cadastramento,"
    SQL = SQL & "   a.cod_linha||'-'||b.desc_linha desc_linha,"
    SQL = SQL & "   a.cod_grupo||'-'||c.desc_grupo desc_grupo,"
    SQL = SQL & "   a.cod_subgrupo||'-'||d.desc_subgrupo desc_subgrupo,"
    SQL = SQL & "   to_char(e.cod_ean) cod_ean, f.gruite, f.sbgruite,"
    SQL = SQL & "   f.nrite , f.dgite, g.sigla, h.cod_fabrica_edi,"
    SQL = SQL & "   a.cod_dpk_ant"
    SQL = SQL & " from item_cadastro a, linha b, grupo c,"
    SQL = SQL & "   subgrupo d, autolog.r_item_ean e,"
    SQL = SQL & "   dep06.r_item_dpk_dpa f, fornecedor g,"
    SQL = SQL & "   compras.r_item_edi h"
    SQL = SQL & " where "
    If lngCod_dpk > 0 Then
        SQL = SQL & " a.COD_DPK = :dpk"
    ElseIf iCOD_FORNECEDOR <> "-1" And strCOD_FABRICA <> "-1" Then
        SQL = SQL & " a.COD_FORNECEDOR = :forn and"
        SQL = SQL & " a.COD_FABRICA = :fabrica"
    Else
        'Inicializar frame de preco de venda
        'Call InitFrmeProduto
        Exit Sub
    End If
    SQL = SQL & " and a.cod_dpk = h.cod_dpk(+) and"
    SQL = SQL & " a.cod_fornecedor = g.cod_fornecedor and"
    SQL = SQL & " a.cod_dpk = f.cod_dpk(+) and"
    SQL = SQL & " a.cod_dpk = e.cod_dpk(+) and"
    SQL = SQL & " c.cod_grupo = d.cod_grupo and"
    SQL = SQL & " a.cod_subgrupo = d.cod_subgrupo and"
    SQL = SQL & " a.cod_grupo = c.cod_grupo and"
    SQL = SQL & " a.cod_linha = b.cod_linha"
  
    oradatabase.Parameters.Remove "forn"
    oradatabase.Parameters.Remove "dpk"
    oradatabase.Parameters.Remove "fabrica"
    oradatabase.Parameters.Remove "loja"
       
    oradatabase.Parameters.Add "forn", CStr(iCOD_FORNECEDOR), 1
    oradatabase.Parameters.Add "dpk", CStr(lngCod_dpk), 1
    oradatabase.Parameters.Add "fabrica", strCOD_FABRICA, 1
    
    
    Set ssItem = oradatabase.dbcreatedynaset(SQL, 8&)
    
    
    If ssItem.EOF() And ssItem.BOF Then
        MsgBox "Item n�o Cadastrado", vbInformation, "Aten��o"
        Call ReInitProduto
    Else
       'carregar dados
        txtCOD_FORNECEDOR = IIf(IsNull(ssItem!cod_fornecedor), "", ssItem!cod_fornecedor)
        lblSIGLA = IIf(IsNull(ssItem!sigla), "", ssItem!sigla)
        txtCOD_FABRICA = IIf(IsNull(ssItem!COD_FABRICA), "", ssItem!COD_FABRICA)
        txtCOD_DPK = IIf(IsNull(ssItem!cod_dpk), "", ssItem!cod_dpk)
        txtEan = IIf(IsNull(ssItem!cod_ean), "", ssItem!cod_ean)
        txtDescricao = IIf(IsNull(ssItem!DESC_ITEM), "", ssItem!DESC_ITEM)
        txtGruite = IIf(IsNull(ssItem!gruite), "", ssItem!gruite)
        txtSbgruite = IIf(IsNull(ssItem!sbgruite), "", ssItem!sbgruite)
        txtNrite = IIf(IsNull(ssItem!nrite), "", ssItem!nrite)
        txtDgite = IIf(IsNull(ssItem!dgite), "", ssItem!dgite)
        lblLinha = IIf(IsNull(ssItem!desc_linha), "", ssItem!desc_linha)
        lblGrupo = IIf(IsNull(ssItem!desc_grupo), "", ssItem!desc_grupo)
        lblSubgrupo = IIf(IsNull(ssItem!desc_subgrupo), "", ssItem!desc_subgrupo)
        lblMascarado = IIf(IsNull(ssItem!mascarado), "", ssItem!mascarado)
        lblClass_Fiscal = IIf(IsNull(ssItem!class_fiscal), "", ssItem!class_fiscal)
        lblPeso = Format(IIf(IsNull(ssItem!peso), 0, ssItem!peso), "0.000")
        lblVolume = Format(IIf(IsNull(ssItem!volume), 0, ssItem!volume), "0.000")
        If ssItem!cod_procedencia = 0 Then
           lblProcedencia = "0-PROD.NACIONAL FABRICADO TERCEIRO"
        ElseIf ssItem!cod_procedencia = 1 Then
           lblProcedencia = "1-PROD.ESTRANGEIRO IMP. DIRETA"
        ElseIf ssItem!cod_procedencia = 2 Then
           lblProcedencia = "1-PROD.ESTRANGEIRO ADQ. MERCADO INTERNO"
        Else
          lblProcedencia = ""
        End If
        If ssItem!cod_tributacao_ipi = 0 Then
          lblIPI = "0-N�O INCIDE"
        ElseIf ssItem!cod_tributacao_ipi = 1 Then
          lblIPI = "1-TRIBUTADO"
        ElseIf ssItem!cod_tributacao_ipi = 2 Then
          lblIPI = "2-ISENTO"
        ElseIf ssItem!cod_tributacao_ipi = 9 Then
          lblIPI = "9-OUTRAS"
        Else
          lblIPI = ""
        End If
        
        lblPC_IPI = Format(IIf(IsNull(ssItem!pc_ipi), 0, ssItem!pc_ipi), "00.00")
        lblQtd_minvda = IIf(IsNull(ssItem!qtd_minvda), 0, ssItem!qtd_minvda)
        lblQtd_MinForn = IIf(IsNull(ssItem!qtd_minforn), 0, ssItem!qtd_minforn)
        lblCadastro = IIf(IsNull(ssItem!dt_cadastramento), "", ssItem!dt_cadastramento)
        lblEdi = IIf(IsNull(ssItem!cod_fabrica_edi), "", ssItem!cod_fabrica_edi)
        lngDPK_Ant = IIf(IsNull(ssItem!cod_dpk_ant), 0, ssItem!cod_dpk_ant)
        
        SQL = "Select distinct(pc_margem_lucro) mg"
        SQL = SQL & " From compras.subst_tributaria"
        SQL = SQL & " Where class_fiscal = :class and"
        SQL = SQL & " pc_margem_lucro <> 0 "
        
        oradatabase.Parameters.Remove "class"
        oradatabase.Parameters.Add "class", lblClass_Fiscal, 1
        
        Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
        
        If ss.EOF And ss.BOF Then
          lblMg_Lucro = Format(0, "0.00")
        Else
          lblMg_Lucro = Format(ss!mg, "0.00")
        End If
        
        
    End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
    
End Sub


Private Sub GetFornecedor(iCOD_FORNECEDOR As Integer)
    On Error GoTo TrataErro

    
    Dim ss As Object
    
    'criar consulta
          SQL = "select nvl(SIGLA,' ') sigla, SITUACAO "
    SQL = SQL & " from FORNECEDOR "
    SQL = SQL & " where SITUACAO = 0 and "
    SQL = SQL & " COD_FORNECEDOR = :forn"
        
    oradatabase.Parameters.Remove "forn"
    oradatabase.Parameters.Add "forn", iCOD_FORNECEDOR, 1
    
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If ss.EOF() And ss.BOF Then
        MsgBox "Fornecedor n�o cadastrado", vbInformation, "Aten��o"
        frmGeral.txtCOD_FORNECEDOR.SetFocus
        'Inicializar frame
        'Call InitFrmeProduto
    ElseIf ss!situacao <> 0 Then
        MsgBox "Fornecedor Desativado", vbInformation, "Aten��o"
        'Inicializar frame
        'Call InitFrmeProduto
    Else
        'carregar dados
        lblSIGLA.Caption = ss!sigla.Value
        'Reinicializar frame
        'Call ReInitProduto
        'Habilita codigo de fabrica
        'If qtd = 0 Then
        '  TXTCOD_FABRICA.Enabled = True
        '  TXTCOD_FABRICA.SetFocus
        'End If
    End If
    
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
      Resume
    ElseIf Err = 5 Then
      Resume Next
    Else
      Call Process_Line_Errors(SQL)
    End If
    
End Sub


Private Sub cmdAplicacao_Click()
    On Error GoTo TrataErro
    
    
    Dim ss As Object
    Dim i As Byte
    Dim COD As Long
    Dim strMontAnterior As String
    Dim strOriginal As String
    
    
  If txtCOD_DPK <> "0" And txtCOD_DPK <> "" Then
    'montar SQL
          SQL = "select a.desc_montadora,"
    SQL = SQL & " b.desc_aplicacao, b.cod_original"
    SQL = SQL & " from montadora a, aplicacao b"
    SQL = SQL & " where a.cod_montadora = b.cod_montadora and"
    SQL = SQL & " b.cod_dpk = :cod"
    SQL = SQL & " order by b.cod_montadora, b.cod_categoria, b.sequencia"
    
    'criar consulta
    COD = txtCOD_DPK
    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Add "cod", COD, 1
        
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
           
    If ss.EOF And ss.BOF Then
        MsgBox "N�o ha aplica��o cadastrada para este item", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    'carrega dados no frame de aplicacao
    frmAplicacao.lblDESC_ITEM.Caption = txtDescricao
    
    With frmAplicacao.grdAplicacao
        .Cols = 3
        .Rows = ss.recordcount
        .ColWidth(0) = 1750
        .ColWidth(1) = 5500
        .ColWidth(2) = 1800
        If .Rows > 8 Then
            frmAplicacao.Width = 8980
            frmAplicacao.cmdSair.Left = 7480
            frmAplicacao.lblCabec(1).Width = 6820
        Else
            frmAplicacao.Width = 8695
            frmAplicacao.cmdSair.Left = 7240
            frmAplicacao.lblCabec(1).Width = 6550
        End If
        
         For i = 0 To .Rows - 1
            .Row = i
            .Col = 0
            If strMontAnterior <> ss!DESC_MONTADORA Then
                .Text = ss!DESC_MONTADORA
                strMontAnterior = ss!DESC_MONTADORA
            Else
                .Text = ""
            End If
            .Col = 1
            .Text = ss!DESC_APLICACAO
            .Col = 2
            If strOriginal <> ss!cod_original Then
                .Text = ss!cod_original
                strOriginal = ss!cod_original
            Else
                .Text = ""
            End If
            
            
            ss.MoveNext
        Next
        
    End With
    
        
    'mostra a aplica��o
    frmAplicacao.Show vbModal
  End If
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
End Sub

Private Sub cmdBuscar_Click()
  Dim ss As Object
  Screen.MousePointer = 11
  
  If txtGruite <> "" And txtSbgruite <> "" And _
     txtNrite <> "" And txtDgite <> "" Then
     SQL = "Select cod_dpk"
     SQL = SQL & " From dep06.r_item_dpk_dpa"
     SQL = SQL & " where dgite = :dg and"
     SQL = SQL & " nrite = :nr and"
     SQL = SQL & " sbgruite = :sbgruite and"
     SQL = SQL & " gruite = :gruite"
     
     oradatabase.Parameters.Remove "dg"
     oradatabase.Parameters.Add "dg", txtDgite, 1
     oradatabase.Parameters.Remove "nr"
     oradatabase.Parameters.Add "nr", txtNrite, 1
     oradatabase.Parameters.Remove "sbgruite"
     oradatabase.Parameters.Add "sbgruite", txtSbgruite, 1
     oradatabase.Parameters.Remove "gruite"
     oradatabase.Parameters.Add "gruite", txtGruite, 1
     
     Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
     
     If ss.EOF And ss.BOF Then
       MsgBox "ITEM DPA N�O CADASTRADO NO BANCO DE DADOS DPK", vbInformation, "ATEN��O"
       ReInitProduto
       Screen.MousePointer = 0
       Exit Sub
     Else
       txtCOD_DPK = ss!cod_dpk
     End If
   End If
   
   If txtOriginal <> "" Then
     Screen.MousePointer = 11
     'montar SQL
      SQL = "select b.COD_FORNECEDOR,"
      SQL = SQL & " b.COD_FABRICA,a.COD_DPK,a.cod_original "
      SQL = SQL & " from aplicacao a, item_cadastro b "
      SQL = SQL & " where   "
      SQL = SQL & " a.cod_original like :original and"
      SQL = SQL & " a.cod_dpk=b.cod_dpk "
      SQL = SQL & " Group by b.cod_fornecedor,b.cod_fabrica,a.cod_dpk,a.cod_original"
      'criar consulta
      oradatabase.Parameters.Remove "original"
      oradatabase.Parameters.Add "original", frmGeral.txtOriginal, 1
    
      Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
      If ss.EOF And ss.BOF Then
          Screen.MousePointer = vbDefault
          MsgBox "N�o ha item cadastrado para este nr.original", vbInformation, "Aten��o"
          frmGeral.txtOriginal = ""
          'Unload Me
          Exit Sub
      End If
      
      'carrega dados
      frmOrig.lblOriginal = frmGeral.txtOriginal
      With frmOrig.grdOriginal
          .Cols = 4
          .Rows = ss.recordcount + 1
          .ColWidth(0) = 660
          .ColWidth(1) = 3000
          .ColWidth(2) = 800
          .ColWidth(3) = 1500
          .Row = 0
          .Col = 0
          .Text = "Forn."
          .Col = 1
          .Text = "Fabrica"
          .Col = 2
          .Text = "DPK"
          .Col = 3
          .Text = "Original"
        
          ss.MoveFirst
          For i = 1 To .Rows - 1
              .Row = i
            
              .Col = 0
              .Text = ss!cod_fornecedor
              .Col = 1
              .Text = ss!COD_FABRICA
              .Col = 2
              .Text = ss!cod_dpk
              .Col = 3
              .Text = ss!cod_original
              
            
              ss.MoveNext
          Next
          .Row = 1
      End With
     Screen.MousePointer = 0
     frmOrig.Show 1
     If txtOriginal.Text <> "" And txtCOD_DPK.Text <> "" Then
        Call GetItem(txtCOD_DPK.Text, -1, -1)
     End If
   End If
   
   If txtCOD_DPK <> "" Then
      Call GetItem(txtCOD_DPK, -1, -1)
   ElseIf txtCOD_FORNECEDOR <> "" And txtCOD_FABRICA <> "" Then
      Call GetItem(0, txtCOD_FORNECEDOR, txtCOD_FABRICA)
   End If
   cmdBuscar.SetFocus
   Screen.MousePointer = 0
End Sub

Private Sub cmdCD_Click()
  If txtCOD_DPK <> "0" And txtCOD_DPK <> "" Then
    frmCD.Show 1
  End If
End Sub

Private Sub cmdEquivalencia_Click()
    
  On Error GoTo TrataErro
    
    Dim ss As Object
    Dim i As Integer
    
  If txtCOD_DPK = "" Or txtCOD_DPK = "0" Then
    MsgBox "Selecione um Item, antes de verificar a convers�o", vbInformation, "Aten��o"
    Exit Sub
  End If
    
    If frmGeral.txtCOD_DPK <> "" Then
      'mouse
      Screen.MousePointer = vbHourglass
    
      'montar SQL
      SQL = "select b.COD_FORNECEDOR,"
      SQL = SQL & " b.COD_FABRICA,a.COD_DPK_EQ dpk "
      SQL = SQL & " from r_dpk_equiv a, item_cadastro b "
      SQL = SQL & " where   "
      SQL = SQL & " a.cod_dpk = :dpk and"
      SQL = SQL & " a.cod_dpk_eq=b.cod_dpk"
      SQL = SQL & " Group by b.cod_fornecedor,b.cod_fabrica,a.cod_dpk_eq"
      'criar consulta
      oradatabase.Parameters.Remove "dpk"
      oradatabase.Parameters.Add "dpk", frmGeral.txtCOD_DPK, 1
    
      Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
      If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o h� item equivalente cadastrado para o item escolhido", vbInformation, "Aten��o"
        Exit Sub
      End If
      
      
    End If
    
      'carrega dados
      frmOrig.Caption = "Lista de Convers�o de Itens"
      frmOrig.Label1.Visible = False
      frmOrig.lblOriginal.Visible = False
      With frmOrig.grdOriginal
          .Cols = 3
          .Rows = ss.recordcount + 1
          .ColWidth(0) = 660
          .ColWidth(1) = 3000
          .ColWidth(2) = 800
          .Row = 0
          .Col = 0
          .Text = "Forn."
          .Col = 1
          .Text = "Fabrica"
          .Col = 2
          .Text = "DPK"
        
        
          ss.MoveFirst
          For i = 1 To .Rows - 1
              .Row = i
            
              .Col = 0
              .Text = ss!cod_fornecedor
              .Col = 1
              .Text = ss!COD_FABRICA
              .Col = 2
              .Text = ss!dpk
            
              ss.MoveNext
          Next
          .Row = 1
      End With
    
            
      'mouse
      Screen.MousePointer = vbDefault
      frmOrig.Show 1
      If txtCOD_DPK <> "" Then
        Call GetItem(txtCOD_DPK, -1, -1)
        Fabrica = txtCOD_FABRICA.Text
      End If
   'End If
   
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
    
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
End Sub

Private Sub cmdLimpar_Click()
  Screen.MousePointer = 11
  Unload frmFabrica
  Unload frmOrig
  ReInitProduto
  Screen.MousePointer = 0
End Sub

Private Sub cmdSubst_Click()
  Screen.MousePointer = 11
  If lngDPK_Ant = 0 Then
    MsgBox "Este item n�o foi substituido", vbInformation, "ATEN��O"
    Screen.MousePointer = 0
    Exit Sub
  Else
    Call GetItem(lngDPK_Ant, -1, -1)
  End If
  Screen.MousePointer = 0
End Sub


Private Sub Form_Load()
  ReInitProduto
End Sub


Private Sub txtCOD_DPK_Change()
  If Len(txtCOD_DPK) = 0 Then
    ReInitProduto
  End If
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
      KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCOD_DPK_LostFocus()
  'If txtCOD_DPK <> "" Then
  '  txtCOD_FORNECEDOR = ""
  '  lblSIGLA = ""
  '  TXTCOD_FABRICA = ""
  'End If
End Sub


Private Sub TXTCOD_FABRICA_DblClick()
    
    txtCOD_FABRICA.DataChanged = False
    txtResposta = txtCOD_FORNECEDOR
    
    frmFabrica.Show 1
      
    If txtResposta <> "" Then
       txtCOD_FABRICA.Text = txtResposta
       Call TXTCOD_FABRICA_LostFocus
    End If
    
End Sub

Private Sub TXTCOD_FABRICA_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub TXTCOD_FABRICA_LostFocus()
  On Error GoTo TrataErro

        If txtCOD_FABRICA.Text = "" Then
            'Inicializar frame
            'Call InitFrmeProduto
            txtDescricao.Enabled = True
         Else
           If txtCOD_FABRICA.DataChanged Then
             'carrega dados do item
             pesq = "%"
             If InStr(1, txtCOD_FABRICA, pesq) > 0 And txtCOD_FABRICA.Text <> "" Then
                frmFabrica.Show 1
                If InStr(1, txtCOD_FABRICA, pesq) = 0 And txtCOD_FABRICA.Text <> "" Then
                  Call GetItem(-1, txtCOD_FORNECEDOR, txtCOD_FABRICA.Text)
                  Fabrica = txtCOD_FABRICA.Text
                  
                Else
                  Exit Sub
                End If
             Else
                Call GetItem(-1, txtCOD_FORNECEDOR, txtCOD_FABRICA.Text)
                Fabrica = txtCOD_FABRICA.Text
             End If
         End If
       End If
Exit Sub

TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
      Label1.Visible = True
      contador = contador + 1
      Label1.Caption = contador
      Resume
   ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
      MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
      txtCOD_FABRICA.Text = ""
      Exit Sub
   Else
      Call Process_Line_Errors(SQL)
   End If


End Sub


Private Sub txtCOD_FORNECEDOR_Change()
   Unload frmFabrica
  If Len(txtCOD_FORNECEDOR.Text) = 3 Then
     Call txtCOD_FORNECEDOR_LostFocus
  End If
  If txtCOD_FORNECEDOR = "" Then
    ReInitProduto
  End If
End Sub

Private Sub txtCOD_FORNECEDOR_DblClick()
  txtResposta = "0"
  frmFornecedor.Show vbModal
  If txtResposta <> "0" Then
      txtCOD_FORNECEDOR.Text = txtResposta
      txtCOD_FORNECEDOR.DataChanged = True
      Call txtCOD_FORNECEDOR_LostFocus
  End If
End Sub

Private Sub txtCOD_FORNECEDOR_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCOD_FORNECEDOR_LostFocus()
   If txtCOD_FORNECEDOR <> "" Then
    Call GetFornecedor(txtCOD_FORNECEDOR.Text)
   End If
  
End Sub


Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtDescricao_LostFocus()
    If txtDescricao.DataChanged Then
        If txtDescricao.Text = "" And txtCOD_FABRICA.Text = "" Then
            'Inicializar frame
            'Call InitFrmeProduto
        Else
            'carrega dados do item
            If txtCOD_FORNECEDOR = "" Then
              MsgBox "Para pesquisar por descri��o � necess�rio informar o fornecedor", vbInformation, "ATEN��O"
              Exit Sub
            Else
              pesq = "%"
              If InStr(1, frmGeral.txtDescricao, pesq) > 0 Then
                 frmFabrica.Show 1
                 If txtResposta <> "" Then
                   txtCOD_FABRICA.Text = txtResposta
                   Call TXTCOD_FABRICA_LostFocus
                 End If
              End If
            End If
        End If
    End If
    
End Sub


Private Sub txtDgite_KeyPress(KeyAscii As Integer)
      KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtEan_KeyPress(KeyAscii As Integer)
      KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtGruite_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtNrite_KeyPress(KeyAscii As Integer)
      KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtOriginal_KeyPress(KeyAscii As Integer)
   KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtOriginal_LostFocus()
  If txtOriginal <> "" Then
      txtCOD_FORNECEDOR = ""
      lblSIGLA = ""
      txtCOD_FABRICA = ""
      txtCOD_DPK = ""
  End If
End Sub


Private Sub txtSbgruite_KeyPress(KeyAscii As Integer)
      KeyAscii = Numerico(KeyAscii)
End Sub



Public Sub ReInitProduto()
  
  txtCOD_FORNECEDOR = ""
  lblSIGLA = ""
  txtCOD_FABRICA = ""
  txtCOD_DPK = ""
  txtOriginal = ""
  txtGruite = ""
  txtSbgruite = ""
  txtNrite = ""
  txtDgite = ""
  txtEan = ""
  txtDescricao = ""
  lblLinha = ""
  lblGrupo = ""
  lblSubgrupo = ""
  lblMascarado = ""
  lblClass_Fiscal = ""
  lblEdi = ""
  lblPeso = ""
  lblVolume = ""
  lblProcedencia = ""
  lblIPI = ""
  lblPC_IPI = ""
  lblQtd_minvda = ""
  lblQtd_MinForn = ""
  lblCadastro = ""
  lngDPK_Ant = 0
  lblMg_Lucro = ""
  
End Sub
