VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmOrig 
   Caption         =   "Lista de Itens por C�digo Original"
   ClientHeight    =   3225
   ClientLeft      =   1305
   ClientTop       =   3150
   ClientWidth     =   7545
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3225
   ScaleWidth      =   7545
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5040
      TabIndex        =   0
      Top             =   2760
      Width           =   1095
   End
   Begin MSGrid.Grid grdOriginal 
      Height          =   2175
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   7035
      _Version        =   65536
      _ExtentX        =   12409
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "Frmorig.frx":0000
   End
   Begin VB.Label Label1 
      Caption         =   "N�mero Original"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblOriginal 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   255
      Left            =   2160
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmOrig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
   lblOriginal = ""
  Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ' Set frmOrig = Nothing
End Sub


Private Sub grdOriginal_DblClick()
  grdOriginal.Col = 2
  frmGeral.txtCOD_DPK = grdOriginal.Text
  frmOrig.Hide
  
End Sub

