VERSION 5.00
Begin VB.Form frmTran1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PROCESSA ARQUIVO DETERIORADOS - GERA PEDIDO"
   ClientHeight    =   3105
   ClientLeft      =   840
   ClientTop       =   2910
   ClientWidth     =   11745
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3105
   ScaleWidth      =   11745
   Begin VB.CommandButton Command2 
      Caption         =   "Sair"
      Height          =   735
      Left            =   7560
      TabIndex        =   3
      Top             =   1320
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Gera Pedidos"
      Height          =   735
      Left            =   7560
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.ListBox lstPedido 
      ForeColor       =   &H00800000&
      Height          =   1815
      ItemData        =   "frmTran1.frx":0000
      Left            =   1320
      List            =   "frmTran1.frx":0002
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.Label lblStatus 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   1
      Top             =   2520
      Width           =   615
   End
End
Attribute VB_Name = "frmTran1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
 Call Verifica_Arquivo
End Sub

Private Sub Command2_Click()
 Unload Me
End Sub

Public Sub Verifica_Arquivo()
  Dim TMP_LINHA As String
  Dim strChar As String
  Dim strLinha As String
  Dim strTp_Linha As String * 1
  Dim SQL1 As String
  Dim dDigitacao As Date
  Dim dProxSeg As Date
  Dim lngLoja As Long
  Dim lngCod_Cliente As Long
  Dim lngCod_DPK As Long
  Dim lngQtde As Long
  Dim dblPreco As Double
  Dim lngNum_Pedido As Long
  Dim dblVl_Contabil As Long
  Dim i As Integer
  Dim dblPC_IPI As Double
  Dim strCod_Nope As String
  
  On Error GoTo Trata_Erro
  
NOVO_ARQUIVO:
  lblStatus = "VERIFICANDO ARQUIVO DE DETERIORADOS"
  Screen.MousePointer = 11
  lstPedido.Visible = True
  DoEvents
 'montar SQL
  V_SQL = "Begin producao.pck_VDA841.pr_deposito(:vCursor,:vErro);END;"

  
  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
  
  i = 0
  If Dir(strDiretorio & "deteriorados*" & ".TXT") = "" Then ' N�O EXISTE TRANSFERENCIA
   lblStatus = "N�O H� ARQUIVO DE DETERIORADOS PARA PROCESSAR (diret�rio:" & strDiretorio & "deteriorados*" & ".TXT" & ")"
   Screen.MousePointer = 0
   DoEvents
   Exit Sub
  Else
    
    
    Do While Dir(strDiretorio & "deteriorados*.TXT") <> ""
      strArquivo = strDiretorio & Dir(strDiretorio & "deteriorados*.TXT")
      i = 0
      
      'montar SQL
    V_SQL = "Begin producao.pck_VDA841.pr_num_pedido(:vCursor,:vErro);END;"

    oradatabase.ExecuteSQL V_SQL
  
    Set ss = oradatabase.Parameters("vCursor").Value
    
    lngNum_Pedido = ss(0)
      
      
      lblStatus = "PROCESSANDO ARQUIVO DE DETERIORADOS: " & strArquivo
      DoEvents
      lstPedido.Visible = True
      lngCod_Cliente = 0
      
      Open strArquivo For Input As #1
        Line Input #1, strLinha
        Do While Not EOF(1)
inicio:
               If Mid(strLinha, 1, 2) = "01" Then
                  lngLoja = Mid(strLinha, 3, 2)
                  lngCod_Cliente = Mid(strLinha, 5, 6)
                  strCod_Nope = Mid(strLinha, 11, 3)
                  
                 If strCod_Nope <> "Q06" Then
                   Line Input #1, strLinha
                   lstPedido.AddItem "Arquivo n�o processado, natureza de opera��o n�o est� correta"
                   GoTo FIM
                 End If
                  
                  
                  'verifica cliente
                 OraParameters.Remove "cliente"
                 OraParameters.Add "cliente", lngCod_Cliente, 1
                 V_SQL = "Begin producao.pck_VDA841.pr_cliente(:vCursor,:cliente,:vErro);END;"

                 oradatabase.ExecuteSQL V_SQL
  
                 Set ss = oradatabase.Parameters("vCursor").Value
  
                 If oradatabase.Parameters("vErro").Value <> 0 Then
                   Line Input #1, strLinha
                   lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " N�O CADASTRADO, PEDIDO N�O GERADO"
                   GoTo FIM
                 End If
                 If ss!situacao <> 0 Then
                   Line Input #1, strLinha
                   lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " DESATIVADO, PEDIDO N�O GERADO"
                   GoTo FIM
                 End If
               
                  Line Input #1, strLinha
                  
              End If
               
              If Mid(strLinha, 1, 2) = "02" Then
                lngCod_DPK = Mid(strLinha, 3, 5)
                lngQtde = Mid(strLinha, 8, 6)
                
                'CHAMA PROCEDURE QUE BUSCA O CUSTO MEDIO DO
                ' PRODUTO
                'verifica dpk
                OraParameters.Remove "loja"
                OraParameters.Add "loja", lngLoja, 1
                OraParameters.Remove "dpk"
                OraParameters.Add "dpk", lngCod_DPK, 1
                V_SQL = "Begin producao.pck_VDA841.pr_PRECO_CUSTO(:vCursor,:loja,:dpk,:vErro);END;"
                
                oradatabase.ExecuteSQL V_SQL
  
                Set ss = oradatabase.Parameters("vCursor").Value
                
                If ss.EOF And ss.BOF Then
                  dblPreco = 0
                Else
                  dblPreco = IIf(IsNull(ss(0)), 0, ss(0))
                End If
                
                If dblPreco = 0 Then
                  Line Input #1, strLinha
                  lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ITEM SEM CUSTO MEDIO"
                  GoTo FIM
                End If
                                
                
                dblPC_IPI = 0
                i = i + 1
                dblVl_Contabil = CDbl(dblVl_Contabil) + (CDbl(dblPreco) * CInt(lngQtde))
                
                If i > 500 Then
                  lstPedido.AddItem "PEDIDO N�O GERADO, ARQUIVO COM MAIS DE 500 ITENS"
                  DoEvents
                  GoTo FIM
                End If
              
                'verifica dpk
                OraParameters.Remove "loja"
                OraParameters.Add "loja", lngLoja, 1
                OraParameters.Remove "dpk"
                OraParameters.Add "dpk", lngCod_DPK, 1
                V_SQL = "Begin producao.pck_VDA841.pr_item(:vCursor,:loja,:dpk,:vErro);END;"

                oradatabase.ExecuteSQL V_SQL
  
                Set ss = oradatabase.Parameters("vCursor").Value
  
                If oradatabase.Parameters("vErro").Value <> 0 Then
                  Line Input #1, strLinha
                  lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ITEM N�O ENCONTRADO"
                  GoTo FIM
                End If
               
                If ss!situacao <> 0 Then
                  Line Input #1, strLinha
                  lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ITEM DESATIVADO"
                  DoEvents
                  GoTo FIM
                End If
                
               'executa procedure
                OraParameters.Remove "loja"
                OraParameters.Add "loja", lngLoja, 1
                OraParameters.Remove "cliente"
                OraParameters.Add "cliente", lngCod_Cliente, 1
                OraParameters.Remove "pedido"
                OraParameters.Add "pedido", lngNum_Pedido, 1
                OraParameters.Remove "NUM_ITEM"
                OraParameters.Add "NUM_ITEM", i, 1
                OraParameters.Remove "dpk"
                OraParameters.Add "dpk", lngCod_DPK, 1
                OraParameters.Remove "qtd"
                OraParameters.Add "qtd", lngQtde, 1
                OraParameters.Remove "preco"
                OraParameters.Add "preco", CDbl(dblPreco), 1
                OraParameters.Remove "ipi"
                OraParameters.Add "ipi", CDbl(dblPC_IPI), 1
                OraParameters.Remove "nope"
                OraParameters.Add "nope", "Q06", 1
                
                
               
                V_SQL = "Begin producao.pck_VDA841.pr_ITEM_PEDIDO(:loja,:cliente,:pedido,"
                V_SQL = V_SQL & ":NUM_ITEM,:dpk,:qtd,:preco,:ipi,:nope, :vErro);END;"

                oradatabase.ExecuteSQL V_SQL
                DoEvents
                If oradatabase.Parameters("vErro").Value <> 0 Then
                    lstPedido.AddItem "Ocorreu o seguinte erro na grava��o dos itens: " & oradatabase.Parameters("vErro").Value
                    GoTo FIM
                End If
                
                
                
                Line Input #1, strLinha
                If strLinha <> "" Then
                  GoTo inicio
                End If
             End If
               
              
               
         Loop
         
          'executa procedure
           OraParameters.Remove "loja"
           OraParameters.Add "loja", lngLoja, 1
           OraParameters.Remove "cliente"
           OraParameters.Add "cliente", lngCod_Cliente, 1
           OraParameters.Remove "pedido"
           OraParameters.Add "pedido", lngNum_Pedido, 1
           OraParameters.Remove "QTD_ITEM"
           OraParameters.Add "QTD_ITEM", i, 1
           OraParameters.Remove "contabil"
           OraParameters.Add "contabil", CDbl(dblVl_Contabil), 1
           OraParameters.Remove "nope"
           OraParameters.Add "nope", "Q06", 1
               
           V_SQL = "Begin producao.pck_VDA841.pr_cabec_PEDIDO(:loja,:cliente,0,:qtd_item,:contabil,:pedido,:nope,:vErro);END;"

           oradatabase.ExecuteSQL V_SQL
           If oradatabase.Parameters("vErro").Value <> 0 Then
             lstPedido.AddItem "PEDIDO N�O GERADO, OCORREU O SEGUINTE ERRO: " & oradatabase.Parameters("vErro").Value
             GoTo FIM
           End If
               
               
           lblStatus = "PROCESSADO O PEDIDO : " & oradatabase.Parameters("pedido").Value & " PARA O FORNECEDOR: " & lngCod_Cliente
           DoEvents
           lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO GERADO: " & oradatabase.Parameters("pedido").Value
           DoEvents
               
         lblStatus = ""
         Call Salva_Arquivo(strArquivo)
    Loop
    
  End If
  
  MsgBox "ARQUIVO PROCESSADO COM SUCESSO", vbInformation, "Aten��o"
  lngNum_Pedido = 0
  Screen.MousePointer = 0
  GoTo NOVO_ARQUIVO
  Exit Sub
  
  
FIM:
  Call Salva_Arquivo(strArquivo)
  MsgBox "ARQUIVO NAO FOI PROCESSADO", vbInformation, "Aten��o"
  lngNum_Pedido = 0
  GoTo NOVO_ARQUIVO
  Screen.MousePointer = 0
  
  
  Exit Sub
Trata_Erro:
  If Err = 62 Then
    strLinha = ""
    Resume Next
  Else
    MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description
    Resume
  End If
  
End Sub

Private Sub List1_Click()

End Sub

Public Sub Salva_Arquivo(strArquivo As String)
  Close #1
  FileCopy strArquivo, strDiretorio_Salva & "DETERIORADOS" & Format(Now, "DDMMYY_hhmmss") & ".TXT"
  
  Kill strArquivo
  
End Sub


Private Sub Timer1_Timer()

End Sub


