Attribute VB_Name = "Module2"
Option Explicit



Public orasession As Object
Public oradatabase As Object
Public oradynaset As Object
Public strPath As String
Public strPathUsuario As String
Public bFl_banco As Byte
Public strUser As String
Public strDiretorio_Salva As String

Public SQL As String
Public pl As String
Public dt_faturamento As Date
Public dt_ini_fech_dia As Date
Public dt_ini_fech_mensal As Date
Public dt_fin_fech_mensal As Date

Public cod_errora As Integer
Public txt_errora As String
Public txtresposta As String
Public ss As Object

Public cont As Long
Public strDiretorio As String
Public lngNum_Cotacao As Long
Public lngCod_Loja As Long
Public strArquivo As String
Public lngCod_Loja_Destino As Long

Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public V_SQL As String
Public bTP_Arquivo As Byte
 
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2
 
 Sub Process_Line_Errors(ByRef SQL)
    Dim iFnum As Integer
  
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
      Resume
    Else
      MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
    
      'fechar banco de dados
      'If bFl_banco = 1 Then
      '  dbaccess.Close
      'End If
      'cursor
      Screen.MousePointer = vbDefault
    
      'para a aplicacao
      End
    End If
    Exit Sub

End Sub

Function Valor(ByVal Keyascii As Integer, strCampo As String) As Integer
    If Keyascii = 8 Then    'BACKSPACE
        Valor = Keyascii
        Exit Function
    End If
    
    If Chr$(Keyascii) = "," Or Chr$(Keyascii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            Keyascii = 0
            Beep
        End If
    Else
        If Chr$(Keyascii) < "0" Or Chr$(Keyascii) > "9" Then
            Keyascii = 0
            Beep
        End If
    End If
    
    Valor = Keyascii
End Function





Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function
Function Texto(ByVal Keyascii As Integer) As Integer
    If Keyascii = 8 Then    'BACKSPACE
        Texto = Keyascii
        Exit Function
    End If
    
    If Chr$(Keyascii) = "'" Then
        Keyascii = 0
        Beep
    Else
        Keyascii = Asc(UCase(Chr$(Keyascii)))
    End If
    
    Texto = Keyascii
End Function

Function Numerico(ByVal Keyascii As Integer) As Integer
    If Keyascii = 8 Then    'BACKSPACE
        Numerico = Keyascii
        Exit Function
    End If
    If Chr(Keyascii) < "0" Or Chr(Keyascii) > "9" Then
        Keyascii = 0
        Beep
    End If
    Numerico = Keyascii
End Function

Function Maiusculo(Keyascii As Integer) As Integer
    Keyascii = Asc(UCase(Chr(Keyascii)))
    Maiusculo = Keyascii
End Function


