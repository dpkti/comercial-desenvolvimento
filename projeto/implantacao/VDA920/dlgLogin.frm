VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form dlgLogin 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Acesso"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3210
   ControlBox      =   0   'False
   Icon            =   "dlgLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   142
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   214
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Informa��es do usu�rio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   45
      TabIndex        =   5
      Top             =   945
      Width           =   3120
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   855
         PasswordChar    =   "l"
         TabIndex        =   1
         Top             =   675
         Width           =   2130
      End
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         TabIndex        =   0
         Top             =   270
         Width           =   2130
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   7
         Top             =   720
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      Top             =   810
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgLogin.frx":000C
      PICN            =   "dlgLogin.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConectar 
      Height          =   690
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgLogin.frx":0902
      PICN            =   "dlgLogin.frx":091E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "dlgLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdConectar_Click()

    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Remove "vOpcao"
    vBanco.Parameters.Add "vOpcao", 1, 1
    
    vBanco.Parameters.Remove "vLogin"
    vBanco.Parameters.Add "vLogin", txtUsuario.Text, 1
    
    vBanco.Parameters.Remove "vSenha"
    vBanco.Parameters.Add "vSenha", txtSenha.Text, 1
    
    vSql = "producao.pck_rec520.pr_checa_senha(:vCursor,:vOpcao,:vLogin,:vSenha)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        End
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    If vObjOracle.EOF = True Then
        Call vVB_Generica_001.Informar("Acesso Negado!")
        mdiVDA920.vCODUSR = 0
        txtUsuario.SetFocus
        txtSenha.Text = ""
        Exit Sub
    Else
        'If vObjOracle!COD_USUARIO = 194 Or vObjOracle!COD_DEPTO = 34 Then
        'ALTERADO EM 19/09/06 CONF. SOLICT FERREIRA
        'INCLUIDO NATALIA EM 01/11/07
        If vObjOracle!COD_USUARIO = 385 Or vObjOracle!COD_USUARIO = 1263 Then
            mdiVDA920.vCODUSR = vObjOracle!COD_USUARIO
            Unload Me
        Else
            Call vVB_Generica_001.Informar("Acesso Negado!")
            mdiVDA920.vCODUSR = 0
            txtUsuario.SetFocus
            txtSenha.Text = ""
            Exit Sub
        End If
    End If

End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub
