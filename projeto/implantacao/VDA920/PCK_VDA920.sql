--- PACKAGE DO SISTEMA VDA920
--- DATA: 15/12/2005
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\VDA920\PADRAO\PCK_VDA920.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_VDA920 is

	Type tp_Cursor is Ref Cursor;
									
									
	PROCEDURE PR_CON_LOJAS( PM_CURSOR  IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
				
				
	PROCEDURE PR_CON_ESTATISTICA_ITEM( PM_DEP     IN VARCHAR2,
									   PM_CODLOJA IN NUMBER,
									   PM_CODDPK  IN NUMBER,
									   PM_ANOMES  IN NUMBER,
									   PM_CURSOR  IN OUT tp_Cursor,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2);
									   
									   
	PROCEDURE PR_CON_PENDENTE( PM_DEP     IN VARCHAR2,
							   PM_CODLOJA IN NUMBER,
							   PM_CODDPK  IN NUMBER,
							   PM_PEDIDO  IN NUMBER,
							   PM_SEQ     IN NUMBER,
							   PM_CURSOR  IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE PR_ACT_ESTATISTICA_ITEM( PM_DEP     IN VARCHAR2,
									   PM_CODLOJA IN NUMBER,
									   PM_CODDPK  IN NUMBER,
									   PM_ANOMES  IN NUMBER,
									   PM_VENDASP IN NUMBER,
									   PM_VENDACP IN NUMBER,
									   PM_OA      IN NUMBER,
									   PM_NT      IN NUMBER,
									   PM_TRANS   IN NUMBER,
									   PM_FAT     IN NUMBER,
									   PM_SP      IN NUMBER,
									   PM_CP      IN NUMBER,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2);
									   
									   
	PROCEDURE PR_ACT_PENDENTE( PM_DEP     IN VARCHAR2,
							   PM_CODLOJA IN NUMBER,
							   PM_CODDPK  IN NUMBER,
							   PM_PEDIDO  IN NUMBER,
							   PM_SEQ     IN NUMBER,
							   PM_VENDASP IN NUMBER,
							   PM_VENDACP IN NUMBER,
							   PM_OA      IN NUMBER,
							   PM_NT      IN NUMBER,
							   PM_TRANS   IN NUMBER,
							   PM_FAT     IN NUMBER,
							   PM_SP      IN NUMBER,
							   PM_CP      IN NUMBER,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2);
									   
									   
	PROCEDURE PR_ACT_LOG_ESTATISTICA_ITEM( PM_CODLOJA  IN NUMBER,
										   PM_CODDPK   IN NUMBER,
										   PM_ANOMES   IN NUMBER,
										   PM_MOTIVO   IN VARCHAR2,
										   PM_CAMPO    IN VARCHAR2,
										   PM_QTDANT   IN NUMBER,
										   PM_QTDATUAL IN NUMBER,
										   PM_COSUSR   IN NUMBER,
										   PM_CODERRO  OUT NUMBER,
										   PM_TXTERRO  OUT VARCHAR2);
										   
										   
	PROCEDURE PR_ACT_LOG_PENDENTE( PM_CODLOJA  IN NUMBER,
								   PM_CODDPK   IN NUMBER,
								   PM_PEDIDO   IN NUMBER,
							       PM_SEQ      IN NUMBER,
							       PM_DTNF     IN VARCHAR2,
							       PM_NUMITEM  IN NUMBER,
								   PM_MOTIVO   IN VARCHAR2,
								   PM_CAMPO    IN VARCHAR2,
								   PM_QTDANT   IN NUMBER,
								   PM_QTDATUAL IN NUMBER,
								   PM_COSUSR   IN NUMBER,
								   PM_CODERRO  OUT NUMBER,
								   PM_TXTERRO  OUT VARCHAR2);
							   

End PCK_VDA920;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_VDA920 is
	
	
	PROCEDURE PR_CON_LOJAS( PM_CURSOR  IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 
	
	BEGIN
		
		PM_CODERRO := 0;
		
			
		OPEN PM_CURSOR FOR
		SELECT COD_LOJA, NOME_FANTASIA FROM LOJA;

		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_LOJAS;
	
	
	PROCEDURE PR_CON_ESTATISTICA_ITEM( PM_DEP     IN VARCHAR2,
									   PM_CODLOJA IN NUMBER,
									   PM_CODDPK  IN NUMBER,
									   PM_ANOMES  IN NUMBER,
									   PM_CURSOR  IN OUT tp_Cursor,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := 'SELECT qtd_venda_sp,
						qtd_venda_cp,
						qtd_oa,
						qtd_nt,
						qtd_transito,
						qtd_fat_forn,qtd_sp,
						qtd_cp
						from '|| PM_DEP ||'.estatistica_item
						where cod_loja = '||PM_CODLOJA ||' and
						cod_dpk = '|| PM_CODDPK ||' and
						ano_mes = '|| PM_ANOMES;
				
			OPEN PM_CURSOR FOR
			STRSQL;
	
			
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_CON_ESTATISTICA_ITEM;
	
	
	PROCEDURE PR_CON_PENDENTE( PM_DEP     IN VARCHAR2,
							   PM_CODLOJA IN NUMBER,
							   PM_CODDPK  IN NUMBER,
							   PM_PEDIDO  IN NUMBER,
							   PM_SEQ     IN NUMBER,
							   PM_CURSOR  IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := 'SELECT 
						qtd_venda_sp,
						qtd_venda_cp,
						qtd_oa,
						qtd_nt,
						qtd_transito,
						qtd_fat_forn,qtd_sp,
						qtd_cp,
						DT_EMISSAO_NOTA,
						NUM_ITEM_PEDIDO
						from '|| PM_DEP ||'.PENDENTE
						where cod_loja = '||PM_CODLOJA ||' and
						cod_dpk = '|| PM_CODDPK ||' and
						NUM_PEDIDO = '|| PM_PEDIDO ||' AND
						SEQ_PEDIDO = '|| PM_SEQ;
				
			OPEN PM_CURSOR FOR
			STRSQL;
	
			
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_CON_PENDENTE;
	
	
	PROCEDURE PR_ACT_ESTATISTICA_ITEM( PM_DEP     IN VARCHAR2,
									   PM_CODLOJA IN NUMBER,
									   PM_CODDPK  IN NUMBER,
									   PM_ANOMES  IN NUMBER,
									   PM_VENDASP IN NUMBER,
									   PM_VENDACP IN NUMBER,
									   PM_OA      IN NUMBER,
									   PM_NT      IN NUMBER,
									   PM_TRANS   IN NUMBER,
									   PM_FAT     IN NUMBER,
									   PM_SP      IN NUMBER,
									   PM_CP      IN NUMBER,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := 'UPDATE '|| PM_DEP ||'.estatistica_item SET
						qtd_venda_sp = '|| PM_VENDASP ||',
						qtd_venda_cp = '|| PM_VENDACP ||',
						qtd_oa = '|| PM_OA ||',
						qtd_nt = '|| PM_NT ||',
						qtd_transito = '|| PM_TRANS ||',
						qtd_fat_forn = '|| PM_FAT ||',
						qtd_sp = '|| PM_SP ||',
						qtd_cp = '|| PM_CP ||'
						where cod_loja = '||PM_CODLOJA ||' and
						cod_dpk = '|| PM_CODDPK ||' and
						ano_mes = '|| PM_ANOMES;
						
			EXECUTE IMMEDIATE STRSQL;
			 
			COMMIT;
	
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_ACT_ESTATISTICA_ITEM;
	
	
	PROCEDURE PR_ACT_PENDENTE( PM_DEP     IN VARCHAR2,
							   PM_CODLOJA IN NUMBER,
							   PM_CODDPK  IN NUMBER,
							   PM_PEDIDO  IN NUMBER,
							   PM_SEQ     IN NUMBER,
							   PM_VENDASP IN NUMBER,
							   PM_VENDACP IN NUMBER,
							   PM_OA      IN NUMBER,
							   PM_NT      IN NUMBER,
							   PM_TRANS   IN NUMBER,
							   PM_FAT     IN NUMBER,
							   PM_SP      IN NUMBER,
							   PM_CP      IN NUMBER,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := 'UPDATE '|| PM_DEP ||'.PENDENTE SET
						qtd_venda_sp = '|| PM_VENDASP ||',
						qtd_venda_cp = '|| PM_VENDACP ||',
						qtd_oa = '|| PM_OA ||',
						qtd_nt = '|| PM_NT ||',
						qtd_transito = '|| PM_TRANS ||',
						qtd_fat_forn = '|| PM_FAT ||',
						qtd_sp = '|| PM_SP ||',
						qtd_cp = '|| PM_CP ||'
						where cod_loja = '||PM_CODLOJA ||' and
						cod_dpk = '|| PM_CODDPK ||' and
						NUM_PEDIDO = '|| PM_PEDIDO ||' AND
						SEQ_PEDIDO = '|| PM_SEQ;
						
			EXECUTE IMMEDIATE STRSQL;
			 
			COMMIT;
	
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_ACT_PENDENTE;
	
	
	PROCEDURE PR_ACT_LOG_ESTATISTICA_ITEM( PM_CODLOJA  IN NUMBER,
										   PM_CODDPK   IN NUMBER,
										   PM_ANOMES   IN NUMBER,
										   PM_MOTIVO   IN VARCHAR2,
										   PM_CAMPO    IN VARCHAR2,
										   PM_QTDANT   IN NUMBER,
										   PM_QTDATUAL IN NUMBER,
										   PM_COSUSR   IN NUMBER,
										   PM_CODERRO  OUT NUMBER,
										   PM_TXTERRO  OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			INSERT INTO LOG_ALTERA_ESTATISTICA 
			(SEQUENCIA,DT_ALTERACAO,COD_LOJA,COD_DPK,ANO_MES,DESC_MOTIVO,
			 DESC_CAMPO,QTD_ANTERIOR,QTD_ATUAL,COD_USUARIO)
			VALUES
			((SELECT NVL(MAX(SEQUENCIA),0)+1 FROM LOG_ALTERA_ESTATISTICA),
			 SYSDATE,PM_CODLOJA,PM_CODDPK,PM_ANOMES,PM_MOTIVO,PM_CAMPO,
			 PM_QTDANT,PM_QTDATUAL,PM_COSUSR);
			 
			 COMMIT;
	
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_ACT_LOG_ESTATISTICA_ITEM;
	
	
	PROCEDURE PR_ACT_LOG_PENDENTE( PM_CODLOJA  IN NUMBER,
								   PM_CODDPK   IN NUMBER,
								   PM_PEDIDO   IN NUMBER,
							       PM_SEQ      IN NUMBER,
							       PM_DTNF     IN VARCHAR2,
							       PM_NUMITEM  IN NUMBER,
								   PM_MOTIVO   IN VARCHAR2,
								   PM_CAMPO    IN VARCHAR2,
								   PM_QTDANT   IN NUMBER,
								   PM_QTDATUAL IN NUMBER,
								   PM_COSUSR   IN NUMBER,
								   PM_CODERRO  OUT NUMBER,
								   PM_TXTERRO  OUT VARCHAR2) AS	 
		
			STRSQL VARCHAR2(1000);
		
		BEGIN
			
			PM_CODERRO := 0;
			
			INSERT INTO LOG_ALTERA_PENDENTE 
			(SEQUENCIA,DT_ALTERACAO,COD_LOJA,NUM_PEDIDO,SEQ_PEDIDO,DT_EMISSAO_NOTA,
			 NUM_ITEM_PEDIDO,COD_DPK,DESC_MOTIVO,
			 DESC_CAMPO,QTD_ANTERIOR,QTD_ATUAL,COD_USUARIO)
			VALUES
			((SELECT NVL(MAX(SEQUENCIA),0)+1 FROM LOG_ALTERA_PENDENTE),
			 SYSDATE,PM_CODLOJA,PM_PEDIDO,PM_SEQ,TO_DATE(PM_DTNF,'DD/MM/RRRR'),PM_NUMITEM,
			 PM_CODDPK,PM_MOTIVO,PM_CAMPO,PM_QTDANT,PM_QTDATUAL,PM_COSUSR);
			 
			 COMMIT;
	
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	END PR_ACT_LOG_PENDENTE;
	
	
	
		
End PCK_VDA920;
---------------
/