VERSION 4.00
Begin VB.Form frmConsulta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CONSULTA"
   ClientHeight    =   5040
   ClientLeft      =   810
   ClientTop       =   1890
   ClientWidth     =   9990
   Height          =   5445
   Left            =   750
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5040
   ScaleWidth      =   9990
   Top             =   1545
   Width           =   10110
   Begin VB.ComboBox cboLoja 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   3120
      TabIndex        =   8
      Top             =   1200
      Width           =   1935
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   375
      Left            =   5280
      TabIndex        =   6
      Top             =   600
      Width           =   1575
   End
   Begin VB.TextBox txtData 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3120
      MaxLength       =   8
      TabIndex        =   5
      Top             =   720
      Width           =   1095
   End
   Begin VB.OptionButton Option3 
      Caption         =   "Ordem NF Entrada (Asc)"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5280
      TabIndex        =   3
      Top             =   240
      Width           =   2295
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Ordem Cota��o (Asc)"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3000
      TabIndex        =   2
      Top             =   240
      Width           =   1935
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Ordem Cota��o (Desc)"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   600
      TabIndex        =   1
      Top             =   240
      Value           =   -1  'True
      Width           =   1935
   End
   Begin VB.Label Label2 
      Caption         =   "CD DA COTA��O"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   720
      TabIndex        =   7
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Label Label1 
      Caption         =   "DT.NF TRANSFERENCIA >="
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   720
      TabIndex        =   4
      Top             =   720
      Width           =   2295
   End
   Begin MSGrid.Grid grdCon 
      Height          =   3015
      Left            =   360
      TabIndex        =   0
      Top             =   1800
      Visible         =   0   'False
      Width           =   9015
      _Version        =   65536
      _ExtentX        =   15901
      _ExtentY        =   5318
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBuscar_Click()
  Dim ss As Object
  Dim i As Long
  
  Screen.MousePointer = 11
  DoEvents
  
  If cboLoja = "" Then
    MsgBox "Para fazer a consulta e necessario escolher um CD", vbExclamation, "Aten��o"
    Screen.MousePointer = 0
    Exit Sub
  End If

  SQL = " SELECT A.COD_LOJA_COTACAO,"
  SQL = SQL & " A.NUM_COTACAO,"
  SQL = SQL & " A.NUM_COTACAO_TEMP,"
  SQL = SQL & " A.COD_LOJA_TRANSF,"
  SQL = SQL & " A.NUM_PEDIDO PEDIDO_TRANSF,"
  SQL = SQL & " A.SEQ_PEDIDO,"
  SQL = SQL & " nvl(B.NUM_NOTA,0) NOTA_ENTRADA,"
  SQL = SQL & " A.COD_LOJA_VENDA,"
  SQL = SQL & " A.NUM_PEDIDO_VENDA,"
  SQL = SQL & " A.SEQ_PEDIDO_VENDA,"
  SQL = SQL & " C.COD_CLIENTE,"
  SQL = SQL & " d.NOME_CLIENTE "
  SQL = SQL & " FROM TRANSFERENCIA.R_COTACAO_TRANSF A,"
  SQL = SQL & " TRANSFERENCIA.TRANSF_NOTA_COMPRA B,"
  SQL = SQL & " vendas.cotacao c,"
  SQL = SQL & " CLIENTE D"
  SQL = SQL & " WHERE "
  SQL = SQL & " a.cod_loja_cotacao = :loja and"
  SQL = SQL & " c.COD_CLIENTE = D.COD_CLIENTE AND"
  SQL = SQL & " A.NUM_COTACAO = C.NUM_COTACAO AND"
  SQL = SQL & " a.cod_loja_cotacao = c.cod_loja AND"
  SQL = SQL & " A.COD_LOJA_TRANSF=B.COD_LOJA_ORIGEM(+) AND"
  SQL = SQL & " A.NUM_PEDIDO = B.NUM_PEDIDO(+) AND"
  SQL = SQL & " A.SEQ_PEDIDO = B.SEQ_PEDIDO (+)"
  If txtData <> "" Then
    SQL = SQL & " and b.dt_emissao_nota >= to_date(:dt,'dd/mm/rr')"
  End If
  If Option1.Value Then
    SQL = SQL & " ORDER BY A.COD_LOJA_COTACAO,NUM_COTACAO DESC"
  ElseIf Option2.Value Then
    SQL = SQL & " ORDER BY A.COD_LOJA_COTACAO,NUM_COTACAO asc"
  ElseIf Option3.Value Then
    SQL = SQL & " ORDER BY B.NUM_NOTA ASC"
  End If

  oradatabase.Parameters.Remove "dt"
  oradatabase.Parameters.Add "dt", txtData, 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", Mid(Trim(cboLoja), 1, 2), 1

  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.EOF And ss.BOF Then
    MsgBox "N�o existe fluxo de transfer�ncia para consulta", vbInformation, "Aten��o"
    If grdCon.Visible = True Then
      grdCon.Visible = False
    End If
    Screen.MousePointer = 0
    Exit Sub
  End If
  
   'carrega dados
    
    With frmConsulta.grdCon
        .Cols = 10
        .Rows = ss.recordcount + 1
        .ColWidth(0) = 900
        .ColWidth(1) = 1000
        .ColWidth(2) = 1500
        .ColWidth(3) = 800
        .ColWidth(4) = 1500
        .ColWidth(5) = 900
        .ColWidth(6) = 1400
        .ColWidth(7) = 1200
        .ColWidth(8) = 1300
        .ColWidth(9) = 1400
        
        .Row = 0
        .Col = 0
        .Text = "CD_COT"
        .Col = 1
        .Text = "COTACAO"
        .Col = 2
        .Text = "COTACAO PAAC"
        .Col = 3
        .Text = "COD."
        .Col = 4
        .Text = "CLIENTE"
        .Col = 5
        .Text = "CD_TRANSF"
        .Col = 6
        .Text = "PEDIDO_TRANSF"
        .Col = 7
        .Text = "NF_ENTRADA"
        .Col = 8
        .Text = "CD_VENDA"
        .Col = 9
        .Text = "PEDIDO_VENDA"
        
        ss.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = IIf(IsNull(ss!cod_loja_cotacao), "", ss!cod_loja_cotacao)
            .Col = 1
            .Text = IIf(IsNull(ss!num_cotacao), "", ss!num_cotacao)
            .Col = 2
            .Text = IIf(IsNull(ss!num_cotacao_temp), "", ss!num_cotacao_temp)
            
            .Col = 5
            .Text = IIf(IsNull(ss!cod_loja_transf), "", ss!cod_loja_transf)
            
            .Col = 3
            .Text = IIf(IsNull(ss!cod_cliente), "", ss!cod_cliente)
            .Col = 4
            .Text = IIf(IsNull(ss!nome_cliente), "", ss!nome_cliente)
            
            .Col = 6
            .Text = IIf(IsNull(ss!pedido_transf), "", ss!pedido_transf)
            .Col = 7
            .Text = IIf(IsNull(ss!nota_entrada), "", ss!nota_entrada)
            .Col = 8
            .Text = IIf(IsNull(ss!cod_loja_venda), "", ss!cod_loja_venda)
            .Col = 9
            .Text = IIf(IsNull(ss!NUM_PEDIDO_VENDA), "", ss!NUM_PEDIDO_VENDA)
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    frmConsulta.grdCon.Visible = True
    Screen.MousePointer = 0

End Sub

Private Sub Form_Load()
  Dim ss As Object
  Dim i As Long
  
 ' SQL = "Select dt_faturamento"
  'SQL = SQL & " from datas"
  
  'Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
  
  'If ss.EOF And ss.BOF Then
  '  txtData = ""
  'Else
  '  txtData = Format(ss!dt_faturamento, "dd/mm/yy")
  'End If
  
  
  If cboLoja.ListCount = 0 Then
    SQL = "Select to_char(cod_loja,'00') cod_loja,"
    SQL = SQL & " nome_fantasia"
    SQL = SQL & " From loja"
    SQL = SQL & " Where cod_loja not in (2,6)"
    SQL = SQL & " Order by cod_loja"
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    For i = 1 To ss.recordcount
      cboLoja.AddItem ss!cod_loja & " - " & ss!nome_fantasia
      ss.MoveNext
    Next
  End If
  
End Sub


Private Sub Option1_Click()
  Call cmdBuscar_Click
End Sub


Private Sub Option2_Click()
 Call cmdBuscar_Click
End Sub


Private Sub Option3_Click()
 Call cmdBuscar_Click
End Sub


Private Sub txtData_KeyPress(KeyAscii As Integer)
   Call DATA(KeyAscii, txtData)
End Sub


