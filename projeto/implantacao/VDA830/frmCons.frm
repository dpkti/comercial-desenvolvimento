VERSION 4.00
Begin VB.Form frmCons 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta"
   ClientHeight    =   3945
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   8220
   Height          =   4350
   Left            =   1080
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3945
   ScaleWidth      =   8220
   Top             =   1170
   Width           =   8340
   Begin VB.Label Label1 
      Caption         =   "duplo click para excluir"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   3600
      Width           =   3255
   End
   Begin MSGrid.Grid grdCon 
      Height          =   3015
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   5318
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmCons"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
   Dim i As Long
   'mouse
    Screen.MousePointer = vbHourglass
    
    'montar SQL
     SQL = "Select a.tp_cesta||'-'||e.nome_cesta cesta,"
    SQL = SQL & " to_char(a.cod_loja,'00')||'-'||b.nome_fantasia loja,"
    SQL = SQL & " to_char(d.cod_fornecedor,'0000')||'-'||C.sigla forn,"
    SQL = SQL & " e.dt_vigencia,d.cod_fabrica, a.cod_dpk"
    SQL = SQL & " From cesta_item a, loja b,"
    SQL = SQL & " fornecedor c, item_cadastro d,"
    SQL = SQL & " cesta_venda e "
    SQL = SQL & " Where"
    SQL = SQL & " d.cod_fornecedor= c.cod_fornecedor and"
    SQL = SQL & " a.tp_cesta=e.tp_cesta and "
    SQL = SQL & " a.cod_dpk = d.cod_dpk and"
    SQL = SQL & " a.cod_loja=b.cod_loja"
    SQL = SQL & " Group by a.tp_cesta,e.nome_cesta,"
    SQL = SQL & " a.cod_loja,b.nome_fantasia,"
    SQL = SQL & " d.cod_fornecedor,c.sigla,e.dt_vigencia,d.cod_fabrica,"
    SQL = SQL & " a.cod_dpk"
    SQL = SQL & " Order by cesta, loja, forn"
    
    'criar consulta
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o h� ITENS EM DESTAQUE cadastrados", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    
    With frmCons.grdCon
        .Cols = 6
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 1000
        .ColWidth(1) = 1500
        .ColWidth(2) = 2000
        .ColWidth(3) = 2000
        .ColWidth(4) = 1000
        .ColWidth(5) = 1200
        
        .Row = 0
        .Col = 0
        .Text = "SEGMENTO"
        .Col = 1
        .Text = "LOJA"
        .Col = 2
        .Text = "FORN."
        .Col = 3
        .Text = "FABRICA."
        .Col = 4
        .Text = "DPK"
        .Col = 5
        .Text = "VIG�NCIA"
        
        ss.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = Trim(ss!cesta)
            .Col = 1
            .Text = Trim(ss!loja)
            .Col = 2
            .Text = ss!forn
            .Col = 3
            .Text = ss!cod_fabrica
            .Col = 4
            .Text = ss!cod_dpk
            .Col = 5
            .Text = ss!DT_VIGENCIA
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
            
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub
End Sub


Private Sub grdCon_DblClick()
  Dim lngCod_Loja As Long
  Dim lngDpk As Long
  Dim intCesta As Long
  
  grdCon.Col = 1
  lngCod_Loja = Mid(Trim(grdCon.Text), 1, 2)
  grdCon.Col = 4
  lngDpk = Mid(Trim(grdCon.Text), 1, 4)
  grdCon.Col = 0
  intCesta = Mid(Trim(grdCon.Text), 1, 1)

   
  SQL = "BEGIN"
  SQL = SQL & " Delete from cesta_item "
  SQL = SQL & " where "
  SQL = SQL & " cod_dpk = :dpk and"
  SQL = SQL & " tp_cesta = :cesta "
  SQL = SQL & " AND cod_loja = :loja;"
  SQL = SQL & " COMMIT;"
  SQL = SQL & "EXCEPTION"
  SQL = SQL & " WHEN OTHERS THEN"
  SQL = SQL & " ROLLBACK;"
  SQL = SQL & " :cod_errora := SQLCODE;"
  SQL = SQL & " :txt_errora := SQLERRM;"
  SQL = SQL & "END;"
    
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Remove "dpk"
  oradatabase.Parameters.Remove "cesta"
  oradatabase.Parameters.Remove "cod_errora"
  oradatabase.Parameters.Remove "txt_errora"
     
  oradatabase.Parameters.Add "loja", lngCod_Loja, 1
  oradatabase.Parameters.Add "dpk", lngDpk, 1
  oradatabase.Parameters.Add "cesta", intCesta, 1
  oradatabase.Parameters.Add "cod_errora", 0, 2
  oradatabase.Parameters.Add "txt_errora", "", 2
  
   
  oradatabase.ExecuteSQL SQL
            
  If oradatabase.Parameters("cod_errora") <> 0 Then
    MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
    Exit Sub
  Else
    MsgBox "Exclus�o OK ", vbInformation, "Aten��o"
    Form_Load
  End If




End Sub

