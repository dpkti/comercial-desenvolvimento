VERSION 4.00
Begin VB.Form frmCada 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CADASTRO DE ITEM"
   ClientHeight    =   3975
   ClientLeft      =   720
   ClientTop       =   1515
   ClientWidth     =   9375
   Height          =   4380
   Left            =   660
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3975
   ScaleWidth      =   9375
   Top             =   1170
   Width           =   9495
   Begin VB.CommandButton Command3 
      Caption         =   "&Sair"
      Height          =   495
      Left            =   5640
      TabIndex        =   15
      Top             =   2880
      Width           =   1815
   End
   Begin VB.CommandButton Command2 
      Caption         =   "L&impar"
      Height          =   495
      Left            =   3600
      TabIndex        =   14
      Top             =   2880
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "I&ncluir"
      Height          =   495
      Left            =   1560
      TabIndex        =   13
      Top             =   2880
      Width           =   1815
   End
   Begin VB.TextBox txtCOD_FABRICA 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5880
      MaxLength       =   19
      TabIndex        =   7
      Top             =   1680
      Width           =   3255
   End
   Begin VB.TextBox txtCOD_FORNECEDOR 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2880
      MaxLength       =   3
      TabIndex        =   6
      Top             =   1680
      Width           =   375
   End
   Begin VB.TextBox txtCOD_DPK 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   5
      TabIndex        =   5
      Top             =   1680
      Width           =   615
   End
   Begin VB.TextBox txtDescricao 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   30
      TabIndex        =   4
      Top             =   2160
      Width           =   6375
   End
   Begin VB.ComboBox cboTipo_Cesta 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   3360
      TabIndex        =   2
      Top             =   1080
      Width           =   1815
   End
   Begin VB.ComboBox cboDeposito 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   3360
      TabIndex        =   0
      Top             =   480
      Width           =   1815
   End
   Begin VB.Label lblDescr 
      AutoSize        =   -1  'True
      Caption         =   "Descri��o"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   12
      Top             =   2160
      Width           =   870
   End
   Begin VB.Label lblCOD_FABRICA 
      AutoSize        =   -1  'True
      Caption         =   "Fabr"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5280
      TabIndex        =   11
      Top             =   1680
      Width           =   390
   End
   Begin VB.Label lblSIGLA 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   10
      Top             =   1680
      Width           =   1710
   End
   Begin VB.Label lblCOD_FORNECEDOR 
      AutoSize        =   -1  'True
      Caption         =   "Forn"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2400
      TabIndex        =   9
      Top             =   1680
      Width           =   390
   End
   Begin VB.Label lblCOD_DPK 
      AutoSize        =   -1  'True
      Caption         =   "C�digo DPK"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   8
      Top             =   1680
      Width           =   1035
   End
   Begin VB.Label Label2 
      Caption         =   "Segmento"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   3
      Top             =   1080
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Dep�sito"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   1
      Top             =   480
      Width           =   855
   End
End
Attribute VB_Name = "frmCada"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  If cboDeposito = "" Or _
     cboTipo_Cesta = "" Or _
     txtCOD_DPK = "" Then
     MsgBox "Para incluir um item � preciso entrar com todas as informa��es", vbInformation, "Aten��o"
     Exit Sub
  End If
  
  'Verifica se j� existe vigencia cadastrada para o segmento escolhido
  SQL = "Select count(*) total"
  SQL = SQL & " From cesta_venda"
  SQL = SQL & " Where tp_cesta = :tp and"
  SQL = SQL & " cod_loja = :loja"
  
  oradatabase.Parameters.Remove "tp"
  oradatabase.Parameters.Add "tp", Mid(Trim(cboTipo_Cesta), 1, 1), 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", Mid(Trim(cboDeposito), 1, 2), 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
  
  If (ss.EOF And ss.BOF) Or ss!total = 0 Then
    MsgBox "Antes de cadastrar o item, � preciso cadastrar uma vig�ncia para este segmento", vbInformation, "Aten��o"
    Exit Sub
  End If
  
  
        SQL = "BEGIN"
   SQL = SQL & " delete from cesta_item where tp_cesta = :tp and"
   SQL = SQL & " cod_dpk = :dpk and"
   SQL = SQL & " cod_loja = :loja;"
   SQL = SQL & " insert into cesta_item values("
   SQL = SQL & " :loja,:dpk,:tp);"
   SQL = SQL & " COMMIT;"
   SQL = SQL & "EXCEPTION"
   SQL = SQL & " WHEN OTHERS THEN"
   SQL = SQL & " ROLLBACK;"
   SQL = SQL & " :cod_errora := SQLCODE;"
   SQL = SQL & " :txt_errora := SQLERRM;"
   SQL = SQL & "END;"

   oradatabase.Parameters.Remove "tp"
   oradatabase.Parameters.Add "tp", Mid(Trim(cboTipo_Cesta), 1, 1), 1
   oradatabase.Parameters.Remove "loja"
   oradatabase.Parameters.Add "loja", Mid(Trim(cboDeposito), 1, 2), 1
   oradatabase.Parameters.Remove "dpk"
   oradatabase.Parameters.Add "dpk", txtCOD_DPK, 1
   oradatabase.Parameters.Remove "cod_errora"
   oradatabase.Parameters.Add "cod_errora", 0, 2
   oradatabase.Parameters.Remove "txt_errora"
   oradatabase.Parameters.Add "txt_errora", "", 2
    
   oradatabase.ExecuteSQL SQL

   If oradatabase.Parameters("cod_errora") <> 0 Then
     MsgBox "Ocorreu o erro: " & oradatabase.Parameters("txt_errora") & " . Contate o analista respons�vel", vbInformation, "Aten��o"
     Exit Sub
     Call Limpa_Tela
   Else
     MsgBox "Inclus�o efetuada com sucesso", vbInformation, "Aten��o"
     Call Limpa_Tela
   End If
  
  
End Sub


Private Sub Command2_Click()
  Call Limpa_Tela
End Sub

Private Sub Command3_Click()
  Unload Me
End Sub

Private Sub Form_Load()
    Dim i As Long
  
  Screen.MousePointer = 11
  If cboDeposito.ListCount = 0 Then
    SQL = "Select to_char(cod_loja,'00')||'-'||"
    SQL = SQL & " nome_fantasia loja"
    SQL = SQL & " From loja"
    SQL = SQL & " Where cod_loja not in (2,6)"
    SQL = SQL & " Order by cod_loja"
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
      MsgBox "Tabela sem dep�sito cadastrado, ligue depto. sistemas", vbInformation, "Aten��o"
       Screen.MousePointer = 0
      Exit Sub
    Else
      For i = 1 To ss.RecordCount
        cboDeposito.AddItem Trim(ss!loja)
        ss.MoveNext
      Next
    End If
  End If
  
  If cboTipo_Cesta.ListCount = 0 Then
    cboTipo_Cesta.AddItem "1 - PECAS"
    cboTipo_Cesta.AddItem "2 - FROTAS"
    cboTipo_Cesta.AddItem "3 - ACESS"
  End If
  
  
  Screen.MousePointer = 0
End Sub


Public Sub GetItem()
  Dim ss As Object
  
  SQL = "Select a.cod_dpk,a.cod_fornecedor,"
  SQL = SQL & " c.sigla,"
  SQL = SQL & " a.cod_fabrica,a.desc_item, b.situacao"
  SQL = SQL & " From item_cadastro a,"
  SQL = SQL & " item_estoque b, fornecedor c"
  SQL = SQL & " Where "
  If txtCOD_DPK <> "" Then
   SQL = SQL & " a.cod_dpk = :dpk and"
  ElseIf txtCOD_DPK <> "" And _
    txtCOD_FORNECEDOR <> "" And _
    txtCOD_FABRICA <> "" Then
    SQL = SQL & " a.cod_fornecedor = :forn and"
    SQL = SQL & " a.cod_fabrica = :fabrica and"
  End If
  
  SQL = SQL & " a.cod_fornecedor = c.cod_fornecedor and"
  SQL = SQL & " b.cod_loja = :loja and"
  SQL = SQL & " a.cod_dpk=b.cod_dpk"
  
  oradatabase.Parameters.Remove "dpk"
  oradatabase.Parameters.Add "dpk", txtCOD_DPK, 1
  oradatabase.Parameters.Remove "forn"
  oradatabase.Parameters.Add "forn", txtCOD_FORNECEDOR, 1
  oradatabase.Parameters.Remove "fabrica"
  oradatabase.Parameters.Add "fabrica", txtCOD_FABRICA, 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", Mid(Trim(cboDeposito), 1, 2), 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.EOF And ss.BOF Then
    MsgBox "Item n�o cadastro, favor verificar", vbInformation, "Aten��o"
    Call Limpa_Tela
  Else
    If ss!situacao = 8 Then
      MsgBox "Item substituido, favor verificar", vbInformation, "Aten��o"
      Call Limpa_Tela
    ElseIf ss!situacao = 9 Then
      MsgBox "Item desativado, favor verificar", vbInformation, "Aten��o"
      Call Limpa_Tela
    Else
      txtCOD_DPK = ss!cod_dpk
      txtCOD_FORNECEDOR = ss!cod_fornecedor
      lblSIGLA = ss!sigla
      txtCOD_FABRICA = ss!cod_fabrica
      txtDescricao = ss!desc_item
    End If
  End If
  
  
End Sub

Public Sub Limpa_Tela()
    txtCOD_DPK = ""
    txtCOD_FORNECEDOR = ""
    lblSIGLA = ""
    txtCOD_FABRICA = ""
    txtDescricao = ""
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
   KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCOD_DPK_LostFocus()
  If cboDeposito <> "" And txtCOD_DPK <> "" Then
    Call GetItem
  End If
End Sub


Private Sub txtCOD_FABRICA_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub txtCOD_FABRICA_LostFocus()
  If cboDeposito <> "" And txtCOD_FORNECEDOR <> "" And txtCOD_FABRICA <> "" Then
    Call GetItem
  End If
End Sub


Private Sub txtCOD_FORNECEDOR_KeyPress(KeyAscii As Integer)
   KeyAscii = Numerico(KeyAscii)
End Sub


