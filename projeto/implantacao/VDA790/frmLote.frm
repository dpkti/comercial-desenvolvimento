VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmLote 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gera Lote para PST"
   ClientHeight    =   5940
   ClientLeft      =   2370
   ClientTop       =   2340
   ClientWidth     =   6690
   Icon            =   "frmLote.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   6690
   Begin MSGrid.Grid grdLote 
      Height          =   2175
      Left            =   360
      TabIndex        =   0
      Top             =   1695
      Width           =   5895
      _Version        =   65536
      _ExtentX        =   10398
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdLote 
      Height          =   495
      Left            =   480
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   4560
      Width           =   2580
      _ExtentX        =   4551
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Gera arquivo de Lote &ORB"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmLote.frx":000C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command1 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   3360
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   4560
      Width           =   2700
      _ExtentX        =   4763
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Gera arquivo de Lote &ZORB"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmLote.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Default         =   -1  'True
      Height          =   1335
      Left            =   360
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   2580
      _ExtentX        =   4551
      _ExtentY        =   2355
      BTYPE           =   5
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmLote.frx":0044
      PICN            =   "frmLote.frx":0060
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "TIPO DE ORDEM: ORB VENDA NORMAL /  ZORB SUPRIMENTOS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   3960
      Width           =   5895
   End
End
Attribute VB_Name = "frmLote"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub cmdLote_Click()
  Dim dn As Snapshot
  
  
'  SQL = "Select cod_loja,num_pedido,seq_pedido,"
'  SQL = SQL & " cod_plano,tipo_ordem"
'  SQL = SQL & " From lote"
'  SQL = SQL & " Where fl_enviado='N' and"
'  SQL = SQL & " (cod_plano = 0 or  "
'  SQL = SQL & "  tipo_ordem = null)"
'
'  Set dn = dbAccess.CreateSnapshot(SQL)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE_GERA_ARQ")

  FreeLocks
  If Not vObjOracle.EOF And Not vObjOracle.BOF Then
    vObjOracle.MoveLast
    vObjOracle.MoveFirst
    If vObjOracle.RecordCount > 0 Then
      MsgBox "Para gerar o arquivo de Lote, todas as informa��es devem estar preenchidas"
      Exit Sub
    End If
  End If
  Call Gera_Arquivo_Lote("ORB")
  'Unload Me
End Sub


Private Sub Command1_Click()
  Dim dn As Snapshot
  
  
'  SQL = "Select cod_loja,num_pedido,seq_pedido,"
'  SQL = SQL & " cod_plano,tipo_ordem"
'  SQL = SQL & " From lote"
'  SQL = SQL & " Where fl_enviado='N' and"
'  SQL = SQL & " (cod_plano = 0  or"
'  SQL = SQL & "  tipo_ordem = null)"
'
'  Set dn = dbAccess.CreateSnapshot(SQL)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE_GERA_ARQ")
  
  FreeLocks
  If Not vObjOracle.EOF And Not vObjOracle.BOF Then
    vObjOracle.MoveLast
    vObjOracle.MoveFirst
    If vObjOracle.RecordCount > 0 Then
      MsgBox "Para gerar o arquivo de Lote, todas as informa��es devem estar preenchidas"
      Exit Sub
    End If
  End If
  Call Gera_Arquivo_Lote("ZORB")
  'Unload Me

End Sub

Private Sub Form_Load()
  Dim dn As Snapshot
  Dim i As Long
  
'  SQL = "Select cod_loja,num_pedido,seq_pedido,"
'  SQL = SQL & " cod_plano,tipo_ordem"
'  SQL = SQL & " From lote"
'  SQL = SQL & " Where fl_enviado='N'"
'
'  Set dn = dbAccess.CreateSnapshot(SQL)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE_N_ENVIADO")

  FreeLocks
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "N�o existe pedido para gerar arquivo de lote", vbExclamation, "ATEN��O"
    Exit Sub
  Else
    vObjOracle.MoveLast
    vObjOracle.MoveFirst
    With frmLote.grdLote
        .Cols = 5
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 1200
        .ColWidth(2) = 500
        .ColWidth(3) = 1000
        .ColWidth(4) = 1500
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "Num Pedido"
        .Col = 2
        .Text = "Seq."
        .Col = 3
        .Text = "Cod.Plano"
        .Col = 4
        .Text = "Tipo Ordem"
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!cod_loja
            .Col = 1
            .Text = vObjOracle!num_pedido
            .Col = 2
            .Text = vObjOracle!seq_pedido
            .Col = 3
            .Text = vObjOracle!cod_plano
            .Col = 4
            .Text = IIf(IsNull(vObjOracle!TIPO_ORDEM), " ", vObjOracle!TIPO_ORDEM)
            
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
  
  End If
  
  
End Sub




Private Sub grdLote_DblClick()
 
  grdLote.Col = 0
 If grdLote.Text = "" Then
   Exit Sub
 End If
  lngCod_Loja = grdLote.Text
  grdLote.Col = 1
  lngNum_Pedido = grdLote.Text
  grdLote.Col = 2
  lngSEQ_PEDIDO = grdLote.Text
  
  frmPlano.Show 1
  Form_Load
  
End Sub


Public Sub Gera_Arquivo_Lote(strTipo As String)
  Dim strNome_Arquivo As String
  Dim dn As Snapshot
  Dim vObjOracle1 As Object
  Dim vObjOracle2 As Object
  Dim intControle As Integer
  Dim i As Long
  Dim x As Long
  Dim intTp_Ordem As Integer
  Dim intPedido As Integer
  Dim intCGC As Integer
  Dim intMens As Integer
  Dim intFabrica As Integer
  Dim intQTD As Integer
  Dim strDT_Pedido As String
  
'  SQL = "Select cod_loja,num_pedido,seq_pedido,"
'  SQL = SQL & " cod_plano,tipo_ordem"
'  SQL = SQL & " From lote"
'  SQL = SQL & " Where fl_enviado='N' "
'  SQL = SQL & " and tipo_ordem = '" & strTipo & "'"
'
'  Set dn = dbAccess.CreateSnapshot(SQL)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE_GERA_ARQ_2", "PM_TIPO_ORDEM", strTipo)

  FreeLocks
  
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "N�o existe pedido para gerar arq. lote", vbInformation, "Aten��o"
    Exit Sub
  End If
  vObjOracle.MoveLast
  vObjOracle.MoveFirst
  
  'verifica se arquivo j� foi gerado
  intControle = 1
  If Dir(strDiretorio, vbDirectory) = "" Then MkDir strDiretorio
Checa_dir:
  strNome_Arquivo = Dir(strDiretorio & Data_Real & "C5" & Format(intControle, "00") & ".TXT")
  If strNome_Arquivo <> "" Then
    intControle = intControle + 1
    GoTo Checa_dir
  Else
    strNome_Arquivo = strDiretorio & Data_Real & "C5" & Format(intControle, "00") & ".TXT"
    Open strNome_Arquivo For Output As #1
      For i = 1 To vObjOracle.RecordCount
        intTp_Ordem = 4 - Len(Trim(vObjOracle!TIPO_ORDEM))
        intPedido = 35 - Len(vObjOracle!num_pedido)
        
'        SQL = " SELECT rpad(b.cgc,16) cgc,a.mensagem, "
'        SQL = SQL & " to_char(decode(a.cod_vend,0,a.cod_repres,a.cod_vend),'0000') resp,"
'        SQL = SQL & " c.cod_fabrica fabrica , c.qtd_solicitada"
'        SQL = SQL & " from indent.pedido_indent a,"
'        SQL = SQL & " cliente b,"
'        SQL = SQL & " indent.item_pedido_indent c"
'        SQL = SQL & " Where"
'        SQL = SQL & " a.tp_indent=1 and"
'        SQL = SQL & " a.seq_pedido = :seq and"
'        SQL = SQL & " a.num_pedido = :pedido and"
'        SQL = SQL & " a.cod_loja = :loja and"
'        SQL = SQL & " a.tp_indent=c.tp_indent and"
'        SQL = SQL & " a.seq_pedido = c.seq_pedido and"
'        SQL = SQL & " a.num_pedido=c.num_pedido and"
'        SQL = SQL & " a.cod_loja=c.cod_loja and"
'        SQL = SQL & " a.cod_cliente = b.cod_cliente"
'
'        oradatabase.Parameters.Remove "seq"
'        oradatabase.Parameters.Add "seq", vObjOracle!seq_pedido, 1
'        oradatabase.Parameters.Remove "pedido"
'        oradatabase.Parameters.Add "pedido", vObjOracle!num_pedido, 1
'        oradatabase.Parameters.Remove "loja"
'        oradatabase.Parameters.Add "loja", vObjOracle!cod_loja, 1
'        Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", vObjOracle!seq_pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", vObjOracle!num_pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", vObjOracle!cod_loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
        vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
        vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_LOTE_GERA_ARQ_3(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        Else
            Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value
        End If
      Else
        Exit Sub
      End If
        
        intCGC = 16 - Len(IIf(IsNull(vObjOracle2!cgc), 0, Format(vObjOracle2!cgc, "00000000000000")))
        intMens = 40 - Len(IIf(IsNull(vObjOracle2!mensagem), "", vObjOracle2!mensagem))
        If intMens < 0 Then
          intMens = 0
        End If
        
'        SQL = "Select dt_pedido"
'        SQL = SQL & " From pedido"
'        SQL = SQL & " Where seq_pedido =  " & vObjOracle!seq_pedido
'        SQL = SQL & " and num_pedido = " & vObjOracle!num_pedido
'        SQL = SQL & " and cod_loja = " & vObjOracle!cod_loja
'        Set vObjOracle1 = dbAccess.CreateSnapshot(SQL)
        
        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", CLng(vObjOracle!seq_pedido), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(vObjOracle!num_pedido), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", CLng(vObjOracle!cod_loja), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
        vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
        vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO(:PM_NUM_PEDIDO,:PM_SEQ_PEDIDO,:PM_COD_LOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        Else
            Set vObjOracle1 = vBanco.Parameters("PM_CURSOR1").Value
        End If
      Else
        Exit Sub
      End If
        
        FreeLocks
        
        If vObjOracle1.EOF And vObjOracle1.BOF Then
          strDT_Pedido = Data_Faturamento
        Else
          strDT_Pedido = vObjOracle1!dt_pedido
        End If
  
        
        'MsgBox Len(vObjOracle2!mensagem)
        
        Print #1, "C" & Trim(vObjOracle!TIPO_ORDEM) & Space(intTp_Ordem) & "C5" & _
        IIf(Trim(vObjOracle!TIPO_ORDEM) = "ORB", "S1", "S2") & vObjOracle!num_pedido & Space(intPedido) & _
        Format(Data_Faturamento, "ddmmyyyy") & Format(vObjOracle2!cgc, "00000000000000") & _
        Space(intCGC) & vObjOracle!cod_plano & "CIF" & "PST" & Space(25) & _
        Format(strDT_Pedido, "ddmmyyyy") & _
        IIf(IsNull(vObjOracle2!mensagem), "", vObjOracle2!mensagem) & Space(intMens) & Trim(vObjOracle2!resp)
         
         For x = 1 To vObjOracle2.RecordCount
           intFabrica = 18 - Len(vObjOracle2!fabrica)
           intQTD = 15 - Len(vObjOracle2!qtd_solicitada)
           Print #1, "I" & vObjOracle!num_pedido & Space(intPedido) & _
           Format(Space(intFabrica) & vObjOracle2!fabrica, "000000000000000000") & _
           vObjOracle2!qtd_solicitada & Space(intQTD)
           vObjOracle2.MoveNext
         Next

        vObjOracle.MoveNext
      Next
    Close #1
    
    vObjOracle.MoveFirst
    For i = 1 To vObjOracle.RecordCount
'      SQL = "update LOTE SET fl_enviado = 'S'"
'      SQL = SQL & " Where seq_pedido = " & vObjOracle!seq_pedido
'      SQL = SQL & " AND num_pedido = " & vObjOracle!num_pedido
'      SQL = SQL & " AND cod_loja = " & vObjOracle!cod_loja
'
'      dbAccess.Execute SQL, dbFailOnError
      
        vBanco.Parameters.Remove "PM_FL_ENVIADO"
        vBanco.Parameters.Add "PM_FL_ENVIADO", "S", ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", vObjOracle!seq_pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", vObjOracle!num_pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", vObjOracle!cod_loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_LOTE(:PM_FL_ENVIADO,:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = vbDefault
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      End If
      
      
      FreeLocks
      vObjOracle.MoveNext
    Next
    MsgBox "LOTE: " & strNome_Arquivo & " GERADO COM SUCESSO, J� PODE SER ENVIADO.", vbInformation, "Aten��o"
    Unload Me
  End If

End Sub

