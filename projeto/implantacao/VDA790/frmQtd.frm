VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmQtd 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Atualiza Quantidade"
   ClientHeight    =   2280
   ClientLeft      =   2925
   ClientTop       =   2340
   ClientWidth     =   3555
   Icon            =   "frmQtd.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2280
   ScaleWidth      =   3555
   Begin VB.TextBox txtPreco 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2160
      MaxLength       =   12
      TabIndex        =   5
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox txtQtd_Atendida 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2400
      MaxLength       =   6
      TabIndex        =   1
      Top             =   640
      Width           =   975
   End
   Begin Bot�o.cmd cmdOK 
      Default         =   -1  'True
      Height          =   495
      Left            =   120
      TabIndex        =   6
      TabStop         =   0   'False
      Top             =   1590
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmQtd.frx":000C
      PICN            =   "frmQtd.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancela 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   1800
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1590
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&CANCELA ITEM"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmQtd.frx":0382
      PICN            =   "frmQtd.frx":039E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      Caption         =   "Pre�o Liq.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1080
      Width           =   1575
   End
   Begin VB.Label lblItem 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   2760
      TabIndex        =   3
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Nr. Item:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Qtd. Atendida:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   670
      Width           =   1575
   End
End
Attribute VB_Name = "frmQtd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancela_Click()
'   SQL = "BEGIN"
'   SQL = SQL & " Update indent.item_pedido_indent set"
'   SQL = SQL & " situacao = 9"
'   SQL = SQL & " Where num_item_pedido = :item and"
'   SQL = SQL & " seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " COMMIT;"
'   SQL = SQL & "EXCEPTION"
'   SQL = SQL & " WHEN OTHERS THEN"
'   SQL = SQL & " ROLLBACK;"
'   SQL = SQL & " :cod_errora := SQLCODE;"
'   SQL = SQL & " :txt_errora := SQLERRM;"
'   SQL = SQL & "END;"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'   oradatabase.Parameters.Remove "item"
'   oradatabase.Parameters.Add "item", lblItem, 1
'
'   oradatabase.Parameters.Remove "cod_errora"
'   oradatabase.Parameters.Add "cod_errora", 0, 2
'   oradatabase.Parameters.Remove "txt_errora"
'   oradatabase.Parameters.Add "txt_errora", "", 2
'   oradatabase.ExecuteSQL SQL

        vBanco.Parameters.Remove "PM_NUM_ITEM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_ITEM_PEDIDO", CLng(lblItem.Caption), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
         vBanco.Parameters.Remove "PM_VL_CONTABIL"
        vBanco.Parameters.Add "PM_VL_CONTABIL", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_CANCELA_ITEM_PEDIDO_INDENT(:PM_NUM_ITEM_PEDIDO,:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_VL_CONTABIL,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If
      frmFatu.txtContabil = IIf(IsNull(vBanco.Parameters("PM_VL_CONTABIL").Value), 0, vBanco.Parameters("PM_VL_CONTABIL").Value)
   
'   If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'      MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'        " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'   End If
   Unload Me
   
End Sub


Private Sub cmdOk_Click()
  Dim dblContabil As Double
  
'   SQL = "BEGIN"
'   SQL = SQL & " Update indent.item_pedido_indent set"
'   SQL = SQL & " qtd_atendida = :qtd,"
'   SQL = SQL & " preco_unitario_liq = :preco"
'   SQL = SQL & " Where num_item_pedido = :item and"
'   SQL = SQL & " seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " COMMIT;"
'   SQL = SQL & "EXCEPTION"
'   SQL = SQL & " WHEN OTHERS THEN"
'   SQL = SQL & " ROLLBACK;"
'   SQL = SQL & " :cod_errora := SQLCODE;"
'   SQL = SQL & " :txt_errora := SQLERRM;"
'   SQL = SQL & "END;"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'   oradatabase.Parameters.Remove "item"
'   oradatabase.Parameters.Add "item", lblItem, 1
'   oradatabase.Parameters.Remove "qtd"
'   oradatabase.Parameters.Add "qtd", txtQtd_Atendida, 1
'   oradatabase.Parameters.Remove "preco"
'   oradatabase.Parameters.Add "preco", txtPreco, 1
'
'   oradatabase.Parameters.Remove "cod_errora"
'   oradatabase.Parameters.Add "cod_errora", 0, 2
'   oradatabase.Parameters.Remove "txt_errora"
'   oradatabase.Parameters.Add "txt_errora", "", 2
'   oradatabase.ExecuteSQL SQL
'
'   If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'      MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'        " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'   End If
'
'   SQL = "Select sum(a.qtd_atendida * a.preco_unitario_liq) item"
'   SQL = SQL & " from indent.Item_pedido_indent a"
'   SQL = SQL & " Where a.situacao=0 and"
'   SQL = SQL & " seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'
'   Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
'
'   If ss.EOF And ss.BOF Then
'     dblContabil = 0
'   Else
'     dblContabil = ss!Item
'   End If
'
'   SQL = "Select vl_despesa"
'   SQL = SQL & " From indent.pedido_indent"
'   SQL = SQL & " Where "
'   SQL = SQL & " seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'
'   Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
'
'   If ss.EOF And ss.BOF Then
'     dblContabil = dblContabil
'   Else
'     dblContabil = dblContabil + ss!vl_despesa
'   End If
'
'    SQL = "BEGIN"
'   SQL = SQL & " Update indent.pedido_indent set"
'   SQL = SQL & " vl_contabil = :contabil"
'   SQL = SQL & " Where "
'   SQL = SQL & " SITUACAO = 0 and"
'   SQL = SQL & " fl_ger_nfis='N' and"
'   SQL = SQL & " num_nota is null and "
'   SQL = SQL & " seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " COMMIT;"
'   SQL = SQL & "EXCEPTION"
'   SQL = SQL & " WHEN OTHERS THEN"
'   SQL = SQL & " ROLLBACK;"
'   SQL = SQL & " :cod_errora := SQLCODE;"
'   SQL = SQL & " :txt_errora := SQLERRM;"
'   SQL = SQL & "END;"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'   oradatabase.Parameters.Remove "contabil"
'   oradatabase.Parameters.Add "contabil", dblContabil, 1
'   oradatabase.Parameters.Remove "cod_errora"
'   oradatabase.Parameters.Add "cod_errora", 0, 2
'   oradatabase.Parameters.Remove "txt_errora"
'   oradatabase.Parameters.Add "txt_errora", "", 2
'   oradatabase.ExecuteSQL SQL

  If txtQtd_Atendida.Text = "" Then 'Or txtQtd_Atendida.Text = "0" Then
    MsgBox "Entre com a quantidade atendida.", vbInformation, "Aten��o"
    Exit Sub
  ElseIf txtPreco = "" Then
    MsgBox "Entre com o pre�o liquido.", vbInformation, "Aten��o"
    Exit Sub
  End If

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_ITEM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_ITEM_PEDIDO", CLng(lblItem.Caption), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_QTD"
        vBanco.Parameters.Add "PM_QTD", CLng(IIf(txtQtd_Atendida.Text = "", 0, txtQtd_Atendida.Text)), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_PRECO_UNITARIO"
        vBanco.Parameters.Add "PM_PRECO_UNITARIO", Replace(txtPreco.Text, ",", "."), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_VL_CONTABIL"
        vBanco.Parameters.Add "PM_VL_CONTABIL", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_ATU_QTD_ITEM_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_NUM_ITEM_PEDIDO,:PM_QTD,:PM_PRECO_UNITARIO,:PM_VL_CONTABIL,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If

   frmFatu.txtContabil = vBanco.Parameters("PM_VL_CONTABIL").Value
   'frmFatu.txtContabil = Format(Replace(vBanco.Parameters("PM_VL_CONTABIL").Value, ".", ","), "0.00")
   Unload Me
End Sub


Private Sub txtPreco_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtPreco)
End Sub


Private Sub txtPreco_LostFocus()
   'txtPreco = Format(FmtBR(txtPreco), "0.00")
End Sub


Private Sub txtQtd_Atendida_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


