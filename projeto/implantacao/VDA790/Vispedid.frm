VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmVisPedido 
   Caption         =   "Manuten��o de Pedido"
   ClientHeight    =   7200
   ClientLeft      =   300
   ClientTop       =   2355
   ClientWidth     =   9420
   ClipControls    =   0   'False
   Icon            =   "Vispedid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7200
   ScaleWidth      =   9420
   Begin VB.TextBox lblDt_Pedido 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   8160
      MaxLength       =   10
      TabIndex        =   46
      Top             =   1320
      Width           =   1215
   End
   Begin MSGrid.Grid grdItemPed 
      Height          =   2535
      Left            =   480
      TabIndex        =   28
      Top             =   4440
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   4471
      _StockProps     =   77
      BackColor       =   16777215
      Cols            =   1
      FixedCols       =   0
   End
   Begin VB.TextBox txtPlano 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1800
      MaxLength       =   20
      TabIndex        =   13
      Top             =   2040
      Width           =   2535
   End
   Begin VB.TextBox txtVl_Despesa 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1800
      MaxLength       =   12
      TabIndex        =   21
      Top             =   3000
      Width           =   1575
   End
   Begin VB.TextBox txtCOD_PLANO 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1320
      MaxLength       =   3
      TabIndex        =   12
      Top             =   2040
      Width           =   495
   End
   Begin VB.ListBox lstSequencia 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   0
      TabIndex        =   40
      Top             =   6720
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox txtQtdeItens 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   8160
      MaxLength       =   3
      TabIndex        =   14
      Top             =   2040
      Width           =   495
   End
   Begin VB.TextBox txtMsgNota 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1320
      MaxLength       =   60
      TabIndex        =   23
      Top             =   3480
      Width           =   7215
   End
   Begin VB.TextBox txtCOD_VENDEDOR 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   6240
      MaxLength       =   4
      TabIndex        =   19
      Top             =   2520
      Width           =   615
   End
   Begin VB.TextBox txtCOD_REPRESENTANTE 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2520
      MaxLength       =   4
      TabIndex        =   16
      Top             =   2520
      Width           =   470
   End
   Begin VB.TextBox txtSEQ_REPRES 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3000
      MaxLength       =   1
      TabIndex        =   17
      Top             =   2520
      Width           =   200
   End
   Begin VB.TextBox txtTpPedido 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1320
      MaxLength       =   2
      TabIndex        =   15
      Top             =   2520
      Width           =   300
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   120
      TabIndex        =   45
      Top             =   720
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd Command1 
      Height          =   495
      Left            =   120
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Alterar &Cabe�alho"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":000C
      PICN            =   "Vispedid.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   495
      Left            =   7080
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   120
      Width           =   2100
      _ExtentX        =   3704
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Fec&har"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":0323
      PICN            =   "Vispedid.frx":033F
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command2 
      Height          =   495
      Left            =   2520
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   2205
      _ExtentX        =   3889
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Excluir Pedido"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":0A51
      PICN            =   "Vispedid.frx":0A6D
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command7 
      Height          =   495
      Left            =   4920
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   1980
      _ExtentX        =   3493
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Finalizar Pedido"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":0DB9
      PICN            =   "Vispedid.frx":0DD5
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command3 
      Height          =   495
      Left            =   1320
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   3860
      Width           =   1620
      _ExtentX        =   2858
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Incluir Item"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":112F
      PICN            =   "Vispedid.frx":114B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command5 
      Height          =   495
      Left            =   3000
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   3840
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Alterar Item"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":1445
      PICN            =   "Vispedid.frx":1461
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command4 
      Height          =   495
      Left            =   4800
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   3860
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "E&xcluir Item"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":175C
      PICN            =   "Vispedid.frx":1778
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command6 
      Default         =   -1  'True
      Height          =   495
      Left            =   6600
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   3860
      Width           =   1740
      _ExtentX        =   3069
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Recalcular"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Vispedid.frx":1AC4
      PICN            =   "Vispedid.frx":1AE0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblIndent 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Height          =   255
      Left            =   6600
      TabIndex        =   7
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label Label6 
      Caption         =   "TP Indent"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   44
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label Label4 
      Caption         =   "Valor Despesa:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   120
      TabIndex        =   43
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Index           =   7
      Left            =   4800
      TabIndex        =   42
      Top             =   1680
      Width           =   195
   End
   Begin VB.Label lblVl_Contabil 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   5520
      TabIndex        =   22
      Top             =   3000
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Valor Cont�bil:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3960
      TabIndex        =   41
      Top             =   3000
      Width           =   1455
   End
   Begin VB.Label lblMsgNota 
      AutoSize        =   -1  'True
      Caption         =   "Mensagem:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   120
      TabIndex        =   39
      Top             =   3480
      Width           =   1035
   End
   Begin VB.Label Label11 
      Caption         =   "Vend.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   5520
      TabIndex        =   38
      Top             =   2520
      Width           =   615
   End
   Begin VB.Label lblRepr 
      Caption         =   "Repres.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1680
      TabIndex        =   37
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label lblPSEUDOVEND 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   6840
      TabIndex        =   20
      Top             =   2520
      Width           =   2235
   End
   Begin VB.Label lblPSEUDOREPR 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3240
      TabIndex        =   18
      Top             =   2520
      Width           =   2235
   End
   Begin VB.Label Label10 
      Caption         =   "Tp.Ped.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   36
      Top             =   2520
      Width           =   735
   End
   Begin VB.Label Label5 
      Caption         =   "Dt. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7080
      TabIndex        =   35
      Top             =   1320
      Width           =   1095
   End
   Begin VB.Label lblSeq_Pedido 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5160
      TabIndex        =   6
      Top             =   840
      Width           =   255
   End
   Begin VB.Label lblNr_Pedido 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4200
      TabIndex        =   5
      Top             =   840
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Nr. Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   34
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblDeposito 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAMPINAS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   300
      Index           =   17
      Left            =   1320
      TabIndex        =   4
      Top             =   840
      Width           =   1695
   End
   Begin VB.Label lblDep 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sito:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Index           =   16
      Left            =   120
      TabIndex        =   33
      Top             =   840
      Width           =   885
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   0
      TabIndex        =   32
      Top             =   6840
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      Caption         =   "Qtde  Itens:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Index           =   10
      Left            =   6960
      TabIndex        =   31
      Top             =   2040
      Width           =   1110
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      Caption         =   "Plano:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Index           =   5
      Left            =   120
      TabIndex        =   30
      Top             =   2040
      Width           =   570
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "XX"
      ForeColor       =   &H00800000&
      Height          =   255
      Index           =   4
      Left            =   4200
      TabIndex        =   11
      Top             =   1560
      Width           =   270
   End
   Begin VB.Line ln1 
      BorderColor     =   &H00800000&
      BorderWidth     =   2
      X1              =   4200
      X2              =   4080
      Y1              =   1680
      Y2              =   1680
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "WWWWWWWWWWXXXXXXXXXX"
      ForeColor       =   &H00800000&
      Height          =   255
      Index           =   3
      Left            =   1320
      TabIndex        =   10
      Top             =   1560
      Width           =   2760
   End
   Begin VB.Label lblDados 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "WWWWWWWWWWXXXXXXXXXXWWWWWWWWWWXXXXXXXXXX"
      ForeColor       =   &H00800000&
      Height          =   300
      Index           =   2
      Left            =   2160
      TabIndex        =   9
      Top             =   1320
      Width           =   4695
   End
   Begin VB.Label lblDados 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "888888"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   300
      Index           =   1
      Left            =   1320
      TabIndex        =   8
      Top             =   1320
      Width           =   735
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Index           =   0
      Left            =   120
      TabIndex        =   29
      Top             =   1320
      Width           =   690
   End
End
Attribute VB_Name = "frmVisPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ssDt As Recordset
Dim bDifIcm As Byte
Dim strTipoRespCliente As String
Dim lngCOD_PREPOSTO As Long
Dim recalcula As Byte
Dim strTp_FIlial As String

'FUN��O IMPLEMENTADA NA PROCEDURE:
'
'(oracle)
' PRODUCAO.Pck_VDA790.PR_DELETE_ITEM_PEDIDO

'
' Sub ReSequenciar()
'    Dim SQL As String
'    Dim ss As Snapshot
'    Dim ss2 As Snapshot
'    Dim PLSQL As String
'    Dim i As Integer
'    Dim ITEM_COT As Integer
'    Dim qtd As Long
'
'        'carregar dpk e sequencias
'        SQL = "select COD_DPK,NUM_ITEM_PEDIDO from ITEM_PEDIDO where "
'        SQL = SQL & " NUM_PEDIDO = " & lngNum_Pedido
'        SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
'        SQL = SQL & " and COD_LOJA = " & lngCod_Loja
'        SQL = SQL & " order by NUM_ITEM_PEDIDO"
'        Set ss = dbAccess.CreateSnapshot(SQL)
'        FreeLocks
'        ss.MoveFirst
'        ss.MoveLast
'        If (ss.EOF And ss.BOF) Or ss.RecordCount = 0 Then
'          Exit Sub
'        Else
'
'        End If
'        ss.MoveFirst
'        i = 1
'        Do
'            If i <> ss("NUM_ITEM_PEDIDO") Then
'                SQL = "update ITEM_PEDIDO "
'                SQL = SQL & "set NUM_ITEM_PEDIDO = " & i
'                SQL = SQL & " where SEQ_PEDIDO = " & lngSEQ_PEDIDO
'                SQL = SQL & " and NUM_PEDIDO = " & lngNum_Pedido
'                SQL = SQL & " and COD_LOJA = " & lngCod_Loja
'                SQL = SQL & " and NUM_ITEM_PEDIDO = " & ss("NUM_ITEM_PEDIDO")
'                'atualizar
'                dbAccess.Execute SQL, dbFailOnError
'                FreeLocks
'            End If
'            i = i + 1
'            ss.MoveNext
'        Loop Until ss.EOF
'        ss.Close
'
'
'End Sub

Private Sub cmdSair_Click()
    
    frmVisPedido.Label1.Visible = False
    
    Unload frmVisPedido
End Sub


Private Sub Command1_Click()
    AtualizaCabecalho
End Sub

Sub AtualizaCabecalho()
  Dim dn As Snapshot
  Dim dblContabil As Double
  
  On Error GoTo TrataErro
  
    If Not ValidaTela Then Exit Sub
  
'  SQL = "Update pedido set"
'  SQL = SQL & " cod_plano = " & IIf(txtCOD_PLANO = "", 0, txtCOD_PLANO)
'  SQL = SQL & ",desc_plano = '" & txtPlano & "'"
'  SQL = SQL & ",tp_pedido = " & txtTpPedido
'  SQL = SQL & ",cod_repres = " & txtCOD_REPRESENTANTE
'  SQL = SQL & ",cod_vend = " & txtCOD_VENDEDOR
'  SQL = SQL & ",vl_despesa = " & txtVl_Despesa
'  SQL = SQL & ",qtd_itens = " & txtQtdeItens
'  SQL = SQL & ",mensagem = '" & txtMsgNota & "'"
'  SQL = SQL & " Where seq_pedido = " & lblSeq_Pedido
'  SQL = SQL & " and num_pedido = " & lblNr_Pedido
'  SQL = SQL & " and cod_loja = " & Mid(Trim(lblDeposito(17)), 1, 2)
'
'  dbAccess.Execute SQL, dbFailOnError
'  FreeLocks
'
'  SQL = "Select sum(qtd_solicitada * preco_liquido) as total"
'    SQL = SQL & " From item_pedido"
'    SQL = SQL & " where "
'    SQL = SQL & "  seq_pedido = " & lngSEQ_PEDIDO
'    SQL = SQL & " and num_pedido = " & lngNum_Pedido
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'    Set dn = dbAccess.CreateSnapshot(SQL)
'    FreeLocks
'
'    If (dn.EOF And dn.BOF) Then
'      dblContabil = 0
'    Else
'      dblContabil = IIf(IsNull(dn!total), 0, dn!total)
'    End If
'
'    SQL = "Select vl_despesa"
'    SQL = SQL & " From pedido"
'    SQL = SQL & " where "
'    SQL = SQL & "  seq_pedido = " & lngSEQ_PEDIDO
'    SQL = SQL & " and num_pedido = " & lngNum_Pedido
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'    Set dn = dbAccess.CreateSnapshot(SQL)
'    FreeLocks
'
'    dblContabil = dblContabil + dn!vl_despesa
'
'    SQL = "Update pedido set"
'    SQL = SQL & " vl_contabil = " & dblContabil
'    SQL = SQL & " where seq_pedido = " & lngSEQ_PEDIDO
'    SQL = SQL & " and num_pedido = " & lngNum_Pedido
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'    dbAccess.Execute SQL, dbFailOnError
'    FreeLocks

     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
     vBanco.Parameters.Add "PM_SEQ_PEDIDO", CLng(lblSeq_Pedido.Caption), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
     vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(lblNr_Pedido.Caption), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_LOJA"
     vBanco.Parameters.Add "PM_COD_LOJA", CLng(Mid(Trim(lblDeposito(17)), 1, 2)), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_PLANO"
     vBanco.Parameters.Add "PM_COD_PLANO", CLng(IIf(txtCOD_PLANO = "", 0, txtCOD_PLANO)), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_DESC_PLANO"
     vBanco.Parameters.Add "PM_DESC_PLANO", txtPlano.Text, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_TP_PEDIDO"
     vBanco.Parameters.Add "PM_TP_PEDIDO", CLng(txtTpPedido.Text), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_REPRES"
     vBanco.Parameters.Add "PM_COD_REPRES", CLng(txtCOD_REPRESENTANTE.Text), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_VEND"
     vBanco.Parameters.Add "PM_COD_VEND", CLng(txtCOD_VENDEDOR.Text), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_VL_DESPESA"
     vBanco.Parameters.Add "PM_VL_DESPESA", CDbl(txtVl_Despesa.Text), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_QTD_ITENS"
     vBanco.Parameters.Add "PM_QTD_ITENS", CInt(txtQtdeItens.Text), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_MENSAGEM"
     vBanco.Parameters.Add "PM_MENSAGEM", txtMsgNota.Text, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_dt"
     vBanco.Parameters.Add "PM_dt", CDate(lblDt_Pedido), ORAPARM_INPUT
     
     
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_PEDIDO(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_PLANO,:PM_DESC_PLANO,:PM_TP_PEDIDO,:PM_COD_REPRES,:PM_COD_VEND,:PM_VL_DESPESA,:PM_QTD_ITENS,:PM_MENSAGEM,:PM_DT,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = 0
         Exit Sub
      End If
       
  FreeLocks
    
  'Unload frmVisPedido
  'frmVisPedido.Show 1
    
  Form_Load
  
   Exit Sub
 
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors("frmVisPEdido.AtualizaCabecalho")
    End If
End Sub

Function ValidaTela() As Boolean
  ValidaTela = True
  If txtTpPedido.Text = "21" And (txtCOD_VENDEDOR.Text = "" Or txtCOD_VENDEDOR.Text = "0") Then
    MsgBox "Entre com o Vendedor", vbInformation, "Aten��o"
    txtTpPedido.SetFocus
    ValidaTela = False
  ElseIf txtTpPedido.Text = "22" And (txtCOD_VENDEDOR.Text = "" Or txtCOD_VENDEDOR.Text = "0") Then
    MsgBox "Entre com o Vendedor", vbInformation, "Aten��o"
    txtTpPedido.SetFocus
    ValidaTela = False
  ElseIf txtVl_Despesa.Text = "" Then
    MsgBox "Entre com o Valor de Despesa", vbInformation, "Aten��o"
    txtVl_Despesa.SetFocus
    ValidaTela = False
  End If
End Function

Private Sub Command2_Click()
Dim ss As Snapshot
Dim response As String

On Error GoTo TrataErro

  
  response = MsgBox("Confirma exclus�o do Pedido ?", vbYesNo, "Aten��o")
  If response = vbNo Then
    Exit Sub
  End If

  recalcula = 0
' SQL = "Select a.num_pedido"
' SQL = SQL & " from pedido as a"
' SQL = SQL & " where a.num_pedido = " & lngNum_Pedido
' SQL = SQL & " and a.seq_pedido = " & lngSEQ_PEDIDO
' SQL = SQL & " and a.cod_loja = " & lngCod_Loja
'
' Set ss = dbAccess.CreateSnapshot(SQL)

Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO", "PM_NUM_PEDIDO", lngNum_Pedido, "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, "PM_COD_LOJA", lngCod_Loja)

 FreeLocks
 
 If vObjOracle.EOF Then
   MsgBox "Ocorreu um erro na consulta do pedido : " & lngNum_Pedido & " - " & lngSEQ_PEDIDO _
   & " Ligue para o depto. Sistemas", vbCritical, "Aten��o"
   Exit Sub
 Else
   vObjOracle.Close
'   SQL = "delete * from pedido where num_pedido = " & lngNum_Pedido
'   SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
'   SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'   dbAccess.Execute SQL, dbFailOnError
' marici 12/02/08
    vBanco.Parameters.Remove "PM_NUM_PEDIDO"
    vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
    vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
       
    vSql = "PRODUCAO.Pck_VDA790.PR_DELETE_PEDIDO(:PM_NUM_PEDIDO,:PM_SEQ_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vErro <> "" Then
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If

   FreeLocks
   MsgBox "Exclus�o OK", vbInformation, "Aten��o"
   Unload Me
 End If
 
 
 Exit Sub

TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors("frmVisPEdido.Command2_Click")
    End If
End Sub

Private Sub Command3_Click()

 On Error GoTo TrataErro
  
  bTipo_Item = 1
  frmItem.Show 1
  
  CarregaGrid
      
  'consulta dados dos itens
'    SQL = "select NUM_ITEM_PEDIDO,COD_DPK,QTD_SOLICITADA,"
'    SQL = SQL & "PRECO_LIQUIDO,"
'    SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as PR_LIQ,"
'    SQL = SQL & "SITUACAO, COD_FORNECEDOR,"
'    SQL = SQL & "COD_FABRICA,desc_item "
'    SQL = SQL & "from ITEM_PEDIDO "
'    SQL = SQL & "where NUM_Pedido = " & lngNum_Pedido
'    SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'    SQL = SQL & " order by NUM_ITEM_PEDIDO"
'    Set ssDt = dbAccess.OpenRecordset(SQL, dbOpenSnapshot)
'    FreeLocks
'
'  'associar ao data control
'  Set dtVisPedido.Recordset = ssDt

   CarregaGrid
    Call AtualizaCabecalho
  Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors("frmVisPedido.Command3_Click")
    End If
  
End Sub

Sub CarregaGrid()

    Dim Rs As Object

    'consulta dados dos itens
'    SQL = "select NUM_ITEM_PEDIDO,COD_DPK,QTD_SOLICITADA,"
'    SQL = SQL & "PRECO_LIQUIDO,"
'    SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as PR_LIQ,"
'    SQL = SQL & "SITUACAO, COD_FORNECEDOR,"
'    SQL = SQL & "COD_FABRICA, DESC_ITEM "
'    SQL = SQL & "from ITEM_PEDIDO "
'    SQL = SQL & "where NUM_PEDIDO = " & lngNum_Pedido
'    SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'    SQL = SQL & " order by NUM_ITEM_PEDIDO"
'    Set ssDt = dbAccess.OpenRecordset(SQL, dbOpenSnapshot)

     vBanco.Parameters.Remove "PM_num_pedido"
     vBanco.Parameters.Add "PM_num_pedido", lngNum_Pedido, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_seq_pedido"
     vBanco.Parameters.Add "PM_seq_pedido", lngSEQ_PEDIDO, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_cod_loja"
     vBanco.Parameters.Add "PM_cod_loja", lngCod_Loja, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_ITENS_PEDIDO_DADOS(:PM_num_pedido,:PM_seq_pedido,:PM_cod_loja,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
      Screen.MousePointer = vbDefault
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
       Exit Sub
    Else
       Set Rs = vBanco.Parameters("PM_CURSOR1").Value
       If Not Rs.EOF And Not Rs.BOF Then
        vVB_Generica_001.CarregaGridTabela grdItemPed, Rs, 10, ""
       Else
        grdItemPed.Rows = 1
        grdItemPed.Rows = 2
        grdItemPed.FixedRows = 1
        'grdItemPed.RemoveItem 0
       End If
       'grdItemPed.ColPos(0)
    End If
    
    grdItemPed.Cols = 10
            
    With grdItemPed
        .Col = 1
        .Row = 0
        .Text = "N� Item"
        
        .Col = 2
        .Row = 0
        .Text = "Qtd.Solic."
        
        .Col = 3
        .Row = 0
        .Text = "DPK"
        
        .Col = 4
        .Row = 0
        .Text = "Forn."
        
        .Col = 5
        .Row = 0
        .Text = "Fabrica"
        
        .Col = 6
        .Row = 0
        .Text = "Descr."
        
        .Col = 7
        .Row = 0
        .Text = "Liq.Unitario"
        
        .Col = 8
        .Row = 0
        .Text = "Preco Liquido"
        
        .Col = 9
        .Row = 0
        .Text = "Sit."
        
        .ColWidth(0) = 300
        .ColWidth(1) = 700
        .ColWidth(2) = 800
        .ColWidth(3) = 1000
        .ColWidth(4) = 600
        .ColWidth(5) = 1800
        .ColWidth(6) = 1900
        .ColWidth(7) = 1300
        .ColWidth(8) = 1300
        .ColWidth(9) = 700
    End With
End Sub


Private Sub Command4_Click()
Dim ss As Snapshot
Dim response As String

On Error GoTo TrataErro
 
  grdItemPed.Col = 1
  If grdItemPed.Row < 1 Or grdItemPed.Text = "" Then
    MsgBox "Selecione um item para excluir", vbExclamation, "Aten��o"
    Exit Sub
  End If
  
  response = MsgBox("Confirma exclus�o do item: " & grdItemPed.Text, vbYesNo, "Aten��o")
  If response = vbNo Then
    Exit Sub
  End If

' SQL = "Select a.num_pedido"
' SQL = SQL & " from pedido as a"
' SQL = SQL & " where a.num_pedido = " & lngNum_Pedido
' SQL = SQL & " and a.seq_pedido = " & lngSEQ_PEDIDO
' SQL = SQL & " and a.cod_loja = " & lngCod_Loja
'
' Set ss = dbAccess.CreateSnapshot(SQL)

Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO", "PM_NUM_PEDIDO", lngNum_Pedido, "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, "PM_COD_LOJA", lngCod_Loja)

 FreeLocks
 If vObjOracle.EOF Then
   MsgBox "Ocorreu um erro na consulta do pedido : " & lngNum_Pedido & " - " & lngSEQ_PEDIDO _
   & " Ligue para o depto. Sistemas", vbCritical, "Aten��o"
   Exit Sub
 Else
'         SQL = "delete * from item_pedido "
'   SQL = SQL & " Where num_item_pedido = " & dbgrdItemPed.Columns(0).Value
'   SQL = SQL & " and num_pedido = " & lngNum_Pedido
'   SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
'   SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'   dbAccess.Execute SQL, dbFailOnError

'   FreeLocks
'   ss.Close

    vBanco.Parameters.Remove "PM_NUM_ITEM_PEDIDO"
    vBanco.Parameters.Add "PM_NUM_ITEM_PEDIDO", CLng(grdItemPed.Text), ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_NUM_PEDIDO"
    vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
    vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
        
    vSql = "PRODUCAO.Pck_VDA790.PR_DELETE_ITEM_PEDIDO(:PM_NUM_ITEM_PEDIDO, :PM_NUM_PEDIDO,:PM_SEQ_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
     If vErro <> "" Then
        MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
     End If

   FreeLocks
   
'   Call ReSequenciar
'   'consulta dados dos itens
'    SQL = "select NUM_ITEM_PEDIDO,COD_DPK,QTD_SOLICITADA,"
'    SQL = SQL & "PRECO_LIQUIDO,"
'    SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as PR_LIQ,"
'    SQL = SQL & "desc_item, SITUACAO, COD_FORNECEDOR,"
'    SQL = SQL & "COD_FABRICA "
'    SQL = SQL & "from ITEM_PEDIDO "
'    SQL = SQL & "where NUM_PEDIDO = " & lngNum_Pedido
'    SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'    SQL = SQL & " order by NUM_ITEM_PEDIDO"
'    Set ssDt = dbAccess.OpenRecordset(SQL, dbOpenSnapshot)
'    FreeLocks
'    'associar ao data control
'    Set dtVisPedido.Recordset = ssDt

    CarregaGrid

'   'Call AtualizaCabecalho
End If
 
 
 Exit Sub

TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
       Resume
    ElseIf Err = 3021 Then
      Call AtualizaCabecalho
      Exit Sub
    Else
      Call Process_Line_Errors("frmVisPedido.Command4_Click")
    End If

End Sub

Private Sub Command5_Click()
    grdItemPed_DblClick
    Call AtualizaCabecalho
End Sub

Private Sub Command6_Click()
  Call AtualizaCabecalho
End Sub

Private Sub Command7_Click()

    Dim response As VbMsgBoxResult
    
    If Not ValidaTela Then Exit Sub
    
    Call AtualizaCabecalho
    
    If txtQtdeItens <> Carrega_Procedure_Scalar("PRODUCAO.Pck_VDA790.PR_SCALAR_TOTAL_ITENS_NO_PEDI", "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, "PM_NUM_PEDIDO", lngNum_Pedido, "PM_COD_LOJA", lngCod_Loja) Then
         MsgBox "Quantidade de itens informada diferente da quantidade de itens digitados", _
         vbCritical, "ATEN��O"
         Exit Sub
    End If
     response = MsgBox("Finalizar o Pedido ?", vbYesNo, "Aten��o")
     If response = vbNo Then
       Exit Sub
     Else

            vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
            vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
            vBanco.Parameters.Remove "PM_NUM_PEDIDO"
            vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
            vBanco.Parameters.Remove "PM_COD_LOJA"
            vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
            vBanco.Parameters.Remove "PM_COD_USUARIO"
            vBanco.Parameters.Add "PM_COD_USUARIO", lngCod_Usuario, ORAPARM_INPUT
            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
             
         vSql = "PRODUCAO.Pck_VDA790.PR_FINALIZA_PEDIDO(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_USUARIO,:PM_CODERRO,:PM_TXTERRO)"
         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
          If vErro = "" Then
            If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
                MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
                Screen.MousePointer = vbDefault
                Exit Sub
            Else
                MsgBox "Pedido finalizado com sucesso!", vbInformation, "Aten��o"
                frmVisPedido.Label1.Visible = False
                Unload Me
            End If
            Unload Me
          Else
            Exit Sub
          End If
     End If
       
End Sub

Private Sub Form_Activate()
  lngNum_Pedido = lblNr_Pedido.Caption
  lngSEQ_PEDIDO = lblSeq_Pedido.Caption
  lngCod_Loja = Mid(Trim(lblDeposito(17).Caption), 1, 2)
  bTipo_Indent = Mid(Trim(Format(lblIndent, "00")), 1, 2)
End Sub

Private Sub Form_Load()
On Error GoTo TrataErro

    
    Dim ss As Snapshot
    Dim vObjOracle2 As Object
    Dim vObjOracle3 As Object
    Dim vObjOracle4 As Object
    Dim vObjOracle5 As Object
    Dim vObjOracle6 As Object
    Dim vObjOracle7 As Object
    Dim cod As Long
    Dim plano As Long
    Dim sq As Long        'sequencia de endere�o
    Dim lojavis As Integer
    
    'setar cursor
    Screen.MousePointer = vbHourglass
    
    lngCod_Repres = 0
    'consulta codigos na base local
'    SQL = " select NUM_PEDIDO, SEQ_PEDIDO,"
'    SQL = SQL & " DT_PEDIDO,"
'    SQL = SQL & " COD_CLIENTE ,"
'    SQL = SQL & "  COD_LOJA,"
'    SQL = SQL & "  COD_PLANO, DESC_PLANO, tp_indent,"
'    SQL = SQL & " TP_PEDIDO,COD_REPRES, COD_VEND,"
'    SQL = SQL & " MENSAGEM, VL_CONTABIL, VL_DESPESA, qtd_itens"
'    SQL = SQL & " from PEDIDO "
'    SQL = SQL & " where NUM_PEDIDO = " & lngNum_Pedido
'    SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'    Set ss = dbAccess.CreateSnapshot(SQL)

    Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO", "PM_NUM_PEDIDO", lngNum_Pedido, "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, "PM_COD_LOJA", lngCod_Loja)

    FreeLocks
    
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "PEDIDO PEDIDO " & lngNum_Pedido & " N�O LOCALIZADO!!!", vbCritical, "ATEN��O"
        'setar cursor
        Screen.MousePointer = vbDefault
        'Unload Me
        Exit Sub
    End If

    'lblVl_Contabil.Caption = Format$(Replace(vObjOracle!vl_contabil, ".", ","), "standard")
    lblVl_Contabil.Caption = Format(vObjOracle!vl_contabil, "0.00")
    txtCOD_PLANO = IIf(IsNull(vObjOracle!cod_plano), 0, vObjOracle!cod_plano)
    txtPlano = vObjOracle!desc_plano
    lngCod_Repres = IIf(vObjOracle!cod_repres = 0, vObjOracle!cod_vend, vObjOracle!cod_repres)
    
    'consulta dados do cabe�alho
'        SQL = "select cli.NOME_CLIENTE,"
'        SQL = SQL & " cid.NOME_CIDADE, CID.COD_UF"
'        SQL = SQL & " from CLIENTE cli, cidade cid "
'        SQL = SQL & " where cli.COD_CLIENTE = :COD  and"
'        SQL = SQL & " cid.cod_cidade = cli.cod_cidade"
    
    cod = vObjOracle!cod_cliente
    
'    oradatabase.Parameters.Remove "COD"
'    oradatabase.Parameters.Add "COD", cod, 1

'    Set ss2 = oradatabase.dbcreatedynaset(SQL, 8&)
    
     vBanco.Parameters.Remove "PM_COD"
     vBanco.Parameters.Add "PM_COD", cod, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_CLIENTE_PELO_CODIGO(:PM_COD,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
        Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Exit Sub
      Else
         Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value
      End If
        
    If vObjOracle2.EOF And vObjOracle2.BOF Then
        MsgBox "CONSULTA ORACLE PEDIDO PENDENTE " & lngNum_Pedido & " FALHOU!!!", vbCritical, "ATEN��O"
        'setar cursor
        Screen.MousePointer = vbDefault
        Unload Me
        Exit Sub
    End If
                
    
    'display dados na tela
    lblNr_Pedido.Caption = vObjOracle!num_pedido
    lblSeq_Pedido.Caption = vObjOracle!seq_pedido
    lblDt_Pedido = Format$(CDate(vObjOracle!dt_pedido), "dd/mm/yy")
    'txtVl_Despesa = Format(vObjOracle!vl_despesa, "0.00")
    txtVl_Despesa = vObjOracle!vl_despesa
    lblIndent = vObjOracle!tp_indent
   
    'SQL = "Select desc_tipo"
    'SQL = SQL & " from indent.tipo_indent"
    'SQL = SQL & " where cod_tipo=:cod"
    
    'oradatabase.Parameters.Remove "cod"
    'oradatabase.Parameters.Add "cod", lblIndent, 1
    
    'Set ss7 = oradatabase.dbcreatedynaset(SQL, 8&)
    
     vBanco.Parameters.Remove "PM_COD"
     vBanco.Parameters.Add "PM_COD", CLng(lblIndent.Caption), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_TIPO_INDENT(:PM_COD,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
      Screen.MousePointer = vbDefault
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
       Exit Sub
    Else
       Set vObjOracle7 = vBanco.Parameters("PM_CURSOR1").Value
    End If
    
    If vObjOracle7.EOF And vObjOracle7.BOF Then
      lblIndent = Format(lblIndent, "00")
    Else
      lblIndent = Format(lblIndent, "00") & "-" & vObjOracle7!desc_tipo
    End If
   
    If Not IsNull(vObjOracle!mensagem) Then
        txtMsgNota.Text = vObjOracle!mensagem
    End If
    txtTpPedido.Text = vObjOracle!tp_pedido
    
    txtCOD_REPRESENTANTE.Text = vObjOracle!cod_repres
    txtSEQ_REPRES.Text = 0
    If vObjOracle!cod_repres = 0 Then
       lblPSEUDOREPR.Caption = ""
    Else
       'SQL = "select nvl(a.pseudonimo,' ') repres"
       'SQL = SQL & " from representante a"
       'SQL = SQL & " Where a.cod_repres(+) = :repres"
    
       'oradatabase.Parameters.Remove "repres"
       'oradatabase.Parameters.Add "repres", txtCOD_REPRESENTANTE.Text, 1
       'Set ss5 = oradatabase.dbcreatedynaset(SQL, 8&)
       
         vBanco.Parameters.Remove "PM_COD"
         vBanco.Parameters.Add "PM_COD", CLng(txtCOD_REPRESENTANTE.Text), ORAPARM_INPUT
         vBanco.Parameters.Remove "PM_CURSOR1"
         vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
         vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
         vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
         vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
         vBanco.Parameters.Remove "PM_CODERRO"
         vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
         vBanco.Parameters.Remove "PM_TXTERRO"
         vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
        
         vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_REPRESENTANTE_CODIGO(:PM_COD,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
        If vErro <> "" Then
          Screen.MousePointer = vbDefault
           MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
           Exit Sub
        Else
           Set vObjOracle5 = vBanco.Parameters("PM_CURSOR1").Value
        End If
       
       lblPSEUDOREPR.Caption = vObjOracle5!repres
    End If
    
    txtCOD_VENDEDOR.Text = vObjOracle!cod_vend
    
    If vObjOracle!cod_vend = 0 Then
       lblPSEUDOVEND.Caption = ""
    Else
       'SQL = "select nvl(a.pseudonimo,' ') vend"
       'SQL = SQL & " from representante a"
       'SQL = SQL & " Where a.cod_repres(+) = :vend"
       
       'oradatabase.Parameters.Remove "vend"
       'oradatabase.Parameters.Add "vend", txtCOD_VENDEDOR.Text, 1
       'Set ss5 = oradatabase.dbcreatedynaset(SQL, 8&)
       
         vBanco.Parameters.Remove "PM_COD"
         vBanco.Parameters.Add "PM_COD", CLng(txtCOD_VENDEDOR.Text), ORAPARM_INPUT
         vBanco.Parameters.Remove "PM_CURSOR1"
         vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
         vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
         vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
         vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
         vBanco.Parameters.Remove "PM_CODERRO"
         vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
         vBanco.Parameters.Remove "PM_TXTERRO"
         vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
        
         vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_REPRESENTANTE_CODIGO(:PM_COD,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
        If vErro <> "" Then
          Screen.MousePointer = vbDefault
           MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
           Exit Sub
        Else
           Set vObjOracle5 = vBanco.Parameters("PM_CURSOR1").Value
        End If
       
       lblPSEUDOVEND.Caption = vObjOracle5!repres
    End If
    
    
    lngCod_Loja = vObjOracle!cod_loja
       
    'SQL = "select nome_fantasia, cod_uf"
    'SQL = SQL & " from loja a, cidade b"
    'SQL = SQL & " where  a.cod_cidade = b.cod_cidade and cod_loja = :loja"
       
    'lojavis = vObjOracle!COD_LOJA
    'oradatabase.Parameters.Remove "loja"
    'oradatabase.Parameters.Add "loja", lngCod_Loja, 1
    'Set ss4 = oradatabase.dbcreatedynaset(SQL, 8&)
    
     vBanco.Parameters.Remove "PM_COD"
     vBanco.Parameters.Add "PM_COD", lngCod_Loja, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_LOJA_PELO_CODIGO(:PM_COD,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
      Screen.MousePointer = vbDefault
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
       Exit Sub
    Else
       Set vObjOracle4 = vBanco.Parameters("PM_CURSOR1").Value
    End If
       
    lblDeposito(17).Caption = Format(vObjOracle!cod_loja, "00") & "-" & vObjOracle4!nome_fantasia
    UF_origem = vObjOracle4!cod_uf
    lblDados(1).Caption = vObjOracle!cod_cliente
    lblDados(2).Caption = vObjOracle2!nome_cliente
    lblDados(3).Caption = vObjOracle2!nome_cidade
    ln1.X1 = lblDados(3).Width + 1520
    ln1.X2 = ln1.X1 - 120
    lblDados(4).Left = lblDados(3).Width + 1590
    lblDados(4).Caption = vObjOracle2!cod_uf
    'uf_destino = lblDados(4).Caption
    
    txtQtdeItens.Text = vObjOracle!qtd_itens
    
    'dt_pedido = lbldt_pedido.Caption
        
    'fechar consulta
    vObjOracle.Close
    'DoEvents
    
    CarregaGrid
    
    FreeLocks
    'associar ao data control
    'Set dtVisPedido.Recordset = ssDt
    
    'setar cursor
    Screen.MousePointer = vbDefault
    
    If strTipoRespCliente = "V" Or strTipoRespCliente = "M" Then
      txtCOD_VENDEDOR.Enabled = False
      'txtCOD_REPRESENTANTE.Enabled = True
    Else
      txtCOD_VENDEDOR.Enabled = True
      'txtCOD_REPRESENTANTE.Enabled = False
    End If
    
    Exit Sub

TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
       Resume
    Else
      Call Process_Line_Errors("frmVisPedido.Form_Load")
    End If
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
 
  If Not ssDt Is Nothing Then
       Set ssDt = Nothing
  End If
  
End Sub

Private Sub grdItemPed_DblClick()
On Error GoTo TrataErro

grdItemPed.Col = 1

 If grdItemPed.Row < 1 Or grdItemPed.Text = "" Then
    MsgBox "Selecione um item para alterar", vbExclamation, "Aten��o"
    Exit Sub
 End If
 grdItemPed.Col = 1
 frmItem.lblItem = grdItemPed.Text
 grdItemPed.Col = 2
 frmItem.txtQTD = grdItemPed.Text
 grdItemPed.Col = 3
 frmItem.txtCOD_DPK = grdItemPed.Text
 grdItemPed.Col = 4
 frmItem.txtForn = grdItemPed.Text
 grdItemPed.Col = 5
 frmItem.txtFabrica = grdItemPed.Text
 grdItemPed.Col = 6
 frmItem.txtDescricao = grdItemPed.Text
 grdItemPed.Col = 7
 frmItem.txtPreco_Liq = grdItemPed.Text
 
  bTipo_Item = 2
  frmItem.Show 1
  'consulta dados dos itens
'    SQL = "select NUM_ITEM_PEDIDO,COD_DPK,QTD_SOLICITADA,"
'    SQL = SQL & "PRECO_LIQUIDO,"
'    SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as PR_LIQ,"
'    SQL = SQL & "SITUACAO, COD_FORNECEDOR,"
'    SQL = SQL & "COD_FABRICA,desc_item "
'    SQL = SQL & "from ITEM_PEDIDO "
'    SQL = SQL & "where NUM_PEDIDO = " & lngNum_Pedido
'    SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
'    SQL = SQL & " and cod_loja = " & lngCod_Loja
'    SQL = SQL & " order by NUM_ITEM_PEDIDO"
'    Set ssDt = dbAccess.OpenRecordset(SQL, dbOpenSnapshot)
'    FreeLocks
'
'    'associar ao data control
'    Set dtVisPedido.Recordset = ssDt

   CarregaGrid
  Call AtualizaCabecalho
  
 Exit Sub
 
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors("frmVispedido.grdItemPed_DblClick")
    End If
End Sub

Private Sub grdItemPed_RowColChange()
    If grdItemPed.Col = 7 Or grdItemPed.Col = 8 Then
        'grdItemPed.Text = Replace(grdItemPed.Text, ".", ","
    End If
End Sub

Private Sub lblDt_Pedido_KeyPress(KeyAscii As Integer)
  Call Data(KeyAscii, lblDt_Pedido)
End Sub

Private Sub lblDt_Pedido_LostFocus()
  If lblDt_Pedido < Data_Faturamento Then
    MsgBox "A data do pedido deve ser maior que a data do faturamento", vbCritical, "Aten��o"
    lblDt_Pedido.SetFocus
  End If
  
End Sub

Private Sub txtCOD_PLANO_KeyPress(KeyAscii As Integer)
       
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCOD_PLANO_LostFocus()
     
     
     Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors("frmVisPedido.txtCOD_PLANO_LostFocus")
    End If
End Sub


Private Sub txtCOD_REPRESENTANTE_DblClick()
    
    txtResposta = "0"
    frmRepresentante.Show vbModal
    If txtResposta <> "0" Then
        txtCOD_REPRESENTANTE.Text = txtResposta
        Call txtCOD_REPRESENTANTE_LostFocus
    End If


End Sub


Private Sub txtCOD_REPRESENTANTE_KeyPress(KeyAscii As Integer)
  
    If Asc(KeyAscii) = 49 And txtCOD_REPRESENTANTE = "" Then
      Call txtCOD_REPRESENTANTE_DblClick
    Else
      KeyAscii = Numerico(KeyAscii)
    End If

End Sub

Private Sub txtCOD_REPRESENTANTE_LostFocus()
     On Error GoTo TrataErro

    Dim SQL As String
    Dim ss As Object

    If txtCOD_REPRESENTANTE.Text = "" Then
      txtCOD_REPRESENTANTE.Text = 0
    End If
   
'    SQL = "select PSEUDONIMO REPR_PSEUDO"
'    SQL = SQL & " from REPRESENTANTE "
'    SQL = SQL & " where COD_REPRES = :repres"
'    SQL = SQL & " and tipo in ('R','P') AND"
'    SQL = SQL & " SITUACAO = 0 AND"
'    SQL = SQL & " DT_DESLIGAMENTO IS NULL"
'
'    oradatabase.Parameters.Remove "repres"
'    oradatabase.Parameters.Add "repres", txtCOD_REPRESENTANTE.Text, 1
'
'    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)

Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_REPRESEN_PELO_CODIGO", "PM_COD", CLng(txtCOD_REPRESENTANTE.Text))
                
    If vObjOracle.EOF And vObjOracle.BOF Then
      MsgBox "Representante n�o cadastrado", vbInformation, "Aten��o"
      txtCOD_REPRESENTANTE.Text = "0"
      txtSEQ_REPRES.Text = "0"
      lblPSEUDOREPR.Caption = ""
    Else
        'preposto
        txtSEQ_REPRES.Text = 0
        lblPSEUDOREPR.Caption = vObjOracle!pseudonimo
    End If
    Call txtTpPedido_LostFocus
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
     Resume
   Else
    Call Process_Line_Errors(SQL)
   End If
End Sub







Private Sub txtCOD_VENDEDOR_DblClick()
    txtCOD_VENDEDOR.DataChanged = False
    frmVendedor.Show vbModal
    If txtResposta <> strTipoRespCliente Then
        txtCOD_VENDEDOR.Text = Val(txtResposta)
        Call txtCOD_VENDEDOR_LostFocus
    End If
End Sub


Private Sub txtCOD_VENDEDOR_KeyPress(KeyAscii As Integer)
    If Asc(KeyAscii) = 49 And txtCOD_VENDEDOR = "" Then
     Call txtCOD_VENDEDOR_DblClick
   Else
     KeyAscii = Numerico(KeyAscii)
   End If
End Sub

Private Sub txtCOD_VENDEDOR_LostFocus()
     On Error GoTo TrataErro

    Dim ss As Object
    Dim SQL As String
    
    If txtCOD_VENDEDOR.Text = "" Then
      txtCOD_VENDEDOR.Text = 0
    End If
 
    If txtCOD_VENDEDOR.DataChanged Then
         If CLng(txtCOD_VENDEDOR.Text) = "599" Then
            txtTpPedido.Text = "21"
                        
            txtCOD_REPRESENTANTE.Text = "0"
            lblPSEUDOREPR.Caption = ""
            
            'SQL = "select NOME_REPRES from REPRESENTANTE "
            'SQL = SQL & "where COD_REPRES = 599"
            
            'Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
            
            Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_REPRESENTANTE_599")
            
            If (Not vObjOracle.EOF And Not vObjOracle.BOF) Then
                txtCOD_VENDEDOR = "599"
                lblPSEUDOVEND.Caption = vObjOracle("NOME_REPRES")
            End If
                        
            'txtCOD_REPRESENTANTE.Enabled = False
            txtSEQ_REPRES.Enabled = False
            
        Else
                'pedido tipo 21 ou 22
                'SQL = "select PSEUDONIMO from REPRESENTANTE where "
                'SQL = SQL & " COD_REPRES = " & txtCOD_VENDEDOR.Text
                'SQL = SQL & " and tipo in ('A','M','V') AND"
                'SQL = SQL & " SITUACAO = 0 AND"
                'SQL = SQL & " DT_DESLIGAMENTO IS NULL"
                
                
                'Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
                
               Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_TELEMARK_PELO_CODIGO", "PM_COD", CLng(txtCOD_VENDEDOR.Text))
                
               If (vObjOracle.EOF And vObjOracle.BOF) Or txtCOD_VENDEDOR = "0" Then
                    MsgBox "Vendedor " & txtCOD_VENDEDOR.Text & " n�o cadastrado", vbInformation, "Aten��o"
                    txtCOD_VENDEDOR.Text = "0"
                    lblPSEUDOVEND.Caption = ""
                    Call txtTpPedido_LostFocus
                    'txtCOD_VENDEDOR.SetFocus
                    Exit Sub
                Else
                    If IsNull(vObjOracle!pseudonimo) Then
                      lblPSEUDOVEND.Caption = ""
                    Else
                      lblPSEUDOVEND.Caption = vObjOracle("PSEUDONIMO")
                    End If
                End If
                
            End If
            
        End If
        
    
      Call txtTpPedido_LostFocus
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
End Sub

Private Sub txtDt_Pedido_Change()

End Sub

Private Sub txtDt_Pedido_KeyPress(KeyAscii As Integer)
  
End Sub

Private Sub txtMsgNota_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtMsgNota_LostFocus()
   txtMsgNota = Tira_Caracter(txtMsgNota)
End Sub








Private Sub txtPlano_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtPlano_LostFocus()
  txtPlano = Tira_Caracter(txtPlano)
End Sub


Private Sub txtQtdeItens_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtQtdeItens)
End Sub

Private Sub txtQtdeItens_LostFocus()
  
  If txtQtdeItens.Text = "" Then
    MsgBox "Entre com a quantidade de itens do Pedido", vbInformation, "Aten��o"
    txtQtdeItens.SetFocus
    Exit Sub
  End If
  
End Sub


Private Sub txtSEQ_REPRES_GotFocus()
  txtSEQ_REPRES.BackColor = Vermelho
End Sub


Private Sub txtSEQ_REPRES_LostFocus()
  If txtSEQ_REPRES.Text = "" Then
    txtSEQ_REPRES.Text = 0
  End If
  txtSEQ_REPRES.BackColor = Branco
End Sub





Private Sub txtTpPedido_GotFocus()
  txtTpPedido.BackColor = Vermelho
End Sub

Private Sub txtTpPedido_KeyPress(KeyAscii As Integer)

  KeyAscii = Numerico(KeyAscii)

End Sub


Private Sub txtTpPedido_LostFocus()
  
  txtTpPedido.BackColor = Branco

  If txtTpPedido <> "10" And txtTpPedido <> "21" And _
     txtTpPedido <> "22" And txtTpPedido <> "11" Then
    MsgBox "Os tipos de pedidos permitido s�o: 10 e 21", vbExclamation, "Aten��o"
    Exit Sub
  End If
  
  If txtCOD_VENDEDOR = "0" Then
    txtTpPedido = "10"
  ElseIf txtCOD_VENDEDOR <> "0" And txtCOD_REPRESENTANTE = "0" Then
    txtTpPedido = "21"
  Else
    txtTpPedido = "22"
  End If

  
End Sub


Private Sub txtVl_Despesa_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtVl_Despesa)
End Sub


Private Sub txtVl_Despesa_LostFocus()
  'txtVl_Despesa = Format(FmtBR(txtVl_Despesa), "0.00")
  txtVl_Despesa = Replace(txtVl_Despesa, ",", ".")
  Call AtualizaCabecalho
End Sub


