VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmLogI 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Log Item - Retorno PST"
   ClientHeight    =   4230
   ClientLeft      =   2340
   ClientTop       =   2340
   ClientWidth     =   6690
   Icon            =   "frmLogI.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4230
   ScaleWidth      =   6690
   Begin MSGrid.Grid grdLogI 
      Height          =   2295
      Left            =   1080
      TabIndex        =   0
      Top             =   960
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   4048
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
End
Attribute VB_Name = "frmLogI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim ss As Object
  Dim i As Long
  
'  SQL = "Select cod_fabrica,qtd_atendida,"
'  SQL = SQL & " valor"
'  SQL = SQL & " From indent.retorno_itped_pst"
'  SQL = SQL & " Where num_nota = :nf"
'
'  oradatabase.Parameters.Remove "nf"
'  oradatabase.Parameters.Add "nf", lngResposta, 1
'
'  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_RETORNO_ITPED_PST", "PM_NUM_NOTA", lngResposta)
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "Itens n�o localizado", vbInformation, "Aten��o"
    Exit Sub
  End If
  
    grdLogI.Visible = True
    With frmLogI.grdLogI
        .Cols = 3
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 1800
        .ColWidth(1) = 1000
        .ColWidth(2) = 1500
        
        .Row = 0
        .Col = 0
        .Text = "Fabrica"
        .Col = 1
        .Text = "Qtd.Atendida"
        .Col = 2
        .Text = "Valor Unit."
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!cod_fabrica
            .Col = 1
            .Text = vObjOracle!qtd_atendida
            .Col = 2
            .Text = vObjOracle!Valor
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
    Screen.MousePointer = 0

  
  
End Sub


