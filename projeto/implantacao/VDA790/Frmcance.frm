VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCancela 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Motivos de Cancelamento"
   ClientHeight    =   3495
   ClientLeft      =   1275
   ClientTop       =   2340
   ClientWidth     =   6720
   Icon            =   "Frmcance.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3495
   ScaleWidth      =   6720
   Begin MSGrid.Grid grdCancela 
      Height          =   2175
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "Frmcance.frx":000C
   End
   Begin Bot�o.cmd Command1 
      Default         =   -1  'True
      Height          =   495
      Left            =   3640
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Frmcance.frx":0028
      PICN            =   "Frmcance.frx":0044
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command2 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   5080
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   2880
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Frmcance.frx":039E
      PICN            =   "Frmcance.frx":03BA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   120
      TabIndex        =   5
      Top             =   2760
      Width           =   6330
      _ExtentX        =   11165
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   4815
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   1500
   End
End
Attribute VB_Name = "frmCancela"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strPesquisa As String

Private Sub Command1_Click()
    grdCancela_DblClick
End Sub

Private Sub Command2_Click()
  cod_cancel = 0
  Unload Me
End Sub

Private Sub Form_Load()
  
    On Error GoTo TrataErro
    
    Dim ss As Object
    Dim i As Integer
    
'    'mouse
'    Screen.MousePointer = vbHourglass
'
'    'montar SQL
'    SQL = "select COD_CANCEL,DESC_CANCEL "
'    SQL = SQL & "from CANCEL_PEDNOTA "
'    SQL = SQL & "order by DESC_CANCEL"
'
'    'criar consulta
'    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_MOTIV_CANCELAMENTO")
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o h� Motivo de Cancelamento cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    
    With frmCancela.grdCancela
        .Cols = 2
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 5325
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Motivo Cancelamento"
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle("COD_CANCEL")
            .Col = 1
            .Text = vObjOracle("DESC_CANCEL")
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
    
            
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors("frmCancela.Form_Load")
   End If

End Sub

Private Sub grdCancela_DblClick()

  If grdCancela.Row < 1 Or grdCancela.Text = "" Then
    MsgBox "Selecione o motivo do cancelamento", vbExclamation, "Aten��o"
    Exit Sub
  End If

 grdCancela.Col = 0
 cod_cancel = grdCancela.Text
 Unload Me
End Sub

Private Sub grdCancela_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdCancela.Col = 0
    If KeyAscii = 13 Then
        grdCancela.Col = 0
        cod_cancel = grdCancela.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdCancela.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdCancela
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdCancela.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdCancela
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdCancela.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub


