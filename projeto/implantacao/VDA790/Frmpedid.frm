VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPedidos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pedidos Digitados"
   ClientHeight    =   3840
   ClientLeft      =   285
   ClientTop       =   2340
   ClientWidth     =   7965
   Icon            =   "Frmpedid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3840
   ScaleWidth      =   7965
   Begin MSGrid.Grid grdPedido 
      Height          =   2535
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   7695
      _Version        =   65536
      _ExtentX        =   13573
      _ExtentY        =   4471
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "Frmpedid.frx":000C
   End
   Begin Bot�o.cmd Command2 
      Default         =   -1  'True
      Height          =   495
      Left            =   4965
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   3240
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Frmpedid.frx":0028
      PICN            =   "Frmpedid.frx":0044
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command1 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   6405
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3240
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Fechar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "Frmpedid.frx":039E
      PICN            =   "Frmpedid.frx":03BA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   120
      TabIndex        =   5
      Top             =   3120
      Width           =   7650
      _ExtentX        =   13494
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   6135
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   1500
   End
End
Attribute VB_Name = "frmPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strPesquisa As String

Private Sub Command1_Click()
  Unload Me
End Sub


Private Sub Command2_Click()
    grdPedido_DblClick
End Sub

Private Sub Form_Load()
  Dim ss As Snapshot
  Dim ss2 As Snapshot
  Dim i As Integer

'  Screen.MousePointer = 11
'
'  SQL = "Select a.num_pedido, a.seq_pedido,"
'  SQL = SQL & " a.cod_loja, "
'  SQL = SQL & " a.dt_digitacao, "
'  SQL = SQL & " a.qtd_itens,"
'  SQL = SQL & " count(b.num_pedido) as qt "
'  SQL = SQL & " from pedido AS a LEFT JOIN  item_pedido AS b "
'  SQL = SQL & " ON (a.num_pedido = b.num_pedido "
'  SQL = SQL & " and a.seq_pedido = b.seq_pedido and"
'  SQL = SQL & " a.cod_loja = b.cod_loja) "
'  SQL = SQL & " where a.fl_pedido_finalizado = 'N' "
'  SQL = SQL & " group by a.num_pedido,a.seq_pedido,a.cod_loja,"
'  SQL = SQL & " a.dt_digitacao, a.qtd_itens"
'  SQL = SQL & " order by a.num_pedido,a.dt_digitacao"
'
'  Set ss = dbAccess.CreateSnapshot(SQL)
  
  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_ITENS_PEDIDO", "PM_FL_PEDIDO_FINALIZADO", strFl_Lib)
  
  Screen.MousePointer = vbHourglass
  
  FreeLocks

  If vObjOracle.EOF Then
    MsgBox "N�o existe pedido digitado", vbExclamation, "Aten��o"
    Screen.MousePointer = vbDefault
    Exit Sub
  End If
  
  grdPedido.Visible = True
  vObjOracle.MoveLast
  With frmPedidos.grdPedido
        .Cols = 6
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 1400
        .ColWidth(1) = 500
        .ColWidth(2) = 1000
        .ColWidth(3) = 1500
        .ColWidth(4) = 800
        .ColWidth(5) = 800
        
        .Row = 0
        .Col = 0
        .Text = "  Nr.Pedido"
        .Col = 1
        .Text = "Sq."
        .Col = 2
        .Text = "  Dep�sito"
        .Col = 3
        .Text = "  Dt.Digita��o"
        .Col = 4
        .Text = " Qtd.Inf."
        .Col = 5
        .Text = "Qtd.Dig."
            
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .ColAlignment(0) = 1
            .Text = vObjOracle!num_pedido
            .Col = 1
            .ColAlignment(1) = 2
            .Text = vObjOracle!seq_pedido
            .Col = 2
            .ColAlignment(2) = 2
            .Text = vObjOracle!cod_loja
            .Col = 3
            .ColAlignment(3) = 2
            .Text = Format$(vObjOracle!dt_digitacao, "dd/mm/yy hh:nn")
            .Col = 4
            .ColAlignment(4) = 2
            .Text = vObjOracle!qtd_itens
            .Col = 5
            .ColAlignment(5) = 2
            .Text = vObjOracle!qt
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
    vObjOracle.Close
            
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
     Resume
   Else
     Call Process_Line_Errors("frmPedidos.Form_Load")
   End If

End Sub


Private Sub Form_Unload(Cancel As Integer)
 Set frmPedidos = Nothing
End Sub


Private Sub grdPedido_DblClick()
  Dim response As String
  
  If grdPedido.Row < 1 Or grdPedido.Text = "" Then
    MsgBox "Selecione um pedido", vbExclamation, "Aten��o"
    Exit Sub
  End If
    
  Screen.MousePointer = 11
  grdPedido.Col = 0
  lngNum_Pedido = grdPedido.Text
  txtResposta = grdPedido.Text
  grdPedido.Col = 1
  lngSEQ_PEDIDO = grdPedido.Text
  grdPedido.Col = 2
  lngCod_Loja = grdPedido.Text
  grdPedido.Col = 4
  lngQtd_inf = grdPedido.Text
  grdPedido.Col = 5
  lngQtd_dig = grdPedido.Text
  If strFl_Lib <> "S" Then
    frmVisPedido.Show vbModal
  Else
     Screen.MousePointer = 0
     response = MsgBox("Libera o Pedido ?", vbYesNo, "Aten��o")
     If response = vbNo Then
       'Exit Sub
        If grdPedido.Row < 1 Or grdPedido.Text = "" Then
          MsgBox "Selecione um pedido", vbExclamation, "Aten��o"
          Exit Sub
        End If
    
        Screen.MousePointer = 11
        grdPedido.Col = 0
        lngNum_Pedido = grdPedido.Text
        txtResposta = grdPedido.Text
        grdPedido.Col = 1
        lngSEQ_PEDIDO = grdPedido.Text
        grdPedido.Col = 2
        lngCod_Loja = grdPedido.Text
        grdPedido.Col = 4
        lngQtd_inf = grdPedido.Text
        grdPedido.Col = 5
        lngQtd_dig = grdPedido.Text
        Unload Me
        If strFl_Lib <> "S" Then
          frmVisPedido.Show vbModal
        Else
           Screen.MousePointer = 0
           response = MsgBox("Coloca o Pedido em Manuten��o ?", vbYesNo, "Aten��o")
           If response = vbNo Then
             Exit Sub
           Else
              Unload Me
              vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
              vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
              vBanco.Parameters.Remove "PM_NUM_PEDIDO"
              vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
              vBanco.Parameters.Remove "PM_COD_LOJA"
              vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
              vBanco.Parameters.Remove "PM_COD_USUARIO"
              vBanco.Parameters.Add "PM_COD_USUARIO", lngCod_Usuario, ORAPARM_INPUT
              vBanco.Parameters.Remove "PM_CODERRO"
              vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
              vBanco.Parameters.Remove "PM_TXTERRO"
              vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
                   
               vSql = "PRODUCAO.Pck_VDA790.PR_MANUT_PEDIDO(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_USUARIO,:PM_CODERRO,:PM_TXTERRO)"
               vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
                If vErro = "" Then
                  If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
                      MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
                      Screen.MousePointer = vbDefault
                      Exit Sub
                  Else
                      MsgBox "Pedido colocado em Manuten��o com sucesso, verifique!", vbInformation, "Aten��o"
                      Unload Me

                  End If
                End If
           End If
        End If
       
       
     Else
       
       If lngQtd_inf <> lngQtd_dig Then
         MsgBox "Quantidade de itens informada diferente da quantidade de itens digitados", _
         vbCritical, "ATEN��O"
         Exit Sub
       Else
         Call Grava_Oracle(lngCod_Loja, lngNum_Pedido, lngSEQ_PEDIDO)
         Unload Me
       End If
       Unload Me
     End If
  End If
  Unload Me
End Sub

Private Sub grdPedido_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
       
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdPedido.Col = 0
    If KeyAscii = 13 Then
        Screen.MousePointer = 11
        grdPedido.Col = 0
        lngNum_Pedido = grdPedido.Text
        txtResposta = grdPedido.Text
        grdPedido.Col = 1
        lngSEQ_PEDIDO = grdPedido.Text
        grdPedido.Col = 2
        lngCod_Loja = grdPedido.Text
        grdPedido.Col = 4
        lngQtd_inf = grdPedido.Text
        grdPedido.Col = 5
        lngQtd_dig = grdPedido.Text
        Unload Me
        frmVisPedido.Show 1
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdPedido.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdPedido
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdPedido.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdPedido
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdPedido.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub


Public Sub Grava_Oracle(loja As Long, pedido As Long, seq As Long)
'  Dim ss As Object

'   SQL = "Select count(*) total"
'   SQL = SQL & " From indent.pedido_indent"
'   SQL = SQL & " where seq_pedido = :seq"
'   SQL = SQL & " and num_pedido = :num"
'   SQL = SQL & " and cod_loja = :loja"
'
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "seq", seq, 1
'   oradatabase.Parameters.Add "num", pedido, 1
'   oradatabase.Parameters.Add "loja", loja, 1
'
'   Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
'
'   If ss!total = 0 Then
'     'comando para atualizar base local
'     SQL = "update PEDIDO set "
'     SQL = SQL & " FL_PEDIDO_FINALIZADO = 'T'"
'     SQL = SQL & " where NUM_PEDIDO = " & pedido
'     SQL = SQL & " and SEQ_PEDIDO = " & seq
'     SQL = SQL & " and cod_loja = " & loja
'
'     'atualizar base local
'     dbAccess.Execute SQL, dbFailOnError
'     FreeLocks
'
'    'montar comando para atualizar oracle
'          SQL = "select b.num_pedido,"
'     SQL = SQL & " b.seq_pedido,b.cod_loja,b.num_item_pedido,"
'     SQL = SQL & " b.cod_dpk, b.cod_fornecedor, b.cod_fabrica,"
'     SQL = SQL & " b.preco_liquido,a.tp_indent,b.qtd_solicitada, "
'     SQL = SQL & " b.desc_item, b.situacao,a.tp_pedido,a.dt_digitacao,"
'     SQL = SQL & " a.cod_repres,a.cod_vend,a.cod_cliente,"
'     SQL = SQL & " a.vl_contabil, a.vl_despesa,a.desc_plano,a.mensagem,"
'     SQL = SQL & " a.qtd_item_pedido as qtd_item_pedido"
'     SQL = SQL & " from PEDIDO a,ITEM_PEDIDO b "
'     SQL = SQL & " where a.NUM_PEDIDO = " & pedido
'     SQL = SQL & " and a.SEQ_PEDIDO = " & seq
'     SQL = SQL & " and a.cod_loja = " & loja
'     SQL = SQL & " and a.num_pedido = b.num_pedido"
'     SQL = SQL & " and a.seq_pedido = b.seq_pedido"
'     SQL = SQL & " and a.cod_loja = b.cod_loja"
'
'     Set ss = dbAccess.CreateSnapshot(SQL)
'     FreeLocks
'
'     Do
'       SQL = "BEGIN"
'       SQL = SQL & " Insert into indent.item_pedido_indent"
'       SQL = SQL & " ( cod_loja,num_pedido,seq_pedido,"
'       SQL = SQL & " tp_indent,num_item_pedido,cod_dpk,"
'       SQL = SQL & " cod_fornecedor,cod_fabrica,desc_item,"
'       SQL = SQL & " qtd_solicitada,qtd_atendida,"
'       SQL = SQL & " preco_unitario_liq,situacao)"
'       SQL = SQL & " Values(:loja,:num_pedido,:seq," & ss!tp_indent
'       SQL = SQL & ", " & ss!num_item_pedido & "," & ss!cod_dpk & ","
'       SQL = SQL & ss!cod_fornecedor & ",'" & ss!cod_fabrica & "','"
'       SQL = SQL & ss!desc_item & "'," & ss!qtd_solicitada & ","
'       SQL = SQL & ss!qtd_solicitada & "," & ss!preco_liquido & ",0);"
'       SQL = SQL & " COMMIT;"
'       SQL = SQL & "EXCEPTION"
'       SQL = SQL & " WHEN OTHERS THEN"
'       SQL = SQL & " ROLLBACK;"
'       SQL = SQL & " :cod_errora := SQLCODE;"
'       SQL = SQL & " :txt_errora := SQLERRM;"
'       SQL = SQL & "END;"
'
'       oradatabase.Parameters.Remove "loja"
'       oradatabase.Parameters.Add "loja", loja, 1
'       oradatabase.Parameters.Remove "num_pedido"
'       oradatabase.Parameters.Add "num_pedido", pedido, 1
'       oradatabase.Parameters.Remove "seq"
'       oradatabase.Parameters.Add "seq", seq, 1
'
'       oradatabase.Parameters.Remove "cod_errora"
'       oradatabase.Parameters.Add "cod_errora", 0, 2
'       oradatabase.Parameters.Remove "txt_errora"
'       oradatabase.Parameters.Add "txt_errora", "", 2
'       oradatabase.ExecuteSQL SQL
'       If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'         MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'           " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'         SQL = "update PEDIDO set FL_PEDIDO_FINALIZADO = 'N'"
'         SQL = SQL & " where NUM_PEDIDO = " & pedido
'         SQL = SQL & " and seq_pedido = " & seq
'         SQL = SQL & " and COD_LOJA = " & loja
'         dbAccess.Execute SQL, dbFailOnError
'         FreeLocks
'       End If
'       ss.MoveNext
'     Loop Until ss.EOF
'     ss.MoveFirst
'
'       SQL = "BEGIN"
'       SQL = SQL & " Insert into indent.pedido_indent"
'       SQL = SQL & " ( cod_loja,num_pedido,seq_pedido,"
'       SQL = SQL & " tp_indent,num_nota,cod_filial,"
'       SQL = SQL & " tp_pedido,dt_digitacao,dt_emissao_nota,"
'       SQL = SQL & " cod_repres,cod_vend,"
'       SQL = SQL & " cod_cliente,vl_pedido,"
'       SQL = SQL & " vl_contabil,vl_despesa,fl_ger_nfis,"
'       SQL = SQL & " desc_plano,mensagem,situacao, cod_cancel)"
'       SQL = SQL & " Values(:loja,:num_pedido,:seq," & ss!tp_indent
'       SQL = SQL & ", NULL," & Trim(Filial_Ped) & "," & ss!tp_pedido & ","
'       SQL = SQL & " TO_DATE('" & Format$(Data_Faturamento, "DD/MM/YY") & "'||TO_CHAR(SYSDATE,'HH24MISS')" & ",'DD/MM/RRHH24MISS'),"
'       SQL = SQL & " NULL,"
'       SQL = SQL & ss!cod_repres & "," & ss!cod_vend & ","
'       SQL = SQL & ss!cod_cliente & "," & ss!vl_contabil
'       SQL = SQL & "," & ss!vl_contabil & "," & ss!vl_despesa & ",'N','"
'       SQL = SQL & ss!desc_plano & "','" & ss!mensagem & "',0,0);"
'       SQL = SQL & " COMMIT;"
'       SQL = SQL & "EXCEPTION"
'       SQL = SQL & " WHEN OTHERS THEN"
'       SQL = SQL & " ROLLBACK;"
'       SQL = SQL & " :cod_errora := SQLCODE;"
'       SQL = SQL & " :txt_errora := SQLERRM;"
'       SQL = SQL & "END;"
'
'       oradatabase.Parameters.Remove "loja"
'       oradatabase.Parameters.Add "loja", loja, 1
'       oradatabase.Parameters.Remove "num_pedido"
'       oradatabase.Parameters.Add "num_pedido", pedido, 1
'       oradatabase.Parameters.Remove "seq"
'       oradatabase.Parameters.Add "seq", seq, 1
'
'       oradatabase.Parameters.Remove "cod_errora"
'       oradatabase.Parameters.Add "cod_errora", 0, 2
'       oradatabase.Parameters.Remove "txt_errora"
'       oradatabase.Parameters.Add "txt_errora", "", 2
'       oradatabase.ExecuteSQL SQL
'       If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'         MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'           " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'          SQL = "update PEDIDO set FL_PEDIDO_FINALIZADO = 'N'"
'         SQL = SQL & " where NUM_PEDIDO = " & pedido
'         SQL = SQL & " and seq_pedido = " & seq
'         SQL = SQL & " and COD_LOJA = " & loja
'         dbAccess.Execute SQL, dbFailOnError
'         FreeLocks
'       Else
'         SQL = "update PEDIDO set FL_PEDIDO_FINALIZADO = 'S'"
'         SQL = SQL & " where NUM_PEDIDO = " & pedido
'         SQL = SQL & " and seq_pedido = " & seq
'         SQL = SQL & " and COD_LOJA = " & loja
'         dbAccess.Execute SQL, dbFailOnError
'         FreeLocks
'         MsgBox "PEDIDO FINALIZADO COM SUCESSO", vbInformation, "ATEN��O"
'       End If
'
'   Else
'     MsgBox "PEDIDO J� EXISTE NO ORACLE,VERIFIQUE O NR.", vbExclamation, "ATEN��O"
'     Exit Sub
'   End If

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_FILIAL"
        vBanco.Parameters.Add "PM_COD_FILIAL", CLng(Trim(Filial_Ped)), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_DATA_FATURAMENTO"
        vBanco.Parameters.Add "PM_DATA_FATURAMENTO", CStr(Data_Faturamento), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_USUARIO"
        vBanco.Parameters.Add "PM_COD_USUARIO", lngCod_Usuario, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_LIBERA_PEDIDO(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_FILIAL,:PM_DATA_FATURAMENTO,:PM_COD_USUARIO,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = 0
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
        ElseIf vBanco.Parameters("PM_CODERRO").Value = 666 Then
            MsgBox vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
        Else
            FreeLocks
      'marici 12/02/08
      '       vBanco.Parameters.Remove "PM_NUM_PEDIDO"
      '      vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
      '      vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
      '      vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
      '      vBanco.Parameters.Remove "PM_COD_LOJA"
      '      vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
      '      vBanco.Parameters.Remove "PM_CODERRO"
      '      vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
      '      vBanco.Parameters.Remove "PM_TXTERRO"
      '      vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
                
      '      vSql = "PRODUCAO.Pck_VDA790.PR_DELETE_PEDIDO(:PM_NUM_PEDIDO,:PM_SEQ_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
      '      vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            
            
            
            MsgBox "PEDIDO FINALIZADO COM SUCESSO", vbInformation, "ATEN��O"
        End If
      End If

  
End Sub
