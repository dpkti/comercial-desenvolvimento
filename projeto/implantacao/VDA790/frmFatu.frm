VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.ocx"
Begin VB.Form frmFatu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Atualiza dados Faturamento"
   ClientHeight    =   5940
   ClientLeft      =   285
   ClientTop       =   2340
   ClientWidth     =   9795
   Icon            =   "frmFatu.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   9795
   Begin VB.PictureBox pctDPK 
      Height          =   1095
      Left            =   0
      Picture         =   "frmFatu.frx":000C
      ScaleHeight     =   1035
      ScaleWidth      =   1155
      TabIndex        =   24
      Top             =   0
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   9240
      Top             =   5400
   End
   Begin Crystal.CrystalReport rptPedido 
      Left            =   9240
      Top             =   840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   262150
      ReportFileName  =   "H:\ORACLE\DADOS\32BITS\INDENT.RPT"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   120
      TabIndex        =   22
      Top             =   720
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdAtu 
      Height          =   495
      Left            =   120
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   120
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Atualiza NF"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":0F8B
      PICN            =   "frmFatu.frx":0FA7
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   495
      Left            =   8040
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   120
      Width           =   1605
      _ExtentX        =   2831
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Fec&har"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":12A2
      PICN            =   "frmFatu.frx":12BE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancela 
      Height          =   495
      Left            =   1680
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   120
      Width           =   1725
      _ExtentX        =   3043
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Cancela Pedido"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":19D0
      PICN            =   "frmFatu.frx":19EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimi 
      Height          =   495
      Left            =   3480
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   120
      Width           =   1605
      _ExtentX        =   2831
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Imprimi Pedido"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":1D38
      PICN            =   "frmFatu.frx":1D54
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLote 
      Height          =   495
      Left            =   5160
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   120
      Width           =   1125
      _ExtentX        =   1984
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "I&nclui Lote"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":206E
      PICN            =   "frmFatu.frx":208A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   735
      Left            =   8640
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   960
      Visible         =   0   'False
      Width           =   885
      _ExtentX        =   1561
      _ExtentY        =   1296
      BTYPE           =   3
      TX              =   "&Voltar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":238A
      PICN            =   "frmFatu.frx":23A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox txtNF 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   1440
      MaxLength       =   6
      TabIndex        =   10
      Top             =   1400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtDt 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   4440
      MaxLength       =   8
      TabIndex        =   11
      Top             =   1400
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtContabil 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   6960
      MaxLength       =   15
      TabIndex        =   12
      Top             =   1400
      Visible         =   0   'False
      Width           =   1455
   End
   Begin MSGrid.Grid grdItem 
      Height          =   3615
      Left            =   240
      TabIndex        =   14
      Top             =   1920
      Visible         =   0   'False
      Width           =   9255
      _Version        =   65536
      _ExtentX        =   16325
      _ExtentY        =   6376
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSGrid.Grid grdFatu 
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   920
      Visible         =   0   'False
      Width           =   9495
      _Version        =   65536
      _ExtentX        =   16748
      _ExtentY        =   8281
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdGera_Pedido 
      Height          =   495
      Left            =   6360
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   120
      Width           =   1485
      _ExtentX        =   2619
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Gera Pedido"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmFatu.frx":2837
      PICN            =   "frmFatu.frx":2853
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblIndent 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   6960
      TabIndex        =   9
      Top             =   915
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblLote 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   240
      TabIndex        =   21
      Top             =   5640
      Width           =   75
   End
   Begin VB.Label Label3 
      Caption         =   "Vl. Cont�bil:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5760
      TabIndex        =   20
      Top             =   1440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Dt.Emiss�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3240
      TabIndex        =   19
      Top             =   1440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblNota 
      Caption         =   "Nota Fiscal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   1440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblDep 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sito:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Index           =   16
      Left            =   240
      TabIndex        =   17
      Top             =   960
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.Label lblDeposito 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "CAMPINAS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   300
      Index           =   17
      Left            =   1440
      TabIndex        =   6
      Top             =   915
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Nr. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3240
      TabIndex        =   16
      Top             =   960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblNr_Pedido 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   4440
      TabIndex        =   7
      Top             =   915
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblSeq_Pedido 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   5400
      TabIndex        =   8
      Top             =   915
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Label Label6 
      Caption         =   "TP Indent:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5880
      TabIndex        =   15
      Top             =   960
      Visible         =   0   'False
      Width           =   1095
   End
End
Attribute VB_Name = "frmFatu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub cmd1_Click()

End Sub

Private Sub cmdAtu_Click()

  If txtNF = "" Then
    MsgBox "Entre com o nr. da NF", vbInformation, "Aten��o"
    txtNF.SetFocus
    Exit Sub
  ElseIf txtDt = "" Then
    MsgBox "Entre com a data da NF", vbInformation, "Aten��o"
    txtDt.SetFocus
    Exit Sub
  ElseIf Not IsDate(txtDt) Then
    MsgBox "Data da NF invalida", vbInformation, "Aten��o"
    txtDt.SetFocus
    Exit Sub
  ElseIf txtContabil = "" Then
    MsgBox "Entre com o valor cont�bil da NF", vbInformation, "Aten��o"
    txtContabil.SetFocus
    Exit Sub
  End If
  
'Comentadas as linhas baixo - Luiza 01/09/2003 a pedido da Michele
'tirado o comentario em 02/10, nao vai haver lancamento do dia anterior
'comentado em 31/10
  
  If CDate(Format(txtDt, "dd/mm/yyyy")) < CDate(Format(Data_Faturamento, "dd/mm/yyyy")) Then
    If CDate(Format(txtDt, "dd/mm/yyyy")) <> CDate(Format(data_ini_fech, "dd/mm/yyyy")) Then
      MsgBox "A data de emiss�o da NF deve ser maior ou igual a data de faturamento", vbExclamation, "Aten��o"
      Exit Sub
    End If
  End If

'   SQL = "BEGIN"
'   SQL = SQL & " Update indent.pedido_indent set"
'   SQL = SQL & " fl_ger_nfis = 'S',"
'   SQL = SQL & " num_nota = :nota,"
'   SQL = SQL & " dt_emissao_nota = to_date(:dt,'dd/mm/rr'),"
'   SQL = SQL & " vl_contabil = :vl"
'   SQL = SQL & " Where seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " COMMIT;"
'   SQL = SQL & "EXCEPTION"
'   SQL = SQL & " WHEN OTHERS THEN"
'   SQL = SQL & " ROLLBACK;"
'   SQL = SQL & " :cod_errora := SQLCODE;"
'   SQL = SQL & " :txt_errora := SQLERRM;"
'   SQL = SQL & "END;"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'   oradatabase.Parameters.Remove "nota"
'   oradatabase.Parameters.Add "nota", txtNF, 1
'   oradatabase.Parameters.Remove "dt"
'   oradatabase.Parameters.Add "dt", txtDt, 1
'   oradatabase.Parameters.Remove "vl"
'   oradatabase.Parameters.Add "vl", txtContabil, 1
'
'   oradatabase.Parameters.Remove "cod_errora"
'   oradatabase.Parameters.Add "cod_errora", 0, 2
'   oradatabase.Parameters.Remove "txt_errora"
'   oradatabase.Parameters.Add "txt_errora", "", 2
'   oradatabase.ExecuteSQL SQL
'
'   If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'      MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'        " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'   Else
'         SQL = "delete from PEDIDO "
'         SQL = SQL & " where NUM_PEDIDO = " & lngNum_Pedido
'         SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
'         SQL = SQL & " and COD_LOJA = " & lngCod_Loja
'         dbAccess.Execute SQL, dbFailOnError
'         FreeLocks
'         MsgBox "PEDIDO ATUALIZADO COM SUCESSO", vbInformation, "ATEN��O"
'   End If

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_NOTA"
        vBanco.Parameters.Add "PM_NUM_NOTA", CLng(txtNF.Text), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_DT_EMISSAO_NOTA"
        vBanco.Parameters.Add "PM_DT_EMISSAO_NOTA", txtDt.Text, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_VL_CONTABIL"
        vBanco.Parameters.Add "PM_VL_CONTABIL", Replace(txtContabil.Text, ",", "."), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_ATUALIZA_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_NUM_NOTA,:PM_DT_EMISSAO_NOTA,:PM_VL_CONTABIL,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = vbDefault
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      End If
      
         FreeLocks
         MsgBox "PEDIDO ATUALIZADO COM SUCESSO", vbInformation, "ATEN��O"
         cmdVoltar_Click

End Sub


Private Sub cmdCancela_Click()
   frmCancela.Show 1
   If cod_cancel = 0 Then
     MsgBox "ESCOLHA UM MOTIVO PARA CANCELAR O PEDIDO ", vbExclamation, "ATEN��O"
     Exit Sub
   End If

'   SQL = "BEGIN"
'   SQL = SQL & " Update indent.pedido_indent set"
'   SQL = SQL & " situacao = 9,"
'   SQL = SQL & " cod_cancel = :cod"
'   SQL = SQL & " Where seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " Update indent.item_pedido_indent set"
'   SQL = SQL & " situacao = 9"
'   SQL = SQL & " Where seq_pedido = :seq and"
'   SQL = SQL & " num_pedido = :num and"
'   SQL = SQL & " cod_loja = :loja;"
'   SQL = SQL & " COMMIT;"
'   SQL = SQL & "EXCEPTION"
'   SQL = SQL & " WHEN OTHERS THEN"
'   SQL = SQL & " ROLLBACK;"
'   SQL = SQL & " :cod_errora := SQLCODE;"
'   SQL = SQL & " :txt_errora := SQLERRM;"
'   SQL = SQL & "END;"
'
'   oradatabase.Parameters.Remove "loja"
'   oradatabase.Parameters.Add "loja", lngCod_Loja, 1
'   oradatabase.Parameters.Remove "num"
'   oradatabase.Parameters.Add "num", lngNum_Pedido, 1
'   oradatabase.Parameters.Remove "seq"
'   oradatabase.Parameters.Add "seq", lngSEQ_PEDIDO, 1
'   oradatabase.Parameters.Remove "cod"
'   oradatabase.Parameters.Add "cod", cod_cancel, 1
'
'
'   oradatabase.Parameters.Remove "cod_errora"
'   oradatabase.Parameters.Add "cod_errora", 0, 2
'   oradatabase.Parameters.Remove "txt_errora"
'   oradatabase.Parameters.Add "txt_errora", "", 2
'   oradatabase.ExecuteSQL SQL
'
'   If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'      MsgBox "OCORREU O ERRO: " & oradatabase.Parameters("txt_errora") & _
'        " . LIGUE PARA DEPTO DE SISTEMAS", vbCritical, "ATEN��O"
'   Else
'         SQL = "delete from PEDIDO "
'         SQL = SQL & " where NUM_PEDIDO = " & lngNum_Pedido
'         SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
'         SQL = SQL & " and COD_LOJA = " & lngCod_Loja
'         dbAccess.Execute SQL, dbFailOnError
'         FreeLocks
'         MsgBox "PEDIDO CANCELADO COM SUCESSO", vbInformation, "ATEN��O"
'   End If

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_CANCEL"
        vBanco.Parameters.Add "PM_COD_CANCEL", cod_cancel, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_CANCELA_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_CANCEL,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = vbDefault
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      End If
      
         FreeLocks
         MsgBox "PEDIDO CANCELADO COM SUCESSO", vbInformation, "ATEN��O"
         Unload Me

End Sub


Private Sub cmdGera_Pedido_Click()
 Dim response As String


 response = MsgBox("Tem certeza que quer gerar um novo pedido com os itens faltantes ?", _
 vbYesNo, "GERA PEDIDO")
 If response = vbNo Then
   Exit Sub
 End If
 

vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
vBanco.Parameters.Remove "PM_NUM_PEDIDO"
vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
vBanco.Parameters.Remove "PM_COD_LOJA"
vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
vBanco.Parameters.Remove "PM_COD_USUARIO"
vBanco.Parameters.Add "PM_COD_USUARIO", lngCod_Usuario, ORAPARM_INPUT
vBanco.Parameters.Remove "PM_CODERRO"
vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
vBanco.Parameters.Remove "PM_TXTERRO"
vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
 
 vSql = "PRODUCAO.Pck_VDA790.PR_GERA_NOVO_PEDIDO(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_COD_USUARIO,:PM_CODERRO,:PM_TXTERRO)"
 vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

  If vErro = "" Then
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        If vBanco.Parameters("PM_CODERRO").Value = -1 Then
          MsgBox "J� foi gerado um novo pedido, verifique na tela de manuten��o.", vbInformation, "Aten��o"
        Else
          MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
          Screen.MousePointer = vbDefault
          Exit Sub
        End If
    Else
        MsgBox "Novo pedido gerado com sucesso, verifique na tela manunte��o!", vbInformation, "Aten��o"
        Unload Me

    End If
 End If

End Sub

Private Sub cmdImprimi_Click()
  'Call Imprimi_Pedido(lblDeposito(17), lblNr_Pedido, lblSeq_Pedido, Format(lblIndent, "00")) 'TI-5028
  Call Imprimi_PedidoNew(lblDeposito(17), lblNr_Pedido, lblSeq_Pedido, Format(lblIndent, "00"))
End Sub

Private Sub cmdLote_Click()
  Dim dn As Snapshot
  Dim response As String
  
'  Sql = "Select FL_ENVIADO"
'  Sql = Sql & " From lote"
'  Sql = Sql & " Where seq_pedido = " & lblSeq_Pedido
'  Sql = Sql & " and num_pedido = " & lblNr_Pedido
'  Sql = Sql & " and cod_loja = " & Mid(Trim(lblDeposito(17)), 1, 2)
'
'  Set dn = dbAccess.CreateSnapshot(Sql)
  
  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE", "PM_seq_pedido", CLng(lblSeq_Pedido.Caption), "PM_num_pedido", CLng(lblNr_Pedido.Caption), "PM_cod_loja", CLng(Mid(Trim(lblDeposito(17)), 1, 2)))
  
  If vObjOracle.EOF And vObjOracle.BOF Then

'    Sql = "Insert into lote (cod_loja,num_pedido,seq_pedido,"
'    Sql = Sql & " fl_enviado, cod_plano,tipo_ordem) values("
'    Sql = Sql & Mid(Trim(lblDeposito(17)), 1, 2) & ","
'    Sql = Sql & lblNr_Pedido & ","
'    Sql = Sql & lblSeq_Pedido & ",'N',0,null)"
'
'    dbAccess.Execute Sql, dbFailOnError

        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", CLng(lblSeq_Pedido), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(lblNr_Pedido.Caption), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", CLng(Mid(Trim(lblDeposito(17)), 1, 2)), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_INSERT_LOTE(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If
      
    FreeLocks
    MsgBox "Inclus�o OK", vbInformation, "Aten��o"
  Else
    If vObjOracle!fl_enviado = "S" Then
      response = MsgBox("Este pedido j� foi enviado via Lote, deseja envia-lo novamente ?", _
      vbYesNo, "ENVIO DE LOTE")
      If response = vbYes Then    ' User chose Yes.
'        Sql = "Update lote set"
'        Sql = Sql & " fl_enviado = 'N'"
'        Sql = Sql & " Where seq_pedido = " & lblSeq_Pedido
'        Sql = Sql & " and num_pedido = " & lblNr_Pedido
'        Sql = Sql & " and cod_loja = " & Mid(Trim(lblDeposito(17)), 1, 2)
'
'        dbAccess.Execute Sql, dbFailOnError

        vBanco.Parameters.Remove "PM_FL_ENVIADO"
        vBanco.Parameters.Add "PM_FL_ENVIADO", "N", ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
        vBanco.Parameters.Add "PM_SEQ_PEDIDO", CLng(lblSeq_Pedido), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(lblNr_Pedido), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", CLng(Mid(Trim(lblDeposito(17)), 1, 2)), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
        vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_LOTE(:PM_FL_ENVIADO,:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = vbDefault
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      End If

        FreeLocks
        
        MsgBox "Inclus�o OK", vbInformation, "Aten��o"
'      Else
'        MsgBox "Inclus�o OK", vbInformation, "Aten��o"
'        Exit Sub
      End If
    End If
  End If
  vObjOracle.Close
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub cmdVoltar_Click()
    Form_Load
End Sub

Private Sub Form_Load()
'  Dim ss As Object
   Dim i As Long
   
   txtNF = ""
   txtDt = ""
   txtContabil = ""
  
'  SQL = "Select a.cod_loja,a.num_pedido,"
'  SQL = SQL & " a.seq_pedido, TO_CHAR(a.tp_indent,'00')||'-'||"
'  SQL = SQL & "b.desc_tipo tipo,a.cod_cliente,"
'  SQL = SQL & " c.nome_cliente, a.vl_contabil, A.DT_DIGITACAO"
'  SQL = SQL & " From indent.pedido_indent a,"
'  SQL = SQL & " indent.tipo_indent b,"
'  SQL = SQL & " cliente c"
'  SQL = SQL & " Where"
'  SQL = SQL & " a.cod_loja = " & Mid(Trim(Deposito_default), 1, 2)
'  SQL = SQL & " and a.situacao=0 and"
'  SQL = SQL & " fl_ger_nfis='N' and"
'  SQL = SQL & " a.cod_cliente = c.cod_cliente and"
'  SQL = SQL & " a.tp_indent=b.cod_tipo"
'  SQL = SQL & " Order by C.NOME_CLIENTE"
'
''  SQL = SQL & " Order by a.tp_indent"
'
'  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO_INDENT", "PM_COD_LOJA", CLng(Mid(Trim(Deposito_default), 1, 2)))
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "N�O H� NFS SEM EMISS�O", vbExclamation, "ATEN��O"
    Exit Sub
  Else
    grdFatu.Visible = True
    lblDep(16).Visible = False
    lblDeposito(17).Visible = False
    Label2.Visible = False
    lblNr_Pedido.Visible = False
    lblSeq_Pedido.Visible = False
    Label6.Visible = False
    lblIndent.Visible = False
    lblNota.Visible = False
    txtNF.Visible = False
    Label1.Visible = False
    txtDt.Visible = False
    Label3.Visible = False
    txtContabil.Visible = False
    cmdVoltar.Visible = False
    grdItem.Visible = False
    
    cmdAtu.Enabled = False
    cmdCancela.Enabled = False
    cmdImprimi.Enabled = False
    cmdVoltar.Enabled = False
    
    lblLote.Visible = False
    Timer1.Enabled = False
    With frmFatu.grdFatu
        .Cols = 8
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 1200
        .ColWidth(2) = 500
        .ColWidth(3) = 1400
        .ColWidth(4) = 500
        .ColWidth(5) = 3200
        .ColWidth(6) = 1800
        .ColWidth(7) = 1800
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "Num Pedido"
        .Col = 2
        .Text = "Seq."
        .Col = 3
        .Text = "Dt Digitacao"
        .Col = 4
        .Text = "Cod."
        .Col = 5
        .Text = "Cliente"
        .Col = 6
        .Text = "Vl.Cont�bil"
        .Col = 7
        .Text = "Tipo"
        
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!cod_loja
            .Col = 1
            .Text = vObjOracle!num_pedido
            .Col = 2
            .Text = vObjOracle!SEQ_PEDIDO
            .Col = 3
            .Text = vObjOracle!dt_digitacao
            .Col = 4
            .Text = vObjOracle!COD_CLIENTE
            .Col = 5
            .Text = vObjOracle!NOME_CLIENTE
            .Col = 6
            .ColAlignment(6) = 1
            '.Text = Format(vObjOracle!vl_contabil, "0.00")
            .Text = IIf(IsNull(vObjOracle!vl_contabil), 0, vObjOracle!vl_contabil)
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Trim(vObjOracle!tipo)
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
    Screen.MousePointer = 0
  End If
  
End Sub


Private Sub grdFatu_DblClick()
  grdFatu.Col = 0
  lngCod_Loja = grdFatu.Text
  grdFatu.Col = 1
  lngNum_Pedido = grdFatu.Text
  grdFatu.Col = 2
  lngSEQ_PEDIDO = grdFatu.Text
  grdFatu.Col = 7
  strTipo = grdFatu.Text
  grdFatu.Col = 6
  frmFatu.txtContabil = grdFatu.Text
    cmdGera_Pedido.Enabled = True
  
  Call Monta_Tela(lngCod_Loja, lngNum_Pedido, lngSEQ_PEDIDO, strTipo)
  
End Sub


Public Sub Monta_Tela(loja As Long, pedido As Long, seq As Long, tipo As String)
  Dim ss As Object
  Dim i As Long
  'Dim dn As Snapshot
    
  grdFatu.Visible = False
  lblDep(16).Visible = True
  lblDeposito(17).Visible = True
  Label2.Visible = True
  lblNr_Pedido.Visible = True
  lblSeq_Pedido.Visible = True
  Label6.Visible = True
  lblIndent.Visible = True
  lblNota.Visible = True
  txtNF.Visible = True
  Label1.Visible = True
  txtDt.Visible = True
  Label3.Visible = True
  txtContabil.Visible = True
  lblDeposito(17) = lngCod_Loja 'Deposito_default
  lblNr_Pedido = pedido
  lblSeq_Pedido = seq
  lblIndent = tipo
  cmdVoltar.Visible = True
   
  cmdAtu.Enabled = True
  cmdCancela.Enabled = True
  cmdImprimi.Enabled = True
  cmdVoltar.Enabled = True
  
  If Mid(Format(lblIndent, "00"), 1, 2) = "01" Then
    cmdLote.Enabled = True
'    SQL = "Select fl_enviado"
'    SQL = SQL & " From lote"
'    SQL = SQL & " Where seq_pedido = " & lblSeq_Pedido
'    SQL = SQL & " and num_pedido = " & lblNr_Pedido
'    SQL = SQL & " and cod_loja = " & Mid(Trim(Deposito_default), 1, 2)
'
'    Set dn = dbAccess.CreateSnapshot(SQL)

    Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOTE", "PM_seq_pedido", CLng(lblSeq_Pedido.Caption), "PM_num_pedido", CLng(lblNr_Pedido.Caption), "PM_cod_loja", CLng(Mid(Trim(Deposito_default), 1, 2)))

    FreeLocks
    If vObjOracle.EOF And vObjOracle.BOF Then
      Timer1.Enabled = False
      lblLote.Visible = False
    Else
      If vObjOracle!fl_enviado = "S" Then
        lblLote = "J� ENVIADO VIA LOTE"
        Timer1.Enabled = True
        lblLote.Visible = True
      Else
        Timer1.Enabled = False
        lblLote.Visible = False
      End If
    End If
  Else
    cmdLote.Enabled = False
  End If
  
  grdItem.Visible = True

'  SQL = "Select num_item_pedido,cod_dpk,"
'  SQL = SQL & " cod_fornecedor,cod_fabrica,"
'  SQL = SQL & " desc_item,qtd_solicitada,"
'  SQL = SQL & " qtd_atendida,preco_unitario_liq,situacao"
'  SQL = SQL & " From indent.item_pedido_indent"
'  SQL = SQL & " Where seq_pedido = :seq and"
'  SQL = SQL & " num_pedido = :num and"
'  SQL = SQL & " cod_loja = :loja"
'
'  oradatabase.Parameters.Remove "seq"
'  oradatabase.Parameters.Add "seq", seq, 1
'  oradatabase.Parameters.Remove "num"
'  oradatabase.Parameters.Add "num", pedido, 1
'  oradatabase.Parameters.Remove "loja"
'  oradatabase.Parameters.Add "loja", loja, 1
'
'  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_ITENS_PEDIDO_INDENT", "PM_seq_pedido", seq, "PM_num_pedido", pedido, "PM_cod_loja", loja)
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "PEDIDO SEM ITEM, VERIFIQUE", vbCritical, "ATEN��O"
    Exit Sub
  Else
    grdItem.Visible = True
    With frmFatu.grdItem
        .Cols = 9
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 500
        'eoliveira - CiT
        '25/07/2012 - ISA68 - alterar c�digo DPK
        .ColWidth(1) = 800
        .ColWidth(2) = 700
        .ColWidth(3) = 1800
        .ColWidth(4) = 2800
        .ColWidth(5) = 800
        .ColWidth(6) = 800
        .ColWidth(7) = 1000
        .ColWidth(8) = 700
        
        
        .Row = 0
        .Col = 0
        .Text = "Nr.Item"
        .Col = 1
        .Text = "DPK"
        .Col = 2
        .Text = "Forn."
        .Col = 3
        .Text = "Fabrica"
        .Col = 4
        .Text = "Descr."
        .Col = 5
        .Text = "Qtd.Sol."
        .Col = 6
        .Text = "Qtd.Ate."
        .Col = 7
        .Text = "Pr.Liq."
        .Col = 8
        .Text = "Sit."
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!NUM_ITEM_PEDIDO
            .Col = 1
            .Text = vObjOracle!cod_dpk
            .Col = 2
            .Text = vObjOracle!cod_fornecedor
            .Col = 3
            .Text = vObjOracle!cod_fabrica
            .Col = 4
            .Text = vObjOracle!DESC_ITEM
            .Col = 5
            .ColAlignment(5) = 1
            .Text = vObjOracle!qtd_solicitada
            .Col = 6
            .ColAlignment(6) = 1
            .Text = vObjOracle!qtd_atendida
            .Col = 7
            .ColAlignment(7) = 1
            '.Text = Format(vObjOracle!preco_unitario_liq, "0.00")
            .Text = vObjOracle!PRECO_UNITARIO_LIQ
            .Col = 8
            .Text = vObjOracle!situacao
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
  End If
End Sub

Private Sub grdItem_DblClick()
  grdItem.Col = 0
  frmQtd.lblItem = grdItem.Text
  grdItem.Col = 6
  frmQtd.txtQtd_Atendida = grdItem.Text
  grdItem.Col = 7
  frmQtd.txtPreco = grdItem.Text
  frmQtd.Show 1

  
  
  Call Monta_Tela(lngCod_Loja, lngNum_Pedido, lngSEQ_PEDIDO, strTipo)
End Sub

Private Sub Timer1_Timer()
    lblLote.Visible = Not lblLote.Visible
End Sub

Private Sub txtContabil_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtContabil)
End Sub


Private Sub txtContabil_LostFocus()
   'txtContabil = Format(FmtBR(txtContabil), "0.00")
   txtContabil = Replace(txtContabil, ",", ".")
End Sub


Private Sub txtDt_KeyPress(KeyAscii As Integer)
  Call Data(KeyAscii, txtDt)
End Sub


Private Sub txtNF_KeyPress(KeyAscii As Integer)
   KeyAscii = Numerico(KeyAscii)
End Sub

'-----------------------------------------------------
'TI-5028
'Imprimi_Pedido substitu�da por Imprimi_PedidoNEW
'-----------------------------------------------------
'Public Sub Imprimi_Pedido(loja As String, pedido As Long, seq As Long, tipo As String)
'  Dim i As Long
'  Dim vObjOracle1 As Object
'  Dim iResult As Integer
'  Dim p As Object
'  Dim SQL As String
'
'    'Conexao Access
'    Set dbAccess = OpenDatabase(strPath & "INDENT.MDB")
'
'   SQL = "Delete * from cabec_aux "
'   dbAccess.Execute SQL, dbFailOnError
'   FreeLocks
'
'   SQL = "Delete * from item_aux "
'   dbAccess.Execute SQL, dbFailOnError
'   FreeLocks
'
'
''   SQL = "Select a.num_pedido,seq_pedido,"
''   SQL = SQL & " b.desc_tipo tipo,"
''   SQL = SQL & " c.nome_cliente,c.endereco,c.bairro,"
''   SQL = SQL & " d.nome_cidade,d.cod_uf,c.cep,c.cgc,"
''   SQL = SQL & " c.inscr_estadual,nvl(c.ddd1,0)||'-'||nvl(c.fone1,0) fone,"
''   SQL = SQL & " nvl(c.fax,0) fax, a.desc_plano, a.dt_digitacao,"
''   SQL = SQL & " a.cod_repres||'-'||e.pseudonimo repres,"
''   SQL = SQL & " a.cod_vend||'-'||f.pseudonimo vend, a.mensagem,a.vl_despesa"
''   SQL = SQL & " From indent.pedido_indent a, indent.tipo_indent b,"
''   SQL = SQL & " cliente c, cidade d, representante e,"
''   SQL = SQL & " representante f"
''   SQL = SQL & " Where tp_indent = :tipo and"
''   SQL = SQL & " a.seq_pedido = :seq and"
''   SQL = SQL & " a.num_pedido = :num and"
''   SQL = SQL & " a.cod_loja = :loja and"
''   SQL = SQL & " a.cod_vend = f.cod_repres(+) and"
''   SQL = SQL & " a.cod_repres = e.cod_repres(+) and"
''   SQL = SQL & " c.cod_cidade = d.cod_cidade and"
''   SQL = SQL & " a.cod_cliente = c.cod_cliente and"
''   SQL = SQL & " a.tp_indent = b.cod_tipo"
''
''   oradatabase.Parameters.Remove "tipo"
''   oradatabase.Parameters.Remove "seq"
''   oradatabase.Parameters.Remove "num"
''   oradatabase.Parameters.Remove "loja"
''
''   oradatabase.Parameters.Add "tipo", Mid(Trim(tipo), 1, 2), 1
''   oradatabase.Parameters.Add "seq", seq, 1
''   oradatabase.Parameters.Add "num", pedido, 1
''   oradatabase.Parameters.Add "loja", Mid(Trim(Deposito_default), 1, 2), 1
''
''   Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
'
'     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
'     vBanco.Parameters.Add "PM_SEQ_PEDIDO", seq, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
'     vBanco.Parameters.Add "PM_NUM_PEDIDO", pedido, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_COD_LOJA"
'     vBanco.Parameters.Add "PM_COD_LOJA", loja, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_TP_INDENT"
'     vBanco.Parameters.Add "PM_TP_INDENT", Mid(Trim(tipo), 1, 2), ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_CURSOR1"
'     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
'     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
'     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
'     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
'     vBanco.Parameters.Remove "PM_CODERRO"
'     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
'     vBanco.Parameters.Remove "PM_TXTERRO"
'     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
'
'     vSql = "PRODUCAO.Pck_VDA790.PR_SEL_IMP_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_TP_INDENT,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
'     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
'
'    If vErro <> "" Then
'      Screen.MousePointer = vbDefault
'       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
'       Exit Sub
'    Else
'       Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
'    End If
'
'   SQL = "Insert into cabec_aux values("
'   SQL = SQL & vObjOracle!num_pedido & "," & vObjOracle!SEQ_PEDIDO & ",'"
'   SQL = SQL & Deposito_default & "','" & vObjOracle!tipo & "','"
'   SQL = SQL & vObjOracle!NOME_CLIENTE & "','" & vObjOracle!ENDERECO & "','"
'   SQL = SQL & vObjOracle!bairro & "','" & vObjOracle!Nome_Cidade & "','"
'   SQL = SQL & vObjOracle!cod_uf & "','" & vObjOracle!cep & "','"
'   SQL = SQL & vObjOracle!cgc & "','" & vObjOracle!INSCR_ESTADUAL & "','"
'   SQL = SQL & vObjOracle!FONE & "','" & vObjOracle!fax & "','"
'   SQL = SQL & vObjOracle!DESC_PLANO & "','" & vObjOracle!dt_digitacao & "','"
'   SQL = SQL & vObjOracle!REPRES & "','" & vObjOracle!VEND & "','" & vObjOracle!MENSAGEM & "',"
'   SQL = SQL & vObjOracle!vl_despesa & ")"
'
'   dbAccess.Execute SQL, dbFailOnError
'   FreeLocks
'
'
''   SQL = "Select num_item_pedido,"
''   SQL = SQL & " cod_dpk,cod_fornecedor,cod_fabrica,"
''   SQL = SQL & " desc_item, qtd_solicitada, preco_unitario_liq"
''   SQL = SQL & " from indent.item_pedido_indent a"
''   SQL = SQL & " Where tp_indent = :tipo and"
''   SQL = SQL & " a.seq_pedido = :seq and"
''   SQL = SQL & " a.num_pedido = :num and"
''   SQL = SQL & " a.cod_loja = :loja and"
''   SQL = SQL & " a.situacao=0 "
''   SQL = SQL & " Order by num_item_pedido"
''
''   oradatabase.Parameters.Remove "tipo"
''   oradatabase.Parameters.Remove "seq"
''   oradatabase.Parameters.Remove "num"
''   oradatabase.Parameters.Remove "loja"
''
''   oradatabase.Parameters.Add "tipo", Mid(Trim(tipo), 1, 2), 1
''   oradatabase.Parameters.Add "seq", seq, 1
''   oradatabase.Parameters.Add "num", pedido, 1
''   oradatabase.Parameters.Add "loja", Mid(Trim(Deposito_default), 1, 2), 1
''
''   Set ss1 = oradatabase.dbcreatedynaset(SQL, 0&)
'
'     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
'     vBanco.Parameters.Add "PM_SEQ_PEDIDO", seq, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
'     vBanco.Parameters.Add "PM_NUM_PEDIDO", pedido, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_COD_LOJA"
'     vBanco.Parameters.Add "PM_COD_LOJA", loja, ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_TP_INDENT"
'     vBanco.Parameters.Add "PM_TP_INDENT", Mid(Trim(tipo), 1, 2), ORAPARM_INPUT
'     vBanco.Parameters.Remove "PM_CURSOR1"
'     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
'     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
'     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
'     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
'     vBanco.Parameters.Remove "PM_CODERRO"
'     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
'     vBanco.Parameters.Remove "PM_TXTERRO"
'     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
'
'     vSql = "PRODUCAO.Pck_VDA790.PR_SEL_IMP_ITENS_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_TP_INDENT,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
'     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
'
'    If vErro <> "" Then
'      Screen.MousePointer = vbDefault
'       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
'       Exit Sub
'    Else
'       Set vObjOracle1 = vBanco.Parameters("PM_CURSOR1").Value
'    End If
'
'   For i = 1 To vObjOracle1.RecordCount
'     SQL = "Insert into item_aux values("
'     SQL = SQL & vObjOracle!num_pedido & "," & vObjOracle!SEQ_PEDIDO & ",'"
'     SQL = SQL & Deposito_default & "','" & vObjOracle!tipo & "',"
'     SQL = SQL & vObjOracle1!NUM_ITEM_PEDIDO & "," & vObjOracle1!cod_dpk & ","
'     SQL = SQL & vObjOracle1!cod_fornecedor & ",'" & vObjOracle1!cod_fabrica
'     SQL = SQL & "','" & vObjOracle1!DESC_ITEM & "',"
'     SQL = SQL & vObjOracle1!qtd_solicitada & "," & vObjOracle1!PRECO_UNITARIO_LIQ & ")"
'
'     dbAccess.Execute SQL, dbFailOnError
'     FreeLocks
'     vObjOracle1.MoveNext
'   Next
'
'  dbAccess.Close
'  Set vObjOracle1 = Nothing
'
'   With frmFatu.rptPedido
'     .DataFiles(0) = strPath & "INDENT.MDB"
'     .ReportFileName = strPath & "INDENT.RPT"
'     .Destination = 1
'     iResult = .PrintReport
'   End With
'
'
'  If iResult > 0 Then
'     MsgBox frmFatu.rptPedido.LastErrorString
'  Else
'    MsgBox "Impress�o finalizada com sucesso.", vbInformation, "Aten��o"
'  End If
'
'End Sub
'-----------------------------------------------------

'TI-5028
Public Sub Imprimi_PedidoNew(loja As String, pedido As Long, seq As Long, tipo As String)
  
On Error GoTo HandleError
  Dim i As Long
  'variaveis TI-5028
  Dim lngTotal_Registro As Long
  Dim intNum_Folha As Integer
  Dim num_linha As Long
  Dim num_linha_pag As Long
  Dim num_pagina As Long
  Dim intItensPorPag As Integer
  Dim dblVlTotal As Double
  Dim ss As Object
  Dim ss2 As Object
    num_pagina = 0
    num_linha = 1
    
    vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
    vBanco.Parameters.Add "PM_SEQ_PEDIDO", seq, 1
    vBanco.Parameters.Remove "PM_NUM_PEDIDO"
    vBanco.Parameters.Add "PM_NUM_PEDIDO", pedido, 1
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", loja, 1
    vBanco.Parameters.Remove "PM_TP_INDENT"
    vBanco.Parameters.Add "PM_TP_INDENT", Mid(Trim(tipo), 1, 2), 1
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
    vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
    vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
        
    vSql = "PRODUCAO.Pck_VDA790.PR_SEL_IMP_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_TP_INDENT,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
     
    If vErro <> "" Then
      Screen.MousePointer = vbDefault
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
       Exit Sub
    Else
       Set ss = vBanco.Parameters("PM_CURSOR1").Value
    End If
   
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "Pedido: " & pedido & " n�o localizado.", vbInformation, "Aten��o"
        Exit Sub
    End If
   
    vSql = "PRODUCAO.Pck_VDA790.PR_SEL_IMP_ITENS_PEDIDO_INDENT(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_TP_INDENT,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
      Screen.MousePointer = vbDefault
       MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
       Exit Sub
    Else
       Set ss2 = vBanco.Parameters("PM_CURSOR1").Value
    End If
   
    If ss2.EOF And ss2.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "Itens do pedido " & pedido & " n�o localizados.", vbInformation, "Aten��o"
        Exit Sub
    End If
   
    'Total de paginas � ser impressa
    If ss2.RecordCount <= 26 Then
        intNum_Folha = 1
    Else
        intNum_Folha = CInt(ss2.RecordCount / 26) 'Imprimi 26 itens por pagina
    End If
   
   
Cabecalho:
    intItensPorPag = 0
    'cria impress�o
    PrintImagem frmFatu.pctDPK.Picture
    Printer.Font = "VERDANA"
    Printer.FontSize = 9
    Printer.FontBold = True
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print Tab(5); "COMERCIAL AUTOMOTIVA S.A.";
    
    Printer.Print
    Printer.Print
    Printer.FontSize = 12
    Printer.Print Tab(40); "PST"; Tab(70);
    
    Printer.FontSize = 9
    Printer.Print
    Printer.Print
    Printer.Print Tab(40); "PEDIDO INDENT: " & ss!num_pedido & "-" & ss!SEQ_PEDIDO; Tab(90); "emiss�o: " & Date;
    Printer.FontBold = False
    Printer.Print
    Printer.FontSize = 8
    
    
    Printer.Print
    Printer.Print
    Printer.Print Tab(5); "RAZ�O SOCIAL: "; Tab(30); IIf(IsNull(ss!NOME_CLIENTE), "", ss!NOME_CLIENTE);
    Printer.Print Tab(5); "ENDERE�O: "; Tab(30); IIf(IsNull(ss!ENDERECO), "", ss!ENDERECO) & " - " & IIf(IsNull(ss!bairro), "", ss!bairro);
    Printer.Print Tab(5); "CIDADE: "; Tab(30); IIf(IsNull(ss!Nome_Cidade), "", ss!Nome_Cidade) & "/" & IIf(IsNull(ss!cod_uf), "", ss!cod_uf) & "     CEP: " & IIf(IsNull(ss!cep), "", ss!cep);
    Printer.Print Tab(5); "CNPJ: "; Tab(30); IIf(IsNull(ss!cgc), "", ss!cgc); Tab(50); "INSC.ESTADUAL: "; Tab(70); IIf(IsNull(ss!INSCR_ESTADUAL), "", ss!INSCR_ESTADUAL)
    Printer.Print Tab(5); "FONE: "; Tab(30); IIf(IsNull(ss!FONE), "", ss!FONE);
    Printer.Print Tab(5); "PLANO PAGTO: "; Tab(30); IIf(IsNull(ss!DESC_PLANO), "", ss!DESC_PLANO);
    Printer.Print Tab(5); "REPRESENTANTE: "; Tab(30); IIf(IsNull(ss!REPRES), "", ss!REPRES);
    Printer.Print Tab(5); "TLMKT: "; Tab(30); IIf(IsNull(ss!VEND), "", ss!VEND);
    Printer.Print Tab(5); "MENSAGEM: "; Tab(30); IIf(IsNull(ss!MENSAGEM), "", ss!MENSAGEM);
    Printer.Print
    Printer.FontBold = True
    Printer.Print " _______________________________________________________________________________________________________"
     
    Printer.Print Tab(5); "|" & "ITEM"; Tab(15); "|" & "QTDE"; Tab(25); "|" & "COD.PRODUTO"; Tab(50); "|" & "DESCRICAO"; Tab(110); "|" & "PRE�O LIQ."
    Printer.Print " _______________________________________________________________________________________________________"
    Printer.FontBold = False
           
   For i = num_linha To ss2.RecordCount
       Printer.Print
       Printer.Print Tab(10); ss2!NUM_ITEM_PEDIDO; Tab(20); ss2!qtd_solicitada; Tab(30); ss2!cod_fabrica; Tab(57); ss2!DESC_ITEM; Tab(125); Format(ss2!PRECO_UNITARIO_LIQ, "0.00")
       dblVlTotal = dblVlTotal + (ss2!PRECO_UNITARIO_LIQ * ss2!qtd_solicitada)
       num_linha = num_linha + 1
       intItensPorPag = intItensPorPag + 1
       ss2.MoveNext
   
       If intItensPorPag = 26 Then
           Exit For
       End If
   Next
   
    While intItensPorPag < 26
        Printer.Print ""
        Printer.Print ""
        intItensPorPag = intItensPorPag + 1
    Wend
    num_pagina = num_pagina + 1
    Printer.Print
    Printer.Print "       ___________________________________________________________________________________________________________"
    
    If num_pagina <> intNum_Folha Then
        Printer.FontBold = False
        Printer.FontBold = True
        Printer.Print Tab(107); "SubTot: " & Format(dblVlTotal, "0.00")
    Else
        Printer.FontBold = False
        Printer.Print Tab(115); "VL.DEPESAS: " & Format(ss!vl_despesa, "0.00")
        Printer.FontBold = True
        Printer.Print Tab(108); "TOTAL: " & Format(dblVlTotal, "0.00")
    End If
    
    Printer.FontBold = False
    Printer.Print "       ___________________________________________________________________________________________________________"
    Printer.FontBold = True
    Printer.Print Tab(115); "P�g:" & num_pagina & "/" & intNum_Folha
    Printer.FontBold = False

    If (ss2.RecordCount - num_linha) > 0 Then
        Printer.FontBold = False
        Printer.NewPage
        GoTo Cabecalho
    End If

    Printer.EndDoc

    Screen.MousePointer = vbDefault

HandleError:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 53 Or Err = 30009 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub Imprimi_Pedido " & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl, &H40000 + 0, &H40000 + 0
    End If

End Sub

'TI -5028
Public Sub PrintImagem(p As IPictureDisp, Optional ByVal x, Optional ByVal y, Optional ByVal resize)
    If IsMissing(x) Then x = Printer.CurrentX
    If IsMissing(y) Then y = Printer.CurrentY
    If IsMissing(resize) Then resize = 1
    Printer.PaintPicture p, x, y, p.Width * resize, p.Height * resize
End Sub


