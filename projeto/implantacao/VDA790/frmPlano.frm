VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPlano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PLANO DE PAGTO PST"
   ClientHeight    =   3840
   ClientLeft      =   2415
   ClientTop       =   2340
   ClientWidth     =   6540
   Icon            =   "frmPlano.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3840
   ScaleWidth      =   6540
   Begin VB.ComboBox CboTipo 
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   3330
      Width           =   2865
   End
   Begin MSGrid.Grid grdPlano 
      Height          =   2295
      Left            =   150
      TabIndex        =   0
      Top             =   540
      Visible         =   0   'False
      Width           =   6135
      _Version        =   65536
      _ExtentX        =   10821
      _ExtentY        =   4048
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
   Begin Bot�o.cmd SSCommand1 
      Default         =   -1  'True
      Height          =   495
      Left            =   3360
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   3120
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmPlano.frx":000C
      PICN            =   "frmPlano.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand2 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   4920
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   3120
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmPlano.frx":0382
      PICN            =   "frmPlano.frx":039E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   140
      TabIndex        =   6
      Top             =   2920
      Width           =   6090
      _ExtentX        =   10742
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label Label2 
      Caption         =   "Selecione um Tipo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   3030
      Width           =   1890
   End
   Begin VB.Label Label1 
      Caption         =   "Selecione um plano de pagto"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   150
      TabIndex        =   1
      Top             =   120
      Width           =   2775
   End
End
Attribute VB_Name = "frmPlano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim lngCod_Plano As Long

Private Sub cboTipo_Click()
  
strTipo_Ordem = Trim(CboTipo.Text)

End Sub


Private Sub cboTipo_GotFocus()
'ESCOLHE O TIPO ORB / ZORB
  
CboTipo.Clear
  
CboTipo.AddItem "ZORB - Suprimentos"
CboTipo.AddItem "ORB - Venda Normal"

frmPlano.Refresh

End Sub

Private Sub Form_Load()
  Dim dn As Snapshot
  Dim i As Long

'  SQL = "Select cod_plano,desc_plano,"
'  SQL = SQL & " tp_venda"
'  SQL = SQL & " From plano_pst"
'  SQL = SQL & " Order by tp_venda, desc_plano"
'
'  Set dn = dbAccess.CreateSnapshot(SQL)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_PLANO_PST")

  FreeLocks
  
  strTipo_Ordem = ""
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    MsgBox "N�o existe plano de pgto cadastrado, entre com contato com depto. Sistemas", vbExclamation, "ATEN��O"
    Exit Sub
  Else
    grdPlano.Visible = True
    vObjOracle.MoveLast
    vObjOracle.MoveFirst
    With frmPlano.grdPlano
        .Cols = 3
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 2200
        .ColWidth(2) = 3000
        
        .Row = 0
        .Col = 0
        .Text = "Cod."
        .Col = 1
        .Text = "Plano"
        .Col = 2
        .Text = "Tp.Ordem"
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!cod_plano
            .Col = 1
            .Text = vObjOracle!desc_plano
            .Col = 2
            .Text = IIf(vObjOracle!tp_venda = "S", "SUPRIMENTOS", "VENDA NORMAL")
            
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
  
  End If
End Sub

Private Sub Grid1_RowColChange()

End Sub



Private Sub grdPlano_Click()
  'Dim StrTipo_Ordem As String
  
  grdPlano.Col = 0
  lngCod_Plano = grdPlano.Text

End Sub

Private Sub SSCommand1_Click()
If Not IsNull(strTipo_Ordem) And Len(strTipo_Ordem) > 0 Then
  
'  strTipo_Ordem = IIf(grdPlano.Text = "SUPRIMENTOS", "ZORB", "ORB")
   strTipo_Ordem = Trim(Left(CboTipo.Text, 4))
   
'  SQL = "Update lote set"
'  SQL = SQL & " cod_plano = " & lngCod_Plano
'  SQL = SQL & " ,tipo_ordem = '" & strTipo_Ordem & "'"
'  SQL = SQL & " Where seq_pedido = " & lngSEQ_PEDIDO
'  SQL = SQL & " and num_pedido = " & lngNum_Pedido
'  SQL = SQL & " and cod_loja = " & lngCod_Loja
'
'  dbAccess.Execute SQL, dbFailOnError
  
       vBanco.Parameters.Remove "PM_COD_PLANO"
       vBanco.Parameters.Add "PM_COD_PLANO", lngCod_Plano, ORAPARM_INPUT
       vBanco.Parameters.Remove "PM_TIPO_ORDEM"
       vBanco.Parameters.Add "PM_TIPO_ORDEM", strTipo_Ordem, ORAPARM_INPUT
       vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
       vBanco.Parameters.Add "PM_SEQ_PEDIDO", lngSEQ_PEDIDO, ORAPARM_INPUT
       vBanco.Parameters.Remove "PM_NUM_PEDIDO"
       vBanco.Parameters.Add "PM_NUM_PEDIDO", lngNum_Pedido, ORAPARM_INPUT
       vBanco.Parameters.Remove "PM_COD_LOJA"
       vBanco.Parameters.Add "PM_COD_LOJA", lngCod_Loja, ORAPARM_INPUT
       vBanco.Parameters.Remove "PM_CODERRO"
       vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
       vBanco.Parameters.Remove "PM_TXTERRO"
       vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
        
    vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_LOTE_PLANO(:PM_COD_PLANO,:PM_TIPO_ORDEM,:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_LOJA,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    FreeLocks
    
      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = vbDefault
         Exit Sub
      Else
        If vBanco.Parameters("PM_CODERRO").Value < 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      End If
  
  Unload Me
  
Else

  MsgBox "Selecione o Tipo de Ordem"

End If


End Sub


Private Sub SSCommand2_Click()
    Unload Me
End Sub
