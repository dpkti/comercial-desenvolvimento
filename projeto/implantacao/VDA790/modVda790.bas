Attribute VB_Name = "modVda790"
Option Explicit

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public vSessao As Object                            'SESS�O DO ORACLE (OLE)
Public vBanco As Object                             'BANCO DO ORACLE (OLE)
Public vNomeBanco As String                         'NOME DO BANCO DO ARQUIVO .INI
Public vObjOracle As Object                         'OBJETO DO ORACLE (OLE)
Public vCd As Integer                               'C�DIGO DO CD CONECTADO
Public vTipoCD As String                            'TIPO DO CD CONECTADO (U, M)
Public vLinhaArquivo                                'LEITURA DE LINHA DE ARQUIVO
Public vResposta                                    'GUARDA A ESCOLHA DO USU�RIO DA FUN��O PERGUNTA
Public vUsuarioBanco As String                      'USU�RIO DO BANCO
Public vUsuarioRede As String                       'USU�RIO DA REDE
Public vNomeSistema As String                       'NOME DO SISTEMA
Public vDescricaoSistema As String                  'DESCRI��O DO SISTEMA
Public vAnalistaResponsavel As String               'ANALISTA RESPONS�VEL PELO SISTEMA
Public vAnalistaBackup As String                    'ANALISTA BACKUP
Public vVB_Generica_001 As New clsVB_Generica_001
Public vErro As String
Public vSelect As String
Public vSql As String
Public strPerfil As String * 1


Public strPath As String
Public strDiretorio As String
Public lngPedido_Automatico As Long

Public oradatabase As Object   'TI-5028
Public OraParameters As Object 'TI-5028
'Public dbAccess As Database
Public Data_Faturamento As Date
Public strTipo_Ordem As String
Public data_ini_fech As Date
Public Data_Real As String
Public Filial_Ped As String
Public UF_origem As String * 2
Public Deposito_default As String
Public lngNum_Pedido As Long
Public lngSEQ_PEDIDO As Long
Public bTipo_Indent As Byte
Public lngQtd_inf As Long
Public lngQtd_dig As Long
Public lngCod_Loja As Long
Public lngCod_Repres As Long
Global Const Branco = &HFFFFFF
Global Const Vermelho = &H8080FF
Global Const Cinza = &H8000000F
Public txtResposta As String
Public bTipo_Item As Byte
Public strFl_Lib As String * 1
Public strTipo As String
Public cod_cancel As Integer
Public lngCod_Usuario As Long
Public lngResposta As Long

Sub Carrega_Procedure(strProcedure As String, Optional strParamName1 As String, Optional strParamValue1 As Variant, Optional strParamName2 As String, Optional strParamValue2 As Variant, Optional strParamName3 As String, Optional strParamValue3 As Variant)

     Dim strParametros As String
     
     Screen.MousePointer = vbHourglass

     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
     
     strParametros = ""
     
     If strParamName1 <> "" Then
        vBanco.Parameters.Remove strParamName1
        vBanco.Parameters.Add strParamName1, strParamValue1, ORAPARM_INPUT
        strParametros = ":" & strParamName1 & ","
     End If
     
     If strParamName2 <> "" Then
        vBanco.Parameters.Remove strParamName2
        vBanco.Parameters.Add strParamName2, strParamValue2, ORAPARM_INPUT
        strParametros = strParametros & ":" & strParamName2 & ","
     End If
     
     If strParamName3 <> "" Then
        vBanco.Parameters.Remove strParamName3
        vBanco.Parameters.Add strParamName3, strParamValue3, ORAPARM_INPUT
        strParametros = strParametros & ":" & strParamName3 & ","
     End If
    
     vSql = strProcedure & "(" & strParametros & ":PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
        Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Exit Sub
      Else
         Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
         Screen.MousePointer = vbDefault
      End If
End Sub

Function Carrega_Procedure_Scalar(strProcedure As String, Optional strParamName1 As String, Optional strParamValue1 As Variant, Optional strParamName2 As String, Optional strParamValue2 As Variant, Optional strParamName3 As String, Optional strParamValue3 As Variant, Optional strParamName4 As String, Optional strParamValue4 As Variant) As Variant

     Dim strParametros As String
     
     vBanco.Parameters.Remove "PM_SCALAR"
     vBanco.Parameters.Add "PM_SCALAR", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
     
     strParametros = ""
     
     If strParamName1 <> "" Then
        vBanco.Parameters.Remove strParamName1
        vBanco.Parameters.Add strParamName1, strParamValue1, ORAPARM_INPUT
        strParametros = ":" & strParamName1 & ","
     End If
     
     If strParamName2 <> "" Then
        vBanco.Parameters.Remove strParamName2
        vBanco.Parameters.Add strParamName2, strParamValue2, ORAPARM_INPUT
        strParametros = strParametros & ":" & strParamName2 & ","
     End If
     
     If strParamName3 <> "" Then
        vBanco.Parameters.Remove strParamName3
        vBanco.Parameters.Add strParamName3, strParamValue3, ORAPARM_INPUT
        strParametros = strParametros & ":" & strParamName3 & ","
     End If
     
     If strParamName4 <> "" Then
        vBanco.Parameters.Remove strParamName4
        vBanco.Parameters.Add strParamName4, strParamValue4, ORAPARM_INPUT
        strParametros = strParametros & ":" & strParamName4 & ","
     End If
    
     vSql = strProcedure & "(" & strParametros & ":PM_SCALAR, :PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Screen.MousePointer = 0
         Exit Function
      Else
         Carrega_Procedure_Scalar = vBanco.Parameters("PM_SCALAR").Value
      End If
End Function

Sub Aguardar()

    frmAguardar.Show
    frmAguardar.Refresh

End Sub
Sub DefinirTelaSobre()

    vNomeSistema = UCase(App.Title)
    vDescricaoSistema = vObjOracle.Fields(0)
    vAnalistaResponsavel = vObjOracle.Fields(1)
    vAnalistaBackup = vObjOracle.Fields(2)
    
    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)
    
End Sub

Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function
 Sub Process_Line_Errors(ByRef SQL)
    Dim iFnum As Integer
    'on error GoTo Handler_Process_Line_Errors
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
      Resume
    Else
      MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
     'cursor
     Screen.MousePointer = vbDefault
     'If Fl_Banco = "S" Then
     '  dbAccess.Close
     'End If
     'para a aplicacao
     End
    End If
     Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub
Sub Data(ByRef KeyAscii, ByRef txtCampo)
    'on erro GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If IsNumeric(strData) Then
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
                End If
            Else
                Beep
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If IsNumeric(strData) Then
                If CInt(strData) < 1 Or CInt(strData > 31) Then
                    Beep
                Else
                    strData = Mid$(txtCampo.Text, 4, 2)     'mes
                    If IsNumeric(strData) Then
                        If CInt(strData) < 1 Or CInt(strData) > 12 Then
                            Beep
                        Else
                            strData = txtCampo.Text & Chr$(KeyAscii)
                            If Not IsDate(CDate(strData)) Then
                                Beep
                            Else
                                txtCampo.Text = strData
                            End If
                        End If
                    Else
                        Beep
                    End If
                End If
            Else
                Beep
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub


Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function


Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function
Function Tira_Caracter(Palavra)
    Dim proibir, trocar, waux, wlen, letra, wpos

    If Not IsNull(Palavra) Then
        proibir = "��������������������������������������'`^~"
        trocar = "cCaAeEiIoOuUaAeEiIoOuUaAeEiIoOuUaAoOyY    "
        wlen = Len(Palavra)
        For waux = 1 To wlen
            letra = Mid(Palavra, waux, 1)
            wpos = InStr(proibir, letra)
            If wpos > 0 Then
                Palavra = Mid(Palavra, 1, waux - 1) & Mid(trocar, wpos, 1) & Mid(Palavra, waux + 1)
            End If
        Next
    End If
    Tira_Caracter = Palavra
End Function
Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function


