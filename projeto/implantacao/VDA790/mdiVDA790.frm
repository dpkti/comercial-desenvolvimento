VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.MDIForm mdiVDA790 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "VDA790 - PEDIDO INDENT"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12075
   Icon            =   "mdiVDA790.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   12075
      _ExtentX        =   21299
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8149
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "11/11/16"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "11:15"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1335
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   12075
      _Version        =   65536
      _ExtentX        =   21299
      _ExtentY        =   2355
      _StockProps     =   15
      BackColor       =   14215660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   60
         TabIndex        =   2
         Top             =   1170
         Width           =   11805
         _ExtentX        =   20823
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   1005
         Left            =   9840
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   90
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":030A
         PICN            =   "mdiVDA790.frx":0326
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   10890
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   90
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":1000
         PICN            =   "mdiVDA790.frx":101C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDigitacao 
         Height          =   1005
         Left            =   1410
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Digita��o"
         Top             =   90
         Width           =   1055
         _ExtentX        =   1852
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Digita��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   16777215
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":1CF6
         PICN            =   "mdiVDA790.frx":1D12
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdManutencao 
         Height          =   1005
         Left            =   2500
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Manuten��o"
         Top             =   90
         Width           =   1125
         _ExtentX        =   1984
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Manuten��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   16777215
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":213F
         PICN            =   "mdiVDA790.frx":215B
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLiberacao 
         Height          =   1005
         Left            =   3680
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Libera��o"
         Top             =   90
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Libera��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   16777215
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":25A5
         PICN            =   "mdiVDA790.frx":25C1
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdFaturamento 
         Height          =   1005
         Left            =   4770
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Manuten��o"
         Top             =   90
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Faturamento"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   16777215
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":2A05
         PICN            =   "mdiVDA790.frx":2A21
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPSTGeraLote 
         Height          =   1005
         Left            =   6030
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Manuten��o"
         Top             =   90
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Gera Lote PST"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   15663103
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":2EB7
         PICN            =   "mdiVDA790.frx":2ED3
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPSTRetorno 
         Height          =   1005
         Left            =   7120
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Manuten��o"
         Top             =   90
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   " Retorno PST"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   15663103
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":32C9
         PICN            =   "mdiVDA790.frx":32E5
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdPSTLog 
         Height          =   1005
         Left            =   8220
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Manuten��o"
         Top             =   90
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Log PST"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   15663103
         MPTR            =   1
         MICON           =   "mdiVDA790.frx":36EA
         PICN            =   "mdiVDA790.frx":3706
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -60
         Picture         =   "mdiVDA790.frx":3B1D
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   0
         Width           =   1455
      End
   End
End
Attribute VB_Name = "mdiVDA790"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDigitacao_Click()
  frmCabec.Show vbModal
'frmItem.Show
End Sub

Private Sub cmdFaturamento_Click()
  frmFatu.Show vbModal
End Sub

Private Sub cmdLiberacao_Click()
  strFl_Lib = "S"
  frmPedidos.Show 1
End Sub

Private Sub cmdManutencao_Click()
  strFl_Lib = "T"
  frmPedidos.Show 1
End Sub

Private Sub cmdPSTGeraLote_Click()
  frmLote.Show vbModal
End Sub

Private Sub cmdPSTLog_Click()
  frmLog.Show vbModal
End Sub

Private Sub cmdPSTRetorno_Click()
  If Dir(strDiretorio & "fatdiac5.txt") = "" And _
    Dir(strDiretorio & "fatprodc5.txt") = "" Then
    MsgBox "N�o h� arquivo de retorno PST para processar", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    'Exit Sub
  Else
    Open strDiretorio & "fatdiac5.txt" For Input As #1
      Call Processa_Arquivo(1)
    Close #1
    
    Open strDiretorio & "fatprodc5.txt" For Input As #1
      Call Processa_Arquivo(2)
    Close #1
    'Kill strDiretorio & "fatdiac5.txt"
    'Kill strDiretorio & "fatprodc5.txt"
 End If
    Call Atualiza_Nota
    MsgBox "Atualiza��o realizada com sucesso, por favor consulte o log", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    stbBarra.Panels(1).Text = ""
    'MDIForm1.lblMsg = ""
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        If Not vBanco Is Nothing Then
            Call vVB_Generica_001.ExcluiBind(vBanco)
        End If
        
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub MDIForm_Load()
   Dim strFilial As String
    
    If App.PrevInstance = True Then
    
        Call vVB_Generica_001.Informar("J� existe uma inst�ncia desse sistema no ar")
        End
    
    End If
    
    'PEGA O C�DIGO DO USU�RIO PASSADO PELO MENU AUTOM�TICO E
    'VERIFICA SE O SISTEMA EST� SENDO EXECUTADO PELO MENU AUTOM�TICO
'    If Command = "" Then
'
'        Call vVB_Generica_001.Informar("S� � poss�vel executar este sistema atrav�s do Menu Autom�tico. " & Chr(13) & _
'            "Favor entrar em contato com o Helpdesk")
'
'        End
'
'    Else
'
'        vCodUsuario = Command
'
'    End If

    'INCLUIR OS ARQUIVOS: (PROJECT -> ADD FILE OU CTRL+D)
    'H:\Usr\Padroes\VB6\Forms\Aguardar.frm
    'H:\Usr\Padroes\VB6\Forms\Logo.frm
    'H:\Usr\Padroes\VB6\M�dulos\M�dulo padr�o.bas
    frmLogo.Show 1
    
    Me.Top = 0
    Me.Left = 0
    
   'path na rede
    strPath = "h:\ORACLE\DADOS\32Bits\"
    'strPath = "F:\ORACLE\DADOS\32Bits\"
    strDiretorio = "C:\LOTE_PST\"
    
    'TI-5028
    'vCd = Mid(Command$, InStr(1, Command$, "CD") + 2, 2)
    'vTipoCD = Mid(Command$, InStr(1, Command$, "CD") + 4)
    'Verifica se foi informado par�metro no atalho do sistema.  (Exemplos: CD01U, CD04M, CD10M)
    If Mid(Command$, InStr(1, Command$, "CD") + 2, 2) = "" Or Mid(Command$, InStr(1, Command$, "CD") + 4) = "" Then
        MsgBox "Par�metro de inicializa��o do CD n�o informado corretamente." & vbCrLf _
             & "Informe o departamento de sistemas.", vbCritical, "Aten��o"
        End
    Else
        vCd = Mid(Command$, InStr(1, Command$, "CD") + 2, 2)
        vTipoCD = Mid(Command$, InStr(1, Command$, "CD") + 4)
    End If

    
    '<BANCO> = BANCO DE DADOS A SER CONECTADO
    '<USU�RIO> = USU�RIO DE CONEX�O COM O ORACLE
    'vErro = vVB_Generica_001.ConectaOracle("producao", "VDA790", True, Me) 'TI-5028
    vErro = vVB_Generica_001.ConectaOracle("CD" & Format(vCd, "00"), "VDA790", True, Me)
    'vErro = vVB_Generica_001.ConectaOracle("sdpkt", "VDA790", True, Me)

    If vErro <> "" Then
        End
    End If

    'vCd = vVB_Generica_001.vCd           'TI-5028
    'vTipoCD = vVB_Generica_001.vTipoCD   'TI-5028
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
      
    
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    If vObjOracle.EOF Then

        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End

    End If

    Call DefinirTelaSobre
         
         If vTipoCD = "U" Then
           Call Carrega_Procedure("PRODUCAO.Pck_VDA790_ESP.PR_SELECT_DATA_FATURAMENTO")
         Else
           strFilial = "DEP" & Trim(Format(vCd, "00")) & ".Pck_VDA790_ESP.PR_SELECT_DATA_FATURAMENTO"
           Call Carrega_Procedure(strFilial)
         End If
    
        '    'carregar data de faturamento,uf origem, filial origem
        '    SQL = "select a.dt_faturamento, a.dt_real,a.dt_real, a.dt_ini_fech_dia,"
        '    SQL = SQL & " to_char(b.cod_filial,'0009') filial, "
        '    SQL = SQL & " d.cod_uf cod_uf,"
        '    SQL = SQL & " to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default"
        '    SQL = SQL & " from datas a, " & strTabela_Banco & "deposito b, filial c,  cidade d, loja e "
        '    SQL = SQL & " where  b.cod_filial=c.cod_filial and "
        '    SQL = SQL & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "
        '
        '    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
     
    If vObjOracle.EOF And vObjOracle.BOF Then
        Call Process_Line_Errors(SQL)
    Else
        Data_Faturamento = Format(CDate(vObjOracle!dt_faturamento), "dd/mm/yyyy")
        data_ini_fech = Format(CDate(vObjOracle!dt_ini_fech_dia), "dd/mm/yyyy")
        Data_Real = Format(CDate(vObjOracle!dt_real), "ddmmyyyy")
        Filial_Ped = vObjOracle!filial
        UF_origem = vObjOracle!cod_uf
        Deposito_default = vObjOracle!Deposito_default
    End If
    
    dlgLogin.Show 1
'    Call cmdCotacao_Click
  
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
    
End Sub

Public Sub Processa_Arquivo(intTipo As Integer)
 Dim strChar As String
 Dim strLinha As String
 Dim strPedido As String
 Dim strCGC As String
 Dim lngNF As Long
 Dim Data_NF As String
 Dim dblContabil As Double
 Dim strFabrica As String
 Dim lngQtde As Long
 Dim dblValor_item As Double

  On Error GoTo Trata_Erro
   
 Screen.MousePointer = 11
 strFabrica = ""
 lngQtde = 0
   stbBarra.Panels(1).Text = "PROCESSANDO ARQUIVO......"
   DoEvents
   Do While Not EOF(1)
     'Line Input #1, strLinha
     strChar = Input(1, #1)
     If strChar <> Chr(10) Then
         strLinha = strLinha & strChar
     Else
NOVA:
      If Len(strLinha) <> 1 Then
        If intTipo = 1 Then
          strPedido = Mid(strLinha, 1, 20)
          strCGC = Mid(strLinha, 21, 14)
          lngNF = Mid(strLinha, 35, 6)
          Data_NF = Mid(strLinha, 41, 6)
          dblContabil = CDbl(Mid(strLinha, 47, 12) / 100)
          Call Atualiza_Retorno(strPedido, strCGC, lngNF, Data_NF, dblContabil)
           
          strLinha = ""
          Line Input #1, strLinha
          If Mid(strLinha, 1, 1) <> "" Then
            GoTo NOVA
          End If
        Else
           strPedido = Mid(strLinha, 1, 20)
           lngNF = Mid(strLinha, 29, 6)
           strFabrica = Mid(strLinha, 46, 18)
           lngQtde = Mid(strLinha, 64, 11)
           dblValor_item = Mid(strLinha, 79, 15) / lngQtde
            Call Atualiza_Retorno_Item(strPedido, lngNF, strFabrica, lngQtde, dblValor_item)
            strLinha = ""
            Line Input #1, strLinha
            If Mid(strLinha, 1, 1) <> "" Then
              GoTo NOVA
            End If
        End If
      Else
          strLinha = ""
          Line Input #1, strLinha
          If Mid(strLinha, 1, 1) <> "" Then
            GoTo NOVA
          End If

      End If
   End If
   Loop
   
   
 Screen.MousePointer = 0
 Exit Sub
 
Trata_Erro:
 If Err = 62 Then
   Resume Next
 Else
   MsgBox Err & "-" & Err.Description
 End If
 
End Sub

Public Sub Atualiza_Nota()
  
  Dim lngCod_Cliente As Long
  Dim ss As Object
  Dim vObjOracle1 As Object
  Dim vObjOracle2 As Object
  Dim intSeq As Integer
  Dim dt_nf As String
  Dim strFin As String * 1
  Dim strCGC As String
  Dim strPedido As String
  Dim strPedido_aux As String
  Dim lngNota As Long
  Dim strDtNota As String
  Dim dblContabil As Double
  Dim i As Long
  Dim pos As Integer
  Dim strSeq_aux As String
  Dim strFabrica As String
  Dim cont As Long
  Dim lngQtde As Long
  Dim dblPreco As Double
  
  On Error GoTo Trata_Erro
  Screen.MousePointer = 11
  
  stbBarra.Panels(1).Text = "ATUALIZANDO INFORMA��ES PEDIDO INDENT...."
  DoEvents
'  SQL = "Select num_nota,num_pedido,"
'  SQL = SQL & " cgc, vl_contabil,dt_nota"
'  SQL = SQL & " From indent.retorno_ped_pst a, "
'  SQL = SQL & " datas b"
'  SQL = SQL & " Where  fl_atualizacao = 'N' and"
'  SQL = SQL & " dt_arquivo >= dt_faturamento"
'
'  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

  Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_RETORNO_PED_PST")
  
  If vObjOracle.EOF And vObjOracle.BOF Then
    Exit Sub
  End If
  
  For i = 1 To vObjOracle.RecordCount
inicio:
    strPedido = 0
    strSeq_aux = ""
    intSeq = 0
    If IsNull(vObjOracle!cgc) Then
      Screen.MousePointer = 0
      Exit Sub
    End If
    strCGC = CStr(vObjOracle!cgc)
    strPedido_aux = Mid(CStr(vObjOracle!num_pedido), 1, 21)
    pos = InStr(1, strPedido_aux, "/", 0) - 1
    If pos <> -1 Then
      strPedido = Mid(strPedido_aux, 1, pos)
      strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
    Else
      pos = InStr(1, strPedido_aux, "-", 0) - 1
      If pos <> -1 Then
        strPedido = Mid(strPedido_aux, 1, pos)
        strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
      Else
        pos = InStr(1, strPedido_aux, "*", 0) - 1
        If pos <> -1 Then
          strPedido = Mid(strPedido_aux, 1, pos)
          strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
        Else
          pos = InStr(1, strPedido_aux, "C", 0) - 1
          If pos <> -1 Then
            strPedido = Mid(strPedido_aux, 1, pos)
            strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
          Else
            pos = InStr(1, strPedido_aux, "G", 0) - 1
            If pos <> -1 Then
              strPedido = Mid(strPedido_aux, 1, pos)
              strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
            Else
              pos = InStr(1, strPedido_aux, "F", 0) - 1
              If pos <> -1 Then
                strPedido = Mid(strPedido_aux, 1, pos)
                strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
              Else
                pos = InStr(1, strPedido_aux, "B", 0) - 1
                If pos <> -1 Then
                  strPedido = Mid(strPedido_aux, 1, pos)
                  strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
                Else
                  pos = InStr(1, strPedido_aux, "P", 0) - 1
                  If pos <> -1 Then
                    strPedido = Mid(strPedido_aux, 1, pos)
                    strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
                  Else
                    pos = InStr(1, strPedido_aux, "D", 0) - 1
                    If pos <> -1 Then
                      strPedido = Mid(strPedido_aux, 1, pos)
                      strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
                    Else
                      pos = InStr(1, strPedido_aux, "R", 0) - 1
                      If pos <> -1 Then
                        strPedido = Mid(strPedido_aux, 1, pos)
                        strSeq_aux = Mid(strPedido_aux, pos + 2, 1)
                      Else
                        strPedido = Mid(strPedido_aux, 1, 7)
                        strSeq_aux = 0
                      End If
                    End If
                  End If
                End If
              End If
            End If
          End If
        End If
      End If
    End If
    lngNota = CStr(vObjOracle!num_nota)
    'strDtNota = vObjOracle!dt_nota
    strDtNota = Data_Faturamento
    dblContabil = CStr(vObjOracle!vl_contabil)
  
    'localiza cliente
'    SQL = "Select cod_cliente"
'    SQL = SQL & " from cliente "
'    SQL = SQL & " Where cgc = :cgc "
'
'    oradatabase.Parameters.Remove "cgc"
'    oradatabase.Parameters.Add "cgc", strCGC, 1
'
'    Set ss1 = oradatabase.dbcreatedynaset(SQL, 8&)

     vBanco.Parameters.Remove "PM_CGC"
     vBanco.Parameters.Add "PM_CGC", strCGC, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_CLIENTE_PELO_CGC(:PM_CGC,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
        Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Exit Sub
      Else
         Set vObjOracle1 = vBanco.Parameters("PM_CURSOR1").Value
      End If

  
    If vObjOracle1.EOF And vObjOracle1.BOF Then
      vObjOracle.MoveNext
      GoTo inicio
    End If
  
    lngCod_Cliente = CStr(vObjOracle1!COD_CLIENTE)
    
    
    If strSeq_aux <> "" Then
      If strSeq_aux = "A" Then
        intSeq = 0
      ElseIf strSeq_aux = "B" Then
        intSeq = 1
      ElseIf strSeq_aux = "C" Then
        intSeq = 2
      ElseIf strSeq_aux = "D" Then
        intSeq = 3
      ElseIf strSeq_aux = "E" Then
        intSeq = 4
      ElseIf strSeq_aux = "F" Then
        intSeq = 5
      ElseIf strSeq_aux = "G" Then
        intSeq = 6
      ElseIf strSeq_aux = "H" Then
        intSeq = 7
      ElseIf strSeq_aux = "I" Then
        intSeq = 8
      ElseIf strSeq_aux = "P" Then
        intSeq = 9
      ElseIf strSeq_aux = "O" Then
        intSeq = 0
      ElseIf strSeq_aux = "J" Then
        intSeq = 0
      Else
        intSeq = CInt(IIf(strSeq_aux = " ", 0, strSeq_aux))
      End If
    End If
    
'    'localiza pedido
'    SQL = "Select seq_pedido "
'    SQL = SQL & " From indent.pedido_indent"
'    SQL = SQL & " Where num_pedido = :pedido"
'    SQL = SQL & " and cod_cliente = :cliente"
'    SQL = SQL & " and seq_pedido = :seq"
'    SQL = SQL & " and num_nota is null"
'    SQL = SQL & " and fl_ger_nfis = 'N' "
'    SQL = SQL & " and situacao = 0"
'    SQL = SQL & " Order by num_pedido,seq_pedido"
'
'    oradatabase.Parameters.Remove "pedido"
'    oradatabase.Parameters.Add "pedido", strPedido, 1
'    oradatabase.Parameters.Remove "cliente"
'    oradatabase.Parameters.Add "cliente", lngCod_Cliente, 1
'    oradatabase.Parameters.Remove "seq"
'    oradatabase.Parameters.Add "seq", intSeq, 1
'
'    Set ss1 = oradatabase.dbcreatedynaset(SQL, 0&)

     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
     vBanco.Parameters.Add "PM_NUM_PEDIDO", strPedido, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_CLIENTE"
     vBanco.Parameters.Add "PM_COD_CLIENTE", lngCod_Cliente, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
     vBanco.Parameters.Add "PM_SEQ_PEDIDO", intSeq, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_PEDIDO_INDENT_CLI(:PM_NUM_PEDIDO,:PM_COD_CLIENTE,:PM_SEQ_PEDIDO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
        Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Exit Sub
      Else
         Set vObjOracle1 = vBanco.Parameters("PM_CURSOR1").Value
      End If
  
    If vObjOracle1.EOF And vObjOracle1.BOF Then
        vObjOracle.MoveNext
        GoTo inicio
    End If
  
    If strSeq_aux = "" Then
      intSeq = 0
    End If
    
'    SQL = "BEGIN"
'    SQL = SQL & " Update indent.pedido_indent set"
'    SQL = SQL & " num_nota = :nota,"
'    SQL = SQL & " dt_emissao_nota = to_date(:dt_nota,'dd/mm/rr'),"
'    SQL = SQL & " vl_contabil = :cont, fl_ger_nfis='S'"
'    SQL = SQL & " Where num_nota is null and FL_GER_NFIS='N' AND"
'    SQL = SQL & " tp_indent = 1 and "
'    SQL = SQL & " seq_pedido = :seq and"
'    SQL = SQL & " num_pedido = :pedido and"
'    SQL = SQL & " cod_cliente = :cliente;"
'    SQL = SQL & " Update indent.retorno_ped_pst set"
'    SQL = SQL & " fl_atualizacao = 'S'"
'    SQL = SQL & " Where num_nota = :nota;"
'    SQL = SQL & " COMMIT;"
'    SQL = SQL & "EXCEPTION"
'    SQL = SQL & " WHEN OTHERS THEN"
'    SQL = SQL & " ROLLBACK;"
'    SQL = SQL & " :cod_errora := SQLCODE;"
'    SQL = SQL & " :txt_errora := SQLERRM;"
'    SQL = SQL & "END;"
'
'    oradatabase.Parameters.Remove "pedido"
'    oradatabase.Parameters.Add "pedido", CLng(strPedido), 1
'    oradatabase.Parameters.Remove "seq"
'    oradatabase.Parameters.Add "seq", intSeq, 1
'    oradatabase.Parameters.Remove "cliente"
'    oradatabase.Parameters.Add "cliente", lngCod_Cliente, 1
'    oradatabase.Parameters.Remove "nota"
'    oradatabase.Parameters.Add "nota", lngNota, 1
'    oradatabase.Parameters.Remove "dt_nota"
'    oradatabase.Parameters.Add "dt_nota", CDate(strDtNota), 1
'    oradatabase.Parameters.Remove "cont"
'    oradatabase.Parameters.Add "cont", dblContabil, 1
'
'    oradatabase.Parameters.Remove "cod_errora"
'    oradatabase.Parameters.Add "cod_errora", 0, 2
'    oradatabase.Parameters.Remove "txt_errora"
'    oradatabase.Parameters.Add "txt_errora", "", 2
'
'    oradatabase.ExecuteSQL SQL

     vBanco.Parameters.Remove "PM_NUM_NOTA"
     vBanco.Parameters.Add "PM_NUM_NOTA", lngNota, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_DT_EMISSAO"
     vBanco.Parameters.Add "PM_DT_EMISSAO", strDtNota, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_VL_CONTABIL"
     vBanco.Parameters.Add "PM_VL_CONTABIL", dblContabil, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
     vBanco.Parameters.Add "PM_SEQ_PEDIDO", intSeq, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
     vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(strPedido), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_CLIENTE"
     vBanco.Parameters.Add "PM_COD_CLIENTE", lngCod_Cliente, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_PEDIDO_INDENT_PST(:PM_NUM_NOTA,:PM_DT_EMISSAO,:PM_VL_CONTABIL,:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_CLIENTE,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

'      If vErro = "" Then
'        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
'            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
'            Screen.MousePointer = vbDefault
'            strFin = "N"
'            'Exit Sub
'        Else
'            strFin = "S"
'        End If
'      Else
'        Exit Sub
'      End If
      
    If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
        MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
        Screen.MousePointer = vbDefault
        strFin = "N"
        'Exit Sub
    Else
        strFin = "S"

'    If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'      strFin = "N"
'
'    Else
'      strFin = "S"
'      SQL = "Select cod_fabrica,qtd_atendida,valor"
'      SQL = SQL & " from indent.retorno_itped_pst"
'      SQL = SQL & " Where num_nota = :nota"
'
'      oradatabase.Parameters.Remove "nota"
'      oradatabase.Parameters.Add "nota", lngNota, 1
'
'      Set ss2 = oradatabase.dbcreatedynaset(SQL, 0&)

     vBanco.Parameters.Remove "PM_NUM_NOTA"
     vBanco.Parameters.Add "PM_NUM_NOTA", lngNota, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CURSOR1"
     vBanco.Parameters.Add "PM_CURSOR1", 0, ORAPARM_BOTH
     vBanco.Parameters("PM_CURSOR1").ServerType = ORATYPE_CURSOR
     vBanco.Parameters("PM_CURSOR1").DynasetOption = ORADYN_NO_BLANKSTRIP
     vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_SELECT_RETORNO_ITPED_PST(:PM_NUM_NOTA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro <> "" Then
        Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description, vbInformation, "Aten��o"
         Exit Sub
      Else
         Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value
      End If

      If vObjOracle2.EOF And vObjOracle2.BOF Then
        GoTo inicio
      End If
'      SQL = "BEGIN"
'      SQL = SQL & " Update indent.item_pedido_indent set"
'      SQL = SQL & " qtd_atendida = 0"
'      SQL = SQL & " Where tp_indent = 1 and "
'      SQL = SQL & " seq_pedido = :seq and"
'      SQL = SQL & " num_pedido = :pedido;"
'      SQL = SQL & " COMMIT;"
'      SQL = SQL & "EXCEPTION"
'      SQL = SQL & " WHEN OTHERS THEN"
'      SQL = SQL & " ROLLBACK;"
'      SQL = SQL & " :cod_errora := SQLCODE;"
'      SQL = SQL & " :txt_errora := SQLERRM;"
'      SQL = SQL & "END;"
'
'      oradatabase.Parameters.Remove "pedido"
'      oradatabase.Parameters.Add "pedido", CLng(strPedido), 1
'      oradatabase.Parameters.Remove "seq"
'      oradatabase.Parameters.Add "seq", intSeq, 1
'      oradatabase.Parameters.Remove "cod_errora"
'      oradatabase.Parameters.Add "cod_errora", 0, 2
'      oradatabase.Parameters.Remove "txt_errora"
'      oradatabase.Parameters.Add "txt_errora", "", 2
'
'      oradatabase.ExecuteSQL SQL

     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
     vBanco.Parameters.Add "PM_SEQ_PEDIDO", intSeq, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
     vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(strPedido), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_CLIENTE"
     vBanco.Parameters.Add "PM_COD_CLIENTE", lngCod_Cliente, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_ITEM_PED_IND_PST(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If
            
      For cont = 1 To vObjOracle2.RecordCount
        strFabrica = Mid(CStr(vObjOracle2!cod_fabrica), 11, 8)
        lngQtde = CStr(vObjOracle2!qtd_atendida)
        dblPreco = vObjOracle2!Valor
        
'        SQL = "BEGIN"
'        SQL = SQL & " Update indent.item_pedido_indent set"
'        SQL = SQL & " qtd_atendida = :qtde,"
'        SQL = SQL & " preco_unitario_liq = :preco"
'        SQL = SQL & " Where tp_indent = 1 and "
'        SQL = SQL & " seq_pedido = :seq and"
'        SQL = SQL & " num_pedido = :pedido and"
'        SQL = SQL & " cod_fabrica like :fabrica;"
'        SQL = SQL & " COMMIT;"
'        SQL = SQL & "EXCEPTION"
'        SQL = SQL & " WHEN OTHERS THEN"
'        SQL = SQL & " ROLLBACK;"
'        SQL = SQL & " :cod_errora := SQLCODE;"
'        SQL = SQL & " :txt_errora := SQLERRM;"
'        SQL = SQL & "END;"
'
'        oradatabase.Parameters.Remove "pedido"
'        oradatabase.Parameters.Add "pedido", CLng(strPedido), 1
'        oradatabase.Parameters.Remove "seq"
'        oradatabase.Parameters.Add "seq", intSeq, 1
'        oradatabase.Parameters.Remove "fabrica"
'        oradatabase.Parameters.Add "fabrica", "%" & strFabrica & "%", 1
'        oradatabase.Parameters.Remove "qtde"
'        oradatabase.Parameters.Add "qtde", lngQtde, 1
'        oradatabase.Parameters.Remove "preco"
'        oradatabase.Parameters.Add "preco", dblPreco, 1
'
'        oradatabase.Parameters.Remove "cod_errora"
'        oradatabase.Parameters.Add "cod_errora", 0, 2
'        oradatabase.Parameters.Remove "txt_errora"
'        oradatabase.Parameters.Add "txt_errora", "", 2
'
'        oradatabase.ExecuteSQL SQL

     vBanco.Parameters.Remove "PM_SEQ_PEDIDO"
     vBanco.Parameters.Add "PM_SEQ_PEDIDO", intSeq, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_NUM_PEDIDO"
     vBanco.Parameters.Add "PM_NUM_PEDIDO", CLng(strPedido), ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_COD_FABRICA"
     vBanco.Parameters.Add "PM_COD_FABRICA", "%" & strFabrica & "%", ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_QTD"
     vBanco.Parameters.Add "PM_QTD", lngQtde, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_PRECO_UNITARIO"
     vBanco.Parameters.Add "PM_PRECO_UNITARIO", dblPreco, ORAPARM_INPUT
     vBanco.Parameters.Remove "PM_CODERRO"
     vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
     vBanco.Parameters.Remove "PM_TXTERRO"
     vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
    
     vSql = "PRODUCAO.Pck_VDA790.PR_UPDATE_ITEM_PED_IND_PST_2(:PM_SEQ_PEDIDO,:PM_NUM_PEDIDO,:PM_COD_FABRICA,:PM_QTD,:PM_PRECO_UNITARIO,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If

        vObjOracle2.MoveNext
      Next
    End If
    vObjOracle.MoveNext
  Next
  

  
  Screen.MousePointer = 0
  
Exit Sub

Trata_Erro:
 If Err = 3022 Then
   Resume Next
 Else
   MsgBox Err & "-" & Err.Description
 End If

End Sub
Public Sub Atualiza_Retorno(pedido As String, cgc As String, nf As Long, dt As String, _
                         contabil As Double)
  Dim ss As Object
  Dim dt_nf As String
  Dim strFin As String * 1
  
  On Error GoTo Trata_Erro
  dt_nf = dt
  strFin = "N"
  Screen.MousePointer = 11

  stbBarra.Panels(1).Text = "GUARDANDO INF. DOS ARQUIVOS...."
  DoEvents
'  SQL = "BEGIN"
'  SQL = SQL & " Insert into indent.retorno_ped_pst"
'  SQL = SQL & " values(:nota,:pedido,:cgc,:cont,sysdate,:fin,"
'  SQL = SQL & " to_date(:dt_nota,'dd/mm/rr'));"
'  SQL = SQL & " COMMIT;"
'  SQL = SQL & "EXCEPTION"
'  SQL = SQL & " WHEN OTHERS THEN"
'  SQL = SQL & " ROLLBACK;"
'  SQL = SQL & " :cod_errora := SQLCODE;"
'  SQL = SQL & " :txt_errora := SQLERRM;"
'  SQL = SQL & "END;"
'
'  oradatabase.Parameters.Remove "pedido"
'  oradatabase.Parameters.Add "pedido", pedido, 1
'  oradatabase.Parameters.Remove "cgc"
'  oradatabase.Parameters.Add "cgc", cgc, 1
'  oradatabase.Parameters.Remove "nota"
'  oradatabase.Parameters.Add "nota", nf, 1
'  oradatabase.Parameters.Remove "dt_nota"
'  oradatabase.Parameters.Add "dt_nota", CDate(Mid(dt, 1, 2) & "/" & Mid(dt, 3, 2) & "/" & Mid(dt, 5, 2)), 1
'  oradatabase.Parameters.Remove "cont"
'  oradatabase.Parameters.Add "cont", contabil, 1
'  oradatabase.Parameters.Remove "fin"
'  oradatabase.Parameters.Add "fin", strFin, 1
'  oradatabase.Parameters.Remove "cod_errora"
'  oradatabase.Parameters.Add "cod_errora", 0, 2
'  oradatabase.Parameters.Remove "txt_errora"
'  oradatabase.Parameters.Add "txt_errora", "", 2
'
'  oradatabase.ExecuteSQL SQL
  
        vBanco.Parameters.Remove "PM_NUM_NOTA"
        vBanco.Parameters.Add "PM_NUM_NOTA", nf, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CGC"
        vBanco.Parameters.Add "PM_CGC", cgc, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_VL_CONTABIL"
        vBanco.Parameters.Add "PM_VL_CONTABIL", Replace(contabil, ",", "."), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_FL_ATUALIZACAO"
        vBanco.Parameters.Add "PM_FL_ATUALIZACAO", strFin, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_DT_NOTA"
        vBanco.Parameters.Add "PM_DT_NOTA", Mid(dt, 1, 2) & "/" & Mid(dt, 3, 2) & "/" & Mid(dt, 5, 2), ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_INSERT_RETORNO_PED_PST(:PM_NUM_NOTA,:PM_NUM_PEDIDO,:PM_CGC,:PM_VL_CONTABIL,:PM_FL_ATUALIZACAO,:PM_DT_NOTA,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
         Screen.MousePointer = vbDefault
         MsgBox "Ocorreu o erro: " & vErro, vbInformation, "Aten��o"
         Exit Sub
      End If
  
'  If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'    Exit Sub
'  End If
  
  
   Screen.MousePointer = 0
Exit Sub

Trata_Erro:
 If Err = 3022 Then
   Resume Next
 Else
   MsgBox Err & "-" & Err.Description
 End If

End Sub


Public Sub Atualiza_Retorno_Item(pedido As String, nf As Long, fabrica As String, _
                         qtde As Long, valor_item As Double)
  Dim ss As Object
  
  On Error GoTo Trata_Erro

  
  Screen.MousePointer = 11
   
  stbBarra.Panels(1).Text = "GUARDANDO INF. DOS ARQUIVOS...."
  DoEvents
'  SQL = "BEGIN"
'  SQL = SQL & " Insert into indent.retorno_itped_pst"
'  SQL = SQL & " values(:nota,:fabrica,:pedido,:qtd,:valor);"
'  SQL = SQL & " COMMIT;"
'  SQL = SQL & "EXCEPTION"
'  SQL = SQL & " WHEN OTHERS THEN"
'  SQL = SQL & " ROLLBACK;"
'  SQL = SQL & " :cod_errora := SQLCODE;"
'  SQL = SQL & " :txt_errora := SQLERRM;"
'  SQL = SQL & "END;"
'
'  oradatabase.Parameters.Remove "pedido"
'  oradatabase.Parameters.Add "pedido", pedido, 1
'  oradatabase.Parameters.Remove "nota"
'  oradatabase.Parameters.Add "nota", nf, 1
'  oradatabase.Parameters.Remove "fabrica"
'  oradatabase.Parameters.Add "fabrica", fabrica, 1
'  oradatabase.Parameters.Remove "qtd"
'  oradatabase.Parameters.Add "qtd", qtde, 1
'  oradatabase.Parameters.Remove "valor"
'  oradatabase.Parameters.Add "valor", valor_item, 1
'  oradatabase.Parameters.Remove "cod_errora"
'  oradatabase.Parameters.Add "cod_errora", 0, 2
'  oradatabase.Parameters.Remove "txt_errora"
'  oradatabase.Parameters.Add "txt_errora", "", 2
'
'  oradatabase.ExecuteSQL SQL

        vBanco.Parameters.Remove "PM_NUM_NOTA"
        vBanco.Parameters.Add "PM_NUM_NOTA", nf, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_COD_FABRICA"
        vBanco.Parameters.Add "PM_COD_FABRICA", fabrica, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_NUM_PEDIDO"
        vBanco.Parameters.Add "PM_NUM_PEDIDO", pedido, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_QTD_ATENDIDA"
        vBanco.Parameters.Add "PM_QTD_ATENDIDA", qtde, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_VALOR"
        vBanco.Parameters.Add "PM_VALOR", valor_item, ORAPARM_INPUT
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, ORAPARM_OUTPUT
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", 0, ORAPARM_OUTPUT
         
     vSql = "PRODUCAO.Pck_VDA790.PR_INSERT_RETORNO_ITPED_PST(:PM_NUM_NOTA,:PM_COD_FABRICA,:PM_NUM_PEDIDO,:PM_QTD_ATENDIDA,:PM_VALOR,:PM_CODERRO,:PM_TXTERRO)"
     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

      If vErro = "" Then
        If vBanco.Parameters("PM_CODERRO").Value <> 0 Then
            MsgBox "Ocorreu o erro: " & vBanco.Parameters("PM_CODERRO").Value & " - " & vBanco.Parameters("PM_TXTERRO").Value, vbInformation, "Aten��o"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
      Else
        Exit Sub
      End If

'  If Val(oradatabase.Parameters("cod_errora")) <> 0 Then
'    Exit Sub
'  End If
  
 Screen.MousePointer = 0

 
Exit Sub

Trata_Erro:
 If Err = 3022 Then
   Resume Next
 Else
   MsgBox Err & "-" & Err.Description
 End If

End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    Cancel = 1
    
End Sub
