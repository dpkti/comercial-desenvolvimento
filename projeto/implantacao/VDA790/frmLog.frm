VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmLog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Log Atualiza��o PST"
   ClientHeight    =   4740
   ClientLeft      =   300
   ClientTop       =   2340
   ClientWidth     =   10815
   Icon            =   "frmLog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4740
   ScaleWidth      =   10815
   Begin VB.TextBox txtDt 
      Alignment       =   1  'Right Justify
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2760
      MaxLength       =   8
      TabIndex        =   1
      Top             =   720
      Width           =   2295
   End
   Begin VB.ComboBox cboAtu 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2760
      TabIndex        =   0
      Top             =   240
      Width           =   2295
   End
   Begin MSGrid.Grid grdLog 
      Height          =   3255
      Left            =   120
      TabIndex        =   4
      Top             =   1320
      Visible         =   0   'False
      Width           =   10575
      _Version        =   65536
      _ExtentX        =   18653
      _ExtentY        =   5741
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
   Begin Bot�o.cmd Command1 
      Default         =   -1  'True
      Height          =   495
      Left            =   5280
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   480
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmLog.frx":000C
      PICN            =   "frmLog.frx":0028
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command2 
      Cancel          =   -1  'True
      Height          =   495
      Left            =   6720
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   480
      Width           =   1380
      _ExtentX        =   2434
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   15663103
      MPTR            =   1
      MICON           =   "frmLog.frx":0382
      PICN            =   "frmLog.frx":039E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   120
      TabIndex        =   7
      Top             =   1080
      Width           =   10530
      _ExtentX        =   18574
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label Label2 
      Caption         =   "Data Atualiza��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   720
      Width           =   1695
   End
   Begin VB.Label Label1 
      Caption         =   "Situa��o de Atualiza��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Dim ss As Object
  Dim i As Long
  
   If txtDt = "" Or cboAtu = "" Then
     MsgBox "Para fazer a consulta � preciso entrar com a data e tipo de atualiza��o", vbInformation, "Aten��o"
     Exit Sub
   End If
  
'    SQL = "Select a.num_pedido,"
'    SQL = SQL & " a.num_nota,a.cgc,"
'    SQL = SQL & "  b.nome_cliente,"
'    SQL = SQL & " a.vl_contabil, a.dt_arquivo,"
'    SQL = SQL & " a.dt_nota"
'    SQL = SQL & " from indent.retorno_ped_pst a,"
'    SQL = SQL & " cliente b"
'    SQL = SQL & " Where fl_atualizacao = :fl_atu"
'    SQL = SQL & " and dt_arquivo >= to_date(:dt,'dd/mm/rr')"
'    SQL = SQL & " and dt_arquivo < to_date(:dt,'dd/mm/rr') + 1"
'    SQL = SQL & " and a.cgc = b.cgc"
'    SQL = SQL & " Order by a.dt_arquivo, a.num_nota desc"
'
'    oradatabase.Parameters.Remove "fl_atu"
'    oradatabase.Parameters.Add "fl_atu", Mid(Trim(cboAtu), 1, 1), 1
'    oradatabase.Parameters.Remove "dt"
'    oradatabase.Parameters.Add "dt", txtDt, 1
'
'    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

    Call Carrega_Procedure("PRODUCAO.Pck_VDA790.PR_SELECT_LOG", "PM_FL_ATUALIZACAO", Mid(Trim(cboAtu), 1, 1), "PM_DT_ARQUIVO", txtDt.Text)
    
    If vObjOracle.EOF And vObjOracle.BOF Then
      MsgBox "N�o h� arquivo nesta data", vbInformation, "Aten��o"
      grdLog.Visible = False
      Exit Sub
    End If
    
   
    grdLog.Visible = True
    With frmLog.grdLog
        .Cols = 7
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 1800
        .ColWidth(1) = 1000
        .ColWidth(2) = 1500
        .ColWidth(3) = 2500
        .ColWidth(4) = 1500
        .ColWidth(5) = 1200
        .ColWidth(6) = 1200
        
        .Row = 0
        .Col = 0
        .Text = "Pedido"
        .Col = 1
        .Text = "NF"
        .Col = 2
        .Text = "CGC"
        .Col = 3
        .Text = "Cliente"
        .Col = 4
        .Text = "Vl.Contabil"
        .Col = 5
        .Text = "Dt.NF"
        .Col = 6
        .Text = "Dt.Atualiza��o"
        
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = vObjOracle!num_pedido
            .Col = 1
            .Text = vObjOracle!num_nota
            .Col = 2
            .Text = vObjOracle!cgc
            .Col = 3
            .Text = vObjOracle!nome_cliente
            .Col = 4
            .Text = Format(vObjOracle!vl_contabil, "0.00")
            .Col = 5
            .Text = vObjOracle!dt_nota
            .Col = 6
            .Text = vObjOracle!dt_arquivo
            
            vObjOracle.MoveNext
        Next
        .Row = 1
    End With
    Screen.MousePointer = 0
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()
  If cboAtu.ListCount = 0 Then
    cboAtu.AddItem "S - ATUALIZADO"
    cboAtu.AddItem "N - N�O ATUALIZADO"
  End If
  txtDt = Format(Data_Faturamento, "dd/mm/yy")
End Sub


Private Sub grdLog_DblClick()
  lngResposta = 0
  frmLog.grdLog.Col = 1
  lngResposta = frmLog.grdLog.Text
  frmLogI.Show 1
End Sub

Private Sub txtDt_KeyPress(KeyAscii As Integer)
  Call Data(KeyAscii, txtDt)
End Sub


