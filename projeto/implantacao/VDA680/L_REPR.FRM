VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmRepresentante 
   Caption         =   "Lista de Representante"
   ClientHeight    =   3075
   ClientLeft      =   1260
   ClientTop       =   1965
   ClientWidth     =   6705
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3075
   ScaleWidth      =   6705
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5400
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
   Begin MSGrid.Grid grdRepresentante 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "L_REPR.frx":0000
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   4935
   End
End
Attribute VB_Name = "frmRepresentante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String
Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmRepresentante = Nothing
End Sub


Private Sub Form_Load()

    On Error GoTo TrataErro
    
    Dim SQL As String
    Dim ss As Object
    Dim i As Integer
    
    'mouse
    Screen.MousePointer = vbHourglass
    
    'montar SQL
    SQL = "select COD_REPRES,PSEUDONIMO,TIPO "
    SQL = SQL & "from REPRESENTANTE "
    SQL = SQL & "where SITUACAO = 0 and DIVISAO = 'D' and TIPO IN ('R','P') "
    SQL = SQL & "and SEQ_FRANQUIA = 0"
    SQL = SQL & "order by PSEUDONIMO"
    
    'criar consulta
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha representante cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    With frmRepresentante.grdRepresentante
        .Cols = 3
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 5000
        .ColWidth(2) = 400
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Nome do Representante"
        .Col = 2
        .Text = "Tipo"
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss("COD_REPRES")
            .Col = 1
            .Text = ss("PSEUDONIMO")
            .Col = 2
            .Text = ss("TIPO")
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
        
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
     
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmRepresentante = Nothing
End Sub


Private Sub grdRepresentante_DblClick()
    grdRepresentante.Col = 0
    txtResposta = grdRepresentante.Text
    Unload Me
End Sub

Private Sub grdRepresentante_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdRepresentante.Col = 1
    If KeyAscii = 13 Then
        grdRepresentante.Col = 0
        txtResposta = grdRepresentante.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdRepresentante.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdRepresentante
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdRepresentante.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdRepresentante
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdRepresentante.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub

