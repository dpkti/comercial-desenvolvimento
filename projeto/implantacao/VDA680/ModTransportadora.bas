Attribute VB_Name = "ModTransportadora"
'---------------------------------------------------------------------------------------
' Module    : ModTransportadora
' Author    : c.samuel.oliveira
' Date      : 10/07/2018
' Purpose   : TI-5912
'---------------------------------------------------------------------------------------

'VXAVIER CIT - 08/09/2015 - TI-3023 - Frete DPK
Public codCidadeCliente As Long
Public tipoFrete As String
Public Type Transportadora
    CodItTransp As Long
    codTransp   As String
    Nome        As String
    Prazo       As Integer
    VlFrete     As Double
    FLRetira    As Boolean
    FatMin      As Double
End Type
Public TotalNota As Double
Public listaTransportadorasCIF() As Transportadora
Public listaTransportadorasCIFD() As Transportadora
Public listaTransportadorasFOB() As Transportadora
Public listaTransportadorasPLANO() As Transportadora 'TI-5912

'VXAVIER CIT - 09/09/2015 - TI-3023 - Frete DPK
Public Sub PrrencheListasTransportadoras()
    
    Dim count As Integer
    Dim ssItiTransp As Object
    Dim ssFOB As Object
    Dim ssPLANO As Object 'TI-5912
    
    ReDim listaTransportadorasCIFD(0)
   
    OraParameters.Remove "P_COD_LOJA"
    OraParameters.Add "P_COD_LOJA", lngCod_Loja, 1
        
    OraParameters.Remove "P_COD_CIDADE"
    OraParameters.Add "P_COD_CIDADE", codCidadeCliente, 1
    
    OraParameters.Remove "P_TOTAL_PEDIDO"
    OraParameters.Add "P_TOTAL_PEDIDO", TotalNota, 1
            
    'Criar_Cursor
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_SELECT_TRANSP_CIFD(:vCursor, :P_COD_LOJA, :P_COD_CIDADE, :P_TOTAL_PEDIDO); END;"
    Set ssItiTransp = oradatabase.Parameters("vCursor").Value
    
    ReDim listaTransportadorasCIFD(0)
    count = 1
    Do While ssItiTransp.EOF = False
        ReDim Preserve listaTransportadorasCIFD(count)
        Dim oTransportadora As Transportadora
        If (ssItiTransp!COD_ITI_TRANSP <> "") Then
            oTransportadora.CodItTransp = ssItiTransp!COD_ITI_TRANSP
        Else
            oTransportadora.CodItTransp = 0
        End If
        oTransportadora.codTransp = ssItiTransp!COD_TRANSP
        oTransportadora.Nome = ssItiTransp!NOME_TRANSP
        If (ssItiTransp!PRAZO_ENTREGA <> "") Then
            oTransportadora.Prazo = ssItiTransp!PRAZO_ENTREGA
        Else
            oTransportadora.Prazo = 0
        End If
        If (ssItiTransp!vl_frete <> "") Then
            oTransportadora.VlFrete = ssItiTransp!vl_frete
        Else
            oTransportadora.VlFrete = 0
        End If
        If (ssItiTransp!RETIRAOUBLOQUEIO = "N") Then
            oTransportadora.FLRetira = False
        Else
            oTransportadora.FLRetira = True
        End If
        If (ssItiTransp!FAT_MINIMO <> "") Then
            oTransportadora.FatMin = ssItiTransp!FAT_MINIMO
        Else
            oTransportadora.FatMin = 0
        End If
        
        listaTransportadorasCIFD(count) = oTransportadora
        
        ssItiTransp.MoveNext
        count = count + 1
    Loop
    
    ReDim listaTransportadorasCIF(0)
   
    OraParameters.Remove "P_COD_LOJA"
    OraParameters.Add "P_COD_LOJA", lngCod_Loja, 1
        
    OraParameters.Remove "P_COD_CIDADE"
    OraParameters.Add "P_COD_CIDADE", codCidadeCliente, 1
            
    'Criar_Cursor
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_SELECT_TRANSP_CIF(:vCursor, :P_COD_LOJA, :P_COD_CIDADE); END;"
    Set ssItiTransp = oradatabase.Parameters("vCursor").Value
    
    ReDim listaTransportadorasCIF(0)
    count = 1
    Do While ssItiTransp.EOF = False
        ReDim Preserve listaTransportadorasCIF(count)
        Dim oTransportadoraCIF As Transportadora
        oTransportadoraCIF.CodItTransp = 0
        oTransportadoraCIF.codTransp = ssItiTransp!COD_TRANSP
        oTransportadoraCIF.Nome = ssItiTransp!NOME_TRANSP
        oTransportadoraCIF.Prazo = 0
        oTransportadoraCIF.VlFrete = 0

        If (ssItiTransp!RETIRAOUBLOQUEIO = "N") Then
            oTransportadoraCIF.FLRetira = False
        Else
            oTransportadoraCIF.FLRetira = True
        End If
        
        listaTransportadorasCIF(count) = oTransportadoraCIF
        
        ssItiTransp.MoveNext
        count = count + 1
    Loop
    
    ReDim listaTransportadorasFOB(0)
    
    'Criar_Cursor
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_SELECT_TRANSP_FOB(:vCursor); END;"
    Set ssFOB = oradatabase.Parameters("vCursor").Value
    
    count = 1
    Do While ssFOB.EOF = False
        ReDim Preserve listaTransportadorasFOB(count)
        Dim oTransportadoraFOB As Transportadora
        oTransportadoraFOB.codTransp = ssFOB!COD_TRANSP
        oTransportadoraFOB.Nome = ssFOB!NOME_TRANSP
        listaTransportadorasFOB(count) = oTransportadoraFOB
        ssFOB.MoveNext
        count = count + 1
    Loop

    'TI-5912
    ReDim listaTransportadorasPLANO(0)

    OraParameters.Remove "P_COD_LOJA"
    OraParameters.Add "P_COD_LOJA", lngCod_Loja, 1
    
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_SELECT_TRANSP_PLANO(:vCursor,:P_COD_LOJA); END;"
    Set ssPLANO = oradatabase.Parameters("vCursor").Value

    count = 1
    Do While ssPLANO.EOF = False
        ReDim Preserve listaTransportadorasPLANO(count)
        Dim oTransportadoraPLANO As Transportadora
        oTransportadoraPLANO.codTransp = ssPLANO!COD_TRANSP
        oTransportadoraPLANO.Nome = ssPLANO!NOME_TRANSP
        listaTransportadorasPLANO(count) = oTransportadoraPLANO
        ssPLANO.MoveNext
        count = count + 1
    Loop
    'FIM TI-5912
    
End Sub
'VXAVIER CIT - 09/09/2015 - TI-3023 - Frete DPK
Public Sub MergeItinerarioPedido(codItiTransp As Long)
        
    OraParameters.Remove "P_COD_LOJA"
    OraParameters.Add "P_COD_LOJA", lngCod_Loja, 1
    
    OraParameters.Remove "P_COD_LOJA_ORIGEM"
    OraParameters.Add "P_COD_LOJA_ORIGEM", lngCD, 1
        
    OraParameters.Remove "P_NUM_PENDENTE"
    OraParameters.Add "P_NUM_PENDENTE", lngNUM_PEDIDO, 1
        
    OraParameters.Remove "P_SEQ_PENDENTE"
    OraParameters.Add "P_SEQ_PENDENTE", lngSEQ_PEDIDO, 1
        
    OraParameters.Remove "P_COD_ITI_TRANSP"
    OraParameters.Add "P_COD_ITI_TRANSP", codItiTransp, 1
         
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_MERGE_ITINERARIO_PEDIDO(:P_COD_LOJA, :P_COD_LOJA_ORIGEM, :P_NUM_PENDENTE, :P_SEQ_PENDENTE, :P_COD_ITI_TRANSP); END;"
End Sub

'VXAVIER CIT - 09/09/2015 - TI-3023 - Frete DPK
Public Function VerificaTransportadoraRetira(codTransp As String) As Boolean

    OraParameters.Remove "P_COD_TRANSP"
    OraParameters.Add "P_COD_TRANSP", codTransp, 1
    
    OraParameters.Remove "P_COD_LOJA"
    OraParameters.Add "P_COD_LOJA", lngCod_Loja, 1
    
    OraParameters.Remove "P_EXISTE"
    OraParameters.Add "P_EXISTE", 0, 2
    
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.pck_vda230.PR_SELECT_VERIFICA_RETIRA(:P_COD_TRANSP, :P_COD_LOJA, :P_EXISTE); END;"
    
    If (Val(oradatabase.Parameters("P_EXISTE")) > 0) Then
        VerificaTransportadoraRetira = True
    Else
        VerificaTransportadoraRetira = False
    End If
    
End Function

'VXAVIER CIT - 09/09/2015 - TI-3023 - Frete DPK
Public Sub CalculaTotalNota()
    
    Dim ssTotalNota As Object
    
    SQL = "SELECT NVL(SUM(((IT.PRECO_UNITARIO * (1 + P.PC_ACRESCIMO / 100) *"
    SQL = SQL & " 1 - P.PC_DESCONTO / 100) * (1 - P.PC_DESC_SUFRAMA / 100) *"
    SQL = SQL & " (1 - IT.PC_DESC_ICM / 100) * (1 - IT.PC_DESCONTO1 / 100)) *"
    SQL = SQL & " IT.QTD_SOLICITADA),0) AS TOTAL_PEDIDO"
    SQL = SQL & " FROM VENDAS.ITPEDIDO_OUTSAI IT, VENDAS.PEDIDO_OUTSAI P"
    SQL = SQL & " Where P.COD_LOJA = IT.COD_LOJA"
    SQL = SQL & " AND P.NUM_PENDENTE = IT.NUM_PENDENTE"
    SQL = SQL & " AND P.SEQ_PEDIDO = IT.SEQ_PEDIDO"
    SQL = SQL & " AND P.COD_LOJA = IT.COD_LOJA"
    SQL = SQL & " AND IT.SITUACAO = 0"
    SQL = SQL & " AND P.NUM_PENDENTE = " & lngNUM_PEDIDO
    SQL = SQL & " AND P.SEQ_PEDIDO = " & lngSEQ_PEDIDO
    SQL = SQL & " AND P.COD_LOJA = " & lngCod_Loja
    
    Set ssTotalNota = oradatabase.dbcreatedynaset(SQL, 0&)  'TI-5026
    FreeLocks
    
    TotalNota = Round(ssTotalNota!TOTAL_PEDIDO, 2)
    
    ssTotalNota.Close
    
End Sub

'VXAVIER CIT - 10/09/2015 - TI-3023 - Frete DPK
Public Function BuscarCodItiTranp() As Long
    
    Dim ss2 As Object
    
    SQL = "SELECT IP.COD_ITI_TRANSP"
    SQL = SQL & " FROM PRODUCAO.ITINERARIO_PEDIDO IP"
    SQL = SQL & " WHERE IP.COD_LOJA = :CODLOJA"
    SQL = SQL & " AND IP.NUM_PENDENTE = :NUMPENDENTE"
    SQL = SQL & " AND IP.SEQ_PENDENTE = :SEQPENDENTE"
    
    oradatabase.Parameters.Remove "CODLOJA"
    oradatabase.Parameters.Add "CODLOJA", lngCod_Loja, 1
    oradatabase.Parameters.Remove "NUMPENDENTE"
    oradatabase.Parameters.Add "NUMPENDENTE", lngNUM_PEDIDO, 1
    oradatabase.Parameters.Remove "SEQPENDENTE"
    oradatabase.Parameters.Add "SEQPENDENTE", lngSEQ_PEDIDO, 1
    
    Set ss2 = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If IsNull(ss2!COD_ITI_TRANSP) Then
        BuscarCodItiTranp = 0
    Else
        BuscarCodItiTranp = ss2!COD_ITI_TRANSP
    End If

End Function
