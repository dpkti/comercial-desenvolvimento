Imports Oracle.DataAccess.Client

Public Class Email
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Ora As Oracle.DataAccess.Client.OracleConnection
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dtgEmail As System.Windows.Forms.DataGrid
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.dtgEmail = New System.Windows.Forms.DataGrid
        Me.Ora = New Oracle.DataAccess.Client.OracleConnection
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        CType(Me.dtgEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtgEmail
        '
        Me.dtgEmail.AllowNavigation = False
        Me.dtgEmail.DataMember = ""
        Me.dtgEmail.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dtgEmail.Location = New System.Drawing.Point(9, 72)
        Me.dtgEmail.Name = "dtgEmail"
        Me.dtgEmail.ReadOnly = True
        Me.dtgEmail.RowHeadersVisible = False
        Me.dtgEmail.Size = New System.Drawing.Size(393, 159)
        Me.dtgEmail.TabIndex = 0
        '
        'Ora
        '
        Me.Ora.ConnectionString = CType(configurationAppSettings.GetValue("strConnection", GetType(System.String)), String)
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(15, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Email"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(54, 24)
        Me.txtEmail.MaxLength = 50
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(252, 20)
        Me.txtEmail.TabIndex = 2
        Me.txtEmail.Text = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(321, 24)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Incluir"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(324, 240)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Excluir"
        '
        'Email
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(412, 273)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtgEmail)
        Me.Name = "Email"
        Me.Text = "Cadastro de Emails"
        CType(Me.dtgEmail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Email_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Ora.Open()

        CarregaGrid()

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If txtEmail.Text = "" Then
            MessageBox.Show("Campo email � Obrigat�rio")
            Exit Sub
        End If

        actEmail("I", txtEmail.Text)

    End Sub


    Private Sub CarregaGrid()

        Dim CMD As New OracleCommand
        CMD.Connection = Ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CON_EMAIL"

        CMD.Parameters.Add(New OracleParameter("cCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim dta_Email As New OracleDataAdapter(CMD)
        Dim dts_Email As New DataTable
        dta_Email.Fill(dts_Email)

        dtgEmail.DataSource = dts_Email

    End Sub

    Private Sub actEmail(ByVal pACT As Char, ByVal pEMAIL As String)

        Dim CMD As New OracleCommand
        CMD.Connection = Ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_ACT_EMAIL"

        '****
        '**** Parametros
        '****
        CMD.Parameters.Add(New OracleParameter("PM_ACT", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_ACT").Value = pACT

        CMD.Parameters.Add(New OracleParameter("PM_EMAIL", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_EMAIL").Value = pEMAIL

        CMD.ExecuteNonQuery()

        txtEmail.Text = ""

        CarregaGrid()

    End Sub

    Private Sub Email_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        Ora.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        Dim vEmail As String

        vEmail = dtgEmail.Item(dtgEmail.CurrentCell.RowNumber, dtgEmail.CurrentCell.ColumnNumber).ToString

        If vEmail.Trim.Length = 0 Then
            MessageBox.Show("Selecione um Email para ecluir")
            Exit Sub
        End If

        actEmail("D", vEmail)
    End Sub
End Class
