Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types
Imports System.IO
Imports System.IO.File
Imports System.IO.Directory
Imports cls_FIL200

Public Class DetPedidos
    Inherits System.Windows.Forms.Form

    Dim strDeposito As String
    Dim strPedido As String
    Dim nrNode As Integer

    Public FIL200 As clsFIL200
    Public frmForm1 As Form1

    Dim strPasta As String

    Public Property DEPOSITO() As String
        Get
            Return strDeposito
        End Get
        Set(ByVal Value As String)
            strDeposito = Value
        End Set
    End Property

    Public Property PEDIDO() As String
        Get
            Return strPedido
        End Get
        Set(ByVal Value As String)
            strPedido = Value
        End Set
    End Property

    Public Property Node() As String
        Get
            Return nrNode
        End Get
        Set(ByVal Value As String)
            nrNode = Value
        End Set
    End Property

    Dim COD_LOJA As String
    Dim NUM_PEDIDO As String
    Dim SEQ_PEDIDO As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblCD As System.Windows.Forms.Label
    Friend WithEvents lblPedido As System.Windows.Forms.Label
    Friend WithEvents dtgPEDNOTA As System.Windows.Forms.DataGrid
    Friend WithEvents dtgITPEDNOTA As System.Windows.Forms.DataGrid
    Friend WithEvents dtgITPEDVENDA As System.Windows.Forms.DataGrid
    Friend WithEvents dtgPEDVENDA As System.Windows.Forms.DataGrid
    Friend WithEvents btnReproc As System.Windows.Forms.Button
    Friend WithEvents btnApagar As System.Windows.Forms.Button
    Friend WithEvents Ora As Oracle.DataAccess.Client.OracleConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.lblCD = New System.Windows.Forms.Label
        Me.lblPedido = New System.Windows.Forms.Label
        Me.dtgPEDNOTA = New System.Windows.Forms.DataGrid
        Me.dtgITPEDNOTA = New System.Windows.Forms.DataGrid
        Me.dtgITPEDVENDA = New System.Windows.Forms.DataGrid
        Me.dtgPEDVENDA = New System.Windows.Forms.DataGrid
        Me.btnReproc = New System.Windows.Forms.Button
        Me.btnApagar = New System.Windows.Forms.Button
        Me.Ora = New Oracle.DataAccess.Client.OracleConnection
        CType(Me.dtgPEDNOTA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgITPEDNOTA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgITPEDVENDA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtgPEDVENDA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblCD
        '
        Me.lblCD.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCD.ForeColor = System.Drawing.Color.Red
        Me.lblCD.Location = New System.Drawing.Point(171, 9)
        Me.lblCD.Name = "lblCD"
        Me.lblCD.Size = New System.Drawing.Size(216, 27)
        Me.lblCD.TabIndex = 0
        Me.lblCD.Text = "Label1"
        Me.lblCD.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'lblPedido
        '
        Me.lblPedido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPedido.Location = New System.Drawing.Point(9, 9)
        Me.lblPedido.Name = "lblPedido"
        Me.lblPedido.Size = New System.Drawing.Size(159, 27)
        Me.lblPedido.TabIndex = 1
        Me.lblPedido.Text = "Label2"
        Me.lblPedido.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'dtgPEDNOTA
        '
        Me.dtgPEDNOTA.CaptionText = "PED_NOTAVENDA"
        Me.dtgPEDNOTA.DataMember = ""
        Me.dtgPEDNOTA.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dtgPEDNOTA.Location = New System.Drawing.Point(9, 261)
        Me.dtgPEDNOTA.Name = "dtgPEDNOTA"
        Me.dtgPEDNOTA.ReadOnly = True
        Me.dtgPEDNOTA.Size = New System.Drawing.Size(618, 87)
        Me.dtgPEDNOTA.TabIndex = 2
        '
        'dtgITPEDNOTA
        '
        Me.dtgITPEDNOTA.CaptionText = "ITPED_NOTAVENDA"
        Me.dtgITPEDNOTA.DataMember = ""
        Me.dtgITPEDNOTA.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dtgITPEDNOTA.Location = New System.Drawing.Point(9, 354)
        Me.dtgITPEDNOTA.Name = "dtgITPEDNOTA"
        Me.dtgITPEDNOTA.ReadOnly = True
        Me.dtgITPEDNOTA.Size = New System.Drawing.Size(618, 108)
        Me.dtgITPEDNOTA.TabIndex = 3
        '
        'dtgITPEDVENDA
        '
        Me.dtgITPEDVENDA.CaptionText = "VENDAS.ITPED_VENDA"
        Me.dtgITPEDVENDA.DataMember = ""
        Me.dtgITPEDVENDA.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dtgITPEDVENDA.Location = New System.Drawing.Point(8, 141)
        Me.dtgITPEDVENDA.Name = "dtgITPEDVENDA"
        Me.dtgITPEDVENDA.ReadOnly = True
        Me.dtgITPEDVENDA.Size = New System.Drawing.Size(618, 108)
        Me.dtgITPEDVENDA.TabIndex = 5
        '
        'dtgPEDVENDA
        '
        Me.dtgPEDVENDA.CaptionText = "VENDAS.PED_VENDA"
        Me.dtgPEDVENDA.DataMember = ""
        Me.dtgPEDVENDA.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.dtgPEDVENDA.Location = New System.Drawing.Point(8, 48)
        Me.dtgPEDVENDA.Name = "dtgPEDVENDA"
        Me.dtgPEDVENDA.ReadOnly = True
        Me.dtgPEDVENDA.Size = New System.Drawing.Size(618, 87)
        Me.dtgPEDVENDA.TabIndex = 4
        '
        'btnReproc
        '
        Me.btnReproc.Enabled = False
        Me.btnReproc.Location = New System.Drawing.Point(420, 12)
        Me.btnReproc.Name = "btnReproc"
        Me.btnReproc.Size = New System.Drawing.Size(102, 23)
        Me.btnReproc.TabIndex = 6
        Me.btnReproc.Text = "Reprocessar"
        '
        'btnApagar
        '
        Me.btnApagar.Enabled = False
        Me.btnApagar.Location = New System.Drawing.Point(531, 12)
        Me.btnApagar.Name = "btnApagar"
        Me.btnApagar.Size = New System.Drawing.Size(93, 23)
        Me.btnApagar.TabIndex = 7
        Me.btnApagar.Text = "Apagar Arquivo"
        '
        'Ora
        '
        Me.Ora.ConnectionString = CType(configurationAppSettings.GetValue("strConnection", GetType(System.String)), String)
        '
        'DetPedidos
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(634, 471)
        Me.Controls.Add(Me.btnApagar)
        Me.Controls.Add(Me.btnReproc)
        Me.Controls.Add(Me.dtgITPEDVENDA)
        Me.Controls.Add(Me.dtgPEDVENDA)
        Me.Controls.Add(Me.dtgITPEDNOTA)
        Me.Controls.Add(Me.dtgPEDNOTA)
        Me.Controls.Add(Me.lblPedido)
        Me.Controls.Add(Me.lblCD)
        Me.Name = "DetPedidos"
        Me.Text = "Detalhes do Pedido Gravado"
        CType(Me.dtgPEDNOTA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgITPEDNOTA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgITPEDVENDA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtgPEDVENDA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub DetPedidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        COD_LOJA = Mid(strDeposito, 1, 2)
        NUM_PEDIDO = Mid(strPedido, 1, Len(strPedido) - 1)
        SEQ_PEDIDO = Mid(strPedido, Len(strPedido), 1)

        lblPedido.Text = "PEDIDO: " & NUM_PEDIDO & "-" & SEQ_PEDIDO
        lblCD.Text = "CD" & strDeposito

        strPasta = frmForm1.arrCDs_Pastas(nrNode)

        ConsultaPedido()

    End Sub

    Private Sub btnReproc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReproc.Click

        Dim strARQUIVO As String
        Dim strNOME As String
        Dim strPATH As String = "P:\IN\" & strPasta & "\"

        If IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO") Then
            File.Move("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO", "P:\IN\" & strPasta & "\PED" & strPedido & ".TXT")
        End If
        If IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".HDR.ERRO") Then
            File.Move("P:\IN\" & strPasta & "\PED" & strPedido & ".HDR.ERRO", "P:\IN\" & strPasta & "\PED" & strPedido & ".HDR")
        End If

        strNOME = Mid(strPATH, InStrRev(strPATH, "IN\") + 3) & "PED" & strPedido & ".TXT"
        strPATH = strPATH

        Dim objE As New FileSystemEventArgs(WatcherChangeTypes.Created, "P:\IN\", strNOME)
        Invoke(New Form1.OnChangedDelegate(AddressOf frmForm1.OnChanged), New Object() {Nothing, objE})
        objE = Nothing

        ConsultaPedido()

    End Sub

    Private Sub btnApagar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApagar.Click

        If IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO") Then
            Kill("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO")
        Else
            Try
                Kill("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT")
            Catch ex As Exception
                MessageBox.Show("N�o foi possivel apagar o arquivo, tente manualmente")
            End Try
        End If

        If IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".HDR.ERRO") Then
            Kill("P:\IN\" & strPasta & "\PED" & strPedido & ".HDR.ERRO")
        Else
            Try
                Kill("P:\IN\" & strPasta & "\PED" & strPedido & ".HDR")
            Catch ex As Exception
                MessageBox.Show("N�o foi possivel apagar o arquivo, tente manualmente")
            End Try
        End If

        btnApagar.Enabled = False
        btnReproc.Enabled = False

    End Sub

    Private Sub ConsultaPedido()


        Dim CMD As New OracleCommand

        CMD.Parameters.Clear()
        CMD.Connection = Ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_BUSCA_DETPEDIDOS"

        Dim DEP As String = "DEP" & COD_LOJA
        CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_DEP").Value = DEP

        CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_LOJA").Value = COD_LOJA

        CMD.Parameters.Add(New OracleParameter("PM_PED", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_PED").Value = NUM_PEDIDO ''Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 2)

        CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_SEQ").Value = SEQ_PEDIDO ''Mid(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1, 1)

        CMD.Parameters.Add(New OracleParameter("PM_PEDVENDA", OracleDbType.RefCursor, ParameterDirection.Output))
        CMD.Parameters.Add(New OracleParameter("PM_ITPEDVENDA", OracleDbType.RefCursor, ParameterDirection.Output))
        CMD.Parameters.Add(New OracleParameter("PM_PEDNOTA", OracleDbType.RefCursor, ParameterDirection.Output))
        CMD.Parameters.Add(New OracleParameter("PM_IPPEDNOTA", OracleDbType.RefCursor, ParameterDirection.Output))

        Dim daPedido As New OracleDataAdapter(CMD)
        Dim dtsPedido As New DataSet
        daPedido.Fill(dtsPedido)

        dtgPEDVENDA.DataSource = dtsPedido.Tables(0)
        dtgITPEDVENDA.DataSource = dtsPedido.Tables(1)
        dtgPEDNOTA.DataSource = dtsPedido.Tables(2)
        dtgITPEDNOTA.DataSource = dtsPedido.Tables(3)

        'MessageBox.Show("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT")

        If dtsPedido.Tables(2).Rows.Count > 0 And _
            dtsPedido.Tables(3).Rows.Count > 0 And _
            (IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT") Or IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO")) Then
            btnApagar.Enabled = True
            btnReproc.Enabled = False
        End If

        If dtsPedido.Tables(2).Rows.Count = 0 And _
            dtsPedido.Tables(3).Rows.Count = 0 And _
            (IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT") Or IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO")) Then
            btnApagar.Enabled = True
            btnReproc.Enabled = True
        ElseIf dtsPedido.Tables(2).Rows.Count = 0 And _
                dtsPedido.Tables(3).Rows.Count = 0 And _
                (Not IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT") Or Not IO.File.Exists("P:\IN\" & strPasta & "\PED" & strPedido & ".TXT.ERRO")) Then
            btnApagar.Enabled = False
            btnReproc.Enabled = False
        End If

    End Sub
End Class
