Imports System.Threading
Imports Oracle.DataAccess.Client
Imports System.IO
Imports System.IO.Directory
Imports cls_FIL200


Public Class Form1
    Inherits System.Windows.Forms.Form

    'Dim FIL200 As New clsFIL200

    Dim fltrwcd As Integer
    Dim flTimer As Boolean = False

    Dim TotProcess As Integer = 0

    Dim Data_Faturamento As Date
    Dim data_real As Date
    Dim FILIAL_PED As String
    Dim uf_origem As String

    Dim trwIndice As Integer

    Public arrCDs() As Integer
    Public arrCDs_Ativos() As Boolean
    Public arrCDs_Pastas() As String
    Public arrCDs_IP() As String

    Dim clsPing As New ICMPClass
    Dim reply As ICMPClass.ICMPReplyStructure
    Dim nrNode As Int32

    Public strPedidoForm As String
    Public strNodeForm As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Ora As Oracle.DataAccess.Client.OracleConnection
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents trwCD As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents btnDetPedido As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblProc As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Ora = New Oracle.DataAccess.Client.OracleConnection
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.trwCD = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.btnDetPedido = New System.Windows.Forms.Button
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblProc = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Ora
        '
        Me.Ora.ConnectionString = CType(configurationAppSettings.GetValue("strConnection", GetType(String)), String)
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(12, 285)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(483, 121)
        Me.ListBox1.TabIndex = 0
        '
        'trwCD
        '
        Me.trwCD.HideSelection = False
        Me.trwCD.Location = New System.Drawing.Point(9, 87)
        Me.trwCD.Name = "trwCD"
        Me.trwCD.Size = New System.Drawing.Size(486, 315)
        Me.trwCD.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        Me.ImageList1.Images.SetKeyName(1, "")
        Me.ImageList1.Images.SetKeyName(2, "")
        Me.ImageList1.Images.SetKeyName(3, "")
        Me.ImageList1.Images.SetKeyName(4, "")
        '
        'Button1
        '
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(12, 413)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(66, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Expadir"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Button1, "Expandir Informa��es")
        '
        'Button2
        '
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(84, 413)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(63, 23)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Retrair"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Button2, "Retrair Informa��es")
        '
        'Button3
        '
        Me.Button3.Image = CType(resources.GetObject("Button3.Image"), System.Drawing.Image)
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(252, 57)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(141, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "Limpar CD"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Button3, "Limpar as Informa��es do CD")
        '
        'Button5
        '
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.ImageList = Me.ImageList1
        Me.Button5.Location = New System.Drawing.Point(105, 57)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(141, 23)
        Me.Button5.TabIndex = 3
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Button5, "Para e Ativar os Processamento do CD")
        '
        'Button7
        '
        Me.Button7.Image = CType(resources.GetObject("Button7.Image"), System.Drawing.Image)
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.Location = New System.Drawing.Point(426, 15)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(63, 23)
        Me.Button7.TabIndex = 5
        Me.Button7.Text = "Email"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.Button7, "Retrair Informa��es")
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(2, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(93, 81)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'Button4
        '
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.ImageIndex = 1
        Me.Button4.ImageList = Me.ImageList1
        Me.Button4.Location = New System.Drawing.Point(393, 411)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(99, 24)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Parar Todos"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Button6
        '
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.ImageIndex = 0
        Me.Button6.ImageList = Me.ImageList1
        Me.Button6.Location = New System.Drawing.Point(282, 411)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(99, 24)
        Me.Button6.TabIndex = 4
        Me.Button6.Text = "Ativar Todos"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnDetPedido
        '
        Me.btnDetPedido.Enabled = False
        Me.btnDetPedido.Location = New System.Drawing.Point(399, 57)
        Me.btnDetPedido.Name = "btnDetPedido"
        Me.btnDetPedido.Size = New System.Drawing.Size(96, 23)
        Me.btnDetPedido.TabIndex = 6
        Me.btnDetPedido.Text = "Button8"
        '
        'Timer1
        '
        Me.Timer1.Interval = 300000
        '
        'lblProc
        '
        Me.lblProc.Location = New System.Drawing.Point(156, 417)
        Me.lblProc.Name = "lblProc"
        Me.lblProc.Size = New System.Drawing.Size(117, 18)
        Me.lblProc.TabIndex = 7
        Me.lblProc.Text = "Label1"
        Me.lblProc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(504, 445)
        Me.Controls.Add(Me.lblProc)
        Me.Controls.Add(Me.btnDetPedido)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.trwCD)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.Text = "GERA��O DE PEDIDOS - PAACS E REPRESENTANTES"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private intNode As Integer
    Dim fswPedidos As New FileSystemWatcher

    Delegate Sub GravaErroDelegate(ByVal Erro As String, ByVal TreeNode As Integer)
    Delegate Sub OnChangedDelegate(ByVal source As Object, ByVal e As FileSystemEventArgs)

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If PrevInstance() Then
                MessageBox.Show("J� existe uma instancia do programa em execu��o")
                Exit Sub
            End If

            Dim dtrLojas As DataRow
            Dim i As Integer

            fltrwcd = 0


            '****
            '**** Buscar CDs
            '****
            Dim CMD As New OracleCommand
            CMD.Connection = Ora
            CMD.CommandType = CommandType.StoredProcedure
            CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CON_LOJAS"

            '****
            '**** Parametros
            '****
            CMD.Parameters.Add(New OracleParameter("cCursor", OracleDbType.RefCursor, ParameterDirection.Output))

            '****
            '**** Codigo do Produto..
            '****
            Dim daLojas As New OracleDataAdapter(CMD)
            Dim tblLojas As New DataTable
            daLojas.Fill(tblLojas)

            ReDim arrCDs(tblLojas.Rows.Count)

            ' Suppress repainting the TreeView until all the objects have been created.
            trwCD.BeginUpdate()

            ' Clear the TreeView each time the method is called.
            trwCD.Nodes.Clear()

            ' Add a root TreeNode for each Customer object in the ArrayList.
            intNode = 1

            trwCD.ImageList = ImageList1

            trwCD.SelectedImageIndex = Nothing

            ReDim Preserve arrCDs_Pastas(1)
            ReDim Preserve arrCDs_Ativos(1)
            ReDim Preserve arrCDs_IP(1)

            trwCD.Nodes.Add(New TreeNode("01 - CAMPINAS"))
            trwCD.Nodes.Item(0).ImageIndex = 0
            arrCDs(0) = 1
            arrCDs_Pastas(1) = "CPS"
            arrCDs_Ativos(1) = True
            arrCDs_IP(1) = ""

            i = 1
            For Each dtrLojas In tblLojas.Rows

                ReDim Preserve arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer))
                ReDim Preserve arrCDs_Ativos(CType(dtrLojas("cod_loja"), Integer))
                ReDim Preserve arrCDs_IP(CType(dtrLojas("cod_loja"), Integer))

                trwCD.Nodes.Add(New TreeNode(dtrLojas("cod_loja") & " - " & dtrLojas("NOME_FANTASIA")))

                arrCDs(i) = CType(dtrLojas("cod_loja"), Integer)

                If dtrLojas("ATIVO") = "0" Then
                    trwCD.Nodes.Item(i).ImageIndex = 0
                    arrCDs_Ativos(CType(dtrLojas("cod_loja"), Integer)) = True
                Else
                    trwCD.Nodes.Item(i).ImageIndex = 1
                    trwCD.Nodes.Item(i).SelectedImageIndex = 1
                    trwCD.Nodes.Item(i).ForeColor = System.Drawing.Color.Red
                    arrCDs_Ativos(CType(dtrLojas("cod_loja"), Integer)) = False
                End If

                arrCDs_IP(CType(dtrLojas("cod_loja"), Integer)) = dtrLojas("IP")

                Select Case CType(dtrLojas("cod_loja"), Integer)
                    Case 2
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "SP"
                    Case 4
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "GO"
                    Case 5
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "FOR"
                    Case 7
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "BH"
                    Case 8
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "POA"
                    Case 9
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "DF"
                    Case 10
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "RJ"
                    Case 11
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "BA"
                    Case 12
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "RE"
                    Case 13
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "MS"
                    Case 14
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "CUR"
                    Case Else
                        arrCDs_Pastas(CType(dtrLojas("cod_loja"), Integer)) = "CD" & dtrLojas("cod_loja").ToString
                End Select

                'If i = 0 Then
                '    arrCDs_Pastas(i) = "CPS"
                'ElseIf i = 1 Then
                '    arrCDs_Pastas(i) = "SP"
                'ElseIf i = 2 Then
                '    arrCDs_Pastas(i) = "GO"
                'ElseIf i = 3 Then
                '    arrCDs_Pastas(i) = "FOR"
                'ElseIf i = 4 Then
                '    arrCDs_Pastas(i) = "BH"
                'ElseIf i = 5 Then
                '    arrCDs_Pastas(i) = "POA"
                'ElseIf i = 7 Then
                '    arrCDs_Pastas(i) = "RJ"
                'ElseIf i = 8 Then
                '    arrCDs_Pastas(i) = "BA"
                'ElseIf i = 9 Then
                '    arrCDs_Pastas(i) = "RE"
                'ElseIf i = 10 Then
                '    arrCDs_Pastas(i) = "MS"
                'ElseIf i = 11 Then
                '    arrCDs_Pastas(i) = "CUR"
                'ElseIf i >= 12 Then
                '    arrCDs_Pastas(i) = "CD" & dtrLojas("cod_loja").ToString
                'End If

                i = i + 1

            Next dtrLojas


            'For i = 0 To UBound(arrCDs_Ativos)
            '   arrCDs_Ativos(i) = True
            'Next

            'MsgBox("teste 1")

            ' Begin repainting the TreeView.
            trwCD.EndUpdate()

            '****
            '**** Buscar dados iniciais
            '****
            CMD.Parameters.Clear()
            CMD.Connection = Ora
            CMD.CommandType = CommandType.StoredProcedure
            CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_DATA"

            '****
            '**** Parametros
            '****
            'CMD.Parameters.Remove("PM_CURSOR")
            CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

            Dim daDados As New OracleDataAdapter(CMD)
            Dim tblDados As New DataTable
            daDados.Fill(tblDados)

            Data_Faturamento = CDate(tblDados.Rows(0)("DT_FATURAMENTO"))
            data_real = CDate(tblDados.Rows(0)("DT_REAL"))
            FILIAL_PED = tblDados.Rows(0)("FILIAL")
            uf_origem = tblDados.Rows(0)("COD_UF")
            'FIL200.DEPOSITO_DEFAULT = tblDados.Rows(0)("deposito_default")

            ApagaArquivos()

            Config_FSW()

            'FIL200.LOG("A", 0, 0, 0, 0, 0, 0)

            trwCD.SelectedNode = Nothing

            Onstart()

            Timer1.Start()

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Catch ex As OracleException
            MessageBox.Show(ex.ToString)
        End Try

    End Sub



    Public Sub OnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)

        Dim strPath As String
        Dim strCD As String
        Dim strArquivo As String
        Dim TEXTO_ERRO As String
        Dim TreeNode As Integer

        Dim FIL200 As New clsFIL200

        Dim blErro As Boolean

        'Dim onTreeViewElement As ThreadEventHandler

        ' Specify what is done when a file is changed, created, or deleted.

        'MessageBox.Show("TESTE FSW - " & CStr(AppDomain.GetCurrentThreadId()) & " - " & e.FullPath)

        strPath = e.Name

        If InStr(strPath, "\") > 0 Then

            strCD = UCase(Mid(strPath, 1, InStr(strPath, "\") - 1))
            strArquivo = Mid(strPath, InStr(strPath, "\") + 1)

            ' MessageBox.Show(strCD & " - " & strArquivo)

            FIL200.DIR_ENTRADA = e.FullPath.Replace(strArquivo, "")
            FIL200.DIR_EN_PEDIDO = e.FullPath.Replace(strArquivo, "")

            FIL200.DIR_SA_PEDIDO = "P:\OUT\"   ''FIL200.DIR_EN_PEDIDO.Replace("IN", "OUT")
            FIL200.DIR_SAIDA = "P:\OUT\" ''FIL200.DIR_ENTRADA.Replace("IN", "OUT")

            FIL200.NOME_ARQUIVO = UCase(strArquivo)

            Dim i As Integer
            For i = 0 To UBound(arrCDs)
                If arrCDs_Pastas(arrCDs(i)) = strCD Then
                    Exit For
                End If
            Next

            If strCD = "CPS" Then  ''CAMPINAS
                FIL200.DEPOSITO_DESTINO = "C"
                blErro = FIL200.DELETA_LOG
                TreeNode = i
            Else
                TreeNode = i
                FIL200.DEPOSITO_DESTINO = arrCDs(i)
            End If

            If arrCDs_Ativos(arrCDs(TreeNode)) = True Then

                'TEXTO_ERRO = UCase(strArquivo) & " - " & FIL200.Processamento()

                'If InStr(TEXTO_ERRO, " > ") > 0 Then
                '    TEXTO_ERRO = Mid(TEXTO_ERRO, 1, InStr(TEXTO_ERRO, " > ") + 3)
                'End If

                'trwCD.Invoke(New GravaErroDelegate(AddressOf GravaErro), New Object() {TEXTO_ERRO, TreeNode})

                If arrCDs_IP(arrCDs(TreeNode)) = "" Then
                    TEXTO_ERRO = UCase(strArquivo) & " - " & FIL200.Processamento()
                Else

                    reply = clsPing.Ping(arrCDs_IP(arrCDs(TreeNode)))

                    If reply.Status = ICMPClass.ICMPStatusEnum.Success Then

                        TEXTO_ERRO = UCase(strArquivo) & " - " & FIL200.Processamento()

                    Else

                        TEXTO_ERRO = UCase(strArquivo) & " - Erro de comunica��o"

                    End If

                End If

                trwCD.Invoke(New GravaErroDelegate(AddressOf GravaErro), New Object() {TEXTO_ERRO, TreeNode})

            End If

        End If

        FIL200 = Nothing

    End Sub


    Public Sub GravaErro(ByVal Erro As String, ByVal Indice As Integer)
        Try

            Dim trwNo As TreeNode

            If InStr(Erro, "SUCESSO") > 0 Then


                trwCD.Nodes(Indice).Nodes.Add(New TreeNode(Erro.Replace(Chr(13), vbCrLf)))
                trwCD.Nodes(Indice).LastNode.ImageIndex = 4
                trwCD.Nodes(Indice).LastNode.SelectedImageIndex = 4
                'trwCD.SelectedNode = Nothing

                TotProcess += 1
            ElseIf InStr(Erro, "HDR") > 0 Then
            Else
                trwCD.Nodes(Indice).Nodes.Add(New TreeNode(Erro.Replace(Chr(13), vbCrLf)))
                trwCD.Nodes(Indice).LastNode.ImageIndex = 2
                trwCD.Nodes(Indice).LastNode.SelectedImageIndex = 2
                trwCD.Nodes(Indice).LastNode.ForeColor = System.Drawing.Color.Red
                'trwCD.SelectedNode = Nothing

                trwCD.Nodes(Indice).ImageIndex = 3
                trwCD.Nodes(Indice).SelectedImageIndex = 3

                If InStr(Erro, "comunica��o") > 0 Then
                    'trwCD.Nodes(Indice).ImageIndex = 1
                    'trwCD.Nodes(Indice).SelectedImageIndex = 1
                    'trwCD.Nodes(Indice).ForeColor = System.Drawing.Color.Red
                    'arrCDs_Ativos(arrCDs(Indice)) = False
                End If

            End If

            'TotProcess += 1
            lblProc.Text = TotProcess & " Pedidos OK"

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        trwCD.ExpandAll()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        trwCD.CollapseAll()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        trwCD.SelectedNode.Nodes.Clear()

        trwCD.SelectedNode.ImageIndex = 0
        trwCD.SelectedNode.SelectedImageIndex = 0
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim dlgSTOP As DialogResult
        Dim txtNode As String

        Dim trwNo As New TreeNode

        trwNo = trwCD.Nodes(trwIndice)

        txtNode = Mid(trwNo.Text, 1, 2)

        If IsNumeric(txtNode) Then

            If trwNo.ImageIndex = 0 Or trwNo.ImageIndex = 3 Then
                dlgSTOP = MessageBox.Show("Deseja parar o servi�o para o CD " & trwNo.Text, "Aten��o", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
            Else
                dlgSTOP = MessageBox.Show("Deseja reativar o servi�o para o CD " & trwNo.Text, "Aten��o", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation)
            End If

            If dlgSTOP = DialogResult.OK Then
                If trwNo.ImageIndex = 0 Or trwNo.ImageIndex = 3 Then
                    trwNo.ImageIndex = 1
                    trwNo.SelectedImageIndex = 1
                    trwNo.ForeColor = System.Drawing.Color.Red

                    arrCDs_Ativos(arrCDs(trwNo.Index)) = False

                    Ativar_CD(CInt(trwNo.Text.Substring(0, InStr(trwNo.Text, " - ") - 1)), True)

                Else
                    trwNo.ImageIndex = 0
                    trwNo.SelectedImageIndex = 0
                    trwNo.ForeColor = System.Drawing.Color.Black

                    arrCDs_Ativos(arrCDs(trwNo.Index)) = True

                    Ativar_CD(CInt(trwNo.Text.Substring(0, InStr(trwNo.Text, " - ") - 1)), False)

                    ProcessaArquivos(arrCDs(trwNo.Index))

                    'Dim strARQUIVO As String
                    'Dim strNOME As String
                    'Dim strPATH As String

                    'For Each strARQUIVO In Directory.GetFiles("P:\IN\" & arrCDs_Pastas(trwIndice), "*.TXT")

                    '    strNOME = Mid(strARQUIVO, InStrRev(strARQUIVO, arrCDs_Pastas(trwIndice) & "\"))
                    '    strPATH = Mid(strARQUIVO, InStr(strARQUIVO, arrCDs_Pastas(trwIndice) & "\"))

                    '    Dim objE As New FileSystemEventArgs(WatcherChangeTypes.Created, "P:\IN\", strNOME)
                    '    Invoke(New OnChangedDelegate(AddressOf OnChanged), New Object() {Nothing, objE})
                    '    objE = Nothing

                    'Next

                End If

                If trwNo.ImageIndex = 0 Then
                    Button5.ImageIndex = 1
                    Button5.Text = "Parar " & trwNo.Text
                Else
                    Button5.ImageIndex = 0
                    Button5.Text = "Ativar" & trwNo.Text
                End If

            End If

        End If

    End Sub

    Private Sub trwCD_BeforeSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles trwCD.BeforeSelect

        Dim txtNode As String
        txtNode = Mid(e.Node.Text, 1, 2)

        If IsNumeric(txtNode) Then

            trwIndice = e.Node.Index

            If e.Node.ImageIndex = 0 Then
                Button5.ImageIndex = 1
                Button5.Text = "Parar " & e.Node.Text
            Else
                Button5.ImageIndex = 0
                Button5.Text = "Ativar" & e.Node.Text
            End If

            Button3.Text = "Limpar " & e.Node.Text
            btnDetPedido.Text = ""
            btnDetPedido.Enabled = False

        Else

            btnDetPedido.Text = Mid(e.Node.Text, 1, InStr(e.Node.Text, ".TXT") + 4)
            strPedidoForm = Mid(e.Node.Text, 4, InStr(e.Node.Text, ".TXT") - 4)
            strNodeForm = e.Node.Parent.Text
            nrNode = e.Node.Parent.Index
            btnDetPedido.Enabled = True

        End If

    End Sub


    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim trwNo As TreeNode

        For Each trwNo In trwCD.Nodes

            trwNo.ImageIndex = 1
            trwNo.SelectedImageIndex = 1
            trwNo.ForeColor = System.Drawing.Color.Red

            arrCDs_Ativos(arrCDs(trwNo.Index)) = False

        Next

        Button4.Focus()

    End Sub


    ''The error event handler
    Private Sub OnError(ByVal source As Object, ByVal e As ErrorEventArgs)
        'private static void WatcherError(object source, ErrorEventArgs e)

        Dim watchException As Exception = e.GetException()

        'MessageBox.Show("A FileSystemWatcher error has occurred: " & watchException.Message)

        '' We need to create new version of the object because the
        '' old one is now corrupted
        fswPedidos = New FileSystemWatcher

        While Not fswPedidos.EnableRaisingEvents

            Try

                '// This will throw an error at the
                '// watcher.NotifyFilter line if it can't get the path.
                Config_FSW()
                'Console.WriteLine("I'm Back!!")

            Catch ex As Exception

                '// Sleep for a bit; otherwise, it takes a bit of
                '// processor time
                System.Threading.Thread.Sleep(5000)

            End Try

        End While
    End Sub

    Sub Config_FSW()

        ' Begin watching.
        'MsgBox(fswPedidos.InternalBufferSize)
        fswPedidos.NotifyFilter = NotifyFilters.FileName
        fswPedidos.Filter = "*.TXT"
        'fswPedidos.Path = "P:\IN"
        fswPedidos.Path = "C:\IN"
        fswPedidos.IncludeSubdirectories = True
        fswPedidos.InternalBufferSize = 12288


        AddHandler fswPedidos.Created, New System.IO.FileSystemEventHandler(AddressOf OnChanged2)
        AddHandler fswPedidos.Error, New System.IO.ErrorEventHandler(AddressOf OnError)

        'AddHandler fswPedidos.Created, AddressOf OnChanged
        'AddHandler fswPedidos.Error, AddressOf OnError

        fswPedidos.EnableRaisingEvents = True

    End Sub


    Public Sub OnChanged2(ByVal source As Object, ByVal e As FileSystemEventArgs)

        Dim td As New OnChangedDelegate(AddressOf OnChanged)
        td.BeginInvoke(source, e, Nothing, Nothing)
        ''td.EndInvoke(Nothing)

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim trwNo As TreeNode

        For Each trwNo In trwCD.Nodes

            trwNo.ImageIndex = 0
            trwNo.SelectedImageIndex = 0
            trwNo.ForeColor = System.Drawing.Color.Black

            arrCDs_Ativos(arrCDs(trwNo.Index)) = True

        Next

        Button4.Focus()
    End Sub



    Private Sub ProcessaArquivos(ByVal indice As Integer)

        Dim strARQUIVO As String
        Dim strNOME As String
        Dim strPATH As String
        Dim dtArquivo As Date

        For Each strARQUIVO In Directory.GetFiles("P:\IN\" & arrCDs_Pastas(indice), "*.TXT")

            dtArquivo = IO.File.GetCreationTime(strARQUIVO)
            strNOME = Mid(strARQUIVO, InStrRev(strARQUIVO, arrCDs_Pastas(indice) & "\"))
            strPATH = Mid(strARQUIVO, InStr(strARQUIVO, arrCDs_Pastas(indice) & "\"))
            Dim source As Object

            If flTimer = True Then

                If dtArquivo < DateAdd(DateInterval.Minute, -5, Now()) Then

                    Dim objE As New FileSystemEventArgs(WatcherChangeTypes.Created, "P:\IN\", strNOME)
                    Dim td As New OnChangedDelegate(AddressOf OnChanged)
                    td.BeginInvoke(source, objE, Nothing, Nothing)
                    'Invoke(New OnChangedDelegate(AddressOf OnChanged), New Object() {Nothing, objE})
                    objE = Nothing

                End If
            Else

                Dim objE As New FileSystemEventArgs(WatcherChangeTypes.Created, "P:\IN\", strNOME)
                Dim td As New OnChangedDelegate(AddressOf OnChanged)
                td.BeginInvoke(source, objE, Nothing, Nothing)
                'Invoke(New OnChangedDelegate(AddressOf OnChanged), New Object() {Nothing, objE})
                objE = Nothing

            End If


        Next

    End Sub

    Private Sub ApagaArquivos()

        Dim strARQUIVO As String
        Dim strNOME As String
        Dim strPATH As String
        Dim dtArquivo As Date

        Dim indice As Integer

        For indice = 0 To UBound(arrCDs)

            For Each strARQUIVO In Directory.GetFiles("P:\IN\" & arrCDs_Pastas(arrCDs(indice)), "*.TXT.ERRO")

                dtArquivo = IO.File.GetLastWriteTime(strARQUIVO)
                'strNOME = Mid(strARQUIVO, InStrRev(strARQUIVO, arrCDs_Pastas(indice) & "\"))
                'strPATH = Mid(strARQUIVO, InStr(strARQUIVO, arrCDs_Pastas(indice) & "\"))

                If dtArquivo.Date < Now.Date Then

                    Try
                        Kill(strARQUIVO.Replace(".TXT.", ".HDR."))
                    Catch ex As Exception

                    End Try

                    Try
                        Kill(strARQUIVO)
                    Catch ex As Exception

                    End Try

                End If

            Next
        Next

    End Sub


    Private Sub Onstart()

        Dim i As Integer

        For i = 0 To UBound(arrCDs)

            If arrCDs_Ativos(arrCDs(i)) = True Then

                'If i > 15 Then

                ProcessaArquivos(arrCDs(i))

                'End If

            End If

        Next

    End Sub

    ' Visual Basic .NET
    Function PrevInstance() As Boolean
        If UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Sub Ativar_CD(ByVal COD_CD As Int64, ByVal ativo As Boolean)

        Dim pATIVO As String
        If ativo = True Then
            pATIVO = "1"
        Else
            pATIVO = "0"
        End If

        Ora.Open()

        Dim CMD As New OracleCommand
        CMD.Parameters.Clear()
        CMD.Connection = Ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_ATIVA_CD"

        '****
        '**** Parametros
        '****
        CMD.Parameters.Add(New OracleParameter("PM_CODCD", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_CODCD").Value = COD_CD.ToString
        CMD.Parameters.Add(New OracleParameter("PM_ATIVO", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_ATIVO").Value = pATIVO

        CMD.ExecuteNonQuery()

        Ora.Close()

    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click

        Dim frmEmail As New Email
        frmEmail.Show()

    End Sub

    Private Sub btnDetPedido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetPedido.Click

        Dim frmPedido As New DetPedidos
        frmPedido.DEPOSITO = strNodeForm
        frmPedido.PEDIDO = strPedidoForm
        frmPedido.Node = nrNode
        'frmPedido.FIL200 = FIL200
        frmPedido.frmForm1 = Me
        frmPedido.ShowDialog()

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        flTimer = True
        Onstart()
        flTimer = False
        StartCD()
    End Sub

    Private Sub StartCD()

        '****
        '**** Buscar CDs
        '****
        Dim CMD As New OracleCommand
        CMD.Connection = Ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CON_LOJAS"

        '****
        '**** Parametros
        '****
        CMD.Parameters.Add(New OracleParameter("cCursor", OracleDbType.RefCursor, ParameterDirection.Output))

        '****
        '**** Codigo do Produto..
        '****
        Dim daLojas As New OracleDataAdapter(CMD)
        Dim tblLojas As New DataTable
        daLojas.Fill(tblLojas)

        Dim dtrLojas As DataRow
        Dim i As Int32
        Dim trwNo As New TreeNode

        i = 1
        For Each dtrLojas In tblLojas.Rows

            If dtrLojas("ATIVO") = "0" And arrCDs_Ativos(arrCDs(i)) = False Then

                trwNo = trwCD.Nodes(i)
                trwNo.ImageIndex = 0
                trwNo.SelectedImageIndex = 0
                trwNo.ForeColor = System.Drawing.Color.Black

                arrCDs_Ativos(i) = True
                ProcessaArquivos(arrCDs(i))
            End If

            i = i + 1
        Next dtrLojas

    End Sub
End Class
