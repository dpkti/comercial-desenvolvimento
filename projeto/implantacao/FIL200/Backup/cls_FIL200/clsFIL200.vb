Imports System.IO
Imports System.IO.File
Imports System.Web
Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types

Public Class clsFIL200

    Private strDIR_SA_PEDIDO As String
    Private strDIR_SAIDA As String
    Private strDIR_ENTRADA As String
    Private strDIR_EN_PEDIDO As String
    Private strDEPOSITO_DESTINO As String
    Private strNOME_ARQUIVO As String
    Private strDEPOSITO_DEFAULT As String

    Private strNUM_PEDIDO As String

    Private Fl_Complemento As String
    Private loja_ped As Integer
    Private Pedido_Def As Integer
    Private Seq_Def As Integer
    Private tipo_pedido As Integer
    Private COD_ERRO As Integer
    'Private TEXTO_ERRO As String
    Private LOJA_DESTINO As Integer
    Private lngcliente As Integer

    Dim Arquivo_Log As String

    Dim MSGPEDGO As String

    Dim NumPed As Integer
    Dim SSeq As Integer

    Dim tmp_to As String
    Dim tmp_from As String
    Dim tmp_date As String
    Dim tmp_subject As String

    Dim EmailErro As String
    Dim EmailErro2 As String

    Dim flemail As Boolean
    Dim flPackage As Boolean = False


    'Dim lngNum_Pedido As Integer
    'Dim lngSeq_Pedido As Integer

    Private ora As New OracleConnection(ConnectionString)

    Public Sub New()

        Dim Config As New System.Configuration.AppSettingsReader

        ora.Open()

    End Sub

    Public Property DEPOSITO_DEFAULT() As String
        Get
            Return strDEPOSITO_DEFAULT
        End Get
        Set(ByVal Value As String)
            strDEPOSITO_DEFAULT = Value
        End Set
    End Property

    Public Property DIR_SAIDA() As String
        Get
            Return strDIR_SAIDA
        End Get
        Set(ByVal Value As String)
            strDIR_SAIDA = Value
        End Set
    End Property

    Public Property DIR_SA_PEDIDO() As String
        Get
            Return strDIR_SA_PEDIDO
        End Get
        Set(ByVal Value As String)
            strDIR_SA_PEDIDO = Value
        End Set
    End Property

    Public Property DIR_ENTRADA() As String
        Get
            Return strDIR_ENTRADA
        End Get
        Set(ByVal Value As String)
            strDIR_ENTRADA = Value
        End Set
    End Property

    Public Property DIR_EN_PEDIDO() As String
        Get
            Return strDIR_EN_PEDIDO
        End Get
        Set(ByVal Value As String)
            strDIR_EN_PEDIDO = Value
        End Set
    End Property

    Public Property DEPOSITO_DESTINO() As String
        Get
            Return strDEPOSITO_DESTINO
        End Get
        Set(ByVal Value As String)
            strDEPOSITO_DESTINO = Value
        End Set
    End Property

    Public Property NOME_ARQUIVO() As String
        Get
            Return strNOME_ARQUIVO
        End Get
        Set(ByVal Value As String)
            strNOME_ARQUIVO = Value
        End Set
    End Property

    Private Function ConnectionString() As String
        Dim Config As New System.Configuration.AppSettingsReader
        Return CType(Config.GetValue("strConnection", GetType(String)), String)
    End Function


    Public Function Processamento() As String
        Try

            '-- Inicia Processamento de Pedidos

            'Dim STRPEDIDO As String
            Dim STRNUM_PEDIDO As String
            Dim strArqHDR As String
            Dim strFullpath As String
            Dim strPEDIDOCON As String
            Dim TEXTO_ERRO As String

            Dim txtReader As IO.StreamReader
            Dim linhaTexto As String
            Dim tmp_i As String
            Dim tmp_conteudo As String
            Dim DEP As String


            STRNUM_PEDIDO = Mid(strNOME_ARQUIVO, 1, InStr(strNOME_ARQUIVO, ".") - 1)

            strArqHDR = STRNUM_PEDIDO & ".HDR"

            strFullpath = strDIR_ENTRADA & strArqHDR

            '------------------------------------------------------------------------
            '-- L� Arquivo de  Mensagem para Identificar Origen e N�mero do Pedido --
            '-----------------------------------------------------------------------*
            Dim sleep As Integer = 0

            While Not IO.File.Exists(strFullpath)
                If sleep > 10000 Then

                    Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")

                    Return "999 - ARQUIVO .HDR N�O ENCONTRADO"
                End If
                sleep += 1
            End While

            'Comentador por Eduardo - 24/09/2008
            'If Not File.Exists("C:\pedidos\" & strArqHDR) Then
            'Copy(strFullpath, "C:\pedidos\" & strArqHDR)
            'End If

            File.SetAttributes(strFullpath, FileAttributes.Normal)

            txtReader = New IO.StreamReader(strFullpath)
            linhaTexto = txtReader.ReadLine

            While linhaTexto <> Nothing

                tmp_i = InStr(linhaTexto, "=")
                If tmp_i <> 0 Then
                    Select Case UCase(Mid(linhaTexto, 1, tmp_i - 1))
                        Case "TO        " : tmp_to = Mid(linhaTexto, tmp_i + 1)
                        Case "FROM      " : tmp_from = Mid(linhaTexto, tmp_i + 1)
                        Case "GERACAO   " : tmp_date = Mid(linhaTexto, tmp_i + 1)
                        Case "DESCRICAO " : tmp_subject = Trim(Mid(linhaTexto, tmp_i + 1))
                        Case Else : tmp_conteudo = tmp_conteudo & linhaTexto & vbCrLf
                    End Select
                Else
                    tmp_conteudo = tmp_conteudo & linhaTexto & vbCrLf
                End If

                linhaTexto = txtReader.ReadLine

            End While

            txtReader.Close()

            '-----------------------------------------
            '-- Leitura dos Pedidos - Arquivo *.TXT --
            '-----------------------------------------
            strFullpath = strDIR_ENTRADA & strNOME_ARQUIVO

            'If IO.File.Exists(strFullpath) Then

            '    txtReader = New IO.StreamReader(strFullpath)
            '    linhaTexto = txtReader.ReadLine

            'End If

            'STRPEDIDO = Dir(strDIR_EN_PEDIDO & "*.TXT")

            '------------------------------------
            '-- Inicia Processamento do Pedido --
            '------------------------------------
            If Len(strDIR_SAIDA & strNOME_ARQUIVO) Then
                TEXTO_ERRO = PROCESSA_PEDIDO()
                If TEXTO_ERRO = "0" Then
                    If strDEPOSITO_DESTINO <> "C" And loja_ped <> 1 Then
                        '----------------------------------------------------------------
                        '-- Se Pedido for para CD REMOTO e n�o tiver dado erro atualiza
                        '-- PEDNOTA_VENDA de CAMPINAS para CD CAMPINAS poder consultar
                        '----------------------------------------------------------------
                        If Fl_Complemento <> "S" Then
                            Call GRAVA_PEDNOTA(loja_ped.ToString.PadLeft(2, "0"), Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 2), Mid(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1, 1), tipo_pedido)
                        End If
                    Else 'Gerar Retorno do Complemento

                        Dim CMD As New OracleCommand
                        CMD.Parameters.Clear()
                        CMD.Connection = ora
                        CMD.CommandType = CommandType.StoredProcedure
                        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_BUSCA_COMPLEMENTO"

                        CMD.Parameters.Add(New OracleParameter("vCursor", OracleDbType.RefCursor, ParameterDirection.Output))

                        DEP = "DEP" & loja_ped.ToString.PadLeft(2, "0")
                        CMD.Parameters.Add(New OracleParameter("DEP", OracleDbType.Varchar2, ParameterDirection.Input))
                        CMD.Parameters("DEP").Value = DEP

                        CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Varchar2, ParameterDirection.Input))
                        CMD.Parameters("PM_LOJA").Value = loja_ped

                        CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Varchar2, ParameterDirection.Input))
                        CMD.Parameters("PM_SEQ").Value = Seq_Def

                        CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Varchar2, ParameterDirection.Input))
                        CMD.Parameters("PM_NUM").Value = Pedido_Def

                        Dim daPedido As New OracleDataAdapter(CMD)
                        Dim tblPedido As New DataTable
                        daPedido.Fill(tblPedido)

                        If tblPedido.Rows.Count = 0 Then
                            Fl_Complemento = "N"
                        Else
                            Fl_Complemento = "S"

                            strPEDIDOCON = "PED" & tblPedido.Rows(0)("num_pedido_compl") & tblPedido.Rows(0)("seq_pedido_compl") & ".CON"
                            Call Cons_Pedido(1, strDIR_SA_PEDIDO & strNOME_ARQUIVO, tblPedido.Rows(0)("cod_loja_compl"), tblPedido.Rows(0)("num_pedido_compl"), tblPedido.Rows(0)("seq_pedido_compl"))

                            'Arquivo de Cabe�alho
                            Dim oArquivo As System.IO.File
                            Dim oEscrever As System.IO.StreamWriter

                            oEscrever = oArquivo.CreateText(strDIR_SAIDA & strArqHDR)

                            oEscrever.WriteLine("GERACAO   =" & tmp_date)

                            oEscrever.WriteLine("GERACAO   =" & tmp_date)
                            oEscrever.WriteLine("TO        =" & tmp_from)
                            oEscrever.WriteLine("FROM      =" & tmp_to)
                            oEscrever.WriteLine("DESCRICAO =" & "#CON" & Mid(Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1), 4))
                            oEscrever.WriteLine("FILE      =" & strNOME_ARQUIVO)

                            oEscrever.Close()

                        End If
                    End If
                Else
                    Return TEXTO_ERRO
                End If

                COD_ERRO = 0
            End If

            Dim msgarq As String

            If IO.File.Exists(strFullpath) Then
                Try
                    Kill(strDIR_EN_PEDIDO & strNOME_ARQUIVO)
                Catch ex As Exception
                    msgarq = " - TXT n�o excluido"
                End Try
            End If
            If IO.File.Exists(strDIR_ENTRADA & strArqHDR) Then
                Try
                    Kill(strDIR_ENTRADA & strArqHDR)
                Catch ex As Exception
                    msgarq = msgarq & " - HDR n�o excluido"
                End Try
            End If

            Return "GRAVADO COM SUCESSO (" & Pedido_Def & "-" & Seq_Def & ")" & msgarq

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            Return ex.ToString

        End Try
    End Function


    Public Function DELETA_LOG() As Boolean
        Dim exdescr As String
        Try
            Dim CD, Arquivo_Log, Data_Log, Hora_Log

            Data_Log = Now.Date.AddDays(-5).Year.ToString & Now.Date.AddDays(-5).Month.ToString & Now.Date.AddDays(-5).Day.ToString
            Arquivo_Log = "LOG" & Data_Log & "*.TXT"

            Kill("c:\log\" & Arquivo_Log)

            Return True
        Catch ex As Exception
            exdescr = ex.Message & " - " & ex.Source & " - " & ex.ToString & " - "
            Return False
        End Try
    End Function

    Private Function PROCESSA_PEDIDO() As String

        Dim oArquivo As System.IO.File
        Dim oEscrever As System.IO.StreamWriter
        Dim strlinha As String
        Dim txtReader As IO.StreamReader

        Dim tipo_retorno As String = "E"

        Try

            '-- GRAVA ITENS E CABE�ALHO NAS TABELAS AUXILIARES DO CD DO PEDIDO
            '-- VENDAS.ITPED_VENDA
            '-- VENDAS.PED_VENDA
            '-- EXECUTA PROCEDURE PR_GERPEDVDA PARA GRAVAR PEDIDO NAS TABELAS
            '-- DEFINITIVAS PEDNOTA_VENDA / ITPEDNOTA_VENDA

            Dim path_drv

            Dim strFullpath As String
            Dim SQL As String

            Dim TEXTO_ERRO As String

            Dim lngNum_Pedido As Integer
            Dim lngSeq_Pedido As Integer
            Dim STRPEDIDO As String

            Dim xx As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly

            path_drv = Left(xx.Location, 2)
            'strPath = "H:\PGMS\"

            strFullpath = strDIR_ENTRADA & strNOME_ARQUIVO

            'Comentador por Eduardo - 25/09/2008
            'If Not File.Exists("C:\pedidos\" & strNOME_ARQUIVO) Then
            'Copy(strDIR_ENTRADA & strNOME_ARQUIVO, "C:\pedidos\" & strNOME_ARQUIVO)
            'System.Threading.Thread.Sleep(1000)
            'End If

            File.SetAttributes(strFullpath, FileAttributes.Normal)

            txtReader = New IO.StreamReader(strFullpath, System.Text.Encoding.Default, True)

            strlinha = txtReader.ReadLine

            Dim CMD As New OracleCommand

            If strlinha <> Nothing Then

                loja_ped = Mid(strlinha, 2, 2)

                Del_PEDVENDA(loja_ped, Mid(strNOME_ARQUIVO, 4, InStr(strNOME_ARQUIVO, ".") - 5), Mid(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1, 1))

            End If

            While strlinha <> Nothing

                If Mid(strlinha, 1, 1) = "I" Then
                    TEXTO_ERRO = GRAVA_ITEM(strlinha, txtReader)
                    If TEXTO_ERRO <> "0" Then
                        Return TEXTO_ERRO
                    End If

                    strlinha = ""

                Else  '-- INCLUS�O DO "C"ABE�ALHO DO PEDIDO
                    TEXTO_ERRO = GRAVA_CABEC(strlinha, txtReader)
                    If TEXTO_ERRO = "0" Then  '-- DISPARA PROCEDURE NO ORACLE       --'

                        If LOJA_DESTINO <> 1 Then
                            SQL = "DEP" & LOJA_DESTINO.ToString.PadLeft(2, "0") & ".PR_GERPEDVDA"
                        Else
                            SQL = "PRODUCAO.PR_GERPEDVDA"
                        End If

                        'Dim CMD As New OracleCommand
                        CMD.Parameters.Clear()
                        CMD.Connection = ora
                        CMD.CommandType = CommandType.StoredProcedure
                        CMD.CommandText = SQL ''"PRODUCAO.DEP" & Format(LOJA_DESTINO, "00") & ".PR_GERPEDVDA"

                        CMD.Parameters.Add(New OracleParameter("P_COD_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                        CMD.Parameters("P_COD_LOJA").Value = Mid(strlinha, 2, 2)

                        CMD.Parameters.Add(New OracleParameter("P_NUM_PENDENTE", OracleDbType.Decimal, ParameterDirection.Input))
                        CMD.Parameters("P_NUM_PENDENTE").Value = Mid(strlinha, 4, 9)

                        CMD.Parameters.Add(New OracleParameter("P_SEQ_PENDENTE", OracleDbType.Decimal, ParameterDirection.Input))
                        CMD.Parameters("P_SEQ_PENDENTE").Value = Mid(strlinha, 13, 1)

                        CMD.Parameters.Add(New OracleParameter("P_FL_TIPO_CHAVE", OracleDbType.Decimal, ParameterDirection.Input))
                        CMD.Parameters("P_FL_TIPO_CHAVE").Value = Mid(strlinha, 14, 1)

                        If LOJA_DESTINO <> 1 Then
                            CMD.Parameters.Add(New OracleParameter("codcli", OracleDbType.Decimal, ParameterDirection.Input))
                            CMD.Parameters("codcli").Value = lngcliente
                        Else
                            CMD.Parameters.Add(New OracleParameter("P_COD_LOJA_OUT", OracleDbType.Int32, ParameterDirection.Output))
                            'CMD.Parameters("P_COD_LOJA_OUT").Value = 0

                            CMD.Parameters.Add(New OracleParameter("P_NUM_PEDIDO", OracleDbType.Int32, ParameterDirection.Output))
                            'CMD.Parameters("P_NUM_PEDIDO").Value = 0

                            CMD.Parameters.Add(New OracleParameter("P_SEQ_PEDIDO", OracleDbType.Int32, ParameterDirection.Output))
                            'CMD.Parameters("P_SEQ_PEDIDO").Value = 0

                            CMD.Parameters.Add(New OracleParameter("P_DT_DIGITACAO", OracleDbType.Date, ParameterDirection.Output))
                            'CMD.Parameters("P_DT_DIGITACAO").Value = ""

                            CMD.Parameters.Add(New OracleParameter("P_DT_PEDIDO", OracleDbType.Date, ParameterDirection.Output))
                            'CMD.Parameters("P_DT_PEDIDO").Value = ""

                            'CMD.Parameters.Add(New OracleParameter("P_CODERRO", OracleDbType.Int32, ParameterDirection.Output))
                            'CMD.Parameters("P_CODERRO").Value = 0

                            'CMD.Parameters.Add(New OracleParameter("P_TXTERRO", OracleDbType.Varchar2, ParameterDirection.Output))
                            'CMD.Parameters("P_TXTERRO").Size = 500
                            'CMD.Parameters("P_TXTERRO").Value = ""
                        End If

                        Try
                            CMD.ExecuteNonQuery()
                        Catch ex As OracleException
                            COD_ERRO = ex.Number
                            TEXTO_ERRO = TrataErro_GERPEDVDA(ex.Message)

                            txtReader.Close()

                            If flPackage = True Then

                            ElseIf flemail = True Then
                                If IO.File.Exists(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO") Then
                                    Kill(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                                End If
                                If IO.File.Exists(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO") Then
                                    Kill(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                                End If

                                Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                                Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                            Else
                                Kill(strDIR_ENTRADA & strNOME_ARQUIVO)
                                Kill(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR")
                            End If

                            Call RETORNO_ERRO(Mid(strlinha, 2, 2), Mid(strlinha, 4, 9), Mid(strlinha, 13, 1), Mid(strlinha, 14, 1), COD_ERRO, TEXTO_ERRO)

                            Return TEXTO_ERRO & "***4"

                        Catch ex As Exception

                            COD_ERRO = ""
                            TEXTO_ERRO = ex.Message

                            txtReader.Close()

                            If IO.File.Exists(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO") Then
                                Kill(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                            End If
                            If IO.File.Exists(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO") Then
                                Kill(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                            End If

                            Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                            Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")

                            Call RETORNO_ERRO(Mid(strlinha, 2, 2), Mid(strlinha, 4, 9), Mid(strlinha, 13, 1), Mid(strlinha, 14, 1), COD_ERRO, TEXTO_ERRO)

                            Return TEXTO_ERRO & "***5"

                        End Try


                        'If CMD.Parameters("P_CODERRO").Value <> 0 Then

                        '    COD_ERRO = CMD.Parameters("P_CODERRO").Value
                        '    TEXTO_ERRO = CMD.Parameters("P_TXTERRO").Value.ToString

                        '    txtReader.Close()
                        '    Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                        '    Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")

                        '    Call RETORNO_ERRO(Format(Mid(strlinha, 2, 2), "00"), Mid(strlinha, 4, 9), Mid(strlinha, 13, 1), Mid(strlinha, 14, 1), COD_ERRO, TEXTO_ERRO)

                        '    Return TEXTO_ERRO

                        'End If

                        '-- RETORNA ERRO DA PROCEDURE
                        If LOJA_DESTINO <> 1 Then        '--> CD REMOTO <--'

                            MSGPEDGO = "PEDIDO FINALIZADO OK "
                            tipo_retorno = "S"

                            If LOJA_DESTINO <> 1 Then
                                COD_ERRO = 0
                                TEXTO_ERRO = 0
                                Call RETORNO_ERRO(Mid(strlinha, 2, 2), Mid(strlinha, 4, 9), Mid(strlinha, 13, 1), Mid(strlinha, 14, 1), COD_ERRO, TEXTO_ERRO)
                            End If

                            Call LOG("I", STRPEDIDO, tmp_from, Mid(strlinha, 4, 9), NumPed, SSeq, 0)

                            'Arquivo de Cabe�alho

                            oEscrever = oArquivo.CreateText(strDIR_SAIDA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR")
                            oEscrever.WriteLine("GERACAO   =" & tmp_date)
                            oEscrever.WriteLine("TO        =" & tmp_from)
                            oEscrever.WriteLine("FROM      =" & tmp_to)
                            oEscrever.WriteLine("DESCRICAO =" & tmp_subject)
                            oEscrever.WriteLine("FILE      =" & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".TXT")

                            oEscrever.Close()

                        Else '--> CD CAMPINAS <--'

                            Call RETORNO_ERRO(Mid(strlinha, 2, 2), Mid(strlinha, 4, 9), Mid(strlinha, 13, 1), Mid(strlinha, 14, 1), COD_ERRO, TEXTO_ERRO)

                            Call LOG("I", strNOME_ARQUIVO, tmp_from, Mid(strlinha, 4, 9), lngNum_Pedido, lngSeq_Pedido, 0)

                            lngNum_Pedido = CMD.Parameters("P_NUM_PEDIDO").Value
                            lngSeq_Pedido = CMD.Parameters("P_SEQ_PEDIDO").Value

                            Pedido_Def = lngNum_Pedido
                            Seq_Def = lngSeq_Pedido

                        End If

                        STRPEDIDO = Mid(strNOME_ARQUIVO, 1, InStr(strNOME_ARQUIVO, ".") - 1) & ".CON"

                        If Pedido_Def <> 0 Then
                            Call Cons_Pedido(1, strDIR_SA_PEDIDO & STRPEDIDO, LOJA_DESTINO, Pedido_Def, Seq_Def)
                            'Call Cons_Pedido(1, strDIR_SA_PEDIDO & STRPEDIDO, Mid(Trim(strDEPOSITO_DEFAULT), 1, 2), Pedido_Def, Seq_Def)
                        Else
                            'txtReader.Close()

                            'Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                            'Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                            'Exit Function
                        End If
                        strlinha = ""

                        '-- Arquivo de Mensagem de Retorno de Pedido Gravado no Diret�rio:
                        '-- p:\OUT\numero_do_pedido.MSG

                        oEscrever = oArquivo.CreateText(strDIR_SAIDA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR")
                        oEscrever.WriteLine("GERACAO   =" & tmp_date)
                        oEscrever.WriteLine("TO        =" & tmp_from)
                        oEscrever.WriteLine("FROM      =" & tmp_to)

                        If LOJA_DESTINO <> 1 Then
                            oEscrever.WriteLine("DESCRICAO =" & tmp_subject)
                            oEscrever.WriteLine("FILE      =" & Left(STRPEDIDO, InStr(STRPEDIDO, ".") - 1) & ".TXT")
                        Else
                            tmp_subject = "#CON" & Mid(tmp_subject, 5)
                            oEscrever.WriteLine("DESCRICAO =" & tmp_subject)
                            oEscrever.WriteLine("FILE      =" & Left(STRPEDIDO, InStr(STRPEDIDO, ".") - 1) & ".CON")
                        End If
                        oEscrever.Close()
                    Else
                        txtReader.Close()
                        Return TEXTO_ERRO & "***8"
                    End If
                End If

                'End If
                'DoEvents
                'Limpa vari�vel para come�ar a montar a linha do Cabe�alho do pedido.
                'strlinha = ""

                strlinha = txtReader.ReadLine

            End While

            txtReader.Close()

            'lblEnvia.Caption = "Buscando Pr�ximo Pedido ..."
            'frmFimPedido.Refresh()

            '--------------------------------------------------
            '-- DELETA DA REDE PEDIDOS QUE FORAM ENVIADOS OK
            '--------------------------------------------------
            'Move(strDIR_ENTRADA & strNOME_ARQUIVO.Replace("TXT", "HDR"), "c:\pedidos_fil200\" & strNOME_ARQUIVO.Replace("TXT", "HDR"))
            'Move(strDIR_EN_PEDIDO & strNOME_ARQUIVO, "c:\pedidos_fil200\" & strNOME_ARQUIVO)

            Try
                Kill(strDIR_EN_PEDIDO & strNOME_ARQUIVO)
            Catch ex As Exception

            End Try
            Try
                Kill(strDIR_ENTRADA & strNOME_ARQUIVO.Replace("TXT", "HDR"))
            Catch ex As Exception

            End Try

            'lblEnvia.Caption = ""
            'Screen.MousePointer = 0

            Return "0"

        Catch ex As OracleException
            txtReader.Close()
            'Debug.WriteLine(EX.ToString)
            Return ex.ToString & "***6"
        Catch ex As Exception
            txtReader.Close()
            'Debug.WriteLine(EX.ToString)
            Return ex.ToString & "***7"
        End Try

    End Function


    Private Sub GRAVA_PEDNOTA(ByVal loja As String, ByVal NPEDIDO As String, ByVal SPEDIDO As Integer, ByVal tpedido As String)
        Try

            '-------------------------------------------------------------------------------------------
            '-- Se Pedido for para CD REMOTO e n�o tiver dado erro atualiza PEDNOTA_VENDA de CAMPINAS
            '-- para que o CD CAMPINAS possa consultar estes pedidos.
            '-------------------------------------------------------------------------------------------

            Dim dep As String

            Dim CMD As New OracleCommand
            CMD.Parameters.Clear()
            CMD.Connection = ora
            CMD.CommandType = CommandType.StoredProcedure
            CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_GRAVA_PEDNOTA"

            Dim oArquivo As System.IO.File
            Dim oEscrever As System.IO.StreamWriter

            If Fl_Complemento = "N" Then

                dep = "DEP" & loja.PadLeft(2, "0")
                CMD.Parameters.Add(New OracleParameter("DEP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("DEP").Value = dep

                CMD.Parameters.Add(New OracleParameter("PM_TP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("PM_TP").Value = tpedido

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = loja

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = Mid(NPEDIDO, 4)

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = SPEDIDO

                CMD.Parameters.Add(New OracleParameter("PM_PEN", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_PEN").Value = Mid(NPEDIDO, 4)

            Else

                dep = "DEP" & loja.PadLeft(2, "0")
                CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("PM_DEP").Value = dep

                CMD.Parameters.Add(New OracleParameter("PM_TP", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_TP").Value = tpedido

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = loja

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = NPEDIDO

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = SPEDIDO

                CMD.Parameters.Add(New OracleParameter("PM_PEN", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_PEN").Value = Mid(NPEDIDO, 4)

            End If

            CMD.Parameters.Add(New OracleParameter("PM_ERRO", OracleDbType.Decimal, ParameterDirection.Output))
            CMD.Parameters("PM_ERRO").Value = 0

            CMD.ExecuteNonQuery()

            '-- Gera Retorno do Complemento
            If Fl_Complemento = "S" Then

                oEscrever = oArquivo.CreateText(strDIR_SAIDA & NPEDIDO & ".HDR")
                oEscrever.WriteLine("GERACAO   =" & tmp_date)
                oEscrever.WriteLine("TO        =" & tmp_from)
                oEscrever.WriteLine("FROM      =" & tmp_to)
                oEscrever.WriteLine("DESCRICAO =" & tmp_subject)
                oEscrever.WriteLine("FILE      =" & NPEDIDO & ".TXT")

                oEscrever.Close()

                '-------------------------------
                '-- CORPO DO E-MAIL DE RETORNO
                '-------------------------------
                oEscrever = oArquivo.CreateText(strDIR_SAIDA & NPEDIDO & ".TXT")
                oEscrever.WriteLine(loja.PadLeft(2, "0") & "|" & NPEDIDO.ToString.PadLeft(7, "0") & "|" & SPEDIDO & "|" & MSGPEDGO)

                oEscrever.Close()

                If COD_ERRO <> 0 Then
                    Call LOG("I", strNOME_ARQUIVO, tmp_from, NPEDIDO, NPEDIDO, SPEDIDO, COD_ERRO)
                End If

            End If

            Exit Sub

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        End Try

    End Sub


    Private Sub Cons_Pedido(ByVal TIPO As Byte, ByVal Arquivo As String, ByVal cod_loja As Integer, ByVal num_pedido As Long, ByVal seq_pedido As Integer)
        Try

            Dim strMsg As String
            Dim pednota As String
            Dim itpednota As String
            Dim romaneio As String
            Dim conf As String
            Dim pedliq As String
            Dim msg_Ped As String
            Dim msg_Nf As String
            Dim strTabela As String
            Dim i As Long
            Dim COD_VDR As String

            Dim FL_FIL As String
            Dim flendentrega As String

            Dim oArquivo As System.IO.File
            Dim oEscrever As System.IO.StreamWriter

            '-----------------------------
            '-- Consulta Dados do Pedido
            '-----------------------------

            'lblEnvia.Caption = "Montando arquivo de Retorno ..."
            'frmFimPedido.Refresh()

            '-->> PEDNOTA_VENDA

            If cod_loja <> 1 Then
                'oEscrever.WriteLine(cod_loja.ToString.PadLeft(2, "0") & "|" & num_pedido.ToString.PadLeft(7, "0") & "|" & seq_pedido & "|")
                'oEscrever.WriteLine("ped|" & pednota & MSGPEDGO)
            Else
                If TIPO = 1 Then
                    oEscrever = oArquivo.CreateText(Arquivo)
                Else
                    oEscrever = oArquivo.AppendText(Arquivo)
                End If

                'lblEnvia.Caption = "Retorno - Pednota_Venda ... "
                'frmFimPedido.Refresh()
                Dim CMD As New OracleCommand
                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSULTA_PEDIDO"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = cod_loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = seq_pedido

                CMD.Parameters.Add(New OracleParameter("PM_PED", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_PED").Value = num_pedido

                CMD.Parameters.Add(New OracleParameter("PM_FL", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_FL").Value = TIPO

                Dim daDados As New OracleDataAdapter(CMD)
                Dim tblDados As New DataTable
                daDados.Fill(tblDados)

                If tblDados.Rows.Count > 0 Then
                    pednota = tblDados.Rows(0)("cod_loja_nota").ToString.PadLeft(2, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("num_nota").ToString.PadLeft(6, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("NUM_PENDENTE").ToString.PadLeft(11, "0") & "|"
                    If FL_FIL = "S" Then ' 4 CASAS
                        pednota = pednota & tblDados.Rows(0)("cod_filial").ToString.PadLeft(4, "0") & "|" 'COD_FILIAL
                    Else
                        pednota = pednota & tblDados.Rows(0)("cod_filial").ToString.PadLeft(2, "0") & "|" 'COD_FILIAL
                    End If
                    pednota = pednota & tblDados.Rows(0)("tp_pedido").ToString.PadLeft(2, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("tp_dpkblau") & "|"
                    pednota = pednota & tblDados.Rows(0)("tp_transacao") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_digitacao") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_pedido") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_emissao_nota") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_ssm") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_bloq_politica") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_bloq_credito") & "|"
                    pednota = pednota & tblDados.Rows(0)("dt_bloq_frete") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_nope") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_cliente").ToString.PadLeft(6, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_fornecedor").ToString.PadLeft(3, "0") & "|"
                    If flendentrega = "N" Then
                        pednota = pednota & tblDados.Rows(0)("cod_end_entrega").ToString.PadLeft(6, "0") & "|"
                        pednota = pednota & tblDados.Rows(0)("cod_end_cobranca").ToString.PadLeft(6, "0") & "|"
                    Else
                        pednota = pednota & tblDados.Rows(0)("cod_end_entrega").ToString.PadLeft(2, "0") & "|"
                        pednota = pednota & tblDados.Rows(0)("cod_end_cobranca").ToString.PadLeft(2, "0") & "|"
                    End If
                    pednota = pednota & tblDados.Rows(0)("cod_transp").ToString.PadLeft(4, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_repres").ToString.PadLeft(4, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_vend").ToString.PadLeft(4, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_plano").ToString.PadLeft(3, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_banco").ToString.PadLeft(3, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_cfo_dest") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_cfo_oper").ToString.PadLeft(2, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("frete_pago") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("peso_bruto"), "00000.00") & "|"
                    pednota = pednota & tblDados.Rows(0)("qtd_ssm").ToString.PadLeft(2, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("qtd_item_pedido").ToString.PadLeft(3, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("qtd_item_nota").ToString.PadLeft(3, "0") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_contabil"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_ipi"), "00000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_baseicm1"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_baseicm2"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_base_1"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_base_2"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_base_3"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_baseisen"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_base_5"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_base_6"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_baseoutr"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_basemaj"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_icmretido"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_baseipi"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_bisenipi"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_Boutripi"), "000000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_frete"), "00000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("vl_desp_acess"), "0000000000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_desconto"), "000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_desc_suframa"), "000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_acrescimo"), "000.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_seguro"), "00.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_icm1"), "00.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_icm2"), "00.00") & "|"
                    pednota = pednota & Format(tblDados.Rows(0)("pc_aliq_interna"), "00.00") & "|"
                    pednota = pednota & tblDados.Rows(0)("cod_cancel").ToString.PadLeft(4, "0") & "|"
                    pednota = pednota & tblDados.Rows(0)("fl_ger_ssm") & "|"
                    pednota = pednota & tblDados.Rows(0)("fl_ger_nfis") & "|"
                    pednota = pednota & tblDados.Rows(0)("fl_pendencia") & "|"
                    pednota = pednota & tblDados.Rows(0)("fl_desp_acess") & "|"
                    pednota = pednota & tblDados.Rows(0)("fl_dif_icm") & "|"
                    pednota = pednota & tblDados.Rows(0)("situacao") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_protesto") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_juros") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_cheque") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_compra") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_duplicata") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_limite") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_saldo") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_clie_novo") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_desconto") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_acrescimo") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_vlfatmin") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_item_desc1") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_item_desc2") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_item_desc3") & "|"
                    pednota = pednota & tblDados.Rows(0)("bloq_frete") & "|"
                    msg_Ped = tblDados.Rows(0)("mens_pedido") & "|"
                    msg_Nf = tblDados.Rows(0)("mens_nota") & "|"

                    COD_VDR = tblDados.Rows(0)("COD_VDR") & "|"

                    oEscrever.WriteLine(cod_loja.ToString.PadLeft(2, "0") & "|" & num_pedido.ToString.PadLeft(7, "0") & "|" & seq_pedido & "|")
                    oEscrever.WriteLine("ped|" & pednota & msg_Ped & msg_Nf & COD_VDR)

                End If

                daDados = Nothing
                tblDados = Nothing

                '-->> ITPEDNOTA_VENDA

                'lblEnvia.Caption = "Retorno - Itpednota_Venda ... "
                'frmFimPedido.Refresh()

                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSULTA_ITEM"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = cod_loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = seq_pedido

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = num_pedido

                Dim daDados2 As New OracleDataAdapter(CMD)
                Dim tblDados2 As New DataTable
                daDados2.Fill(tblDados2)

                Dim dtrItens As DataRow

                For Each dtrItens In tblDados2.Rows
                    strTabela = dtrItens("tabela_venda")
                    itpednota = ""
                    itpednota = dtrItens("num_item_pedido").ToString.PadLeft(3, "0") & "|"
                    itpednota = itpednota & dtrItens("cod_loja_nota").ToString.PadLeft(2, "0") & "|"
                    itpednota = itpednota & dtrItens("num_nota").ToString.PadLeft(6, "0") & "|"
                    itpednota = itpednota & dtrItens("num_item_nota").ToString.PadLeft(3, "0") & "|"
                    itpednota = itpednota & dtrItens("cod_dpk").ToString.PadLeft(5, "0") & "|"
                    itpednota = itpednota & dtrItens("qtd_solicitada").ToString.PadLeft(5, "0") & "|"
                    itpednota = itpednota & dtrItens("qtd_atendida").ToString.PadLeft(5, "0") & "|"
                    itpednota = itpednota & Format(dtrItens("preco_unitario"), "0000000000.00") & "|"
                    itpednota = itpednota & strTabela & "|"
                    itpednota = itpednota & Format(dtrItens("pc_desc1"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_desc2"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_desc3"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_dificm"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_ipi"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_comiss"), "00.00") & "|"
                    itpednota = itpednota & Format(dtrItens("pc_comisstlmk"), "00.00") & "|"
                    itpednota = itpednota & dtrItens("cod_trib") & "|"
                    itpednota = itpednota & Format(dtrItens("cod_tribipi"), "00.00") & "|"
                    itpednota = itpednota & dtrItens("situacao") & "|"

                    oEscrever.WriteLine("ite|" & itpednota)
                Next

                daDados2 = Nothing
                tblDados2 = Nothing

                '-->> ROMANEIO
                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSULTA_ROMANEIO"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = cod_loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = seq_pedido

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = num_pedido

                Dim daDados3 As New OracleDataAdapter(CMD)
                Dim tblDados3 As New DataTable
                daDados3.Fill(tblDados3)

                If tblDados3.Rows.Count = 0 Then
                    'oEscrever.WriteLine("rom|")
                ElseIf tblDados3.Rows(0)("dt_coleta") Is System.DBNull.Value Then
                    'oEscrever.WriteLine("rom|")
                Else
                    romaneio = Format(tblDados3.Rows(0)("dt_coleta"), "dd/MM/yy") & "|"
                    romaneio = romaneio & tblDados3.Rows(0)("dt_despacho") & "|"
                    romaneio = romaneio & tblDados3.Rows(0)("num_romaneio").ToString.PadLeft(6, "0") & "|"
                    romaneio = romaneio & tblDados3.Rows(0)("num_conhecimento").ToString.PadLeft(4, "0") & "|"
                    romaneio = romaneio & tblDados3.Rows(0)("num_carro").ToString.PadLeft(6, "0") & "|"
                    romaneio = romaneio & tblDados3.Rows(0)("qtd_volume").ToString.PadLeft(4, "0") & "|"
                    oEscrever.WriteLine("rom|" & romaneio)
                End If

                daDados3 = Nothing
                tblDados3 = Nothing

                '-->> R_PEDIDO_CONF

                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSULTA_R_PEDIDO_CONF"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = cod_loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = seq_pedido

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = num_pedido

                Dim daDados4 As New OracleDataAdapter(CMD)
                Dim tblDados4 As New DataTable
                daDados4.Fill(tblDados4)

                For Each dtrItens In tblDados4.Rows
                    conf = ""
                    conf = dtrItens("num_caixa").ToString.PadLeft(4, "0") & "|"
                    conf = conf & dtrItens("cod_conferente").ToString.PadLeft(5, "0") & "|"
                    conf = conf & dtrItens("cod_embalador").ToString.PadLeft(4, "0") & "|"
                    conf = conf & dtrItens("cod_expedidor").ToString.PadLeft(4, "0") & "|"
                    conf = conf & dtrItens("fl_pend_etiq") & "|"
                    conf = conf & dtrItens("qtd_volume_parc").ToString.PadLeft(6, "0") & "|"
                    oEscrever.WriteLine("r_p|" & conf)
                Next

                daDados4 = Nothing
                tblDados4 = Nothing

                '-->> V_PEDLIQ_VENDA
                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSULTA_V_PEDLIQ_VENDA"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output))

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = cod_loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = seq_pedido

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = num_pedido

                Dim daDados5 As New OracleDataAdapter(CMD)
                Dim tblDados5 As New DataTable
                daDados5.Fill(tblDados5)

                For Each dtrItens In tblDados5.Rows
                    pedliq = ""
                    pedliq = dtrItens("num_item_pedido").ToString.PadLeft(3, "0") & "|"
                    pedliq = pedliq & dtrItens("situacao") & "|"
                    pedliq = pedliq & Format(dtrItens("pr_liquido"), "000000000000.00") & "|"
                    pedliq = pedliq & Format(dtrItens("vl_liquido"), "000000000000.00") & "|"
                    pedliq = pedliq & Format(dtrItens("pr_liquido_suframa"), "000000000000.00") & "|"
                    pedliq = pedliq & Format(dtrItens("vl_liquido_suframa"), "000000000000.00") & "|"
                    oEscrever.WriteLine("v_p|" & pedliq)
                Next

                daDados5 = Nothing
                tblDados5 = Nothing

                oEscrever.Close()

            End If

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        End Try
    End Sub


    Private Function GRAVA_ITEM(ByVal Linha_Arq As String, ByRef objArq As IO.StreamReader) As String
        Try

            Dim DEP As String
            Dim TXT_ERRO As String

            If InStr(Linha_Arq, ",") > 0 Then

                COD_ERRO = "9999"
                TXT_ERRO = "Erro no formato do Arquivo, verificar configura��es regionais"

                EnviaEmail(COD_ERRO & " - " & TXT_ERRO, CInt(Mid(Linha_Arq, 4, 9)), CInt(Mid(Linha_Arq, 13, 1)))

                'Call RETORNO_ERRO(CInt(Mid(Linha_Arq, 2, 2)), CInt(Mid(Linha_Arq, 4, 9)), CInt(Mid(Linha_Arq, 13, 1)), Mid(Linha_Arq, 14, 1), COD_ERRO, COD_ERRO & " - " & TXT_ERRO)

                objArq.Close()

                If IO.File.Exists(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO") Then
                    Kill(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                End If
                If IO.File.Exists(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO") Then
                    Kill(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                End If

                Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")

                Return COD_ERRO & " - " & TXT_ERRO

            End If

            Dim CMD As New OracleCommand
            CMD.Parameters.Clear()
            CMD.Connection = ora
            CMD.CommandType = CommandType.StoredProcedure
            CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_GRAVA_ITEM"

            '-- LINK
            CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Char, ParameterDirection.Input))
            DEP = "DEP" & Mid(Linha_Arq, 2, 2)
            CMD.Parameters("PM_DEP").Size = 6
            CMD.Parameters("PM_DEP").Value = DEP

            '-- COD_LOJA
            CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_LOJA").Value = CLng(Mid(Linha_Arq, 2, 2))
            loja_ped = Mid(Linha_Arq, 2, 2)
            LOJA_DESTINO = Mid(Linha_Arq, 2, 2)

            '-- NUM_PENDENTE
            CMD.Parameters.Add(New OracleParameter("PM_PED", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_PED").Value = CLng(Mid(Linha_Arq, 4, 9))

            '-- SEQ_PENDENTE
            CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_SEQ").Value = CLng(Mid(Linha_Arq, 13, 1))

            '-- FL_TIPO_CHAVE
            CMD.Parameters.Add(New OracleParameter("PM_TP", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_TP").Value = CLng(Mid(Linha_Arq, 14, 1))

            '-- NUM_ITEM_PEDIDO
            CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_NUM").Value = CLng(Mid(Linha_Arq, 15, 3))
            'lngItem = Mid(Linha_Arq, 15, 3)

            '-- COD_DPK
            CMD.Parameters.Add(New OracleParameter("PM_DPK", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_DPK").Value = CLng(Mid(Linha_Arq, 18, 5))

            '-- QTD_SOLICITADA
            CMD.Parameters.Add(New OracleParameter("PM_QTD", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_QTD").Value = CLng(Mid(Linha_Arq, 23, 6))

            '-- PRECO_UNITARIO
            CMD.Parameters.Add(New OracleParameter("PM_PRECO", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_PRECO").Value = CType(Mid(Linha_Arq, 29, 13), Double)
            'CMD.Parameters.Add("PM_PRECO", FmtBR(Mid(Linha_Arq, 29, 13)), 1)

            '-- TABELA_VENDA
            CMD.Parameters.Add(New OracleParameter("PM_TAB", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_TAB").Size = 6
            CMD.Parameters("PM_TAB").Value = Mid(Linha_Arq, 42, 6)

            '-- PC_DESC1
            CMD.Parameters.Add(New OracleParameter("PM_DESC1", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_DESC1").Value = CType(Mid(Linha_Arq, 48, 5), Double)
            'CMD.Parameters.Add("PM_DESC1", FmtBR(Mid(Linha_Arq, 48, 5)), 1)

            Dim desc_neg As Integer
            If Mid(Linha_Arq, 53, 1) = "-" Then
                desc_neg = 1
            Else
                desc_neg = 0
            End If

            '-- PC_DESC2
            CMD.Parameters.Add(New OracleParameter("PM_DESC2", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_DESC2").Value = CType(Mid(Linha_Arq, 53, 5 + desc_neg), Double)
            'CMD.Parameters.Add("PM_DESC2", FmtBR(Mid(Linha_Arq, 53, 5)), 1)

            Dim desc_neg1 As Integer
            If Mid(Linha_Arq, 58 + desc_neg, 1) = "-" Then
                desc_neg1 = 1
            Else
                desc_neg1 = 0
            End If

            '-- PC_DESC3
            CMD.Parameters.Add(New OracleParameter("PM_DESC3", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_DESC3").Value = CType(Mid(Linha_Arq, 58 + desc_neg, 5 + desc_neg1), Double)

            If Mid(Linha_Arq, 58 + desc_neg, 1) = "-" Then
                desc_neg += 1
            Else
                desc_neg += 0
            End If

            '-- PC_DIFICM
            CMD.Parameters.Add(New OracleParameter("PM_ICM", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_ICM").Value = CType(Mid(Linha_Arq, 63 + desc_neg, 5), Double)

            '-- SITUACAO
            CMD.Parameters.Add(New OracleParameter("PM_SIT", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_SIT").Value = CLng(Mid(Linha_Arq, 68 + desc_neg, 1))

            '-- cod ERRO
            CMD.Parameters.Add(New OracleParameter("PM_CODERRO", OracleDbType.Int32, ParameterDirection.Output))
            CMD.Parameters("PM_CODERRO").Value = 0

            '-- txt ERRO
            CMD.Parameters.Add(New OracleParameter("PM_TXTERRO", OracleDbType.Varchar2, ParameterDirection.Output))
            CMD.Parameters("PM_TXTERRO").Size = 1000
            CMD.Parameters("PM_TXTERRO").Value = ""

            CMD.ExecuteNonQuery()

            If CMD.Parameters("PM_CODERRO").Value <> 0 Then

                COD_ERRO = CMD.Parameters("PM_CODERRO").Value
                TXT_ERRO = CMD.Parameters("PM_TXTERRO").Value.ToString

                If COD_ERRO = -1 Then
                    Dim strlinha As String
                    flemail = True
                    strlinha = objArq.ReadLine
                    While strlinha <> Nothing
                        strlinha = objArq.ReadLine
                        If Mid(strlinha, 1, 1) = "C" Then
                            TXT_ERRO = " - ITEM DO PEDIDO J� EXISTE - LOJA: " & Mid(strlinha, 2, 2) & " - REPRES: " & Mid(strlinha, 63, 4)
                        End If
                    End While
                End If

                Call RETORNO_ERRO(CInt(Mid(Linha_Arq, 2, 2)), CInt(Mid(Linha_Arq, 4, 9)), CInt(Mid(Linha_Arq, 13, 1)), Mid(Linha_Arq, 14, 1), COD_ERRO, COD_ERRO & " - " & TXT_ERRO)

                objArq.Close()

                If IO.File.Exists(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO") Then
                    Kill(strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                End If
                If IO.File.Exists(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO") Then
                    Kill(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")
                End If

                Move(strDIR_ENTRADA & strNOME_ARQUIVO, strDIR_ENTRADA & strNOME_ARQUIVO & ".ERRO")
                Move(strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR", strDIR_ENTRADA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR.ERRO")

                Return COD_ERRO & TXT_ERRO

            End If

            Return "0"

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            objArq.Close()
            Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            flemail = True
            Call RETORNO_ERRO(CInt(Mid(Linha_Arq, 2, 2)), CInt(Mid(Linha_Arq, 4, 9)), CInt(Mid(Linha_Arq, 13, 1)), Mid(Linha_Arq, 14, 1), "99", ex.ToString)
            objArq.Close()
            Return ex.ToString
        End Try


    End Function


    Private Function GRAVA_CABEC(ByVal Linha_Arq As String, ByRef objArq As IO.StreamReader) As String
        Try

            Dim bTipoPedido As Integer
            Dim DEP As String
            Dim TXT_ERRO As String

            Dim CMD As New OracleCommand
            CMD.Parameters.Clear()
            CMD.Connection = ora
            CMD.CommandType = CommandType.StoredProcedure
            CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_GRAVA_CABEC"


            DEP = "DEP" & Mid(Linha_Arq, 2, 2)
            CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_DEP").Value = DEP

            '-- COD_LOJA
            CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_LOJA").Value = CLng(Mid(Linha_Arq, 2, 2))
            LOJA_DESTINO = Mid(Linha_Arq, 2, 2)

            '-- NUM_PENDENTE
            CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_NUM").Value = CLng(Mid(Linha_Arq, 4, 9))

            '-- SEQ_PENDENTE
            CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_SEQ").Value = CLng(Mid(Linha_Arq, 13, 1))

            '-- FL_TIPO_CHAVE
            CMD.Parameters.Add(New OracleParameter("PM_TP", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_TP").Value = CLng(Mid(Linha_Arq, 14, 1))
            tipo_pedido = Mid(Linha_Arq, 14, 1)

            '-- COD_FILIAL
            CMD.Parameters.Add(New OracleParameter("PM_FIL", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_FIL").Value = CLng(Mid(Linha_Arq, 15, 4))

            '-- TP_PEDIDO
            CMD.Parameters.Add(New OracleParameter("PM_TPPED", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_TPPED").Value = CLng(Mid(Linha_Arq, 19, 2))
            bTipoPedido = Mid(Linha_Arq, 19, 2)

            '-- TP_DPKBLAU
            CMD.Parameters.Add(New OracleParameter("PM_TPDPK", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_TPDPK").Value = CLng(Mid(Linha_Arq, 21, 1))

            '-- DT_DIGITACAO
            'CDate(Format(Left(tmp_date, 8), "SHORT DATE") & " " & Format(Mid(tmp_date, 10, 8), "LONG TIME")), 1
            CMD.Parameters.Add(New OracleParameter("PM_DTDIG", OracleDbType.Date, ParameterDirection.Input))
            CMD.Parameters("PM_DTDIG").Value = CDate(Mid(Linha_Arq, 22, 8))

            '-- DT_PEDIDO
            CMD.Parameters.Add(New OracleParameter("PM_DTPED", OracleDbType.Date, ParameterDirection.Input))
            CMD.Parameters("PM_DTPED").Value = CDate(Mid(Linha_Arq, 36, 8))

            '-- COD_NOPE
            CMD.Parameters.Add(New OracleParameter("PM_NOPE", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_NOPE").Value = Mid(Linha_Arq, 44, 3)

            '-- COD_CLIENTE
            CMD.Parameters.Add(New OracleParameter("PM_CLI", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_CLI").Value = Mid(Linha_Arq, 47, 6)
            lngcliente = Mid(Linha_Arq, 47, 6)

            '-- COD_END_ENTREGA
            CMD.Parameters.Add(New OracleParameter("PM_END", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_END").Value = Mid(Linha_Arq, 53, 6)

            '-- COD_TRANSP
            CMD.Parameters.Add(New OracleParameter("PM_TRA", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_TRA").Value = Mid(Linha_Arq, 59, 4)

            If bTipoPedido = 10 Then

                '-- COD_REPRES
                CMD.Parameters.Add(New OracleParameter("PM_REP", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_REP").Value = Mid(Linha_Arq, 63, 4)

                '-- COD_VEND
                CMD.Parameters.Add(New OracleParameter("PM_VEND", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_VEND").Value = 0

            Else
                '-- COD_REPRES
                CMD.Parameters.Add(New OracleParameter("PM_REP", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_REP").Value = Mid(Linha_Arq, 63, 4)

                '-- COD_VEND
                CMD.Parameters.Add(New OracleParameter("PM_VEND", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_VEND").Value = Mid(Linha_Arq, 67, 4)

            End If

            '-- COD_PLANO
            CMD.Parameters.Add(New OracleParameter("PM_PLA", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_PLA").Value = Mid(Linha_Arq, 71, 3)

            '-- COD_BANCO
            CMD.Parameters.Add(New OracleParameter("PM_BAN", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_BAN").Value = Mid(Linha_Arq, 74, 3)

            '-- MENS_PEDIDO
            CMD.Parameters.Add(New OracleParameter("PM_MPED", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_MPED").Value = Mid(Linha_Arq, 77, 50)

            '-- MENS_NOTA
            CMD.Parameters.Add(New OracleParameter("PM_MNF", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_MNF").Value = Mid(Linha_Arq, 127, 60)

            '-- FRETE_PAGO
            CMD.Parameters.Add(New OracleParameter("PM_FRE", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_FRE").Value = Mid(Linha_Arq, 187, 1)

            '-- QTD_ITEM_PEDIDO
            CMD.Parameters.Add(New OracleParameter("PM_QTD", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_QTD").Value = Mid(Linha_Arq, 188, 3)

            '-- VL_CONTABIL
            CMD.Parameters.Add(New OracleParameter("PM_VL", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_VL").Value = CDbl(Mid(Linha_Arq, 191, 15))
            'OraParameters.Add("PM_VL", FmtBR(Format$(Mid(Linha_Arq, 191, 15), "0.00")), 1)

            '-- VL_IPI
            CMD.Parameters.Add(New OracleParameter("PM_IPI", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_IPI").Value = CDbl(Mid(Linha_Arq, 206, 14))
            'OraParameters.Add("PM_IPI", FmtBR(Format$(Mid(Linha_Arq, 206, 14), "0.00")), 1)

            '-- PC_DESCONTO
            CMD.Parameters.Add(New OracleParameter("PM_DES", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_DES").Value = Mid(Linha_Arq, 220, 6)
            'OraParameters.Add("PM_DES", FmtBR(Mid(Linha_Arq, 220, 6)), 1)

            '-- PC_DESC_SUFRAMA
            CMD.Parameters.Add(New OracleParameter("PM_SUF", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_SUF").Value = Mid(Linha_Arq, 226, 6)
            'OraParameters.Add("PM_SUF", FmtBR(Mid(Linha_Arq, 226, 6)), 1)

            '-- PC_ACRESCIMO
            CMD.Parameters.Add(New OracleParameter("PM_ACR", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_ACR").Value = Mid(Linha_Arq, 232, 6)
            'OraParameters.Add("PM_ACR", FmtBR(Mid(Linha_Arq, 232, 6)), 1)

            '-- COD_CANCEL
            CMD.Parameters.Add(New OracleParameter("PM_CAN", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_CAN").Value = 0

            '-- FL_PENDENCIA
            CMD.Parameters.Add(New OracleParameter("PM_FLPEN", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_FLPEN").Value = Mid(Linha_Arq, 240, 1)

            '-- FL_DIF_ICM
            CMD.Parameters.Add(New OracleParameter("PM_ICM", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_ICM").Value = Mid(Linha_Arq, 241, 1)

            '-- BLOQ_CLIE_NOVO
            CMD.Parameters.Add(New OracleParameter("PM_BLO", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_BLO").Value = "N"

            '-- SITUACAO
            CMD.Parameters.Add(New OracleParameter("PM_SIT", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_SIT").Value = 0

            '-- COD_VDR
            CMD.Parameters.Add(New OracleParameter("PM_VDR", OracleDbType.Decimal, ParameterDirection.Input))
            CMD.Parameters("PM_VDR").Value = Mid(Linha_Arq, 243, 1)

            '-- COD_INTERNET
            CMD.Parameters.Add(New OracleParameter("PM_INT", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_INT").Value = "N"

            'lblEnvia.Caption = "Enviando Cabe�alho para o ORACLE"
            'frmFimPedido.Refresh()

            '-- cod ERRO
            CMD.Parameters.Add(New OracleParameter("PM_CODERRO", OracleDbType.Int32, ParameterDirection.Output))
            'CMD.Parameters("PM_ERRO").Value = 0

            '-- txt ERRO
            CMD.Parameters.Add(New OracleParameter("PM_TXTERRO", OracleDbType.Varchar2, ParameterDirection.Output))
            CMD.Parameters("PM_TXTERRO").Size = 1000
            CMD.Parameters("PM_TXTERRO").Value = ""

            CMD.ExecuteNonQuery()

            If Val(CMD.Parameters("PM_CODERRO").Value) <> 0 Then
                COD_ERRO = CMD.Parameters("PM_CODERRO").Value
                TXT_ERRO = CMD.Parameters("PM_TXTERRO").Value.ToString

                If COD_ERRO = -1 Then
                    TXT_ERRO = " PEDIDO J� EXISTE"
                End If

                'MsgBox OraParameters("verro").Value & " Grava_Cabec "

                objArq.Close()

                Call RETORNO_ERRO(Mid(Linha_Arq, 2, 2), Mid(Linha_Arq, 4, 9), Mid(Linha_Arq, 13, 1), Mid(Linha_Arq, 14, 1), COD_ERRO, COD_ERRO & " - " & TXT_ERRO)
                'Call GRAVA_ERRO(Format(Mid(Linha_Arq, 2, 2), "00"), Mid(Linha_Arq, 4, 9), Mid(Linha_Arq, 13, 1), COD_ERRO, TEXTO_ERRO, Mid(Linha_Arq, 15, 4))
                Return COD_ERRO & " - " & TXT_ERRO
            End If

            Return "0"

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            objArq.Close()
            Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            objArq.Close()
            Return ex.ToString
        End Try

    End Function


    Private Sub RETORNO_ERRO(ByVal loja As Integer, ByVal pedido As Integer, ByVal SEQUENCIA As Integer, ByVal TIPO As String, ByVal Erro As String, ByVal TEXTO As String)
        Try

            '---------------------------------------------------------------------------------------
            '-- GERA E-MAIL DE RETORNO DE ERRO PARA FRANQUIA/PAAC DOS PEDIDOS DE CD�s REMOTOS
            '---------------------------------------------------------------------------------------

            Dim DEP As String
            Dim CMD As New OracleCommand

            If TIPO = 2 And Erro = "0" Then

                '-- BUSCA NUMERO DEFINITIVO DO PEDIDO REMOTAMENTE
                '-- LINK

                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CONSPED"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.InputOutput))

                DEP = "DEP" & loja.ToString.PadLeft(2, "0")
                CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("PM_DEP").Value = DEP

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = SEQUENCIA

                CMD.Parameters.Add(New OracleParameter("PM_PED", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_PED").Value = pedido

                CMD.Parameters.Add(New OracleParameter("PM_TP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("PM_TP").Value = TIPO



                Dim daDados As New OracleDataAdapter(CMD)
                Dim tblDados As New DataTable
                daDados.Fill(tblDados)

                '-- SE OCORRER ERRO DE COMUNICA��O ENT�O CONTINUA LEVANDO O NUMERO PENDENTE, AGORA SE
                '-- CONSEGUIR CONEX�O ENVIA O NUMERO DEFINITIVO (NUM_PEDIDO)
                '-- ENVIANDO O NUM_PEDIDO ESSES PEDIDOS CONSEGUIR�O SER CONSULTADOS POSTERIORMENTE
                '-- PELO FIL080

                If tblDados.Rows.Count > 0 Then
                    pedido = tblDados.Rows(0)("num_pedido")
                    NumPed = pedido
                    SSeq = tblDados.Rows(0)("seq_pedido")

                    Pedido_Def = pedido
                    Seq_Def = SSeq

                End If

                If TEXTO Like "*2068*" Then
                    MSGPEDGO = "PEDIDO N�O PROCESSADO. ESTAMOS SEM COMUNICA��O. "
                ElseIf TEXTO Like "*>*" Then
                    MSGPEDGO = "PROBLEMA DE CADASTRO. CORRIGIR E REENVIA-LO."
                ElseIf Erro = "-1" Then
                    MSGPEDGO = "JA EXISTE PEDIDO COM ESTE NUMERO. VERIFIQUE!"
                ElseIf Erro > 0 Then
                    MSGPEDGO = "PEDIDO N�O PROCESSADO. ESTAMOS SEM COMUNICA��O. "
                End If
            Else

                If TEXTO Like "*>*" Then
                    MSGPEDGO = "PROBLEMA DE CADASTRO. CORRIGIR E REENVIA-LO."
                ElseIf Erro = "-1" Then
                    MSGPEDGO = "JA EXISTE PEDIDO COM ESTE NUMERO. VERIFIQUE!"
                Else
                    MSGPEDGO = "PEDIDO N�O PROCESSADO. ESTAMOS SEM COMUNICA��O. "
                End If

                pedido = pedido
                NumPed = pedido
                SSeq = SEQUENCIA
            End If

            Dim oArquivo As System.IO.File
            Dim oEscrever As System.IO.StreamWriter

            If LOJA_DESTINO <> 1 Then
                '-----------------------------------
                '-- CABE�ALHO DO E-MAIL DE RETORNO
                '-----------------------------------

                oEscrever = oArquivo.CreateText(strDIR_SAIDA & Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1) & ".HDR")
                oEscrever.WriteLine("GERACAO   =" & tmp_date)
                oEscrever.WriteLine("TO        =" & tmp_from)
                oEscrever.WriteLine("FROM      =" & tmp_to)
                oEscrever.WriteLine("DESCRICAO =" & tmp_subject)
                oEscrever.WriteLine("FILE      =" & strNOME_ARQUIVO)
                oEscrever.Close()

                '-------------------------------
                '-- CORPO DO E-MAIL DE RETORNO
                '-------------------------------

                oEscrever = oArquivo.CreateText(strDIR_SAIDA & strNOME_ARQUIVO)
                oEscrever.WriteLine(LOJA_DESTINO.ToString.PadLeft(2, "0") & "|" & Format(pedido, "0000000") & "|" & Format(SEQUENCIA, "0") & "|" & MSGPEDGO)
                oEscrever.Close()
            End If

            If COD_ERRO <> 0 Or Erro <> 0 Then
                LOG("I", strNOME_ARQUIVO, tmp_from, pedido, pedido, SEQUENCIA, COD_ERRO)

                If flemail = True Then
                    EnviaEmail(TEXTO, pedido, SEQUENCIA)
                End If

            End If

            '--------------------------------------------------------------------------------------
            '-- Busca Pedido Desmembrado - Itens com tributa��o Normal e Itens com Subst Tributaria

            If Erro = 0 Then 'Finalizou Pedido, sem erros, ent�o busca complemento

                CMD.Parameters.Clear()
                CMD.Connection = ora
                CMD.CommandType = CommandType.StoredProcedure
                CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_BUSCA_COMPLEMENTO"

                CMD.Parameters.Add(New OracleParameter("PM_CURSOR", OracleDbType.RefCursor, ParameterDirection.InputOutput))

                DEP = "DEP" & loja.ToString.PadLeft(2, "0")
                CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
                CMD.Parameters("PM_DEP").Value = DEP

                CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_LOJA").Value = loja

                CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_SEQ").Value = SSeq

                CMD.Parameters.Add(New OracleParameter("PM_NUM", OracleDbType.Decimal, ParameterDirection.Input))
                CMD.Parameters("PM_NUM").Value = pedido

                Dim daDados As New OracleDataAdapter(CMD)
                Dim tblDados As New DataTable
                daDados.Fill(tblDados)

                If tblDados.Rows.Count = 0 Then
                    Fl_Complemento = "N"
                Else
                    Fl_Complemento = "S"
                    Call GRAVA_PEDNOTA(tblDados.Rows(0)("COD_LOJA_COMPL"), tblDados.Rows(0)("NUM_PEDIDO_COMPL"), tblDados.Rows(0)("SEQ_PEDIDO_COMPL"), TIPO)
                End If

            End If


            'Del_PEDVENDA(loja, pedido, SEQUENCIA)


            '--------------------------------------------------------------------------------------

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString & "***3")
            'Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
        End Try
    End Sub

    Public Sub LOG(ByVal Status, ByVal Arquivo, ByVal Filial, ByVal Pend, ByVal Num, ByVal Seq, ByVal Erro)

        Try
            'Status  = A - Abrir Arquivo
            '        = I - Incluir no Arquivo j� aberto
            'Arquivo = Nome do Arquivo que veio com o pedido
            'Filial  = Filial que enviou o pedido
            'Pend    = Num Pendente do pedido
            'Num     = Num Definitivo do pedido
            'Erro    = 0 - Pedido Finalizado Ok
            '        <> 0 - C�digo do Erro

            Dim strCD As String
            Dim cd As Integer
            Dim Data_Log
            Dim Hora_Log

            Dim oArquivo As System.IO.File
            Dim oEscrever As System.IO.StreamWriter

            strCD = strDEPOSITO_DESTINO

            If strCD = "C" Then
                cd = "01"
            Else
                cd = strCD
            End If

            Data_Log = Now.Date.Year.ToString & Now.Date.Month.ToString & Now.Date.Day.ToString
            Hora_Log = Now.TimeOfDay.Hours.ToString & Now.TimeOfDay.Minutes.ToString & Now.TimeOfDay.Seconds.ToString
            Arquivo_Log = "LOG" & Data_Log & cd & ".TXT"

            If IO.File.Exists("c:\log\" & Arquivo_Log) Then
                oEscrever = oArquivo.AppendText("c:\log\" & Arquivo_Log)
                oEscrever.WriteLine(Now.Date.ToString & "|" & Now.TimeOfDay.ToString & "| Inicio do Processamento ")
            Else
                oEscrever = oArquivo.CreateText("c:\log\" & Arquivo_Log)
            End If

            If Status = "A" Then


            Else

                oEscrever.WriteLine(Now.Date.ToString & "|" & Now.TimeOfDay.ToString & "|" & Filial & "|" & Arquivo & "|" & Pend & "|" & Num & "|" & Seq & "|" & Erro)
                oEscrever.Close()

            End If

        Catch ex As OracleException
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        Catch ex As Exception
            'Debug.WriteLine(EX.ToString)
            'Return ex.ToString
        End Try

    End Sub


    Private Sub EnviaEmail(ByVal MSGERRO As String, ByVal PEDIDO As String, ByVal SEQ As String)
        'Try
        Dim CMD As New OracleCommand

        Dim SUBJECT As String
        Dim BODY As String

        SUBJECT = "FIL200 - ERRO PEDIDO " & PEDIDO & "-" & SEQ

        BODY = "OCORREU ERRO NO PROCESSAMENTO DO PEDIDO " & PEDIDO & "-" & SEQ & vbCrLf & vbCrLf & "ERRO:" & vbCrLf & MSGERRO

        If InStr(MSGERRO, "TNS") > 0 Then
            BODY = BODY & vbCrLf & "ATEN��O: VERIFICAR ESTE PEDIDO MANUALMENTE"
        End If

        CMD.Connection = ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_CON_EMAIL"
        CMD.Parameters.Add(New OracleParameter("cCursor", OracleDbType.RefCursor, ParameterDirection.Output))
        Dim dta_Email As New OracleDataAdapter(CMD)
        Dim dts_Email As New DataTable
        Dim dtr_Email As DataRow
        dta_Email.Fill(dts_Email)


        CMD.Connection = ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "INTRANET.ENVIA_EMAIL"

        For Each dtr_Email In dts_Email.Rows
            CMD.Parameters.Clear()

            CMD.Parameters.Add(New OracleParameter("PM_TO", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_TO").Value = dtr_Email("Email")

            CMD.Parameters.Add(New OracleParameter("PM_SUBJECT", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_SUBJECT").Value = SUBJECT

            CMD.Parameters.Add(New OracleParameter("PM_BODY", OracleDbType.Varchar2, ParameterDirection.Input))
            CMD.Parameters("PM_BODY").Value = BODY

            CMD.ExecuteNonQuery()
        Next

        'Catch ex As Exception
        'Return ex.ToString

        'Debug.WriteLine(ex.ToString)
        'End Try

    End Sub

    Protected Overrides Sub Finalize()
        ora.Close()
        MyBase.Finalize()
    End Sub


    Private Function TrataErro_GERPEDVDA(ByVal msgERRO As String) As String

        flPackage = False

        If msgERRO Like "*PR_SITPEDVDA > 0*" Then
            flemail = True
            Return " - PR_SITPEDVDA > 0 - Pedido j� existe no CD para onde est� sendo enviado o pedido"
        ElseIf msgERRO Like "*PR_SITPEDVDA > LOJA*" Then
            flemail = False
            Return "PR_SITPEDVDA > LOJA - A loja do pedido n�o � a mesma para onde foi enviado o pedido"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 1*" Then
            flemail = False
            Return "PR_SITPEDVDA > 1 - Cliente ou plano do pedido  n�o existente no CD para onde est� sendo enviado o pedido"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 2*" Then
            flemail = False
            Return "PR_SITPEDVDA > 2 - O campo representante e o campo vendedor est� vazio"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 3*" Then
            flemail = False
            Return "PR_SITPEDVDA > 3 - O representante n�o existe no CD para onde est� sendo enviado o pedido"""
        ElseIf msgERRO Like "*PR_SITPEDVDA > 4*" Then
            flemail = False
            Return "PR_SITPEDVDA > 4 - O vendedor n�o existe no CD para onde est� sendo enviado o pedido"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 5*" Then
            flemail = False
            Return "PR_SITPEDVDA > 5 - Cliente est� desativado no CD para onde o pedido est� sendo enviado ou o tipo de pedido � 99(forn/func.) por�m o vendedor n�o � 599"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 6*" Then
            flemail = False
            Return "PR_SITPEDVDA > 6 - O cliente � suframa (inscr_suframa <> nulo) e a natureza n�o suframa"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 7*" Then
            flemail = False
            Return "PR_SITPEDVDA > 7 - O plano de pagamento est� desativado no CD para onde o pedido est� sendo enviado"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 8*" Then
            flemail = False
            Return "PR_SITPEDVDA > 8 - O representante ou o vendedor est� desativado no CD para onde o pedido est� sendo enviado"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 9*" Then
            flemail = False
            Return "PR_SITPEDVDA > 9 - O pedido � tipo 22, por�m o representante ou o vendedor est� vazio"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 10*" Then
            flemail = False
            Return "PR_SITPEDVDA > 10 - O pedido � tipo 11, por�m o representante ou o vendedor est� vazio"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 11*" Then
            flemail = False
            Return "PR_SITPEDVDA > 11 - O pedido � tipo 11 e o tipo da regional � REGIONAL"
        ElseIf msgERRO Like "*PR_SITPEDVDA > 12*" Then
            flemail = False
            Return "PR_SITPEDVDA > 12 - O pedido � tipo 10  por�m tem vendedor"
        ElseIf msgERRO Like "*PR_ITPEDVDA > 1*" Then
            flemail = False
            Return "PR_ITPEDVDA > 1 - O cliente  n�o existe no CD para onde est� sendo enviado o pedido"
        ElseIf msgERRO Like "*PR_ITPEDVDA > 2*" Then
            flemail = False
            Return "PR_ITPEDVDA > 2 - O item n�o existe no CD para onde est� sendo enviado o pedido ou a tabela de venda de uma dos itens n�o est� mais em vigor"
        ElseIf msgERRO Like "*PR_ITPEDVDA > 3*" Then
            flemail = False
            Return "PR_ITPEDVDA > 3 - O item n�o existe no CD para onde est� sendo enviado o pedido"
        ElseIf msgERRO Like "*PR_ITPEDVDA > 4*" Then
            flemail = False
            Return "PR_ITPEDVDA > 4 - O item � substitui��o tribut�ria por�m n�o est� cadastrado na tabela compra.subst_tributaria"
        ElseIf msgERRO Like "*PR_ITPEDVDA > 5*" Then
            flemail = False
            Return "PR_ITPEDVDA > 5 - A quantidade de itens informada n�o � igual aos itens enviados"
        ElseIf msgERRO Like "*unique constraint (PRODUCAO.UN_NUMPEND_PEDNOTA)*" Then
            flemail = False
            Return "Pedido j� existe na PEDNOTA_VENDA"
        ElseIf msgERRO Like "*packages has been discarded*" Then
            flemail = False
            flPackage = True
            Return "PACKAGES HAS BEEN DISCARDED - Reprocessar o arquivo"
        ElseIf UCase(msgERRO) Like "*NET8*" Then
            flemail = False
            flPackage = True
            Return "ERRO DE NET8 - Reprocessar o arquivo"
        Else
            flemail = True
            Return msgERRO
        End If


    End Function


    Public Function Del_PEDVENDA(ByVal cod_loja As Integer, ByVal num_pedido As Integer, ByVal seq_pedido As Integer)

        Dim CMD As New OracleCommand

        CMD.Parameters.Clear()
        CMD.Connection = ora
        CMD.CommandType = CommandType.StoredProcedure
        CMD.CommandText = "PRODUCAO.PCK_FIL200_NET.PR_LIMPA_PEDIDOS"

        Dim DEP As String = "DEP" & cod_loja.ToString.PadLeft(2, "0")
        CMD.Parameters.Add(New OracleParameter("PM_DEP", OracleDbType.Varchar2, ParameterDirection.Input))
        CMD.Parameters("PM_DEP").Value = DEP

        CMD.Parameters.Add(New OracleParameter("PM_LOJA", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_LOJA").Value = cod_loja.ToString.PadLeft(2, "0")

        CMD.Parameters.Add(New OracleParameter("PM_PED", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_PED").Value = num_pedido ''Left(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 2)

        CMD.Parameters.Add(New OracleParameter("PM_SEQ", OracleDbType.Decimal, ParameterDirection.Input))
        CMD.Parameters("PM_SEQ").Value = seq_pedido ''Mid(strNOME_ARQUIVO, InStr(strNOME_ARQUIVO, ".") - 1, 1)

        '-- cod ERRO
        CMD.Parameters.Add(New OracleParameter("PM_CODERRO", OracleDbType.Int32, ParameterDirection.Output))
        'CMD.Parameters("PM_ERRO").Value = 0

        '-- txt ERRO
        CMD.Parameters.Add(New OracleParameter("PM_TXTERRO", OracleDbType.Varchar2, ParameterDirection.Output))
        CMD.Parameters("PM_TXTERRO").Size = 1000
        CMD.Parameters("PM_TXTERRO").Value = ""

        CMD.ExecuteNonQuery()


    End Function

End Class


