CREATE OR REPLACE PROCEDURE pr_gerpedvda_fabio
       (p_cod_loja        IN      vendas.ped_venda.cod_loja%TYPE,
        p_num_pendente    IN      vendas.ped_venda.num_pendente%TYPE,
        p_seq_pendente    IN      vendas.ped_venda.seq_pendente%TYPE,
        p_fl_tipo_chave   IN      vendas.ped_venda.fl_tipo_chave%TYPE,
        p_cod_loja_out    OUT     pednota_venda.cod_loja%TYPE,
        p_num_pedido      OUT     pednota_venda.num_pedido%TYPE,
        p_seq_pedido      OUT     pednota_venda.seq_pedido%TYPE,
        p_dt_digitacao    OUT     pednota_venda.dt_digitacao%TYPE,
        p_dt_pedido       OUT     pednota_venda.dt_pedido%TYPE,
        p_coderro         OUT     NUMBER,
        p_txterro         OUT     VARCHAR2)
IS
    s_cod_loja                pednota_venda.cod_loja%TYPE;
    s_num_pedido              pednota_venda.num_pedido%TYPE;
    s_seq_pedido              pednota_venda.seq_pedido%TYPE;
    s_vl_contabil             pednota_venda.vl_contabil%TYPE;
    s_vl_ipi                  pednota_venda.vl_ipi%TYPE;
    s_peso_bruto              pednota_venda.peso_bruto%TYPE := 0;
    s_cod_cfo_dest            pednota_venda.cod_cfo_dest%TYPE;
    s_cod_cfo_oper            pednota_venda.cod_cfo_oper%TYPE;
    s_cod_cfo_oper_st         pednota_venda.cod_cfo_oper_st%TYPE;
    s_vl_desp_acess           pednota_venda.vl_desp_acess%TYPE;
    s_bloq_protesto           pednota_venda.bloq_protesto%TYPE;
    s_bloq_juros              pednota_venda.bloq_juros%TYPE;
    s_bloq_duplicata          pednota_venda.bloq_duplicata%TYPE;
    s_bloq_limite             pednota_venda.bloq_limite%TYPE;
    s_bloq_saldo              pednota_venda.bloq_saldo%TYPE;
    s_bloq_cheque             pednota_venda.bloq_cheque%TYPE;
    s_bloq_compra             pednota_venda.bloq_compra%TYPE;
    s_bloq_desconto           pednota_venda.bloq_desconto%TYPE;
    s_bloq_acrescimo          pednota_venda.bloq_acrescimo%TYPE;
    s_bloq_vlfatmin           pednota_venda.bloq_vlfatmin%TYPE;
    s_bloq_item_desc1         pednota_venda.bloq_item_desc1%TYPE;
    s_bloq_item_desc2         pednota_venda.bloq_item_desc2%TYPE;
    s_bloq_item_desc3         pednota_venda.bloq_item_desc3%TYPE;
    s_bloq_frete              pednota_venda.bloq_frete%TYPE;
    s_bloq_clie_novo          pednota_venda.bloq_clie_novo%TYPE;
    w_cod_uf_origem           uf.cod_uf%TYPE;
    w_cod_filial              vendas.ped_venda.cod_filial%TYPE;
    w_tp_pedido               vendas.ped_venda.tp_pedido%TYPE;
    w_tp_dpkblau              vendas.ped_venda.tp_dpkblau%TYPE;
    w_dt_digitacao            vendas.ped_venda.dt_digitacao%TYPE;
    w_dt_pedido               vendas.ped_venda.dt_pedido%TYPE;
    w_cod_nope                vendas.ped_venda.cod_nope%TYPE;
    w_cod_cliente             vendas.ped_venda.cod_cliente%TYPE;
    w_cod_end_entrega         vendas.ped_venda.cod_end_entrega%TYPE;
    w_cod_transp              vendas.ped_venda.cod_transp%TYPE;
    w_cod_repres              vendas.ped_venda.cod_repres%TYPE;
    w_cod_vend                vendas.ped_venda.cod_vend%TYPE;
    w_cod_plano               vendas.ped_venda.cod_plano%TYPE;
    w_cod_banco               vendas.ped_venda.cod_banco%TYPE;
    w_mens_pedido             vendas.ped_venda.mens_pedido%TYPE;
    w_mens_nota               vendas.ped_venda.mens_nota%TYPE;
    w_frete_pago              vendas.ped_venda.frete_pago%TYPE;
    w_qtd_item_pedido         vendas.ped_venda.qtd_item_pedido%TYPE;
    w_vl_contabil             vendas.ped_venda.vl_contabil%TYPE;
    w_ped_aberto              vendas.ped_venda.vl_contabil%TYPE := NULL;
    w_dupl_aberto             vendas.ped_venda.vl_contabil%TYPE := NULL;
    w_dupl_aberto_dia         vendas.ped_venda.vl_contabil%TYPE := NULL;
    w_vl_ipi                  vendas.ped_venda.vl_ipi%TYPE;
    w_pc_desconto             vendas.ped_venda.pc_desconto%TYPE;
    w_pc_desc_suframa         vendas.ped_venda.pc_desc_suframa%TYPE;
    w_pc_acrescimo            vendas.ped_venda.pc_acrescimo%TYPE;
    w_cod_cancel              vendas.ped_venda.cod_cancel%TYPE;
    w_fl_pendencia            vendas.ped_venda.fl_pendencia%TYPE;
    w_fl_dif_icm              vendas.ped_venda.fl_dif_icm%TYPE;
    w_bloq_clie_novo          vendas.ped_venda.bloq_clie_novo%TYPE;
    w_situacao                vendas.ped_venda.situacao%TYPE;
    w_sit_cancel              pednota_venda.situacao%TYPE := 9;
    w_cod_cancelamento        pednota_venda.cod_cancel%TYPE := 9;
    w_vl_frete                pednota_venda.vl_frete%TYPE;
    w_tp_transacao            pednota_venda.tp_transacao%TYPE := 1;
    w_pc_seguro               pednota_venda.pc_seguro%TYPE := 0;
    w_fl_desp_acess           pednota_venda.fl_desp_acess%TYPE := 'S';
    w_cod_fornecedor          pednota_venda.cod_fornecedor%TYPE := NULL;
    w_fl_manut                NUMBER(1,0) := 1;
    w_qtd_repres              NUMBER(4) := 0;
    w_cod_vdr                 pednota_venda.cod_vdr%TYPE := 0;
    w_cod_internet            pednota_venda.cod_internet%TYPE :='N';
    s_st_rs		      VARCHAR2(1);
    s_seq		      NUMBER(1) :=0;
    rs_cod_loja_out           pednota_venda.cod_loja%TYPE;
    rs_num_pedido             pednota_venda.num_pedido%TYPE;
    rs_seq_pedido             pednota_venda.seq_pedido%TYPE;
    rs_dt_digitacao           pednota_venda.dt_digitacao%TYPE;
    rs_dt_pedido              pednota_venda.dt_pedido%TYPE;
    w_compl		      NUMBER(1) :=0;
    w_bloq_credito	      NUMBER(1) :=0;
    w_uf_cd		      uf.cod_uf%TYPE;
    w_uf_entrega	      uf.cod_uf%TYPE;
    w_uf_cliente              uf.cod_uf%TYPE;
    w_cgc_cliente             cliente.cgc%TYPE;
    w_cgc_entrega	      cliente.cgc%TYPE;
    w_sit_entrega	      cliente.situacao%TYPE;
    w_prazo_medio             plano_pgto.prazo_medio%TYPE;
    CURSOR gerpeddpk0 IS
      select cod_filial,tp_pedido,tp_dpkblau,dt_digitacao,dt_pedido,cod_nope,
          cod_cliente,nvl(cod_end_entrega,0) cod_end_entrega,cod_transp,cod_repres,cod_vend,cod_plano,
          cod_banco,mens_pedido,mens_nota,frete_pago,qtd_item_pedido,
          vl_contabil,vl_ipi,pc_desconto,pc_desc_suframa,pc_acrescimo,
          cod_cancel,fl_pendencia,fl_dif_icm,bloq_clie_novo,situacao,
          cod_vdr,cod_internet
        from vendas.ped_venda
        where fl_tipo_chave = p_fl_tipo_chave and
              seq_pendente = p_seq_pendente   and
              num_pendente = p_num_pendente   and
              cod_loja = p_cod_loja FOR UPDATE;
     CURSOR gerpeddpk1 IS
       select vl_frete
         from pednota_venda
         where seq_pedido = s_seq_pedido and
               num_pedido = s_num_pedido and
               cod_loja   = s_cod_loja;
     CURSOR gerpeddpk2 IS
       select count(*)
         from producao.complemento
         where seq_pedido_compl = s_seq_pedido and
               num_pedido_compl = s_num_pedido and
               cod_loja_compl = s_cod_loja;
     CURSOR gerpeddpk3 IS
       Select cod_uf
         from loja a,
         cidade b
         where cod_loja = p_cod_loja and
           a.cod_cidade=b.cod_cidade;
     CURSOR gerpeddpk4 IS
       Select cod_uf,cgc
         from clie_endereco A,
         cidADE B
         where
         sequencia = w_cod_end_entrega and
         tp_endereco = 1 and
         cod_cliente = w_cod_cliente and
         a.cod_cidade = b.cod_cidade;
     CURSOR gerpeddpk5 IS
       Select cod_uf,cgc
         from cliente A,
         cidADE B
         where
         cod_cliente = w_cod_cliente and
         a.cod_cidade = b.cod_cidade;
     CURSOR gerpeddpk6 IS
       Select situacao
       from cliente
       where cgc=w_cgc_entrega and
             situacao=0 ;
     CURSOR gerpeddpk7 IS
       Select prazo_medio
       from plano_pgto
       where cod_plano = w_cod_plano and
             situacao=0 ;


BEGIN
-- ---------------------------------------------------------------------
-- GERA PEDIDO DE VENDA DEFINITIVO ATRAVEZ DA TABELA AUXILIAR
-- ---------------------------------------------------------------------

p_coderro := 0;
p_txterro := NULL;

  OPEN gerpeddpk0;
   FETCH gerpeddpk0 INTO w_cod_filial,w_tp_pedido,w_tp_dpkblau,w_dt_digitacao,
      w_dt_pedido,w_cod_nope,w_cod_cliente,w_cod_end_entrega,w_cod_transp,
      w_cod_repres,w_cod_vend,w_cod_plano,w_cod_banco,w_mens_pedido,
      w_mens_nota,w_frete_pago,w_qtd_item_pedido,w_vl_contabil,w_vl_ipi,
      w_pc_desconto,w_pc_desc_suframa,w_pc_acrescimo,w_cod_cancel,
      w_fl_pendencia,w_fl_dif_icm,w_bloq_clie_novo,w_situacao,w_cod_vdr,
      w_cod_internet;
   IF gerpeddpk0%NOTFOUND  THEN
      ROLLBACK;
      GOTO fim_gerpeddpk;
   END IF;
  CLOSE gerpeddpk0;
  IF w_situacao in (8,9)    THEN
     ROLLBACK;
     GOTO fim_gerpeddpk;
  END IF;
  IF w_cod_repres = 0  THEN w_cod_repres := NULL; END IF;
  IF w_cod_vend = 0  THEN w_cod_vend := NULL; END IF;
  w_cod_cancel := NULL;

 IF w_cod_end_entrega > 0 and w_tp_dpkblau <> 4 THEN

   OPEN gerpeddpk7;
        FETCH gerpeddpk7 INTO w_prazo_medio;
        IF gerpeddpk7%NOTFOUND THEN
          w_prazo_medio := 0;
        END IF;
   CLOSE gerpeddpk7;

   OPEN gerpeddpk3;
     FETCH gerpeddpk3 INTO w_uf_cd;
   CLOSE gerpeddpk3;

   OPEN gerpeddpk4;
       FETCH gerpeddpk4 INTO w_uf_entrega,w_cgc_entrega;
   CLOSE gerpeddpk4;

   OPEN gerpeddpk5;
     FETCH gerpeddpk5 INTO w_uf_cliente,w_cgc_cliente;
   CLOSE gerpeddpk5;

   IF w_cgc_entrega <> w_cgc_cliente THEN

     OPEN gerpeddpk6;
          FETCH gerpeddpk6 INTO w_sit_entrega;
          IF gerpeddpk6%NOTFOUND THEN
            w_sit_entrega := 9;
          END IF;
     CLOSE gerpeddpk6;

     IF w_sit_entrega = 0 THEN
   	w_cod_nope := 'B04';
     END IF;
   ELSE
     w_cod_end_entrega := 0;
   END IF;

 END IF;



  pr_sitpedvda (p_cod_loja,p_num_pendente,p_seq_pendente,p_fl_tipo_chave,
     w_cod_cliente,w_cod_plano,w_cod_repres,w_cod_vend,w_cod_nope,w_tp_pedido,
     w_tp_dpkblau,w_tp_transacao,w_cod_filial,w_pc_desc_suframa,
     w_cod_uf_origem,s_vl_contabil);
  pr_pedvda (p_cod_loja,p_num_pendente,p_seq_pendente,p_fl_tipo_chave,
     w_cod_filial,w_tp_pedido,w_tp_dpkblau,w_dt_digitacao,w_dt_pedido,
     w_cod_nope,w_cod_cliente,w_cod_end_entrega,w_cod_transp,w_cod_repres,
     w_cod_vend,w_cod_plano,w_cod_banco,w_mens_pedido,w_mens_nota,w_frete_pago,
     w_qtd_item_pedido,w_vl_contabil,w_vl_ipi,w_pc_desconto,w_pc_desc_suframa,
     w_pc_acrescimo,w_cod_cancel,w_fl_pendencia,w_fl_dif_icm,w_bloq_clie_novo,
     s_cod_loja,s_num_pedido,s_seq_pedido,w_cod_vdr,w_cod_internet);
  pr_itpedvda (p_cod_loja,p_num_pendente,p_seq_pendente,p_fl_tipo_chave,
     s_num_pedido,s_seq_pedido,w_qtd_item_pedido,w_cod_nope,w_cod_cliente,
     w_cod_uf_origem,w_cod_vdr,s_st_rs,s_seq);
  COMMIT;               -- ---------------------------- commit
-- ---------------------------------------------------------------------
-- CALCULA VALORES DO PEDIDO DEFINITIVO
-- ---------------------------------------------------------------------
  pr_calccomis (s_cod_loja,s_num_pedido,s_seq_pedido,w_tp_pedido,w_dt_pedido,
     w_cod_plano,w_cod_nope,w_cod_vend,w_cod_repres,w_cod_cliente,
     w_cod_uf_origem,w_fl_manut,w_qtd_repres,w_cod_vdr,w_cod_internet);
  s_vl_ipi := w_vl_ipi;

  OPEN gerpeddpk2;
    FETCH gerpeddpk2 INTO w_compl;
    IF gerpeddpk2%NOTFOUND then
       w_compl := 0;
    END IF;
  CLOSE gerpeddpk2;

  IF w_compl <> 0 THEN
      w_fl_desp_acess := 'N';
      w_frete_pago := 'C';
  END IF;

  OPEN gerpeddpk1;
  FETCH gerpeddpk1 INTO w_vl_frete;
    IF gerpeddpk1%NOTFOUND then
      w_vl_frete := 0;
    END IF;
  CLOSE gerpeddpk1;
  IF w_vl_frete = 0 and (w_frete_pago = 'D' or w_frete_pago = 'C') THEN
    pr_calcfrete(s_cod_loja,s_num_pedido,s_seq_pedido,w_cod_cliente,
                 w_cod_transp,w_vl_frete,w_frete_pago);
  END IF;


  pr_calccab (s_cod_loja,s_num_pedido,s_seq_pedido,w_tp_transacao,w_tp_dpkblau,
     w_qtd_item_pedido,w_cod_nope,w_cod_cliente,w_cod_fornecedor,w_cod_transp,
     w_pc_acrescimo,w_pc_desc_suframa,w_pc_desconto,w_vl_frete,w_pc_seguro,
     w_fl_desp_acess,w_cod_uf_origem,s_vl_contabil,s_vl_ipi,s_peso_bruto,
     s_cod_cfo_dest,s_cod_cfo_oper,
     s_cod_cfo_oper_st,s_vl_desp_acess);
  pr_calcbloq (s_cod_loja,s_num_pedido,s_seq_pedido,w_tp_transacao,
     w_tp_dpkblau,w_cod_cliente,w_cod_plano,w_cod_nope,w_cod_transp,
     w_pc_acrescimo,w_pc_desconto,w_frete_pago,s_vl_contabil,w_ped_aberto,
     w_dupl_aberto,w_dupl_aberto_dia,s_bloq_protesto,s_bloq_juros,
     s_bloq_duplicata,s_bloq_limite,s_bloq_saldo,s_bloq_cheque,s_bloq_compra,
     s_bloq_desconto,s_bloq_acrescimo,s_bloq_vlfatmin,s_bloq_item_desc1,
     s_bloq_item_desc2,s_bloq_item_desc3,s_bloq_frete,s_bloq_clie_novo,
     w_cod_vdr);


-- ---------------------------------------------------------------------
-- ATUALIZA PEDNOTA_VENDA
-- ---------------------------------------------------------------------
  update pednota_venda set
         frete_pago = w_frete_pago,
         tp_pedido = w_tp_pedido,
         cod_repres = w_cod_repres,
         vl_contabil = s_vl_contabil,
         vl_ipi = s_vl_ipi,
         peso_bruto = s_peso_bruto,
         cod_cfo_dest = s_cod_cfo_dest,
         cod_cfo_oper = s_cod_cfo_oper,
         cod_cfo_oper_st = s_cod_cfo_oper_st,
         fl_desp_acess = w_fl_desp_acess,
         vl_desp_acess = s_vl_desp_acess,
         bloq_protesto = s_bloq_protesto,
         bloq_juros = s_bloq_juros,
         bloq_duplicata = s_bloq_duplicata,
         bloq_limite = s_bloq_limite,
         bloq_saldo = s_bloq_saldo,
         bloq_cheque = s_bloq_cheque,
         bloq_compra = s_bloq_compra,
         bloq_desconto = s_bloq_desconto,
         bloq_acrescimo = s_bloq_acrescimo,
         bloq_vlfatmin = s_bloq_vlfatmin,
         bloq_item_desc1 = s_bloq_item_desc1,
         bloq_item_desc2 = s_bloq_item_desc2,
         bloq_item_desc3 = s_bloq_item_desc3,
         bloq_frete = s_bloq_frete,
         bloq_clie_novo = s_bloq_clie_novo,
         situacao = w_situacao
    where seq_pedido = s_seq_pedido and
          num_pedido = s_num_pedido and
          cod_loja = s_cod_loja;

  IF s_cod_cfo_oper = 87 or s_cod_cfo_oper = 502 THEN
     update itpednota_venda set
	   cod_trib = 4
	    where seq_pedido = s_seq_pedido and
	          num_pedido = s_num_pedido and
                  cod_loja = s_cod_loja;
  END IF;
  p_cod_loja_out := s_cod_loja;
  p_num_pedido := s_num_pedido;
  p_seq_pedido := s_seq_pedido;
  p_dt_digitacao := w_dt_digitacao;
  p_dt_pedido := w_dt_pedido;


  --insere BOLETO_OP
    IF w_tp_transacao = 1 AND w_tp_dpkblau <> 4 THEN
      producao.pr_insere_boletoop(s_cod_loja,s_num_pedido,s_seq_pedido,w_cod_plano,
                            w_cod_cliente,w_cod_uf_origem,w_cod_transp);
    END IF;

    COMMIT;

    w_bloq_credito :=  PRODUCAO.ft_bloq_cred_op(s_cod_loja,s_num_pedido,s_seq_pedido);
    IF w_bloq_credito > 0 THEN
       update pednota_venda set
         bloq_cheque = 'S'
        where seq_pedido = s_seq_pedido and
              num_pedido = s_num_pedido and
       	      cod_loja = s_cod_loja;
    END IF;




   COMMIT;                 -- ---------------------------- commit


   IF w_cod_nope = 'B02' and w_cod_end_entrega <> 0 and w_tp_dpkblau <> 4 THEN
         pr_cancela (s_cod_loja,s_num_pedido,s_seq_pedido,w_sit_cancel,30);
         GOTO fim_gerpeddpk;
   END IF;

   IF w_cod_nope = 'B04' and w_cod_end_entrega = 0 THEN
      pr_cancela (s_cod_loja,s_num_pedido,s_seq_pedido,w_sit_cancel,29);
      GOTO fim_gerpeddpk;
   END IF;

   IF w_cod_nope = 'B04' and w_prazo_medio < 14 THEN
         pr_cancela (s_cod_loja,s_num_pedido,s_seq_pedido,w_sit_cancel,31);
         GOTO fim_gerpeddpk;
   END IF;


  --SE UF CD <> UF ENTREGA pedido cancelado automaticamente
  IF w_cod_nope='B04' and w_tp_transacao = 1 and w_tp_dpkblau <> 4 and
     ((w_uf_cd <> w_uf_entrega ) or (w_uf_cd <> w_uf_cliente)) THEN
     pr_cancela (s_cod_loja,s_num_pedido,s_seq_pedido,w_sit_cancel,28);
     GOTO fim_gerpeddpk;
  END IF;



  -- --------------------------- CANCELA PEDIDOS COM VL_CONTABIL = 0
  -- desde que n�o seja AUTOZ
  IF s_vl_contabil = 0 AND w_tp_dpkblau <> 4  THEN
     pr_cancela (s_cod_loja,s_num_pedido,s_seq_pedido,w_sit_cancel,
       w_cod_cancelamento);
  END IF;

  IF s_st_rs = 'S' THEN
    pr_gerpedvda_FABIO(s_cod_loja,s_num_pedido,s_seq,1,rs_cod_loja_out,
    rs_num_pedido,rs_seq_pedido,rs_dt_digitacao,rs_dt_pedido,P_CODERRO,P_TXTERRO);
  END IF;

  <<fim_gerpeddpk>>
  NULL;
EXCEPTION
		WHEN OTHERS THEN
    ROLLBACK;
		P_CODERRO := SQLCODE;
		P_TXTERRO := SQLERRM;
END;
/
