VERSION 4.00
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "Consulta Pedido/Nota Fiscal"
   ClientHeight    =   4230
   ClientLeft      =   405
   ClientTop       =   1815
   ClientWidth     =   9450
   Height          =   4920
   Icon            =   "MDIPED.frx":0000
   Left            =   345
   LinkTopic       =   "MDIForm1"
   MouseIcon       =   "MDIPED.frx":030A
   Top             =   1185
   Width           =   9570
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9450
      _version        =   65536
      _extentx        =   16669
      _extenty        =   873
      _stockprops     =   15
      backcolor       =   12632256
      Begin Threed.SSCommand sscmdVendedor 
         Height          =   495
         Left            =   3720
         TabIndex        =   7
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":0614
      End
      Begin Threed.SSCommand SSCommand5 
         Height          =   495
         Left            =   1920
         TabIndex        =   6
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":092E
      End
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   2520
         TabIndex        =   5
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":0C48
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1320
         TabIndex        =   4
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":0F62
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   720
         TabIndex        =   3
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":127C
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _version        =   65536
         _extentx        =   873
         _extenty        =   873
         _stockprops     =   78
         picture         =   "MDIPED.frx":1596
      End
   End
   Begin Threed.SSPanel ssmsg 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   3735
      Width           =   9450
      _version        =   65536
      _extentx        =   16669
      _extenty        =   873
      _stockprops     =   15
      forecolor       =   8388608
      backcolor       =   12632256
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "Arial"
         charset         =   1
         weight          =   400
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      alignment       =   0
   End
   Begin VB.Menu mnuDados 
      Caption         =   "&Dados"
   End
   Begin VB.Menu mnuItens 
      Caption         =   "&Itens"
   End
   Begin VB.Menu mnuTransporte 
      Caption         =   "&Transporte"
   End
   Begin VB.Menu mnuLimpar 
      Caption         =   "&Limpar"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False




Private Sub MDIForm_Load()

On Error GoTo TRATA_ERRO
  
  Call SELECTS
  
  Set orasession = CreateObject("oracleinprocserver.xorasession")
  Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA030/PROD", 0&)
  'Set oradatabase = orasession.OpenDatabase("DESENV", "DESENV/DPK", 0&)
 
  'oradatabase.ExecuteSQL ("alter session set sql_trace=true")
  
  ' carregar codigo do vendedor
    F = FreeFile
    Open "C:\USER.TXT" For Input As #F
    Input #F, cod_vend
    cod_vend = CInt(cod_vend)
    Close #F
   
   
   If Command$ = "V" Then
     MDIForm1.sscmdVendedor.Visible = True
   Else
     MDIForm1.sscmdVendedor.Visible = False
   End If
   
 frmPed.Show
 
 Exit Sub
  
 


TRATA_ERRO:
   frmTrataerro.Top = 500
   frmTrataerro.Left = 1160
   frmTrataerro.Width = 7600
   frmTrataerro.Height = 2100
   frmTrataerro.Show
End Sub

Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = ""
    
End Sub


Private Sub MDIForm_Terminate()

    End
  
End Sub

Private Sub mnuDados_Click()
Call INFORMACOES
End Sub

Private Sub mnuItens_Click()

If frmPed.txtnrpedido.Text = "" Then
    MsgBox "Entre com um pedido", 0, "ATEN��O"
    frmPed.txtnrpedido.SetFocus
    Exit Sub
  Else
    botao = 4
    frmItem.Move 50, 1500, 9400, 5040
  
    Call INFO_ITEM
 End If

End Sub

Private Sub mnuLimpar_Click()

 If frmPed.Grid1.Visible = True Then
   Unload frmPed
   Unload frmDados
   frmPed.Show
   frmPed.txtnrpedido.Text = ""
   frmPed.txtsequencia.Text = ""
   frmPed.txtnrnotafiscal.Text = ""
 End If
 
 If frmPed.Grid2.Visible = True Then
   Unload frmPed
   Unload frmDados
   frmPed.Show
   frmPed.txtnrpedido.Text = ""
   frmPed.txtsequencia.Text = ""
   frmPed.txtnrnotafiscal.Text = ""
 End If
 
   
 Unload frmPed
 Unload frmDados
 Unload frmTransp
 frmPed.Show


End Sub

Private Sub mnuSair_Click()

 Set frmPed = Nothing
 Set frmDados = Nothing
 Set frmTransp = Nothing
 Set frmItem = Nothing
 Set frmSobre = Nothing
 Set frmVend = Nothing
 Set frmTrataerro = Nothing
  
 End

End Sub


Private Sub mnuSobre_Click()
  
  frmSobre.Show
  
End Sub

Private Sub mnuTransporte_Click()

  If frmPed.txtnrpedido.Text = "" Then
    MsgBox "Entre com um pedido", 0, "ATEN��O"
    Exit Sub
  Else
    botao = 3
    frmTransp.Move 50, 1500, 9400, 5040
  
    Call INFO_TRANSP
  
    If oradynaset.EOF Then
      Exit Sub
    Else
      frmTransp.Show
    End If
  
  End If



End Sub

Private Sub sscmdvendedor_Click()

   frmPed.Grid1.Visible = False
   frmPed.Grid2.Visible = False
   Unload frmPed
   Unload frmDados
   Unload frmItem
   Unload frmTransp
   frmPed.Show
  
  
  frmVend.lblvend.Caption = cod_vend
  frmVend.Show 1

End Sub

Private Sub sscmdvendedor_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = "Pedido por Vendedor"
  
End Sub


Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

MDIForm1.ssmsg.Caption = "Dados do Pedido"

End Sub

Private Sub SSCommand2_Click()
 
 If frmPed.txtnrpedido.Text = "" Then
    MsgBox "Entre com um pedido", 0, "ATEN��O"
    frmPed.txtnrpedido.SetFocus
    Exit Sub
  Else
    botao = 4
    frmItem.Move 50, 1500, 9400, 5040
  
    Call INFO_ITEM
    
  End If
  
End Sub

Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  
  MDIForm1.ssmsg.Caption = "Consulta Itens"

End Sub


Private Sub SSCommand3_Click()

  If frmPed.txtnrpedido.Text = "" Then
    MsgBox "Entre com um pedido", 0, "ATEN��O"
    Exit Sub
  Else
    botao = 3
    frmTransp.Move 50, 1500, 9400, 5040
  
    Call INFO_TRANSP
  
    If oradynaset.EOF Then
      Exit Sub
    Else
      frmTransp.Show
    End If
  
  End If

End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = "Dados de Transporte"

End Sub


Private Sub SSCommand4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = "Sair do Programa"

End Sub

Private Sub SSCommand5_Click()
 
 If frmPed.Grid1.Visible = True Then
   Unload frmPed
   Unload frmDados
   frmPed.Show
   frmPed.txtnrpedido.Text = ""
   frmPed.txtsequencia.Text = ""
   frmPed.txtnrnotafiscal.Text = ""
 End If
 
 If frmPed.Grid2.Visible = True Then
   Unload frmPed
   Unload frmDados
   frmPed.Show
   frmPed.txtnrpedido.Text = ""
   frmPed.txtsequencia.Text = ""
   frmPed.txtnrnotafiscal.Text = ""
 End If
 
   
 Unload frmPed
 Unload frmDados
 Unload frmTransp
 frmPed.Show
   
 
End Sub


Private Sub SSCommand1_Click()
  
  Call INFORMACOES
  
End Sub

Private Sub SSCommand4_Click()
  
 Set frmPed = Nothing
 Set frmDados = Nothing
 Set frmTransp = Nothing
 Set frmItem = Nothing
 Set frmSobre = Nothing
 Set frmVend = Nothing
 Set frmTrataerro = Nothing
  
 End
 
End Sub


Private Sub SSCommand5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = "Limpar a Tela"

End Sub


Private Sub SSCommand6_Click()

  MDIForm1.ssmsg.Caption = "Pedido por Vendedor"

End Sub

Private Sub ssmsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = ""
  
End Sub


Private Sub SSPanel1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  MDIForm1.ssmsg.Caption = ""

End Sub


