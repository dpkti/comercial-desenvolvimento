VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmLig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Resultado do Contato"
   ClientHeight    =   1500
   ClientLeft      =   1665
   ClientTop       =   2025
   ClientWidth     =   4320
   Icon            =   "frmLig.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1500
   ScaleWidth      =   4320
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      Caption         =   "COMENT�RIO"
      ForeColor       =   &H000000FF&
      Height          =   2835
      Left            =   0
      TabIndex        =   6
      Top             =   1770
      Width           =   4215
      Begin VB.TextBox txtComentario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1875
         Left            =   90
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   240
         Width           =   4035
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "RESULTADO"
      ForeColor       =   &H000000FF&
      Height          =   705
      Left            =   15
      TabIndex        =   3
      Top             =   765
      Width           =   4245
      Begin VB.ComboBox cboresultado 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   135
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   225
         Width           =   3945
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "LIGA��O"
      ForeColor       =   &H000000FF&
      Height          =   735
      Left            =   15
      TabIndex        =   0
      Top             =   15
      Width           =   3075
      Begin VB.OptionButton Option1 
         Appearance      =   0  'Flat
         Caption         =   "ATIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   270
         TabIndex        =   2
         Top             =   315
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Appearance      =   0  'Flat
         Caption         =   "RECEPTIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1530
         TabIndex        =   1
         Top             =   315
         Width           =   1455
      End
   End
   Begin Bot�o.cmd SSCommand1 
      Height          =   705
      Left            =   3300
      TabIndex        =   5
      Top             =   30
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLig.frx":08CA
      PICN            =   "frmLig.frx":08E6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmLig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    On Error GoTo Trata_Erro

    Dim i As Long
    
    SQL = "Select * From Representante where Cod_repres = " & sCOD_VEND & " And Tipo = 'F'"
    Set ss1 = dbAccess2.CreateSnapshot(SQL)
    
    If ss1.EOF = False Then 'Ent�o � Promotor
    
        Me.Height = 5220 'Mostra a op��o de comentario
        Me.SSCommand1.ToolTipText = "Gravar informa��es de suas visitas."
        
        Set ss1 = Nothing
        Frame1.Visible = False
        frmLig.Caption = ".:: Resultado das Visitas ::."
        SQL = "Select * From Status_Visita"
        Set ss1 = dbAccess2.CreateSnapshot(SQL)
        
        If ss1.BOF And ss1.EOF Then
            MsgBox "N�o h� resultados cadastrados!", vbInformation, "Aten��o"
            Exit Sub
        End If
        ss1.MoveLast
        ss1.MoveFirst
        For i = 1 To ss1.RecordCount
            cboresultado.AddItem Format(ss1!COD_STATUS, "00") & " - " & ss1!Status
            ss1.MoveNext
        Next
    Else 'Representante ou Paac
        SQL = "SELECT * FROM RESULTADO "
        Set ss1 = dbAccess2.CreateSnapshot(SQL)
        FreeLocks
    
        If ss1.BOF And ss1.EOF Then
            MsgBox "N�o h� resultados cadastrados!", vbInformation, "Aten��o"
            Exit Sub
        End If
        ss1.MoveLast
        ss1.MoveFirst
    
        For i = 1 To ss1.RecordCount
            cboresultado.AddItem Format(ss1!cod_resultado, "00") & " - " & ss1!desc_resultado
            ss1.MoveNext
        Next
    End If
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Load FrmLig" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub SSCommand1_Click()
    On Error GoTo Trata_Erro
    Dim ProxSeq As Integer
    Dim CNPJ As Boolean
    Dim vResp As Integer
    Dim vCGC As String
    Dim vFlag As Boolean
    Dim TIPO As Integer
    Dim ss2 As Snapshot
    Dim ss3 As Snapshot
    Dim vObjDtFat As Snapshot
    
    vFlag = True
    
    SQL = "Select * From Representante where Cod_repres = " & sCOD_VEND & "And Tipo = 'F'"
    Set ss1 = dbAccess2.CreateSnapshot(SQL)
    
    If Trim(frmLig.cboresultado) = "" Then
        MsgBox "Selecione um resultado para gravar", vbInformation, "Aten��o"
        Exit Sub
    ElseIf frmVenda.txtCOD_CLIENTE.Text = "" Then
        vFlag = False
        vResp = MsgBox("Este � um novo contato?", vbInformation + vbYesNo, ":: Aten��o ::")
        If vResp = 7 Then
            MsgBox "Digite o c�digo do cliente.", vbInformation, "Aten��o"
            Unload Me
            Exit Sub
        Else
DeNovo:
            vCGC = InputBox("Por favor, informe o numero do CGC do cliente." & vbCrLf & "Por favor preencher no seguinte formato: 00.000.000/0000-00", ":: Preencha o campo ::")
            If vCGC <> "" Then
                CNPJ = IsCNPJ(vCGC)
                If CNPJ = False Then
                    MsgBox "Erro de Valida��o do CGC do cliente." & vbCrLf & "Por favor digite novamente.", vbCritical, ":: Erro ::"
                    GoTo DeNovo
                End If
            Else
               Exit Sub
            End If
        End If
    End If
    'eduardo - 30/07/2008
    
    If ss1.EOF = False Then 'Ent�o � Promotor
        If vFlag = True Then
            SQL = "SELECT VISITA_PROMOTOR.CGC, VISITA_PROMOTOR.DT_VISITA_AGENDA, "
            SQL = SQL & " VISITA_PROMOTOR.COD_PROMOTOR , VISITA_PROMOTOR.DT_RESULTADO, VISITA_PROMOTOR.COD_STATUS,"
            SQL = SQL & " VISITA_PROMOTOR.COMENTARIO_GERENTE , VISITA_PROMOTOR.COMENTARIO_PROMOTOR"
            SQL = SQL & " From VISITA_PROMOTOR, DATAS, CLIENTE"
            SQL = SQL & " WHERE VISITA_PROMOTOR.CGC=CLIENTE.CGC AND "
            SQL = SQL & " VISITA_PROMOTOR.DT_VISITA_AGENDA=DATAS.DT_FATURAMENTO AND CLIENTE.CGC = " & frmVenda.txtCGC.Text
            Set ss2 = dbAccess2.CreateSnapshot(SQL)
        
            Set vObjDtFat = Nothing
            SQL = "Select Dt_Faturamento From Datas"
            Set vObjDtFat = dbAccess2.CreateSnapshot(SQL)
        
            If ss2.EOF = False Then
                'Update
                SQL = "Update Visita_Promotor set Cod_Promotor = " & sCOD_VEND & ", " & _
                " Dt_Resultado = #" & Format(vObjDtFat!dt_faturamento, "MM/DD/YYYY") & "#, " & _
                " Cod_Status = " & Val(cboresultado.Text) & ", " & _
                " Comentario_Promotor = '" & txtComentario.Text & "' Where Cgc = " & ss2!cgc & " And Dt_Visita_Agenda = #" & Format(ss2!DT_VISITA_AGENDA, "MM/DD/YYYY") & "#"
                dbAccess2.Execute SQL, dbFailOnError
                
                MsgBox "Altera��o dos dados realizado com sucesso.", vbInformation, ".: Visita Promotor :."
                Unload Me
            Else
                'Insert
                SQL = "Insert Into Visita_Promotor(CGC, DT_VISITA_AGENDA, COD_PROMOTOR, DT_RESULTADO, COD_STATUS, COMENTARIO_GERENTE, COMENTARIO_PROMOTOR)Values( " & _
                frmVenda.txtCGC.Text & _
                ", #" & Format(vObjDtFat!dt_faturamento, "MM/DD/YYYY") & _
                "#, " & sCOD_VEND & _
                ", #" & Format(vObjDtFat!dt_faturamento, "MM/DD/YYYY") & _
                "#, " & Val(cboresultado.Text) & _
                ", Null " & _
                ", '" & txtComentario.Text & "')"
                dbAccess2.Execute SQL, dbFailOnError
                MsgBox "Inclus�o dos dados realizado com sucesso.", vbInformation, ".: Visita Promotor :."
                Unload Me
            End If
        Else
            SQL = "Select Dt_Faturamento From Datas"
            Set vObjDtFat = dbAccess2.CreateSnapshot(SQL)
            
            'Insert
            SQL = "Insert Into Visita_Promotor(CGC, DT_VISITA_AGENDA, COD_PROMOTOR, DT_RESULTADO, COD_STATUS, COMENTARIO_GERENTE, COMENTARIO_PROMOTOR)Values( " & _
            vCGC & _
            ", #" & Format(vObjDtFat!dt_faturamento, "MM/DD/YYYY") & _
            "#, " & sCOD_VEND & _
            ", #" & Format(vObjDtFat!dt_faturamento, "MM/DD/YYYY") & _
            "#, " & Val(cboresultado.Text) & _
            ", Null " & _
            ", '" & txtComentario.Text & "')"
            dbAccess2.Execute SQL, dbFailOnError
            MsgBox "Inclus�o dos dados realizado com sucesso.", vbInformation, ".: Visita Promotor :."
            Unload Me
        End If
    Else
        
        SQL = "SELECT MAX(SEQUENCIA) AS SEQ FROM CONTATO "
        Set ss1 = dbAccess2.CreateSnapshot(SQL)
        FreeLocks
        If ss1!seq = "" Then
            ProxSeq = 1
        Else
            ProxSeq = IIf(IsNull(ss1!seq), 1, ss1!seq + 1)
        End If
    
        If Option1.Value = True Then
            TIPO = 1
        Else
            TIPO = 2
        End If
    
        Dim Data As String
        Data = Format(CStr(Date), "m/d/yyyy")
        SQL = "INSERT INTO CONTATO VALUES (" & ProxSeq & "," & frmVenda.txtCOD_CLIENTE & ", '" & Data & "' " & _
        "," & sCOD_VEND & "," & TIPO & "," & Mid(cboresultado, 1, 2) & ",0)"
        dbAccess2.Execute SQL
        FreeLocks
        MsgBox "Resultado cadastrado com sucesso!"
        Call frmVenda.Consulta_Ligacoes
        Unload Me
    End If
    
Trata_Erro:
    If Err.Number = 3022 Then
        MsgBox "J� existe uma visita cadastrada para este CGC no sistema.", vbInformation, " :: Duplicidade de Cadastro ::"
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub txtCod_Vendedor_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub
