VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmAgendaPromotor 
   Caption         =   " :: Agenda Promotor ::"
   ClientHeight    =   6885
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10965
   Icon            =   "frmAgendaPromotor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6885
   ScaleWidth      =   10965
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar pbAgenda 
      Height          =   255
      Left            =   6570
      TabIndex        =   15
      Top             =   6630
      Width           =   4395
      _ExtentX        =   7752
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   13
      Top             =   6600
      Width           =   10965
      _ExtentX        =   19341
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            TextSave        =   "13:54"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informa��es:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   1095
      Left            =   30
      TabIndex        =   1
      Top             =   60
      Width           =   8655
      Begin VB.Frame Frame2 
         Height          =   855
         Left            =   4230
         TabIndex        =   11
         Top             =   150
         Width           =   2385
         Begin VB.OptionButton optResultado 
            Caption         =   "S/ Data de Resultado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   180
            TabIndex        =   16
            Top             =   510
            Width           =   2115
         End
         Begin VB.OptionButton optResultado 
            Caption         =   "C/ Data de Resultado"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   180
            TabIndex        =   12
            Top             =   210
            Width           =   2115
         End
      End
      Begin Bot�o.cmd cmdConsulta 
         Height          =   615
         Left            =   6990
         TabIndex        =   10
         ToolTipText     =   "Consultar"
         Top             =   330
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAgendaPromotor.frx":08CA
         PICN            =   "frmAgendaPromotor.frx":08E6
         PICH            =   "frmAgendaPromotor.frx":11C0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtCGCAgenda 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2340
         MaxLength       =   14
         TabIndex        =   9
         Top             =   270
         Width           =   1380
      End
      Begin VB.TextBox txtCOD_CLIENTE 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   690
         MaxLength       =   6
         TabIndex        =   7
         Top             =   270
         Width           =   1170
      End
      Begin MSComCtl2.DTPicker dtpDe 
         Height          =   315
         Left            =   690
         TabIndex        =   2
         Top             =   630
         Width           =   1155
         _ExtentX        =   2037
         _ExtentY        =   556
         _Version        =   393216
         Format          =   66584577
         CurrentDate     =   39657
      End
      Begin MSComCtl2.DTPicker dtpAte 
         Height          =   315
         Left            =   2340
         TabIndex        =   3
         Top             =   630
         Width           =   1395
         _ExtentX        =   2461
         _ExtentY        =   556
         _Version        =   393216
         Format          =   66584577
         CurrentDate     =   39657
      End
      Begin Bot�o.cmd cmdImprimir 
         Height          =   615
         Left            =   7890
         TabIndex        =   18
         ToolTipText     =   "Imprimir agenda"
         Top             =   330
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   8388608
         FCOLO           =   8388608
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAgendaPromotor.frx":1E9A
         PICN            =   "frmAgendaPromotor.frx":1EB6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblCGC 
         AutoSize        =   -1  'True
         Caption         =   "CGC:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1920
         TabIndex        =   8
         Top             =   330
         Width           =   390
      End
      Begin VB.Label lblCliente 
         AutoSize        =   -1  'True
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   60
         TabIndex        =   6
         Top             =   330
         Width           =   615
      End
      Begin VB.Label lblAte 
         AutoSize        =   -1  'True
         Caption         =   "At�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1920
         TabIndex        =   5
         Top             =   720
         Width           =   360
      End
      Begin VB.Label lblDe 
         AutoSize        =   -1  'True
         Caption         =   "De:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   390
         TabIndex        =   4
         Top             =   720
         Width           =   285
      End
   End
   Begin MSComctlLib.ListView lsvAgendaPromotor 
      Height          =   5205
      Left            =   30
      TabIndex        =   0
      Top             =   1290
      Width           =   10905
      _ExtentX        =   19235
      _ExtentY        =   9181
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   8
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Cod Cliente"
         Object.Width           =   2363
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Cliente"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Dt Agendada"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Pseudonimo"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Dt_Resultado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Status"
         Object.Width           =   4480
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Comentario Promotor"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Comentario Gerente"
         Object.Width           =   5362
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   45
      Left            =   -30
      TabIndex        =   14
      Top             =   1200
      Width           =   15375
      _ExtentX        =   27120
      _ExtentY        =   79
      Color1          =   8388608
      Color2          =   16777215
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   825
      Left            =   10170
      TabIndex        =   17
      ToolTipText     =   "Sair"
      Top             =   210
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAgendaPromotor.frx":2B90
      PICN            =   "frmAgendaPromotor.frx":2BAC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmAgendaPromotor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdConsulta_Click()
          Dim SQL As String
          Dim Rs As Recordset
          Dim sList As Snapshot
          Dim Litem As ListItem
          Dim i As Integer
          
1         On Error GoTo TrataErro
          

2         lsvAgendaPromotor.ListItems.Clear
3         pbAgenda.Value = 0

4         SQL = "SELECT DISTINCTROW CLIENTE.COD_CLIENTE, CLIENTE.NOME_CLIENTE, REPRESENTANTE.pseudonimo,VISITA_PROMOTOR.DT_VISITA_AGENDA, VISITA_PROMOTOR.DT_RESULTADO, STATUS_VISITA.STATUS, VISITA_PROMOTOR.COMENTARIO_PROMOTOR, VISITA_PROMOTOR.COMENTARIO_GERENTE "
5         SQL = SQL & " FROM (CLIENTE RIGHT JOIN (VISITA_PROMOTOR LEFT JOIN STATUS_VISITA ON VISITA_PROMOTOR.COD_STATUS = STATUS_VISITA.COD_STATUS) ON CLIENTE.CGC = VISITA_PROMOTOR.CGC) INNER JOIN REPRESENTANTE ON VISITA_PROMOTOR.COD_PROMOTOR = REPRESENTANTE.cod_repres"

6         SQL = SQL & " Where "
7         SQL = SQL & IIf(txtCOD_CLIENTE.Text = "", "", " CLIENTE.COD_CLIENTE = " & txtCOD_CLIENTE.Text & " AND ")
          
8         If optResultado(1).Value = True Then
9             SQL = SQL & " VISITA_PROMOTOR.DT_RESULTADO Is Null AND "
10        Else
11            SQL = SQL & " VISITA_PROMOTOR.DT_RESULTADO Is NOT Null AND "
12        End If
13        SQL = SQL & " VISITA_PROMOTOR.DT_VISITA_AGENDA >=#" & Format(dtpDe.Value, "MM/DD/YYYY") & "# And "
14        SQL = SQL & " VISITA_PROMOTOR.DT_VISITA_AGENDA <=#" & Format(dtpAte.Value, "MM/DD/YYYY") & "#"
          
15        Set Rs = dbAccess2.OpenRecordset(SQL)
16        If Rs.EOF = False Then
17            Rs.MoveLast
18            Rs.MoveFirst
19            pbAgenda.Max = Rs.RecordCount
              
20            For i = 0 To Rs.RecordCount - 1
21                Set Litem = lsvAgendaPromotor.ListItems.Add
22                Litem = "" & Rs!Cod_cliente
23                Litem.SubItems(1) = "" & UCase(Rs!NOME_CLIENTE)
24                Litem.SubItems(2) = "" & UCase(Rs!DT_VISITA_AGENDA)
25                Litem.SubItems(3) = "" & UCase(Rs!pseudonimo)
26                Litem.SubItems(4) = "" & UCase(Rs!DT_RESULTADO)
27                Litem.SubItems(5) = "" & UCase(Rs!Status)
28                Litem.SubItems(6) = "" & UCase(Rs!COMENTARIO_PROMOTOR)
29                Litem.SubItems(7) = "" & UCase(Rs!COMENTARIO_GERENTE)
30                Rs.MoveNext
31                pbAgenda.Value = i + 1
32            Next
33        Else
34            If Me.Visible = True Then
35                MsgBox "N�o existe resultado para este filtro.", vbInformation
36            End If
37        End If
38        pbAgenda.Value = 0
39        Set Rs = Nothing

TrataErro:
40        If Err.Number <> 0 Then
41            MsgBox "Sub cmdConsulta_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
42        End If
End Sub

Private Sub cmdImprimir_Click()
          Dim sTexto As String
          Dim vResp As Integer
          Dim sNome As String

1         On Error GoTo TrataError

2         If lsvAgendaPromotor.ListItems.Count = 0 Then
3             MsgBox "Impossivel executar esta opera��o. A listagem esta vazia.", vbCritical, "Aten��o"
4             Exit Sub
5         Else
6             sNome = lsvAgendaPromotor.ListItems(1).SubItems(3) & vbCrLf
7             For i = 1 To lsvAgendaPromotor.ListItems.Count
8                 sTexto = sTexto & "Data Agendada:  " & Space(5) & lsvAgendaPromotor.ListItems.Item(i).SubItems(2) & vbCrLf
9                 sTexto = sTexto & "C�digo:         " & Space(5) & lsvAgendaPromotor.ListItems.Item(i) & vbCrLf
10                sTexto = sTexto & "Cliente:        " & Space(5) & lsvAgendaPromotor.ListItems.Item(i).SubItems(1) & vbCrLf
11                sTexto = sTexto & "Data Resultado: " & Space(5) & lsvAgendaPromotor.ListItems.Item(i).SubItems(4) & vbCrLf
12                sTexto = sTexto & "Status:         " & Space(5) & lsvAgendaPromotor.ListItems.Item(i).SubItems(5) & vbCrLf
13                sTexto = sTexto & "Coment�rio:     " & Space(5) & lsvAgendaPromotor.ListItems.Item(i).SubItems(6) & vbCrLf
14                sTexto = sTexto & vbCrLf
15            Next
16        End If
17        vResp = MsgBox("Antes de imprimir pense em sua responsabilidade e compromisso com o MEIO AMBIENTE!", vbOKCancel + vbInformation, " :: Aten��o ::")
18        If vResp <> 2 Then
19            Printer.Print
20            Printer.FontName = "Courier"
21            Printer.FontSize = 10
22            Printer.Print "-----------------------------------------------------------------------------------------------"
23            Printer.Print " AGENDA PROMOTOR: " & Format(sCOD_VEND, "0000") & " - " & UCase(sNome)
24            Printer.Print "-----------------------------------------------------------------------------------------------"
25            Printer.Print
26            Printer.FontSize = 8
27            Printer.Print sTexto
28            Printer.EndDoc
29        End If

TrataError:
30        If Err.Number <> 0 Then
31            MsgBox "Sub cmdImprimir_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
32        End If
End Sub

Private Sub cmdSair_Click()
1         Unload Me
End Sub

Private Sub Form_Load()
1         dtpDe = Now - 30
2         dtpAte = Now
3         optResultado(1).Value = True
4         cmdConsulta_Click
End Sub

'EDUARDO - 30/07/2008
Private Sub lsvAgendaPromotor_ItemClick(ByVal Item As MSComctlLib.ListItem)
1         If Item.SubItems(5) = "" Then
2             frmVenda.txtCOD_CLIENTE = ""
3             frmVenda.txtCOD_CLIENTE = Item
4             frmVenda.txtCOD_CLIENTE_LostFocus
5             Unload Me
6         End If
End Sub

Private Sub optResultado_LostFocus(Index As Integer)
1         If optResultado(0).Value = True Then
2             optResultado(0).ForeColor = &H8000000D
3             optResultado(1).ForeColor = &H0&
4         Else
5             optResultado(1).ForeColor = &H8000000D
6             optResultado(0).ForeColor = &H0&
7         End If
End Sub

Private Sub txtCOD_CLIENTE_DblClick()
1         frmClienteFimPedido.Show vbModal
2         If txtResposta <> "0" Then
3             txtCOD_CLIENTE.Text = txtResposta
4             txtCOD_CLIENTE.DataChanged = True
5             Call txtCOD_CLIENTE_LostFocus
6         End If
End Sub

Private Sub txtCOD_CLIENTE_LostFocus()
          'Fazer um select trazendo o CGC do cliente para preencher o campo
          Dim SQL As String
          Dim sAgenda As Snapshot
          
1         If txtCOD_CLIENTE.Text <> "" Then
2             SQL = "Select * from Cliente where Cod_Cliente = " & txtCOD_CLIENTE.Text
3             Set sAgenda = dbAccess2.CreateSnapshot(SQL)
4             If sAgenda.EOF = False Then
5                 txtCGCAgenda.Text = sAgenda!cgc
6             End If
7         End If
End Sub
