Attribute VB_Name = "Module2"
'---------------------------------------------------------------------------------------
' Module    : Module2
' Author    : c.samuel.oliveira
' Date      : 01/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------

Option Explicit

'Eduardo - 05/08/2005
Public strTP_Empresa As String * 1
Public strBase_Red As String * 1
Public bdif_ICMS As Byte

'Declarar variaveis globais
Global lngNUM_COTACAO As Long   'NUMERO DE COTACAO
Global lngNUM_PEDIDO As Long    'NUMERO DE PEDIDO
Public ssItemCot As Snapshot
Public SQL As String
Public SQL2 As String
Public lngCOD_LOJA As Long      'codigo do deposito
Global sCOD_VEND As Long        'CODIGO DE VENDEDOR
Global sFILIAL_VEND As Long     'FILIAL DO VENDEDOR
Public fil As Integer
Public FIL1 As Integer
Public Cod_Filial As Integer
Public Sigla As String
Public lngNUM_PENDENTE As Long
Public fl_existe_pedido
Public fl_existe_cotacao
Public DepTransf As Integer
Public UF As String
Public FL_TRANSF As String
Public ss8 As Snapshot
Public Data_atual As Date
Public dDia_Util As Date
Public dDt_Real As Date

Global strPath As String        'PATH DOS DADOS NA REDE
Global strPath_Envio As String
Public strPath_Msg As String
Global strPathUsuario As String 'PATH DO USUARIO
Global dbAccess As Database     'BANCO DE DADOS ACCESS
Global dbAccess2 As Database
Global dbAccess3 As Database
Global dbAccess4 As Database    'TI-3952
Public dia_atua As Integer      'indica o n�mero de dias que est�o sem atualizacao
Public flag_avista
Global lngCOD_CLIENTE As Long   'CODIGO DO CLIENTE - DPK 24 HORAS
Global lngCOD_REPRES As Long
Global bOPERACAO As Byte        'OPERACAO (1,3,5 OU 7) - DPK 24 HORAS
Global strRETORNO As String     'RETORNO (V OU F) - DPK 24 HORAS
Global lngCODIGO As Long        'CODIGO ( cotacao ou pedido ) - DPK 24 HORAS
Public qtd As Byte
Public FILIAL_PED As String     'FILIAL QUE SER� UTILIZADA NO PEDIDO,
'� EXTRAIDA DA TABELA DEPOSITO

Public uf_origem As String        'uf do deposito onde sair� a mercadoria
Public uf_destino As String       'UF DO CLIENTE
Public uf_filial As String
Public class_fiscal As String    'classificacao do item
Public tipo_cliente As String     'categoria do cliente
Public item_grupo As Integer      'grupo do item
Public item_subgrupo As Integer   'subgrupo do item
Public deposito_default As String 'deposito default para visao
Public pc_icm As Double           'percentual de ICMS do estado destino

Public strDesc_auto As String
Public strCaract As String

Public strdesc
Public cont As Long

Public tot_grdfabr As Integer

Public tp_cliente As String
Public TpCliente As String

Public counter As Long                          'contador de registros da tabela de venda
Public tabela(1 To 2, 0 To 2) As String         'nome da tabela (ocorrencia,tipo tabela)
Public data_vig(1 To 2, 0 To 2, 1 To 5) As Date ' data de vigencia
Public seq_atu(0 To 2) As Byte                  ' sequencia do desconto atual
Public seq_ant(0 To 2) As Byte                  ' sequencia do desconto anterior
Public I As Integer                             ' �ndice que indica tipo de tabela (0,1,2)
Public j As Integer                             ' �ndica que indica ocorrencia de preco (1,2)
Public w As Integer                             ' �ndice que indica ocorrencia da data (1 a 5)
Public ocor_atu(0 To 2) As Byte                 ' ocorrencia de preco atual
Public ocor_ant(0 To 2) As Byte                 'ocorrencia de preco anterior

Public forn As Long
Public pesq As String
Public data_real As Date
Public contador As Long

Public Operacao As String
Public num_arq As Integer
Public strEmail_To As String
Public strEmail_from As String
Public bCod_Trib
Public strFl_Dif_Icms
Public strFl_Adicional

Public ss3 As Recordset
Public ss7 As Recordset

Public StrTipo_Desconto
Public Tipo_Usuario
Public CODUF2

'-- VARI�VEIS UTILIZADAS PARA C�LCULO DE ICMS RETIDO --
Global p_cod_loja As Integer
Global p_cod_cliente As Long
Global p_cod_dpk As Long
Global p_cod_trib As Integer
Global p_preco_liquido As Double
Global w_cod_procedencia As Integer
Global w_cod_uf_origem As String
Global w_cod_uf_destino As String
Global w_inscr_estadual As String
Global w_fl_cons_final As String
Global w_tp_cliente As String
Global w_vl_baseicm1 As Double
Global w_vl_baseicm2 As Double
Global w_vl_basemaj_1 As Double
Global w_vl_basemaj_3 As Double
Global w_cliente_antec As String
Global w_cod_mensagem As Integer
Global w_cod_mensagem_fiscal As Integer
Global w_pc_mg_lucro_antec As Double
Global w_pc_margem_lucro As Double
Global w_class_antec As Integer
Global w_class_antec_RJ As Integer
Global w_class_fiscal As Double
Global w_inscr_suframa As String
Global w_vl_baseicm1_ret As Double
Global w_vl_baseicm2_ret As Double
Global w_vl_base_1 As Double
Global w_vl_base_1_aux As Double
Global w_vl_base_2 As Double
Global w_vl_base_2_aux As Double
Global w_vl_base_3 As Double
Global w_vl_baseisen As Double
Global w_vl_baseisen_aux As Double
Global w_vl_baseisen_aux1 As Double
Global w_vl_base_5 As Double
Global w_vl_base_6 As Double
Global w_vl_baseoutr As Double
Global w_pc_aliq_interna As Double
Global w_pc_aliq_interna_me As Double
Global w_pc_icm As Double
Global w_vl_icmretido As Double

Public lngNUM_MIX As Long 'TI-3952

'30/09/05
Global w_cod_trib_novo As Double
Global w_cod_trib_revendedor As Double
Global w_cod_trib_inscrito As Double
Global w_cod_trib_isento As Double

'------- Jairo Almeida - Linhas comentadas Atendendo � solicita��o DUM SDS2405 - 12/01/2012 ---------
'Public Sub CONTROLE_VERSAO()

 '   SQL = "select VERSAO "
 '   SQL = SQL & " from CONTROLE "
 '   SQL = SQL & " where  NOME_PROGRAMA='FIL030' "

    'Set ss = dbAccess2.CreateSnapshot(SQL)
    'FreeLocks


    'Verificar se o sistema est� atualizado ou se � s�bado e domingo.
    'If ss!Versao <> 3 Then
    '    MsgBox "Sistema N�o Pode Ser Executado. Vers�o de Sistema Incorreta!!!", _
    '            vbExclamation, "Aten��o"
    '    ss.Close
    '    dbAccess.Close
    '    End
 '   End If
 '------------------------------------------------------------------------------------------------------
 Public vCommand() As String 'TI-4405

'End Sub

Public Sub LiberaMemoria()
    Set frmAplicacao = Nothing
    Set frmGerarPedido = Nothing
    Set frmFimCotacao = Nothing
    Set frmFimPedido = Nothing
    Set frmAgenda = Nothing
    Set frmBanco = Nothing
    Set frmClienteFimPedido = Nothing
    Set frmListaCotacao = Nothing
    Set frmFornecedor = Nothing
    Set frmPlano = Nothing
    Set frmRepresentante = Nothing
    Set frmTransp = Nothing
    Set frmVendedor = Nothing
    Set frmQtdItem = Nothing
    frmVenda.Label1.Visible = False
    contador = 0
    qtd = 0
    Unload frmFabrica
    Unload frmVenda
    frmVenda.Show 1
End Sub


Public Sub TABELA_VENDA()

    On Error GoTo Trata_Erro

          Dim ssTabela As Snapshot

          'CARREGA TABELA DE VENDA
1         SQL = "select tabela_venda, tp_tabela, ocorr_preco,"
2         SQL = SQL & " dt_vigencia1,"
3         SQL = SQL & " dt_vigencia2,"
4         SQL = SQL & " dt_vigencia3,"
5         SQL = SQL & " dt_vigencia4,"
6         SQL = SQL & " dt_vigencia5 "
7         SQL = SQL & " from tabela_venda "
8         SQL = SQL & " order by ocorr_preco"

9         Set ssTabela = dbAccess2.CreateSnapshot(SQL)

10        If ssTabela.EOF And ssTabela.BOF Then
              'setar cursor
11            Screen.MousePointer = vbDefault
12            ssTabela.Close
13            Exit Sub
14        End If

15        ssTabela.MoveFirst

16        For counter = 1 To ssTabela.RecordCount
17            tabela(ssTabela!ocorr_preco, ssTabela!tp_tabela) = ssTabela("tabela_venda")

18            If ssTabela!dt_vigencia1 = "0" Then
19                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 1) = 0
20            Else
21                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 1) = CDate(Mid(ssTabela!dt_vigencia1, 7, 2) & "/" & Mid(ssTabela!dt_vigencia1, 5, 2) & "/" & Mid(ssTabela!dt_vigencia1, 1, 4))
22            End If

23            If ssTabela!dt_vigencia2 = "0" Then
24                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 2) = 0
25            Else
26                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 2) = CDate(Mid(ssTabela!dt_vigencia2, 7, 2) & "/" & Mid(ssTabela!dt_vigencia2, 5, 2) & "/" & Mid(ssTabela!dt_vigencia2, 1, 4))
27            End If

28            If ssTabela!dt_vigencia3 = "0" Then
29                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 3) = 0
30            Else
31                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 3) = CDate(Mid(ssTabela!dt_vigencia3, 7, 2) & "/" & Mid(ssTabela!dt_vigencia3, 5, 2) & "/" & Mid(ssTabela!dt_vigencia3, 1, 4))
32            End If

33            If ssTabela!dt_vigencia4 = "0" Then
34                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 4) = 0
35            Else
36                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 4) = CDate(Mid(ssTabela!dt_vigencia4, 7, 2) & "/" & Mid(ssTabela!dt_vigencia4, 5, 2) & "/" & Mid(ssTabela!dt_vigencia4, 1, 4))
37            End If

38            If ssTabela!dt_vigencia5 = "0" Then
39                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 5) = 0
40            Else
41                data_vig(ssTabela!ocorr_preco, ssTabela!tp_tabela, 5) = CDate(Mid(ssTabela!dt_vigencia5, 7, 2) & "/" & Mid(ssTabela!dt_vigencia5, 5, 2) & "/" & Mid(ssTabela!dt_vigencia5, 1, 4))
42            End If

43            ssTabela.MoveNext
44        Next


          'inicializar tabela
          'frmVenda.frmeTabela.Caption = ""
          'frmVenda.frmeOFERTA.Caption = ""
          'frmVenda.frmeSP.Caption = ""

45        seq_atu(0) = 0
46        seq_atu(1) = 0
47        seq_atu(2) = 0
48        seq_ant(0) = 0
49        seq_ant(1) = 0
50        seq_ant(2) = 0

51        For I = 0 To 2
52            For j = 1 To 2
53                For w = 1 To 5
54                    If Data_Faturamento >= data_vig(j, I, w) And data_vig(j, I, w) <> 0 Then
55                        If seq_atu(I) = 0 Then
56                            seq_atu(I) = w
57                            ocor_atu(I) = j
58                        Else
59                            seq_ant(I) = w
60                            ocor_ant(I) = j
61                            Exit For
62                        End If
63                    End If
64                Next
65                If seq_ant(I) <> 0 Then
66                    Exit For
67                End If
68            Next
69        Next

70        ssTabela.Close

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand4_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If


End Sub




Sub ReSequenciar(strTIPO As String)
              
    On Error GoTo Trata_Erro
          
          Dim ss2 As Snapshot
          Dim PLSQL As String
          Dim I As Integer
          Dim ITEM_COT As Integer

1         If strTIPO = "PEDIDO" Then
              'carregar dpk e sequencias
2             SQL = "select COD_DPK,NUM_ITEM_PEDIDO from ITEM_PEDIDO where "
3             SQL = SQL & "SEQ_PEDIDO = 0 and NUM_PENDENTE = " & lngNUM_PEDIDO
4             SQL = SQL & " order by NUM_ITEM_PEDIDO"
5             Set ss = dbAccess.CreateSnapshot(SQL)
6             FreeLocks
7             ss.MoveFirst
8             I = 1
9             Do
10                If I <> ss!num_item_pedido Then
11                    SQL = "update ITEM_PEDIDO "
12                    SQL = SQL & "set NUM_ITEM_PEDIDO = " & I
13                    SQL = SQL & " where SEQ_PEDIDO = 0 and NUM_PENDENTE = " & lngNUM_PEDIDO
14                    SQL = SQL & " and NUM_ITEM_PEDIDO = " & ss!num_item_pedido
                      'atualizar
15                    dbAccess.Execute SQL, dbFailOnError
16                    FreeLocks
17                End If
18                I = I + 1
19                ss.MoveNext
20            Loop Until ss.EOF
21            ss.Close
22        Else
              'carregar dpk e sequencias
23            SQL = "select COD_DPK,NUM_ITEM_COTACAO from ITEM_COTACAO "
24            SQL = SQL & " where NUM_COTACAO = " & lngNUM_COTACAO
25            SQL = SQL & " AND COD_LOJA =  " & lngCOD_LOJA
26            SQL = SQL & " order by NUM_ITEM_COTACAO"

27            Set ss2 = dbAccess2.CreateSnapshot(SQL)
28            FreeLocks

29            I = 1
30            Do
31                If I <> ss2!NUM_ITEM_COTACAO Then

32                    ITEM_COT = ss2!NUM_ITEM_COTACAO

33                    PLSQL = " Update ITEM_cotacao set"
34                    PLSQL = PLSQL & " num_item_cotacao = " & I
35                    PLSQL = PLSQL & " where num_cotacao = " & lngNUM_COTACAO
36                    PLSQL = PLSQL & " and cod_loja = " & lngCOD_LOJA
37                    PLSQL = PLSQL & " and num_item_cotacao = " & ITEM_COT

38                    dbAccess2.Execute PLSQL, dbFailOnError
39                    FreeLocks

40                End If
41                I = I + 1
42                ss2.MoveNext
43            Loop Until ss2.EOF
44        End If
45        FreeLocks

Trata_Erro:

    If Err.Number <> 0 Then
        MsgBox "Sub ReSequenciar" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub


Public Sub Visible_Cor()
    'Pre�o Normal
    If frmVenda.lblPRECO3_NORMAL.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoA_Normal.Visible = True
        frmVenda.lblPRECO3_NORMAL.Visible = True
        If frmVenda.lblPC_DESC_PERIODO3_NORMAL.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO3_NORMAL.Visible = True
            frmVenda.lblSinal(0).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(2).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_NORMAL.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(1).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_NORMAL.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_Normal.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO3_NORMAL.ForeColor = QBColor(9)
        frmVenda.lblSinal(0).ForeColor = QBColor(9)
        frmVenda.lblPRECO3_NORMAL.ForeColor = QBColor(9)
    End If

    If frmVenda.lblPRECO2_NORMAL.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoB_Normal.Visible = True
        frmVenda.lblPRECO2_NORMAL.Visible = True
        If frmVenda.lblPC_DESC_PERIODO2_NORMAL.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO2_NORMAL.Visible = True
            frmVenda.lblSinal(1).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(2).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_NORMAL.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_Normal.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO2_NORMAL.ForeColor = QBColor(9)
        frmVenda.lblSinal(1).ForeColor = QBColor(9)
        frmVenda.lblPRECO2_NORMAL.ForeColor = QBColor(9)
        frmVenda.lbl_TipoA_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(0).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_NORMAL.ForeColor = QBColor(1)
    End If

    If frmVenda.lblPRECO1_NORMAL.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoC_Normal.Visible = True
        frmVenda.lblPRECO1_NORMAL.Visible = True
        If frmVenda.lblPC_DESC_PERIODO1_NORMAL.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO1_NORMAL.Visible = True
            frmVenda.lblSinal(2).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_Normal.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO1_NORMAL.ForeColor = QBColor(9)
        frmVenda.lblSinal(2).ForeColor = QBColor(9)
        frmVenda.lblPRECO1_NORMAL.ForeColor = QBColor(9)
        frmVenda.lbl_TipoB_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(1).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_NORMAL.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_Normal.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_NORMAL.ForeColor = QBColor(1)
        frmVenda.lblSinal(0).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_NORMAL.ForeColor = QBColor(1)
    End If

    'Pre�o Oferta
    If frmVenda.lblPRECO3_OFERTA.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoA_Oferta.Visible = True
        frmVenda.lblPRECO3_OFERTA.Visible = True
        If frmVenda.lblPC_DESC_PERIODO3_OFERTA.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO3_OFERTA.Visible = True
            frmVenda.lblSinal(3).Visible = True
        End If

        'mudar cor
        frmVenda.lbl_TipoC_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(5).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_OFERTA.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(4).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_OFERTA.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_Oferta.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.ForeColor = QBColor(9)
        frmVenda.lblSinal(3).ForeColor = QBColor(9)
        frmVenda.lblPRECO3_OFERTA.ForeColor = QBColor(9)
    End If

    If frmVenda.lblPRECO2_OFERTA.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoB_Oferta.Visible = True
        frmVenda.lblPRECO2_OFERTA.Visible = True
        If frmVenda.lblPC_DESC_PERIODO2_OFERTA.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO2_OFERTA.Visible = True
            frmVenda.lblSinal(4).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(5).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_OFERTA.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_Oferta.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.ForeColor = QBColor(9)
        frmVenda.lblSinal(4).ForeColor = QBColor(9)
        frmVenda.lblPRECO2_OFERTA.ForeColor = QBColor(9)
        frmVenda.lbl_TipoA_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(3).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_OFERTA.ForeColor = QBColor(1)
    End If

    If frmVenda.lblPRECO1_OFERTA.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoC_Oferta.Visible = True
        frmVenda.lblPRECO1_OFERTA.Visible = True
        If frmVenda.lblPC_DESC_PERIODO1_OFERTA.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO1_OFERTA.Visible = True
            frmVenda.lblSinal(5).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_Oferta.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO1_OFERTA.ForeColor = QBColor(9)
        frmVenda.lblSinal(5).ForeColor = QBColor(9)
        frmVenda.lblPRECO1_OFERTA.ForeColor = QBColor(9)
        frmVenda.lbl_TipoB_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(4).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_OFERTA.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_Oferta.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.ForeColor = QBColor(1)
        frmVenda.lblSinal(3).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_OFERTA.ForeColor = QBColor(1)
    End If

    'Pre�o Super Promo��o
    If frmVenda.lblPRECO3_SP.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoA_SP.Visible = True
        frmVenda.lblPRECO3_SP.Visible = True
        If frmVenda.lblPC_DESC_PERIODO3_SP.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO3_SP.Visible = True
            frmVenda.lblSinal(6).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(8).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_SP.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(7).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_SP.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_SP.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO3_SP.ForeColor = QBColor(9)
        frmVenda.lblSinal(6).ForeColor = QBColor(9)
        frmVenda.lblPRECO3_SP.ForeColor = QBColor(9)
    End If

    If frmVenda.lblPRECO2_SP.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoB_SP.Visible = True
        frmVenda.lblPRECO2_SP.Visible = True
        If frmVenda.lblPC_DESC_PERIODO2_SP.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO2_SP.Visible = True
            frmVenda.lblSinal(7).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO1_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(8).ForeColor = QBColor(1)
        frmVenda.lblPRECO1_SP.ForeColor = QBColor(1)
        frmVenda.lbl_TipoB_SP.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO2_SP.ForeColor = QBColor(9)
        frmVenda.lblSinal(7).ForeColor = QBColor(9)
        frmVenda.lblPRECO2_SP.ForeColor = QBColor(9)
        frmVenda.lbl_TipoA_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(6).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_SP.ForeColor = QBColor(1)
    End If

    If frmVenda.lblPRECO1_SP.Caption <> "" Then
        'mudar visible
        frmVenda.lbl_TipoC_SP.Visible = True
        frmVenda.lblPRECO1_SP.Visible = True
        If frmVenda.lblPC_DESC_PERIODO1_SP.Caption <> "" Then
            frmVenda.lblPC_DESC_PERIODO1_SP.Visible = True
            frmVenda.lblSinal(8).Visible = True
        End If
        'mudar cor
        frmVenda.lbl_TipoC_SP.ForeColor = QBColor(9)
        frmVenda.lblPC_DESC_PERIODO1_SP.ForeColor = QBColor(9)
        frmVenda.lblSinal(8).ForeColor = QBColor(9)
        frmVenda.lblPRECO1_SP.ForeColor = QBColor(9)
        frmVenda.lbl_TipoB_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO2_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(7).ForeColor = QBColor(1)
        frmVenda.lblPRECO2_SP.ForeColor = QBColor(1)
        frmVenda.lbl_TipoA_SP.ForeColor = QBColor(1)
        frmVenda.lblPC_DESC_PERIODO3_SP.ForeColor = QBColor(1)
        frmVenda.lblSinal(6).ForeColor = QBColor(1)
        frmVenda.lblPRECO3_SP.ForeColor = QBColor(1)
    End If

    'Verifica se � Representante
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then

        frmVenda.lblPRECO2_NORMAL.Visible = False
        frmVenda.lblPRECO2_OFERTA.Visible = False
        frmVenda.lblPRECO2_SP.Visible = False

        frmVenda.lblPRECO3_NORMAL.Visible = False
        frmVenda.lblPRECO3_OFERTA.Visible = False
        frmVenda.lblPRECO3_SP.Visible = False

        frmVenda.lblPC_DESC_PERIODO2_NORMAL.Visible = False
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.Visible = False
        frmVenda.lblPC_DESC_PERIODO2_SP.Visible = False

        frmVenda.lblPC_DESC_PERIODO3_NORMAL.Visible = False
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.Visible = False
        frmVenda.lblPC_DESC_PERIODO3_SP.Visible = False

        frmVenda.lblSinal(0).Visible = False
        frmVenda.lblSinal(1).Visible = False
        'frmVenda.lblSinal(2).Visible = False
        frmVenda.lblSinal(3).Visible = False
        frmVenda.lblSinal(4).Visible = False
        'frmVenda.lblSinal(5).Visible = False
        frmVenda.lblSinal(6).Visible = False
        frmVenda.lblSinal(7).Visible = False
        'frmVenda.lblSinal(8).Visible = False

    End If

    'Verifica se � Cliente
    If Tipo_Usuario = "C" Then

        frmVenda.lblPRECO3_NORMAL.Visible = False
        frmVenda.lblPRECO3_OFERTA.Visible = False
        frmVenda.lblPRECO3_SP.Visible = False

        frmVenda.lblPRECO2_NORMAL.Visible = False
        frmVenda.lblPRECO2_OFERTA.Visible = False
        frmVenda.lblPRECO2_SP.Visible = False

        frmVenda.lblPC_DESC_PERIODO3_NORMAL.Visible = False
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.Visible = False
        frmVenda.lblPC_DESC_PERIODO3_SP.Visible = False

        frmVenda.lblPC_DESC_PERIODO2_NORMAL.Visible = False
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.Visible = False
        frmVenda.lblPC_DESC_PERIODO2_SP.Visible = False

        frmVenda.lbl_TipoA_Normal.Visible = False
        frmVenda.lbl_TipoA_Oferta.Visible = False
        frmVenda.lbl_TipoA_SP.Visible = False

        frmVenda.lbl_TipoB_Normal.Visible = False
        frmVenda.lbl_TipoB_Oferta.Visible = False
        frmVenda.lbl_TipoB_SP.Visible = False

        frmVenda.lblSinal(0).Visible = False
        frmVenda.lblSinal(1).Visible = False
        frmVenda.lblSinal(3).Visible = False
        frmVenda.lblSinal(4).Visible = False
        frmVenda.lblSinal(6).Visible = False
        frmVenda.lblSinal(7).Visible = False

    End If


End Sub
Sub ImprimirCotacao(NUM_COTACAO As Long)

1         On Error GoTo HandleError

          Dim ss As Snapshot
          Dim lngCOD_VENDEDOR As Long
          Dim sngPC_DESCONTO As Single
          Dim sngPC_ACRESCIMO As Single
          Dim sngPc_Sufr As Single
          Dim iResult As Integer
          Dim strNomeImpressoraAtual As String
          Dim Resp As String

2         Screen.MousePointer = vbHourglass

3         SQL = "select cot.DT_VALIDADE as dt_validade,"
4         SQL = SQL & " cot.COD_CLIENTE as cod_cliente,"
5         SQL = SQL & " cot.COD_REPRES as cod_repres,"
6         SQL = SQL & " cot.COD_VEND as cod_vend,"
7         SQL = SQL & " cot.OBSERV as observ,"
8         SQL = SQL & " COT.PC_DESC_SUFRAMA as PC_DESC_SUFRAMA,"
9         SQL = SQL & " cot.PC_DESCONTO as pc_desconto,"
10        SQL = SQL & " cot.PC_ACRESCIMO as pc_acrescimo,"
11        SQL = SQL & " cli.NOME_CLIENTE as nome_cliente,"
12        SQL = SQL & " cid.NOME_CIDADE as nome_cidade,"
13        SQL = SQL & " cid.COD_UF as cod_uf,"
14        SQL = SQL & " vend.PSEUDONIMO as pseudonimo,"
15        SQL = SQL & " pla.DESC_PLANO as desc_plano "
16        SQL = SQL & "from COTACAO cot,"
17        SQL = SQL & "CLIENTE cli,"
18        SQL = SQL & "REPRESENTANTE vend,"
19        SQL = SQL & "PLANO_PGTO pla,"
20        SQL = SQL & "CIDADE cid "
21        SQL = SQL & "where cot.COD_PLANO = pla.COD_PLANO and "
22        SQL = SQL & "cot.COD_VEND = vend.COD_REPRES and "
23        SQL = SQL & "cli.COD_CIDADE = cid.COD_CIDADE and "
24        SQL = SQL & "cot.COD_CLIENTE = cli.COD_CLIENTE and "
25        SQL = SQL & "cot.NUM_COTACAO = " & NUM_COTACAO & " and "
26        SQL = SQL & "cot.COD_LOJA = " & lngCOD_LOJA


27        Set ss = dbAccess2.CreateSnapshot(SQL)
28        FreeLocks

29        If ss.EOF And ss.BOF Then
30            Screen.MousePointer = vbDefault
31            MsgBox "Cota��o " & NUM_COTACAO & " n�o localizada.", vbInformation, "Aten��o"
32            ss.Close
33            Exit Sub
34        End If


35        lngCOD_VENDEDOR = ss!COD_VEND
36        sngPC_DESCONTO = (1 - ss!PC_DESCONTO / 100)
37        sngPC_ACRESCIMO = (1 + ss!PC_ACRESCIMO / 100)
38        sngPc_Sufr = (1 - ss!pc_desc_suframa / 100)

          'elimina cota��es do vendedor
39        SQL = "delete from CABEC_COTACAO where COD_VEND = " & lngCOD_VENDEDOR
40        dbAccess.Execute SQL, dbFailOnError
41        FreeLocks

          'carregar dados da cotacao
42        SQL = "insert into CABEC_COTACAO values ("
43        SQL = SQL & NUM_COTACAO & ","
44        SQL = SQL & "'" & ss!DT_VALIDADE & "',"
45        SQL = SQL & ss!Cod_cliente & ","
46        SQL = SQL & "'" & Trim$(ss!NOME_CLIENTE) & "',"
47        SQL = SQL & "'" & Trim$(ss!NOME_CIDADE) & "',"
48        SQL = SQL & "'" & ss!cod_uf & "',"
49        SQL = SQL & ss!COD_VEND & ","
50        SQL = SQL & "'" & Trim$(ss!pseudonimo) & "',"
51        SQL = SQL & "'" & Trim$(ss!DESC_PLANO) & " dd',"
52        If IsNull(ss!OBSERV) Then
53            SQL = SQL & "'',"
54        Else
55            SQL = SQL & "'" & Trim$(ss!OBSERV) & "',"
56        End If
57        SQL = SQL & ss!cod_repres & ")"

58        dbAccess.Execute SQL, dbFailOnError
59        FreeLocks
60        ss.Close

61        SQL = "select itcot.COD_DPK as cod_dpk,"
62        SQL = SQL & " itcot.COMPLEMENTO as complemento,"
63        SQL = SQL & " itcot.QTD_SOLICITADA as qtd_solicitada,"
64        SQL = SQL & " (itcot.PRECO_UNITARIO      *"
65        SQL = SQL & " (1 - itcot.PC_DESC1/100)  *"
66        SQL = SQL & " (1 - itcot.PC_DESC2/100)  *"
67        SQL = SQL & " (1 - itcot.PC_DESC3/100)  *"
68        SQL = SQL & " (1 - itcot.PC_DIFICM/100) ) as PRECO,"
69        SQL = SQL & " it.DESC_ITEM as desc_item,"

'Alterado conforme solicitado pelo Eduardo Nadim (Chamado 442417)
'31/08/2009 - Eduardo Relvas

'70        SQL = SQL & " it.MASCARADO as mascarado,"
70        SQL = SQL & " it.COD_FABRICA as mascarado,"
71        SQL = SQL & " fr.SIGLA as sigla,"
72        SQL = SQL & " it.COD_UNIDADE as cod_unidade "
73        SQL = SQL & " from ITEM_CADASTRO it, ITEM_COTACAO itcot, fornecedor fr "
74        SQL = SQL & "where itcot.COD_DPK = it.COD_DPK and "
75        SQL = SQL & "it.cod_fornecedor = fr.cod_fornecedor and "
76        SQL = SQL & "itcot.NUM_COTACAO = " & NUM_COTACAO
77        SQL = SQL & " and itcot.COD_LOJA= " & lngCOD_LOJA
78        SQL = SQL & " order by NUM_ITEM_COTACAO"


79        Set ss = dbAccess2.CreateSnapshot(SQL)
80        FreeLocks
            Dim III As Integer
81        For III = 1 To ss.RecordCount
        
82            SQL = "insert into ITENS_COTACAO values ("
83            SQL = SQL & NUM_COTACAO & ","
84            SQL = SQL & ss!cod_dpk & ","
85            If IsNull(ss!COMPLEMENTO) Then
86                SQL = SQL & "'',"
87            Else
88                SQL = SQL & "'" & Trim$(ss!COMPLEMENTO) & "',"
89            End If
90            SQL = SQL & ss!qtd_solicitada & ","
91            SQL = SQL & "'" & Trim$(ss!desc_item) & "',"
92            SQL = SQL & "'" & Trim$(ss!MASCARADO) & "',"
93            SQL = SQL & "'" & ss!COD_UNIDADE & "',"
94            SQL = SQL & Format((ss!PRECO * sngPc_Sufr * sngPC_DESCONTO * sngPC_ACRESCIMO), "###########.##") & ","
95            SQL = SQL & "'" & Trim$(ss!Sigla) & "')"
96            dbAccess.Execute SQL, dbFailOnError
97            FreeLocks
98            ss.MoveNext
99        Next

100       Resp = MsgBox("Deseja gerar arquivo da cota��o para mandar via e-mail? ", 4)

101       If Resp = vbYes Then
              'gerar arquivo texto
102           With MDIForm1.rptGeral2
103               .DataFiles(0) = strPath & "VENDAS.MDB"
104               If MsgBox("Imprime t�tulo como Cota��o ?", vbQuestion + vbYesNo + vbDefaultButton1, "Cota��o ") = vbYes Then
105                   .ReportFileName = strPath & "COTACAOE.RPT"
106                   .PrintFileName = "C:\COTACAO\COTACAO" & NUM_COTACAO & ".TXT"
107               Else
108                   .ReportFileName = strPath & "OFERTAE.RPT"
109                   .PrintFileName = "C:\COTACAO\OFERTA" & NUM_COTACAO & ".TXT"
110               End If
111               .SelectionFormula = "{CABEC_COTACAO.NUM_COTACAO} = " & NUM_COTACAO
112               .PrintFileType = 2  'TIPO DE ARQUIVO (NO CASO, "TEXTO")
113               .Destination = 2    'ARQUIVO
114               iResult = .PrintReport
115           End With

116       Else
              'imprimir

117           With MDIForm1.rptGeral2

118               .DataFiles(0) = strPath & "VENDAS.MDB"

119               If MsgBox("Imprime t�tulo como Cota��o ?", vbQuestion + vbYesNo + vbDefaultButton1, "Cota��o ") = vbYes Then
120                   .ReportFileName = strPath & "COTACAO.RPT"
121               Else
122                   .ReportFileName = strPath & "OFERTA.RPT"
123               End If
124               .SelectionFormula = "{CABEC_COTACAO.NUM_COTACAO} = " & NUM_COTACAO
125               .Destination = 1    'IMPRESSORA
126               iResult = .PrintReport
127           End With

128           Screen.MousePointer = vbDefault

129           If iResult > 0 Then
130               MsgBox MDIForm1.rptGeral2.LastErrorString
131               'MsgBox " Verificar Tabela Filial_Endereco do Arquivo BASE_DPK.MDB"
132           End If

133       End If

134       Screen.MousePointer = vbDefault
135       FreeLocks

136       Exit Sub

HandleError:
137       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
138           Resume
139       Else
140           MsgBox "Sub ImprimirCotacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
141       End If
End Sub

Public Sub Valida_TPFrete()
          
    On Error GoTo Trata_Erro
          
          Dim ss As Snapshot

          'validar tipo de frete

1         SQL = "Select tp_frete "
2         SQL = SQL & " From frete_entrega a,"
3         SQL = SQL & " cidade c,"
4         SQL = SQL & " cliente b"
5         SQL = SQL & " Where a.situacao=0 and"
6         SQL = SQL & " a.cod_cidade in (c.cod_cidade,0) and"
7         SQL = SQL & " a.cod_subregiao in (c.cod_subregiao,0) and"
8         SQL = SQL & " a.cod_regiao in (c.cod_regiao,0) and"
9         SQL = SQL & " a.cod_uf = c.cod_uf and"
10        SQL = SQL & " a.cod_transp = " & frmFimPedido.txtCOD_TRANSP.Text & " And "
11        SQL = SQL & " a.cod_loja = " & lngCOD_LOJA & " and"
12        SQL = SQL & " c.cod_cidade = b.cod_cidade and"
13        SQL = SQL & " b.cod_cliente = " & frmFimPedido.txtCOD_CLIENTE.Text
14        SQL = SQL & " Order by a.cod_cidade desc,"
15        SQL = SQL & "          a.cod_subregiao desc,"
16        SQL = SQL & "          a.cod_regiao desc"


17        Set ss = dbAccess2.CreateSnapshot(SQL)
18        FreeLocks


19        If ss.EOF And ss.BOF Then
20            ss.Close
21            frmFimPedido.optCIF.Enabled = True
22            frmFimPedido.optFOBD.Enabled = True
23            frmFimPedido.optFOB.Enabled = True
24        Else
25            ss.MoveFirst
26            If ss!tp_frete = "F" Then
27                frmFimPedido.optFOB.Value = True
28                frmFimPedido.optFOB.Enabled = True
29                frmFimPedido.optFOBD.Enabled = False
30                frmFimPedido.optCIF.Enabled = False
31            ElseIf ss!tp_frete = "C" Then
32                frmFimPedido.optCIF.Value = True
33                frmFimPedido.optCIF.Enabled = True
34                frmFimPedido.optFOBD.Enabled = False
35                frmFimPedido.optFOB.Enabled = False
36            ElseIf ss!tp_frete = "D" Then
37                frmFimPedido.optFOBD.Value = True
38                frmFimPedido.optFOBD.Enabled = True
39                frmFimPedido.optCIF.Enabled = False
40                frmFimPedido.optFOB.Enabled = False
41            End If
42        End If
          
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand4_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Public Function VL_ICMRETIDO(p_cod_loja As Integer, p_cod_cliente As Long, p_cod_dpk As Long, _
            p_cod_trib As Integer, p_preco_liquido As Double, Optional w_cod_procedencia As Integer, _
            Optional w_class_fiscal As Double, Optional w_cod_uf_destino As String, Optional w_inscr_estadual As String, _
            Optional w_fl_cons_final As String, Optional w_inscr_suframa As String, Optional w_tp_cliente As String) As Double
1         On Error GoTo TrataErro

          'Eduardo - 18/08/05
          Dim SELECT0
          
          Dim SELECT1
          Dim SELECT2
          Dim SELECT3
          Dim SELECT4
          Dim SELECT5
          Dim SELECT6
          Dim SELECT7
          Dim SELECT8
          Dim ss9

          'inicializa vari�veis
2         w_cod_uf_origem = ""
3         w_vl_baseicm1 = 0
4         w_vl_baseicm2 = 0
5         w_vl_basemaj_1 = 0
6         w_vl_basemaj_3 = 0
7         w_cliente_antec = ""
8         w_cod_mensagem = 0
9         w_cod_mensagem_fiscal = 0
10        w_pc_mg_lucro_antec = 0
11        w_pc_margem_lucro = 0
12        w_class_antec = 0
13        w_class_antec_RJ = 0
14        w_vl_baseicm1_ret = 0
15        w_vl_baseicm2_ret = 0
16        w_vl_base_1 = 0
17        w_vl_base_1_aux = 0
18        w_vl_base_2 = 0
19        w_vl_base_2_aux = 0
20        w_vl_base_3 = 0
21        w_vl_baseisen = 0
22        w_vl_baseisen_aux = 0
23        w_vl_baseisen_aux1 = 0
24        w_vl_base_5 = 0
25        w_vl_base_6 = 0
26        w_vl_baseoutr = 0
27        w_pc_aliq_interna = 0
28        w_pc_aliq_interna_me = 0
29        w_pc_icm = 0
30        w_vl_icmretido = 0

    '30/09/05
31        w_cod_trib_novo = 0
32        w_cod_trib_revendedor = 0
33        w_cod_trib_inscrito = 0
34        w_cod_trib_isento = 0

          'inicializa retorno da funcao
35        VL_ICMRETIDO = 0

      'Eduardo - 18/08/05
          'W_COD_PROCEDENCIA
          'W_CLASS_FISCAL
36        SELECT0 = " Select cod_procedencia, class_fiscal " & _
                    " From item_cadastro " & _
                    " Where cod_dpk = " & p_cod_dpk

37        Set SELECT0 = dbAccess2.CreateSnapshot(SELECT0)
38        If SELECT0.EOF = False And SELECT0.BOF = False Then
39            w_cod_procedencia = SELECT0!cod_procedencia
40            w_class_fiscal = SELECT0!class_fiscal
41        Else
42            w_cod_procedencia = 0
43            w_class_fiscal = 0
44        End If
          
          'monta SQL
          'W_COD_UF_ORIGEM
45        SELECT1 = "Select a.cod_uf from cidade a, loja b where a.cod_cidade = b.cod_cidade and b.cod_loja= " & p_cod_loja
46        Set ss9 = dbAccess2.CreateSnapshot(SELECT1)
47        FreeLocks
48        w_cod_uf_origem = ss9!cod_uf

          'Eduardo - 18/08/05
          'W_COD_UF_DESTINO
          'W_INSCR_ESTADUAL
          'W_FL_CONS_FINAL
          'W_INSCR_SUFRAMA
49        SELECT0 = "Select a.cod_uf as COD_UF, b.inscr_estadual as Inscr_Estadual, b.fl_cons_final as fl_Cons_Final, b.cod_mensagem as cod_mensagem, " & _
                    " b.cod_mensagem_fiscal as cod_mensagem_fiscal, b.inscr_suframa as inscr_Suframa " & _
                    " from cidade a, cliente b " & _
                    " where a.cod_cidade = b.cod_cidade and b.cod_cliente = " & p_cod_cliente
50        Set SELECT0 = dbAccess2.CreateSnapshot(SELECT0)

51        If SELECT0.EOF = False And SELECT0.BOF = False Then
52           w_cod_uf_destino = SELECT0!cod_uf
53           w_inscr_estadual = SELECT0!inscr_estadual
54           w_fl_cons_final = SELECT0!fl_cons_final
55           w_inscr_suframa = IIf(IsNull(SELECT0!inscr_suframa), "0", SELECT0!inscr_suframa)
56        Else
57           w_cod_uf_destino = ""
58           w_inscr_estadual = ""
59           w_fl_cons_final = ""
60           w_inscr_suframa = "0"
61        End If

          'W_COD_MENSAGEM, W_COD_MENSAGEM_FISCAL
62        SELECT2 = "Select cod_mensagem, cod_mensagem_fiscal " & _
                    "From cliente where cod_cliente = " & p_cod_cliente

63        Set ss9 = dbAccess2.CreateSnapshot(SELECT2)
64        FreeLocks


65        If w_inscr_estadual = "ISENTO" Then
66            w_tp_cliente = "I"
67        ElseIf w_fl_cons_final = "S" Then
68            w_tp_cliente = "C"
69        Else
70            w_tp_cliente = "R"
71        End If

72        w_cod_mensagem = IIf(IsNull(ss9!cod_mensagem), 0, ss9!cod_mensagem)
73        w_cod_mensagem_fiscal = IIf(IsNull(ss9!cod_mensagem_fiscal), 0, ss9!cod_mensagem_fiscal)

'30/09/05
74        SELECT2 = "Select Cod_trib_revendedor, cod_trib_inscrito, cod_trib_isento "
75        SELECT2 = SELECT2 & " From Item_Cadastro A, Subst_tributaria B "
76        SELECT2 = SELECT2 & " Where A.Cod_Dpk = " & p_cod_dpk & " and b.cod_uf_origem = '" & w_cod_uf_origem & "' and "
77        SELECT2 = SELECT2 & " b.cod_uf_destino = '" & w_cod_uf_destino & "' and a.class_fiscal=b.class_fiscal"

78        Set ss9 = dbAccess2.CreateSnapshot(SELECT2)
79        FreeLocks
            
80        If p_cod_trib = 1 Or p_cod_trib = 8 Then
81           If ss9.EOF = True Then
82              w_cod_trib_revendedor = p_cod_trib
83              w_cod_trib_inscrito = p_cod_trib
84              w_cod_trib_isento = p_cod_trib
85           Else
86              w_cod_trib_revendedor = ss9!Cod_trib_Revendedor
87              w_cod_trib_inscrito = ss9!Cod_trib_inscrito
88              w_cod_trib_isento = ss9!cod_trib_isento
89           End If
90           If w_inscr_estadual = "ISENTO" Then
91              w_cod_trib_novo = w_cod_trib_isento
92           ElseIf w_fl_cons_final = "S" Then
93              w_cod_trib_novo = w_cod_trib_inscrito
94           Else
95              w_cod_trib_novo = w_cod_trib_revendedor
96           End If
97         Else
98           w_cod_trib_novo = p_cod_trib
99         End If

'Fim 30/09/05

100       If (w_tp_cliente = "R" Or w_tp_cliente = "C") And w_cod_mensagem_fiscal <> 53 Then
              'W_PC_MG_LUCRO_ANTEC
              
              'Eduardo - 16/08/05
              
              'SELECT3 = "Select pc_margem_lucro " & _
                      "From antecipacao_tributaria a, cliente b, cidade c, loja d, cidade e " & _
                      "Where b.cod_cliente = " & p_cod_cliente & " and d.cod_loja = " & p_cod_loja & " and " & _
                      "a.cod_uf_origem = e.cod_uf and d.cod_cidade = e.cod_cidade and " & _
                      "a.cod_uf_destino = c.cod_uf and b.cod_cidade = c.cod_cidade"
101           SELECT3 = "Select pc_margem_lucro " & _
                        " from CLASS_ANTEC_ENTRADA a," & _
                        " cliente                b," & _
                        " cidade                 c," & _
                        " loja                   d," & _
                        " cidade                 e," & _
                        " item_cadastro f "
'Luciano - 28/02/2008
102           SELECT3 = SELECT3 & " where b.cod_cliente = " & p_cod_cliente & " and " & _
                        " d.cod_loja = " & p_cod_loja & " and " & _
                        " f.cod_dpk = " & p_cod_dpk & " and " & _
                        " a.class_fiscal = f.class_fiscal and " & _
                        " a.cod_uf_origem = e.cod_uf and " & _
                        " d.cod_cidade = e.cod_cidade and " & _
                        " a.cod_uf_destino = c.cod_uf and " & _
                        " b.cod_cidade = c.cod_cidade and c.cod_cidade = e.cod_cidade"

'72            SELECT3 = SELECT3 & " where b.cod_cliente = " & p_cod_cliente & " and " & _
'                        " d.cod_loja = " & p_cod_loja & " and " & _
'                        " f.cod_dpk = " & p_cod_dpk & " and " & _
'                        " a.class_fiscal = f.class_fiscal and " & _
'                        " a.cod_uf_origem = e.cod_uf and " & _
'                        " d.cod_cidade = e.cod_cidade and " & _
'                        " a.cod_uf_destino = c.cod_uf and " & _
'                        " b.cod_cidade = c.cod_cidade"


103           Set ss9 = dbAccess2.CreateSnapshot(SELECT3)
104           FreeLocks

105           If ss9.EOF Then
106               w_pc_mg_lucro_antec = 0
107               w_cliente_antec = "N"
108           Else
109               w_pc_mg_lucro_antec = ss9!PC_MARGEM_LUCRO
110               w_cliente_antec = "S"
111           End If
112       End If

          'W_CLASS_ANTEC
113       SELECT4 = "Select count(*) as count " & _
                  "From class_antec_entrada " & _
                  "Where class_fiscal = " & w_class_fiscal & " and cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                  "cod_uf_destino = '" & w_cod_uf_destino & "' "

114       Set ss9 = dbAccess2.CreateSnapshot(SELECT4)
115       FreeLocks

116       If ss9.EOF Then
117           w_class_antec = 0
118       Else
119           w_class_antec = ss9!Count
120       End If

          'W_PC_MARGEM_LUCRO
121       If p_cod_trib = 1 Or p_cod_trib = 3 Then
122           If w_fl_cons_final = "S" Then
123               w_pc_margem_lucro = 0
124           Else
125               SELECT5 = "Select pc_margem_lucro " & _
                          "From subst_tributaria " & _
                          "Where cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                          "cod_uf_destino = '" & IIf(w_inscr_suframa = 0, w_cod_uf_destino, "SU") & "' and " & _
                          "class_fiscal = " & w_class_fiscal

126               Set ss9 = dbAccess2.CreateSnapshot(SELECT5)
127               FreeLocks

128               If ss9.EOF Then
129                   w_pc_margem_lucro = 0
130               Else
131                   w_pc_margem_lucro = ss9!PC_MARGEM_LUCRO
132               End If
133           End If
'104       ElseIf (p_cod_trib = 0 Or p_cod_trib = 8) And w_class_antec <> 0 And w_cliente_antec = "S" Then
134       ElseIf (w_cod_trib_novo = 0 Or w_cod_trib_novo = 8) And w_class_antec <> 0 And w_cliente_antec = "S" Then
135           w_pc_margem_lucro = w_pc_mg_lucro_antec
136       End If

          'W_PC_ALIQ_INTERNA
'107       SELECT6 = "Select pc_icm " & _
'                  "From uf_origem_destino " & _
'                  "Where cod_uf_origem = '" & w_cod_uf_destino & "' and " & _
'                  "cod_uf_destino = '" & w_cod_uf_destino & "' "
'
'108       Set ss9 = dbAccess2.CreateSnapshot(SELECT6)
'109       FreeLocks
'
'110       w_pc_aliq_interna = ss9!pc_icm
          'Consultor30 28/05/2009
137       w_pc_aliq_interna = AliquotaInterna(w_cod_uf_origem, w_cod_uf_destino, p_cod_dpk)

          'W_PC_ALIQ_INTERNA_ME
138       If w_cod_uf_origem = "GO" And w_cod_uf_destino = "GO" Then
139           SELECT7 = "Select pc_icm " & _
                      "From aliquota_me a, cliente b " & _
                      "Where b.cod_cliente = " & p_cod_cliente & " and " & _
                      "a.cod_loja = " & p_cod_loja & " and " & _
                      "a.cgc = b.cgc"

140           Set ss9 = dbAccess2.CreateSnapshot(SELECT7)
141           FreeLocks

142           If ss9.EOF Then
143               w_pc_aliq_interna_me = 0
144           Else
145               w_pc_aliq_interna_me = ss9!pc_icm
146           End If
147           If w_pc_aliq_interna_me <> 0 Then
148               w_pc_aliq_interna = w_pc_aliq_interna_me
149           End If
150       End If

          'W_PC_ICM
151       SELECT8 = "Select pc_icm " & _
                  "From uf_origem_destino " & _
                  "Where cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                  "cod_uf_destino = '" & IIf(w_inscr_suframa = 0, w_cod_uf_destino, "SU") & "' "

152       Set ss9 = dbAccess2.CreateSnapshot(SELECT8)
153       FreeLocks

154       w_pc_icm = ss9!pc_icm

'128       If (p_cod_trib = 0 And w_cod_procedencia = 0) Or _
                  (p_cod_trib = 0 And ((w_cod_procedencia = 1 Or w_cod_procedencia = 2) And p_cod_loja <> 3)) Then
155       If (w_cod_trib_novo = 0 And w_cod_procedencia = 0) Or _
                  (w_cod_trib_novo = 0 And ((w_cod_procedencia = 1 Or w_cod_procedencia = 2) And p_cod_loja <> 3)) Then
156           w_vl_baseicm1 = p_preco_liquido

157           If w_cliente_antec = "S" And w_class_antec <> 0 Then
158               w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
159               w_vl_baseicm1_ret = p_preco_liquido
160           End If

161       ElseIf w_cod_trib_novo = 1 Then
              '-- Tratamento especial para Goiania base reduzida 24/03/00
162           If (w_tp_cliente = "R" And w_cod_uf_origem = "GO" And w_cod_uf_destino = "GO") Then
163               w_vl_base_1_aux = p_preco_liquido
                  '-- Redu��o
164               w_vl_baseisen_aux1 = w_vl_base_1_aux
165               w_vl_base_1 = (w_vl_base_1_aux * 10) / 17
166               w_vl_baseisen = (w_vl_base_1_aux - (w_vl_base_1_aux * 10) / 17)
167           Else
168               w_vl_base_1 = p_preco_liquido
169           End If

170           If w_pc_margem_lucro = 0 And w_cod_uf_origem = "RJ" And w_cod_uf_destino = "RJ" Then
171               w_pc_margem_lucro = 20
172           End If

173           w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)

174       ElseIf w_cod_trib_novo = 2 Then
175           w_vl_base_2_aux = p_preco_liquido

              '-- Redu��o base colocado para tratar Goiania
176           w_vl_baseisen_aux = w_vl_base_2_aux
177           w_vl_base_2 = (w_vl_base_2_aux * 10) / 17
178           w_vl_baseisen = (w_vl_base_2_aux - (w_vl_base_2_aux * 10) / 17)

179       ElseIf p_cod_trib = 3 Then
180           w_vl_base_3 = p_preco_liquido
181           w_vl_basemaj_3 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)

182       ElseIf p_cod_trib = 4 Then
183           w_vl_baseisen = p_preco_liquido

184       ElseIf p_cod_trib = 5 Then
185           w_vl_base_5 = p_preco_liquido

186       ElseIf p_cod_trib = 6 Then
187           w_vl_base_6 = p_preco_liquido

188       ElseIf (w_cod_trib_novo = 8 Or ((w_cod_procedencia = 1 Or w_cod_procedencia = 2) _
                      And p_cod_loja = 3)) Then
189           w_vl_baseicm2 = p_preco_liquido

190           If w_cliente_antec = "S" And w_class_antec <> 0 Then
191               w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
192               w_vl_baseicm2_ret = p_preco_liquido
193           End If

194       ElseIf p_cod_trib = 9 Then
195           w_vl_baseoutr = p_preco_liquido
196       End If

197       If w_cliente_antec = "N" Then
198           w_vl_icmretido = ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) - _
                      (w_vl_base_1 * w_pc_icm / 100)) + _
                      (w_vl_basemaj_3 * w_pc_aliq_interna / 100)
199       Else
200           w_vl_icmretido = ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) - _
                      ((w_vl_baseicm1_ret * w_pc_icm / 100) + _
                      (w_vl_baseicm2_ret * w_pc_icm / 100) + _
                      (w_vl_base_1 * w_pc_icm / 100))) + _
                      (w_vl_basemaj_3 * w_pc_aliq_interna / 100)

201       End If

202       VL_ICMRETIDO = Format(w_vl_icmretido, "#0.00")

203       Exit Function

TrataErro:
204       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
205           Resume
206       Else
207           MsgBox "Sub Vl_ICMRETIDO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
208       End If
End Function

Public Function fTipo_empresa(Cod_cliente As Long) As String
    
    Dim ss As Recordset
    
    SQL = "select cli.COD_CLIENTE, cli.ddd1, cli.fone1,"
    SQL = SQL & " cli.CGC,"
    SQL = SQL & " cli.NOME_CLIENTE as nome_cliente,"
    SQL = SQL & " cli.INSCR_ESTADUAL as inscr_estadual,"
    SQL = SQL & " cli.INSCR_SUFRAMA as inscr_suframa,"
    SQL = SQL & " cli.FL_CONS_FINAL as fl_cons_final,"
    SQL = SQL & " cli.SITUACAO,"
    SQL = SQL & " cli.CLASSIFICACAO as CLASSIFICACAO,"
    SQL = SQL & " cid.NOME_CIDADE as nome_cidade,"
    SQL = SQL & " cli.cod_tipo_cliente as cod_tipo_cliente,"
    SQL = SQL & " cid.COD_UF, cli.caracteristica "
    SQL = SQL & " ,cli.tp_empresa as TP_EMPRESA"
    SQL = SQL & " from CLIENTE cli, CIDADE cid "
    SQL = SQL & " where cli.COD_CIDADE = cid.COD_CIDADE and "
    SQL = SQL & " cli.COD_CLIENTE = " & CStr(Cod_cliente)

    Set ss = dbAccess2.CreateSnapshot(SQL)
    FreeLocks

    fTipo_empresa = IIf(IsNull(ss!tp_empresa), "", ss!tp_empresa)

End Function

'CONSULTOR30 28/05/2009
Function AliquotaInterna(pUFOrigem As String, pUFDestino As String, pCodDPK As Long) As Integer

Dim vPcIcm As Integer
Dim vPcIcmGerencial As Integer
Dim vFlRedIcms As String * 1
Dim vSQL As String
Dim rs As Recordset
    
    vSQL = vSQL & "select pc_icm,pc_icm_gerencial from uf_origem_destino Where "
    vSQL = vSQL & "cod_uf_origem='" & pUFOrigem & "' and cod_uf_destino='" & pUFDestino & "'"
    Set rs = dbAccess2.CreateSnapshot(vSQL)
    FreeLocks
    
    If Not rs.BOF And Not rs.EOF Then
            
        vPcIcm = IIf(Len(rs!pc_icm) = 0, 0, rs!pc_icm)
        vPcIcmGerencial = IIf(IsNull(rs!pc_icm_gerencial), 0, rs!pc_icm_gerencial)
    
    Else
        
        vPcIcm = 0
        vPcIcmGerencial = 0
        
    End If
    
    If pUFDestino = "PR" Then
    
        vSQL = "select b.fl_red_icms "
        vSQL = vSQL & " from item_cadastro a, subst_tributaria b"
        vSQL = vSQL & " Where a.class_fiscal=b.class_fiscal and b.cod_uf_origem = '" & pUFOrigem & "'"
        vSQL = vSQL & " and b.cod_uf_destino = '" & pUFDestino & "' and a.cod_dpk = " & pCodDPK
        Set rs = dbAccess2.CreateSnapshot(vSQL)
        FreeLocks
        
        If Not rs.BOF And Not rs.EOF Then
            vFlRedIcms = IIf(IsNull(rs!fl_red_icms), "N", rs!fl_red_icms)
        Else
            vFlRedIcms = "N"
        End If
        
        If vFlRedIcms = "S" Then
            AliquotaInterna = vPcIcmGerencial
        Else
            AliquotaInterna = vPcIcm
        End If
        
    Else
        AliquotaInterna = vPcIcm
    End If
    
End Function

'TI-4405
'---------------------------------------------------------------------------------------
' Procedure : ChecarCommand
' Author    : c.samuel.oliveira
' Date      : 25/07/16
' Purpose   : Respons�vel por gerar um arquivo texto com o valor do icms retido,
'             esse m�todo � chamado pelos programas: GER030, FIL010, FIL020,
'             FIL370, FIL380 e FIL390. Ele � excutado atrav�s do SHELL
'---------------------------------------------------------------------------------------
'
Sub ChecarCommand()
Dim dblICMRetido As Double, rs As Snapshot
Dim intArquivo As Integer
Dim strArquivo As String
Dim w_vl_fator As Double
Dim CarregarICMSPreco As Boolean

On Error GoTo TrataErro

    vCommand = Split(Command, ";")
    
    If CStr(UBound(vCommand)) = "-1" Then
    
        GoTo TrataErro
    
    End If
    
    Path_drv = Pegar_Drive
        
    strArquivo = vCommand(0) & vCommand(1) & vCommand(2) & vCommand(3) & ".TXT"
    
    If Dir(Path_drv & "\DADOS\" & strArquivo) = "" And Val(vCommand(1)) > 0 Then
        

        Set dbAccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")
        Set dbAccess2 = OpenDatabase(Path_drv & "\DADOS\" & "BASE_DPK.MDB")
        
        Set rs = dbAccess2.CreateSnapshot("Select class_fiscal From item_cadastro Where cod_dpk=" & CLng(vCommand(2)))
        FreeLocks
        If Not rs.BOF And Not rs.EOF Then
          CarregarICMSPreco = frmVenda.GetClassFiscal(rs!class_fiscal, w_vl_fator)
        End If
        
        If CarregarICMSPreco Then
            dblICMRetido = frmVenda.VL_ICMRETIDO(CInt(vCommand(0)), CLng(vCommand(1)), CLng(vCommand(2)), CInt(vCommand(3)), CDbl(vCommand(4)))
        End If
        
        dbAccess.Close
        dbAccess2.Close
        
        intArquivo = FreeFile
        
        Open Path_drv & "\DADOS\" & strArquivo For Output As #intArquivo
        Print #intArquivo, Trim(dblICMRetido)
        
        Close #intArquivo
        
    End If
    
TrataErro:
    End
   
End Sub
'FIM TI-4405
