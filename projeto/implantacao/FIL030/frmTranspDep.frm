VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmTranspDep 
   Caption         =   "Transportadora x UF do Dep�sito"
   ClientHeight    =   1455
   ClientLeft      =   2775
   ClientTop       =   2625
   ClientWidth     =   3930
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1455
   ScaleWidth      =   3930
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboTranspDep 
      Height          =   315
      Left            =   90
      TabIndex        =   0
      Top             =   360
      Width           =   3750
   End
   Begin Bot�o.cmd sscmdOK 
      Height          =   525
      Left            =   1560
      TabIndex        =   2
      Top             =   810
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTranspDep.frx":0000
      PICN            =   "frmTranspDep.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "Escolha a transportadora na lista abaixo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   90
      TabIndex        =   1
      Top             =   75
      Width           =   3750
   End
End
Attribute VB_Name = "frmTranspDep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub sscmdOK_Click()
    If cboTranspDep.Text = "" Then
        MsgBox "Escolha uma transportadora!"
        Exit Sub
    End If
    frmFimPedido.txtCOD_TRANSP.Text = Mid(cboTranspDep.Text, 1, 4)
    Unload Me
End Sub


