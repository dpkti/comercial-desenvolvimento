VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmRepres 
   BorderStyle     =   0  'None
   Caption         =   "SELECIONE O RESPONS�VEL PELO CLIENTE "
   ClientHeight    =   3405
   ClientLeft      =   2130
   ClientTop       =   2055
   ClientWidth     =   5175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3405
   ScaleWidth      =   5175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdrep 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   4575
      _Version        =   65536
      _ExtentX        =   8070
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "Frmrep.frx":0000
   End
   Begin VB.Label Label2 
      Caption         =   "SELECIONE O RESPONS�VEL PELO CLIENTE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   4695
   End
   Begin VB.Label Label1 
      Caption         =   "D� UM DUPLO CLICK SOBRE O REPRES. ESCOLHIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   3000
      Width           =   4815
   End
End
Attribute VB_Name = "frmRepres"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub grdrep_DblClick()

    On Error GoTo Trata_Erro

1         grdrep.Col = 2
2         frmVenda.cboFilial = grdrep.Text
3         grdrep.Col = 3
          'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
4         If grdrep.Text = "R" Or grdrep.Text = "P" Or grdrep.Text = "G" Then
5             grdrep.Col = 0
6             lngCOD_REPRES = grdrep.Text
7         Else
8             lngCOD_REPRES = 0
9         End If
10        grdrep.Col = 0
11        fil = Format(Mid(Trim(grdrep.Text), 1, 4), "0000")
12        FIL1 = Trim(Format(Mid(Trim(grdrep.Text), 1, 4), "0000"))
13        grdrep.Col = 1
14        frmVenda.txtRepres = Format(fil, "0000") & "-" & grdrep.Text
15        grdrep.Col = 2
16        Cod_Filial = Format(Mid(grdrep.Text, 1, 4), "0000")
17        Sigla = Mid(grdrep.Text, 8, 3)
18        Unload Me

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub grdRep_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

