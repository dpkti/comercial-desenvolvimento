VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmVenda 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TELEMARKETING"
   ClientHeight    =   8055
   ClientLeft      =   60
   ClientTop       =   375
   ClientWidth     =   11970
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "Venda.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8055
   ScaleWidth      =   11970
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Frame SSFrame3 
      Caption         =   "Pedidos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1065
      Left            =   10920
      TabIndex        =   149
      Top             =   4920
      Width           =   1005
      Begin VB.TextBox txtTotal_Pedidos 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   150
         TabIndex        =   150
         Top             =   300
         Width           =   690
      End
   End
   Begin VB.Frame SSFrame2 
      Caption         =   "Liga��es: 21/01/04"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1065
      Left            =   9000
      TabIndex        =   142
      Top             =   4920
      Width           =   1905
      Begin VB.TextBox txtAtivas 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1095
         TabIndex        =   145
         Top             =   210
         Width           =   690
      End
      Begin VB.TextBox txtReceptivas 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1095
         TabIndex        =   144
         Top             =   480
         Width           =   690
      End
      Begin VB.TextBox txtLig_Total 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   1095
         TabIndex        =   143
         Top             =   750
         Width           =   690
      End
      Begin VB.Label Label8 
         Caption         =   "Ativas"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   148
         Top             =   300
         Width           =   870
      End
      Begin VB.Label Label9 
         Caption         =   "Receptivas"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   147
         Top             =   570
         Width           =   1005
      End
      Begin VB.Label Label10 
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   146
         Top             =   840
         Width           =   825
      End
   End
   Begin VB.Frame SSFrame1 
      Caption         =   "6 Maiores Linhas (�ltimos 30 dias)"
      ForeColor       =   &H00800000&
      Height          =   2025
      Left            =   9000
      TabIndex        =   140
      Top             =   6030
      Width           =   2925
      Begin MSGrid.Grid grdLinhas 
         Height          =   1725
         Left            =   60
         TabIndex        =   141
         Top             =   240
         Visible         =   0   'False
         Width           =   2805
         _Version        =   65536
         _ExtentX        =   4948
         _ExtentY        =   3043
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   7
         Cols            =   3
         FixedCols       =   0
      End
   End
   Begin Bot�o.cmd cmdVisualiza 
      Height          =   315
      Left            =   7230
      TabIndex        =   126
      Top             =   7290
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   "&Visualizar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Venda.frx":0CCA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Frame frmAproveitamento 
      Caption         =   "Aproveitamento"
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   5490
      TabIndex        =   123
      Top             =   7290
      Width           =   1410
      Begin VB.Label lblAproveitamento 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   450
         TabIndex        =   125
         Top             =   315
         Visible         =   0   'False
         Width           =   510
      End
   End
   Begin VB.Frame frmTxConversao 
      Caption         =   "Tx. Convers�o"
      ForeColor       =   &H00800000&
      Height          =   735
      Left            =   4140
      TabIndex        =   122
      Top             =   7290
      Width           =   1320
      Begin VB.Label lblTxConversao 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   495
         TabIndex        =   124
         Top             =   315
         Visible         =   0   'False
         Width           =   375
      End
   End
   Begin VB.Frame frmeCondicaoPgto 
      Caption         =   "Condi��es de Pagamento"
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2040
      Left            =   9000
      TabIndex        =   14
      Top             =   2880
      Width           =   2940
      Begin VB.TextBox txtDescAdicional 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2220
         MaxLength       =   6
         TabIndex        =   5
         Top             =   765
         Width           =   645
      End
      Begin VB.TextBox txtPC_DESC_FIN_DPK 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2220
         MaxLength       =   5
         TabIndex        =   13
         Top             =   1035
         Width           =   645
      End
      Begin VB.TextBox txtPC_ACRES_FIN_DPK 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2220
         MaxLength       =   5
         TabIndex        =   32
         Top             =   1035
         Width           =   630
      End
      Begin VB.TextBox txtCOD_PLANO 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2445
         MaxLength       =   3
         TabIndex        =   4
         Top             =   210
         Width           =   420
      End
      Begin VB.Label lblIPI 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0,00"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2220
         TabIndex        =   94
         Top             =   1755
         Width           =   645
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "IPI"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   93
         Top             =   1800
         Width           =   195
      End
      Begin VB.Label lblDescSuframa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0,00"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2220
         TabIndex        =   90
         Top             =   1530
         Width           =   645
      End
      Begin VB.Label lblSuframa 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Desconto Suframa"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   89
         Top             =   1575
         Width           =   1320
      End
      Begin VB.Label lblPC_ACRES_FIN_DPK 
         AutoSize        =   -1  'True
         Caption         =   "Acr�scimo"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   31
         Top             =   1095
         Width           =   735
      End
      Begin VB.Label lblPC_DESC_FIN_DPK 
         AutoSize        =   -1  'True
         Caption         =   "Desconto"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   30
         Top             =   1125
         Width           =   690
      End
      Begin VB.Label lblPercDifIcm 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0,00"
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2220
         TabIndex        =   63
         Top             =   1305
         Width           =   645
      End
      Begin VB.Label lblDifIcm 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Desconto Dif. de ICM"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   37
         Top             =   1350
         Width           =   1530
      End
      Begin VB.Label lblDescAdicional 
         Caption         =   "Desc Adicional"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   36
         Top             =   855
         Width           =   1590
      End
      Begin VB.Label lblDESC_PLANO 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "WWWWWWWWWWWWWWWWWWWW"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   75
         TabIndex        =   29
         Top             =   495
         Width           =   2790
      End
      Begin VB.Label lblCOD_PLANO 
         AutoSize        =   -1  'True
         Caption         =   "Plano de Pagamento"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   28
         Top             =   270
         Width           =   1485
      End
   End
   Begin VB.Frame frmeCliente 
      Caption         =   "Cliente"
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   2850
      Left            =   60
      TabIndex        =   10
      Top             =   0
      Width           =   11880
      Begin VB.TextBox txtMensagem 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   930
         TabIndex        =   115
         Top             =   2490
         Width           =   4725
      End
      Begin VB.TextBox TxtFone 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   4245
         TabIndex        =   112
         Top             =   855
         Width           =   1410
      End
      Begin VB.TextBox txtVend 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   3615
         Locked          =   -1  'True
         TabIndex        =   110
         Top             =   2160
         Width           =   2040
      End
      Begin VB.TextBox txtRepres 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   930
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   2190
         Width           =   1815
      End
      Begin VB.ComboBox cboFilial 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   315
         ItemData        =   "Venda.frx":0CE6
         Left            =   540
         List            =   "Venda.frx":0CE8
         Sorted          =   -1  'True
         TabIndex        =   85
         Text            =   "cboFilial"
         Top             =   1170
         Width           =   2670
      End
      Begin VB.ComboBox cboUf 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   315
         ItemData        =   "Venda.frx":0CEA
         Left            =   3240
         List            =   "Venda.frx":0CEC
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   1170
         Width           =   960
      End
      Begin VB.CheckBox chkDifIcm 
         Appearance      =   0  'Flat
         Caption         =   "Difer. de ICM"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4230
         TabIndex        =   12
         Top             =   1215
         Value           =   1  'Checked
         Width           =   1455
      End
      Begin VB.TextBox txtCGC 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2295
         MaxLength       =   14
         TabIndex        =   3
         Top             =   270
         Width           =   1830
      End
      Begin VB.TextBox txtCOD_CLIENTE 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   750
         MaxLength       =   6
         TabIndex        =   0
         Top             =   270
         Width           =   780
      End
      Begin GraphLib.Graph Graph1 
         Height          =   2085
         Left            =   5760
         TabIndex        =   118
         Top             =   720
         Visible         =   0   'False
         Width           =   6045
         _Version        =   65536
         _ExtentX        =   10663
         _ExtentY        =   3678
         _StockProps     =   96
         BorderStyle     =   1
         GraphTitle      =   "Desempenho Mensal"
         NumPoints       =   4
         RandomData      =   1
         ColorData       =   0
         ExtraData       =   0
         ExtraData[]     =   0
         FontFamily      =   4
         FontSize        =   4
         FontSize[0]     =   130
         FontSize[1]     =   150
         FontSize[2]     =   100
         FontSize[3]     =   100
         FontStyle       =   4
         GraphData       =   0
         GraphData[]     =   0
         LabelText       =   0
         LegendText      =   4
         PatternData     =   0
         SymbolData      =   0
         XPosData        =   0
         XPosData[]      =   0
      End
      Begin Bot�o.cmd SSCommand4 
         Height          =   555
         Left            =   6960
         TabIndex        =   137
         ToolTipText     =   "Ranking"
         Top             =   150
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":0CEE
         PICN            =   "Venda.frx":0D0A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand6 
         Height          =   555
         Left            =   7560
         TabIndex        =   138
         ToolTipText     =   "Resultado do Contato"
         Top             =   150
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":115C
         PICN            =   "Venda.frx":1178
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd sscmdAlvo 
         Height          =   555
         Left            =   8160
         TabIndex        =   139
         ToolTipText     =   "Clientes Fi�is"
         Top             =   150
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":15CA
         PICN            =   "Venda.frx":15E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdQuadrante 
         Height          =   555
         Left            =   5760
         TabIndex        =   151
         ToolTipText     =   "Ranking"
         Top             =   150
         Visible         =   0   'False
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":1EC0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDicas 
         Height          =   555
         Left            =   6360
         TabIndex        =   152
         Top             =   150
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":1EDC
         PICN            =   "Venda.frx":1EF8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAgenda 
         Height          =   555
         Left            =   8760
         TabIndex        =   153
         ToolTipText     =   "Agenda"
         Top             =   150
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":2BD2
         PICN            =   "Venda.frx":2BEE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdHistoricoVisita 
         Height          =   555
         Left            =   9360
         TabIndex        =   154
         ToolTipText     =   "Consulta Hist�rico de Visita"
         Top             =   150
         Visible         =   0   'False
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":38C8
         PICN            =   "Venda.frx":38E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Lim.Cr�dito:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2730
         TabIndex        =   157
         ToolTipText     =   "Limite aproximado de cr�dito, sujeito a an�lise"
         Top             =   1620
         Width           =   1065
      End
      Begin VB.Label lblLimite 
         AutoSize        =   -1  'True
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4020
         TabIndex        =   156
         ToolTipText     =   "Limite aproximado de cr�dito, sujeito a an�lise"
         Top             =   1610
         Width           =   60
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Fiel:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   117
         Top             =   1620
         Width           =   645
      End
      Begin VB.Label lblTipo_Fiel 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   930
         TabIndex        =   116
         Top             =   1560
         Width           =   1605
      End
      Begin VB.Label lblMensagem 
         AutoSize        =   -1  'True
         Caption         =   "Mensagem:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   114
         Top             =   2580
         Width           =   825
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Vend.:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3000
         TabIndex        =   111
         Top             =   2250
         Width           =   465
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Repres.:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   109
         Top             =   2250
         Width           =   600
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Caracter�stica:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2730
         TabIndex        =   108
         Top             =   1920
         Width           =   1035
      End
      Begin VB.Label lblCaracteristica 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   4020
         TabIndex        =   107
         Top             =   1830
         Width           =   1635
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Class."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4635
         TabIndex        =   92
         Top             =   405
         Width           =   420
      End
      Begin VB.Label lblClass 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   5235
         TabIndex        =   91
         Top             =   270
         Width           =   420
      End
      Begin VB.Label lblIS 
         Caption         =   "INSCRITO SUFRAMA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   4230
         TabIndex        =   87
         Top             =   630
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblFilial 
         Caption         =   "Filial"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   90
         TabIndex        =   86
         Top             =   1260
         Width           =   660
      End
      Begin VB.Label lblTipoCliente 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   930
         TabIndex        =   69
         Top             =   1860
         Width           =   1605
      End
      Begin VB.Label lblTipoCli 
         AutoSize        =   -1  'True
         Caption         =   "Categoria:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   68
         Top             =   1920
         Width           =   720
      End
      Begin VB.Label lblNOME_CIDADE 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "WWWWWWWWWWWWWWWWWW"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   90
         TabIndex        =   35
         Top             =   855
         Width           =   4035
      End
      Begin VB.Label lblNOME_CLIENTE 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "WWWWWWWWWWWWWWWWWWWWWWWW"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   90
         TabIndex        =   34
         Top             =   585
         Width           =   4035
      End
      Begin VB.Label lblCGC 
         AutoSize        =   -1  'True
         Caption         =   "C.G.C."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1710
         TabIndex        =   27
         Top             =   360
         Width           =   465
      End
      Begin VB.Label lblCODIGO_CLIENTE 
         AutoSize        =   -1  'True
         Caption         =   "C�digo"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   26
         Top             =   345
         Width           =   495
      End
   End
   Begin VB.CheckBox chkSemanaPassada 
      Caption         =   "Semana Passada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   675
      TabIndex        =   83
      Top             =   7680
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Frame frmePrecoVenda 
      Caption         =   "Pre�o de Venda"
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1200
      Left            =   45
      TabIndex        =   25
      Top             =   6030
      Width           =   8415
      Begin Bot�o.cmd SSCommand3 
         Height          =   255
         Left            =   7320
         TabIndex        =   136
         Top             =   180
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Super Prom."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":41BE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand2 
         Height          =   255
         Left            =   4440
         TabIndex        =   135
         Top             =   150
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   450
         BTYPE           =   3
         TX              =   "Oferta"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":41DA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Frame frmeTabela 
         Caption         =   "2209    Uf 10,00%"
         ForeColor       =   &H00800000&
         Height          =   930
         Left            =   135
         TabIndex        =   38
         Top             =   225
         Width           =   2130
         Begin VB.Label lbl_TipoC_Normal 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "C"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   103
            Top             =   675
            Width           =   135
         End
         Begin VB.Label lbl_TipoB_Normal 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "B"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   102
            Top             =   450
            Width           =   135
         End
         Begin VB.Label lbl_TipoA_Normal 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   101
            Top             =   225
            Width           =   135
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   1
            Left            =   855
            TabIndex        =   75
            Top             =   450
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   2
            Left            =   855
            TabIndex        =   74
            Top             =   675
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   0
            Left            =   855
            TabIndex        =   73
            Top             =   225
            Width           =   120
         End
         Begin VB.Label lblPRECO3_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "40,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1125
            TabIndex        =   16
            Top             =   225
            Width           =   915
         End
         Begin VB.Label lblPRECO2_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "50,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1545
            TabIndex        =   19
            Top             =   450
            Width           =   495
         End
         Begin VB.Label lblPRECO1_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "60,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1140
            TabIndex        =   22
            Top             =   675
            Width           =   900
         End
         Begin VB.Label lblPC_DESC_PERIODO3_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "30,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   41
            Top             =   225
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO1_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "10,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   40
            Top             =   675
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO2_NORMAL 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "20,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   39
            Top             =   450
            Width           =   405
         End
      End
      Begin VB.Frame frmeSP 
         Caption         =   "SP0196"
         ClipControls    =   0   'False
         ForeColor       =   &H00008000&
         Height          =   930
         Left            =   5400
         TabIndex        =   46
         Top             =   225
         Width           =   2940
         Begin VB.Label lbl_TipoC_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "C"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   106
            Top             =   675
            Width           =   135
         End
         Begin VB.Label lbl_TipoB_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "B"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   105
            Top             =   450
            Width           =   135
         End
         Begin VB.Label lbl_TipoA_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   104
            Top             =   225
            Width           =   135
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   8
            Left            =   855
            TabIndex        =   81
            Top             =   675
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   7
            Left            =   855
            TabIndex        =   80
            Top             =   450
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   6
            Left            =   855
            TabIndex        =   79
            Top             =   225
            Width           =   120
         End
         Begin VB.Label lblPRECO1_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "50,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1965
            TabIndex        =   24
            Top             =   675
            Width           =   900
         End
         Begin VB.Label lblPRECO2_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "50,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1965
            TabIndex        =   21
            Top             =   450
            Width           =   900
         End
         Begin VB.Label lblPRECO3_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "50,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1965
            TabIndex        =   18
            Top             =   225
            Width           =   900
         End
         Begin VB.Label lblPC_DESC_PERIODO1_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "11,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   49
            Top             =   675
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO2_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "12,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   48
            Top             =   450
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO3_SP 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "13,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   47
            Top             =   225
            Width           =   405
         End
      End
      Begin VB.Frame frmeOFERTA 
         Caption         =   "OR0596"
         ClipControls    =   0   'False
         ForeColor       =   &H000000FF&
         Height          =   930
         Left            =   2430
         TabIndex        =   42
         Top             =   225
         Width           =   2655
         Begin VB.Label lbl_TipoC_Oferta 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "C"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   100
            Top             =   675
            Width           =   135
         End
         Begin VB.Label lbl_TipoB_Oferta 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "B"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   99
            Top             =   450
            Width           =   135
         End
         Begin VB.Label lbl_TipoA_Oferta 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "A"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   165
            TabIndex        =   98
            Top             =   225
            Width           =   135
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   5
            Left            =   855
            TabIndex        =   78
            Top             =   675
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   4
            Left            =   855
            TabIndex        =   77
            Top             =   450
            Width           =   120
         End
         Begin VB.Label lblSinal 
            AutoSize        =   -1  'True
            Caption         =   "%"
            ForeColor       =   &H00800000&
            Height          =   195
            Index           =   3
            Left            =   855
            TabIndex        =   76
            Top             =   225
            Width           =   120
         End
         Begin VB.Label lblPRECO1_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "20,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1680
            TabIndex        =   23
            Top             =   675
            Width           =   900
         End
         Begin VB.Label lblPRECO2_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "15,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1680
            TabIndex        =   20
            Top             =   450
            Width           =   900
         End
         Begin VB.Label lblPRECO3_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "10,00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   1680
            TabIndex        =   17
            Top             =   225
            Width           =   900
         End
         Begin VB.Label lblPC_DESC_PERIODO1_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "10,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   45
            Top             =   675
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO2_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "20,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   44
            Top             =   450
            Width           =   405
         End
         Begin VB.Label lblPC_DESC_PERIODO3_OFERTA 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            BackColor       =   &H00C0C0C0&
            Caption         =   "30,00"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   420
            TabIndex        =   43
            Top             =   225
            Width           =   405
         End
      End
   End
   Begin VB.Frame frmeProduto 
      Caption         =   "Produto"
      ClipControls    =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   3120
      Left            =   60
      TabIndex        =   15
      Top             =   2880
      Width           =   8925
      Begin VB.Frame frmeOpcao 
         Height          =   375
         Left            =   6435
         TabIndex        =   119
         Top             =   135
         Width           =   2400
         Begin VB.OptionButton optPedido 
            Appearance      =   0  'Flat
            Caption         =   "Pedido"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   195
            Left            =   135
            TabIndex        =   121
            Top             =   135
            Width           =   1095
         End
         Begin VB.OptionButton optCotacao 
            Appearance      =   0  'Flat
            Caption         =   "Cota��o"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   1215
            TabIndex        =   120
            Top             =   135
            Width           =   1050
         End
      End
      Begin VB.TextBox txtoriginal 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2025
         TabIndex        =   97
         Top             =   1260
         Width           =   3300
      End
      Begin VB.ComboBox cboDeposito 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   795
         TabIndex        =   2
         Text            =   "cboDeposito"
         Top             =   180
         Width           =   2145
      End
      Begin VB.TextBox txtDescricao 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   795
         MaxLength       =   40
         TabIndex        =   9
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   900
         Width           =   4515
      End
      Begin VB.TextBox txtCOD_FABRICA 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   5940
         MaxLength       =   19
         TabIndex        =   8
         Top             =   540
         Width           =   2895
      End
      Begin VB.TextBox txtCOD_FORNECEDOR 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2925
         MaxLength       =   3
         TabIndex        =   7
         Top             =   540
         Width           =   420
      End
      Begin VB.TextBox txtCOD_DPK 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   795
         MaxLength       =   9
         TabIndex        =   6
         Top             =   540
         Width           =   900
      End
      Begin MSGrid.Grid grdEstoque 
         Height          =   1365
         Left            =   5400
         TabIndex        =   113
         Top             =   1575
         Visible         =   0   'False
         Width           =   3450
         _Version        =   65536
         _ExtentX        =   6085
         _ExtentY        =   2408
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Enabled         =   0   'False
         ScrollBars      =   2
      End
      Begin Bot�o.cmd cmdAplicacao 
         Height          =   285
         Left            =   2070
         TabIndex        =   130
         Top             =   2460
         Width           =   885
         _ExtentX        =   1561
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "&Aplica��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":41F6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand1 
         Height          =   285
         Left            =   2070
         TabIndex        =   131
         Top             =   2760
         Width           =   885
         _ExtentX        =   1561
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "Convers�o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":4212
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCesta 
         Height          =   585
         Left            =   1140
         TabIndex        =   132
         Top             =   2460
         Width           =   885
         _ExtentX        =   1561
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   "Itens em Destaque"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":422E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdNovos 
         Height          =   285
         Left            =   90
         TabIndex        =   133
         Top             =   2460
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "Itens Novos"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":424A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdVeiculo 
         Height          =   285
         Left            =   90
         TabIndex        =   134
         Top             =   2760
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "Kit Ve�culo"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":4266
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdMIX 
         Height          =   285
         Left            =   3015
         TabIndex        =   155
         Top             =   180
         Width           =   450
         _ExtentX        =   794
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Venda.frx":4282
         PICN            =   "Venda.frx":429E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Original"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1425
         TabIndex        =   96
         Top             =   1350
         Width           =   540
      End
      Begin VB.Label lblMsg_IPI 
         AutoSize        =   -1  'True
         Caption         =   "ITEM IMPORTADO"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   3735
         TabIndex        =   95
         Top             =   240
         Width           =   1635
      End
      Begin VB.Label lblDep�sito 
         AutoSize        =   -1  'True
         Caption         =   "Dep�sito"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   88
         Top             =   285
         Width           =   630
      End
      Begin VB.Label lblmsgaliq 
         AutoSize        =   -1  'True
         Caption         =   "Al�quota Reduzida de ICMS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   2475
         TabIndex        =   82
         Top             =   1710
         Visible         =   0   'False
         Width           =   2370
      End
      Begin VB.Label lblPENDENTE 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "888888"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   1215
         TabIndex        =   72
         Top             =   3105
         Visible         =   0   'False
         Width           =   930
      End
      Begin VB.Label lblPend 
         AutoSize        =   -1  'True
         Caption         =   "Pendente"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   90
         TabIndex        =   71
         Top             =   3105
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.Label lblDescr 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   70
         Top             =   990
         Width           =   690
      End
      Begin VB.Label lblDigito 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "8"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1860
         TabIndex        =   67
         Top             =   540
         Width           =   195
      End
      Begin VB.Label lblMsgTributacao 
         AutoSize        =   -1  'True
         Caption         =   "Item com substitui��o tribut�ria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   2475
         TabIndex        =   66
         Top             =   1710
         Width           =   2670
      End
      Begin VB.Label lblESTOQUE 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "888888"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   300
         Left            =   3105
         TabIndex        =   65
         Top             =   3105
         Visible         =   0   'False
         Width           =   1035
      End
      Begin VB.Label lblEstoq 
         AutoSize        =   -1  'True
         Caption         =   "Estoque"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   2115
         TabIndex        =   64
         Top             =   3105
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.Label lblCOD_UNIDADE 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "PC"
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   2025
         TabIndex        =   62
         Top             =   1620
         Width           =   360
      End
      Begin VB.Label lblQTD_MAXVDA 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "888"
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   2025
         TabIndex        =   61
         Top             =   1980
         Width           =   375
      End
      Begin VB.Label lblCATEGORIA 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "*"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   795
         TabIndex        =   60
         Top             =   1620
         Width           =   195
      End
      Begin VB.Label lblQTD_MINVDA 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "888"
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   795
         TabIndex        =   59
         Top             =   1980
         Width           =   375
      End
      Begin VB.Label lblPESO 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "88888"
         ForeColor       =   &H80000008&
         Height          =   300
         Left            =   795
         TabIndex        =   58
         Top             =   1260
         Width           =   540
      End
      Begin VB.Label lblUNIDADE 
         AutoSize        =   -1  'True
         Caption         =   "Unidade"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1410
         TabIndex        =   57
         Top             =   1710
         Width           =   585
      End
      Begin VB.Label lblPES 
         AutoSize        =   -1  'True
         Caption         =   "Peso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   56
         Top             =   1350
         Width           =   345
      End
      Begin VB.Label lblMAXVDA 
         AutoSize        =   -1  'True
         Caption         =   "Qtde M�x"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1305
         TabIndex        =   55
         Top             =   2070
         Width           =   705
      End
      Begin VB.Label lblMINVDA 
         AutoSize        =   -1  'True
         Caption         =   "Qtde Min"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   54
         Top             =   2070
         Width           =   645
      End
      Begin VB.Label lblCATEG 
         AutoSize        =   -1  'True
         Caption         =   "Escala"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   53
         Top             =   1710
         Width           =   450
      End
      Begin VB.Label lblCOD_FABRICA 
         AutoSize        =   -1  'True
         Caption         =   "Fabr."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5535
         TabIndex        =   52
         Top             =   630
         Width           =   390
      End
      Begin VB.Label lblSIGLA 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "WWWWWWWWWW"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3375
         TabIndex        =   51
         Top             =   540
         Width           =   1950
      End
      Begin VB.Label lblCOD_FORNECEDOR 
         AutoSize        =   -1  'True
         Caption         =   "Forn."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2520
         TabIndex        =   50
         Top             =   630
         Width           =   390
      End
      Begin VB.Line lneDigito 
         BorderWidth     =   2
         X1              =   1725
         X2              =   1815
         Y1              =   690
         Y2              =   690
      End
      Begin VB.Label lblCOD_DPK 
         AutoSize        =   -1  'True
         Caption         =   "C�d DPK"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   33
         Top             =   630
         Width           =   615
      End
   End
   Begin Bot�o.cmd cmdFinalizar 
      Height          =   315
      Left            =   8130
      TabIndex        =   127
      Top             =   7290
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   "&Finalizar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Venda.frx":4895
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   315
      Left            =   7230
      TabIndex        =   128
      Top             =   7650
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   "&Excluir"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Venda.frx":48B1
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   315
      Left            =   8130
      TabIndex        =   129
      Top             =   7650
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   "&Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Venda.frx":48CD
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   45
      TabIndex        =   84
      Top             =   7410
      Visible         =   0   'False
      Width           =   615
   End
End
Attribute VB_Name = "frmVenda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmVenda
' Author    : c.samuel.oliveira
' Date      : 20/07/16
' Purpose   : TI-4405
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVenda
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 22/03/16
' Purpose   : TI-3934
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVenda
' Author    : c.samuel.oliveira
' Date      : 01/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVenda
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 02/10/15
' Purpose   : TI-3030
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVenda
' Author    : c.samuel.oliveira
' Date      : 23/10/15
' Purpose   : TI-3221
'---------------------------------------------------------------------------------------

Option Explicit

Private strIE As String                'INSCRICAO ESTADUAL DO CLIENTE
Private strIS As String                'INSCRICAO SUFRAMA DO CLIENTE
Private sPC_DESCONTO_PLANO As Single   'DESCONTO DO PLANO EM USO
Private sPC_ACRESCIMO_PLANO As Single  'ACRESCIMO DO PLANO EM USO
Private ssItem As Snapshot             'SNAPSHOT PARA DADOS DE ITEM
Private dblPRECO_VENDA(3) As Double    'PRECO BRUTO DO ITEM
Private Fabrica As String

Const BuscaDescTab = True              'Chamada a GETITEM buscando TABDESCPER
Const NaoBuscaDescTab = False
Const CalcItem = True                  'Chamada a GETDIFICM chamando CALCULAPRECO
Const NaoCalcItem = False
Const Key_F2 = &H71

Private Sub CalculaPrecos(Recalcula As Boolean)
1         On Error GoTo TrataErro

          Dim ssTabela As Snapshot
          Dim ssUF As Snapshot
          Dim sngDESC_UF As Single
          Dim DataSemanaPassada As Date
          Dim DT_VIG(5) As Date
          Dim vVlrICMRetido As Double 'TI-4405

          'validar calculo
2         If txtCOD_DPK.Text = "" Then
3             Exit Sub
4         End If

5         If txtDescAdicional.Text = "" Then
6             txtDescAdicional.Text = 0
7         End If

          'setar cursor
8         Screen.MousePointer = vbHourglass

          'desconto de UF
9         SQL = "select PC_DESC from UF_DPK where "
10        SQL = SQL & " COD_DPK = " & txtCOD_DPK.Text & " and "
11        SQL = SQL & " COD_UF_ORIGEM = '" & uf_origem & "' and "
12        SQL = SQL & " COD_UF_DESTINO = '" & uf_destino & "'"


13        Set ssUF = dbAccess2.CreateSnapshot(SQL)
14        FreeLocks

15        If ssUF.EOF And ssUF.BOF Then
16            sngDESC_UF = 0
17        Else
18            If frmVenda.lblMsgTributacao.Visible = True And tipo_cliente = "ISENTO" Then
19                sngDESC_UF = 0
20            Else
21                sngDESC_UF = CSng(ssUF!pc_desc)
22            End If
23        End If
24        ssUF.Close

          'Carrega descontos de periodo
25        If Recalcula Then
26            Call GetDescontos
27        End If

          'inicializar tabela
28        frmVenda.frmeTabela.Caption = ""
29        frmVenda.frmeOFERTA.Caption = ""
30        frmVenda.frmeSP.Caption = ""

31        If chkSemanaPassada.Value = vbChecked Then
32            If ssItem!PRECO_VENDA_ANT > 0 And _
                      frmeTabela.Caption = "" And _
                      ocor_ant(0) = 2 Then

33                If sngDESC_UF = 0 Then
34                    frmeTabela.Caption = tabela(ocor_ant(0), 0)
35                Else
36                    frmeTabela.Caption = tabela(ocor_ant(0), 0) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
37                End If
38            End If
39        Else
40            If ssItem!PRECO_VENDA_ANT > 0 And _
                      frmeTabela.Caption = "" And _
                      ocor_ant(0) = 2 Then

41                If sngDESC_UF = 0 Then
42                    frmeTabela.Caption = tabela(ocor_atu(0), 0)
43                Else
44                    frmeTabela.Caption = tabela(ocor_atu(0), 0) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
45                End If
46            End If
47        End If

48        If chkSemanaPassada.Value = vbChecked Then
49            If ssItem!PRECO_OF_ANT > 0 And _
                      frmeOFERTA.Caption = "" And _
                      ocor_ant(1) = 2 Then
50                If sngDESC_UF = 0 Then
51                    frmeOFERTA.Caption = tabela(ocor_ant(1), 1)
52                Else
53                    frmeOFERTA.Caption = tabela(ocor_ant(1), 1) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
54                End If
55            End If
56        Else
57            If ssItem!PRECO_OF_ANT > 0 And _
                      frmeOFERTA.Caption = "" And _
                      ocor_ant(1) = 2 Then

58                If sngDESC_UF = 0 Then
59                    frmeOFERTA.Caption = tabela(ocor_atu(1), 1)
60                Else
61                    frmeOFERTA.Caption = tabela(ocor_atu(1), 1) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
62                End If
63            End If
64        End If

65        If chkSemanaPassada.Value = vbChecked Then
66            If ssItem!PRECO_SP_ANT > 0 And _
                      frmeSP.Caption = "" And _
                      ocor_ant(2) = 2 Then
67                If sngDESC_UF = 0 Then
68                    frmeSP.Caption = tabela(ocor_ant(2), 2)
69                Else
70                    frmeSP.Caption = tabela(ocor_ant(2), 2) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
71                End If
72            End If
73        Else
74            If ssItem!PRECO_SP_ANT > 0 And _
                      frmeSP.Caption = "" And _
                      ocor_ant(2) = 2 Then
75                If sngDESC_UF = 0 Then
76                    frmeSP.Caption = tabela(ocor_atu(2), 2)
77                Else
78                    frmeSP.Caption = tabela(ocor_atu(2), 2) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
79                End If
80            End If
81        End If

82        If ssItem!PRECO_VENDA > 0 And _
                  ocor_atu(0) = 1 And _
                  frmeTabela.Caption = "" Then
83            If sngDESC_UF = 0 Then
84                frmeTabela.Caption = tabela(ocor_atu(0), 0)
85            Else
86                frmeTabela.Caption = tabela(ocor_atu(0), 0) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
87            End If
88        End If


89        If ssItem!PRECO_OF > 0 And _
                  ocor_atu(1) = 1 And _
                  frmeOFERTA.Caption = "" Then
90            If sngDESC_UF = 0 Then
91                frmeOFERTA.Caption = tabela(ocor_atu(1), 1)
92            Else
93                frmeOFERTA.Caption = tabela(ocor_atu(1), 1) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
94            End If

95        End If

96        If ssItem!PRECO_SP > 0 And _
                  ocor_atu(2) = 1 And _
                  frmeSP.Caption = "" Then
97            If sngDESC_UF = 0 Then
98                frmeSP.Caption = tabela(ocor_atu(2), 2)
99            Else
100               frmeSP.Caption = tabela(ocor_atu(2), 2) & "      UF " & Format$(sngDESC_UF, "#0.00") & "%"
101           End If

102       End If



          'preco de tabela
103       If chkSemanaPassada.Value = vbChecked Then
104           If ocor_ant(0) = 2 Then
105               dblPRECO_VENDA(0) = ssItem!PRECO_VENDA_ANT
106           Else
107               dblPRECO_VENDA(0) = ssItem!PRECO_VENDA
108           End If
109       Else
110           If ocor_atu(0) = 2 Then
111               dblPRECO_VENDA(0) = ssItem!PRECO_VENDA_ANT
112           Else
113               dblPRECO_VENDA(0) = ssItem!PRECO_VENDA
114           End If
115       End If

116       If chkSemanaPassada.Value = vbChecked Then
117           If ocor_ant(1) = 2 Then
118               dblPRECO_VENDA(1) = ssItem!PRECO_OF_ANT
119           Else
120               dblPRECO_VENDA(1) = ssItem!PRECO_OF
121           End If
122       Else
123           If ocor_atu(1) = 2 Then
124               dblPRECO_VENDA(1) = ssItem!PRECO_OF_ANT
125           Else
126               dblPRECO_VENDA(1) = ssItem!PRECO_OF
127           End If
128       End If

129       If chkSemanaPassada.Value = vbChecked Then
130           If ocor_ant(2) = 2 Then
131               dblPRECO_VENDA(2) = ssItem!PRECO_SP_ANT
132           Else
133               dblPRECO_VENDA(2) = ssItem!PRECO_SP
134           End If
135       Else
136           If ocor_atu(2) = 2 Then
137               dblPRECO_VENDA(2) = ssItem!PRECO_SP_ANT
138           Else
139               dblPRECO_VENDA(2) = ssItem!PRECO_SP
140           End If
141       End If

          'calcula preco de venda - TABELA NORMAL
142       If dblPRECO_VENDA(0) > 0 Then
143           If lblPC_DESC_PERIODO1_NORMAL.Caption = "" And _
                      lblPC_DESC_PERIODO2_NORMAL.Caption = "" And _
                      lblPC_DESC_PERIODO3_NORMAL.Caption = "" Then

144               lblPRECO3_NORMAL.Caption = Format$(dblPRECO_VENDA(0) * _
                          (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                          (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                          (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                          (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                          (1 - CDbl(txtDescAdicional.Text) / 100) * _
                          (1 - sngDESC_UF / 100), "Standard")
145               lblPRECO3_NORMAL.Enabled = True
146           Else
147               If lblPC_DESC_PERIODO1_NORMAL.Caption = "" Then
148                   lblPRECO1_NORMAL.Caption = ""
149               Else
150                   lblPRECO1_NORMAL.Caption = Format$(dblPRECO_VENDA(0) * _
                              (1 - CDbl(lblPC_DESC_PERIODO1_NORMAL.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
                      
151                   lblPRECO1_NORMAL.Enabled = True
152               End If

153               If lblPC_DESC_PERIODO2_NORMAL.Caption = "" Then
154                   lblPRECO2_NORMAL.Caption = ""
155               Else
156                   lblPRECO2_NORMAL.Caption = Format$(dblPRECO_VENDA(0) * _
                              (1 - CDbl(lblPC_DESC_PERIODO2_NORMAL.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
157                   lblPRECO2_NORMAL.Enabled = True
158               End If

159               If lblPC_DESC_PERIODO3_NORMAL.Caption = "" Then
160                   lblPRECO3_NORMAL.Caption = ""
161               Else
162                   lblPRECO3_NORMAL.Caption = Format$(dblPRECO_VENDA(0) * _
                              (1 - CDbl(lblPC_DESC_PERIODO3_NORMAL.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
163                   lblPRECO3_NORMAL.Enabled = True
164               End If
165           End If
166       End If

          'calcula preco de venda - TABELA OFERTA
167       If dblPRECO_VENDA(1) > 0 Then
168           If lblPC_DESC_PERIODO1_OFERTA.Caption = "" And _
                      lblPC_DESC_PERIODO2_OFERTA.Caption = "" And _
                      lblPC_DESC_PERIODO3_OFERTA.Caption = "" Then

169               lblPRECO3_OFERTA.Caption = Format$(dblPRECO_VENDA(1) * _
                          (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                          (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                          (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                          (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                          (1 - CDbl(txtDescAdicional.Text) / 100) * _
                          (1 - sngDESC_UF / 100), "Standard")
170               lblPRECO3_OFERTA.Enabled = True

171           Else
172               If lblPC_DESC_PERIODO1_OFERTA.Caption = "" Then
173                   lblPRECO1_OFERTA.Caption = ""
174               Else
175                   lblPRECO1_OFERTA.Caption = Format$(dblPRECO_VENDA(1) * _
                              (1 - CDbl(lblPC_DESC_PERIODO1_OFERTA.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
176                   lblPRECO1_OFERTA.Enabled = True
177               End If

178               If lblPC_DESC_PERIODO2_OFERTA.Caption = "" Then
179                   lblPRECO2_OFERTA.Caption = ""
180               Else
181                   lblPRECO2_OFERTA.Caption = Format$(dblPRECO_VENDA(1) * _
                              (1 - CDbl(lblPC_DESC_PERIODO2_OFERTA.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
182                   lblPRECO2_OFERTA.Enabled = True
183               End If

184               If lblPC_DESC_PERIODO3_OFERTA.Caption = "" Then
185                   lblPRECO3_OFERTA.Caption = ""
186               Else
187                   lblPRECO3_OFERTA.Caption = Format$(dblPRECO_VENDA(1) * _
                              (1 - CDbl(lblPC_DESC_PERIODO3_OFERTA.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
188                   lblPRECO3_OFERTA.Enabled = True
189               End If
190           End If
191       End If

          'calcula preco de venda - TABELA SUPER PROMO��O
192       If dblPRECO_VENDA(2) > 0 Then
193           If lblPC_DESC_PERIODO1_SP.Caption = "" And _
                      lblPC_DESC_PERIODO2_SP.Caption = "" And _
                      lblPC_DESC_PERIODO3_SP.Caption = "" Then

194               lblPRECO3_SP.Caption = Format$(dblPRECO_VENDA(2) * _
                          (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                          (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                          (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                          (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                          (1 - CDbl(txtDescAdicional.Text) / 100) * _
                          (1 - sngDESC_UF / 100), "Standard")
195               lblPRECO3_SP.Enabled = True

196           Else
197               If lblPC_DESC_PERIODO1_SP.Caption = "" Then
198                   lblPRECO1_SP.Caption = ""
199               Else
200                   lblPRECO1_SP.Caption = Format$(dblPRECO_VENDA(2) * _
                              (1 - CDbl(lblPC_DESC_PERIODO1_SP.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
201                   lblPRECO1_SP.Enabled = True
202               End If

203               If lblPC_DESC_PERIODO2_SP.Caption = "" Then
204                   lblPRECO2_SP.Caption = ""
205               Else
206                   lblPRECO2_SP.Caption = Format$(dblPRECO_VENDA(2) * _
                              (1 - CDbl(lblPC_DESC_PERIODO2_SP.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
207                   lblPRECO2_SP.Enabled = True
208               End If

209               If lblPC_DESC_PERIODO3_SP.Caption = "" Then
210                   lblPRECO3_SP.Caption = ""
211               Else
212                   lblPRECO3_SP.Caption = Format$(dblPRECO_VENDA(2) * _
                              (1 - CDbl(lblPC_DESC_PERIODO3_SP.Caption) / 100) * _
                              (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                              (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                              (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                              (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                              (1 - CDbl(txtDescAdicional.Text) / 100) * _
                              (1 - sngDESC_UF / 100), "Standard")
213                   lblPRECO3_SP.Enabled = True
214               End If
215           End If
216       End If
                     
        'TI-4405
217     If CarregarICMSPreco(Val(txtCOD_DPK.Text)) Then
218         If dblPRECO_VENDA(0) > 0 Then
219             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO3_NORMAL.Caption))
220             If vVlrICMRetido > 0 Then
221                 lblPRECO3_NORMAL.Caption = Format$(CDbl(lblPRECO3_NORMAL.Caption) + vVlrICMRetido, "Standard")
222             End If
223             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO2_NORMAL.Caption))
224             If vVlrICMRetido > 0 Then
225                 lblPRECO2_NORMAL.Caption = Format$(CDbl(lblPRECO2_NORMAL.Caption) + vVlrICMRetido, "Standard")
226             End If
227             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO1_NORMAL.Caption))
228             If vVlrICMRetido > 0 Then
229                 lblPRECO1_NORMAL.Caption = Format$(CDbl(lblPRECO1_NORMAL.Caption) + vVlrICMRetido, "Standard")
230             End If
231         End If
232         If dblPRECO_VENDA(1) > 0 Then
233             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO3_OFERTA.Caption))
234             If vVlrICMRetido > 0 Then
235                 lblPRECO3_OFERTA.Caption = Format$(CDbl(lblPRECO3_OFERTA.Caption) + vVlrICMRetido, "Standard")
236             End If
237             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO2_OFERTA.Caption))
238             If vVlrICMRetido > 0 Then
239                 lblPRECO2_OFERTA.Caption = Format$(CDbl(lblPRECO2_OFERTA.Caption) + vVlrICMRetido, "Standard")
240             End If
241             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO1_OFERTA.Caption))
242             If vVlrICMRetido > 0 Then
243                 lblPRECO1_OFERTA.Caption = Format$(CDbl(lblPRECO1_OFERTA.Caption) + vVlrICMRetido, "Standard")
244             End If
245         End If
246         If dblPRECO_VENDA(2) > 0 Then
247             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO3_SP.Caption))
248             If vVlrICMRetido > 0 Then
249                 lblPRECO3_SP.Caption = Format$(CDbl(lblPRECO3_SP.Caption) + vVlrICMRetido, "Standard")
250             End If
251             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO2_SP.Caption))
252             If vVlrICMRetido > 0 Then
253                 lblPRECO2_SP.Caption = Format$(CDbl(lblPRECO2_SP.Caption) + vVlrICMRetido, "Standard")
254             End If
255             vVlrICMRetido = VL_ICMRETIDO(CInt(Mid(cboDeposito.Text, 1, 2)), _
                                                        Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), _
                                                        Val(lblPRECO1_SP.Caption))
256             If vVlrICMRetido > 0 Then
257                 lblPRECO1_SP.Caption = Format$(CDbl(lblPRECO1_SP.Caption) + vVlrICMRetido, "Standard")
258             End If
259         End If
260     End If
        'FIM TI-4405
    
          'setar cursor
261       Screen.MousePointer = vbDefault

262       Exit Sub

TrataErro:
263       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
264           Label1.Visible = True
265           contador = contador + 1
266           Label1.Caption = contador
267           Resume
              'ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
              'MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
              'ReInitProduto
              'Exit Sub
268       Else
269           MsgBox "Sub CalculaPrecos" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
270       End If

End Sub

Private Sub cboDeposito_Change()

    On Error GoTo Trata_Erro
          
          Dim ss As Snapshot

1         lngCOD_LOJA = Mid(cboDeposito.Text, 1, 2)


2         If cboDeposito.DataChanged Then

3             SQL = "Select b.cod_uf as cod_uf"
4             SQL = SQL & " From loja a, cidade b"
5             SQL = SQL & " Where a.cod_cidade = b.cod_cidade and"
6             SQL = SQL & " a.cod_loja = " & Val(cboDeposito)

7             Set ss = dbAccess2.CreateSnapshot(SQL)
8             FreeLocks
9             If ss.EOF Then
10                uf_origem = uf_origem
11            Else
12                uf_origem = ss!cod_uf
13            End If
14            ss.Close

'15            If uf_origem = uf_destino Then
17                chkDifIcm.Value = vbChecked
18                chkDifIcm.Enabled = True
'19            End If
            
                'TI-3952
                lngNUM_MIX = 0
                
                Unload frmMIXItem
                Set frmMIXItem = Nothing
                Unload frmMIXProduto
                Set frmMIXProduto = Nothing
                'FIM TI-3952

20        End If
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CboDeposito_Change" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub cboDeposito_Click()

    cboDeposito.DataChanged = True
    Call cboDeposito_Change

    If txtCOD_DPK.Text <> "" Then
        Call txtCOD_DPK_LostFocus
    End If

End Sub

Private Sub cboDeposito_GotFocus()
    cboDeposito.DataChanged = False
End Sub

Private Sub cboDeposito_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
    Beep
End Sub

Private Sub cboDeposito_LostFocus()
    On Error GoTo Trata_Erro

1         If cboDeposito.Text = "" Then cboDeposito.Text = deposito_default

          'TRANSFER�NCIA - SALVADOR / BRAS�LIA
          'CDs Virtuais
4         SQL = "Select * "
5         SQL = SQL & "From R_UF_DEPOSITO "
6         SQL = SQL & "Where COD_LOJA = " & Val(cboDeposito.Text)

7         Set ss5 = dbAccess2.CreateSnapshot(SQL)

8         If Not ss5.EOF And UF <> "" Then
9             SQL = "Select COD_UF "
10            SQL = SQL & "From R_UF_DEPOSITO "
11            SQL = SQL & "Where COD_LOJA = " & Val(cboDeposito.Text) & " AND "
12            SQL = SQL & "COD_UF = '" & UF & "' "

13            Set ss5 = dbAccess2.CreateSnapshot(SQL)

14            If ss5.EOF Then
15                MsgBox "Transfer�ncia n�o dispon�vel! Escolha outro dep�sito para o pedido.", vbInformation, "Aten��o"
16                If cboDeposito.Enabled = True Then cboDeposito.SetFocus
17                Exit Sub
18            End If

19            lngCOD_LOJA = CLng(Mid(cboDeposito.Text, 1, 2))
20        End If
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub cboDeposito_Lostfocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub cboFilial_Click()
    Call CalculaPrecos(BuscaDescTab)
End Sub

Private Sub cboFilial_DropDown()
    Call GetFilial
End Sub

Private Sub cboFilial_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
    Beep
End Sub

Private Sub cboFilial_LostFocus()
    If cboFilial.Text = "" Then
        cboFilial.Text = FILIAL_PED
    End If
    If cboFilial.DataChanged = True Then
        Call CalculaPrecos(BuscaDescTab)
    End If
End Sub

Private Sub cboUf_Click()

    uf_destino = cboUf.List(cboUf.ListIndex)

    If cboUf.ListIndex <> -1 Then
        If chkDifIcm.Value = vbChecked Then
            'calcula diferenca de icm
            Call GetDifIcm(CalcItem, bdif_ICMS)
        Else
            chkDifIcm.Value = vbChecked
            chkDifIcm.Enabled = True
        End If
    End If
    uf_destino = cboUf.Text
End Sub

Private Sub chkDifIcm_Click()

    'Eduardo - 19/04/2006
    'Ajustar_Flag_ICMS

    'calcula diferenca de ICM
    Call GetDifIcm(CalcItem, bdif_ICMS)

End Sub

Private Sub chkSemanaPassada_Click()

    If txtCOD_DPK.Text <> "" And txtCOD_FABRICA.Text <> "" Then
        'calcula pre�os
        InitFrmePrecoVenda
        Call CalculaPrecos(BuscaDescTab)
        Call Visible_Cor
    End If

End Sub

Private Sub Cliente_Ligacao()
          Dim linha As String

1         On Error GoTo Trata_Erro
          
2         linha = ""
3         num_arq = FreeFile
4         Open "C:\tessw\cliente.txt" For Input As #num_arq

5         txtCOD_CLIENTE.Text = ""
6         Do While Not EOF(num_arq)
7             linha = linha & Input(1, #1)
8         Loop


9         txtCOD_CLIENTE.Text = Trim(linha)
10        Call txtCOD_CLIENTE_LostFocus

11        Close #num_arq
12        Exit Sub

Trata_Erro:
13        If Err = 53 Or Err = 76 Then
14            Exit Sub
15        Else
16            MsgBox "Sub Cliente_Ligacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
17        End If


End Sub

Private Sub cmd1_Click()

End Sub

Private Sub cmdAgenda_Click()
    frmAgendaPromotor.Show 1
End Sub

Private Sub cmdAplicacao_Click()
1         On Error GoTo TrataErro

          Dim ss As Snapshot
          Dim i As Long
          Dim COD As Long
          Dim strMontAnterior As String

2         COD = ssItem!cod_dpk

          'montar SQL
3         SQL = "select a.desc_montadora,"
4         SQL = SQL & " b.desc_aplicacao"
5         SQL = SQL & " from montadora a, aplicacao b"
6         SQL = SQL & " where a.cod_montadora = b.cod_montadora and"
7         SQL = SQL & " b.cod_dpk = " & COD
8         SQL = SQL & " order by b.cod_montadora, b.cod_categoria, b.sequencia"

9         Set ss = dbAccess2.CreateSnapshot(SQL)
10        FreeLocks

11        If ss.EOF And ss.BOF Then
12            MsgBox "N�o ha aplica��o cadastrada para este item", vbInformation, "Aten��o"
13            ss.Close
14            Exit Sub
15        End If

          'carrega dados no frame de aplicacao
16        frmAplicacao.lblDESC_ITEM.Caption = "" & ssItem!desc_item

17        With frmAplicacao.grdAplicacao
18            .Cols = 2
19            .Rows = ss.RecordCount
20            .ColWidth(0) = 1750
21            .ColWidth(1) = 5500
22            If .Rows > 8 Then
23                frmAplicacao.Width = 7980
24                frmAplicacao.cmdSair.Left = 6480
25                frmAplicacao.lblCabec(1).Width = 5820
26            Else
27                frmAplicacao.Width = 7695
28                frmAplicacao.cmdSair.Left = 6240
29                frmAplicacao.lblCabec(1).Width = 5550
30            End If

31            For i = 0 To .Rows - 1
32                .Row = i
33                .Col = 0
34                If strMontAnterior <> ss!DESC_MONTADORA Then
35                    .Text = "" & ss!DESC_MONTADORA
36                    strMontAnterior = " & ss!DESC_MONTADORA"
37                Else
38                    .Text = ""
39                End If
40                .Col = 1
41                .Text = IIf(IsNull(ss!desc_aplicacao), "", ss!desc_aplicacao)
42                ss.MoveNext
43            Next
44            ss.Close
45        End With
          'mostra a aplica��o
46        frmAplicacao.Show vbModal

47        Exit Sub

TrataErro:
48        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
49            Resume
50        Else
51            MsgBox "Sub cmdAplicacao_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
52        End If

End Sub

Private Sub cmdCesta_Click()
1         strTipo_Tela = "D"
2         Unload frmCesta
3         frmCesta.Show 1
4         If txtCOD_DPK <> "" Then
5             txtCOD_DPK_LostFocus
6         End If

End Sub

Private Sub cmdDicas_Click()
1     On Error GoTo TrataErro
2         If Val(frmVenda.txtCOD_CLIENTE.Text) <> 0 Then
3             frmDicas.Show 1
4         Else
5             MsgBox "Por favor selecione um cliente para consulta de dicas.", vbInformation, "Aten��o"
6             frmVenda.txtCOD_CLIENTE.SetFocus
7         End If
TrataErro:
8         If Err.Number <> 0 Then
9             MsgBox "Sub cmdDicas_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
10        End If
End Sub

Private Sub cmdExcluir_Click()
1         On Error GoTo TrataErro

          Dim PLSQL As String
          Dim PLSQL1 As String
          Dim ss As Snapshot
          Dim ss5 As Snapshot

2         If optPedido.Value Then

              'consulta dados dos itens
3             SQL = "select NUM_PENDENTE,"
4             SQL = SQL & " COD_LOJA, COD_CLIENTE"
5             SQL = SQL & " from PEDIDO "
6             SQL = SQL & " where fl_pedido_finalizado='N' and cod_vend = " & sCOD_VEND
7             SQL = SQL & " ORDER BY NUM_PENDENTE"
8             Set ss5 = dbAccess.CreateSnapshot(SQL)
9             FreeLocks

10            If ss5.EOF Then
11                Exit Sub
12            End If

13            ss5.MoveLast
14            If ss5.RecordCount > 1 Then
15                Screen.MousePointer = 11
16                With frmPedAb.grdPedAb
17                    .Cols = 3
18                    .Rows = ss5.RecordCount + 1
19                    .ColWidth(0) = 1200
20                    .ColWidth(1) = 1500
21                    .ColWidth(2) = 1500

22                    .Row = 0
23                    .Col = 0
24                    .Text = "    Nr.Pedido"
25                    .Col = 1
26                    .Text = "       Deposito"
27                    .Col = 2
28                    .Text = "   Cod. Cliente"


29                    ss5.MoveFirst
30                    For i = 1 To .Rows - 1
31                        .Row = i

32                        .Col = 0
33                        .ColAlignment(0) = 2
34                        .Text = ss5!num_pendente
35                        .Col = 1
36                        .ColAlignment(1) = 2
37                        .Text = ss5!COD_LOJA
38                        .Col = 2
39                        .ColAlignment(2) = 2
40                        .Text = ss5!Cod_cliente

41                        ss5.MoveNext
42                    Next
43                    .Row = 1
44                End With

                  'mouse
45                Screen.MousePointer = vbDefault
46                frmPedAb.Label1 = "Selecione o pedido desejado"
47                frmPedAb.Label2 = "Os pedidos s� poder�o ficar pendentes no dia de sua confec��o, ap�s esta data ser�o exclu�dos"
48                frmPedAb.Show 1

49            Else
50                lngNUM_PEDIDO = ss5!num_pendente
51                lngCOD_LOJA = ss5!COD_LOJA
52            End If

53            If MsgBox("Confirma a exclus�o do pedido " & lngNUM_PEDIDO, vbYesNo, "EXCLUIR PEDIDO") = vbYes Then

                  'eliminar pedido
54                SQL = "delete from PEDIDO where NUM_PENDENTE = " & lngNUM_PEDIDO
55                SQL = SQL & " and COD_LOJA = " & lngCOD_LOJA
56                dbAccess.Execute SQL, dbFailOnError
57                FreeLocks

58                If ss5.RecordCount > 1 Then
                      'Call LiberaMemoria
59                    Exit Sub
60                Else
61                    lngNUM_PEDIDO = 0
62                    lngCOD_LOJA = 0
63                    cmdVisualiza.Enabled = False
64                    cmdFinalizar.Enabled = False
65                    cmdExcluir.Enabled = False
66                    Call LiberaMemoria
67                End If
68            End If
69        Else
              'consulta dados dos itens
70            SQL = "select NUM_COTACAO,"
71            SQL = SQL & " COD_LOJA, COD_CLIENTE"
72            SQL = SQL & " from COTACAO "
73            SQL = SQL & " where SITUACAO=8 and cod_vend = " & sCOD_VEND
74            SQL = SQL & " ORDER BY NUM_COTACAO"
75            Set ss = dbAccess2.CreateSnapshot(SQL)
76            FreeLocks

77            If ss.EOF Then
78                ss.Close
79                Exit Sub
80            End If

81            ss.MoveFirst
82            If ss.RecordCount > 1 Then
83                With frmPedAb.grdPedAb
84                    .Cols = 3
85                    .Rows = ss.RecordCount + 1
86                    .ColWidth(0) = 1200
87                    .ColWidth(1) = 1500
88                    .ColWidth(2) = 1500

89                    .Row = 0
90                    .Col = 0
91                    .Text = "   Nr.Cota��o"
92                    .Col = 1
93                    .Text = "       Deposito"
94                    .Col = 2
95                    .Text = "   Cod. Cliente"


96                    ss.MoveFirst
97                    For i = 1 To .Rows - 1
98                        .Row = i

99                        .Col = 0
100                       .ColAlignment(0) = 2
101                       .Text = ss!NUM_COTACAO
102                       .Col = 1
103                       .ColAlignment(1) = 2
104                       .Text = ss!COD_LOJA
105                       .Col = 2
106                       .ColAlignment(2) = 2
107                       .Text = ss!Cod_cliente

108                       ss.MoveNext
109                   Next
110                   .Row = 1
111               End With

112               ss.Close
                  'mouse
113               Screen.MousePointer = vbDefault
114               frmPedAb.Label1 = "Selecione a cota��o desejada"
115               frmPedAb.Label2 = ""
116               frmPedAb.Show 1

117           Else
118               lngNUM_COTACAO = ss!NUM_COTACAO
119               lngCOD_LOJA = ss!COD_LOJA
120           End If

121           If MsgBox("Confirma a exclus�o da cota��o " & lngNUM_COTACAO, vbYesNo, "EXCLUIR COTA��O") = vbYes Then

                  'eliminar itens/cabe�alho da cotacao

122               PLSQL = " delete from ITEM_COTACAO where NUM_COTACAO =" & lngNUM_COTACAO
123               PLSQL = PLSQL & " and COD_LOJA = " & lngCOD_LOJA
124               PLSQL1 = PLSQL1 & " delete from COTACAO where NUM_COTACAO = " & lngNUM_COTACAO
125               PLSQL1 = PLSQL1 & " and COD_LOJA = " & lngCOD_LOJA

126               dbAccess2.Execute PLSQL
127               dbAccess2.Execute PLSQL1
128               FreeLocks

129               lngNUM_COTACAO = 0

130               cmdVisualiza.Enabled = False
131               cmdFinalizar.Enabled = False
132               cmdExcluir.Enabled = False
133           End If
134       End If
135       FreeLocks
136       If lngNUM_PEDIDO = 0 And lngNUM_COTACAO = 0 Then
137           Call InitVenda
138       End If
          'Call LiberaMemoria
139       Exit Sub

TrataErro:
140       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
141           Resume
142       Else
143           MsgBox "Sub cmdExcluir_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
144       End If

End Sub

Private Sub cmdFinalizar_Click()
    
    If optPedido.Value Then
        frmFimPedido.Show vbModal
        If lngNUM_PEDIDO = 0 Then
            Call InitVenda
        End If
    Else
        frmFimCotacao.txtCOD_VENDEDOR.Enabled = False
        frmFimCotacao.Show vbModal
        If lngNUM_COTACAO = 0 Then
            Call InitVenda
        End If
    End If

End Sub

Private Sub cmdHistoricoVisita_Click()
    frmHistoricoVisita.Show 1
End Sub



Private Sub cmdNovos_Click()
    strTipo_Tela = "N"
    Unload frmCesta
    frmCesta.Show 1

    If txtCOD_DPK <> "" Then
        txtCOD_DPK_LostFocus
    End If
End Sub

Private Sub cmdQuadrante_Click()
    frmAcoesQuadrante.Show 1
End Sub

Private Sub cmdSair_Click()

    Set frmAplicacao = Nothing
    Set frmGerarPedido = Nothing
    Set frmFimCotacao = Nothing
    Set frmFimPedido = Nothing
    Set frmAgenda = Nothing
    Set frmBanco = Nothing
    Set frmClienteFimPedido = Nothing
    Set frmListaCotacao = Nothing
    Set frmFornecedor = Nothing
    Set frmPlano = Nothing
    Set frmRepresentante = Nothing
    Set frmTransp = Nothing
    Set frmVendedor = Nothing
    frmVenda.Label1.Visible = False
    contador = 0
    qtd = 0

    Unload frmFabrica
    Unload frmVenda


End Sub

Private Sub cmdVeiculo_Click()
    strTipo_Tela = "V"
    frmCesta.Show 1

    If txtCOD_DPK <> "" Then
        txtCOD_DPK_LostFocus
    End If
End Sub

Private Sub cmdVisualiza_Click()
    Screen.MousePointer = 11
    If optPedido.Value Then
        frmVisPedido.Show vbModal
    Else
        frmVisCotacao.Show vbModal
    End If

End Sub

Public Sub Consulta_Ligacoes()

1     On Error GoTo Trata_Erro

2     frmVenda.SSFrame2.Caption = "Liga��es: " & Date
        
3     SQL = "Select iif(isnull(sum(iif(tipo_ligacao='1',1,0))),0,sum(iif(tipo_ligacao='1',1,0))) as ativas, " & _
          "iif(IsNull(Sum(iif(tipo_ligacao='2',1,0))), 0, Sum(iif(tipo_ligacao='2',1,0))) as receptivas " & _
          "From contato " & _
          "where cod_vend = " & Val(sCOD_VEND) & " and " & _
          "cdate(dt_contato)>= #" & Format(Date, "mm/dd/yy") & "# and " & _
          "cdate(dt_contato)< #" & Format(Date + 1, "mm/dd/yy") & "#"

4     Set ss1 = dbAccess2.CreateSnapshot(SQL)
5     FreeLocks

6     If ss1.EOF Then
7       frmVenda.txtAtivas = 0
8       frmVenda.txtReceptivas = 0
9       frmVenda.txtLig_Total = 0
10    Else
11      frmVenda.txtAtivas = ss1!ativas
12      frmVenda.txtReceptivas = ss1!receptivas
13      frmVenda.txtLig_Total = CLng(ss1!ativas) + CLng(ss1!receptivas)
14    End If

Trata_Erro:
15    If Err.Number <> 0 Then
16       MsgBox "Sub Consulta_ligacoes" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl & vbCrLf & "Select: " & SQL
17    End If
End Sub

Private Sub Consulta_Pedidos()
          
    On Error GoTo Trata_Erro
    
          Dim Data As String

1         Data = Format(Date, "mm/dd/yyyy")

2         SQL = "Select count(*) AS total " & _
                  "From pedido " & _
                  "Where (cod_vend=" & sCOD_VEND & " or cod_repres=" & sCOD_VEND & ") and " & _
                  "      dt_digitacao = #" & Data & "# "

3         Set ss1 = dbAccess.CreateSnapshot(SQL)
4         FreeLocks

5         If ss1.EOF Then
6             frmVenda.txtTotal_Pedidos = 0
7         Else
8             frmVenda.txtTotal_Pedidos = ss1!total
9         End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Consulta_Pedidos" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Sub ContVenda()
1         On Error GoTo TrataErro

          Dim ss As Snapshot
          Dim ss2 As Snapshot
          Dim ss3 As Snapshot
          Dim ss4 As Snapshot
          Dim tam As Byte
          Dim i As Byte

2         SQL = "select COD_CLIENTE,"
3         SQL = SQL & "COD_REPRES,"
4         SQL = SQL & "COD_VEND,"
5         SQL = SQL & "FL_DIF_ICM,"
6         SQL = SQL & "COD_PLANO,"
7         SQL = SQL & "PC_ACRESCIMO,"
8         SQL = SQL & "PC_DESCONTO, "
9         SQL = SQL & "PC_DESC_SUFRAMA "
10        If optPedido.Value Then
              'continuar pedido
11            SQL = SQL & ",COD_UF "
12            SQL = SQL & ",COD_LOJA "
13            SQL = SQL & "from PEDIDO "
14            SQL = SQL & "where NUM_PENDENTE = " & CStr(lngNUM_PEDIDO)
15            Set ss = dbAccess.CreateSnapshot(SQL)
16            FreeLocks

17            If ss.EOF() And ss.BOF Then
18                MsgBox "N�O FOI POSSIVEL CONTINUAR O PEDIDO DE NUMERO PENDENTE " & CStr(lngNUM_PEDIDO), vbCritical, " A T E N � � O "
19                GoTo Fim
20            End If
21            If IsNull(ss!pc_desc_suframa) Then
22                lblDescSuframa.Caption = 0
23            Else
24                lblDescSuframa.Caption = ss!pc_desc_suframa
25            End If
26        Else
              'continuar cota��o

27            SQL = SQL & "from COTACAO "
28            SQL = SQL & "where NUM_COTACAO = " & CStr(lngNUM_COTACAO) & " and "
29            SQL = SQL & " COD_LOJA = " & CStr(lngCOD_LOJA)



30            Set ss2 = dbAccess2.CreateSnapshot(SQL)
31            FreeLocks

32            If ss2.EOF() And ss2.BOF Then
33                MsgBox "N�O FOI POSSIVEL CONTINUAR A COTACAO DE NUMERO " & CStr(lngNUM_COTACAO), vbCritical, " A T E N � � O "
34                GoTo Fim
35            End If
36            If IsNull(ss2!pc_desc_suframa) Then
37                lblDescSuframa.Caption = 0
38            Else
39                lblDescSuframa.Caption = ss2!pc_desc_suframa
40            End If
41        End If

          'carregar dados do cliente
42        If optPedido.Value Then
43            If ss!Cod_cliente = 0 Then
                  'cliente
44                Call InitCliente
                  'uf
45                Call GetListaUf
46                tam = cboUf.ListCount
47                For i = 0 To tam - 1
48                    If cboUf.List(i) = ss!cod_uf Then
49                        Exit For
50                    End If
51                Next i
52                cboUf.ListIndex = -1
53                cboUf.ListIndex = i

                  'deposito
54                For i = 0 To tam - 1
55                    If cboDeposito.List(i) = ss!COD_LOJA Then
56                        Exit For
57                    End If
58                Next i
59                cboUf.ListIndex = -1
60                cboUf.ListIndex = i

                  'diferenca de icm
61                If ss!FL_DIF_ICM = "S" Then
62                    chkDifIcm.Value = vbChecked
63                Else
64                    chkDifIcm.Value = vbUnchecked
65                End If

66            ElseIf Not GetCliente(ss!Cod_cliente, 0) Then
67                If optPedido.Value = vbChecked Then
68                    MsgBox "CLIENTE " & CStr(ss!Cod_cliente) & " INV�LIDO" & Chr(13) & "PEDIDO " & CStr(lngNUM_PEDIDO), vbCritical, "LIGUE S.O.S."
69                Else
70                    MsgBox "CLIENTE " & CStr(ss!Cod_cliente) & " INV�LIDO" & Chr(13) & "COTA��O " & CStr(lngNUM_COTACAO), vbCritical, "LIGUE S.O.S."
71                End If
72            End If

              'carregar dados do plano
73            If Not GetPlanoPgto(ss!COD_PLANO, ss!PC_ACRESCIMO, ss!PC_DESCONTO) Then
74                If optPedido.Value = vbChecked Then
75                    MsgBox "PLANO DE PAGAMENTO " & CStr(ss!COD_PLANO) & " INV�LIDO" & Chr(13) & "PEDIDO " & CStr(lngNUM_PEDIDO), vbCritical, "LIGUE S.O.S."
76                Else
77                    MsgBox "PLANO DE PAGAMENTO " & CStr(ss!COD_PLANO) & " INV�LIDO" & Chr(13) & "COTA��O " & CStr(lngNUM_COTACAO), vbCritical, "LIGUE S.O.S."
78                End If
79            End If

              'carrega desconto adicional
80            txtDescAdicional.Text = "0"

              'diferen�a de ICM
81            If ss!FL_DIF_ICM = "S" Then 'And cboUf.List(cboUf.ListIndex) <> uf_origem Then
82                chkDifIcm.Value = vbChecked
83            Else
84                chkDifIcm.Value = vbUnchecked
85            End If
86            SQL = "Select nome_fantasia"
87            SQL = SQL & " From loja"
88            SQL = SQL & " Where cod_loja = " & CStr(lngCOD_LOJA)


89            Set ss4 = dbAccess2.CreateSnapshot(SQL)
90            FreeLocks

91            cboDeposito.Text = Format$(ss!COD_LOJA, "00") & "-" & ss4!NOME_FANTASIA


92        Else
93            If ss2!Cod_cliente = 0 Then
                  'cliente
94                Call InitCliente
95                Call GetFilial
                  'uf
96                Call GetListaUf
97                tam = cboUf.ListCount
98                For i = 0 To tam - 1
99                    If cboUf.List(i) = ss!cod_uf Then
100                       Exit For
101                   End If
102               Next i
103               cboUf.ListIndex = -1
104               cboUf.ListIndex = i
                  'diferenca de icm
105               If ss2!FL_DIF_ICM = "S" Then
106                   chkDifIcm.Value = vbChecked
107               Else
108                   chkDifIcm.Value = vbUnchecked
109               End If

110           ElseIf Not GetCliente(ss2!Cod_cliente, 0) Then
111               If optPedido.Value = vbChecked Then
112                   MsgBox "CLIENTE " & CStr(ss2!Cod_cliente) & " INV�LIDO" & Chr(13) & "PEDIDO " & CStr(lngNUM_PEDIDO), vbCritical, "LIGUE S.O.S."
113               Else
114                   MsgBox "CLIENTE " & CStr(ss2!Cod_cliente) & " INV�LIDO" & Chr(13) & "COTA��O " & CStr(lngNUM_COTACAO), vbCritical, "LIGUE S.O.S."
115               End If
116           End If

              'carregar dados do plano
117           If Not GetPlanoPgto(ss2!COD_PLANO, ss2!PC_ACRESCIMO, ss2!PC_DESCONTO) Then
118               If optPedido.Value = vbChecked Then
119                   MsgBox "PLANO DE PAGAMENTO " & CStr(ss2!COD_PLANO) & " INV�LIDO" & Chr(13) & "PEDIDO " & CStr(lngNUM_PEDIDO), vbCritical, "LIGUE S.O.S."
120               Else
121                   MsgBox "PLANO DE PAGAMENTO " & CStr(ss2!COD_PLANO) & " INV�LIDO" & Chr(13) & "COTA��O " & CStr(lngNUM_COTACAO), vbCritical, "LIGUE S.O.S."
122               End If
123           End If

              'carrega desconto adicional
124           txtDescAdicional.Text = "0"

              'diferen�a de ICM
              'Comentado por Eduardo - 18/07/2006
'125           If ss2!FL_DIF_ICM = "S" And cboUf.List(cboUf.ListIndex) <> uf_origem Then
126               chkDifIcm.Value = vbChecked
'127           Else
'128               chkDifIcm.Value = vbUnchecked
'129           End If
130       End If

          'limpar frame de produto
131       InitFrmeProduto

          'habilitar botoes
132       cmdVisualiza.Enabled = True
133       cmdFinalizar.Enabled = True
134       cmdExcluir.Enabled = True
135       FreeLocks
Fim:
136       Close

137       Exit Sub

TrataErro:
138       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
139           Label1.Visible = True
140           contador = contador + 1
141           Label1.Caption = contador
142           Resume
143       ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
144           MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
145           ReInitProduto
146           Exit Sub
147       Else
148           MsgBox "Sub ContVenda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
149       End If

End Sub

Private Sub Form_Activate()
    Screen.MousePointer = 0
End Sub

Private Sub Form_Load()

1         On Error GoTo TrataErro

          Dim i As Byte

          dbAccess2.Execute "Delete * from CLASS_FISCAL_RED_BASE"

2         Me.Caption = Me.Caption & " - Vers�o: " & Versao

3         fl_existe_pedido = "N"
4         fl_existe_cotacao = "N"
          'mouse pointer
5         Screen.MousePointer = vbHourglass

          '-- INICIALIZA ALGUMAS VARI�VEIS DE C�LCULO DE ICMS RETIDO
6         w_tp_cliente = "R"
7         w_vl_baseicm1 = 0
8         w_vl_baseicm2 = 0
9         w_vl_basemaj_1 = 0
10        w_vl_basemaj_3 = 0
11        w_cliente_antec = "N"
12        w_pc_mg_lucro_antec = 0
13        w_pc_margem_lucro = 0
14        w_class_antec = 0
15        w_class_antec_RJ = 0
16        w_inscr_suframa = 0
17        w_vl_baseicm1_ret = 0
18        w_vl_baseicm2_ret = 0
19        w_vl_base_1 = 0
20        w_vl_base_1_aux = 0
21        w_vl_base_2 = 0
22        w_vl_base_2_aux = 0
23        w_vl_base_3 = 0
24        w_vl_baseisen = 0
25        w_vl_baseisen_aux = 0
26        w_vl_baseisen_aux1 = 0
27        w_vl_base_5 = 0
28        w_vl_base_6 = 0
29        w_vl_baseoutr = 0
30        w_vl_icmretido = 0
31        w_cod_procedencia = 0
32        w_class_fiscal = 0
          '--

          'carrega deposito default e depositos "visao"
37        SQL = "Select a.cod_loja as cod_loja,"
38        SQL = SQL & " b.nome_fantasia as nome_fantasia"
39        SQL = SQL & " from deposito_visao a, loja b"
40        SQL = SQL & " where a.cod_loja=b.cod_loja "
41        SQL = SQL & " and a.nome_programa = 'FIL030'"
42        SQL = SQL & " ORDER BY A.COD_LOJA       "

43        Set ss1 = dbAccess2.CreateSnapshot(SQL)
44        FreeLocks

45        If ss1.EOF Then
46            MsgBox "Tabela DEPOSITO_VISAO est� vazia ligue para Depto.Sistemas", vbExclamation, "Aten��o"
47            ss1.Close
48            Exit Sub
50        End If

53        For i = 1 To ss1.RecordCount
54            frmVenda.cboDeposito.AddItem Format(ss1!COD_LOJA, "00") & "-" & ss1!NOME_FANTASIA
55            ss1.MoveNext
56        Next

57        If frmVenda.cboDeposito.ListCount = 1 Then
58            frmVenda.cboDeposito.Enabled = False
59        Else
60            frmVenda.cboDeposito.Enabled = True
61        End If

62        frmVenda.cboDeposito.Text = deposito_default

63        Call Consulta_Ligacoes
64        Call Consulta_Pedidos
65        Call Taxa_Aproveitamento

          'verificar pedido pendente
66        SQL = "select NUM_PENDENTE,"
67        SQL = SQL & "COD_LOJA,"
68        SQL = SQL & "COD_CLIENTE,"
69        SQL = SQL & "COD_REPRES,"
70        SQL = SQL & "FL_DIF_ICM,"
71        SQL = SQL & "COD_PLANO,"
72        SQL = SQL & "PC_ACRESCIMO,"
73        SQL = SQL & "PC_DESCONTO "
74        SQL = SQL & " from PEDIDO "
75        SQL = SQL & " where FL_PEDIDO_FINALIZADO = 'N' and COD_VEND = " & CStr(sCOD_VEND)
76        SQL = SQL & " Order by num_pendente    "

77        Set ss = dbAccess.CreateSnapshot(SQL)
78        FreeLocks


79        If ss.EOF() And ss.BOF Then
              'n�o ha pedido pendente
80            lngNUM_PEDIDO = 0
81            fl_existe_pedido = "N"

              '--------- s� ir� pegar a cota��o quando n�o existir pedido pendente
              'verificar cota��o pendente
82            SQL = "select NUM_COTACAO,"
83            SQL = SQL & " COD_LOJA,"
84            SQL = SQL & " COD_CLIENTE,"
85            SQL = SQL & " COD_REPRES,"
86            SQL = SQL & " COD_VEND,"
87            SQL = SQL & " FL_DIF_ICM,"
88            SQL = SQL & " COD_PLANO,"
89            SQL = SQL & " PC_ACRESCIMO,"
90            SQL = SQL & " PC_DESCONTO"
91            SQL = SQL & " from COTACAO "
92            SQL = SQL & " where "
93            SQL = SQL & " cod_vend = " & CStr(sCOD_VEND) & " And "
94            SQL = SQL & " SITUACAO = 8"

95            Set ss1 = dbAccess2.CreateSnapshot(SQL)
96            FreeLocks

97            If ss1.EOF() And ss1.BOF Then
                  'n�o ha cotacao pendente
98                lngNUM_COTACAO = 0
99                FIL1 = sCOD_VEND
100               fil = 0
101               fl_existe_cotacao = "N"
102           Else
103               lngNUM_COTACAO = ss1!NUM_COTACAO
104               lngCOD_LOJA = ss1!COD_LOJA
105               fil = ss1!Cod_Repres
106               FIL1 = sCOD_VEND
107               fl_existe_cotacao = "S"
108           End If

109           ss1.Close
              '-----------------
110       Else
111           ss.MoveFirst
112           lngNUM_PEDIDO = ss!num_pendente
113           lngCOD_LOJA = ss!COD_LOJA
114           fil = IIf(ss!Cod_Repres = 0, sCOD_VEND, ss!Cod_Repres)
115           fl_existe_pedido = "S"
116       End If
117       ss.Close


          'inicializar form
118       If lngNUM_PEDIDO > 0 And lngNUM_COTACAO > 0 Then
              'selecionar cotacao ou pedido para continuar
119           optPedido.Value = True
120           Call ContVenda
121       ElseIf lngNUM_PEDIDO > 0 Then
              'selecionar pedido
122           optPedido.Value = True
123           Call ContVenda
124       ElseIf lngNUM_COTACAO > 0 Then
              'selecionar cotacao
125           optCotacao.Value = True
126           Call ContVenda
127       Else
              'selecionar cotacao - default
128           optCotacao.Value = True
129           Call InitVenda
130           Call GetFilial
131       End If

134       Call TABELA_VENDA
135       uf_destino = cboUf.Text

          'mouse pointer
136       Screen.MousePointer = vbDefault


137       Exit Sub

TrataErro:
138       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
139           Label1.Visible = True
140           contador = contador + 1
141           Label1.Caption = contador
142           Resume
143       Else
144           MsgBox "Sub Form_Load FrmVenda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
145       End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    'liberar menu
    MDIForm1.mnuVendas.Visible = True
    MDIForm1.mnuSair.Visible = True
    MDIForm1.mnuCotacao.Visible = True
    MDIForm1.mnuAgenda.Visible = True
    MDIForm1.mnuSobre.Visible = True
    MDIForm1.sspMenu.Visible = True
End Sub

Private Function GetCliente(lngCOD_CLIENTE As Long, strCGC As String) As Boolean
    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim ss1 As Snapshot
    Dim ss4 As Snapshot
    Dim ss5 As Snapshot
    Dim sQuadrante As Snapshot
    Dim sHistVisita As Snapshot
    Dim tam As Byte
    Dim i As Byte
    Dim lngLinha As Integer
    Dim ssLimite As Snapshot, dblLimite As Double 'TI-4405
    
    cmdQuadrante.Visible = False
    GetCliente = False
    
    SQL = "select cli.COD_CLIENTE, cli.ddd1, cli.fone1,"
    SQL = SQL & " cli.CGC,"
    SQL = SQL & " cli.NOME_CLIENTE as nome_cliente,"
    SQL = SQL & " cli.INSCR_ESTADUAL as inscr_estadual,"
    SQL = SQL & " cli.INSCR_SUFRAMA as inscr_suframa,"
    SQL = SQL & " cli.FL_CONS_FINAL as fl_cons_final,"
    SQL = SQL & " cli.SITUACAO,"
    SQL = SQL & " cli.CLASSIFICACAO as CLASSIFICACAO,"
    SQL = SQL & " cid.NOME_CIDADE as nome_cidade,"
    SQL = SQL & " cli.cod_tipo_cliente as cod_tipo_cliente,"
    SQL = SQL & " cid.COD_UF, cli.caracteristica "
    SQL = SQL & " ,cli.tp_empresa as TP_EMPRESA"
    SQL = SQL & " from CLIENTE cli, CIDADE cid "
    SQL = SQL & " where cli.COD_CIDADE = cid.COD_CIDADE and "
    
    If lngCOD_CLIENTE > 0 Then
        SQL = SQL & " cli.COD_CLIENTE = " & CStr(lngCOD_CLIENTE)
    ElseIf strCGC <> "" Then
        SQL = SQL & " cli.CGC = " & CStr(strCGC)
    Else
        strIE = ""
        lblTipoCliente.Caption = ""
        Exit Function
    End If

    Set ss = dbAccess2.CreateSnapshot(SQL)
    FreeLocks

    If ss.EOF And ss.BOF Then
        MsgBox "Cliente n�o Cadastrado", vbInformation, "Aten��o"
        strTP_Empresa = "M"
        InitCliente
        frmVenda.txtCOD_CLIENTE.SetFocus
    ElseIf ss!situacao = 9 Then
        MsgBox "Cliente Desativado", vbInformation, "Aten��o"
        InitCliente
        frmVenda.txtCOD_CLIENTE.SetFocus
    ElseIf ss!situacao = 0 Then
        strTP_Empresa = IIf(IsNull(Trim(ss!tp_empresa)), "M", Trim(ss!tp_empresa))
        txtCOD_CLIENTE.Text = ss!Cod_cliente
        lngCOD_CLIENTE = ss!Cod_cliente
        UF = ss!cod_uf
        TxtFone = ss!ddd1 & " " & ss!fone1
        
        'Quadrante
          
        SQL = " SELECT top 1 R_CLIENTE_QUADRANTE.COD_CLIE, R_CLIENTE_QUADRANTE.ANO_MES, R_CLIENTE_QUADRANTE.COD_QUAD From DATAS, R_CLIENTE_QUADRANTE, CLIENTE "
        SQL = SQL & " Where R_CLIENTE_QUADRANTE.COD_CLIE= " & lngCOD_CLIENTE & " AND CLIENTE.COD_CLIENTE = R_CLIENTE_QUADRANTE.COD_CLIE"
        SQL = SQL & " order by R_CLIENTE_QUADRANTE.ANO_MES desc;"
          
        Set sQuadrante = dbAccess2.CreateSnapshot(SQL)
        If sQuadrante.EOF = False Then
            cmdQuadrante.Visible = True
            cmdQuadrante.Caption = sQuadrante!COD_QUAD
          Select Case cmdQuadrante.Caption
              Case "Q1"
                  cmdQuadrante.ToolTipText = "Bom Volume de Compra com Boa Margem de Lucro"
              Case "Q2"
                  cmdQuadrante.ToolTipText = "Bom Volume de Compra e Margem de Lucro abaixo do Ideal"
              Case "Q3"
                  cmdQuadrante.ToolTipText = "Volume de Compra abaixo do Ideal e Boa Margem de Lucro"
              Case "Q4"
                  cmdQuadrante.ToolTipText = "Volume de Compra e Margem de Lucro abaixo do Ideal"
              Case "SC"
                  cmdQuadrante.ToolTipText = "Sem Compra"
              Case Else
                  cmdQuadrante.ToolTipText = ""
                  cmdQuadrante.Visible = False
          End Select
        Else
            cmdQuadrante.Visible = False
        End If
        
        
        'Hist�rico de visitas
        SQL = "SELECT A.COD_CLIENTE"
        SQL = SQL & " FROM CLIENTE A, VISITA_PROMOTOR_HIST B"
        SQL = SQL & " Where A.CGC = B.CGC And A.Cod_cliente = " & lngCOD_CLIENTE
        Set sHistVisita = dbAccess2.CreateSnapshot(SQL)
        If sHistVisita.EOF = False Then
            cmdHistoricoVisita.Visible = True
        End If
        
        
        '-- CARREGA VARI�VEIS PARA UTILIZAR EM C�LCULO DO ICMS RETIDO
        w_cod_uf_destino = ss!cod_uf
        w_inscr_estadual = ss!inscr_estadual
        w_fl_cons_final = ss!fl_cons_final
        w_inscr_suframa = IIf(IsNull(ss!inscr_suframa), 0, ss!inscr_suframa)
        
        'VERIFICA REPRES
        If fl_existe_pedido = "N" Or fl_existe_cotacao = "N" Then
            
            'RTI-142 - Verifica se o usu�rio � do tipo Gerente, caso seja, busca o seu c�digo.
            If (Tipo_Usuario <> "G") Then
            
            SQL = "Select a.cod_repres, b.pseudonimo,"
            SQL = SQL & " b.cod_filial, c.sigla as filial,"
            SQL = SQL & " b.tipo"
            SQL = SQL & " From r_clie_repres a, representante b,"
            SQL = SQL & " filial c"
            SQL = SQL & " Where a.cod_cliente = " & txtCOD_CLIENTE & " and"
            SQL = SQL & " b.cod_filial = c.cod_filial and"
            SQL = SQL & " a.cod_repres = b.cod_repres"
            SQL = SQL & " Order by b.cod_filial, a.cod_repres"
            
            Else
            
                SQL = "Select b.cod_repres, b.pseudonimo,"
                SQL = SQL & " b.cod_filial, c.sigla as filial,"
                SQL = SQL & " b.tipo"
                SQL = SQL & " From  representante b,"
                SQL = SQL & " filial c"
                SQL = SQL & " Where "
                SQL = SQL & " b.cod_filial = c.cod_filial and"
                SQL = SQL & " b.tipo = 'G' "
                SQL = SQL & " Order by b.cod_filial, b.cod_repres"
            
            End If
            
            Set ss5 = dbAccess2.CreateSnapshot(SQL)
                If ss5.EOF And ss5.BOF Then
                    MsgBox "Cliente sem representante cadastrado, entre em contato com o Depto. SPC.", vbInformation, "Aten��o"
                    InitCliente
                    frmVenda.txtCOD_CLIENTE.SetFocus
                    Exit Function
                End If
                
                If ss5.RecordCount > 1 Then
                    With frmRepres.grdrep
                        .Cols = 4
                        .Rows = ss5.RecordCount + 1
                        .ColWidth(0) = 660
                        .ColWidth(1) = 1500
                        .ColWidth(2) = 2000
                        .ColWidth(3) = 800
                        
                        .Row = 0
                        .Col = 0
                        .Text = "COD."
                        .Col = 1
                        .Text = "REPRES."
                        .Col = 2
                        .Text = "FILIAL"
                        .Col = 3
                        .Text = "TIPO"
                        ss5.MoveFirst
                        For i = 1 To .Rows - 1
                            .Row = i
                            .Col = 0
                            .Text = ss5!Cod_Repres
                            .Col = 1
                            .Text = ss5!pseudonimo
                            .Col = 2
                            .Text = Format(ss5!Cod_Filial, "0000") & " - " & ss5!filial
                            .Col = 3
                            .Text = ss5!TIPO
                            ss5.MoveNext
                        Next
                        .Row = 1
                    End With
                    If frmRepres.Visible = False Then
                    frmRepres.Show 1
                    End If
                    DoEvents
                Else
                    cboFilial.Text = Format(ss5!Cod_Filial, "0000") & " - " & ss5!filial
                    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
                    If ss5!TIPO = "R" Or ss5!TIPO = "P" Or ss5!TIPO = "G" Then
                        lngCOD_REPRES = ss5!Cod_Repres
                        txtRepres = Format(ss5!Cod_Repres, "0000") & "-" & ss5!pseudonimo
                    Else
                       lngCOD_REPRES = 0
                        txtRepres = "0000"
                    End If
                    
                    SQL = "Select pseudonimo from representante where cod_repres= " & sCOD_VEND
                    Set ss1 = dbAccess2.CreateSnapshot(SQL)
                    
                    txtVend = Format(sCOD_VEND, "0000") & "-" & ss1!pseudonimo
                    
                    fil = Format(lngCOD_REPRES, "0000")
                    FIL1 = Format(sCOD_VEND, "0000")
                    Cod_Filial = Format(ss5!Cod_Filial, "0000")
                    Sigla = ss5!filial
                End If
            Else
                If fil = 0 Then
                    txtRepres = "0000"
                Else
                    SQL = "Select a.pseudonimo, a.cod_filial, b.sigla "
                    SQL = SQL & " From representante a, filial b"
                    SQL = SQL & " Where cod_repres = " & fil & " and "
                    SQL = SQL & " a.cod_filial=b.cod_filial"
                    Set ss1 = dbAccess2.CreateSnapshot(SQL)

                    txtRepres.Text = Format(fil, "0000") & " - " & ss1!pseudonimo
                    Cod_Filial = ss1!Cod_Filial
                    Sigla = ss1!Sigla
                End If
                
                SQL = "Select a.pseudonimo, a.cod_filial, b.sigla "
                SQL = SQL & " From representante a, filial b"
                SQL = SQL & " Where cod_repres = " & sCOD_VEND & " and "
                SQL = SQL & " a.cod_filial=b.cod_filial"
                Set ss1 = dbAccess2.CreateSnapshot(SQL)
                
                txtVend = Format(sCOD_VEND, "0000") & "-" & ss1!pseudonimo

                If fil = 0 Then
                    Cod_Filial = ss1!Cod_Filial
                    Sigla = ss1!Sigla
                End If
            End If
            
            'cboDeposito.SetFocus
            SQL = "Select cod_loja "
            SQL = SQL & " From r_fildep"
            SQL = SQL & " Where cod_filial = " & Cod_Filial
            Set ss1 = dbAccess2.CreateSnapshot(SQL)
            
            If Not ss1.EOF Then
                If Format(Trim(ss1!COD_LOJA), "00") <> Mid(Trim(deposito_default), 1, 2) Then
                    MsgBox "Cliente pertence a filial : " & Cod_Filial & ". N�o pode ser digitado por esta filial.", vbExclamation, "Aten��o"
                    ss1.Close
                    InitCliente
                    frmVenda.txtCOD_CLIENTE.SetFocus
                    Exit Function
                End If
            End If
            
            'carregar dados
            strIE = Trim$(ss!inscr_estadual)
            If IsNull(ss!inscr_suframa) Then
                strIS = ""
            Else
                strIS = Trim$(ss!inscr_suframa)
            End If
            
            txtCOD_CLIENTE.Text = ss!Cod_cliente
            txtCGC.Text = ss!cgc
            lblClass.Caption = ss!CLASSIFICACAO
            lblNOME_CLIENTE.Caption = ss!NOME_CLIENTE
            lblNOME_CIDADE.Caption = ss!NOME_CIDADE
            cboFilial.Text = Format(Cod_Filial, "0000") & "-" & Sigla
            
            'uf
            Call GetListaUf
            tam = cboUf.ListCount
            For i = 0 To tam - 1
                If cboUf.List(i) = ss!cod_uf Then
                    uf_destino = ss!cod_uf
                    Exit For
                End If
            Next i
            cboUf.ListIndex = -1
            cboUf.ListIndex = i
            cboUf.Enabled = False
            cboFilial.Enabled = False
            
            'tipo de cliente
            lblTipoCliente.AutoSize = True
            If ss!fl_cons_final = "S" Then
                lblTipoCliente.Caption = "CONS. FINAL"
                tipo_cliente = "CONS. FINAL"
                w_tp_cliente = "C"
            Else
                If strIE = "ISENTO" Then
                    lblTipoCliente.Caption = "ISENTO"
                    tipo_cliente = "ISENTO"
                    w_tp_cliente = "I"
                Else
                    lblTipoCliente.Caption = "REVENDEDOR"
                    tipo_cliente = "REVENDEDOR"
                    w_tp_cliente = "R"
                End If
           End If

            If strIS <> "" Then
                lblIS.Visible = True
                lblIS.Caption = "INSCRITO SUFRAMA"
            End If
            lblTipoCliente.AutoSize = False
            'caracteristica do cliente
            strCaract = ss!caracteristica
            
            SQL = "Select desc_caracteristica,"
            SQL = SQL & " fl_desc_automatico"
            SQL = SQL & " From cliente_caracteristica"
            SQL = SQL & " where caracteristica = '" & strCaract & "' "
            Set ss4 = dbAccess2.CreateSnapshot(SQL)
            
            If ss4.EOF And ss4.BOF Then
                lblCaracteristica = ""
                strDesc_auto = "N"
            Else
                lblCaracteristica = ss4!desc_caracteristica
                strDesc_auto = ss4!fl_desc_automatico
            End If
           
            SQL = "Select * from v_cliente_fiel Where cod_cliente =" & frmVenda.txtCOD_CLIENTE.Text
            Set ss4 = dbAccess2.CreateDynaset(SQL)
            
            If Not ss4.EOF Then
                lblTipo_Fiel.Caption = ss4!desc_fiel
            End If
            
            'valida funcao
            GetCliente = True
            
            'TI-3221
            DoEvents
            If txtCOD_FORNECEDOR = "" Then
                SQL = " SELECT COD_CLIENTE,categoria "
                SQL = SQL & " FROM VDR_CLIENTE_CATEG_VDR "
                SQL = SQL & " WHERE COD_CLIENTE= " & txtCOD_CLIENTE.Text
                Set ss = dbAccess2.CreateSnapshot(SQL)
                
                If Not ss.BOF And Not ss.EOF Then
                    If ss!categoria = "A" Or ss!categoria = "B" Or ss!categoria = "C" Or ss!categoria = "D" Then
                        MsgBox "Este cliente � SERVI�O AUTORIZADO BOSCH DIESEL, " & vbCrLf & _
                        "se for vender diesel efetue a venda pelo sistema VDR " & vbCrLf & _
                        "onde as condi��es s�o diferenciadas.", vbInformation, "Aten��o"
                    ElseIf ss!categoria = "E" Or ss!categoria = "F" Or ss!categoria = "G" Or ss!categoria = "H" Then
                        MsgBox "Este cliente � DOPB, se for vender PST(Linha controlada) " & vbCrLf & _
                        "efetue a venda pelo Sistema VDR onde as condi��es s�o diferenciadas.", vbInformation, "Aten��o"
                    End If
                End If
            End If
            'FIM TI-3221

            'TI-4405
            SQL = " SELECT limite_credito From CLIE_CREDITO "
            SQL = SQL & " Where cod_cliente = " & lngCOD_CLIENTE
              
            Set ssLimite = dbAccess2.CreateSnapshot(SQL)
            dblLimite = 0
            
            If Not ssLimite.EOF And Not ssLimite.BOF Then
                dblLimite = ssLimite!limite_credito
            End If
    
            lblLimite = Format$(dblLimite, "0.00")
            If dblLimite >= 0 Then
                lblLimite.ForeColor = vbBlue
            Else
                lblLimite.ForeColor = vbRed
            End If
            'FIM TI-4405
        
            'carregar tabela Venda_Limitada
            Dim CODUF2 As String
            Dim coduf As String
            Dim codloja As Integer
            
            TpCliente = Left(tipo_cliente, 1)
            CODUF2 = cboUf.Text
            SQL = "select * from VENDA_LIMITADA where tp_cliente = '" & TpCliente & "' and cod_uf = '" & CODUF2 & "'"
            Set ss7 = dbAccess2.CreateSnapshot(SQL)
            FreeLocks
            If Not ss7.EOF Then
                coduf = ss7!cod_uf
                codloja = ss7!cod_loja_obrigatoria
                SQL = "select cod_loja, nome_fantasia from loja where cod_loja = " & codloja
                
                Set ss7 = dbAccess2.CreateSnapshot(SQL)
                FreeLocks
                If Not ss7.EOF Then
                    cboDeposito.Text = Format(ss7!COD_LOJA, "00") & "-" & ss7!NOME_FANTASIA
                Else
                    MsgBox "Cliente do DF, pedido deve ser digitado pelo CD DF!"
                    Unload Me
                    frmVenda.Show
                End If
            End If
            
            '-- MONTA GRAFICO DO CLIENTE COM AS COMPRAS DO M�S ATUAL (VALOR_1) E OS 3 MESES ANTERIORES.
            SQL = "Select * from cliente_acumulado where cod_cliente = " & txtCOD_CLIENTE.Text
            Set ss7 = dbAccess2.CreateSnapshot(SQL)
            FreeLocks
            If ss7.EOF Then
                Graph1.Visible = False
            Else
                Graph1.NumPoints = 4
                Graph1.GraphStyle = 0
                Graph1.ThisPoint = 1
                Graph1.GraphData = IIf(IsNull(ss7!valor4), 0, ss7!valor4)
                Graph1.ThisPoint = 1
                Graph1.LabelText = IIf(IsNull(ss7!valor4), 0, ss7!valor4)
                Graph1.ThisPoint = 2
                Graph1.GraphData = IIf(IsNull(ss7!valor3), 0, ss7!valor3)
                Graph1.ThisPoint = 2
                Graph1.LabelText = IIf(IsNull(ss7!valor3), 0, ss7!valor3)
                Graph1.ThisPoint = 3
                Graph1.GraphData = IIf(IsNull(ss7!valor2), 0, ss7!valor2)
                Graph1.ThisPoint = 3
                Graph1.LabelText = IIf(IsNull(ss7!valor2), 0, ss7!valor2)
                Graph1.ThisPoint = 4
                Graph1.GraphData = IIf(IsNull(ss7!valor1), 0, ss7!valor1)
                Graph1.ThisPoint = 4
                Graph1.LabelText = IIf(IsNull(ss7!valor1), 0, ss7!valor1)
                Graph1.LegendStyle = 0
                Graph1.ThisPoint = 1
                Graph1.LegendText = IIf(IsNull(ss7!ano_mes4), "", ss7!ano_mes4)
                Graph1.ThisPoint = 2
                Graph1.LegendText = IIf(IsNull(ss7!ano_mes3), "", ss7!ano_mes3)
                Graph1.ThisPoint = 3
                Graph1.LegendText = IIf(IsNull(ss7!ano_mes2), "", ss7!ano_mes2)
                Graph1.ThisPoint = 4
                Graph1.LegendText = IIf(IsNull(ss7!ano_mes1), "", ss7!ano_mes1)
                Graph1.DrawMode = 2
                Graph1.Visible = True
            End If
            
            '-- Mostra grid com os 6 fornecedores mais comprados pelo cliente escolhido
            SQL = "Select c.cod_fornecedor, e.sigla, format(SUM(F.VL_LIQUIDO),'###,###,###') as valor "
            SQL = SQL & " From pednota_venda a, itpednota_venda b, item_cadastro c, "
            SQL = SQL & " datas d, fornecedor e, V_PEDLIQ_VENDA F "
            SQL = SQL & " Where a.cod_cliente = " & frmVenda.txtCOD_CLIENTE.Text & " and "
            SQL = SQL & " a.fl_ger_nfis='S' and a.dt_emissao_nota >= dateadd('M',-3,d.dt_faturamento) "
            SQL = SQL & " and a.dt_emissao_nota < dt_faturamento + 1 and a.tp_transacao=1 and "
            SQL = SQL & " a.situacao=0 and b.situacao=0 and c.cod_fornecedor=e.cod_fornecedor and "
            SQL = SQL & " B.NUM_ITEM_PEDIDO = F.NUM_ITEM_PEDIDO AND B.SEQ_PEDIDO = F.SEQ_PEDIDO AND "
            SQL = SQL & " B.NUM_PEDIDO=F.NUM_PEDIDO AND B.COD_LOJA=F.COD_LOJA AND "
            SQL = SQL & " b.cod_dpk=c.cod_dpk and a.seq_pedido=b.seq_pedido and "
            SQL = SQL & " a.num_pedido=b.num_pedido and a.cod_loja = b.cod_loja "
            SQL = SQL & " Group by c.cod_fornecedor,E.SIGLA "
            SQL = SQL & " Order by SUM(F.VL_LIQUIDO) DESC "
            Set ss7 = dbAccess2.CreateSnapshot(SQL)
            FreeLocks
            
            If ss7.EOF And ss7.BOF Then
                grdLinhas.Visible = False
                Exit Function
            End If
            
            If ss7.RecordCount <= 6 Then
                lngLinha = ss7.RecordCount
            Else
                lngLinha = 6
            End If

            'carrega dados
            With grdLinhas
                .Cols = 3
                .Rows = lngLinha + 1
                .ColWidth(0) = 400
                .ColWidth(1) = 1200
                .ColWidth(2) = 900
                .Row = 0
                .Col = 0
                .Text = " COD"
                .Col = 1
                .Text = " FORN."
                .Col = 2
                .ColAlignment(2) = 1
                .Text = "    VL."
                For i = 1 To lngLinha
                    .Row = i
                    .Col = 0
                    .Text = ss7!Cod_Fornecedor
                    .Col = 1
                    .Text = ss7!Sigla
                    .Col = 2
                    .Text = ss7!Valor
                    ss7.MoveNext
                Next
                .Row = 1
            End With
            grdLinhas.Visible = True
        End If
    
    Exit Function
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err.Number = 3078 Then
        MsgBox "Por favor encaminhe esta mensagem para o HelpDesk" & vbCrLf & _
        "Codigo: " & Err.Number & vbCrLf & _
        "Descri��o: " & Err.Description, vbInformation, "Aten��o"
        Resume Next
    ElseIf Err.Number = 91 Then
        Resume Next
    Else
        MsgBox "Sub GetCliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function
Private Sub GetDescontos()
1         On Error GoTo TrataErro

          Dim ssDescontos As Snapshot
          Dim dblPRECO_NORMAL As Double
          Dim dblPRECO_OFERTA As Double
          Dim dblPRECO_SP As Double
          Dim strSEQUENCIA0 As String * 1
          Dim strSEQUENCIA1 As String * 1
          Dim strSEQUENCIA2 As String * 1
          Dim ocorr_preco0 As Integer
          Dim ocorr_preco1 As Integer
          Dim ocorr_preco2 As Integer
          Dim seq0 As Integer
          Dim seq1 As Integer
          Dim seq2 As Integer


2         If chkSemanaPassada.Value = vbUnchecked Then
3             ocorr_preco0 = ocor_atu(0)
4             ocorr_preco1 = ocor_atu(1)
5             ocorr_preco2 = ocor_atu(2)
6             seq0 = seq_atu(0)
7             seq1 = seq_atu(1)
8             seq2 = seq_atu(2)
9         Else
10            ocorr_preco0 = ocor_ant(0)
11            ocorr_preco1 = ocor_ant(1)
12            ocorr_preco2 = ocor_ant(2)
13            seq0 = seq_ant(0)
14            seq1 = seq_ant(1)
15            seq2 = seq_ant(2)
16        End If


          'consulta descontos
17        SQL = " Select SEQUENCIA,"
18        SQL = SQL & " TP_TABELA,"
19        SQL = SQL & " COD_FILIAL,"
20        SQL = SQL & " COD_FORNECEDOR,"
21        SQL = SQL & " COD_DPK,"
22        SQL = SQL & " COD_GRUPO,"
23        SQL = SQL & " COD_SUBGRUPO,"
24        SQL = SQL & " PC_DESC_PERIODO1,"
25        SQL = SQL & " PC_DESC_PERIODO2,"
26        SQL = SQL & " PC_DESC_PERIODO3 "
27        SQL = SQL & " from TABELA_DESCPER "
28        SQL = SQL & " where SITUACAO = 0 and  "
29        SQL = SQL & " sequencia = " & seq0 & " and"
30        SQL = SQL & " tp_tabela=0 and"
31        SQL = SQL & " ocorr_preco = " & ocorr_preco0 & " and"
32        SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
33        SQL = SQL & " cod_filial = 0 and"
34        SQL = SQL & " cod_grupo in (0," & item_grupo & " ) and"
35        SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & " ) and"
36        SQL = SQL & " cod_dpk in (0," & txtCOD_DPK.Text & " ) "
37        SQL = SQL & " Union "
38        SQL = SQL & " select SEQUENCIA,"
39        SQL = SQL & " TP_TABELA,"
40        SQL = SQL & " COD_FILIAL,"
41        SQL = SQL & " COD_FORNECEDOR,"
42        SQL = SQL & " COD_DPK,"
43        SQL = SQL & " COD_GRUPO,"
44        SQL = SQL & " COD_SUBGRUPO,"
45        SQL = SQL & " PC_DESC_PERIODO1,"
46        SQL = SQL & " PC_DESC_PERIODO2,"
47        SQL = SQL & " PC_DESC_PERIODO3 "
48        SQL = SQL & " from TABELA_DESCPER "
49        SQL = SQL & " where SITUACAO = 0 and"
50        SQL = SQL & " sequencia = " & seq0 & " and"
51        SQL = SQL & " tp_tabela=0 and"
52        SQL = SQL & " ocorr_preco = " & ocorr_preco0 & " and"
53        SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
54        SQL = SQL & " cod_filial = " & CByte(Mid(cboFilial.Text, 1, 4)) & " and"
55        SQL = SQL & " cod_grupo in (0," & item_grupo & " ) and"
56        SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & " ) and"
57        SQL = SQL & " cod_dpk in (0," & txtCOD_DPK.Text & " ) "
58        SQL = SQL & " Union "
59        SQL = SQL & " select SEQUENCIA,"
60        SQL = SQL & " TP_TABELA,"
61        SQL = SQL & " COD_FILIAL,"
62        SQL = SQL & " COD_FORNECEDOR,"
63        SQL = SQL & " COD_DPK,"
64        SQL = SQL & " COD_GRUPO,"
65        SQL = SQL & " COD_SUBGRUPO,"
66        SQL = SQL & " PC_DESC_PERIODO1,"
67        SQL = SQL & " PC_DESC_PERIODO2,"
68        SQL = SQL & " PC_DESC_PERIODO3 "
69        SQL = SQL & " from TABELA_DESCPER "
70        SQL = SQL & " where SITUACAO = 0 and  "
71        SQL = SQL & " sequencia = " & seq1 & " and"
72        SQL = SQL & " tp_tabela=1 and"
73        SQL = SQL & " ocorr_preco = " & ocorr_preco1 & " And "
74        SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
75        SQL = SQL & " cod_filial = 0 and"
76        SQL = SQL & " cod_grupo in (0," & item_grupo & " ) and"
77        SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & " ) and"
78        SQL = SQL & " cod_dpk in (0," & txtCOD_DPK.Text & " ) "
79        SQL = SQL & " Union "
80        SQL = SQL & " select SEQUENCIA,"
81        SQL = SQL & " TP_TABELA,"
82        SQL = SQL & " COD_FILIAL,"
83        SQL = SQL & " COD_FORNECEDOR,"
84        SQL = SQL & " COD_DPK,"
85        SQL = SQL & " COD_GRUPO,"
86        SQL = SQL & " COD_SUBGRUPO,"
87        SQL = SQL & " PC_DESC_PERIODO1,"
88        SQL = SQL & " PC_DESC_PERIODO2,"
89        SQL = SQL & " PC_DESC_PERIODO3 "
90        SQL = SQL & " from TABELA_DESCPER "
91        SQL = SQL & " where SITUACAO = 0 and  "
92        SQL = SQL & " sequencia = " & seq1 & " and"
93        SQL = SQL & " tp_tabela=1 and"
94        SQL = SQL & " ocorr_preco = " & ocorr_preco1 & " and"
95        SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
96        SQL = SQL & " cod_filial = " & CByte(Mid(cboFilial.Text, 1, 4)) & " and"
97        SQL = SQL & " cod_grupo in (0," & item_grupo & ") and"
98        SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & ") and"
99        SQL = SQL & " cod_dpk in (0," & txtCOD_DPK.Text & " ) "
100       SQL = SQL & " Union"
101       SQL = SQL & " select SEQUENCIA,"
102       SQL = SQL & " TP_TABELA,"
103       SQL = SQL & " COD_FILIAL,"
104       SQL = SQL & " COD_FORNECEDOR,"
105       SQL = SQL & " COD_DPK,"
106       SQL = SQL & " COD_GRUPO,"
107       SQL = SQL & " COD_SUBGRUPO,"
108       SQL = SQL & " PC_DESC_PERIODO1,"
109       SQL = SQL & " PC_DESC_PERIODO2,"
110       SQL = SQL & " PC_DESC_PERIODO3 "
111       SQL = SQL & " from TABELA_DESCPER "
112       SQL = SQL & " where SITUACAO = 0 and  "
113       SQL = SQL & " sequencia = " & seq2 & " and"
114       SQL = SQL & " tp_tabela=2 and"
115       SQL = SQL & " ocorr_preco = " & ocorr_preco2 & " and"
116       SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
117       SQL = SQL & " cod_filial = 0 and"
118       SQL = SQL & " cod_grupo in (0," & item_grupo & " ) and"
119       SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & ") and"
120       SQL = SQL & " cod_dpk in (0," & txtCOD_DPK & " ) "
121       SQL = SQL & " Union "
122       SQL = SQL & " select SEQUENCIA,"
123       SQL = SQL & " TP_TABELA,"
124       SQL = SQL & " COD_FILIAL,"
125       SQL = SQL & " COD_FORNECEDOR,"
126       SQL = SQL & " COD_DPK,"
127       SQL = SQL & " COD_GRUPO,"
128       SQL = SQL & " COD_SUBGRUPO,"
129       SQL = SQL & " PC_DESC_PERIODO1,"
130       SQL = SQL & " PC_DESC_PERIODO2,"
131       SQL = SQL & " PC_DESC_PERIODO3 "
132       SQL = SQL & " from TABELA_DESCPER "
133       SQL = SQL & " where SITUACAO = 0 and  "
134       SQL = SQL & " sequencia = " & seq2 & " and"
135       SQL = SQL & " tp_tabela=2 and"
136       SQL = SQL & " ocorr_preco = " & ocorr_preco2 & " and"
137       SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR.Text & " and"
138       SQL = SQL & " cod_filial = " & CByte(Mid(cboFilial.Text, 1, 4)) & " and"
139       SQL = SQL & " cod_grupo in (0," & item_grupo & " ) and"
140       SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & " ) and"
141       SQL = SQL & " cod_dpk in (0," & txtCOD_DPK.Text & " ) "
142       SQL = SQL & " order by 2 desc,3 desc, 5 desc,7 desc, 6 desc"

143       If chkSemanaPassada.Value = vbUnchecked Then
144           strSEQUENCIA0 = seq_atu(0)
145           strSEQUENCIA1 = seq_atu(1)
146           strSEQUENCIA2 = seq_atu(2)

147       Else
148           strSEQUENCIA0 = seq_ant(0)
149           strSEQUENCIA1 = seq_ant(1)
150           strSEQUENCIA2 = seq_ant(2)

151       End If

152       Set ssDescontos = dbAccess2.CreateSnapshot(SQL)
153       FreeLocks


          'inicializar os descontos
154       lblPC_DESC_PERIODO1_NORMAL.Caption = ""
155       lblPC_DESC_PERIODO2_NORMAL.Caption = ""
156       lblPC_DESC_PERIODO3_NORMAL.Caption = ""
157       lblPC_DESC_PERIODO1_OFERTA.Caption = ""
158       lblPC_DESC_PERIODO2_OFERTA.Caption = ""
159       lblPC_DESC_PERIODO3_OFERTA.Caption = ""
160       lblPC_DESC_PERIODO1_SP.Caption = ""
161       lblPC_DESC_PERIODO2_SP.Caption = ""
162       lblPC_DESC_PERIODO3_SP.Caption = ""

163       If ssDescontos.EOF() And ssDescontos.BOF Then
              'n�o ha descontos
164       Else

165           If chkSemanaPassada.Value = vbChecked Then
166               If ocor_ant(0) = 2 Then
167                   dblPRECO_NORMAL = ssItem!PRECO_VENDA_ANT
168               Else
169                   dblPRECO_NORMAL = ssItem!PRECO_VENDA
170               End If
171           Else
172               If ocor_atu(0) = 2 Then
173                   dblPRECO_NORMAL = ssItem!PRECO_VENDA_ANT
174               Else
175                   dblPRECO_NORMAL = ssItem!PRECO_VENDA
176               End If
177           End If

178           If chkSemanaPassada.Value = vbChecked Then
179               If ocor_ant(1) = 2 Then
180                   dblPRECO_OFERTA = ssItem!PRECO_OF_ANT
181               Else
182                   dblPRECO_OFERTA = ssItem!PRECO_OF
183               End If
184           Else
185               If ocor_atu(1) = 2 Then
186                   dblPRECO_OFERTA = ssItem!PRECO_OF_ANT
187               Else
188                   dblPRECO_OFERTA = ssItem!PRECO_OF
189               End If
190           End If

191           If chkSemanaPassada.Value = vbChecked Then
192               If ocor_ant(2) = 2 Then
193                   dblPRECO_SP = ssItem!PRECO_SP_ANT
194               Else
195                   dblPRECO_SP = ssItem!PRECO_SP
196               End If
197           Else
198               If ocor_atu(2) = 2 Then
199                   dblPRECO_SP = ssItem!PRECO_SP_ANT
200               Else
201                   dblPRECO_SP = ssItem!PRECO_SP
202               End If
203           End If

              'localizar desconto da tabela normal
204           ssDescontos.MoveFirst
205           Do
206               If dblPRECO_NORMAL > 0 And _
                          ssDescontos!tp_tabela <> "2" And _
                          ssDescontos!tp_tabela <> "1" And _
                          lblPC_DESC_PERIODO1_NORMAL.Caption = "" And _
                          lblPC_DESC_PERIODO2_NORMAL.Caption = "" And _
                          lblPC_DESC_PERIODO3_NORMAL.Caption = "" And _
                          ssDescontos!SEQUENCIA = strSEQUENCIA0 Then

207                   If CSng(ssDescontos!PC_DESC_PERIODO1) >= 0 Then
208                       lblPC_DESC_PERIODO1_NORMAL.Caption = Format$(ssDescontos!PC_DESC_PERIODO1, "00.00")
209                   End If
210                   If CSng(ssDescontos!PC_DESC_PERIODO2) >= 0 Then
211                       lblPC_DESC_PERIODO2_NORMAL.Caption = Format$(ssDescontos!PC_DESC_PERIODO2, "00.00")
212                   End If
213                   If CSng(ssDescontos!PC_DESC_PERIODO3) >= 0 Then
214                       lblPC_DESC_PERIODO3_NORMAL.Caption = Format$(ssDescontos!PC_DESC_PERIODO3, "00.00")
215                   End If

216               End If

                  'localizar desconto da tabela de oferta
217               If dblPRECO_OFERTA > 0 And _
                          ssDescontos!tp_tabela = "1" And _
                          lblPC_DESC_PERIODO1_OFERTA.Caption = "" And _
                          lblPC_DESC_PERIODO2_OFERTA.Caption = "" And _
                          lblPC_DESC_PERIODO3_OFERTA.Caption = "" And _
                          ssDescontos!SEQUENCIA = strSEQUENCIA1 Then

218                   If CSng(ssDescontos!PC_DESC_PERIODO1) >= 0 Then
219                       lblPC_DESC_PERIODO1_OFERTA.Caption = Format$(ssDescontos!PC_DESC_PERIODO1, "00.00")
220                   End If
221                   If CSng(ssDescontos!PC_DESC_PERIODO2) >= 0 Then
222                       lblPC_DESC_PERIODO2_OFERTA.Caption = Format$(ssDescontos!PC_DESC_PERIODO2, "00.00")
223                   End If
224                   If CSng(ssDescontos!PC_DESC_PERIODO3) >= 0 Then
225                       lblPC_DESC_PERIODO3_OFERTA.Caption = Format$(ssDescontos!PC_DESC_PERIODO3, "00.00")
226                   End If

227               End If

                  'localizar desconto da tabela de SP
228               If dblPRECO_SP > 0 And _
                          ssDescontos!tp_tabela = "2" And _
                          lblPC_DESC_PERIODO1_SP.Caption = "" And _
                          lblPC_DESC_PERIODO2_SP.Caption = "" And _
                          lblPC_DESC_PERIODO3_SP.Caption = "" And _
                          ssDescontos!SEQUENCIA = strSEQUENCIA2 Then

229                   If CSng(ssDescontos!PC_DESC_PERIODO1) >= 0 Then
230                       lblPC_DESC_PERIODO1_SP.Caption = Format$(ssDescontos!PC_DESC_PERIODO1, "00.00")
231                   End If
232                   If CSng(ssDescontos!PC_DESC_PERIODO2) >= 0 Then
233                       lblPC_DESC_PERIODO2_SP.Caption = Format$(ssDescontos!PC_DESC_PERIODO2, "00.00")
234                   End If
235                   If CSng(ssDescontos!PC_DESC_PERIODO3) >= 0 Then
236                       lblPC_DESC_PERIODO3_SP.Caption = Format$(ssDescontos!PC_DESC_PERIODO3, "00.00")
237                   End If

238               End If

239               ssDescontos.MoveNext
240           Loop Until ssDescontos.EOF

241       End If

242       Exit Sub

TrataErro:
243       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
244           Resume
245       Else
246           MsgBox "Sub GetDescontos" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
247       End If

End Sub



Private Sub GetDifIcm(CalcPreco As Boolean, bTp_Desconto As Byte)
1         On Error GoTo TrataErro

          Dim strDifIcm As String
          Dim ss As Snapshot
          Dim ss2 As Snapshot
          Dim cod_dpk As Long

          Dim TIPO_CLI As String
          Dim dblPC_ICM As Double
2         lblPercDifIcm.Caption = "0"
3         strDifIcm = 0
4         dblPC_ICM = 0
          Dim BTrib_Novo


5         If uf_destino = "" Then
6             uf_destino = cboUf.Text
7         End If

8         If chkDifIcm.Value = vbUnchecked Or txtCOD_DPK.Text = "" Then
9             lblPercDifIcm.Caption = "0"
10            strDifIcm = 0
11        Else

              'Parte Nova
              'Verifica Tributa��o
12            If tipo_cliente <> "" And txtCOD_DPK.Text <> "" And chkDifIcm.Value Then
13                If tipo_cliente = "REVENDEDOR" Then
14                    TIPO_CLI = "R"
15                    w_tp_cliente = "R"
16                ElseIf tipo_cliente = "CONS. FINAL" Then
17                    TIPO_CLI = "C"
18                    w_tp_cliente = "C"
19                ElseIf tipo_cliente = "ISENTO" Then
20                    TIPO_CLI = "I"
21                    w_tp_cliente = "I"
22                End If
23            End If

24            SQL = " Select cod_tributacao_final"
25            SQL = SQL & " from uf_tpcliente "
26            SQL = SQL & " where cod_tributacao_final<>2 and "
27            SQL = SQL & " cod_tributacao = " & bCod_Trib & " and "
28            SQL = SQL & " tp_cliente = '" & TIPO_CLI & "' and "
29            SQL = SQL & " cod_uf_origem = '" & uf_origem & "'  and "

30            If strIS <> "" And uf_origem <> uf_destino Then
31                SQL = SQL & " COD_UF_DESTINO ='" & "SU" & "'"
32            Else
33                SQL = SQL & " COD_UF_DESTINO ='" & uf_destino & "' "
34            End If

35            Set ss2 = dbAccess2.CreateSnapshot(SQL)


36            If ss2.EOF Then
37                BTrib_Novo = bCod_Trib
38            Else
39                BTrib_Novo = ss2!cod_tributacao_final
40            End If

              'Fim PArte Nova

              'dados da uf
41            SQL = "select PC_ICM,PC_DIFICM "
42            SQL = SQL & " from UF_ORIGEM_DESTINO "
43            SQL = SQL & " where COD_UF_ORIGEM = '" & uf_origem & "' and"
44            SQL = SQL & " COD_UF_DESTINO = '" & uf_destino & "'"

45            Set ss = dbAccess2.CreateSnapshot(SQL)
46            FreeLocks

47            If IsNull(ss!pc_icm) Then
48                MsgBox "Problema na consulta. UF ORIGEM: " _
                          & uf_origem & " - UF DESTINO:  " & uf_destino & " . Ligue para Depto.Sistemas" _
                          , vbCritical, "Aten��o"
49                End
50            End If

51            pc_icm = ss!pc_icm

              'Verifica desconto suframa
52            If strIS <> "" And tipo_cliente = "REVENDEDOR" Then
53                lblDescSuframa.Caption = FmtBR(pc_icm)
54            Else
55                lblDescSuframa.Caption = FmtBR(0)
56            End If

              'calcula diferen�a de ICM
57            If chkDifIcm.Value = vbUnchecked Then
58                strDifIcm = 0
59            Else
60                bCod_Trib = BTrib_Novo
61                If strIE <> "ISENTO" And IIf(BTrib_Novo = "8" And ss!pc_icm <> "7", False, True) And BTrib_Novo <> "1" Then
62                    strDifIcm = ss!PC_DIFICM
63                ElseIf strIE = "ISENTO" Then
64                    strDifIcm = 0
65                ElseIf strIE <> "ISENTO" And IIf(BTrib_Novo = "8" And ss!pc_icm = "12", True, False) And BTrib_Novo <> "1" Then
66                    strDifIcm = IIf(IsNull(ss!PC_DIFICM), 0, ss!PC_DIFICM)
67                Else
68                    SQL = " Select cod_trib_revendedor,"
69                    SQL = SQL & " cod_trib_inscrito,"
70                    SQL = SQL & " cod_trib_isento"
71                    SQL = SQL & " From subst_tributaria"
72                    SQL = SQL & " Where class_fiscal =" & class_fiscal & " and "
73                    SQL = SQL & " cod_uf_origem = '" & uf_origem & "' and"

74                    If strIS <> "" And uf_origem <> uf_destino Then
75                        SQL = SQL & " cod_uf_destino = 'SU'"
76                    Else
77                        SQL = SQL & " cod_uf_destino = '" & uf_destino & "'"
78                    End If

79                    Set ss2 = dbAccess2.CreateSnapshot(SQL)
80                    FreeLocks

81                    If ss2.EOF Then
82                        strDifIcm = 0
83                    ElseIf tipo_cliente = "REVENDEDOR" Then
84                        bCod_Trib = ss2!Cod_trib_Revendedor
85                        If ss2!Cod_trib_Revendedor <> 0 Then
86                            strDifIcm = 0
87                        Else
88                            strDifIcm = ss!PC_DIFICM
89                        End If
90                    ElseIf tipo_cliente = "CONS. FINAL" Then
91                        bCod_Trib = ss2!Cod_trib_inscrito
92                        If ss2!Cod_trib_inscrito <> 0 Then
93                            strDifIcm = 0
94                        Else
95                            strDifIcm = ss!PC_DIFICM
96                        End If
97                    ElseIf tipo_cliente = "ISENTO" Then
98                        bCod_Trib = ss2!cod_trib_isento
99                        If ss2!cod_trib_isento <> 0 Then
100                           strDifIcm = 0
101                       Else
102                           strDifIcm = ss!PC_DIFICM
103                       End If
104                   End If
105               End If
106           End If
107           lblPercDifIcm.Caption = Format(strDifIcm, "00.00")
108       End If

          '*********************************************
          'Inicio - Eduardo - Base Reduzida - 05/07/2005
          '*********************************************
          If Trim(frmVenda.txtCOD_CLIENTE) <> "" Then
            Dim vTpEmpresa As String
            vTpEmpresa = fTipo_empresa(frmVenda.txtCOD_CLIENTE)
            If vTpEmpresa <> "" Then
                strTP_Empresa = vTpEmpresa
            End If
          End If
109       If strTP_Empresa = "E" Then
              Dim SS_BR As Object
              Dim SS_BR1 As Object
              Dim vTp_Cli As String
              
110           If strIS <> "" And uf_origem <> uf_destino Then
111               uf_destino = "SU"
112           Else
113               uf_destino = uf_destino
114           End If

              'oradatabase.ExecuteSQL "BEGIN PRODUCAO.Pck_VDA230.Pr_BASE_RED(:vCursor, :Class, :UF_Origem, :UF_Destino); END;"
115           SQL = "SELECT COUNT(*) as TOTAL From CLASS_FISCAL_RED_BASE "
116           SQL = SQL & " WHERE CLASS_FISCAL = " & w_class_fiscal & " AND "
117           SQL = SQL & " COD_UF_ORIGEM = '" & UCase(uf_origem) & "' AND "
118           SQL = SQL & " COD_UF_DESTINO = '" & UCase(uf_destino) & "'"

119           Set SS_BR = dbAccess2.CreateSnapshot(SQL)

120           If (Not SS_BR.EOF And Not SS_BR.BOF) And SS_BR!total > 0 Then
121               strBase_Red = "S"

122               If strIS <> "" And uf_origem <> uf_destino Then
123                   uf_destino = "SU"
124               Else
125                   uf_destino = uf_destino
126               End If
                  
127               If tipo_cliente = "REVENDEDOR" Then
128                   vTp_Cli = "R"
129               ElseIf tipo_cliente = "CONS. FINAL" Then
130                   vTp_Cli = "C"
131               ElseIf tipo_cliente = "ISENTO" Then
132                   vTp_Cli = "I"
133               End If

                  'oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA230.Pr_Desconto_Adicional(:vCursor, :Trib, :Tp_cli, :UF_Destino, :UF_Origem); END;"
134               SQL = "SELECT pc_desc, pc_desc_diesel From uf_tpcliente "
135               SQL = SQL & " WHERE cod_tributacao = 8 AND tp_cliente = '" & vTp_Cli & "' AND"
136               SQL = SQL & " cod_uf_origem = '" & uf_origem & "' AND cod_uf_destino = '" & uf_destino & "'"

137               Set SS_BR1 = dbAccess2.CreateSnapshot(SQL)
                  
138               If SS_BR1.EOF And SS_BR1.BOF Then
139                   dblPC_ICM = 0
140               Else
141                   If bTp_Desconto = 2 Then
142                       dblPC_ICM = SS_BR1!pc_desc_diesel
143                   Else
144                       dblPC_ICM = SS_BR1!pc_desc
145                   End If
146                   strDifIcm = 100 - (100 * _
                              (1 - CDbl(strDifIcm) / 100) * _
                              (1 - CDbl(dblPC_ICM) / 100))

147                   lblPercDifIcm.Caption = Format(strDifIcm, "00.00")
148               End If

149           Else
150               strBase_Red = "N"
151           End If

152       End If
          '*********************************************
          'Fim - Eduardo - Base Reduzida - 05/07/2005
          '*********************************************

153       If tipo_cliente <> "" And txtCOD_DPK.Text <> "" And chkDifIcm.Value Then
154           If tipo_cliente = "REVENDEDOR" Then
155               TIPO_CLI = "R"
156           ElseIf tipo_cliente = "CONS. FINAL" Then
157               TIPO_CLI = "C"
158           ElseIf tipo_cliente = "ISENTO" Then
159               TIPO_CLI = "I"
160           End If
161           w_tp_cliente = TIPO_CLI

162           SQL = " SELECT PC_DESC,PC_DESC_DIESEL "
163           SQL = SQL & " FROM UF_TPCLIENTE "
164           SQL = SQL & " WHERE COD_TRIBUTACAO = " & bCod_Trib & " AND "
165           SQL = SQL & " TP_CLIENTE= '" & TIPO_CLI & "' AND"
166           SQL = SQL & " COD_UF_ORIGEM ='" & uf_origem & "' AND"
167           If strIS <> "" And uf_origem <> uf_destino Then
168               SQL = SQL & " COD_UF_DESTINO ='" & "SU" & "'"
169           Else
170               SQL = SQL & " COD_UF_DESTINO ='" & uf_destino & "' "
171           End If

172           Set ss2 = dbAccess2.CreateSnapshot(SQL)

173           If ss2.EOF And ss2.BOF Then
174               dblPC_ICM = 0
175           Else
176               If StrTipo_Desconto = 1 Then
177                   dblPC_ICM = ss2!pc_desc
178               ElseIf StrTipo_Desconto = 2 Then
179                   dblPC_ICM = ss2!pc_desc_diesel
180               Else
181                   dblPC_ICM = 0
182               End If
183               strDifIcm = 100 - (100 * _
                          (1 - CDbl(strDifIcm) / 100) * _
                          (1 - CDbl(dblPC_ICM) / 100))
184           End If
185           lblPercDifIcm.Caption = Format(strDifIcm, "00.00")
186       End If
187       uf_destino = cboUf
188       If CalcPreco Then
189           Call CalculaPrecos(NaoBuscaDescTab)
190       End If

191       Exit Sub

TrataErro:
192       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
193           Resume
194       Else
195           MsgBox "Sub GetDifICM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
196       End If
End Sub

Private Sub GetFilial()

          Dim ss As Snapshot

1         On Error GoTo TrataErro

2         If frmVenda.cboFilial.ListCount = 0 Then
3             SQL = "select cod_filial, sigla "
4             SQL = SQL & " from filial "
5             SQL = SQL & " order by cod_filial"

6             Set ss = dbAccess2.CreateSnapshot(SQL)

7             Do
8                 frmVenda.cboFilial.AddItem Format(ss!Cod_Filial, "0000") & "-" & ss!Sigla
9                 ss.MoveNext
10            Loop Until ss.EOF

11        End If
12        Exit Sub

TrataErro:
13        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
14            Resume
15        Else
16            MsgBox "Sub GetFilial" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
17        End If

End Sub

Private Sub GetFornecedor(iCOD_FORNECEDOR As Integer)
1         On Error GoTo TrataErro

          Dim ss As Snapshot

          'criar consulta
2         SQL = "select SIGLA, SITUACAO "
3         SQL = SQL & " from FORNECEDOR "
4         SQL = SQL & " where SITUACAO = 0 and "
5         SQL = SQL & " COD_FORNECEDOR = " & iCOD_FORNECEDOR


6         Set ss = dbAccess2.CreateSnapshot(SQL)

7         If ss.EOF() And ss.BOF Then
8             MsgBox "Fornecedor n�o cadastrado", vbInformation, "Aten��o"
9             frmVenda.txtCOD_FORNECEDOR.SetFocus
              'Inicializar frame
10            ss.Close
11            Call InitFrmeProduto
12        ElseIf ss!situacao <> 0 Then
13            MsgBox "Fornecedor Desativado", vbInformation, "Aten��o"
14            Call InitFrmeProduto
15        Else
16            lblSIGLA.Caption = ss!Sigla.Value
17            Call ReInitProduto
18            If qtd = 0 Then
19                frmVenda.txtCOD_FABRICA.Enabled = True
20            End If
21        End If

22        Exit Sub

TrataErro:
23        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
24            Resume
25        Else
26            MsgBox "Sub GetFornecedor" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If

End Sub

Private Sub GetItem(lngCod_dpk As Long, iCOD_FORNECEDOR As Integer, strCod_Fabrica As String)
1         On Error GoTo TrataErro
          Dim ss2 As Object
          Dim ss4 As Object
          Dim bcateg As Byte
          Dim lngTotal_Grid As Integer
          Dim bLinha_Selecionada As Integer

          Static FechaConsultaItem As Boolean

          '----------------------------
          'Alterado por: Eduardo Relvas
          'Alterado em : 19/01/2005
          '----------------------------
          'inicializa frame de precos
2         Call InitFrmePrecoVenda

          'WILLIAM LEITE-SDS2687 - MINVDA = 1
3         Call VerificaLojaACC(Mid(cboDeposito.Text, 1, 2))

4         If lngCod_dpk > 0 Then
5             SQL = " select e.COD_DPK as cod_dpk,"
6             SQL = SQL & " e.COD_FORNECEDOR as cod_fornecedor,"
7             SQL = SQL & " f.SIGLA as sigla,"
8             SQL = SQL & " e.COD_FABRICA as cod_fabrica,"
9             SQL = SQL & " e.DESC_ITEM as desc_item,"
10            SQL = SQL & " b.QTD_ATUAL as qtd_atual,"
11            SQL = SQL & " b.QTD_RESERV as qtd_reserv,"
12            SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
13            SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
14            SQL = SQL & " a.SINAL as sinal,"
'14            SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
15            If retornoACC = True Then
16               SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
17            Else
18               SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
19            End If
20            SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
21            SQL = SQL & " e.PESO as peso,"
22            SQL = SQL & " e.cod_grupo as cod_grupo,"
23            SQL = SQL & " e.cod_subgrupo as cod_subgrupo,"
24            SQL = SQL & " e.COD_UNIDADE as cod_unidade,"
25            SQL = SQL & " e.COD_TRIBUTACAO as cod_tributacao,"
26            SQL = SQL & " e.COD_TRIBUTACAO_IPI as cod_tributacao_ipi,"
27            SQL = SQL & " e.PC_IPI as pc_ipi,"
28            SQL = SQL & " d.PRECO_VENDA as PRECO_VENDA,"
29            SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
30            SQL = SQL & " d.PRECO_OF as PRECO_OF,"
31            SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
32            SQL = SQL & " d.PRECO_SP as PRECO_SP,"
33            SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
34            SQL = SQL & " b.SITUACAO as situacao,"
35            SQL = SQL & " e.COD_DPK_ANT as COD_DPK_ANT,"
36            SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
37            SQL = SQL & " f.DIVISAO as divisao,"
38            SQL = SQL & " c.categoria as categoria,"
39            SQL = SQL & " e.cod_procedencia as cod_procedencia"
40            SQL = SQL & " from categ_sinal    a,"
41            SQL = SQL & " item_estoque   b,"
42            SQL = SQL & " item_analitico c,"
43            SQL = SQL & " item_preco     d,"
44            SQL = SQL & " item_cadastro  e,"
45            SQL = SQL & " fornecedor f"
46            SQL = SQL & " where c.categoria = a.categoria and b.cod_loja = c.cod_loja and"
47            SQL = SQL & " b.cod_dpk = c.cod_dpk and c.cod_loja = d.cod_loja and"
48            SQL = SQL & " c.cod_dpk = d.cod_dpk and d.cod_loja = " & Mid(cboDeposito.Text, 1, 2) & " and"
49            SQL = SQL & " d.cod_dpk = e.cod_dpk and"
50            SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
51            SQL = SQL & " e.COD_DPK = " & CStr(lngCod_dpk)

52        ElseIf iCOD_FORNECEDOR <> "-1" And strCod_Fabrica <> "-1" Then
53            SQL = " select e.COD_DPK, e.COD_FORNECEDOR, f.SIGLA as sigla,"
54            SQL = SQL & " e.COD_FABRICA as cod_fabrica, e.DESC_ITEM as desc_item,"
55            SQL = SQL & " b.QTD_ATUAL as qtd_atual, b.QTD_RESERV as qtd_reserv,"
56            SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
57            SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
'53            SQL = SQL & " a.SINAL as sinal, e.QTD_MINVDA as qtd_minvda,"
58            SQL = SQL & " a.SINAL as sinal, "
'14            SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
59            If retornoACC = True Then
60               SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
61            Else
62               SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
63            End If
64            SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
65            SQL = SQL & " e.PESO as peso, e.cod_grupo as cod_grupo,"
66            SQL = SQL & " e.cod_subgrupo as cod_subgrupo, e.COD_UNIDADE as cod_unidade,"
67            SQL = SQL & " e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI,"
68            SQL = SQL & " e.PC_IPI, d.PRECO_VENDA as PRECO_VENDA,"
69            SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
70            SQL = SQL & " d.PRECO_OF as PRECO_OF,"
71            SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
72            SQL = SQL & " d.PRECO_SP as PRECO_SP,"
73            SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
74            SQL = SQL & " b.SITUACAO, e.COD_DPK_ANT as COD_DPK_ANT,"
75            SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
76            SQL = SQL & " f.DIVISAO as divisao, c.categoria as categoria,"
77            SQL = SQL & " e.cod_procedencia as cod_procedencia"
78            SQL = SQL & " from categ_sinal a,"
79            SQL = SQL & " item_estoque b,"
80            SQL = SQL & " item_analitico c,"
81            SQL = SQL & " item_preco d,"
82            SQL = SQL & " item_cadastro e,"
83            SQL = SQL & " fornecedor f"
84            SQL = SQL & " where c.categoria = a.categoria and"
85            SQL = SQL & " b.cod_loja = c.cod_loja and"
86            SQL = SQL & " b.cod_dpk = c.cod_dpk and"
87            SQL = SQL & " c.cod_loja = d.cod_loja and"
88            SQL = SQL & " c.cod_dpk = d.cod_dpk and"
89            SQL = SQL & " d.cod_loja =" & Mid(cboDeposito.Text, 1, 2) & " and"
90            SQL = SQL & " d.cod_dpk = e.cod_dpk and"
91            SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
              'Alterado em 27/06/2006
              'O codigo do fornecedor vem Zerado quando d� o duplo clique sobre o item da lista
92            If iCOD_FORNECEDOR <> 0 Then SQL = SQL & " e.COD_FORNECEDOR = " & CStr(iCOD_FORNECEDOR) & " and"
93            If InStr(1, strCod_Fabrica, "*") > 0 Then
94                SQL = SQL & " e.COD_FABRICA like '" & strCod_Fabrica & "'"
95            Else
96                SQL = SQL & " e.COD_FABRICA = '" & strCod_Fabrica & "'"
97            End If

98            SQL = SQL & " Union"
99            SQL = SQL & " select e.COD_DPK, e.COD_FORNECEDOR, f.SIGLA as sigla,"
100           SQL = SQL & " e.COD_FABRICA as cod_fabrica, e.DESC_ITEM as desc_item,"
101           SQL = SQL & " b.QTD_ATUAL as qtd_atual, b.QTD_RESERV as qtd_reserv,"
102           SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
103           SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
'94            SQL = SQL & " a.SINAL as sinal, e.QTD_MINVDA as qtd_minvda,"
104           SQL = SQL & " a.SINAL as sinal, "
'14            SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
105           If retornoACC = True Then
106              SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
107           Else
108              SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
109           End If
110           SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
111           SQL = SQL & " e.PESO as peso, e.cod_grupo as cod_grupo,"
112           SQL = SQL & " e.cod_subgrupo as cod_subgrupo, e.COD_UNIDADE as cod_unidade,"
113           SQL = SQL & " e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI,"
114           SQL = SQL & " e.PC_IPI, d.PRECO_VENDA as PRECO_VENDA,"
115           SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
116           SQL = SQL & " d.PRECO_OF as PRECO_OF,"
117           SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
118           SQL = SQL & " d.PRECO_SP as PRECO_SP,"
119           SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
120           SQL = SQL & " b.SITUACAO, e.COD_DPK_ANT as COD_DPK_ANT,"
121           SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
122           SQL = SQL & " f.DIVISAO as divisao , c.categoria as categoria,"
123           SQL = SQL & " e.cod_procedencia as cod_procedencia"
124           SQL = SQL & " from categ_sinal a,"
125           SQL = SQL & " item_estoque b,"
126           SQL = SQL & " item_analitico c,"
127           SQL = SQL & " item_preco d,"
128           SQL = SQL & " item_cadastro e,"
129           SQL = SQL & " fornecedor f,"
130           SQL = SQL & " R_FABRICA_DPK G"
131           SQL = SQL & " where c.categoria = a.categoria and"
132           SQL = SQL & " b.cod_loja = c.cod_loja and"
133           SQL = SQL & " b.cod_dpk = c.cod_dpk and"
134           SQL = SQL & " c.cod_loja = d.cod_loja and"
135           SQL = SQL & " c.cod_dpk = d.cod_dpk and"
136           SQL = SQL & " d.cod_loja =" & Mid(cboDeposito.Text, 1, 2) & " and"
137           SQL = SQL & " E.COD_DPK = G.COD_DPK AND"
138           SQL = SQL & " d.cod_dpk = e.cod_dpk and"
139           SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
              'Alterado em 27/06/2006
              'O codigo do fornecedor vem Zerado quando d� o duplo clique sobre o item da lista
140           If iCOD_FORNECEDOR <> 0 Then SQL = SQL & " e.COD_FORNECEDOR = " & CStr(iCOD_FORNECEDOR) & " and"
141           If InStr(1, strCod_Fabrica, "*") > 0 Then
142               SQL = SQL & " G.COD_FABRICA like '" & strCod_Fabrica & "'"
143           Else
144               SQL = SQL & " G.COD_FABRICA = '" & strCod_Fabrica & "'"
145           End If
              'Incluido 23/01/2008 - Altera��o da tabela R_Fabrica_Dpk
              'Codigos de identifica��o do item(Fabrica e/ou Fornecedor)
146           SQL = SQL & " Union"
147           SQL = SQL & " select e.COD_DPK, e.COD_FORNECEDOR, f.SIGLA as sigla,"
148           SQL = SQL & " e.COD_FABRICA as cod_fabrica, e.DESC_ITEM as desc_item,"
149           SQL = SQL & " b.QTD_ATUAL as qtd_atual, b.QTD_RESERV as qtd_reserv,"
150           SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
151           SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
'137           SQL = SQL & " a.SINAL as sinal, e.QTD_MINVDA as qtd_minvda,"
152           SQL = SQL & " a.SINAL as sinal, "
'14            SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
153           If retornoACC = True Then
154              SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
155           Else
156              SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
157           End If
158           SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
159           SQL = SQL & " e.PESO as peso, e.cod_grupo as cod_grupo,"
160           SQL = SQL & " e.cod_subgrupo as cod_subgrupo, e.COD_UNIDADE as cod_unidade,"
161           SQL = SQL & " e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI,"
162           SQL = SQL & " e.PC_IPI, d.PRECO_VENDA as PRECO_VENDA,"
163           SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
164           SQL = SQL & " d.PRECO_OF as PRECO_OF,"
165           SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
166           SQL = SQL & " d.PRECO_SP as PRECO_SP,"
167           SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
168           SQL = SQL & " b.SITUACAO, e.COD_DPK_ANT as COD_DPK_ANT,"
169           SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
170           SQL = SQL & " f.DIVISAO as divisao , c.categoria as categoria,"
171           SQL = SQL & " e.cod_procedencia as cod_procedencia"
172           SQL = SQL & " from categ_sinal a,"
173           SQL = SQL & " item_estoque b,"
174           SQL = SQL & " item_analitico c,"
175           SQL = SQL & " item_preco d,"
176           SQL = SQL & " item_cadastro e,"
177           SQL = SQL & " fornecedor f,"
178           SQL = SQL & " R_FABRICA_DPK G"
179           SQL = SQL & " where c.categoria = a.categoria and"
180           SQL = SQL & " b.cod_loja = c.cod_loja and"
181           SQL = SQL & " b.cod_dpk = c.cod_dpk and"
182           SQL = SQL & " c.cod_loja = d.cod_loja and"
183           SQL = SQL & " c.cod_dpk = d.cod_dpk and"
184           SQL = SQL & " d.cod_loja =" & Mid(cboDeposito.Text, 1, 2) & " and"
185           SQL = SQL & " E.COD_DPK = G.COD_DPK AND"
186           SQL = SQL & " d.cod_dpk = e.cod_dpk and"
187           SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
188           If iCOD_FORNECEDOR <> 0 Then SQL = SQL & " g.COD_FORNECEDOR = " & CStr(iCOD_FORNECEDOR) & " and"
189           If InStr(1, strCod_Fabrica, "*") > 0 Then
190               SQL = SQL & " G.COD_FABRICA like '" & strCod_Fabrica & "'"
191           Else
192               SQL = SQL & " G.COD_FABRICA = '" & strCod_Fabrica & "'"
193           End If

194       ElseIf iCOD_FORNECEDOR = "-1" And strCod_Fabrica <> "-1" Then
195           SQL = " select e.COD_DPK,"
196           SQL = SQL & " e.COD_FORNECEDOR,"
197           SQL = SQL & " f.SIGLA as sigla,"
198           SQL = SQL & " e.COD_FABRICA as cod_fabrica,"
199           SQL = SQL & " e.DESC_ITEM as desc_item,"
200           SQL = SQL & " b.QTD_ATUAL as qtd_atual,"
201           SQL = SQL & " b.QTD_RESERV as qtd_reserv,"
202           SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
203           SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
204           SQL = SQL & " a.SINAL as sinal,"
'14            SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
205           If retornoACC = True Then
206              SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
207           Else
208              SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
209           End If
210           SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
211           SQL = SQL & " e.PESO as peso,"
212           SQL = SQL & " e.cod_grupo as cod_grupo,"
213           SQL = SQL & " e.cod_subgrupo as cod_subgrupo,"
214           SQL = SQL & " e.COD_UNIDADE as cod_unidade,"
215           SQL = SQL & " e.COD_TRIBUTACAO,"
216           SQL = SQL & " e.COD_TRIBUTACAO_IPI,"
217           SQL = SQL & " e.PC_IPI,"
218           SQL = SQL & " d.PRECO_VENDA as PRECO_VENDA,"
219           SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
220           SQL = SQL & " d.PRECO_OF as PRECO_OF,"
221           SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
222           SQL = SQL & " d.PRECO_SP as PRECO_SP,"
223           SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
224           SQL = SQL & " b.SITUACAO,"
225           SQL = SQL & " e.COD_DPK_ANT as COD_DPK_ANT,"
226           SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
227           SQL = SQL & " f.DIVISAO as divisao,"
228           SQL = SQL & " c.categoria as categoria,"
229           SQL = SQL & " e.cod_procedencia as cod_procedencia"
230           SQL = SQL & " from categ_sinal    a,"
231           SQL = SQL & " item_estoque   b,"
232           SQL = SQL & " item_analitico c,"
233           SQL = SQL & " item_preco     d,"
234           SQL = SQL & " item_cadastro  e,"
235           SQL = SQL & " fornecedor f"
236           SQL = SQL & " where c.categoria = a.categoria and b.cod_loja = c.cod_loja and"
237           SQL = SQL & " b.cod_dpk = c.cod_dpk and c.cod_loja = d.cod_loja and"
238           SQL = SQL & " c.cod_dpk = d.cod_dpk and d.cod_loja =  " & Mid(cboDeposito.Text, 1, 2) & " and"
239           SQL = SQL & " d.cod_dpk = e.cod_dpk and"
240           SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
241           SQL = SQL & " e.COD_FABRICA like '" & strCod_Fabrica & "'"
              
242           SQL = SQL & " Union"
243           SQL = SQL & " select e.COD_DPK,"
244           SQL = SQL & " e.COD_FORNECEDOR,"
245           SQL = SQL & " f.SIGLA as sigla,"
246           SQL = SQL & " e.COD_FABRICA as cod_fabrica,"
247           SQL = SQL & " e.DESC_ITEM as desc_item,"
248           SQL = SQL & " b.QTD_ATUAL as qtd_atual,"
249           SQL = SQL & " b.QTD_RESERV as qtd_reserv,"
250           SQL = SQL & " b.QTD_PENDENTE as qtd_pendente,"
251           SQL = SQL & " (b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE) as ESTOQUE,"
252           SQL = SQL & " a.SINAL as sinal,"
'229           SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
253           If retornoACC = True Then
254              SQL = SQL & " " & minVDA_ACC & " as qtd_minvda,"
255           Else
256              SQL = SQL & " e.QTD_MINVDA as qtd_minvda,"
257           End If
258           SQL = SQL & " b.QTD_MAXVDA as qtd_maxvda,"
259           SQL = SQL & " e.PESO as peso,"
260           SQL = SQL & " e.cod_grupo as cod_grupo,"
261           SQL = SQL & " e.cod_subgrupo as cod_subgrupo,"
262           SQL = SQL & " e.COD_UNIDADE as cod_unidade,"
263           SQL = SQL & " e.COD_TRIBUTACAO,"
264           SQL = SQL & " e.COD_TRIBUTACAO_IPI,"
265           SQL = SQL & " e.PC_IPI,"
266           SQL = SQL & " d.PRECO_VENDA as PRECO_VENDA,"
267           SQL = SQL & " d.PRECO_VENDA_ANT as PRECO_VENDA_ANT,"
268           SQL = SQL & " d.PRECO_OF as PRECO_OF,"
269           SQL = SQL & " d.PRECO_OF_ANT as PRECO_OF_ANT,"
270           SQL = SQL & " d.PRECO_SP as PRECO_SP,"
271           SQL = SQL & " d.PRECO_SP_ANT as PRECO_SP_ANT,"
272           SQL = SQL & " b.SITUACAO,"
273           SQL = SQL & " e.COD_DPK_ANT as COD_DPK_ANT,"
274           SQL = SQL & " e.CLASS_FISCAL as CLASS_FISCAL,"
275           SQL = SQL & " f.DIVISAO as divisao,"
276           SQL = SQL & " c.categoria as categoria,"
277           SQL = SQL & " e.cod_procedencia as cod_procedencia"
278           SQL = SQL & " from categ_sinal    a,"
279           SQL = SQL & " item_estoque   b,"
280           SQL = SQL & " item_analitico c,"
281           SQL = SQL & " item_preco     d,"
282           SQL = SQL & " item_cadastro  e,"
283           SQL = SQL & " fornecedor     f,"
284           SQL = SQL & " R_FABRICA_DPK G"
285           SQL = SQL & " where c.categoria = a.categoria and b.cod_loja = c.cod_loja and"
286           SQL = SQL & " b.cod_dpk = c.cod_dpk and c.cod_loja = d.cod_loja and"
287           SQL = SQL & " c.cod_dpk = d.cod_dpk and d.cod_loja = " & Mid(cboDeposito.Text, 1, 2) & " and"
288           SQL = SQL & " d.cod_dpk = e.cod_dpk and E.COD_DPK = G.COD_DPK AND"
289           SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor and"
290           SQL = SQL & " G.COD_FABRICA like '" & strCod_Fabrica & "'"
291       Else
292           Call InitFrmeProduto
293           Exit Sub
294       End If
295           If FechaConsultaItem Then FechaConsultaItem = True
296           Set ssItem = dbAccess2.CreateSnapshot(SQL)
297           FreeLocks
298           If ssItem.EOF() And ssItem.BOF Then
299               MsgBox "Item n�o Cadastrado", vbInformation, "Aten��o"
300               ssItem.Close
301               Call ReInitProduto
302               frmVenda.txtCOD_FABRICA.Enabled = True
303           ElseIf ssItem!divisao = "B" Then
304               MsgBox "Este item pertence a divis�o BLAU", vbInformation, "Aten��o"
305               Call InitFrmeProduto
306           ElseIf ssItem!situacao = 8 Then
307               MsgBox "Item Substituido", vbInformation, "Aten��o"
308               Call GetItem(ssItem!COD_DPK_ANT, -1, -1)
309           ElseIf ssItem!situacao = 9 Then
310              MsgBox "Item Desativado", vbInformation, "Aten��o"
311              Call ReInitProduto
312              frmVenda.txtCOD_FABRICA.Enabled = True
313           Else
                  'carregar dados
314               txtCOD_DPK.Text = ssItem!cod_dpk
315               lblDigito.Caption = iRetornaDigito(ssItem!cod_dpk)
316               class_fiscal = ssItem!class_fiscal
317               bCod_Trib = ssItem!cod_tributacao
318               w_cod_procedencia = ssItem!cod_procedencia
319               w_class_fiscal = ssItem!class_fiscal
                  'BUSCA NR. ORIGINAL
320               SQL = "SELECT DISTINCT COD_ORIGINAL FROM APLICACAO WHERE COD_DPK= " & txtCOD_DPK.Text & " ORDER BY COD_ORIGINAL DESC"
321               Set ss2 = dbAccess2.CreateSnapshot(SQL)
322               FreeLocks
                  
323               If ss2.EOF And ss2.BOF Then
324                   txtoriginal = ""
325               Else
326                   If IsNull(ss2!COD_ORIGINAL) Then
327                       txtoriginal = ""
328                   Else
329                       txtoriginal.Enabled = True
330                       txtoriginal = LTrim(ss2!COD_ORIGINAL)
331                   End If
332               End If

333               txtCOD_FORNECEDOR.Text = ssItem!Cod_Fornecedor
334               lblSIGLA.Caption = ssItem!Sigla
335               item_grupo = ssItem!cod_grupo
336               item_subgrupo = ssItem!cod_SUBgrupo
337               txtCOD_FABRICA.Text = ssItem!cod_fabrica
338               txtCOD_FABRICA.Enabled = True
339               txtDescricao.Text = ssItem!desc_item
340               txtDescricao.Enabled = True

                  'FORNECEDOR ESPEC�FICO
341               SQL = "Select fl_dif_icms, fl_adicional, tp_dif_icms"
342               SQL = SQL & " From fornecedor_especifico"
343               SQL = SQL & " where cod_dpk in (0," & txtCOD_DPK & ") and"
344               SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & ") and"
345               SQL = SQL & " cod_grupo in (0," & item_grupo & ") and"
346               SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR & ""
347               SQL = SQL & " ORDER BY COD_DPK DESC, COD_SUBGRUPO DESC, COD_GRUPO DESC, COD_FORNECEDOR"
348               Set ss3 = dbAccess2.CreateSnapshot(SQL)
349               FreeLocks

350               If ss3.EOF Or ss3.BOF Then
351                   strFl_Dif_Icms = "S"
352                   strFl_Adicional = "S"
353                   StrTipo_Desconto = 1
                      'Eduardo - 08/08/05
354                   bdif_ICMS = 1
355               Else
356                   If ss3!fl_dif_icms = "N" Then
357                       strFl_Dif_Icms = ss3!fl_dif_icms
358                       strFl_Adicional = ss3!fl_adicional
359                       StrTipo_Desconto = 0
                          'Eduardo - 08/08/05
360                       bdif_ICMS = IIf(IsNull(ss3!TP_DIF_ICMS), 1, ss3!TP_DIF_ICMS)
361                   Else
362                       strFl_Dif_Icms = ss3!fl_dif_icms
363                       strFl_Adicional = ss3!fl_adicional
364                       StrTipo_Desconto = ss3!TP_DIF_ICMS
                          'Eduardo - 08/0/05
365                       bdif_ICMS = IIf(IsNull(ss3!TP_DIF_ICMS), 1, ss3!TP_DIF_ICMS)
366                   End If
367               End If
                  
368               If ssItem!qtd_atual < 0 Or ssItem!QTD_RESERV < 0 Or ssItem!QTD_PENDENTE < 0 Then
369                   MsgBox "Item com problema, ligue para o Depto. Auditoria. " & _
                          " Qtde.Atual = " & ssItem!qtd_atual & _
                          " Qtde.Reserv = " & ssItem!QTD_RESERV & _
                          " Qtde.Pendente = " & ssItem!QTD_PENDENTE, vbExclamation, "Aten��o"
370                   Call InitFrmeProduto
371                   Exit Sub
372               Else
373                   lblESTOQUE.Caption = ssItem!ESTOQUE
374                   lblCATEGORIA.Caption = IIf(IsNull(ssItem!sinal), " ", ssItem!sinal)
375                   lblQTD_MINVDA.Caption = ssItem!QTD_MINVDA
376                   lblQTD_MAXVDA.Caption = ssItem!QTD_MAXVDA
377                   lblPESO.Caption = ssItem!PESO
378                   lblCOD_UNIDADE.Caption = ssItem!COD_UNIDADE
379                   lblPENDENTE.Caption = ssItem!QTD_PENDENTE
                      
                      'alimenta grid com informa��es de estoque
380                   SQL = "select b.cod_loja as cod,c.nome_fantasia as Deposito, " & _
                      "       b.qtd_pendente as qtd_pendente,(b.qtd_atual - b.qtd_reserv - b.qtd_pendente) as qtd_disponivel " & _
                      "from deposito_visao a, item_estoque b, loja c " & _
                      "where  b.cod_dpk = " & txtCOD_DPK & " and a.nome_programa='FIL030' AND " & _
                      "       a.cod_loja=b.cod_loja AND a.cod_loja=c.cod_loja " & _
                      "Order by b.cod_loja "
381                   Set ss5 = dbAccess2.CreateSnapshot(SQL)
382                   FreeLocks
383                   If ss5.EOF Then
384                       frmVenda.grdEstoque.Visible = False
385                       lngTotal_Grid = 0
386                   Else
                          'carrega dados
387                       lngTotal_Grid = 0
388                       lngTotal_Grid = ss5.RecordCount
389                       DoEvents
390                       grdEstoque.Visible = False
391                       DoEvents
392                       With frmVenda.grdEstoque
393                           .Cols = 3
394                           .Rows = ss5.RecordCount + 1
395                           .ColWidth(0) = 1500
396                           .ColWidth(1) = 900
397                           .ColWidth(2) = 900
398                           .Row = 0
399                           .Col = 0
400                           .Text = "Dep"
401                           .Col = 1
402                           .Text = "Qtd Disp."
403                           .Col = 2
404                           .Text = "Qtd Pend."
                              'grdEstoque.TopRow = 1
405                           .HighLight = False
406                           ss5.MoveFirst
407                           For i = 1 To .Rows - 1
408                               .Row = i
409                               .Col = 0
410                               If Format(ss5!COD, "00") = Mid(Trim(cboDeposito), 1, 2) Then
411                                   .HighLight = True
412                                   .SelStartRow = i
413                                   .SelEndRow = i
414                                   .SelStartCol = 1
415                                   .SelEndCol = 2
416                                   .Text = ss5!Deposito
417                                   bLinha_Selecionada = i
418                               Else
419                                   .Text = ss5!Deposito
420                                   DoEvents
421                               End If
422                               .Col = 1
423                               .ColAlignment(1) = 2
424                               .Text = ss5!qtd_disponivel
425                               .Col = 2
426                               .ColAlignment(2) = 2
427                               .Text = ss5!QTD_PENDENTE
428                               ss5.MoveNext
429                           Next
430                           .Row = 1
431                           frmVenda.grdEstoque.Visible = True
432                           DoEvents
433                       End With
434                   End If
435                   DoEvents
                      '==========
436                   If ssItem!cod_tributacao = 1 Then
437                       lblMsgTributacao.Visible = True
438                       lblmsgaliq.Visible = False
439                   ElseIf ssItem!cod_tributacao = 8 Then
440                       lblmsgaliq.Visible = True
441                       lblMsgTributacao.Visible = False
442                   Else
443                       lblMsgTributacao.Visible = False
444                       lblmsgaliq.Visible = False
445                   End If
                  
446                   If ssItem!COD_TRIBUTACAO_IPI = 1 Then
447                       lblIPI = Format(ssItem!PC_IPI, "00.00")
448                       lblMsg_IPI.Visible = True
449                   Else
450                       lblIPI = Format(0, "00.00")
451                       lblMsg_IPI.Visible = False
452                   End If
                  
453                   cmdAplicacao.Enabled = True
454                   SSCommand1.Enabled = True
455                   bcateg = ssItem!categoria
                  
                      'VERIFICA DESC.ADICIONAL AUTOMATICO
456                   If strDesc_auto = "S" And strFl_Adicional = "S" Then
457                       SQL = "Select pc_desc_adic"
458                       SQL = SQL & " from uf_categ"
459                       SQL = SQL & " Where cod_uf in ('UF', '" & uf_destino & "')"
460                       SQL = SQL & "  and categoria = " & bcateg & " "
461                       SQL = SQL & "  and caracteristica = '" & strCaract & "' "
462                       SQL = SQL & " Order by cod_uf asc"
463                       Set ss4 = dbAccess2.CreateSnapshot(SQL)
                      
464                       If Not ss4.EOF And Not ss4.BOF Then
465                           txtDescAdicional = ss4!pc_desc_adic
466                       Else
467                           txtDescAdicional = 0
468                       End If
469                   End If
                  
                      'calculo de icm
470                   Call GetDifIcm(NaoCalcItem, bdif_ICMS)
                  
                      'calcula pre�os
471                   Call CalculaPrecos(BuscaDescTab)
472               End If
473           End If
474       Exit Sub
TrataErro:
475       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
476           Resume
477       Else
478           MsgBox "Sub GetItem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
479       End If
End Sub

Private Sub GetListaUf()

          Dim ss As Snapshot

1         On Error GoTo TrataErro

2         If frmVenda.cboUf.ListCount = 0 Then
3             SQL = "select COD_UF from UF order by COD_UF"

4             Set ss = dbAccess2.CreateSnapshot(SQL)
5             FreeLocks

6             If ss.RecordCount = 0 Then
7                 MsgBox "Problemas com a tabela UF. Entre em, contato com o Depto de Sistemas", vbExclamation, "Aten��o"
8                 dbAccess.Close
9                 dbAccess2.Close
10                dbAccess3.Close
11                End
12            End If
13            ss.MoveFirst
14            Do
15                frmVenda.cboUf.AddItem ss!cod_uf
16                ss.MoveNext
17            Loop Until ss.EOF
18            ss.Close
19        End If
20        Exit Sub

TrataErro:
21        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
22            Resume
23        Else
24            MsgBox "Sub GetListaUF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If


End Sub

Private Function GetPlanoPgto(iCOD_PLANO As Integer, sPC_ACRESCIMO As Single, sPC_DESCONTO As Single) As Boolean
1         On Error GoTo TrataErro

          Dim ss As Snapshot

          'inicializa retorno
2         GetPlanoPgto = False

3         SQL = "select DESC_PLANO,"
4         SQL = SQL & "PC_ACRES_FIN_DPK,"
5         SQL = SQL & "PC_DESC_FIN_DPK,"
6         SQL = SQL & "SITUACAO, FL_AVISTA "
7         SQL = SQL & "from PLANO_PGTO "
8         SQL = SQL & "where COD_PLANO = " & CStr(iCOD_PLANO)

9         Set ss = dbAccess2.CreateSnapshot(SQL)
10        FreeLocks

11        If ss.EOF() And ss.BOF Then
              'plano n�o cadastrado
12            MsgBox "C�digo de plano " & CStr(iCOD_PLANO) & " n�o cadastrado", vbExclamation, "Aten��o"
13            txtCOD_PLANO.Text = ""
14            ss.Close
15        ElseIf ss!situacao <> 0 Then
              'plano inv�lido
16            MsgBox "C�digo de plano " & CStr(iCOD_PLANO) & " desativado", vbInformation, "Aten��o"
17            txtCOD_PLANO.Text = ""
18        Else
              'carregar dados do plano
19            txtCOD_PLANO.Text = iCOD_PLANO
20            If Len(Trim$(ss!DESC_PLANO)) > 17 Then
21                lblDESC_PLANO.Caption = ss!DESC_PLANO
22            Else
23                If CStr(iCOD_PLANO) <> 69 Then
24                    lblDESC_PLANO.Caption = Trim$(ss!DESC_PLANO) & " dd"
25                Else
26                    lblDESC_PLANO.Caption = Trim$(ss!DESC_PLANO)
27                End If
28            End If
29            If sPC_ACRESCIMO = -1 And sPC_DESCONTO = -1 Then
30                If ss!PC_ACRES_FIN_DPK > 0 Then
31                    txtPC_ACRES_FIN_DPK.Text = ss!PC_ACRES_FIN_DPK
32                    txtPC_ACRES_FIN_DPK.DataChanged = False
33                    txtPC_ACRES_FIN_DPK.Visible = True
34                    lblPC_ACRES_FIN_DPK.Visible = True
35                    txtPC_DESC_FIN_DPK.Text = "0"
36                    txtPC_DESC_FIN_DPK.DataChanged = False
37                    txtPC_DESC_FIN_DPK.Visible = False
38                    lblPC_DESC_FIN_DPK.Visible = False
39                Else
40                    txtPC_DESC_FIN_DPK.Text = ss!PC_DESC_FIN_DPK
41                    txtPC_DESC_FIN_DPK.DataChanged = False
42                    txtPC_DESC_FIN_DPK.Visible = True
43                    lblPC_DESC_FIN_DPK.Visible = True
44                    txtPC_ACRES_FIN_DPK.Text = "0"
45                    txtPC_ACRES_FIN_DPK.DataChanged = False
46                    txtPC_ACRES_FIN_DPK.Visible = False
47                    lblPC_ACRES_FIN_DPK.Visible = False
48                End If
49            Else
50                If sPC_ACRESCIMO > 0 Then
51                    txtPC_ACRES_FIN_DPK.Text = sPC_ACRESCIMO
52                    txtPC_ACRES_FIN_DPK.DataChanged = False
53                    txtPC_ACRES_FIN_DPK.Visible = True
54                    lblPC_ACRES_FIN_DPK.Visible = True
55                    txtPC_DESC_FIN_DPK.Text = "0"
56                    txtPC_DESC_FIN_DPK.DataChanged = False
57                    txtPC_DESC_FIN_DPK.Visible = False
58                    lblPC_DESC_FIN_DPK.Visible = False
59                Else
60                    txtPC_DESC_FIN_DPK.Text = sPC_DESCONTO
61                    txtPC_DESC_FIN_DPK.DataChanged = False
62                    txtPC_DESC_FIN_DPK.Visible = True
63                    lblPC_DESC_FIN_DPK.Visible = True
64                    txtPC_ACRES_FIN_DPK.Text = "0"
65                    txtPC_ACRES_FIN_DPK.DataChanged = False
66                    txtPC_ACRES_FIN_DPK.Visible = False
67                    lblPC_ACRES_FIN_DPK.Visible = False
68                End If
69            End If

              'carrega limites do plano
70            sPC_DESCONTO_PLANO = ss!PC_DESC_FIN_DPK
71            sPC_ACRESCIMO_PLANO = ss!PC_ACRES_FIN_DPK

              'validar retorno da funcao
72            GetPlanoPgto = True
73        End If
74        ss.Close

75        Exit Function

TrataErro:
76        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
77            Resume
          ElseIf Err.Number = 3420 Then
              Resume Next
78        Else
79            MsgBox "Sub GetPlanoPagto" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
80        End If

End Function

Private Sub grdLinhas_DblClick()
          
    On Error GoTo Trata_Erro
          
          Dim lngForn As Long

1         txtCOD_DPK = ""
2         txtCOD_DPK_LostFocus
3         grdLinhas.Col = 0
4         lngForn = grdLinhas.Text

5         SQL = "Select c.cod_fornecedor, e.sigla, c.cod_fabrica, c.desc_item, b.cod_dpk " & _
                  "From pednota_venda a, itpednota_venda b, item_cadastro c, datas d, fornecedor e " & _
                  "Where c.cod_fornecedor = " & lngForn & " and " & _
                  "      a.cod_cliente = " & txtCOD_CLIENTE.Text & " and a.fl_ger_nfis = 'S' and " & _
                  "      a.dt_emissao_nota >= dateadd('M',-3,d.dt_faturamento) and " & _
                  "      a.dt_emissao_nota < dt_faturamento + 1 and a.tp_transacao=1 and " & _
                  "      a.situacao=0 and b.situacao=0 and c.cod_fornecedor=e.cod_fornecedor and " & _
                  "      b.cod_dpk=c.cod_dpk and a.seq_pedido=b.seq_pedido and " & _
                  "      a.num_pedido = b.num_pedido And a.cod_loja = b.cod_loja " & _
                  "Group by c.cod_fornecedor,e.sigla,c.cod_fabrica,c.desc_item,b.cod_dpk " & _
                  "Order by c.cod_fornecedor,e.sigla,c.cod_fabrica,c.desc_item,b.cod_dpk "

6         Set ss7 = dbAccess2.CreateSnapshot(SQL)
7         FreeLocks

          '-- Carrega dados dos itens mais comprados durante os �ltimos 30 dias do fornecedor escolhido
          '-- e do cliente escolhido.
8         With frmINF.grdINF2
9             .Cols = 5
10            .Rows = ss7.RecordCount + 1
11            .ColWidth(0) = 800
12            .ColWidth(1) = 1300
13            .ColWidth(2) = 1500
14            .ColWidth(3) = 2000
15            .ColWidth(4) = 800

16            .Row = 0
17            .Col = 0
18            .Text = " COD"
19            .Col = 1
20            .Text = " FORN."
21            .Col = 2
22            .Text = "FABRICA"
23            .Col = 3
24            .Text = "DESCRI��O"
25            .Col = 4
26            .Text = "DPK"

27            For i = 1 To .Rows - 1
28                .Row = i

29                .Col = 0
30                .Text = ss7!Cod_Fornecedor
31                .Col = 1
32                .Text = ss7!Sigla
33                .Col = 2
34                .Text = ss7!cod_fabrica
35                .Col = 3
36                .Text = ss7!desc_item
37                .Col = 4
38                .Text = ss7!cod_dpk

39                ss7.MoveNext
40            Next
41            .Row = 1
42        End With

43        frmINF.grdINF2.Visible = True
44        frmINF.Show 1
45        If frmVenda.txtCOD_DPK <> "" Then
46            txtCOD_DPK_LostFocus
47        End If
48        Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub grdLinhas_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub InitCliente()

    'Eduardo - 05/08/2005
    strTP_Empresa = ""

    txtCGC.Text = ""
    txtCOD_CLIENTE = ""
    txtMensagem = ""
    strIE = ""
    strIS = ""
    lblNOME_CLIENTE.Caption = ""
    lblNOME_CIDADE.Caption = ""
    lblTipoCliente.Caption = ""
    lblClass.Caption = ""
    cboFilial.Text = FILIAL_PED
    lblIS.Caption = ""
    lblIS.Visible = False
    lblDescSuframa = 0
    tipo_cliente = ""
    cboUf.AddItem uf_filial
    uf_destino = cboUf.Text
    tipo_cliente = "REVENDEDOR"
    cboUf.Enabled = True
    cboFilial.Enabled = True
    lblCaracteristica = ""
    strCaract = ""
    strDesc_auto = "N"
    fil = 0
    txtRepres = ""
    txtVend = ""
    txtCOD_CLIENTE.DataChanged = False
    lblLimite.Caption = "" 'TI-4405


    '-- INICIALIZA ALGUMAS VARI�VEIS DE C�LCULO DE ICMS RETIDO
    w_tp_cliente = "R"
    w_vl_baseicm1 = 0
    w_vl_baseicm2 = 0
    w_vl_basemaj_1 = 0
    w_vl_basemaj_3 = 0
    w_cliente_antec = "N"
    w_pc_mg_lucro_antec = 0
    w_pc_margem_lucro = 0
    w_class_antec = 0
    w_class_antec_RJ = 0
    w_inscr_suframa = 0
    w_vl_baseicm1_ret = 0
    w_vl_baseicm2_ret = 0
    w_vl_base_1 = 0
    w_vl_base_1_aux = 0
    w_vl_base_2 = 0
    w_vl_base_2_aux = 0
    w_vl_base_3 = 0
    w_vl_baseisen = 0
    w_vl_baseisen_aux = 0
    w_vl_baseisen_aux1 = 0
    w_vl_base_5 = 0
    w_vl_base_6 = 0
    w_vl_baseoutr = 0
    w_vl_icmretido = 0
    w_cod_procedencia = 0
    w_class_fiscal = 0
    grdLinhas.Visible = False
    '--
End Sub

Private Sub InitFrmePlano()
    txtCOD_PLANO.Text = ""
    lblDESC_PLANO.Caption = ""

    txtPC_ACRES_FIN_DPK.Text = ""
    txtPC_ACRES_FIN_DPK.DataChanged = False
    txtPC_ACRES_FIN_DPK.Visible = False
    lblPC_ACRES_FIN_DPK.Visible = False

    txtPC_DESC_FIN_DPK.Text = ""
    txtPC_DESC_FIN_DPK.DataChanged = False
    lblPC_DESC_FIN_DPK.Visible = True

    txtDescAdicional.Text = "0"

End Sub

Private Sub InitFrmePrecoVenda()
    Dim i As Byte

    'frame de preco normal
    frmeTabela.Caption = ""
    lblPRECO3_NORMAL.Caption = ""
    lblPRECO2_NORMAL.Caption = ""
    lblPRECO1_NORMAL.Caption = ""
    lblPC_DESC_PERIODO3_NORMAL.Caption = ""
    lblPC_DESC_PERIODO2_NORMAL.Caption = ""
    lblPC_DESC_PERIODO1_NORMAL.Caption = ""
    lbl_TipoA_Normal.Visible = False
    lbl_TipoB_Normal.Visible = False
    lbl_TipoC_Normal.Visible = False
    lblPRECO3_NORMAL.Visible = False
    lblPRECO2_NORMAL.Visible = False
    lblPRECO1_NORMAL.Visible = False

    'frame de preco de oferta
    frmeOFERTA.Caption = ""
    lblPRECO3_OFERTA.Caption = ""
    lblPRECO2_OFERTA.Caption = ""
    lblPRECO1_OFERTA.Caption = ""
    lblPC_DESC_PERIODO3_OFERTA.Caption = ""
    lblPC_DESC_PERIODO2_OFERTA.Caption = ""
    lblPC_DESC_PERIODO1_OFERTA.Caption = ""
    lbl_TipoA_Oferta.Visible = False
    lbl_TipoB_Oferta.Visible = False
    lbl_TipoC_Oferta.Visible = False
    lblPRECO3_OFERTA.Visible = False
    lblPRECO2_OFERTA.Visible = False
    lblPRECO1_OFERTA.Visible = False

    'frame de preco de super promocao
    frmeSP.Caption = ""
    lblPRECO3_SP.Caption = ""
    lblPRECO2_SP.Caption = ""
    lblPRECO1_SP.Caption = ""
    lblPC_DESC_PERIODO3_SP.Caption = ""
    lblPC_DESC_PERIODO2_SP.Caption = ""
    lblPC_DESC_PERIODO1_SP.Caption = ""
    lbl_TipoA_SP.Visible = False
    lbl_TipoB_SP.Visible = False
    lbl_TipoC_SP.Visible = False
    lblPRECO3_SP.Visible = False
    lblPRECO2_SP.Visible = False
    lblPRECO1_SP.Visible = False

    'sinal de percentual
    For i = 0 To 8
        lblSinal(i).Visible = False
    Next i

End Sub

Private Sub InitFrmeProduto()
    'inicializa fornecedor
    txtCOD_FORNECEDOR.Text = ""
    lblSIGLA.Caption = ""
    Call ReInitProduto
End Sub

Private Sub InitVenda()
          Dim i As Byte
          Dim tam As Byte

1         On Error GoTo TrataErro
          'inicializa objetos do frame cliente
2         Call GetListaUf                ' carregar lista de uf - cboUF
3         cboUf.Enabled = True
4         cboFilial.Enabled = True
5         tam = cboUf.ListCount
6         For i = 0 To tam - 1
7             If cboUf.List(i) = uf_filial Then
8                 Exit For
9             End If
10        Next i
11        cboUf.ListIndex = -1
12        cboUf.ListIndex = i

13        Call InitCliente

          'inicializa objetos do frame condicao de pagamento
14        txtCOD_PLANO.Text = 82
15        If Not GetPlanoPgto(82, -1, -1) Then       ' carrega plano c�digo 53
16            MsgBox "Ligue para o departamento de sistemas", vbCritical, "Aten��o"
17        End If
18        txtDescAdicional.Text = "0"
19        lblPercDifIcm.Caption = "0"
20        If txtPC_ACRES_FIN_DPK = "" Then
21            sPC_ACRESCIMO_PLANO = 0
22        Else
23            sPC_ACRESCIMO_PLANO = CSng(txtPC_ACRES_FIN_DPK)
24        End If
25        If txtPC_DESC_FIN_DPK = "" Then
26            sPC_DESCONTO_PLANO = 0
27        Else
28            sPC_DESCONTO_PLANO = CSng(txtPC_DESC_FIN_DPK)
29        End If

          'inicializa objetos do frame produto
30        Call InitFrmeProduto

          'desabilitar botoes
31        cmdVisualiza.Enabled = False
32        cmdFinalizar.Enabled = False
33        cmdExcluir.Enabled = False
34        txtRepres.Text = ""
35        txtVend.Text = ""
36        Exit Sub

TrataErro:
37        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
38            Resume
39        Else
40            MsgBox "Sub InitVenda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
41        End If

End Sub

Private Sub InputItem(lngNUM_PENDENTE As Long, iNUM_ITEM As Integer, bNum_Preco As Byte)
1         On Error GoTo TrataErro

          Dim PLSQL As String
          Dim ss As Snapshot
          Dim ss2 As Snapshot
          Dim ss3 As Snapshot
          Dim ss4 As Snapshot
          Dim strTabela As String * 6
          Dim sngDescPer As Single
          Dim sngDescUf As Single
          Dim i As Byte
          Dim j As Byte
          Dim dblVL_IPI As Double
          Dim dblVL_IPI_ANT As Double
          Dim dblVL_PRECO_LIQ As Double
          Dim dblPreco_Liquido As Double
          Dim semana As Integer
          Dim dblVl_ICMretido As Double
          Dim vVlrICMRetido As Double 'TI-4405
          Dim vVlrPrecoLiquido As Double 'TI-4405
          
          'carregar tabela, desconto de periodo e desconto de uf
2         If bNum_Preco = 1 Then
3             strTabela = Mid$(frmeTabela.Caption, 1, 6)
4             If lblPC_DESC_PERIODO3_NORMAL.Caption = "" Then
5                 sngDescPer = 0
6             Else
7                 sngDescPer = lblPC_DESC_PERIODO3_NORMAL.Caption
8             End If
9             i = InStr(frmeTabela, "UF")
10            If i = 0 Then
11                sngDescUf = 0
12            Else
13                j = Len(frmeTabela) - (i + 2) - 1
14                sngDescUf = CSng(Mid$(frmeTabela.Caption, i + 3, j))
15            End If
16            i = 0           'TABELA NORMAL
17            dblVL_PRECO_LIQ = lblPRECO3_NORMAL.Caption

18        ElseIf bNum_Preco = 2 Then
19            strTabela = Mid$(frmeTabela.Caption, 1, 6)
20            If lblPC_DESC_PERIODO2_NORMAL.Caption = "" Then
21                sngDescPer = 0
22            Else
23                sngDescPer = lblPC_DESC_PERIODO2_NORMAL.Caption
24            End If
25            i = InStr(frmeTabela, "UF")
26            If i = 0 Then
27                sngDescUf = 0
28            Else
29                j = Len(frmeTabela) - (i + 2) - 1
30                sngDescUf = CSng(Mid$(frmeTabela.Caption, i + 3, j))
31            End If
32            i = 0           'TABELA NORMAL
33            dblVL_PRECO_LIQ = lblPRECO2_NORMAL.Caption

34        ElseIf bNum_Preco = 3 Then
35            strTabela = Mid$(frmeTabela.Caption, 1, 6)
36            If lblPC_DESC_PERIODO1_NORMAL.Caption = "" Then
37                sngDescPer = 0
38            Else
39                sngDescPer = lblPC_DESC_PERIODO1_NORMAL.Caption
40            End If
41            i = InStr(frmeTabela, "UF")
42            If i = 0 Then
43                sngDescUf = 0
44            Else
45                j = Len(frmeTabela) - (i + 2) - 1
46                sngDescUf = CSng(Mid$(frmeTabela.Caption, i + 3, j))
47            End If
48            i = 0           'TABELA NORMAL
49            dblVL_PRECO_LIQ = lblPRECO1_NORMAL.Caption

50        ElseIf bNum_Preco = 4 Then
51            strTabela = Mid$(frmeOFERTA.Caption, 1, 6)
52            If lblPC_DESC_PERIODO3_OFERTA.Caption = "" Then
53                sngDescPer = 0
54            Else
55                sngDescPer = lblPC_DESC_PERIODO3_OFERTA.Caption
56            End If
57            i = InStr(frmeOFERTA, "UF")
58            If i = 0 Then
59                sngDescUf = 0
60            Else
61                j = Len(frmeOFERTA) - (i + 2) - 1
62                sngDescUf = CSng(Mid$(frmeOFERTA.Caption, i + 3, j))
63            End If
64            i = 1           'TABELA OFERTA
65            dblVL_PRECO_LIQ = lblPRECO3_OFERTA.Caption

66        ElseIf bNum_Preco = 5 Then
67            strTabela = Mid$(frmeOFERTA.Caption, 1, 6)
68            If lblPC_DESC_PERIODO2_OFERTA.Caption = "" Then
69                sngDescPer = 0
70            Else
71                sngDescPer = lblPC_DESC_PERIODO2_OFERTA.Caption
72            End If
73            i = InStr(frmeOFERTA, "UF")
74            If i = 0 Then
75                sngDescUf = 0
76            Else
77                j = Len(frmeOFERTA) - (i + 2) - 1
78                sngDescUf = CSng(Mid$(frmeOFERTA.Caption, i + 3, j))
79            End If
80            i = 1           'TABELA OFERTA
81            dblVL_PRECO_LIQ = lblPRECO2_OFERTA.Caption

82        ElseIf bNum_Preco = 6 Then
83            strTabela = Mid$(frmeOFERTA.Caption, 1, 6)
84            If lblPC_DESC_PERIODO1_OFERTA.Caption = "" Then
85                sngDescPer = 0
86            Else
87                sngDescPer = lblPC_DESC_PERIODO1_OFERTA.Caption
88            End If
89            i = InStr(frmeOFERTA, "UF")
90            If i = 0 Then
91                sngDescUf = 0
92            Else
93                j = Len(frmeOFERTA) - (i + 2) - 1
94                sngDescUf = CSng(Mid$(frmeOFERTA.Caption, i + 3, j))
95            End If
96            i = 1           'TABELA OFERTA
97            dblVL_PRECO_LIQ = lblPRECO1_OFERTA.Caption

98        ElseIf bNum_Preco = 7 Then
99            strTabela = Mid$(frmeSP.Caption, 1, 6)
100           If lblPC_DESC_PERIODO3_SP.Caption = "" Then
101               sngDescPer = 0
102           Else
103               sngDescPer = lblPC_DESC_PERIODO3_SP.Caption
104           End If
105           i = InStr(frmeSP, "UF")
106           If i = 0 Then
107               sngDescUf = 0
108           Else
109               j = Len(frmeSP) - (i + 2) - 1
110               sngDescUf = CSng(Mid$(frmeSP.Caption, i + 3, j))
111           End If
112           i = 2           'TABELA SP
113           dblVL_PRECO_LIQ = lblPRECO3_SP.Caption

114       ElseIf bNum_Preco = 8 Then
115           strTabela = Mid$(frmeSP.Caption, 1, 6)
116           If lblPC_DESC_PERIODO2_SP.Caption = "" Then
117               sngDescPer = 0
118           Else
119               sngDescPer = lblPC_DESC_PERIODO2_SP.Caption
120           End If
121           i = InStr(frmeSP, "UF")
122           If i = 0 Then
123               sngDescUf = 0
124           Else
125               j = Len(frmeSP) - (i + 2) - 1
126               sngDescUf = CSng(Mid$(frmeSP.Caption, i + 3, j))
127           End If
128           i = 2           'TABELA SP
129           dblVL_PRECO_LIQ = lblPRECO2_SP.Caption

130       ElseIf bNum_Preco = 9 Then
131           strTabela = Mid$(frmeSP.Caption, 1, 6)
132           If lblPC_DESC_PERIODO1_SP.Caption = "" Then
133               sngDescPer = 0
134           Else
135               sngDescPer = lblPC_DESC_PERIODO1_SP.Caption
136           End If
137           i = InStr(frmeSP, "UF")
138           If i = 0 Then
139               sngDescUf = 0
140           Else
141               j = Len(frmeSP) - (i + 2) - 1
142               sngDescUf = CSng(Mid$(frmeSP.Caption, i + 3, j))
143           End If
144           i = 2           'TABELA SP
145           dblVL_PRECO_LIQ = lblPRECO1_SP.Caption

146       End If

        'TI-4405
        If CarregarICMSPreco(Val(frmVenda.txtCOD_DPK)) Then
            vVlrPrecoLiquido = Format$(dblPRECO_VENDA(i) * _
                                (1 - CDbl(sngDescPer) / 100) * _
                                (1 - CDbl(txtPC_DESC_FIN_DPK.Text) / 100) * _
                                (1 + CDbl(txtPC_ACRES_FIN_DPK.Text) / 100) * _
                                (1 - CDbl(lblDescSuframa.Caption) / 100) * _
                                (1 - CDbl(lblPercDifIcm.Caption) / 100) * _
                                (1 - CDbl(txtDescAdicional.Text) / 100) * _
                                (1 - sngDescUf / 100), "Standard")
     
            vVlrICMRetido = VL_ICMRETIDO(Val(cboDeposito.Text), Val(txtCOD_CLIENTE.Text), Val(txtCOD_DPK.Text), Val(bCod_Trib), vVlrPrecoLiquido)
            If vVlrICMRetido > 0 Then
                dblVL_PRECO_LIQ = dblVL_PRECO_LIQ - vVlrICMRetido
            End If
        End If
        'FIM TI-4405
    
          'valor contabil
147       dblPreco_Liquido = dblVL_PRECO_LIQ
148       dblVL_PRECO_LIQ = dblVL_PRECO_LIQ * CLng(frmQtdItem.txtQtdSolicitada.Text)

          'valor de IPI do item
149       If ssItem!COD_TRIBUTACAO_IPI = 1 Then
150           dblVL_IPI = dblVL_PRECO_LIQ * (1 - ssItem!PC_IPI / 100)
151       Else
152           dblVL_IPI = 0
153       End If

154       If optPedido.Value Then
              'PEDIDO
              'verificar existencia do item
155           SQL = "select QTD_SOLICITADA,"
156           SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as VL_CONTB_ANT "
157           SQL = SQL & "from ITEM_PEDIDO where "
158           SQL = SQL & "NUM_PENDENTE = " & lngNUM_PENDENTE
159           SQL = SQL & " and SEQ_PEDIDO = 0 and "
160           SQL = SQL & "COD_DPK = " & txtCOD_DPK.Text
161           Set ss = dbAccess.CreateSnapshot(SQL)
162           FreeLocks

      'Eduardo - 18/08/05
163           p_cod_loja = Mid(Trim(frmVenda.cboDeposito), 1, 2)
164           p_cod_cliente = frmVenda.txtCOD_CLIENTE
165           p_cod_dpk = frmVenda.txtCOD_DPK
166           p_cod_trib = bCod_Trib
167           p_preco_liquido = FmtBR(dblPreco_Liquido)

168           dblVl_ICMretido = VL_ICMRETIDO(p_cod_loja, p_cod_cliente, p_cod_dpk, p_cod_trib, p_preco_liquido)

              'If dblVl_ICMretido > 0 Then
              '    MsgBox "Valor de ICMS RETIDO unit�rio Aproximado: " & Format(dblVl_ICMretido, "0.00"), , "Aten��o"
              'End If

169           If ss.EOF And ss.BOF Then
170               If frmQtdItem.txtQtdSolicitada.Text = "0" Then
171                   MsgBox "Item com quantidade 0 (zero) n�o incluido", vbExclamation
172                   ss.Close
173                   Exit Sub
174               End If

                  'Eduardo - 13/04/2006
                  'Verificar se existem itens com o mesmo numero de sequencia
175               SQL = "select * from ITEM_PEDIDO "
176               SQL = SQL & "where NUM_PENDENTE = "
177               SQL = SQL & lngNUM_PEDIDO & " and SEQ_PEDIDO = 0 and NUM_ITEM_PEDIDO=" & iNUM_ITEM
178               Set ss = dbAccess.CreateSnapshot(SQL)
179               FreeLocks

180               If ss.EOF = False Then
181                  MsgBox "Existem itens no seu pedido com o mesmo numero de sequencia." & vbCrLf & "Por favor, exclua este pedido e redigite-o.", vbInformation, "Aten��o"
182                  Exit Sub
183               End If
184               ss.Close
                  

185               SQL = " Insert into ITEM_PEDIDO ( NUM_PENDENTE,"
186               SQL = SQL & " SEQ_PEDIDO,"
187               SQL = SQL & " COD_LOJA,"
188               SQL = SQL & " NUM_ITEM_PEDIDO,"
189               SQL = SQL & " COD_DPK,"
190               SQL = SQL & " COD_FORNECEDOR,"
191               SQL = SQL & " COD_FABRICA,"
192               SQL = SQL & " COD_TRIBUTACAO,"
193               SQL = SQL & " COD_TRIBUTACAO_IPI,"
194               SQL = SQL & " QTD_SOLICITADA,"
195               SQL = SQL & " TABELA_COMPRA,"
196               SQL = SQL & " PRECO_UNITARIO,"
197               SQL = SQL & " PRECO_LIQUIDO,"
198               SQL = SQL & " PC_DESCONTO1,"
199               SQL = SQL & " PC_DESCONTO2,"
200               SQL = SQL & " PC_DESCONTO3,"
201               SQL = SQL & " PC_DESC_ICM,"
202               SQL = SQL & " PC_IPI,"
203               SQL = SQL & " VL_ICMRETIDO) "
204               SQL = SQL & " values (" & lngNUM_PENDENTE & ","
205               SQL = SQL & " 0,"
206               SQL = SQL & Mid(frmVenda.cboDeposito.Text, 1, 2) & ","
207               SQL = SQL & iNUM_ITEM & ","
208               SQL = SQL & txtCOD_DPK.Text & ","
209               SQL = SQL & txtCOD_FORNECEDOR & ","
210               SQL = SQL & "'" & Fabrica & "',"
211               SQL = SQL & ssItem!cod_tributacao & ","
212               SQL = SQL & ssItem!COD_TRIBUTACAO_IPI & ","
213               SQL = SQL & frmQtdItem.txtQtdSolicitada.Text & ","
214               SQL = SQL & "'" & strTabela & "',"
215               SQL = SQL & FmtBR(dblPRECO_VENDA(i)) & ","
216               SQL = SQL & FmtBR(dblPreco_Liquido) & ","
217               SQL = SQL & FmtBR(sngDescPer) & ","
218               SQL = SQL & FmtBR(sngDescUf) & ","
219               SQL = SQL & FmtBR(txtDescAdicional.Text) & ","
220               SQL = SQL & FmtBR(lblPercDifIcm.Caption) & ","
221               SQL = SQL & FmtBR(ssItem!PC_IPI) & ","
222               SQL = SQL & FmtBR(dblVl_ICMretido) & ")"

                  'criar item de pedido
223               dbAccess.Execute SQL, dbFailOnError
224               FreeLocks

                  'atualizar pedido de venda
225               SQL = "update PEDIDO set "
226               SQL = SQL & "VL_IPI = VL_IPI + " & FmtBR(dblVL_IPI) & ","
227               SQL = SQL & "VL_CONTABIL = VL_CONTABIL + " & FmtBR(dblVL_PRECO_LIQ) & ","
228               SQL = SQL & "QTD_ITENS = QTD_ITENS + 1 "
229               SQL = SQL & "where NUM_PENDENTE = " & lngNUM_PENDENTE
230               SQL = SQL & " and SEQ_PEDIDO = 0"

                  'executa atualizacao
231               dbAccess.Execute SQL, dbFailOnError
232               FreeLocks


                  'disponibilizar botoes
233               cmdVisualiza.Enabled = True
234               cmdFinalizar.Enabled = True
235               cmdExcluir.Enabled = True

236           Else

                  'valor de IPI do item original
237               If ssItem!COD_TRIBUTACAO_IPI = 1 Then
238                   dblVL_IPI_ANT = ss!VL_CONTB_ANT * (1 - ssItem!PC_IPI / 100)
239               Else
240                   dblVL_IPI_ANT = 0
241               End If

242               If frmQtdItem.txtQtdSolicitada.Text = "0" Then
                      'eliminar item

                      'atualizar pedido de venda
243                   SQL = "update PEDIDO set "
244                   SQL = SQL & "VL_IPI = VL_IPI - " & FmtBR(dblVL_IPI_ANT) & ","
245                   SQL = SQL & "VL_CONTABIL = VL_CONTABIL - " & FmtBR(ss!VL_CONTB_ANT) & ","
246                   SQL = SQL & "QTD_ITENS = QTD_ITENS - 1 "
247                   SQL = SQL & "where NUM_PENDENTE = " & lngNUM_PENDENTE
248                   SQL = SQL & " and SEQ_PEDIDO = 0"

                      'executa atualizacao
249                   dbAccess.Execute SQL, dbFailOnError
250                   FreeLocks


                      'carregar SQL
251                   SQL = "select QTD_ITENS from PEDIDO where NUM_PENDENTE = " & lngNUM_PENDENTE
252                   SQL = SQL & " and SEQ_PEDIDO = 0"

                      'executar consulta
253                   Set ss2 = dbAccess.CreateSnapshot(SQL)
254                   FreeLocks

255                   If ss2!QTD_ITENS = 0 Then
                          'inibir botoes
256                       cmdVisualiza.Enabled = False
257                       cmdFinalizar.Enabled = False
258                       cmdExcluir.Enabled = False

                          'eliminar pedido
259                       SQL = "delete from PEDIDO where NUM_PENDENTE = " & lngNUM_PENDENTE
260                       SQL = SQL & " and SEQ_PEDIDO = 0"
261                       dbAccess.Execute SQL, dbFailOnError
262                       FreeLocks

263                       lngNUM_PEDIDO = 0
264                   Else
                          'eliminar item de pedido
265                       SQL = "delete from ITEM_PEDIDO where NUM_PENDENTE = " & lngNUM_PENDENTE
266                       SQL = SQL & " and SEQ_PEDIDO = 0 and COD_DPK = " & txtCOD_DPK.Text
267                       dbAccess.Execute SQL, dbFailOnError
268                       FreeLocks

                          'resequenciar pedido
269                       Call ReSequenciar("PEDIDO")

270                   End If
271                   ss2.Close

272               Else
                      'alterar quantidade

                      'atualiza item
273                   SQL = " update ITEM_PEDIDO set "
274                   SQL = SQL & "QTD_SOLICITADA = " & frmQtdItem.txtQtdSolicitada.Text & ","
275                   SQL = SQL & "TABELA_COMPRA = " & "'" & strTabela & "',"
276                   SQL = SQL & "PRECO_UNITARIO = " & FmtBR(dblPRECO_VENDA(i)) & ","
277                   SQL = SQL & "PRECO_LIQUIDO = " & FmtBR(dblPreco_Liquido) & ","
278                   SQL = SQL & "PC_DESCONTO1 = " & FmtBR(sngDescPer) & ","
279                   SQL = SQL & "PC_DESCONTO2 = " & FmtBR(sngDescUf) & ","
280                   SQL = SQL & "PC_DESCONTO3 = " & FmtBR(txtDescAdicional.Text) & ","
281                   SQL = SQL & "PC_DESC_ICM = " & FmtBR(lblPercDifIcm.Caption) & ", "
282                   SQL = SQL & "VL_ICMRETIDO = " & FmtBR(dblVl_ICMretido) & " "
283                   SQL = SQL & "where NUM_PENDENTE = " & lngNUM_PENDENTE
284                   SQL = SQL & " and SEQ_PEDIDO = 0 and COD_DPK = " & txtCOD_DPK.Text
                      'executa atualiza��o
285                   dbAccess.Execute SQL, dbFailOnError
286                   FreeLocks


                      'atualizar pedido de venda
287                   SQL = "update PEDIDO set "
288                   SQL = SQL & "VL_IPI = VL_IPI - " & FmtBR(dblVL_IPI_ANT) & " + " & FmtBR(dblVL_IPI) & ","
289                   SQL = SQL & "VL_CONTABIL = VL_CONTABIL - " & FmtBR(ss!VL_CONTB_ANT) & " + " & FmtBR(dblVL_PRECO_LIQ) & " "
290                   SQL = SQL & "where NUM_PENDENTE = " & lngNUM_PENDENTE
291                   SQL = SQL & " and SEQ_PEDIDO = 0"
                      'executa atualizacao
292                   dbAccess.Execute SQL, dbFailOnError
293                   FreeLocks

294               End If
295           End If

296       Else

              'COTACAO
              'verificar existencia do item
297           SQL = "select QTD_SOLICITADA"
298           SQL = SQL & " from ITEM_COTACAO where"
299           SQL = SQL & " COD_DPK = " & txtCOD_DPK.Text
300           SQL = SQL & " and COD_LOJA = " & lngCOD_LOJA
301           SQL = SQL & " and NUM_COTACAO = " & lngNUM_PENDENTE



302           Set ss3 = dbAccess2.CreateSnapshot(SQL)
303           FreeLocks


304           If ss3.EOF And ss3.BOF Then
305               If frmQtdItem.txtQtdSolicitada.Text = "0" Then
306                   MsgBox "Item com quantidade 0 (zero) n�o incluido", vbExclamation
307                   ss3.Close
308                   Exit Sub
309               End If


310               PLSQL = " insert into ITEM_COTACAO ("
311               PLSQL = PLSQL & " COD_LOJA,"
312               PLSQL = PLSQL & " NUM_COTACAO,"
313               PLSQL = PLSQL & " NUM_ITEM_COTACAO,"
314               PLSQL = PLSQL & " COD_DPK,"
315               PLSQL = PLSQL & " PRECO_UNITARIO,"
316               PLSQL = PLSQL & " PC_DESC1,"
317               PLSQL = PLSQL & " PC_DESC2,"
318               PLSQL = PLSQL & " PC_DESC3,"
319               PLSQL = PLSQL & " PC_DIFICM,"
320               PLSQL = PLSQL & " QTD_SOLICITADA,"
321               PLSQL = PLSQL & " TABELA_VENDA,"
322               PLSQL = PLSQL & " SITUACAO,"
323               PLSQL = PLSQL & " FL_SEMANA,"
324               PLSQL = PLSQL & " COMPLEMENTO) "
325               PLSQL = PLSQL & " values ("
326               PLSQL = PLSQL & Mid(cboDeposito.Text, 1, 2) & ","
327               PLSQL = PLSQL & lngNUM_PENDENTE & ","
328               PLSQL = PLSQL & iNUM_ITEM & ","
329               PLSQL = PLSQL & txtCOD_DPK.Text & ","
330               PLSQL = PLSQL & FmtBR(dblPRECO_VENDA(i)) & ","
331               PLSQL = PLSQL & FmtBR(sngDescPer) & ","
332               PLSQL = PLSQL & FmtBR(sngDescUf) & ","
333               PLSQL = PLSQL & FmtBR(txtDescAdicional.Text) & ","
334               PLSQL = PLSQL & FmtBR(lblPercDifIcm.Caption) & ","
335               PLSQL = PLSQL & frmQtdItem.txtQtdSolicitada.Text & ","
336               PLSQL = PLSQL & "'" & strTabela & "' ,"
337               PLSQL = PLSQL & " 0,"
338               semana = 0
339               If chkSemanaPassada.Value = vbChecked Then
340                   PLSQL = PLSQL & " 1,"
341               Else
342                   PLSQL = PLSQL & " 0,"
343               End If
344               PLSQL = PLSQL & "'" & frmQtdItem.txtComplemento.Text & "')"

                  'criar item
345               dbAccess2.Execute PLSQL, dbFailOnError
346               FreeLocks


                  'disponibilizar botoes
347               cmdVisualiza.Enabled = True
348               cmdFinalizar.Enabled = True
349               cmdExcluir.Enabled = True

350           Else

351               If frmQtdItem.txtQtdSolicitada.Text = "0" Then

                      'eliminar item da cotacao

352                   PLSQL = " delete from ITEM_COTACAO where "
353                   PLSQL = PLSQL & " NUM_COTACAO = " & lngNUM_PENDENTE
354                   PLSQL = PLSQL & " and COD_LOJA = " & lngCOD_LOJA
355                   PLSQL = PLSQL & " and COD_DPK = " & txtCOD_DPK.Text
356                   dbAccess2.Execute PLSQL, dbFailOnError
357                   FreeLocks


                      'carregar quantidade de itens da cotacao
358                   SQL = "select count(*) as QTD_ITENS from ITEM_COTACAO "
359                   SQL = SQL & " where NUM_COTACAO = " & lngNUM_PENDENTE & "  and"
360                   SQL = SQL & " COD_LOJA = " & lngCOD_LOJA

361                   Set ss4 = dbAccess2.CreateSnapshot(SQL)
362                   FreeLocks


363                   If ss4!QTD_ITENS = 0 Then
                          'inibir botoes
364                       ss4.Close
365                       cmdVisualiza.Enabled = False
366                       cmdFinalizar.Enabled = False
367                       cmdExcluir.Enabled = False

                          'eliminar cotacao

368                       PLSQL = " delete from ITEM_COTACAO where "
369                       PLSQL = PLSQL & " NUM_COTACAO = " & lngNUM_PENDENTE
370                       PLSQL = PLSQL & " and COD_LOJA = " & lngCOD_LOJA

371                       dbAccess2.Execute PLSQL, dbFailOnError
372                       FreeLocks


373                       lngNUM_COTACAO = 0
374                   Else
                          'resequenciar cotacao
375                       Call ReSequenciar("COTACAO")
376                   End If

377               Else
                      'alterar quantidade

                      'atualiza item
378                   semana = 0
379                   If chkSemanaPassada.Value = vbChecked Then
380                       SQL = SQL & "FL_SEMANA = 1,"
381                   Else
382                       SQL = SQL & "FL_SEMANA = 0,"
383                   End If

384                   PLSQL = " update ITEM_COTACAO set "
385                   PLSQL = PLSQL & " QTD_SOLICITADA =" & frmQtdItem.txtQtdSolicitada.Text & ","
386                   PLSQL = PLSQL & " TABELA_VENDA = '" & strTabela & "',"
387                   PLSQL = PLSQL & " PRECO_UNITARIO = " & FmtBR(dblPRECO_VENDA(i)) & ","
388                   PLSQL = PLSQL & " PC_DESC1 =" & FmtBR(sngDescPer) & ","
389                   PLSQL = PLSQL & " PC_DESC2 =" & FmtBR(sngDescUf) & ","
390                   PLSQL = PLSQL & " PC_DESC3 =" & FmtBR(txtDescAdicional.Text) & ","
391                   PLSQL = PLSQL & " PC_DIFICM =" & FmtBR(lblPercDifIcm.Caption) & ","
392                   PLSQL = PLSQL & " FL_SEMANA =" & semana & ","
393                   PLSQL = PLSQL & " COMPLEMENTO = '" & frmQtdItem.txtComplemento.Text & "'"
394                   PLSQL = PLSQL & " where NUM_COTACAO = " & lngNUM_PENDENTE
395                   PLSQL = PLSQL & " and COD_LOJA = " & lngCOD_LOJA
396                   PLSQL = PLSQL & " and COD_DPK = " & txtCOD_DPK.Text

                      'executa atualiza��o
397                   dbAccess2.Execute PLSQL, dbFailOnError
398                   FreeLocks


399               End If
400           End If

401       End If
402       Unload frmQtdItem
403       FreeLocks

404       Exit Sub

TrataErro:
405       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
406           Label1.Visible = True
407           contador = contador + 1
408           Label1.Caption = contador
409           Resume
              '  ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
410           MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
411           ReInitProduto
412           Exit Sub
413       Else
414           MsgBox "Sub InputItem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
415       End If

End Sub

Private Sub InputPedidoCotacao(bNum_Preco As Byte)
1         On Error GoTo TrataErro

          Dim PLSQL As String
          Dim ssSeqCotacao As Snapshot
          Dim ss As Snapshot
          Dim ss1 As Snapshot
          Dim dn As Snapshot
          Dim dif As String
          Dim lngNUM_PENDENTE As Long
          Dim lngNUM_COTACAO As Long
          Dim dDigitacao As Date
          Dim dProxSeg As Date
          Dim Trava_pedido As Integer
          

'------- Jairo Almeida - Comentando as linhas 2 � 11  Atendendo � solicita��o DUM SDS2405 - 12/01/2012 -------
'2         If frmVenda.optPedido.Value Then
'3             If dia_atua > 3 Then
'4                 MsgBox "O sistema est� desatualizado � : " & dia_atua _
                          & " dias. IMPOSS�VEL montar PEDIDO. Fa�a a atualiza��o.", vbExclamation, "ATEN��O"
'5                 Exit Sub
'6             End If
'7         Else
'8             If dia_atua > 3 Then
'9                 MsgBox "O sistema est� desatualizado � : " & dia_atua _
'                          & " dias. IMPOSS�VEL montar COTA��O. Fa�a a atualiza��o.", vbExclamation, "ATEN��O"
'10                Exit Sub
'11            End If
'12        End If
'--------------------------------------------------------------------------------------------------------------


13        If frmVenda.optPedido.Value Then

14            dbAccess.BeginTrans
15            SQL = "Update sequencia set proximo_pedido = proximo_pedido + 1"
16            dbAccess.Execute SQL, dbFailOnError

17            SQL = "select PROXIMO_PEDIDO from SEQUENCIA"
18            Set dn = dbAccess.CreateSnapshot(SQL)

19            dn.MoveFirst
20            dbAccess.CommitTrans

21            SQL = "Select tipo_usuario"
22            SQL = SQL & " From usuario"
23            Set ss1 = dbAccess3.CreateSnapshot(SQL)

              'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
24            If UCase(ss1!Tipo_Usuario) <> "R" And UCase(ss1!Tipo_Usuario) <> "G" Then
25                lngNUM_PENDENTE = CLng(CStr(dn("PROXIMO_PEDIDO")) & Trim(Mid(FILIAL_PED, 2, 3)))
26            Else
27                lngNUM_PENDENTE = Format(sCOD_VEND, "0000") & CLng(CStr(dn("PROXIMO_PEDIDO")))
28            End If

29            dn.Close
30            ss1.Close
31            FreeLocks

              'comando para criar pedido
32            SQL = "insert into PEDIDO (NUM_PENDENTE,"
33            SQL = SQL & "SEQ_PEDIDO,"
34            SQL = SQL & "COD_LOJA,"
35            SQL = SQL & "FL_PEDIDO_FINALIZADO,"
36            SQL = SQL & "DT_DIGITACAO,"
37            SQL = SQL & "COD_CLIENTE,"
38            SQL = SQL & "COD_UF,"
39            SQL = SQL & "COD_REPRES,"
40            SQL = SQL & "COD_VEND,"
41            SQL = SQL & "PC_DESC_SUFRAMA,"
42            SQL = SQL & "COD_PLANO,"
43            SQL = SQL & "PC_DESCONTO,"
44            SQL = SQL & "PC_ACRESCIMO,"
45            SQL = SQL & "FL_DIF_ICM) "
46            SQL = SQL & "values (" & lngNUM_PENDENTE & ","
47            SQL = SQL & "0,"
48            SQL = SQL & Mid(frmVenda.cboDeposito.Text, 1, 2) & ","
49            SQL = SQL & "'N',"
'50            sql = sql & "'" & Date & "',"
50            SQL = SQL & "'" & Data_Faturamento & "',"
51            If txtCOD_CLIENTE.Text = "" Or txtCOD_CLIENTE.Text = "0" Then
52                SQL = SQL & "0,"
53            Else
54                SQL = SQL & txtCOD_CLIENTE.Text & ","
55            End If
56            SQL = SQL & "'" & cboUf.List(cboUf.ListIndex) & "',"
57            SQL = SQL & " IIF(" & fil & "=" & sCOD_VEND & ",0," & fil & "),"
58            SQL = SQL & sCOD_VEND & ","
59            SQL = SQL & FmtBR(lblDescSuframa.Caption) & ","
60            SQL = SQL & txtCOD_PLANO.Text & ","
61            SQL = SQL & FmtBR(txtPC_DESC_FIN_DPK.Text) & ","
62            SQL = SQL & FmtBR(txtPC_ACRES_FIN_DPK.Text) & ","
63            If chkDifIcm.Value = vbChecked Then
64                SQL = SQL & "'S')"
65            Else
66                SQL = SQL & "'N')"
67            End If

              'criar pedido
68            dbAccess.Execute SQL, dbFailOnError
69            FreeLocks

70            lngNUM_PEDIDO = lngNUM_PENDENTE

71        Else
              'Dim vNumSeqCotacao1 As Double
              'Dim vNumSeqCotacao2 As Double
              'Dim vNumSeqCotacaoDefinitivo As Double
              
              'Pegar numero da ultima Cotacao
72            'sql = "SELECT max(Num_Cotacao) as NumCotacao from Cotacao"
73            'Set ssSeqCotacao = dbAccess2.CreateSnapshot(sql)
74            'If IsNull(ssSeqCotacao!numCotacao) Then
75            '   vNumSeqCotacao1 = 0
76            'Else
77            '   vNumSeqCotacao1 = ssSeqCotacao!numCotacao
78            'End If
              
              'Eduardo - 02/01/06
79            'sql = "SELECT max(Num_Cotacao) as NumCotacao from ITEM_Cotacao"
80            'Set ssSeqCotacao = dbAccess2.CreateSnapshot(sql)
81            'If IsNull(ssSeqCotacao!numCotacao) Then
82            '   vNumSeqCotacao2 = 0
83            'Else
84            '   vNumSeqCotacao2 = ssSeqCotacao!numCotacao
85            'End If
              
86            'If vNumSeqCotacao1 > vNumSeqCotacao2 Then
87            '   vNumSeqCotacaoDefinitivo = vNumSeqCotacao1
88            'Else
89            '   vNumSeqCotacaoDefinitivo = vNumSeqCotacao2
90            'End If
                
91            dbAccess.BeginTrans
92            SQL = "Update seq_cotacao set proxima_cotacao = proxima_cotacao + 1"
94            dbAccess.Execute SQL, dbFailOnError
95            dbAccess.CommitTrans

96            SQL = "select PROXIMA_COTACAO from SEQ_COTACAO"
97            Set dn = dbAccess.CreateSnapshot(SQL)
98            dn.MoveFirst

99            lngNUM_COTACAO = CLng(CStr(dn("PROXIMA_COTACAO")))
100           dn.Close
101           FreeLocks

              'comando para criar cotacao
102           dif = ""
103           If chkDifIcm.Value = vbChecked Then
104               dif = "S"                              'dif.de icm
105           Else
106               dif = "N"
107           End If


108           PLSQL = " insert into COTACAO (cod_loja, num_cotacao,"
109           PLSQL = PLSQL & " dt_cotacao,dt_validade,cod_cliente,"
110           PLSQL = PLSQL & " cod_vend,cod_plano,pc_desconto,pc_desc_suframa,"
111           PLSQL = PLSQL & " pc_acrescimo,fl_tipo,fl_dif_icm,situacao,"
112           PLSQL = PLSQL & " observ,cod_repres) values("
113           PLSQL = PLSQL & Mid(frmVenda.cboDeposito.Text, 1, 2) & ","
114           PLSQL = PLSQL & lngNUM_COTACAO & ","           'numero da cotacao
115           PLSQL = PLSQL & "'" & Format(CDate(Data_Faturamento), "dd/mm/YY") & "',"  'data da cotacao
116           dDigitacao = CDate(Data_Faturamento)
              'O c�lculo foi alterado de segunda para s�bado. A cota��o ter� validade at� o s�bado, pois
              'na segunda j� houve a troca de tabelas. - 03/2004
117           If Weekday(dDigitacao) = 0 Then
                  'domingo
118               dProxSeg = dDigitacao
119           ElseIf Weekday(dDigitacao) = 1 Then
                  'segunda
120               dProxSeg = dDigitacao + 5
121           Else
                  'ter�a � s�bado
122               dProxSeg = dDigitacao + (7 - Weekday(dDigitacao))
123           End If

124           PLSQL = PLSQL & "'" & Format(CDate(dProxSeg), "dd/mm/yy") & "' ,"
125           PLSQL = PLSQL & txtCOD_CLIENTE.Text & ","         'codigo do cliente
126           PLSQL = PLSQL & sCOD_VEND & " ,"         'codigo do vendedor
127           PLSQL = PLSQL & txtCOD_PLANO.Text & ","        'codigo do plano
128           PLSQL = PLSQL & FmtBR(txtPC_DESC_FIN_DPK.Text) & ","     '% desconto do plano
129           PLSQL = PLSQL & FmtBR(lblDescSuframa.Caption) & ","     '%suframa
130           PLSQL = PLSQL & FmtBR(txtPC_ACRES_FIN_DPK.Text) & ","
131           PLSQL = PLSQL & "0 ,"
132           PLSQL = PLSQL & " '" & dif & "',"
133           PLSQL = PLSQL & " 8,'',"                                 'situacao e msg cotacao
134           PLSQL = PLSQL & Mid(txtRepres.Text, 1, 4) & ")"       'codigo do representante escolhido

              'criar pedido
135           dbAccess2.Execute PLSQL, dbFailOnError
136           FreeLocks

137           SQL = "select NUM_COTACAO from COTACAO where "
138           SQL = SQL & " COD_CLIENTE = " & txtCOD_CLIENTE.Text & " and"
139           SQL = SQL & " COD_LOJA = " & Mid(cboDeposito.Text, 1, 2) & " and"
140           SQL = SQL & " COD_VEND = " & sCOD_VEND & " and COD_REPRES = " & Mid(txtRepres.Text, 1, 4) & " AND "
141           SQL = SQL & " FL_TIPO = 0 and SITUACAO = 8"

142           Set ss = dbAccess2.CreateSnapshot(SQL)
143           FreeLocks
144           If ss.EOF And ss.BOF Then
145               MsgBox "CONSULTA NUMERO DA COTA��O PENDENTE FALHOU." & Chr(13) & "Ligue para o Departamento de Sistemas", vbCritical, "ATEN��O"
146               ss.Close
147               MsgBox "Sub txtCod_Banco_Lostfocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
148           End If
149           lngNUM_COTACAO = ss!NUM_COTACAO
150           lngNUM_PENDENTE = lngNUM_COTACAO
151           ss.Close
152       End If

          'inclui item de pedido
153       Call InputItem(lngNUM_PENDENTE, 1, bNum_Preco)
154       FreeLocks
155       Exit Sub

TrataErro:
156       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
157           Label1.Visible = True
158           contador = contador + 1
159           Label1.Caption = contador
160           Resume
161           MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
162           ReInitProduto
163           Exit Sub
164       ElseIf Err.Number = 3022 Then
              
              
          
165       Else
166           MsgBox "Sub inputPedidoCotacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
167       End If


End Sub

Private Sub lbl_TipoA_Normal_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO3_NORMAL.Visible = True
        frmVenda.lblPC_DESC_PERIODO3_NORMAL.Visible = True
        frmVenda.lblSinal(0).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If

    Call SelecionaPrecoVenda(lblPRECO3_NORMAL, 1)

End Sub

Private Sub lbl_TipoA_Oferta_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO3_OFERTA.Visible = True
        frmVenda.lblPC_DESC_PERIODO3_OFERTA.Visible = True
        frmVenda.lblSinal(3).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If


    Call SelecionaPrecoVenda(lblPRECO3_OFERTA, 4)

End Sub

Private Sub lbl_TipoA_SP_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO3_SP.Visible = True
        frmVenda.lblPC_DESC_PERIODO3_SP.Visible = True
        frmVenda.lblSinal(6).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If

    Call SelecionaPrecoVenda(lblPRECO3_SP, 7)

End Sub

Private Sub lbl_TipoB_Normal_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO2_NORMAL.Visible = True
        frmVenda.lblPC_DESC_PERIODO2_NORMAL.Visible = True
        frmVenda.lblSinal(1).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If

    Call SelecionaPrecoVenda(lblPRECO2_NORMAL, 2)

End Sub

Private Sub lbl_TipoB_Oferta_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO2_OFERTA.Visible = True
        frmVenda.lblPC_DESC_PERIODO2_OFERTA.Visible = True
        frmVenda.lblSinal(4).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If

    Call SelecionaPrecoVenda(lblPRECO2_OFERTA, 5)

End Sub

Private Sub lbl_TipoB_SP_Click()
    'selecionar preco
    'RTI-142 - Adicionando a Condi��o para o Tipo de Usu�rio igual a Gerente
    If Tipo_Usuario = "R" Or Tipo_Usuario = "G" Then
        frmVenda.lblPRECO2_SP.Visible = True
        frmVenda.lblPC_DESC_PERIODO2_SP.Visible = True
        frmVenda.lblSinal(7).Visible = True
    ElseIf Tipo_Usuario = "C" Then
        Exit Sub
    End If

    Call SelecionaPrecoVenda(lblPRECO2_SP, 8)

End Sub

Private Sub lbl_TipoC_Normal_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_NORMAL, 3)

End Sub

Private Sub lbl_TipoC_Oferta_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_OFERTA, 6)

End Sub

Private Sub lbl_TipoC_SP_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_SP, 9)

End Sub

Private Sub lblPRECO1_NORMAL_Change()
    If lblPRECO1_NORMAL.Caption <> "" And lblPC_DESC_PERIODO1_NORMAL <> "" Then
        lblSinal(2).Visible = True
    Else
        lblSinal(2).Visible = False
    End If
End Sub

Private Sub lblPRECO1_NORMAL_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_NORMAL, 3)
End Sub

Private Sub lblPRECO1_OFERTA_Change()
    If lblPRECO1_OFERTA.Caption <> "" And lblPC_DESC_PERIODO1_OFERTA <> "" Then
        lblSinal(5).Visible = True
    Else
        lblSinal(5).Visible = False
    End If
End Sub

Private Sub lblPRECO1_OFERTA_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_OFERTA, 6)
End Sub

Private Sub lblPRECO1_SP_Change()
    If lblPRECO1_SP.Caption <> "" And lblPC_DESC_PERIODO1_SP <> "" Then
        lblSinal(8).Visible = True
    Else
        lblSinal(8).Visible = False
    End If
End Sub

Private Sub lblPRECO1_SP_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO1_SP, 9)
End Sub

Private Sub lblPRECO2_NORMAL_Change()
    If lblPRECO2_NORMAL.Caption <> "" And lblPC_DESC_PERIODO2_NORMAL <> "" Then
        lblSinal(1).Visible = True
    Else
        lblSinal(1).Visible = False
    End If
End Sub

Private Sub lblPRECO2_NORMAL_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO2_NORMAL, 2)
End Sub

Private Sub lblPRECO2_OFERTA_Change()
    If lblPRECO2_OFERTA.Caption <> "" And lblPC_DESC_PERIODO2_OFERTA <> "" Then
        lblSinal(4).Visible = True
    Else
        lblSinal(4).Visible = False
    End If
End Sub

Private Sub lblPRECO2_OFERTA_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO2_OFERTA, 5)
End Sub

Private Sub lblPRECO2_SP_Change()
    If lblPRECO2_SP.Caption <> "" And lblPC_DESC_PERIODO2_SP <> "" Then
        lblSinal(7).Visible = True
    Else
        lblSinal(7).Visible = False
    End If
End Sub

Private Sub lblPRECO2_SP_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO2_SP, 8)
End Sub

Private Sub lblPRECO3_NORMAL_Change()
    If lblPRECO3_NORMAL.Caption <> "" And lblPC_DESC_PERIODO3_NORMAL <> "" Then
        lblSinal(0).Visible = True
    Else
        lblSinal(0).Visible = False
    End If
End Sub

Private Sub lblPRECO3_NORMAL_Click()

    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO3_NORMAL, 1)

End Sub

Private Sub lblPRECO3_OFERTA_Change()
    If lblPRECO3_OFERTA.Caption <> "" And lblPC_DESC_PERIODO3_OFERTA <> "" Then
        lblSinal(3).Visible = True
    Else
        lblSinal(3).Visible = False
    End If
End Sub

Private Sub lblPRECO3_OFERTA_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO3_OFERTA, 4)
End Sub

Private Sub lblPRECO3_SP_Change()
    If lblPRECO3_SP.Caption <> "" And lblPC_DESC_PERIODO3_SP <> "" Then
        lblSinal(6).Visible = True
    Else
        lblSinal(6).Visible = False
    End If
End Sub

Private Sub lblPRECO3_SP_Click()
    'selecionar preco
    Call SelecionaPrecoVenda(lblPRECO3_SP, 7)
End Sub

Private Sub optCotacao_Click()
          'cboDeposito.Text = deposito_default
          'cboDeposito.Enabled = False

    On Error GoTo Trata_Erro

1         If fl_existe_cotacao = "N" Then
2             lngNUM_PEDIDO = 0
3             fl_existe_pedido = "N"

              '--------- s� ir� pegar a cota��o quando n�o existir pedido pendente
              'verificar cota��o pendente
4             SQL = "select NUM_COTACAO,"
5             SQL = SQL & " COD_LOJA,"
6             SQL = SQL & " COD_CLIENTE,"
7             SQL = SQL & " COD_REPRES,"
8             SQL = SQL & " COD_VEND,"
9             SQL = SQL & " FL_DIF_ICM,"
10            SQL = SQL & " COD_PLANO,"
11            SQL = SQL & " PC_ACRESCIMO,"
12            SQL = SQL & " PC_DESCONTO"
13            SQL = SQL & " from COTACAO "
14            SQL = SQL & " where "
15            SQL = SQL & " cod_vend = " & CStr(sCOD_VEND) & " And "
16            SQL = SQL & " SITUACAO = 8"

17            Set ss1 = dbAccess2.CreateSnapshot(SQL)
18            FreeLocks

19            If ss1.EOF() And ss1.BOF Then
                  'n�o ha cotacao pendente
20                lngNUM_COTACAO = 0
21                FIL1 = sCOD_VEND
22                fil = 0
23                fl_existe_cotacao = "N"
24            Else
25                lngNUM_COTACAO = ss1!NUM_COTACAO
26                lngCOD_LOJA = ss1!COD_LOJA
27                fil = ss1!Cod_Repres
28                FIL1 = sCOD_VEND
29                fl_existe_cotacao = "S"
30            End If

31            ss1.Close
              'inicializar form
32            If lngNUM_PEDIDO > 0 And lngNUM_COTACAO > 0 Then
                  '        'selecionar cotacao ou pedido para continuar
                  '        optPedido.Value = True
33                Call ContVenda
34            ElseIf lngNUM_PEDIDO > 0 Then
                  '        'selecionar pedido
                  '        optPedido.Value = True
35                Call ContVenda
36            ElseIf lngNUM_COTACAO > 0 Then
                  '        'selecionar cotacao
                  '        optCotacao.Value = True
37                Call ContVenda
38            Else
                  'selecionar cotacao - default
                  '        optCotacao.Value = True
                  '        Call InitVenda
                  '        Call GetFilial
39            End If

40            If optCotacao.Value = True Then
                  'cboDeposito.Enabled = False
41            End If

42            Call TABELA_VENDA
43            uf_destino = cboUf.Text

              'mouse pointer
44            Screen.MousePointer = vbDefault

45        End If

46        If lngNUM_COTACAO > 0 Then
47            cmdVisualiza.Enabled = True
48            cmdFinalizar.Enabled = True
49            cmdExcluir.Enabled = True
50        Else
51            cmdVisualiza.Enabled = False
52            cmdFinalizar.Enabled = False
53            cmdExcluir.Enabled = False
54        End If

          'carregar tabela Venda_Limitada
          Dim CODUF2 As String
          Dim coduf As String
          Dim codloja As Integer

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub optCotacao_click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub optPedido_Click()

    On Error GoTo Trata_Erro

1         If fl_existe_pedido = "N" Then
2             fl_existe_cotacao = "N"
3             SQL = "select NUM_PENDENTE,"
4             SQL = SQL & "COD_LOJA,"
5             SQL = SQL & "COD_CLIENTE,"
6             SQL = SQL & "COD_REPRES,"
7             SQL = SQL & "FL_DIF_ICM,"
8             SQL = SQL & "COD_PLANO,"
9             SQL = SQL & "PC_ACRESCIMO,"
10            SQL = SQL & "PC_DESCONTO "
11            SQL = SQL & " from PEDIDO "
12            SQL = SQL & " where FL_PEDIDO_FINALIZADO = 'N' and COD_VEND = " & CStr(sCOD_VEND)
13            SQL = SQL & " Order by num_pendente    "

14            Set ss = dbAccess.CreateSnapshot(SQL)
15            FreeLocks

16            If Not ss.EOF Then

17                ss.MoveFirst
18                lngNUM_PEDIDO = ss!num_pendente
19                lngCOD_LOJA = ss!COD_LOJA
20                fil = IIf(ss!Cod_Repres = 0, sCOD_VEND, ss!Cod_Repres)
21                fl_existe_pedido = "S"

23            End If

              'inicializar form
24            If lngNUM_PEDIDO > 0 And lngNUM_COTACAO > 0 Then
25                optPedido.Value = True
26                Call ContVenda
27            ElseIf lngNUM_PEDIDO > 0 Then
28                optPedido.Value = True
29                Call ContVenda
30            ElseIf lngNUM_COTACAO > 0 Then
31                optPedido.Value = True
32            Else
33            End If

36            Call TABELA_VENDA
37            uf_destino = cboUf.Text

38            Screen.MousePointer = vbDefault

39        End If
40        cboDeposito.Enabled = True
41        If lngNUM_PEDIDO > 0 Then
42            cmdVisualiza.Enabled = True
43            cmdFinalizar.Enabled = True
44            cmdExcluir.Enabled = True
45        Else
46            cmdVisualiza.Enabled = False
47            cmdFinalizar.Enabled = False
48            cmdExcluir.Enabled = False
49        End If

          'carregar tabela Venda_Limitada
          Dim CODUF2 As String
          Dim coduf As String
          Dim codloja As Integer

50        TpCliente = Left(tipo_cliente, 1)
51        CODUF2 = cboUf.Text

52        SQL = "select * from VENDA_LIMITADA " & _
                  "where tp_cliente = '" & TpCliente & "' and cod_uf = '" & CODUF2 & "'"

53        Set ss7 = dbAccess2.CreateSnapshot(SQL)
54        FreeLocks

55        If ss7.EOF Then
56            Exit Sub
57        Else
58            coduf = ss7!cod_uf
59            codloja = ss7!cod_loja_obrigatoria

60            SQL = "select cod_loja, nome_fantasia " & _
                      "from loja " & _
                      "where cod_loja = " & codloja & ""

61            Set ss7 = dbAccess2.CreateSnapshot(SQL)
62            FreeLocks

63            If Not ss7.EOF Then
64            Else
65                MsgBox "Cliente do DF, pedido deve ser digitado pelo CD DF!"
66                Unload Me
67                frmVenda.Show
68            End If
69        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub optPedido_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub ReInitProduto()
    
1         On Error GoTo Trata_Erro

          'reinicializa frame com excessao do fornecedor
2         txtCOD_DPK.Text = ""
3         lblDigito.Caption = ""
4         class_fiscal = 0

5         item_grupo = 0
6         item_subgrupo = 0

7         txtCOD_FABRICA.Text = ""
8         txtCOD_FABRICA.Enabled = True
9         txtDescricao.Text = ""
          'txtDescricao.Enabled = False
10        lblIPI.Caption = Format(0, "00,00")
11        lblMsg_IPI.Visible = False

12        lblESTOQUE.Caption = ""

13        lblCATEGORIA.Caption = ""
14        lblQTD_MINVDA.Caption = ""
15        lblQTD_MAXVDA.Caption = ""
16        lblPESO.Caption = ""
17        lblCOD_UNIDADE.Caption = ""
18        lblPENDENTE.Caption = ""
19        txtoriginal.Text = ""

          'William Leite
'20        retornoACC = Nothing
21        minVDA_ACC = 0

22        If cboUf.Text = "DF" Then
              'frmVenda.cboDeposito.Enabled = False
23        ElseIf frmVenda.cboDeposito.ListCount = 1 Then
24            frmVenda.cboDeposito.Enabled = False
25        Else
26            frmVenda.cboDeposito.Enabled = True
27        End If


28        lblMsgTributacao.Visible = False
29        lblmsgaliq.Visible = False

30        cmdAplicacao.Enabled = False
31        SSCommand1.Enabled = False
          'inicializar preco de venda
32        Call InitFrmePrecoVenda

Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub ReInitProduto" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
35        End If
End Sub

Private Sub SelecionaPrecoVenda(lblPreco As Label, bNum_Preco As Byte)
1         On Error GoTo TrataErro

          Dim ss As Snapshot
          Dim ss2 As Snapshot
          Dim ss3 As Snapshot
          Dim ss4 As Snapshot
          Dim lngCor As Long
          Dim lngCliente As Long
          Dim response As String
          Dim coduf, codloja
          Dim IRETIDO As Double
          '********************************************************************************
          'Trata DF

2         If cboUf.Text = "DF" And Val(Left(cboDeposito, 2)) <> 9 Then

3             TpCliente = Left(tipo_cliente, 1)
4             CODUF2 = cboUf.Text

5             SQL = "select * from VENDA_LIMITADA " & _
                      "where tp_cliente = '" & TpCliente & "' and cod_uf = '" & CODUF2 & "'"

6             Set ss7 = dbAccess2.CreateSnapshot(SQL)
7             FreeLocks

8             If ss7.EOF Then
9                 Exit Sub
10            Else
11                MsgBox "Cliente do DF, pedido deve ser digitado pelo CD DF!"
12                cboDeposito.SetFocus
                  'Unload Me
13                Exit Sub
14            End If

15        End If
          '********************************************************************************

16        If txtCOD_CLIENTE.Text = "" Or txtCOD_CLIENTE.Text = "0" Then
17            MsgBox "Selecione um cliente para fazer o pedido", vbExclamation, "Aten��o"
18            lblPreco.ForeColor = lngCor
19            Screen.MousePointer = 0
20            Exit Sub
21        End If

          '///// Parte de ICMS RETIDO
22        IRETIDO = 0
23        p_cod_loja = Mid(cboDeposito.Text, 1, 2)
24        p_cod_cliente = txtCOD_CLIENTE.Text
25        p_cod_dpk = txtCOD_DPK.Text
26        p_cod_trib = bCod_Trib
27        p_preco_liquido = lblPreco

28        DoEvents
29        IRETIDO = VL_ICMRETIDO(p_cod_loja, p_cod_cliente, p_cod_dpk, p_cod_trib, p_preco_liquido, _
                  w_cod_procedencia, w_class_fiscal, w_cod_uf_destino, w_inscr_estadual, w_fl_cons_final, w_inscr_suframa, w_tp_cliente)

30        If IRETIDO <> 0 And Not CarregarICMSPreco(p_cod_dpk) Then 'TI-4405
31            MsgBox "O Valor de ICMS RETIDO � de: " & Format(IRETIDO, "#0.00")
32        End If
          '/////

33        If txtCOD_PLANO.Text = "" Or txtCOD_PLANO.Text = "0" Then
34            MsgBox "Selecione um Plano de Pagamento", 0, "Aten��o"
35            Exit Sub
36        End If

          'alterar cor do preco selecionado
37        lngCor = lblPreco.ForeColor
38        lblPreco.ForeColor = RGB(255, 0, 0)

39        If frmVenda.lblESTOQUE.Caption < 0 Then
40            MsgBox "Item com problema, ligue no Depto. Auditoria", vbExclamation, "Aten��o"
41            frmVenda.txtDescAdicional.SetFocus
42            Exit Sub
43        End If

44        If frmVenda.txtDescAdicional.Text = "" Or frmVenda.txtDescAdicional.Text = "," Or frmVenda.txtDescAdicional.Text = "." Then
45            frmVenda.txtDescAdicional.Text = "0"
46        End If


47        If CDbl(frmVenda.txtDescAdicional.Text) >= 10# And frmVenda.txtDescAdicional.Text <> "" Then
48            response = MsgBox("O desconto Adicional para este item est� correto ? " _
                      & frmVenda.txtDescAdicional.Text & "%", vbYesNo, "Aten��o")
49            If response = vbNo Then
50                lblPreco.ForeColor = lngCor
51                Exit Sub
52            End If
53        End If

          'selecionar quantidade
54        qtd = 1
55        frmQtdItem.txtQtdSolicitada.Text = ""
56        frmQtdItem.Show 1
57        qtd = 0

58        DoEvents
59        If frmQtdItem.txtQtdSolicitada.Text = "" Then
              'alterar cor do preco selecionado
60            lblPreco.ForeColor = lngCor
61            Exit Sub
62        End If

          'setar cursor
63        Screen.MousePointer = vbHourglass

64        If optPedido.Value Then
65            If txtCOD_CLIENTE.Text = "" Or txtCOD_CLIENTE.Text = "0" Then
66                MsgBox "Selecione um cliente para fazer o pedido", vbExclamation, "Aten��o"
67                lblPreco.ForeColor = lngCor
68                Screen.MousePointer = 0
69                Exit Sub
70            End If

71            SQL = "select NUM_PENDENTE,"
72            SQL = SQL & "COD_CLIENTE,"
73            SQL = SQL & "COD_UF,"
74            SQL = SQL & "COD_LOJA,"
75            SQL = SQL & "COD_PLANO,"
76            SQL = SQL & "PC_ACRESCIMO,"
77            SQL = SQL & "PC_DESCONTO,"
78            SQL = SQL & "QTD_ITENS,"
79            SQL = SQL & "FL_DIF_ICM "
80            SQL = SQL & "from PEDIDO "
81            SQL = SQL & "where FL_PEDIDO_FINALIZADO = 'N' and "
82            SQL = SQL & "COD_VEND = " & sCOD_VEND & " and "
83            If lngNUM_PEDIDO > 0 Then
84                SQL = SQL & "COD_CLIENTE = " & txtCOD_CLIENTE.Text
85                SQL = SQL & " and COD_LOJA = " & Mid(cboDeposito.Text, 1, 2)
86            ElseIf txtCOD_CLIENTE.Text = "0" Or txtCOD_CLIENTE.Text = "" Then
87                SQL = SQL & "COD_CLIENTE = 0 and "
88                SQL = SQL & "COD_UF = '" & cboUf.List(cboUf.ListIndex()) & "'"
89            Else
90                SQL = SQL & "COD_CLIENTE = " & txtCOD_CLIENTE.Text & " and "
91                SQL = SQL & "COD_UF = '" & cboUf.List(cboUf.ListIndex()) & "'"
92            End If

93            Set ss = dbAccess.CreateSnapshot(SQL)
94            FreeLocks

95            If ss.EOF And ss.BOF Then
96                If frmQtdItem.txtQtdSolicitada.Text <> "0" Then
                      'n�o ha pedido pendente
97                    Call InputPedidoCotacao(bNum_Preco)     'PARAMETRO - NUMERO DO PRECO DO ITEM
98                End If
99            Else
                  'validar condi��es do pedido
100               If txtCOD_CLIENTE.Text = "" Then
101                   lngCliente = 0
102               Else
103                   lngCliente = CLng(txtCOD_CLIENTE.Text)
104               End If
105               If lngCliente <> ss!Cod_cliente Then
106                   MsgBox "Cliente diferente do cliente do pedido" & Chr(13) & _
                              "O Cliente deve ser " & ss!Cod_cliente, vbInformation

107               ElseIf cboUf.List(cboUf.ListIndex) <> ss!cod_uf Then
108                   MsgBox "A UF deve ser " & ss!cod_uf, vbInformation

109               ElseIf txtCOD_PLANO.Text <> ss!COD_PLANO Then
110                   MsgBox "O plano deve ser " & ss!COD_PLANO, vbInformation

111               ElseIf txtPC_ACRES_FIN_DPK.Text <> ss!PC_ACRESCIMO Then
112                   MsgBox "O acrescimo do plano deve ser " & Format$(ss!PC_ACRESCIMO, "standard"), vbInformation

113               ElseIf txtPC_DESC_FIN_DPK.Text <> ss!PC_DESCONTO Then
114                   MsgBox "O desconto do plano deve ser " & Format$(ss!PC_DESCONTO, "standard"), vbInformation

115               Else
116                   Call InputItem(ss!num_pendente, ss!QTD_ITENS + 1, bNum_Preco)
                      'Numero do Pedido,Numero do Item,Numero do Preco
117               End If
118           End If
119           ss.Close

120       Else

121           If txtCOD_CLIENTE.Text = "" Or txtCOD_CLIENTE.Text = "0" Then
122               MsgBox "Selecione um cliente para fazer cota��o", vbExclamation, "Aten��o"

123           Else

124               SQL = "select NUM_COTACAO,"
125               SQL = SQL & " COD_LOJA,"
126               SQL = SQL & " COD_CLIENTE,"
127               SQL = SQL & " COD_REPRES,"
128               SQL = SQL & " COD_PLANO,"
129               SQL = SQL & " PC_ACRESCIMO,"
130               SQL = SQL & " PC_DESCONTO,"
131               SQL = SQL & " FL_DIF_ICM"
132               SQL = SQL & " from COTACAO"
133               SQL = SQL & " where SITUACAO = 8 and"
134               SQL = SQL & " COD_VEND = " & sCOD_VEND & " and"
135               SQL = SQL & " COD_CLIENTE = " & txtCOD_CLIENTE.Text
136               SQL = SQL & " and COD_LOJA = " & Mid(cboDeposito.Text, 1, 2)

137               Set ss4 = dbAccess2.CreateSnapshot(SQL)
138               FreeLocks

139               If ss4.EOF And ss4.BOF Then

140                   If frmQtdItem.txtQtdSolicitada.Text <> "0" Then
                          'n�o ha cotacao pendente
141                       Call InputPedidoCotacao(bNum_Preco)     'PARAMETRO - NUMERO DO PRECO DO ITEM
142                   End If
143               Else
144                   lngNUM_COTACAO = ss4!NUM_COTACAO
145                   lngCOD_LOJA = ss4!COD_LOJA

                      'validar condi��es da cotacao
146                   If txtCOD_CLIENTE.Text = "" Then
147                       lngCliente = 0
148                   Else
149                       lngCliente = CLng(txtCOD_CLIENTE.Text)
150                   End If
151                   If lngCliente <> ss4!Cod_cliente Then
152                       MsgBox "Cliente diferente do cliente do pedido" & Chr(13) & _
                                  "O Cliente deve ser " & ss4!Cod_cliente, vbInformation

153                   ElseIf txtCOD_PLANO.Text <> ss4!COD_PLANO Then
154                       MsgBox "O plano deve ser " & ss4!COD_PLANO, vbInformation

155                   ElseIf txtPC_ACRES_FIN_DPK.Text <> ss4!PC_ACRESCIMO Then
156                       MsgBox "O acrescimo do plano deve ser " & Format$(ss4!PC_ACRESCIMO, "standard"), vbInformation

157                   ElseIf txtPC_DESC_FIN_DPK.Text <> ss4!PC_DESCONTO Then
158                       MsgBox "O desconto do plano deve ser " & Format$(ss4!PC_DESCONTO, "standard"), vbInformation

159                   Else
160                       SQL = "select count(*) AS QTD_ITENS "
161                       SQL = SQL & " from ITEM_COTACAO "
162                       SQL = SQL & " where NUM_COTACAO = " & lngNUM_COTACAO & " and"
163                       SQL = SQL & " cod_loja = " & lngCOD_LOJA


164                       Set ss2 = dbAccess2.CreateSnapshot(SQL)
165                       FreeLocks

166                       Call InputItem(lngNUM_COTACAO, ss2!QTD_ITENS + 1, bNum_Preco)
                          'Numero da Cotacao,Numero do Item,Numero do Preco
167                       ss4.Close
168                       ss2.Close
169                   End If
170               End If
171           End If

172       End If

          'alterar cor do preco selecionado
173       lblPreco.ForeColor = lngCor

          'setar cursor
174       Screen.MousePointer = vbDefault
175       FreeLocks
176       Exit Sub

TrataErro:
177       If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
178           Label1.Visible = True
179           contador = contador + 1
180           Label1.Caption = contador
181           Resume
182       ElseIf Err = 13 Or Err = 3075 Or Err = 3319 Then
183           MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
184           Call ReInitProduto
185           Exit Sub

186       Else
187           MsgBox "Sub SelecionaPrecoVenda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
188       End If

End Sub

Private Sub ssCmdAlvo_Click()
          
    On Error GoTo Trata_Erro
          
          Dim ss7 As Object
          Dim i As Integer

1         Screen.MousePointer = 11

          'RTI-142 - Busca os dados do cliente
          If (Tipo_Usuario <> "G") Then
          
          'busca equipe de vendas
2         SQL = "Select distinct a.cod_cliente as Cod, a.nome_cliente as Razao_Social, b.nome_cidade as Cidade, "
3         SQL = SQL & "b.cod_uf as UF, g.desc_fiel as Fiel, e.desc_caracteristica as tipo, "
4         SQL = SQL & "c.dt_ult_compra as Ult_Compra, a.ddd1 as DDD, a.Fone1 as Fone, f.cod_repres as responsavel "
5         SQL = SQL & "From cliente a, cidade b, clie_credito c, datas d, "
6         SQL = SQL & "cliente_caracteristica e, r_clie_repres f, v_cliente_fiel g "
7         SQL = SQL & "Where a.situacao = 0 and (f.cod_repres in "
8         SQL = SQL & "(Select cod_repres From r_repven Where cod_vend = " & sCOD_VEND & ") "
9         SQL = SQL & "or f.cod_repres in (Select cod_repres From representante Where cod_repres = " & sCOD_VEND & ")) and "
10        SQL = SQL & "(c.dt_ult_compra >= dateadd('m',-3,D.DT_FATURAMENTO) and "
11        SQL = SQL & "c.dt_ult_compra < d.dt_fin_fech_mensal+1 ) and "
12        SQL = SQL & "g.desc_fiel in ('DIAMANTE','OURO','PLATINA','PRATA') and "
13        SQL = SQL & "a.caracteristica = e.caracteristica and a.cod_cliente = g.cod_cliente and "
14        SQL = SQL & "a.cod_cliente = f.cod_cliente and a.cod_cliente = c.cod_cliente and "
15        SQL = SQL & "a.cod_cidade = b.cod_cidade "
16        SQL = SQL & "Order by f.cod_repres,g.desc_fiel,c.dt_ult_compra"

          Else
            
              SQL = "Select distinct a.cod_cliente as Cod, a.nome_cliente as Razao_Social, b.nome_cidade as Cidade, "
              SQL = SQL & "b.cod_uf as UF, g.desc_fiel as Fiel, e.desc_caracteristica as tipo, "
              SQL = SQL & "c.dt_ult_compra as Ult_Compra, a.ddd1 as DDD, a.Fone1 as Fone, f.cod_repres as responsavel "
              SQL = SQL & "From cliente a, cidade b, clie_credito c, datas d, "
              SQL = SQL & "cliente_caracteristica e, r_clie_repres f, v_cliente_fiel g "
              SQL = SQL & "Where a.situacao = 0 and (f.cod_repres in "
              SQL = SQL & "(Select cod_repres From r_repven Where cod_vend in (Select r.cod_repres From representante r, representante g Where r.cic_gerente = g.cic and g.cod_repres = " & sCOD_VEND & ")) "
              SQL = SQL & "or f.cod_repres in (Select cod_repres From representante Where cod_repres  in (Select r.cod_repres From representante r, representante g Where r.cic_gerente = g.cic and g.cod_repres = " & sCOD_VEND & ")))  and "
              SQL = SQL & "(c.dt_ult_compra >= dateadd('m',-3,D.DT_FATURAMENTO) and "
              SQL = SQL & "c.dt_ult_compra < d.dt_fin_fech_mensal+1 ) and "
              SQL = SQL & "g.desc_fiel in ('DIAMANTE','OURO','PLATINA','PRATA') and "
              SQL = SQL & "a.caracteristica = e.caracteristica and a.cod_cliente = g.cod_cliente and "
              SQL = SQL & "a.cod_cliente = f.cod_cliente and a.cod_cliente = c.cod_cliente and "
              SQL = SQL & "a.cod_cidade = b.cod_cidade "
              SQL = SQL & "Order by f.cod_repres,g.desc_fiel,c.dt_ult_compra"
            
          End If
          
17        Set ss7 = dbAccess2.CreateSnapshot(SQL)
18        FreeLocks

19        If Not ss7.EOF And Not ss7.BOF Then
              'carrega dados
20            With frmAlvo.grdFiel
21                .Cols = 10
22                .Rows = ss7.RecordCount + 1
23                .ColWidth(0) = 600
24                .ColWidth(1) = 2800
25                .ColWidth(2) = 1500
26                .ColWidth(3) = 400
27                .ColWidth(4) = 600
28                .ColWidth(5) = 1200
29                .ColWidth(6) = 800
30                .ColWidth(7) = 1200
31                .ColWidth(8) = 1200
32                .ColWidth(9) = 1200

33                .Row = 0
34                .Col = 0
35                .Text = "COD."
36                .Col = 1
37                .Text = "CLIENTE"
38                .Col = 2
39                .Text = "CIDADE"
40                .Col = 3
41                .Text = "UF"
42                .Col = 4
43                .Text = "FIEL"
44                .Col = 5
45                .Text = "ULT.COMPRA"
46                .Col = 6
47                .Text = "DDD"
48                .Col = 7
49                .Text = "FONE"
50                .Col = 8
51                .Text = "TIPO"
52                .Col = 9
53                .Text = "RESPONSAVEL"

54                For i = 1 To .Rows - 1
55                    .Row = i

56                    .Col = 0
57                    .Text = ss7!COD
58                    .Col = 1
59                    .Text = ss7!RAZAO_SOCIAL
60                    .Col = 2
61                    .Text = ss7!cidade
62                    .Col = 3
63                    .Text = ss7!UF
64                    .Col = 4
65                    .Text = ss7!Fiel
66                    .Col = 5
67                    .Text = ss7!ult_compra
68                    .Col = 6
69                    .Text = ss7!ddd
70                    .Col = 7
71                    .Text = ss7!fone
72                    .Col = 8
73                    .Text = ss7!TIPO
74                    .Col = 9
75                    .Text = ss7!responsavel

76                    ss7.MoveNext
77                Next
78                .Row = 1
79            End With
80            Screen.MousePointer = 0
81            frmAlvo.grdFiel.Visible = True
82            frmAlvo.Show vbModal
83        Else
84            MsgBox "N�o h� cliente fiel sem compra no M�s para sua equipe de vendas!", vbInformation, "Aten��o"
85        End If

86        txtCOD_CLIENTE.SetFocus
87        Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ssCmdAlvo_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub SSCommand1_Click()
1         On Error GoTo TrataErro

          Dim ss As Object
          Dim i As Integer

2         If txtCOD_DPK = "" Then
3             MsgBox "Selecione um item, antes de verificar convers�o", vbInformation, "Aten��o"
4             Exit Sub
5         End If

6         If frmVenda.txtCOD_DPK <> "" Then
7             Screen.MousePointer = vbHourglass
8             SQL = "select b.cod_fornecedor, "
9             SQL = SQL & " b.cod_fabrica,a.cod_dpk_eq "
10            SQL = SQL & " from r_dpk_equiv a, item_cadastro b "
11            SQL = SQL & "  where"
12            SQL = SQL & " a.cod_dpk = " & frmVenda.txtCOD_DPK
13            SQL = SQL & " and a.cod_dpk_eq=b.cod_dpk "
14            SQL = SQL & " group by b.cod_fornecedor,b.cod_fabrica, a.cod_dpk_eq " & _
                          " order by b.cod_fornecedor,b.cod_fabrica, a.cod_dpk_eq"

15            Set ss = dbAccess2.CreateSnapshot(SQL)

16            If ss.EOF And ss.BOF Then
17                Screen.MousePointer = vbDefault
18                MsgBox "N�o h� convers�o cadastrada para o item escolhido", vbInformation, "Aten��o"
19                Exit Sub
20            End If

21        End If

22        frmorig.Caption = "Lista de Convers�o de Itens"
23        frmorig.Label1.Visible = False
24        frmorig.lbloriginal.Visible = False

25        With frmorig.grdoriginal
26            .Cols = 3
27            .Rows = ss.RecordCount + 1
28            .ColWidth(0) = 660
29            .ColWidth(1) = 3000
30            .ColWidth(2) = 800
31            .Row = 0
32            .Col = 0
33            .Text = "Forn."
34            .Col = 1
35            .Text = "Fabrica"
36            .Col = 2
37            .Text = "DPK"
38            ss.MoveFirst
39            For i = 1 To .Rows - 1
40                .Row = i
41                .Col = 0
42                .Text = ss!Cod_Fornecedor
43                .Col = 1
44                .Text = ss!cod_fabrica
45                .Col = 2
46                .Text = ss!cod_dpk_eq
47                ss.MoveNext
48            Next
49            .Row = 1
50        End With

51        Screen.MousePointer = vbDefault
52        frmorig.Show 1
53        If txtCOD_DPK <> "" Then
54            Call GetItem(txtCOD_DPK, -1, -1)
55            Fabrica = txtCOD_FABRICA.Text
56        End If

57        Call Visible_Cor

58        Exit Sub

TrataErro:
59        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
60            Resume
61        Else
62            MsgBox "Sub SSCommand1_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
63        End If

End Sub

Private Sub SSCommand2_Click()
              
1         On Error GoTo Trata_Erro
          
2         If txtCOD_FORNECEDOR = "" Then
3             MsgBox "Para Utilizar esta op��o selecione um fornecedor", vbExclamation, "Aten��o"
4             Exit Sub
5         Else
6             strdesc = "O"
7             If frmFabrica.Visible = True Then
8                frmFabrica.SetFocus
9             Else
10               frmFabrica.Show 1
11            End If
12            If txtResposta <> "" Then
13                txtCOD_FABRICA.Text = txtResposta
14                Call txtCOD_FABRICA_LostFocus
15            End If
16            strdesc = ""
17        End If

Trata_Erro:
18        If Err.Number <> 0 Then
19            MsgBox "Sub SSCommand2_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
20        End If

End Sub

Private Sub SSCommand3_Click()
1         On Error GoTo Trata_Erro
          
2         If txtCOD_FORNECEDOR = "" Then
3             MsgBox "Para utilizar esta op��o selecione um fornecedor", vbExclamation, "Aten��o"
4             Exit Sub
5         Else
6             strdesc = "P"
7             If frmFabrica.Visible = True Then
8                 frmFabrica.SetFocus
9             Else
10                frmFabrica.Show 1
11            End If
12            If txtResposta <> "" Then
13                txtCOD_FABRICA.Text = txtResposta
14                Call txtCOD_FABRICA_LostFocus
15            End If
16            strdesc = ""
17        End If
Trata_Erro:
18        If Err.Number <> 0 Then
19            MsgBox "Sub SSCommand3_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
20        End If
End Sub

Private Sub SSCommand4_Click()

          Dim SQL As String
          Dim i As Long

1         Screen.MousePointer = 11

2         On Error GoTo Trata_Erro

3         SQL = "Select a.cod_vend, b.pseudonimo, " & _
                  "       sum(iif(tipo_ligacao='1',1,0)) as ativa, " & _
                  "       sum(iif(tipo_ligacao='2',1,0)) as receptiva, " & _
                  "       (sum(iif(tipo_ligacao='1',1,0)) + sum(iif(tipo_ligacao='2',1,0))) as total " & _
                  "From contato a, representante b, datas c " & _
                  "Where cdate(a.dt_contato) >= c.dt_faturamento and " & _
                  "      cdate(a.dt_contato) < c.dt_faturamento + 1 and a.cod_vend = b.cod_repres " & _
                  "Group by a.cod_vend,b.pseudonimo " & _
                  "Order by 5 desc"

4         Set ss1 = dbAccess2.CreateSnapshot(SQL)

5         If ss1.EOF Then
6             MsgBox "N�o h� dados para a consulta", vbInformation, "Aten��o"
7             Screen.MousePointer = 0
8             Exit Sub
9         Else
10            ss1.MoveLast
11            ss1.MoveFirst

12            With frmRank.Grid1
13                .Cols = 5
14                .Rows = ss1.RecordCount + 1
15                .ColWidth(0) = 500
16                .ColWidth(1) = 1200
17                .ColWidth(2) = 900
18                .ColWidth(3) = 1000
19                .ColWidth(4) = 900

20                .Row = 0
21                .Col = 0
22                .Text = "RANK."
23                .Col = 1
24                .Text = "TLMKT"
25                .Col = 2
26                .Text = "  ATIVA"
27                .Col = 3
28                .Text = "RECEPTIVA"
29                .Col = 4
30                .Text = "  TOTAL"

31                ss1.MoveFirst
32                For i = 1 To ss1.RecordCount
33                    .Row = i

34                    .Col = 0
                      '.ColAlignment(0) = 1
35                    .Text = i
36                    .Col = 1
37                    .ColAlignment(1) = 0
38                    .Text = ss1!pseudonimo
39                    .Col = 2
40                    .ColAlignment(2) = 1
41                    .Text = ss1!Ativa
42                    .Col = 3
43                    .ColAlignment(3) = 1
44                    .Text = ss1!Receptiva
45                    .Col = 4
46                    .ColAlignment(4) = 1
47                    .Text = ss1!total

48                    ss1.MoveNext
49                Next
50                .Row = 1
51            End With
52            frmRank.Grid1.Visible = True
53            Screen.MousePointer = 0
54            frmRank.Show 1
55        End If
56        Exit Sub

Trata_Erro:
57        If Err = 30009 Then
58            Resume Next
59        ElseIf Err.Number <> 0 Then
60            MsgBox "Sub SSCommand4_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
61        End If

End Sub

Private Sub SSCommand6_Click()
    frmLig.Show 1
End Sub

Private Sub Taxa_Aproveitamento()

1         On Error GoTo Trata_Erro

2         If CLng(IIf(frmVenda.txtLig_Total = "", 0, frmVenda.txtLig_Total)) <> 0 And IIf(frmVenda.txtTotal_Pedidos = "", 0, frmVenda.txtTotal_Pedidos) <> 0 Then
              'C�lculo da Taxa de Convers�o
3             frmVenda.lblTxConversao = Format(CStr((CLng(frmVenda.txtLig_Total.Text) / _
                      CLng(frmVenda.txtTotal_Pedidos.Text))), "0")

              'C�lculo do Aproveitamento
4             frmVenda.lblAproveitamento = Format(CStr((CLng(frmVenda.txtTotal_Pedidos.Text) / _
                      CLng(frmVenda.txtLig_Total.Text) * 100)), "0") & "%"
5         Else
6             frmVenda.lblTxConversao = "0"
7             frmVenda.lblAproveitamento = "0%"
8         End If

9         frmVenda.lblTxConversao.Visible = True
10        frmVenda.lblAproveitamento.Visible = True
                
Trata_Erro:
11        If Err.Number <> 0 Then
12            MsgBox "Sub Taxa_Aproveitamento" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
13        End If

End Sub


Private Sub txtCGC_Change()

    If txtCGC.Text = "" Then
        Call InitCliente
    End If

End Sub

Private Sub txtCGC_GotFocus()
    txtCGC.DataChanged = False
End Sub

Private Sub txtCGC_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
        txtCOD_DPK.SetFocus
    End If

End Sub

Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCGC_LostFocus()
    If txtCGC.DataChanged Then
        If txtCGC.Text = "0" Or txtCGC.Text = "" Then
            Call InitVenda
        ElseIf Not GetCliente(0, txtCGC.Text) Then
            txtCGC.SetFocus
        End If
    End If
End Sub

Private Sub txtCOD_CLIENTE_Change()
    'optCotacao.Value = True
    'fil = 0
    'txtRepres = ""
    'txtVend = ""
    txtMensagem = ""
    If txtCOD_CLIENTE = "" Then
        Call InitCliente
        Call txtCOD_CLIENTE_LostFocus
    End If

End Sub

Private Sub txtCOD_CLIENTE_DblClick()
    frmClienteFimPedido.Show vbModal
    If txtResposta <> "0" Then
        txtCOD_CLIENTE.Text = txtResposta
        txtCOD_CLIENTE.DataChanged = True
        Call txtCOD_CLIENTE_LostFocus
        txtRepres.SetFocus
    End If
End Sub

Private Sub txtCOD_CLIENTE_GotFocus()

    cboDeposito.Enabled = True
    fl_existe_pedido = "N"
    fl_existe_cotacao = "N"
End Sub

Private Sub txtCOD_CLIENTE_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
        txtCGC.SetFocus
    End If
End Sub

Private Sub txtCOD_CLIENTE_KeyPress(KeyAscii As Integer)

    KeyAscii = Numerico(KeyAscii)

End Sub

Public Sub txtCOD_CLIENTE_LostFocus()
    
    On Error GoTo Trata_Erro
    
    Dim tam As Integer
    Dim i As Integer
    cmdQuadrante.Visible = False
    cmdHistoricoVisita.Visible = False
    
    If frmVenda.txtCOD_CLIENTE.DataChanged Then
        lblDescSuframa.Caption = 0
        lblIS = ""
        If txtCOD_CLIENTE.Text = "0" Or txtCOD_CLIENTE.Text = "" Then
            'inicializa objetos do frame cliente
            Call GetListaUf                 ' carregar lista de uf - cboUF
            cboUf.Enabled = True
            cboFilial.Enabled = True
            tam = cboUf.ListCount
            For i = 0 To tam - 1
                If cboUf.List(i) = uf_origem Then
                    Exit For
                End If
            Next i
            cboUf.ListIndex = -1
            cboUf.ListIndex = i
            Call InitCliente

        ElseIf Not GetCliente(txtCOD_CLIENTE.Text, 0) Then
            txtCOD_CLIENTE.DataChanged = False
        End If

        Call GetDifIcm(CalcItem, bdif_ICMS)
        Call CalculaPrecos(BuscaDescTab)


        'carregar tabela Venda_Limitada
        Dim CODUF2 As String
        Dim coduf As String
        Dim codloja As Integer

        TpCliente = Left(tipo_cliente, 1)
        CODUF2 = cboUf.Text

        SQL = "select * from VENDA_LIMITADA " & _
                "where tp_cliente = '" & TpCliente & "' and cod_uf = '" & CODUF2 & "'"

        Set ss7 = dbAccess2.CreateSnapshot(SQL)
        FreeLocks

        If ss7.EOF Then
            'cboDeposito.SetFocus
            Exit Sub
        Else
            coduf = ss7!cod_uf
            codloja = ss7!cod_loja_obrigatoria

            SQL = "select cod_loja, nome_fantasia " & _
                    "from loja " & _
                    "where cod_loja = " & codloja & ""

            Set ss7 = dbAccess2.CreateSnapshot(SQL)
            FreeLocks

            If Not ss7.EOF Then
                cboDeposito.Text = Format(ss7!COD_LOJA, "00") & "-" & ss7!NOME_FANTASIA
                'cboDeposito.Enabled = False
            Else
                MsgBox "Cliente do DF, pedido deve ser digitado pelo CD DF!"
                Unload Me
                frmVenda.Show
            End If
        End If
        'cboDeposito.SetFocus
    End If
Trata_Erro:
    If Err.Number <> 0 Then
  MsgBox "Sub txtCod_Cliente_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub txtCOD_DPK_Change()
    txtDescAdicional.Text = "0"
    chkSemanaPassada.Value = 0
End Sub

Private Sub txtCOD_DPK_GotFocus()
    txtCOD_DPK.DataChanged = False
End Sub

Private Sub txtCOD_DPK_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
    End If
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Public Sub txtCOD_DPK_LostFocus()

    On Error GoTo Trata_Erro

1         If txtCOD_DPK.DataChanged Or cboDeposito.DataChanged Then
2             If txtCOD_DPK.Text = "0" Or txtCOD_DPK.Text = "" Then
                  'Inicializar frame
3                 Call InitFrmeProduto
4             Else
                  'carrega dados do item
5                 Call GetItem(txtCOD_DPK.Text, -1, -1)
6                 Fabrica = txtCOD_FABRICA.Text
7                 Unload frmFabrica
8                 frmFabrica.cboMontadora.Clear
9                 frmFabrica.cboGrupo.Clear
10                frmFabrica.cboSubgrupo.Clear
11            End If
12            Call Visible_Cor
13        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub txtCod_DPK_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub TXTCOD_FABRICA_Change()

    'txtDescAdicional.Text = "0"
    chkSemanaPassada.Value = 0

End Sub

Private Sub TXTCOD_FABRICA_DblClick()

1         On Error GoTo Trata_Erro

2         txtCOD_FABRICA.DataChanged = False
3         txtResposta = txtCOD_FORNECEDOR
4         If frmFabrica.Visible = True Then
5            frmFabrica.SetFocus
6         Else
7            frmFabrica.Show 1
8         End If
9         If txtResposta <> "" Then
10            txtCOD_FABRICA.Text = txtResposta
11            Call txtCOD_FABRICA_LostFocus
12        End If

Trata_Erro:
13        If Err.Number <> 0 Then
14            MsgBox "Sub txtCod_fabrica_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
15        End If

End Sub

Private Sub txtCOD_FABRICA_GotFocus()
    txtCOD_FABRICA.DataChanged = False
End Sub

Private Sub TXTCOD_FABRICA_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
    End If


End Sub

Private Sub txtCOD_FABRICA_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub

Public Sub txtCOD_FABRICA_LostFocus()
1         On Error GoTo TrataErro

2         If txtCOD_FABRICA.Text = "" Then
              'Inicializar frame
              'Call InitFrmeProduto
3             txtDescricao.Enabled = True
4         Else
5             If txtCOD_FABRICA.DataChanged Then
                  'carrega dados do item
6                 pesq = "*"
7                 If InStr(1, frmVenda.txtCOD_FABRICA, pesq) > 0 And frmVenda.txtCOD_FABRICA.Text <> "" Then
8                     If frmFabrica.Visible = True Then
9                        frmFabrica.SetFocus
10                    Else
11                       frmFabrica.Show 1
12                    End If
13                    If InStr(1, frmVenda.txtCOD_FABRICA, pesq) = 0 And frmVenda.txtCOD_FABRICA.Text <> "" Then
14                        Call GetItem(-1, IIf(txtCOD_FORNECEDOR = "", 0, txtCOD_FORNECEDOR), txtCOD_FABRICA.Text)
15                        Fabrica = txtCOD_FABRICA.Text
16                        txtCOD_FABRICA.Enabled = True
17                        If qtd <> 1 Then
                              'frmVenda.txtDescAdicional.Text = ""
18                            frmVenda.txtDescAdicional.SetFocus
19                        End If
20                    Else
21                        Exit Sub
22                    End If
23                Else
24                    Call GetItem(-1, IIf(txtCOD_FORNECEDOR = "", -1, txtCOD_FORNECEDOR), txtCOD_FABRICA.Text)
25                    Fabrica = txtCOD_FABRICA.Text
26                    If qtd <> 1 Then
27                       If frmVenda.txtDescAdicional.Enabled = True Then frmVenda.txtDescAdicional.SetFocus
                          'frmVenda.txtDescAdicional.Text = ""
28                    End If
29                End If
30            End If
31            Call Visible_Cor
32        End If
33        Exit Sub

TrataErro:
34        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
35            Label1.Visible = True
36            contador = contador + 1
37            Label1.Caption = contador
38            Resume
39        ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
40            MsgBox "Selecione o item novamente", vbExclamation, "Aten��o"
41            txtCOD_FABRICA.Text = ""
42            Exit Sub
43        Else
44            MsgBox "Sub txtCod_Fabrica_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
45        End If

End Sub

Private Sub TXTCOD_FORNECEDOR_Change()

1         On Error GoTo Trata_Erro

2         If txtCOD_DPK.Text = "" Then
3             If Len(txtCOD_FORNECEDOR.Text) = 3 Then
4                 Call txtCOD_FORNECEDOR_LostFocus
5                 frmVenda.txtCOD_FABRICA.SetFocus
6             End If
7         End If
Trata_Erro:
8         If Err.Number = 5 Then
9             Resume Next
10        ElseIf Err.Number <> 0 Then
11            MsgBox "Sub TxtCod_Fornecedor_Change" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description
12        End If

End Sub

Private Sub TXTCOD_FORNECEDOR_Click()

    If frmQtdItem.Visible = False Then
        If qtd = 0 Then
            txtCOD_FORNECEDOR.Text = ""
            Unload frmFabrica
            Call InitFrmeProduto
            chkSemanaPassada.Value = 0
        End If
    End If

End Sub

Private Sub txtCOD_FORNECEDOR_DblClick()
    qtd = 0
    txtResposta = "0"
    frmFornecedor.Show vbModal
    If txtResposta <> "0" Then
        txtCOD_FORNECEDOR.Text = txtResposta
        txtCOD_FORNECEDOR.DataChanged = True
        Call txtCOD_FORNECEDOR_LostFocus
    End If
End Sub

Private Sub txtCOD_FORNECEDOR_GotFocus()
    txtCOD_FORNECEDOR.DataChanged = False
End Sub

Private Sub TXTCOD_FORNECEDOR_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
    End If

End Sub

Private Sub txtCOD_FORNECEDOR_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCOD_FORNECEDOR_LostFocus()
1         On Error GoTo TrataErro

2         If txtCOD_DPK.Text = "" Then
3             If qtd = 0 Then
4                 If txtCOD_FORNECEDOR.DataChanged Then
5                     If txtCOD_FORNECEDOR.Text = "0" Or txtCOD_FORNECEDOR.Text = "" Then
                          'Inicializar frame
6                         Call InitFrmeProduto
7                     Else
8                         Call GetFornecedor(txtCOD_FORNECEDOR.Text)
                          'Foi comentada esta linha abaixo por ocorrer o erro 5. Foi colocado no click do grdFabrica
9                         If strdesc = "N" Then
10                            Unload frmFabrica
11                            Set frmFabrica = Nothing
12                            frmFabrica.cboMontadora.Clear
13                            frmFabrica.cboGrupo.Clear
14                            frmFabrica.cboSubgrupo.Clear
15                        End If
16                    End If
17                End If
18            End If
19        End If
20        Exit Sub

TrataErro:
21        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
22            Label1.Visible = True
23            contador = contador + 1
24            Label1.Caption = contador
25            Resume
26        ElseIf Err = 3075 Or Err = 3319 Or Err = 13 Then
27            MsgBox "Selecione o fornecedor novamente", vbExclamation, "Aten��o"
28            txtCOD_FORNECEDOR.Text = ""
29            Exit Sub
30        Else
31            MsgBox "Sub txtCod_Fornecedor_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
32        End If

End Sub

Private Sub txtCOD_PLANO_DblClick()
    txtResposta = "0"
    frmPlano.Show vbModal
    If txtResposta <> "0" Then
        txtCOD_PLANO.Text = txtResposta
        Call txtCOD_PLANO_LostFocus
    End If
End Sub

Private Sub txtCOD_PLANO_GotFocus()
    txtCOD_PLANO.DataChanged = False
End Sub

Private Sub txtCOD_PLANO_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
    End If

End Sub

Private Sub txtCOD_PLANO_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCOD_PLANO_LostFocus()

    On Error GoTo Trata_Erro

1         If txtCOD_PLANO.Text = "" Then
2             MsgBox "Selecione um Plano de Pagamento", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         If frmPlano.grdPlano.Visible = False Then
6             If txtCOD_PLANO.Text = "" And frmPlano.grdPlano.Visible = False Then
7                 MsgBox "Selecione um Plano de Pagamento", vbInformation, "ATEN��O"
8                 txtCOD_PLANO.SetFocus
9                 Exit Sub
10            Else
11                If txtCOD_PLANO.DataChanged And txtCOD_PLANO.Text <> "" Then
12                    If txtCOD_PLANO.Text = "0" Or txtCOD_PLANO.Text = "" Then
13                        Call InitFrmePlano
14                        MsgBox "Selecione um Plano de Pagamento", vbInformation, "ATEN��O"
15                        txtCOD_PLANO.SetFocus
16                        Exit Sub
17                    ElseIf GetPlanoPgto(txtCOD_PLANO.Text, -1, -1) Then
                          'efetua calculo de pre�o
18                        Call CalculaPrecos(NaoBuscaDescTab)
19                    Else
20                        txtCOD_PLANO.SetFocus
21                    End If
22                End If
23            End If
24        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand4_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub txtDescAdicional_Click()
    'txtDescAdicional.Text = ""
End Sub

Private Sub txtDescAdicional_GotFocus()
    txtDescAdicional.SelStart = 0
    txtDescAdicional.SelLength = Len(txtDescAdicional)
End Sub

Private Sub txtDescAdicional_KeyPress(KeyAscii As Integer)
    If KeyAscii = 8 Then Exit Sub
    
    If InStr(1, ".-0123456789", Chr(KeyAscii)) = 0 Then KeyAscii = 0
    If InStr(1, txtDescAdicional, "-") > 0 And Chr(KeyAscii) = "-" Then KeyAscii = 0
    If InStr(1, txtDescAdicional, ".") > 0 And Chr(KeyAscii) = "." Then KeyAscii = 0
    
End Sub

Private Sub txtDescAdicional_LostFocus()
          Dim ss2
1         On Error GoTo TrataErro
          'validar campo
2         If txtDescAdicional.Text = "," Or txtDescAdicional.Text = "." Then
3             txtDescAdicional.Text = ""
4         End If
5         txtDescAdicional.Text = FmtBR(txtDescAdicional.Text)
6         If txtDescAdicional.DataChanged Then
              'aqui
7             If txtDescAdicional.Text = "" Or txtDescAdicional.Text = "0" Then
8                 txtDescAdicional.Text = "0"
9             ElseIf CDbl(txtDescAdicional.Text) > "99.99" Or _
                          CDbl(txtDescAdicional.Text) < "-99.99" Then
10                MsgBox "Deve estar entre -99 e 99", vbInformation, "Desconto Adicional"
11                txtDescAdicional.Text = "0"
'COMENTADO POR EDUARDO EM - 26/02/2007
'12            Else
'13                If txtCOD_FORNECEDOR = "" Then
'14                    txtCOD_FORNECEDOR = 0
'15                End If
'16                sql = "Select fl_dif_icms,"
'17                sql = sql & " fl_adicional,"
'18                sql = sql & " tp_dif_icms"
'19                sql = sql & " From fornecedor_especifico"
'20                sql = sql & " where cod_dpk in (0," & txtCOD_DPK & " ) and"
'21                sql = sql & " cod_subgrupo in (0," & item_subgrupo & ") and"
'22                sql = sql & " cod_grupo in (0," & item_grupo & ") and"
'23                sql = sql & " cod_fornecedor = " & txtCOD_FORNECEDOR
'24                sql = sql & " ORDER BY COD_DPK DESC, COD_SUBGRUPO DESC, COD_GRUPO DESC, COD_FORNECEDOR"
'
'25                Set ss2 = dbAccess2.CreateSnapshot(sql)
'26                FreeLocks
'
'27                If ss2.EOF Or ss2.BOF Then
'28                    strFl_Dif_Icms = "S"
'29                    strFl_Adicional = "S"
'30                Else
'31                    If ss2.fl_adicional = "N" And txtDescAdicional > 0 Then
'32                        txtDescAdicional = 0
'33                        MsgBox "Este Fornecedor N�O Permite Desconto Adicional!!", vbInformation, "ATEN��O!"
'34                        txtDescAdicional = 0
'35                    End If
'36                    strFl_Dif_Icms = ss2!fl_dif_icms
'37                    strFl_Adicional = ss2!fl_adicional
'38                End If

39            End If
              
              
13                If txtCOD_FORNECEDOR = "" Then
14                    txtCOD_FORNECEDOR = 0
15                End If
16                SQL = "Select fl_dif_icms,"
17                SQL = SQL & " fl_adicional,"
18                SQL = SQL & " tp_dif_icms"
19                SQL = SQL & " From fornecedor_especifico"
20                SQL = SQL & " where cod_dpk in (0," & txtCOD_DPK & " ) and"
21                SQL = SQL & " cod_subgrupo in (0," & item_subgrupo & ") and"
22                SQL = SQL & " cod_grupo in (0," & item_grupo & ") and"
23                SQL = SQL & " cod_fornecedor = " & txtCOD_FORNECEDOR
24                SQL = SQL & " ORDER BY COD_DPK DESC, COD_SUBGRUPO DESC, COD_GRUPO DESC, COD_FORNECEDOR"

25                Set ss2 = dbAccess2.CreateSnapshot(SQL)
26                FreeLocks

27                If ss2.EOF Or ss2.BOF Then
28                    strFl_Dif_Icms = "S"
29                    strFl_Adicional = "S"
30                Else
31                    If ss2.fl_adicional = "N" And txtDescAdicional > 0 Then
32                        txtDescAdicional = 0
33                        MsgBox "Este Fornecedor N�O Permite Desconto Adicional!!", vbInformation, "ATEN��O!"
34                        txtDescAdicional = 0
35                    End If
36                    strFl_Dif_Icms = ss2!fl_dif_icms
37                    strFl_Adicional = ss2!fl_adicional
38                End If
              
              'efetua calculo de pre�o
40            Call CalculaPrecos(NaoBuscaDescTab)
41            Call cboDeposito_GotFocus

42        End If
43        Exit Sub

TrataErro:
44        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
45            Resume
46        ElseIf Err = 13 Then
47            txtDescAdicional = 0
48            Exit Sub
49        Else
50            MsgBox "Sub txtDescAdicional_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
51        End If

End Sub

Private Sub txtDescricao_KeyDown(KeyCode As Integer, Shift As Integer)

    If KeyCode = Key_F2 Then
        Call Cliente_Ligacao
    End If

End Sub

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtDescricao_LostFocus()
    
1         On Error GoTo Trata_Erro

2         If txtDescricao.DataChanged Then
3             If txtDescricao.Text = "" And txtCOD_FABRICA.Text = "" Then
                  'Inicializar frame
                  'Call InitFrmeProduto
4             Else
                  'carrega dados do item
5                 pesq = "*"
6                 If InStr(1, frmVenda.txtDescricao, pesq) > 0 Then

7                     If frmFabrica.Visible = True Then
8                        frmFabrica.SetFocus
9                     Else
10                       frmFabrica.Show 1
11                    End If
12                    If txtResposta <> "" Then
13                        txtCOD_FABRICA.Text = txtResposta
14                        Call txtCOD_FABRICA_LostFocus
15                    End If
16                End If
17            End If
18            Call Visible_Cor
19        End If

Trata_Erro:
20        If Err.Number <> 0 Then
21      MsgBox "Sub TxtDescricao_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If


End Sub

Private Sub txtoriginal_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
    If Asc(KeyAscii) = 51 Then
        KeyAscii = 0
        Beep
    End If

End Sub

Private Sub txtoriginal_LostFocus()
1         On Error GoTo TrataErro
          Dim ss As Object
          Dim i As Integer

2         If frmVenda.txtoriginal <> "" Then
              'mouse
3             Screen.MousePointer = vbHourglass

              'montar SQL
4             SQL = " Select cod_Fornecedor, "
5             SQL = SQL & " b.cod_fabrica,a.cod_dpk "
6             SQL = SQL & " from aplicacao a, item_cadastro b "
7             SQL = SQL & " where "
8             SQL = SQL & " a.cod_original like '*" & frmVenda.txtoriginal & "*'"
9             SQL = SQL & " and a.cod_dpk=b.cod_dpk "
10            SQL = SQL & " group by b.cod_fornecedor, b.cod_fabrica, a.cod_dpk order by b.cod_fornecedor, b.cod_fabrica, a.cod_dpk"

              'criar consulta
              'oradatabase.Parameters.Remove "original"
              'oradatabase.Parameters.Add "original", frmVenda.txtoriginal, 1

11            Set ss = dbAccess2.CreateSnapshot(SQL)

12            If ss.EOF And ss.BOF Then
13                Screen.MousePointer = vbDefault
14                MsgBox "N�o ha item cadastrado para este nr. original", vbInformation, "Aten��o"
15                frmVenda.txtoriginal = ""
16                Exit Sub
17            End If

              'carrega dados
18            frmorig.lbloriginal = frmVenda.txtoriginal
19            With frmorig.grdoriginal
20                .Cols = 3
21                .Rows = ss.RecordCount + 1
22                .ColWidth(0) = 660
23                .ColWidth(1) = 3000
24                .ColWidth(2) = 800
25                .Row = 0
26                .Col = 0
27                .Text = "Forn."
28                .Col = 1
29                .Text = "Fabrica"
30                .Col = 2
31                .Text = "DPK"
32                ss.MoveFirst
33                For i = 1 To .Rows - 1
34                    .Row = i
35                    .Col = 0
36                    .Text = ss!Cod_Fornecedor
37                    .Col = 1
38                    .Text = ss!cod_fabrica
39                    .Col = 2
40                    .Text = ss!cod_dpk
41                    ss.MoveNext
42                Next
43                .Row = 1
44            End With

45            Screen.MousePointer = vbDefault

46            frmorig.Show 1
47            If txtoriginal.Text <> "" And txtCOD_DPK.Text <> "" Then
48                Call GetItem(txtCOD_DPK.Text, -1, -1)
49                Fabrica = txtCOD_FABRICA.Text
50            End If
51            Call Visible_Cor
52        End If

53        Exit Sub

TrataErro:
54        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
55            Resume
56        ElseIf Err = 364 Or Err = 400 Then
57            Resume Next
58        Else
            MsgBox "Sub txtOriginal_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
59        End If


End Sub

Private Sub txtPC_ACRES_FIN_DPK_GotFocus()
    txtPC_ACRES_FIN_DPK.DataChanged = False
End Sub

Private Sub txtPC_ACRES_FIN_DPK_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtPC_ACRES_FIN_DPK.Text)
End Sub

Private Sub txtPC_ACRES_FIN_DPK_LostFocus()

    If txtPC_ACRES_FIN_DPK = "" Or txtPC_ACRES_FIN_DPK = "," Then
        txtPC_ACRES_FIN_DPK = 0
    End If

    'validar entrada
    If txtPC_ACRES_FIN_DPK.DataChanged Then
        If CDbl(txtPC_ACRES_FIN_DPK.Text) < sPC_ACRESCIMO_PLANO Then
            MsgBox "Acrescimo deve ser maior ou igual a " & CStr(sPC_ACRESCIMO_PLANO), vbInformation, "Aten��o"
            txtPC_ACRES_FIN_DPK.Text = CStr(sPC_ACRESCIMO_PLANO)
            txtPC_ACRES_FIN_DPK.DataChanged = False
        End If
        'calcula preco de venda
        Call CalculaPrecos(NaoBuscaDescTab)
    End If
End Sub

Private Sub txtPC_DESC_FIN_DPK_GotFocus()
    txtPC_DESC_FIN_DPK.DataChanged = False
End Sub

Private Sub txtPC_DESC_FIN_DPK_KeyPress(KeyAscii As Integer)
    KeyAscii = Valor(KeyAscii, txtPC_DESC_FIN_DPK.Text)
End Sub

Private Sub txtPC_DESC_FIN_DPK_LostFocus()

1         On Error GoTo TrataErro

2         If txtPC_DESC_FIN_DPK = "" Or txtPC_DESC_FIN_DPK = "," Or txtPC_DESC_FIN_DPK = ",0" Or txtPC_DESC_FIN_DPK = "." Then
3             txtPC_DESC_FIN_DPK = 0
4         End If
          'validar entrada
5         If CDbl(txtPC_DESC_FIN_DPK.Text) > 99 Then
6             MsgBox "O Desconto deve ser menor que 100%", vbInformation, "Aten��o"
7             txtPC_DESC_FIN_DPK.Text = CStr(sPC_DESCONTO_PLANO)
8             txtPC_DESC_FIN_DPK.DataChanged = False
              'calcula preco de venda
9             Call CalculaPrecos(NaoBuscaDescTab)
10        ElseIf txtPC_DESC_FIN_DPK.DataChanged Then
11            If sPC_DESCONTO_PLANO = 0 Then
12                MsgBox "O Desconto deve ser igual a zero", vbInformation, "Aten��o"
13                txtPC_DESC_FIN_DPK.Text = CStr(sPC_DESCONTO_PLANO)
14                txtPC_DESC_FIN_DPK.DataChanged = False
15            ElseIf CLng(txtPC_DESC_FIN_DPK.Text) > sPC_DESCONTO_PLANO Then
16                MsgBox "Desconto deve ser menor ou igual a " & CStr(sPC_DESCONTO_PLANO), vbInformation, "Aten��o"
17                txtPC_DESC_FIN_DPK.Text = CStr(sPC_DESCONTO_PLANO)
18                txtPC_DESC_FIN_DPK.DataChanged = False
19            End If
              'calcula preco de venda
20            Call CalculaPrecos(NaoBuscaDescTab)
21        End If

22        Exit Sub

TrataErro:
23        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
24            Resume
25        ElseIf Err = 13 Then
26            txtPC_DESC_FIN_DPK.Text = 0
27            Exit Sub
28        Else
29            MsgBox "Sub txtPC_DESC_FIN_DPK_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If


End Sub

Private Sub txtRepres_GotFocus()

    On Error GoTo Trata_Erro

1         If txtCOD_CLIENTE <> "" Then

              '************************************
              'MENSAGEM DO CLIENTE

2             SQL = " SELECT A.DESC_MENS "
3             SQL = SQL & " FROM CLIE_MENSAGEM A, CLIENTE B "
4             SQL = SQL & " WHERE A.COD_MENSAGEM=B.COD_MENSAGEM AND "
5             SQL = SQL & " A.FL_BLOQUEIO = 'S' AND "
6             SQL = SQL & " B.COD_CLIENTE = " & txtCOD_CLIENTE & ""

7             Set ss8 = dbAccess2.CreateSnapshot(SQL)
8             FreeLocks

9             If Not ss8.EOF Then
10                txtMensagem.Text = ss8!desc_mens
11            Else
12                txtMensagem.Text = ""
13            End If
              '************************************

              '*******
              'ALERTAS

              '************************************
              'DUPLICATAS VENCIDAS A MAIS DE 5 DIAS

14            SQL = " SELECT COUNT(*) as TOTAL "
15            SQL = SQL & " FROM DUPLICATAS, DATAS "
16            SQL = SQL & " WHERE SITUACAO_PAGTO = 0 AND "
17            SQL = SQL & " CDATE(DT_VENCIMENTO) < (DT_FATURAMENTO - 5) AND "
18            SQL = SQL & " COD_CLIENTE = " & txtCOD_CLIENTE & ""

19            Set ss8 = dbAccess2.CreateSnapshot(SQL)
20            FreeLocks

21            If ss8!total > 0 Then
22                MsgBox "Cliente com DUPLICATA VENCIDA A MAIS DE 5 DIAS. VERIFIQUE", vbExclamation, "                                               ATEN��O!!!!"
23            End If
              '************************************

              '*********************************************
              'CLIENTE INATIVO, SEM COMPRA A MAIS DE 6 MESES

24            SQL = " SELECT COUNT(*) as TOTAL "
25            SQL = SQL & " FROM CLIE_CREDITO A, DATAS B"
26            SQL = SQL & " WHERE A.DT_ULT_COMPRA < dateadd('m',-6,B.DT_FATURAMENTO) AND "
27            SQL = SQL & " COD_CLIENTE = " & txtCOD_CLIENTE & ""

28            Set ss8 = dbAccess2.CreateSnapshot(SQL)
29            FreeLocks

30            If ss8!total > 0 Then
31                MsgBox "CLIENTE INATIVO, Sem Compra a Mais de 6 Meses. VERIFIQUE", vbExclamation, "                                                ATEN��O!!!!"
32            End If
              '*********************************************
33        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub txtRepres_GotFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Public Function VL_ICMRETIDO(p_cod_loja As Integer, p_cod_cliente As Long, p_cod_dpk As Long, p_cod_trib As Integer, p_preco_liquido As Double, Optional w_cod_procedencia As Integer, Optional w_class_fiscal As Double, Optional w_cod_uf_destino As String, Optional w_inscr_estadual As String, Optional w_fl_cons_final As String, Optional w_inscr_suframa As String, Optional w_tp_cliente As String) As Double
          
10        On Error GoTo TrataErro

          Dim vUfDestino As String
          Dim vUfOrigem As String
          Dim vCodCidadeDestino As Integer
          Dim vCodCidadeOrigem As Integer
          Dim flg_regime_pe As Boolean 'TI-3030
          Dim w_vl_cuema As Double 'TI-3030
          Dim w_vl_fator As Double 'TI-3030
          Dim w_fl_red_icms As String 'TI-4405
          Dim w_vl_aliq_red As Double 'TI-4405
          
          'Eduardo - 18/08/05
          Dim SELECT0
          Dim SELECT1
          Dim SELECT2
          Dim SELECT3
          Dim SELECT4
          Dim SELECT5
          Dim SELECT6
          Dim SELECT7
          Dim SELECT8
          Dim SELECT9 'TI-3030
          Dim SELECT10 'TI-3030
          Dim ss9
          
          'inicializa vari�veis
20        w_cod_uf_origem = ""
30        w_vl_baseicm1 = 0
40        w_vl_baseicm2 = 0
50        w_vl_basemaj_1 = 0
60        w_vl_basemaj_3 = 0
70        w_cliente_antec = ""
80        w_cod_mensagem = 0
90        w_cod_mensagem_fiscal = 0
100       w_pc_mg_lucro_antec = 0
110       w_pc_margem_lucro = 0
120       w_class_antec = 0
130       w_class_antec_RJ = 0
140       w_vl_baseicm1_ret = 0
150       w_vl_baseicm2_ret = 0
160       w_vl_base_1 = 0
170       w_vl_base_1_aux = 0
180       w_vl_base_2 = 0
190       w_vl_base_2_aux = 0
200       w_vl_base_3 = 0
210       w_vl_baseisen = 0
220       w_vl_baseisen_aux = 0
230       w_vl_baseisen_aux1 = 0
240       w_vl_base_5 = 0
250       w_vl_base_6 = 0
260       w_vl_baseoutr = 0
270       w_pc_aliq_interna = 0
280       w_pc_aliq_interna_me = 0
290       w_pc_icm = 0
300       w_vl_icmretido = 0
310       w_vl_cuema = 0 'TI-3030
320       w_vl_fator = 0 'TI-3030
321       w_vl_aliq_red = 0 'TI-4405

          'inicializa retorno da funcao
330       VL_ICMRETIDO = 0

          'Eduardo - 18/08/05
          'W_COD_PROCEDENCIA
          'W_CLASS_FISCAL
   ' Query testada - tudo ok
340       SELECT0 = " Select cod_procedencia, class_fiscal From item_cadastro Where cod_dpk = " & p_cod_dpk
350       Set SELECT0 = dbAccess2.CreateSnapshot(SELECT0)
360       If SELECT0.EOF = False And SELECT0.BOF = False Then
370           w_cod_procedencia = SELECT0!cod_procedencia
380           w_class_fiscal = SELECT0!class_fiscal
390       Else
400           w_cod_procedencia = 0
410           w_class_fiscal = 0
420       End If
                
          'monta SQL
          'W_COD_UF_ORIGEM
430       SELECT1 = "Select a.cod_uf from cidade a, loja b where a.cod_cidade = b.cod_cidade and b.cod_loja= " & p_cod_loja
440       Set ss9 = dbAccess2.CreateSnapshot(SELECT1)
450       FreeLocks
460       w_cod_uf_origem = ss9!cod_uf

          'Eduardo - 18/08/05
          'W_COD_UF_DESTINO
          'W_INSCR_ESTADUAL
          'W_FL_CONS_FINAL
          'W_INSCR_SUFRAMA
470       SELECT0 = "Select a.cod_uf as COD_UF, b.inscr_estadual as Inscr_Estadual, b.fl_cons_final as fl_Cons_Final, b.cod_mensagem as cod_mensagem, " & _
                      " b.cod_mensagem_fiscal as cod_mensagem_fiscal, b.inscr_suframa as inscr_Suframa " & _
                      " from cidade a, cliente b " & _
                      " where a.cod_cidade = b.cod_cidade and b.cod_cliente = " & p_cod_cliente
480       Set SELECT0 = dbAccess2.CreateSnapshot(SELECT0)
490       FreeLocks
500       If SELECT0.EOF = False And SELECT0.BOF = False Then
510           w_cod_uf_destino = SELECT0!cod_uf
520           w_inscr_estadual = SELECT0!inscr_estadual
530           w_fl_cons_final = SELECT0!fl_cons_final
540           w_inscr_suframa = IIf(IsNull(SELECT0!inscr_suframa), "0", SELECT0!inscr_suframa)
550       Else
560           w_cod_uf_destino = ""
570           w_inscr_estadual = ""
580           w_fl_cons_final = ""
590           w_inscr_suframa = "0"
600       End If

          'W_COD_MENSAGEM, W_COD_MENSAGEM_FISCAL
610       SELECT2 = "Select cod_mensagem, cod_mensagem_fiscal From cliente where cod_cliente = " & p_cod_cliente
620       Set ss9 = dbAccess2.CreateSnapshot(SELECT2)
630       FreeLocks
640       If w_inscr_estadual = "ISENTO" Then
650           w_tp_cliente = "I"
660       ElseIf w_fl_cons_final = "S" Then
670           w_tp_cliente = "C"
680       Else
690           w_tp_cliente = "R"
700       End If
710       w_cod_mensagem = IIf(IsNull(ss9!cod_mensagem), 0, ss9!cod_mensagem)
720       w_cod_mensagem_fiscal = IIf(IsNull(ss9!cod_mensagem_fiscal), 0, ss9!cod_mensagem_fiscal)
              


730       If (w_tp_cliente = "R" Or w_tp_cliente = "C") And w_cod_mensagem_fiscal <> 53 Then
              'W_PC_MG_LUCRO_ANTEC
              
              'Pega cidade e uf destino e cidade e uf origem para trazer a margem de lucro
              'para o calculo de icms retido.
              'No select anterior estava demorando a consulta.
                    
              'Pega Uf_Destino
740           SELECT3 = "SELECT COD_UF, COD_CIDADE FROM CIDADE WHERE COD_CIDADE = (SELECT COD_CIDADE FROM CLIENTE WHERE COD_CLIENTE= " & p_cod_cliente & ")"
750           Set ss9 = dbAccess2.CreateSnapshot(SELECT3)
760           FreeLocks
770           If ss9.EOF = False Then
780               vUfDestino = ss9!cod_uf
790               vCodCidadeDestino = ss9!cod_cidade
800           End If
              
              'Pega Uf_Origem
810           SELECT3 = "SELECT COD_UF, COD_CIDADE FROM CIDADE WHERE COD_CIDADE = (SELECT COD_CIDADE FROM LOJA WHERE COD_LOJA = " & p_cod_loja & ")"
820           Set ss9 = dbAccess2.CreateSnapshot(SELECT3)
830           FreeLocks
840           If ss9.EOF = False Then
850               vUfOrigem = ss9!cod_uf
860               vCodCidadeOrigem = ss9!cod_cidade
870           End If
                    
880           SELECT3 = " SELECT PC_MARGEM_LUCRO FROM CLASS_ANTEC_ENTRADA, " & _
                          " CLIENTE, LOJA, ITEM_CADASTRO " & _
                          " WHERE CLIENTE.COD_CLIENTE = " & p_cod_cliente & " AND " & _
                          " LOJA.COD_LOJA = " & p_cod_loja & " AND " & _
                          " ITEM_CADASTRO.COD_DPK = " & p_cod_dpk & " AND " & _
                          " CLASS_ANTEC_ENTRADA.CLASS_FISCAL = ITEM_CADASTRO.CLASS_FISCAL AND " & _
                          " CLASS_ANTEC_ENTRADA.COD_UF_ORIGEM = '" & vUfOrigem & "' AND " & _
                          " LOJA.COD_CIDADE = " & vCodCidadeOrigem & " AND " & _
                          " CLASS_ANTEC_ENTRADA.COD_UF_DESTINO = '" & vUfDestino & "' AND " & _
                          " CLIENTE.COD_CIDADE = " & vCodCidadeDestino
              
              'COMENTADO DIA 11/04/2007 - POR LUCIANO CARLOS DE JESUS
              ''Eduardo - 16/08/05
              'SELECT3 = "Select pc_margem_lucro " & _
              '" from CLASS_ANTEC_ENTRADA a," & _
              '" cliente                b," & _
              '" cidade                 c," & _
              '" loja                   d," & _
              '" cidade                 e," & _
              '" item_cadastro f "

              ''Luciano - 28/02/2008
              'SELECT3 = SELECT3 & " where b.cod_cliente = " & p_cod_cliente & " and " & _
              '" d.cod_loja = " & p_cod_loja & " and " & _
              '" f.cod_dpk = " & p_cod_dpk & " and " & _
              '" a.class_fiscal = f.class_fiscal and " & _
              '" a.cod_uf_origem = e.cod_uf and " & _
              '" d.cod_cidade = e.cod_cidade and " & _
              '" a.cod_uf_destino = c.cod_uf and " & _
              '" b.cod_cidade = c.cod_cidade and " & _
              '" c.cod_cidade = e.cod_cidade "


890           Set ss9 = dbAccess2.CreateSnapshot(SELECT3)
900           FreeLocks
          
910           If ss9.EOF Then
920               w_pc_mg_lucro_antec = 0
930               w_cliente_antec = "N"
940           Else
950               w_pc_mg_lucro_antec = ss9!PC_MARGEM_LUCRO
960               w_cliente_antec = "S"
970           End If
980       End If

          'W_CLASS_ANTEC
990       SELECT4 = "Select count(*) as count " & _
                      "From class_antec_entrada " & _
                      "Where class_fiscal = " & w_class_fiscal & " and cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                      "cod_uf_destino = '" & w_cod_uf_destino & "' "
          
1000      Set ss9 = dbAccess2.CreateSnapshot(SELECT4)
1010      FreeLocks
1020      If ss9.EOF Then
1030          w_class_antec = 0
1040      Else
1050          w_class_antec = ss9!Count
1060      End If

          'W_PC_MARGEM_LUCRO
1070      If p_cod_trib = 1 Or p_cod_trib = 3 Then
1080          If w_fl_cons_final = "S" Then
1090              w_pc_margem_lucro = 0
1100          Else
1110              SELECT5 = "Select pc_margem_lucro,fl_red_icms " & _
                              "From subst_tributaria " & _
                              "Where cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                              "cod_uf_destino = '" & IIf(Val(w_inscr_suframa) = 0, w_cod_uf_destino, "SU") & "' and " & _
                              "class_fiscal = " & w_class_fiscal
1120              Set ss9 = dbAccess2.CreateSnapshot(SELECT5)
1130              FreeLocks
1140              If ss9.EOF Then
1150                  w_pc_margem_lucro = 0
1151                  w_fl_red_icms = "N" 'TI-4405
1160              Else
1170                  w_pc_margem_lucro = ss9!PC_MARGEM_LUCRO
1171                  w_fl_red_icms = IIf(IsNull(ss9!fl_red_icms), "N", ss9!fl_red_icms) 'TI-4405
1180              End If
1190          End If
1200      ElseIf (p_cod_trib = 0 Or p_cod_trib = 8) And w_class_antec <> 0 And w_cliente_antec = "S" Then
1210          w_pc_margem_lucro = w_pc_mg_lucro_antec
1220      End If
          
          'W_PC_ALIQ_INTERNA
'1121       SELECT6 = "Select pc_icm " & _
'                      "From uf_origem_destino " & _
'                      "Where cod_uf_origem = '" & w_cod_uf_destino & "' and " & _
'                      "cod_uf_destino = '" & w_cod_uf_destino & "' "
'
'1122       Set ss9 = dbAccess2.CreateSnapshot(SELECT6)
'1123       FreeLocks
'1124       w_pc_aliq_interna = ss9!pc_icm

          'Consultor30 28/05/2009
1230      w_pc_aliq_interna = AliquotaInterna(w_cod_uf_origem, w_cod_uf_destino, p_cod_dpk)
            
          'W_PC_ALIQ_INTERNA_ME
1240      If w_cod_uf_origem = "GO" And w_cod_uf_destino = "GO" Then
1250          SELECT7 = "Select pc_icm " & _
                          "From aliquota_me a, cliente b " & _
                          "Where b.cod_cliente = " & p_cod_cliente & " and " & _
                          "a.cod_loja = " & p_cod_loja & " and " & _
                          "a.cgc = b.cgc"
          
1260          Set ss9 = dbAccess2.CreateSnapshot(SELECT7)
1270          FreeLocks
1280          If ss9.EOF Then
1290              w_pc_aliq_interna_me = 0
1300          Else
1310              w_pc_aliq_interna_me = ss9!pc_icm
1320          End If
1330          If w_pc_aliq_interna_me <> 0 Then
1340              w_pc_aliq_interna = w_pc_aliq_interna_me
1350          End If
1360      End If

          'W_PC_ICM
1370      SELECT8 = "Select pc_icm " & _
                      "From uf_origem_destino " & _
                      "Where cod_uf_origem = '" & w_cod_uf_origem & "' and " & _
                      "cod_uf_destino = '" & IIf(Val(w_inscr_suframa) = 0, w_cod_uf_destino, "SU") & "' "
          
1380      Set ss9 = dbAccess2.CreateSnapshot(SELECT8)
1390      FreeLocks
1400      w_pc_icm = ss9!pc_icm
             
          'w_vl_cuema TI-3030
1410      SELECT9 = "Select cuema " & _
                    "From item_estoque " & _
                    "Where cod_loja = " & p_cod_loja & " and " & _
                    "cod_dpk = " & p_cod_dpk & " and " & _
                    "situacao = 0"
          
1420      Set ss9 = dbAccess2.CreateSnapshot(SELECT9)
1430      FreeLocks
1440      If Not ss9.BOF And Not ss9.EOF Then
1450        w_vl_cuema = ss9!cuema
1460      End If
1470      If w_tp_cliente = "R" And w_cod_uf_origem = "PE" And w_cod_uf_destino = "PE" Then
1480        flg_regime_pe = GetClassFiscal(w_class_fiscal, w_vl_fator)
1490      End If

          'W_COD_TRIB_REVENDEDOR, W_COD_TRIB_INSCRITO, W_COD_TRIB_ISENTO
1500      If p_cod_trib = 1 Or p_cod_trib = 8 Then
1510          SELECT10 = "SELECT B.COD_TRIB_REVENDEDOR, B.COD_TRIB_INSCRITO, B.COD_TRIB_ISENTO " & _
                         "FROM ITEM_CADASTRO A, SUBST_TRIBUTARIA B " & _
                         "Where a.cod_dpk = " & p_cod_dpk & " " & _
                         "AND B.COD_UF_ORIGEM = '" & w_cod_uf_origem & "' " & _
                         "AND B.COD_UF_DESTINO = '" & w_cod_uf_destino & "' " & _
                         "AND A.CLASS_FISCAL = B.CLASS_FISCAL"
                        
1520          Set ss9 = dbAccess2.CreateSnapshot(SELECT10)
1530          FreeLocks
1540          If Not ss9.BOF And Not ss9.EOF Then
1550              If w_tp_cliente = "I" Then
1560                  p_cod_trib = ss9!cod_trib_isento
1570              ElseIf w_tp_cliente = "S" Then
1580                  p_cod_trib = ss9!Cod_trib_inscrito
1590              ElseIf w_tp_cliente = "R" Then
1600                  p_cod_trib = ss9!Cod_trib_Revendedor
1610              End If
1620          End If
1630      End If
          'FIM TI-3030
          
          'TI-4405
1631      If w_tp_cliente = "R" And w_fl_red_icms = "S" Then
1632        w_vl_aliq_red = CarregarAliqReduzida(p_cod_loja)
1633      End If
          'FIM TI-4405
          
1640      If (p_cod_trib = 0 And w_cod_procedencia = 0) Or _
          (p_cod_trib = 0 And ((w_cod_procedencia = 1 Or w_cod_procedencia = 2) And p_cod_loja <> 3)) Then
1650          w_vl_baseicm1 = p_preco_liquido
          
1660          If w_cliente_antec = "S" And w_class_antec <> 0 Then
1670              w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
1680              w_vl_baseicm1_ret = p_preco_liquido
1690          End If
          
1700      ElseIf p_cod_trib = 1 Then
              '-- Tratamento especial para Goiania base reduzida 24/03/00
1710          If (w_tp_cliente = "R" And w_cod_uf_origem = "GO" And w_cod_uf_destino = "GO") Then
1720              w_vl_base_1_aux = p_preco_liquido
                  '-- Redu��o
1730              w_vl_baseisen_aux1 = w_vl_base_1_aux
1740              w_vl_base_1 = (w_vl_base_1_aux * 10) / 17
1750              w_vl_baseisen = (w_vl_base_1_aux - (w_vl_base_1_aux * 10) / 17)
1760          Else
1770              w_vl_base_1 = p_preco_liquido
1780          End If
          
1790          If w_pc_margem_lucro = 0 And w_cod_uf_origem = "RJ" And w_cod_uf_destino = "RJ" Then
1800              w_pc_margem_lucro = 20
1810          End If
1820          w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
          
              'TI-3030
1830          If flg_regime_pe = True And w_tp_cliente = "R" And w_cod_uf_origem = "PE" And w_cod_uf_destino = "PE" Then
1840            w_vl_base_1 = w_vl_cuema * (1 + w_pc_margem_lucro / 100)
1850            w_vl_basemaj_1 = w_vl_cuema * (1 + w_vl_fator / 100)
1860          End If
              'FIM TI-3030
            
              'TI-3934
1870          If w_vl_aliq_red > 0 Then
1880            w_vl_base_1 = w_vl_base_1 * (w_vl_aliq_red / 100)
1890          End If
              'FIM TI-3934
              
1900      ElseIf p_cod_trib = 2 Then
1910          w_vl_base_2_aux = p_preco_liquido
              '-- Redu��o base colocado para tratar Goiania
1920          w_vl_baseisen_aux = w_vl_base_2_aux
1930          w_vl_base_2 = (w_vl_base_2_aux * 10) / 17
1940          w_vl_baseisen = (w_vl_base_2_aux - (w_vl_base_2_aux * 10) / 17)
1950      ElseIf p_cod_trib = 3 Then
1960          w_vl_base_3 = p_preco_liquido
1970          w_vl_basemaj_3 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
1980      ElseIf p_cod_trib = 4 Then
1990          w_vl_baseisen = p_preco_liquido
2000      ElseIf p_cod_trib = 5 Then
2010          w_vl_base_5 = p_preco_liquido
2020      ElseIf p_cod_trib = 6 Then
2030          w_vl_base_6 = p_preco_liquido
2040      ElseIf (p_cod_trib = 8 Or ((w_cod_procedencia = 1 Or w_cod_procedencia = 2) And p_cod_loja = 3)) Then
2050          w_vl_baseicm2 = p_preco_liquido
2060          If w_cliente_antec = "S" And w_class_antec <> 0 Then
2070              w_vl_basemaj_1 = p_preco_liquido * (1 + w_pc_margem_lucro / 100)
2080              w_vl_baseicm2_ret = p_preco_liquido
2090          End If
2100      ElseIf p_cod_trib = 9 Then
2110          w_vl_baseoutr = p_preco_liquido
2120      End If

2130      If w_cliente_antec = "N" Then
2140          w_vl_icmretido = ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) - _
              (w_vl_base_1 * w_pc_icm / 100)) + _
              (w_vl_basemaj_3 * w_pc_aliq_interna / 100)
2150      Else
2160          w_vl_icmretido = ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) - _
              ((w_vl_baseicm1_ret * w_pc_icm / 100) + _
              (w_vl_baseicm2_ret * w_pc_icm / 100) + _
              (w_vl_base_1 * w_pc_icm / 100))) + _
              (w_vl_basemaj_3 * w_pc_aliq_interna / 100)
          
2170      End If

          'TI-3030
2180      If flg_regime_pe = True And w_tp_cliente = "R" And w_cod_uf_origem = "PE" And w_cod_uf_destino = "PE" Then
2190           w_vl_icmretido = (w_vl_base_1 * w_pc_aliq_interna / 100) - (w_vl_basemaj_1 * w_pc_aliq_interna / 100)
2200      End If
          'FIM TI-3030
              
2210      VL_ICMRETIDO = Format(w_vl_icmretido, "#0.00")
2220  Exit Function

TrataErro:
2230      If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
2240          Resume
2250      Else
2260          MsgBox "Sub VL_ICMRETIDO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
2270      End If
End Function
'Atualizar a flag na tabela PEDIDO ou COTACAO do banco VENDAS.
Sub Ajustar_Flag_ICMS()
    Dim vRst As Object
    
    '******
    'Pedido
    '******
    If optPedido.Value = True Then
       SQL = "select NUM_PENDENTE, COD_CLIENTE, COD_UF, COD_LOJA, COD_PLANO "
       SQL = SQL & "from PEDIDO "
       SQL = SQL & "where FL_PEDIDO_FINALIZADO = 'N' and "
       SQL = SQL & "COD_VEND = " & sCOD_VEND & " and "
       If lngNUM_PEDIDO > 0 Then
            SQL = SQL & "COD_CLIENTE = " & frmVenda.txtCOD_CLIENTE.Text
            SQL = SQL & " and COD_LOJA = " & Val(frmVenda.cboDeposito.Text)
       ElseIf frmVenda.txtCOD_CLIENTE.Text = "0" Or frmVenda.txtCOD_CLIENTE.Text = "" Then
            SQL = SQL & "COD_CLIENTE = 0 and "
            SQL = SQL & "COD_UF = '" & frmVenda.cboUf.List(frmVenda.cboUf.ListIndex()) & "'"
       Else
            SQL = SQL & "COD_CLIENTE = " & frmVenda.txtCOD_CLIENTE.Text & " and "
            SQL = SQL & "COD_UF = '" & frmVenda.cboUf.List(frmVenda.cboUf.ListIndex()) & "'"
       End If
   
       Set vRst = dbAccess.CreateSnapshot(SQL)
       FreeLocks
       
       If vRst.EOF = False And vRst.BOF = False Then
            SQL = "UPDATE PEDIDO SET FL_DIF_ICM = '" & IIf(chkDifIcm.Value = vbChecked, "S", "N") & "'"
            SQL = SQL & " WHERE NUM_PENDENTE = " & vRst("NUM_PENDENTE")
            dbAccess.Execute SQL
       End If
   Else
        '*******
        'COTACAO
        '*******
        SQL = "select NUM_COTACAO from COTACAO where "
        SQL = SQL & " COD_CLIENTE = " & txtCOD_CLIENTE & " and"
        SQL = SQL & " COD_LOJA = " & Val(cboDeposito) & " And "
        SQL = SQL & " COD_VEND = " & sCOD_VEND & " And "
        SQL = SQL & " FL_TIPO = 0 and SITUACAO = 8"
    
        Set vRst = dbAccess2.CreateDynaset(SQL)
    
        If vRst.EOF = False And vRst.BOF = False Then
            
            SQL = "UPDATE COTACAO SET FL_DIF_ICM = '" & IIf(chkDifIcm.Value = vbChecked, "S", "N") & "' WHERE NUM_COTACAO=" & lngNUM_COTACAO & " and COD_LOJA=" & Val(cboDeposito)
            
            dbAccess2.Execute SQL
        End If
    End If
    
    vRst.Close
    Set vRst = Nothing
    
End Sub

'TI-3030
Public Function GetClassFiscal(p_class_fiscal As Double, p_vl_fator As Double) As Boolean
1         On Error GoTo TrataErro

          'inicializa retorno
2         GetClassFiscal = False
          
          Dim loja As String, class As String, Valor As String, lngArq As Integer, TextLine As String
          
3         If Dir(Path_drv & "\Dados\NCM_REGIME_ESPECIAL_PE.TXT") <> "" Then
4             strPath = Path_drv & "\Dados\NCM_REGIME_ESPECIAL_PE.TXT"
5         Else
6             GetClassFiscal = False
7             p_vl_fator = 0
8             Exit Function
9         End If
        
10        lngArq = FreeFile
          
11        Open strPath For Input Shared As #lngArq
12        Do While Not EOF(lngArq)
13            Line Input #lngArq, TextLine
14            class = Mid(TextLine, 1, 10)
15            Valor = Mid(TextLine, 12, 5)
16            If Val(class) = p_class_fiscal Then
17                p_vl_fator = CDbl(Valor)
18                GetClassFiscal = True
19                Exit Function
20            End If
21        Loop
22        Close #lngArq

23        Exit Function

TrataErro:
24        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
25            Resume
26        ElseIf Err.Number = 3420 Then
27            Resume Next
28        Else
29            MsgBox "Function GetClassFiscal" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If

End Function
'FIM TI-3030
'TI-3952
Private Sub cmdMIX_Click()

    If lngNUM_MIX > 0 Then
        frmMIXItem.Show 1
    Else
        frmMIXProduto.Show 1
    End If

End Sub
'FIM TI-3952
'TI-4405
Private Function CarregarAliqReduzida(pCodLoja As Integer) As Double
1         On Error GoTo TrataErro

          'inicializa retorno
2         CarregarAliqReduzida = 0
          
          Dim lngArq As Integer, TextLine As String, Vetor(0 To 2) As String
          
3         If Dir(Path_drv & "\Dados\PARAM_REGIME.TXT") <> "" Then
4             strPath = Path_drv & "\Dados\PARAM_REGIME.TXT"
5         Else
6             CarregarAliqReduzida = 0
7             Exit Function
8         End If

9         lngArq = FreeFile
          
10        Open strPath For Input Shared As #lngArq
11        Do While Not EOF(lngArq)
12            Line Input #lngArq, TextLine
13            ParseToArray TextLine, Vetor()
14            If Vetor(1) = "VL_ALIQ_RED_LOJA_" & Right("00" & pCodLoja, 2) Then
15                CarregarAliqReduzida = Vetor(2)
16                Exit Function
17            End If
18        Loop
19        Close #lngArq

20        Exit Function

TrataErro:
21        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
22            Resume
23        ElseIf Err.Number = 3420 Then
24            Resume Next
25        Else
26            MsgBox "Function CarregarAliqReduzida" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If
End Function
Sub ParseToArray(sLine As String, A() As String)

Dim P As Long, LastPos As Long, i As Long

    P = InStr(sLine, ",")
    
    Do While P
        A(i) = Mid$(sLine, LastPos + 1, P - LastPos - 1)
        LastPos = P
        i = i + 1
        P = InStr(LastPos + 1, sLine, ",", vbBinaryCompare)
    Loop
    
    A(i) = Mid$(sLine, LastPos + 1)

End Sub
Public Function CarregarICMSPreco(pCodDPK As Long) As Boolean
1         On Error GoTo TrataErro

          'inicializa retorno
2         CarregarICMSPreco = False
          
          Dim w_vl_fator As Double, rs As Snapshot
          
3         If uf_origem = "PE" And Trim(frmVenda.cboUf.Text) = "PE" Then
          
4             Set rs = dbAccess2.CreateSnapshot("Select class_fiscal From item_cadastro Where cod_dpk=" & pCodDPK)
5             FreeLocks
6             If Not rs.BOF And Not rs.EOF Then
7               CarregarICMSPreco = GetClassFiscal(rs!class_fiscal, w_vl_fator)
8             End If

9         End If

10        Exit Function

TrataErro:
11        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
12            Resume
13        ElseIf Err.Number = 3420 Then
14            Resume Next
15        Else
16            MsgBox "Function CarregarICMSPreco" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
17        End If
End Function
'FIM TI-4405

