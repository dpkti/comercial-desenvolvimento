VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmCesta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PRODUTOS EM DESTAQUE"
   ClientHeight    =   5430
   ClientLeft      =   1155
   ClientTop       =   1890
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5430
   ScaleWidth      =   7215
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame ssfraPesq 
      Caption         =   "Pesquisa"
      Height          =   585
      Left            =   120
      TabIndex        =   13
      Top             =   4740
      Width           =   6915
      Begin VB.TextBox txtPesquisa 
         Appearance      =   0  'Flat
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   60
         TabIndex        =   14
         Top             =   225
         Width           =   3720
      End
      Begin Bot�o.cmd sscmdDesc 
         Height          =   375
         Left            =   5880
         TabIndex        =   15
         Top             =   120
         Width           =   945
         _ExtentX        =   1667
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Descri��o"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCesta.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.TextBox txtVeiculo 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2475
      MaxLength       =   20
      TabIndex        =   9
      Top             =   810
      Width           =   2370
   End
   Begin VB.Frame framCesta 
      Caption         =   "SEGMENTO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   585
      Left            =   120
      TabIndex        =   0
      Top             =   90
      Width           =   3495
      Begin VB.OptionButton optAcessorios 
         Appearance      =   0  'Flat
         Caption         =   "Acess�rios"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2250
         TabIndex        =   3
         Top             =   270
         Width           =   1185
      End
      Begin VB.OptionButton optFrotas 
         Appearance      =   0  'Flat
         Caption         =   "Frotas"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1200
         TabIndex        =   2
         Top             =   270
         Width           =   855
      End
      Begin VB.OptionButton optPecas 
         Appearance      =   0  'Flat
         Caption         =   "Pe�as"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   150
         TabIndex        =   1
         Top             =   270
         Width           =   915
      End
   End
   Begin MSGrid.Grid grdItem 
      Height          =   3135
      Left            =   120
      TabIndex        =   7
      Top             =   1170
      Visible         =   0   'False
      Width           =   6975
      _Version        =   65536
      _ExtentX        =   12303
      _ExtentY        =   5530
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "frmCesta.frx":001C
   End
   Begin Bot�o.cmd cmdBuscar 
      Height          =   585
      Left            =   3720
      TabIndex        =   11
      Top             =   120
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCesta.frx":0038
      PICN            =   "frmCesta.frx":0054
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd sscmdSair 
      Height          =   585
      Left            =   6540
      TabIndex        =   12
      ToolTipText     =   "Sair"
      Top             =   60
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCesta.frx":0D2E
      PICN            =   "frmCesta.frx":0D4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "VE�CULO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1395
      TabIndex        =   10
      Top             =   855
      Width           =   810
   End
   Begin VB.Label lblObs 
      AutoSize        =   -1  'True
      Caption         =   "Itens Cadastrados no �ltimo m�s com estoque dispon�vel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   135
      TabIndex        =   8
      Top             =   4500
      Visible         =   0   'False
      Width           =   4845
   End
   Begin VB.Label lblSIGLA 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "WWWWWWWWWW"
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2430
      TabIndex        =   6
      Top             =   810
      Width           =   2355
   End
   Begin VB.Label lblCod_fornecedor 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1710
      TabIndex        =   5
      Top             =   855
      Width           =   615
   End
   Begin VB.Label lblForn 
      AutoSize        =   -1  'True
      Caption         =   "FORNECEDOR:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   4
      Top             =   855
      Width           =   1425
   End
End
Attribute VB_Name = "frmCesta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bTipo_Cesta As Byte

'VARIAVEIS UTILIZADAS PARA PESQUISA
Public CONTINUAPESQUISA As Integer
Public OCORRENCIA As Integer
Public CONTADOROCORRENCIA As Long
Public TEXTOPARAPROCURA As String
Public INICIOPESQUISA As Long
Public INILINHA As Long
Public tot_grdfabr As Long

Private Sub cmdBuscar_Click()

    On Error GoTo Trata_Erro

1         If strTipo_Tela = "V" And txtVeiculo = "" Then
2             MsgBox "Digite um ve�culo, para efetuar a consulta"
3             Exit Sub
4         End If

5         If bTipo_Cesta = 0 Then
6             MsgBox "Selecione um segmento para fazer a busca", vbInformation, "Aten��o"
7             Exit Sub
8         End If

9         If strTipo_Tela = "V" And txtVeiculo <> "" Then
10            Screen.MousePointer = 11
11            Call Carrega_Kit(txtVeiculo)
12            ssfraPesq.Visible = True
13            Screen.MousePointer = 0
14        Else
15            ssfraPesq.Visible = False
16            If bTipo_Cesta = 0 Then
17                MsgBox "Selecione um segmento para fazer a busca", vbInformation, "Aten��o"
18                Exit Sub
19            End If
20            If Verifica_Cesta(bTipo_Cesta) Then
21                Screen.MousePointer = 11
22                Call Carrega_Itens(bTipo_Cesta)
23                Screen.MousePointer = 0
24            Else
25                MsgBox "N�o h� itens em destaque para este segmento", vbInformation, "Aten��o"
26                Exit Sub
27            End If
28        End If

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub cmdBuscar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub


Private Sub Form_Load()
    
    On Error GoTo Trata_Erro
    
    If strTipo_Tela = "D" Then
        frmCesta.Caption = "ITENS EM DESTAQUE"
        framCesta.Visible = True
        lblCod_fornecedor.Visible = True
        lblForn.Visible = True
        lblSIGLA.Visible = True
        cmdBuscar.Visible = True
        sscmdSair.Visible = True
        lblObs.Visible = False
        Label1.Visible = False
        txtVeiculo.Visible = False

        bTipo_Cesta = 0
        If frmVenda.txtCOD_FORNECEDOR <> "" Then
            frmCesta.lblForn.Visible = False
            frmCesta.lblSIGLA.Visible = False
            frmCesta.lblCod_fornecedor.Visible = False

            frmCesta.lblCod_fornecedor = frmVenda.txtCOD_FORNECEDOR
            frmCesta.lblSIGLA = frmVenda.lblSIGLA
            DoEvents
        Else
            frmCesta.lblForn.Visible = False
            frmCesta.lblSIGLA.Visible = False
            frmCesta.lblCod_fornecedor.Visible = False

            frmCesta.lblCod_fornecedor = ""
            frmCesta.lblSIGLA = ""
            DoEvents
        End If

    ElseIf strTipo_Tela = "N" Then
        frmCesta.Caption = "ITENS NOVOS"
        framCesta.Visible = False
        lblCod_fornecedor.Visible = False
        lblForn.Visible = False
        lblSIGLA.Visible = False
        cmdBuscar.Visible = False
        sscmdSair.Visible = False
        lblObs.Visible = True
        Label1.Visible = False
        txtVeiculo.Visible = False

        Screen.MousePointer = 11

        sql = "Select b.cod_fornecedor,c.sigla,b.cod_fabrica,b.desc_item,b.cod_dpk " & _
                "from item_estoque a, item_cadastro b, fornecedor c, datas d " & _
                "Where (a.qtd_atual - a.qtd_pendente - a.qtd_reserv) > 0 and " & _
                "b.dt_cadastramento >= DateAdd('m',-1,d.dt_faturamento) and " & _
                "b.dt_cadastramento < d.dt_faturamento - 1 and " & _
                "b.cod_fornecedor = c.cod_fornecedor and " & _
                "a.cod_loja = " & Mid(Trim(frmVenda.cboDeposito), 1, 2) & " and " & _
                "a.cod_dpk = b.cod_dpk " & _
                "Order by b.cod_fornecedor,b.cod_fabrica "

        Set ss = dbAccess2.CreateSnapshot(sql)

        Screen.MousePointer = 0

        If ss.EOF And ss.BOF Then
            MsgBox "N�o h� itens novos"
        Else
            With frmCesta.grdItem
                .Cols = 5
                .Rows = ss.RecordCount + 1
                .ColWidth(0) = 660
                .ColWidth(1) = 1500
                .ColWidth(2) = 1500
                .ColWidth(3) = 2500
                .ColWidth(4) = 800

                .Row = 0
                .Col = 0
                .Text = "Cod"
                .Col = 1
                .Text = "Forn"
                .Col = 2
                .Text = "Fabrica"
                .Col = 3
                .Text = "Descri��o"
                .Col = 4
                .Text = "DPK"

                ss.MoveFirst
                For i = 1 To .Rows - 1
                    .Row = i

                    .Col = 0
                    .Text = ss!cod_fornecedor
                    .Col = 1
                    .Text = ss!Sigla
                    .Col = 2
                    .Text = ss!cod_fabrica
                    .Col = 3
                    .Text = ss!desc_item
                    .Col = 4
                    .Text = ss!cod_dpk

                    ss.MoveNext
                Next
                .Row = 1
            End With
            frmCesta.grdItem.Visible = True
        End If

    ElseIf strTipo_Tela = "V" Then
        frmCesta.Caption = "KIT POR VEICULO"
        framCesta.Visible = False
        lblCod_fornecedor.Visible = False
        lblForn.Visible = False
        lblSIGLA.Visible = False
        cmdBuscar.Visible = True
        sscmdSair.Visible = True
        lblObs.Visible = False
        Label1.Visible = True
        txtVeiculo.Visible = True

        bTipo_Cesta = 9

    End If

    Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub Form_Load FrmCesta" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub grdItem_DblClick()
    grdItem.Col = 4
    frmVenda.txtCOD_DPK = grdItem.Text
    Unload Me
End Sub


Private Sub optAcessorios_Click()
    grdItem.Visible = False
    bTipo_Cesta = 3
End Sub

Private Sub optFrotas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 2
End Sub

Private Sub optPecas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 1
End Sub



Public Function Verifica_Cesta(tipo_cesta As Byte) As Boolean
    
    On Error GoTo Trata_Erro
    
    Dim ss As Object

    sql = "Select dt_vigencia"
    sql = sql & " From cesta_venda"
    sql = sql & " Where tp_cesta = " & tipo_cesta
    sql = sql & " and cod_loja = " & Mid(Trim(frmVenda.cboDeposito), 1, 2)

    Set ss = dbAccess2.CreateSnapshot(sql)

    If (ss.BOF And ss.EOF) Or IsNull(ss!dt_vigencia) Then
        Verifica_Cesta = False
    Else
        If ss!dt_vigencia > Format(Data_Faturamento, "YYYYMMDD") Then
            Verifica_Cesta = False
        Else
            Verifica_Cesta = True
        End If
    End If

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub Verifica_Cesta" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Sub Carrega_Kit(strVeiculo As String)
    
    On Error GoTo Trata_Erro
    
    Dim ss As Object
    Dim i As Long
    Dim strFabr As String
    Dim bAltura As Byte

    tot_grdfabr = 0

    Screen.MousePointer = 11

    'strVeiculo = "*" & strVeiculo & "*"

    sql = "Select b.cod_fornecedor,c.sigla, b.cod_fabrica, " & _
            "b.desc_item , b.cod_dpk, e.desc_aplicacao " & _
            "from  item_estoque a, item_cadastro b,fornecedor c, datas d, aplicacao e " & _
            "Where a.situacao in (0,8) and b.cod_fornecedor = c.cod_fornecedor and " & _
            "a.cod_loja = " & Mid(Trim(frmVenda.cboDeposito), 1, 2) & " and " & _
            "e.desc_aplicacao like '" & strVeiculo & "' AND " & _
            "b.cod_dpk = e.cod_dpk and a.cod_dpk = b.cod_dpk " & _
            "Order by B.COD_DPK,B.DESC_ITEM,B.COD_FORNECEDOR "

    Set ss = dbAccess2.CreateSnapshot(sql)

    Screen.MousePointer = 0

    tot_grdfabr = ss.RecordCount
    If ss.EOF And ss.BOF Then
        MsgBox "N�o h� itens para este ve�culo"
        Screen.MousePointer = 0
    Else
        With frmCesta.grdItem
            .Cols = 6
            '.Rows = ss.RecordCount + 1
            .Rows = 1
            .ColWidth(0) = 660
            .ColWidth(1) = 1500
            .ColWidth(2) = 1500
            .ColWidth(3) = 2500
            .ColWidth(4) = 800
            .ColWidth(5) = 3870

            .Row = 0
            .Col = 0
            .Text = "Cod"
            .Col = 1
            .Text = "Forn"
            .Col = 2
            .Text = "Fabrica"
            .Col = 3
            .Text = "Descri��o"
            .Col = 4
            .Text = "DPK"
            .Col = 5
            .Text = "APLICA��O"

            ss.MoveFirst
            strFabr = ""

            For i = 1 To ss.RecordCount
                'cria linha
                If strFabr <> ss!cod_fabrica Then
                    'cria linha
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = ss!cod_fornecedor
                    .Col = 1
                    .Text = ss!Sigla
                    .Col = 2
                    .Text = ss!cod_fabrica
                    .Col = 3
                    .Text = ss!desc_item
                    .Col = 4
                    .Text = ss!cod_dpk
                    .Col = 5
                    If IsNull(ss!desc_aplicacao) Then
                        .Text = ""
                    Else
                        .Text = Trim$(ss!desc_aplicacao)
                    End If
                    'carrega codigo de fabrica
                    strFabr = ss!cod_fabrica
                Else
                    'carrega aplicacao
                    If Not IsNull(ss!desc_aplicacao) Then
                        .Col = 5
                        .Text = .Text & " " & Trim$(ss!desc_aplicacao)
                        bAltura = Len(Trim$(.Text)) \ 30
                        If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
                            bAltura = bAltura + 1
                        End If
                        If bAltura > 1 Then
                            .RowHeight(.Row) = bAltura * 225
                        End If
                    End If
                End If

                ss.MoveNext
            Next
            .Row = 1
            .FixedRows = 1
        End With
        Screen.MousePointer = 0
        frmCesta.grdItem.Visible = True
    End If
Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub Carrega_Kit" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub
Public Sub Carrega_Itens(tipo_cesta As Byte)
    
    On Error GoTo Trata_Erro
    
    Dim ss As Object

    sql = "Select b.cod_fornecedor,c.sigla,"
    sql = sql & " b.cod_fabrica,b.desc_item,a.cod_dpk"
    sql = sql & " From cesta_item a, item_cadastro b,"
    sql = sql & " fornecedor c"
    sql = sql & " Where a.tp_cesta = " & tipo_cesta & " and"
    sql = sql & " a.cod_loja = " & Mid(Trim(frmVenda.cboDeposito), 1, 2) & " and"
    'If lblCOD_FORNECEDOR <> "" Then
    '    SQL = SQL & " b.cod_fornecedor = " & lblCOD_FORNECEDOR & " and "
    'End If
    sql = sql & " b.cod_fornecedor = c.cod_fornecedor and"
    sql = sql & " a.cod_dpk = b.cod_dpk "
    sql = sql & " Order by b.cod_fornecedor,b.cod_fabrica"

    Set ss = dbAccess2.CreateSnapshot(sql)

    If ss.EOF And ss.BOF Then
        MsgBox "N�o h� itens em destaque para o dep�sito escolhido."
    Else
        With frmCesta.grdItem
            .Cols = 5
            .Rows = ss.RecordCount + 1
            .ColWidth(0) = 660
            .ColWidth(1) = 1500
            .ColWidth(2) = 1500
            .ColWidth(3) = 2500
            .ColWidth(4) = 800

            .Row = 0
            .Col = 0
            .Text = "Cod"
            .Col = 1
            .Text = "Forn"
            .Col = 2
            .Text = "Fabrica"
            .Col = 3
            .Text = "Descri��o"
            .Col = 4
            .Text = "DPK"

            ss.MoveFirst
            For i = 1 To .Rows - 1
                .Row = i

                .Col = 0
                .Text = ss!cod_fornecedor
                .Col = 1
                .Text = ss!Sigla
                .Col = 2
                .Text = ss!cod_fabrica
                .Col = 3
                .Text = ss!desc_item
                .Col = 4
                .Text = ss!cod_dpk

                ss.MoveNext
            Next
            .Row = 1
        End With

        frmCesta.grdItem.Visible = True

    End If
Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub Carrega_Itens" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub sscmdDesc_Click()
    On Error GoTo TopRowError
    'N�mero da coluna onde a pesquisa deve agir
    grdItem.Col = 3

    'texto que dever� ser encontrado
    TEXTOPARAPROCURA = Trim(txtPesquisa.Text)

    'desabilita o highlight
    grdItem.HighLight = False

    'posiciona no in�cio do grid
    grdItem.Row = 1
    grdItem.TopRow = 1

    For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grdfabr
        grdItem.Row = INICIOPESQUISA

        'verifica se a string digitada est� contida no texto da aplica�ao
        If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 Then
            'define nova ocorr�ncia
            OCORRENCIA = OCORRENCIA + 1
        End If

        If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
            'vari�vel que controla quantas vezes determinado string j� foi localizado
            CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1

            'liga o highlight
            grdItem.HighLight = True
            grdItem.SelStartRow = INICIOPESQUISA
            grdItem.SelEndRow = INICIOPESQUISA
            grdItem.SelStartCol = 0
            grdItem.SelEndCol = 5

            'se encontrou o texto posiciona a linha do grid
            If grdItem.CellSelected Then
                If INICIOPESQUISA > 1 Then
                    For INILINHA = 1 To INICIOPESQUISA - 1
                        'o m�ximo valor que toprow pode ter deve ser igual ao numero de
                        'linhas total do grid menos o numero de linhas visiveis do grid
                        If grdItem.TopRow = grdItem.Row - tot_grdfabr Then
                            grdItem.TopRow = grdItem.Rows - tot_grdfabr
                        Else
                            grdItem.TopRow = grdItem.TopRow + 1
                        End If
                    Next
                End If
            End If

            'a proxima pesquisa continuara a partir desta variavel
            CONTINUAPESQUISA = INICIOPESQUISA
            sscmdDesc.Caption = "Pr�xima"
            Exit Sub
        End If
    Next


    MsgBox "Fim de Pesquisa", vbExclamation, "ATEN��O"
    sscmdDesc.Caption = "Descri��o"
    INICIOPESQUISA = 0
    CONTINUAPESQUISA = 0
    Exit Sub

TopRowError:
    If Err = 30009 Then
        grdItem.TopRow = 1
        Resume Next
    End If
End Sub

Private Sub sscmdSair_Click()
    Unload Me
End Sub


Private Sub txtPesquisa_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtVeiculo_Change()
    If txtVeiculo = "" Then
        grdItem.Visible = False
    End If
End Sub


Private Sub txtVeiculo_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub


