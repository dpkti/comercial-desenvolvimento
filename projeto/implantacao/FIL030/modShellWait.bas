Attribute VB_Name = "modShellWait"
Option Explicit

Private Type STARTUPINFO
  cb As Long
  lpReserved As Long
  lpDesktop As Long
  lpTitle As Long
  dwX As Long
  dwY As Long
  dwXSize As Long
  dwYSize As Long
  dwXCountChars As Long
  dwYCountChars As Long
  dwFillAttribute As Long
  dwFlags As Long
  bytShowWindow1 As Byte
  bytShowWindow2 As Byte
  bytReserved21 As Byte
  bytReserved22 As Byte
  lpReserved2 As Long
  hStdInput As Long
  hStdOutput As Long
  hStdError As Long
End Type
Private Type SECURITY_ATTRIBUTES
  nLength As Long
  lpSecurityDescriptor As Long
  bInheritHandle As Long
End Type
Private Type PROCESS_INFORMATION
  hProcess As Long
  hThread As Long
  dwProcessId As Long
  dwThreadID As Long
End Type

Const NORMAL_PRIORITY_CLASS = &H20&
Const HIGH_PRIORITY_CLASS = 128
Const SWP_NOMOVE = 2
Const SWP_NOSIZE = 1
Const SWP_DRAWFRAME = &H20
Const FLAGS = SWP_NOSIZE Or SWP_DRAWFRAME
Const HWND_TOPMOST = -1
Const HWND_NOTOPMOST = -2
Const SYNCHRONIZE = &H100000

Private Declare Function CreateProcess Lib "kernel32" Alias "CreateProcessA" _
  (ByVal lpApplicationName As String, ByVal lpCommandLine As String, _
  lpProcessAttributes As Any, lpThreadAttributes As Any, _
  ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, lpEnvironment As Any, _
  ByVal lpCurrentDirectory As String, lpStartupInfo As STARTUPINFO, _
  lpProcessInformation As PROCESS_INFORMATION) As Long
Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Declare Function ShowWindow Lib "user32.dll" (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Public Declare Function GetEnvironmentVariable Lib "kernel32" Alias "GetEnvironmentVariableA" (ByVal lpName As String, ByVal lpBuffer As String, ByVal nSize As Long) As Long
Public Declare Function WaitForInputIdle Lib "user32" (ByVal hProcess As Long, ByVal dwMilliseconds As Long) As Long
Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, _
     ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, _
     ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Private Const INFINITE = -1
Private Const STARTF_USESHOWWINDOW = &H1&
Private Const STARTF_USESIZE = &H2
Private Const STARTF_USEPOSITION = &H4
Private Const STARTF_USECOUNTCHARS = &H8
Private Const STARTF_USEFILLATTRIBUTE = &H10
Private Const STARTF_RUNFULLSCREEN = &H20
Private Const STARTF_FORCEONFEEDBACK = &H40
Private Const STARTF_FORCEOFFFEEDBACK = &H80
Private Const STARTF_USESTDHANDLES = &H100
Private Const STARTF_USEHOTKEY = &H200
Private Declare Function GetExitCodeProcess Lib "kernel32" _
  (ByVal hProcess As Long, lpExitCode As Long) As Long
Private Declare Function WaitForSingleObject Lib "kernel32" _
  (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function OpenProcess Lib "kernel32" _
     (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, _
     ByVal dwProcessId As Long) As Long
Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" (ByVal lpClassName As Any, ByVal lpWindowName As Any) As Long
'
' Run the commandline, optionally showing the window, and wait for return
'
Function LaunchAppAndWait(ByVal CommandLine As String, bShowWindow As Boolean) As Long
  Dim hTask As Long
  Dim lExitCode As Long
  Dim SI As STARTUPINFO
  Dim udtPI As PROCESS_INFORMATION
  Dim X As Long
  Dim ShowWindow As Long
  Dim ret As Double
  Dim hwnd As Long
  
  On Error GoTo LaunchAppAndWait_ERR
  
  LaunchAppAndWait = 1
  
  SI.cb = LenB(SI)
  SI.dwFlags = STARTF_USESHOWWINDOW
  
  ' set the flag for showing the window:
  ' TODO, this code does not work for showing the window
  If bShowWindow Then
      SI.bytShowWindow1 = ShowWindow And 255
      SI.bytShowWindow2 = (ShowWindow \ 256) And 255
  Else
      SI.bytShowWindow1 = 0
      SI.bytShowWindow2 = 0
  End If
  
  ' NORMAL_PRIORITY_CLASS ISSUES- MUST BE SET:
  hTask = CreateProcess(vbNullString, CommandLine, ByVal 0&, ByVal 0&, False, NORMAL_PRIORITY_CLASS&, ByVal 0&, vbNullString, SI, udtPI)
  
  ' Exit if error was raised:
  If Err.Number <> 0 Then
      Exit Function
  End If
  
  ' or if there is no process:
  If udtPI.hProcess = 0 Then
      Exit Function
  End If
  
  ' wait for the process to end:
  ret = WaitForSingleObject(udtPI.hProcess, 100)
  
  ' loop until process ends:
  Do While ret <> 0
      If ret < 0 Then
          Exit Function
      End If
      
      ret = WaitForSingleObject(udtPI.hProcess, _
      10)
  Loop
  
  ' get exit code:
  X = GetExitCodeProcess(udtPI.hProcess, lExitCode)
  
  ' close the handle and return:
  CloseHandle udtPI.hProcess
  X = lExitCode
  LaunchAppAndWait = X
  
  Exit Function
  
LaunchAppAndWait_ERR:
  Debug.Assert 0
  Debug.Print "LaunchAppAndWait_ERR: " & Err.Description
    
End Function
'
' alternative method for running the a commandline
'
Function ShellWait(strPath As String, _
     Optional intWindow As Integer = vbNormalFocus) As Boolean
     
  On Error GoTo ShellWait_ERR
  
  Dim hTask As Long
  Dim hProc As Long
  ShellWait = False

  ' run the command via shell:
  hTask = Shell(strPath, intWindow)
  If Err Then Exit Function
  
  ' open the process and wait for end:
  hProc = OpenProcess(SYNCHRONIZE, True, hTask)
  WaitForSingleObject hProc, INFINITE
  
  ShellWait = True
    
  Exit Function
  
ShellWait_ERR:
  Debug.Print "ShellWait_ERR: " & Err.Description
  Debug.Assert 0
End Function


