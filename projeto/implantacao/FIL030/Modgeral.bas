Attribute VB_Name = "Module1"
Option Explicit

Public Versao As String

Public Data_Faturamento As Date
Public txtResposta As String           'RESPOSTA DE UMA LISTA
Public Abre_Banco As Byte

Public Path_drv
Public ss  As Snapshot
Public ss5 As Snapshot
Public ss1 As Snapshot

'William Leite
Public minVDA_ACC As Integer
Public retornoACC As Boolean

Public strTipo_Tela As String

Sub Data(ByRef KeyAscii, ByRef txtCampo As Control)
1         On Error GoTo TrataErro

          Dim bTam As Byte    'tamanho do campo JA digitado
          Dim strData As String
          Dim bKey As Byte
2         bTam = Len(txtCampo)

3         If KeyAscii = 8 And bTam > 0 Then 'backspace
4             If Mid$(txtCampo.Text, bTam) = "/" Then
5                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
6             Else
7                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
8             End If

9         ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
10            If bTam = 1 Then
11                strData = txtCampo.Text & Chr$(KeyAscii)
12                If CInt(strData) < 1 Or CInt(strData > 31) Then
13                    Beep
14                Else
15                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
16                End If

17            ElseIf bTam = 4 Then
18                strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
19                If CInt(strData) < 1 Or CInt(strData > 12) Then
20                    Beep
21                Else
22                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
23                End If

24            ElseIf bTam = 7 Then
25                strData = Mid$(txtCampo.Text, 1, 2)     'dia
26                If CInt(strData) < 1 Or CInt(strData > 31) Then
27                    Beep
28                Else
29                    strData = Mid$(txtCampo.Text, 4, 2)     'mes
30                    If CInt(strData) < 1 Or CInt(strData > 12) Then
31                        Beep
32                    Else
33                        strData = txtCampo.Text & Chr$(KeyAscii)
34                        If Not IsDate(CDate(strData)) Then
35                            Beep
36                        Else
37                            txtCampo.Text = strData
38                        End If
39                    End If
40                End If

41            ElseIf bTam < 8 Then
42                bKey = KeyAscii

43            Else
44                bKey = 0
45            End If
46        Else
47            Beep
48        End If

49        SendKeys "{END}"
50        KeyAscii = bKey
51        Exit Sub

TrataErro:

52        If Err.Number = 13 Then
53            MsgBox strData, vbInformation, "Data Inv�lida"
54            KeyAscii = 0
55            Beep
56            Err.Clear
57        ElseIf Err.Number <> 0 Then
58            MsgBox "Sub Data" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
59        End If

End Sub


Sub Process_Line_Errors(ByRef SQL)

    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
        Resume
    Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
        'fechar banco de dados
        If Abre_Banco <> 0 Then
            dbAccess.Close
            dbAccess2.Close
            dbAccess3.Close
        End If

        'cursor
        Screen.MousePointer = vbDefault
        'para a aplicacao
        End
    End If
    Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer

    Temp = Trim(Valor)

    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function iRetornaDigito(ByVal iOrigem As Long) As Integer

    Dim iSoma As Integer
    Dim gResto As Single
    Dim iTam As Integer
    Dim iFator As Integer
    Dim iContador As Integer
    Dim sOrigem As String


    iTam = Len(Trim$(Str$(iOrigem)))
    iFator = 2

    sOrigem = Trim$(Str$(iOrigem))


    For iContador = iTam To 1 Step -1
        iSoma = iSoma + Val(Mid$(sOrigem, iContador, 1)) * iFator
        iFator = iFator + 1
    Next iContador


    gResto = iSoma Mod 11

    If gResto = 0 Then
        iRetornaDigito = 1
    ElseIf gResto = 1 Then
        iRetornaDigito = 0
    Else
        iRetornaDigito = 11 - gResto
    End If

End Function









Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function

Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If

    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "�" Or Chr$(KeyAscii) = "`" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If

    Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If

    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If

    Valor = KeyAscii
End Function

Public Function Pegar_Drive() As String
    Dim fs, d, dc, n
    Dim vArq As Integer
   
    Pegar_Drive = Left(App.Path, 2)
    If Pegar_Drive = "\\" Then
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set dc = fs.Drives
        For Each d In dc
            If d.DriveType = 3 Then
                n = d.ShareName
            Else
                n = d.Path
            End If
            If InStr(1, App.Path, n) > 0 Then
                Pegar_Drive = d.DriveLetter & ":"
                Exit For
            End If
        Next
    End If

    If Pegar_Drive = "\\" Then
        vArq = FreeFile
        If Dir("C:\DRIVE_PAAC.INI") <> "" Then
            Open "C:\DRIVE_PAAC.INI" For Input As #vArq
            Input #vArq, Pegar_Drive
        Else
            Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
            Open "C:\DRIVE_PAAC.INI" For Output As #vArq
            Print #vArq, Pegar_Drive
        End If
        Close #vArq
    End If
End Function

Public Function IsCNPJ(strCNPJ As String) As Boolean

    IsCNPJ = False

    Dim a, j, d1, d2 As Double
    Dim i As Integer

    strCNPJ = Replace(Replace(Replace(strCNPJ, ".", ""), "/", ""), "-", "")

    If Len(strCNPJ) <> 14 Then
        IsCNPJ = False
        Exit Function
    End If

    If Not IsNumeric(strCNPJ) Then
        IsCNPJ = False
        Exit Function
    End If
    a = 0
    i = 0
    d1 = 0
    d2 = 0
    j = 5
    For i = 1 To 12 Step 1
        a = a + (Val(Mid(strCNPJ, i, 1)) * j)
        j = CDbl(IIf(j > 2, j - 1, 9))
    Next i
    a = a Mod 11
    d1 = CDbl(IIf(a > 1, 11 - a, 0))
    a = 0
    i = 0
    j = 6
    For i = 1 To 13 Step 1
        a = a + (Val(Mid(strCNPJ, i, 1)) * j)
        j = CDbl(IIf(j > 2, j - 1, 9))
    Next i
    a = a Mod 11
    d2 = CDbl(IIf(a > 1, 11 - a, 0))
    If d1 = Val(Mid(strCNPJ, 13, 1)) And d2 = Val(Mid(strCNPJ, 14, 1)) Then
        IsCNPJ = True
    Else
        IsCNPJ = False
    End If
End Function


Public Function VerificaLojaACC(COD_LOJA As String)

Dim SQL_ACC As String
Dim ss_ACC As Recordset

SQL_ACC = "Select VL_PARAMETRO from PARAMETROS " & _
      " where NOME_SOFTWARE = 'FIL030' and NOME_PARAMETRO = 'CD" & COD_LOJA & "_ACC'"

Set ss_ACC = dbAccess2.CreateSnapshot(SQL_ACC)
FreeLocks

If ss_ACC.EOF Then
   retornoACC = False 'Coloquei para verifica��o de minimo de venda, neste caso n�o ser� loja ACC
Else
   retornoACC = True
   minVDA_ACC = Int(ss_ACC("VL_PARAMETRO"))
End If

Set ss_ACC = Nothing

End Function

