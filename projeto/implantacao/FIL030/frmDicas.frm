VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDicas 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dicas"
   ClientHeight    =   4770
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7305
   Icon            =   "frmDicas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   318
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   487
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame frm 
      Caption         =   "Dicas Cadastradas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3480
      Left            =   45
      TabIndex        =   5
      Top             =   900
      Width           =   7215
      Begin VB.TextBox txtseq 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   225
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   3015
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.TextBox txtDicas 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2130
         Left            =   180
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   11
         Top             =   765
         Width           =   6855
      End
      Begin VB.TextBox lblData 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4995
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   315
         Width           =   2040
      End
      Begin VB.TextBox lblPseudonimo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1845
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   315
         Width           =   2445
      End
      Begin VB.TextBox lblcod_vend 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
      Begin Bot�o.cmd cmd4 
         Height          =   420
         Left            =   4905
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Primeiro"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "<<"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd5 
         Height          =   420
         Left            =   5445
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Anterior"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   "<"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd6 
         Height          =   420
         Left            =   6030
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Pr�ximo"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   ">"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":240A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd7 
         Height          =   420
         Left            =   6570
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "�ltimo"
         Top             =   2970
         Width           =   465
         _ExtentX        =   820
         _ExtentY        =   741
         BTYPE           =   3
         TX              =   ">>"
         ENAB            =   0   'False
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDicas.frx":2426
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Data:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4500
         TabIndex        =   10
         Top             =   405
         Width           =   465
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Vendedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   7
         Top             =   405
         Width           =   870
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4440
      Width           =   7305
      _ExtentX        =   12885
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12832
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Novo"
      Top             =   45
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":2442
      PICN            =   "frmDicas.frx":245E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   2205
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":3138
      PICN            =   "frmDicas.frx":3154
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2925
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   45
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":3E2E
      PICN            =   "frmDicas.frx":3E4A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd8 
      Height          =   690
      Left            =   765
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Alterar"
      Top             =   45
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":4B24
      PICN            =   "frmDicas.frx":4B40
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd9 
      Height          =   690
      Left            =   1485
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar"
      Top             =   45
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":581A
      PICN            =   "frmDicas.frx":5836
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand4 
      Height          =   675
      Left            =   6570
      TabIndex        =   19
      ToolTipText     =   "Sair"
      Top             =   60
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDicas.frx":6110
      PICN            =   "frmDicas.frx":612C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   3
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDicas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ACT As String
Dim sDicas As Recordset

Private Sub cmd1_Click()
1         On Error GoTo Trata_Erro
2         lblData.Locked = True
3         lblcod_vend.Locked = False
4         lblPseudonimo.Locked = True
5         txtDicas.Locked = False
6         lblcod_vend.Text = sCOD_VEND
          
7         If lblcod_vend.Locked = False And lblcod_vend.Text <> "" Then
8             Set sDicas = dbAccess2.CreateSnapshot("Select * from representante where COD_REPRES = " & lblcod_vend.Text)
9             If Not sDicas.EOF Then
10                lblPseudonimo.Text = sDicas("PSEUDONIMO")
11            Else
12                lblPseudonimo.Text = ""
13            End If
14        End If



15        Set sDicas = dbAccess2.CreateSnapshot("Select dt_faturamento from datas")
16        lblData.Text = sDicas!dt_faturamento & " " & Time
17        txtDicas.Text = ""
18        txtseq.Text = ""
          
19        cmd2.Enabled = True
20        cmd9.Enabled = True
          
21        cmd4.Enabled = False
22        cmd5.Enabled = False
23        cmd6.Enabled = False
24        cmd7.Enabled = False
25        cmd8.Enabled = False
26        cmd3.Enabled = False
          
27        ACT = "I"
28        Set sDicas = Nothing
          
Trata_Erro:
29        If Err.Number <> 0 Then
30            MsgBox "Sub cmd1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
31        End If
End Sub

Private Sub cmd2_Click()
1         If lblcod_vend = "" Then
2             MsgBox "Escolha um Vendedor", vbInformation, "Aten��o"
3             lblcod_vend.SetFocus
4         Else
5             ACT_DICA
6             Form_Load
7         End If
End Sub

Private Sub cmd3_Click()
1         strResposta = MsgBox("Confirma a exclus�o desta dica?", vbYesNo, "ATEN��O")
2         If strResposta = vbYes Then
3             ACT = "D"
4             ACT_DICA
5             Form_Load
6         End If
End Sub

Private Sub cmd4_Click()
1         If Not sDicas.BOF Then
2             sDicas.MoveFirst
3             Call Preencher_Campos(sDicas)
4         End If
End Sub

Private Sub cmd5_Click()
1         If Not sDicas.BOF Then
2             sDicas.MovePrevious
3             If Not sDicas.BOF Then
4                 Call Preencher_Campos(sDicas)
5             End If
6         End If
End Sub

Private Sub cmd6_Click()
1        If Not sDicas.EOF Then
2           sDicas.MoveNext
3           If Not sDicas.EOF Then
4              Call Preencher_Campos(sDicas)
5           End If
6       End If
End Sub

Private Sub cmd7_Click()
1         On Error GoTo Trata_Erro

2         If Not sDicas.EOF Then
3            sDicas.MoveLast
4            Call Preencher_Campos(sDicas)
5         End If
Trata_Erro:
6   If Err.Number <> 0 Then
7       MsgBox "Sub Cmd7_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
8   End If
End Sub

Private Sub cmd8_Click()
          
1         lblcod_vend.Locked = False
2         txtDicas.Locked = False
          
3         cmd2.Enabled = True
4         cmd9.Enabled = True
          
5         cmd4.Enabled = False
6         cmd5.Enabled = False
7         cmd6.Enabled = False
8         cmd7.Enabled = False
9         cmd1.Enabled = False
10        cmd3.Enabled = False
          
11        ACT = "A"
          
End Sub

Private Sub cmd9_Click()
1         Form_Load
End Sub

Private Sub cmdVoltar_Click()

1         Unload Me

End Sub
Private Sub Form_Load()
1         On Error GoTo Trata_Erro
         
2         cmd2.Enabled = False
3         cmd9.Enabled = False
4         lblcod_vend.Locked = True
5         ACT = ""
6         If Val(frmVenda.txtCOD_CLIENTE.Text) <> 0 Then
              'Traz a ultima dica cadastra para o cliente.
7             SQL = "SELECT DICAS.SEQUENCIA, DICAS.COD_VEND, DICAS.COD_CLIENTE, DICAS.DT_DICA, REPRESENTANTE.pseudonimo, DICAS.DICA, DICAS.FL_STATUS "
8             SQL = SQL & " FROM DICAS LEFT JOIN REPRESENTANTE ON DICAS.COD_VEND = REPRESENTANTE.cod_repres "
9             SQL = SQL & " Where DICAS.Cod_cliente = " & Val(frmVenda.txtCOD_CLIENTE.Text)
10            Set sDicas = dbAccess2.CreateDynaset(SQL)
11            If sDicas.EOF = False Then
12                cmd1.Enabled = True
13                cmd8.Enabled = True
14                cmd3.Enabled = True
15                cmd4.Enabled = True
16                cmd5.Enabled = True
17                cmd6.Enabled = True
18                cmd7.Enabled = True
19                Call Preencher_Campos(sDicas)
20            Else
21                MsgBox "Nenhuma Dica cadastrada", vbInformation, "Aten��o"
22                cmd1.Enabled = True
23            End If
24        End If
Trata_Erro:
25        If Err.Number <> 0 Then
26            MsgBox "Sub Form_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
27        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
1         Set frmDicas = Nothing
End Sub

Private Sub lblcod_vend_Change()
1         On Error GoTo Trata_Erro
2         If lblcod_vend.Locked = False And lblcod_vend.Text <> "" Then
          
3             Set sDicas = dbAccess2.CreateSnapshot("Select * from representante where COD_REPRES = " & lblcod_vend.Text)
4             If Not sDicas.EOF Then
5                 lblPseudonimo.Text = IIf(IsNull(sDicas("PSEUDONIMO")), "", sDicas("PSEUDONIMO"))
6             Else
7                 lblPseudonimo.Text = ""
8             End If
9         End If
Trata_Erro:
10        If Err.Number <> 0 Then
11      MsgBox "Sub lblcod_Vend_Change" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
12        End If

End Sub

Sub ACT_DICA()
1         On Error GoTo Trata_Erro
          Dim vSeq As Long
          Dim vdate As Date
          Dim sdtDica As Recordset
          
2         SQL = ""
3         Select Case ACT
              Case "I"
4                 Set sDicas = dbAccess2.CreateSnapshot("SELECT Max(Sequencia)AS Seq FROM Dicas;")
5                 vSeq = IIf(IsNull(sDicas!seq), 1, sDicas!seq + 1)
                  
6                 SQL = "INSERT INTO DICAS(SEQUENCIA,COD_CLIENTE,DT_DICA,COD_VEND,DICA,FL_STATUS)VALUES(" & vSeq & "," & frmVenda.txtCOD_CLIENTE.Text & ", #" & Format(Now, "MM/DD/YY HH:MM:SS") & "# ," & lblcod_vend & ", '" & txtDicas.Text & "',1);"
7                 dbAccess2.Execute SQL, dbFailOnError
                  
8                 Set sdtDica = dbAccess2.CreateSnapshot("SELECT Dt_dica FROM DICAS WHERE SEQUENCIA = " & vSeq)
9                 vdate = sdtDica!dt_dica
10                Set sdtDica = Nothing
                  
11                Call Controle_Dicas(vSeq, ACT, txtDicas.Text, frmVenda.txtCOD_CLIENTE.Text, lblcod_vend, vdate)
                  
12            Case "A"
13                Set sdtDica = dbAccess2.CreateSnapshot("SELECT Dt_dica FROM DICAS WHERE SEQUENCIA = " & txtseq)
14                vdate = sdtDica!dt_dica
15                Set sdtDica = Nothing
              
16                SQL = "UPDATE DICAS SET COD_VEND = " & lblcod_vend & _
                  ", DICA = '" & txtDicas & "', FL_STATUS = 1 WHERE SEQUENCIA = " & txtseq
17                dbAccess2.Execute SQL, dbFailOnError
18                Call Controle_Dicas(txtseq, ACT, txtDicas.Text, frmVenda.txtCOD_CLIENTE.Text, lblcod_vend, vdate)

19            Case "D"
20                Set sdtDica = dbAccess2.CreateSnapshot("SELECT Dt_dica FROM DICAS WHERE SEQUENCIA = " & txtseq)
21                vdate = sdtDica!dt_dica
22                Set sdtDica = Nothing
              
23                SQL = "DELETE FROM DICAS WHERE SEQUENCIA = " & txtseq
24                dbAccess2.Execute SQL, dbFailOnError
25                Call Controle_Dicas(txtseq, ACT, "", frmVenda.txtCOD_CLIENTE.Text, lblcod_vend, vdate)

26        End Select
Trata_Erro:
27        If Err.Number <> 0 Then
28           MsgBox "Sub cmdSalvar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
29        End If
End Sub

Function Controle_Dicas(pSequencia As Long, pOpcoes As String, pDica As String, pCod_Cliente As Long, pCod_Vend As Long, pDate As Date)
1         On Error GoTo Trata_Erro
          
          Dim vMaxCod As Integer
          Dim sControle As Recordset
          
2         Set sControle = dbAccess2.CreateSnapshot("SELECT Max(SEQ_ALTERACAO) AS SEQ_ALT FROM CONTROLE_DICAS WHERE SEQUENCIA = " & pSequencia)
3         vMaxCod = IIf(IsNull(sControle!Seq_Alt), 1, sControle!Seq_Alt + 1)
          

4         SQL = "INSERT INTO CONTROLE_DICAS( "
5         SQL = SQL & " SEQUENCIA, "
6         SQL = SQL & " COD_CLIENTE, "
7         SQL = SQL & " DT_DICA, "
8         SQL = SQL & " SEQ_ALTERACAO, "
9         SQL = SQL & " DT_ALTERACAO, "
10        SQL = SQL & " COD_VEND, "
11        SQL = SQL & " FL_TIPO "
12        If pDica <> "" Then
13            SQL = SQL & ", Dica"
14        End If
15        SQL = SQL & " )VALUES( "
16        SQL = SQL & pSequencia & ", "
17        SQL = SQL & pCod_Cliente & ", #"
18        SQL = SQL & Format(pDate, "MM/DD/YY HH:MM:SS") & "#, "
19        SQL = SQL & vMaxCod & ", #"
20        SQL = SQL & Format(Now, "MM/DD/YY HH:MM:SS") & "#, "
21        SQL = SQL & pCod_Vend
22        If sControle.EOF = False Then
23            Select Case pOpcoes
                Case "I" 'Inserir
24                    SQL = SQL & ", 1"
25                Case "A" 'Altera��o
26                    SQL = SQL & ", 2"
27                Case "D" 'Delete
28                    SQL = SQL & ", 3"
29            End Select
30        Else
31            SQL = SQL & ", 1 "
32        End If
33        If pDica <> "" Then
34            SQL = SQL & ",'" & pDica & "'"
35        End If
36        SQL = SQL & ");"
          
37        Set sControle = Nothing
38        dbAccess2.Execute SQL, dbFailOnError

Trata_Erro:
39        If Err.Number <> 0 Then
40           MsgBox "Sub Controle_Dicas" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
41        End If

End Function

Function Preencher_Campos(pDicas As Recordset)
1         On Error GoTo Trata_Erro

2         If IsNull(pDicas!COD_VEND) Then
3            lblcod_vend.Text = ""
4         Else
5            lblcod_vend.Text = pDicas!COD_VEND
6         End If
7         If IsNull(pDicas!pseudonimo) Then
8            lblPseudonimo.Text = ""
9         Else
10           lblPseudonimo.Text = pDicas!pseudonimo
11        End If
12        If IsNull(pDicas!dt_dica) Then
13           lblData.Text = ""
14        Else
15           lblData.Text = pDicas!dt_dica
16        End If
17        If IsNull(pDicas!Dica) Then
18            txtDicas.Text = ""
19        Else
20            txtDicas.Text = pDicas!Dica
21        End If
22        If IsNull(pDicas!SEQUENCIA) Then
23           txtseq.Text = ""
24        Else
25           txtseq.Text = pDicas!SEQUENCIA
26        End If
Trata_Erro:
27        If Err.Number <> 0 Then
28            MsgBox "Sub Preencher_Campos" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
29        End If
End Function

Private Sub SSCommand4_Click()
    Unload Me
End Sub
