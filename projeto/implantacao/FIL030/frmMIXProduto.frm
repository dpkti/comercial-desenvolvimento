VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmMIXProduto 
   Caption         =   "MIX de Produtos"
   ClientHeight    =   4965
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7950
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   ScaleHeight     =   4965
   ScaleWidth      =   7950
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtDesc 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1440
      MaxLength       =   30
      TabIndex        =   0
      Top             =   920
      Width           =   2415
   End
   Begin MSGrid.Grid grdMIX 
      Height          =   3615
      Left            =   0
      TabIndex        =   1
      Top             =   1260
      Width           =   7905
      _Version        =   65536
      _ExtentX        =   13944
      _ExtentY        =   6376
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmMIXProduto.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   2
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMIXProduto.frx":001C
      PICN            =   "frmMIXProduto.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   3
      Top             =   800
      Width           =   9090
      _ExtentX        =   16034
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdBuscar 
      Height          =   675
      Left            =   7200
      TabIndex        =   4
      ToolTipText     =   "Buscar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMIXProduto.frx":0D12
      PICN            =   "frmMIXProduto.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblMSG 
      AutoSize        =   -1  'True
      Caption         =   "N�O H� MIX DE PRODUTOS PARA A PESQUISA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   1320
      TabIndex        =   6
      Top             =   360
      Width           =   4245
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Descri��o MIX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   45
      TabIndex        =   5
      Top             =   960
      Width           =   1530
   End
End
Attribute VB_Name = "frmMIXProduto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmMIXProduto
' Author    : c.samuel.oliveira
' Date      : 01/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------

Private Sub cmdBuscar_Click()
    
    If Dir(strPath & "MIX.MDB") = "" Then Exit Sub
    CarregarMix UCase(Trim(txtDesc.Text))
    txtDesc.Text = ""
End Sub

Private Sub cmdSair_Click()
    Unload Me
    Set frmMIXProduto = Nothing
End Sub

Private Sub Form_Activate()
    If Dir(strPath & "MIX.MDB") = "" Then Exit Sub
    CarregarMix ""
End Sub

Private Sub CarregarMix(pDesc As String)

    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim i As Long
    Dim vSQL As String
    
    grdMIX.Visible = True
    lblMSG.Visible = False
    
    Screen.MousePointer = vbHourglass

    If Len(Trim(frmVenda.txtCOD_DPK.Text)) = 0 Then
    
        vSQL = "SELECT A.COD_LOJA,"
        vSQL = vSQL & " A.NUM_MIX,"
        vSQL = vSQL & " A.DT_INICIAL,"
        vSQL = vSQL & " A.DT_FINAL,"
        vSQL = vSQL & " A.DESCRICAO"
        vSQL = vSQL & " FROM MIX_PRODUTO A "
        vSQL = vSQL & " WHERE A.COD_LOJA = " & Mid(Trim(frmVenda.cboDeposito), 1, 2)
        If Len(pDesc) > 0 Then vSQL = vSQL & " AND A.DESCRICAO LIKE '*" & pDesc & "*'"
        vSQL = vSQL & " ORDER BY A.NUM_MIX"
    
    Else
    
        vSQL = "SELECT A.COD_LOJA,"
        vSQL = vSQL & " A.NUM_MIX,"
        vSQL = vSQL & " A.DT_INICIAL,"
        vSQL = vSQL & " A.DT_FINAL,"
        vSQL = vSQL & " A.DESCRICAO"
        vSQL = vSQL & " FROM MIX_PRODUTO A,MIX_ITEM B "
        vSQL = vSQL & " WHERE A.COD_LOJA = " & Mid(Trim(frmVenda.cboDeposito), 1, 2)
        If Len(pDesc) > 0 Then vSQL = vSQL & " AND A.DESCRICAO LIKE '*" & pDesc & "*'"
        vSQL = vSQL & " AND B.COD_DPK = " & Val(frmVenda.txtCOD_DPK.Text)
        vSQL = vSQL & " AND A.NUM_MIX = B.NUM_MIX "
        vSQL = vSQL & " ORDER BY A.NUM_MIX"
            
    End If

    Set ss = dbAccess4.CreateSnapshot(vSQL)

    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        grdMIX.Visible = False
        lblMSG.Visible = True
        Exit Sub
    End If

    'carrega dados
    With grdMIX
        .Cols = 5
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 1500
        .ColWidth(1) = 1000
        .ColWidth(2) = 1250
        .ColWidth(3) = 1250
        .ColWidth(4) = 2500

        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "Mix"
        .Col = 2
        .Text = "Vig�ncia Inicial"
        .Col = 3
        .Text = "Vig�ncia Final"
        .Col = 4
        .Text = "Descri��o"

        For i = 1 To .Rows - 1
            .Row = i

            .Col = 0
            .Text = Trim(frmVenda.cboDeposito)
            .Col = 1
            .Text = ss("NUM_MIX")
            .Col = 2
            .Text = ss("DT_INICIAL")
            .Col = 3
            .Text = ss("DT_FINAL")
            .Col = 4
            .Text = ss("DESCRICAO")
            
            ss.MoveNext
        Next
        .Row = 1
    End With

    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        MsgBox "Sub CarregarMix" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, "Aten��o", &H40000 + 0
    End If

    Screen.MousePointer = vbDefault
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmMIXProduto = Nothing
End Sub

Private Sub grdMIX_DblClick()

    On Error GoTo Trata_Erro

    Screen.MousePointer = 11
    grdMIX.Col = 1
    grdMIX.Row = grdMIX.Row
    lngNUM_MIX = Val(grdMIX.Text)
    Me.Refresh

    frmMIXProduto.Hide
    
    frmMIXItem.Show 1

    Me.MousePointer = 0
    Screen.MousePointer = 0

Trata_Erro:

    If Err.Number <> 0 Then
          MsgBox "grdMIX_DblClick" & vbCrLf & "C�digo:" & Err.Number & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl, "Aten�ao", &H40000 + 0
    End If

End Sub

