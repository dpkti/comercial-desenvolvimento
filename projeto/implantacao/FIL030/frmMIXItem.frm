VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmMIXItem 
   Caption         =   "MIX Item"
   ClientHeight    =   5835
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13065
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5835
   ScaleWidth      =   13065
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdItem 
      Height          =   4455
      Left            =   0
      TabIndex        =   0
      Top             =   1260
      Width           =   13005
      _Version        =   65536
      _ExtentX        =   22939
      _ExtentY        =   7858
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "frmMIXItem.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   0
      TabIndex        =   1
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMIXItem.frx":001C
      PICN            =   "frmMIXItem.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient 
      Height          =   30
      Left            =   0
      TabIndex        =   2
      Top             =   840
      Width           =   13050
      _ExtentX        =   23019
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label lblMSG 
      AutoSize        =   -1  'True
      Caption         =   "N�O H� ITENS PARA O MIX DE PRODUTOS ESCOLHIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   3480
      TabIndex        =   5
      Top             =   360
      Width           =   5010
   End
   Begin VB.Label lblNumMix 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1530
      TabIndex        =   4
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "N�mero de MIX"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   3
      Top             =   990
      Width           =   1320
   End
End
Attribute VB_Name = "frmMIXItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmMIXItem
' Author    : c.samuel.oliveira
' Date      : 01/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------



Private Sub cmdSair_Click()
    lngNUM_MIX = 0
    
    Unload Me
    Set frmMIXItem = Nothing
    frmMIXProduto.Show 1
End Sub

Private Sub Form_Load()
    
    If Dir(strPath & "MIX.MDB") = "" Then Exit Sub
    CarregarItem 0

End Sub

Private Sub CarregarItem(pMostrarPreco As Byte)
    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim ssUF As Snapshot
    Dim ssDesc As Snapshot
    Dim vSQL As String
    Dim vDESC_UF As Single
    Dim i As Long
    Dim dblCalculo As Double
    Dim strCod_Deposito As String
    Dim strUf_Origem As String
    Dim strUf_Destino As String
    Dim vCODFORN As Integer
    Dim vFORN As String
    Dim vCODFABRICA As String
    Dim vDescItem As String
    Dim vQTD_DISP As Long
    Dim vPreco As Double
        
    lblMSG.Visible = False
    
    Screen.MousePointer = vbHourglass
    
    'Dep�sito / UF destino / UF Origem
    strCod_Deposito = Mid(Trim(frmVenda.cboDeposito.Text), 1, 2)
    strUf_Destino = frmVenda.cboUf.Text
    
    vSQL = "Select a.cod_uf from cidade a, loja b where a.cod_cidade = b.cod_cidade and b.cod_loja= " & strCod_Deposito
    Set ssDesc = dbAccess2.CreateSnapshot(vSQL)
    FreeLocks
    If Not ssDesc.BOF And Not ssDesc.EOF Then strUf_Origem = ssDesc!cod_uf
    ssDesc.Close
    
    vSQL = "SELECT A.NUM_MIX,"
    vSQL = vSQL & " A.COD_DPK,"
    vSQL = vSQL & " A.QTD_DPK"
    vSQL = vSQL & " FROM MIX_ITEM A"
    vSQL = vSQL & " WHERE A.NUM_MIX = " & lngNUM_MIX
    vSQL = vSQL & " ORDER BY A.NUM_MIX"

    Set ss = dbAccess4.CreateSnapshot(vSQL)

    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        lblMSG.Visible = True
        Exit Sub
    End If
    
    lblNumMix.Caption = CStr(lngNUM_MIX)
    
    'carrega dados
    With grdItem
        .Cols = 9
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 1500
        .ColWidth(1) = 1000
        .ColWidth(2) = 800
        .ColWidth(3) = 1200
        .ColWidth(4) = 2000
        .ColWidth(5) = 3000
        .ColWidth(6) = 1000
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "C�d. DPK"
        .Col = 2
        .Text = "C�d. Forn."
        .Col = 3
        .Text = "Forn."
        .Col = 4
        .Text = "C�d. F�brica"
        .Col = 5
        .Text = "Desc. Item"
        .Col = 6
        .Text = "Qtde. Disp."
        .Col = 7
        .Text = "Qtde. MIX"
        .Col = 8
        .Text = "Pre�o Venda"
        
        For i = 1 To .Rows - 1
            .Row = i
              
            Call CarregarDadosItem(ss("COD_DPK"), vCODFORN, vFORN, vCODFABRICA, vDescItem, vQTD_DISP, vPreco)

            .Col = 0
            .Text = Trim(frmVenda.cboDeposito)
            .Col = 1
            .Text = ss("COD_DPK")
            .Col = 2
            .Text = vCODFORN
            .Col = 3
            .Text = vFORN
            .Col = 4
            .Text = vCODFABRICA
            .Col = 5
            .Text = vDescItem
            .Col = 6
            .ColAlignment(6) = 1
            .Text = vQTD_DISP
            .Col = 7
            .ColAlignment(7) = 1
            .Text = ss("QTD_DPK")
            
            'desconto de UF
            vSQL = "select PC_DESC from UF_DPK where "
            vSQL = vSQL & " COD_DPK = " & ss("COD_DPK") & " and "
            vSQL = vSQL & " COD_UF_ORIGEM = '" & strUf_Origem & "' and "
            vSQL = vSQL & " COD_UF_DESTINO = '" & strUf_Destino & "'"
            
            Set ssUF = dbAccess2.CreateSnapshot(vSQL)
            FreeLocks
            
            If ssUF.EOF And ssUF.BOF Then
                vDESC_UF = 0
            Else
                If frmVenda.lblMsgTributacao.Visible = True And tipo_cliente = "ISENTO" Then
                    vDESC_UF = 0
                Else
                    vDESC_UF = CSng(ssUF!pc_desc)
                End If
            End If
            ssUF.Close
            
            .Col = 8
            .ColAlignment(8) = 1
            dblCalculo = vPreco * _
                    (1 - CDbl(frmVenda.txtPC_DESC_FIN_DPK.Text) / 100) * _
                    (1 + CDbl(frmVenda.txtPC_ACRES_FIN_DPK.Text) / 100) * _
                    (1 - CDbl(frmVenda.lblDescSuframa.Caption) / 100) * _
                    (1 - CDbl(frmVenda.lblPercDifIcm.Caption) / 100) * _
                    (1 - CDbl(frmVenda.txtDescAdicional.Text) / 100) * _
                    (1 - vDESC_UF / 100)
            
            .Text = Format$(dblCalculo, "Standard")
            
            ss.MoveNext
        Next
        .Row = 1
    End With

    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        MsgBox "Sub CarregarItem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

    Screen.MousePointer = vbDefault
End Sub


Private Sub grdItem_DblClick()

On Error GoTo TrataErro
    grdItem.Col = 1
    frmVenda.txtCOD_DPK = grdItem.Text
    frmVenda.txtCOD_DPK_LostFocus
    
    frmMIXItem.Hide

    Exit Sub
TrataErro:
    MsgBox "Sub grdItem_DblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
End Sub

Private Sub CarregarDadosItem(pCodDPK As Long, ByRef pCodForn As Integer, ByRef pForn As String, ByRef pCodFab As String, ByRef pDescItem As String, ByRef pQTDDisp As Long, ByRef pPreco As Double)
    
    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim vSQL As String
         
    'montar SQL
    vSQL = "Select b.cod_fornecedor,c.sigla, b.cod_fabrica,b.desc_item, " & _
            "(a.QTD_ATUAL - a.QTD_RESERV - a.QTD_PENDENTE) as QTD_DISP,d.preco_venda " & _
            "from  item_estoque a, item_cadastro b,fornecedor c, item_preco d " & _
            "Where b.cod_fornecedor = c.cod_fornecedor and " & _
            "a.cod_loja = " & Mid(Trim(frmVenda.cboDeposito), 1, 2) & " and " & _
            "a.cod_dpk = " & pCodDPK & " AND " & _
            "a.cod_dpk = d.cod_dpk and a.cod_loja = d.cod_loja and a.cod_dpk = b.cod_dpk "

    Set ss = dbAccess2.CreateSnapshot(vSQL)
    FreeLocks

    If Not ss.EOF And Not ss.BOF Then
        pCodForn = ss("cod_fornecedor")
        pForn = ss("sigla")
        pCodFab = ss("cod_fabrica")
        pDescItem = ss("desc_item")
        pQTDDisp = ss("QTD_DISP")
        pPreco = ss("preco_venda")
    End If
    ss.Close
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
        Resume
    Else
        MsgBox "Sub CarregarDadosItem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub
