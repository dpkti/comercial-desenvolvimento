VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "TELEMARKETING"
   ClientHeight    =   6510
   ClientLeft      =   765
   ClientTop       =   1845
   ClientWidth     =   6870
   Icon            =   "vda230.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin MSComctlLib.StatusBar sspmsg 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   7
      Top             =   6195
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11589
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox sspMenu 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      ForeColor       =   &H80000008&
      Height          =   705
      Left            =   0
      ScaleHeight     =   675
      ScaleWidth      =   6840
      TabIndex        =   0
      Top             =   0
      Width           =   6870
      Begin Bot�o.cmd SSCommand1 
         Height          =   615
         Left            =   30
         TabIndex        =   1
         ToolTipText     =   "Tela de Vendas"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":08CA
         PICN            =   "vda230.frx":08E6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand2 
         Height          =   615
         Left            =   720
         TabIndex        =   2
         ToolTipText     =   "Manuten��o de Cota��o"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":2078
         PICN            =   "vda230.frx":2094
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand5 
         Height          =   615
         Left            =   1440
         TabIndex        =   3
         ToolTipText     =   "Gerar Pedido Atrav�s da Cota��o"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":2D6E
         PICN            =   "vda230.frx":2D8A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd sscmdContatos 
         Height          =   615
         Left            =   2130
         TabIndex        =   4
         ToolTipText     =   "Contatos"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":3A64
         PICN            =   "vda230.frx":3A80
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand3 
         Height          =   615
         Left            =   2790
         TabIndex        =   5
         ToolTipText     =   "Agenda"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":435A
         PICN            =   "vda230.frx":4376
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand4 
         Height          =   615
         Left            =   4170
         TabIndex        =   6
         ToolTipText     =   "Sair"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":5050
         PICN            =   "vda230.frx":506C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCompactar 
         Height          =   615
         Left            =   3480
         TabIndex        =   8
         ToolTipText     =   "Compactar Base"
         Top             =   30
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "vda230.frx":5D46
         PICN            =   "vda230.frx":5D62
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   3
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin Crystal.CrystalReport rptGeral2 
      Left            =   120
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   262150
   End
   Begin VB.Menu mnuVendas 
      Caption         =   "&Vendas"
   End
   Begin VB.Menu mnuCotacao 
      Caption         =   "Co&ta��o"
      Begin VB.Menu mnuCotacaoManutencao 
         Caption         =   "&Manuten��o"
      End
      Begin VB.Menu mnuCotacaoPed 
         Caption         =   "&Gerar Pedido"
      End
   End
   Begin VB.Menu mnuContatos 
      Caption         =   "&Contatos"
   End
   Begin VB.Menu mnuAgenda 
      Caption         =   "&Agenda"
   End
   Begin VB.Menu mnuCompactar 
      Caption         =   "&Compactar Base"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : MDIForm1
' Author    : c.samuel.oliveira
' Date      : 01/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------


Option Explicit

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Private Sub cmdCompactar_Click()
   mnuCompactar_Click
End Sub

Private Sub MDIForm_Load()

1         Versao = App.Major & "." & App.Minor & "." & App.Revision

2         On Error GoTo TrataErro
          Dim strLogin As String
          Dim ss As Recordset
          Dim dDia_Util As Date
          Dim dDt_Real As Date
          Dim vArqVersao As Integer
          Dim vArqConteudo As String
          Dim vVersaoOK As Boolean
          Dim Trava_pedido As Integer
          
          
3         Abre_Banco = 0
4         Path_drv = Pegar_Drive
5         'Path_drv = "C:" TI-3934, comentado para resolver o problema dos PAACs que ficam no H:

    '################################Atualiza��o do sistema FIL450.############################################
            'Luciano Carlos de Jesus  - 03/03/2008
            'Como o sistema FIL450 � o primeiro sistema chamado ap�s o ftp ent�o sempre dever� ser enviado
            'uma vers�o como nome de "FIL450_N.exe", pois esta rotina ir� renomear a vers�o anterior do
            '"FIL450.EXE" para "FIL450.OLD" e logo ap�s renomear o "FIL450_N.EXE" para "FIL450.EXE"
            
6       If Trim(Dir(Path_drv & "\PGMS\FIL450_N.EXE")) <> "" Then
7           FileCopy Path_drv & "\PGMS\FIL450.EXE", Path_drv & "\PGMS\FIL450.OLD"
8           Kill Path_drv & "\PGMS\FIL450.EXE"
9           FileCopy Path_drv & "\PGMS\FIL450_N.EXE", Path_drv & "\PGMS\FIL450.EXE"
10          Kill Path_drv & "\PGMS\FIL450_N.EXE"
11      End If
            
    '################################Fim da Atualiza��o do sistema FIL450.######################################

12       If Get_locale(LOCALE_SDECIMAL) <> "." Or _
            Get_locale(LOCALE_STHOUSAND) <> "," Or _
            Get_locale(LOCALE_SSHORTDATE) <> "dd/MM/yy" Or _
            Get_locale(LOCALE_STIMEFORMAT) <> "HH:mm:ss" Then
13          MsgBox "As Configura��es Regionais de seu computador est�o incorretas." & vbCrLf & "Entre em contato com o Helpdesk e informe esta mensagem.", vbInformation, "Aten��o"
14          End
15       End If

16       If FileLen(Path_drv & "\DADOS\BASE_DPK.MDB") / 1000000 > 250 Then
17          MsgBox "Sua base de dados est� acima do tamanho indicado." & vbCrLf & "Ela ser� Compactada para que volte ao tamanho normal."
18          mnuCompactar_Click
19       End If

20        strPathUsuario = Path_drv & "\COM\"
21        strPath = Path_drv & "\DADOS\"
22        strPath_Envio = Path_drv & "\COM\SAIDA\PEDIDOS\"
23        strPath_Msg = Path_drv & "\COM\SAIDA\MSG\"

          'Conexao Access
24        Set dbAccess = OpenDatabase(strPath & "VENDAS.MDB")
25        Set dbAccess2 = OpenDatabase(strPath & "BASE_DPK.MDB")
26        Set dbAccess3 = OpenDatabase(strPathUsuario & "DBMAIL.MDB")
27        If Dir(strPath & "MIX.MDB") <> "" Then Set dbAccess4 = OpenDatabase(strPath & "MIX.MDB") 'TI-3952

          'Eduardo - 04/05/2006
28        Set ss = dbAccess2.OpenRecordset("SELECT DT_FATURAMENTO, DT_REAL FROM DATAS")
29        data_real = CDate(ss!dt_real)
          'Setando Vari�vel com a Data Atual - Jairo - 12/01/2012
30        Data_atual = Date

'------------' Comentando linhas 27 � 30 conforme solicitado na DUM SDS2405 - Jairo Almeida 12/01/2012------
'27        If Format(Date, "DD/MM/YY") <> Format(CDate(ss!dt_faturamento), "DD/MM/YY") And Weekday(Date) <> 1 And Weekday(Date) <> 7 Then
'28           MsgBox "A data do seu micro esta incorreta ou voce deve fazer a atualiza�ao diaria.", vbInformation, "Aten�ao"
'29           End
'30        End If
'-------------- Fim da Atualiza��o -------------------------------------------------------------------------

31        Atualizar_Vl_ICM_RETIDO

32        Abre_Banco = 1

          'verifica se codigo de acesso est� correto
33        SQL = "Select pseudonimo,cod_filial"
34        SQL = SQL & " from representante"
35        SQL = SQL & " Where cod_repres = " & sCOD_VEND
36        Set ss = dbAccess2.CreateSnapshot(SQL)
37        FreeLocks

38        If ss.EOF And ss.BOF Then
39            MsgBox "C�digo de acesso incorreto. ", vbInformation, "Aten��o"
40            dbAccess.Close
41            dbAccess2.Close
42            dbAccess3.Close
43            End
44        Else
45            sFILIAL_VEND = ss!Cod_Filial
46        End If
47        ss.Close

48      If Weekday(CDate(Format(Now, "dd/mm/yy"))) = 1 Then
49        dDia_Util = CDate(Format(Now, "dd/mm/yy")) + 1
50      ElseIf Weekday(CDate(Format(Now, "dd/mm/yy"))) = 7 Then
51        dDia_Util = CDate(Format(Now, "dd/mm/yy")) + 2
52      Else
53        dDia_Util = CDate(Format(Now, "dd/mm/yy"))
54      End If
        
55      If Weekday(CDate(Format(data_real, "dd/mm/yy"))) = 6 Then
56        dDt_Real = CDate(Format(data_real, "dd/mm/yy")) + 2
57      Else
58        dDt_Real = CDate(Format(data_real, "dd/mm/yy"))
59      End If
       

60      dia_atua = dDia_Util - dDt_Real
         
'------- Jairo Almeida - Atendendo � solicita��o DUM SDS2405 - 13/01/2012 ---------
'60     If dia_atua > 3 Then - Linha Original
        '  MsgBox "O sistema est� desatualizado a = " & dia_atua & " dia(s)." & _
        '     " IMPOSS�VEL digitar um pedido. Fa�a a atualiza��o.", vbExclamation, "Aten��o"
        '  dbAccess.Close
        '  dbAccess2.Close
        '  End
        'End If
'-----------------------------------------------------------------------------------


          'carregar data de faturamento,uf origem, filial origem
61        SQL = "Select a.dt_faturamento AS DT_FATURAMENTO , "
62        SQL = SQL & " dt_real , "
63        SQL = SQL & " b.cod_filial AS COD_FILIAL, c.sigla AS SIGLA , "
64        SQL = SQL & " b.cod_uf as uf_filial ,"
65        SQL = SQL & " d.cod_uf AS COD_UF ,"
66        SQL = SQL & " b.cod_loja AS COD_LOJA, e.nome_fantasia AS NOME_FANTASIA "
67        SQL = SQL & " from datas a, deposito b, filial c,  cidade d, loja e "
68        SQL = SQL & " where  b.cod_filial=c.cod_filial and "
69        SQL = SQL & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "
70        Set ss = dbAccess2.CreateSnapshot(SQL)
71        FreeLocks

'------- Jairo Almeida - Atendendo � solicita��o DUM SDS2405 - 13/01/2012 ---------
' Comentandas as linhas 59 � 64
    'Verificar se o sistema est� atualizado.
'59        If ss!dt_real < Date And Weekday(Date) <> 1 And Weekday(Date) <> 7 Then
'60            MsgBox "Voc� deve fazer a atualiza��o di�ria antes de digitar pedido!!!", _
'              vbExclamation, "Aten��o"
'61            ss.Close
'62            dbAccess.Close
'63            End
'64        End If
'-----------------------------------------------------------------------------------
72        If ss.EOF And ss.BOF Then
73            ss.Close
74            MsgBox "Sub txtCod_Banco_Lostfocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
75        Else
'------------ Jairo Almeida - Mudan�a para Atender DUM SDS2405 - 13/01/2012---------------------------------
'69          Data_Faturamento = CDate(ss!dt_faturamento) - Linha Original
76           Data_Faturamento = CDate(Data_atual) ' Data do Faturamento deve ser igual � data do Sistema
77           data_real = CDate(ss!dt_real)
'-----------------------------------------------------------------------------------

78            FILIAL_PED = Format(ss!Cod_Filial, "0000") & "-" & ss!Sigla
79            uf_origem = ss!cod_uf
80            uf_filial = ss!uf_filial
81            deposito_default = Format(ss!COD_LOJA, "00") & "-" & ss!NOME_FANTASIA
82        End If
83        ss.Close

84        SQL = "Select b.destino as email_to, b.usuario as email_from, tipo_usuario "
85        SQL = SQL & " from usuario b"
86        Set ss = dbAccess3.CreateSnapshot(SQL)

87        If ss.EOF And ss.BOF Then
88            MsgBox "Tabela sem e-mail cadastrado, ligue para Depto Sistemas", vbInformation, "Aten��o"
89            ss.Close
90        Else
91            Tipo_Usuario = UCase(ss!Tipo_Usuario)
              'RTI-142 - Definindo o T�tulo do Formul�rio
92            If Tipo_Usuario = "R" Then
93                MDIForm1.Caption = "TELEMARKETING - REPRESENTANTE - Vers�o: " & Versao
94            ElseIf Tipo_Usuario = "C" Then
95                MDIForm1.Caption = "TELEMARKETING - CLIENTE - Vers�o: " & Versao
96            ElseIf Tipo_Usuario = "G" Then
97                MDIForm1.Caption = "TELEMARKETING - GERENTE - Vers�o: " & Versao
98            Else
99                MDIForm1.Caption = "TELEMARKETING - PAAC - Vers�o: " & Versao
100           End If
101           strEmail_To = ss!email_to
102           strEmail_from = ss!email_from
103           ss.Close
104       End If

105       Me.Visible = True
106       Call SSCommand1_Click
107   Exit Sub
TrataErro:
108       If Err = 13 Then
109           MsgBox "Entre com o c�digo correto, para acessar o sistema", vbExclamation, "Aten��o"
110           End
111       ElseIf Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
112           Resume
113       Else
114           MsgBox "Sub MDIFORM_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
115       End If
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    dbAccess.Close
    End
End Sub

Private Sub mnuAgenda_Click()
    frmAgenda.Show 1
End Sub

Private Sub mnuCompactar_Click()
    Dim vTerminou As Boolean
    
    'Fechar as conexoes com o Banco
    Set dbAccess = Nothing
    Set dbAccess2 = Nothing
    Set dbAccess3 = Nothing
    
    MsgBox "Sua base de dados ser� compactada", vbInformation, "Aten��o"
    
    If Dir(strPath & "VENDAS.ldb") <> "" Or Dir(strPath & "BASE_DPK.ldb") <> "" Or Dir(strPath & "DBMAIL.ldb") <> "" Then
        MsgBox "Sua base esta acima do tamanho permitido" & vbCrLf & "Entre em contato com Helpdesk", vbInformation, "Aten��o"
        GoTo Sair
    End If
    
    vTerminou = ShellWait(Path_drv & "\PGMS\FIL046.EXE")
Sair:
    'Set dbAccess = OpenDatabase(Path_drv & "\DADOS\VENDAS.MDB")
    'Set dbAccess2 = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB")
    'Set dbAccess3 = OpenDatabase(Path_drv & "\DADOS\DBMAIL.MDB")

End Sub

Private Sub mnuCotacaoManutencao_Click()
1         On Error GoTo TrataErro

          Dim ss1 As Snapshot
          Dim PLSQL As String

          'verificar cota��o pendente
2         SQL = "select NUM_COTACAO "
3         SQL = SQL & "from COTACAO "
4         SQL = SQL & "where COD_VEND = " & CStr(sCOD_VEND)
5         SQL = SQL & " and SITUACAO = 8"

6         Set ss1 = dbAccess2.CreateSnapshot(SQL)
7         FreeLocks

8         If ss1.EOF() And ss1.BOF Then
              'n�o ha cotacao pendente
9             ss1.Close
10            lngNUM_COTACAO = 0
11        Else
12            lngNUM_COTACAO = ss1!NUM_COTACAO
13        End If


14        If lngNUM_COTACAO = 0 Then
15            txtResposta = "0"
16            Unload frmVenda
17            frmListaCotacao.Show vbModal
18            If txtResposta <> "0" And txtResposta <> "" Then
                  'coloca cotacao em manuten��o

19                PLSQL = " Update cotacao set situacao=8,"
20                PLSQL = PLSQL & " cod_vend = " & CStr(sCOD_VEND)
21                PLSQL = PLSQL & " where num_cotacao = " & txtResposta
22                PLSQL = PLSQL & " and cod_loja = " & lngCOD_LOJA

23                dbAccess2.Execute PLSQL, dbFailOnError
24                FreeLocks

25                Unload frmVenda
26                Set frmVenda = Nothing
                  'ativa a venda
27                frmVenda.Show 1
28            End If
29            Screen.MousePointer = vbDefault
30        Else
31            MsgBox "Cota��o " & lngNUM_COTACAO & " est� pendente. Finalize-a.", vbExclamation, "Aten��o"
32        End If

33        Exit Sub

TrataErro:
34        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
35            Resume
36        Else
37            MsgBox "Sub mnuCotacaoManutencao_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
38        End If
End Sub

Private Sub mnuCotacaoPed_Click()
1         On Error GoTo TrataErro

          Dim ss As Snapshot

          Me.Tag = "CotacaoParaPedido"
          
          'verificar pedido pendente
2         SQL = "select NUM_PENDENTE "
3         SQL = SQL & "from PEDIDO "
4         SQL = SQL & "where FL_PEDIDO_FINALIZADO = 'N' and COD_VEND = " & CStr(sCOD_VEND)
5         Set ss = dbAccess.CreateSnapshot(SQL)
6         FreeLocks
7         If ss.EOF() And ss.BOF Then
              'n�o ha pedido pendente
8             lngNUM_PEDIDO = 0
9         Else
10            lngNUM_PEDIDO = ss!num_pendente
11        End If
12        ss.Close

13        If lngNUM_PEDIDO = 0 Then
15            txtResposta = "0"
16            Unload frmVenda
17            frmListaCotacao.Show vbModal
18            If txtResposta <> "0" And txtResposta <> "" Then

                  'gera pedido atrav�s de cotacao
19                frmGerarPedido.Show vbModal

20                If lngNUM_PEDIDO > 0 Then
                      'finalizar pedido
21                    txtResposta = "Gerar Pedido Atraves de Cotacao"

22                    frmFimPedido.Show vbModal
23                    txtResposta = ""
24                End If

25            End If
26            Screen.MousePointer = vbDefault
27        Else
28            MsgBox "Pedido " & lngNUM_PEDIDO & " pendente. Finalize-o.", vbExclamation, "Aten��o"
29        End If
30        FreeLocks

          Me.Tag = ""

31        Exit Sub

TrataErro:
32        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
33            Resume
34        Else
35            MsgBox "Sub mnuCotacaoPed_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
36        End If
End Sub


Private Sub mnuSair_Click()
    Set frmFabrica = Nothing
    Set frmVenda = Nothing
    Set frmAplicacao = Nothing
    Set frmGerarPedido = Nothing
    Set frmFimCotacao = Nothing
    Set frmFimPedido = Nothing
    Set frmAgenda = Nothing
    Set frmBanco = Nothing
    Set frmClienteFimPedido = Nothing
    Set frmListaCotacao = Nothing
    Set frmFornecedor = Nothing
    Set frmPlano = Nothing
    Set frmRepresentante = Nothing
    Set frmTransp = Nothing
    Set frmVendedor = Nothing
    End
End Sub


Private Sub mnuSobre_Click()
    frmSobre.Show 1
End Sub

Private Sub mnuVendas_Click()
    Screen.MousePointer = vbHourglass
    frmVenda.Show 1
    Screen.MousePointer = vbDefault
End Sub

Private Sub sscmdContatos_Click()
    frmContatos.Show vbModal
End Sub

Private Sub SSCommand1_Click()
    Call mnuVendas_Click
End Sub

Private Sub SSCommand2_Click()
    Call mnuCotacaoManutencao_Click
End Sub

Private Sub SSCommand3_Click()
    Call mnuAgenda_Click
End Sub

Private Sub SSCommand4_Click()
    Call mnuSair_Click
End Sub

Private Sub SSCommand5_Click()
    Call mnuCotacaoPed_Click
End Sub

Sub Atualizar_Vl_ICM_RETIDO()
    dbAccess.Execute "Update item_pedido set vl_icmretido = 0 where vl_icmretido is null"
End Sub

Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "DPK": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

