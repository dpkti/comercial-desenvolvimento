VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmClienteFimPedido 
   Caption         =   "Lista de Cliente"
   ClientHeight    =   3135
   ClientLeft      =   1170
   ClientTop       =   2085
   ClientWidth     =   7020
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3135
   ScaleWidth      =   7020
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtNomeCliente 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   3
      Top             =   1320
      Width           =   4215
   End
   Begin MSGrid.Grid grdCliente 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   3836
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "L_CLIFIM.frx":0000
   End
   Begin Bot�o.cmd cmdOk 
      Height          =   555
      Left            =   5640
      TabIndex        =   4
      ToolTipText     =   "OK"
      Top             =   2580
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   979
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "L_CLIFIM.frx":001C
      PICN            =   "L_CLIFIM.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   555
      Left            =   6360
      TabIndex        =   5
      ToolTipText     =   "Sair"
      Top             =   2580
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   979
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "L_CLIFIM.frx":0D12
      PICN            =   "L_CLIFIM.frx":0D2E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblNomeCliente 
      AutoSize        =   -1  'True
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   720
      TabIndex        =   6
      Top             =   1320
      Width           =   600
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   5175
   End
End
Attribute VB_Name = "frmClienteFimPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String


Private Sub cmdOK_Click()
    On Error GoTo TrataErro


    Dim ss As Snapshot
    Dim i As Integer
    Dim j As Integer

    'mouse
    Screen.MousePointer = vbHourglass

    'nome do form
    frmClienteFimPedido.Caption = frmClienteFimPedido.Caption

    'montar SQL
    sql = "select cli.COD_CLIENTE as cod_cliente,"
    sql = sql & "  cli.NOME_CLIENTE as nome_cliente,"
    sql = sql & " cid.nome_cidade as nome_cidade,"
    sql = sql & " cid.cod_uf as cod_uf"
    sql = sql & " from CLIENTE cli,CIDADE cid"
    sql = sql & " where cli.COD_CIDADE = cid.COD_CIDADE and"
    sql = sql & " cli.SITUACAO = 0"
    sql = sql & " and NOME_CLIENTE like '" & txtNomeCliente.Text & "'"
    sql = sql & " order by cli.NOME_CLIENTE"

    'criar consulta

    Set ss = dbAccess2.CreateSnapshot(sql)
    FreeLocks

    If ss.EOF And ss.BOF Then
        ss.Close
        txtResposta = "0"
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha cliente cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If


    'carrega dados
    With grdCliente
        .Cols = 4
        .Rows = ss.RecordCount + 1
        j = .Rows - 1
        .ColWidth(0) = 660
        .ColWidth(1) = 3800
        .ColWidth(2) = 1800
        .ColWidth(3) = 550

        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Nome da Cliente"
        .Col = 2
        .Text = "Cidade"
        .Col = 3
        .Text = "UF"


        For i = 1 To j
            .Row = i
            .Col = 0
            .Text = ss("COD_CLIENTE")
            .Col = 1
            .Text = ss("NOME_CLIENTE")
            .Col = 2
            .Text = ss("NOME_CIDADE")
            .Col = 3
            .Text = ss("COD_UF")
            ss.MoveNext
        Next
        .Row = 0
        .FixedRows = 1
    End With


    'mouse
    Screen.MousePointer = vbDefault

    'desabilitar botao/label/textboxes combo
    cmdOk.Visible = False

    lblNomeCliente.Visible = False
    txtNomeCliente.Visible = False


    'habilitar label de pesquisa e grid
    lblPesquisa.Visible = True
    grdCliente.Visible = True
    ss.Close
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
        FreeLocks
        DoEvents
        Resume
    Else
        MsgBox "Sub cmdOK_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmClienteFimPedido = Nothing
End Sub


Private Sub Form_Load()

    'nome do cliente
    frmClienteFimPedido.txtNomeCliente.Text = ""

End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmClienteFimPedido = Nothing
End Sub


Private Sub grdCliente_DblClick()
    grdCliente.Col = 0
    txtResposta = grdCliente.Text
    Unload Me
End Sub

Private Sub grdCliente_KeyPress(KeyAscii As Integer)
    
    On Error GoTo Trata_Erro
    
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer

    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdCliente.Col = 1
    If KeyAscii = 13 Then
        grdCliente.Col = 0
        txtResposta = grdCliente.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdCliente.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdCliente
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If

    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdCliente.Row = 1
        SendKeys "{LEFT}{RIGHT}"

    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdCliente
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdCliente.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With

                End If
            End If
        Else
            Beep
        End If
    End If

    'mouse
    Screen.MousePointer = vbDefault

Trata_Erro:
    If Err.Number = 93 Then
        MsgBox "Verifique se o crit�rio digitado est� correto.", vbInformation, "Aten��o"
        Exit Sub
    ElseIf Err.Number <> 0 Then
       MsgBox "Sub grdCliente_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub txtNomeCliente_Change()
    If txtNomeCliente.Text = "" Then
        cmdOk.Enabled = False
    Else
        cmdOk.Enabled = True
    End If
End Sub


Private Sub txtNomeCliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdOK_Click
        
    KeyAscii = Texto(KeyAscii)
End Sub

