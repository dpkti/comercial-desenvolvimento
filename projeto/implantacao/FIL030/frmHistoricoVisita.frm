VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmHistoricoVisita 
   Caption         =   ":: Hist�rico das Visitas ::"
   ClientHeight    =   2805
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11025
   Icon            =   "frmHistoricoVisita.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2805
   ScaleWidth      =   11025
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   2550
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lsvHistoricoAgenda 
      Height          =   1725
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   10905
      _ExtentX        =   19235
      _ExtentY        =   3043
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   8
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Cod Cliente"
         Object.Width           =   2363
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Cliente"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Dt Agendada"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Pseudonimo"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   4
         Text            =   "Dt_Resultado"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Status"
         Object.Width           =   4480
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   6
         Text            =   "Comentario Promotor"
         Object.Width           =   5362
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Comentario Gerente"
         Object.Width           =   5362
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   705
      Left            =   10230
      TabIndex        =   1
      ToolTipText     =   "Sair"
      Top             =   1800
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmHistoricoVisita.frx":08CA
      PICN            =   "frmHistoricoVisita.frx":08E6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmHistoricoVisita"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Load()
      Dim sHistVisita As Snapshot
      Dim Litem As ListItem
1     On Error GoTo TrataErro

2         SQL = "SELECT CLIENTE.COD_CLIENTE,"
3         SQL = SQL & " CLIENTE.NOME_CLIENTE,"
4         SQL = SQL & " REPRESENTANTE.pseudonimo,"
5         SQL = SQL & " VISITA_PROMOTOR_HIST.DT_VISITA_AGENDA,"
6         SQL = SQL & " VISITA_PROMOTOR_HIST.DT_RESULTADO,"
7         SQL = SQL & " STATUS_VISITA.STATUS,"
8         SQL = SQL & " VISITA_PROMOTOR_HIST.COMENTARIO_PROMOTOR,"
9         SQL = SQL & " VISITA_PROMOTOR_HIST.COMENTARIO_GERENTE"
10        SQL = SQL & " FROM (CLIENTE INNER JOIN (VISITA_PROMOTOR_HIST LEFT JOIN STATUS_VISITA ON VISITA_PROMOTOR_HIST.COD_STATUS = STATUS_VISITA.COD_STATUS) ON CLIENTE.CGC = VISITA_PROMOTOR_HIST.CGC) INNER JOIN REPRESENTANTE ON VISITA_PROMOTOR_HIST.COD_PROMOTOR = REPRESENTANTE.cod_repres"
11        SQL = SQL & " WHERE CLIENTE.COD_CLIENTE= " & frmVenda.txtCOD_CLIENTE.Text
12        Set sHistVisita = dbAccess2.OpenRecordset(SQL)
13        If sHistVisita.EOF = False Then
14            sHistVisita.MoveLast
15            sHistVisita.MoveFirst
             
16            For i = 0 To sHistVisita.RecordCount - 1
17                Set Litem = lsvHistoricoAgenda.ListItems.Add
18                Litem = "" & sHistVisita!Cod_cliente
19                Litem.SubItems(1) = "" & UCase(sHistVisita!NOME_CLIENTE)
20                Litem.SubItems(2) = "" & UCase(sHistVisita!DT_VISITA_AGENDA)
21                Litem.SubItems(3) = "" & UCase(sHistVisita!pseudonimo)
22                Litem.SubItems(4) = "" & UCase(sHistVisita!DT_RESULTADO)
23                Litem.SubItems(5) = "" & UCase(sHistVisita!Status)
24                Litem.SubItems(6) = "" & UCase(sHistVisita!COMENTARIO_PROMOTOR)
25                Litem.SubItems(7) = "" & UCase(sHistVisita!COMENTARIO_GERENTE)
26                sHistVisita.MoveNext
27            Next
28        End If
29        Set sHistVisita = Nothing
TrataErro:
30        If Err.Number <> 0 Then
31            MsgBox "Sub Form_load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
32        End If
End Sub
