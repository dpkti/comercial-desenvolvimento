VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmContatos 
   Caption         =   "Contatos Telef�nicos - Gera��o de Arquivo"
   ClientHeight    =   1185
   ClientLeft      =   3480
   ClientTop       =   3180
   ClientWidth     =   4815
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1185
   ScaleWidth      =   4815
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtData 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1680
      TabIndex        =   0
      Top             =   360
      Width           =   1365
   End
   Begin Bot�o.cmd cmdArquivo 
      Height          =   375
      Left            =   1680
      TabIndex        =   2
      Top             =   750
      Width           =   1395
      _ExtentX        =   2461
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "Gerar Arquivo"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmContatos.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Digite a data:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   1680
      TabIndex        =   1
      Top             =   150
      Width           =   1170
   End
End
Attribute VB_Name = "frmContatos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdArquivo_Click()

1         On Error GoTo Trata_Erro

          '-- Gera arquivo texto para ser enviado via e-mail para T�nia (acess�rios CPS)
          Dim Data As String
          Dim Tipo_Codigo As String
          Dim Codigo As Double
          Dim Resultado As String
          Dim Tipo_Ligacao As String

2         If MsgBox("Confirma gera��o do arquivo?", vbYesNo, "FIL030") = vbYes Then

3             Screen.MousePointer = 11

              '-- Gera arquivo com o nome CONTDDMMAAHHMMSS.TXT NO DIRET�RIO CONTATOS
4             Data = Format(txtData.Text, "DD/MM/YY")

5             sql = "Select a.cod_cliente, a.cod_vend, a.tipo_ligacao, a.cod_resultado, "
6             sql = sql & "c.desc_resultado, a.tempo_ligacao, b.cod_filial, b.cod_repres "
7             sql = sql & "From contato a, deposito b, resultado c "
8             sql = sql & "Where a.cod_resultado = c.cod_resultado and "
9             sql = sql & "cdate(a.dt_contato) = #" & Format(Data, "mm/dd/yyyy") & "#"

10            Set ss4 = dbAccess2.CreateSnapshot(sql)
11            FreeLocks

12            If Val(ss4!Cod_Repres) = 0 Then
13                Tipo_Codigo = "P"
14                Codigo = ss4!Cod_Filial
15            Else

                  Tipo_Codigo = "R"
                  Codigo = ss4!cod_repres
                  
                  'RTI-142 - Verificando o Tipo de Usu�rio
                  SQL = "Select r.tipo "
                  SQL = SQL & "From representante r "
                  SQL = SQL & "Where r.cod_repres = " & ss4!cod_repres

                  Set ss5 = dbAccess2.CreateSnapshot(SQL)
                  FreeLocks
                
                  If Not s5.EOF Then
                    If ss5!TIPO = "G" Then
                        Tipo_Codigo = "G"
                    End If
                  End If
16
18            End If

19            If Dir("C:\CONTATOS", vbDirectory) = "" Then MkDir "C:\CONTATOS"

20            Open "C:\CONTATOS\CONT" & Tipo_Codigo & Codigo & Mid(Data, 1, 2) & Mid(Data, 4, 2) & _
                      Mid(Data, 7, 2) & ".txt" For Output As #1

 			  'RTI-142 - Definindo o nome do relat�rio para o tipo do usu�rio
21            If TIPO = P Then
22                Print #1, "RELAT�RIO DE CONTATOS TELEF�NICOS - PAAC: " & Codigo & " - " & Data
              ElseIf TIPO = G Then
                  Print #1, "RELAT�RIO DE CONTATOS TELEF�NICOS - GERENTE: " & Codigo & " - " & Data
23            Else
24                Print #1, "RELAT�RIO DE CONTATOS TELEF�NICOS - REPRES.: " & Codigo & " - " & Data
25            End If

26            Print #1, ""
27            Print #1, "C�D.CLIENTE;C�D.VENDEDOR;TP.LIGA��O;C�D.RESULTADO;TEMPO LIGA��O"
28            Print #1, ""

29            Do While Not ss4.EOF

                  '-- GRAVAR LINHA DE DETALHE
30                lstrLinha = ss4!Cod_cliente & ";" & ss4!cod_vend & ";"
31                If ss4!Tipo_Ligacao = 1 Then
32                    Tipo_Ligacao = "1 - ATIVA"
33                Else
34                    Tipo_Ligacao = "2 - RECEPTIVA"
35                End If
36                Resultado = ss4!cod_resultado & " - " & ss4!desc_resultado
37                lstrLinha = lstrLinha & Tipo_Ligacao & ";" & Resultado & ";"
38                lstrLinha = lstrLinha & ss4!tempo_ligacao

39                Print #1, lstrLinha

40                ss4.MoveNext
41            Loop

42        End If

43        Close #1

44        MsgBox "Arquivo gerado com sucesso! Favor enviar por e-mail.", vbInformation, "Aten��o!"

45        txtData.Text = ""

Trata_Erro:
46        If Err.Number = 3021 Then
47           MsgBox "N�o existem registros de contato.", vbInformation, "Aten��o"
48           Exit Sub
49        ElseIf Err.Number <> 0 Then
50           MsgBox "Sub cmdArquivo_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
51        End If
52        Screen.MousePointer = 0
End Sub



Private Sub txtData_KeyPress(KeyAscii As Integer)
    Call Data(KeyAscii, frmContatos.txtData)
End Sub


