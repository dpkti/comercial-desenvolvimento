VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmFIL500 
   Caption         =   "FIL500 - Leitura de Dicas e Visitas"
   ClientHeight    =   8115
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11415
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFIL500.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8115
   ScaleWidth      =   11415
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraVisitas 
      Caption         =   "Visitas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   0
      TabIndex        =   13
      Top             =   3300
      Width           =   11415
      Begin VB.ListBox lstVisitas 
         Appearance      =   0  'Flat
         Height          =   3345
         Left            =   60
         TabIndex        =   14
         Top             =   420
         Width           =   1665
      End
      Begin MSComctlLib.ListView lsvVisitas 
         Height          =   3360
         Left            =   1770
         TabIndex        =   15
         Top             =   420
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   5927
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "CGC"
            Object.Width           =   2999
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Dt Visita Agenda"
            Object.Width           =   3351
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Promotor"
            Object.Width           =   1676
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Dt Resultado"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "C�d Resultado"
            Object.Width           =   2293
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Coment�rio"
            Object.Width           =   4410
         EndProperty
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Registros do Arquivo"
         Height          =   195
         Left            =   1770
         TabIndex        =   17
         Top             =   210
         Width           =   1500
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Arquivos"
         Height          =   195
         Left            =   90
         TabIndex        =   16
         Top             =   210
         Width           =   630
      End
   End
   Begin VB.Frame fraDicas 
      Caption         =   "Dicas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   0
      TabIndex        =   8
      Top             =   30
      Width           =   11415
      Begin VB.ListBox lstDicas 
         Appearance      =   0  'Flat
         Height          =   2760
         Left            =   60
         TabIndex        =   10
         Top             =   420
         Width           =   1665
      End
      Begin MSComctlLib.ListView lsv 
         Height          =   2760
         Left            =   1770
         TabIndex        =   9
         Top             =   420
         Width           =   9585
         _ExtentX        =   16907
         _ExtentY        =   4868
         View            =   3
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   10
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "A��o"
            Object.Width           =   970
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "PAAC"
            Object.Width           =   1323
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Repres"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Seq"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Cliente"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "C�d Vend"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Data Cria��o"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   7
            Text            =   "Data Altera��o"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   8
            Text            =   "Dicas"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(10) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   9
            Text            =   "Tipo"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Arquivos"
         Height          =   195
         Left            =   90
         TabIndex        =   12
         Top             =   210
         Width           =   630
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Registros do Arquivo"
         Height          =   195
         Left            =   1770
         TabIndex        =   11
         Top             =   210
         Width           =   1500
      End
   End
   Begin VB.TextBox txtCaminhoVisitas 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   3840
      TabIndex        =   6
      Top             =   7410
      Width           =   3795
   End
   Begin VB.TextBox txtOUT 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   7680
      TabIndex        =   4
      Top             =   7410
      Width           =   2625
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   345
      Left            =   0
      TabIndex        =   3
      Top             =   7770
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11748
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   5292
            MinWidth        =   5292
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
         EndProperty
      EndProperty
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   10770
      Top             =   30
   End
   Begin VB.TextBox txtCaminho 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   30
      TabIndex        =   1
      Top             =   7410
      Width           =   3765
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   10470
      TabIndex        =   0
      Top             =   7320
      Width           =   885
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Caminho dos arquivos de Visitas:"
      Height          =   195
      Left            =   3840
      TabIndex        =   7
      Top             =   7200
      Width           =   2355
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Caminho do OUT"
      Height          =   195
      Left            =   7680
      TabIndex        =   5
      Top             =   7200
      Width           =   1200
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Caminho dos arquivos de Dicas:"
      Height          =   195
      Left            =   30
      TabIndex        =   2
      Top             =   7200
      Width           =   2280
   End
End
Attribute VB_Name = "frmFIL500"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vSessao As Object
Dim vBanco As Object
Dim Y As Integer
Dim vTempo As Byte 'Tempo em minutos para proxima execucao
Dim vTempoAtual As Byte 'Tempo em minutos


Private Sub cmdSair_Click()
    
    Set vSessao = Nothing
    Set vBanco = Nothing
    
    End
    
End Sub


Private Sub Form_Load()
        
    Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
        
    Me.Visible = True
        
    StatusBar1.Panels(1).Text = "Carregando parametros... Aguarde..."
    
    Me.Refresh
    
    Set vSessao = CreateObject("oracleinproCServer.xorasession")
    Set vBanco = vSessao.OpenDatabase("PRODUCAO", "FIL500/DPK500", 0&)
    'Set vBanco = vSessao.OpenDatabase("SDPKT", "FIL500/DPK500", 0&)
    
    Me.StatusBar1.Panels(3).Text = vBanco.DatabaseName
    
    Pegar_parametros
        
    StatusBar1.Panels(1).Text = "Carregando Arquivos... Aguarde..."
    
    Me.Refresh
        
    Carregando_Arquivos_Dicas
    Carregando_Arquivos_Visitas
    Gravar_Banco_Producao
    
    StatusBar1.Panels(2).Text = "Ler novamente em: 5 minutos"
    
    StatusBar1.Panels(1).Text = "Pronto"
        
    Set vBanco = Nothing
    Set vSessao = Nothing
    
        
End Sub

Sub Carregando_Arquivos_Dicas()

1         On Error GoTo Trata_Erro

          Dim fso As New FileSystemObject
          Dim Pasta As Folder
          Dim Arquivo As File
          
2         If Trim(txtCaminho) = "" Then
3             MsgBox "Informe o caminho dos arquivos de Dicas.", vbInformation, "Aten��o"
4             Exit Sub
5         End If
          
6         Me.lstDicas.Clear
          
7         Set Pasta = fso.GetFolder(txtCaminho)

8         For Each Arquivo In Pasta.Files
              DoEvents
9             If Left(UCase(Arquivo.Name), 8) = "PEDDICAS" And Right(UCase(Arquivo.Name), 3) = "TXT" Then
10                lstDicas.AddItem Arquivo.Name
11            End If
12        Next
          
13        Set fso = Nothing
14        Set Pasta = Nothing
15        Set Arquivo = Nothing
          
16        If Me.lstDicas.ListCount > 0 Then Ler_Arquivo
          
Trata_Erro:
17        If Err.Number <> 0 Then
18            MsgBox "Sub Carregando_Arquivos" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
19        End If
          
End Sub

Sub Carregando_Arquivos_Visitas()

1         On Error GoTo Trata_Erro

          Dim fso As New FileSystemObject
          Dim Pasta As Folder
          Dim Arquivo As File
          
2         If Trim(txtCaminhoVisitas) = "" Then
3             MsgBox "Informe o caminho dos arquivos de Visitas.", vbInformation, "Aten��o"
4             Exit Sub
5         End If
          
6         lstVisitas.Clear
          
7         Set Pasta = fso.GetFolder(txtCaminhoVisitas)

8         For Each Arquivo In Pasta.Files
9             If Left(UCase(Arquivo.Name), 8) = "PEDVISIT" And Right(UCase(Arquivo.Name), 3) = "TXT" Then
10                lstVisitas.AddItem Arquivo.Name
11            End If
12        Next
          
13        Set fso = Nothing
14        Set Pasta = Nothing
15        Set Arquivo = Nothing
          
16        If lstVisitas.ListCount > 0 Then Ler_Arquivo_Visitas
          
Trata_Erro:
17        If Err.Number <> 0 Then
18            MsgBox "Sub Carregando_Arquivos" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
19        End If
          
End Sub


Sub Ler_Arquivo()

1         On Error GoTo Trata_Erro

          Dim vArq As Integer
          Dim vArquivo As String
          Dim vLinha As String
          Dim i As Integer
          Dim ii As Integer
          Dim iii As Integer
          Dim lItem As ListItem
          Dim vTipo As String
          Dim vData As String
          Dim vCod As Long
          
2         vArq = FreeFile
          
3         lsv.ListItems.Clear
          
4         For i = 0 To lstDicas.ListCount - 1

5             DoEvents

6             vArquivo = lstDicas.List(i)
7             vCod = Val(Mid(vArquivo, 10, 4))
8             vTipo = Mid(vArquivo, 9, 1)
              
9             Me.StatusBar1.Panels(1).Text = "Lendo arquivo: " & vArquivo
              
10            Open txtCaminho & IIf(Right(txtCaminho, 1) = "\", "", "\") & vArquivo For Input As #vArq
                  
11            Do While Not EOF(vArq)
12                Line Input #vArq, vLinha
                                  
13                If (Mid(vLinha, 1, 1) = "I" Or Mid(vLinha, 1, 1) = "U") And Trim(Mid(vLinha, 51)) = "" Then
                                                      
14                Else
15                  Set lItem = Me.lsv.ListItems.Add
16                  lItem = Mid(vLinha, 1, 1)                                 'Acao
17                  lItem.SubItems(1) = IIf(vTipo = "P", vCod, "")            'PAAC
18                  lItem.SubItems(2) = IIf(vTipo = "R", vCod, "")            'Repres
19                  lItem.SubItems(3) = Val(Mid(vLinha, 2, 9))                'Sequencia
20                  lItem.SubItems(4) = Val(Mid(vLinha, 11, 6))               'Cliente
21                  lItem.SubItems(5) = Val(Mid(vLinha, 46, 5))               'Vend
22                  lItem.SubItems(6) = Formata_data_HORA(Mid(vLinha, 17, 14)) 'Data
23                  lItem.SubItems(7) = Formata_data_HORA(Mid(vLinha, 31, 14)) 'Data
24                  lItem.SubItems(8) = Mid(vLinha, 51)                       'Dica
25                End If
26            Loop

27            Close vArq

28        Next

29        Me.Visible = True

          'Gravar no Banco todos os registros que estiverem com Status OK
30        Gravar_Banco
          
          'Os registros que estiverem com Status de Erro
31        Gerar_Arquivo_Retorno

32        Kill txtCaminho & IIf(Right(txtCaminho, 1) = "\", "", "\") & vArquivo

Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub Ler_Arquivo" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
35        End If
End Sub

Sub Ler_Arquivo_Visitas()

    On Error GoTo Trata_Erro

    Dim vArq As Integer
    Dim vArquivo As String
    Dim vLinha As String
    Dim vL As String
    Dim intNum As String
    Dim i As Integer
    Dim iii As Integer
    
    Dim vTipo As String
    Dim vData As String
    Dim vCod As Long
    Dim strConcatena As String
    
    vArq = FreeFile
    
    lsvVisitas.ListItems.Clear
    
    For i = 0 To lstVisitas.ListCount - 1

        DoEvents

        vArquivo = lstVisitas.List(i)
        vCod = Val(Mid(vArquivo, 10, 4))
        vTipo = Mid(vArquivo, 9, 1)
        
        Me.StatusBar1.Panels(1).Text = "Lendo arquivo: " & vArquivo
        
        Open txtCaminhoVisitas & IIf(Right(txtCaminhoVisitas, 1) = "\", "", "\") & vArquivo For Input As #vArq
            
        If EOF(vArq) Then
            Close vArq
            Kill txtCaminhoVisitas & IIf(Right(txtCaminhoVisitas, 1) = "\", "", "\") & vArquivo
        Else
            Do While Not EOF(vArq)
                Line Input #vArq, vLinha
                intNum = Val(Trim(Mid(vLinha, 1, 14)))
                If Val(intNum) <> 0 Then
                    'If Y > 0 Then
                        Call preencherGrid(vLinha)
                    'End If
                    'vL = vLinha
                Else
                    'vL = vL & vLinha
                End If
                'Y = Y + 1
            Loop
            Close vArq
            Kill txtCaminhoVisitas & IIf(Right(txtCaminhoVisitas, 1) = "\", "", "\") & vArquivo
        End If
    Next

    'Gravar no Banco todos os registros que estiverem com Status OK
    Gravar_Banco_Visitas
    
Trata_Erro:
    If Err.Number = 53 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub Ler_Arquivo_Visitas" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
    End If
    
End Sub

Sub preencherGrid(pLinha As String)
    Dim lItem As ListItem
    Set lItem = Me.lsvVisitas.ListItems.Add
    lItem = Mid(pLinha, 1, 14)                                 'CGC
    lItem.SubItems(1) = Formata_data(Mid(pLinha, 15, 8))       'Dt Agendada
    lItem.SubItems(2) = Val(Mid(pLinha, 23, 4))                'C�d Promotor
    lItem.SubItems(3) = Formata_data(Mid(pLinha, 27, 8)) 'Data do Resultado
    lItem.SubItems(4) = Val(Mid(pLinha, 35, 2))                'Status
    lItem.SubItems(5) = Mid(pLinha, 37)                        'Resultado
    Y = 0
End Sub




Private Sub Timer1_Timer()
          
1         On Error GoTo Trata_Erro
          
2         vTempoAtual = vTempoAtual + 1
          
3         If fTempo(5) = True Then
4             vTempoAtual = 0
              
5             Set vSessao = CreateObject("oracleinproCServer.xorasession")
6             Set vBanco = vSessao.OpenDatabase("PRODUCAO", "FIL500/DPK500", 0&)
              
7             Carregando_Arquivos_Dicas
8             Carregando_Arquivos_Visitas
9             Gravar_Banco_Producao
          
              ExcluirBind
          
10            Set vBanco = Nothing
11            Set vSessao = Nothing
              
12        End If
          
13        StatusBar1.Panels(2).Text = "Rodar novamente em " & 5 - vTempoAtual & " minutos"
          
Trata_Erro:
14        If Err.Number <> 0 Then
15            MsgBox "Sub Timer1_Timer" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
16        End If
          
End Sub

'pTempo � em Minutos
Function fTempo(pTempo As Byte) As Boolean

    If vTempoAtual >= pTempo Then
        fTempo = True
    Else
        fTempo = False
    End If

End Function

'ler o Listview registro a registro e inserir na tabela
'Se por algum motivo der erro, nao inserir na tabela TMP e
'colocar a palavra erro na coluna status do listview
Sub Gravar_Banco()

1         On Error GoTo Trata_Erro

2         Me.StatusBar1.Panels(3).Text = vBanco.DatabaseName
          
          Dim i As Integer
3         For i = 1 To Me.lsv.ListItems.Count
          
4             vBanco.Parameters.Remove "PM_SEQ"
5             vBanco.Parameters.Add "PM_SEQ", Val(lsv.ListItems(i).SubItems(3)), 1
              
6             vBanco.Parameters.Remove "PM_CLIENTE"
7             vBanco.Parameters.Add "PM_CLIENTE", Val(lsv.ListItems(i).SubItems(4)), 1
              
8             vBanco.Parameters.Remove "PM_DT_DICA"
9             vBanco.Parameters.Add "PM_DT_DICA", CDate(lsv.ListItems(i).SubItems(6)), 1
              
10            vBanco.Parameters.Remove "PM_VEND"
11            vBanco.Parameters.Add "PM_VEND", lsv.ListItems(i).SubItems(5), 1
              
12            vBanco.Parameters.Remove "PM_DICA"
13            vBanco.Parameters.Add "PM_DICA", lsv.ListItems(i).SubItems(8), 1
              
14            vBanco.Parameters.Remove "PM_TIPO"
15            vBanco.Parameters.Add "PM_TIPO", lsv.ListItems(i), 1
              
16            vBanco.ExecuteSQL "Begin VENDAS.PR_DICAS_TMP(:PM_SEQ, :PM_CLIENTE, :PM_DT_DICA, :PM_VEND, :PM_DICA, :PM_TIPO);END;"
          
17        Next
          
18            vBanco.Parameters.Remove "PM_SEQ"
19            vBanco.Parameters.Remove "PM_CLIENTE"
20            vBanco.Parameters.Remove "PM_DT_DICA"
21            vBanco.Parameters.Remove "PM_VEND"
22            vBanco.Parameters.Remove "PM_DICA"
23            vBanco.Parameters.Remove "PM_TIPO"
          
Trata_Erro:
24        If Err.Number = 440 And InStr(UCase(Err.Description), "UNIQUE") > 0 Then
25            Resume Next
26        ElseIf Err.Number <> 0 Then
27            MsgBox "Sub Gravar_Banco" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
28        End If
End Sub

'ler o Listview registro a registro e inserir na tabela
'Se por algum motivo der erro, nao inserir na tabela TMP e
'colocar a palavra erro na coluna status do listview
Sub Gravar_Banco_Visitas()

1         On Error GoTo Trata_Erro

          
          Dim i As Integer
2         For i = 1 To lsvVisitas.ListItems.Count
          
3             vBanco.Parameters.Remove "PM_PROMOTOR"
4             vBanco.Parameters.Add "PM_PROMOTOR", Val(lsvVisitas.ListItems(i).SubItems(2)), 1
              
5             vBanco.Parameters.Remove "PM_DT_AGENDA"
6             vBanco.Parameters.Add "PM_DT_AGENDA", CDate(lsvVisitas.ListItems(i).SubItems(1)), 1
              
7             vBanco.Parameters.Remove "PM_CNPJ"
8             vBanco.Parameters.Add "PM_CNPJ", Val(lsvVisitas.ListItems(i)), 1
              
9             vBanco.Parameters.Remove "PM_STATUS"
10            vBanco.Parameters.Add "PM_STATUS", lsvVisitas.ListItems(i).SubItems(4), 1
              
11            vBanco.Parameters.Remove "PM_COMENTARIO"
12            vBanco.Parameters.Add "PM_COMENTARIO", Mid(Trim(lsvVisitas.ListItems(i).SubItems(5)), 1, 300), 1
              
13            vBanco.ExecuteSQL "Begin VENDAS.PR_VISITAS(:PM_PROMOTOR, :PM_DT_AGENDA, :PM_CNPJ, :PM_STATUS, :PM_COMENTARIO);END;"
          
14        Next
          
15        vBanco.Parameters.Remove "PM_PROMOTOR"
16        vBanco.Parameters.Remove "PM_DT_AGENDA"
17        vBanco.Parameters.Remove "PM_CNPJ"
18        vBanco.Parameters.Remove "PM_STATUS"
19        vBanco.Parameters.Remove "PM_COMENTARIO"


Trata_Erro:
20        If Err.Number = 440 And InStr(UCase(Err.Description), "UNIQUE") > 0 Then
21            Resume Next
22        ElseIf Err.Number <> 0 Then
23            MsgBox "Sub Gravar_Banco_Visitas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
24        End If

End Sub


Sub Gerar_Arquivo_Retorno()
          
1         On Error GoTo Trata_Erro
          
          Dim i As Integer
          Dim vArq As Integer
          
2         If Me.lsv.ListItems.Count = 0 Then Exit Sub
          
3         vArq = FreeFile
          
4         Gerar_hdr IIf(lsv.ListItems(1).SubItems(1) <> "", "PAAC" & Format(lsv.ListItems(1).SubItems(1), "0000"), "REPR" & Format(lsv.ListItems(1).SubItems(2), "0000")), Format(IIf(lsv.ListItems(1).SubItems(1) <> "", lsv.ListItems(1).SubItems(1), lsv.ListItems(1).SubItems(2)), "0000"), IIf(lsv.ListItems(1).SubItems(1) <> "", "P", "R")
          
5         Open txtOUT & IIf(Right(txtOUT, 1) = "\", "", "\") & "PEDDICAS" & IIf(lsv.ListItems(1).SubItems(1) <> "", "P", "R") & Format(IIf(lsv.ListItems(1).SubItems(1) <> "", lsv.ListItems(1).SubItems(1), Me.lsv.ListItems(1).SubItems(2)), "0000") & ".TXT" For Output As #vArq
          
6         For i = 1 To lsv.ListItems.Count
              
7             Print #vArq, lsv.ListItems(i) & Format(lsv.ListItems(i).SubItems(1) & lsv.ListItems(i).SubItems(2), "0000") & Format(Val(lsv.ListItems(i).SubItems(3)), "000000000") & Format(Val(lsv.ListItems(i).SubItems(4)), "000000") & Format(lsv.ListItems(i).SubItems(5), "000000") & Format(CDate(lsv.ListItems(i).SubItems(6)), "YYYYMMDDHHMMSS") & Format(CDate(lsv.ListItems(i).SubItems(7)), "YYYYMMDDHHMMSS")
              
8         Next
          
9         Close #vArq
          
Trata_Erro:
10        If Err.Number <> 0 Then
11            MsgBox "Sub Gerar_Arquivo_Retorno" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
12        End If
          
End Sub

Sub Gerar_hdr(pTo As String, pRepres As String, pTipo As String)
          
1         On Error GoTo Trata_Erro
          
          Dim vArq As Integer
          
2         vArq = FreeFile
               
3         Open txtOUT & IIf(Right(txtOUT, 1) = "\", "", "\") & "PEDDICAS" & pTipo & pRepres & ".HDR" For Output As #vArq
                  
4         Print #vArq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
5         Print #vArq, "TO        =" & pTo
6         Print #vArq, "FROM      =DPKPEDID"
7         Print #vArq, "DESCRICAO =" & "#PEDDICAS" & pTipo & pRepres
8         Print #vArq, "FILE      =" & "PEDDICAS" & pTipo & pRepres & ".TXT"
                  
9         Close #vArq
          
Trata_Erro:
10        If Err.Number <> 0 Then
11            MsgBox "Sub Gerar_Hdr" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
12        End If

          
End Sub

Function Formata_data_HORA(pData) As String
    Dim vData As String
    Dim vHora As String
    
    vData = Mid(pData, 1, 8)
    
    vData = Right(vData, 2) & "/" & Mid(vData, 5, 2) & "/" & Mid(vData, 1, 4)
    
    vHora = Right(pData, 6)
    
    vHora = Mid(vHora, 1, 2) & ":" & Mid(vHora, 3, 2) & ":" & Right(vHora, 2)
    
    Formata_data_HORA = vData & " " & vHora
    
End Function

Function Formata_data(pData) As String
    Dim vData As String
    Dim vHora As String
    
    vData = Mid(pData, 1, 8)
    
    vData = Right(vData, 2) & "/" & Mid(vData, 5, 2) & "/" & Mid(vData, 1, 4)
    
    Formata_data = vData
    
End Function

Sub Pegar_parametros()
    
    On Error GoTo Trata_Erro
    
    Dim vCod As Long
    Dim vObj As Object
    
    Set vObj = vBanco.createdynaset("Select * from helpdesk.software where nome_software = 'FIL500'", 0&)
    vCod = vObj!Cod_Software
    
    Set vObj = vBanco.createdynaset("Select * from helpdesk.parametros where cod_software = " & vCod & " and nome_parametro = 'CAMINHO_DICAS'", 0&)
    txtCaminho = vObj!vl_parametro
    'txtCaminho = "C:\IN\DICAS"
    
    Set vObj = vBanco.createdynaset("Select * from helpdesk.parametros where cod_software = " & vCod & " and nome_parametro = 'CAMINHO_VISITAS'", 0&)
    txtCaminhoVisitas = vObj!vl_parametro
    'txtCaminhoVisitas = "C:\IN\VISITAS"
    
    Set vObj = vBanco.createdynaset("Select * from helpdesk.parametros where cod_software = " & vCod & " and nome_parametro = 'CAMINHO_OUT'", 0&)
    txtOUT = vObj!vl_parametro
    'txtOUT = "C:\OUT"
        
    Set vObj = Nothing
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Pegar_parametros" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If
    
End Sub

Sub ExcluirBind()

    Dim vIndice

    For vIndice = vBanco.Parameters.Count To 0 Step -1

        vBanco.Parameters.Remove (vIndice)

    Next

End Sub

'Pegar os registros da Dicas_tmp e inserir na Dicas da producao
Sub Gravar_Banco_Producao()

1         On Error GoTo Trata_Erro

5         vBanco.ExecuteSQL "Begin VENDAS.PR_DICAS();END;"
          
Trata_Erro:
8         If Err.Number = 440 And InStr(UCase(Err.Description), "UNIQUE") > 0 Then
9             Resume Next
10        ElseIf Err.Number <> 0 Then
11            MsgBox "Sub Gravar_Banco_Producao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
12        End If
End Sub
