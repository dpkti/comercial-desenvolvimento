Attribute VB_Name = "modFIL050OUT"
Option Explicit

Sub Inicio_FIL050OUT()
        
    Dim num_arq As Integer
    Dim strEmail_To As String
    Dim strEmail_From As String
    Dim i As Integer
    Dim vConsulta As String
    Dim Fl_Aberto As String
    Dim SS3 As Object
    
    Pegar_Drive 'vPath_Drv = Left(App.Path, 2)
    
   '******************************************
   'busca informacoes sobre FTP origem/destino
   '******************************************
    Set dbaccess2 = OpenDatabase(vPath_Drv & "\COM\" & "DBMAIL.MDB")
    
    SQL = "SELECT DESTINO as EMAIL_TO, USUARIO as EMAIL_FROM FROM USUARIO "
    
    Set SS = dbaccess2.CreateSnapshot(SQL)
    FreeLocks
    
    vEmail_To = SS!email_to
    vEmail_From = SS!email_from
    
    SS.Close
    dbaccess2.Close
    
    
   '**********************************************
   'monta arquivo de consulta de pedidos em aberto
   '**********************************************
    Set dbaccess = OpenDatabase(vPath_Drv & "\DADOS\base_dpk.MDB")
    
    SQL = "SELECT a.COD_LOJA, a.NUM_PEDIDO, a.SEQ_PEDIDO, a.COD_FILIAL, b.dt_real"
    SQL = SQL & " FROM PEDNOTA_VENDA a, DATAS b"
    SQL = SQL & " WHERE FL_GER_NFIS='N' and SITUACAO=0 "
    
    Set SS = dbaccess.CreateSnapshot(SQL)
    FreeLocks
    
    SQL2 = " SELECT iif(a.COD_LOJA_VENDA=0,iif(a.COD_LOJA_TRANSF=0,a.COD_LOJA_COTACAO,a.COD_LOJA_TRANSF),a.COD_LOJA_VENDA) AS COD_LOJA_TRANSF, " & _
           "        iif(a.NUM_PEDIDO_VENDA=0,iif(a.NUM_PEDIDO=0,a.NUM_COTACAO_TEMP,a.NUM_PEDIDO),a.NUM_PEDIDO_VENDA) as NUM_PEDIDO_TRANSF, " & _
           "        b.COD_FILIAL, c.DT_REAL, A.NUM_PEDIDO, A.COD_LOJA_TRANSF, A.SEQ_PEDIDO "
    SQL2 = SQL2 & " FROM R_COTACAO_TRANSF a, DEPOSITO b, DATAS c"
    SQL2 = SQL2 & " WHERE COD_LOJA_VENDA = 0 AND NUM_PEDIDO_VENDA = 0 "
    
    Set SS2 = dbaccess.CreateSnapshot(SQL2)
    FreeLocks
    
    num_arq = FreeFile
        
    Fl_Aberto = "N"
    
    Do While Not SS2.EOF
       'Verifica se j� existe o num_pedido de transferencia
        If Len(SS2!num_pedido) > 0 Or SS2!num_pedido > 0 Then
        
            SQL = "SELECT p.COD_LOJA, p.NUM_PEDIDO, p.SEQ_PEDIDO "
            SQL = SQL & " FROM PEDNOTA_VENDA p, R_COTACAO_TRANSF r"
            SQL = SQL & " WHERE p.cod_loja=r.cod_loja_transf and "
            SQL = SQL & " p.num_pedido = r.num_pedido and"
            SQL = SQL & " p.seq_pedido = r.seq_pedido and "
            SQL = SQL & " p.cod_loja=" & SS2!cod_loja_transf & " and"
            SQL = SQL & " p.num_pedido = " & SS2!num_pedido & " and"
            SQL = SQL & " p.seq_pedido = " & SS2!SEQ_PEDIDO & " and "
            SQL = SQL & " p.SITUACAO=0 "
            
'Eduardo - 17/02/06
            SQL = SQL & " and r.cod_loja_venda <> 0 and r.num_pedido_venda <> 0"
            
            Set SS3 = dbaccess.CreateSnapshot(SQL)
            FreeLocks
        
        End If
        
            If SS3.EOF = True Then
                
                If Fl_Aberto = "N" Then
                    vConsulta = vPath_Drv & "\COM\SAIDA\PEDIDOS\CON" & Format(SS2!COD_FILIAL, "0000") & Format(Time, "hhmmss") & ".CON"
                    Open vConsulta For Output As #num_arq
                    Fl_Aberto = "S"
                End If
                
                Print #num_arq, Format(SS2!cod_loja_transf, "00"); Format(SS2!num_pedido_transf, "0000000"); "0"
            End If
            
            SS2.MoveNext
        Loop
        
        If Not SS.EOF Then
            If Fl_Aberto = "N" Then
                vConsulta = vPath_Drv & "\COM\SAIDA\PEDIDOS\CON" & Format(SS!COD_FILIAL, "0000") & Format(Time, "hhmmss") & ".CON"
                Open vConsulta For Output As #num_arq
                Fl_Aberto = "S"
            End If
            
            For i = 1 To SS.RecordCount
                Print #num_arq, Format(SS!cod_loja, "00"); Format(SS!num_pedido, "0000000"); Format(SS!SEQ_PEDIDO, "0")
                SS.MoveNext
            Next
        End If
        
        Close #num_arq
              
        num_arq = FreeFile
        
        If Fl_Aberto = "S" Then
            Open vPath_Drv & "\COM\SAIDA\MSG\CON" & Mid(vConsulta, 25, 10) & ".HDR" For Output As #num_arq
            
            Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
            Print #num_arq, "TO        =" & strEmail_To
            Print #num_arq, "FROM      =" & strEmail_From
            Print #num_arq, "DESCRICAO =" & "#CON"; Mid(vConsulta, 25, 10)
            Print #num_arq, "FILE      =" & "CON" & Mid(vConsulta, 25, 10) & ".CON"
            
            Close #num_arq
        End If
        
        SS.Close
        SS2.Close
        dbaccess.Close
    
   '*************************
        
   'Verifica as tarefas a serem executadas e apresenta ao usu�rio
    
    chkConsulta.Value = 0
    chkPedido.Value = 0
        
    If Trim(Dir(vPath_Drv & "\COM\SAIDA\MSG\PED*.HDR")) <> "" And _
       Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\PED*.TXT")) <> "" Then
       chkPedido.Value = 1
    End If

    If Trim(Dir(vPath_Drv & "\COM\SAIDA\MSG\CON*.HDR")) <> "" And _
       Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\CON*.CON")) <> "" Then
       chkConsulta.Value = 1
    End If

    chkConsulta.Enabled = False
    chkPedido.Enabled = False
    sscmdIniciar.Enabled = True

    Screen.MousePointer = 0
End Sub

Private Sub FIL050OUT()
    Dim vCarga          'Verifica se existe Carga.zip
    Dim vPedido_Enviar  'Verifica se existe Pedido para enviar
    Dim vHDR            'Verifica os HDR referente aos arquivos a serem enviados
    Dim vCopiarDe As String
    Dim vCopiarPara As String
    Dim vNome As String
    
    On Error GoTo trata_erro
    
   '***************************************************************
   'Faz o processo de busca de pedidos e consultas a serem enviadas
   '***************************************************************

   'Verifica se existe pedidos a serem enviados para o dep�sito
    vPedido_Enviar = Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\PED*.TXT"))
    If vPedido_Enviar <> "" Then
        Do While vPedido_Enviar <> ""
            vCopiarDe = vPath_Drv & "\COM\SAIDA\PEDIDOS\" & vPedido_Enviar
            vCopiarPara = vPath_Drv & "\COMUNICA\OUT\" & vPedido_Enviar
            FileCopy vCopiarDe, vCopiarPara
            Kill vPath_Drv & "\COM\SAIDA\PEDIDOS\" & vPedido_Enviar
            
            vNome = Mid(vPedido_Enviar, 1, InStr(vPedido_Enviar, ".") - 1)
            vHDR = vNome & ".HDR"
            vCopiarDe = vPath_Drv & "\COM\SAIDA\MSG\" & vHDR
            vCopiarPara = vPath_Drv & "\COMUNICA\OUT\" & vHDR
            FileCopy vCopiarDe, vCopiarPara
            Kill vPath_Drv & "\COM\SAIDA\MSG\" & vHDR
            
            vPedido_Enviar = Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\PED*.TXT"))
        Loop
    End If
   'T�rmino dos pedidos
    
   '*************************************************************
   'Verifica se existe consultas a serem enviados para o dep�sito
   '*************************************************************
    vPedido_Enviar = Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\CON*.CON"))
    If vPedido_Enviar <> "" Then
        Do While vPedido_Enviar <> ""
            vCopiarDe = vPath_Drv & "\COM\SAIDA\PEDIDOS\" & vPedido_Enviar
            vCopiarPara = vPath_Drv & "\COMUNICA\OUT\" & vPedido_Enviar
            FileCopy vCopiarDe, vCopiarPara
            Kill vPath_Drv & "\COM\SAIDA\PEDIDOS\" & vPedido_Enviar
            
            vNome = Mid(vPedido_Enviar, 1, InStr(vPedido_Enviar, ".") - 1)
            vHDR = vNome & ".HDR"
            vCopiarDe = vPath_Drv & "\COM\SAIDA\MSG\" & vHDR
            vCopiarPara = vPath_Drv & "\COMUNICA\OUT\" & vHDR
            FileCopy vCopiarDe, vCopiarPara
            Kill vPath_Drv & "\COM\SAIDA\MSG\" & vHDR
            
            vPedido_Enviar = Trim(Dir(vPath_Drv & "\COM\SAIDA\PEDIDOS\CON*.CON"))
        Loop
    End If
   '*********************
   'T�rmino das consultas
   '*********************
   '*********************************
   'T�rmino da verifica��o de pedidos
   '*********************************
   
   '******************************************************************************************************************************
   'Faz a chamada do sistema do Marcos Ap. onde executa o envio e recebimento de mensagens da maquina do Repres para a m�quina FTP
   '******************************************************************************************************************************
    Shell (vPath_Drv & "\PGMS\FTPCLIEN.EXE /AUTO"), 1
   '**********************
    End
   'Ap�s o t�rmino do FTPCLIENT ele ir� chamar o Fil050IN onde far� o tratamento
   'de tudo o que recebeu no Comunica\In da sua caixa do FTP
       
End Sub





