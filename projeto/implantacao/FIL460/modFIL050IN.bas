Attribute VB_Name = "modFIL050IN"
Option Explicit

Private Sub Inicio_FIL050IN()
   
    '--------------------------------------------
    '-- Variaveis de Lican�a para uso do BWZIP
    frmFIL460.MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    frmFIL460.MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    frmFIL460.MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    '--------------------------------------------
   
    Pegar_Drive
     
    If GetKeyVal(vPath_Drv & "\CARGA\PAAC.INI", "VERSAO", "FIL050IN") <> App.Major & "." & Format(App.Minor, "00") & "." & Format(App.Revision, "000") Then
       MsgBox "Sua vers�o est� desatualizada!" & vbCrLf & "Por favor efetue a atualiza��o do seu sistema.", vbOKOnly, "Aten��o"
       End
    End If
        
    '*************************************************************
    'Verifica as tarefas a serem executadas e apresenta ao usu�rio
    '*************************************************************
    If Trim(Dir(vPath & "\COMUNICA\IN\*.CON")) <> "" Or _
       Trim(Dir(vPath & "\COMUNICA\IN\*.TXT")) <> "" Then
        frmFIL460.chkRetorno.Value = 1
    End If
    
End Sub

Private Sub FIL050IN()
          Dim vDBF     'Guarda    'Verifica se existe DBF
          Dim vMDB     'Guarda2   'Verifica se existe MDB
          Dim vTxt_Con 'Guarda3   'Verifica se existe TXT ou CON
          Dim vZip     'Guarda4   'Verifica se existe ZIP
          Dim vCarga   'Guarda5   'Verifica se existe Carga.zip
          Dim vPedido_Envia 'Guarda6  'Verifica se existe Pedido para enviar
          Dim vNome_Arquivo_Atu As String
          
1         On Error GoTo trata_erro
          
         '*************************************************************************************
         'Voltou do FTPCLIENT e ir� fazer o processo de verifica��o e execu��o de tudo que veio
         'para o Comunica\In e estava na caixa do FTP
         '*************************************************************************************
         
         'Verificar se existe carga de sistemas a ser executada e faz�-la
2         vCarga = Trim(Dir(vPath_Drv & "\COMUNICA\IN\CAR*.ZIP"))
3         If vCarga <> "" Then
4             FileCopy vPath_Drv & "\COMUNICA\IN\" & vCarga, vPath_Drv & "\CARGA\" & vCarga
5             RunCmd vPath_Drv & "\carga\fil240.exe", 1
6             If vCarga <> "" Then Kill vPath_Drv & "\COMUNICA\IN\" & vCarga
7         End If
         'T�rmino da etapa de carga de sistemas
           
         'Verificar se existe atualiza��o a ser executada e faz�-la
8         vZip = Trim(Dir(vPath & "\COMUNICA\IN\ATU*.ZIP"))
9         If vZip <> "" Then
10            Do While vZip <> ""
11                FileCopy vPath_Drv & "\comunica\in\" & vZip, vPath & "\COM\ENTRADA\ARQUIVOS\" & vZip
12                Kill vPath_Drv & "\COMUNICA\IN\" & vZip
13                vZip = Trim(Dir(vPath_Drv & "\COMUNICA\IN\ATU*.ZIP"))
14            Loop
                 
             'Deletar todas as consultas e retornos de pedidos pois s�o anteriores � atualiza��o
15            vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.TXT"))
16            Do While vTxt_Con <> ""
17                Kill vPath & "\COMUNICA\IN\" & vTxt_Con
18                vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.TXT"))
19            Loop
              
20            vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.CON"))
21            Do While vTxt_Con <> ""
22                Kill vPath_Drv & "\COMUNICA\IN\" & vTxt_Con
23                vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.CON"))
24            Loop
              
25            Do While Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\ATU*.ZIP") <> ""
                  'Nome do Arquivo de Atualiza��o Compactado que ser� Descompactado
26                vNome_Arquivo_Atu = Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\ATU*.ZIP")
                 
                  'BWZIP
27                MaqZip1.ZipUncompress vPath_Drv & "\com\entrada\arquivos\" & vNome_Arquivo_Atu, "*", vPath & "\com\entrada\arquivos\", "-o"
                              
28                DoEvents
29                vNome_Arquivo_Atu = ""
                  
                 'Verifica se existe DBF para continuar o processo pois s� assim se tem a certeza de que o
                 'Unzipa j� terminou a descompacta��o.
30                vDBF = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
31                Do While vDBF = ""
32                    vDBF = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
33                Loop
                  
34                If vDBF <> "" Then
                     'Roda a convers�o de arquivos para colocar todos os DBF's dentro do Base_atu.mdb
35                    RunCmd vPath_Drv & "\PGMS\fil260.exe", 1
36                    DoEvents
                      
                     'Fica em loop at� sumir todos os DBF's pois s� ent�o a convers�o foi conclu�da.
37                    vDBF = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
38                    Do While vDBF <> ""
39                        vDBF = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
40                    Loop
                      
                     'Se n�o tiver nenhum DBF e tiver um MDB continua o processo pois o Fil040
                     'que � chamado dentro do Fil260 ainda n�o terminou o processo
41                    vMDB = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"))
42                    Do While vMDB <> ""
43                        vMDB = Trim(Dir(vPath_Drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"))
44                    Loop
45                End If
46            Loop
              
47            vZip = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.ZIP"))
48            Do While vZip <> ""
49                Kill vPath_Drv & "\COMUNICA\IN\" & vZip
50                vZip = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.ZIP"))
51            Loop
52        End If
         'T�rmino da etapa de Atualiza��o

         'Verifica se existe consulta e retorno de pedidos para pegar e executa
53        vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.TXT"))
54        If vTxt_Con <> "" Then
55            Do While vTxt_Con <> ""
56                FileCopy vPath_Drv & "\comunica\in\" & vTxt_Con, vPath_Drv & "\COM\ENTRADA\PEDIDOS\" & vTxt_Con
57                Kill vPath_Drv & "\comunica\in\" & vTxt_Con
58                vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.TXT"))
59            Loop
60        End If
                                        
61        vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.CON"))
62        If vTxt_Con <> "" Then
63            Do While vTxt_Con <> ""
64                FileCopy vPath_Drv & "\comunica\in\" & vTxt_Con, vPath_Drv & "\COM\ENTRADA\PEDIDOS\" & vTxt_Con
65                Kill vPath_Drv & "\comunica\in\" & vTxt_Con
66                vTxt_Con = Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.CON"))
67            Loop
68        End If
         'Se existir arquivos para serem atualizados roda o fil040.exe
69        If Trim(Dir(vPath_Drv & "\COM\ENTRADA\PEDIDOS\*.TXT")) <> "" Or _
             Trim(Dir(vPath_Drv & "\COM\ENTRADA\PEDIDOS\*.CON")) <> "" Then
70            RunCmd vPath_Drv & "\pgms\fil040.EXE", 1
71            DoEvents
72        End If
         'T�rmino das consultas e retornos
         
         'Deleta todos os cabe�alhos (.HDR) que foram recebidos com os arquivos
73        Do While Trim(Dir(vPath_Drv & "\COMUNICA\IN\*.HDR")) <> ""
74            Kill vPath_Drv & "\COMUNICA\IN\*.HDR"
75        Loop
          
76        frmFIL460.chkRetorno.Value = 0
77        frmFIL460.chkAtualiza.Value = 0

trata_erro:
79        If Err.Number <> 0 Then
80            MsgBox "Sub FIL050IN" & vbCrLf & "C�digo:" & Err.Number & vbcrl & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
81        End If
          
End Sub

