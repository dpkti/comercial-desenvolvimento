VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFIL460 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Processo de Atualiza��o"
   ClientHeight    =   4500
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11580
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   11580
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   915
      Left            =   3870
      TabIndex        =   4
      Top             =   30
      Width           =   7665
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descri��o:"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   60
         TabIndex        =   7
         Top             =   600
         Width           =   765
      End
      Begin VB.Label lblDetalhes 
         Appearance      =   0  'Flat
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   870
         TabIndex        =   6
         Top             =   570
         Width           =   6705
      End
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00800000&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   405
         Left            =   60
         TabIndex        =   5
         Top             =   150
         Width           =   7515
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      Height          =   3405
      Left            =   3870
      TabIndex        =   0
      Top             =   990
      Width           =   7665
      Begin VB.Frame Frame3 
         BackColor       =   &H00FFFFFF&
         Caption         =   "Atualiza��o - FIL040 - Passo 3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1185
         Left            =   60
         TabIndex        =   14
         Top             =   2160
         Width           =   7545
         Begin MSComctlLib.ProgressBar pbr 
            Height          =   255
            Left            =   60
            TabIndex        =   15
            Top             =   840
            Width           =   7425
            _ExtentX        =   13097
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   0
            Scrolling       =   1
         End
         Begin VB.Label Label2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Progresso:"
            Height          =   195
            Left            =   90
            TabIndex        =   17
            Top             =   630
            Width           =   765
         End
         Begin VB.Label lblExecutando 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H00800000&
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   30
            TabIndex        =   16
            Top             =   270
            Width           =   7455
         End
      End
      Begin VB.Frame fraFIL050IN 
         BackColor       =   &H00FFFFFF&
         Caption         =   "FIL050-IN - Passo 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   60
         TabIndex        =   11
         Top             =   1170
         Width           =   2655
         Begin VB.CheckBox chkAtualiza 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "Atualiza��o da Base Di�ria"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   90
            TabIndex        =   13
            Top             =   270
            Width           =   2505
         End
         Begin VB.CheckBox chkRetorno 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "Retorno de Pedidos"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   90
            TabIndex        =   12
            Top             =   540
            Width           =   1785
         End
      End
      Begin VB.Frame fraFIL050OUT 
         BackColor       =   &H00FFFFFF&
         Caption         =   "FIL050-OUT - Passo 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   885
         Left            =   60
         TabIndex        =   8
         Top             =   180
         Width           =   2655
         Begin VB.CheckBox Check2 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "Envio de Pedidos"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   90
            TabIndex        =   10
            Top             =   540
            Width           =   1605
         End
         Begin VB.CheckBox Check1 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "Envio de Consulta de Pedidos"
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   90
            TabIndex        =   9
            Top             =   270
            Width           =   2505
         End
      End
   End
   Begin MSComctlLib.ImageList ImlTrv 
      Left            =   10950
      Top             =   -30
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFIL460.frx":0000
            Key             =   "ProcessoAtual"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFIL460.frx":059A
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFIL460.frx":0A34
            Key             =   "ProcessoExecutado"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFIL460.frx":0FCE
            Key             =   "ProcessoAguardando"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFIL460.frx":1044
            Key             =   "Processos"
         EndProperty
      EndProperty
   End
   Begin Bot�o.cmd cmdInicar 
      Height          =   885
      Left            =   30
      TabIndex        =   1
      Top             =   3510
      Width           =   1305
      _ExtentX        =   2302
      _ExtentY        =   1561
      BTYPE           =   3
      TX              =   "&Iniciar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL460.frx":15DE
      PICN            =   "frmFIL460.frx":15FA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.TreeView trvProcessos 
      Height          =   3405
      Left            =   30
      TabIndex        =   2
      Top             =   60
      Width           =   3765
      _ExtentX        =   6641
      _ExtentY        =   6006
      _Version        =   393217
      LabelEdit       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      SingleSel       =   -1  'True
      ImageList       =   "ImlTrv"
      BorderStyle     =   1
      Appearance      =   0
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   885
      Left            =   2490
      TabIndex        =   3
      Top             =   3510
      Width           =   1305
      _ExtentX        =   2302
      _ExtentY        =   1561
      BTYPE           =   3
      TX              =   "&Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL460.frx":22D4
      PICN            =   "frmFIL460.frx":22F0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmFIL460"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdInicar_Click()
    trvProcessos.Nodes.Item(1).ForeColor = vbBlack
    trvProcessos.Nodes.Item(2).Bold = True
    trvProcessos.Nodes.Item(2).ForeColor = vbBlack
    lblStatus.Caption = trvProcessos.SelectedItem
    lblDetalhes.Caption = " Executando envio de Pedidos e Consultas"
    
End Sub

Private Sub cmdSair_Click()
    End
End Sub

Private Sub Form_Load()
    Dim vNo As Node
    Set vNo = trvProcessos.Nodes.Add(, , "P", "Processos", "Processos")
    vNo.ForeColor = vbBlue
    vNo.Bold = True
    Set vNo = trvProcessos.Nodes.Add("P", tvwChild, "FIO", "Efetuar comunica��o FTP", "ProcessoAguardando")
    Set vNo = trvProcessos.Nodes.Add("P", tvwChild, "FIL050I", "Envia Arquivos", "ProcessoAguardando")
    Set vNo = trvProcessos.Nodes.Add("P", tvwChild, "FIL050O", "Receber Arquivos", "ProcessoAguardando")
    Set vNo = trvProcessos.Nodes.Add("P", tvwChild, "FIL040", "Efetuar Atualiza��o", "ProcessoAguardando")
    vNo.EnsureVisible
    lblStatus.Caption = "Clique sobre o bot�o Iniciar"
    trvProcessos.Nodes.Item(1).Selected = True

    Set Conn = New ADODB.Connection
    
    Conn.Open "Provider=MSDASQL;" & _
                   "Driver={Microsoft Access Driver (*.mdb)};" & _
                   "Dbq=c:\teste.mdb;" & _
                   "Uid=Admin;" & _
                   "Pwd=;"

End Sub

