Attribute VB_Name = "modPublico"
Option Explicit

Public Conn As ADODB.Connection
Public rst As ADODB.Recordset

Public vPath_Drv As String
Public vSql As String
Public vSql2 As String
Public vEmail_To As String
Public vEmail_From As String

Private Const INFINITE = &HFFFF
Private Const SYNCHRONIZE = &H100000

Private Declare Sub WaitForSingleObject Lib "kernel32.dll" (ByVal hHandle As Long, ByVal dwMilliseconds As Long)
Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDA As Long, ByVal bIH As Integer, ByVal dwPID As Long) As Long
Private Declare Sub CloseHandle Lib "kernel32.dll" (ByVal hObject As Long)

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Public Sub RunCmd(CmdPath As String, _
   Optional WindowStyle As VbAppWinStyle = vbNormalFocus)

   Dim hProcess As Long

   On Error GoTo Err_RunCmd

   hProcess = OpenProcess(SYNCHRONIZE, 0, Shell(CmdPath, WindowStyle))

   If hProcess Then
       WaitForSingleObject hProcess, INFINITE
       CloseHandle hProcess
   End If

   Exit Sub

Err_RunCmd:

   Err.Clear

End Sub

Public Sub Pegar_Drive()
    vPath_Drv = Left(App.Path, 2)
    If vPath_Drv = "\\" Then
        On Error Resume Next
        If Dir(vPath_Drv & "\pgms\fil050in.exe") = "" Then
            vPath_Drv = "h:"
        Else
            vPath_Drv = "c:"
        End If
        If Err <> 0 Then vPath_Drv = "h:"
        Err.Clear
        On Error GoTo 0
    End If
End Sub

Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "DPK": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function





