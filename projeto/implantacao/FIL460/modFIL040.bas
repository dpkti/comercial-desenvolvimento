Attribute VB_Name = "modFIL040"
Option Explicit

'*********************************************************
'1.- Pega Drive
'2.- Verificar se existe arquivo MDB para ser rodada atualizacao
'       Se tiver roda Comeca_Atual e Otimizar_Bases
'3.- Verificar se tem arquivo .CON ou .TXT para rodar Atu_Pedido e Atu_Flag
'
'*********************************************************

Dim MyRec As Recordset
Dim recTab As ADODB.Recordset
Dim ss As ADODB.Recordset

Private Sub INICIA_ATUALIZACAO_FIL040()
      Dim BOERRO As Boolean
      Dim RESP As Integer
      Dim inicio As String, fim As String
      Dim dblAtual As Double

      '45 � o n�mero de tabelas a atualizar
3     prgbar1.Max = 45 * numArq
4     dblAtual = 0

5     Do While contArq <= numArq
          
          'DELETA_APLICACAO - 1
          DEL_APLICACAO
          
          'DELETA_CLIENTE - 2
          DEL_CLIENTE
          
          'DELETA_REPRESENTANTE - 3
          DEL_REPR
          
          'DELETA_TABELA_DESCPER - 4
          DEL_TABDESC

          'DELETA_PEDNOTA_VENDA E ITPEDNOTA_VENDA - 45
          DEL_PEDNOTA

          'APLICACAO - 5
          ATU_APLICACAO

          'BANCO - 6
          ATU_BANCO

          ' CATEG_SINAL - 7
          ATU_CATEGSINAL
          
          ' CIDADE - 8
          ATU_CIDADE
          
          'CLIE_CREDITO - 9
          ATU_CLIECRED
          
          'CLIE_ENDERECO - 10
          ATU_CLIEEND
          
          'CLIE_MENSAGEM - 11
          ATU_CLIEMSG
          
          'CLIENTE - 12
          ATU_CLIENTE
          
          'DATAS - 13
          ATU_DATAS
              
          'DOLAR_DIARIO - 14
          ATU_DOLARDIARIO

          'DUPLICATA - 15
          ATU_DUPLICATA
          
          'FILIAL - 17
          ATU_FILIAL
          
          'FORNECEDOR - 18
          ATU_FORNECEDOR

          'GRUPO - 19
          ATU_GRUPO
          
          'ITEM_ANALITICO - 20
          ATU_ITEMANAL
          
          'ITEM_CADASTRO - 21
          ATU_ITEMCAD
          
          'ITEM_CUSTO - 22
          ATU_ITEMCUSTO
          
          'ITEM_ESTOQUE - 23
          ATU_ITEMESTOQUE
          
          'ITEM_GLOBAL - 24
          ATU_ITEMGLOBAL
          
          'ITEM_PRECO - 25
          ATU_ITEMPRECO

          'MONTADORA - 26
          ATU_MONTADORA
          
          'PLANO_PGTO - 27
          ATU_PLANO
          
          'R_REPCGC - 28
          ATU_REPCGC
          
          'R_REPVEN - 29
          ATU_REPVEN
          
          'REPR_END_CORRESP - 30
          ATU_REPEND

          'REPRESENTACAO - 31
          ATU_REPRESENTACAO
          
          'REPRESENTANTE - 32
          ATU_REPRESENTANTE

          'SUBGRUPO - 33
          ATU_SUBGRUPO
          
          'SUBST_TRIBUTARIA - 34
          ATU_SUBTRIB
          
          'TABELA_DESCPER - 35
          ATU_TABDESCPER
          
          'TABELA_VENDA - 36
          ATU_TABVENDA

          'TIPO DE CLIENTE - 38
          ATU_TPCLIE
          
          'TRANSPORTADORA - 39
          ATU_TRANSPORTADORA
          
          'UF - 40
          ATU_UF
          
          'UF_DEPOSITO - 41
          ATU_UF_DEPOSITO
          
          'UF_ORIGEM_DESTINO - 42
          ATU_UF_ORIGDEST

          'UF_DPK - 43
          ATU_UFDPK
         
          'SALDO_PEDIDOS - 44
          ATU_SALDOPED
         
307      contArq = contArq + 1
308      If contArq <= numArq Then
309           dbExt.Close
310           mdiGER040.File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
311           mdiGER040.File1.Pattern = "*.mdb"
312           nomeArq = mdiGER040.File1.List(contArq - 1)

313           On Error GoTo TRATA_ERRO_AUXILIAR
314           Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
              
315      End If
         
316   Loop

      'SSPanel2.Caption = "Atualiza��o Finalizada com " & dblatu & " Registros Atualizados Ok! "
317   If Trim(Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB")) <> "" Then
318       Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"
319   End If
         
320   Screen.MousePointer = 1
         
321   fim = Time

322   MsgBox ("Inicio : " & inicio & " Fim: " & fim)

      'Call FUNCAO_ERRO(1, "Retorno da Atualiza��o", "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim, "Dep. Opera��o(CPD) ", "")

323   strSQL = "INSERT INTO LOGDIV " & _
              " ( DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL ) VALUES " & _
              " ( '" & Now & "' , 1, 'Retorno da Atualiza��o', '" & _
              "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim & "', '" & _
              "Dep. Opera��o (CPD) ' ) "
              
324   dbControle.Execute strSQL, dbFailOnError

325   Screen.MousePointer = 1

326   Exit Sub

Trata_Erro:

327   If BOERRO Then
328       SSPanel2.Caption = "Problemas na Atualiza��o !!! "
329       Screen.MousePointer = 1

330       Call FUNCAO_ERRO("")
331   End If

332   RESP = MsgBox("Continua a Atualiza��o ?", vbYesNo)
333   If RESP = vbYes Then
334       Resume Next
335   End If

336   Exit Sub

TRATA_ERRO_AUXILIAR:

337   If Err = 3049 Then

338       MousePointer = 11
339       DoEvents

          ' Reparar Banco de Dados
340       RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
          
341       COMPACTA_AUXILIAR
          
342       MousePointer = 0
          ' Ap�s corrigir o banco danificado processa novamente a linha do programa
          ' que gerou o erro.
343       Resume 0
344   Else

345       Call FUNCAO_ERRO("")
346       MsgBox "Sub Inicia_Atualizacao" & vbCrLf & "ERRO: " & Err.Number & " - " & Err.Description

347   End If

End Sub

Private Sub Executar_SQLs()
    On Error GoTo Trata_Erro
    
    Dim i As Integer
                   
    Set rst = New ADODB.Recordset
    rst.Open "Select * from SQL order by sequencia, ordem ", Conn, adOpenKeyset, adLockOptimistic
    
    For i = 1 To rst.RecordCount
        Conn.Execute rst!SQL
    Next

Trata_Erro:
    If Err.Number = 3010 Then
       Conn.Execute "DROP TABLE " & rst!Tabela
       Resume
    ElseIf Err = 3022 Then
       'Registro ja existe (Fez Update)
       Resume Next
    ElseIf Err.Number <> 0 Then
       CLASS_FISCAL_RED_BASE = False
       MsgBox "Sub Executar_Sqls" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & vbCrLf & "Sql: " & rst!SQL
    End If
    
    rst.Close
    Conn.Close
    Set rst = Nothing
    Set Conn = Nothing
    
End Sub


Public Function ATU_FLAG()
    '*******************************************************************************
    'Atualiza Flag do Pedido de Digitacao (DIGITA) ou Telemarketing (VENDAS) quando
    'o e-mail de retorno voltou indicando que ocorreu erro na finaliza��o do pedido.
    ' a Flag (FL_PEDIDO_FINALIZADO) voltar� para "N" para que o pedido possa ser
    ' enviado novamente para Campinas
    '*******************************************************************************
    Dim Filial
          
1   On Error GoTo TRATA_ERRO_LOCAL
          
'2   Set conn = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
4   lblExecutando "Atualizando Posi��o de Pedidos"
          
5   Do While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
6      STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT")
7      STRAT_PED = Path_drv & "\COM\ENTRADA\PEDIDOS\" & STRAT_PED
8      Open STRAT_PED For Input As #1
9      Line Input #1, TMP_LINHA
10     POS_SEP = InStr(TMP_LINHA, "|")
11     Ped_Loja = Left(TMP_LINHA, POS_SEP - 1)
12     TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
13     POS_SEP = InStr(TMP_LINHA, "|")
14     Ped_Num = Left(TMP_LINHA, POS_SEP - 1)
15     TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
16     POS_SEP = InStr(TMP_LINHA, "|")
17     Ped_Seq = Left(TMP_LINHA, POS_SEP - 1)
18     TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
19     Ped_MensP = TMP_LINHA
                
20     lblExecutando = "Atualizando Pedido n�: " & Ped_Num
21     Close #1

       'DEFINE A FILIAL DO PEDIDO
       Set recTab = New ADODB.Recordset
24     recTab.Open "SELECT COD_FILIAL from DEPOSITO ", adOpenKeyset, adLockOptimistic
25     If Not recTab.EOF Then
26        Filial = recTab!COD_FILIAL
27     Else
28        Filial = 0
29     End If
              
31     strSQL = "SELECT * from PEDNOTA_VENDA "
32     strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
33     strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
34     strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "
                 
35     Set recTab = New ADODB.Recordset
       recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
                
36     Conn.BeginTrans
37     If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
39        strSQL = "UPDATE "
40        strSQL = strSQL & " PEDNOTA_VENDA "
41        strSQL = strSQL & " SET "
42        strSQL = strSQL & " SITUACAO = 0 ,"
43        strSQL = strSQL & " FL_GER_NFIS = 'N' ,"
44        strSQL = strSQL & " COD_FILIAL = " & Filial & " ,"
45        strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' "
46        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
47        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
48        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
49        Conn.Execute strSQL
50     Else ' N�o Existe o Pedido - Incluir
52        strSQL = "INSERT "
53        strSQL = strSQL & " into PEDNOTA_VENDA "
54        strSQL = strSQL & " (cod_loja, num_pedido, seq_pedido,Mens_pedido,fl_ger_nfis,SITUACAO,COD_FILIAL)"
55        strSQL = strSQL & " Values "
56        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
57        strSQL = strSQL & " ' " & Ped_MensP & " ', 'N',0," & Filial & ")"
58        Conn.Execute strSQL
59     End If
          
60     Conn.CommitTrans
               
61     Kill STRAT_PED
62  Loop
          
    frmFIL460.lblExecutando.Caption = "Atualiza��o OK! "

TRATA_ERRO_LOCAL:
66    If Err = 3049 Then
67        DoEvents
68        RepairDatabase (Path_drv & "\DADOS\BASE_DPK.MDB")
69        COMPACTA_LOCAL
          
          'MousePointer = 0
          ' Ap�s corrigir o banco danificado processa novamente a linha do programa
          ' que gerou o erro.
70        Resume 0
71    ElseIf Err.Number <> 0 Then
          MsgBox "Sub ATU_FLAG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
74    End If
End Function
Public Function ATU_PEDIDOS()

    On Error GoTo Trata_Erro

3   frmFIL460.lblExecutando.Caption = "Atualizando Posi��o de Pedidos"

4   While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> ""
5         STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON")
6         If Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") = "" Then
7            STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT")
8         End If
9         STRAT_PED = Path_drv & "\COM\ENTRADA\PEDIDOS\" & STRAT_PED
10        Open STRAT_PED For Input As #1
11        Do While Not EOF(1)
12           Line Input #1, TMP_LINHA
13           TMP_I = InStr(TMP_LINHA, "|")
14           If TMP_I <> 0 Then
15              Select Case UCase(Mid(TMP_LINHA, 1, TMP_I - 1))
                     Case "PED": Call ATU_PED
16                   Case "ITE": Call ATU_ITE
17                   Case "V_P": Call ATU_V_P
18                   Case "ROM": Call ATU_ROM
19                   Case "R_P": Call ATU_R_P
20                   Case Else: Call CHAVE_PED
21              End Select
22           Else
23              TMP_LINHA = TMP_LINHA & vbCrLf
24           End If
25        Loop
26        Close #1
              
27        Kill STRAT_PED
28     Wend
          
29     frmFIL460.lblExecutando.Caption = "Atualiza��o OK! "

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_PEDIDOS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_ITE()
      '*************************************************************
      'Separa valores dos demais campos da tabela ITPEDNOTA_VENDA fora
      ' a chave da tabela

1         On Error GoTo Trata_Erro

2         Call SEPARA
3         Ite_Num_It = CONTEUDO
4         frmFIL460.lblExecutando.Caption = "Atualizando Item : " & Ite_Num_It
          
5         Call SEPARA
6         Ite_Loja_Nota = CONTEUDO
7         Call SEPARA
8         Ite_Num_Nota = CONTEUDO
9         Call SEPARA
10        Ite_It_Nota = CONTEUDO
11        Call SEPARA
12        Ite_Dpk = CONTEUDO
13        Call SEPARA
14        Ite_Sol = CONTEUDO
15        Call SEPARA
16        Ite_Ate = CONTEUDO
17        Call SEPARA
18        Ite_Pre = CONTEUDO
19        Call SEPARA
20        Ite_Tab = CONTEUDO
21        Call SEPARA
22        Ite_Desc1 = CONTEUDO
23        Call SEPARA
24        Ite_Desc2 = CONTEUDO
25        Call SEPARA
26        Ite_Desc3 = CONTEUDO
27        Call SEPARA
28        Ite_Icm = CONTEUDO
29        Call SEPARA
30        Ite_Ipi = CONTEUDO
31        Call SEPARA
32        Ite_Com = CONTEUDO
33        Call SEPARA
34        Ite_Tlm = CONTEUDO
35        Call SEPARA
36        Ite_Trib = CONTEUDO
37        Call SEPARA
38        Ite_Trip = CONTEUDO
39        Call SEPARA
40        Ite_Sit = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set conn = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

41    strSQL = "SELECT * from ITPEDNOTA_VENDA "
42    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
43    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
44    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
45    strSQL = strSQL & " num_item_pedido = " & Ite_Num_It

46    Set recTab = New ADODB.Recordset
      recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
48    Conn.BeginTrans
49    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
50        strSQL = "UPDATE "
51        strSQL = strSQL & " ITPEDNOTA_VENDA "
52        strSQL = strSQL & " SET "
53        strSQL = strSQL & " COD_LOJA_NOTA =  " & Ite_Loja_Nota & ", "
54        strSQL = strSQL & " NUM_NOTA = " & Ite_Num_Nota & ", "
55        strSQL = strSQL & " NUM_ITEM_NOTA = " & Ite_It_Nota & ", "
56        strSQL = strSQL & " COD_DPK = " & Ite_Dpk & ", "
57        strSQL = strSQL & " QTD_SOLICITADA = " & Ite_Sol & ","
58        strSQL = strSQL & " QTD_ATENDIDA = " & Ite_Ate & ","
59        strSQL = strSQL & " PRECO_UNITARIO = " & Ite_Pre & ", "
60        strSQL = strSQL & " TABELA_VENDA = ' " & Ite_Tab & "',"
61        strSQL = strSQL & " PC_DESC1 =  " & Ite_Desc1 & " , "
62        strSQL = strSQL & " PC_DESC2 =  " & Ite_Desc2 & " ,"
63        strSQL = strSQL & " PC_DESC3 =  " & Ite_Desc3 & ","
64        strSQL = strSQL & " PC_DIFICM =  " & Ite_Icm & ","
65        strSQL = strSQL & " PC_IPI =  " & Ite_Ipi & ","
66        strSQL = strSQL & " PC_COMISS =  " & Ite_Com & ","
67        strSQL = strSQL & " PC_COMISSTLMK=  " & Ite_Tlm & ","
68        strSQL = strSQL & " COD_TRIB = " & Ite_Trib & ","
69        strSQL = strSQL & " COD_TRIBIPI = " & Ite_Trip & ","
70        strSQL = strSQL & " SITUACAO = " & Ite_Sit
71        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
72        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
73        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " and "
74        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & Ite_Num_It
         
75        Conn.Execute strSQL

76    Else ' N�o Existe o Pedido - Incluir
77       strSQL = "INSERT "
78       strSQL = strSQL & " into ITPEDNOTA_VENDA values "
79       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
80       strSQL = strSQL & " " & Ite_Num_It & "," & Ite_Loja_Nota & " , " & Ite_Num_Nota & ", "
81       strSQL = strSQL & " " & Ite_It_Nota & ", " & Ite_Dpk & " , " & Ite_Sol & " ,"
82       strSQL = strSQL & " " & Ite_Ate & " , " & Ite_Pre & ", '" & Ite_Tab & "', "
83       strSQL = strSQL & " " & Ite_Desc1 & " , " & Ite_Desc2 & " , " & Ite_Desc3 & " , "
84       strSQL = strSQL & " " & Ite_Icm & " , " & Ite_Ipi & "," & Ite_Com & ","
85       strSQL = strSQL & " " & Ite_Tlm & " , " & Ite_Trib & ", " & Ite_Trip & " ," & Ite_Sit & ")"

86       Conn.Execute strSQL
         
87    End If
88    Conn.CommitTrans
      '*********************************************

Trata_Erro:
89        If Err.Number <> 0 Then
90            MsgBox "Sub ATU_ITE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
91        End If

End Function

Public Function ATU_V_P()
    
    On Error GoTo Trata_Erro

         '*************************************************************
         'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
         'a chave da tabela

1         Call SEPARA
2         V_P_Ite = CONTEUDO
3         frmFIL460.lblExecutando.Caption = "Atualizando V_PEDLIQ_VENDA do Item : " & V_P_Ite
4         Call SEPARA
5         V_P_Sit = CONTEUDO
6         Call SEPARA
7         V_P_Pr = CONTEUDO
8         Call SEPARA
9         V_P_Vl = CONTEUDO
10        Call SEPARA
11        V_P_Prs = CONTEUDO
12        Call SEPARA
13        V_P_Vls = CONTEUDO
         '*******************************************
          
         '*******************************************
         'Grava valores lidos na tabela PEDNOTA_VENDA
         'Set conn = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14                 strSQL = "SELECT * from V_PEDLIQ_VENDA "
15        strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16        strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17        strSQL = strSQL & " seq_pedido =  " & Ped_Seq & " AND "
18        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
          
19        Set recTab = New ADODB.Recordset
          recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic

21        Conn.BeginTrans
22        If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
23            strSQL = "UPDATE "
24            strSQL = strSQL & " V_PEDLIQ_VENDA "
25            strSQL = strSQL & " SET "
26            strSQL = strSQL & " SITUACAO = " & V_P_Sit & ", "
27            strSQL = strSQL & " PR_LIQUIDO = " & V_P_Pr & ", "
28            strSQL = strSQL & " VL_LIQUIDO = " & V_P_Vl & ", "
29            strSQL = strSQL & " PR_LIQUIDO_SUFRAMA = " & V_P_Prs & ","
30            strSQL = strSQL & " VL_LIQUIDO_SUFRAMA = " & V_P_Vls & ""
31            strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32            strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33            strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND"
34            strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
35            Conn.Execute strSQL
36        Else    'N�o Existe o Pedido - Incluir
37            strSQL = "INSERT "
38            strSQL = strSQL & " into V_PEDLIQ_VENDA values "
39            strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
40            strSQL = strSQL & " " & V_P_Ite & ", " & V_P_Sit & ", " & V_P_Pr & " , "
41            strSQL = strSQL & " " & V_P_Vl & " , " & V_P_Prs & ", " & V_P_Vls & ")"
42            Conn.Execute strSQL
43        End If
44        Conn.CommitTrans
         '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_V_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function
Public Function ATU_ROM()

    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
      ' a chave da tabela
1     frmFIL460.lblExecutando.Caption = "Atualizando ROMANEIO do Pedido n�: " & Ped_Num & " - " & Ped_Seq

2         Call SEPARA
3         Rom_DtC = CONTEUDO
4         Call SEPARA
5         Rom_DtD = CONTEUDO
6         Call SEPARA
7         Rom_Num = CONTEUDO
8         Call SEPARA
9         Rom_Con = CONTEUDO
10        Call SEPARA
11        Rom_Car = CONTEUDO
12        Call SEPARA
13        Rom_Qtd = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set conn = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14    strSQL = "SELECT * from ROMANEIO "
15    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "

18    Set recTab = New ADODB.Recordset
      recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
20    Conn.BeginTrans
21    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
22        strSQL = "UPDATE "
23        strSQL = strSQL & " ROMANEIO "
24        strSQL = strSQL & " SET "
25        strSQL = strSQL & " DT_COLETA =  '" & Rom_DtC & "', "
26        strSQL = strSQL & " DT_DESPACHO = '" & Rom_DtD & "', "
27        strSQL = strSQL & " NUM_ROMANEIO = " & Rom_Num & ", "
28        strSQL = strSQL & " NUM_CONHECIMENTO = '" & Rom_Con & "', "
29        strSQL = strSQL & " NUM_CARRO = '" & Rom_Car & "',"
30        strSQL = strSQL & " QTD_VOLUME = " & Rom_Qtd & " "
31        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
         
34        Conn.Execute strSQL

35    Else ' N�o Existe o Pedido - Incluir
36       strSQL = "INSERT "
37       strSQL = strSQL & " into ROMANEIO values "
38       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
39       strSQL = strSQL & " '" & Rom_DtC & "', '" & Rom_DtD & "', " & Rom_Num & " , "
40       strSQL = strSQL & " '" & Rom_Con & "' , '" & Rom_Car & "', " & Rom_Qtd & ")"
         
41       Conn.Execute strSQL
         
42    End If
43    Conn.CommitTrans
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_ROM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_R_P()
    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela R_PEDIDO_CONF fora
      ' a chave da tabela
1     frmFIL460.lblExecutando.Caption = "Atualizando R_PEDIDO_CONF do Pedido n�: " & Ped_Num & " - " & Ped_Seq

2     Call SEPARA
3     R_P_Num = CONTEUDO
4     Call SEPARA
5     R_P_Con = CONTEUDO
6     Call SEPARA
7     R_P_Emb = CONTEUDO
8     Call SEPARA
9     R_P_Exp = CONTEUDO
10    Call SEPARA
11    R_P_Fl = CONTEUDO
12    Call SEPARA
13    R_P_Qtd = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set conn = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14    strSQL = "SELECT * from R_PEDIDO_CONF "
15    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
18    strSQL = strSQL & " NUM_CAIXA =  " & R_P_Num & ""

19    Set recTab = New ADODB.Recordset
      recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
21    Conn.BeginTrans
22    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
23        strSQL = "UPDATE "
24        strSQL = strSQL & " R_PEDIDO_CONF "
25        strSQL = strSQL & " SET "
26        strSQL = strSQL & " COD_CONFERENTE = " & R_P_Con & ", "
27        strSQL = strSQL & " COD_EMBALADOR = " & R_P_Emb & ", "
28        strSQL = strSQL & " COD_EXPEDIDOR = " & R_P_Exp & ", "
29        strSQL = strSQL & " FL_PEND_ETIQ = '" & R_P_Fl & "',"
30        strSQL = strSQL & " QTD_VOLUME_PARC = " & R_P_Qtd & ""
31        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND "
34        strSQL = strSQL & " NUM_CAIXA= " & R_P_Num
35        Conn.Execute strSQL
36    Else ' N�o Existe o Pedido - Incluir
37       strSQL = "INSERT "
38       strSQL = strSQL & " into R_PEDIDO_CONF values "
39       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
40       strSQL = strSQL & " " & R_P_Num & ", " & R_P_Con & ", " & R_P_Emb & " , "
41       strSQL = strSQL & " " & R_P_Exp & " , '" & R_P_Fl & "', " & R_P_Qtd & ")"
42       Conn.Execute strSQL
43    End If
44    Conn.CommitTrans
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_R_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_PED()

    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela PEDNOTA_VENDA fora
      ' a chave da tabela
1     frmFIL460.lblExecutando.Caption = "Atualizando Pedido n�: " & Ped_Num & " - " & Ped_Seq

2         Call SEPARA
3         Ped_Loja_Nota = CONTEUDO
4         Call SEPARA
5         Ped_Num_Nota = CONTEUDO
6         Call SEPARA
7         Ped_Pen = CONTEUDO
8         Call SEPARA
9         Ped_Fil = CONTEUDO
10        Call SEPARA
11        Ped_Tp_Ped = CONTEUDO
12        Call SEPARA
13        Ped_Tp_DB = CONTEUDO
14        Call SEPARA
15        Ped_Tp_Tran = CONTEUDO
16        Call SEPARA
17        Ped_Dt_Dig = CONTEUDO
18        Call SEPARA
19        Ped_Dt_Ped = CONTEUDO
20        Call SEPARA
21        Ped_Dt_Emiss = CONTEUDO
22        Call SEPARA
23        Ped_Dt_Ssm = CONTEUDO
24        Call SEPARA
25        Ped_Dt_Pol = CONTEUDO
26        Call SEPARA
27        Ped_Dt_Cre = CONTEUDO
28        Call SEPARA
29        Ped_Dt_Fre = CONTEUDO
30        Call SEPARA
31        Ped_Nope = Trim(CONTEUDO)
32        Call SEPARA
33        Ped_Cli = CONTEUDO
34        Call SEPARA
35        Ped_For = CONTEUDO
36        Call SEPARA
37        Ped_Ent = CONTEUDO
38        Call SEPARA
39        Ped_Cob = CONTEUDO
40        Call SEPARA
41        Ped_Tran = CONTEUDO
42        Call SEPARA
43        Ped_Rep = CONTEUDO
44        Call SEPARA
45        Ped_Ven = CONTEUDO
46        Call SEPARA
47        Ped_Pla = CONTEUDO
48        Call SEPARA
49        Ped_Ban = CONTEUDO
50        Call SEPARA
51        Ped_Dest = CONTEUDO
52        Call SEPARA
53        Ped_Oper = CONTEUDO
54        Call SEPARA
55        Ped_Fre = CONTEUDO
56        Call SEPARA
57        Ped_Pes = CONTEUDO
58        Call SEPARA
59        Ped_Qtd_Ssm = CONTEUDO
60        Call SEPARA
61        Ped_Qtd_Ped = CONTEUDO
62        Call SEPARA
63        Ped_Qtd_Not = CONTEUDO
64        Call SEPARA
65        Ped_Vl = CONTEUDO
66        Call SEPARA
67        Ped_Ipi = CONTEUDO
68        Call SEPARA
69        Ped_Icm1 = CONTEUDO
70        Call SEPARA
71        Ped_Icm2 = CONTEUDO
72        Call SEPARA
73        Ped_Bas1 = CONTEUDO
74        Call SEPARA
75        Ped_Bas2 = CONTEUDO
76        Call SEPARA
77        Ped_Bas3 = CONTEUDO
78        Call SEPARA
79        Ped_Base = CONTEUDO
80        Call SEPARA
81        Ped_Bas5 = CONTEUDO
82        Call SEPARA
83        Ped_Bas6 = CONTEUDO
84        Call SEPARA
85        Ped_Out = CONTEUDO
86        Call SEPARA
87        Ped_Maj = CONTEUDO
88        Call SEPARA
89        Ped_Ret = CONTEUDO
90        Call SEPARA
91        Ped_Vl_Ipi = CONTEUDO
92        Call SEPARA
93        Ped_Is_Ipi = CONTEUDO
94        Call SEPARA
95        Ped_Ou_Ipi = CONTEUDO
96        Call SEPARA
97        Ped_Vl_Fre = CONTEUDO
98        Call SEPARA
99        Ped_Ace = CONTEUDO
100       Call SEPARA
101       Ped_Des = CONTEUDO
102       Call SEPARA
103       Ped_Suf = CONTEUDO
104       Call SEPARA
105       Ped_Acr = CONTEUDO
106       Call SEPARA
107       Ped_Seg = CONTEUDO
108       Call SEPARA
109       Ped_Pc_Icm1 = CONTEUDO
110       Call SEPARA
111       Ped_Pc_Icm2 = CONTEUDO
112       Call SEPARA
113       Ped_Ali = CONTEUDO
114       Call SEPARA
115       Ped_Can = CONTEUDO
116       Call SEPARA
117       Ped_Fl_Ssm = CONTEUDO
118       Call SEPARA
119       Ped_Fl_Nfis = CONTEUDO
120       Call SEPARA
121       Ped_Fl_Pen = CONTEUDO
122       Call SEPARA
123       Ped_Fl_Ace = CONTEUDO
124       Call SEPARA
125       Ped_Fl_Icm = CONTEUDO
126       Call SEPARA
127       Ped_Sit = CONTEUDO
128       Call SEPARA
129       Ped_Pro = CONTEUDO
130       Call SEPARA
131       Ped_Jur = CONTEUDO
132       Call SEPARA
133       Ped_Che = CONTEUDO
134       Call SEPARA
135       Ped_Com = CONTEUDO
136       Call SEPARA
137       Ped_Dup = CONTEUDO
138       Call SEPARA
139       Ped_Lim = CONTEUDO
140       Call SEPARA
141       Ped_Sal = CONTEUDO
142       Call SEPARA
143       Ped_Cli_Nov = CONTEUDO
144       Call SEPARA
145       Ped_Desc = CONTEUDO
146       Call SEPARA
147       Ped_Acre = CONTEUDO
148       Call SEPARA
149       Ped_VlFat = CONTEUDO
150       Call SEPARA
151       Ped_Desc1 = CONTEUDO
152       Call SEPARA
153       Ped_Desc2 = CONTEUDO
154       Call SEPARA
155       Ped_Desc3 = CONTEUDO
156       Call SEPARA
157       Ped_Fret = CONTEUDO
158       Call SEPARA
159       Ped_MensP = CONTEUDO
160       Call SEPARA
161       Ped_MensN = CONTEUDO
162       Call SEPARA
163       Ped_CODVDR = CONTEUDO
          
         '*******************************************
             
         '*******************************************
         'Grava valores lidos na tabela PEDNOTA_VENDA
165       strSQL = "SELECT * from PEDNOTA_VENDA "
166       strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
167       strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
168       strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "

169       Set recTab = New ADODB.Recordset
          recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
          
         'Formata��o das datas
171       If Ped_Dt_Emiss <> "0" Then
172           Ped_Dt_Emiss = Format$(Ped_Dt_Emiss, "dd/mm/yy hh:nn:ss")
173       End If
174       If Ped_Dt_Ssm <> "0" Then
175           Ped_Dt_Ssm = Format$(Ped_Dt_Ssm, "dd/mm/yy hh:nn:ss")
176       End If
177       If Ped_Dt_Pol <> "0" Then
178           Ped_Dt_Pol = Format$(Ped_Dt_Pol, "dd/mm/yy hh:nn:ss")
179       End If
180       If Ped_Dt_Cre <> "0" Then
181           Ped_Dt_Cre = Format$(Ped_Dt_Cre, "dd/mm/yy hh:nn:ss")
182       End If
183       If Ped_Dt_Fre <> "0" Then
184           Ped_Dt_Fre = Format$(Ped_Dt_Fre, "dd/mm/yy hh:nn:ss")
185       End If
          
186       Conn.BeginTrans
187       If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
188           strSQL = "UPDATE "
189           strSQL = strSQL & " PEDNOTA_VENDA "
190           strSQL = strSQL & " SET "
191           strSQL = strSQL & " COD_LOJA_NOTA =  " & Ped_Loja_Nota & ", "
192           strSQL = strSQL & " NUM_NOTA = " & Ped_Num_Nota & ", "
193           strSQL = strSQL & " NUM_PENDENTE = " & Val(Mid(Ped_Pen, 3, 11)) & ", "
194           strSQL = strSQL & " COD_FILIAL = " & Ped_Fil & ", "
195           strSQL = strSQL & " TP_PEDIDO = " & Ped_Tp_Ped & ","
196           strSQL = strSQL & " TP_DPKBLAU = " & Ped_Tp_DB & ","
197           strSQL = strSQL & " TP_TRANSACAO = " & Ped_Tp_Tran & ", "
198           strSQL = strSQL & " DT_DIGITACAO = '" & Ped_Dt_Dig & "',"
199           strSQL = strSQL & " DT_PEDIDO = '" & Format$(Ped_Dt_Ped, "DD/MM/YY") & "', "
200           strSQL = strSQL & " DT_EMISSAO_NOTA = '" & Ped_Dt_Emiss & "' ,"
201           strSQL = strSQL & " DT_SSM = '" & Ped_Dt_Ssm & "',"
202           strSQL = strSQL & " DT_BLOQ_POLITICA = '" & Ped_Dt_Pol & "',"
203           strSQL = strSQL & " DT_BLOQ_CREDITO = '" & Ped_Dt_Cre & "',"
204           strSQL = strSQL & " DT_BLOQ_FRETE = '" & Ped_Dt_Fre & "',"
205           strSQL = strSQL & " COD_NOPE= '" & Trim(Ped_Nope) & "',"
206           strSQL = strSQL & " COD_CLIENTE = " & Ped_Cli & ","
207           strSQL = strSQL & " COD_FORNECEDOR = " & Ped_For & ","
208           strSQL = strSQL & " COD_END_ENTREGA = " & Ped_Ent & ","
209           strSQL = strSQL & " COD_END_COBRANCA = " & Ped_Cob & ","
210           strSQL = strSQL & " COD_TRANSP = " & Ped_Tran & ", "
211           strSQL = strSQL & " COD_REPRES = " & Ped_Rep & " ,"
212           strSQL = strSQL & " COD_BANCO = " & Ped_Ban & " ,"
213           strSQL = strSQL & " COD_VEND = " & Ped_Ven & ", "
214           strSQL = strSQL & " COD_PLANO = " & Ped_Pla & ", "
215           strSQL = strSQL & " COD_CFO_DEST = " & Ped_Dest & " ,"
216           strSQL = strSQL & " COD_CFO_OPER = " & Ped_Oper & ", "
217           strSQL = strSQL & " FRETE_PAGO = '" & Ped_Fre & "', "
218           strSQL = strSQL & " PESO_BRUTO = " & Ped_Pes & ", "
219           strSQL = strSQL & " QTD_SSM = " & Ped_Qtd_Ssm & ","
220           strSQL = strSQL & " QTD_ITEM_PEDIDO = " & Ped_Qtd_Ped & ","
221           strSQL = strSQL & " QTD_ITEM_NOTA = " & Ped_Qtd_Not & ","
222           strSQL = strSQL & " VL_CONTABIL = " & Ped_Vl & ","
223           strSQL = strSQL & " VL_IPI = " & Ped_Ipi & ","
224           strSQL = strSQL & " VL_BASEICM1 = " & Ped_Icm1 & ","
225           strSQL = strSQL & " VL_BASEICM2 = " & Ped_Icm2 & ","
226           strSQL = strSQL & " VL_BASE_1 = " & Ped_Bas1 & ", "
227           strSQL = strSQL & " VL_BASE_2 = " & Ped_Bas2 & ","
228           strSQL = strSQL & " VL_BASE_3 = " & Ped_Bas3 & ","
229           strSQL = strSQL & " VL_BASEISEN = " & Ped_Base & ","
230           strSQL = strSQL & " VL_BASE_5 = " & Ped_Bas5 & " ,"
231           strSQL = strSQL & " VL_BASE_6 = " & Ped_Bas6 & ","
232           strSQL = strSQL & " VL_BASEOUTR = " & Ped_Out & " ,"
233           strSQL = strSQL & " VL_BASEMAJ = " & Ped_Maj & ","
234           strSQL = strSQL & " VL_ICMRETIDO= " & Ped_Ret & " ,"
235           strSQL = strSQL & " VL_BASEIPI = " & Ped_Vl_Ipi & " ,"
236           strSQL = strSQL & " VL_BISENIPI = " & Ped_Is_Ipi & " ,"
237           strSQL = strSQL & " VL_BOUTRIPI = " & Ped_Ou_Ipi & ","
238           strSQL = strSQL & " VL_FRETE = " & Ped_Vl_Fre & ","
239           strSQL = strSQL & " VL_DESP_ACESS = " & Ped_Ace & " ,"
240           strSQL = strSQL & " PC_DESCONTO = " & Ped_Des & " ,"
241           strSQL = strSQL & " PC_DESC_SUFRAMA = " & Ped_Suf & " ,"
242           strSQL = strSQL & " PC_ACRESCIMO = " & Ped_Acr & " , "
243           strSQL = strSQL & " PC_SEGURO = " & Ped_Seg & " ,"
244           strSQL = strSQL & " PC_ICM1 = " & Ped_Pc_Icm1 & " ,"
245           strSQL = strSQL & " PC_ICM2 = " & Ped_Pc_Icm2 & ","
246           strSQL = strSQL & " PC_ALIQ_INTERNA = " & Ped_Ali & " ,"
247           strSQL = strSQL & " COD_CANCEL = " & Ped_Can & ","
248           strSQL = strSQL & " FL_GER_SSM = '" & Ped_Fl_Ssm & "' ,"
249           strSQL = strSQL & " FL_GER_NFIS = '" & Ped_Fl_Nfis & "',"
250           strSQL = strSQL & " FL_PENDENCIA = '" & Ped_Fl_Pen & "' ,"
251           strSQL = strSQL & " FL_DESP_ACESS = '" & Ped_Fl_Ace & "',"
252           strSQL = strSQL & " FL_DIF_ICM = '" & Ped_Fl_Icm & "',"
253           strSQL = strSQL & " SITUACAO = " & Ped_Sit & " ,"
254           strSQL = strSQL & " BLOQ_PROTESTO = '" & Ped_Pro & "' ,"
255           strSQL = strSQL & " BLOQ_JUROS = '" & Ped_Jur & "',"
256           strSQL = strSQL & " BLOQ_CHEQUE = '" & Ped_Che & "',"
257           strSQL = strSQL & " BLOQ_COMPRA = '" & Ped_Com & "' ,"
258           strSQL = strSQL & " BLOQ_DUPLICATA = '" & Ped_Dup & "',"
259           strSQL = strSQL & " BLOQ_LIMITE ='" & Ped_Lim & "' ,"
260           strSQL = strSQL & " BLOQ_SALDO = '" & Ped_Sal & "',"
261           strSQL = strSQL & " BLOQ_CLIE_NOVO = '" & Ped_Cli_Nov & "', "
262           strSQL = strSQL & " BLOQ_DESCONTO = '" & Ped_Desc & "', "
263           strSQL = strSQL & " BLOQ_ACRESCIMO = '" & Ped_Acre & "', "
264           strSQL = strSQL & " BLOQ_VLFATMIN = '" & Ped_VlFat & "', "
265           strSQL = strSQL & " BLOQ_ITEM_DESC1 = '" & Ped_Desc1 & "' ,"
266           strSQL = strSQL & " BLOQ_ITEM_DESC2 = '" & Ped_Desc2 & "' ,"
267           strSQL = strSQL & " BLOQ_ITEM_DESC3 = '" & Ped_Desc3 & "',"
268           strSQL = strSQL & " BLOQ_FRETE = '" & Ped_Fret & "',"
269           strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' ,"
270           strSQL = strSQL & " MENS_NOTA = '" & Ped_MensN & "' ,"
271           strSQL = strSQL & " COD_VDR = " & Ped_CODVDR & " "
272           strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
273           strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
274           strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq

275           Conn.Execute strSQL
276       Else    'N�o Existe o Pedido - Incluir
277           strSQL = "INSERT "
278           strSQL = strSQL & " into PEDNOTA_VENDA values "
279           strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
280           strSQL = strSQL & " " & Ped_Loja_Nota & " , " & Ped_Num_Nota & ", "
281           strSQL = strSQL & " " & Val(Mid(Ped_Pen, 3, 11)) & ", " & Ped_Fil & " , " & Ped_Tp_Ped & " ,"
282           strSQL = strSQL & " " & Ped_Tp_DB & " , " & Ped_Tp_Tran & ", '" & Ped_Dt_Dig & "', "
283           strSQL = strSQL & " '" & Ped_Dt_Ped & "' , '" & Ped_Dt_Emiss & "' , '" & Ped_Dt_Ssm & "' , "
284           strSQL = strSQL & " '" & Ped_Dt_Pol & "' , '" & Ped_Dt_Cre & "','" & Ped_Dt_Fre & "',"
285           strSQL = strSQL & " '" & Ped_Nope & "' , " & Ped_Cli & ", " & Ped_For & " ,"
286           strSQL = strSQL & " " & Ped_Ent & " , " & Ped_Cob & ", " & Ped_Tran & ","
287           strSQL = strSQL & " " & Ped_Rep & ", " & Ped_Ven & " , " & Ped_Pla & ", "
288           strSQL = strSQL & " " & Ped_Ban & ", " & Ped_Dest & ", " & Ped_Oper & " ,"
289           strSQL = strSQL & " '" & Ped_Fre & "', " & Ped_Pes & ", " & Ped_Qtd_Ssm & " , "
290           strSQL = strSQL & " " & Ped_Qtd_Ped & ", " & Ped_Qtd_Not & ", " & Ped_Vl & " ,"
291           strSQL = strSQL & " " & Ped_Ipi & ", " & Ped_Icm1 & ", " & Ped_Icm2 & ", "
292           strSQL = strSQL & " " & Ped_Bas1 & ", " & Ped_Bas2 & " , "
293           strSQL = strSQL & " " & Ped_Bas3 & " , " & Ped_Base & " , " & Ped_Bas5 & " ,"
294           strSQL = strSQL & " " & Ped_Bas6 & " , " & Ped_Out & " , " & Ped_Maj & ","
295           strSQL = strSQL & " " & Ped_Ret & ", " & Ped_Vl_Ipi & ", " & Ped_Is_Ipi & " ,"
296           strSQL = strSQL & " " & Ped_Ou_Ipi & ", " & Ped_Vl_Fre & " , " & Ped_Ace & " ,"
297           strSQL = strSQL & " " & Ped_Des & " , " & Ped_Suf & ", " & Ped_Acr & " ,"
298           strSQL = strSQL & " " & Ped_Seg & ", " & Ped_Pc_Icm1 & " , " & Ped_Pc_Icm2 & " , "
299           strSQL = strSQL & " " & Ped_Ali & " , " & Ped_Can & " , '" & Ped_Fl_Ssm & "' , "
300           strSQL = strSQL & " '" & Ped_Fl_Nfis & "' , '" & Ped_Fl_Pen & "' , '" & Ped_Fl_Ace & "' ,"
301           strSQL = strSQL & " '" & Ped_Fl_Icm & "' , " & Ped_Sit & " , '" & Ped_Pro & "' ,"
302           strSQL = strSQL & " '" & Ped_Jur & "', '" & Ped_Che & "',  '" & Ped_Com & "', '" & Ped_Dup & "',"
303           strSQL = strSQL & " '" & Ped_Lim & "' , '" & Ped_Sal & "' , '" & Ped_Cli_Nov & "', "
304           strSQL = strSQL & " '" & Ped_Desc & "', '" & Ped_Acre & "' , '" & Ped_VlFat & "' ,"
305           strSQL = strSQL & " '" & Ped_Desc1 & "' , '" & Ped_Desc2 & "' , '" & Ped_Desc3 & "' ,"
306           strSQL = strSQL & " '" & Ped_Fret & "' , '" & Ped_MensP & "' , '" & Ped_MensN & "', " & Ped_CODVDR & ")"
307           Conn.Execute strSQL
308       End If
309       Conn.CommitTrans
          
         'SE HOUVER PEDIDO DE TRANSFER�NCIA
310       strSQL = "Select * from R_COTACAO_TRANSF where COD_LOJA_COTACAO = " & Val(Mid(Ped_Pen, 1, 2)) & " AND " & _
                   "NUM_COTACAO_TEMP = " & Val(Mid(Ped_Pen, 3, 11))
          
311       Set recTab = New ADODB.Recordset
          recTab.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
              
          Dim ped_cotacao
          
312       If Not recTab.EOF Then
313           Conn.BeginTrans
314           If Ped_Nope = "A30" Then
315               ped_cotacao = Mid(Ped_MensN, InStr(Ped_MensN, ":") + 2)
                  
                 'n�mero do pedido de transfer�ncia
316               strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_TRANSF =" & Ped_Loja & ", " & _
                           "NUM_PEDIDO =" & Ped_Num & ", SEQ_PEDIDO =" & Ped_Seq & ", " & _
                           "NUM_COTACAO = " & ped_cotacao & " where " & _
                           "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                           "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                           
317               Conn.Execute strSQL
318           Else
                 'n�mero do pedido de venda
319               strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_VENDA =" & Ped_Loja & ", " & _
                           "NUM_PEDIDO_VENDA =" & Ped_Num & ", SEQ_PEDIDO_VENDA =" & Ped_Seq & " where " & _
                           "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                           "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                           
320               Conn.Execute strSQL
321           End If
322           Conn.CommitTrans
323       End If
          
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_PED" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl & "StrSql:" & strSQL
    End If

End Function

Public Function CHAVE_PED()
   'Primeira linha do arquivo guarda a chave da tabela
   'PEDNOTA_VENDA: COD_LOJA + NUM_PEDIDO + SEQ_PEDIDO
    
    Call SEPARA
    Ped_Loja = CONTEUDO
    Call SEPARA
    Ped_Num = CONTEUDO
    Call SEPARA
    Ped_Seq = CONTEUDO

End Function

Public Function SEPARA()

    On Error GoTo Trata_Erro

1         POS_SEP = InStr(TMP_LINHA, "|")

2         CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
3         If UCase(CONTEUDO) = "PED" Then
4             TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
5             POS_SEP = InStr(TMP_LINHA, "|")
6             CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
7         End If
          
8         If UCase(CONTEUDO) = "ITE" Then
9             TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
10            POS_SEP = InStr(TMP_LINHA, "|")
11            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
12        End If
          
13        If UCase(CONTEUDO) = "ROM" Then
14            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
15            POS_SEP = InStr(TMP_LINHA, "|")
16            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
17        End If
          
18        If UCase(CONTEUDO) = "V_P" Then
19            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
20            POS_SEP = InStr(TMP_LINHA, "|")
21            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
22        End If
          
23        If UCase(CONTEUDO) = "R_P" Then
24            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
25            POS_SEP = InStr(TMP_LINHA, "|")
26            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
27        End If
          
28        If CONTEUDO = "" Then
29            CONTEUDO = 0
30        End If
          
31        TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub SEPARA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function
Public Sub COMPACTA_BANCO()

    On Error GoTo Trata_Erro

1         arqmdb = Dir(Path_drv & "\DADOS\BASE_DPK.MDB")
2         dir_base = Path_drv & "\DADOS\"
         'arqmdb = Path_drv &"\COM\ENTRADA\ARQUIVOS\" & arqmdb
          
3         mdiGER040.Label1.Caption = "COMPACTANDO BANCO = " & Mid(arqmdb, 1, 4)
4         DBEngine.CompactDatabase dir_base & Mid(arqmdb, 1, 8) & ".MDB", dir_base & "temp.mdb", "", dbEncrypt
5         Kill dir_base & Mid(arqmdb, 1, 8) & ".MDB"
6         Name dir_base & "temp.mdb" As dir_base & Mid(arqmdb, 1, 8) & ".MDB"
7         arqmdb = Dir

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub COMPACTA_BANCO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Function ATU_CLIENTE_ACUMULADO() As Boolean
          Dim DTFATU As String
          Dim DTINIDIA As String
          Dim CLIENTE As Long
          Dim MES1 As String
          Dim VL_1 As Double
          Dim MES2 As String
          Dim VL_2 As Double
          Dim MES3 As String
          Dim VL_3 As Double
          Dim MES4 As String
          Dim VL_4 As Double

1         On Error GoTo VerErro

         '-------------------------------------------------------------
          'Alterado por Eduardo Relvas
          'Alterado em 21/01/05
          'Objetivo:
          '    Atualizar o campo Valor1=0 quando o campo Valor1 for nulo
          '    Atualizar o campo Valor2=0 quando o campo Valor2 for nulo
          '    Atualizar o campo Valor3=0 quando o campo Valor3 for nulo
          '    Atualizar o campo Valor4=0 quando o campo Valor4 for nulo
          '-------------------------------------------------------------
          
2         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR1 = 0 " & _
                   "WHERE VALOR1 IS NULL"
3         Conn.Execute strSQL

4         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR2 = 0 " & _
                   "WHERE VALOR2 IS NULL"
5         Conn.Execute strSQL

6         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR3 = 0 " & _
                   "WHERE VALOR3 IS NULL"
7         Conn.Execute strSQL

8         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR4 = 0 " & _
                   "WHERE VALOR4 IS NULL"
9         Conn.Execute strSQL

          
10        strSQL = "SELECT dt_faturamento, dt_ini_fech_dia FROM DATAS"
          
11        Set ss = New ADODB.Recordset
          ss.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
          
12        DTFATU = Format(ss!DT_FATURAMENTO, "dd/mm/yy")
13        DTINIDIA = Format(ss!DT_INI_FECH_DIA, "dd/mm/yy")
          
14        If Mid(DTFATU, 4, 2) <> Mid(DTINIDIA, 4, 2) Then
15            strSQL = "SELECT * FROM cliente_acumulado"
              
16            Set ss = New ADODB.Recordset
              ss.Open strSQL, Conn, adOpenKeyset, adLockOptimistic
              
17            Do While Not ss.EOF
18                CLIENTE = ss!cod_cliente
19                MES1 = Mid(DTFATU, 4, 2) & "-" & Mid(Format(DTFATU, "DD/MM/YYYY"), 7, 4)
20                VL_1 = 0
21                MES2 = ss!ANO_MES1
22                VL_2 = IIf(IsNull(ss!valor1), 0, ss!valor1)
23                MES3 = ss!ANO_MES2
24                VL_3 = IIf(IsNull(ss!valor2), 0, ss!valor2)
25                MES4 = ss!ANO_MES3
26                VL_4 = IIf(IsNull(ss!valor3), 0, ss!valor3)
              
27                strSQL = "UPDATE cliente_acumulado " & _
                           "SET ANO_MES1 = '" & MES1 & "', VALOR1 = " & VL_1 & ", " & _
                           "ANO_MES2 = '" & MES2 & "', VALOR2 = " & VL_2 & ", " & _
                           "ANO_MES3 = '" & MES3 & "', VALOR3 = " & VL_3 & ", " & _
                           "ANO_MES4 = '" & MES4 & "', VALOR4 = " & VL_4 & " " & _
                           "WHERE COD_CLIENTE = " & CLIENTE

28                Conn.Execute strSQL
              
29                ss.MoveNext
30            Loop
31        End If

         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
32        strSQL = "SELECT * INTO AUX_CLIENTE_ACUMULADO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIENTE_ACUMULADO "
33        dbExt.Execute strSQL
            
34        If FL_TIPO = "D" Then
              'Efetua Update de uma tabela com base na outra .......................
35             strSQL = "UPDATE CLIENTE_ACUMULADO INNER JOIN AUX_CLIENTE_ACUMULADO ON  " & _
                        " CLIENTE_ACUMULADO.COD_CLIENTE = AUX_CLIENTE_ACUMULADO.COD_CLIENTE " & _
                        " SET CLIENTE_ACUMULADO.ANO_MES1 = AUX_CLIENTE_ACUMULADO.ANO_MES1, " & _
                        " CLIENTE_ACUMULADO.VALOR1 = AUX_CLIENTE_ACUMULADO.VALOR1, " & _
                        " CLIENTE_ACUMULADO.ANO_MES2 = AUX_CLIENTE_ACUMULADO.ANO_MES2, " & _
                        " CLIENTE_ACUMULADO.ANO_MES3 = AUX_CLIENTE_ACUMULADO.ANO_MES3, " & _
                        " CLIENTE_ACUMULADO.ANO_MES4 = AUX_CLIENTE_ACUMULADO.ANO_MES4 "
36        Else
              'Efetua Update de uma tabela com base na outra .......................
37             strSQL = "UPDATE CLIENTE_ACUMULADO INNER JOIN AUX_CLIENTE_ACUMULADO ON  " & _
                        " CLIENTE_ACUMULADO.COD_CLIENTE = AUX_CLIENTE_ACUMULADO.COD_CLIENTE " & _
                        " SET CLIENTE_ACUMULADO.ANO_MES1 = AUX_CLIENTE_ACUMULADO.ANO_MES1, " & _
                        " CLIENTE_ACUMULADO.VALOR1 = AUX_CLIENTE_ACUMULADO.VALOR1, " & _
                        " CLIENTE_ACUMULADO.ANO_MES2 = AUX_CLIENTE_ACUMULADO.ANO_MES2, " & _
                        " CLIENTE_ACUMULADO.VALOR2 = AUX_CLIENTE_ACUMULADO.VALOR2, " & _
                        " CLIENTE_ACUMULADO.ANO_MES3 = AUX_CLIENTE_ACUMULADO.ANO_MES3, " & _
                        " CLIENTE_ACUMULADO.VALOR3 = AUX_CLIENTE_ACUMULADO.VALOR3, " & _
                        " CLIENTE_ACUMULADO.ANO_MES4 = AUX_CLIENTE_ACUMULADO.ANO_MES4, " & _
                        " CLIENTE_ACUMULADO.VALOR4 = AUX_CLIENTE_ACUMULADO.VALOR4 "
38        End If
          
39        Conn.Execute strSQL
             
         'Com base na tabela AUX, inclui registros na tabela Original .........
40        strSQL = "INSERT INTO CLIENTE_ACUMULADO " & _
                   "SELECT COD_CLIENTE, ANO_MES1, VALOR1, ANO_MES2, ANO_MES3, ANO_MES4 " & _
                   "FROM AUX_CLIENTE_ACUMULADO "

41        Conn.Execute strSQL
          
         'Deleta do Banco atual a tabela temporaria ...........................
42        Conn.Execute "DROP TABLE AUX_CLIENTE_ACUMULADO"
              
43        ATU_CLIENTE_ACUMULADO = True

44    Exit Function

VerErro:

45        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
46            Conn.Execute "DROP TABLE AUX_CLIENTE_ACUMULADO"
47            Resume
48        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
49            Resume Next
50        Else
51            ATU_CLIENTE_ACUMULADO = False
52            Screen.MousePointer = 1
              
53            Call FUNCAO_ERRO("BC")
              MsgBox "Sub ATU_CLIENTE_ACUMULADO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
54        End If

End Function

Public Sub COMPACTA_AUXILIAR()
          
    On Error GoTo Trata_Erro
          
          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD") = Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD" Then
2             Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, Path_drv & "\COM\ENTRADA\ARQUIVOS\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq
6         VDestino = Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".NEW"
9         VDestino = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub COMPACTA_AUXILIAR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Public Sub COMPACTA_LOCAL()

    On Error GoTo Trata_Erro

          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\DADOS\OLD\BASE_DPK.OLD") = "BASE_DPK.OLD" Then
2             Kill Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASE_DPK.NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\DADOS\BASE_DPK.MDB"
6         VDestino = Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\DADOS\BASE_DPK.NEW"
9         VDestino = Path_drv & "\DADOS\BASE_DPK.MDB"
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub COMPACTA_LOCAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Sub COMPACTA_CONTROLE()

    On Error GoTo Trata_Erro

          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\LOG\OLD\CONTROLE.OLD") = "CONTROLE.OLD" Then
2             Kill Path_drv & "\LOG\OLD\CONTROLE.OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase "Path_drv & \LOG\CONTROLE.MDB", Path_drv & "\LOG\CONTROLE.NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\LOG\CONTROLE.MDB"
6         VDestino = Path_drv & "\LOG\OLD\CONTROLE.OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\LOG\CONTROLE.NEW"
9         VDestino = Path_drv & "\LOG\CONTROLE.MDB"
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub COMPACTA_CONTROLE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Public Function DEL_PEDNOTA() As Boolean

         'DELETA PEDIDOS (PEDNOTA_VENDA) E ITENS DO PEDIDO (ITPEDNOTA_VENDA)
         'COM DATA DE PEDIDO MAIOR QUE 30 DIAS.
         'MANTEM APENAS OS PEDIDOS COM NF EMITIDAS POR UM PERIODO DE 1 MES.

1         On Error GoTo VerErro

         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
         'strSQL = "SELECT * INTO AUX_DEL_PEDNOTA IN " + Chr(34) + Path_drv &"\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_PEDNOTA "
         'dbExt.Execute strSQL
          
         'PEDIDOS QUE DEVEM SER DELETADOS
         
2         strSQL = "Select dt_faturamento from datas"
          
3         Set ss = Conn.OpenRecordset(strSQL, dbOpenSnapshot)

          Dim MDT_AUX As Date
4         MDT_AUX = ss!DT_FATURAMENTO - 30
         
5         strSQL = "INSERT INTO  DELETA_PEDNOTA " & _
                   " SELECT COD_LOJA, NUM_PEDIDO,SEQ_PEDIDO " & _
                   " FROM PEDNOTA_VENDA " & _
                   " WHERE DT_PEDIDO <  cdate('" & CDate(MDT_AUX) & "')"
6         Conn.Execute strSQL
            
         'DELETA O CABECALHO DOS PEDIDOS
7                  strSQL = " DELETE PEDNOTA_VENDA.* "
8         strSQL = strSQL & " FROM PEDNOTA_VENDA INNER JOIN DELETA_PEDNOTA ON "
9         strSQL = strSQL & " PEDNOTA_VENDA.COD_LOJA=DELETA_PEDNOTA.COD_LOJA AND "
10        strSQL = strSQL & " PEDNOTA_VENDA.NUM_PEDIDO = DELETA_PEDNOTA.NUM_PEDIDO  AND "
11        strSQL = strSQL & " PEDNOTA_VENDA.SEQ_PEDIDO = DELETA_PEDNOTA.SEQ_PEDIDO  "

12        Conn.Execute strSQL

         'DELETA ITENS DOS PEDIDOS
13                 strSQL = "DELETE ITPEDNOTA_VENDA.* "
14        strSQL = strSQL & " FROM ITPEDNOTA_VENDA INNER JOIN DELETA_PEDNOTA ON "
15        strSQL = strSQL & " ITPEDNOTA_VENDA.COD_LOJA = DELETA_PEDNOTA.COD_LOJA AND "
16        strSQL = strSQL & " ITPEDNOTA_VENDA.NUM_PEDIDO = DELETA_PEDNOTA.NUM_PEDIDO AND "
17        strSQL = strSQL & " ITPEDNOTA_VENDA.SEQ_PEDIDO = DELETA_PEDNOTA.SEQ_PEDIDO  "
          
18        Conn.Execute strSQL
             
19        DEL_PEDNOTA = True

20    Exit Function

VerErro:

21        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
22            Conn.Execute "DROP TABLE AUX_DEL_PEDNOTA"
23            Resume
24        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
25            Resume Next
26        Else
27            DEL_PEDNOTA = False
28            Screen.MousePointer = 1
29            Call FUNCAO_ERRO("")
              MsgBox "Sub DEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If

End Function

Public Function ATU_DATAS() As Boolean

1         On Error GoTo VerErro
          
         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2         strSQL = "SELECT * INTO AUX_DATAS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DATAS "
3         dbExt.Execute strSQL
4         Set MyRec = Conn.OpenRecordset("AUX_DATAS")
5         MyRec.MoveLast
          
6         If MyRec.RecordCount > 0 Then
             'Efetua Update de uma tabela com base na outra .......................
7             strSQL = "DELETE FROM DATAS "
8             Conn.Execute strSQL
          
             'Com base na tabela AUX, inclui registros na tabela Original .........
9             strSQL = "INSERT INTO DATAS SELECT * FROM AUX_DATAS"
10            Conn.Execute strSQL
11        End If
          
12        MyRec.Close
          
13        strSQL = "DELETE DOLAR_DIARIO.* FROM DOLAR_DIARIO INNER JOIN DATAS " & _
                   " ON DOLAR_DIARIO.DATA_USS <=  " & _
                   " DATEADD('M',-3,DATAS.DT_FATURAMENTO) "
14        Conn.Execute strSQL
              
         'Deleta do Banco atual a tabela temporaria ...........................
15        Conn.Execute "DROP TABLE AUX_DATAS"
                  
16        ATU_DATAS = True

17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            Conn.Execute "DROP TABLE AUX_DATAS"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_DATAS = False
25            Screen.MousePointer = 1
              MsgBox "Sub ATU_DATAS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If
          

End Function

Public Function ATU_DEPOSITO_VISAO() As Boolean

          Dim MyRec As Recordset
          
1         On Error GoTo VerErro
          
         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2         strSQL = "SELECT * INTO AUX_DEPOSITO_VISAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEPOSITO_VISAO "
3         dbExt.Execute strSQL
         'Label2.Caption = dbExt.RecordsAffected
4         Set MyRec = Conn.OpenRecordset("AUX_DEPOSITO_VISAO")
         'MyRec.MoveLast
          
         'If MyRec.RecordCount > 0 Then
5         If MyRec.BOF = False Then
             'Efetua Update de uma tabela com base na outra .......................
6             strSQL = "DELETE FROM DEPOSITO_VISAO "
7             Conn.Execute strSQL
          
             'Com base na tabela AUX, inclui registros na tabela Original .........
8             strSQL = "INSERT INTO DEPOSITO_VISAO SELECT * FROM AUX_DEPOSITO_VISAO"
9             Conn.Execute strSQL
10        End If
          
11        MyRec.Close
          
         'Deleta do Banco atual a tabela temporaria ...........................
12        Conn.Execute "DROP TABLE AUX_DEPOSITO_VISAO"
          
13        ATU_DEPOSITO_VISAO = True

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            Conn.Execute "DROP TABLE AUX_DEPOSITO_VISAO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_DEPOSITO_VISAO = False
22            Screen.MousePointer = 1
23            Call FUNCAO_ERRO("EW")
              MsgBox "Sub ATU_DEPOSITO_VISAO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_LOJA() As Boolean

1     On Error GoTo VerErro

      ' Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2     strSQL = "SELECT * INTO AUX_LOJA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM LOJA "
3     dbExt.Execute strSQL
      'Label2.Caption = dbExt.RecordsAffected
4     Set MyRec = Conn.OpenRecordset("AUX_LOJA")
      'MyRec.MoveLast
5     If MyRec.BOF = False Then
      'If MyRec.RecordCount > 0 Then
          ' Efetua Update de uma tabela com base na outra .......................
6         strSQL = "DELETE FROM LOJA "
7         Conn.Execute strSQL

          ' Com base na tabela AUX, inclui registros na tabela Original .........
8         strSQL = "INSERT INTO LOJA SELECT * FROM AUX_LOJA"
9         Conn.Execute strSQL
10    End If

11    MyRec.Close

      ' Deleta do Banco atual a tabela temporaria ...........................
12    Conn.Execute "DROP TABLE AUX_LOJA"

              
13    ATU_LOJA = True

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        Conn.Execute "DROP TABLE AUX_LOJA"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_LOJA = False
22        Screen.MousePointer = 1
23        Call FUNCAO_ERRO("CB")
          MsgBox "Sub ATU_LOJA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_CANCEL_PEDNOTA() As Boolean

1         On Error GoTo VerErro
          
         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2         strSQL = "SELECT * INTO AUX_CANCEL_PEDNOTA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CANCEL_PEDNOTA "
3         dbExt.Execute strSQL
         'Label2.Caption = dbExt.RecordsAffected
4         Set MyRec = Conn.OpenRecordset("AUX_CANCEL_PEDNOTA")
         
         'MyRec.MoveLast
         'strSQL = "SELECT * FROM AUX_CANCEL_PEDNOTA "
         'conn.Execute strSQL
          
5         If MyRec.BOF = False Then
          
             'If MyRec.RecordCount > 0 Then
             'Efetua Update de uma tabela com base na outra .......................
6             strSQL = "DELETE FROM CANCEL_PEDNOTA "
7             Conn.Execute strSQL
               
             'Com base na tabela AUX, inclui registros na tabela Original .........
8             strSQL = "INSERT INTO CANCEL_PEDNOTA SELECT * FROM AUX_CANCEL_PEDNOTA"
9             Conn.Execute strSQL
10        End If
          
11        MyRec.Close
          
         'Deleta do Banco atual a tabela temporaria ...........................
12        Conn.Execute "DROP TABLE AUX_CANCEL_PEDNOTA"
          
13        ATU_CANCEL_PEDNOTA = True

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            Conn.Execute "DROP TABLE AUX_CANCEL_PEDNOTA"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CANCEL_PEDNOTA = False
22            Screen.MousePointer = 1
23            Call FUNCAO_ERRO("AB")
              MsgBox "Sub ATU_CANCEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function

Public Function ATU_SALDO_PEDIDO() As Boolean

1         On Error GoTo VerErro
          
         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2         strSQL = "SELECT * INTO AUX_SALDO_PEDIDOS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM SALDO_PEDIDOS "
3         dbExt.Execute strSQL
         'Label2.Caption = dbExt.RecordsAffected
4         Set MyRec = Conn.OpenRecordset("AUX_SALDO_PEDIDOS")
         'MyRec.MoveLast
          
         'If MyRec.RecordCount > 0 Then
5         If MyRec.BOF = False Then
             'Efetua Update de uma tabela com base na outra .......................
6             strSQL = "DELETE FROM SALDO_PEDIDOS "
7             Conn.Execute strSQL
          
             'Com base na tabela AUX, inclui registros na tabela Original .........
8             strSQL = "INSERT INTO SALDO_PEDIDOS SELECT * FROM AUX_SALDO_PEDIDOS"
9             Conn.Execute strSQL
10        End If
          
11        MyRec.Close
              
         'Deleta do Banco atual a tabela temporaria ...........................
12        Conn.Execute "DROP TABLE AUX_SALDO_PEDIDOS"
                  
13        ATU_SALDO_PEDIDO = True

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            Conn.Execute "DROP TABLE AUX_SALDO_PEDIDOS"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_SALDO_PEDIDO = False
22            Screen.MousePointer = 1
23            Call FUNCAO_ERRO("AF")
              MsgBox "Sub ATU_SALDO_PEDIDO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function

Public Function ATU_ESTAITEM() As Boolean

1     On Error GoTo VerErro

      ' Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2     strSQL = "SELECT * INTO AUX_ESTAITEM IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ESTATISTICA_ITEM "
3     dbExt.Execute strSQL
4     If dbExt.RecordsAffected > 0 Then
5         strSQL = "DELETE ESTATISTICA_ITEM.* FROM ESTATISTICA_ITEM "
6         Conn.Execute strSQL
7     End If
             
      ' Com base na tabela AUX, inclui registros na tabela Original .........
8     strSQL = "INSERT INTO ESTATISTICA_ITEM SELECT * FROM AUX_ESTAITEM "
9     Conn.Execute strSQL
      '    Label4.Caption = Db.RecordsAffected
          
      ' Deleta do Banco atual a tabela temporaria ...........................
10    Conn.Execute "DROP TABLE AUX_ESTAITEM "
              
11    ATU_ESTAITEM = True

12    Exit Function

VerErro:

13    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
14        Conn.Execute "DROP TABLE AUX_ESTAITEM"
15        Resume
16    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
17        Resume Next
18    Else
19        ATU_ESTAITEM = False
20        Screen.MousePointer = 1
21        Call FUNCAO_ERRO("AI")
          MsgBox "Sub ATU_ESTAITEM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22    End If

End Function

Public Function ATU_TAXA() As Boolean

1         On Error GoTo VerErro
          
         'Cria a Tabela Auxiliar no BASE_DPK (Banco de AUX da Regional) .........
2         strSQL = "SELECT * INTO AUX_TAXA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TAXA "
3         dbExt.Execute strSQL
         'Label2.Caption = dbExt.RecordsAffected
4         Set MyRec = Conn.OpenRecordset("AUX_TAXA")
5         MyRec.MoveLast
6         If MyRec.RecordCount > 0 Then
             'Efetua Update de uma tabela com base na outra .......................
7             strSQL = "DELETE FROM TAXA "
8             Conn.Execute strSQL
          
             'Com base na tabela AUX, inclui registros na tabela Original .........
9             strSQL = "INSERT INTO TAXA SELECT * FROM AUX_TAXA"
10            Conn.Execute strSQL
             'Label4.Caption = Db.RecordsAffected
11        End If
          
12        MyRec.Close
              
         'Deleta do Banco atual a tabela temporaria ...........................
13        Conn.Execute "DROP TABLE AUX_TAXA"
                  
14        ATU_TAXA = True

15    Exit Function

VerErro:

16        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
17            Conn.Execute "DROP TABLE AUX_TAXA"
18            Resume
19        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
20            Resume Next
21        ElseIf Err = 3021 Then
22            Resume Next
23        Else
24            ATU_TAXA = False
25            Screen.MousePointer = 1
26            Call FUNCAO_ERRO("AS")
              MsgBox "Sub ATU_TAXA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If

End Function

Private Sub COMECA_ATUAL()
    Dim BOERRO As Boolean
    Dim RESP As Integer
    Dim inicio As String, fim As String
    Dim dblAtual As Double
    Dim FL_VDR As String
    Dim SQL As String

   '72 tabelas de atualiza��o
   '11 tabelas de dele��o
   '02 tabelas desativadas
   '06 tabelas VDR
   '02 tabelas de dele��o VDR
   
    prgbar1.Max = 95 * numArq
    dblAtual = 0
    FL_VDR = "N"
    
   '-- Verifica se � VDR
    Set ss = New ADODB.Recordset
    ss.Open "SELECT FL_VDR FROM DEPOSITO", Conn, adOpenKeyset, adLockOptimistic
        
    If Not ss.EOF Then
        FL_VDR = ss!FL_VDR
    Else
        FL_VDR = "N"
    End If

    Do While contArq <= numArq
    
       'DATAS - 1
        frmFIL460.lblExecutando.Caption = "Tabela em Atualiza��o: Datas "
        ATU_DATAS
       
        'DEL_CLASS_FISCAL_RED_BASE - 1
        frmFIL460.lblExecutando.Caption = "Tabela em Atualiza��o: Deleta Class_Fiscal_Red_Base "
        '--DEL_CLASS_FISCAL_RED_BASE
        
        'DEL_R_FABRICA_DPK - 2
        frmFIL460.lblExecutando.Caption = "Tabela em Atualiza��o: Deleta R_Fabrica_DPK "
        '--DEL_R_FABRICA_DPK
       
       'DELETA_APLICACAO - 3
        Label1.Caption = "Tabela em Atualiza��o: Deleta Aplica��o "
        '--DEL_APLICACAO
        
       'DELETA_CLIENTE - 4
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Cliente "
        '--DEL_CLIENTE
        
       'DELETA_FORNECEDOR_ESPECIFICO - 5
        Label1.Caption = "Tabela em Atualiza��o: Dele��o Fornecedor_Especifico "
        '--DEL_FORNECEDOR_ESPECIFICO
        
       'DELETA_REPRESENTANTE - 6
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Representante "
        '--DEL_REPR
        
       'DELETA_R_CLIE_REPRES - 7
        Label1.Caption = "Tabela em Atualiza��o: Dele��o da R_CLIE_REPRES "
        '--DEL_R_CLIE_REPRES
        
       'DELETA_TABELA_DESCPER - 8
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Tabela_Descper "
        '--DEL_TABDESC
    
       'DELETA_PEDNOTA_VENDA - 9
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de PEDNOTA_VENDA "
        '--DEL_PEDNOTA
    
       'DELETA_R_UF_DEPOSITO - 10
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de R_UF_DEPOSITO "
        '--DEL_R_UF_DEPOSITO
       
       'DELETA_R_DPK_EQUIV - 11
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de R_DPK_EQUIV "
        '--DEL_R_DPK_EQUIV
    
       'CLASS_FISCAL_RED_BASE - 11
        Label1.Caption = "Tabela em Atualiza��o: DEL_CLASS_FISCAL_RED_BASE"
        '--CLASS_FISCAL_RED_BASE
    
       'ALIQUOTA_ME - 12
        Label1.Caption = "Tabela em Atualiza��o: Al�quota "
        '--ATU_ALIQUOTA_ME
       
       'ANTECIPACAO_TRIBUTARIA - 13
        Label1.Caption = "Tabela em Atualiza��o: Antecipa��o Tribut�ria "
        '--ATU_ANTECIPACAO_TRIBUTARIA
       
       'APLICACAO - 14
        Label1.Caption = "Tabela em Atualiza��o: Aplica��o "
        '--ATU_APLICACAO
    
       'BANCO - 15
        Label1.Caption = "Tabela em Atualiza��o: Banco "
        '--ATU_BANCO
    
       'CANCEL_PEDNOTA - 16
        Label1.Caption = "Tabela em Atualiza��o: Cancel_Pednota "
        ATU_CANCEL_PEDNOTA
     
       'CATEG_SINAL - 17
        Label1.Caption = "Tabela em Atualiza��o: Categ_Sinal "
        '--ATU_CATEGSINAL
        
       'CESTA_ITEM - 18
        Label1.Caption = "Tabela em Atualiza��o: CESTA_ITEM "
        '--ATU_CESTAITEM
            
       'CESTA_VENDA - 19
        Label1.Caption = "Tabela em Atualiza��o: CESTA_VENDA "
        '--ATU_CESTAVENDA
            
       'CIDADE - 20
        Label1.Caption = "Tabela em Atualiza��o: Cidade "
        '--ATU_CIDADE
        
       'CLASS_ANTEC_ENTRADA - 21
        Label1.Caption = "Tabela em Atualiza��o: Class_Antec_Entrada"
        '--ATU_CLASS_ANTEC_ENTRADA
        
       'CLIE_CREDITO - 22
        Label1.Caption = "Tabela em Atualiza��o: Clie_Credito"
        '--ATU_CLIECRED
        
       'CLIE_ENDERECO - 23
        Label1.Caption = "Tabela em Atualiza��o: Clie_Endereco "
        '--ATU_CLIEEND
        
       'CLIE_INTERNET - 24
        Label1.Caption = "Tabela em Atualiza��o: Clie_Internet "
        '--ATU_CLIEINT
        
       'CLIE_MENSAGEM - 25
        Label1.Caption = "Tabela em Atualiza��o: Clie_Mensagem "
        '--ATU_CLIEMSG
        
       'CLIENTE - 26
        Label1.Caption = "Tabela em Atualiza��o: Cliente "
        '--ATU_CLIENTE
        
       'CLIENTE_ACUMULADO - 27
        Label1.Caption = "Tabela em Atualiza��o: Cliente Acumulado "
        ATU_CLIENTE_ACUMULADO
        
       'CLIENTE_CARACTERISTICA - 28
        Label1.Caption = "Tabela em Atualiza��o: CLIENTE_CARACTERISTICA "
        '--ATU_CLIENTE_CARACTERISTICA
        
       'DEPOSITO_VISAO - 29
        Label1.Caption = "Tabela em Atualiza��o: Deposito_Vis�o "
        ATU_DEPOSITO_VISAO
        
       'DOLAR_DIARIO - 30
        Label1.Caption = "Tabela em Atualiza��o: Dolar_Diario "
        '--ATU_DOLARDIARIO
    
       'DUPLICATA - 31
        Label1.Caption = "Tabela em Atualiza��o: Duplicatas "
        '--ATU_DUPLICATA
        
       'EMBALADOR - 32
        Label1.Caption = "Tabela em Atualiza��o: Embalador "
        '--ATU_EMBALADOR
        
       'FILIAL - 34
        Label1.Caption = "Tabela em Atualiza��o: Filial "
        '--ATU_FILIAL
        
       'FILIAL_FRANQUIA - 35
        Label1.Caption = "Tabela em Atualiza��o: Filial_Franquia "
        '--ATU_FILIAL_FRANQUIA
        
       'FORNECEDOR - 36
        Label1.Caption = "Tabela em Atualiza��o: Fornecedor "
        mdiGER040.Label1.Refresh
        '--ATU_FORNECEDOR
    
       'FORNECEDOR_ESPECIFICO - 37
        Label1.Caption = "Tabela em Atualiza��o: Fornecedor_Especifico "
        '--ATU_FORNECEDOR_ESPECIFICO
            
       'FRETE_ENTREGA - 38
        Label1.Caption = "Tabela em Atualiza��o: Frete_Entrega "
        '--ATU_FRETE_ENTREGA
        
       'FRETE_UF - 39
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF "
        '--ATU_FRETE_UF
            
       'FRETE_UF_BLOQ - 40
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF_BLOQ "
        '--ATU_FRETE_UF_BLOQ
        
       'FRETE_UF_TRANSP - 41
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF_TRANSP "
        '--ATU_FRETE_UF_TRANSP
       
       'GRUPO - 42
        Label1.Caption = "Tabela em Atualiza��o: Grupo "
        '--ATU_GRUPO
        
       'ITPEDNOTA_VENDA - 43
        Label1.Caption = "Tabela em Atualiza��o: ITPEDNOTA_VENDA "
        '--ATU_ITPEDNOTA_VENDA
          
       'ITEM_ANALITICO - 44
        Label1.Caption = "Tabela em Atualiza��o: Item_Analitico "
        '--ATU_ITEMANAL
        
       'ITEM_CADASTRO - 45
        Label1.Caption = "Tabela em Atualiza��o: Item_Cadastro "
        '--ATU_ITEMCAD
        
       'ITEM_CUSTO - 46
        Label1.Caption = "Tabela em Atualiza��o: Item_Custo "
        '--ATU_ITEMCUSTO
        
       'ITEM_ESTOQUE - 47
        Label1.Caption = "Tabela em Atualiza��o: Item_Estoque "
        '--ATU_ITEMESTOQUE
        
       'ITEM_GLOBAL - 48
        Label1.Caption = "Tabela em Atualiza��o: Item_Global "
        '--ATU_ITEMGLOBAL
        
       'ITEM_PRECO - 49
        Label1.Caption = "Tabela em Atualiza��o: Item_Pre�o "
        '--ATU_ITEMPRECO
    
       'LOJA - 50
        Label1.Caption = "Tabela em Atualiza��o: Loja "
        ATU_LOJA
        
       'MONTADORA - 51
        Label1.Caption = "Tabela em Atualiza��o: Item_Montadora "
        '--ATU_MONTADORA
        
       'NATUREZA_OPERACAO - 52
        Label1.Caption = "Tabela em Atualiza��o: Natureza_Opera��o "
        '--ATU_NAT_OPERACAO
        
       'PEDNOTA_VENDA - 53
        Label1.Caption = "Tabela em Atualiza��o: PEDNOTA_VENDA "
        '--ATU_PEDNOTA_VENDA
        
       'PLANO_PGTO - 54
        Label1.Caption = "Tabela em Atualiza��o: Plano_Pgto "
        '--ATU_PLANO
        
       'R_CLIE_REPRES - 55
        Label1.Caption = "Tabela em Atualiza��o: R_CLIE_REPRES "
        '--ATU_R_CLIE_REPRES
       
       'R_DPK_EQUIV - 56
        Label1.Caption = "Tabela em Atualiza��o: R_DPK_EQUIV "
        '--ATU_R_DPK_EQUIV
            
       'R_FILDEP - 57
        Label1.Caption = "Tabela em Atualiza��o: R_FilDep "
        '--ATU_R_FILDEP
       
       'R_PEDIDO_CONF - 58
        Label1.Caption = "Tabela em Atualiza��o: R_pedido_conf "
        '--ATU_R_PEDIDO_CONF
    
       'R_REPCGC - 59
        Label1.Caption = "Tabela em Atualiza��o: RepCgc "
        '--ATU_REPCGC
        
       'R_REPVEN - 60
        Label1.Caption = "Tabela em Atualiza��o: RepVen "
        '--ATU_REPVEN
        
       'R_UF_DEPOSITO - 61
        Label1.Caption = "Tabela em Atualiza��o: R_UF_DEPOSITO "
        '--ATU_R_UF_DEPOSITO
        
       'REPR_END_CORRESP - 62
        Label1.Caption = "Tabela em Atualiza��o: Repr_End_Corresp "
        '--ATU_REPEND
    
       'REPRESENTACAO - 63
        Label1.Caption = "Tabela em Atualiza��o: Representa��o "
        '--ATU_REPRESENTACAO
        
       'REPRESENTANTE - 64
        Label1.Caption = "Tabela em Atualiza��o: Representante "
        '--ATU_REPRESENTANTE
    
       'RESULTADO - 65
        Label1.Caption = "Tabela em Atualiza��o: Resultado "
        '--ATU_RESULTADO
    
       'ROMANEIO - 66
        Label1.Caption = "Tabela em Atualiza��o: Romaneio "
        '--ATU_ROMANEIO
    
       'SALDO_PEDIDOS - 67
        Label1.Caption = "Tabela em Atualiza��o: Saldo_Pedidos "
        ATU_SALDO_PEDIDO
       
       'SUBGRUPO - 68
        Label1.Caption = "Tabela em Atualiza��o: Subgrupo "
        '--ATU_SUBGRUPO
        
       'SUBST_TRIBUTARIA - 69
        Label1.Caption = "Tabela em Atualiza��o: Substitui��o Tributaria "
        '--ATU_SUBTRIB
        
       'TABELA_DESCPER - 70
        Label1.Caption = "Tabela em Atualiza��o: Tabela_Descper "
        '--ATU_TABDESCPER
        
       'TABELA_VENDA - 71
        Label1.Caption = "Tabela em Atualiza��o: Tabela_Venda "
        '--ATU_TABVENDA
    
        
       'TP_CLIENTE - 73
        Label1.Caption = "Tabela em Atualiza��o: Tipo Cliente "
        '--ATU_TPCLIE
        
       'TIPO_CLIENTE_BLAU - 74
        Label1.Caption = "Tabela em Atualiza��o: Tipo_Cliente_Blau "
        '--ATU_TIPO_CLIENTE_BLAU
        
       'TRANSPORTADORA - 75
        Label1.Caption = "Tabela em Atualiza��o: Transportadora "
        '--ATU_TRANSPORTADORA
        
       'UF - 76
        Label1.Caption = "Tabela em Atualiza��o: Uf "
        '--ATU_UF
        
       'UF_CATEG - 77
        Label1.Caption = "Tabela em Atualiza��o: Uf_CATEG "
        '--ATU_UF_CATEG
            
       'UF_DEPOSITO - 78
        Label1.Caption = "Tabela em Atualiza��o: Uf_Deposito "
        '--ATU_UF_DEPOSITO
        
       'UF_DPK - 79
        Label1.Caption = "Tabela em Atualiza��o: Uf_DPK "
        '--ATU_UFDPK
       
       'UF_ORIGEM_DESTINO - 80
        Label1.Caption = "Tabela em Atualiza��o: Uf_Origem_Destino "
        '--ATU_UF_ORIGDEST
    
       'UF_TPCLIENTE - 81
        Label1.Caption = "Tabela em Atualiza��o: UF_TPCLIENTE "
        '--ATU_UF_TPCLIENTE
            
       'V_CLIENTE_FIEL - 82
        Label1.Caption = "Tabela em Atualiza��o: V_Cliente_Fiel "
        '--ATU_V_CLIENTE_FIEL
    
       'V_PEDLIQ_VENDA - 83
        Label1.Caption = "Tabela em Atualiza��o: V_Pedliq_venda "
        '--ATU_V_PEDLIQ_VENDA
    
       'VENDA_LIMITADA - 84
        Label1.Caption = "Tabela em Atualiza��o: VENDA_LIMITADA "
        '--ATU_VENDA_LIMITADA
    
        If FL_VDR = "S" Then
           'VDR_CONTROLE_VDR - 85
            Label1.Caption = "Tabela em Atualiza��o: VDR_CONTROLE_VDR "
            '--ATU_VDR_CONTROLE_VDR
        
           'VDR_ITEM_PRECO - 86
            Label1.Caption = "Tabela em Atualiza��o: VDR_ITEM_PRECO "
            '--ATU_VDR_ITEM_PRECO
        
           'DELETA_VDR_R_CLIE_LINHA_PRODUTO - 87
            Label1.Caption = "Dele��o da tabela: VDR_R_CLIE_LINHA_PRODUTO "
            '--DEL_VDR_R_CLIE_LINHA_PRODUTO
           
           'VDR_R_CLIE_LINHA_PRODUTO - 88
            Label1.Caption = "Tabela em Atualiza��o: VDR_R_CLIE_LINHA_PRODUTO "
            '--ATU_VDR_R_CLIE_LINHA_PRODUTO
        
           'DELETA_VDR_CLIENTE_CATEG_VDR - 89
            Label1.Caption = "Dele��o da tabela: VDR_CLIENTE_CATEG_VDR "
            '--DEL_VDR_CLIENTE_CATEG_VDR
           
           'VDR_CLIENTE_CATEG_VDR - 90
            Label1.Caption = "Tabela em Atualiza��o: VDR_CLIENTE_CATEG_VDR "
            '--ATU_VDR_CLIENTE_CATEG_VDR
        
           'VDR_DESCONTO_CATEG - 91
            Label1.Caption = "Tabela em Atualiza��o: VDR_DESCONTO_CATEG "
            '--ATU_VDR_DESCONTO_CATEG
        
           'VDR_TABELA_VENDA - 92
            Label1.Caption = "Tabela em Atualiza��o: VDR_TABELA_VENDA "
            '--ATU_VDR_TABELA_VENDA
        
        End If
        
        '--------------
        'Eduardo Relvas
        '18/01/2005
        '--------------
        'R_FABRICA_DPK - 92
        Label1.Caption = "Tabela em Atualiza��o: R_FABRICA_DPK"
        '--R_FABRICA_DPK
        
        '--------------
        'Eduardo Relvas
        '26/07/05
        '--------------
        'CLASS_FISCAL_RED_BASE - 93
        Label1.Caption = "Tabela em Atualiza��o: CLASS_FISCAL_RED_BASE"
        '--CLASS_FISCAL_RED_BASE
        
        Label1.Visible = False
        prgbar1.Visible = False
        lblPercent.Visible = False
        Label2.Visible = True
        Label2.Caption = "AGUARDE - Otimizando Base de Dados !!!!!"
        DoEvents
        
        contArq = contArq + 1
       
        If contArq <= numArq Then
            dbExt.Close
            mdiGER040.File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
            mdiGER040.File1.Pattern = "*.mdb"
            nomeArq = mdiGER040.File1.List(contArq - 1)
            On Error GoTo TRATA_ERRO_AUXILIAR
            Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
        End If
       
    Loop
    
    If Trim(Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB")) <> "" Then
        dbExt.Close
        Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"
    End If
    
    Screen.MousePointer = 1
       
    fim = Time
    
    strSQL = "INSERT INTO LOGDIV " & _
            " ( DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL ) VALUES " & _
            " ( '" & Now & "' , 1, 'Retorno da Atualiza��o', '" & _
            "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim & "', '" & _
            "Dep. Opera��o (CPD) ' ) "
    dbControle.Execute strSQL, dbFailOnError
    
    Screen.MousePointer = 1

Exit Sub

Trata_Erro:

    If BOERRO Then frmFIL460.lblExecutando.Caption = "Problemas na Atualiza��o !!! "
    
    RESP = MsgBox("Continua a Atualiza��o ?", vbYesNo)

    If RESP = vbYes Then Resume Next

Exit Sub


TRATA_ERRO_AUXILIAR:
    
    If Err = 3049 Then

        DoEvents
    
       'Reparar Banco de Dados
        RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
        
        COMPACTA_AUXILIAR
        
        MousePointer = 0
       'Ap�s corrigir o banco danificado processa novamente a linha do programa
       'que gerou o erro.
        Resume 0
    Else

        MsgBox "Sub Comeca_Atual" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub GERA_E_MAIL()
         'GERA E-MAL PARA CONTROLE DE ATUALIZA��O EM CAMPINAS E GRAVA
         'SEQUENCIA DE ATUALIZA��O NA TABELA FILIAL_FRANQUIA
        
          Dim dir_saida As String
          Dim SQL As String
1         On Error GoTo TrataBanco
2         If controle_atual > 0 Then
             'Verifica se arquivo zipado j� foi gerado
3             dir_saida = Path_drv & "\COM\SAIDA\MSG\ATU" & filial_atual & controle_atual & ".MSG"
4             Open dir_saida For Output As #1
             'Grava linha da data
5             Print #1, "Date: " & Format(Now, "mmm, dd yyyy")
             'Grava linha do endereco
6             Print #1, "To: " & "dpkpedid@dpk.com.br"
             'Grava linha do Subject
7             Print #1, "Subject: #ATU" & filial_atual & controle_atual
             'Fecha arquivo texto
8             Print #1, filial_atual & controle_atual
9             Close #1
        
10            dir_saida = Path_drv & "\COM\SAIDA\PEDIDOS\ATU" & filial_atual & controle_atual & ".ATU"
11            Open dir_saida For Output As #1
             'Grava linha da data
12            Print #1, filial_atual & controle_atual
13            Close #1

             'GRAVA ATUALIZA��O
14                  SQL = "UPDATE FILIAL_FRANQUIA SET SEQ_BATCH= " & icontrole_atual & " "
15            SQL = SQL & " WHERE COD_FILIAL= " & ifilial_atual
16            dbLocal.Execute SQL
17        End If

18    Exit Sub

TrataBanco:
19        If Err = 3024 Then
20            MsgBox "ARQUIVO" & Path_drv & "\COM\DBMAIL.MDB N�O ENCONTRADO!!!", 48, "    ARQUIVO N�O ENCONTRADO"
21        ElseIf Err <> 0 Then
22            MsgBox "Sub Gera_E_Mail" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If
          
End Sub

Private Sub OTIMIZAR_BASES()
          
                  
          Dim SQL As String
          Dim SQL2 As String
          Dim sql3 As String
          Dim ss As Dynaset
          Dim ss3 As Dynaset
          Dim i As Long
          Dim TOT_ITEM As Integer
          Dim dbaccess
          Dim dbaccess1
          Dim dbaccess2
          Dim dbaccess3
          Dim DT_HOJE
          Dim loja
          Dim cotacao
            
1         Set dbaccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")

2         On Error GoTo Trata_Erro
3         Screen.MousePointer = 11

         '************************************ DIGITA.MDB ************************************

         '*****************************************************
         'DELETA DE DIGITA.MDB PEDIDOS QUE J� FORAM FINALIZADOS
         '*****************************************************
4         SSPanel2.Caption = "Otimizando VENDAS!"

5               SQL = "SELECT NUM_PENDENTE"
6         SQL = SQL & " FROM PEDIDO"
7         SQL = SQL & " WHERE FL_PEDIDO_FINALIZADO <> 'N'  "
8         Set ss = dbaccess.CreateDynaset(SQL)
        
9         If ss.EOF And ss.BOF Then
10            Screen.MousePointer = 0
11        Else
12            ss.MoveLast
13            TOT_ITEM = ss.RecordCount
14            ss.MoveFirst
15            For i = 1 To TOT_ITEM
16                SQL2 = "delete from pedido where"
17                SQL2 = SQL2 & " num_pendente = " & ss("num_pendente")
18                sql3 = "delete from item_pedido where"
19                sql3 = sql3 & " num_pendente = " & ss("num_pendente")
20                dbaccess.Execute SQL2
21                dbaccess.Execute sql3
22                ss.MoveNext
23            Next
24        End If
             
25        dbaccess.Close
26        Screen.MousePointer = 11
         
         '**************************
         'OTIMIZA /REPARA DIGITA.MDB
         '**************************
27        SSPanel2.Caption = "Otimizando DIGITA!"

28        If Dir(Path_drv & "\DADOS\DIGITA.BAK") <> "" Then
29            Kill Path_drv & "\DADOS\DIGITA.BAK"
30        End If
         
31        FileCopy Path_drv & "\DADOS\DIGITA.MDB", Path_drv & "\DADOS\DIGITA.BAK"
32        DBEngine.CompactDatabase Path_drv & "\DADOS\DIGITA.MDB", Path_drv & "\DADOS\DIGNEW.MDB", "", dbEncrypt
33        Kill Path_drv & "\DADOS\DIGITA.MDB"
34        Name Path_drv & "\DADOS\DIGNEW.MDB" As Path_drv & "\DADOS\DIGITA.MDB"
35        DBEngine.RepairDatabase Path_drv & "\DADOS\DIGITA.MDB"
         
         '************************************ VENDAS.MDB ************************************
         
         '****************************
         'VERIFICA SEQUENCE DA COTA��O
         '****************************
36        SSPanel2.Caption = "Otimizando VENDAS!"
          
37        Set dbaccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")
          
38              SQL = "Select proximo_pedido"
39        SQL = SQL & " From sequencia"
          
40        Set ss = dbaccess.CreateSnapshot(SQL)
41        FreeLocks
         
42        Set dbaccess3 = OpenDatabase(Path_drv & "\COM\" & "DBMAIL.MDB")
          
43              SQL = "Select TIPO_USUARIO"
44        SQL = SQL & " From USUARIO"
          
45        Set ss3 = dbaccess3.CreateSnapshot(SQL)
46        FreeLocks
          
47        If ss3!TIPO_USUARIO = "R" Then
             
48            If ss!proximo_pedido >= 995 Then
49                SQL = "update sequencia set proximo_pedido=0"
50                dbaccess.Execute SQL
51                FreeLocks
52            End If
              
53        Else
          
54            If ss!proximo_pedido >= 9995 Then
55                SQL = "update sequencia set proximo_pedido=0"
56                dbaccess.Execute SQL
57                FreeLocks
58            End If
          
59        End If
          
60        dbaccess.Close
         
         '***************************
         'OTIMIZA / REPARA VENDAS.MDB
         '***************************

61        If Dir(Path_drv & "\DADOS\VENDAS.BAK") <> "" Then
62            Kill Path_drv & "\DADOS\VENDAS.BAK"
63        End If
          
64        FileCopy Path_drv & "\DADOS\VENDAS.MDB", Path_drv & "\DADOS\VENDAS.BAK"
65        DBEngine.CompactDatabase Path_drv & "\DADOS\VENDAS.MDB", Path_drv & "\DADOS\VDANEW.MDB", "", dbEncrypt
66        Kill Path_drv & "\DADOS\VENDAS.MDB"
67        Name Path_drv & "\DADOS\VDANEW.MDB" As Path_drv & "\DADOS\VENDAS.MDB"
68        DBEngine.RepairDatabase Path_drv & "\DADOS\VENDAS.MDB"
          
69        Screen.MousePointer = 11
          
         '************************************ BASE_DPK.MDB ************************************
          
         '********************************************************************
         'FAZ DELE��O NA TABELA PEDNOTA_VENDA DO BASE_DPK.MDB
         'DELETA OS PEDIDOS PARA GOIANIA COM RETORNO DO DIA DE ENVIO DO PEDIDO
         '********************************************************************
70        SSPanel2.Caption = "Otimizando BASE_DPK!"
             
71        Set dbaccess2 = OpenDatabase(Path_drv & "\DADOS\" & "BASE_DPK.MDB", False, False)
                
72              SQL = "DELETE  "
73        SQL = SQL & " FROM PEDNOTA_VENDA"
74        SQL = SQL & " WHERE cod_cliente IS NULL  "
             
75        dbaccess2.Execute SQL
            
76        SSPanel2.Caption = "Deletando ITENS COTA��ES Antigas!"
          
77              SQL = "SELECT DT_FATURAMENTO "
78        SQL = SQL & " FROM DATAS"
          
79        Set ss = dbaccess2.CreateDynaset(SQL)
          
80        DT_HOJE = ss!DT_FATURAMENTO
                
81              SQL = "SELECT COD_LOJA, NUM_COTACAO"
82        SQL = SQL & " FROM COTACAO "
83        SQL = SQL & " WHERE DT_VALIDADE < cdate('" & CDate(DT_HOJE) & "')"
         
84        Set ss = dbaccess2.CreateDynaset(SQL)
         
85        Do While Not ss.EOF
86            loja = ss!COD_LOJA
87            cotacao = ss!NUM_COTACAO
              
88                  SQL = " DELETE "
89            SQL = SQL & " FROM ITEM_COTACAO "
90            SQL = SQL & " WHERE COD_LOJA = " & loja & " AND "
91            SQL = SQL & " NUM_COTACAO = " & cotacao & " "
              
92            dbaccess2.Execute SQL
93            ss.MoveNext
94        Loop
             
95        SSPanel2.Caption = "Deletando COTA��ES Antigas!"
            
            
96              SQL = "DELETE "
97        SQL = SQL & " FROM COTACAO "
98        SQL = SQL & " WHERE dt_validade < cdate('" & CDate(DT_HOJE) & "')"
          
99        dbaccess2.Execute SQL
            
100       dbaccess2.Close
            
         '****************************
         'OTIMIZA /REPARA BASE_DPK.MDB
         '****************************
         'If Dir(Path_drv & "\DADOS\BASE_DPK.BAK") <> "" Then
             'Kill Path_drv & "\DADOS\DIGITA.BAK"
         'End If
              
         'FileCopy Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASE_DPK.BAK"
         'DBEngine.CompactDatabase Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASENEW.MDB", "", dbEncrypt
         'Kill Path_drv & "\DADOS\BASE_DPK.MDB"
         'Name Path_drv & "\DADOS\BASENEW.MDB" As Path_drv & "\DADOS\BASE_DPK.MDB"
         'DBEngine.RepairDatabase Path_drv & "\DADOS\BASE_DPK.MDB"
            
         
         '************************************ VDR.MDB ************************************

101       Set dbaccess2 = OpenDatabase(Path_drv & "\DADOS\" & "VDR.MDB")

         '*****************************************************
         'DELETA DA VDR.MDB PEDIDOS QUE J� FORAM FINALIZADOS
         '*****************************************************
102       SSPanel2.Caption = "Otimizando VDR!"

103             SQL = "SELECT NUM_PENDENTE"
104       SQL = SQL & " FROM PEDIDO"
105       SQL = SQL & " WHERE FL_PEDIDO_FINALIZADO <> 'N'  "
106       Set ss = dbaccess2.CreateDynaset(SQL)
        
107       If ss.EOF And ss.BOF Then
108           Screen.MousePointer = 0
109       Else
110           ss.MoveLast
111           TOT_ITEM = ss.RecordCount
112           ss.MoveFirst
113           For i = 1 To TOT_ITEM
114               SQL2 = "delete from pedido where"
115               SQL2 = SQL2 & " num_pendente = " & ss!num_pendente
116               sql3 = "delete from item_pedido where"
117               sql3 = sql3 & " num_pendente = " & ss!num_pendente
118               dbaccess2.Execute SQL2
119               dbaccess2.Execute sql3
120               ss.MoveNext
121           Next
122       End If
123       dbaccess2.Close
          
         '**************************
         'OTIMIZA/REPARA VDR.MDB
         '**************************
124       SSPanel2.Caption = "Otimizando VDR!"

125       If Dir(Path_drv & "\DADOS\VDR.BAK") <> "" Then
126           Kill Path_drv & "\DADOS\VDR.BAK"
127       End If
         
128       FileCopy Path_drv & "\DADOS\VDR.MDB", Path_drv & "\DADOS\VDR.BAK"
129       DBEngine.CompactDatabase Path_drv & "\DADOS\VDR.MDB", Path_drv & "\DADOS\VDRNEW.MDB", "", dbEncrypt
130       Kill Path_drv & "\DADOS\VDR.MDB"
131       Name Path_drv & "\DADOS\VDRNEW.MDB" As Path_drv & "\DADOS\VDR.MDB"
132       DBEngine.RepairDatabase Path_drv & "\DADOS\VDR.MDB"

133       Screen.MousePointer = 0


134   Exit Sub
          
Trata_Erro:
135       If Err = 70 Then
136           MsgBox "O Banco de Vendas/Digita��o/VDR est� sendo utilizado, verifique se h� algum micro ligado no Telemarketing.", vbExclamation, "Aten��o"
137           Screen.MousePointer = 0
138           Unload Me
139           Exit Sub
          ElseIf Err.Number <> 0 Then
              MsgBox "Sub Otimizar_Bases" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
140       End If

End Sub

Private Sub MDIForm_Load()

          Dim strSQL As String
          Dim strOrigem As String
          Dim strDestino As String
          
1         If App.PrevInstance Then
2            End
3         End If
          
          '-------------------------------
          'Data       Versao      Analista
          '-------------------------------
          '11/02/05   2.0.0       Eduardo
          '26/07/05   2.0.1       Eduardo
          '
          '-------------------------------
              
4         Me.Caption = Me.Caption & " Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
         
          'Eduardo Relvas - 23/08/05
          'Criar_Campo_VL_ICMRETIDO
         
         
         '***************************
         'Define drive de execu��o
         '***************************
5        Path_drv = Left(App.Path, 2)
         'Path_drv = "C:"
          
6         File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
7         File1.Pattern = "*.mdb"
          
8         numArq = File1.ListCount
          
9         contArq = 0
          
10        mHoje = Date
          
11        If numArq > 0 Then
          
12            scmdAtualiza.Enabled = True
          
13            strOrigem = Path_drv & "\DADOS\BASE_DPK.MDB"
14            strDestino = Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
             'FileCopy strOrigem, strDestino
              
15            On Error GoTo TRATA_ERRO_CTL
16            Set dbControle = OpenDatabase(Path_drv & "\LOG\CONTROLE.MDB", False, False)
          
17            On Error GoTo TRATA_ERRO_LOCAL
18            Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
19            contArq = contArq + 1
20            nomeArq = File1.List(contArq - 1)
              
21            On Error GoTo TRATA_ERRO_AUXILIAR
              
22            Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
              
23            FL_TIPO = "D"
24            If Mid(nomeArq, 1, 1) <> "B" Then
25                FL_TIPO = "C"
26            End If
              
27            strSQL = "SELECT DT_FATURAMENTO FROM DATAS where dt_faturamento  "
          
28            Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
          
29            If Not recTab.EOF Then
30                mHoje = Mid(recTab!DT_FATURAMENTO, 1, 8)
31            Else
32                mHoje = Date
33            End If
              
34        End If
          
35        If numArq > 0 Then
36            mdiGER040.Show
37            SSPanel2.Caption = "Aguarde Atualizando Base Local ..."
38            SSPanel5.Visible = True
39            lblPercent.Visible = True
40            prgbar1.Visible = True
              
41            Call COMECA_ATUAL
              
42            Screen.MousePointer = 11
43            SSPanel2.Caption = "Atualiza��o Finalizada!"
44            Screen.MousePointer = 0
45            Label1.Caption = ""
46            dbLocal.Close
47            dbControle.Close
             
             'OTIMIZA��O DE BASE DE DADOS
48            Screen.MousePointer = 1
49            SSPanel2.Caption = "Inicio OTIMIZA��O!"
50            SSPanel5.Visible = False
51            lblPercent.Visible = False
52            prgbar1.Visible = False
53            Label2.Visible = True
54            Label2.Caption = "AGUARDE - Otimizando Base de Dados !!!!!"
              
55            Call OTIMIZAR_BASES
              
56            SSPanel2.Caption = "T�rmino OTIMIZA��O!"
57        End If
          
         '****** ATUALIZA POSI��O DE PEDIDOS
         '****** VOLTA FLAG DE PEDIDOS QUANDO O MESMO D� ERRO DE FINALIZA��O
58        While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> "" Or _
                Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
59              While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> ""
60                    mdiGER040.Show
61                    Call ATU_PEDIDOS
62              Wend
63              While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
64                    mdiGER040.Show
65                    Call ATU_FLAG
66              Wend
67        Wend
68        End

69    Exit Sub

TRATA_ERRO_CTL:

70        If Err = 3049 Then
          
71            MousePointer = 11
72            DoEvents
          
             'Reparar Banco de Dados
73            RepairDatabase (Path_drv & "\LOG\CONTROLE.MDB")
74            COMPACTA_CONTROLE
75            MousePointer = 0
          
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
76            Resume 0
77        Else
78            Call FUNCAO_ERRO("")
79            MsgBox "Sub Mdiform_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
80            Resume Next
81        End If

82        Exit Sub

TRATA_ERRO_LOCAL:

83        If Err = 3049 Then
          
84            MousePointer = 11
85            DoEvents
          
             'Reparar Banco de Dados
86            RepairDatabase (Path_drv & "\DADOS\BASE_DPK.MDB")
87            COMPACTA_LOCAL
88            MousePointer = 0
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
89            Resume 0
90        Else
91            Call FUNCAO_ERRO("")
92            MsgBox "Sub Mdiform_load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
93        End If
          
TRATA_ERRO_AUXILIAR:
          
94        If Err = 3049 Then
95            MousePointer = 11
96            DoEvents
          
             'Reparar Banco de Dados
97            RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
98            COMPACTA_AUXILIAR
99            MousePointer = 0
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
100           Resume 0
101       Else
102           Call FUNCAO_ERRO("")
818           MsgBox "Sub Mdiform_load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
104       End If

End Sub
Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = False
End Sub


Private Sub mnuAtu_Click()
    scmdAtualiza_Click
End Sub


Private Sub mnuBackup_Click()
    scmdBackup_Click
End Sub

Private Sub mnuCompacta_Click()
   'scmdCompacta_Click
End Sub

Private Sub mnuComunica_Click()
    scmdComunica_Click
End Sub

Private Sub mnuDiv_Click()
    scmdDiv_Click
End Sub


Private Sub mnuSair_Click()
    scmdSair_Click
End Sub


Private Sub scmdAtualiza_Click()
    lblPercent.Visible = False
    SSPanel5.Visible = False
    frmGER0401.Show vbModal
End Sub

Private Sub scmdAtualiza_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Atualizar"
    lblHelp.Left = 0
    lblHelp.Visible = True
End Sub

Private Sub scmdBackup_Click()
          
    On Error GoTo Trata_Erro
          
          Dim strOrigem As String
          Dim strDestino As String
          
1         lblPercent.Visible = False
2         SSPanel5.Visible = False
          
3         strOrigem = InputBox("Indique o Caminho e o Nome do Arquivo de Origem", "Backup")
          
4         If Trim(strOrigem) <> "" Then
5             If Dir(strOrigem) = "" Then
6                 MsgBox "Caminho ou Nome do Arquivo de Origem n�o existe"
7                 Exit Sub
8             End If
9             strDestino = InputBox("Indique o Caminho e o Nome do Arquivo Destino", "Backup")
10            If Trim(strDestino) <> "" Then
11                If Dir(strDestino) <> "" Then
12                    Kill strDestino
13                End If
                  
14                Name strOrigem As strDestino
15            End If
16        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub scmdBackup_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub scmdBackup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Back-up"
    lblHelp.Left = 3465
    lblHelp.Visible = True
End Sub


Private Sub scmdCompacta_Click()

    On Error GoTo Trata_Erro

          Dim RESP As Integer

1         lblPercent.Visible = False
2         SSPanel5.Visible = False

3         RESP = MsgBox("Deseja realmente Compactar seu Banco de Dados ? ", vbYesNo, "Compacta��o do Banco de Dados")
          
4         If RESP = vbNo Then
5             Exit Sub
6         End If

7         Screen.MousePointer = 11

         'Fecha Banco de Dados
8         dbLocal.Close
          
         'Compacta��o de Banco de Dados
9         COMPACTA_LOCAL
          
         'Abre Banco de Dados Novamente
10        Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
         'Fecha Banco de Dados
11        dbControle.Close
          
         'Compacta��o de Banco de Dados de Controle
12        COMPACTA_CONTROLE
          
         'Abre Banco de Dados Novamente
13        Set dbControle = OpenDatabase(Path_drv & "\COM\CONTROLE.MDB", False, False)
         
14        Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub scmdCompacta" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

'Eduardo - 18/08/05
'Esta sub foi criada somente para inserir o campo VL_ICMRETIDO na tabela ITEM_PEDIDO pois nao existe uma forma de
'fazer esta altera��o pelo sistema de atualiza��o.
Sub Criar_Campo_VL_ICMRETIDO()
          
1         On Error GoTo Trata_Erro
          
          Dim Db As DAO.Database
          
2         Set Db = OpenDatabase("C:\DADOS\VENDAS.MDB")
          
3         Db.Execute "ALTER TABLE ITEM_PEDIDO ADD COLUMN VL_ICMRETIDO DOUBLE "

4         Db.Execute "UPDATE ITEM_PEDIDO SET VL_ICMRETIDO = 0 WHERE VL_ICMRETIDO IS NULL"

Trata_Erro:
5         If Err.Number = 3380 Then
6             Resume Next
7         ElseIf Err.Number <> 0 Then
8             MsgBox "Sub Criar_Campo_Vl_IcmRetido" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
9         End If

End Sub



