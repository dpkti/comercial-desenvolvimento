VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCalendario 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Agendar Contato"
   ClientHeight    =   6750
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10245
   Icon            =   "frmCalendario.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   450
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   683
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6420
      Width           =   10245
      _ExtentX        =   18071
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18018
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   1
      Top             =   750
      Width           =   10200
      _ExtentX        =   17992
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   60
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCalendario.frx":23D2
      PICN            =   "frmCalendario.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgAgenda 
      Height          =   2265
      Left            =   60
      TabIndex        =   6
      Top             =   4125
      Width           =   10155
      _ExtentX        =   17912
      _ExtentY        =   3995
      _Version        =   393216
      Cols            =   11
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame fra 
      Caption         =   "Data e hora do agendamento:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3015
      Left            =   60
      TabIndex        =   3
      Top             =   840
      Width           =   3675
      Begin VB.ComboBox cmbHora_Agenda 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "frmCalendario.frx":30C8
         Left            =   1500
         List            =   "frmCalendario.frx":30F3
         TabIndex        =   5
         Top             =   2610
         Width           =   1305
      End
      Begin MSComCtl2.MonthView MonthView1 
         Height          =   2370
         Left            =   90
         TabIndex        =   4
         Top             =   240
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   4180
         _Version        =   393216
         ForeColor       =   -2147483630
         BackColor       =   -2147483633
         Appearance      =   1
         StartOfWeek     =   57999361
         CurrentDate     =   38309
         MinDate         =   2
      End
      Begin Bot�o.cmd cmdGravar_Agenda 
         Height          =   690
         Left            =   2880
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Gravar Agendamento"
         Top             =   2250
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCalendario.frx":3152
         PICN            =   "frmCalendario.frx":316E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Hor�rio:"
         Height          =   195
         Left            =   930
         TabIndex        =   10
         Top             =   2730
         Width           =   555
      End
   End
   Begin Bot�o.cmd cmdExcluirCompromisso 
      Height          =   690
      Left            =   9480
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Excluir Agendamento"
      Top             =   3390
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCalendario.frx":3E48
      PICN            =   "frmCalendario.frx":3E64
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Contatos j� agendados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   75
      TabIndex        =   7
      Top             =   3900
      Width           =   1935
   End
End
Attribute VB_Name = "frmCalendario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public vData_Selecionada As Date
Public vQuemChamou As String

Private Sub cmdExcluirCompromisso_Click()
    
    mfgAgenda.col = 1
    If mfgAgenda.Text <> "" Then
        If MsgBox("Confirma exclus�o do registro selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
            vBanco.Parameters.Remove "PM_CODCLIENTE"
            vBanco.Parameters.Add "PM_CODCLIENTE", mfgAgenda.TextMatrix(mfgAgenda.row, 6), 1
            vBanco.Parameters.Remove "PM_CODUSUARIO"
            vBanco.Parameters.Add "PM_CODUSUARIO", vCod_Usuario, 1
            vBanco.Parameters.Remove "PM_NUMERO"
            vBanco.Parameters.Add "PM_NUMERO", mfgAgenda.TextMatrix(mfgAgenda.row, 4), 1
            vBanco.Parameters.Remove "PM_DATA"
            vBanco.Parameters.Add "PM_DATA", Format(CDate(mfgAgenda.TextMatrix(mfgAgenda.row, 1)), "DD/MM/YY"), 1
            vBanco.Parameters.Remove "PM_HORA"
            vBanco.Parameters.Add "PM_HORA", mfgAgenda.TextMatrix(mfgAgenda.row, 2), 1
            
            vSql = "Producao.PCK_VDA640.PR_EXCLUIR_AGENDAMENTO(:PM_CODCLIENTE, :PM_CODUSUARIO, :PM_NUMERO, TO_DATE(:PM_DATA,'DD/MM/RR'), :PM_HORA)"
        
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processo de exclus�o da PR_EXCLUIR_AGENDAMENTO. Entre em contato com o SUPORTE.")
                Exit Sub
            Else
                Call vVB_Generica_001.Informar("Agendamento exclu�do com sucesso!")
            End If
            Form_Load
        End If
    End If
End Sub

Private Sub cmdGravar_Agenda_Click()
       
    If vData_Selecionada = Null Or vData_Selecionada = "00:00:00" Then
        MsgBox "Informe o Dia.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If Year(vData_Selecionada) < Year(Now) Then
        MsgBox "Voc� n�o pode agendar um compromisso para o ano selecionado.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If cmbHora_Agenda.Text = "" Then Exit Sub
       
    If vNum_Atendimento = 0 Then Exit Sub
    
    If vCod_Usuario = 0 Then Exit Sub
   
    If frmVDA640.txtCodigo_Cliente.Text = "" Then Exit Sub
    
   '-- GRAVA OS DADOS DA TABELA DE AGENDA
    vBanco.Parameters.Remove "PM_CODCLIENTE"
    vBanco.Parameters.Add "PM_CODCLIENTE", frmVDA640.txtCodigo_Cliente.Text, 1
    vBanco.Parameters.Remove "PM_CODUSUARIO"
    vBanco.Parameters.Add "PM_CODUSUARIO", vCod_Usuario, 1
    vBanco.Parameters.Remove "PM_NUMERO"
    vBanco.Parameters.Add "PM_NUMERO", vNum_Atendimento, 1
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Add "PM_DATA", CStr(vData_Selecionada), 1
    vBanco.Parameters.Remove "PM_HORA"
    vBanco.Parameters.Add "PM_HORA", cmbHora_Agenda.Text, 1
    
    'If chkFinalizado.Value = 0 Then
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
    'Else
    '    vBanco.Parameters.Remove "PM_SITUACAO"
    '    vBanco.Parameters.Add "PM_SITUACAO", 9, 1
    'End If
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_ATUALIZA_AGENDA(:PM_CODCLIENTE, :PM_CODUSUARIO, " & _
           ":PM_NUMERO, TO_DATE(:PM_DATA, 'DD/MM/RR'), :PM_HORA, :PM_SITUACAO)"

    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_AGENDA. Entre em contato com o SUPORTE.")
        Exit Sub
    Else
        Call vVB_Generica_001.Informar("Agendamento conclu�do com sucesso!")
        cmbHora_Agenda.Text = ""
        'chkFinalizado.Value = 0
    End If

   '-- CARREGA OS DADOS DO GRID DE PEND�NCIA DE AGENDA
    vBanco.Parameters.Remove "PM_CODIGO"
    vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
    vBanco.Parameters.Remove "PM_TIPO"
    vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", frmVDA640.txtCodigo_Cliente.Text, 1
    vBanco.Parameters.Remove "PM_ROWNUM"
    vBanco.Parameters.Add "PM_ROWNUM", 0, 1

    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_CLIENTE(:vCursor, :PM_CLIENTE, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_CLIENTE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.CarregaGridTabela(mfgAgenda, vObjOracle, 10)
    End If

End Sub

Private Sub cmdVoltar_Click()

    mfgAgenda.Clear
    mfgAgenda.Rows = 2
    mfgAgenda.FormatString = "DT.AGENDA|DT.LIGA��O|N�M.ATEND.|ATENDENTE|C�D.CLIENTE|NOME CLIENTE|ASSUNTO|T�PICO|DETALHE|SITUA��O"
    
    Unload Me

End Sub
Private Sub Form_Load()
    
    fra.Enabled = Not (frmVDA640.txtCodigo_Cliente = "" Or frmVDA640.stb.Tab <> 1 Or vNum_Atendimento = 0)
    
    If vQuemChamou = "Timer" Then fra.Enabled = False
    
    MonthView1.MinDate = Date
    vData_Selecionada = 0

   '-- CARREGA OS DADOS DO GRID DE PEND�NCIA DE AGENDA POR CLIENTE
    vBanco.Parameters.Remove "PM_CODIGO"
    vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
    vBanco.Parameters.Remove "PM_TIPO"
    'vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
    'Conversei com a Natalia e ela disse que � para exibir somente os agendamentos de cada usuario
    'independente se ele � Coordenador ou Atendente.
    vBanco.Parameters.Add "PM_TIPO", "A", 1
    
    If vQuemChamou <> "Timer" Then
        vBanco.Parameters.Remove "PM_CLIENTE"
        vBanco.Parameters.Add "PM_CLIENTE", frmVDA640.txtCodigo_Cliente.Text, 1
    
        vBanco.Parameters.Remove "PM_ROWNUM"
        vBanco.Parameters.Add "PM_ROWNUM", 0, 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_CLIENTE(:vCursor, :PM_CLIENTE, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_CLIENTE, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
    
    Else
        
        vBanco.Parameters.Remove "PM_CLIENTE"
        vBanco.Parameters.Add "PM_CLIENTE", 0, 1
        vSql = "PRODUCAO.PCK_VDA640.PR_BUSCA_ATEND_AGENDA(:vCursor, :PM_CODIGO, " & " TO_DATE(:PM_DATA, 'DD/MM/RR'), :PM_HORA)"
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento PR_BUSCA_ATEND_AGENDA, entr em contato com o SUPORTE")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vcursor").Value
        End If
    
    End If
    
    If Not vObjOracle.EOF Then
        Call vVB_Generica_001.CarregaGridTabela(mfgAgenda, vObjOracle, 10)
    Else
        mfgAgenda.Clear
    End If
    
    Set vObjOracle = Nothing
    
End Sub

Private Sub mfgAgenda_DblClick()
    
    mfgAgenda.col = 4
    If mfgAgenda.Text = "" Then Exit Sub
    
    frmVDA640.vNovo = False
    
    vNum_Atendimento = mfgAgenda.Text
    fl_Novo = "N"
    
    mfgAgenda.col = 6
   '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO C�DIGO DO CLIENTE
    vBanco.Parameters.Remove "PM_CODCLIENTE"
    vBanco.Parameters.Add "PM_CODCLIENTE", mfgAgenda.Text, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,:PM_CODCLIENTE,NULL,NULL)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        frmVDA640.txtCodigo_Cliente.Text = mfgAgenda.Text
        frmVDA640.txtRazao_Social.Text = vObjOracle!NOME_CLIENTE
        frmVDA640.txtContato.Text = IIf(IsNull(vObjOracle!NOME_CONTATO), "", vObjOracle!NOME_CONTATO)
        frmVDA640.txtCGC.Text = vObjOracle!CGC
        frmVDA640.txtCidade.Text = vObjOracle!NOME_CIDADE
        frmVDA640.txtUF.Text = vObjOracle!COD_UF
        frmVDA640.txtCod_Representante.Text = vObjOracle!COD_REPRES
        frmVDA640.txtNome_Representante.Text = vObjOracle!NOME_REPRES
        frmVDA640.txtDDD1.Text = vObjOracle!DDD1
        frmVDA640.txtTelefone1.Text = vObjOracle!FONE1
        frmVDA640.txtDDD2.Text = IIf(IsNull(vObjOracle!DDD2), 0, vObjOracle!DDD2)
        frmVDA640.txtTelefone2.Text = IIf(IsNull(vObjOracle!FONE2), 0, vObjOracle!FONE2)
        frmVDA640.txtFilial.Text = vObjOracle!COD_FILIAL & " - " & vObjOracle!NOME_FILIAL
    End If
    
   '-- BUSCA O TIPO_FIEL DO CLIENTE
    vBanco.Parameters.Remove "PM_CODCLIENTE"
    vBanco.Parameters.Add "PM_CODCLIENTE", mfgAgenda.Text, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_TP_FIEL(:vCursor,:PM_CODCLIENTE)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TP_FIEL, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        frmVDA640.txtTipo_Fiel.Text = vObjOracle!DESC_FIEL
    End If

   '-- CARREGA OS DADOS DO ATENDIMENTO
    mfgAgenda.col = 4
    vBanco.Parameters.Remove "PM_NUMERO"
    vBanco.Parameters.Add "PM_NUMERO", mfgAgenda.Text, 1
    vNum_Atendimento = mfgAgenda.Text
        
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        If vObjOracle!Situacao = "P" Then
            frmVDA640.optPendente.Value = True
        ElseIf vObjOracle!Situacao = "R" Then
            frmVDA640.optRetornado.Value = True
            frmVDA640.optPendente.Enabled = False
            frmVDA640.chkComplemento.Enabled = False
            'frmVDA640.optRetornar.Enabled = False
            'frmVDA640.optDispensa_Retorno.Enabled = False
        ElseIf vObjOracle!Situacao = "S" Then
            frmVDA640.optSolucionado.Value = True
            'frmVDA640.optRetornar.Enabled = False
            frmVDA640.optPendente.Enabled = False
            frmVDA640.optRetornado.Enabled = False
            frmVDA640.chkComplemento.Enabled = False
        ElseIf vObjOracle!Situacao = "L" Then
            frmVDA640.optLiberado.Value = True
            'frmVDA640.optRetornar.Enabled = False
            frmVDA640.optPendente.Enabled = False
            frmVDA640.optRetornado.Enabled = False
            frmVDA640.optSolucionado.Enabled = False
            frmVDA640.chkComplemento.Enabled = False
        End If
        
        If vObjOracle!CONTATO = "C" Then
            frmVDA640.optCliente.Value = True
        ElseIf vObjOracle!CONTATO = "R" Then
            frmVDA640.optRepresentante.Value = True
        ElseIf vObjOracle!CONTATO = "T" Then
            frmVDA640.optTelevendas.Value = True
        ElseIf vObjOracle!CONTATO = "O" Then
            frmVDA640.optOutros.Value = True
        End If
        
        frmVDA640.txtComentarios.Text = IIf(IsNull(vObjOracle!COMENTARIOS), "", vObjOracle!COMENTARIOS)
        Call frmVDA640.Inserir_Nome_Atendente_Descricao
        
        frmVDA640.txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
        frmVDA640.txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
        frmVDA640.txtNota_Fiscal_LostFocus
        frmVDA640.txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
        frmVDA640.txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
        
        If vObjOracle!FL_PROCEDE = "S" Then
            frmVDA640.chkProcede.Value = 1
        Else
            frmVDA640.chkProcede.Value = 0
        End If
    
        If vObjOracle!FL_DEVOLUCAO = "S" Then
            frmVDA640.chkDevolucao.Value = 1
        Else
            frmVDA640.chkDevolucao.Value = 0
        End If
    
        If vObjOracle!FL_PRORROGACAO = "S" Then
            frmVDA640.chkProrrogacao.Value = 1
        Else
            frmVDA640.chkProrrogacao.Value = 0
        End If
    
        frmVDA640.cmbAssunto.Text = vObjOracle!DESC_ASSUNTO
        
        If Trim(vObjOracle!cod_topico) = 0 Then
            frmVDA640.cmbTopico.ListIndex = -1
        Else
            frmVDA640.cmbTopico.Text = vObjOracle!DESC_TOPICO
        End If
        
        If Trim(vObjOracle!cod_detalhe) = 0 Then
            frmVDA640.cmbDetalhe.ListIndex = -1
        Else
            frmVDA640.cmbDetalhe.Text = vObjOracle!DESC_DETALHE
        End If
        
        frmVDA640.txtProcedimento.Text = vObjOracle!PROC_RECLAMACAO
        frmVDA640.txtRetorno.Text = vObjOracle!DIAS_RETORNO
        If frmVDA640.txtRetorno.Text = 0 Then
            frmVDA640.optRetornado.Enabled = False
            'frmVDA640.optRetornar.Enabled = False
            'frmVDA640.optDispensa_Retorno.Enabled = False
        Else
            frmVDA640.optRetornado.Enabled = True
            'frmVDA640.optRetornar.Enabled = True
            'frmVDA640.optDispensa_Retorno.Enabled = True
        End If
        
        frmVDA640.txtSolucao.Text = vObjOracle!DIAS_SOLUCAO
        If frmVDA640.txtSolucao.Text = 0 Then
            frmVDA640.optSolucionado.Enabled = False
        Else
            frmVDA640.optSolucionado.Enabled = True
        End If
        
        frmVDA640.txtLiberacao.Text = vObjOracle!DIAS_LIBERACAO
        
        If vObjOracle!FL_DISPENSA_RETORNO = "S" Then
            'frmVDA640.optDispensa_Retorno.Value = True
            'frmVDA640.optRetornar.Enabled = False
        Else
            'frmVDA640.optRetornar.Value = True
        End If
        
        If vObjOracle!fl_falta_complemento = "S" Then
            frmVDA640.chkComplemento.Value = 1
            frmVDA640.optPendente.Enabled = False
            frmVDA640.optRetornado.Enabled = False
            frmVDA640.optSolucionado.Enabled = False
            frmVDA640.optLiberado.Enabled = False
            'frmVDA640.optRetornar.Enabled = False
            'frmVDA640.optDispensa_Retorno.Enabled = False
        Else
            frmVDA640.chkComplemento.Value = 0
        End If
        
        fl_Novo = "N"
        
        Unload Me
        
        frmVDA640.cmdAgenda.Enabled = True
        frmVDA640.cmbAssunto.Enabled = False
        frmVDA640.txtComentarios.SetFocus
        frmVDA640.stb.TabEnabled(1) = True
        frmVDA640.stb.Tab = 1
        
    Else
        Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
    
        frmVDA640.cmdAgenda.Enabled = False
    End If
    
End Sub

Private Sub MonthView1_DateClick(ByVal DateClicked As Date)

    vData_Selecionada = DateClicked
    
End Sub
