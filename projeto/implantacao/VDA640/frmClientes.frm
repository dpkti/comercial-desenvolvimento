VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmClientes 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Busca cliente pela raz�o social"
   ClientHeight    =   5115
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5505
   Icon            =   "frmClientes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   341
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   367
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1050
      Left            =   135
      TabIndex        =   3
      Top             =   45
      Width           =   5235
      Begin VB.TextBox txtRazao_Social 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   90
         TabIndex        =   5
         Top             =   585
         Width           =   3525
      End
      Begin Bot�o.cmd cmdBusca_Clientes 
         Height          =   690
         Left            =   4410
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   225
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmClientes.frx":23D2
         PICN            =   "frmClientes.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Digite a raz�o social (somente o nome):"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   4
         Top             =   360
         Width           =   3345
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4785
      Width           =   5505
      _ExtentX        =   9710
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9657
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid mfgClientes 
      Height          =   3165
      Left            =   135
      TabIndex        =   1
      Top             =   1485
      Width           =   5235
      _ExtentX        =   9234
      _ExtentY        =   5583
      _Version        =   393216
      BackColorBkg    =   -2147483633
      HighLight       =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Clique no cliente desejado:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   2
      Top             =   1215
      Width           =   2355
   End
End
Attribute VB_Name = "frmClientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub cmdBusca_Clientes_Click()
       
   '-- CARREGA O GRID DE CLIENTES COM TODAS AS OP��ES DO NOME PROCURADO
    If txtRazao_Social.Text <> "" Then
                
        frmAguardar.Show
        StayOnTop frmAguardar
        frmAguardar.Refresh
        
        vBanco.Parameters.Remove "PM_RAZAOSOCIAL"
        vBanco.Parameters.Add "PM_RAZAOSOCIAL", "%" & txtRazao_Social.Text & "%", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,NULL,NULL,:PM_RAZAOSOCIAL)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Unload frmAguardar
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        If Not vObjOracle.EOF Then
            Preencher_Grid vObjOracle
            'Call vVB_Generica_001.CarregaGridTabela(mfgClientes, vObjOracle, 3, "0;1")
        Else
            Call vVB_Generica_001.Informar("Nenhum registro encontrado!")
        End If
    End If
    Unload frmAguardar
End Sub

Private Sub mfgClientes_Click()

    mfgClientes.col = 1
    frmVDA640.txtCodigo_Cliente.Text = mfgClientes.Text
    
    If frmVDA640.txtCodigo_Cliente.Text <> "" Then
        Call frmVDA640.txtCodigo_Cliente_LostFocus
    Else
        frmVDA640.txtCodigo_Cliente.SetFocus
    End If
    
    Unload Me
    
End Sub

Private Sub txtRazao_Social_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Sub Preencher_Grid(vObj As Object)
    Dim i As Double
    
    mfgClientes.Clear
    
    mfgClientes.Cols = 3
    mfgClientes.ColWidth(0) = 400
    mfgClientes.ColWidth(1) = 1000
    mfgClientes.ColWidth(2) = 3300
    
    mfgClientes.TextMatrix(0, 1) = "C�digo"
    mfgClientes.CellAlignment = 4
    mfgClientes.TextMatrix(0, 2) = "Nome"
    mfgClientes.CellAlignment = 4
    
    mfgClientes.Rows = vObj.RecordCount + 1
    
    For i = 1 To vObj.RecordCount
        mfgClientes.Rows = mfgClientes.Rows + 1
        mfgClientes.TextMatrix(i, 1) = IIf(IsNull(vObj.Fields(0)), "", vObj.Fields(0))
        mfgClientes.TextMatrix(i, 2) = IIf(IsNull(vObj.Fields(1)), "", vObj.Fields(1))
        vObj.MoveNext
    Next
    
End Sub
