VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MsDatGrd.ocx"
Begin VB.Form frmListagem 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Registros Selecionados"
   ClientHeight    =   5850
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9165
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5850
   ScaleWidth      =   9165
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin Bot�o.cmd cmdExportar 
      Height          =   375
      Left            =   7710
      TabIndex        =   3
      ToolTipText     =   "Gera o arquivo C:\ESTATISTICA_CA.TXT"
      Top             =   30
      Width           =   1425
      _ExtentX        =   2514
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "Exportar para Txt"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmListagem.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox txtQtd 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1950
      Locked          =   -1  'True
      TabIndex        =   2
      Top             =   30
      Width           =   825
   End
   Begin MSDataGridLib.DataGrid DG 
      Height          =   5385
      Left            =   60
      TabIndex        =   0
      Top             =   420
      Width           =   9075
      _ExtentX        =   16007
      _ExtentY        =   9499
      _Version        =   393216
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Quantidade de Registros:"
      Height          =   195
      Left            =   120
      TabIndex        =   1
      Top             =   90
      Width           =   1800
   End
End
Attribute VB_Name = "frmListagem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExportar_Click()
    Dim vArq As Integer
    Dim ii As Integer
    Dim vCampos As String
    Dim vRegistro As String
    
    vArq = FreeFile()
    
    If Dir("C:\Estatistica_CA.txt") <> "" Then Kill "C:\Estatistica_CA.txt"
    
    Open "C:\Estatistica_CA.txt" For Output As #vArq
    
    'Imprimir os nomes dos campos
    For i = 0 To frmConsultar.rst.Fields.Count - 1
        vCampos = vCampos & UCase(frmConsultar.rst.Fields(i).Name) & ";"
    Next
     
    Print #vArq, vCampos
    frmConsultar.rst.MoveFirst
    For i = 1 To frmConsultar.rst.RecordCount
        For ii = 0 To frmConsultar.rst.Fields.Count - 1
            If Not IsNull(frmConsultar.rst.Fields(ii).Value) Then
                vRegistro = vRegistro & Replace(Replace(UCase(frmConsultar.rst.Fields(ii).Value), vbCrLf, ""), Chr(13), "") & ";"
                
            Else
                vRegistro = vRegistro & ";"
            End If
        Next
        
        Print #vArq, vRegistro
        
        vRegistro = ""
        
        frmConsultar.rst.MoveNext
    
    Next
    
    Close #vArq
    
End Sub

Private Sub Form_Load()
    If frmConsultar.vRstUsar = "R" Then
        Set DG.DataSource = frmConsultar.rst
        txtQtd = frmConsultar.rst.RecordCount
    ElseIf frmConsultar.vRstUsar = "F" Then
        Set DG.DataSource = frmConsultar.rst_ForaPrazo
        txtQtd = frmConsultar.rst_ForaPrazo.RecordCount
    ElseIf frmConsultar.vRstUsar = "P" Then
        Set DG.DataSource = frmConsultar.rst_NoPrazo
        txtQtd = frmConsultar.rst_NoPrazo.RecordCount
    End If
    
    
End Sub
