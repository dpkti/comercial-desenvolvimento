Attribute VB_Name = "Mod_VDA640"
Public vLogin As String

Public Declare Function GetUserName Lib _
        "advapi32.dll" Alias "GetUserNameA" _
        (ByVal lpBuffer As String, nSize _
        As Long) As Long

#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#Else
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32

Public vNum_Atendimento As Long
Public fl_Novo As String
Public vCod_Usuario As Long
Public vSenha As String
Public vAtendente As String
Public vEmailAtendente As String
Public vTipo_Atendente As String
Public vCount_Abertos As Long
Public vCount_Finalizados As Long
Public vCount_Prazo As Long
Public vCount_Agenda As Long
Public vCount_Complemento As Long
Public vObjOracle2 As Object                         'OBJETO DO ORACLE
Public vObjOracle3 As Object                         'OBJETO DO ORACLE

Public vTimer As Boolean
Public Declare Function SetTimer Lib "user32" (ByVal hwnd As Long, ByVal nIDEvent As Long, ByVal uElapse As Long, ByVal lpTimerFunc As Long) As Long
Public Declare Function KillTimer Lib "user32" (ByVal hwnd As Long, ByVal nIDEvent As Long) As Long

Public Sub MyTimer(hwnd As Long, msg As Long, idTimer As Long, dwTime As Long)
    
   '-- VERIFICA SE EXISTE CONTATO AGENDADO PARA O ATENDENTE ANTERIOR A HORA ATUAL E DATA DE HOJE
    vBanco.Parameters.Remove "PM_CODIGO"
    vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
    vBanco.Parameters.Remove "PM_DATA"
    vBanco.Parameters.Add "PM_DATA", Format(Now, "dd/mm/yy"), 1
    vBanco.Parameters.Remove "PM_HORA"
    vBanco.Parameters.Add "PM_HORA", CStr(Format(Now, "hh:mm")), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_AGENDA_USUARIO(:PM_CODIGO, " & " TO_DATE(:PM_DATA, 'DD/MM/RR'), :PM_HORA, :vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_AGENDA_USUARIO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Val(vObjOracle!CONTADOR) > 0 Then
        If vTimer = False Then
            vTimer = True
            If Form_Aberto("frmcalendario") = False Then
                If MsgBox("Existem contatos agendados." & vbCrLf & "Deseja abrir a Agenda?", vbYesNo, "Aten��o") = vbYes Then
                    vTimer = False
                    frmCalendario.vQuemChamou = "Timer"
                    frmCalendario.Show
                    StayOnTop frmCalendario
                Else
                    vTimer = False
                End If
            End If
        End If
    End If
    
End Sub

Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

Public Sub Inicia_Grid_Aberto()
    
    frmVDA640.mfgAberto.row = 0
    frmVDA640.mfgAberto.FormatString = ""
    
    frmVDA640.mfgAberto.ColWidth(0) = 1200
    frmVDA640.mfgAberto.ColAlignment(0) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 0) = "DATA"
    
    frmVDA640.mfgAberto.ColWidth(1) = 1150
    frmVDA640.mfgAberto.ColAlignment(1) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 1) = "N�M.ATEND."
    
    frmVDA640.mfgAberto.ColWidth(2) = 2500
    frmVDA640.mfgAberto.FixedAlignment(2) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 2) = "ATENDENTE"
    
    frmVDA640.mfgAberto.ColWidth(3) = 1000
    frmVDA640.mfgAberto.ColAlignment(3) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 3) = "DEP�SITO"
    
    frmVDA640.mfgAberto.ColWidth(4) = 1300
    frmVDA640.mfgAberto.ColAlignment(4) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 4) = "NOTA FISCAL"
    
    frmVDA640.mfgAberto.ColWidth(5) = 2500
    frmVDA640.mfgAberto.FixedAlignment(5) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 5) = "ASSUNTO"
    
    frmVDA640.mfgAberto.ColWidth(6) = 2500
    frmVDA640.mfgAberto.FixedAlignment(6) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 6) = "T�PICO"
    
    frmVDA640.mfgAberto.ColWidth(7) = 2500
    frmVDA640.mfgAberto.FixedAlignment(7) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 7) = "DETALHE"
    
    frmVDA640.mfgAberto.ColWidth(8) = 1500
    frmVDA640.mfgAberto.ColAlignment(8) = 4
    frmVDA640.mfgAberto.TextMatrix(0, 8) = "SITUA��O"
    
    frmVDA640.mfgAberto.ColWidth(9) = 0
    frmVDA640.mfgAberto.TextMatrix(0, 9) = "COMENT�RIOS"
    
End Sub

Public Sub Inicia_Grid_Complemento()
    
    frmPendencias.mfgComplemento_Pendencia.row = 0
    frmPendencias.mfgComplemento_Pendencia.FormatString = ""
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(0) = 1100
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(0) = 4
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 0) = "DT.LIGA��O"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(1) = 1100
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(1) = 4
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 1) = "N�M.ATEND."
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(2) = 2000
    frmPendencias.mfgComplemento_Pendencia.FixedAlignment(2) = 1
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 2) = "ATENDENTE"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(3) = 1200
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(3) = 4
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 3) = "C�D.CLIENTE"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(4) = 2000
    frmPendencias.mfgComplemento_Pendencia.FixedAlignment(4) = 1
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(4) = 2
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 4) = "NOME CLIENTE"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(5) = 2000
    frmPendencias.mfgComplemento_Pendencia.FixedAlignment(5) = 1
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(5) = 2
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 5) = "ASSUNTO"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(6) = 4000
    frmPendencias.mfgComplemento_Pendencia.FixedAlignment(6) = 1
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(6) = 2
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 6) = "T�PICO"
    
    frmPendencias.mfgComplemento_Pendencia.ColWidth(7) = 2000
    frmPendencias.mfgComplemento_Pendencia.FixedAlignment(7) = 1
    frmPendencias.mfgComplemento_Pendencia.ColAlignment(7) = 2
    frmPendencias.mfgComplemento_Pendencia.TextMatrix(0, 7) = "DETALHE"

End Sub

Public Sub Inicia_Grid_Agenda()
    frmPendencias.mfgAgenda_Pendencia.row = 0
    
    frmPendencias.mfgAgenda_Pendencia.FormatString = ""
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(0) = 1100
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(0) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 0) = "DT.AGENDA"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(1) = 1100
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(1) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 1) = "DT.LIGA��O"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(2) = 1150
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(2) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 2) = "N�M.ATEND."
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(3) = 2000
    frmPendencias.mfgAgenda_Pendencia.FixedAlignment(3) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 3) = "ATENDENTE"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(4) = 1100
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(4) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 4) = "C�D.CLIENTE"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(5) = 2500
    frmPendencias.mfgAgenda_Pendencia.FixedAlignment(5) = 4
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(5) = 2
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 5) = "NOME CLIENTE"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(6) = 2000
    frmPendencias.mfgAgenda_Pendencia.FixedAlignment(6) = 4
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(6) = 2
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 6) = "ASSUNTO"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(7) = 2000
    frmPendencias.mfgAgenda_Pendencia.FixedAlignment(7) = 4
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(7) = 2
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 7) = "T�PICO"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(8) = 2000
    frmPendencias.mfgAgenda_Pendencia.FixedAlignment(8) = 4
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(8) = 2
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 8) = "DETALHE"
    
    frmPendencias.mfgAgenda_Pendencia.ColWidth(9) = 1000
    frmPendencias.mfgAgenda_Pendencia.ColAlignment(9) = 4
    frmPendencias.mfgAgenda_Pendencia.TextMatrix(0, 9) = "SITUA��O"
    
End Sub

Public Sub Inicia_Grid_Prazo()

    frmPendencias.mfgPrazo_Pendencia.row = 0
    
    frmPendencias.mfgPrazo_Pendencia.FormatString = ""
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(0) = 1200
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(0) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 0) = "DT.CADASTRO"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(1) = 1100
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(1) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 1) = "N�M.ATEND."
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(2) = 2000
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(2) = 1
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 2) = "ATENDENTE"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(3) = 1100
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(3) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 3) = "C�D.CLIENTE"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(4) = 2000
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(4) = 1
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(4) = 1
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 4) = "NOME CLIENTE"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(5) = 1500
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(5) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 5) = "SITUA��O"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(6) = 700
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(6) = 4
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(6) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 6) = "Loja"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(7) = 1100
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(7) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 7) = "DT.PREVISTA"

    frmPendencias.mfgPrazo_Pendencia.ColWidth(8) = 1000
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(8) = 4
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 8) = "Dias Atraso"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(9) = 2000
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(9) = 1
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(9) = 2
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 9) = "ASSUNTO"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(10) = 2000
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(10) = 4
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(10) = 2
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 10) = "T�PICO"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(11) = 2000
    frmPendencias.mfgPrazo_Pendencia.FixedAlignment(11) = 4
    frmPendencias.mfgPrazo_Pendencia.ColAlignment(11) = 2
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 11) = "DETALHE"
    
    frmPendencias.mfgPrazo_Pendencia.ColWidth(12) = 0
    frmPendencias.mfgPrazo_Pendencia.TextMatrix(0, 12) = "NumDtPrev"
    
End Sub


Public Sub Pintar_Linha(pGrid As MSFlexGrid, pColunas As Byte, pCor, pLinha As Double)
    Dim i As Integer

    pGrid.row = pLinha
    For i = 0 To pColunas - 1
        pGrid.col = i
        pGrid.CellForeColor = pCor
    Next

End Sub

Public Sub Preenche_Combo(combo As ComboBox, vObj As Object)
    Dim i As Integer
    combo.Clear
    For i = 1 To vObj.RecordCount
        If Not IsNull(vObj.Fields(0)) And Not IsNull(vObj.Fields(1)) Then
            combo.AddItem IIf(IsNull(vObj.Fields(1)), "", vObj.Fields(1))
            combo.ItemData(combo.NewIndex) = vObj.Fields(0)
        End If
        vObj.MoveNext
    Next
End Sub

Public Sub Preenche_Combo_Completo()
          
    On Error GoTo Trata_Erro
          
          Dim i As Integer
          Dim vObj As Object
          
1         frmVDA640.cmbAssuntoCompleto.Clear
2         frmVDA640.cmbTopicoCompleto.Clear
3         frmVDA640.cmbDetalheCompleto.Clear
          
      'Assunto
4         Criar_Cursor vBanco.Parameters, "vCursor"
5         vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA640.PR_BUSCA_ASSUNTO(:vCursor); END;"
          
6         Set vObj = vBanco.Parameters("vCursor").Value
7         For i = 1 To vObj.RecordCount
8             If Not IsNull(vObj.Fields(0)) And Not IsNull(vObj.Fields(1)) Then
9                 frmVDA640.cmbAssuntoCompleto.AddItem IIf(IsNull(vObj.Fields(1)), "", vObj.Fields(1))
10                frmVDA640.cmbAssuntoCompleto.ItemData(frmVDA640.cmbAssuntoCompleto.NewIndex) = vObj.Fields(0)
11            End If
12            vObj.MoveNext
13        Next
          
      'Topico
14        Criar_Cursor vBanco.Parameters, "vCursor"
15        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA640.PR_BUSCA_TOPICO(:vCursor); END;"
16        Set vObj = vBanco.Parameters("vCursor").Value
17        For i = 1 To vObj.RecordCount
18            If Not IsNull(vObj.Fields(0)) And Not IsNull(vObj.Fields(1)) Then
19                frmVDA640.cmbTopicoCompleto.AddItem IIf(IsNull(vObj.Fields(1)), "", vObj.Fields(1))
20                frmVDA640.cmbTopicoCompleto.ItemData(frmVDA640.cmbTopicoCompleto.NewIndex) = vObj.Fields(0)
21            End If
22            vObj.MoveNext
23        Next
          
      'Detalhe
24        Criar_Cursor vBanco.Parameters, "vCursor"
25        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA640.PR_BUSCA_DETALHE(:vCursor); END;"
26        Set vObj = vBanco.Parameters("vCursor").Value
27        For i = 1 To vObj.RecordCount
28            If Not IsNull(vObj.Fields(0)) And Not IsNull(vObj.Fields(1)) Then
29                frmVDA640.cmbDetalheCompleto.AddItem IIf(IsNull(vObj.Fields(1)), "", vObj.Fields(1))
30                frmVDA640.cmbDetalheCompleto.ItemData(frmVDA640.cmbDetalheCompleto.NewIndex) = vObj.Fields(0)
31            End If
32            vObj.MoveNext
33        Next
          
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Preenche_Combo_Completo" & vbCrLf & "C�digo:" & Err.Number & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
          
End Sub

Function StayOnTop(Form As Form) 'EX: Call StayOnTop(Me)
    Dim lFlags As Long
    Dim lStay As Long
    
    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Function NonStayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, -2, 0, 0, 0, 0, lFlags)
End Function

Public Function Form_Aberto(frm As String) As Boolean
    Dim f As Form
    For Each f In Forms
        If UCase(frm) = UCase(f.Name) Then
            Form_Aberto = True
            Exit Function
        End If
    Next
    Form_Aberto = False
End Function

Public Function Formata_Data(Dt As String) As String
    Dim vDia As String
    Dim vMes As String
    Dim vAno As String
    
    vDia = Day(Dt)
    vAno = Year(Dt)
    
    vMes = Choose(Month(Dt), "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        
    Formata_Data = vDia & "-" & vMes & "-" & vAno
    
    
End Function

Public Function Formata_Data_Relatorios(Dt As String) As String
    Dim vDia As String
    Dim vMes As String
    Dim vAno As String
    
    vDia = Format(Day(Dt), "00")
    vAno = Mid(Year(Dt), 3, 2)
    
    vMes = Choose(Month(Dt), "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        
    Formata_Data_Relatorios = vDia & "-" & vMes & "-" & vAno
    
    
End Function

Public Function fGet_CodUsuario(pCodAtendente As Double)
          
1         On Error GoTo Trata_Erro
          
2         Criar_Cursor vBanco.Parameters, "vCursor"
          
3         vSql = "Producao.PCK_VDA640.PR_SELECT_ATENDENTE(:VCURSOR)"
          
4         If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
5             Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_ATENDENTE, entre em contato com o SUPORTE!")
6             Exit Function
7         Else
8             Set vObjOracle = vBanco.Parameters("VCURSOR").Value
9         End If
          
10        If vObjOracle.EOF Then
11            Call vVB_Generica_001.Informar("Problemas com o cadastro de atendentes!")
12            Screen.MousePointer = 0
13            Exit Function
14        End If
             
15        For i = 1 To vObjOracle.RecordCount
16            If vObjOracle!Cod_Usuario = pCodAtendente Then
17                fGet_CodUsuario = vObjOracle!Cod_Usuario
18                Exit For
19            End If
20            vObjOracle.MoveNext
21        Next

Trata_Erro:
22        If Err.Number <> 0 Then
23            MsgBox "Sub fGet_CodUsuario" & vbCrLf & "C�digo:" & Err.Number & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
24        End If
          
End Function

Public Sub Pegar_VL_Parametro()
    
    On Error GoTo Trata_Erro
    
    Dim vRstParam As Object
    Dim vRstEmail As Object
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "Cod_Sistema"
    vBanco.Parameters.Add "Cod_Sistema", 1107, 1

    vBanco.Parameters.Remove "Nome_Parametro"
    vBanco.Parameters.Add "Nome_Parametro", "ENVIAR_EMAIL", 1

    vBanco.Parameters.Remove "DEP"
    vBanco.Parameters.Add "DEP", "HELPDESK.", 1

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA230.PR_SELECT_VL_PARAMETRO2(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP)")

    If vErro = "" Then
        Set vRstParam = vBanco.Parameters("vCursor").Value
        
        For i = 1 To vRstParam.RecordCount
            If Val(vRstParam!VL_PARAMETRO) = frmVDA640.cboAtendente.ItemData(frmVDA640.cboAtendente.ListIndex) Then
                Set vRstEmail = vBanco.CreateDynaset("Select E_mail FROM HELPDESK.USUARIO WHERE COD_USUARIO=" & Val(vRstParam!VL_PARAMETRO), 0&)
                GoTo Enviar_email
            End If
            vRstParam.MoveNext
        Next
    
    End If
    
Sair:
    vBanco.Parameters.Remove "Cod_Sistema"
    vBanco.Parameters.Remove "Nome_Parametro"
    vBanco.Parameters.Remove "DEP"
    GoTo FIM
    
Enviar_email:
    vBanco.Parameters.Remove "PM_TO"
    vBanco.Parameters.Add "PM_TO", vRstEmail!E_mail, 1
    
    vBanco.Parameters.Remove "PM_SUBJECT"
    vBanco.Parameters.Add "PM_SUBJECT", "Aviso de Pend�ncia", 1
    
    vBanco.Parameters.Remove "PM_TEXTO"
    vBanco.Parameters.Add "PM_TEXTO", "Voc� tem uma nova pend�ncia no VDA640", 1

    vBanco.ExecuteSQL "BEGIN INTRANET.ENVIA_EMAIL(:PM_TO, :PM_SUBJECT, :PM_TEXTO); end;"
    
FIM:
    Set vRstEmail = Nothing
    Set vRstParam = Nothing
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Pegar_Vl_Parametro" & vbCrLf & "C�digo:" & Err.Number & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

