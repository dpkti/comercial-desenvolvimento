VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MsMask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmConsultar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Crit�rios para Pesquisa"
   ClientHeight    =   7695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11400
   Icon            =   "frmConsultar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   513
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   760
   ShowInTaskbar   =   0   'False
   Begin VB.ListBox lstNatOpe 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   9210
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   40
      Top             =   2070
      Width           =   2175
   End
   Begin VB.Frame fraPrazo 
      Appearance      =   0  'Flat
      Caption         =   "Prazo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1005
      Left            =   3060
      TabIndex        =   35
      Top             =   840
      Visible         =   0   'False
      Width           =   1275
      Begin VB.ComboBox cboPrazo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmConsultar.frx":23D2
         Left            =   90
         List            =   "frmConsultar.frx":23DF
         Style           =   2  'Dropdown List
         TabIndex        =   36
         Top             =   390
         Width           =   1125
      End
   End
   Begin Bot�o.cmd cmdExpandir 
      Height          =   240
      Left            =   7350
      TabIndex        =   31
      TabStop         =   0   'False
      ToolTipText     =   "Detalhes"
      Top             =   0
      Width           =   240
      _ExtentX        =   423
      _ExtentY        =   423
      BTYPE           =   3
      TX              =   "+"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":2400
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.ListBox lstCriterios 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   750
      Left            =   3060
      TabIndex        =   30
      Top             =   0
      Width           =   4275
   End
   Begin VB.Frame FraOutros 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1005
      Left            =   6900
      TabIndex        =   21
      Top             =   840
      Width           =   4455
      Begin VB.ComboBox cboProrrogacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmConsultar.frx":241C
         Left            =   3330
         List            =   "frmConsultar.frx":2429
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   570
         Width           =   1065
      End
      Begin VB.ComboBox cboDevolucao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmConsultar.frx":243A
         Left            =   3330
         List            =   "frmConsultar.frx":2447
         Style           =   2  'Dropdown List
         TabIndex        =   24
         Top             =   180
         Width           =   1065
      End
      Begin VB.ComboBox cboProcede 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmConsultar.frx":2458
         Left            =   1110
         List            =   "frmConsultar.frx":2465
         Style           =   2  'Dropdown List
         TabIndex        =   23
         Top             =   570
         Width           =   1065
      End
      Begin VB.ComboBox cboFaltaCompl 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "frmConsultar.frx":2476
         Left            =   1110
         List            =   "frmConsultar.frx":2483
         Style           =   2  'Dropdown List
         TabIndex        =   22
         Top             =   180
         Width           =   1065
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Prorroga��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2280
         TabIndex        =   29
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Devolu��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   2280
         TabIndex        =   28
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Procede:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   27
         Top             =   630
         Width           =   690
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Falta Compl:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   26
         Top             =   270
         Width           =   990
      End
   End
   Begin VB.ListBox lstLojas 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   30
      Style           =   1  'Checkbox
      TabIndex        =   19
      Top             =   2070
      Width           =   1845
   End
   Begin VB.ListBox lstTipoAtendimento 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   1920
      Style           =   1  'Checkbox
      TabIndex        =   15
      Top             =   2070
      Width           =   4695
   End
   Begin VB.Frame Frame3 
      Caption         =   "Resultados"
      Height          =   3705
      Left            =   30
      TabIndex        =   13
      Top             =   3960
      Width           =   11325
      Begin MSComctlLib.TreeView trv 
         Height          =   3435
         Left            =   60
         TabIndex        =   14
         Top             =   210
         Width           =   11115
         _ExtentX        =   19606
         _ExtentY        =   6059
         _Version        =   393217
         Indentation     =   617
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         FullRowSelect   =   -1  'True
         BorderStyle     =   1
         Appearance      =   0
      End
   End
   Begin VB.Frame FraSituacao 
      Appearance      =   0  'Flat
      Caption         =   "Situa��o"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1005
      Left            =   4380
      TabIndex        =   7
      Top             =   840
      Width           =   2475
      Begin VB.CheckBox chkLiberado 
         Appearance      =   0  'Flat
         Caption         =   "Liberado"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1230
         TabIndex        =   11
         Top             =   660
         Width           =   1125
      End
      Begin VB.CheckBox chkSolucionado 
         Appearance      =   0  'Flat
         Caption         =   "Solucionado"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   1230
         TabIndex        =   10
         Top             =   300
         Width           =   1185
      End
      Begin VB.CheckBox chkRetornado 
         Appearance      =   0  'Flat
         Caption         =   "Retornado"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   60
         TabIndex        =   9
         Top             =   660
         Width           =   1095
      End
      Begin VB.CheckBox chkPendente 
         Appearance      =   0  'Flat
         Caption         =   "Pendente"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   60
         TabIndex        =   8
         Top             =   300
         Width           =   1065
      End
   End
   Begin VB.Frame fraPeriodo 
      Appearance      =   0  'Flat
      Caption         =   "Informe o Per�odo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   30
      TabIndex        =   3
      Top             =   840
      Width           =   2835
      Begin MSMask.MaskEdBox txtDtInicio 
         Height          =   330
         Left            =   180
         TabIndex        =   4
         Top             =   240
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox txtDtFim 
         Height          =   330
         Left            =   180
         TabIndex        =   5
         Top             =   600
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox txtDtRefer 
         Height          =   330
         Left            =   1500
         TabIndex        =   37
         Top             =   600
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   " "
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Dt Refer�ncia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   1500
         TabIndex        =   38
         Top             =   360
         Width           =   1110
      End
   End
   Begin VB.ListBox lstAtendentes 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      Left            =   6660
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   2
      Top             =   2070
      Width           =   2505
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   780
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":2494
      PICN            =   "frmConsultar.frx":24B0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   8490
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":318A
      PICN            =   "frmConsultar.frx":31A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   10650
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Detalhar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":3A80
      PICN            =   "frmConsultar.frx":3A9C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportTxt 
      Height          =   690
      Left            =   9930
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Exportar para Txt"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":4376
      PICN            =   "frmConsultar.frx":4392
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNenhumLoja 
      Height          =   240
      Left            =   30
      TabIndex        =   32
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   3690
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   423
      BTYPE           =   3
      TX              =   "Nenhum"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":4C6C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNenhumAtendimento 
      Height          =   240
      Left            =   1920
      TabIndex        =   33
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   3690
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   423
      BTYPE           =   3
      TX              =   "Nenhum"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":4C88
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNenhumAtendente 
      Height          =   240
      Left            =   6690
      TabIndex        =   34
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   3690
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   423
      BTYPE           =   3
      TX              =   "Nenhum"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":4CA4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExpandirCheck 
      Height          =   690
      Left            =   9210
      TabIndex        =   39
      TabStop         =   0   'False
      ToolTipText     =   "Expandir para Selecionar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":4CC0
      PICN            =   "frmConsultar.frx":4CDC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNenhumNatOpe 
      Height          =   240
      Left            =   9240
      TabIndex        =   41
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   3690
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   423
      BTYPE           =   3
      TX              =   "Nenhum"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsultar.frx":55B6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Natureza Opera��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   9240
      TabIndex        =   42
      Top             =   1860
      Width           =   1785
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Lojas:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   60
      TabIndex        =   20
      Top             =   1860
      Width           =   540
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Tipo Atendimento:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   1950
      TabIndex        =   16
      Top             =   1860
      Width           =   1725
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Atendentes:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   210
      Left            =   6660
      TabIndex        =   6
      Top             =   1860
      Width           =   1140
   End
End
Attribute VB_Name = "frmConsultar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Conn                    As ADODB.Connection
Dim ConnString              As String
Dim ConnCommand             As ADODB.Command
Public rst                  As ADODB.Recordset
Public rst_NoPrazo          As ADODB.Recordset
Public rst_ForaPrazo        As ADODB.Recordset

Dim varLOJA                 As String
Dim RstObj As Object
Dim vSelecionado As Boolean
Dim vLimpandoList As Boolean

Public vRstUsar As String  'N - Normal = RST    F - Fora do Prazo = rst_foraPrazo

Private Sub cboDevolucao_Click()
    If cboDevolucao <> "Todos" Then
        lstCriterios.AddItem "E Devolu��o = " & IIf(cboDevolucao = "S", "Sim", "N�o")
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E Devolu��o = Sim" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub cboFaltaCompl_Click()
    If cboFaltaCompl <> "Todos" Then
        lstCriterios.AddItem "E Falta Complemento = " & IIf(cboFaltaCompl = "S", "Sim", "N�o")
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E Falta Complemento = Sim" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub cboPrazo_Click()
    If cboPrazo <> "Todos" Then
       If cboPrazo = "No Prazo" Then
          lstCriterios.AddItem "E estiver no Prazo"
          For i = 0 To lstCriterios.ListCount - 1
              If Trim(lstCriterios.List(i)) = "E estiver Fora do Prazo" Then
                 lstCriterios.RemoveItem (i)
              End If
          Next
       
       ElseIf cboPrazo = "Fora do Prazo" Then
          lstCriterios.AddItem "E estiver Fora do Prazo"
          For i = 0 To lstCriterios.ListCount - 1
              If Trim(lstCriterios.List(i)) = "E estiver no Prazo" Then
                 lstCriterios.RemoveItem (i)
              End If
          Next
       End If
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E estiver no Prazo" Or Trim(lstCriterios.List(i)) = "E estiver Fora do Prazo" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub cboProcede_Click()
    If cboProcede <> "Todos" Then
       lstCriterios.AddItem "E Procede = " & IIf(cboProcede = "S", "Sim", "N�o")
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E Procede = Sim" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub cboProrrogacao_Click()
    If cboProrrogacao <> "Todos" Then
        lstCriterios.AddItem "E Prorroga��o = " & IIf(cboProrrogacao = "S", "Sim", "N�o")
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E Prorroga��o = Sim" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub


Private Sub chkLiberado_Click()
    If chkLiberado.Value = 1 Then
       lstCriterios.AddItem "E estiver Liberado"
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E estiver Liberado" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub


Private Sub chkPendente_Click()
    If chkPendente.Value = 1 Then
       lstCriterios.AddItem "E estiver Pendente"
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E estiver Pendente" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub chkRetornado_Click()
    If chkRetornado.Value = 1 Then
       lstCriterios.AddItem "E estiver Retornado"
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E estiver Retornado" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub chkSolucionado_Click()
    If chkSolucionado.Value = 1 Then
       lstCriterios.AddItem "E estiver Solucionado"
    Else
        For i = 0 To lstCriterios.ListCount - 1
            If Trim(lstCriterios.List(i)) = "E estiver Solucionado" Then
                lstCriterios.RemoveItem (i)
            End If
        Next
    End If
End Sub

Private Sub cmd1_Click()
    NonStayOnTop frmConsultar
    frmListagem.Show
    StayOnTop frmListagem
End Sub


Private Sub cmdConsultar_Click()
    If txtDtInicio.ClipText = "" And _
       txtDtFim.ClipText = "" And _
       lstAtendentes.SelCount = 0 And _
       chkPendente.Value = False And _
       chkRetornado.Value = False And _
       chkSolucionado.Value = False And _
       chkLiberado.Value = False Then
    
        MsgBox "Defina os crit�rios para Pesquisa.", vbInformation, "Aten��o"
        Exit Sub
    
    End If
    
    NonStayOnTop frmConsultar
    frmAguardar.Show
    frmAguardar.Refresh
    Screen.MousePointer = vbHourglass
    StayOnTop frmAguardar
            
    
    Consultar
        
    trv.Checkboxes = True
    
    Unload frmAguardar
    Screen.MousePointer = vbNormal
End Sub


Private Sub cmdExpandir_Click()
    frmCriterios.txtCriterios = ""
    For i = 0 To Me.lstCriterios.ListCount - 1
        If frmCriterios.txtCriterios = "" Then
            frmCriterios.txtCriterios = lstCriterios.List(i)
        Else
            frmCriterios.txtCriterios = frmCriterios.txtCriterios & vbCrLf & lstCriterios.List(i)
        End If
    Next
    frmCriterios.Visible = True
End Sub


Private Sub cmdMarcarTodos_Click()
    Dim i As Integer
    For i = 0 To lstAtendentes.ListCount - 1
        lstAtendentes.Selected(i) = True
    Next
End Sub


Private Sub cmdExpandirCheck_Click()
    Expandir
End Sub

Private Sub cmdExportTxt_Click()
    Dim vLinha As Integer
    Dim vArq As Integer
    Dim vUltLinha As String
    
    vArq = FreeFile
    
    Open "C:\Estatisticas.txt" For Output As #vArq
    
    For i = 1 To Me.trv.Nodes.Count - 1
    
        If trv.Nodes(i).Checked = True Then
            vLinha = i
            
            vUltLinha = trv.Nodes(i).Text
            
            Print #vArq, Space(NodeNestingLevel(trv.Nodes(i))) & trv.Nodes(i).Text
            
            If trv.Nodes(i).Children <> 0 Then
                a = i
                trv.SelectedItem = trv.Nodes(a).Child
                For ii = 1 To trv.Nodes(i).Children - 1

                    If trv.SelectedItem.Checked = True Then
                        i = a
                        If vUltLinha <> trv.SelectedItem Then
                            vUltLinha = trv.Nodes(i).Text
                            Print #vArq, Space(NodeNestingLevel(trv.SelectedItem)); trv.SelectedItem.Text
                        End If
                    End If
                    
                    If a = vLinha Then
                    
                        trv.SelectedItem = trv.Nodes(a).Child.Next
                    
                    Else
                    
                        trv.SelectedItem = trv.Nodes(a).Next
                    
                    End If
                    
                    a = trv.SelectedItem.Index

                Next
            End If
        End If
    
    Next
    
    Close #vArq
    
End Sub

Private Sub cmdNenhumAtendente_Click()
    vLimpandoList = True
    
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Atendente for igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    
    For i = 0 To lstAtendentes.ListCount - 1
        lstAtendentes.Selected(i) = False
    Next
    vLimpandoList = False
End Sub

Private Sub cmdNenhumAtendimento_Click()
    vLimpandoList = True
    
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Assunto/Topico/Detalhe igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    
    For i = 0 To lstTipoAtendimento.ListCount - 1
        lstTipoAtendimento.Selected(i) = False
    Next
    
    vLimpandoList = False

End Sub

Private Sub cmdNenhumLoja_Click()
    vLimpandoList = True
    For i = 0 To lstLojas.ListCount - 1
        lstLojas.Selected(i) = False
    Next
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Loja for igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    vLimpandoList = False
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.width - Me.width) / 2
    
    'vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "VDA020", False, Me)

    'If vErro <> "" Then

    '    End

    'End If
    
    'vCd = vVB_Generica_001.vCd
    'vTipoCD = vVB_Generica_001.vTipoCD
    'Set vSessao = vVB_Generica_001.vSessao
    'Set vBanco = vVB_Generica_001.vBanco
    
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1

    'Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    cboDevolucao = "Todos"
    cboFaltaCompl = "Todos"
    cboProcede = "Todos"
    cboProrrogacao = "Todos"
    cboPrazo = "Todos"
    
    Preenche_Combo_Atendente
    
    Preenche_List_Procedimento
    
    Preenche_List_Lojas
    
    Preenche_List_NatOpe
    
    Conecta_ADO
    
End Sub

Sub Preenche_Combo_Atendente()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vSql = "Producao.PCK_VDA640.Pr_Select_Atendente(:PM_Cursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_ATENDENTE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    For i = 1 To vObjOracle.RecordCount
        lstAtendentes.AddItem vObjOracle.Fields(1).Value
        lstAtendentes.ItemData(lstAtendentes.NewIndex) = vObjOracle.Fields(0).Value
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub

Sub Preenche_List_Procedimento()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vSql = "Producao.PCK_VDA640.Pr_Select_Procedimento(:PM_Cursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    For i = 1 To vObjOracle.RecordCount
        lstTipoAtendimento.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value & "//" & vObjOracle.Fields(2).Value & "-" & vObjOracle.Fields(3) & "//" & vObjOracle.Fields(4) & " - " & vObjOracle.Fields(5)
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub

Sub Preenche_List_Lojas()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vBanco.Parameters.Remove "Loja"
    vBanco.Parameters.Add "Loja", 0, 1
    
    vSql = "Producao.PCK_VDA640.Pr_Select_Lojas(:PM_Cursor, :Loja)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    lstLojas.Clear
    
    For i = 1 To vObjOracle.RecordCount
        lstLojas.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
        lstLojas.ItemData(lstLojas.NewIndex) = vObjOracle.Fields(0)
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub

Sub Preenche_List_NatOpe()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vSql = "Producao.PCK_VDA640.Pr_Select_NatOpe(:PM_Cursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_NATOPE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    lstNatOpe.Clear
    
    For i = 1 To vObjOracle.RecordCount
        lstNatOpe.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub


Sub Consultar()

    Dim vDtInicio As String
    Dim vDtFim As String
    Dim vDtRefer As String
    Dim vPendente As Byte
    Dim vRetorno As Byte
    Dim vSolucionado As Byte
    Dim vPrazo As Byte
    Dim vForaPrazo As Byte
    Dim vAtendentes As String
    Dim vAssunto As String
    Dim vTopico As String
    Dim vDetalhe As String
    Dim vAssuntoAux As String
    Dim vTopicoAux As String
    Dim vDetalheAux As String
    Dim vAssTopDet() As String
    Dim vLoja As String
    Dim vFaltaCompl As String
    Dim vProcede As String
    Dim vDevolucao As String
    Dim vProrrogacao As String
    
    Dim vNatOpe As String
    
    If txtDtInicio.ClipText <> "" Then
       vDtInicio = Formata_Data(txtDtInicio)
    Else
       vDtInicio = "0"
    End If
    
    If txtDtFim.ClipText <> "" Then
       vDtFim = Formata_Data(txtDtFim)
    Else
       vDtFim = "0"
    End If
    
    If txtDtRefer.ClipText <> "" Then
       vDtRefer = Formata_Data(txtDtRefer)
    Else
       vDtRefer = "0"
    End If
    
    
    '------------------------------
    'Lojas
    '------------------------------
    vLoja = ""
    If lstLojas.SelCount > 0 Then
         For i = 0 To lstLojas.ListCount - 1
            If lstLojas.Selected(i) = True Then
                If vLoja = "" Then
                    vLoja = lstLojas.ItemData(i)
                Else
                    vLoja = vLoja & "," & lstLojas.ItemData(i)
                End If
            End If
         Next
    End If
        
    
    '------------------------------
    'Atendentes
    '------------------------------
    vAtendentes = ""
    If lstAtendentes.SelCount > 0 Then
         For i = 0 To lstAtendentes.ListCount - 1
            If lstAtendentes.Selected(i) = True Then
                If vAtendentes = "" Then
                    vAtendentes = lstAtendentes.ItemData(i)
                Else
                    vAtendentes = vAtendentes & "," & lstAtendentes.ItemData(i)
                End If
            End If
         Next
    End If
        
    '------------------------------
    'NatOpe
    '------------------------------
    vNatOpe = ""
    If lstNatOpe.SelCount > 0 Then
         For i = 0 To lstNatOpe.ListCount - 1
            If lstNatOpe.Selected(i) = True Then
               If vNatOpe = "" Then
                 vNatOpe = "''" & Left(lstNatOpe.List(i), 3) & "''"
               Else
                 vNatOpe = vNatOpe & "," & "''" & Left(lstNatOpe.List(i), 3) & "''"
               End If
            End If
         Next
'         If Right(vNatOpe, 5) = Chr(34) & "','" ) Then
'            vNatOpe = Mid(vNatOpe, 1, Len(vNatOpe) - 5)
'         End If
         vNatOpe = "'" & vNatOpe & "'"
    
    End If
    
    
    '------------------------------
    'Situacao
    '------------------------------
    If chkPendente.Value = 1 Then vPendente = 1 Else vPendente = 0
    If chkRetornado.Value = 1 Then vRetorno = 1 Else vRetorno = 0
    If chkSolucionado.Value = 1 Then vSolucionado = 1 Else vSolucionado = 0
    If chkLiberado.Value = 1 Then vLiberado = 1 Else vLiberado = 0
    
    '------------------------------
    'Prazo
    '------------------------------
    If cboPrazo = "Prazo" Then
       vPrazo = 1
    Else
       vPrazo = 0
    End If
    If cboPrazo = "Fora Prazo" Then
       vForaPrazo = 1
    Else
       vForaPrazo = 0
    End If
    
    '------------------------------
    'Assunto / Topico / Detalhe
    '------------------------------
    For i = 0 To lstTipoAtendimento.ListCount - 1
        If lstTipoAtendimento.Selected(i) = True Then
           vAssTopDet = Split(lstTipoAtendimento.List(i), "//")
           vAssuntoAux = Left(vAssTopDet(0), InStr(1, vAssTopDet(0), "-") - 1)
           vTopicoAux = Left(vAssTopDet(1), InStr(1, vAssTopDet(1), "-") - 1)
           vDetalheAux = Left(vAssTopDet(2), InStr(1, vAssTopDet(2), "-") - 1)
            
           If vAssunto = "" Then
              vAssunto = vAssuntoAux
           Else
              vAssunto = vAssunto & "," & vAssuntoAux
           End If
           If vTopico = "" Then
              vTopico = vTopicoAux
           Else
              vTopico = vTopico & "," & vTopicoAux
           End If
           If vDetalhe = "" Then
              vDetalhe = vDetalheAux
           Else
              vDetalhe = vDetalhe & "," & vDetalheAux
           End If
        End If
    Next
        
    vFaltaCompl = Left(cboFaltaCompl, 1)
    vProcede = Left(cboProcede, 1)
    vDevolucao = Left(cboDevolucao, 1)
    vProrrogacao = Left(cboProrrogacao, 1)
    
    Set rst = rsSelect("{CALL PRODUCAO.PCK_VDA640.PR_CONSULTA({resultset 0, PM_CURSOR}, '" _
              & vDtInicio & "','" & vDtFim & "'," & vPendente & "," & vRetorno & "," & vSolucionado & "," & vLiberado & "," & vPrazo & "," & vForaPrazo & ",'" & vAtendentes & "','" & vAssunto & "','" & vTopico & "','" & vDetalhe & "','" & vLoja & "','" & vFaltaCompl & "','" & vProcede & "','" & vDevolucao & "','" & vProrrogacao & "'," & IIf(vNatOpe = "", "'T'", vNatOpe) & ")}")
    
    Set rst_ForaPrazo = rsSelect("{CALL PRODUCAO.PCK_VDA640.PR_SELECT_FORAPRAZO({resultset 0, PM_CURSOR}, '" _
              & vDtInicio & "','" & vDtFim & "'," & vPendente & "," & vRetorno & "," & vSolucionado & "," & vLiberado & "," & vPrazo & "," & vForaPrazo & ",'" & vAtendentes & "','" & vAssunto & "','" & vTopico & "','" & vDetalhe & "','" & vLoja & "','" & vFaltaCompl & "','" & vProcede & "','" & vDevolucao & "','" & vProrrogacao & "','" & vDtRefer & "'," & IIf(vNatOpe = "", "'T'", vNatOpe) & ")}")
    
    Set rst_NoPrazo = rsSelect("{CALL PRODUCAO.PCK_VDA640.PR_SELECT_NOPRAZO({resultset 0, PM_CURSOR}, '" _
              & vDtInicio & "','" & vDtFim & "'," & vPendente & "," & vRetorno & "," & vSolucionado & "," & vLiberado & "," & vPrazo & "," & vForaPrazo & ",'" & vAtendentes & "','" & vAssunto & "','" & vTopico & "','" & vDetalhe & "','" & vLoja & "','" & vFaltaCompl & "','" & vProcede & "','" & vDevolucao & "','" & vProrrogacao & "','" & vDtRefer & "'," & IIf(vNatOpe = "", "''", vNatOpe) & ")}")
    
    Formatar_Treeview
    
End Sub
Sub Criar_Cursor(Banco As Object, pNomeCursor As String)
    
    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

Sub Formatar_Treeview()

On Error Resume Next

    Dim rst_Clone As ADODB.Recordset
    Dim vTotalGeral As Double
    Dim vTotalPendente As Double
    Dim vTotalRetorno As Double
    Dim vTotalSolucionado As Double
    Dim vTotalLiberado As Double
    Dim vTotalGeralForadoPrazo As Double
    Dim vTotalGeralNoPrazo As Double

    Dim vForaPrazoPendentes As Double
    Dim vForaPrazoRetornados As Double
    Dim vForaPrazoSolucionados As Double
    Dim vForaPrazoLiberados As Double
    Dim vNoPrazoPendentes As Double
    Dim vNoPrazoRetornados As Double
    Dim vNoPrazoSolucionados As Double
    Dim vNoPrazoLiberados As Double
    
    
    Dim vAssTopDet() As String
    Dim vAssunto As Double
    Dim vTopico As Double
    Dim vDetalhe As Double

    vTotalGeral = rst.RecordCount
    vTotalGeralNoPrazo = rst_NoPrazo.RecordCount
    vTotalGeralForadoPrazo = rst_ForaPrazo.RecordCount
    
    'No Prazo
    rst_NoPrazo.Filter = adFilterNone
    rst_NoPrazo.Filter = "SITUACAO = 'P'"
    vNoPrazoPendentes = rst_NoPrazo.RecordCount
    
    rst_NoPrazo.Filter = adFilterNone
    rst_NoPrazo.Filter = "SITUACAO = 'R'"
    vNoPrazoRetornados = rst_NoPrazo.RecordCount
    
    rst_NoPrazo.Filter = adFilterNone
    rst_NoPrazo.Filter = "SITUACAO = 'S'"
    vNoPrazoSolucionados = rst_NoPrazo.RecordCount
    
    rst_NoPrazo.Filter = adFilterNone
    rst_NoPrazo.Filter = "SITUACAO = 'L'"
    vNoPrazoLiberados = rst_NoPrazo.RecordCount

    'Fora Prazo
    rst_ForaPrazo.Filter = adFilterNone
    rst_ForaPrazo.Filter = "SITUACAO = 'P'"
    vForaPrazoPendentes = rst_ForaPrazo.RecordCount
    
    rst_ForaPrazo.Filter = adFilterNone
    rst_ForaPrazo.Filter = "SITUACAO = 'R'"
    vForaPrazoRetornados = rst_ForaPrazo.RecordCount
    
    rst_ForaPrazo.Filter = adFilterNone
    rst_ForaPrazo.Filter = "SITUACAO = 'S'"
    vForaPrazoSolucionados = rst_ForaPrazo.RecordCount
    
    rst_ForaPrazo.Filter = adFilterNone
    rst_ForaPrazo.Filter = "SITUACAO = 'L'"
    vForaPrazoLiberados = rst_ForaPrazo.RecordCount

    'Normal
    rst.Filter = "SITUACAO = 'P'"
    vTotalPendente = rst.RecordCount
        
    rst.Filter = "SITUACAO = 'R'"
    vTotalRetorno = rst.RecordCount
        
    rst.Filter = "SITUACAO = 'S'"
    vTotalSolucionado = rst.RecordCount
        
    rst.Filter = "SITUACAO = 'L'"
    vTotalLiberado = rst.RecordCount

    trv.Nodes.Clear
    
    'Principais
    trv.Nodes.Add , , "Geral", "Total Geral - " & vTotalGeral
    trv.Nodes.Add "Geral", tvwChild, "PorAtendente", "Por Atendente"

    'Nivel 2 - Se o usuario selecionar do list
    For i = 0 To lstAtendentes.ListCount - 1
        If lstAtendentes.Selected(i) = True Then
            trv.Nodes.Add "PorAtendente", tvwChild, "Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.List(i)
        End If
    Next
    
    For i = 0 To lstAtendentes.ListCount - 1
        rst.Filter = adFilterNone
        rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i)
        
        If rst.EOF = False Then
           
           trv.Nodes.Add "PorAtendente", tvwChild, "Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.ItemData(i) & "-" & lstAtendentes.List(i) & " - " & rst.RecordCount
               
               trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i), tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_SITUACAO", "Situacao"
               
                   'Por Situacao Dentro do Atendente
                   rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'P'"
                   trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_SITUACAO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_PENDENTE", "Pendente - " & rst.RecordCount
                
                   rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'R'"
                   trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_SITUACAO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_RETORNO", "Retorno - " & rst.RecordCount
                
                   rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'S'"
                   trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_SITUACAO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_SOLUCIONADO", "Solucionado - " & rst.RecordCount
                
                   rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'L'"
                   trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_SITUACAO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_LIBERADO", "Liberado - " & rst.RecordCount
               
                       For ii = 0 To lstTipoAtendimento.ListCount - 1
                        
                            vAssTopDet = Split(lstTipoAtendimento.List(ii), "//")
                            vAssunto = Left(vAssTopDet(0), InStr(1, vAssTopDet(0), "-") - 1)
                            vTopico = Left(vAssTopDet(1), InStr(1, vAssTopDet(1), "-") - 1)
                            vDetalhe = Left(vAssTopDet(2), InStr(1, vAssTopDet(2), "-") - 1)
                                    
                            rst.Filter = adFilterNone
                            rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'P' and COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
                            If rst.RecordCount > 0 Then trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_PENDENTE", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_PROCEDIMENTO_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(ii) & " - " & rst.RecordCount
                    
                            rst.Filter = adFilterNone
                            rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'R' and COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
                            If rst.RecordCount > 0 Then trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_RETORNO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_PROCEDIMENTO_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(ii) & " - " & rst.RecordCount
                    
                            rst.Filter = adFilterNone
                            rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'S' and COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
                            If rst.RecordCount > 0 Then trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_SOLUCIONADO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_PROCEDIMENTO_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(ii) & " - " & rst.RecordCount
                    
                            rst.Filter = adFilterNone
                            rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " and SITUACAO = 'L' and COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
                            If rst.RecordCount > 0 Then trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_LIBERADO", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_PROCEDIMENTO_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(ii) & " - " & rst.RecordCount
                    
                       Next
               
               'Por Procedimento dentro do usuario
               trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i), tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_proc", "Procedimento"
                                          
                   For ii = 0 To lstTipoAtendimento.ListCount - 1
                    
                        vAssTopDet = Split(lstTipoAtendimento.List(ii), "//")
                        vAssunto = Left(vAssTopDet(0), InStr(1, vAssTopDet(0), "-") - 1)
                        vTopico = Left(vAssTopDet(1), InStr(1, vAssTopDet(1), "-") - 1)
                        vDetalhe = Left(vAssTopDet(2), InStr(1, vAssTopDet(2), "-") - 1)
                                
                        rst.Filter = ""
                        rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " AND COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
                        
                        If rst.RecordCount > 0 Then
                           trv.Nodes.Add "Atendente_" & lstAtendentes.ItemData(i) & "_proc", tvwChild, "Atendente_" & lstAtendentes.ItemData(i) & "_proc_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(ii) & " - " & rst.RecordCount
                        End If
                   Next
        
        End If
        
        rst.Filter = ""
    
    Next
    
    trv.Nodes.Add "Geral", tvwChild, "PorSituacao", "Por Situacao"
    
        trv.Nodes.Add "PorSituacao", tvwChild, "Pendente", "Pendente - " & vTotalPendente
        trv.Nodes.Add "PorSituacao", tvwChild, "Retorno", "Retorno - " & vTotalRetorno
        trv.Nodes.Add "PorSituacao", tvwChild, "Solucionado", "Solucionado - " & vTotalSolucionado
        trv.Nodes.Add "PorSituacao", tvwChild, "Liberado", "Liberado - " & vTotalLiberado
    
            'Nivel 3 - Se o usuario n�o selecionar do list
            For i = 0 To lstAtendentes.ListCount - 1
         
                 rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " AND SITUACAO = 'P'"
                 trv.Nodes.Add "Pendente", tvwChild, "Pendente_Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.ItemData(i) & "-" & lstAtendentes.List(i) & " - " & rst.RecordCount
                          
                 rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " AND SITUACAO = 'R'"
                 trv.Nodes.Add "Retorno", tvwChild, "Retorno_Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.ItemData(i) & "-" & lstAtendentes.List(i) & " - " & rst.RecordCount
                 
                 rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " AND SITUACAO = 'S'"
                 trv.Nodes.Add "Solucionado", tvwChild, "Solucionado_Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.ItemData(i) & "-" & lstAtendentes.List(i) & " - " & rst.RecordCount
                 
                 rst.Filter = "COD_USUARIO = " & lstAtendentes.ItemData(i) & " AND SITUACAO = 'L'"
                 trv.Nodes.Add "Liberado", tvwChild, "Liberado_Atendente_" & lstAtendentes.ItemData(i), lstAtendentes.ItemData(i) & "-" & lstAtendentes.List(i) & " - " & rst.RecordCount
                 
            Next
           
    Dim vPendenteNoPrazo As Double
    Dim vPendenteForaPrazo As Double
    
    trv.Nodes.Add "Geral", tvwChild, "PorPrazo", "Por Prazo"
    
    trv.Nodes.Add "PorPrazo", tvwChild, "NoPrazo", "No Prazo - " & vTotalGeralNoPrazo
    
        trv.Nodes.Add "NoPrazo", tvwChild, "NoPrazo_Pendente", "Pendente - " & vNoPrazoPendentes
            NoPrazoPorAtendente "P", "NoPrazo_Pendente"
                NoPrazoPorAssunto "P"
                
        trv.Nodes.Add "NoPrazo", tvwChild, "NoPrazo_Retorno", "Retorno - " & vNoPrazoRetornados
            NoPrazoPorAtendente "R", "NoPrazo_Retorno"
                NoPrazoPorAssunto "R"
                
        trv.Nodes.Add "NoPrazo", tvwChild, "NoPrazo_Solucionado", "Solucionado - " & vNoPrazoSolucionados
            NoPrazoPorAtendente "S", "NoPrazo_Solucionado"
                NoPrazoPorAssunto "S"
        
        trv.Nodes.Add "NoPrazo", tvwChild, "NoPrazo_Liberado", "Liberado - " & vNoPrazoLiberados
            NoPrazoPorAtendente "L", "NoPrazo_Liberado"
                NoPrazoPorAssunto "L"
    
    trv.Nodes.Add "PorPrazo", tvwChild, "ForaPrazo", "Fora do Prazo - " & vTotalGeralForadoPrazo
        trv.Nodes.Add "ForaPrazo", tvwChild, "ForaPrazo_Pendente", "Pendente - " & vForaPrazoPendentes
            ForaPrazoPorAtendente "P", "ForaPrazo_Pendente"
                ForaPrazoPorAssunto "P"
                
        trv.Nodes.Add "ForaPrazo", tvwChild, "ForaPrazo_Retorno", "Retorno - " & vForaPrazoRetornados
            ForaPrazoPorAtendente "R", "ForaPrazo_Retorno"
                ForaPrazoPorAssunto "R"
                
        trv.Nodes.Add "ForaPrazo", tvwChild, "ForaPrazo_Solucionado", "Solucionado - " & vForaPrazoSolucionados
            ForaPrazoPorAtendente "S", "ForaPrazo_Solucionado"
                ForaPrazoPorAssunto "S"
                
        trv.Nodes.Add "ForaPrazo", tvwChild, "ForaPrazo_Liberado", "Liberado - " & vForaPrazoLiberados
            ForaPrazoPorAtendente "L", "ForaPrazo_Liberado"
                ForaPrazoPorAssunto "L"
                
    trv.Nodes.Add "Geral", tvwChild, "PorTipoAtendimento", "Por Tipo Atendimento"
    
    'Nivel 4 - Por Procedimento
    For i = 0 To lstTipoAtendimento.ListCount - 1
                
        vAssTopDet = Split(lstTipoAtendimento.List(i), "//")
        vAssunto = Left(vAssTopDet(0), InStr(1, vAssTopDet(0), "-") - 1)
        vTopico = Left(vAssTopDet(1), InStr(1, vAssTopDet(1), "-") - 1)
        vDetalhe = Left(vAssTopDet(2), InStr(1, vAssTopDet(2), "-") - 1)
                
        rst.Filter = "COD_ASSUNTO = " & vAssunto & " AND COD_TOPICO = " & vTopico & " AND COD_DETALHE = " & vDetalhe
        trv.Nodes.Add "PorTipoAtendimento", tvwChild, "Procedimento_" & vAssunto & "_" & vTopico & "_" & vDetalhe, lstTipoAtendimento.List(i) & " - " & rst.RecordCount

    Next
    
    'Nivel 2
    trv.Nodes.Add "Geral", tvwChild, "PorNatOpe", "Por Natureza Operacao"
        'Nivel 3 - Por Natureza de Operacao
        For i = 0 To lstNatOpe.ListCount - 1
            If lstNatOpe.SelCount > 0 Then
                If lstNatOpe.Selected(i) = True Then
                    rst.Filter = adFilterNone
                    rst.Filter = "COD_NOPE = '" & Left(lstNatOpe.List(i), 3) & "'"
                    If rst.EOF = False Then
                       trv.Nodes.Add "PorNatOpe", tvwChild, "NatOpe_" & Left(lstNatOpe.List(i), 3), lstNatOpe.List(i) & " - " & rst.RecordCount
                    End If
                End If
            Else
                rst.Filter = adFilterNone
                rst.Filter = "COD_NOPE = '" & Left(lstNatOpe.List(i), 3) & "'"
                
                If rst.EOF = False Then
                    trv.Nodes.Add "PorNatOpe", tvwChild, "NatOpe_" & Left(lstNatOpe.List(i), 3), lstNatOpe.List(i) & " - " & rst.RecordCount
                End If
            End If
        Next
    
End Sub


Public Sub Conecta_ADO()
   
   DBOpen "Provider=MSDAORA;" & _
           "Password=PROD;" & _
           "User ID=VDA640;" & _
           "Data Source=PRODUCAO;" & _
           "Persist Security Info=True"

End Sub



Public Function rsSelect(Optional strCommand As String = "") As ADODB.Recordset
   
   If strCommand <> "" Then
    
       Set ConnCommand = New ADODB.Command
       ConnCommand.ActiveConnection = Conn
       ConnCommand.CommandText = strCommand
       ConnCommand.CommandType = adCmdText
       Set rsSelect = ConnCommand.Execute
   End If

End Function

Public Function DBOpen(Optional ConnectionString As String = "") As Boolean
On Error GoTo err_DBopen
   
   Set Conn = New ADODB.Connection
   
   If Conn.state = adStateClosed Then  'Check for previous connection
       If ConnectionString <> "" Then
           ConnString = ConnectionString
       End If
       Conn.CursorLocation = adUseClient
       Conn.Open ConnectionString
    
       Set ConnCommand = New ADODB.Command
       Set ConnCommand.ActiveConnection = Conn 'Set command object
   End If
   
   DBOpen = True
   
   Exit Function

err_DBopen:
   DBOpen = False
   Set Conn = Nothing
   'Err.Raise Err.Number, Err.source & "[DBOpen]", Err.Description

End Function


Private Sub lstAtendentes_Click()
    Dim vAtendentes As String
    
    If vLimpandoList = True Then Exit Sub
    
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Atendente for igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    If lstAtendentes.SelCount > 0 Then
        For i = 0 To lstAtendentes.ListCount - 1
            If lstAtendentes.Selected(i) = True Then
                If vAtendentes = "" Then
                    vAtendentes = lstAtendentes.ItemData(i)
                Else
                    vAtendentes = vAtendentes & ", " & lstAtendentes.ItemData(i)
                End If
            End If
        Next
        lstCriterios.AddItem "E Atendente for igual a " & vAtendentes
    End If
    
    
End Sub

Private Sub lstLojas_Click()
    
    If vLimpandoList = True Then Exit Sub
    
    Dim vLojas As String
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Loja for igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    
    If lstLojas.SelCount > 0 Then
        For i = 0 To lstLojas.ListCount - 1
            If lstLojas.Selected(i) = True Then
                If vLojas = "" Then
                    vLojas = Mid(lstLojas.List(i), 1, InStr(1, lstLojas.List(i), "-") - 1)
                Else
                    vLojas = vLojas & ", " & Mid(lstLojas.List(i), 1, InStr(1, lstLojas.List(i), "-") - 1)
                End If
            End If
        Next
        lstCriterios.AddItem "E Loja for igual a " & vLojas
    End If
End Sub

Private Sub lstTipoAtendimento_Click()
    Dim vAssunto As String
    Dim vTopico As String
    Dim vDetalhe As String
    Dim vAssTopDet() As String
    Dim vAux As String
    Dim vLojas As String
    
    If vLimpandoList = True Then Exit Sub
    
    For i = 0 To lstCriterios.ListCount - 1
        If InStr(1, lstCriterios.List(i), "E Assunto/Topico/Detalhe igual a ") > 0 Then
            lstCriterios.RemoveItem (i)
            Exit For
        End If
    Next
    
    '--------------------------
    'Assunto / Topico / Detalhe
    '--------------------------
    If lstTipoAtendimento.SelCount > 0 Then
        For i = 0 To lstTipoAtendimento.ListCount - 1
            If lstTipoAtendimento.Selected(i) = True Then
               vAssTopDet = Split(lstTipoAtendimento.List(i), "//")
               vAssunto = Left(vAssTopDet(0), InStr(1, vAssTopDet(0), "-") - 1)
               vTopico = Left(vAssTopDet(1), InStr(1, vAssTopDet(1), "-") - 1)
               vDetalhe = Left(vAssTopDet(2), InStr(1, vAssTopDet(2), "-") - 1)
                
               If vAux = "" Then
                  vAux = vAssunto & "/" & vTopico & "/" & vDetalhe
               Else
                  vAux = vAux & " OU " & vAssunto & "/" & vTopico & "/" & vDetalhe
               End If
            
            End If
        Next
        lstCriterios.AddItem "E Assunto/Topico/Detalhe igual a " & vAux
    End If
End Sub

Private Sub trv_NodeCheck(ByVal Node As MSComctlLib.Node)
    Dim vChecked As Boolean
    
    vChecked = Node.Checked
    
    If Not Nothing Is Node.Child And vSelecionado = False Then
        Dim indice As Long
        indice = Node.Child.Index
        
        For i = 0 To Node.Children - 1
            If trv.Nodes.Item(indice + i).Children = 0 Then
                trv.Nodes.Item(indice + i).Checked = vChecked
            End If
        Next

    End If
End Sub

Private Sub trv_NodeClick(ByVal Node As MSComctlLib.Node)
    
    On Error GoTo Trata_Erro
    
    Dim vCriterios As String
    Dim vQtd As Byte
    Dim vCrit() As String
                
    If Node.Key = "Geral" Then
        rst.Filter = ""
        vRstUsar = "R"
        
    ElseIf Mid(Node.Key, 1, 9) = "Atendente" Then
        vCrit = Split(Node.Key, "_")
        If UBound(vCrit()) = 1 Then 'Clicou no nome da atendente
           vCriterios = "COD_USUARIO = " & vCrit(1)
        ElseIf UBound(vCrit()) > 1 Then 'Clicou no Tipo de Atendimento
           vCriterios = "COD_USUARIO = " & vCrit(1) & " AND COD_ASSUNTO = " & vCrit(3) & " AND COD_TOPICO = " & vCrit(4) & " AND COD_DETALHE = " & vCrit(5)
        End If
        vRstUsar = "R"
       
    ElseIf Mid(Node.Key, 1, 8) = "Pendente" Then
         vCrit = Split(Node.Key, "_")
         vCriterios = "COD_USUARIO = " & vCrit(2) & " AND SITUACAO = 'P'"
         vRstUsar = "R"
    
    ElseIf Mid(Node.Key, 1, 7) = "Retorno" Then
         vCrit = Split(Node.Key, "_")
         vCriterios = "COD_USUARIO = " & vCrit(2) & " AND SITUACAO = 'R'"
         vRstUsar = "R"
     
     ElseIf Mid(Node.Key, 1, 11) = "Solucionado" Then
         vCrit = Split(Node.Key, "_")
         vCriterios = "COD_USUARIO = " & vCrit(2) & " AND SITUACAO = 'S'"
         vRstUsar = "R"
     
     ElseIf Mid(Node.Key, 1, 8) = "Liberado" Then
         vCrit = Split(Node.Key, "_")
         If UBound(vCrit()) = 2 Then 'Clicou no nome da atendente
            vCriterios = "COD_USUARIO = " & vCrit(2) & " AND SITUACAO = 'L'"
         End If
         vRstUsar = "R"

     ElseIf InStr(1, Node.Key, "NoPrazoPorAt") > 0 Then
         Dim vNoPrazo() As String
         vNoPrazo = Split(Node.Key, "_")
         If UBound(vNoPrazo()) = 2 Then 'Clicou no nome da atendente
            vCriterios = "COD_USUARIO = " & vNoPrazo(2) & " AND SITUACAO = '" & vNoPrazo(1) & "'"
                 
         ElseIf UBound(vNoPrazo()) > 2 Then 'Clicou no Tipo de Atendimento dentro da Atendente
            vCriterios = "COD_USUARIO = " & vNoPrazo(2) & " AND SITUACAO = '" & vNoPrazo(1) & "' AND cod_assunto = " & vNoPrazo(3) & " AND COD_TOPICO = " & vNoPrazo(4) & " AND COD_DETALHE = " & vNoPrazo(5)
         
         End If
         vRstUsar = "P"
         
     ElseIf InStr(1, Node.Key, "ForaPrazoPorAt") > 0 Then
         Dim vForaPrazo() As String
         vForaPrazo = Split(Node.Key, "_")
         If UBound(vForaPrazo()) = 2 Then 'Clicou no nome da atendente
            vCriterios = "COD_USUARIO = " & vForaPrazo(2) & " AND SITUACAO = '" & vForaPrazo(1) & "'"
                 
         ElseIf UBound(vForaPrazo()) > 2 Then 'Clicou no Tipo de Atendimento dentro da Atendente
            vCriterios = "COD_USUARIO = " & vForaPrazo(2) & " AND SITUACAO = '" & vForaPrazo(1) & "' AND cod_assunto = " & vForaPrazo(3) & " AND COD_TOPICO = " & vForaPrazo(4) & " AND COD_DETALHE = " & vForaPrazo(5)
         
         End If
         vRstUsar = "F"
     
     ElseIf Left(Node.Key, 12) = "Procedimento" Then
         vCrit = Split(Node.Key, "_")
         vCriterios = "COD_ASSUNTO = " & vCrit(1) & " AND COD_TOPICO = " & vCrit(2) & " AND COD_DETALHE = " & vCrit(3)
         vRstUsar = "P"
         
     End If

FIM:
    If vRstUsar = "R" Then
        rst.Filter = vCriterios
    ElseIf vRstUsar = "F" Then
        rst_ForaPrazo.Filter = adFilterNone
        rst_ForaPrazo.Filter = vCriterios '& " AND (NUM_ATENDIMENTO <> " & vNumAtendsForaPrazo & ")"
    ElseIf vRstUsar = "P" Then
        rst_NoPrazo.Filter = adFilterNone
        rst_NoPrazo.Filter = vCriterios '& " AND (NUM_ATENDIMENTO = " & vNumAtendsNoPrazo & ")"
    End If
        
Trata_Erro:
    If Err.Number = 9 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
        Resume
        Exit Sub
    End If
    
End Sub
 Sub Expandir()
    Dim a As Integer
    For a = 1 To trv.Nodes.Count
        trv.Nodes(a).Expanded = True
    Next
End Sub

Private Sub txtDtFim_LostFocus()
    If txtDtFim.ClipText <> "" Then
       lstCriterios.List(0) = "De: " & Trim(txtDtInicio) & " At�: " & Trim(txtDtFim)
       txtDtRefer = txtDtFim
    Else
       lstCriterios.Clear
    End If
End Sub

Private Sub txtDtInicio_LostFocus()
    
    'fraPrazo.Enabled = txtDtInicio.ClipText <> ""
    FraSituacao.Enabled = txtDtInicio.ClipText <> ""
    FraOutros.Enabled = txtDtInicio.ClipText <> ""
    lstLojas.Enabled = txtDtInicio.ClipText <> ""
    lstAtendentes.Enabled = txtDtInicio.ClipText <> ""
    lstTipoAtendimento.Enabled = txtDtInicio.ClipText <> ""
    lstNatOpe.Enabled = txtDtInicio.ClipText <> ""
    
    If txtDtInicio.ClipText <> "" Then
       If lstCriterios.ListCount <= 0 Then
          lstCriterios.AddItem "De: " & txtDtInicio
       Else
          lstCriterios.List(0) = "De: " & txtDtInicio
       End If
    Else
       lstCriterios.Clear
    End If
End Sub

'Sub TotalPrazo()
'    Dim Qtd As Double
'
'    vNumAtendsForaPrazo = ""
'    vNumAtendsNoPrazo = ""
'
'    rst_ForaPrazo.MoveFirst
'    rst_NoPrazo.MoveFirst
'
'    While rst_ForaPrazo.EOF = False
'        If rst_ForaPrazo!Situacao = "P" And Not IsNull(rst_ForaPrazo!dt_prev_retorno) Then
'           If rst_ForaPrazo!dt_prev_retorno < Format(Now, "DD/MM/YY") Then
'              If Not IsNull(rst_ForaPrazo!dt_retorno) Then
'                 If Format(CDate(rst_ForaPrazo!dt_retorno), "DD/MM/YY") > Format(CDate(rst_ForaPrazo!dt_prev_retorno), "DD/MM/YY") Then
'                    GoTo proximo
'                 Else
'                    If vNumAtendsApagar = "" Then
'                        vNumAtendsForaPrazo = rst_ForaPrazo!NUM_ATENDIMENTO
'                    Else
'                        vNumAtendsForaPrazo = vNumAtendsForaPrazo & " AND NUM_ATENDIMENTO <> " & rst_ForaPrazo!NUM_ATENDIMENTO
'                    End If
'                 End If
'              End If
'           End If
'        ElseIf rst_ForaPrazo!Situacao = "R" Then
'           If rst_ForaPrazo!dt_prev_solucao < Format(Now, "DD/MM/YY") Then
'              If Not IsNull(rst_ForaPrazo!dt_solucao) Then
'                 If Format(CDate(rst_ForaPrazo!dt_solucao), "DD/MM/YY") > Format(CDate(rst_ForaPrazo!dt_prev_solucao), "DD/MM/YY") Then
'                    GoTo proximo
'                 Else
'                    If vNumAtendsForaPrazo = "" Then
'                        vNumAtendsForaPrazo = rst_ForaPrazo!NUM_ATENDIMENTO
'                    Else
'                        vNumAtendsForaPrazo = vNumAtendsForaPrazo & " AND NUM_ATENDIMENTO <> " & rst_ForaPrazo!NUM_ATENDIMENTO
'                    End If
'                 End If
'              End If
'           End If
'        ElseIf rst_ForaPrazo!Situacao = "S" Then
'           If rst_ForaPrazo!dt_Prev_liberacao < Format(Now, "DD/MM/YY") Then
'              If Not IsNull(rst_ForaPrazo!dt_liberacao) Then
'                 If Format(CDate(rst_ForaPrazo!dt_liberacao), "DD/MM/YY") > Format(CDate(rst_ForaPrazo!dt_Prev_liberacao), "DD/MM/YY") Then
'                    GoTo proximo
'                 Else
'                    If vNumAtendsForaPrazo = "" Then
'                        vNumAtendsForaPrazo = rst_ForaPrazo!NUM_ATENDIMENTO
'                    Else
'                        vNumAtendsForaPrazo = vNumAtendsForaPrazo & " AND NUM_ATENDIMENTO <> " & rst_ForaPrazo!NUM_ATENDIMENTO
'                    End If
'                 End If
'              Else
'                GoTo proximo
'              End If
'           End If
'        ElseIf rst_ForaPrazo!Situacao = "L" Then
'            If vNumAtendsForaPrazo = "" Then
'                vNumAtendsForaPrazo = rst_ForaPrazo!NUM_ATENDIMENTO
'            Else
'                vNumAtendsForaPrazo = vNumAtendsForaPrazo & " AND NUM_ATENDIMENTO <> " & rst_ForaPrazo!NUM_ATENDIMENTO
'            End If
'        End If
'proximo:
'        rst_ForaPrazo.MoveNext
'    Wend
'
'    rst_ForaPrazo.Filter = "NUM_ATENDIMENTO <> " & vNumAtendsForaPrazo
'    vTotalGeralForadoPrazo = rst_ForaPrazo.RecordCount
'
'    vNumAtendsNoPrazo = Replace(vNumAtendsForaPrazo, "<>", "=")
'    vNumAtendsNoPrazo = Replace(vNumAtendsNoPrazo, "AND", "OR")
'    rst_NoPrazo.Filter = "NUM_ATENDIMENTO = " & vNumAtendsNoPrazo
'    vTotalGeralNoPrazo = rst_NoPrazo.RecordCount
'
'    'Contar registros No Prazo Pendentes/Retornados/Solucionados/Liberados
'
'    For i = 1 To rst_NoPrazo.RecordCount
'        If rst_NoPrazo!Situacao = "P" Then
'            vNoPrazoPendentes = vNoPrazoPendentes + 1
'        ElseIf rst_NoPrazo!Situacao = "R" Then
'            vNoPrazoRetornados = vNoPrazoRetornados + 1
'        ElseIf rst_NoPrazo!Situacao = "S" Then
'            vNoPrazoSolucionados = vNoPrazoSolucionados + 1
'        ElseIf rst_NoPrazo!Situacao = "L" Then
'            vNoPrazoLiberados = vNoPrazoLiberados + 1
'        End If
'        rst_NoPrazo.MoveNext
'    Next
'
'    'Contar registros Fora do Prazo Pendentes/Retornados/Solucionados/Liberados
'    For i = 1 To rst_ForaPrazo.RecordCount
'        If rst_ForaPrazo!Situacao = "P" Then
'            vForaPrazoPendentes = vForaPrazoPendentes + 1
'        ElseIf rst_ForaPrazo!Situacao = "R" Then
'            vForaPrazoRetornados = vForaPrazoRetornados + 1
'        ElseIf rst_ForaPrazo!Situacao = "S" Then
'            vForaPrazoSolucionados = vForaPrazoSolucionados + 1
'        ElseIf rst_ForaPrazo!Situacao = "L" Then
'            vForaPrazoLiberados = vForaPrazoLiberados + 1
'        End If
'        rst_ForaPrazo.MoveNext
'    Next
'End Sub


Sub NoPrazoPorAtendente(Situacao As String, N�Superior As String)
    Dim Qtd As Double
    Dim iii As Integer
    For iii = 0 To lstAtendentes.ListCount - 1
        Qtd = 0
        rst_NoPrazo.Filter = adFilterNone
        rst_NoPrazo.Filter = "cod_usuario = " & lstAtendentes.ItemData(iii) & " AND Situacao ='" & Situacao & "'"
        Qtd = rst_NoPrazo.RecordCount
        rst_NoPrazo.Filter = adFilterNone
        trv.Nodes.Add N�Superior, tvwChild, "NoPrazoPorAtendente_" & Situacao & "_" & lstAtendentes.ItemData(iii), lstAtendentes.ItemData(iii) & "-" & lstAtendentes.List(iii) & " - " & Qtd
    Next
    
End Sub

Sub ForaPrazoPorAtendente(Situacao As String, N�Superior As String)
    Dim Qtd As Double
    Dim iii As Integer
    
    For iii = 0 To lstAtendentes.ListCount - 1
        rst_ForaPrazo.MoveFirst
        Qtd = 0
        rst_ForaPrazo.Filter = adFilterNone
        rst_ForaPrazo.Filter = "cod_usuario = " & lstAtendentes.ItemData(iii) & " AND Situacao ='" & Situacao & "'"
        Qtd = rst_ForaPrazo.RecordCount
        rst_ForaPrazo.Filter = adFilterNone
        trv.Nodes.Add N�Superior, tvwChild, "ForaPrazoPorAtendente_" & Situacao & "_" & lstAtendentes.ItemData(iii), lstAtendentes.ItemData(iii) & "-" & lstAtendentes.List(iii) & " - " & Qtd
    Next
    
End Sub

Sub NoPrazoPorAssunto(Situacao As String)

    Dim Qtd As Double
    Dim vAtend As Integer
    Dim vTipo As Integer
    Dim vAssTopDet() As String
    Dim vCodAssunto As Double
    Dim vCodTopico As Double
    Dim vCodDetalhe As Double
    
    For vAtend = 0 To lstAtendentes.ListCount - 1
        
        For vTipo = 0 To lstTipoAtendimento.ListCount - 1
            
            vAssTopDet = Split(lstTipoAtendimento.List(vTipo), "//")
            vCodAssunto = Mid(vAssTopDet(0), 1, InStr(1, vAssTopDet(0), "-") - 1)
            vCodTopico = Mid(vAssTopDet(1), 1, InStr(1, vAssTopDet(1), "-") - 1)
            vCodDetalhe = Mid(vAssTopDet(2), 1, InStr(1, vAssTopDet(2), "-") - 1)
            
            Qtd = 0
            
            rst_NoPrazo.Filter = adFilterNone
            rst_NoPrazo.Filter = "NUM_ATENDIMENTO = " & vNumAtendsNoPrazo
            rst_NoPrazo.Filter = "Situacao = '" & Situacao & "' And cod_usuario = " & lstAtendentes.ItemData(vAtend) & " And Cod_assunto =" & vCodAssunto & " And Cod_topico = " & vCodTopico & " And Cod_Detalhe =" & vCodDetalhe
            Qtd = rst_NoPrazo.RecordCount
            rst_NoPrazo.Filter = adFilterNone
            
            'While rst_NoPrazo.EOF = False
            '    If rst_NoPrazo!Situacao = Situacao And rst_NoPrazo!cod_usuario = lstAtendentes.ItemData(vAtend) And rst_NoPrazo!Cod_assunto = vCodAssunto And rst_NoPrazo!Cod_topico = vCodTopico And rst_NoPrazo!Cod_Detalhe = vCodDetalhe Then
            '       Qtd = Qtd + 1
            '    End If
            '    rst_NoPrazo.MoveNext
            'Wend
            
            trv.Nodes.Add "NoPrazoPorAtendente_" & Situacao & "_" & lstAtendentes.ItemData(vAtend), tvwChild, "NoPrazoPorAt_" & Situacao & "_" & lstAtendentes.ItemData(vAtend) & "_" & vCodAssunto & "_" & vCodTopico & "_" & vCodDetalhe, lstTipoAtendimento.List(vTipo) & " - " & Qtd
        
        Next vTipo
    Next vAtend

End Sub

Sub ForaPrazoPorAssunto(Situacao As String)

    Dim Qtd As Double
    Dim vAtend As Integer
    Dim vTipo As Integer
    Dim vAssTopDet() As String
    Dim vCodAssunto As Double
    Dim vCodTopico As Double
    Dim vCodDetalhe As Double
    
    For vAtend = 0 To lstAtendentes.ListCount - 1
        
        For vTipo = 0 To lstTipoAtendimento.ListCount - 1
            
            vAssTopDet = Split(lstTipoAtendimento.List(vTipo), "//")
            vCodAssunto = Mid(vAssTopDet(0), 1, InStr(1, vAssTopDet(0), "-") - 1)
            vCodTopico = Mid(vAssTopDet(1), 1, InStr(1, vAssTopDet(1), "-") - 1)
            vCodDetalhe = Mid(vAssTopDet(2), 1, InStr(1, vAssTopDet(2), "-") - 1)
            
            rst_ForaPrazo.MoveFirst
            
            Qtd = 0
            rst_ForaPrazo.Filter = adFilterNone
            rst_ForaPrazo.Filter = "NUM_ATENDIMENTO = " & vNumAtendsForaPrazo
            rst_ForaPrazo.Filter = "Situacao = '" & Situacao & "' And cod_usuario = " & lstAtendentes.ItemData(vAtend) & " And Cod_assunto =" & vCodAssunto & " And Cod_topico = " & vCodTopico & " And Cod_Detalhe =" & vCodDetalhe
            Qtd = rst_ForaPrazo.RecordCount
            rst_ForaPrazo.Filter = adFilterNone
            
            trv.Nodes.Add "ForaPrazoPorAtendente_" & Situacao & "_" & lstAtendentes.ItemData(vAtend), tvwChild, "ForaPrazoPorAt_" & Situacao & "_" & lstAtendentes.ItemData(vAtend) & "_" & vCodAssunto & "_" & vCodTopico & "_" & vCodDetalhe, lstTipoAtendimento.List(vTipo) & " - " & Qtd
        
        Next vTipo
    Next vAtend



End Sub

Function NodeNestingLevel(ByVal Node As Node) As Integer
    Do Until (Node.Parent Is Nothing)
        NodeNestingLevel = NodeNestingLevel + 1
        Set Node = Node.Parent
    Loop
End Function

