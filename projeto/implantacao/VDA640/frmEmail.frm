VERSION 5.00
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmEmail 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Enviar Email - Coleta"
   ClientHeight    =   4350
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6525
   ClipControls    =   0   'False
   Icon            =   "frmEmail.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   290
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   435
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.FileListBox File1 
      Height          =   675
      Left            =   2400
      Pattern         =   "*.HTML"
      TabIndex        =   12
      Top             =   60
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ListBox lstFiles 
      Appearance      =   0  'Flat
      Height          =   1380
      Left            =   60
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   10
      Top             =   2910
      Width           =   5025
   End
   Begin VB.ComboBox cboUsuarios 
      Height          =   315
      Left            =   2730
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   1140
      Width           =   3780
   End
   Begin MSComctlLib.ListView lsvEmail 
      Height          =   765
      Left            =   60
      TabIndex        =   4
      Top             =   1500
      Width           =   6465
      _ExtentX        =   11404
      _ExtentY        =   1349
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nome"
         Object.Width           =   4498
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Email"
         Object.Width           =   6086
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Codigo"
         Object.Width           =   0
      EndProperty
   End
   Begin VB.ComboBox cboLojas 
      Height          =   315
      Left            =   60
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   1140
      Width           =   2640
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   6465
      _ExtentX        =   11404
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEmail.frx":23D2
      PICN            =   "frmEmail.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   4290
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Salvar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEmail.frx":30C8
      PICN            =   "frmEmail.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   690
      Left            =   5040
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEmail.frx":3DBE
      PICN            =   "frmEmail.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEnviar 
      Height          =   690
      Left            =   5790
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Enviar Email"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmEmail.frx":4AB4
      PICN            =   "frmEmail.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSMAPI.MAPIMessages MAPIMensagem1 
      Left            =   1500
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISessao1 
      Left            =   900
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin MSComCtl2.DTPicker DtDia 
      Height          =   315
      Left            =   540
      TabIndex        =   13
      Top             =   2280
      Width           =   1245
      _ExtentX        =   2196
      _ExtentY        =   556
      _Version        =   393216
      Format          =   57999361
      CurrentDate     =   38401
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   90
      TabIndex        =   14
      Top             =   2370
      Width           =   375
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Lojas com Email Pendente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   90
      TabIndex        =   11
      Top             =   2700
      Width           =   2205
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Usu�rio:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   2760
      TabIndex        =   6
      Top             =   930
      Width           =   645
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Lojas:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   90
      TabIndex        =   3
      Top             =   900
      Width           =   465
   End
End
Attribute VB_Name = "frmEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboLojas_Click()
    Dim Litem As ListItem
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", cboLojas.ItemData(cboLojas.ListIndex), 1
    
    vSql = "Producao.PCK_VDA640.PR_SELECT_EMAIL(:vCursor, :PM_COD_LOJA)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_USUARIOS, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    lsvEmail.ListItems.Clear

    For i = 1 To vObjOracle.RecordCount
        Set Litem = lsvEmail.ListItems.Add
        Litem = vObjOracle!Nome_Usuario
        Litem.SubItems(1) = vObjOracle!E_mail
        Litem.SubItems(2) = vObjOracle!Cod_Usuario
        vObjOracle.MoveNext
    Next

End Sub

Private Sub cmdEnviar_Click()
    On Error GoTo Erro
    
    Dim vArqs(100) As String
    Dim L As Integer
    
    For L = 0 To lstFiles.ListCount - 1
        If lstFiles.Selected(L) = True Then
            
            MAPISessao1.SignOn
            
            If Err <> 0 Then
                MsgBox "Falha de conex�o: " + Error$
            Else
                MAPIMensagem1.SessionID = MAPISessao1.SessionID
            End If
            
            MAPIMensagem1.Compose
            'Elisson SDS2275
            'MAPIMensagem1.MsgSubject = "Servico de Atendimento ao Cliente - Autoriza��o de Coleta"
            MAPIMensagem1.MsgSubject = TITULO_EMAIL
            MAPIMensagem1.MsgNoteText = "Segue arquivo contendo Autoriza��o de Coleta."
            MAPIMensagem1.RecipDisplayName = Mid(lstFiles.List(L), InStr(1, lstFiles.List(L), "-") + 2)
            MAPIMensagem1.ResolveName
            
            For ii = 0 To File1.ListCount - 1
                If lstFiles.ItemData(L) = Left(File1.List(ii), InStr(1, File1.List(ii), "_") - 1) Then
                    MAPIMensagem1.AttachmentIndex = ii
                    MAPIMensagem1.AttachmentPosition = MAPIMensagem1.AttachmentPosition + IIf(ii = 0, 10, 5)
                    MAPIMensagem1.AttachmentPathName = "C:\Coleta\" & File1.List(ii)
                    For iii = 0 To 100
                        If vArqs(iii) = "" Then
                            vArqs(iii) = File1.List(ii)
                            Exit For
                        End If
                    Next
                End If
            Next ii
            
            MAPIMensagem1.Send True
            MAPISessao1.SignOff
        
            For i = 0 To 100
                If vArqs(i) <> "" Then
                    FileCopy "C:\Coleta\" & vArqs(i), "C:\Coleta\Backup\" & vArqs(i)
                
                    While Dir("C:\Coleta\Backup\" & vArqs(i)) = ""
                    Wend
                
                    Kill "C:\Coleta\" & vArqs(i)
                Else
                    Exit For
                End If
            Next
        End If
    Next L
    
    Exit Sub

Erro:
    
    MAPISessao1.SignOff

    If Err.Number = 32003 Then
        MsgBox "Login cancelado", vbExclamation
    ElseIf Err.Number = 32001 Then
        MsgBox "Envio de email cancelado !", vbExclamation
    ElseIf Err.Number = 32025 Or Err.Number = 32014 Then
        MsgBox "Endere�o invalido !", vbExclamation
    Else
        MsgBox "N�o foi possivel enviar o email" & vbLf & vbLf & Err.Number & " - " & Err.Description, vbExclamation
    End If

End Sub

Private Sub cmdExcluir_Click()
    If Not lsvEmail.SelectedItem Is Nothing Then
        If MsgBox("Confirma exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
            vBanco.Parameters.Remove "PM_COD_LOJA"
            vBanco.Parameters.Add "PM_COD_LOJA", cboLojas.ItemData(cboLojas.ListIndex), 1
            
            vBanco.Parameters.Remove "PM_COD_USUARIO"
            vBanco.Parameters.Add "PM_COD_USUARIO", lsvEmail.SelectedItem.SubItems(2), 1
            
            vSql = "Producao.PCK_VDA640.PR_DELETE_EMAIL(:PM_COD_LOJA, :PM_COD_USUARIO)"
            
            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
                Call vVB_Generica_001.Informar("Erro no processamento da PR_INSERT_EMAIL, entre em contato com o SUPORTE!")
                Exit Sub
            Else
                cboLojas_Click
            End If
        End If
    Else
        MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
    End If
End Sub

Private Sub cmdSalvar_Click()

    If lsvEmail.ListItems.Count > 0 Then
       MsgBox "Remova o registro existente antes de inserir outro.", vbInformation, "Aten��o"
       Exit Sub
    End If

    If cboUsuarios = "" Then
        MsgBox "Escolha um usu�rio da lista.", vbInformation, "Aten��o"
        Exit Sub
    End If

    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", cboLojas.ItemData(cboLojas.ListIndex), 1
    
    vBanco.Parameters.Remove "PM_COD_USUARIO"
    vBanco.Parameters.Add "PM_COD_USUARIO", cboUsuarios.ItemData(cboUsuarios.ListIndex), 1
    
    vSql = "Producao.PCK_VDA640.PR_INSERT_EMAIL(:PM_COD_LOJA, :PM_COD_USUARIO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_INSERT_EMAIL, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        cboLojas_Click
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Dir1_Change()
    File1.Path = Dir1.Path
    Preencher_Lista_Files
End Sub

Private Sub Drive1_Change()
    Dir1.Path = Drive1
End Sub

Private Sub DtDia_Change()
    Preenche_Email_Pendente
End Sub

Private Sub Form_Load()
        
    DtDia.Value = Date
    File1.Path = "C:\Coleta"
    Preenche_Cbo_Lojas
    Preenche_Cbo_Usuarios
    Preenche_Email_Pendente
    
End Sub

Sub Preenche_Cbo_Lojas()

    On Error GoTo TrataErro

1    Criar_Cursor vBanco.Parameters, "vCursor"
    
2    vBanco.Parameters.Remove "PM_COD_LOJA"
3    vBanco.Parameters.Add "PM_COD_LOJA", 0, 1
    
4    vSql = "Producao.PCK_VDA640.Pr_Select_Lojas(:vCursor, :PM_COD_LOJA)"
    
5    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
6        Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_Lojas, entre em contato com o SUPORTE!")
        Exit Sub
    Else
7        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    cboLojas.Clear
    
8    For i = 1 To vObjOracle.RecordCount
9        cboLojas.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
10        cboLojas.ItemData(cboLojas.NewIndex) = vObjOracle.Fields(0)
11        vObjOracle.MoveNext
    Next
    
12    vObjOracle.Close
13    Set vObjOracle = Nothing

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Cbo_Lojas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Sub Preenche_Cbo_Usuarios()
    On Error GoTo TrataErro
    
1    Criar_Cursor vBanco.Parameters, "vCursor"
2    vSql = "Producao.PCK_VDA640.PR_BUSCA_USUARIO(:vCursor)"
    
3    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
4        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
5        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    cboUsuarios.Clear
    
6    For i = 1 To vObjOracle.RecordCount
7        cboUsuarios.AddItem vObjOracle.Fields(1).Value
8        cboUsuarios.ItemData(cboUsuarios.NewIndex) = vObjOracle.Fields(0)
        vObjOracle.MoveNext
    Next
    
9    vObjOracle.Close
10    Set vObjOracle = Nothing

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Cbo_usuario" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Sub Preencher_Lista_Files()

    Me.lstFiles.Clear
    
    For i = 0 To Me.File1.ListCount - 1
        lstFiles.AddItem File1.List(i)
    Next

End Sub

Sub Preenche_Email_Pendente()
    On Error GoTo TrataErro

    Dim vArr() As String
    Dim vLoja As Integer
    Dim vData As String
    Dim L As Integer
    
    lstFiles.Clear
    
1    For L = 0 To cboLojas.ListCount - 1
2        For ii = 0 To File1.ListCount - 1
             If File1.List(ii) <> "" Then
                 vArr = Split(File1.List(ii), "_")
                 vLoja = vArr(0)
                 vData = vArr(1)
3                If cboLojas.ItemData(L) = vLoja And CStr(Format(DtDia, "YYYYMMDD")) = vData Then
4                    If floja_ja_existe(Mid(cboLojas.List(L), InStr(1, cboLojas.List(L), "-") + 1)) = False Then
5                        lstFiles.AddItem Mid(cboLojas.List(L), InStr(1, cboLojas.List(L), "-") + 1) & " - " & fPegar_Email_Loja(cboLojas.ItemData(L))
6                        lstFiles.ItemData(lstFiles.NewIndex) = cboLojas.ItemData(L)
                    End If
                End If
            End If
        Next
    Next
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Email_Pendente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Function floja_ja_existe(Loja As String) As Boolean
    For i = 0 To Me.lstFiles.ListCount - 1
        If Loja = lstFiles.List(i) Then
            floja_ja_existe = True
            Exit Function
        End If
    Next
    floja_ja_existe = False
End Function

Function fPegar_Email_Loja(Loja As Double) As String
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", Loja, 1
    
    vSql = "Producao.PCK_VDA640.PR_SELECT_EMAIL(:vCursor, :PM_COD_LOJA)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_USUARIOS, entre em contato com o SUPORTE!")
        Exit Function
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
        fPegar_Email_Loja = IIf(IsNull(vObjOracle!E_mail), "", vObjOracle!E_mail)
    End If

End Function

Private Sub lstFiles_ItemCheck(Item As Integer)
    If UCase(Right(lstFiles.List(Item), 3)) <> ".BR" Then
        MsgBox "Defina um email para esta loja.", vbInformation, "Aten��o"
        lstFiles.Selected(lstFiles.ListIndex) = False
        Exit Sub
    End If
End Sub
