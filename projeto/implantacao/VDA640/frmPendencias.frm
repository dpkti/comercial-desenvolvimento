VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPendencias 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pend�ncias"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11910
   Icon            =   "frmPendencias.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8760
   ScaleWidth      =   11910
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cboAtendente 
      Height          =   315
      Left            =   4170
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   600
      Width           =   2865
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   8430
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18389
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   1020
      Width           =   11820
      _ExtentX        =   20849
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   11160
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   150
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendencias.frx":23D2
      PICN            =   "frmPendencias.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgPrazo_Pendencia 
      Height          =   960
      Left            =   30
      TabIndex        =   3
      Tag             =   "Dt.Cadastro|N�m.Atend.|Atendente|C�d.Cliente|Nome Cliente|Assunto|T�pico|Detalhe|Situa��o|Dt.Prevista|NumDtPrev"
      Top             =   1320
      Width           =   11790
      _ExtentX        =   20796
      _ExtentY        =   1693
      _Version        =   393216
      Cols            =   13
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdPrazo 
      Height          =   930
      Left            =   90
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pend�ncias � Resolver"
      Top             =   30
      Width           =   1140
      _ExtentX        =   2011
      _ExtentY        =   1640
      BTYPE           =   3
      TX              =   "Pend�ncias"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendencias.frx":30C8
      PICN            =   "frmPendencias.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAgenda 
      Height          =   930
      Left            =   1275
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Retornos agendados"
      Top             =   30
      Width           =   1080
      _ExtentX        =   1905
      _ExtentY        =   1640
      BTYPE           =   3
      TX              =   "Agenda"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendencias.frx":3DBE
      PICN            =   "frmPendencias.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdComplemento 
      Height          =   930
      Left            =   2400
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Falta Complemento"
      Top             =   30
      Width           =   1395
      _ExtentX        =   2461
      _ExtentY        =   1640
      BTYPE           =   3
      TX              =   "Complemento"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendencias.frx":4AB4
      PICN            =   "frmPendencias.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgAgenda_Pendencia 
      Height          =   960
      Left            =   30
      TabIndex        =   9
      Tag             =   "Dt.Agenda|Dt.Liga��o|N�m.Atend.|Atendente|C�d.Cliente|Nome Cliente|Assunto|T�pico|Detalhe|Situa��o"
      Top             =   2640
      Width           =   11790
      _ExtentX        =   20796
      _ExtentY        =   1693
      _Version        =   393216
      Cols            =   12
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid mfgComplemento_Pendencia 
      Height          =   1005
      Left            =   30
      TabIndex        =   12
      Tag             =   "Dt.Liga��o|N�m.Atend.|Atendente|C�d.Cliente|Nome Cliente|Assunto|T�pico|Detalhe"
      Top             =   3945
      Width           =   11790
      _ExtentX        =   20796
      _ExtentY        =   1773
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      ForeColor       =   128
      ForeColorFixed  =   0
      BackColorBkg    =   -2147483633
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblConsulta 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   330
      Left            =   4170
      TabIndex        =   17
      Top             =   30
      Width           =   75
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Filtrar por atendente:"
      Height          =   195
      Left            =   4170
      TabIndex        =   16
      Top             =   390
      Width           =   1410
   End
   Begin VB.Label lblComplemento 
      Appearance      =   0  'Flat
      Caption         =   "Falta Complemento:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   75
      TabIndex        =   14
      Top             =   3720
      Width           =   1725
   End
   Begin VB.Label lblQtd_Complemento 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1830
      TabIndex        =   13
      Top             =   3720
      Width           =   420
   End
   Begin VB.Label lblAgenda 
      Appearance      =   0  'Flat
      Caption         =   "Agenda:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   75
      TabIndex        =   11
      Top             =   2415
      Width           =   780
   End
   Begin VB.Label lblQtd_Agenda 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   885
      TabIndex        =   10
      Top             =   2415
      Width           =   420
   End
   Begin VB.Label lblQtd_Prazo 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   705
      TabIndex        =   5
      Top             =   1110
      Width           =   420
   End
   Begin VB.Label lblPrazo 
      Appearance      =   0  'Flat
      Caption         =   "Prazo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   75
      TabIndex        =   4
      Top             =   1110
      Width           =   600
   End
End
Attribute VB_Name = "frmPendencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Grid_Visivel As String
Dim vColuna As Byte
Dim vCboAtendente As Boolean
Dim vDataHoje As String

Private Sub cmdVoltar_Pendencias_Click()
    Unload Me
End Sub

Private Sub cboAtendente_Click()
    
1   On Error GoTo Trata_Erro
        
    Static vPassou As Byte
    
2   If Grid_Visivel = "" Then
3       vPassou = vPassou + 1
4       If vPassou = 1 Then
5           MsgBox "Clique em um dos tipos de Consulta.", vbInformation, "Aten��o"
6           cboAtendente = "<Nenhum>"
7       End If
8       Exit Sub
9   End If
10  If cboAtendente <> "<Nenhum>" And cboAtendente <> "" Then
11      vBanco.Parameters.Remove "PM_COD_ATENDENTE"
12      vBanco.Parameters.Add "PM_COD_ATENDENTE", cboAtendente.ItemData(cboAtendente.ListIndex), 1
13  End If

14  If Grid_Visivel = "mfgPrazo_Pendencia" Then
15     cmdPrazo_Click
16  ElseIf Grid_Visivel = "mfgAgenda_Pendencia" Then
17     cmdAgenda_Click
18  ElseIf Grid_Visivel = "mfgComplemento_Pendencia" Then
19     cmdComplemento_Click
20  End If

Trata_Erro:
21    If Err.Number <> 0 Then
22      MsgBox "Sub FrmPendencias" & vbCrLf & "CboAtendente_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
23    End If
End Sub

Private Sub cmdAgenda_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim Pretas As Integer
          Dim Vermelhas As Integer

2         Grid_Visivel = "mfgAgenda_Pendencia"
          
3         lblConsulta.Caption = "AGENDA - " & vDataHoje
             
4         mfgAgenda_Pendencia.Clear
5         mfgAgenda_Pendencia.Rows = 2
          
6         Call Inicia_Grid_Agenda
          
7         lblAgenda.Top = lblPrazo.Top
8         lblQtd_Agenda.Top = lblPrazo.Top
9         mfgAgenda_Pendencia.Top = mfgPrazo_Pendencia.Top
10        mfgAgenda_Pendencia.Height = Me.Height - (cmdAgenda.Height * 2.3)
11        mfgAgenda_Pendencia.width = Me.width - (cmdAgenda.Height / 3.8)
          
12        lblPrazo.Visible = False
13        lblQtd_Prazo.Visible = False
14        mfgPrazo_Pendencia.Visible = False
15        lblAgenda.Visible = True
16        lblQtd_Agenda.Visible = True
17        mfgAgenda_Pendencia.Visible = True
18        lblComplemento.Visible = False
19        lblQtd_Complemento.Visible = False
20        mfgComplemento_Pendencia.Visible = False
          
         '-- CARREGA OS DADOS DO GRID DE PEND�NCIA DE AGENDA
21        vBanco.Parameters.Remove "PM_CODIGO"
22        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
23        vBanco.Parameters.Remove "PM_TIPO"
24        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
25        vBanco.Parameters.Remove "PM_ROWNUM"
26        vBanco.Parameters.Add "PM_ROWNUM", 0, 1
          
27        If (cboAtendente = "<Nenhum>" Or cboAtendente = "") And vTipo_Atendente = "C" Then
28            vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_AGENDA(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
29        Else
              'Mudo o tipo para fazer a pesquisa
30            vBanco.Parameters.Remove "PM_TIPO"
31            vBanco.Parameters.Add "PM_TIPO", "A", 1
              
32            If vTipo_Atendente = "A" Then
33                vBanco.Parameters.Remove "PM_COD_ATENDENTE"
34                vBanco.Parameters.Add "PM_COD_ATENDENTE", vCod_Usuario, 1
35            End If
              
36            vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_AGENDA(:vCursor, :PM_COD_ATENDENTE, :PM_TIPO, :PM_ROWNUM)"
          
37        End If
          
38        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
39            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_AGENDA, entre em contato com o SUPORTE!")
40            Exit Sub
41        Else
42            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
43        End If
          
44        If vobjatendente = True Then
              'volto o valor do tipo
45            vBanco.Parameters.Remove "PM_TIPO"
46            vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
47        End If
          
48        vData = Format(Date, "DD/MM/YY")
          
49        If Not vObjOracle.EOF Then
50            vCount_Agenda = vObjOracle.RecordCount
51            lblQtd_Agenda.Caption = vCount_Agenda
              
52            mfgAgenda_Pendencia.Rows = vObjOracle.RecordCount + 1
53            mfgAgenda_Pendencia.row = 0
              
54            For i = 1 To vObjOracle.RecordCount
55                mfgAgenda_Pendencia.TextMatrix(i, 0) = vObjOracle.Fields(0)
56                mfgAgenda_Pendencia.TextMatrix(i, 1) = vObjOracle.Fields(1)
57                mfgAgenda_Pendencia.TextMatrix(i, 2) = vObjOracle.Fields(2)
58                mfgAgenda_Pendencia.TextMatrix(i, 3) = vObjOracle.Fields(3)
59                mfgAgenda_Pendencia.TextMatrix(i, 4) = vObjOracle.Fields(4)
60                mfgAgenda_Pendencia.TextMatrix(i, 5) = vObjOracle.Fields(5)
61                mfgAgenda_Pendencia.TextMatrix(i, 6) = IIf(Left(Trim(vObjOracle.Fields(6)), 3) = 0, "", vObjOracle.Fields(6) & " - " & vObjOracle.Fields(7))
62                mfgAgenda_Pendencia.TextMatrix(i, 7) = IIf(Left(Trim(vObjOracle.Fields(8)), 3) = 0, "", vObjOracle.Fields(8) & " - " & vObjOracle.Fields(9))
63                mfgAgenda_Pendencia.TextMatrix(i, 8) = IIf(Left(Trim(vObjOracle.Fields(10)), 3) = 0, "", vObjOracle.Fields(10) & " - " & vObjOracle.Fields(11))
64                If vObjOracle.Fields(12) = "P" Then
65                    mfgAgenda_Pendencia.TextMatrix(i, 9) = "Pendente"
66                ElseIf vObjOracle.Fields(12) = "R" Then
67                    mfgAgenda_Pendencia.TextMatrix(i, 10) = "Retornado"
68                ElseIf vObjOracle.Fields(12) = "S" Then
69                    mfgAgenda_Pendencia.TextMatrix(i, 11) = "Solucionado"
70                End If
                  
71                If vObjOracle!DT_AGENDAMENTO < vData Then
72                    Vermelhas = Vermelhas + 1
73                    Pintar_Linha mfgAgenda_Pendencia, 10, vbRed, i
74                Else
75                    Pretas = Pretas + 1
76                    Pintar_Linha mfgAgenda_Pendencia, 10, vbBlack, i
77                End If
78                vObjOracle.MoveNext
79            Next
80        End If

81        vCboAtendente = False

82        stbBarra.Panels(1).Text = "No Prazo: " & Pretas
83        stbBarra.Panels(2).Text = "Fora do Prazo: " & Vermelhas

84        Unload frmAguardar

Trata_Erro:
85    If Err.Number <> 0 Then
86      MsgBox "Sub FrmPendencias" & vbCrLf & "CmdAgenda_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
87    End If
End Sub

Private Sub cmdComplemento_Click()

        On Error GoTo Trata_Erro

1         frmAguardar.Show
2         StayOnTop frmAguardar
3         frmAguardar.Refresh

4         mfgComplemento_Pendencia.Clear
5         mfgComplemento_Pendencia.Rows = 2
          
6         Call Inicia_Grid_Complemento
          
7         lblConsulta.Caption = "COMPLEMENTO - " & vDataHoje

8         lblComplemento.Top = lblPrazo.Top
9         lblQtd_Complemento.Top = lblPrazo.Top
10        mfgComplemento_Pendencia.Top = mfgPrazo_Pendencia.Top
11        mfgComplemento_Pendencia.Height = Me.Height - (Me.cmdAgenda.Height * 2.3)
12        mfgComplemento_Pendencia.width = Me.width - (Me.cmdAgenda.Height / 3.8)
          
13        Grid_Visivel = "mfgComplemento_Pendencia"
          
14        lblPrazo.Visible = False
15        lblQtd_Prazo.Visible = False
16        mfgPrazo_Pendencia.Visible = False
17        lblAgenda.Visible = False
18        lblQtd_Agenda.Visible = False
19        mfgAgenda_Pendencia.Visible = False
20        lblComplemento.Visible = True
21        lblQtd_Complemento.Visible = True
22        mfgComplemento_Pendencia.Visible = True
          
         '-- CARREGA OS DADOS DO GRID DE PEND�NCIA DE COMPLEMENTO
23        vBanco.Parameters.Remove "PM_CODIGO"
24        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
25        vBanco.Parameters.Remove "PM_TIPO"
26        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
27        vBanco.Parameters.Remove "PM_ROWNUM"
28        vBanco.Parameters.Add "PM_ROWNUM", 0, 1
          
29        Criar_Cursor vBanco.Parameters, "vCursor"
          
30        If (cboAtendente = "<Nenhum>" Or cboAtendente = "") And vTipo_Atendente = "C" Then
31            vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_COMPLEMENTO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
32        Else
              'Mudo o tipo para fazer a pesquisa
33            vBanco.Parameters.Remove "PM_TIPO"
34            vBanco.Parameters.Add "PM_TIPO", "A", 1
35            If vTipo_Atendente = "A" Then
36                vBanco.Parameters.Remove "PM_COD_ATENDENTE"
37                vBanco.Parameters.Add "PM_COD_ATENDENTE", vCod_Usuario, 1
38            End If
              
39            vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_COMPLEMENTO(:vCursor, :PM_COD_ATENDENTE, :PM_TIPO, :PM_ROWNUM)"
40        End If
              
41        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
42            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_COMPLEMENTO, entre em contato com o SUPORTE!")
43            Exit Sub
44        Else
45            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
46        End If
          
47        vData = Format(Date, "DD/MM/YY")
          
48        If Not vObjOracle.EOF Then
49            vCount_Complemento = vObjOracle.RecordCount
50            lblQtd_Complemento.Caption = vCount_Complemento
              
51            mfgComplemento_Pendencia.Clear
52            mfgComplemento_Pendencia.Rows = 2
              
53            Call Inicia_Grid_Complemento
              
54            mfgComplemento_Pendencia.Rows = vObjOracle.RecordCount + 1
              
55            For i = 1 To vObjOracle.RecordCount
56                mfgComplemento_Pendencia.TextMatrix(i, 0) = vObjOracle.Fields(0)
57                mfgComplemento_Pendencia.TextMatrix(i, 1) = vObjOracle.Fields(1)
58                mfgComplemento_Pendencia.TextMatrix(i, 2) = vObjOracle.Fields(2)
59                mfgComplemento_Pendencia.TextMatrix(i, 3) = vObjOracle.Fields(3)
60                mfgComplemento_Pendencia.TextMatrix(i, 4) = vObjOracle.Fields(4)
61                mfgComplemento_Pendencia.TextMatrix(i, 5) = IIf(Left(Trim(vObjOracle.Fields(5)), 3) = 0, "", vObjOracle.Fields(5) & " - " & vObjOracle.Fields(6))
62                mfgComplemento_Pendencia.TextMatrix(i, 6) = IIf(Left(Trim(vObjOracle.Fields(7)), 3) = 0, "", vObjOracle.Fields(7) & " - " & vObjOracle.Fields(8))
63                mfgComplemento_Pendencia.TextMatrix(i, 7) = IIf(Left(Trim(vObjOracle.Fields(9)), 3) = 0, "", vObjOracle.Fields(9) & " - " & vObjOracle.Fields(10))
                  
64                Pintar_Linha mfgComplemento_Pendencia, 8, vbRed, i
                  
65                vObjOracle.MoveNext
66            Next
67        End If

68        If mfgComplemento_Pendencia.Rows = 2 Then
69           If mfgComplemento_Pendencia.TextMatrix(1, 1) = "" Then stbBarra.Panels(1).Text = "Pendentes: 0"
70        Else
71            stbBarra.Panels(1).Text = "Pendentes: " & mfgComplemento_Pendencia.Rows - 1
72            stbBarra.Panels(2).Text = ""
73        End If
          
74        Unload frmAguardar

Trata_Erro:
109  If Err.Number <> 0 Then
110     MsgBox "Sub FrmPendencias" & vbCrLf & "cmdComplemento_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
111  End If
          
End Sub

Private Sub cmdPrazo_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim Atendente As String
          Dim Vermelhas As Integer
          Dim Pretas As Integer
          Dim vCount_Prazo As Integer
          Dim vAtendente As Boolean
          
2         lblConsulta.Caption = "PEND�NCIAS - " & vDataHoje
          
3         frmAguardar.Show
4         StayOnTop frmAguardar
5         frmAguardar.Refresh
          
6         Grid_Visivel = "mfgPrazo_Pendencia"
          
7         mfgPrazo_Pendencia.Clear
8         mfgPrazo_Pendencia.Rows = 2
          
9         Call Inicia_Grid_Prazo
          
10        mfgPrazo_Pendencia.Height = Me.Height - (cmdAgenda.Height * 2.3)
11        mfgPrazo_Pendencia.width = Me.width - (cmdAgenda.Height / 3.8)
          
12        lblPrazo.Visible = True
13        lblQtd_Prazo.Visible = True
14        mfgPrazo_Pendencia.Visible = True
15        lblAgenda.Visible = False
16        lblQtd_Agenda.Visible = False
17        mfgAgenda_Pendencia.Visible = False
18        lblComplemento.Visible = False
19        lblQtd_Complemento.Visible = False
20        mfgComplemento_Pendencia.Visible = False
          
          'Eduardo - 23/06/05
          'Verificar se o usuario � do TIPO A-Atendente  C-Coordenador
21         vSql = "Producao.PCK_VDA640.PR_BUSCA_USUARIO_CA(:vCursor, :PM_CODIGO)"
          
22        If (cboAtendente <> "<Nenhum>" And cboAtendente <> "") Then
23            vBanco.Parameters.Remove "PM_CODIGO"
24            vBanco.Parameters.Add "PM_CODIGO", cboAtendente.ItemData(cboAtendente.ListIndex), 1
25        Else
26            vBanco.Parameters.Remove "PM_CODIGO"
27            vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
28        End If
          
29        Criar_Cursor vBanco.Parameters, "vCursor"
          
30        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
31        If vErro <> "" Then
32            MsgBox vErro
33        Else
34            Set vObjOracle = vBanco.Parameters("vCursor").Value
35        End If
          
36        If vObjOracle!TIPO_ATENDENTE = "A" Then
37            vBanco.Parameters.Remove "PM_CODIGO"
38            If cboAtendente.ListIndex = -1 Then
39                vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
40            Else
41                vBanco.Parameters.Add "PM_CODIGO", cboAtendente.ItemData(cboAtendente.ListIndex), 1
42            End If
43            vBanco.Parameters.Remove "PM_TIPO"
44            vBanco.Parameters.Add "PM_TIPO", "A", 1
          
45        Else
              '-- CARREGA OS DADOS DO GRID DE PEND�NCIA DE PRAZO
46            vBanco.Parameters.Remove "PM_CODIGO"
47            If cboAtendente.ListIndex = -1 Or cboAtendente.ListIndex = 0 Then
48                vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
49                vBanco.Parameters.Remove "PM_TIPO"
50                vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
51            Else
52                vBanco.Parameters.Add "PM_CODIGO", cboAtendente.ItemData(cboAtendente.ListIndex), 1
53                vBanco.Parameters.Remove "PM_TIPO"
54                vBanco.Parameters.Add "PM_TIPO", "A", 1
55            End If

56            vBanco.Parameters.Remove "PM_ROWNUM"
57            vBanco.Parameters.Add "PM_ROWNUM", 0, 1
58        End If
          
59        Criar_Cursor vBanco.Parameters, "vCursor"
          
60        vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_PRAZO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
          
61        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
62            Unload frmAguardar
63            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_PRAZO, entre em contato com o SUPORTE!")
64            Exit Sub
65        Else
66            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
67        End If
          
          'volto o valor do tipo
68        vBanco.Parameters.Remove "PM_TIPO"
69        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
          
70        vData = Format(Date, "DD/MM/YY")
          
71        If Not vObjOracle.EOF Then
              
72            vCount_Prazo = vObjOracle.RecordCount
73            lblQtd_Prazo.Caption = vCount_Prazo
              
74            mfgPrazo_Pendencia.Rows = vObjOracle.RecordCount + 1
              
75            For i = 1 To vObjOracle.RecordCount
76                mfgPrazo_Pendencia.TextMatrix(i, 0) = vObjOracle.Fields(0)
77                mfgPrazo_Pendencia.TextMatrix(i, 1) = vObjOracle.Fields(1)
78                mfgPrazo_Pendencia.TextMatrix(i, 2) = vObjOracle.Fields(2)
79                mfgPrazo_Pendencia.TextMatrix(i, 3) = vObjOracle.Fields(3)
80                mfgPrazo_Pendencia.TextMatrix(i, 4) = vObjOracle.Fields(4)
                  
81                If vObjOracle.Fields(11) = "P" Then
82                    mfgPrazo_Pendencia.TextMatrix(i, 5) = "Pendente"
83                ElseIf vObjOracle.Fields(11) = "R" Then
84                    mfgPrazo_Pendencia.TextMatrix(i, 5) = "Retornado"
85                ElseIf vObjOracle.Fields(11) = "S" Then
86                    mfgPrazo_Pendencia.TextMatrix(i, 5) = "Solucionado"
87                End If
                  
88                mfgPrazo_Pendencia.TextMatrix(i, 6) = IIf(IsNull(vObjOracle.Fields(18)), "", vObjOracle.Fields(18))
                  
89                If vObjOracle.Fields(11) = "P" Then
90                    If Not IsNull(vObjOracle!DT_PREV_RETORNO) Then
91                        mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_RETORNO
92                        mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_RETORNO), "YYYYMMDD")
93                    ElseIf Not IsNull(vObjOracle!DT_PREV_SOLUCAO) Then
94                        mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_SOLUCAO
95                        mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_SOLUCAO), "YYYYMMDD")
96                    ElseIf Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
97                        mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_LIBERACAO
98                        mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_LIBERACAO), "YYYYMMDD")
99                    End If
                      
100               ElseIf vObjOracle.Fields(11) = "R" Then
101                   If Not IsNull(vObjOracle!DT_PREV_SOLUCAO) Then
102                       mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_SOLUCAO
103                       mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_SOLUCAO), "YYYYMMDD")
104                   ElseIf Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
105                       mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_LIBERACAO
106                       mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_LIBERACAO), "YYYYMMDD")
107                   End If
                      
108               ElseIf vObjOracle.Fields(11) = "S" And Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
109                   mfgPrazo_Pendencia.TextMatrix(i, 7) = vObjOracle!DT_PREV_LIBERACAO
110                   mfgPrazo_Pendencia.TextMatrix(i, 12) = Format(CDate(vObjOracle!DT_PREV_LIBERACAO), "YYYYMMDD")
111               End If
                  
112               If vObjOracle!tempo_atraso > 0 Then
113                   mfgPrazo_Pendencia.TextMatrix(i, 8) = Format(Val(0 & vObjOracle!tempo_atraso), "##0")
114               End If
                  
115               mfgPrazo_Pendencia.TextMatrix(i, 9) = IIf(Left(Trim(vObjOracle.Fields(5)), 3) = 0, "", vObjOracle.Fields(5) & " - " & vObjOracle.Fields(6))
116               mfgPrazo_Pendencia.TextMatrix(i, 10) = IIf(Left(Trim(vObjOracle.Fields(7)), 3) = 0, "", vObjOracle.Fields(7) & " - " & vObjOracle.Fields(8))
117               mfgPrazo_Pendencia.TextMatrix(i, 11) = IIf(Left(Trim(vObjOracle.Fields(9)), 3) = 0, "", vObjOracle.Fields(9) & " - " & vObjOracle.Fields(10))
                  
                 'eDUARDO - 23/05/2006
                  'Call Linha_Preta_Prazo(12)
                  
118               If mfgPrazo_Pendencia.TextMatrix(i, 7) <> "" Then
119                   If CDate(mfgPrazo_Pendencia.TextMatrix(i, 7)) < vData Then
120                           Vermelhas = Vermelhas + 1
121                           Pintar_Linha mfgPrazo_Pendencia, 12, vbRed, i
122                   End If
123               End If
                  
                  'Eduardo
                  '22/06/05
124               If vObjOracle!Proveniente = "I" Then
125                   Pintar_Linha mfgPrazo_Pendencia, 12, &H80&, i
126               End If
                  
127               vObjOracle.MoveNext
              
128           Next
129       End If
130       SortByColumn 12
          
131       mfgPrazo_Pendencia.ColWidth(12) = 0
          
132       stbBarra.Panels(1).Text = "No Prazo: " & mfgPrazo_Pendencia.Rows - (Vermelhas + 1)
133       stbBarra.Panels(2).Text = "Fora do Prazo: " & Vermelhas

134       Unload frmAguardar

Trata_Erro:
135   If Err.Number <> 0 Then
136     MsgBox "Sub FrmPendencias" & vbCrLf & "cmdPrazo_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
137   End If
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    lblPrazo.Visible = False
    lblQtd_Prazo.Visible = False
    mfgPrazo_Pendencia.Visible = False
    lblAgenda.Visible = False
    lblQtd_Agenda.Visible = False
    mfgAgenda_Pendencia.Visible = False
    lblComplemento.Visible = False
    lblQtd_Complemento.Visible = False
    mfgComplemento_Pendencia.Visible = False
    
    If vTipo_Atendente = "C" Then
        Preenche_Combo_Atendente
    End If

    Dim vObjData As Object
    Set vObjData = vBanco.CreateDynaset("Select to_char(sysdate,'DD/MM/RR') from dual", &O0)

    vDataHoje = vObjData.Fields(0)
    
    Set vObjData = Nothing

End Sub

Private Sub mfgAgenda_Pendencia_dblClick()

1         On Error GoTo Trata_Erro

2         mfgAgenda_Pendencia.col = 2
              
3         If mfgAgenda_Pendencia.Text = "" Then Exit Sub
          
4         vNum_Atendimento = mfgAgenda_Pendencia.Text
5         fl_Novo = "N"
          
6         mfgAgenda_Pendencia.col = 4
         '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO C�DIGO DO CLIENTE
7         If mfgAgenda_Pendencia.Text <> "" Then
8             vBanco.Parameters.Remove "PM_CODCLIENTE"
9             vBanco.Parameters.Add "PM_CODCLIENTE", mfgAgenda_Pendencia.Text, 1
              
10            Criar_Cursor vBanco.Parameters, "vCursor"
              
11            vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,:PM_CODCLIENTE,NULL,NULL)"
              
12            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
13                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
14                Exit Sub
15            Else
16                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
17            End If
              
18            If Not vObjOracle.EOF Then
19                frmVDA640.txtDeposito = ""
20                frmVDA640.txtNota_Fiscal = ""
21                frmVDA640.txtCodigo_Cliente = mfgAgenda_Pendencia.Text
22                frmVDA640.txtRazao_Social.Text = vObjOracle!NOME_CLIENTE
23                frmVDA640.txtContato.Text = IIf(IsNull(vObjOracle!NOME_CONTATO), "", vObjOracle!NOME_CONTATO)
24                frmVDA640.txtCGC.Text = vObjOracle!CGC
25                frmVDA640.txtCidade.Text = vObjOracle!NOME_CIDADE
26                frmVDA640.txtUF.Text = vObjOracle!COD_UF
27                frmVDA640.txtCod_Representante.Text = vObjOracle!COD_REPRES
28                frmVDA640.txtNome_Representante.Text = vObjOracle!NOME_REPRES
29                frmVDA640.txtDDD1.Text = vObjOracle!DDD1
30                frmVDA640.txtTelefone1.Text = vObjOracle!FONE1
31                frmVDA640.txtDDD2.Text = IIf(IsNull(vObjOracle!DDD2), 0, vObjOracle!DDD2)
32                frmVDA640.txtTelefone2.Text = IIf(IsNull(vObjOracle!FONE2), 0, vObjOracle!FONE2)
33                frmVDA640.txtFilial.Text = vObjOracle!COD_FILIAL & " - " & vObjOracle!NOME_FILIAL
              
34                frmVDA640.cboAtendente.Tag = mfgAgenda_Pendencia.TextMatrix(mfgAgenda_Pendencia.row, 3)
35                frmVDA640.cboAtendente = mfgAgenda_Pendencia.TextMatrix(mfgAgenda_Pendencia.row, 3)
36                frmVDA640.lblNumAtendimento.Caption = mfgAgenda_Pendencia.TextMatrix(mfgAgenda_Pendencia.row, 1) & " - " & mfgAgenda_Pendencia.TextMatrix(mfgAgenda_Pendencia.row, 2)
              
37            End If
              
             '-- BUSCA O TIPO_FIEL DO CLIENTE
38            vBanco.Parameters.Remove "PM_CODCLIENTE"
39            vBanco.Parameters.Add "PM_CODCLIENTE", mfgAgenda_Pendencia.Text, 1
              
40            Criar_Cursor vBanco.Parameters, "vCursor"
              
41            vSql = "Producao.PCK_VDA640.PR_BUSCA_TP_FIEL(:vCursor,:PM_CODCLIENTE)"
              
42            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
43                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TP_FIEL, entre em contato com o SUPORTE!")
44                Exit Sub
45            Else
46                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
47            End If
              
48            If Not vObjOracle.EOF Then
49                frmVDA640.txtTipo_Fiel.Text = vObjOracle!DESC_FIEL
50            End If
              
             '-- CARREGA OS DADOS DO ATENDIMENTO
51            mfgAgenda_Pendencia.col = 2
52            vBanco.Parameters.Remove "PM_NUMERO"
53            vBanco.Parameters.Add "PM_NUMERO", mfgAgenda_Pendencia.Text, 1
54            vNum_Atendimento = mfgAgenda_Pendencia.Text
                  
55            Criar_Cursor vBanco.Parameters, "vCursor"
              
56            vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
              
57            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
58                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
59                Exit Sub
60            Else
61                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
62            End If
              
63            If Not vObjOracle.EOF Then
64                If vObjOracle!Situacao = "P" Then
65                    frmVDA640.optPendente.Value = True
66                ElseIf vObjOracle!Situacao = "R" Then
67                    frmVDA640.optRetornado.Value = True
68                    frmVDA640.optPendente.Enabled = False
69                    frmVDA640.chkComplemento.Enabled = False
70                ElseIf vObjOracle!Situacao = "S" Then
71                    frmVDA640.optSolucionado.Value = True
72                    frmVDA640.optPendente.Enabled = False
73                    frmVDA640.optRetornado.Enabled = False
74                    frmVDA640.chkComplemento.Enabled = False
75                ElseIf vObjOracle!Situacao = "L" Then
76                    frmVDA640.optLiberado.Value = True
77                    frmVDA640.optPendente.Enabled = False
78                    frmVDA640.optRetornado.Enabled = False
79                    frmVDA640.optSolucionado.Enabled = False
80                    frmVDA640.chkComplemento.Enabled = False
81                End If
                  
82                If vObjOracle!CONTATO = "C" Then
83                    frmVDA640.optCliente.Value = True
84                ElseIf vObjOracle!CONTATO = "R" Then
85                    frmVDA640.optRepresentante.Value = True
86                ElseIf vObjOracle!CONTATO = "T" Then
87                    frmVDA640.optTelevendas.Value = True
88                ElseIf vObjOracle!CONTATO = "O" Then
89                    frmVDA640.optOutros.Value = True
90                End If
                  
91                frmVDA640.txtComentarios.Text = vObjOracle!COMENTARIOS

92                Call frmVDA640.Inserir_Nome_Atendente_Descricao
                  
93                frmVDA640.txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
94                frmVDA640.txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
95                frmVDA640.txtNota_Fiscal_LostFocus
96                frmVDA640.txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
97                frmVDA640.txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
                  
98                If vObjOracle!FL_PROCEDE = "S" Then
99                    frmVDA640.chkProcede.Value = 1
100               Else
101                   frmVDA640.chkProcede.Value = 0
102               End If
              
103               If vObjOracle!FL_DEVOLUCAO = "S" Then
104                   frmVDA640.chkDevolucao.Value = 1
105               Else
106                   frmVDA640.chkDevolucao.Value = 0
107               End If
              
108               If vObjOracle!FL_PRORROGACAO = "S" Then
109                   frmVDA640.chkProrrogacao.Value = 1
110               Else
111                   frmVDA640.chkProrrogacao.Value = 0
112               End If
              
113               frmVDA640.cmbAssunto.Text = vObjOracle!DESC_ASSUNTO
                  
114               If Trim(vObjOracle!cod_topico) = 0 Then
115                   frmVDA640.cmbTopico.ListIndex = -1
116               Else
117                   frmVDA640.cmbTopico.Text = vObjOracle!DESC_TOPICO
118               End If
                  
119               If Trim(vObjOracle!cod_detalhe) = 0 Then
120                   frmVDA640.cmbDetalhe.ListIndex = -1
121               Else
122                   frmVDA640.cmbDetalhe.Text = vObjOracle!DESC_DETALHE
123               End If
                  
124               frmVDA640.txtProcedimento.Text = vObjOracle!PROC_RECLAMACAO
125               If frmVDA640.txtRetorno.Text = 0 Then
126                   frmVDA640.optRetornado.Enabled = False
127               Else
128                   frmVDA640.optRetornado.Enabled = True
129               End If
                  
130               frmVDA640.txtSolucao.Text = vObjOracle!DIAS_SOLUCAO
131               If frmVDA640.txtSolucao.Text = 0 Then
132                   frmVDA640.optSolucionado.Enabled = False
133               Else
134                   frmVDA640.optSolucionado.Enabled = True
135               End If
136               frmVDA640.txtLiberacao.Text = vObjOracle!DIAS_LIBERACAO
                  
137               If vObjOracle!fl_falta_complemento = "S" Then
138                   frmVDA640.chkComplemento.Value = 1
139                   frmVDA640.optPendente.Enabled = False
140                   frmVDA640.optRetornado.Enabled = False
141                   frmVDA640.optSolucionado.Enabled = False
142                   frmVDA640.optLiberado.Enabled = False
143               Else
144                   frmVDA640.chkComplemento.Value = 0
145               End If
                
146               fl_Novo = "N"
                  
147               Me.Visible = False
                  
148               frmVDA640.cmdAgenda.Enabled = True
149               frmVDA640.cmbAssunto.Enabled = False
150               frmVDA640.txtComentarios.SetFocus
151               frmVDA640.stb.TabEnabled(1) = True
152               frmVDA640.stb.Tab = 1
          
153           Else
154               Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
              
155               frmVDA640.cmdAgenda.Enabled = False
156           End If
157       End If
          
Trata_Erro:
158   If Err.Number <> 0 Then
159     MsgBox "Sub FrmPendencias" & vbCrLf & "mfgAgenda_Pendencia" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
160   End If
          
End Sub

Private Sub mfgComplemento_Pendencia_dblClick()
    
1     On Error GoTo Trata_Erro
    
2     mfgComplemento_Pendencia.col = 1
    
3     If mfgComplemento_Pendencia.Text = "" Then Exit Sub
    
4     vNum_Atendimento = mfgComplemento_Pendencia.Text
5     fl_Novo = "N"
   
6     mfgComplemento_Pendencia.col = 3
      '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO C�DIGO DO CLIENTE
7     vBanco.Parameters.Remove "PM_CODCLIENTE"
8     vBanco.Parameters.Add "PM_CODCLIENTE", mfgComplemento_Pendencia.Text, 1
    
9     Criar_Cursor vBanco.Parameters, "vCursor"
    
10    vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,:PM_CODCLIENTE,NULL,NULL)"
    
11    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
12       Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
13       Exit Sub
14    Else
15       Set vObjOracle = vBanco.Parameters("VCURSOR").Value
16    End If
    
17    If Not vObjOracle.EOF Then
18      frmVDA640.txtDeposito = ""
19      frmVDA640.txtNota_Fiscal = ""
20      frmVDA640.txtCodigo_Cliente.Text = mfgComplemento_Pendencia.Text
21      frmVDA640.txtRazao_Social.Text = vObjOracle!NOME_CLIENTE
22      frmVDA640.txtContato.Text = IIf(IsNull(vObjOracle!NOME_CONTATO), "", vObjOracle!NOME_CONTATO)
23      frmVDA640.txtCGC.Text = vObjOracle!CGC
24      frmVDA640.txtCidade.Text = vObjOracle!NOME_CIDADE
25      frmVDA640.txtUF.Text = vObjOracle!COD_UF
26      frmVDA640.txtCod_Representante.Text = vObjOracle!COD_REPRES
27      frmVDA640.txtNome_Representante.Text = vObjOracle!NOME_REPRES
28      frmVDA640.txtDDD1.Text = vObjOracle!DDD1
29      frmVDA640.txtTelefone1.Text = vObjOracle!FONE1
30      frmVDA640.txtDDD2.Text = IIf(IsNull(vObjOracle!DDD2), 0, vObjOracle!DDD2)
31      frmVDA640.txtTelefone2.Text = IIf(IsNull(vObjOracle!FONE2), 0, vObjOracle!FONE2)
32      frmVDA640.txtFilial.Text = vObjOracle!COD_FILIAL & " - " & vObjOracle!NOME_FILIAL
33      frmVDA640.cboAtendente.Tag = mfgComplemento_Pendencia.TextMatrix(mfgComplemento_Pendencia.row, 2)
34      frmVDA640.cboAtendente = mfgComplemento_Pendencia.TextMatrix(mfgComplemento_Pendencia.row, 2)
35      frmVDA640.lblNumAtendimento = mfgComplemento_Pendencia.TextMatrix(mfgComplemento_Pendencia.row, 0) & " - " & mfgComplemento_Pendencia.TextMatrix(mfgComplemento_Pendencia.row, 1)
36    End If
    
   '-- BUSCA O TIPO_FIEL DO CLIENTE
37    vBanco.Parameters.Remove "PM_CODCLIENTE"
38    vBanco.Parameters.Add "PM_CODCLIENTE", mfgComplemento_Pendencia.Text, 1
    
39    Criar_Cursor vBanco.Parameters, "vCursor"
    
40    vSql = "Producao.PCK_VDA640.PR_BUSCA_TP_FIEL(:vCursor,:PM_CODCLIENTE)"
    
41    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
42       Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TP_FIEL, entre em contato com o SUPORTE!")
43       Exit Sub
44    Else
45       Set vObjOracle = vBanco.Parameters("VCURSOR").Value
46    End If
    
47    If Not vObjOracle.EOF Then
48       frmVDA640.txtTipo_Fiel.Text = vObjOracle!DESC_FIEL
49    End If
   
   '-- CARREGA OS DADOS DO ATENDIMENTO
50    mfgComplemento_Pendencia.col = 1
51    vBanco.Parameters.Remove "PM_NUMERO"
52    vBanco.Parameters.Add "PM_NUMERO", mfgComplemento_Pendencia.Text, 1
53    vNum_Atendimento = mfgComplemento_Pendencia.Text
        
54    Criar_Cursor vBanco.Parameters, "vCursor"
    
55    vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
    
56    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
57       Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
58       Exit Sub
59    Else
60       Set vObjOracle = vBanco.Parameters("VCURSOR").Value
61    End If
    
62    If Not vObjOracle.EOF Then
63      If vObjOracle!Situacao = "P" Then
64          frmVDA640.optPendente.Value = True
65      ElseIf vObjOracle!Situacao = "R" Then
66          frmVDA640.optRetornado.Value = True
67          frmVDA640.optPendente.Enabled = False
68          frmVDA640.chkComplemento.Enabled = False
69      ElseIf vObjOracle!Situacao = "S" Then
70          frmVDA640.optSolucionado.Value = True
71          frmVDA640.optPendente.Enabled = False
72          frmVDA640.optRetornado.Enabled = False
73          frmVDA640.chkComplemento.Enabled = False
74      ElseIf vObjOracle!Situacao = "L" Then
75          frmVDA640.optLiberado.Value = True
76          frmVDA640.optPendente.Enabled = False
77          frmVDA640.optRetornado.Enabled = False
78          frmVDA640.optSolucionado.Enabled = False
79          frmVDA640.chkComplemento.Enabled = False
80      End If
        
81      If vObjOracle!CONTATO = "C" Then
82          frmVDA640.optCliente.Value = True
83      ElseIf vObjOracle!CONTATO = "R" Then
84          frmVDA640.optRepresentante.Value = True
85      ElseIf vObjOracle!CONTATO = "T" Then
86          frmVDA640.optTelevendas.Value = True
87      ElseIf vObjOracle!CONTATO = "O" Then
88          frmVDA640.optOutros.Value = True
89      End If
        
90      frmVDA640.txtComentarios.Text = vObjOracle!COMENTARIOS

91      Call frmVDA640.Inserir_Nome_Atendente_Descricao
    
92      frmVDA640.txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
93      frmVDA640.txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
94      frmVDA640.txtNota_Fiscal_LostFocus
95      frmVDA640.txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
96      frmVDA640.txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
        
97      If vObjOracle!FL_PROCEDE = "S" Then
98          frmVDA640.chkProcede.Value = 1
99      Else
100         frmVDA640.chkProcede.Value = 0
101     End If
    
102     If vObjOracle!FL_DEVOLUCAO = "S" Then
103         frmVDA640.chkDevolucao.Value = 1
104     Else
105         frmVDA640.chkDevolucao.Value = 0
106     End If
    
107     If vObjOracle!FL_PRORROGACAO = "S" Then
108         frmVDA640.chkProrrogacao.Value = 1
109     Else
110         frmVDA640.chkProrrogacao.Value = 0
111     End If
    
112     frmVDA640.cmbAssunto.Text = vObjOracle!DESC_ASSUNTO
        
113     If Trim(vObjOracle!cod_topico) = 0 Then
114         frmVDA640.cmbTopico.ListIndex = -1
115     Else
116         frmVDA640.cmbTopico.Text = vObjOracle!DESC_TOPICO
117     End If
        
118     If Trim(vObjOracle!cod_detalhe) = 0 Then
119         frmVDA640.cmbDetalhe.ListIndex = -1
120     Else
121         frmVDA640.cmbDetalhe.Text = vObjOracle!DESC_DETALHE
122     End If
        
123     frmVDA640.txtProcedimento.Text = vObjOracle!PROC_RECLAMACAO
124     If frmVDA640.txtRetorno.Text = 0 Then
125         frmVDA640.optRetornado.Enabled = False
126     Else
127         frmVDA640.optRetornado.Enabled = True
128     End If
        
129     frmVDA640.txtSolucao.Text = vObjOracle!DIAS_SOLUCAO
130     If frmVDA640.txtSolucao.Text = 0 Then
131         frmVDA640.optSolucionado.Enabled = False
132     Else
133         frmVDA640.optSolucionado.Enabled = True
134     End If
135     frmVDA640.txtLiberacao.Text = vObjOracle!DIAS_LIBERACAO
        
136     If vObjOracle!fl_falta_complemento = "S" Then
137         frmVDA640.chkComplemento.Value = 1
138         frmVDA640.optPendente.Enabled = False
139         frmVDA640.optRetornado.Enabled = False
140         frmVDA640.optSolucionado.Enabled = False
141         frmVDA640.optLiberado.Enabled = False
142     Else
143         frmVDA640.chkComplemento.Value = 0
144     End If
    
145     If frmVDA640.Enabled = True Then frmVDA640.txtComentarios.SetFocus
        
146     fl_Novo = "N"
        
        'Unload Me
147     Me.Visible = False
        
148     frmVDA640.cmdAgenda.Enabled = True
149     frmVDA640.cmbAssunto.Enabled = False
150     frmVDA640.txtComentarios.SetFocus
151     frmVDA640.stb.TabEnabled(1) = True
152     frmVDA640.stb.Tab = 1
        
153   Else
154     Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
    
155     frmVDA640.cmdAgenda.Enabled = False
156   End If

Trata_Erro:
157   If Err.Number <> 0 Then
158     MsgBox "Sub FrmPendencias" & vbCrLf & "mfgComplemento_Pendencia_dblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
159   End If

End Sub

Private Sub mfgPrazo_Pendencia_dblClick()
          
1         On Error GoTo Trata_Erro
          
2         frmVDA640.cmbAssunto.Visible = True
3         frmVDA640.cmbAssunto.Enabled = True
4         frmVDA640.cmbTopico.Visible = True
5         frmVDA640.cmbTopico.Enabled = True
6         frmVDA640.cmbDetalhe.Visible = True
7         frmVDA640.cmbDetalhe.Enabled = True
8         frmVDA640.cmbTopicoCompleto.Visible = False
9         frmVDA640.cmbAssuntoCompleto.Visible = False
10        frmVDA640.cmbDetalheCompleto.Visible = False
          
11        mfgPrazo_Pendencia.col = 1
          
12        If mfgPrazo_Pendencia.Text = "" Then Exit Sub
          
13        fl_Novo = "N"
          
14        mfgPrazo_Pendencia.col = 3
         '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO C�DIGO DO CLIENTE
15        vBanco.Parameters.Remove "PM_CODCLIENTE"
16        vBanco.Parameters.Add "PM_CODCLIENTE", mfgPrazo_Pendencia.Text, 1
          
17        Criar_Cursor vBanco.Parameters, "vCursor"
          
18        vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,:PM_CODCLIENTE,NULL,NULL)"
          
19        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
20            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
21            Exit Sub
22        Else
23            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
24        End If
          
25        If Not vObjOracle.EOF Then
26            frmVDA640.txtDeposito = ""
27            frmVDA640.txtNota_Fiscal = ""
28            frmVDA640.txtCodigo_Cliente.Text = mfgPrazo_Pendencia.Text
29            frmVDA640.txtRazao_Social.Text = vObjOracle!NOME_CLIENTE
30            frmVDA640.txtContato.Text = IIf(IsNull(vObjOracle!NOME_CONTATO), "", vObjOracle!NOME_CONTATO)
31            frmVDA640.txtCGC.Text = vObjOracle!CGC
32            frmVDA640.txtCidade.Text = vObjOracle!NOME_CIDADE
33            frmVDA640.txtUF.Text = vObjOracle!COD_UF
34            frmVDA640.txtCod_Representante.Text = vObjOracle!COD_REPRES
35            frmVDA640.txtNome_Representante.Text = vObjOracle!NOME_REPRES
36            frmVDA640.txtDDD1.Text = vObjOracle!DDD1
37            frmVDA640.txtTelefone1.Text = vObjOracle!FONE1
38            frmVDA640.txtDDD2.Text = IIf(IsNull(vObjOracle!DDD2), 0, vObjOracle!DDD2)
39            frmVDA640.txtTelefone2.Text = IIf(IsNull(vObjOracle!FONE2), 0, vObjOracle!FONE2)
40            frmVDA640.txtFilial.Text = vObjOracle!COD_FILIAL & " - " & vObjOracle!NOME_FILIAL
          
41            frmVDA640.cboAtendente.Tag = mfgPrazo_Pendencia.TextMatrix(mfgPrazo_Pendencia.row, 2)
42            For i = 0 To frmVDA640.cboAtendente.ListCount - 1
43                If frmVDA640.cboAtendente.List(i) = mfgPrazo_Pendencia.TextMatrix(mfgPrazo_Pendencia.row, 2) Then
44                   frmVDA640.cboAtendente = mfgPrazo_Pendencia.TextMatrix(mfgPrazo_Pendencia.row, 2)
45                End If
46            Next
47            If frmVDA640.cboAtendente = "" Then frmVDA640.cboAtendente = vAtendente
              
48            frmVDA640.lblNumAtendimento.Caption = mfgPrazo_Pendencia.TextMatrix(mfgPrazo_Pendencia.row, 0) & " - " & mfgPrazo_Pendencia.TextMatrix(mfgPrazo_Pendencia.row, 1)
                 
49        End If
          
         '-- BUSCA O TIPO_FIEL DO CLIENTE
50        vBanco.Parameters.Remove "PM_CODCLIENTE"
51        vBanco.Parameters.Add "PM_CODCLIENTE", mfgPrazo_Pendencia.Text, 1
          
52        Criar_Cursor vBanco.Parameters, "vCursor"
          
53        vSql = "Producao.PCK_VDA640.PR_BUSCA_TP_FIEL(:vCursor,:PM_CODCLIENTE)"
          
54        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
55            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TP_FIEL, entre em contato com o SUPORTE!")
56            Exit Sub
57        Else
58            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
59        End If
          
60        If Not vObjOracle.EOF Then
61            frmVDA640.txtTipo_Fiel.Text = vObjOracle!DESC_FIEL
62        End If

         '-- CARREGA OS DADOS DO ATENDIMENTO
63        mfgPrazo_Pendencia.col = 1
64        vBanco.Parameters.Remove "PM_NUMERO"
65        vBanco.Parameters.Add "PM_NUMERO", mfgPrazo_Pendencia.Text, 1
66        vNum_Atendimento = mfgPrazo_Pendencia.Text
              
67        Criar_Cursor vBanco.Parameters, "vCursor"
          
68        vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
          
69        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
70            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
71            Exit Sub
72        Else
73            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
74        End If
          
75        If Not vObjOracle.EOF Then
76            If vObjOracle!Situacao = "P" Then
77                frmVDA640.optPendente.Value = True
78            ElseIf vObjOracle!Situacao = "R" Then
79                frmVDA640.optRetornado.Value = True
80                frmVDA640.optPendente.Enabled = False
81                frmVDA640.chkComplemento.Enabled = False
82            ElseIf vObjOracle!Situacao = "S" Then
83                frmVDA640.optSolucionado.Value = True
84                frmVDA640.optPendente.Enabled = False
85                frmVDA640.optRetornado.Enabled = False
86                frmVDA640.chkComplemento.Enabled = False
87            ElseIf vObjOracle!Situacao = "L" Then
88                frmVDA640.optLiberado.Value = True
89                frmVDA640.optPendente.Enabled = False
90                frmVDA640.optRetornado.Enabled = False
91                frmVDA640.optSolucionado.Enabled = False
92                frmVDA640.chkComplemento.Enabled = False
93            End If
              
94            If vObjOracle!CONTATO = "C" Then
95                frmVDA640.optCliente.Value = True
96            ElseIf vObjOracle!CONTATO = "R" Then
97                frmVDA640.optRepresentante.Value = True
98            ElseIf vObjOracle!CONTATO = "T" Then
99                frmVDA640.optTelevendas.Value = True
100           ElseIf vObjOracle!CONTATO = "O" Then
101               frmVDA640.optOutros.Value = True
102           End If
              
103           frmVDA640.txtComentarios.Text = IIf(IsNull(vObjOracle!COMENTARIOS), "", vObjOracle!COMENTARIOS)
104           Call frmVDA640.Inserir_Nome_Atendente_Descricao
              
105           frmVDA640.txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
106           frmVDA640.txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
107           frmVDA640.txtNota_Fiscal_LostFocus
108           frmVDA640.txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
109           frmVDA640.txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
              
110           If vObjOracle!FL_PROCEDE = "S" Then
111               frmVDA640.chkProcede.Value = 1
112           Else
113               frmVDA640.chkProcede.Value = 0
114           End If
          
115           If vObjOracle!FL_DEVOLUCAO = "S" Then
116               frmVDA640.chkDevolucao.Value = 1
117           Else
118               frmVDA640.chkDevolucao.Value = 0
119           End If
          
120           If vObjOracle!FL_PRORROGACAO = "S" Then
121               frmVDA640.chkProrrogacao.Value = 1
122           Else
123               frmVDA640.chkProrrogacao.Value = 0
124           End If
          
125           frmVDA640.cmbAssunto.Text = vObjOracle!DESC_ASSUNTO
              
126           If 0 & Trim(vObjOracle!cod_topico) = 0 Then
127               frmVDA640.cmbTopico.ListIndex = -1
128           Else
129               frmVDA640.cmbTopico.Text = vObjOracle!DESC_TOPICO
130           End If
              
131           If 0 & Trim(vObjOracle!cod_detalhe) = 0 Then
132               frmVDA640.cmbDetalhe.ListIndex = -1
133           Else
134               frmVDA640.cmbDetalhe.Text = vObjOracle!DESC_DETALHE
135           End If
              
Continuar:
136           frmVDA640.txtProcedimento.Text = "" & vObjOracle!PROC_RECLAMACAO
137           frmVDA640.txtRetorno.Text = 0 & vObjOracle!DIAS_RETORNO
138           If frmVDA640.txtRetorno.Text = 0 Then
139               frmVDA640.optRetornado.Enabled = False
140           Else
141               frmVDA640.optRetornado.Enabled = True
142           End If
              
143           frmVDA640.txtSolucao.Text = 0 & vObjOracle!DIAS_SOLUCAO
144           If frmVDA640.txtSolucao.Text = 0 Then
145               frmVDA640.optSolucionado.Enabled = False
146           Else
147               frmVDA640.optSolucionado.Enabled = True
148           End If
              
149           frmVDA640.txtLiberacao.Text = 0 & vObjOracle!DIAS_LIBERACAO
              
150           If vObjOracle!fl_falta_complemento = "S" Then
151               frmVDA640.chkComplemento.Value = 1
152               frmVDA640.optPendente.Enabled = False
153               frmVDA640.optRetornado.Enabled = False
154               frmVDA640.optSolucionado.Enabled = False
155               frmVDA640.optLiberado.Enabled = False
156           Else
157               frmVDA640.chkComplemento.Value = 0
158           End If
              
159           fl_Novo = "N"
              
160           Me.Visible = False
              
161           frmVDA640.cmdAgenda.Enabled = True
162           frmVDA640.cmbAssunto.Enabled = False
163           frmVDA640.txtComentarios.SetFocus
164           frmVDA640.stb.TabEnabled(1) = True
165           frmVDA640.stb.Tab = 1
              
166       Else
167           Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
          
168           frmVDA640.cmdAgenda.Enabled = False
169       End If

Trata_Erro:
170       If Err.Number <> 0 Then
171           If Err.Number = 383 Then
                  '29/09/2006
172               frmVDA640.cmbAssuntoCompleto.width = frmVDA640.cmbAssunto.width
173               frmVDA640.cmbTopicoCompleto.width = frmVDA640.cmbTopico.width
174               frmVDA640.cmbDetalheCompleto.width = frmVDA640.cmbDetalhe.width
                      
175               frmVDA640.cmbAssunto.Visible = False
176               frmVDA640.cmbTopico.Visible = False
177               frmVDA640.cmbDetalhe.Visible = False

178               frmVDA640.cmbAssuntoCompleto.Visible = True
179               frmVDA640.cmbTopicoCompleto.Visible = True
180               frmVDA640.cmbDetalheCompleto.Visible = True
                      
181               frmVDA640.cmbAssuntoCompleto = vObjOracle!DESC_ASSUNTO
                        
182               If IsNull(vObjOracle!DESC_DETALHE) Then
183                   frmVDA640.cmbTopicoCompleto.ListIndex = -1
184               Else
185                   frmVDA640.cmbTopicoCompleto = vObjOracle!DESC_TOPICO
186               End If
                        
187               If IsNull(vObjOracle!DESC_DETALHE) Then
188                   frmVDA640.cmbDetalheCompleto.ListIndex = -1
189               Else
190                   frmVDA640.cmbDetalheCompleto = vObjOracle!DESC_DETALHE
191               End If
                  '29/09/2006
192               Err.Clear
193               GoTo Continuar
194           Else
196              MsgBox "Sub FrmPendencias" & vbCrLf & "mfgPrazo_Pendencia_dblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
198           End If
199       End If
          
End Sub

' Sort by the indicated column.
Private Sub SortByColumn(ByVal sort_column As Integer)
    ' Hide the FlexGrid.
    mfgPrazo_Pendencia.Visible = False
    mfgPrazo_Pendencia.Refresh

    ' Sort using the clicked column.
    mfgPrazo_Pendencia.col = sort_column
    mfgPrazo_Pendencia.ColSel = sort_column
    mfgPrazo_Pendencia.row = 0
    mfgPrazo_Pendencia.RowSel = 0

    ' If this is a new sort column, sort ascending.
    ' Otherwise switch which sort order we use.
    If m_SortColumn <> sort_column Then
        m_SortOrder = flexSortGenericAscending
    ElseIf m_SortOrder = flexSortGenericAscending Then
        m_SortOrder = flexSortGenericDescending
    Else
        m_SortOrder = flexSortGenericAscending
    End If
    mfgPrazo_Pendencia.Sort = m_SortOrder

    ' Restore the previous sort column's name.
    If m_SortColumn >= 0 Then
        mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn) = _
            Mid$(mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn), 3)
    End If

    ' Display the new sort column's name.
    m_SortColumn = sort_column
    If m_SortOrder = flexSortGenericAscending Then
        mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn) = "> " & _
            mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn)
    Else
        mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn) = "< " & _
            mfgPrazo_Pendencia.TextMatrix(0, m_SortColumn)
    End If

    ' Display the FlexGrid.
    mfgPrazo_Pendencia.Visible = True
End Sub

Sub Preenche_Combo_Atendente()
    
    vSql = "Producao.PCK_VDA640.Pr_Select_Atendente(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    cboAtendente.AddItem "<Nenhum>"
    
    For i = 1 To vObjOracle.RecordCount
        cboAtendente.AddItem vObjOracle.Fields(1).Value
        cboAtendente.ItemData(cboAtendente.NewIndex) = vObjOracle.Fields(0).Value
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub

Sub Exibir_Ocultar(pTipoAtendente As String, pGrid As MSFlexGrid, pColuna As Integer)
    Dim vAltura As Long
    vAltura = pGrid.RowHeight(0)
    
    If pTipoAtendente = "C" Then
        For i = 1 To pGrid.Rows - 1
            If cboAtendente <> pGrid.TextMatrix(i, pColuna) Then
                pGrid.RowHeight(i) = 0
            Else
                pGrid.RowHeight(i) = vAltura
            End If
        Next
    End If
End Sub
