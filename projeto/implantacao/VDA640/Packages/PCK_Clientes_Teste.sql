CREATE OR REPLACE PACKAGE PCK_Clientes_Teste IS

	TYPE tp_cursor IS REF CURSOR;
/*
     Procedure pr_VerCGC	(PM_CURSOR 	in out 	tp_cursor,
			      PM_OPCAO	     in	number,
			      PM_CGC 	     in 	producao.cliente.cgc%type,
                     PM_MESES      OUT  Number,
			      PM_ERRO 	     out 	number,
			      PM_MSGERRO	out	varchar);
*/
	Procedure pr_Con_UF		(PM_CURSOR	in out 	tp_cursor,
					 PM_ERRO	out 	number,
					 PM_MSGERRO	out 	varchar);

	Procedure pr_Con_Cidade		(PM_CURSOR 	in out 	tp_cursor,
					 PM_UF		in	producao.uf.cod_uf%type,
					 PM_ERRO 	out 	number,
					 PM_MSGERRO 	out 	varchar);

	Procedure pr_Con_Tp_CLiente	(PM_CURSOR 	in out 	tp_cursor,
					 PM_ERRO 	out 	number,
					 PM_MSGERRO out 	varchar);

	Procedure pr_Con_Categ_Bosch	(PM_CURSOR 	in out 	tp_cursor,
					 PM_ERRO 	out 	number,
					 PM_MSGERRO 	out 	varchar);

	Procedure pr_Con_Repres		(PM_CURSOR 	in out 	tp_cursor,
					 PM_ERRO 	out 	number,
					 PM_MSGERRO 	out 	varchar) ;

	Procedure pr_Msg_Fiscal		(PM_CURSOR 	in out 	tp_cursor,
					 PM_ERRO 	out 	number,
					 PM_MSGERRO 	out 	varchar) ;

  Procedure PR_VER_REPRES_TARE (PM_CURSOR 	in out 	tp_cursor,
					 	  PM_OPCAO	in		number,
						  PM_REPRES 	in 		producao.representante.cod_repres%type,
						  PM_ERRO 	out 	number,
                                PM_MSGERRO	out		varchar);

   Procedure PR_RET_TIPO	(PM_TIPO      in 	Number,
  					             PM_RET_TIPO 	out Number);

  Procedure PR_RET_FILIAL21 (PM_COD_REPRES  in producao.representante.cod_repres%Type,
				 	    PM_RET_FILIAL  out Number);

  Procedure PR_INSERT_PRE_CADASTRO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pDDD2               IN intranet.cliente_web.ddd2%Type,
                                    pFONE2              IN intranet.cliente_web.fone2%Type,
				    pTpEmpresa		IN intranet.cliente_web.Tp_Empresa%Type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pINSCR_SUFRAMA      IN intranet.cliente_web.inscr_suframa%Type,
                                    pCOD_TIPO_CLIENTE   IN intranet.cliente_web.cod_tipo_cliente%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pDT_FUNDACAO        IN CHAR,
                                    pPROPRIEDADE        IN intranet.cliente_web.propriedade%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_CATEG_BOSCH_DIESEL IN intranet.cliente_web.fl_categ_bosch_diesel%Type,
                                    pFL_GRUPO_MASTER       IN intranet.cliente_web.fl_grupo_master%Type,
                                    pCOD_TARE              IN intranet.cliente_web.cod_tare%Type,
                                    pDT_TARE               IN CHAR,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pE_MAIL                IN intranet.cliente_web.e_mail%Type,
                                    pHOMEPAGE              IN intranet.cliente_web.homepage%Type,
                                    pEMPRESA1_REF_COM      IN intranet.cliente_web.empresa1_ref_com%Type,
                                    pDDD1_REF_COM          IN intranet.cliente_web.ddd1_ref_com%Type,
                                    pFONE1_REF_COM         IN intranet.cliente_web.fone1_ref_com%Type,
                                    pEMPRESA2_REF_COM      IN intranet.cliente_web.empresa2_ref_com%Type,
                                    pDDD2_REF_COM          IN intranet.cliente_web.ddd2_ref_com%Type,
                                    pFONE2_REF_COM         IN intranet.cliente_web.fone2_ref_com%Type,
                                    pEMPRESA3_REF_COM      IN intranet.cliente_web.empresa3_ref_com%Type,
                                    pDDD3_REF_COM          IN intranet.cliente_web.ddd3_ref_com%Type,
                                    pFONE3_REF_COM         IN intranet.cliente_web.fone3_ref_com%Type,
                                    pEMPRESA4_REF_COM      IN intranet.cliente_web.empresa4_ref_com%Type,
                                    pDDD4_REF_COM          IN intranet.cliente_web.ddd4_ref_com%Type,
                                    pFONE4_REF_COM         IN intranet.cliente_web.fone4_ref_com%Type,
                                    pEMPRESA5_REF_COM      IN intranet.cliente_web.empresa5_ref_com%Type,
                                    pDDD5_REF_COM          IN intranet.cliente_web.ddd5_ref_com%Type,
                                    pFONE5_REF_COM         IN intranet.cliente_web.fone5_ref_com%Type,
                                    pBANCO_REF_BCO         IN intranet.cliente_web.banco_ref_bco%Type,
                                    pAGENCIA_REF_BCO       IN intranet.cliente_web.agencia_ref_bco%Type,
                                    pDDD_REF_BCO           IN intranet.cliente_web.ddd_ref_bco%Type,
                                    pFONE_REF_BCO          IN intranet.cliente_web.fone_ref_bco%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2);


  Procedure PR_INSERT_PRE_ALTERACAO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pTpEmpresa		IN intranet.cliente_web.Tp_Empresa%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pDDD2               IN intranet.cliente_web.ddd2%Type,
                                    pFONE2              IN intranet.cliente_web.fone2%Type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pINSCR_SUFRAMA      IN intranet.cliente_web.inscr_suframa%Type,
                                    pCOD_TIPO_CLIENTE   IN intranet.cliente_web.cod_tipo_cliente%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pDT_FUNDACAO        IN CHAR,
                                    pPROPRIEDADE        IN intranet.cliente_web.propriedade%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_CATEG_BOSCH_DIESEL IN intranet.cliente_web.fl_categ_bosch_diesel%Type,
                                    pCOD_TARE              IN intranet.cliente_web.cod_tare%Type,
                                    pDT_TARE               IN CHAR,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pE_MAIL                IN intranet.cliente_web.e_mail%Type,
                                    pHOMEPAGE              IN intranet.cliente_web.homepage%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2);

  Procedure PR_INSERT_PRE_RECADASTRO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pTp_Empresa		IN intranet.cliente_web.Tp_Empresa%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_GRUPO_MASTER       IN intranet.cliente_web.fl_grupo_master%Type,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pEMPRESA1_REF_COM      IN intranet.cliente_web.empresa1_ref_com%Type,
                                    pDDD1_REF_COM          IN intranet.cliente_web.ddd1_ref_com%Type,
                                    pFONE1_REF_COM         IN intranet.cliente_web.fone1_ref_com%Type,
                                    pEMPRESA2_REF_COM      IN intranet.cliente_web.empresa2_ref_com%Type,
                                    pDDD2_REF_COM          IN intranet.cliente_web.ddd2_ref_com%Type,
                                    pFONE2_REF_COM         IN intranet.cliente_web.fone2_ref_com%Type,
                                    pEMPRESA3_REF_COM      IN intranet.cliente_web.empresa3_ref_com%Type,
                                    pDDD3_REF_COM          IN intranet.cliente_web.ddd3_ref_com%Type,
                                    pFONE3_REF_COM         IN intranet.cliente_web.fone3_ref_com%Type,
                                    pEMPRESA4_REF_COM      IN intranet.cliente_web.empresa4_ref_com%Type,
                                    pDDD4_REF_COM          IN intranet.cliente_web.ddd4_ref_com%Type,
                                    pFONE4_REF_COM         IN intranet.cliente_web.fone4_ref_com%Type,
                                    pEMPRESA5_REF_COM      IN intranet.cliente_web.empresa5_ref_com%Type,
                                    pDDD5_REF_COM          IN intranet.cliente_web.ddd5_ref_com%Type,
                                    pFONE5_REF_COM         IN intranet.cliente_web.fone5_ref_com%Type,
                                    pBANCO_REF_BCO         IN intranet.cliente_web.banco_ref_bco%Type,
                                    pAGENCIA_REF_BCO       IN intranet.cliente_web.agencia_ref_bco%Type,
                                    pDDD_REF_BCO           IN intranet.cliente_web.ddd_ref_bco%Type,
                                    pFONE_REF_BCO          IN intranet.cliente_web.fone_ref_bco%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2);

  Procedure PR_SELECT_CLIENTES (cPreCad          IN OUT tp_cursor,
                                   pCodUsuario      IN Number,
                                   pSituacao        IN Number,
                                   pTipo            IN Number);

  Procedure PR_SELECT_CLIENTE_PRE_ALT_REC(cCGC  IN OUT TP_CURSOR,
                                          pCGC  IN intranet.cliente_web.Cgc%Type,
                                          pTipo IN Number,
                                          pCodUsr IN Number,
                                          pRetCli OUT Number,
                                          pMeses OUT Number,
                                          pCodErro OUT Number,
                                          pTxtErro OUT Varchar2);

  Procedure PR_SELECT_NOME_CIDADE_COB (cCidadeCob IN OUT TP_CURSOR,
                                       pCGC       IN INTRANET.CLIENTE_WEB.CGC%Type,
                                       pTipo      IN Number);

  Procedure PR_SELECT_CLIENTE_CONSULTAS(cCLIENTE        IN OUT TP_CURSOR,
                                       pCGC             IN Intranet.Cliente_Web.cgc%Type,
                                       pTp_Solicitacao  IN Number,
                                       pSituacao        IN Number);

  Procedure PR_SELECT_CLIENTE_VB_ALTERADOS (cALTERADOS       IN OUT TP_CURSOR,
                                            pCGC             IN Producao.Cliente.cgc%Type,
                                            pTIPO            IN Number);

  Procedure PR_SELECT_REF_BANCO 	(cRefBco      IN OUT TP_CURSOR,
                                 	pCOD_CLI_CGC IN Producao.Cliente.cgc%TYpe,
				   	pm_ERRO      OUT Number,
				   	pm_MSGERRO   OUT CHAR);

  Procedure PR_SELECT_REF_COML   (cRefComl     IN OUT TP_CURSOR,
  				  pCOD_CLI_CGC IN Producao.Cliente.cgc%TYpe,
				  pm_ERRO      OUT Number,
				  pm_MSGERRO   OUT CHAR);

  Procedure PR_SELECT_OUT_END   	(cOutEnd      IN OUT TP_CURSOR,
                                	 pCOD_CLI_CGC IN PRODUCAO.Cliente.cgc%TYpe,
				   	 pm_ERRO      OUT Number,
				   	 pm_MSGERRO   OUT CHAR);

PROCEDURE Pr_Form_Pend_Web		(pm_CURSOR1  	In Out Tp_Cursor,
					 pm_TP_CONS	    In Number,
					 pm_TP_SOL	    In Number,
					 pm_COD_FIL	    In Number,
					 pm_ERRO     	   Out Number,
					 pm_MSGERRO	   Out Char);


  Procedure PR_SELECT_CLIENTE_RECAD_VB_ALT (cALTERADOS       IN OUT TP_CURSOR,
                                            cALTREFBCO       IN OUT TP_CURSOR,
                                            cALTCOBRAN       IN OUT TP_CURSOR,
                                            pCGC             IN Producao.Cliente.cgc%Type);

  Procedure PR_SELECT_CLIENTE_RECAD_REFCOM (cREFCOM  IN OUT TP_CURSOR,
                                             pCGC     IN Producao.Cliente.cgc%Type);


PROCEDURE PR_ATU_PRECAD_CTRCLIWEB      (PM_CGC 		IN INTRANET.CONTROLE_CLIENTE_WEB.CGC%TYPE,
										PM_TPSOL	IN INTRANET.CONTROLE_CLIENTE_WEB.TP_SOLICITACAO%TYPE,
										PM_DT_ANALISE	IN INTRANET.CONTROLE_CLIENTE_WEB.DT_ANALISE%TYPE,
										PM_SITUACAO	IN INTRANET.CONTROLE_CLIENTE_WEB.SITUACAO%TYPE,
										PM_COD_USER	IN INTRANET.CONTROLE_CLIENTE_WEB.COD_USUARIO_CREDITO%TYPE,
										PM_ERRO	       OUT NUMBER,
										PM_MSGERRO     OUT CHAR);


PROCEDURE PR_DEL_CLIWEB		       (PM_CGC 		IN INTRANET.CONTROLE_CLIENTE_WEB.CGC%TYPE,
					PM_TPSOL	IN INTRANET.CONTROLE_CLIENTE_WEB.TP_SOLICITACAO%TYPE,
					PM_ERRO	       OUT NUMBER,
					PM_MSGERRO     OUT CHAR);


     PROCEDURE PR_GRAVA_MOTIVORECUSA	       (PM_CGC 		IN INTRANET.CLIENTE_WEB.CGC%TYPE,
					PM_TPSOL	IN INTRANET.CLIENTE_WEB.TP_SOLICITACAO%TYPE,
					PM_CODMOTIVO	IN INTRANET.CLIENTE_WEB.COD_MOTIVO_RECUSA%TYPE,
					PM_ERRO	       OUT NUMBER,
					PM_MSGERRO     OUT CHAR);

     Procedure Pr_Select_Cliente_Pre_Cadastro (PM_CURSOR1 IN OUT TP_CURSOR,
                                               PM_CGC     IN Producao.cliente.cgc%Type,
                                               PM_TPSOL   IN Number,
                                               PM_CODERRO OUT Number,
                                               PM_TxtERRO OUT Number);

    Procedure Pr_Select_Log (pm_CURSOR1 IN OUT TP_CURSOR,
                             pm_Tipo          IN Number,
                             pm_Situacao      IN Number,
                             pm_CodUsuarioWeb IN Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
                             pm_CodUsuarioCre IN Intranet.Controle_Cliente_Web.Cod_Usuario_Credito%Type,
                             pm_DTINI         IN Char,
                             pm_DTFIM         IN Char,
                             pm_CodErro       OUT Number,
                             pm_TxtErro       OUT Varchar2);

    Procedure Pr_Select_Usuarios (pm_Cursor1 IN OUT TP_CURSOR,
                                      pm_Cursor2 IN OUT TP_CURSOR);

    Procedure PR_SELECT_PRECAD_EXISTENTE(PM_CURSOR1  IN OUT TP_CURSOR,
                                            pCGC        IN intranet.cliente_web.Cgc%Type);

    Procedure Pr_Totalizacoes(pm_Cursor1 IN OUT TP_CURSOR,
                              pm_Opcao   IN Number,
                              pm_DtDe    IN Varchar2,
                              pm_DtAte   IN Varchar2);

	PROCEDURE Pr_Sel_UserIntranet		(pm_CURSOR1  	In Out Tp_Cursor,
					 	 pm_CGC	    	In Intranet.Controle_Cliente_Web.CGC%Type,
					 	 pm_TPSOL	In Intranet.Controle_Cliente_Web.Tp_Solicitacao%Type,
				 	 	 pm_ERRO     	   Out Number,
				 	 	 pm_MSGERRO	   Out Char);

	Procedure Pr_Select_Limite (pm_Cursor1 In Out Tp_Cursor,
                            pCGC       In cliente.cgc%TYpe);

	Procedure Pr_Select_Nome_Cliente (PM_CURSOR1 IN OUT TP_CURSOR,
					  PM_CGC IN cliente.cgc%Type);

	Procedure PR_CLIE_ACEITO_ENROSC		(pm_ERRO     	   Out Number,
				 	 	 pm_MSGERRO	   Out Char);

	PROCEDURE Pr_RespCli		(pm_CURSOR1  	In Out Tp_Cursor,
					 	 		 pm_CGC	    	In Intranet.Controle_Cliente_Web.CGC%Type,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char);

	PROCEDURE Pr_Responsaveis	(pm_CURSOR1  	In Out Tp_Cursor,
							 	 pm_CPO_CONS	In Varchar2,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char);

	PROCEDURE PR_TROCA_RESP_CLI	(pm_CGC			In Intranet.Controle_Cliente_Web.CGC%Type,
							 	 pm_USU_ATU	    In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 pm_USU_NOVO    In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char);

	PROCEDURE Pr_Cli_Responsavel	(pm_CURSOR1  	In Out Tp_Cursor,
							 	 	 pm_USU_ATU		In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 	 pm_TP_CONS	    In Number,
				 	 	 		 	 pm_ERRO     	Out Number,
						 	 	 	 pm_MSGERRO	    Out Char);

 	PROCEDURE Pr_Select_Repres_Tipo_R  (PM_COD_REPRES IN representante.cod_repres%Type,
                                     	PM_Resultado  OUT NUMBER);

	PROCEDURE PR_USUARIO_VISAO_CLI	(pm_CODUSU	In Intranet.Usuario_Visao_Cliente.Cod_Usuario%Type,
							 	 	 pm_CODFIL	In Intranet.Usuario_Visao_Cliente.Cod_Filial%Type,
							 	 	 pm_TP_CONS	In Varchar2,
				 	 	 		 	 pm_ERRO    Out Number,
						 	 	 	 pm_MSGERRO	Out Char);

	PROCEDURE PR_SEL_USU_VISAO		(pm_CURSOR1  	In Out Tp_Cursor,
									 pm_CODUSU	In Intranet.Usuario_Visao_Cliente.Cod_Usuario%Type,
							 	 	 pm_CODFIL	In Intranet.Usuario_Visao_Cliente.Cod_Filial%Type,
							 	 	 pm_TP_CONS	In Number,
				 	 	 		 	 pm_ERRO    Out Number,
						 	 	 	 pm_MSGERRO	Out Char);

End PCK_Clientes_Teste;
/
CREATE OR REPLACE PACKAGE BODY PCK_Clientes_Teste IS

---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR UFs EXISTENTES NO BANCO DE DADOS
---------------------------------------------------------------------------------------------------------------------
  Procedure pr_Con_UF	(PM_CURSOR 	in out 	tp_cursor,
			          PM_ERRO 	out 	number,
			          PM_MSGERRO 	out 	varchar) is
     Begin

	  PM_ERRO    	:= 0;
	  PM_MSGERRO 	:= '';

	  Open PM_CURSOR for
		SELECT *
		FROM   PRODUCAO.UF
	     ORDER BY Cod_Uf;

	Exception
	When others then
		PM_ERRO		:=sqlcode;
		PM_MSGERRO	:=sqlerrm;

  End pr_Con_UF;


---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR CIDADES DE ACORDO COM A UF ESCOLHIDA
---------------------------------------------------------------------------------------------------------------------
  Procedure pr_Con_Cidade	(PM_CURSOR 	in out 	tp_cursor,
			               PM_UF		in	producao.uf.cod_uf%type,
			               PM_ERRO 	out 	number,
			               PM_MSGERRO 	out 	varchar) is
    Begin
	  PM_ERRO    	:= 0;
	  PM_MSGERRO 	:= '';

	  Open PM_CURSOR for
		SELECT   C.Nome_Cidade, C.Cod_Cidade
		FROM     PRODUCAO.Cidade C, PRODUCAO.Uf U
		WHERE    U.Cod_Uf = C.Cod_Uf AND
                   U.Cod_Uf = PM_UF
	     ORDER BY C.Nome_Cidade;

	Exception
	When others then
		PM_ERRO		:=sqlcode;
		PM_MSGERRO	:=sqlerrm;

  End pr_Con_Cidade;

---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR TIPOS DE CLIENTE
---------------------------------------------------------------------------------------------------------------------
  Procedure pr_Con_Tp_CLiente	(PM_CURSOR 	in out 	tp_cursor,
				          PM_ERRO 	out 	number,
				          PM_MSGERRO 	out 	varchar) is
     Begin
	  PM_ERRO    	:= 0;
	  PM_MSGERRO 	:= '';

	  Open PM_CURSOR for
		SELECT Cod_Tipo_Cli, Desc_Tipo_Cli
		FROM   PRODUCAO.Tipo_Cliente
	     ORDER BY Desc_Tipo_Cli;

	Exception
	When others then
		PM_ERRO		:=sqlcode;
		PM_MSGERRO	:=sqlerrm;

End pr_Con_Tp_CLiente;


---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR CATEGORIAS BOSCH DIESEL
---------------------------------------------------------------------------------------------------------------------
  Procedure pr_Con_Categ_Bosch(PM_CURSOR 	in out 	tp_cursor,
  				           PM_ERRO 	     out 	number,
  				           PM_MSGERRO 	out 	varchar) is
     Begin

  	PM_ERRO    	:= 0;
  	PM_MSGERRO 	:= '';

  	Open PM_CURSOR for
    		SELECT *
  		  FROM VDR.V_DESCONTO_CATEG
        ORDER BY Categoria;

  	Exception
  	When others then
  		PM_ERRO		:=sqlcode;
  		PM_MSGERRO	:=sqlerrm;


  End pr_Con_Categ_Bosch;



---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR REPRESENTANTES DPK
---------------------------------------------------------------------------------------------------------------------
Procedure pr_Con_Repres	(PM_CURSOR 	in out 	tp_cursor,
			 PM_ERRO 	out 	number,
			 PM_MSGERRO out 	varchar) is
Begin

	PM_ERRO    	:= 0;
	PM_MSGERRO 	:= '';

	Open PM_CURSOR for
       Select *
       From representante a, filial b
       Where Situacao = 0
         AND a.cod_filial = b.cod_filial
         And Dt_desligamento is null
         AND (a.tipo = 'R' or a.cod_repres = 599 or
              (a.tipo in ('M','A','V') and b.tp_filial in ('P','T')))
         ORDER BY PSEUDONIMO;
/*
		SELECT *
		  FROM PRODUCAO.REPRESENTANTE
		 WHERE situacao = 0
		   AND dt_desligamento IS NULL
       AND Tipo = 'R'
       OR  COD_REPRES = 599
      	      ORDER BY Pseudonimo;
*/

	Exception
	When others then
		PM_ERRO		:=sqlcode;
		PM_MSGERRO	:=sqlerrm;

End pr_Con_Repres;


---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA SELECIONAR MENSAGEM FISCAL
---------------------------------------------------------------------------------------------------------------------
Procedure pr_Msg_Fiscal	(PM_CURSOR 	in out 	tp_cursor,
			           PM_ERRO 	     out 	number,
			           PM_MSGERRO 	out 	varchar) is
Begin

	PM_ERRO    	:= 0;
	PM_MSGERRO 	:= '';

	Open PM_CURSOR for

	  SELECT Cod_Mensagem, Desc_Mens
	    FROM Producao.Clie_Mensagem
	   WHERE Cod_Mensagem in (53, 56);

	Exception
	When others then
		PM_ERRO		:=sqlcode;
		PM_MSGERRO	:=sqlerrm;

End pr_Msg_Fiscal;


---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA VARIFICAR SE O REPRESENTANTE PERTENCE A FILIAL 26 - PARA SER TARE
---------------------------------------------------------------------------------------------------------------------
Procedure PR_VER_REPRES_TARE	(PM_CURSOR 	in out 	tp_cursor,
				 		 PM_OPCAO	     in		number,
						 PM_REPRES 	in 		producao.representante.cod_repres%type,
						 PM_ERRO 	     out 	     number,
						 PM_MSGERRO	out		varchar) is

Begin
	PM_ERRO    	:= 0;
	PM_MSGERRO 	:= '';

	If PM_OPCAO = 0 Then

		Open PM_CURSOR for
			SELECT Cod_Filial
			  FROM Producao.Representante
			 WHERE Situacao = 0
   			   AND Divisao = 'D'
			   AND Dt_Desligamento IS NULL
			   AND Cod_Repres = PM_REPRES;

	End If;

	Exception
	When others then
		PM_ERRO 	:= sqlcode;
		PM_MSGERRO 	:= sqlerrm;


End PR_VER_REPRES_TARE;


---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA RETORNAR O TIPO DO CLIENTE
---------------------------------------------------------------------------------------------------------------------
  Procedure PR_RET_TIPO	(PM_TIPO      in 	Number,
  					             PM_RET_TIPO 	out Number) is

  Begin
  		SELECT Count(Cod_Tipo_Cliente)
  		  INTO PM_RET_TIPO
  		  FROM VDR.V_Controle_Vdr
  		 WHERE Cod_Tipo_Cliente = PM_TIPO;

  	Exception
  	When others then
  		PM_RET_TIPO := 100;

  End PR_RET_TIPO;

---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA INSERIR O PRE CADASTRO
---------------------------------------------------------------------------------------------------------------------
  Procedure PR_INSERT_PRE_CADASTRO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pDDD2               IN intranet.cliente_web.ddd2%Type,
                                    pFONE2              IN intranet.cliente_web.fone2%Type,
				    pTpEmpresa		IN intranet.cliente_web.Tp_Empresa%type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pINSCR_SUFRAMA      IN intranet.cliente_web.inscr_suframa%Type,
                                    pCOD_TIPO_CLIENTE   IN intranet.cliente_web.cod_tipo_cliente%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pDT_FUNDACAO        IN CHAR,
                                    pPROPRIEDADE        IN intranet.cliente_web.propriedade%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_CATEG_BOSCH_DIESEL IN intranet.cliente_web.fl_categ_bosch_diesel%Type,
                                    pFL_GRUPO_MASTER       IN intranet.cliente_web.fl_grupo_master%Type,
                                    pCOD_TARE              IN intranet.cliente_web.cod_tare%Type,
                                    pDT_TARE               IN CHAR,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pE_MAIL                IN intranet.cliente_web.e_mail%Type,
                                    pHOMEPAGE              IN intranet.cliente_web.homepage%Type,
                                    pEMPRESA1_REF_COM      IN intranet.cliente_web.empresa1_ref_com%Type,
                                    pDDD1_REF_COM          IN intranet.cliente_web.ddd1_ref_com%Type,
                                    pFONE1_REF_COM         IN intranet.cliente_web.fone1_ref_com%Type,
                                    pEMPRESA2_REF_COM      IN intranet.cliente_web.empresa2_ref_com%Type,
                                    pDDD2_REF_COM          IN intranet.cliente_web.ddd2_ref_com%Type,
                                    pFONE2_REF_COM         IN intranet.cliente_web.fone2_ref_com%Type,
                                    pEMPRESA3_REF_COM      IN intranet.cliente_web.empresa3_ref_com%Type,
                                    pDDD3_REF_COM          IN intranet.cliente_web.ddd3_ref_com%Type,
                                    pFONE3_REF_COM         IN intranet.cliente_web.fone3_ref_com%Type,
                                    pEMPRESA4_REF_COM      IN intranet.cliente_web.empresa4_ref_com%Type,
                                    pDDD4_REF_COM          IN intranet.cliente_web.ddd4_ref_com%Type,
                                    pFONE4_REF_COM         IN intranet.cliente_web.fone4_ref_com%Type,
                                    pEMPRESA5_REF_COM      IN intranet.cliente_web.empresa5_ref_com%Type,
                                    pDDD5_REF_COM          IN intranet.cliente_web.ddd5_ref_com%Type,
                                    pFONE5_REF_COM         IN intranet.cliente_web.fone5_ref_com%Type,
                                    pBANCO_REF_BCO         IN intranet.cliente_web.banco_ref_bco%Type,
                                    pAGENCIA_REF_BCO       IN intranet.cliente_web.agencia_ref_bco%Type,
                                    pDDD_REF_BCO           IN intranet.cliente_web.ddd_ref_bco%Type,
                                    pFONE_REF_BCO          IN intranet.cliente_web.fone_ref_bco%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2) is

     vDt_Atual VARCHAR2(20);
     Begin
       Select TO_CHAR(Sysdate,'DD/MM/YY HH24:MI:SS') INTO vDt_Atual From dual;
       pCod_erro :=0;
       pTxt_erro :='';

       UPDATE INTRANET.CLIENTE_WEB
       SET NOME_CLIENTE            = pNOME_CLIENTE,
           NOME_CONTATO            = Decode(TRIM(pNOME_CONTATO),'',NULL,pNOME_CONTATO),
           ENDERECO                = Decode(TRIM(pENDERECO),'',NULL,pENDERECO),
           COD_CIDADE              = Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
           BAIRRO                  = Decode(TRIM(pBAIRRO),'',NULL,pBAIRRO),
           DDD1                    = Decode(pDDD1,0,NULL,pDDD1),
           FONE1                   = Decode(pFONE1,0,NULL,pFONE1),
           DDD2                    = Decode(pDDD2,0,NULL,pDDD2),
           FONE2                   = Decode(pFONE2,0,NULL,pFONE2),
           TP_EMPRESA		   = pTpEmpresa,
           CEP                     = Decode(pCEP,0,NULL,pCEP),
           INSCR_ESTADUAL          = Decode(TRIM(pINSCR_ESTADUAL),'',NULL,pINSCR_ESTADUAL),
           INSCR_SUFRAMA           = Decode(TRIM(pINSCR_SUFRAMA),'',NULL,pINSCR_SUFRAMA),
           COD_TIPO_CLIENTE        = Decode(pCOD_TIPO_CLIENTE,0,NULL,pCOD_TIPO_CLIENTE),
           COD_REPR_VEND           = Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
           DT_FUNDACAO             = Decode(TRIM(pDT_FUNDACAO),'',NULL,TO_DATE(pDT_FUNDACAO,'DD/MM/YY')),
           PROPRIEDADE             = Decode(TRIM(pPROPRIEDADE),'','N',pPROPRIEDADE),
           NOME_PROPRIETARIO       = Decode(TRIM(pNOME_PROPRIETARIO),'',NULL,pNOME_PROPRIETARIO),
           FL_CONS_FINAL           = Decode(TRIM(pFL_CONS_FINAL),'','N',pFL_CONS_FINAL),
           FL_CATEG_BOSCH_DIESEL   = Decode(TRIM(pFL_CATEG_BOSCH_DIESEL),'',NULL,pFL_CATEG_BOSCH_DIESEL),
           FL_GRUPO_MASTER         = Decode(TRIM(pFL_GRUPO_MASTER),'','N',pFL_GRUPO_MASTER),
           COD_TARE                = Decode(pCOD_TARE,0,NULL,pCOD_TARE),
           DT_TARE                 = Decode(TRIM(pDT_TARE),'0',NULL,TO_DATE(pDT_TARE,'DD/MM/YY')),
           COD_MENSAGEM_FISCAL     = Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
           E_MAIL                  = Decode(TRIM(pE_MAIL),'',NULL,pE_MAIL),
           HOMEPAGE                = Decode(TRIM(pHOMEPAGE),'',NULL,pHOMEPAGE),
           EMPRESA1_REF_COM        = Decode(TRIM(pEMPRESA1_REF_COM),'',NULL,pEMPRESA1_REF_COM),
           DDD1_REF_COM            = Decode(pDDD1_REF_COM,0,NULL,pDDD1_REF_COM),
           FONE1_REF_COM           = Decode(pFONE1_REF_COM,0,NULL,pFONE1_REF_COM),
           EMPRESA2_REF_COM        = Decode(TRIM(pEMPRESA2_REF_COM),'',NULL,pEMPRESA2_REF_COM),
           DDD2_REF_COM            = Decode(pDDD2_REF_COM,0,NULL,pDDD2_REF_COM),
           FONE2_REF_COM           = Decode(pFONE2_REF_COM,0,NULL,pFONE2_REF_COM),
           EMPRESA3_REF_COM        = Decode(TRIM(pEMPRESA3_REF_COM),'',NULL,pEMPRESA3_REF_COM),
           DDD3_REF_COM            = Decode(pDDD3_REF_COM,0,NULL,pDDD3_REF_COM),
           FONE3_REF_COM           = Decode(pFONE3_REF_COM,0,NULL,pFONE3_REF_COM),
           EMPRESA4_REF_COM        = Decode(TRIM(pEMPRESA4_REF_COM),'',NULL,pEMPRESA4_REF_COM),
           DDD4_REF_COM            = Decode(pDDD4_REF_COM,0,NULL,pDDD4_REF_COM),
           FONE4_REF_COM           = Decode(pFONE4_REF_COM,0,NULL,pFONE4_REF_COM),
           EMPRESA5_REF_COM        = Decode(TRIM(pEMPRESA5_REF_COM),'',NULL,pEMPRESA5_REF_COM),
           DDD5_REF_COM            = Decode(pDDD5_REF_COM,0,NULL,pDDD5_REF_COM),
           FONE5_REF_COM           = Decode(pFONE5_REF_COM,0,NULL,pFONE5_REF_COM),
           BANCO_REF_BCO           = Decode(TRIM(pBANCO_REF_BCO),'',NULL,pBANCO_REF_BCO),
           AGENCIA_REF_BCO         = Decode(pAGENCIA_REF_BCO,0,NULL,pAGENCIA_REF_BCO),
           DDD_REF_BCO             = Decode(pDDD_REF_BCO,0,NULL,pDDD_REF_BCO),
           FONE_REF_BCO            = Decode(pFONE_REF_BCO,0,NULL,pFONE_REF_BCO),
           TP_DOCTO_COB            = Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
           CGC_COB                 = Decode(pCGC_COB,0,NULL,pCGC_COB),
           NOME_CLIENTE_COB        = Decode(TRIM(pNOME_CLIENTE_COB),'',NULL,pNOME_CLIENTE_COB),
           ENDERECO_COB            = Decode(TRIM(pENDERECO_COB),'',NULL,pENDERECO_COB),
           COD_CIDADE_COB          = Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
           BAIRRO_COB              = Decode(TRIM(pBAIRRO_COB),'',NULL,pBAIRRO_COB),
           CEP_COB                 = Decode(pCEP_COB,0,NULL,pCEP_COB),
           INSCR_ESTADUAL_COB      = Decode(TRIM(pINSCR_ESTADUAL_COB),'0',NULL,pINSCR_ESTADUAL_COB),
           DT_SOLICITACAO          = TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
           COD_USUARIO             = pCOD_USUARIO,
           COD_MOTIVO_RECUSA_ANT   = DECODE(COD_MOTIVO_RECUSA,NULL,COD_MOTIVO_RECUSA_ANT
                                                             ,''  ,COD_MOTIVO_RECUSA_ANT
                                                                  ,COD_MOTIVO_RECUSA),
           COD_MOTIVO_RECUSA       = NULL
      WHERE CGC = pCGC AND
            TP_SOLICITACAO = pTP_SOLICITACAO;

      IF SQL%NOTFOUND THEN
        INSERT INTO INTRANET.CLIENTE_WEB (
                                        CGC,
                                        TP_SOLICITACAO,
                                        NOME_CLIENTE,
                                        NOME_CONTATO,
                                        ENDERECO,
                                        COD_CIDADE,
                                        BAIRRO,
                                        DDD1,
                                        FONE1,
                                        DDD2,
                                        FONE2,
                                        TP_EMPRESA,
                                        CEP,
                                        INSCR_ESTADUAL,
                                        INSCR_SUFRAMA,
                                        COD_TIPO_CLIENTE,
                                        COD_REPR_VEND,
                                        DT_FUNDACAO,
                                        PROPRIEDADE,
                                        NOME_PROPRIETARIO,
                                        FL_CONS_FINAL,
                                        FL_CATEG_BOSCH_DIESEL,
                                        FL_GRUPO_MASTER,
                                        COD_TARE,
                                        DT_TARE,
                                        COD_MENSAGEM_FISCAL,
                                        E_MAIL,
                                        HOMEPAGE,
                                        EMPRESA1_REF_COM,
                                        DDD1_REF_COM,
                                        FONE1_REF_COM,
                                        EMPRESA2_REF_COM,
                                        DDD2_REF_COM,
                                        FONE2_REF_COM,
                                        EMPRESA3_REF_COM,
                                        DDD3_REF_COM,
                                        FONE3_REF_COM,
                                        EMPRESA4_REF_COM,
                                        DDD4_REF_COM,
                                        FONE4_REF_COM,
                                        EMPRESA5_REF_COM,
                                        DDD5_REF_COM,
                                        FONE5_REF_COM,
                                        BANCO_REF_BCO,
                                        AGENCIA_REF_BCO,
                                        DDD_REF_BCO,
                                        FONE_REF_BCO,
                                        TP_DOCTO_COB,
                                        CGC_COB,
                                        NOME_CLIENTE_COB,
                                        ENDERECO_COB,
                                        COD_CIDADE_COB,
                                        BAIRRO_COB,
                                        CEP_COB,
                                        INSCR_ESTADUAL_COB,
                                        DT_SOLICITACAO,
                                        COD_USUARIO)
           VALUES(   pCGC,
                     pTP_SOLICITACAO,
                     pNOME_CLIENTE,
                     Decode(TRIM(pNOME_CONTATO),'',NULL,pNOME_CONTATO),
                     Decode(TRIM(pENDERECO),'',NULL,pENDERECO),
                     Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
                     Decode(TRIM(pBAIRRO),'',NULL,pBAIRRO),
                     Decode(pDDD1,0,NULL,pDDD1),
                     Decode(pFONE1,0,NULL,pFONE1),
                     Decode(pDDD2,0,NULL,pDDD2),
                     Decode(pFONE2,0,NULL,pFONE2),
                     pTpEmpresa,
                     Decode(pCEP,0,NULL,pCEP),
                     Decode(TRIM(pINSCR_ESTADUAL),'',NULL,pINSCR_ESTADUAL),
                     Decode(TRIM(pINSCR_SUFRAMA),'',NULL,pINSCR_SUFRAMA),
                     Decode(pCOD_TIPO_CLIENTE,0,NULL,pCOD_TIPO_CLIENTE),
                     Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
                     Decode(TRIM(pDT_FUNDACAO),'',NULL,TO_DATE(pDT_FUNDACAO,'DD/MM/YY')),
                     Decode(TRIM(pPROPRIEDADE),'','N',pPROPRIEDADE),
                     Decode(TRIM(pNOME_PROPRIETARIO),'',NULL,pNOME_PROPRIETARIO),
                     Decode(TRIM(pFL_CONS_FINAL),'','N',pFL_CONS_FINAL),
                     Decode(TRIM(pFL_CATEG_BOSCH_DIESEL),'',NULL,pFL_CATEG_BOSCH_DIESEL),
                     Decode(TRIM(pFL_GRUPO_MASTER),'','N',pFL_GRUPO_MASTER),
                     Decode(pCOD_TARE,0,NULL,pCOD_TARE),
                     Decode(TRIM(pDT_TARE),'0',NULL,TO_DATE(pDT_TARE,'DD/MM/YY')),
                     Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
                     Decode(TRIM(pE_MAIL),'',NULL,pE_MAIL),
                     Decode(TRIM(pHOMEPAGE),'',NULL,pHOMEPAGE),
                     Decode(TRIM(pEMPRESA1_REF_COM),'',NULL,pEMPRESA1_REF_COM),
                     Decode(pDDD1_REF_COM,0,NULL,pDDD1_REF_COM),
                     Decode(pFONE1_REF_COM,0,NULL,pFONE1_REF_COM),
                     Decode(TRIM(pEMPRESA2_REF_COM),'',NULL,pEMPRESA2_REF_COM),
                     Decode(pDDD2_REF_COM,0,NULL,pDDD2_REF_COM),
                     Decode(pFONE2_REF_COM,0,NULL,pFONE2_REF_COM),
                     Decode(TRIM(pEMPRESA3_REF_COM),'',NULL,pEMPRESA3_REF_COM),
                     Decode(pDDD3_REF_COM,0,NULL,pDDD3_REF_COM),
                     Decode(pFONE3_REF_COM,0,NULL,pFONE3_REF_COM),
                     Decode(TRIM(pEMPRESA4_REF_COM),'',NULL,pEMPRESA4_REF_COM),
                     Decode(pDDD4_REF_COM,0,NULL,pDDD4_REF_COM),
                     Decode(pFONE4_REF_COM,0,NULL,pFONE4_REF_COM),
                     Decode(TRIM(pEMPRESA5_REF_COM),'',NULL,pEMPRESA5_REF_COM),
                     Decode(pDDD5_REF_COM,0,NULL,pDDD5_REF_COM),
                     Decode(pFONE5_REF_COM,0,NULL,pFONE5_REF_COM),
                     Decode(TRIM(pBANCO_REF_BCO),'',NULL,pBANCO_REF_BCO),
                     Decode(pAGENCIA_REF_BCO,0,NULL,pAGENCIA_REF_BCO),
                     Decode(pDDD_REF_BCO,0,NULL,pDDD_REF_BCO),
                     Decode(pFONE_REF_BCO,0,NULL,pFONE_REF_BCO),
                     Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
                     Decode(pCGC_COB,0,NULL,pCGC_COB),
                     Decode(TRIM(pNOME_CLIENTE_COB),'',NULL,pNOME_CLIENTE_COB),
                     Decode(TRIM(pENDERECO_COB),'',NULL,pENDERECO_COB),
                     Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
                     Decode(TRIM(pBAIRRO_COB),'',NULL,pBAIRRO_COB),
                     Decode(pCEP_COB,0,NULL,pCEP_COB),
                     Decode(TRIM(pINSCR_ESTADUAL_COB),'0',NULL,pINSCR_ESTADUAL_COB),
                     TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
                     pCOD_USUARIO);
      END IF;
      commit;


      --Se o registro existir na tabela INTRANET.CONTROLE_CLIENTE_WEB
      --Ent�o dar Update senao Insert
      UPDATE INTRANET.CONTROLE_CLIENTE_WEB
      SET    COD_USUARIO    = pCOD_USUARIO,
             DT_SOLICITACAO = TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
             SITUACAO       = 0
      WHERE  CGC = pCGC AND
             TP_SOLICITACAO=pTp_Solicitacao;

      IF SQL%NOTFOUND THEN
           INSERT INTO INTRANET.CONTROLE_CLIENTE_WEB
                  (CGC,
                   COD_USUARIO,
                   TP_SOLICITACAO,
                   DT_SOLICITACAO,
                   SITUACAO)
           Values (pCGC,
                   pCOD_USUARIO,
                   pTP_SOLICITACAO,
                   TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
                   0);
      END IF;

      commit;

      Exception
         When others then
             Rollback;
             pCod_erro := sqlcode;
             pTxt_erro := sqlerrm;

  End PR_INSERT_PRE_CADASTRO;

---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA INSERIR A PRE ALTERACAO
---------------------------------------------------------------------------------------------------------------------
  Procedure PR_INSERT_PRE_ALTERACAO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pTpEmpresa		IN intranet.cliente_web.Tp_Empresa%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pDDD2               IN intranet.cliente_web.ddd2%Type,
                                    pFONE2              IN intranet.cliente_web.fone2%Type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pINSCR_SUFRAMA      IN intranet.cliente_web.inscr_suframa%Type,
                                    pCOD_TIPO_CLIENTE   IN intranet.cliente_web.cod_tipo_cliente%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pDT_FUNDACAO        IN CHAR,
                                    pPROPRIEDADE        IN intranet.cliente_web.propriedade%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_CATEG_BOSCH_DIESEL IN intranet.cliente_web.fl_categ_bosch_diesel%Type,
                                    pCOD_TARE              IN intranet.cliente_web.cod_tare%Type,
                                    pDT_TARE               IN CHAR,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pE_MAIL                IN intranet.cliente_web.e_mail%Type,
                                    pHOMEPAGE              IN intranet.cliente_web.homepage%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2) is
     vDt_Atual VARCHAR2(20);
     Begin
       Select TO_CHAR(Sysdate,'DD/MM/YY HH24:MI:SS') INTO vDt_Atual From dual;
       pCod_erro :=0;
       pTxt_erro :='';

       UPDATE INTRANET.CLIENTE_WEB
       SET    NOME_CLIENTE         = pNOME_CLIENTE,
              NOME_CONTATO         = Decode(pNOME_CONTATO,'',NULL,pNOME_CONTATO),
              Tp_Empresa	         = pTpEmpresa,
              ENDERECO             = Decode(pENDERECO,'',NULL,pENDERECO),
              COD_CIDADE           = Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
              BAIRRO               = Decode(pBAIRRO,'',NULL,pBAIRRO),
              DDD1                 = Decode(pDDD1,0,NULL,pDDD1),
              FONE1                = Decode(pFONE1,0,NULL,pFONE1),
              DDD2                 = Decode(pDDD2,0,NULL,pDDD2),
              FONE2                = Decode(pFONE2,0,NULL,pFONE2),
              CEP                  = Decode(pCEP,0,NULL,pCEP),
              INSCR_ESTADUAL       = Decode(pINSCR_ESTADUAL,'',NULL,pINSCR_ESTADUAL),
              INSCR_SUFRAMA        = Decode(pINSCR_SUFRAMA,'',NULL,pINSCR_SUFRAMA),
              COD_TIPO_CLIENTE     = Decode(pCOD_TIPO_CLIENTE,0,NULL,pCOD_TIPO_CLIENTE),
              COD_REPR_VEND        = Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
              DT_FUNDACAO          = Decode(pDT_FUNDACAO,'',NULL,TO_DATE(pDT_FUNDACAO,'DD/MM/YY')),
              PROPRIEDADE          = Decode(pPROPRIEDADE,'','N',pPROPRIEDADE),
              NOME_PROPRIETARIO    = Decode(pNOME_PROPRIETARIO,'',NULL,pNOME_PROPRIETARIO),
              FL_CONS_FINAL        = Decode(pFL_CONS_FINAL,'','N',pFL_CONS_FINAL),
              FL_CATEG_BOSCH_DIESEL= Decode(pFL_CATEG_BOSCH_DIESEL,'',NULL,pFL_CATEG_BOSCH_DIESEL),
              COD_TARE             = Decode(pCOD_TARE,0,NULL,pCOD_TARE),
              DT_TARE              = Decode(pDT_TARE,'0',NULL,TO_DATE(pDT_TARE,'DD/MM/YY')),
              COD_MENSAGEM_FISCAL  = Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
              E_MAIL               = Decode(pE_MAIL,'',NULL,pE_MAIL),
              HOMEPAGE             = Decode(pHOMEPAGE,'',NULL,pHOMEPAGE),
              TP_DOCTO_COB         = Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
              CGC_COB              = Decode(pCGC_COB,0,NULL,pCGC_COB),
              NOME_CLIENTE_COB     = Decode(pNOME_CLIENTE_COB,'',NULL,pNOME_CLIENTE_COB),
              ENDERECO_COB         = Decode(pENDERECO_COB,'',NULL,pENDERECO_COB),
              COD_CIDADE_COB       = Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
              BAIRRO_COB           = Decode(pBAIRRO_COB,'',NULL,pBAIRRO_COB),
              CEP_COB              = Decode(pCEP_COB,0,NULL,pCEP_COB),
              INSCR_ESTADUAL_COB   = Decode(pINSCR_ESTADUAL_COB,'0',NULL,pINSCR_ESTADUAL_COB),
              DT_SOLICITACAO       = TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
              COD_USUARIO          = pCOD_USUARIO
       WHERE CGC=pCGC AND TP_SOLICITACAO=1;

       IF SQL%NOTFOUND THEN
              INSERT INTO INTRANET.CLIENTE_WEB (
                                        CGC,
                                        TP_SOLICITACAO,
                                        NOME_CLIENTE,
                                        NOME_CONTATO,
                                        Tp_Empresa,
                                        ENDERECO,
                                        COD_CIDADE,
                                        BAIRRO,
                                        DDD1,
                                        FONE1,
                                        DDD2,
                                        FONE2,
                                        CEP,
                                        INSCR_ESTADUAL,
                                        INSCR_SUFRAMA,
                                        COD_TIPO_CLIENTE,
                                        COD_REPR_VEND,
                                        DT_FUNDACAO,
                                        PROPRIEDADE,
                                        NOME_PROPRIETARIO,
                                        FL_CONS_FINAL,
                                        FL_CATEG_BOSCH_DIESEL,
                                        COD_TARE,
                                        DT_TARE,
                                        COD_MENSAGEM_FISCAL,
                                        E_MAIL,
                                        HOMEPAGE,
                                        TP_DOCTO_COB,
                                        CGC_COB,
                                        NOME_CLIENTE_COB,
                                        ENDERECO_COB,
                                        COD_CIDADE_COB,
                                        BAIRRO_COB,
                                        CEP_COB,
                                        INSCR_ESTADUAL_COB,
                                        DT_SOLICITACAO,
                                        COD_USUARIO)
            VALUES(pCGC,
                   pTP_SOLICITACAO,
                   pNOME_CLIENTE,
                   Decode(pNOME_CONTATO,'',NULL,pNOME_CONTATO),
                   pTpEmpresa,
                   Decode(pENDERECO,'',NULL,pENDERECO),
                   Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
                   Decode(pBAIRRO,'',NULL,pBAIRRO),
                   Decode(pDDD1,0,NULL,pDDD1),
                   Decode(pFONE1,0,NULL,pFONE1),
                   Decode(pDDD2,0,NULL,pDDD2),
                   Decode(pFONE2,0,NULL,pFONE2),
                   Decode(pCEP,0,NULL,pCEP),
                   Decode(pINSCR_ESTADUAL,'',NULL,pINSCR_ESTADUAL),
                   Decode(pINSCR_SUFRAMA,'',NULL,pINSCR_SUFRAMA),
                   Decode(pCOD_TIPO_CLIENTE,0,NULL,pCOD_TIPO_CLIENTE),
                   Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
                   Decode(pDT_FUNDACAO,'',NULL,TO_DATE(pDT_FUNDACAO,'DD/MM/YY')),
                   Decode(pPROPRIEDADE,'','N',pPROPRIEDADE),
                   Decode(pNOME_PROPRIETARIO,'',NULL,pNOME_PROPRIETARIO),
                   Decode(pFL_CONS_FINAL,'','N',pFL_CONS_FINAL),
                   Decode(pFL_CATEG_BOSCH_DIESEL,'',NULL,pFL_CATEG_BOSCH_DIESEL),
                   Decode(pCOD_TARE,0,NULL,pCOD_TARE),
                   Decode(pDT_TARE,'0',NULL,TO_DATE(pDT_TARE,'DD/MM/YY')),
                   Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
                   Decode(pE_MAIL,'',NULL,pE_MAIL),
                   Decode(pHOMEPAGE,'',NULL,pHOMEPAGE),
                   Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
                   Decode(pCGC_COB,0,NULL,pCGC_COB),
                   Decode(pNOME_CLIENTE_COB,'',NULL,pNOME_CLIENTE_COB),
                   Decode(pENDERECO_COB,'',NULL,pENDERECO_COB),
                   Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
                   Decode(pBAIRRO_COB,'',NULL,pBAIRRO_COB),
                   Decode(pCEP_COB,0,NULL,pCEP_COB),
                   Decode(pINSCR_ESTADUAL_COB,'0',NULL,pINSCR_ESTADUAL_COB),
                   TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
                   pCOD_USUARIO);
      END IF;

      commit;

      --Se o registro existir na tabela INTRANET.CONTROLE_CLIENTE_WEB
      --Ent�o dar Update senao Insert
      UPDATE INTRANET.CONTROLE_CLIENTE_WEB
      SET    COD_USUARIO    = pCOD_USUARIO,
             DT_SOLICITACAO = TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
             SITUACAO       = 0
      WHERE  CGC = pCGC AND
             TP_SOLICITACAO=pTp_Solicitacao;

      --Inserir o registro na tabela de Controle_Cliente_Web
      if Sql%NotFound then
          INSERT INTO INTRANET.CONTROLE_CLIENTE_WEB
                      (CGC,
                       COD_USUARIO,
                       TP_SOLICITACAO,
                       DT_SOLICITACAO,
                       SITUACAO)
               Values (pCGC,
                       pCOD_USUARIO,
                       pTP_SOLICITACAO,
                       TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
                       0);
      End if;

      Commit;

      Exception
         When others then
             Rollback;
             pCod_erro := sqlcode;
             pTxt_erro := sqlerrm;

  End PR_INSERT_PRE_ALTERACAO;

---------------------------------------------------------------------------------------------------------------------
--PROCEDURE PARA INSERIR O PRE RECADASTRO
---------------------------------------------------------------------------------------------------------------------
  Procedure PR_INSERT_PRE_RECADASTRO (pCGC                IN intranet.cliente_web.cgc%Type,
                                    pTP_SOLICITACAO     IN intranet.cliente_web.tp_solicitacao%Type,
                                    pNOME_CLIENTE       IN intranet.cliente_web.nome_cliente%Type,
                                    pNOME_CONTATO       IN intranet.cliente_web.nome_contato%Type,
                                    pTp_Empresa		IN intranet.cliente_web.Tp_Empresa%Type,
                                    pENDERECO           IN intranet.cliente_web.endereco%Type,
                                    pCOD_CIDADE         IN intranet.cliente_web.cod_cidade%Type,
                                    pBAIRRO             IN intranet.cliente_web.bairro%Type,
                                    pDDD1               IN intranet.cliente_web.ddd1%Type,
                                    pFONE1              IN intranet.cliente_web.fone1%Type,
                                    pCEP                IN intranet.cliente_web.cep%Type,
                                    pINSCR_ESTADUAL     IN intranet.cliente_web.inscr_estadual%Type,
                                    pCOD_REPR_VEND      IN intranet.cliente_web.cod_repr_vend%Type,
                                    pNOME_PROPRIETARIO  IN intranet.cliente_web.nome_proprietario%Type,
                                    pFL_CONS_FINAL      IN intranet.cliente_web.fl_cons_final%Type,
                                    pFL_GRUPO_MASTER       IN intranet.cliente_web.fl_grupo_master%Type,
                                    pCOD_MENSAGEM_FISCAL   IN intranet.cliente_web.cod_mensagem_fiscal%Type,
                                    pEMPRESA1_REF_COM      IN intranet.cliente_web.empresa1_ref_com%Type,
                                    pDDD1_REF_COM          IN intranet.cliente_web.ddd1_ref_com%Type,
                                    pFONE1_REF_COM         IN intranet.cliente_web.fone1_ref_com%Type,
                                    pEMPRESA2_REF_COM      IN intranet.cliente_web.empresa2_ref_com%Type,
                                    pDDD2_REF_COM          IN intranet.cliente_web.ddd2_ref_com%Type,
                                    pFONE2_REF_COM         IN intranet.cliente_web.fone2_ref_com%Type,
                                    pEMPRESA3_REF_COM      IN intranet.cliente_web.empresa3_ref_com%Type,
                                    pDDD3_REF_COM          IN intranet.cliente_web.ddd3_ref_com%Type,
                                    pFONE3_REF_COM         IN intranet.cliente_web.fone3_ref_com%Type,
                                    pEMPRESA4_REF_COM      IN intranet.cliente_web.empresa4_ref_com%Type,
                                    pDDD4_REF_COM          IN intranet.cliente_web.ddd4_ref_com%Type,
                                    pFONE4_REF_COM         IN intranet.cliente_web.fone4_ref_com%Type,
                                    pEMPRESA5_REF_COM      IN intranet.cliente_web.empresa5_ref_com%Type,
                                    pDDD5_REF_COM          IN intranet.cliente_web.ddd5_ref_com%Type,
                                    pFONE5_REF_COM         IN intranet.cliente_web.fone5_ref_com%Type,
                                    pBANCO_REF_BCO         IN intranet.cliente_web.banco_ref_bco%Type,
                                    pAGENCIA_REF_BCO       IN intranet.cliente_web.agencia_ref_bco%Type,
                                    pDDD_REF_BCO           IN intranet.cliente_web.ddd_ref_bco%Type,
                                    pFONE_REF_BCO          IN intranet.cliente_web.fone_ref_bco%Type,
                                    pTP_DOCTO_COB          IN intranet.cliente_web.tp_docto_cob%Type,
                                    pCGC_COB               IN intranet.cliente_web.cgc_cob%Type,
                                    pNOME_CLIENTE_COB      IN intranet.cliente_web.nome_cliente_cob%Type,
                                    pENDERECO_COB          IN intranet.cliente_web.endereco_cob%Type,
                                    pCOD_CIDADE_COB        IN intranet.cliente_web.cod_cidade_cob%Type,
                                    pBAIRRO_COB            IN intranet.cliente_web.bairro_cob%Type,
                                    pCEP_COB               IN intranet.cliente_web.cep_cob%Type,
                                    pINSCR_ESTADUAL_COB    IN intranet.cliente_web.inscr_estadual_cob%Type,
                                    pCOD_USUARIO           IN intranet.cliente_web.cod_usuario%Type,
                                    pCod_Erro              OUT NUMBER,
                                    pTxt_Erro              OUT Varchar2) is

     vDt_Atual VARCHAR2(20);
     Begin
       Select TO_CHAR(Sysdate,'DD-MON-YY HH24:MI:SS') INTO vDt_Atual From dual;
       pCod_erro :=0;
       pTxt_erro :='';

       UPDATE INTRANET.CLIENTE_WEB
       SET    NOME_CLIENTE         = pNOME_CLIENTE,
              NOME_CONTATO         = Decode(pNOME_CONTATO,'',NULL,pNOME_CONTATO),
              Tp_Empresa	   = pTp_Empresa,
              ENDERECO             = Decode(pENDERECO,'',NULL,pENDERECO),
              COD_CIDADE           = Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
              BAIRRO               = Decode(pBAIRRO,'',NULL,pBAIRRO),
              DDD1                 = Decode(pDDD1,0,NULL,pDDD1),
              FONE1                = Decode(pFONE1,0,NULL,pFONE1),
              CEP                  = Decode(pCEP,0,NULL,pCEP),
              INSCR_ESTADUAL       = Decode(pINSCR_ESTADUAL,'',NULL,pINSCR_ESTADUAL),
              COD_REPR_VEND        = Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
              NOME_PROPRIETARIO    = Decode(pNOME_PROPRIETARIO,'',NULL,pNOME_PROPRIETARIO),
              FL_CONS_FINAL        = Decode(pFL_CONS_FINAL,'','N',pFL_CONS_FINAL),
              FL_GRUPO_MASTER      = Decode(pFL_GRUPO_MASTER,'','N',pFL_GRUPO_MASTER),
              COD_MENSAGEM_FISCAL  = Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
              EMPRESA1_REF_COM     = Decode(pEMPRESA1_REF_COM,'',NULL,pEMPRESA1_REF_COM),
              DDD1_REF_COM         = Decode(pDDD1_REF_COM,0,NULL,pDDD1_REF_COM),
              FONE1_REF_COM        = Decode(pFONE1_REF_COM,0,NULL,pFONE1_REF_COM),
              EMPRESA2_REF_COM     = Decode(pEMPRESA2_REF_COM,'',NULL,pEMPRESA2_REF_COM),
              DDD2_REF_COM         = Decode(pDDD2_REF_COM,0,NULL,pDDD2_REF_COM),
              FONE2_REF_COM        = Decode(pFONE2_REF_COM,0,NULL,pFONE2_REF_COM),
              EMPRESA3_REF_COM     = Decode(pEMPRESA3_REF_COM,'',NULL,pEMPRESA3_REF_COM),
              DDD3_REF_COM         = Decode(pDDD3_REF_COM,0,NULL,pDDD3_REF_COM),
              FONE3_REF_COM        = Decode(pFONE3_REF_COM,0,NULL,pFONE3_REF_COM),
              EMPRESA4_REF_COM     = Decode(pEMPRESA4_REF_COM,'',NULL,pEMPRESA4_REF_COM),
              DDD4_REF_COM         = Decode(pDDD4_REF_COM,0,NULL,pDDD4_REF_COM),
              FONE4_REF_COM        = Decode(pFONE4_REF_COM,0,NULL,pFONE4_REF_COM),
              EMPRESA5_REF_COM     = Decode(pEMPRESA5_REF_COM,'',NULL,pEMPRESA5_REF_COM),
              DDD5_REF_COM         = Decode(pDDD5_REF_COM,0,NULL,pDDD5_REF_COM),
              FONE5_REF_COM        = Decode(pFONE5_REF_COM,0,NULL,pFONE5_REF_COM),
              BANCO_REF_BCO        = Decode(pBANCO_REF_BCO,'',NULL,pBANCO_REF_BCO),
              AGENCIA_REF_BCO      = Decode(pAGENCIA_REF_BCO,0,NULL,pAGENCIA_REF_BCO),
              DDD_REF_BCO          = Decode(pDDD_REF_BCO,0,NULL,pDDD_REF_BCO),
              FONE_REF_BCO         = Decode(pFONE_REF_BCO,0,NULL,pFONE_REF_BCO),
              TP_DOCTO_COB         = Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
              CGC_COB              = Decode(pCGC_COB,0,NULL,pCGC_COB),
              NOME_CLIENTE_COB     = Decode(pNOME_CLIENTE_COB,'',NULL,pNOME_CLIENTE_COB),
              ENDERECO_COB         = Decode(pENDERECO_COB,'',NULL,pENDERECO_COB),
              COD_CIDADE_COB       = Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
              BAIRRO_COB           = Decode(pBAIRRO_COB,'',NULL,pBAIRRO_COB),
              CEP_COB              = Decode(pCEP_COB,0,NULL,pCEP_COB),
              INSCR_ESTADUAL_COB   = Decode(pINSCR_ESTADUAL_COB,'0',NULL,pINSCR_ESTADUAL_COB),
              DT_SOLICITACAO       = TO_DATE(vDT_Atual,'DD-MON-RR HH24:MI:SS'),
              COD_USUARIO          = pCOD_USUARIO
       WHERE CGC=pCGC AND TP_SOLICITACAO=2;

       IF SQL%NOTFOUND THEN
           INSERT INTO INTRANET.CLIENTE_WEB (
                                            CGC,
                                            TP_SOLICITACAO,
                                            NOME_CLIENTE,
                                            NOME_CONTATO,
                                            Tp_Empresa,
                                            ENDERECO,
                                            COD_CIDADE,
                                            BAIRRO,
                                            DDD1,
                                            FONE1,
                                            CEP,
                                            INSCR_ESTADUAL,
                                            COD_REPR_VEND,
                                            NOME_PROPRIETARIO,
                                            FL_CONS_FINAL,
                                            FL_GRUPO_MASTER,
                                            COD_MENSAGEM_FISCAL,
                                            EMPRESA1_REF_COM,
                                            DDD1_REF_COM,
                                            FONE1_REF_COM,
                                            EMPRESA2_REF_COM,
                                            DDD2_REF_COM,
                                            FONE2_REF_COM,
                                            EMPRESA3_REF_COM,
                                            DDD3_REF_COM,
                                            FONE3_REF_COM,
                                            EMPRESA4_REF_COM,
                                            DDD4_REF_COM,
                                            FONE4_REF_COM,
                                            EMPRESA5_REF_COM,
                                            DDD5_REF_COM,
                                            FONE5_REF_COM,
                                            BANCO_REF_BCO,
                                            AGENCIA_REF_BCO,
                                            DDD_REF_BCO,
                                            FONE_REF_BCO,
                                            TP_DOCTO_COB,
                                            CGC_COB,
                                            NOME_CLIENTE_COB,
                                            ENDERECO_COB,
                                            COD_CIDADE_COB,
                                            BAIRRO_COB,
                                            CEP_COB,
                                            INSCR_ESTADUAL_COB,
                                            DT_SOLICITACAO,
                                            COD_USUARIO)
        VALUES(pCGC,
               pTP_SOLICITACAO,
               pNOME_CLIENTE,
               Decode(pNOME_CONTATO,'',NULL,pNOME_CONTATO),
               pTp_Empresa,
               Decode(pENDERECO,'',NULL,pENDERECO),
               Decode(pCOD_CIDADE,0,NULL,pCOD_CIDADE),
               Decode(pBAIRRO,'',NULL,pBAIRRO),
               Decode(pDDD1,0,NULL,pDDD1),
               Decode(pFONE1,0,NULL,pFONE1),
               Decode(pCEP,0,NULL,pCEP),
               Decode(pINSCR_ESTADUAL,'',NULL,pINSCR_ESTADUAL),
               Decode(pCOD_REPR_VEND,0,NULL,pCOD_REPR_VEND),
               Decode(pNOME_PROPRIETARIO,'',NULL,pNOME_PROPRIETARIO),
               Decode(pFL_CONS_FINAL,'','N',pFL_CONS_FINAL),
               Decode(pFL_GRUPO_MASTER,'','N',pFL_GRUPO_MASTER),
               Decode(pCOD_MENSAGEM_FISCAL,0,NULL,pCOD_MENSAGEM_FISCAL),
               Decode(pEMPRESA1_REF_COM,'',NULL,pEMPRESA1_REF_COM),
               Decode(pDDD1_REF_COM,0,NULL,pDDD1_REF_COM),
               Decode(pFONE1_REF_COM,0,NULL,pFONE1_REF_COM),
               Decode(pEMPRESA2_REF_COM,'',NULL,pEMPRESA2_REF_COM),
               Decode(pDDD2_REF_COM,0,NULL,pDDD2_REF_COM),
               Decode(pFONE2_REF_COM,0,NULL,pFONE2_REF_COM),
               Decode(pEMPRESA3_REF_COM,'',NULL,pEMPRESA3_REF_COM),
               Decode(pDDD3_REF_COM,0,NULL,pDDD3_REF_COM),
               Decode(pFONE3_REF_COM,0,NULL,pFONE3_REF_COM),
               Decode(pEMPRESA4_REF_COM,'',NULL,pEMPRESA4_REF_COM),
               Decode(pDDD4_REF_COM,0,NULL,pDDD4_REF_COM),
               Decode(pFONE4_REF_COM,0,NULL,pFONE4_REF_COM),
               Decode(pEMPRESA5_REF_COM,'',NULL,pEMPRESA5_REF_COM),
               Decode(pDDD5_REF_COM,0,NULL,pDDD5_REF_COM),
               Decode(pFONE5_REF_COM,0,NULL,pFONE5_REF_COM),
               Decode(pBANCO_REF_BCO,'',NULL,pBANCO_REF_BCO),
               Decode(pAGENCIA_REF_BCO,0,NULL,pAGENCIA_REF_BCO),
               Decode(pDDD_REF_BCO,0,NULL,pDDD_REF_BCO),
               Decode(pFONE_REF_BCO,0,NULL,pFONE_REF_BCO),
               Decode(pTP_DOCTO_COB,0,NULL,pTP_DOCTO_COB),
               Decode(pCGC_COB,0,NULL,pCGC_COB),
               Decode(pNOME_CLIENTE_COB,'',NULL,pNOME_CLIENTE_COB),
               Decode(pENDERECO_COB,'',NULL,pENDERECO_COB),
               Decode(pCOD_CIDADE_COB,0,NULL,pCOD_CIDADE_COB),
               Decode(pBAIRRO_COB,'',NULL,pBAIRRO_COB),
               Decode(pCEP_COB,0,NULL,pCEP_COB),
               Decode(pINSCR_ESTADUAL_COB,'0',NULL,pINSCR_ESTADUAL_COB),
               TO_DATE(vDT_Atual,'DD-MON-YY HH24:MI:SS'),
               pCOD_USUARIO);
      END IF;

      commit;

      --Se o registro existir na tabela INTRANET.CONTROLE_CLIENTE_WEB
      --Ent�o dar Update senao Insert
      UPDATE INTRANET.CONTROLE_CLIENTE_WEB
      SET    COD_USUARIO    = pCOD_USUARIO,
             DT_SOLICITACAO = TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
             SITUACAO       = 0
      WHERE  CGC = pCGC AND
             TP_SOLICITACAO=pTp_Solicitacao;

      IF SQL%NOTFOUND THEN
          --Inserir o registro na tabela de Controle_Cliente_Web
          INSERT INTO INTRANET.CONTROLE_CLIENTE_WEB
                      (CGC,
                       COD_USUARIO,
                       TP_SOLICITACAO,
                       DT_SOLICITACAO,
                       SITUACAO)
               Values (pCGC,
                       pCOD_USUARIO,
                       pTP_SOLICITACAO,
                       TO_DATE(vDT_Atual,'DD/MM/YY HH24:MI:SS'),
                       0);
      END IF;
       Commit;

      Exception
         When others then
             Rollback;
             pCod_erro := sqlcode;
             pTxt_erro := sqlerrm;

  End PR_INSERT_PRE_RECADASTRO;

  ---------------------------------------------------------------------------------------------------------------------
  --PROCEDURE PARA RETORNAR O FILIAL DO REPRESENTANTE QUANDO UF FOR DF
  ---------------------------------------------------------------------------------------------------------------------
  Procedure PR_RET_FILIAL21 (PM_COD_REPRES  in producao.representante.cod_repres%Type,
				 	    PM_RET_FILIAL  out number) is
     Begin
       SELECT count(f.Cod_filial)
       INTO   PM_RET_FILIAL
       FROM   filial f, representante r
       WHERE  r.cod_repres = pm_cod_repres and
              f.cod_filial = r.cod_filial and
              (f.cod_filial in (21, 143, 186) or f.cod_regional in (21, 143, 186));

  End PR_RET_FILIAL21;

  --Nesta procedure deve trazer tantos os registros da tabela Auxiliar e da tabela Producao
  --Falta trazer os dados da tab da producao
  --Este select � usado para preencher as listagens
  Procedure PR_SELECT_CLIENTES (cPreCad          IN OUT tp_cursor,
                                pCodUsuario      IN Number,
                                pSituacao        IN Number,
                                pTipo            IN Number) is

     vQtd_Cod_Filial Number(5):=0;

     BEGIN

          Select Count(cod_filial)
          INTO   vQtd_Cod_Filial
          from   intranet.usuario_visao_Cliente
          where  cod_usuario = pCodUsuario;

          IF pSituacao <> 99 Then
                --Se encontrar registros na Intranet.usuario_visao_cliente ent�o
                IF vQtd_Cod_Filial >0 THEN
                   OPEN Cprecad FOR
                        Select B.CGC,
                              B.NOME_CLIENTE "Nome Cliente",
                              C.DESCRICAO "Motivo Recusa",
                              Decode(A.SITUACAO,0,'N�o Analisado',
                                         1,'Em An�lise',
                                         2,'Aprovado',
                                         9,'Recusado') Status,
                              B.TP_SOLICITACAO,
                              A.Situacao
                       From   INTRANET.CONTROLE_CLIENTE_WEB A,
                              INTRANET.CLIENTE_WEB B,
                              Credito.Motivo_Recusa C
                       Where  A.CGC = B.CGC  AND
                              B.Cod_Motivo_Recusa = C.COD_MOTIVO(+) AND
                              A.Situacao    = pSituacao   AND
                              A.TP_SOLICITACAO = pTipo AND
                              B.TP_SOLICITACAO = pTipo and
                              A.cgc in (Select distinct A.cgc
                                        from  cliente A,
                                              intranet.Controle_Cliente_Web B,
                                              r_clie_repres C,
                                              Representante D,
                                              Filial E
                                        where A.cgc = B.Cgc and
                                              A.Cod_Cliente = C.Cod_Cliente and
                                              C.Cod_Repres = D.Cod_Repres and
                                              D.Cod_Filial = E.Cod_Filial and
                                              E.Cod_Filial in (select cod_filial
                                                                 from   intranet.usuario_visao_Cliente
                                                                 where  cod_usuario = pCodUsuario))

                         UNION

                         Select B.CGC,
                                B.NOME_CLIENTE "Nome Cliente",
                                '' Motivo_Recusa,
                                Decode(A.SITUACAO,0,'N�o Analisado',
                                           1,'Em An�lise',
                                           2,'Aprovado',
                                           9,'Recusado') Status,
                                A.TP_SOLICITACAO,
                                A.SITUACAO
                         From   INTRANET.CONTROLE_CLIENTE_WEB A,
                                PRODUCAO.CLIENTE B
                         Where  A.CGC = B.CGC  AND
                                A.Situacao    = pSituacao AND
                                A.TP_SOLICITACAO = pTipo and
                                A.CGC in (Select distinct A.cgc
                                          from cliente A, intranet.Controle_Cliente_Web B,
                                               r_clie_repres C,
                                               Representante D,
                                               Filial E
                                          where A.cgc = B.Cgc and
                                                A.Cod_Cliente = C.Cod_Cliente and
                                                C.Cod_Repres = D.Cod_Repres and
                                                D.Cod_Filial = E.Cod_Filial and
                                                E.Cod_Filial in (select cod_filial
                                                                 from   intranet.usuario_visao_Cliente
                                                                 where  cod_usuario = pCodUsuario))
                         ORDER BY 2;
                ELSE

                     OPEN Cprecad FOR
                          Select B.CGC,
                                 B.NOME_CLIENTE "Nome Cliente",
                                 C.DESCRICAO "Motivo Recusa",
                                 Decode(A.SITUACAO,0,'N�o Analisado',
                                                 1,'Em An�lise',
                                                 2,'Aprovado',
                                                 9,'Recusado') Status,
                                 B.TP_SOLICITACAO,
                                 A.SITUACAO
                          from   INTRANET.CONTROLE_CLIENTE_WEB A,
                                 INTRANET.CLIENTE_WEB B,
                                 Credito.Motivo_Recusa C
                          where  A.CGC = B.CGC  AND
                                 B.Cod_Motivo_Recusa = C.COD_MOTIVO(+) AND
                                 A.Cod_usuario = pCodUsuario AND
                                 A.TP_SOLICITACAO = pTipo AND
                                 B.Tp_Solicitacao = pTipo

                          UNION

                          Select B.CGC,
                                 B.NOME_CLIENTE "Nome Cliente",
                                 '' Motivo_Recusa,
                                 Decode(A.SITUACAO,0,'N�o Analisado',
                                                     1,'Em An�lise',
                                                     2,'Aprovado',
                                                     9,'Recusado') Status,
                                 A.TP_SOLICITACAO,
                                 A.SITUACAO
                          From   INTRANET.CONTROLE_CLIENTE_WEB A,
                                 PRODUCAO.CLIENTE B
                          Where  A.CGC = B.CGC  AND
                                 A.Cod_usuario = pCodUsuario AND
                                 A.TP_SOLICITACAO = pTipo
                          ORDER BY 2;

                 END IF;

          ELSE --Tipo = 99
                 --Segunda parte
                 --Se encontrar registros na Intranet.usuario_visao_cliente ent�o
                 IF vQtd_Cod_Filial >0 THEN

                      OPEN Cprecad FOR
                           Select B.CGC,
                                  B.NOME_CLIENTE "Nome Cliente",
                                  C.DESCRICAO "Motivo Recusa",
                                  Decode(A.SITUACAO,0,'N�o Analisado',
                                         1,'Em An�lise',
                                         2,'Aprovado',
                                         9,'Recusado') Status,
                                  B.TP_SOLICITACAO,
                                  A.SITUACAO
                           From   INTRANET.CONTROLE_CLIENTE_WEB A,
                                  INTRANET.CLIENTE_WEB B,
                                  Credito.Motivo_Recusa C
                           Where  A.CGC = B.CGC  AND
                                  B.Cod_Motivo_Recusa = C.COD_MOTIVO(+) AND
                                  A.TP_SOLICITACAO = pTipo AND
                                  B.Tp_Solicitacao = pTipo AND
                                  A.cgc in (Select distinct A.cgc
                                            from cliente A, intranet.Controle_Cliente_Web B, r_clie_repres C, Representante D, Filial E
                                            where A.cgc = B.Cgc and
                                            A.Cod_Cliente = C.Cod_Cliente and
                                            C.Cod_Repres = D.Cod_Repres and
                                            D.Cod_Filial = E.Cod_Filial and
                                            E.Cod_Filial in (select cod_filial from   intranet.usuario_visao_Cliente where  cod_usuario = pCodUsuario))

                           UNION

                           Select B.CGC,
                                  B.NOME_CLIENTE "Nome Cliente",
                                  '' Motivo_Recusa,
                                  Decode(A.SITUACAO,0,'N�o Analisado',
                                               1,'Em An�lise',
                                               2,'Aprovado',
                                               9,'Recusado') Status,
                                  A.TP_SOLICITACAO,
                                  A.SITUACAO
                           From   INTRANET.CONTROLE_CLIENTE_WEB A,
                                  PRODUCAO.CLIENTE B
                           Where  A.CGC = B.CGC  AND
                                  A.TP_SOLICITACAO = pTipo AND
                                  A.cgc in (Select distinct A.cgc
                                            from cliente A, intranet.Controle_Cliente_Web B, r_clie_repres C, Representante D, Filial E
                                            where A.cgc = B.Cgc and
                                            A.Cod_Cliente = C.Cod_Cliente and
                                            C.Cod_Repres = D.Cod_Repres and
                                            D.Cod_Filial = E.Cod_Filial and
                                            E.Cod_Filial in (select cod_filial from   intranet.usuario_visao_Cliente where  cod_usuario = pCodUsuario))
                           Order by 2;
                 ELSE
                      OPEN Cprecad FOR
                          Select B.CGC,
                                 B.NOME_CLIENTE "Nome Cliente",
                                 C.DESCRICAO "Motivo Recusa",
                                 Decode(A.SITUACAO,0,'N�o Analisado',
                                               1,'Em An�lise',
                                               2,'Aprovado',
                                               9,'Recusado') Status,
                                 B.TP_SOLICITACAO,
                                 A.SITUACAO
                          from   INTRANET.CONTROLE_CLIENTE_WEB A,
                                 INTRANET.CLIENTE_WEB B,
                                 Credito.Motivo_Recusa C
                          where  A.CGC = B.CGC  AND
                                 B.Cod_Motivo_Recusa = C.COD_MOTIVO(+) AND
                                 A.Cod_usuario = pCodUsuario AND
                                 A.TP_SOLICITACAO = pTipo AND
                                 B.Tp_Solicitacao = pTipo

                          UNION

                          Select B.CGC,
                                 B.NOME_CLIENTE "Nome Cliente",
                                 '' Motivo_Recusa,
                                 Decode(A.SITUACAO,0,'N�o Analisado',
                                                   1,'Em An�lise',
                                                   2,'Aprovado',
                                                   9,'Recusado') Status,
                                 A.TP_SOLICITACAO,
                                 A.SITUACAO
                          From   INTRANET.CONTROLE_CLIENTE_WEB A,
                                 PRODUCAO.CLIENTE B
                          Where  A.CGC = B.CGC  AND
                                 A.Cod_usuario = pCodUsuario AND
                                 A.TP_SOLICITACAO = pTipo AND
                                 A.CGC NOT IN (	Select 	B.CGC
			                      	From   	INTRANET.CONTROLE_CLIENTE_WEB A,
                                 	       		INTRANET.CLIENTE_WEB B,
                                 	      	 	Credito.Motivo_Recusa C
                          			where  	A.CGC = B.CGC  AND
                                 			B.Cod_Motivo_Recusa = C.COD_MOTIVO(+) AND
                                 			A.Cod_usuario = pCodUsuario AND
                                 			A.TP_SOLICITACAO = pTipo AND
                                 			B.Tp_Solicitacao = pTipo)


                          Order by 2;
                 END IF;
      END IF;

	    Exception When others then
      	  raise_application_error(-20000,'Deu erro');


  End PR_SELECT_CLIENTES;

  --Selecionar Registros da Tabela INTRANET.CLIENTE_WEB quando da
  --abertura das paginas a partir da pagina de validacao do CGC
  --Esta procedure serve para os 3 tipos de cadastros, basta passar
  --o CGC e o Tipo como parametro.
  --Se Tipo = 0 Ent�o � Pre Cadastro
  --Se Tipo = 1 Ent�o � Pr� Alteracao
  --Se Tipo = 2 Ent�o � Pr� Recadastro
  Procedure PR_SELECT_CLIENTE_PRE_ALT_REC(cCGC     IN OUT TP_CURSOR,
                                          pCGC     IN intranet.cliente_web.Cgc%Type,
                                          pTipo    IN Number,
                                          pCodUsr  IN Number,
                                          pRetCli  OUT Number,
                                          pMeses   OUT Number,
                                          pCodErro OUT Number,
                                          pTxtErro OUT Varchar2) is
       vCountProd NUMBER(2);
       vCodUsr Number(5);
       vCountWeb Number(2);
       vCountControle Number(2);
       vMesesCadastro Number(5):=0;
       vMesesReCadastro Number(5):=0;
       vMesesUltCompra Number(5):=0;
       vDtUltCompra varchar2(11):=NULL;
       vDtRecadastro varchar2(11):=NULL;
       vDtCadastro varchar2(11):=NULL;

       BEGIN
       	    --pRetCli
       	    --Se for 0 (zero) ent�o o Cliente est� cadastrado na controle_cliente_web e n�o pertence a este usuario
       	    --Se for 1 (hum)  ent�o o Cliente est� cadastrado na Controle_cliente_web e pertence a este usu�rio.
       	    --Se for 2 (Dois) ent�o o Cliente n�o est� cadastrado na Controle_cliente_web e existe somente na producao.
            --Se for 3 (tres) ent�o este Cliente n�o existe na Producao

            --Saber se o registro existe na Controle_Cliente_web
            --Se existir saber se o Codigo do Usuario � o mesmo
            --do usuario existente na tabela Controle_Cliente_web
            --Se for o mesmo continua com o processo senao
            --Exibir mensagem informando que o representante atual
            --n�o poder� efetuar altera��es neste cliente.

            vCountProd :=0;
            vCodUsr    :=0;
            vCountWeb  :=0;
            vCountControle :=0;

  	        Select count(*)
            Into   vCountControle
            From   Controle_cliente_web
            Where  CGC = pCGC;

            IF vCountControle >0 then
  		         Select Distinct nvl(Cod_usuario,0)
  		         Into   vCodUsr
  		         From   Controle_cliente_web
  		         Where  CGC = pCGC AND TP_SOLICITACAO = pTipo;

  		         IF vCodUsr = pCodUsr then
  			          pRetCli := 1; --Este cliente pertence a este usuario

  			          --Se existir na Cliente_Web exibir os dados
  			          Select Count(cgc)
  			          Into   vCountweb
  			          From   Cliente_web
  			          Where  Cgc = pCGC
  			          AND  TP_SOLICITACAO = pTipo;

  			          IF vCountWeb >0 then
  				           --Abrir o Cursor com os dados da Cliente Web
  				           OPEN cCGC FOR
  					         SELECT  A.*,
  						               B.SITUACAO STATUS_REGISTRO,
  						               C.COD_UF,
  						               0 Situacao,
  						               'CLIENTE_WEB' Tabela
  					         FROM  	 INTRANET.CLIENTE_WEB A,
  						               Intranet.Controle_cliente_Web B,
  						               Producao.Cidade C
  					         WHERE 	 A.CGC = B.CGC AND
  						               A.COD_CIDADE = C.COD_CIDADE AND
  						               A.CGC = pCGC  AND
  						               A.TP_SOLICITACAO = pTipo AND
  						               B.TP_SOLICITACAO = pTipo;
  			          ELSE
  				            --Saber se o registro existe na Producao
  				            Select count(cgc)
                      INTO   vCountProd
  				            FROM   PRODUCAO.CLIENTE
  				            WHERE  CGC=pCGC;

  				            --Se nao Existir na Producao
  				            IF vCountProd>0 THEN
  					             --Se Existir na Producao
  					             --Tratar no asp
  					             OPEN cCGC FOR
  						                SELECT A.*,
  							                     B.SITUACAO STATUS_REGISTRO,
  							                     C.COD_UF,
  							                     D.HOMEPAGE,
  							                     D.E_MAIL,
  							                     E.*,
  							                     F.*,
  							                     G.TP_DOCTO TP_DOCTO_COB,
  							                     G.CGC CGC_COB,
  							                     G.NOME_CLIENTE NOME_CLIENTE_COB,
  							                     G.ENDERECO ENDERECO_COB,
  							                     G.COD_CIDADE COD_CIDADE_COB,
  							                     G.BAIRRO BAIRRO_COB,
  							                     G.CEP CEP_COB,
  							                     G.INSCR_ESTADUAL INSCR_ESTADUAL_COB,
  							                     H.CATEGORIA FL_CATEG_BOSCH_DIESEL,
  							                     'CLIENTE' Tabela
          			          	FROM   	 PRODUCAO.CLIENTE A,
          				               	   intranet.Controle_cliente_Web B,
          				               	   Producao.Cidade C,
          				               	   MARKETING.cliente_internet D,
          				               	   PRODUCAO.Clie_Refer E,
          				               	   PRODUCAO.Clie_Refbco F,
          				               	   PRODUCAO.Clie_Endereco G,
          				               	   VDR.CLIENTE_CATEG_VDR H
          			          	WHERE  	 A.CGC = pCGC AND
          				               	   A.CGC = B.CGC AND
          				               	   A.COD_CIDADE = C.COD_CIDADE AND
          				               	   A.COD_CLIENTE = D.COD_CLIENTE(+) AND
          				               	   A.COD_CLIENTE = E.COD_CLIENTE(+) AND
          				               	   A.COD_CLIENTE = F.COD_CLIENTE(+) AND
          				               	   A.COD_CLIENTE = G.Cod_Cliente(+) AND
          				               	   A.COD_CLIENTE = H.COD_CLIENTE(+);
  				            ELSE
  					              pRetCli := 0; --Problema pois o cliente deveria existir ou na Cliente_web ou na Producao para esta situacao
  					              OPEN cCGC FOR
  						                 Select Dummy From Dual;
  				            END IF;
  			          END IF;
  		         ELSE
  			          --26/01/2005

                  Select count(cgc)
  			          INTO   vCountProd
  			          FROM   PRODUCAO.CLIENTE
  			          WHERE  CGC=pCGC;

                  IF vCountProd >0 then
    		             SELECT
                         NVL(Months_Between(TO_DATE(Tab_2.DT_HOJE,'DD/MM/YYYY'),TO_DATE(Tab_1.DT_CADASTRO,'DD/MM/YYYY')),0),
                         NVL(Months_Between(TO_DATE(Tab_2.DT_HOJE,'DD/MM/YYYY'),TO_DATE(Tab_1.DT_ULT_COMPRA,'DD/MM/YYYY')),0),
                         NVL(Months_Between(TO_DATE(Tab_2.DT_HOJE,'DD/MM/YYYY'),TO_DATE(Tab_1.DT_RECADASTRO,'DD/MM/YYYY')),0),
                         Tab_1.DT_ULT_COMPRA DtUltimaCompra,
                         Tab_1.Dt_ReCadastro DtReCadastro,
                         Tab_1.Dt_Cadastro DtCadastro
    		             INTO  vMesesCadastro, vMesesUltCompra, vMesesReCadastro, vDtUltCompra, vDtRecadastro, vDtCadastro
    		             FROM
    			             (SELECT To_Char(A.DT_CADASTR,'DD/MM/YYYY') Dt_Cadastro,
                               To_Char(B.DT_ULT_COMPRA,'DD/MM/YYYY') Dt_Ult_Compra,
                               to_char(A.Dt_Recadastro,'DD/MM/YYYY') Dt_ReCadastro
    			              FROM   PRODUCAO.Cliente A, Producao.Clie_Credito B
    			              WHERE  	A.COD_CLIENTE = B.COD_CLIENTE AND
                     	          A.CGC = pCGC) Tab_1,
                       (Select TO_CHAR(Sysdate,'DD/MM/YYYY') DT_HOJE From dual) Tab_2;
                  END IF;                 
  
    		          IF vDtReCadastro IS NULL then
    				         IF vDtCadastro is not null then
    					          IF vMesesCadastro >6 AND vMesesUltCompra >6 OR vDtUltCompra is NULL THEN
    						           pRetCli := 1;
            		           --Se nao Existir na Producao
            		           IF vCountProd>0 THEN
            			            --neste momento o cod_usuario deste cliente passar� a ser o mesmo do cliente que est� solicitando a altera��o ou Recadastro.
            			            --pois o mesmo estava em branco
            			            pRetCli := 1; --Este cliente pertence a este usuario
            			          
                              --Se Existir na Producao
            			            --Tratar no asp
            			            OPEN cCGC FOR
            				               SELECT  A.*,
                    					             0 STATUS_REGISTRO,
                    					             C.COD_UF,
                    					             D.HOMEPAGE,
                    					             D.E_MAIL,
                    					             E.*,
                    					             F.*,
                    					             G.TP_DOCTO TP_DOCTO_COB,
                    					             G.CGC CGC_COB,
                    					             G.NOME_CLIENTE NOME_CLIENTE_COB,
                    					             G.ENDERECO ENDERECO_COB,
                    					             G.COD_CIDADE COD_CIDADE_COB,
                    					             G.BAIRRO BAIRRO_COB,
                    					             G.CEP CEP_COB,
                    					             G.INSCR_ESTADUAL INSCR_ESTADUAL_COB,
                    					             H.CATEGORIA FL_CATEG_BOSCH_DIESEL,
                    					             'CLIENTE' Tabela
            				               FROM    PRODUCAO.CLIENTE A,
            					                     --intranet.Controle_cliente_Web B,
            					                     Producao.Cidade C,
            					                     MARKETING.cliente_internet D,
            					                     PRODUCAO.Clie_Refer E,
            					                     PRODUCAO.Clie_Refbco F,
            					                     PRODUCAO.Clie_Endereco G,
            					                     VDR.CLIENTE_CATEG_VDR H
            				             WHERE  	 A.CGC = pCGC AND
                                           --A.CGC = B.CGC AND
            					                     A.COD_CIDADE = C.COD_CIDADE AND
            					                     A.COD_CLIENTE = D.COD_CLIENTE(+) AND
            					                     A.COD_CLIENTE = E.COD_CLIENTE(+) AND
            					                     A.COD_CLIENTE = F.COD_CLIENTE(+) AND
            					                     A.COD_CLIENTE = G.Cod_Cliente(+) AND
            					                     A.COD_CLIENTE = H.COD_CLIENTE(+);
                           END IF;
  					            ELSE
                        	 pRetCli:=2;--Este cliente n�o pertence a este usuario
  						             OPEN cCGC FOR
  						             	    Select 'X' From Dual;
  					            END IF;
  				           END IF;
  			          ELSE
  				           IF vDtReCadastro IS NOT NULL THEN
  					            IF vMesesRecadastro >6 AND vMesesUltCompra >6	OR vDtUltCompra is NULL THEN
  						             pRetCli := 1;
          		             --Se nao Existir na Producao
          		             IF vCountProd>0 THEN
          			              -- neste momento o cod_usuario deste cliente passar� a ser o mesmo do cliente que est� solicitando a altera��o ou Recadastro.
          			              -- pois o mesmo estava em branco
          			              pRetCli := 1; --Este cliente pertence a este usuario
          			              --Se Existir na Producao
          			              --Tratar no asp
          			              OPEN cCGC FOR
          				                 SELECT A.*,
                                					0 STATUS_REGISTRO,
                                					C.COD_UF,
                                					D.HOMEPAGE,
                                					D.E_MAIL,
                                					E.*,
                                					F.*,
                                					G.TP_DOCTO TP_DOCTO_COB,
                                					G.CGC CGC_COB,
                                					G.NOME_CLIENTE NOME_CLIENTE_COB,
                                					G.ENDERECO ENDERECO_COB,
                                					G.COD_CIDADE COD_CIDADE_COB,
                                					G.BAIRRO BAIRRO_COB,
                                					G.CEP CEP_COB,
                                					G.INSCR_ESTADUAL INSCR_ESTADUAL_COB,
                                					H.CATEGORIA FL_CATEG_BOSCH_DIESEL,
                                					'CLIENTE' Tabela
                                	 FROM   PRODUCAO.CLIENTE A,
                                					--intranet.Controle_cliente_Web B,
                                					Producao.Cidade C,
                                					MARKETING.cliente_internet D,
                                					PRODUCAO.Clie_Refer E,
                                					PRODUCAO.Clie_Refbco F,
                                					PRODUCAO.Clie_Endereco G,
                                					VDR.CLIENTE_CATEG_VDR H
                                	WHERE  	A.CGC = pCGC AND
                                					--A.CGC = B.CGC AND
                                					A.COD_CIDADE = C.COD_CIDADE AND
                                					A.COD_CLIENTE = D.COD_CLIENTE(+) AND
                                					A.COD_CLIENTE = E.COD_CLIENTE(+) AND
                                					A.COD_CLIENTE = F.COD_CLIENTE(+) AND
                                					A.COD_CLIENTE = G.Cod_Cliente(+) AND
                                					A.COD_CLIENTE = H.COD_CLIENTE(+);
                           END IF;
  					            ELSE
  						             pRetCli := 2;
                           OPEN cCGC FOR
                                Select Dummy From Dual;
  					            END IF;
  				           END IF;
  			          END IF;
  			          
                  pRetCli := 2; --Este cliente n�o pertence a este usuario
  			          OPEN cCGC FOR
  			               Select Dummy From Dual;
  		         END IF;
  	        ELSE
  		         --Saber se o registro existe na Producao
  		         Select count(cgc)
  		         INTO   vCountProd
  		         FROM   PRODUCAO.CLIENTE
  		         WHERE  CGC=pCGC;

  		         --Se nao Existir na Producao
  		         IF vCountProd>0 THEN
  			          -- neste momento o cod_usuario deste cliente passar� a ser o mesmo do cliente que est� solicitando a altera��o ou Recadastro.
  			          -- pois o mesmo estava em branco     
  			          pRetCli := 1; --Este cliente pertence a este usuario

  			          --Se Existir na Producao
  			          --Tratar no asp
  			          OPEN cCGC FOR
  				             SELECT A.*,
  					                  0 STATUS_REGISTRO,
  					                  C.COD_UF,
  					                  D.HOMEPAGE,
  					                  D.E_MAIL,
  					                  E.*,
  					                  F.*,
  					                  G.TP_DOCTO TP_DOCTO_COB,
  					                  G.CGC CGC_COB,
  					                  G.NOME_CLIENTE NOME_CLIENTE_COB,
  					                  G.ENDERECO ENDERECO_COB,
  					                  G.COD_CIDADE COD_CIDADE_COB,
  					                  G.BAIRRO BAIRRO_COB,
  					                  G.CEP CEP_COB,
  					                  G.INSCR_ESTADUAL INSCR_ESTADUAL_COB,
  					                  H.CATEGORIA FL_CATEG_BOSCH_DIESEL,
  					                  'CLIENTE' Tabela
  				             FROM 	PRODUCAO.CLIENTE A,
  					                  --intranet.Controle_cliente_Web B,
  					                  Producao.Cidade C,
  					                  MARKETING.cliente_internet D,
  					                  PRODUCAO.Clie_Refer E,
  					                  PRODUCAO.Clie_Refbco F,
  					                  PRODUCAO.Clie_Endereco G,
  					                  VDR.CLIENTE_CATEG_VDR H
  				             WHERE  A.CGC = pCGC AND
  					                  --A.CGC = B.CGC AND
  					                  A.COD_CIDADE = C.COD_CIDADE AND
  					                  A.COD_CLIENTE = D.COD_CLIENTE(+) AND
  					                  A.COD_CLIENTE = E.COD_CLIENTE(+) AND
  					                  A.COD_CLIENTE = F.COD_CLIENTE(+) AND
  					                  A.COD_CLIENTE = G.Cod_Cliente(+) AND
  					                  A.COD_CLIENTE = H.COD_CLIENTE(+);
  		         ELSE
  			           --Este cliente n�o existe
  			           pRetCli :=3; --Cliente novo s� pode para Pre Cadastro

  			           OPEN cCGC FOR
  				              Select Dummy From Dual;
  		         END IF;
  	        END IF;

  	        --Se o Tipo for 1 ou 2 - PRE ALTERACAO / PRE Recadastro e tiver na tabela Producao.Cliente
  	        IF (pTipo=1 or pTipo=2) and pRetCli=1 THEN
  		         SELECT NVL(Months_Between(TO_DATE(Tab_2.DT_HOJE,'DD/MM/YYYY'),TO_DATE(Tab_1.DT_ULT_COMPRA,'DD/MM/YYYY')),0)
  		         INTO   pMeses
  		         FROM
  			           (SELECT To_Char(B.DT_ULT_COMPRA,'DD/MM/YYYY') Dt_Ult_Compra
  			           FROM   	PRODUCAO.Cliente A, Producao.Clie_Credito B       
  			           WHERE  	A.COD_CLIENTE = B.COD_CLIENTE AND
                           	A.CGC = pCGC) Tab_1,
                   (Select TO_CHAR(Sysdate,'DD/MM/YYYY') DT_HOJE From dual) Tab_2;
  	        ELSE
  		          pMeses :=0;
  	        END IF;
    
End PR_SELECT_CLIENTE_PRE_ALT_REC;


  --Selecionar os Pre ReCadastro j� existente na tabela Cliente_Web
  --Quando da abertura de um Pre Recadastro
  Procedure PR_SELECT_PRECAD_EXISTENTE(PM_CURSOR1  IN OUT TP_CURSOR,
                                          pCGC        IN intranet.cliente_web.Cgc%Type) is
     BEGIN
        OPEN PM_CURSOR1 FOR
             SELECT Distinct A.*,
                  B.SITUACAO STATUS_REGISTRO,
                  C.COD_UF,
                  0 Situacao,
                  E.DT_CONSULTA DT_CONSULTA,
                  E.CONCEITO_EMPRESA CONCEITO_EMPRESA,
                  E.OBSERVACAO
           FROM   INTRANET.CLIENTE_WEB A,
                  Intranet.Controle_cliente_Web B,
                  Producao.Cidade C,
                  Producao.Cliente D,
                  Producao.Clie_Refer E
           WHERE  A.CGC = B.CGC AND
                  A.COD_CIDADE = C.COD_CIDADE AND
                  A.CGC = D.CGC AND
                  D.COD_CLIENTE = E.COD_CLIENTE AND
                  A.CGC = pCGC  AND
                  A.TP_SOLICITACAO = 2 AND
                  B.TP_SOLICITACAO = 2;
           /*SELECT A.*,
                  B.SITUACAO STATUS_REGISTRO,
                  C.COD_UF,
                  0 Situacao
           FROM   INTRANET.CLIENTE_WEB A,
                  Intranet.Controle_cliente_Web B,
                  Producao.Cidade C
           WHERE  A.CGC = B.CGC AND
                  A.COD_CIDADE = C.COD_CIDADE AND
                  A.CGC = pCGC  AND
                  A.TP_SOLICITACAO = 2 AND
                  B.TP_SOLICITACAO = 2;*/
  End PR_SELECT_PRECAD_EXISTENTE;


  Procedure PR_SELECT_NOME_CIDADE_COB (cCidadeCob IN OUT TP_CURSOR,
                                       pCGC       IN INTRANET.CLIENTE_WEB.CGC%Type,
                                       pTipo      IN Number) is
     BEGIN
       OPEN cCidadeCob FOR
         SELECT B.Cod_Cidade_Cob,
                A.NOME_CIDADE,
                A.COD_UF
         FROM   PRODUCAO.CIDADE A,
                INTRANET.CLIENTE_WEB B
         WHERE  B.Cod_Cidade_Cob = A.Cod_Cidade AND
                B.CGC = pCGC AND
                B.TP_SOLICITACAO = pTipo;

  END PR_SELECT_NOME_CIDADE_COB;

  --Retornar os registros que existirem na Tabela INTRANET.Cliente_WEB
  --com o CGC passado como parametro e o campo SITUACAO=0 (Zero)
  --este campo igual a Zero significa que o registro ainda n�o foi visto
  --pelo pessoal do Credito.
  Procedure PR_SELECT_CLIENTE_CONSULTAS(cCLIENTE        IN OUT TP_CURSOR,
                                       pCGC             IN Intranet.Cliente_Web.cgc%Type,
                                       pTp_Solicitacao  IN Number,
                                       pSituacao        IN Number) is
     vCount Number(5);
     BEGIN
        --Verificar se o Registro est� na Cliente WEB
--        IF pTp_Solicitacao=0 Then
           --Contar os registros existentes na Tabela INTRANET.CLIENTE_WEB
           SELECT COUNT(A.CGC)
           INTO   vCount
           FROM   INTRANET.CLIENTE_WEB A,
                  Controle_Cliente_Web B
           WHERE  A.CGC = B.CGC(+) AND
                  B.TP_SOLICITACAO = Ptp_Solicitacao AND
                  A.TP_SOLICITACAO = pTp_Solicitacao AND
                  A.CGC            = pCGC AND
                  B.Situacao       = pSituacao;

           IF vCount>0 THEN
              OPEN cCLIENTE FOR
                 SELECT DISTINCT A.*,
                        B.Situacao,
                        C.COD_UF,
                        C.NOME_CIDADE,
                        D.NOME_REPRES,
                        E.DESC_TIPO_CLI,
                        F.DESC_MENS || ' - ' || F.COD_MENSAGEM MENSAGEM_FISCAL,
                        G.NOME_CIDADE_COB,
                        G.COD_UF_COB,
                        'CLIENTE_WEB' Tabela
                 FROM   INTRANET.CLIENTE_WEB A,
                        Controle_Cliente_Web B,
                        PRODUCAO.CIDADE C,
                        PRODUCAO.REPRESENTANTE D,
                        PRODUCAO.TIPO_CLIENTE E,
                        PRODUCAO.CLIE_MENSAGEM F,
                        (SELECT X.NOME_CIDADE NOME_CIDADE_COB, X.COD_UF COD_UF_COB
                         FROM   PRODUCAO.CIDADE X, INTRANET.CLIENTE_WEB Y
                         WHERE  X.COD_CIDADE(+) = Y.COD_CIDADE_COB AND
                                Y.TP_SOLICITACAO = pTp_Solicitacao AND
                                Y.CGC = pCGC) G
                 WHERE  A.CGC = B.CGC(+) AND
                        A.TP_SOLICITACAO = pTp_Solicitacao AND
                        A.CGC = pCGC AND
                        B.Situacao = pSituacao AND
                        A.COD_CIDADE = C.COD_CIDADE(+) AND
                        A.COD_REPR_VEND = D.COD_REPRES(+) AND
                        A.COD_TIPO_CLIENTE = E.COD_TIPO_CLI(+) AND
                        A.Cod_Mensagem_Fiscal = F.Cod_Mensagem(+);
           ELSE
              OPEN cCLIENTE FOR
               SELECT A.*,
                      C.NOME_CIDADE,
                      C.COD_UF,
                      D.HOMEPAGE,
                      D.E_MAIL,
                      D.*,
                      E.TP_DOCTO TP_DOCTO_COB,
                      E.CGC CGC_COB,
                      E.NOME_CLIENTE NOME_CLIENTE_COB,
                      E.ENDERECO ENDERECO_COB,
                      E.COD_CIDADE COD_CIDADE_COB,
                      E.BAIRRO BAIRRO_COB,
                      E.CEP CEP_COB,
                      E.INSCR_ESTADUAL INSCR_ESTADUAL_COB,
                      F.CATEGORIA FL_CATEG_BOSCH_DIESEL,
                      G.Nome_Repres,
                      H.DESC_TIPO_CLI,
                      I.DESC_MENS || ' - ' || I.COD_MENSAGEM MENSAGEM_FISCAL,
                      'CLIENTE' Tabela
               FROM   PRODUCAO.CLIENTE A,
                      intranet.Controle_cliente_Web B,
                      Producao.Cidade C,
                      MARKETING.cliente_internet D,
                      PRODUCAO.Clie_Endereco E,
                      VDR.CLIENTE_CATEG_VDR F,
                      PRODUCAO.REPRESENTANTE G,
                      PRODUCAO.TIPO_CLIENTE H,
                      PRODUCAO.CLIE_MENSAGEM I
               WHERE  A.CGC = B.CGC AND
                      A.COD_CIDADE = C.COD_CIDADE AND
                      A.COD_CLIENTE = D.COD_CLIENTE(+) AND
                      A.COD_CLIENTE = E.COD_CLIENTE(+) AND
                      A.COD_CLIENTE = F.COD_CLIENTE(+) AND
                      A.Cod_Repr_Vend = G.COD_REPRES(+) AND
                      A.Cod_Tipo_Cliente = H.COD_TIPO_CLI AND
                      A.Cod_Mensagem_Fiscal = I.Cod_Mensagem(+) AND
                      A.CGC = pCGC AND
                      B.TP_SOLICITACAO = pTP_Solicitacao;
          END IF;
 --      END IF;

  End PR_SELECT_CLIENTE_CONSULTAS;


  --Selecionar os campos da Producao que sofreram alteracoes
  Procedure PR_SELECT_CLIENTE_VB_ALTERADOS (cALTERADOS       IN OUT TP_CURSOR,
                                            pCGC             IN Producao.Cliente.cgc%Type,
                                            pTIPO            IN Number) is
     Begin
       OPEN cALTERADOS FOR
            SELECT
                  Nome_Cliente "Nome Cliente",
                  CGC,
                  Endereco Endere�o,
                  Bairro Bairro,
                  CEP Cep,
                  B.Cod_Uf "C�digo UF",
                  A.COD_CIDADE "C�digo Cidade",
                  B.Nome_Cidade "Nome Cidade",
                  DDD1 "DDD 1",
                  FONE1 "Fone 1",
                  DDD2 "DDD 2",
                  FONE2 "Fone 2",
                  Nome_Contato "Nome Contato",
                  HOMEPAGE "Home Page",
                  E_MAIL "E-Mail",
                  COD_TARE "C�digo Tare",
                  DT_TARE "Data Tare",
                  COD_TIPO_CLIENTE "C�digo Tipo Cliente",
                  FL_CATEG_BOSCH_DIESEL "Categoria Bosch Diesel",
                  COD_REPR_VEND "C�digo Representante",
                  NOME_PROPRIETARIO "Nome Propriet�rio",
                  PROPRIEDADE "Propriedade",
                  DT_FUNDACAO "Data Funda��o",
                  INSCR_SUFRAMA "Inscri��o Suframa",
                  INSCR_ESTADUAL "Inscri��o Estadual",
                  TP_DOCTO_COB "Tipo do Docto de Cobran�a",
                  CGC_COB "CGC Cobran�a",
                  INSCR_ESTADUAL_COB "Inscri��o Estadual Cobran�a",
                  NOME_CLIENTE_COB "Nome Cliente Cobran�a",
                  COD_CIDADE_COB "C�digo Cidade Cobran�a",
                  ENDERECO_COB "Endere�o Cobran�a",
                  BAIRRO_COB "Bairro Cobran�a",
                  CEP_COB "Cep Cobran�a",
                  COD_MENSAGEM_FISCAL "C�digo Mensagem Fiscal",
      			  TP_EMPRESA "TP_EMPRESA",
                  B.COD_UF "C�digo UF Cobran�a",
                  Nvl(a.Fl_Cons_Final,'') "Consumidor Final"
            from  Intranet.cliente_web A,
                  Producao.cidade B
            where CGC = pCGC AND
                  A.TP_SOLICITACAO = pTIPO AND
                  A.Cod_Cidade = B.Cod_Cidade;

  End PR_SELECT_CLIENTE_VB_ALTERADOS;


--------------------------------------------------------------------------------------------------
-- PROCEDURE PARA TRAZER INFOR. REFERENCIAS BANCARIAS DA TABELA INTRANET (CLIENTE_WEB)
--------------------------------------------------------------------------------------------------
  Procedure PR_SELECT_REF_BANCO 	(cRefBco      IN OUT TP_CURSOR,
                                 	pCOD_CLI_CGC IN Producao.Cliente.cgc%TYpe,
				   	pm_ERRO      OUT Number,
				   	pm_MSGERRO   OUT CHAR) is
     BEGIN

	pm_ERRO := 0;
	pm_MSGERRO := '';

           OPEN cRefBco FOR
                select banco_Ref_bco,
                       Agencia_ref_bco,
                       DDD_REF_BCO,
                       FONE_REF_BCO
                from   cliente_web
                where  CGC = pCOD_CLI_CGC;

	Exception When No_Data_Found Then Open cRefBco for
	Select Dummy From Dual;
	When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

  End PR_SELECT_REF_BANCO;


--------------------------------------------------------------------------------------------------
-- PROCEDURE PARA TRAZER INFOR. REFERENCIAS COMERCIAS DA TABELA INTRANET (CLIENTE_WEB)
--------------------------------------------------------------------------------------------------
	Procedure PR_SELECT_REF_COML   (cRefComl     IN OUT TP_CURSOR,
					pCOD_CLI_CGC IN Producao.Cliente.cgc%TYpe,
				   	pm_ERRO      OUT Number,
				   	pm_MSGERRO   OUT CHAR) is
	BEGIN

	pm_ERRO := 0;
	pm_MSGERRO := '';

	   OPEN cRefComl FOR
		select Empresa1_Ref_Com, DDD1_Ref_Com, Fone1_Ref_Com,
		       Empresa2_Ref_Com, DDD2_Ref_Com, Fone2_Ref_Com,
		       Empresa3_Ref_Com, DDD3_Ref_Com, Fone3_Ref_Com,
		       Empresa4_Ref_Com, DDD4_Ref_Com, Fone4_Ref_Com,
		       Empresa5_Ref_Com, DDD5_Ref_Com, Fone5_Ref_Com
		 from  cliente_web
		where  CGC = pCOD_CLI_CGC;

	Exception When No_Data_Found Then Open cRefComl for
	Select Dummy From Dual;
	When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End PR_SELECT_REF_COML;


--------------------------------------------------------------------------------------------------
-- PROCEDURE PARA TRAZER INFOR. OUTROS ENDERE�OS DA TABELA INTRANET (CLIENTE_WEB)
--------------------------------------------------------------------------------------------------
  Procedure PR_SELECT_OUT_END   	(cOutEnd      IN OUT TP_CURSOR,
                                	 pCOD_CLI_CGC IN PRODUCAO.Cliente.cgc%TYpe,
				   	 pm_ERRO      OUT Number,
				   	 pm_MSGERRO   OUT CHAR) is
     BEGIN

	pm_ERRO := 0;
	pm_MSGERRO := '';

           OPEN cOutEnd FOR
                select C.Tp_Docto_Cob, C.CGC_Cob, C.Inscr_Estadual_Cob, C.Nome_Cliente_Cob,
                       C.Endereco_Cob, C.Bairro_Cob, C.Cep_Cob,
                       C.Cod_Cidade_Cob, CI.NOME_CIDADE, CI.COD_UF
                 from  cliente_web C, CIDADE CI
                where  C.CGC = pCOD_CLI_CGC
                  and  C.Cod_Cidade = CI.Cod_Cidade;

	Exception When No_Data_Found Then Open cOutEnd for
	Select Dummy From Dual;
	When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

  End PR_SELECT_OUT_END;


--------------------------------------------------------------------------------------------------
-- PROCEDURE PARA TRAZER INFOR. B�SICAS DOS FORMUL�RIOS A SEREM ANALISADOS
--------------------------------------------------------------------------------------------------
PROCEDURE Pr_Form_Pend_Web		(pm_CURSOR1  	In Out Tp_Cursor,
					 pm_TP_CONS	    In Number,
					 pm_TP_SOL	    In Number,
					 pm_COD_FIL	    In Number,
					 pm_ERRO     	   Out Number,
					 pm_MSGERRO	   Out Char) as

Begin

	pm_ERRO    := 0;
	pm_MSGERRO := '';

	--Vai trazer somente as solicita��es que estiverem pendentes ou em an�lise
	If pm_TP_CONS = 0 then

			/*Select to_char(C.Cgc,'FM99999999999999') CGC,
			       Nvl(C.nome_cliente,'') NOME,
			       Nvl(C.Cod_Repr_Vend,0) REPR,
			       CT.Cod_Usuario || ' - ' || U.Nome_Usuario USER_REMET,
			       C.dt_solicitacao DTSOL,
			       CT.Situacao SIT,
			       Nvl(CT.Dt_Analise,'') DTANA
			  From Intranet.Cliente_Web C,
			       Intranet.Controle_Cliente_Web CT,
			       Producao.V_Usuario_Intranet U
			 Where Cod_Motivo_Recusa IS NULL
		           And CT.Situacao <> 9
		           And CT.Situacao <> 2
			   And C.CGC = CT.CGC
			   And C.Tp_Solicitacao = CT.Tp_Solicitacao
			   And CT.Cod_Usuario = U.Cod_Usuario
			   And C.Tp_Solicitacao = pm_TP_SOL;*/


		open pm_CURSOR1 for
			Select to_char(C.Cgc,'FM99999999999999') CGC,
			       Nvl(C.nome_cliente,'') NOME,
			       Nvl(C.Cod_Repr_Vend,0) REPR,
			       CT.Cod_Usuario || ' - ' || U.Nome_Usuario USER_REMET,
			       To_Char(C.dt_solicitacao,'dd/mm/rr HH24:MI:ss') DTSOL,
			       CT.Situacao SIT,
			       U2.Nome_Usuario USER_CRED,
			       Nvl(To_Char(CT.Dt_Analise,'dd/mm/rr HH24:MI:ss'),'') DTANA
			  From Intranet.Cliente_Web C,
			       Intranet.Controle_Cliente_Web CT,
			       Producao.Representante R,
			       Producao.V_Usuario_Intranet U,
			       Producao.V_Usuario_Intranet U2
			 Where CT.CGC = C.CGC
			   And CT.Tp_Solicitacao = C.Tp_Solicitacao
			   And R.Cod_Repres = C.Cod_Repr_Vend
			   And U.Cod_Usuario = CT.Cod_Usuario
		       And U2.Cod_Usuario(+) = CT.Cod_Usuario_Credito
		       And CT.Situacao <> 9
			   And CT.Situacao <> 2
		       And Cod_Motivo_Recusa IS NULL
			   And R.Cod_Filial+0 = pm_COD_FIL
			   And C.Tp_Solicitacao = pm_TP_SOL;

	--Vai trazer somente as solicita��es que j� tiverem sido aceitas e que foram pr�-cadastro ou recadastro
	ElsIf pm_TP_CONS = 1 then

		open pm_CURSOR1 for
			Select to_char(C.Cgc,'FM99999999999999') CGC,
			       CL.Cod_Cliente COD,
			       Nvl(C.nome_cliente,'') NOME,
			       Nvl(C.Cod_Repr_Vend,0) REPR,
			       CT.Cod_Usuario || ' - ' || U.Nome_Usuario USER_REMET,
			       To_Char(C.dt_solicitacao,'dd/mm/rr HH24:MI:ss') DTSOL,
			       CT.Situacao SIT,
			       U2.Nome_Usuario USER_CRED,
			       Nvl(To_Char(CT.Dt_Analise,'dd/mm/rr HH24:MI:ss'),'') DTANA,
			       CT.Tp_Solicitacao TPSOL
			  From Producao.Cliente CL,
			       Intranet.Cliente_Web C,
			       Intranet.Controle_Cliente_Web CT,
			       Producao.Representante R,
			       Producao.V_Usuario_Intranet U,
			       Producao.V_Usuario_Intranet U2
			 Where C.CGC = CL.CGC
			   And CT.CGC = C.CGC
			   And CT.Tp_Solicitacao = C.Tp_Solicitacao
			   And R.Cod_Repres = C.Cod_Repr_Vend
			   And U.Cod_Usuario = CT.Cod_Usuario
			   And U2.Cod_Usuario(+) = CT.Cod_Usuario_Credito
			   And C.Cod_Motivo_Recusa IS NULL
		       And CT.Situacao = 2
			   And C.Tp_Solicitacao In (0,2)
			   And R.Cod_Filial+0 = pm_COD_FIL
		      Order by CT.Dt_Analise;

	--Vai trazer somente as solicita��es que tiverem sido recusadas e que foram pr�-cadastro ou recadastro
	ElsIf pm_TP_CONS = 2 then

		open pm_CURSOR1 for
			Select to_char(C.Cgc,'FM99999999999999') CGC,
			       CL.Cod_Cliente COD,
			       Nvl(C.nome_cliente,'') NOME,
			       Nvl(C.Cod_Repr_Vend,0) REPR,
			       CT.Cod_Usuario || ' - ' || U.Nome_Usuario USER_REMET,
			       To_Char(C.dt_solicitacao,'dd/mm/rr HH24:MI:ss') DTSOL,
			       CT.Situacao SIT,
			       U2.Nome_Usuario USER_CRED,
			       Nvl(To_Char(CT.Dt_Analise,'dd/mm/rr HH24:MI:ss'),'') DTANA,
			       CT.Tp_Solicitacao TPSOL,
			       M.Cod_Motivo || ' - ' || M.Descricao DESC_RECUSA
			  From Producao.Cliente CL,
			       Intranet.Cliente_Web C,
			       Intranet.Controle_Cliente_Web CT,
			       Producao.Representante R,
			       Producao.V_Usuario_Intranet U,
			       Producao.V_Usuario_Intranet U2,
			       Credito.Motivo_Recusa M
			 Where C.CGC = CL.CGC(+)
			   And CT.CGC = C.CGC
			   And CT.Tp_Solicitacao = C.Tp_Solicitacao
			   And R.Cod_Repres = C.Cod_Repr_Vend
			   And U.Cod_Usuario = CT.Cod_Usuario
			   And U2.Cod_Usuario(+) = CT.Cod_Usuario_Credito
			   And C.Cod_Motivo_Recusa = M.Cod_Motivo(+)
		       And CT.Situacao = 9
			   And C.Tp_Solicitacao In (0,2)
			   And R.Cod_Filial+0 = pm_COD_FIL
		      Order by CT.Dt_Analise, C.CGC;


	End If;



	Exception When No_Data_Found Then Open pm_CURSOR1 for
	Select Dummy From Dual;
	When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

  End Pr_Form_Pend_Web;



  -- Selecionar dados da Tabela Cliente_Web para serem exibidas
  -- na coluna PARA da tela de RECADASTRO, onde o usuario do
  -- Depto de Credito ir� aceitar ou n�o as altera��es efetuadas
  Procedure PR_SELECT_CLIENTE_RECAD_VB_ALT (cALTERADOS       IN OUT TP_CURSOR,
                                            cALTREFBCO       IN OUT TP_CURSOR,
                                            cALTCOBRAN       IN OUT TP_CURSOR,
                                            pCGC             IN Producao.Cliente.cgc%Type) is
     Begin
       OPEN cALTERADOS FOR
            SELECT
                  Nome_Cliente "Nome Cliente",
                  CGC,
                  Endereco Endere�o,
                  Bairro Bairro,
                  CEP Cep,
                  B.Cod_Uf "C�digo UF",
                  A.COD_CIDADE "C�digo Cidade",
                  B.Nome_Cidade "Nome Cidade",
                  DDD1 "DDD 1",
                  FONE1 "Fone 1",
                  Nome_Contato "Nome Contato",
                  NOME_PROPRIETARIO "Nome Propriet�rio",
                  INSCR_ESTADUAL "Inscri��o Estadual",
                  COD_REPR_VEND "C�digo do Representante",
                  COD_MENSAGEM_FISCAL "C�digo Mensagem Fiscal",
                  TP_EMPRESA "TP_EMPRESA",
                  Nvl(Fl_Grupo_Master,'') "GrMaster"
            from  Intranet.cliente_web A,
                  Producao.cidade B
            where CGC = pCGC AND
                  A.TP_SOLICITACAO = 2 AND
                  A.Cod_Cidade = B.Cod_Cidade;

       OPEN cALTREFBCO FOR
            SELECT
                  A.BANCO_REF_BCO,
                  A.AGENCIA_REF_BCO,
                  A.DDD_REF_BCO,
                  A.FONE_REF_BCO
            from  Intranet.cliente_web A
            where CGC = pCGC AND
                  A.TP_SOLICITACAO = 2;

       OPEN cALTCOBRAN FOR
            SELECT
                  TP_DOCTO_COB,
                  CGC_COB,
                  INSCR_ESTADUAL_COB,
                  NOME_CLIENTE_COB,
                  COD_CIDADE_COB,
                  ENDERECO_COB,
                  BAIRRO_COB,
                  CEP_COB,
                  B.COD_UF
            from  Intranet.cliente_web A,
                  Producao.cidade B
            where CGC = pCGC AND
                  A.TP_SOLICITACAO = 2 AND
                  A.Cod_Cidade = B.Cod_Cidade;

  End PR_SELECT_CLIENTE_RECAD_VB_ALT;

  -- Trazer os dados comerciais da tabela CLIENTE_WEB para que o usuario
  -- do Departamento de Credito aceite ou n�o estas informa��es
  Procedure PR_SELECT_CLIENTE_RECAD_REFCOM (cREFCOM  IN OUT TP_CURSOR,
                                            pCGC     IN Producao.Cliente.cgc%Type) is
     Begin
       OPEN cREFCOM FOR
            SELECT
                  A.EMPRESA1_REF_COM,
                  A.DDD1_REF_COM,
                  A.FONE1_REF_COM,
                  A.EMPRESA2_REF_COM,
                  A.DDD2_REF_COM,
                  A.FONE2_REF_COM,
                  A.EMPRESA3_REF_COM,
                  A.DDD3_REF_COM,
                  A.FONE3_REF_COM,
                  A.EMPRESA4_REF_COM,
                  A.DDD4_REF_COM,
                  A.FONE4_REF_COM,
                  A.EMPRESA5_REF_COM,
                  A.DDD5_REF_COM,
                  A.FONE5_REF_COM
            from  Intranet.cliente_web A
            where CGC = pCGC AND
                  A.TP_SOLICITACAO = 2;
  End PR_SELECT_CLIENTE_RECAD_REFCOM;



--------------------------------------------------
PROCEDURE PR_ATU_PRECAD_CTRCLIWEB      (PM_CGC 		IN INTRANET.CONTROLE_CLIENTE_WEB.CGC%TYPE,
					PM_TPSOL	IN INTRANET.CONTROLE_CLIENTE_WEB.TP_SOLICITACAO%TYPE,
					PM_DT_ANALISE	IN INTRANET.CONTROLE_CLIENTE_WEB.DT_ANALISE%TYPE,
					PM_SITUACAO	IN INTRANET.CONTROLE_CLIENTE_WEB.SITUACAO%TYPE,
					PM_COD_USER	IN INTRANET.CONTROLE_CLIENTE_WEB.COD_USUARIO_CREDITO%TYPE,
					PM_ERRO	       OUT NUMBER,
					PM_MSGERRO     OUT CHAR)
IS
BEGIN
	PM_ERRO    := 0;
	PM_MSGERRO := '';

		---- Atualiza situa��o da solicita��o para  ----

		UPDATE INTRANET.Controle_Cliente_Web
		   SET Dt_Analise          = PM_DT_ANALISE,
		       Situacao            = PM_SITUACAO,
		       Cod_Usuario_Credito = PM_COD_USER
		 WHERE CGC = PM_CGC
		   AND Tp_Solicitacao = PM_TPSOL;

		COMMIT;

	Exception When others then Rollback;
	PM_ERRO := SQLCODE; PM_MSGERRO := SQLERRM;

End PR_ATU_PRECAD_CTRCLIWEB;


--------------------------------------------------
PROCEDURE PR_DEL_CLIWEB		       (PM_CGC 		IN INTRANET.CONTROLE_CLIENTE_WEB.CGC%TYPE,
					PM_TPSOL	IN INTRANET.CONTROLE_CLIENTE_WEB.TP_SOLICITACAO%TYPE,
					PM_ERRO	       OUT NUMBER,
					PM_MSGERRO     OUT CHAR)
IS
BEGIN

	PM_ERRO    := 0;
	PM_MSGERRO := '';

	---- Deleta solicita��o da CLIENTE_WEB pois cliente j� foi inserido/alterado ----

	DELETE FROM INTRANET.Cliente_Web
	      WHERE CGC = PM_CGC
	        AND Tp_Solicitacao = PM_TPSOL;
	Commit;

	Exception When others then Rollback;
	PM_ERRO := SQLCODE; PM_MSGERRO := SQLERRM;

End PR_DEL_CLIWEB;




--------------------------------------------------
PROCEDURE PR_GRAVA_MOTIVORECUSA	       (PM_CGC 		IN INTRANET.CLIENTE_WEB.CGC%TYPE,
					PM_TPSOL	IN INTRANET.CLIENTE_WEB.TP_SOLICITACAO%TYPE,
					PM_CODMOTIVO	IN INTRANET.CLIENTE_WEB.COD_MOTIVO_RECUSA%TYPE,
					PM_ERRO	       OUT NUMBER,
					PM_MSGERRO     OUT CHAR)
IS
BEGIN

	PM_ERRO    := 0;
	PM_MSGERRO := '';

	---- Cadastra o motivo da recusa do cliente na CLIENTE_WEB ----

	UPDATE INTRANET.Cliente_Web
	   SET Cod_Motivo_Recusa = PM_CODMOTIVO
	 WHERE CGC = PM_CGC
	   AND Tp_Solicitacao = PM_TPSOL;
	Commit;

	Exception When others then Rollback;
	PM_ERRO := SQLCODE; PM_MSGERRO := SQLERRM;

End PR_GRAVA_MOTIVORECUSA;


     Procedure Pr_Select_Cliente_Pre_Cadastro (PM_CURSOR1 IN OUT TP_CURSOR,
                                               PM_CGC     IN Producao.cliente.cgc%Type,
                                               PM_TPSOL   IN Number,
                                               PM_CODERRO OUT Number,
                                               PM_TxtERRO OUT Number) is
        BEGIN
           OPEN PM_CURSOR1 FOR
                Select To_Char(C.CGC,'00000000000000') CGC,
                C.Nome_Cliente,
                C.Nome_Contato,
                C.Endereco,
                C.Cod_Cidade, CI.Nome_Cidade, CI.Cod_Uf,
                C.Bairro,
                C.DDD1, C.Fone1, Nvl(C.DDD2,0) DDD2, Nvl(C.Fone2,0) FONE2,
                C.Cep,
                C.Inscr_Estadual,
                Nvl(C.Inscr_Suframa,'') INSCR_SUFRAMA,
                C.Cod_Tipo_Cliente, TC.Desc_Tipo_Cli,
                Nvl(C.Cod_Repr_Vend,0) COD_REPR_VEND, R.Nome_Repres,
                To_Char(C.Dt_Fundacao,'dd/mm/yy') Dt_Fundacao, C.Propriedade,
                C.Nome_Proprietario,
                Nvl(C.Fl_Cons_Final,'') FL_CONS_FINAL,
                Nvl(C.Fl_Categ_Bosch_Diesel,'') FL_CATEG_BOSCH_DIESEL,
                Nvl(C.Fl_Grupo_Master,'') FL_GRUPO_MASTER,
                Nvl(C.Cod_Tare,0) COD_TARE,
                Nvl(To_Char(C.Dt_Tare,'dd/mm/yy'),'') DT_TARE,
                Nvl(C.Cod_Mensagem_Fiscal,0) CODFIS, M.Desc_Mens MSGFIS,
                Nvl(C.E_Mail,'') E_MAIL, Nvl(C.HomePage,'') HOMEPAGE,
                To_Char(C.Dt_Solicitacao,'dd/mm/yy HH24:MI:ss') Dt_Solicitacao,
                CT.Cod_Usuario, U.Nome_Usuario,
                Nvl(C.Empresa1_Ref_Com,'') EMP1,
                Nvl(C.DDD1_Ref_Com,0) DDD1, Nvl(C.Fone1_Ref_Com,0) FONE1,
                Nvl(C.Empresa2_Ref_Com,'') EMP2,
                Nvl(C.DDD2_Ref_Com,0) DDD2, Nvl(C.Fone2_Ref_Com,0) FONE2,
                Nvl(C.Empresa3_Ref_Com,'') EMP3,
                Nvl(C.DDD3_Ref_Com,0) DDD3, Nvl(C.Fone3_Ref_Com,0) FONE3,
                Nvl(C.Empresa4_Ref_Com,'') EMP3,
                Nvl(C.DDD4_Ref_Com,0) DDD4, Nvl(C.Fone4_Ref_Com,0) FONE3,
                Nvl(C.Empresa5_Ref_Com,'') EMP3,
                Nvl(C.DDD5_Ref_Com,0) DDD5, Nvl(C.Fone5_Ref_Com,0) FONE5,
                Nvl(C.Banco_Ref_Bco,'') BCO_BCO,
                Nvl(C.Agencia_Ref_Bco,0) AG_BCO, Nvl(C.DDD_Ref_Bco,0) DDD_BCO,
                Nvl(C.Tp_Docto_Cob,0) TPDOC_COB,
                Nvl(C.CGC_Cob,0) CGC_COB, Nvl(C.Nome_Cliente_Cob,'') NOME_COB,
                Nvl(C.Endereco_Cob,'') END_COB, Nvl(C.Cod_Cidade_Cob,0) CID_COB,
                CI2.Nome_Cidade, CI2.Cod_Uf,
                Nvl(C.Bairro_Cob,'') BAI_COB, Nvl(C.Cep_Cob,0) CEP_COB,
                Nvl(C.Inscr_Estadual_Cob,'') INSR_EST_COB
                From Intranet.Cliente_Web C,
                    Producao.Cidade CI,
                    Producao.Cidade CI2,
                    Producao.Representante R,
                    Intranet.Controle_Cliente_Web CT,
                    Producao.V_Usuario_Intranet U,
                    Producao.Clie_Mensagem M,
                    Producao.Tipo_Cliente TC
               Where C.CGC = pm_CGC
                 And C.Tp_Solicitacao = pm_TPSOL
                 And C.CGC = CT.CGC
                 And C.Tp_Solicitacao = CT.Tp_Solicitacao
                 And C.Cod_Tipo_Cliente = TC.Cod_Tipo_Cli
                 And C.Cod_Cidade = CI.Cod_Cidade
                 And CT.Cod_Usuario = U.Cod_Usuario
                 And C.Cod_Cidade = CI2.Cod_Cidade(+)
                 And C.Cod_Repr_Vend = R.Cod_Repres(+)
                 And C.Cod_Mensagem_Fiscal = M.Cod_Mensagem(+)
                 And (CT.Situacao = 0 Or CT.Situacao = 1);

          Exception
            When Others then
                 pm_CodErro :=SqlCode;
                 pm_TxtErro :=Sqlerrm;

    End Pr_Select_Cliente_Pre_Cadastro;

    Procedure Pr_Select_Log (pm_CURSOR1 IN OUT TP_CURSOR,
                             pm_Tipo          IN Number,
                             pm_Situacao      IN Number,
                             pm_CodUsuarioWeb IN Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
                             pm_CodUsuarioCre IN Intranet.Controle_Cliente_Web.Cod_Usuario_Credito%Type,
                             pm_DTINI         IN Char,
                             pm_DTFIM         IN Char,
                             pm_CodErro       OUT Number,
                             pm_TxtErro       OUT Varchar2) is
       vTabela Char(20);
       vSelect Varchar2(4000);
       vWhere  Varchar2(4000);
       Begin
            --Tipo Solicitacao = 0 (Pre Cadastro)
            IF pm_Tipo = 0 and (pm_Situacao = 0 or pm_situacao=1 or pm_Situacao=9) then
               vTabela := ' Cliente_Web ';
               vWhere :=  ' AND A.TP_SOLICITACAO = B.TP_SOLICITACAO ';
            ELSIF pm_Tipo = 0 and pm_Situacao=2 then
               vTabela := ' Cliente ';
            End if;

            --Tipo Solicitacao = 1 (Alteracao)
            IF pm_Tipo = 1 or pm_tipo = 2 then
               vTabela := ' Cliente ';
            End if;

            vWhere := vWhere || ' AND A.TP_SOLICITACAO = ' || pm_Tipo;

            IF pm_COdUsuarioWeb <> 0 then
               vWhere := vWhere || ' AND A.Cod_Usuario= ' || Pm_Codusuarioweb;
            ELSIF Pm_Codusuariocre <> 0 then
               vWhere := vWhere || ' AND A.Cod_Usuario_Credito= ' || Pm_CodusuarioCre;
            END IF;

            IF Pm_Situacao <> 99 then
               vWhere := vWhere || ' AND A.Situacao = ' || pm_situacao;
            End if;

            IF LENGTH(TRIM(pm_DTINI))> 0 then
               --vWhere := vWhere || ' AND A.DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DTINI || Chr(39) || ',' || Chr(39) || 'DD/MM/RR' || chr(39) || ')';
               vWhere := vWhere || ' AND TRUNC(A.DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DTINI || Chr(39) || ',' || Chr(39) || 'DD/MM/RR' || chr(39) || ')';
               IF LENGTH(TRIM(pm_DTFIM)) > 0 then
                  --vWhere := vWhere || ' AND A.DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTFIM || chr(39) || ','|| chr(39) || 'DD/MM/RR' || chr(39) || ')';
                  vWhere := vWhere || ' AND TRUNC(A.DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTFIM || chr(39) || ','|| chr(39) || 'DD/MM/RR' || chr(39) || ')';
               END IF;
            End IF;

            -- Tratar se escolheu apenas o TIPO
            -- Se a pm_Situacao = 99 quer dizer que escolheu apenas o tipo
            IF pm_Situacao<> 99 then
               vSelect := 'Select
                       A.CGC,
                       B.Nome_Cliente,
                       Decode(A.Tp_Solicitacao, 0, ' || CHR(39) || 'Pr�-Cadastro' || CHR(39) || ', 1,' || CHR(39) || 'Pr�-Altera��o' || CHR(39) || ', 2,' || CHR(39) || 'Pr�-Recadastro' || CHR(39) || ') Tp_Solicitacao,
                       TO_CHAR(A.Dt_solicitacao,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_SOLICITACAO,
                       Decode(A.SITUACAO,0,' || CHR(39) || 'Pendente' || CHR(39) || ',1,' || CHR(39) || 'Em An�lise' || CHR(39) || ',2,' || CHR(39) || 'Aceito' || CHR(39) || ',9,' || CHR(39) || 'Recusado' || CHR(39) || ') Situacao,
                       TO_CHAR(A.Dt_Analise,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_ANALISE,
                       A.Cod_usuario Cod_usuario_Web,
                       D.Nome_usuario Nome_usuario_web,
                       A.Cod_usuario_Credito,
                       C.NOME_USUARIO
                       from
                            controle_cliente_web A, '
                            || vTabela || ' B,
                            v_usuario_intranet C,
                            v_usuario_intranet D
                       where
                            A.cgc = B.CGC And
                            A.COD_USUARIO_CREDITO = C.Cod_Usuario(+) And
                            A.Cod_Usuario = D.Cod_Usuario(+) '
                            || vWhere ||
                       ' order by A.CGC, A.TP_SOLICITACAO';
            ELSIF pm_Situacao = 99 Then --(Escolheu apenas o Tipo)
                  vSelect := 'Select
                       A.CGC,
                       B.Nome_Cliente,
                       Decode(A.Tp_Solicitacao, 0, ' || CHR(39) || 'Pr�-Cadastro' || CHR(39) || ', 1,' || CHR(39) || 'Pr�-Altera��o' || CHR(39) || ', 2,' || CHR(39) || 'Pr�-Recadastro' || CHR(39) || ') Tp_Solicitacao,
                       TO_CHAR(A.Dt_solicitacao,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_SOLICITACAO,
                       Decode(A.SITUACAO,0,' || CHR(39) || 'Pendente' || CHR(39) || ',1,' || CHR(39) || 'Em An�lise' || CHR(39) || ',2,' || CHR(39) || 'Aceito' || CHR(39) || ',9,' || CHR(39) || 'Recusado' || CHR(39) || ') Situacao,
                       TO_CHAR(A.Dt_Analise,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_ANALISE,
                       A.Cod_usuario Cod_usuario_Web,
                       D.Nome_usuario Nome_usuario_web,
                       A.Cod_usuario_Credito,
                       C.NOME_USUARIO
                       from
                            controle_cliente_web A,
                            Cliente B,
                            v_usuario_intranet C,
                            v_usuario_intranet D
                       where
                            A.cgc = B.CGC And
                            A.COD_USUARIO_CREDITO = C.Cod_Usuario(+) And
                            A.Cod_Usuario = D.Cod_Usuario(+) '
                            || vWhere ;
               vSelect := vSelect || ' UNION ';
               vSelect := vSelect || 'Select
                       A.CGC,
                       B.Nome_Cliente,
                       Decode(A.Tp_Solicitacao, 0, ' || CHR(39) || 'Pr�-Cadastro' || CHR(39) || ', 1,' || CHR(39) || 'Pr�-Altera��o' || CHR(39) || ', 2,' || CHR(39) || 'Pr�-Recadastro' || CHR(39) || ') Tp_Solicitacao,
                       TO_CHAR(A.Dt_solicitacao,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_SOLICITACAO,
                       Decode(A.SITUACAO,0,' || CHR(39) || 'Pendente' || CHR(39) || ',1,' || CHR(39) || 'Em An�lise' || CHR(39) || ',2,' || CHR(39) || 'Aceito' || CHR(39) || ',9,' || CHR(39) || 'Recusado' || CHR(39) || ') Situacao,
                       TO_CHAR(A.Dt_Analise,' || CHR(39) || 'DD/MM/RR HH24:MI:SS' || CHR(39) || ') DT_ANALISE,
                       A.Cod_usuario Cod_usuario_Web,
                       D.Nome_usuario Nome_usuario_web,
                       A.Cod_usuario_Credito,
                       C.NOME_USUARIO
                       from
                            controle_cliente_web A,
                            Cliente_Web B,
                            v_usuario_intranet C,
                            v_usuario_intranet D
                       where
                            A.cgc = B.CGC And
                            A.COD_USUARIO_CREDITO = C.Cod_Usuario(+) And
                            A.Cod_Usuario = D.Cod_Usuario(+) And
                            A.TP_SOLICITACAO = B.TP_SOLICITACAO '
                            || vWhere;
             END IF;

             OPEN pm_CURSOR1 FOR
                  vSelect;

        Exception
        When others then
             pm_CodErro := Sqlcode;
             pm_txtErro := Sqlerrm;

    End Pr_Select_Log;

    Procedure Pr_Select_Usuarios (pm_Cursor1 IN OUT TP_CURSOR,
                                      pm_Cursor2 IN OUT TP_CURSOR) is
        BEGIN
           OPEN pm_CURSOR1 FOR
                Select A.cod_usuario, B.NOME_USUARIO
                From   controle_cliente_web A, v_usuario_intranet B
                Where  A.COD_USUARIO = B.COD_USUARIO
                Group By A.Cod_usuario, B.NOME_USUARIO 
                Order By A.Cod_usuario, B.NOME_USUARIO ;

           OPEN pm_CURSOR2 FOR
                Select A.cod_usuario_Credito, B.NOME_USUARIO
                From   controle_cliente_web A, v_usuario_intranet B
                Where  A.COD_USUARIO = B.COD_USUARIO
                and    Cod_usuario_Credito is not null
                Group By A.Cod_usuario_Credito, B.NOME_USUARIO
                Order By A.Cod_usuario_Credito, B.NOME_USUARIO;

    End Pr_Select_Usuarios;

    Procedure Pr_Totalizacoes(pm_Cursor1 IN OUT TP_CURSOR,
                              pm_Opcao   IN Number,
                              pm_DtDe    IN Varchar2,
                              pm_DtAte   IN Varchar2) is
         vSelect varchar2(4000);
         BEGIN

             --Contar Tudo
             IF pm_Opcao = 1 or pm_Opcao = 6 Then
                vSelect := 'Select count(Cgc) from Controle_Cliente_web ';

                IF pm_DtDe is not null Then

                   --vSelect := vSelect || ' WHERE DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';
                   vSelect := vSelect || ' WHERE trunc(DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';

                End IF;

			   IF pm_DtAte is not null Then

                  --vSelect := vSelect || ' AND DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';
                  vSelect := vSelect || ' AND trunc(DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';

               END IF;

             --Contar Tudo por Solicitacao
             ELSIF pm_Opcao = 2 or pm_Opcao = 7 Then

                vSelect := 'Select count(CGC),
                           Decode(Tp_Solicitacao, 0, ' || CHR(39) || 'Pr�-Cadastro' || CHR(39) || ', 1,' || CHR(39) || 'Pr�-Altera��o' || CHR(39) || ', 2,' || CHR(39) || 'Pr�-Recadastro' || CHR(39) || ') Tp_Solicitacao
                           from Controle_Cliente_web ';

                IF pm_DtDe is not null Then

                   --vSelect := vSelect || ' WHERE DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';
                   vSelect := vSelect || ' WHERE TRUNC(DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';

                End IF;

			   IF pm_DtAte is not null Then

                  --vSelect := vSelect || ' AND DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';
                  vSelect := vSelect || ' AND TRUNC(DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';

               END IF;

                vSelect := vSelect || ' Group by Tp_Solicitacao Order by Tp_Solicitacao ';
             --Contar Tudo por Situacao
             ELSIF pm_Opcao = 3 or pm_Opcao = 8 Then

                vSelect := 'Select count(CGC),
                           Decode(SITUACAO,0,' || CHR(39) || 'Pendente' || CHR(39) || ',1,' || CHR(39) || 'Em An�lise' || CHR(39) || ',2,' || CHR(39) || 'Aceito' || CHR(39) || ',9,' || CHR(39) || 'Recusado' || CHR(39) || ') Situacao
                           from Controle_Cliente_web ';

                IF pm_DtDe is not null Then

                  --vSelect := vSelect || ' WHERE DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';
                   vSelect := vSelect || ' WHERE TRUNC(DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';

                End IF;

			   IF pm_DtAte is not null Then

                  --vSelect := vSelect || ' AND DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';
                  vSelect := vSelect || ' AND TRUNC(DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';

               END IF;

                vSelect := vSelect || ' Group by Situacao Order by Situacao ';
             --Contar Tudo por Usuario Web
             ELSIF pm_Opcao = 4 or pm_Opcao = 9 Then

               vSelect := 'Select count(A.CGC), B.NOME_USUARIO
                          from   Controle_Cliente_web A, v_usuario_intranet B
                          Where  A.COD_USUARIO = B.Cod_Usuario ';

                IF pm_DtDe is not null Then

                   --vSelect := vSelect || ' AND DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';
                   vSelect := vSelect || ' AND TRUNC(DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';

                End IF;

			   IF pm_DtAte is not null Then

                  --vSelect := vSelect || ' AND DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';
                  vSelect := vSelect || ' AND TRUNC(DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';

               END IF;

               vSelect := vSelect || ' Group by A.Cod_usuario, B.Nome_Usuario Order by A.Cod_usuario, B.Nome_Usuario ';
             --Contar Tudo por Usuario Credito
             ELSIF pm_Opcao = 5  or pm_Opcao = 10 Then

                vSelect := 'Select count(A.CGC), B.NOME_USUARIO
                           from   Controle_Cliente_web A, v_usuario_intranet B
                           Where  A.COD_USUARIO_CREDITO = B.Cod_Usuario ';

                IF pm_DtDe is not null Then

                   --vSelect := vSelect || ' AND DT_ANALISE >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';
                   vSelect := vSelect || ' AND TRUNC(DT_ANALISE) >= TO_DATE(' || Chr(39) || pm_DtDe || Chr(39) || ',' || Chr(39) || 'DD-MM-RR' || chr(39) || ')';

                End IF;

			   IF pm_DtAte is not null Then

                  --vSelect := vSelect || ' AND DT_ANALISE <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';
                  vSelect := vSelect || ' AND TRUNC(DT_ANALISE) <= TO_DATE(' || Chr(39) || pm_DTATE || chr(39) || ','|| chr(39) || 'DD-MM-RR' || chr(39) || ')';

               END IF;

                vSelect := vSelect || ' Group by A.Cod_usuario_Credito, B.Nome_Usuario Order by A.Cod_usuario_Credito, B.Nome_Usuario ';
             END IF;
             OPEN PM_CURSOR1 FOR
                  vSelect;

    End Pr_Totalizacoes;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMA��ES DO C�DIGO DE USU�RIO
	--------------------------------------------------------------------------------------------------
	PROCEDURE Pr_Sel_UserIntranet		(pm_CURSOR1  	In Out Tp_Cursor,
					 	 pm_CGC	    	In Intranet.Controle_Cliente_Web.CGC%Type,
					 	 pm_TPSOL	In Intranet.Controle_Cliente_Web.Tp_Solicitacao%Type,
				 	 	 pm_ERRO     	   Out Number,
				 	 	 pm_MSGERRO	   Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		OPEN pm_CURSOR1 FOR
		  Select A.cod_usuario, To_Char(A.Dt_Solicitacao,'DD/MM/RR HH24:MI:SS') Dt_Solicitacao,
		         To_Char(A.Dt_Analise,'DD/MM/RR HH24:MI:SS') Dt_Analise, B.E_mail
		    From Intranet.Controle_Cliente_Web A, v_usuario_intranet B
		   Where A.CGC = pm_CGC
		     And A.Tp_Solicitacao = pm_TPSOL
		     And A.COD_USUARIO = B.COD_USUARIO;

		Exception When No_Data_Found Then Open pm_CURSOR1 for
		Select Dummy From Dual;
		When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End Pr_Sel_UserIntranet;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMA��ES DO LIMITE
	--------------------------------------------------------------------------------------------------
	Procedure Pr_Select_Limite (pm_Cursor1 In Out Tp_Cursor,
                            pCGC       In cliente.cgc%TYpe) is

         BEGIN
              Open pm_Cursor1 For
                  Select DT_CONSULTA,
                         CONCEITO_EMPRESA,
                         OBSERVACAO
                  from   producao.clie_refer
                  where  cod_cliente = (Select Cod_Cliente From Producao.Cliente Where  Cgc =  pCGC)
                  and    TRIM(Empresa) = 'LIMITE' and DT_Consulta >= To_date('07-JUL-04','DD-MON-RR')
                  Order by DT_CONSULTA desc;
                  /*select DT_CONSULTA,
                         CONCEITO_EMPRESA,
                         OBSERVACAO
                  from   producao.clie_refer
                  where cod_cliente = (Select Cod_Cliente From Producao.Cliente Where  Cgc = pCGC)
                  and Empresa = 'LIMITE';
                  */

   	End Pr_Select_Limite;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMA��ES DO CLIENTE NO RECADASTRO
	--------------------------------------------------------------------------------------------------
   	Procedure Pr_Select_Nome_Cliente (PM_CURSOR1 IN OUT TP_CURSOR, PM_CGC IN cliente.cgc%Type) is
   	    BEGIN
   	       OPEN PM_CURSOR1 FOR
   	            SELECT NOME_CLIENTE FROM cliente WHERE CGC=PM_CGC;

   	End Pr_Select_Nome_Cliente;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE P/ VERIFICAR SE EXISTE ALGUM CLIENTE (PRE-CADASTRO), J� ACEITO E QUE AINDA CONTINUA
	-- COM SITUCAOA = 1 (EM ANALISE)
	-- SE HOUVER, ALTERA A SITUACAO PARA 2 (ACEITO)
	--------------------------------------------------------------------------------------------------
	Procedure PR_CLIE_ACEITO_ENROSC		(pm_ERRO     	   Out Number,
				 	 	 pm_MSGERRO	   Out Char)
	Is

	vCGC 	Producao.Cliente.CGC%Type;

	Cursor C_CLIENTE Is
		  Select CT.CGC
		    From Intranet.Controle_Cliente_Web CT,
		    	 Producao.Cliente C
		   Where CT.CGC = C.CGC
		     And CT.Tp_Solicitacao = 0
		     And CT.Situacao = 1;
	L_CLIENTE C_CLIENTE%RowType;


	Begin

		pm_ERRO := 0;
		pm_MSGERRO := '';

		Open C_CLIENTE;
		Loop
			Fetch C_CLIENTE Into L_CLIENTE;

				-- Se nao encontrar itens para calcular, registra log e l� pr�ximo item
				If C_CLIENTE%NotFound Then
					vCGC := 0;
					Exit;
				End If;
				vCGC := L_CLIENTE.CGC;

				Update Intranet.Controle_Cliente_Web
				   Set Situacao = 2
				 Where Tp_Solicitacao = 0
				   And Situacao = 1
				   And CGC = vCGC;

				Commit;
		End Loop;
		Close C_CLIENTE;

		Exception When others then pm_ERRO := Sqlcode; pm_MSGERRO := Sqlerrm;


		--Exception When No_Data_Found Then Open C_CLIENTE for
		--Select Dummy From Dual;
		--When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End PR_CLIE_ACEITO_ENROSC;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE TRAZER INFORMA��ES DO RESPONS�VEL PELO CLIENTE - QUEM DIGITOU O CLIENTE
	--------------------------------------------------------------------------------------------------
	PROCEDURE Pr_RespCli		(pm_CURSOR1  	In Out Tp_Cursor,
					 	 		 pm_CGC	    	In Intranet.Controle_Cliente_Web.CGC%Type,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		If pm_TP_CONS = 0 then

			OPEN pm_CURSOR1 FOR

				Select Distinct Nvl(C.nome_cliente,'NOME INDISPONIVEL') Nome_Cliente, A.cod_usuario, B.Nome_Usuario
				  From Producao.Cliente C,
					   Intranet.Controle_Cliente_Web A,
					   v_usuario_intranet B
				 Where A.CGC = C.CGC(+)
				   And B.COD_USUARIO = A.COD_USUARIO
				   And A.CGC = pm_CGC;

		-- Para conseguir verificar nome de solicita��es que foram recusadas, acessa a cliente_web
		Elsif pm_TP_CONS = 1 then

			OPEN pm_CURSOR1 FOR

				Select Nome_Cliente
				  From Intranet.Cliente_Web
				 Where CGC = pm_CGC;

		End If;

		Exception When No_Data_Found Then Open pm_CURSOR1 for
		Select Dummy From Dual;
		When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End Pr_RespCli;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PARA PREENCHER COMBOS REF. A RESPONSAVEIS POR CLIENTES (ATUAIS OU NOVOS)
	--------------------------------------------------------------------------------------------------
	PROCEDURE Pr_Responsaveis	(pm_CURSOR1  	In Out Tp_Cursor,
							 	 pm_CPO_CONS	In Varchar2,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		-- Traz somente usu�rios que j� t�m clientes cadastrados sob sua responsabilidade
		If pm_TP_CONS = 0 Then

			Open pm_CURSOR1 For

				SELECT distinct LPAD(c.cod_usuario,5,0) Cod_Usuario, u.Nome_Usuario
				  FROM intranet.controle_cliente_web c, v_usuario_intranet u
				 WHERE c.cod_usuario = u.cod_usuario
			  ORDER BY u.nome_usuario;

		-- Traz todos os usu�rios cadastrados na Intrante (para possibilitar troca de resp.)
		Elsif pm_TP_CONS = 1 Then

			Open pm_CURSOR1 For

				SELECT distinct LPAD(cod_usuario,5,0) Cod_Usuario, Nome_Usuario
				  FROM v_usuario_intranet
				 WHERE fl_ativo = 'S'
			  ORDER BY nome_usuario;

		End If;

		Exception When No_Data_Found Then Open pm_CURSOR1 for
		Select Dummy From Dual;
		When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End Pr_Responsaveis;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE QUE FAZ A TROCA DE RESPONS�VEIS DE 1 CLIENTE OU DE 1 CARTEIRA DE CLIENTES
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_TROCA_RESP_CLI	(pm_CGC			In Intranet.Controle_Cliente_Web.CGC%Type,
							 	 pm_USU_ATU	    In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 pm_USU_NOVO    In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 pm_TP_CONS	    In Number,
				 	 	 		 pm_ERRO     	Out Number,
						 	 	 pm_MSGERRO	    Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		-- Altera o respons�vel pelo cliente especificado (tanto na cliente_web, quanto na de controle)
		If pm_TP_CONS = 0 Then

			Update Intranet.Cliente_Web
			   Set Cod_Usuario = pm_USU_NOVO
			 Where CGC = pm_CGC
			   And Cod_Usuario = pm_USU_ATU;

			Update Intranet.Controle_Cliente_Web
			   Set Cod_Usuario = pm_USU_NOVO
			 Where CGC = pm_CGC
			   And Cod_Usuario = pm_USU_ATU;

		-- Altera para um novo respons�vel, toda a carteira de clientes do responsavel atual especificado
		Elsif pm_TP_CONS = 1 Then

			Update Intranet.Cliente_Web
			   Set Cod_Usuario = pm_USU_NOVO
			 Where Cod_Usuario = pm_USU_ATU;

			Update Intranet.Controle_Cliente_Web
			   Set Cod_Usuario = pm_USU_NOVO
			 Where Cod_Usuario = pm_USU_ATU;

		End If;

		Commit;
		Exception When others then Rollback; PM_ERRO := SQLCODE; PM_MSGERRO := SQLERRM;

	End PR_TROCA_RESP_CLI;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE PREENCHER GRID COM O CGC DOS CLIENTES QUE EST�O SOB A RESPONSABILIDADE DO USU�RIO ATUAL ESCOLHIDO
	--------------------------------------------------------------------------------------------------
	PROCEDURE Pr_Cli_Responsavel	(pm_CURSOR1  	In Out Tp_Cursor,
							 	 	 pm_USU_ATU		In Intranet.Controle_Cliente_Web.Cod_Usuario%Type,
							 	 	 pm_TP_CONS	    In Number,
				 	 	 		 	 pm_ERRO     	Out Number,
						 	 	 	 pm_MSGERRO	    Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		-- Traz somente usu�rios que j� t�m clientes cadastrados sob sua responsabilidade
		If pm_TP_CONS = 0 Then

			Open pm_CURSOR1 For

				SELECT distinct TO_CHAR(CGC,'00000000000000') CGC
				  FROM intranet.controle_cliente_web
				 WHERE cod_usuario = pm_USU_ATU
			  ORDER BY CGC;

		End If;

		Exception When No_Data_Found Then Open pm_CURSOR1 for
		Select Dummy From Dual;
		When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End Pr_Cli_Responsavel;


	--------------------------------------------------------------------------------------------------
	--
	--------------------------------------------------------------------------------------------------
  	Procedure Pr_Select_Repres_Tipo_R 	(PM_COD_REPRES IN representante.cod_repres%Type,
                                     	 PM_Resultado  OUT NUMBER) is
      vQtd Number(5):=0;
      BEGIN
         SELECT Count(COD_REPRES) INTO vQtd
         FROM   REPRESENTANTE
         WHERE  COD_REPRES = PM_COD_REPRES
         AND    TIPO = 'R';

         PM_RESULTADO :=vQtd;

  	End Pr_Select_Repres_Tipo_R;


	--------------------------------------------------------------------------------------------------
	-- PROCEDURE QUE DELETA/INSERE TODOS OS RELACIONAMENTOS USUARIO x FILIAL
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_USUARIO_VISAO_CLI	(pm_CODUSU	In Intranet.Usuario_Visao_Cliente.Cod_Usuario%Type,
							 	 	 pm_CODFIL	In Intranet.Usuario_Visao_Cliente.Cod_Filial%Type,
							 	 	 pm_TP_CONS	In Varchar2,
				 	 	 		 	 pm_ERRO    Out Number,
						 	 	 	 pm_MSGERRO	Out Char)
	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		-- Deleta todos os registros de relacionamento do usu�rio
		If pm_TP_CONS = 'D' Then

			Delete Intranet.Usuario_Visao_Cliente
			 Where Cod_Usuario = pm_CODUSU;

		-- Insere relacionamentos para o usu�rio (Usuario x Filial)
		Elsif pm_TP_CONS = 'I' Then

			Insert into Intranet.Usuario_Visao_Cliente (Cod_Usuario, Cod_Filial)
				 Values (pm_CODUSU, pm_CODFIL);

		End If;

		Commit;
		Exception When others then Rollback; PM_ERRO := SQLCODE; PM_MSGERRO := SQLERRM;

	End PR_USUARIO_VISAO_CLI;

	--------------------------------------------------------------------------------------------------
	-- PROCEDURE QUE CONSULTA TODOS OS RELACIONAMENTOS USUARIO x FILIAL
	--------------------------------------------------------------------------------------------------
	PROCEDURE PR_SEL_USU_VISAO		(pm_CURSOR1  	In Out Tp_Cursor,
									 pm_CODUSU		In Intranet.Usuario_Visao_Cliente.Cod_Usuario%Type,
							 	 	 pm_CODFIL		In Intranet.Usuario_Visao_Cliente.Cod_Filial%Type,
							 	 	 pm_TP_CONS		In Number,
				 	 	 		 	 pm_ERRO    	Out Number,
						 	 	 	 pm_MSGERRO		Out Char)

	As
	Begin
		pm_ERRO := 0;
		pm_MSGERRO := '';

		-- Deleta todos os registros de relacionamento do usu�rio
		If pm_TP_CONS = 0 Then

			Open pm_CURSOR1 For

				Select Cod_Usuario, Cod_Filial
				  From Intranet.Usuario_Visao_Cliente
				 Where Cod_Usuario = pm_CODUSU;

		End If;

		Exception When No_Data_Found Then Open pm_CURSOR1 for
		Select Dummy From Dual;
		When Others Then pm_ERRO := SQLCODE; pm_MSGERRO := SQLERRM;

	End PR_SEL_USU_VISAO;



------------------------------FIM da PACKAGE------------------------------
End PCK_Clientes_Teste;
/
