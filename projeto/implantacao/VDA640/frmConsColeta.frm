VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MsMask32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmConsColeta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consultar Coletas"
   ClientHeight    =   6690
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11895
   ControlBox      =   0   'False
   Icon            =   "frmConsColeta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   446
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   793
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtTotal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10830
      TabIndex        =   15
      Top             =   6300
      Width           =   1035
   End
   Begin VB.TextBox txtNumColeta 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6090
      TabIndex        =   12
      Top             =   1575
      Width           =   885
   End
   Begin VB.TextBox txtCodCliente 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4980
      TabIndex        =   10
      Top             =   1560
      Width           =   1035
   End
   Begin VB.ComboBox cboLojas 
      Height          =   315
      Left            =   2220
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   1560
      Width           =   2640
   End
   Begin VB.Frame fraPeriodo 
      Appearance      =   0  'Flat
      Caption         =   "Informe o Per�odo:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   60
      TabIndex        =   3
      Top             =   930
      Width           =   2055
      Begin MSMask.MaskEdBox txtDtInicio 
         Height          =   330
         Left            =   690
         TabIndex        =   4
         Top             =   210
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox txtDtFim 
         Height          =   330
         Left            =   690
         TabIndex        =   5
         Top             =   570
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   582
         _Version        =   393216
         Appearance      =   0
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "99/99/9999"
         PromptChar      =   " "
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "At�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   60
         TabIndex        =   9
         Top             =   660
         Width           =   360
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "De:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   60
         TabIndex        =   8
         Top             =   270
         Width           =   285
      End
   End
   Begin MSComctlLib.ListView lsvColetas 
      Height          =   4305
      Left            =   60
      TabIndex        =   2
      Top             =   1950
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   7594
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   9
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Coleta N�"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "N� Atend"
         Object.Width           =   1852
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Loja"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Cliente"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   4
         Text            =   "CodCliente2"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   5
         Text            =   "Transportadora"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   6
         Text            =   "CodTransp"
         Object.Width           =   0
      EndProperty
      BeginProperty ColumnHeader(8) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   7
         Text            =   "Data"
         Object.Width           =   3969
      EndProperty
      BeginProperty ColumnHeader(9) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   8
         Text            =   "Data2"
         Object.Width           =   0
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   11895
      _ExtentX        =   20981
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsColeta.frx":23D2
      PICN            =   "frmConsColeta.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdBusca_Clientes 
      Height          =   690
      Left            =   11160
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsColeta.frx":30C8
      PICN            =   "frmConsColeta.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Total:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   10320
      TabIndex        =   14
      Top             =   6390
      Width           =   480
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "N� Coleta:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   6105
      TabIndex        =   13
      Top             =   1350
      Width           =   840
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�d Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   4965
      TabIndex        =   11
      Top             =   1350
      Width           =   990
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Lojas:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   210
      Left            =   2250
      TabIndex        =   7
      Top             =   1320
      Width           =   465
   End
End
Attribute VB_Name = "frmConsColeta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Private Declare Function SendMessage Lib "user32.dll" _
'Alias "SendMessageA" (ByVal hwnd As Long, _
'ByVal Msg As Long, ByVal wParam As Long, _
'ByVal lParam As Long) As Long
'Private Const LVM_FIRST = &H1000
'Private Const LVM_SETCOLUMNWIDTH = (LVM_FIRST + 30)
'Private Const LVSCW_AUTOSIZE = -1
'Private Const LVSCW_AUTOSIZE_USEHEADER = -2

Private Declare Function SendMessage _
                Lib "user32" _
                Alias "SendMessageA" _
                (ByVal hwnd As Long, _
                 ByVal wMsg As Long, _
                 ByVal wParam As Long, _
                 lParam As Any) As Long


Private Sub cmdBusca_Clientes_Click()
    If Me.txtDtInicio.ClipText = "" And Me.txtDtFim.ClipText = "" And Me.txtCodCliente = "" And Me.txtNumColeta = "" And Me.cboLojas = "" Then
        MsgBox "Selecione um ou mais par�metros para a pesquisa.", vbInformation, "Aten��o"
        txtDtInicio.SetFocus
        Exit Sub
    End If
    
    If txtDtInicio.ClipText <> "" And Me.txtDtFim.ClipText = "" Then
        MsgBox "Informe a data de T�rmino.", vbInformation, "Aten��o"
        txtDtFim.SetFocus
        Exit Sub
    End If
    
    vBanco.Parameters.Remove "Dt_inicio"
    vBanco.Parameters.Remove "Dt_Fim"
    
    If txtDtInicio.ClipText <> "" Then
       vBanco.Parameters.Add "Dt_inicio", Formata_Data(txtDtInicio), 1
       vBanco.Parameters.Add "Dt_Fim", Formata_Data(txtDtFim), 1
    Else
       vBanco.Parameters.Add "Dt_inicio", "0", 1
       vBanco.Parameters.Add "Dt_Fim", "0", 1
    End If
    
    vBanco.Parameters.Remove "CodCliente"
    If txtCodCliente <> "" Then
       vBanco.Parameters.Add "CodCliente", txtCodCliente, 1
    Else
       vBanco.Parameters.Add "CodCliente", 0, 1
    End If
    
    vBanco.Parameters.Remove "NumColeta"
    If txtNumColeta <> "" Then
       vBanco.Parameters.Add "NumColeta", txtNumColeta, 1
    Else
       vBanco.Parameters.Add "NumColeta", 0, 1
    End If
        
    vBanco.Parameters.Remove "CodLoja"
    If cboLojas <> "" Then
       vBanco.Parameters.Add "CodLoja", cboLojas.ItemData(Me.cboLojas.ListIndex), 1
    Else
       vBanco.Parameters.Add "CodLoja", 0, 1
    End If
    
    Criar_Cursor vBanco.Parameters, "vCursor"
        
    vSql = "Producao.PCK_VDA640.PR_SELECT_CONS_COLETAS(:vCursor, :DT_INICIO, :DT_FIM, :CODLOJA, :CODCLIENTE, :NUMCOLETA)"
        
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
       Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_CONS_COLETAS, entre em contato com o SUPORTE!")
       Exit Sub
    Else
       Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    Preencher_lsv
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Preenche_Cbo_Lojas
    
End Sub

Sub Preenche_Cbo_Lojas()

    On Error GoTo TrataErro

1    Criar_Cursor vBanco.Parameters, "vCursor"
    
2    vBanco.Parameters.Remove "PM_COD_LOJA"
3    vBanco.Parameters.Add "PM_COD_LOJA", 0, 1
    
4    vSql = "Producao.PCK_VDA640.Pr_Select_Lojas(:vCursor, :PM_COD_LOJA)"
    
5    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
6        Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_Lojas, entre em contato com o SUPORTE!")
        Exit Sub
    Else
7        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    cboLojas.Clear
    
8    For i = 1 To vObjOracle.RecordCount
9        cboLojas.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
10        cboLojas.ItemData(cboLojas.NewIndex) = vObjOracle.Fields(0)
11        vObjOracle.MoveNext
    Next
    
12    vObjOracle.Close
13    Set vObjOracle = Nothing

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Cbo_Lojas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub


Sub Preencher_lsv()
    Dim Litem As ListItem
    
    lsvColetas.ListItems.Clear
    
    For i = 1 To vObjOracle.RecordCount
        Set Litem = lsvColetas.ListItems.Add
        Litem = CDbl(vObjOracle("Seq_Coleta"))
        Litem.SubItems(1) = vObjOracle("Num_Atendimento")
        Litem.SubItems(2) = vObjOracle("Loja")
        Litem.SubItems(3) = vObjOracle("Cliente")
        Litem.SubItems(4) = CDbl(Left(vObjOracle("Cliente"), InStr(1, vObjOracle("Cliente"), "-") - 1))
        Litem.SubItems(5) = vObjOracle("Transportadora")
        Litem.SubItems(6) = CDbl(Left(vObjOracle("Transportadora"), InStr(1, vObjOracle("Transportadora"), "-") - 1))
        Litem.SubItems(7) = vObjOracle("Dt_Coleta")
        Litem.SubItems(8) = CDbl(Format(CDate(vObjOracle("Dt_Coleta")), "YYYYMMDD"))
        vObjOracle.MoveNext
    Next
    
    txtTotal = i - 1
    
    AutosizeColumns lsvColetas
    
    lsvColetas.ColumnHeaders(5).width = 0
    lsvColetas.ColumnHeaders(7).width = 0
    lsvColetas.ColumnHeaders(9).width = 0
    
End Sub

'Private Sub lsvColetas_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
'    If UCase(ColumnHeader) = "CLIENTE" Or UCase(ColumnHeader) = "TRANSPORTADORA" Then
'        SortLvwOnLong lsvColetas, ColumnHeader.Index + 1
'    ElseIf UCase(ColumnHeader) = "DATA" Then
'        SortLvwOnDate lsvColetas, ColumnHeader.Index
'    Else
'        SortLvwOnLong lsvColetas, ColumnHeader.Index
'    End If
'End Sub



Private Sub AutosizeColumns(ByVal TargetListView As ListView)

  Const SET_COLUMN_WIDTH    As Long = 4126
  Const AUTOSIZE_USEHEADER  As Long = -2

  Dim lngColumn As Long

  For lngColumn = 0 To (TargetListView.ColumnHeaders.Count - 1)
   
    Call SendMessage(TargetListView.hwnd, _
                     SET_COLUMN_WIDTH, _
                     lngColumn, _
                     ByVal AUTOSIZE_USEHEADER)
        
  Next lngColumn

End Sub


