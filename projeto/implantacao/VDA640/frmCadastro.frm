VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Formul�rio para Cadastros"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9690
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   441
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   646
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   25
      Top             =   6285
      Width           =   9690
      _ExtentX        =   17092
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17039
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   26
      Top             =   810
      Width           =   9510
      _ExtentX        =   16775
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   90
      TabIndex        =   24
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23D2
      PICN            =   "frmCadastro.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stb 
      Height          =   5325
      Left            =   90
      TabIndex        =   27
      Top             =   900
      Width           =   9510
      _ExtentX        =   16775
      _ExtentY        =   9393
      _Version        =   393216
      Tabs            =   4
      Tab             =   2
      TabsPerRow      =   4
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      BackColor       =   6
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Usu�rios"
      TabPicture(0)   =   "frmCadastro.frx":30C8
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame4"
      Tab(0).Control(1)=   "Frame6"
      Tab(0).Control(2)=   "cmdGravar_Atendente"
      Tab(0).Control(3)=   "cmdLimpar_Atendente"
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Tipos"
      TabPicture(1)   =   "frmCadastro.frx":30E4
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "Frame7"
      Tab(1).Control(2)=   "Frame5"
      Tab(1).Control(3)=   "cmdCancelar_Tipos"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Procedimentos"
      TabPicture(2)   =   "frmCadastro.frx":3100
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "lblProc_Reclamacao"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "cmdConsultar_Procedimentos"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "txtProc_Reclamacao"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cmdCancelar_Procedimentos"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "cmdGravar_Procedimentos"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "frmDias"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "Frame2"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).Control(7)=   "Frame1"
      Tab(2).Control(7).Enabled=   0   'False
      Tab(2).Control(8)=   "fra"
      Tab(2).Control(8).Enabled=   0   'False
      Tab(2).Control(9)=   "chkDesativar"
      Tab(2).Control(9).Enabled=   0   'False
      Tab(2).ControlCount=   10
      TabCaption(3)   =   "CA - Intranet"
      TabPicture(3)   =   "frmCadastro.frx":311C
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lstUsuarios"
      Tab(3).Control(1)=   "Label1"
      Tab(3).ControlCount=   2
      Begin VB.ListBox lstUsuarios 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   -74820
         Style           =   1  'Checkbox
         TabIndex        =   65
         Top             =   960
         Width           =   6945
      End
      Begin VB.Frame Frame4 
         Caption         =   "Dep�sitos desta Atendente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   3330
         Left            =   -69600
         TabIndex        =   63
         Top             =   720
         Width           =   3960
         Begin VB.ListBox lstDepAtend 
            Appearance      =   0  'Flat
            Height          =   2955
            Left            =   90
            Style           =   1  'Checkbox
            TabIndex        =   64
            Top             =   270
            Width           =   3795
         End
      End
      Begin VB.CheckBox chkDesativar 
         Caption         =   "Desativar Relacionamento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   62
         Top             =   4380
         Width           =   2535
      End
      Begin VB.Frame Frame3 
         Caption         =   "Detalhe"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2760
         Left            =   -68610
         TabIndex        =   45
         Top             =   1215
         Width           =   2985
         Begin VB.OptionButton optDesativar_Detalhe 
            Appearance      =   0  'Flat
            Caption         =   "Desativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1530
            TabIndex        =   58
            Top             =   1485
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.OptionButton optAtivar_Detalhe 
            Appearance      =   0  'Flat
            Caption         =   "Ativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   495
            TabIndex        =   57
            Top             =   1485
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   825
         End
         Begin Bot�o.cmd cmdNovo_Detalhe 
            Height          =   330
            Left            =   450
            TabIndex        =   48
            TabStop         =   0   'False
            Top             =   315
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Novo"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3138
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdExiste_Detalhe 
            Height          =   330
            Left            =   1440
            TabIndex        =   49
            TabStop         =   0   'False
            Top             =   315
            Width           =   1140
            _ExtentX        =   2011
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Alterar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3154
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdGravar_Detalhe 
            Height          =   690
            Left            =   1170
            TabIndex        =   51
            TabStop         =   0   'False
            ToolTipText     =   "Salvar as informa��es"
            Top             =   1935
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1217
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3170
            PICN            =   "frmCadastro.frx":318C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.TextBox txtDetalhe 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   135
            MaxLength       =   25
            TabIndex        =   46
            Top             =   1035
            Visible         =   0   'False
            Width           =   2715
         End
         Begin VB.ComboBox cmbDetalhe 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   47
            Top             =   1035
            Visible         =   0   'False
            Width           =   2715
         End
         Begin VB.Label lblDetalhe 
            Appearance      =   0  'Flat
            Caption         =   "Descri��o do Detalhe:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   50
            Top             =   810
            Visible         =   0   'False
            Width           =   1950
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "T�pico"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2760
         Left            =   -71715
         TabIndex        =   38
         Top             =   1215
         Width           =   3030
         Begin VB.OptionButton optDesativar_Topico 
            Appearance      =   0  'Flat
            Caption         =   "Desativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1485
            TabIndex        =   56
            Top             =   1485
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.OptionButton optAtivar_Topico 
            Appearance      =   0  'Flat
            Caption         =   "Ativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   450
            TabIndex        =   55
            Top             =   1485
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   825
         End
         Begin Bot�o.cmd cmdNovo_Topico 
            Height          =   330
            Left            =   450
            TabIndex        =   41
            TabStop         =   0   'False
            Top             =   315
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Novo"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3E66
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdExiste_Topico 
            Height          =   330
            Left            =   1440
            TabIndex        =   42
            TabStop         =   0   'False
            Top             =   315
            Width           =   1140
            _ExtentX        =   2011
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Alterar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3E82
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdGravar_Topico 
            Height          =   690
            Left            =   1125
            TabIndex        =   44
            TabStop         =   0   'False
            ToolTipText     =   "Salvar as informa��es"
            Top             =   1935
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1217
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":3E9E
            PICN            =   "frmCadastro.frx":3EBA
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.TextBox txtTopico 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   135
            MaxLength       =   25
            TabIndex        =   40
            Top             =   1035
            Visible         =   0   'False
            Width           =   2760
         End
         Begin VB.ComboBox cmbTopico 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   39
            Top             =   1035
            Visible         =   0   'False
            Width           =   2760
         End
         Begin VB.Label lblTopico 
            Appearance      =   0  'Flat
            Caption         =   "Descri��o do T�pico:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   43
            Top             =   810
            Visible         =   0   'False
            Width           =   1950
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Dados do Atendente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2220
         Left            =   -74850
         TabIndex        =   33
         Top             =   720
         Width           =   4980
         Begin VB.OptionButton optDesativar_Atendente 
            Appearance      =   0  'Flat
            Caption         =   "Desativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1200
            TabIndex        =   61
            Top             =   1710
            Width           =   1050
         End
         Begin VB.OptionButton optAtivar_Atendente 
            Appearance      =   0  'Flat
            Caption         =   "Ativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   300
            TabIndex        =   60
            Top             =   1710
            Value           =   -1  'True
            Width           =   1050
         End
         Begin VB.ComboBox cmbCodigo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":4B94
            Left            =   60
            List            =   "frmCadastro.frx":4B9E
            Sorted          =   -1  'True
            TabIndex        =   59
            Top             =   510
            Width           =   4605
         End
         Begin VB.ComboBox cmbTipo 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmCadastro.frx":4BC2
            Left            =   90
            List            =   "frmCadastro.frx":4BCC
            Sorted          =   -1  'True
            TabIndex        =   21
            Top             =   1170
            Width           =   2490
         End
         Begin VB.Label lblTipo 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Tipo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   75
            TabIndex        =   20
            Top             =   930
            Width           =   420
         End
         Begin VB.Label lblCodigo 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "C�digo:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   60
            TabIndex        =   19
            Top             =   300
            Width           =   615
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Assunto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   2760
         Left            =   -74820
         TabIndex        =   32
         Top             =   1215
         Width           =   3030
         Begin VB.OptionButton optDesativar_Assunto 
            Appearance      =   0  'Flat
            Caption         =   "Desativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   1485
            TabIndex        =   54
            Top             =   1530
            Visible         =   0   'False
            Width           =   1140
         End
         Begin VB.OptionButton optAtivar_Assunto 
            Appearance      =   0  'Flat
            Caption         =   "Ativar"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   450
            TabIndex        =   53
            Top             =   1530
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   825
         End
         Begin Bot�o.cmd cmdGravar_Assunto 
            Height          =   690
            Left            =   1170
            TabIndex        =   17
            TabStop         =   0   'False
            ToolTipText     =   "Salvar as informa��es"
            Top             =   1935
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1217
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":4BF0
            PICN            =   "frmCadastro.frx":4C0C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdNovo_Assunto 
            Height          =   330
            Left            =   450
            TabIndex        =   35
            TabStop         =   0   'False
            Top             =   315
            Width           =   780
            _ExtentX        =   1376
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Novo"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":58E6
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdExiste_Assunto 
            Height          =   330
            Left            =   1440
            TabIndex        =   36
            TabStop         =   0   'False
            Top             =   315
            Width           =   1140
            _ExtentX        =   2011
            _ExtentY        =   582
            BTYPE           =   3
            TX              =   "Alterar"
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmCadastro.frx":5902
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.TextBox txtAssunto 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   135
            MaxLength       =   25
            TabIndex        =   16
            Top             =   1035
            Visible         =   0   'False
            Width           =   2760
         End
         Begin VB.ComboBox cmbAssunto 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   37
            Top             =   1035
            Visible         =   0   'False
            Width           =   2760
         End
         Begin VB.Label lblAssunto 
            Appearance      =   0  'Flat
            Caption         =   "Descri��o do Assunto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   34
            Top             =   810
            Visible         =   0   'False
            Width           =   1950
         End
      End
      Begin VB.Frame fra 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   915
         Left            =   180
         TabIndex        =   31
         Top             =   540
         Width           =   2940
         Begin VB.ComboBox cmbAssunto1 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   1
            Top             =   450
            Width           =   2670
         End
         Begin VB.Label lblAssunto1 
            Appearance      =   0  'Flat
            Caption         =   "Assunto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   0
            Top             =   225
            Width           =   870
         End
      End
      Begin VB.Frame Frame1 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   915
         Left            =   3285
         TabIndex        =   30
         Top             =   540
         Width           =   2940
         Begin VB.ComboBox cmbTopico1 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   3
            Top             =   450
            Width           =   2670
         End
         Begin VB.Label lblTopico1 
            Appearance      =   0  'Flat
            Caption         =   "T�pico:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   2
            Top             =   225
            Width           =   870
         End
      End
      Begin VB.Frame Frame2 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   915
         Left            =   6435
         TabIndex        =   29
         Top             =   540
         Width           =   2940
         Begin VB.ComboBox cmbDetalhe1 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   135
            Sorted          =   -1  'True
            TabIndex        =   5
            Top             =   450
            Width           =   2670
         End
         Begin VB.Label lblDetalhe1 
            Appearance      =   0  'Flat
            Caption         =   "Detalhe:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   180
            TabIndex        =   4
            Top             =   225
            Width           =   870
         End
      End
      Begin VB.Frame frmDias 
         Caption         =   "Qtde. de Dias para:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1590
         Left            =   7335
         TabIndex        =   28
         Top             =   1755
         Width           =   2040
         Begin VB.TextBox txtDias_Solucao 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            MaxLength       =   2
            TabIndex        =   11
            Text            =   "0"
            Top             =   720
            Width           =   510
         End
         Begin VB.TextBox txtDias_Liberacao 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            MaxLength       =   2
            TabIndex        =   13
            Text            =   "0"
            Top             =   1125
            Width           =   510
         End
         Begin VB.TextBox txtDias_Retorno 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1260
            MaxLength       =   2
            TabIndex        =   9
            Text            =   "0"
            Top             =   315
            Width           =   510
         End
         Begin VB.Label lblDias_Solucao 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            Caption         =   "Solu��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   270
            TabIndex        =   10
            Top             =   810
            Width           =   870
         End
         Begin VB.Label lblDias_Retorno 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            Caption         =   "Retorno:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   270
            TabIndex        =   8
            Top             =   405
            Width           =   870
         End
         Begin VB.Label lblDias_Liberacao 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            Caption         =   "Libera��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   225
            TabIndex        =   12
            Top             =   1215
            Width           =   915
         End
      End
      Begin Bot�o.cmd cmdGravar_Procedimentos 
         Height          =   690
         Left            =   7920
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Salvar as informa��es"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":591E
         PICN            =   "frmCadastro.frx":593A
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar_Procedimentos 
         Height          =   690
         Left            =   8685
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Limpar a tela"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":6614
         PICN            =   "frmCadastro.frx":6630
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCancelar_Tipos 
         Height          =   690
         Left            =   -66315
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Limpar a tela"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":6F0A
         PICN            =   "frmCadastro.frx":6F26
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravar_Atendente 
         Height          =   690
         Left            =   -67080
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Salvar as informa��es"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":7800
         PICN            =   "frmCadastro.frx":781C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimpar_Atendente 
         Height          =   690
         Left            =   -66315
         TabIndex        =   23
         TabStop         =   0   'False
         ToolTipText     =   "Limpar a tela"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":84F6
         PICN            =   "frmCadastro.frx":8512
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtProc_Reclamacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2445
         Left            =   135
         MaxLength       =   2000
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   1845
         Width           =   6810
      End
      Begin Bot�o.cmd cmdConsultar_Procedimentos 
         Height          =   690
         Left            =   7155
         TabIndex        =   52
         TabStop         =   0   'False
         ToolTipText     =   "Salvar as informa��es"
         Top             =   4500
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCadastro.frx":8DEC
         PICN            =   "frmCadastro.frx":8E08
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Funcion�rios que ter�o acesso ao CA - Intranet - Inclusao/Altera��o:"
         Height          =   195
         Left            =   -74820
         TabIndex        =   66
         Top             =   720
         Width           =   4860
      End
      Begin VB.Label lblProc_Reclamacao 
         Appearance      =   0  'Flat
         Caption         =   "Procedimento a ser seguido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   6
         Top             =   1620
         Width           =   2535
      End
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vCarregando As Boolean

Public fl_Novo_Assunto As String
Public fl_Novo_Topico As String
Public fl_Novo_Detalhe As String
Public vSituacao As String
Public vCodigo As Double

Private Sub cmbAssunto_Click()
    If cmbAssunto = "" Then Exit Sub
    vCodigo = cmbAssunto.ItemData(cmbAssunto.ListIndex)
    txtAssunto.Text = cmbAssunto.Text
    cmbAssunto.Visible = False
    txtAssunto.Visible = True

End Sub

Private Sub cmbAssunto1_Click()
    lblProc_Reclamacao.Visible = False
    txtProc_Reclamacao.Visible = False
    cmdGravar_Procedimentos.Enabled = False
   
   '-- BUSCAR OS T�PICOS AP�S A ESCOLHA DE ALGUM ASSUNTO
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_TOPICO(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TOPICO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    'Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbTopico1, "COD_TOPICO", "DESC_TOPICO")
    Preenche_Combo frmCadastro.cmbTopico1, vObjOracle

End Sub

Private Sub cmbCodigo_Click()
   '-- BUSCAR OS DADOS DO USU�RIO QUE J� ESTIVER CADASTRADO
    vBanco.Parameters.Remove "PM_CODUSUARIO"
    vBanco.Parameters.Add "PM_CODUSUARIO", cmbCodigo.ItemData(cmbCodigo.ListIndex), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_USUARIO_CA(:vCursor, :PM_CODUSUARIO)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO_CA, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        If vObjOracle!TIPO_ATENDENTE = "C" Then
            cmbTipo.Text = "C - COORDENADOR"
        Else
            cmbTipo.Text = "A - ATENDENTE"
        End If
        
        If vObjOracle!Situacao = 9 Then
            optAtivar_Atendente.Value = True
        Else
            optDesativar_Atendente.Value = True
        End If
    End If
    Preenche_Cbo_Lojas
    Selecionar_Dep_Atendente
End Sub

Private Sub cmbDetalhe_Click()
    
    vCodigo = cmbDetalhe.ItemData(cmbDetalhe.ListIndex)
    txtDetalhe.Text = cmbDetalhe.Text
    cmbDetalhe.Visible = False
    txtDetalhe.Visible = True

End Sub


Private Sub cmbTopico_Click()
    
    vCodigo = cmbTopico.ItemData(cmbTopico.ListIndex)
    txtTopico.Text = cmbTopico.Text
    cmbTopico.Visible = False
    txtTopico.Visible = True

End Sub

Private Sub cmbTopico1_Click()
   
   '-- BUSCAR OS DETALHES AP�S A ESCOLHA DE ALGUM T�PICO
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_DETALHE(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_DETALHE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    'Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbDetalhe1, "COD_DETALHE", "DESC_DETALHE")
    Preenche_Combo frmCadastro.cmbDetalhe1, vObjOracle
    
End Sub

Private Sub cmdCancelar_Procedimentos_Click()
    cmbAssunto1.Text = ""
    cmbTopico1.Clear
    cmbDetalhe1.Clear
    txtProc_Reclamacao.Text = ""
    txtDias_Retorno.Text = ""
    txtDias_Solucao.Text = ""
    txtDias_Liberacao.Text = ""
    
    lblProc_Reclamacao.Visible = False
    txtProc_Reclamacao.Visible = False
    cmdGravar_Procedimentos.Enabled = False
    chkDesativar.Value = 0
    
End Sub

Private Sub cmdCancelar_Tipos_Click()
    
    txtAssunto.Text = ""
    txtTopico.Text = ""
    txtDetalhe.Text = ""
    
    'cmbAssunto.Clear
    cmbAssunto.ListIndex = -1
    cmbTopico.Clear
    cmbDetalhe.Clear
    
    lblAssunto.Visible = False
    lblTopico.Visible = False
    lblDetalhe.Visible = False
    txtAssunto.Visible = False
    txtTopico.Visible = False
    txtDetalhe.Visible = False
    cmbAssunto.Visible = False
    cmbTopico.Visible = False
    cmbDetalhe.Visible = False
    optAtivar_Assunto.Visible = False
    optAtivar_Topico.Visible = False
    optAtivar_Detalhe.Visible = False
    optDesativar_Assunto.Visible = False
    optDesativar_Topico.Visible = False
    optDesativar_Detalhe.Visible = False
    
    optAtivar_Assunto.Value = True
    optAtivar_Topico.Value = True
    optAtivar_Detalhe.Value = True
    
    cmdVoltar.SetFocus

End Sub

Private Sub cmdConsultar_Procedimentos_Click()
        
    vBanco.Parameters.Remove "PM_ASSUNTO"
    vBanco.Parameters.Add "PM_ASSUNTO", Val(cmbAssunto1.ItemData(cmbAssunto1.ListIndex)), 1
    vBanco.Parameters.Remove "PM_TOPICO"
'    vBanco.Parameters.Add "PM_TOPICO", IIf(Mid(cmbTopico1.Text, 1, 3) = "", 0, Val(Mid(cmbTopico1.Text, 1, 3))), 1
    If cmbTopico1.Text = "" Then
        vBanco.Parameters.Add "PM_TOPICO", 0, 1
    Else
        vBanco.Parameters.Add "PM_TOPICO", cmbTopico1.ItemData(cmbTopico1.ListIndex), 1
    End If
    vBanco.Parameters.Remove "PM_DETALHE"
'    vBanco.Parameters.Add "PM_DETALHE", IIf(Mid(cmbDetalhe1.Text, 1, 3) = "", 0, Val(Mid(cmbDetalhe1.Text, 1, 3))), 1
    If cmbDetalhe1.Text = "" Then
        vBanco.Parameters.Add "PM_DETALHE", 0, 1
    Else
        vBanco.Parameters.Add "PM_DETALHE", cmbDetalhe1.ItemData(cmbDetalhe1.ListIndex), 1
    End If
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_ASSUNTO,:PM_TOPICO,:PM_DETALHE,:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PROCEDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Not vObjOracle.EOF Then
        lblProc_Reclamacao.Visible = True
        txtProc_Reclamacao.Visible = True
        cmdGravar_Procedimentos.Enabled = True
        
        txtProc_Reclamacao.Text = vObjOracle!PROC_RECLAMACAO
        txtDias_Retorno.Text = vObjOracle!DIAS_RETORNO
        txtDias_Solucao.Text = vObjOracle!DIAS_SOLUCAO
        txtDias_Liberacao.Text = vObjOracle!DIAS_LIBERACAO

        chkDesativar.Value = IIf(vObjOracle!Status = 0, 0, 1)

    Else
        If vVB_Generica_001.Perguntar("Nenhum procedimento cadastrado. Deseja cadastrar um novo?") = vbYes Then
            lblProc_Reclamacao.Visible = True
            txtProc_Reclamacao.Visible = True
            cmdGravar_Procedimentos.Enabled = True
        Else
            Call cmdCancelar_Procedimentos_Click
        End If
    End If

End Sub

Private Sub cmdExiste_Assunto_Click()

    txtAssunto.Visible = False
    lblAssunto.Visible = True
    cmbAssunto.Visible = True
    optAtivar_Assunto.Visible = True
    optDesativar_Assunto.Visible = True
    fl_Novo_Assunto = "N"
        
   '-- BUSCAR OS ASSUNTOS
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_ASSUNTO(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ASSUNTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    'Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbAssunto, "COD_ASSUNTO", "DESC_ASSUNTO")
    Preenche_Combo frmCadastro.cmbAssunto, vObjOracle
End Sub

Private Sub cmdExiste_Detalhe_Click()

    lblDetalhe.Visible = True
    cmbDetalhe.Visible = True
    txtDetalhe.Visible = False
    optAtivar_Detalhe.Visible = True
    optDesativar_Detalhe.Visible = True
    fl_Novo_Detalhe = "N"
        
   '-- BUSCAR OS ASSUNTOS
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_DETALHE(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_DETALHE, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    'Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbDetalhe, "COD_DETALHE", "DESC_DETALHE")
    Preenche_Combo frmCadastro.cmbDetalhe, vObjOracle
End Sub

Private Sub cmdExiste_Topico_Click()

    lblTopico.Visible = True
    cmbTopico.Visible = True
    txtTopico.Visible = False
    optAtivar_Topico.Visible = True
    optDesativar_Topico.Visible = True
    fl_Novo_Topico = "N"
        
   '-- BUSCAR OS ASSUNTOS
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_BUSCA_TOPICO(:vCursor)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TOPICO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
    
    Preenche_Combo frmCadastro.cmbTopico, vObjOracle
End Sub

Private Sub cmdGravar_Assunto_Click()
    
    If fl_Novo_Assunto = "S" Then
        
        If txtAssunto = "" Then
            MsgBox "Informe o Assunto.", vbInformation, "Aten��o"
            txtAssunto.SetFocus
            Exit Sub
        End If
        
        vBanco.Parameters.Remove "PM_ASSUNTO"
        vBanco.Parameters.Add "PM_ASSUNTO", txtAssunto.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
        vBanco.Parameters.Remove "PM_CODASSUNTO"
        vBanco.Parameters.Add "PM_CODASSUNTO", 0, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "S", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_ASSUNTO(:PM_ASSUNTO, :PM_SITUACAO, :PM_CODASSUNTO, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_ASSUNTO. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    Else
        If optDesativar_Assunto.Value = True Then
            vSituacao = 9
        Else
            vSituacao = 0
        End If
        
        If optDesativar_Assunto.Value = True Then
           Set vObj = vBanco.CreateDynaset("Select count(*) qtd from caclie.r_tipo_procedimento where cod_assunto = " & cmbAssunto.ItemData(cmbAssunto.ListIndex) & " and Status =0 ", &O0)
           If vObj("Qtd") > 0 Then
              MsgBox "N�o � poss�vel desativar este Assunto, pois existem relacionamento que o utilizam.", vbInformation, "Aten��o"
              Exit Sub
            End If
        End If
        
        vBanco.Parameters.Remove "PM_ASSUNTO"
        vBanco.Parameters.Add "PM_ASSUNTO", txtAssunto.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", vSituacao, 1
        vBanco.Parameters.Remove "PM_CODASSUNTO"
        vBanco.Parameters.Add "PM_CODASSUNTO", vCodigo, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "N", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_ASSUNTO(:PM_ASSUNTO, :PM_SITUACAO, :PM_CODASSUNTO, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_ASSUNTO. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    
    End If
    
    Call cmdCancelar_Tipos_Click

End Sub

Private Sub cmdGravar_Atendente_Click()

        vBanco.Parameters.Remove "PM_CODUSUARIO"
        vBanco.Parameters.Add "PM_CODUSUARIO", Val(cmbCodigo.ItemData(cmbCodigo.ListIndex)), 1
        vBanco.Parameters.Remove "PM_TIPO"
        vBanco.Parameters.Add "PM_TIPO", Mid(cmbTipo.Text, 1, 1), 1
    
        If optAtivar_Atendente.Value = True Then
            vBanco.Parameters.Remove "PM_SITUACAO"
            vBanco.Parameters.Add "PM_SITUACAO", 0, 1
        Else
            vBanco.Parameters.Remove "PM_SITUACAO"
            vBanco.Parameters.Add "PM_SITUACAO", 9, 1
        End If
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_USUARIO(:PM_CODUSUARIO, :PM_TIPO, :PM_SITUACAO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_USUARIO. Entre em contato com o SUPORTE.")
            Exit Sub
        Else
            Call vVB_Generica_001.Informar("Grava��o conclu�da com sucesso!")
        End If
        
        
        Atualizar_Depositos_Atendentes
        
        Call cmdLimpar_Atendente_Click
        
End Sub

Private Sub cmdGravar_Detalhe_Click()
    
    If fl_Novo_Detalhe = "S" Then
        
        If txtDetalhe = "" Then
           MsgBox "Informe o Detalhe.", vbInformation, "Aten��o"
           txtDetalhe.SetFocus
           Exit Sub
        End If
        
        If optDesativar_Detalhe.Value = True Then
           Set vObj = vBanco.CreateDynaset("Select count(*) qtd from caclie.r_tipo_procedimento where cod_detalhe = " & cmbDetalhe.ItemData(cmbDetalhe.ListIndex) & " and Status =0 ", &O0)
           If vObj("Qtd") > 0 Then
              MsgBox "N�o � poss�vel desativar este Detalhe, pois existem relacionamento que o utilizam.", vbInformation, "Aten��o"
              Exit Sub
            End If
        End If
        
        
        vBanco.Parameters.Remove "PM_DETALHE"
        vBanco.Parameters.Add "PM_DETALHE", txtDetalhe.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
        vBanco.Parameters.Remove "PM_CODDETALHE"
        vBanco.Parameters.Add "PM_CODDETALHE", 0, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "S", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_DETALHE(:PM_DETALHE, :PM_SITUACAO, :PM_CODDETALHE, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_DETALHE. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    Else
        If optDesativar_Detalhe.Value = True Then
            vSituacao = 9
        Else
            vSituacao = 0
        End If

        vBanco.Parameters.Remove "PM_DETALHE"
        vBanco.Parameters.Add "PM_DETALHE", txtDetalhe.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", vSituacao, 1
        vBanco.Parameters.Remove "PM_CODDETALHE"
        vBanco.Parameters.Add "PM_CODDETALHE", vCodigo, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "N", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_DETALHE(:PM_DETALHE, :PM_SITUACAO, :PM_CODDETALHE, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_DETALHE. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    
    End If
    
    Call cmdCancelar_Tipos_Click

End Sub

Private Sub cmdGravar_Procedimentos_Click()
    
    Dim vAssunto As Integer
    Dim vTopico As Integer
    Dim vDetalhe As Integer
    Dim vObj As Object
    
    If cmbAssunto1.Text = "" Then
        Call vVB_Generica_001.Informar("Escolha o assunto.")
        cmbAssunto1.SetFocus
        Exit Sub
    End If
    
    If txtProc_Reclamacao.Text = "" Then
        Call vVB_Generica_001.Informar("Digite o procedimento para reclama��o.")
        txtProc_Reclamacao.SetFocus
        Exit Sub
    End If
    
    If txtDias_Liberacao.Text = "" Or Val(txtDias_Liberacao.Text) = 0 Then
        Call vVB_Generica_001.Informar("Digite a qtde. de dias para libera��o.")
        txtDias_Liberacao.SetFocus
        Exit Sub
    End If
    
    If cmbAssunto1.ListIndex = -1 Then
        vAssunto = 0
    Else
        vAssunto = cmbAssunto1.ItemData(cmbAssunto1.ListIndex)
    End If
    If cmbTopico1.ListIndex = -1 Then
        vTopico = 0
    Else
        vTopico = cmbTopico1.ItemData(cmbTopico1.ListIndex)
    End If
    If cmbDetalhe1.ListIndex = -1 Then
        vDetalhe = 0
    Else
        vDetalhe = cmbDetalhe1.ItemData(cmbDetalhe1.ListIndex)
    End If
    
    vBanco.Parameters.Remove "PM_ASSUNTO"
    vBanco.Parameters.Add "PM_ASSUNTO", vAssunto, 1
    
    vBanco.Parameters.Remove "PM_TOPICO"
    vBanco.Parameters.Add "PM_TOPICO", vTopico, 1

    vBanco.Parameters.Remove "PM_DETALHE"
    vBanco.Parameters.Add "PM_DETALHE", vDetalhe, 1
    
    If chkDesativar.Value = 1 Then
       Set vObj = vBanco.CreateDynaset("Select count(*) qtd from caclie.atendimento_ca where cod_assunto = " & vAssunto & " and cod_topico = " & vTopico & " and cod_detalhe = " & vDetalhe & " and Situacao <> 'L'", &O0)
       If vObj("Qtd") > 0 Then
          MsgBox "N�o � poss�vel desativar este relacionamento, pois existem registros n�o liberados que o utilizam.", vbInformation, "Aten��o"
          Exit Sub
        End If
    End If
    
    vBanco.Parameters.Remove "PM_RECLAMACAO"
    vBanco.Parameters.Add "PM_RECLAMACAO", txtProc_Reclamacao.Text, 1
    vBanco.Parameters.Remove "PM_INFORMACAO"
    vBanco.Parameters.Add "PM_INFORMACAO", " ", 1
    vBanco.Parameters.Remove "PM_RETORNO"
    vBanco.Parameters.Add "PM_RETORNO", Val(txtDias_Retorno.Text), 1
    vBanco.Parameters.Remove "PM_SOLUCAO"
    vBanco.Parameters.Add "PM_SOLUCAO", Val(txtDias_Solucao.Text), 1
    vBanco.Parameters.Remove "PM_LIBERACAO"
    vBanco.Parameters.Add "PM_LIBERACAO", Val(txtDias_Liberacao.Text), 1
    
    Dim vDesativar As Byte
    If chkDesativar.Value = 1 Or Me.chkDesativar.Value = True Then
        vDesativar = 9
    ElseIf Me.chkDesativar.Value = 0 Or Me.chkDesativar.Value = False Then
        vDesativar = 0
    End If
    
    vBanco.Parameters.Remove "PM_DESATIVAR"
    vBanco.Parameters.Add "PM_DESATIVAR", vDesativar, 1
    
    'Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_ATUALIZA_PROCEDIMENTO(:PM_ASSUNTO, " & _
           ":PM_TOPICO, :PM_DETALHE, :PM_RECLAMACAO, :PM_INFORMACAO, " & _
           ":PM_RETORNO, :PM_SOLUCAO, :PM_LIBERACAO, :PM_DESATIVAR)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_PROCEDIMENTO. Entre em contato com o SUPORTE.")
        Exit Sub
    End If
    
    Call cmdCancelar_Procedimentos_Click
    
    
    '-- CARREGA OS ASSUNTOS
    Criar_Cursor vBanco.Parameters, "vCursor"
          
    vSql = "Producao.PCK_VDA640.PR_BUSCA_ASSUNTO(:vCursor)"
          
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ASSUNTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("VCURSOR").Value
    End If
          
              
    Preenche_Combo frmVDA640.cmbAssunto, vObjOracle

End Sub

Private Sub cmdGravar_Topico_Click()
    
    If fl_Novo_Topico = "S" Then
        
        If Me.txtTopico = "" Then
            MsgBox "Informe o T�pico.", vbInformation, "Aten��o"
            txtTopico.SetFocus
            Exit Sub
        End If
        
        If optDesativar_Topico.Value = True Then
           Set vObj = vBanco.CreateDynaset("Select count(*) qtd from caclie.r_tipo_procedimento where cod_topico = " & cmbTopico.ItemData(cmbTopico.ListIndex) & " and Status =0 ", &O0)
           If vObj("Qtd") > 0 Then
              MsgBox "N�o � poss�vel desativar este T�pico, pois existem relacionamento que o utilizam.", vbInformation, "Aten��o"
              Exit Sub
            End If
        End If
                
        
        vBanco.Parameters.Remove "PM_TOPICO"
        vBanco.Parameters.Add "PM_TOPICO", txtTopico.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", 0, 1
        vBanco.Parameters.Remove "PM_CODTOPICO"
        vBanco.Parameters.Add "PM_CODTOPICO", 0, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "S", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_TOPICO(:PM_TOPICO, :PM_SITUACAO, :PM_CODTOPICO, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_TOPICO. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    Else
        If optDesativar_Topico.Value = True Then
            vSituacao = 9
        Else
            vSituacao = 0
        End If
        
        vBanco.Parameters.Remove "PM_TOPICO"
        vBanco.Parameters.Add "PM_TOPICO", txtTopico.Text, 1
        vBanco.Parameters.Remove "PM_SITUACAO"
        vBanco.Parameters.Add "PM_SITUACAO", vSituacao, 1
        vBanco.Parameters.Remove "PM_CODTOPICO"
        vBanco.Parameters.Add "PM_CODTOPICO", vCodigo, 1
        vBanco.Parameters.Remove "PM_NOVO"
        vBanco.Parameters.Add "PM_NOVO", "N", 1
        
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_ATUALIZA_TOPICO(:PM_TOPICO, :PM_SITUACAO, :PM_CODTOPICO, :PM_NOVO)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_TOPICO. Entre em contato com o SUPORTE.")
            Exit Sub
        End If
    
    End If
    
    Call cmdCancelar_Tipos_Click

End Sub

Private Sub cmdLimpar_Atendente_Click()
    
    cmbCodigo.Text = ""
    cmbTipo.Text = ""
    optAtivar_Atendente.Value = True
        
End Sub

Private Sub cmdNovo_Assunto_Click()
    
    lblAssunto.Visible = True
    cmbAssunto.Visible = False
    txtAssunto.Visible = True
    optAtivar_Assunto.Visible = False
    optDesativar_Assunto.Visible = False
    fl_Novo_Assunto = "S"
    
End Sub

Private Sub cmdNovo_Detalhe_Click()

    lblDetalhe.Visible = True
    cmbDetalhe.Visible = False
    txtDetalhe.Visible = True
    optAtivar_Detalhe.Visible = False
    optDesativar_Detalhe.Visible = False
    fl_Novo_Detalhe = "S"

End Sub

Private Sub cmdNovo_Topico_Click()

    lblTopico.Visible = True
    cmbTopico.Visible = False
    txtTopico.Visible = True
    optAtivar_Topico.Visible = False
    optDesativar_Topico.Visible = False
    fl_Novo_Topico = "S"

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
              
    stb.Tab = 0
        
    fl_Novo_Assunto = "N"
    fl_Novo_Topico = "N"
    fl_Novo_Detalhe = "N"
    
    lblProc_Reclamacao.Visible = False
    txtProc_Reclamacao.Visible = False
    cmdGravar_Procedimentos.Enabled = False
    
    '-- BUSCAR OS USU�RIOS CADASTRADOS NA INTRANET
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     vSql = "Producao.PCK_VDA640.PR_BUSCA_USUARIO(:vCursor)"
     
     If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
         Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
         Exit Sub
     Else
         Set vObjOracle = vBanco.Parameters("VCURSOR").Value
     End If
     
     Preenche_Combo frmCadastro.cmbCodigo, vObjOracle

End Sub


Private Sub lstUsuarios_Click()
        
    If vCarregando = True Then Exit Sub
        
    vBanco.Parameters.Remove "COD_USUARIO"
    vBanco.Parameters.Add "Cod_usuario", lstUsuarios.ItemData(lstUsuarios.ListIndex), 1
    
    vBanco.Parameters.Remove "Selecionado"
    vBanco.Parameters.Add "Selecionado", IIf(lstUsuarios.Selected(lstUsuarios.ListIndex) = True, 1, 0), 1
        
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA640.Pr_Atualiza_CA_Intranet(:Cod_usuario, :Selecionado)")
        
    If vErro <> "" Then
        MsgBox vErro
    End If
        
End Sub

Private Sub stb_Click(PreviousTab As Integer)
    If stb.Tab = 2 Then
       '-- BUSCAR OS ASSUNTOS PARA CADASTRAR UM PROCEDIMENTO
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_BUSCA_ASSUNTO_ATIVO(:vCursor)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ASSUNTO_ATIVO, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        Preenche_Combo frmCadastro.cmbAssunto1, vObjOracle
    End If
    
    If stb.Tab = 3 Then
        Criar_Cursor vBanco.Parameters, "vCursor"
        
        vSql = "Producao.PCK_VDA640.PR_SELECT_CA_INTRANET(:vCursor)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_CA_INTRANET, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        lstUsuarios.Clear
        
        For i = 0 To vObjOracle.RecordCount - 1
            lstUsuarios.AddItem vObjOracle!Cod_Usuario & " - " & vObjOracle!Nome_Usuario
            lstUsuarios.ItemData(lstUsuarios.NewIndex) = vObjOracle!Cod_Usuario
            vObjOracle.MoveNext
        Next
        
    End If
    
    vCarregando = True
    Marcar_usuarios_Intranet
    vCarregando = False
End Sub

Private Sub txtAssunto_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtDetalhe_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Private Sub txtProc_Reclamacao_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub

Private Sub txtTopico_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub

Sub Preenche_Cbo_Lojas()

1         On Error GoTo TrataErro

2     Criar_Cursor vBanco.Parameters, "vCursor"
    
3     vBanco.Parameters.Remove "PM_COD_LOJA"
4     vBanco.Parameters.Add "PM_COD_LOJA", 0, 1
    
5     vSql = "Producao.PCK_VDA640.Pr_Select_Lojas(:vCursor, :PM_COD_LOJA)"
    
6     If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
7        Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_Lojas, entre em contato com o SUPORTE!")
8       Exit Sub
9         Else
10       Set vObjOracle = vBanco.Parameters("vCursor").Value
11        End If
    
12        lstDepAtend.Clear
    
13    For i = 1 To vObjOracle.RecordCount
14       lstDepAtend.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
15       lstDepAtend.ItemData(lstDepAtend.NewIndex) = vObjOracle.Fields(0)
16       vObjOracle.MoveNext
17         Next
    
18    vObjOracle.Close
19    Set vObjOracle = Nothing

TrataErro:
20        If Err.Number <> 0 Then
21      MsgBox "Sub: Preenche_Cbo_Lojas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Sub

Sub Selecionar_Dep_Atendente()
    On Error GoTo TrataErro

    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "PM_COD_USUARIO"
    vBanco.Parameters.Add "PM_COD_USUARIO", cmbCodigo.ItemData(cmbCodigo.ListIndex), 1
   
    vSql = "Producao.PCK_VDA640.Pr_Select_Depositos_Atend(:vCursor, :PM_COD_USUARIO)"
   
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_Depositos_Atend, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    'Tirar as marcadas
    For i = 0 To lstDepAtend.ListCount - 1
        lstDepAtend.Selected(i) = False
    Next
    
    While vObjOracle.EOF = False
        For i = 0 To lstDepAtend.ListCount - 1
            If Val(lstDepAtend.List(i)) = Val(vObjOracle!COD_LOJA) Then
               lstDepAtend.Selected(i) = True
               Exit For
            End If
         Next
    
        vObjOracle.MoveNext
    Wend
    
    vObjOracle.Close
    Set vObjOracle = Nothing

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Cbo_Lojas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
    
End Sub

Sub Atualizar_Depositos_Atendentes()
    
    vBanco.Parameters.Remove "COD_USUARIO"
    vBanco.Parameters.Add "Cod_usuario", cmbCodigo.ItemData(cmbCodigo.ListIndex), 1
    
    For i = 0 To lstDepAtend.ListCount - 1
        
        vBanco.Parameters.Remove "Deposito"
        vBanco.Parameters.Add "Deposito", lstDepAtend.ItemData(i), 1
        
        vBanco.Parameters.Remove "Selecionado"
        vBanco.Parameters.Add "Selecionado", IIf(lstDepAtend.Selected(i) = True, 1, 0), 1
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA640.Pr_Insert_Dep_Atend(:Cod_usuario, :Deposito,:Selecionado)")
        
        If vErro <> "" Then
            MsgBox vErro
        End If
        
    Next
    
End Sub

Sub Marcar_usuarios_Intranet()
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.Parameters.Remove "COD_SOFT"
    vBanco.Parameters.Add "COD_SOFT", 1107, 1
    
    vSql = "Producao.PCK_VDA640.Pr_Select_CA_Intranet_Marcados(:vCursor, :COD_SOFT)"
   
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_CA_Intranet_Marcados, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    'Tirar as marcadas
    If lstUsuarios.SelCount > 0 Then
        For i = 0 To lstUsuarios.ListCount - 1
            lstUsuarios.Selected(i) = False
        Next
    End If
    
    While vObjOracle.EOF = False
        For i = 0 To lstUsuarios.ListCount - 1
            If Val(lstUsuarios.List(i)) = Val(vObjOracle!VL_PARAMETRO) Then
               lstUsuarios.Selected(i) = True
               Exit For
            End If
         Next
    
        vObjOracle.MoveNext
    Wend
    
End Sub
