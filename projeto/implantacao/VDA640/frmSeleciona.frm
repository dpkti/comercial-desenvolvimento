VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmSeleciona 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista de e-mails "
   ClientHeight    =   3015
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3015
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstEmail 
      Appearance      =   0  'Flat
      Height          =   2055
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   630
      Left            =   3840
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   2280
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1111
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSeleciona.frx":0000
      PICN            =   "frmSeleciona.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEnviar_Email 
      Height          =   630
      Left            =   3000
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Enviar e-mail"
      Top             =   2280
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1111
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSeleciona.frx":0CF6
      PICN            =   "frmSeleciona.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmSeleciona"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private intCodLoja As Integer
Private strListaEmail As String

Public Property Let CodigoLoja(Valor As Integer)
    intCodLoja = Valor
End Property

Public Property Get CodigoLoja() As Integer
    CodigoLoja = intCodLoja
End Property

Private Sub cmdEnviar_Email_Click()
    
    strListaEmail = ""
    
    For i = 0 To lstEmail.ListCount - 1
        
        If lstEmail.Selected(i) Then
            If strListaEmail <> "" Then
                strListaEmail = strListaEmail & ";"
            End If
            strListaEmail = strListaEmail & lstEmail.List(i)
        End If
    
    Next
    
    Unload Me
    
End Sub

Private Sub cmdSair_Click()
    strListaEmail = ""
    Unload Me
End Sub

Public Property Get ListaEmail() As String
    ListaEmail = strListaEmail
End Property

Private Sub Form_Load()

    On Error GoTo TrataErro

    Criar_Cursor vBanco.Parameters, "vCursor"

    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", intCodLoja, 1

    vSql = "Producao.PCK_VDA640.PR_SELECT_EMAIL_CARGO(:vCursor, :PM_COD_LOJA)"

    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_EMAIL_CARGO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    lstEmail.Clear

    For i = 1 To vObjOracle.RecordCount
        lstEmail.AddItem vObjOracle.Fields(1).Value
        vObjOracle.MoveNext
    Next
        

    vObjOracle.Close
    Set vObjOracle = Nothing

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Preenche_Cbo_Lojas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub
