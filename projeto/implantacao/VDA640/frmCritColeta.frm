VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmCritColeta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Emiss�o de Autoriza��o de Coleta"
   ClientHeight    =   2670
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3555
   Icon            =   "frmCritColeta.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   178
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   237
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.ProgressBar PB 
      Height          =   135
      Left            =   30
      TabIndex        =   22
      Top             =   810
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   238
      _Version        =   393216
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.CheckBox chkRegerar 
      Appearance      =   0  'Flat
      Caption         =   "Regerar Coletas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   225
      Left            =   1830
      TabIndex        =   18
      Top             =   2370
      Width           =   1665
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1035
      Left            =   2220
      Picture         =   "frmCritColeta.frx":23D2
      ScaleHeight     =   1005
      ScaleWidth      =   1215
      TabIndex        =   16
      Top             =   1080
      Visible         =   0   'False
      Width           =   1245
   End
   Begin VB.Frame fraCliDia 
      Caption         =   "Por Cliente e por Dia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   855
      Left            =   660
      TabIndex        =   12
      Top             =   2760
      Visible         =   0   'False
      Width           =   2175
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   690
         TabIndex        =   14
         Top             =   450
         Width           =   960
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�d Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   690
         TabIndex        =   13
         Top             =   240
         Width           =   990
      End
   End
   Begin VB.Frame fraTranspDia 
      Caption         =   "Transportadora Por Dia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   885
      Left            =   30
      TabIndex        =   9
      Top             =   2730
      Visible         =   0   'False
      Width           =   3465
      Begin VB.ComboBox cboTransportadora 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   450
         Width           =   3360
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Transportadoras:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   10
         Top             =   210
         Width           =   1380
      End
   End
   Begin VB.Frame fraLojaDia 
      Caption         =   "Loja Por Dia"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   885
      Left            =   30
      TabIndex        =   7
      Top             =   2730
      Visible         =   0   'False
      Width           =   3465
      Begin VB.ComboBox cboLojas 
         Height          =   315
         Left            =   60
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   480
         Width           =   3360
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Lojas:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   210
         Left            =   90
         TabIndex        =   8
         Top             =   240
         Width           =   465
      End
   End
   Begin VB.Frame fraOpcoes 
      Caption         =   "Op��es"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1245
      Left            =   30
      TabIndex        =   2
      Top             =   990
      Width           =   3465
      Begin VB.OptionButton optCliDia 
         Appearance      =   0  'Flat
         Caption         =   "Por cliente e por dia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   45
         TabIndex        =   6
         Top             =   960
         Width           =   2565
      End
      Begin VB.OptionButton optTranspDia 
         Appearance      =   0  'Flat
         Caption         =   "Por transp e por dia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   45
         TabIndex        =   5
         Top             =   720
         Width           =   1935
      End
      Begin VB.OptionButton optDia 
         Appearance      =   0  'Flat
         Caption         =   "Todas do dia"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   45
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   3330
      End
      Begin VB.OptionButton optLojaDia 
         Appearance      =   0  'Flat
         Caption         =   "Por dia e por loja"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   45
         TabIndex        =   3
         Top             =   480
         Width           =   2175
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   750
      Width           =   3465
      _ExtentX        =   6112
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   705
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   15
      Width           =   780
      _ExtentX        =   1376
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCritColeta.frx":6104
      PICN            =   "frmCritColeta.frx":6120
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdColeta 
      Height          =   705
      Left            =   2730
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Gerar Arquivos de Coleta"
      Top             =   30
      Width           =   780
      _ExtentX        =   1376
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCritColeta.frx":6DFA
      PICN            =   "frmCritColeta.frx":6E16
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComCtl2.DTPicker DtDia 
      Height          =   315
      Left            =   450
      TabIndex        =   19
      Top             =   2310
      Width           =   1275
      _ExtentX        =   2249
      _ExtentY        =   556
      _Version        =   393216
      CustomFormat    =   "dd/MM/yy"
      Format          =   58130435
      CurrentDate     =   38401
   End
   Begin Bot�o.cmd cmdEnviar_Email 
      Height          =   705
      Left            =   1890
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Enviar e-mail"
      Top             =   30
      Width           =   780
      _ExtentX        =   1376
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCritColeta.frx":76F0
      PICN            =   "frmCritColeta.frx":770C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Data"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   30
      TabIndex        =   20
      Top             =   2400
      Width           =   375
   End
End
Attribute VB_Name = "frmCritColeta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vObjOracleColetas As Object


Private Sub cmdColeta_Click()
    On Error GoTo TrataErro

    PB.Min = 0
    
    Dim vObjOracle9 As Object
    Dim fs As FileSystemObject
    Set fs = New FileSystemObject
    
    If fs.FolderExists("C:\COLETA") = False Then MkDir "C:\Coleta"
    If fs.FolderExists("C:\COLETA\BACKUP") = False Then MkDir "C:\Coleta\Backup"
    
    vBanco.Parameters.Remove "PM_DIA"
    vBanco.Parameters.Add "PM_DIA", CDate(DtDia), 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vSql = "Producao.PCK_VDA640.PR_SELECT_LOJAS_COLETA(:vCursor, :PM_DIA)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
        PB.Value = 0
        Exit Sub
    Else
        Set vObjOracle9 = vBanco.Parameters("VCURSOR").Value
    End If
    
    PB.Max = IIf(vObjOracle9.RecordCount = 0, 1, vObjOracle9.RecordCount)
    PB.Value = 0
    
    While vObjOracle9.EOF = False
        
        PB.Value = PB.Value + 1
        
        If chkRegerar.Value = 1 Then
            Kill "C:\Coleta\" & vObjOracle9!COD_LOJA & "_*.HTML"
        End If
        
        If optDia.Value = True Then
            GerarPDF vObjOracle9!COD_LOJA, Format(DtDia, "YYYYMMDD"), Formata_Data(DtDia), 0, 0
        
        ElseIf optLojaDia.Value = True Then
            If cboLojas.ItemData(cboLojas.ListIndex) = vObjOracle9!COD_LOJA Then
               GerarPDF vObjOracle9!COD_LOJA, Format(DtDia, "YYYYMMDD"), Formata_Data(DtDia), 0, 0
            End If
        
        ElseIf optTranspDia.Value = True Then
           GerarPDF vObjOracle9!COD_LOJA, Format(DtDia, "YYYYMMDD"), Formata_Data(DtDia), cboTransportadora.ItemData(cboTransportadora.ListIndex), 0
        
        ElseIf optCliDia.Value = True Then
           GerarPDF vObjOracle9!COD_LOJA, Format(DtDia, "YYYYMMDD"), Formata_Data(DtDia), 0, txtCodCliente
        End If
        
        vObjOracle9.MoveNext
    
    Wend

    MsgBox "Conclu�do", vbInformation, "Aten��o"
    PB.Value = 0

TrataErro:
    If Err.Number = 53 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
    End If
End Sub

Private Sub cmdEnviar_Email_Click()
    frmEmail.Show 1
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()

    Me.Height = 3045

    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.width - Me.width) / 2
    
    Preencher_Transportadora

    DtDia.Value = Date

End Sub

Sub Preencher_Transportadora()
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     vSql = "Producao.PCK_VDA640.PR_SELECT_TRANSP(:vCursor)"
     
     If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
         Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
         Exit Sub
     Else
         Set vObjOracle = vBanco.Parameters("VCURSOR").Value
     End If

    cboTransportadora.AddItem "<Nenhum>"

    While vObjOracle.EOF = False
        cboTransportadora.AddItem Trim(vObjOracle!nome_transp)
        cboTransportadora.ItemData(cboTransportadora.NewIndex) = vObjOracle!COD_Transp
        vObjOracle.MoveNext
    Wend

End Sub

Private Sub optCliDia_Click()
    Me.Height = 4020
    Frames False, False, True
    cboLojas.ListIndex = -1
    cboTransportadora.ListIndex = -1
End Sub

Private Sub optDia_Click()
    Me.Height = 3045
    Frames False, False, False
    cboLojas.ListIndex = -1
    cboTransportadora.ListIndex = -1
End Sub

Private Sub optLojaDia_Click()
    Me.Height = 4020
    Frames True, False, False
    Preenche_Cbo_Lojas
    cboLojas.ListIndex = -1
    cboTransportadora.ListIndex = -1
End Sub

Sub Frames(LojaDia As Boolean, TranspDia As Boolean, CliDia As Boolean)

    fraLojaDia.Visible = LojaDia
    fraTranspDia.Visible = TranspDia
    fraCliDia.Visible = CliDia

End Sub

Private Sub optTranspDia_Click()
    Me.Height = 4020
    Frames False, True, False
    cboLojas.ListIndex = -1
    cboTransportadora.ListIndex = -1
End Sub

Sub GerarPDF(pLoja As Integer, pDia As String, pDia2 As String, pTransp As Double, pCliente As Double)
   On Error GoTo TrataErro
    Dim vObjOracleTransp As Object
    Dim vObjOracleloja As Object
    Dim P As Printer
    Dim vImpressoraAtual As String
    Dim vNome_Arquivo_Pdf As String
    Dim vQtdColetas As Byte
    Dim CodCliente As String
    Dim NomeCliente As String
    Dim Endereco As String
    Dim NFDevol As String
    Dim Valor As String
    Dim Telefone As String
    Dim NomeContato As String
    Dim Mudou As Double
    Dim T As Integer
    Dim vNomeTransp As String
    Dim vSeqColeta As String
    Dim Cidade As String
    Dim DDD As String
    
    Set vObjOracleColetas = Nothing
    
    vBanco.Parameters.Remove "PM_TRANSP"
    vBanco.Parameters.Add "PM_TRANSP", pTransp, 1
    
    vBanco.Parameters.Remove "PM_LOJA"
    vBanco.Parameters.Add "PM_LOJA", pLoja, 1
    
    vBanco.Parameters.Remove "PM_DIA"
    vBanco.Parameters.Add "PM_DIA", pDia2, 1
    
    vBanco.Parameters.Remove "PM_CLIENTE"
    vBanco.Parameters.Add "PM_CLIENTE", pCliente, 1
    
    vSql = "Producao.PCK_VDA640.PR_SELECT_TRANSP_COLETA(:vCursor, :PM_DIA, :PM_LOJA, :PM_TRANSP, :PM_CLIENTE)"
     
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
       Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_TRANSP_COLETA, entre em contato com o SUPORTE!")
       Exit Sub
    Else
       Set vObjOracleTransp = vBanco.Parameters("VCURSOR").Value
    End If
    
    If Dir("C:\Coleta\ArqHtml.TXT") <> "" Then Kill "C:\Coleta\ArqHtml.TXT"
    
    Open "C:\Coleta\ArqHtml.TXT" For Output As #1
    
    For T = 1 To vObjOracleTransp.RecordCount
        
        vBanco.Parameters.Remove "PM_STATUS"
        vBanco.Parameters.Add "PM_STATUS", chkRegerar.Value, 1
        
        vBanco.Parameters.Remove "PM_LOJA"
        vBanco.Parameters.Add "PM_LOJA", pLoja, 1
        
        vBanco.Parameters.Remove "PM_TRANSP"
        vBanco.Parameters.Add "PM_TRANSP", IIf(IsNull(vObjOracleTransp!COD_Transp), 0, vObjOracleTransp!COD_Transp), 1
            
        vBanco.Parameters.Remove "PM_DIA"
        vBanco.Parameters.Add "PM_DIA", pDia2, 1
    
        vBanco.Parameters.Remove "PM_CLIENTE"
        vBanco.Parameters.Add "PM_CLIENTE", pCliente, 1
    
        vBanco.Parameters.Remove "PM_NOTA"
        vBanco.Parameters.Add "PM_NOTA", CLng(vObjOracleTransp!NF_ORIGEM), 1
    
        vBanco.Parameters.Remove "PM_TIPO_END"
        vBanco.Parameters.Add "PM_TIPO_END", 0, 2
    
        vSql = "Producao.PCK_VDA640.PR_SELECT_COLETA(:vCursor, :PM_LOJA, :PM_DIA, :PM_TRANSP, :PM_CLIENTE, :PM_STATUS, :PM_NOTA, :PM_TIPO_END)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        If vErro <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_COLETA, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracleColetas = vBanco.Parameters("VCURSOR").Value
        End If
    
        If vObjOracleColetas.RecordCount = 0 Or vObjOracleColetas.Fields(0).Name = "DUMMY" Then
            GoTo FIM:
        End If
    
        vNomeTransp = IIf(IsNull(vObjOracleColetas!nome_transp), vObjOracleColetas!Nome_transportadora, vObjOracleColetas!nome_transp)
        vSeqColeta = Format(vObjOracleColetas!Seq_Coleta, "000000")
        
        'Loja
        vBanco.Parameters.Remove "PM_COD_LOJA"
        vBanco.Parameters.Add "PM_COD_LOJA", vObjOracleColetas!COD_LOJA, 1
        vSql = "Producao.PCK_VDA640.PR_SELECT_LOJAS(:vCursor, :PM_COD_LOJA)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        If vErro <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_LOJA, entre em contato com o SUPORTE!")
            Close #1
            Exit Sub
        Else
            Set vObjOracleloja = vBanco.Parameters("VCURSOR").Value
        End If
    
        Print #1, "<HTML>"
        Print #1, "<HEAD>"
        Print #1, "<style>"
        Print #1, "    TD{font-family:Arial;font-size:8px;}"
        Print #1, "</style>"
        Print #1, "</HEAD>"
        Print #1, "<BODY class=page>"
    
        Print #1, "<TABLE Cellpadding=0 cellspacing=0 border=0 width=100%>"
        
            Print #1, "<TR>"
                Print #1, "<TD>"
                    Print #1, "<IMG HEIGHT:85px height=85 src=file://F:\USR\Padroes\Imagens\Logotipo\DPK.bmp align=left>"
                Print #1, "</TD>"
                
                Print #1, "<TD Align=Center width=60%>"
                    Print #1, "<FONT Size=5>SOLICITA��O DE COLETA</FONT>"
                Print #1, "</TD>"
            
                Print #1, "<TD width=20%>"
                    Print #1, "<FONT Size=2>COLETA N�: " & vSeqColeta
                Print #1, "</TD>"
            Print #1, "</TR>"
            
        Print #1, "</TABLE>"
        
        Print #1, "<TABLE Cellpadding=0 cellspacing=0 border=0 width=100%>"
        
            Print #1, "<TR>"
                Print #1, "<TD>"
                    Print #1, "<FONT Size=2>" & vObjOracleloja!Nome_Fantasia & Space(225 - (Len(vObjOracleloja!Nome_Fantasia) + 25))
                Print #1, "</TD>"
            Print #1, "<TR>"
            
            Print #1, "<TR>"
                Print #1, "<TD>"
                    Print #1, "<FONT Size=2>Endereco"
                Print #1, "</TD>"
            Print #1, "<TR>"
                    
            Print #1, "<TR>"
                Print #1, "<TD>"
                    Print #1, "<FONT Size=2>" & vObjOracleloja!Bairro & " - " & vObjOracleloja!NOME_CIDADE
                Print #1, "</TD>"
            Print #1, "<TR>"
                    
            Print #1, "<TR>"
                Print #1, "<TD>"
                    Print #1, "<FONT Size=2>CEP: " & vObjOracleloja!CEP & " Fone: " & vObjOracleColetas!DDD1 & " - " & vObjOracleColetas!FONE1
                Print #1, "</TD>"
            Print #1, "<TR>"
            
            Print #1, "<TR>"
                Print #1, "<TD>&nbsp;</TD>"
            Print #1, "</TR>"
            
            Print #1, "<TR>"
                Print #1, "<TD COLSPAN=2>"
                    Print #1, "<FONT Size=2>Transportadora: " & vNomeTransp
                Print #1, "</TD>"
            Print #1, "</TR>"
        
        Print #1, "</TABLE><br>"
    
        
        CriarTabelaInicial
        
        'vObjOracle.MoveFirst
        
        For i = 1 To vObjOracleColetas.RecordCount

            If vObjOracleColetas!COD_LOJA = pLoja Then
                
                CodCliente = vObjOracleColetas!cod_cliente
                NomeCliente = vObjOracleColetas!NOME_CLIENTE
                Endereco = vObjOracleColetas!Endereco
                NFDevol = IIf(IsNull(vObjOracleColetas!NF_DEVOLUCAO), "&nbsp;", vObjOracleColetas!NF_DEVOLUCAO)
                Cidade = vObjOracleColetas!NOME_CIDADE

                If vBanco.Parameters("PM_TIPO_END").Value = 0 Then
                    DDD = vObjOracleColetas!DDD
                    Telefone = vObjOracleColetas!FONE
                Else
                    DDD = vObjOracleColetas!DDD1
                    Telefone = vObjOracleColetas!FONE1
                End If
                
                If IsNull(vObjOracleColetas!NOME_CONTATO) Then
                    NomeContato = ""
                Else
                    NomeContato = vObjOracleColetas!NOME_CONTATO
                End If
                Valor = IIf(IsNull(vObjOracleColetas!VL_NOTA), 0, vObjOracleColetas!VL_NOTA)
                
                Imprimir_Registro_Word CodCliente, NomeCliente, Endereco, Cidade, NFDevol, DDD, Telefone, NomeContato, vObjOracleColetas!VL_NOTA, "" 'IIf(IsNull(vObjOracleColetas!Observacoes), "", vObjOracleColetas!Observacoes)
                
                'Atualizar o Status para 1 que significa GERADO, do Registro na tabela CACLIE.DEVOLUCAO_COLETA
                vBanco.Parameters.Remove "PM_NUM_ATEND"
                vBanco.Parameters.Add "PM_NUM_ATEND", vObjOracleColetas!Num_Atendimento, 1
            
                vSql = "Producao.PCK_VDA640.PR_UPDATE_STATUS_COLETA(:PM_NUM_ATEND)"
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                
                If vErro <> "" Then
                    Call vVB_Generica_001.Informar("Erro no processamento da PR_UPDATE_STATUS_COLETA, entre em contato com o SUPORTE!")
                End If
            
            End If
            
            vObjOracleColetas.MoveNext
            
        Next
        
        vObjOracleColetas.MoveFirst
        
        FimPagina vObjOracleColetas.RecordCount
        Imprimir_Fim vNomeTransp, vSeqColeta
        Print #1, "</BODY>"
        Print #1, "</HTML>"
        
        vObjOracleTransp.MoveNext
FIM:
    Next

    If vObjOracleTransp.RecordCount > 0 Then
        Dim vNomeArq As String
        
        vNomeArq = pLoja & "_" & pDia & "_" & Format(Now, "HHMM") & ".HTML"
        
        Close #1
        
        Kill "C:\Coleta\" & vNomeArq
        
        Name "C:\Coleta\arqhtml.txt" As "C:\Coleta\" & vNomeArq
        
        If vObjOracleColetas.RecordCount = 0 Then
           ' Kill "C:\Coleta\" & vNomeArq
        End If
        
    End If
    
TrataErro:
    If Err.Number = 53 Then
        Resume Next
    ElseIf Err.Number = 55 Then
        Close #1
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Sub Preenche_Cbo_Lojas()
    
    Criar_Cursor vBanco.Parameters, "PM_CURSOR"
    
    vBanco.Parameters.Remove "Loja"
    vBanco.Parameters.Add "LOJA", 0, 1
    
    vSql = "Producao.PCK_VDA640.Pr_Select_Lojas(:PM_Cursor, :LOJA)"
    
    If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
        Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    End If
    
    cboLojas.Clear
    
    For i = 1 To vObjOracle.RecordCount
        cboLojas.AddItem vObjOracle.Fields(0).Value & "-" & vObjOracle.Fields(1).Value
        cboLojas.ItemData(cboLojas.NewIndex) = vObjOracle.Fields(0)
        vObjOracle.MoveNext
    Next
    
    vObjOracle.Close
    Set vObjOracle = Nothing
End Sub

Private Sub txtCodCliente_LostFocus()
    If txtCodCliente <> "" Then
        
        vBanco.Parameters.Remove "PM_CODCLIENTE"
        vBanco.Parameters.Add "PM_CODCLIENTE", txtCodCliente, 1
        
        vSql = "Producao.PCK_VDA640.PR_SELECT_CLIENTE_COLETA(:vCursor,:PM_CODCLIENTE)"
        
        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
            Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_CLIENTE_COLETA, entre em contato com o SUPORTE!")
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If
        
        If vObjOracle.EOF = True And vObjOracle.BOF = True Then
            Call vVB_Generica_001.Informar("N�o existe Coleta para este cliente!")
            txtCodCliente.SetFocus
            Exit Sub
        End If
    
    End If
End Sub

Sub Imprimir_Registro_Word(Codigo As String, RazaoSocial As String, Endereco As String, Cidade As String, NFDevol As String, DDD As String, Telefone As String, NomeContato As String, Valor As String, Observacoes As String)
  
    Print #1, "<TR bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
    Print #1, "<TD bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Codigo & "</TD>"
    Print #1, "<TD bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & RazaoSocial & "</TD>"
    Print #1, "<TD bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Endereco & "</TD>"
    Print #1, "<TD bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Cidade & "</TD>"
    Print #1, "<TD align=right bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & NFDevol & "</TD>"
    Print #1, "<TD align=right bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Valor & "</TD>"
    Print #1, "<TD bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & NomeContato & "</TD>"
    Print #1, "<TD align=right bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & DDD & "</TD>"
    Print #1, "<TD align=right bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Telefone & "</TD>"
    Print #1, "<TD align=Left bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & Observacoes & "</TD>"
    Print #1, "</TR>"
End Sub

Sub CriarTabelaInicial()
    On Error GoTo TrataErro
    
    Print #1, "<TABLE Cellpadding=2 cellspacing=0 border=1 width=100% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
    
        Print #1, "<TR align=center bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
        
            Print #1, "<TD Width=5% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "C�digo"
            Print #1, "</TD>"
            
            Print #1, "<TD Width=20% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Raz�o Social"
            Print #1, "</TD>"
            
            Print #1, "<TD Width=20% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Endere�o"
            Print #1, "</TD>"
        
            Print #1, "<TD Width=8% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Cidade"
            Print #1, "</TD>"
            
            Print #1, "<TD Width=5% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "NF Devol"
            Print #1, "</TD>"
            
            Print #1, "<TD Width=5% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Valor"
            Print #1, "</TD>"
        
            Print #1, "<TD Width=9% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Contato"
            Print #1, "</TD>"
        
            Print #1, "<TD colspan=2 Width=7% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Telefone"
            Print #1, "</TD>"
        
            Print #1, "<TD Width=21% bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
                Print #1, "Itens"
            Print #1, "</TD>"
        
        Print #1, "</TR>"
    
TrataErro:
    If Err.Number = 462 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

'Sub Criar_NomeTransp_NumColeta(Transp As String, Coleta As String)
'
'    sel.TypeText Text:="Transportadora: " & Transp & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "COLETA N�: " & Coleta
'
'    For i = vlinhas To 34
'        sel.TypeParagraph
'    Next
'
'    'MsgBox sel.Information(wdFirstCharacterLineNumber)
'
'    sel.TypeText Text:="Transportadora: " & Transp & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "COLETA N�: " & Coleta
'    WordApp.Visible = True
'    sel.GoTo What:=wdGoToPage, Which:=wdGoToNext, Name:=vPaginas
'    sel.MoveDown Unit:=wdLine, Count:=2
'
'End Sub


Sub Imprimir_Fim(Transp As String, Coleta As String)
    Dim vlinhas As Integer
    
    Print #1, "<TABLE Cellpadding=1 cellspacing=0 border=0 width=100%>"
    
        Print #1, "<TR>"
            Print #1, "<TD COLSPAN=2 >"
                Print #1, "<HR>"
            Print #1, "</TD>"
        
        Print #1, "</TR>"
        
        Print #1, "<TR>"
            
            Print #1, "<TD>"
                Print #1, "<font size=2>Transportadora: " & Transp
            Print #1, "</TD>"
            
            Print #1, "<TD Align=RIGHT>"
                Print #1, "<font size=2>COLETA N�: " & Coleta
            Print #1, "</TD>"
            
        Print #1, "</TR>"
        
        Print #1, "<TR>"
            Print #1, "<TD>"
                Print #1, "&nbsp;"
            Print #1, "</TD>"
        Print #1, "</TR>"
    
        Print #1, "<TR>"
            Print #1, "<TD>"
                Print #1, "<font size=2>Recebi esta Solicita��o de Coleta em: ____/____/________."
            Print #1, "</TD>"
        Print #1, "</TR>"
        
        Print #1, "<TR>"
            Print #1, "<TD>"
                Print #1, "&nbsp;"
            Print #1, "</TD>"
        Print #1, "</TR>"
        
        Print #1, "<TR>"
            Print #1, "<TD>"
                Print #1, "<FONT Size=2>Ass.: _____________________________________."
            Print #1, "</TD>"
        Print #1, "</TR>"
    
    Print #1, "</TABLE>"
    Print #1, "<p style='page-break-after:always'></p>"

End Sub

Sub FimPagina(Linhas)
    Print #1, "</TABLE>"
    
    For j = Linhas To 22
        Print #1, "<BR>"
    Next

End Sub

'Sub Imprimir_Observacoes(pLoja)
'
'    Dim vItens
'
'    Do While vObjOracleColetas.EOF = False
'
'        If Val(vObjOracleColetas!COD_LOJA) = pLoja Then
'
'            vItens = Split(vObjOracleColetas!Observacoes, "D:")
'
'            For i = 0 To UBound(vItens)
'                Print #1, "<TR bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>"
'                Print #1, "<TD colspan=9 align=right bordercolor=Silver bordercolordark=Silver bordercolorlight=Silver>" & vItens(i) & "</TD>"
'                Print #1, "</TR>"
'            Next
'        End If
'
'        vObjOracleColetas.MoveNext
'
'    Loop
'
'End Sub

