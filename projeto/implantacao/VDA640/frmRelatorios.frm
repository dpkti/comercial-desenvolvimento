VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmRelatorios 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Relat�rios - Gerar Arquivo TXT"
   ClientHeight    =   6795
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmRelatorios.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   453
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   421
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optConsultaNF 
      Appearance      =   0  'Flat
      Caption         =   "Consulta NF"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   15
      Top             =   1500
      Width           =   1845
   End
   Begin VB.Frame fra 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   4935
      Left            =   90
      TabIndex        =   10
      Top             =   1800
      Width           =   6165
      Begin VB.CheckBox chkTodos 
         Appearance      =   0  'Flat
         Caption         =   "Todas as Lojas"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   4530
         TabIndex        =   20
         Top             =   3240
         Width           =   1515
      End
      Begin VB.TextBox txtNF 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3210
         MaxLength       =   6
         TabIndex        =   18
         Top             =   1740
         Width           =   1500
      End
      Begin VB.TextBox txtCodFabrica 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4110
         MaxLength       =   50
         TabIndex        =   16
         Top             =   1080
         Width           =   1500
      End
      Begin VB.TextBox txtNomeCliente 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1110
         MaxLength       =   50
         TabIndex        =   3
         Top             =   510
         Width           =   4500
      End
      Begin VB.TextBox txtDtEmissaoNF 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         MaxLength       =   50
         TabIndex        =   5
         Top             =   1740
         Width           =   1740
      End
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         MaxLength       =   6
         TabIndex        =   2
         Top             =   510
         Width           =   960
      End
      Begin VB.ComboBox cboFornecedor 
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Text            =   "cboFornecedor"
         Top             =   1110
         Width           =   3930
      End
      Begin VB.ListBox lstFilesDeposito 
         Appearance      =   0  'Flat
         Height          =   2505
         Left            =   120
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   6
         Top             =   2340
         Width           =   4365
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Nota Fiscal:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3210
         TabIndex        =   19
         Top             =   1530
         Width           =   915
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�d F�brica:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   4110
         TabIndex        =   17
         Top             =   870
         Width           =   990
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Data Emiss�o da Nota (DD/MM/AA):"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   120
         TabIndex        =   14
         Top             =   1530
         Width           =   2940
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Dep�sito:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   120
         TabIndex        =   13
         Top             =   2100
         Width           =   780
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   120
         TabIndex        =   12
         Top             =   300
         Width           =   615
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   120
         TabIndex        =   11
         Top             =   900
         Width           =   990
      End
   End
   Begin VB.OptionButton optConsultaDupl 
      Appearance      =   0  'Flat
      Caption         =   "Consulta Duplicata"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   1
      Top             =   960
      Value           =   -1  'True
      Width           =   1860
   End
   Begin VB.OptionButton optConsultaItem 
      Appearance      =   0  'Flat
      Caption         =   "Consulta Item"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   180
      TabIndex        =   0
      Top             =   1230
      Width           =   1845
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   810
      Width           =   6105
      _ExtentX        =   10769
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   90
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRelatorios.frx":23D2
      PICN            =   "frmRelatorios.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerarArqTxt 
      Height          =   675
      Left            =   5460
      TabIndex        =   7
      ToolTipText     =   "Gerar Arquivo TXT"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRelatorios.frx":30C8
      PICN            =   "frmRelatorios.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmRelatorios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vNomeArquivo As String

Private Sub cboFornecedor_KeyUp(KeyCode As Integer, Shift As Integer)
   On Error GoTo TrataErro

10        If KeyCode <> 8 Then

              Dim vValorDigitado As String

20            vValorDigitado = cboFornecedor

30            For i = 0 To cboFornecedor.ListCount - 1

40                If Mid(cboFornecedor.List(i), 1, Len(vValorDigitado)) = cboFornecedor Then

50                    cboFornecedor.Text = cboFornecedor.List(i)
60                    cboFornecedor.SelStart = Len(vValorDigitado)
70                    cboFornecedor.SelLength = Len(cboFornecedor)
80                    Exit Sub

90                End If

100           Next

110       End If



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: cboFornecedor_KeyUp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub chkTodos_Click()
   On Error GoTo TrataErro

10        If chkTodos Then
20            For i = 0 To lstFilesDeposito.ListCount - 1
30                lstFilesDeposito.Selected(i) = True
40            Next
50        Else
60            For i = 0 To lstFilesDeposito.ListCount - 1
70                lstFilesDeposito.Selected(i) = False
80            Next
90        End If



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: chkTodos_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub cmdGerarArqTxt_Click()
          Dim vDeposito As String

10       On Error GoTo TrataErro

20        If VERIFICA_DADOS = True Then
          
30            If optConsultaDupl Then
                  
40                Criar_Cursor vBanco.Parameters, "vCursor"
                  
50                vBanco.Parameters.Remove "COD_CLIENTE"
60                vBanco.Parameters.Add "COD_CLIENTE", Trim(txtCodCliente), 1
                  
70                Call Aguardar
                  
80                vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_SELECT_CONSULTA_DUPLICATA(:VCURSOR,:COD_CLIENTE)")
                  
90                Unload frmAguardar
                  
100               If vErro <> "" Then
110                   vVB_Generica_001.ProcessaErro vErro
120                   Exit Sub
130               Else
140                   Set vObjOracle = vBanco.Parameters("VCURSOR").Value
150                   If vObjOracle.EOF Then
160                       vVB_Generica_001.Informar ("N�o h� dados para gerar o arquivo!")
170                       Exit Sub
180                   Else
190                       vNomeArquivo = "C:\Consulta_Duplicata.txt"
200                   End If
210               End If
220           ElseIf optConsultaItem Then
              
                  'PREENCHER VARI�VEL DE DEPOSITO COM VALORES DO LIST
230               vDeposito = ""
240               For i = 0 To lstFilesDeposito.ListCount - 1
250                   If lstFilesDeposito.Selected(i) = True Then
260                       vDeposito = vDeposito & Val(lstFilesDeposito.List(i)) & ","
270                   End If
280               Next
290               vDeposito = Mid(vDeposito, 1, Len(vDeposito) - 1)
                  
                  'CHAMADA DA PROCEDURE PR_SELECT_CONSULTA_ITEM
300               Criar_Cursor vBanco.Parameters, "vCursor"

310               vBanco.Parameters.Remove "COD_CLIENTE"
320               vBanco.Parameters.Add "COD_CLIENTE", CLng(Trim(txtCodCliente)), 1
                  
330               vBanco.Parameters.Remove "COD_FORNEC"
340               vBanco.Parameters.Add "COD_FORNEC", CInt(Trim(Val(cboFornecedor))), 1
                  
350               vBanco.Parameters.Remove "DATA_EMISSAO"
360               vBanco.Parameters.Add "DATA_EMISSAO", Formata_Data_Relatorios(Trim(txtDtEmissaoNF)), 1
                  
370               vBanco.Parameters.Remove "DEPOSITO"
380               vBanco.Parameters.Add "DEPOSITO", vDeposito, 1
                  
390               vBanco.Parameters.Remove "COD_FABRICA"
400               vBanco.Parameters.Add "COD_FABRICA", Trim(txtCodFabrica), 1
                  
410               Call Aguardar
                  
420               vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_SELECT_CONSULTA_ITEM(:VCURSOR,:COD_CLIENTE,:COD_FORNEC,:DATA_EMISSAO,:DEPOSITO,:COD_FABRICA)")
                  
430               Unload frmAguardar
                  
440               If vErro <> "" Then
450                   vVB_Generica_001.ProcessaErro vErro
460                   Exit Sub
470               Else
480                   Set vObjOracle = vBanco.Parameters("VCURSOR").Value
490                   If vObjOracle.EOF Then
500                       vVB_Generica_001.Informar ("N�o h� dados para gerar o arquivo!")
510                       Exit Sub
520                   Else
530                       vNomeArquivo = "C:\Consulta_Item.txt"
540                   End If
550               End If
560           ElseIf optConsultaNF Then
              
                  'PREENCHER VARI�VEL DE DEPOSITO COM VALORES DO LIST
570               vDeposito = "0"
                  If Me.chkTodos.Value <> 1 Then
        
580                  For i = 0 To lstFilesDeposito.ListCount - 1
590                      If lstFilesDeposito.Selected(i) = True Then
600                         vDeposito = vDeposito & Val(lstFilesDeposito.List(i)) & ","
610                      End If
620                  Next
630                  vDeposito = Mid(vDeposito, 1, Len(vDeposito) - 1)
                  End If
                  
                  'CHAMADA DA PROCEDURE PR_SELECT_CONSULTA_ITEM
640               Criar_Cursor vBanco.Parameters, "vCursor"
                           
650               vBanco.Parameters.Remove "NUM_NOTA"
660               vBanco.Parameters.Add "NUM_NOTA", Trim(txtNF), 1
                  
670               vBanco.Parameters.Remove "DEPOSITO"
680               vBanco.Parameters.Add "DEPOSITO", vDeposito, 1
                  
690               Call Aguardar
                  
700               vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_SELECT_CONSULTA_NF(:VCURSOR,:NUM_NOTA,:DEPOSITO)")
                  
710               Unload frmAguardar
                  
720               If vErro <> "" Then
730                   vVB_Generica_001.ProcessaErro vErro
740                   Exit Sub
750               Else
760                   Set vObjOracle = vBanco.Parameters("VCURSOR").Value
770                   If vObjOracle.EOF Then
780                       vVB_Generica_001.Informar ("N�o h� dados para gerar o arquivo!")
790                       Exit Sub
800                   Else
810                       vNomeArquivo = "C:\Consulta_NF.txt"
820                   End If
830               End If
              
840           End If
              
              'chama sub q gera arquivo txt
850           GerarArquivo
              
860       End If

TrataErro:
870       If Err.Number <> 0 Then
880      MsgBox "Sub: cmdGerarArqTxt_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
890       End If
End Sub

Private Sub cmdVoltar_Click()

   On Error GoTo TrataErro

10        Unload Me



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: cmdVoltar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub
Private Sub Form_Load()
          
   On Error GoTo TrataErro

10        Me.Top = 0
20        Me.Left = 0
              
30        LIMPAR_CAMPOS



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: Form_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
          
End Sub

Sub GerarArquivo()
          Dim vArq As Integer
          Dim ii As Integer
          Dim vCampos As String
          Dim vRegistro As String
          Dim i As Integer
          
   On Error GoTo TrataErro

10        vArq = FreeFile()
          
20        If Dir(vNomeArquivo) <> "" Then Kill vNomeArquivo
          
30        Open vNomeArquivo For Output As #vArq
          
          'Imprimir os nomes dos campos
40        For i = 0 To vObjOracle.Fields.Count - 1
50            vCampos = vCampos & UCase(vObjOracle.Fields(i).Name) & ";"
60        Next
          
70        Print #vArq, vCampos
80        vObjOracle.MoveFirst
90        For i = 1 To vObjOracle.RecordCount
          
100           For ii = 0 To vObjOracle.Fields.Count - 1
110               If Not IsNull(vObjOracle.Fields(ii).Value) Then
120                   vRegistro = vRegistro & vObjOracle.Fields(ii).Value & ";"
130               Else
140                   vRegistro = vRegistro & ";"
150               End If
160           Next
              
170           Print #vArq, vRegistro
              
180           vRegistro = ""
              
190           vObjOracle.MoveNext
          
200       Next
          
210       Close #vArq
220       vVB_Generica_001.Informar ("Arquivo gerado com sucesso em " & vNomeArquivo)
230       LIMPAR_CAMPOS



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: GerarArquivo" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
          
End Sub

Private Sub optConsultaDupl_Click()

10       On Error GoTo TrataErro

20        LIMPAR_CAMPOS

30        DESABILITAR
          
40        txtCodCliente.Enabled = True
50        chkTodos.Value = 0

TrataErro:
60        If Err.Number <> 0 Then
70             MsgBox "Sub: optConsultaDupl_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
80        End If
End Sub

Private Sub optConsultaItem_Click()
          Dim vObjDep As Object
          Dim vObjFornec As Object
          Dim vObjCliente As Object
          
10       On Error GoTo TrataErro

20        LIMPAR_CAMPOS
          
          'preencher list com os depositos
          
30        Set vObjDep = vVB_Generica_001.TabelaLoja(vBanco)
          
40        Call vVB_Generica_001.PreencheComboList(vObjDep, lstFilesDeposito, vObjDep.Fields(0).Name)
          
          'preencher cbo fornecedores
50        Criar_Cursor vBanco.Parameters, "vCursor"
          
60        vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_SELECT_FORNECEDOR(:VCURSOR)")
          
70        If vErro <> "" Then
80            vVB_Generica_001.ProcessaErro vErro
90        Else
100           Set vObjFornec = vBanco.Parameters("VCURSOR").Value
110           Call vVB_Generica_001.PreencheComboList(vObjFornec, cboFornecedor, vObjFornec.Fields(0).Name)
120       End If
               
          'habilitar campos para pesquisa
       
130       HABILITAR
          
140       txtNF.Enabled = False

TrataErro:
150       If Err.Number <> 0 Then
160      MsgBox "Sub: optConsultaItem_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
170       End If
          
End Sub

Private Sub optConsultaNF_Click()
          Dim vObjDep As Object
          
   On Error GoTo TrataErro

10        DESABILITAR
          
20        txtNF.Enabled = True
30        chkTodos.Enabled = True
          
          'preencher list com os depositos
          
40        Set vObjDep = vVB_Generica_001.TabelaLoja(vBanco)
          
50        Call vVB_Generica_001.PreencheComboList(vObjDep, lstFilesDeposito, vObjDep.Fields(0).Name)



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: optConsultaNF_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
          
End Sub

Private Sub txtCodCliente_LostFocus()
          Dim vObjCliente As Object
          
   On Error GoTo TrataErro

10        If txtCodCliente = "" Then
20            Exit Sub
30        Else
40            Set vObjCliente = vVB_Generica_001.TabelaCliente(vBanco, Trim(txtCodCliente))
50            If IsNull(vObjCliente.Fields(3)) Then
60                vVB_Generica_001.Informar ("Cliente inexistente!")
70                txtNomeCliente = ""
80                txtCodCliente = ""
90                txtCodCliente.SetFocus
100               Exit Sub
110           Else
120               txtNomeCliente = vObjCliente.Fields(3)
130           End If
140       End If



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: txtCodCliente_LostFocus" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub txtDtEmissaoNF_KeyPress(KeyAscii As Integer)

   On Error GoTo TrataErro

10        Call vVB_Generica_001.Data(KeyAscii, txtDtEmissaoNF)



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: txtDtEmissaoNF_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Sub LIMPAR_CAMPOS()
          
10       On Error GoTo TrataErro

20        txtCodCliente = ""
30        txtNomeCliente = ""
40        txtDtEmissaoNF = ""
50        cboFornecedor.Clear
60        lstFilesDeposito.Clear
70        txtCodFabrica = ""
80        txtNF = ""
90        chkTodos.Value = 0

TrataErro:
100       If Err.Number <> 0 Then
110      MsgBox "Sub: LIMPAR_CAMPOS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
120       End If
          
End Sub

Function VERIFICA_DADOS() As Boolean
10       On Error GoTo TrataErro

20    If optConsultaDupl Then
30        GoTo OptDupl
40    ElseIf optConsultaItem Then
50        If lstFilesDeposito.SelCount = 0 Then
60            vVB_Generica_001.Informar ("Selecione pelo menos um dep�sito!")
70            VERIFICA_DADOS = False
80            Exit Function
90        End If
          
100       If cboFornecedor = "" Then
110           vVB_Generica_001.Informar ("Selecione um fornecedor!")
120           VERIFICA_DADOS = False
130           Exit Function
140       End If

150       If txtDtEmissaoNF = "" Then
160           vVB_Generica_001.Informar ("Informe a data de emiss�o da nota!")
170           VERIFICA_DADOS = False
180           Exit Function
190       End If
          
              
200 OptDupl:        If txtCodCliente = "" Then
210                   vVB_Generica_001.Informar ("Digite o c�digo do cliente!")
220                   VERIFICA_DADOS = False
230                   Exit Function
240               End If

250   End If

260   If optConsultaNF Then

270       If txtNF = "" Then
280           vVB_Generica_001.Informar ("Informe o n�mero da nota fiscal!")
290           VERIFICA_DADOS = False
300           Exit Function
310       End If
          
320   End If
330       VERIFICA_DADOS = True



TrataErro:
340       If Err.Number <> 0 Then
350           MsgBox "Sub: VERIFICA_DADOS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
360       End If
         
End Function

Sub HABILITAR()
   On Error GoTo TrataErro

10        txtCodCliente.Enabled = True
20        txtCodFabrica.Enabled = True
30        txtDtEmissaoNF.Enabled = True
          'lstFilesDeposito.Clear
          'cboFornecedor.Clear
40        chkTodos.Enabled = True



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: HABILITAR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Sub DESABILITAR()
   On Error GoTo TrataErro

10        txtCodCliente.Enabled = False
20        txtCodFabrica.Enabled = False
30        txtDtEmissaoNF.Enabled = False
40        lstFilesDeposito.Clear
50        cboFornecedor.Clear
60        chkTodos.Enabled = False



TrataErro:
    If Err.Number <> 0 Then
         MsgBox "Sub: DESABILITAR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

