VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCadEmail 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cadastro de E-mail"
   ClientHeight    =   5520
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8805
   ClipControls    =   0   'False
   Icon            =   "frmCadEmail.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   8805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSFlexGridLib.MSFlexGrid msfEmail 
      Height          =   2175
      Left            =   30
      TabIndex        =   24
      Top             =   3300
      Width           =   8745
      _ExtentX        =   15425
      _ExtentY        =   3836
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      WordWrap        =   -1  'True
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
   End
   Begin VB.TextBox txtDtAtual 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7470
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   1050
      Width           =   1260
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1365
      Left            =   30
      TabIndex        =   8
      Top             =   1440
      Width           =   8715
      Begin VB.TextBox txtEmail 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   100
         TabIndex        =   13
         Top             =   900
         Width           =   7680
      End
      Begin VB.TextBox txtDtCadastro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7410
         Locked          =   -1  'True
         MaxLength       =   8
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   540
         Width           =   1260
      End
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         MaxLength       =   70
         TabIndex        =   11
         Top             =   540
         Width           =   5220
      End
      Begin VB.TextBox txtSeq 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         Locked          =   -1  'True
         MaxLength       =   100
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   180
         Width           =   540
      End
      Begin VB.ComboBox cboDepto 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   4380
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   180
         Width           =   4275
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "E-mail:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   420
         TabIndex        =   18
         Top             =   990
         Width           =   525
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Dt.Cadastro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6360
         TabIndex        =   17
         Top             =   630
         Width           =   1020
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   240
         TabIndex        =   16
         Top             =   630
         Width           =   720
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Sequ�ncia:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   60
         TabIndex        =   15
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3120
         TabIndex        =   14
         Top             =   270
         Width           =   1245
      End
   End
   Begin VB.TextBox txtCodCliente 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H8000000F&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1080
      Locked          =   -1  'True
      MaxLength       =   100
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1050
      Width           =   960
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      Top             =   960
      Width           =   8715
      _ExtentX        =   15372
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   855
      Left            =   7920
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   1508
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":0CCA
      PICN            =   "frmCadEmail.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdd 
      Height          =   405
      Left            =   7440
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Salvar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":19C0
      PICN            =   "frmCadEmail.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   405
      Left            =   7890
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Excluir E-mail selecionado"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":1F76
      PICN            =   "frmCadEmail.frx":1F92
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovo 
      Height          =   405
      Left            =   6120
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Adicionar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":252C
      PICN            =   "frmCadEmail.frx":2548
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAlterar 
      Height          =   405
      Left            =   6555
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Alterar E-mail"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2A04
      PICN            =   "frmCadEmail.frx":2A20
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   405
      Left            =   7005
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":2FBA
      PICN            =   "frmCadEmail.frx":2FD6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSemEmail 
      Height          =   405
      Left            =   8340
      TabIndex        =   25
      TabStop         =   0   'False
      ToolTipText     =   "Cliente n�o possui email"
      Top             =   2850
      Width           =   405
      _ExtentX        =   714
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadEmail.frx":32F0
      PICN            =   "frmCadEmail.frx":330C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Dt.Atual:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   6720
      TabIndex        =   23
      Top             =   1140
      Width           =   735
   End
   Begin VB.Label lblNomeCliente 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   330
      Left            =   2100
      TabIndex        =   19
      Top             =   1050
      Width           =   4515
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "E-mails Cadastrados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   6
      Top             =   3060
      Width           =   1635
   End
   Begin VB.Label lblCodCliente 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "C�d.Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   5
      Top             =   1140
      Width           =   990
   End
   Begin VB.Image Image1 
      Height          =   1035
      Left            =   -90
      Picture         =   "frmCadEmail.frx":3776
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1305
   End
End
Attribute VB_Name = "frmCadEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmCadEmail
' Author    : c.samuel.oliveira
' Date      : 28/09/2018
' Purpose   : TI-6746
'---------------------------------------------------------------------------------------

Option Explicit

Dim vLoad As Boolean
Dim vRst As Object

Private Sub cmdAdd_Click()
    
    If txtCodCliente = "" Then
        MsgBox "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If Trim(txtEmail) = "" Then
        MsgBox "Informe o E-mail.", vbInformation, "Aten��o"
        txtEmail.SetFocus
        Exit Sub
    End If
        
    If Me.cboDepto.ListIndex = -1 Then
        MsgBox "Informe o Departamento.", vbInformation, "Aten��o"
        cboDepto.SetFocus
        Exit Sub
    End If
        
    'Validar Email
    If isEmail(txtEmail) = False Then
       MsgBox "E-mail inv�lido.", vbInformation, "Aten��o"
       txtEmail.SetFocus
       Exit Sub
    End If

    'TI-6746
    For i = 1 To msfEmail.Rows - 1
        If txtSeq <> msfEmail.TextMatrix(i, 0) And (cboDepto.ItemData(cboDepto.ListIndex) = "19" And cboDepto = msfEmail.TextMatrix(i, 3)) Then
            MessageBox 0, "J� existe E-Mail Comercial cadastrado!", "Aten��o", &H40000
            Exit Sub
        End If
    Next
    'FIM TI-6746
        
'Verificar se existe um igual, se existir sair da rotina
    For i = 1 To msfEmail.Rows - 1
        If txtEmail = msfEmail.TextMatrix(i, 1) And txtContato = msfEmail.TextMatrix(i, 2) And cboDepto = msfEmail.TextMatrix(i, 3) Then
            MsgBox "Este email j� existe.", vbInformation, "Aten��o"
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MsgBox("Confirma a altera��o deste e-mail ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
           For i = 1 To msfEmail.Rows
               If txtSeq = msfEmail.TextMatrix(i, 0) Then
                  Exit For
               End If
           Next
           
           msfEmail.TextMatrix(i, 0) = txtSeq
           msfEmail.TextMatrix(i, 1) = txtEmail
           msfEmail.TextMatrix(i, 2) = txtContato
           msfEmail.TextMatrix(i, 3) = cboDepto
           msfEmail.TextMatrix(i, 4) = txtDtCadastro
        Else
            Exit Sub
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.Rows = msfEmail.Rows + 1
       msfEmail.TextMatrix(i, 0) = txtSeq
       msfEmail.TextMatrix(i, 1) = txtEmail
       msfEmail.TextMatrix(i, 2) = txtContato
       msfEmail.TextMatrix(i, 3) = cboDepto
       msfEmail.TextMatrix(i, 4) = txtDtCadastro
    End If
    
    'Inserir no banco
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Remove "Sequencia"
    vBanco.Parameters.Remove "Email"
    vBanco.Parameters.Remove "CodDepto"
    vBanco.Parameters.Remove "Contato"
    
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    vBanco.Parameters.Add "Sequencia", txtSeq, 1
    vBanco.Parameters.Add "Email", txtEmail, 1
    vBanco.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    vBanco.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    End If
    
    If cboDepto.ItemData(cboDepto.ListIndex) = "19" Then EnviarParaAprovacaoCAD060 UCase(Trim(txtEmail)), Val(txtCodCliente) 'TI-6746
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, True
    
    
End Sub

Private Sub cmdAlterar_Click()
    Frame1.Enabled = True
    Frame1.Caption = "Alterar"
    txtDtCadastro = txtDtAtual
    Botoes False, False, True, True, False, True
End Sub

Private Sub cmdExcluir_Click()
    Me.Visible = False
    If Val(txtSeq) = 0 Then
       MsgBox "Selecione um item da lista.", vbInformation, "Aten��o"
       Me.Visible = True
       Exit Sub
    End If
    If MsgBox("Confirma a exclus�o do e-mail selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
        vBanco.Parameters.Remove "COD_CLIENTE"
        vBanco.Parameters.Remove "Sequencia"
        vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
        vBanco.Parameters.Add "Sequencia", txtSeq, 1
        
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_EXCLUIR_EMAIL(:Cod_Cliente,:Sequencia); END;"
        
        vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_RESEQUENCIAR(:Cod_Cliente);END;"
        
        Preencher_Email
    End If
    
    txtSeq = ""
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
        
    Frame1.Enabled = False
    
    Botoes True, False, False, False, False, False
    Me.Visible = True
End Sub

Private Sub cmdLimpar_Click()
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    Me.Frame1.Caption = ""
    
    Frame1.Enabled = False
    
    Botoes True, False, True, False, False, False
End Sub

Private Sub cmdNovo_Click()
    
    Frame1.Enabled = True
    
    If msfEmail.Rows = 1 Then
        txtSeq = 1
    Else
        txtSeq = msfEmail.TextMatrix(msfEmail.Rows - 1, 0) + 1
    End If
    
    txtContato = ""
    txtEmail = ""
    cboDepto.ListIndex = -1
    
    txtDtCadastro = txtDtAtual
    
    cboDepto.SetFocus
    
    Frame1.Caption = "Novo"
    
    Botoes False, False, True, True, False, True
    
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub cmdSemEmail_Click()

    NonStayOnTop frmCadEmail

    If MsgBox("Confirma que este cliente n�o possui email ?", vbYesNo + vbQuestion, "Aten��o") = vbNo Then Exit Sub
    
    If msfEmail.Rows > 1 Then
        MsgBox "Este cliente j� possui email cadastrado." & vbCrLf & "Caso queria informar que o cliente n�o possui email" & vbCrLf & "exclua os emails da lista.", vbInformation, "Aten��o"
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If cboDepto.Text = "" Then
        MsgBox "O Campo Departamento n�o pode estar em branco.", , "Aten��o"
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    If txtCodCliente = "" Then
        MsgBox "O Campo C�digo do Cliente n�o pode estar em branco." & vbCrLf & "Entre em contato com o Helpdesk.", , "Aten��o"
        StayOnTop frmCadEmail
        Exit Sub
    End If
    
    txtEmail = ""
    
'Verificar se existe um igual, se existir sair da rotina
    For i = 1 To msfEmail.Rows - 1
        If txtEmail = msfEmail.TextMatrix(i, 1) And txtContato = msfEmail.TextMatrix(i, 2) And cboDepto = msfEmail.TextMatrix(i, 3) And txtDtCadastro = msfEmail.TextMatrix(i, 4) Then
            MsgBox "Este email j� existe.", , "Aten��o"
            StayOnTop frmCadEmail
            Exit Sub
        End If
    Next
    
    'Verificar se existe uma sequencia igual, se existir dar update
    If UCase(Frame1.Caption) = "ALTERAR" Then
        If MsgBox("Confirma a altera��o deste e-mail ?", vbYesNo, "Aten��o") = vbYes Then
           For i = 1 To msfEmail.Rows
               If txtSeq = msfEmail.TextMatrix(i, 0) Then
                  Exit For
               End If
           Next
           
           msfEmail.TextMatrix(i, 0) = txtSeq
           msfEmail.TextMatrix(i, 1) = txtEmail
           msfEmail.TextMatrix(i, 2) = txtContato
           msfEmail.TextMatrix(i, 3) = cboDepto
           msfEmail.TextMatrix(i, 4) = txtDtCadastro
        Else
            StayOnTop frmCadEmail
            Exit Sub
        End If
    ElseIf UCase(Frame1.Caption) = "NOVO" Then
       msfEmail.Rows = msfEmail.Rows + 1
       msfEmail.TextMatrix(i, 0) = txtSeq
       msfEmail.TextMatrix(i, 1) = txtEmail
       msfEmail.TextMatrix(i, 2) = txtContato
       msfEmail.TextMatrix(i, 3) = cboDepto
       msfEmail.TextMatrix(i, 4) = txtDtCadastro
    End If
    
    'Inserir no banco
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Remove "Sequencia"
    vBanco.Parameters.Remove "Email"
    vBanco.Parameters.Remove "CodDepto"
    vBanco.Parameters.Remove "Contato"
    
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    vBanco.Parameters.Add "Sequencia", txtSeq, 1
    vBanco.Parameters.Add "Email", Null, 1
    vBanco.Parameters.Add "CodDepto", cboDepto.ItemData(cboDepto.ListIndex), 1
    vBanco.Parameters.Add "Contato", txtContato, 1
    
    If UCase(Frame1.Caption) = "NOVO" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INCLUIR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    ElseIf UCase(Frame1.Caption) = "ALTERAR" Then
       vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_ALTERAR_EMAIL(:Cod_Cliente,:Sequencia, :Email, :CodDepto, :Contato); END;"
    End If
    
    txtContato = ""
    txtEmail = ""
    txtSeq = ""
    cboDepto.ListIndex = -1
     
    Frame1.Enabled = False
    
    Preencher_Email
    
    Botoes True, False, False, False, False, False
    

End Sub

Private Sub Form_Load()
    
    vLoad = True
        
    Me.txtCodCliente = frmVDA640.txtCodigo_Cliente
        
    frmVDA640.Tag = "Aberto"
    
    Botoes True, False, False, False, False, False

    Preencher_Data
        
    Preencher_Depto
    
    Preencher_Email
    
    Pegar_nome_Cliente
    
    vLoad = False
    
End Sub

Private Sub msfEmail_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtSeq = Item
    txtEmail = Item.SubItems(1)
    txtContato = Item.SubItems(2)
    cboDepto = Item.SubItems(3)
    txtDtCadastro = Item.SubItems(4)
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes False, True, True, False, True, True
        
End Sub

Sub Preencher_Data()
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_DATA_CADASTRO(:vCursor); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    txtDtAtual = vRst.Fields(0)

End Sub

Sub Preencher_Depto()
    
    Criar_Cursor vBanco.Parameters, "vCursor"

    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_SELECT_DEPTO(:vCursor); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
            
    For i = 1 To vRst.RecordCount
        cboDepto.AddItem vRst!Descricao
        cboDepto.ItemData(cboDepto.NewIndex) = vRst!Cod_Depto
        vRst.MoveNext
    Next

End Sub

Sub Preencher_Email()
    Dim Litem As ListItem
    Dim ii As Integer
    
    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_cliente", txtCodCliente, 1
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.ExecuteSQL "Begin Producao.PCK_CAD_EMAIL.PR_SELECT_EMAIL(:vCursor, :Cod_Cliente);End;"
    
    Set vRst = vBanco.Parameters("vCursor").Value
    
    msfEmail.Clear
    
    Montar_Grid vRst
    
    For i = 1 To vRst.RecordCount
        With msfEmail
            .TextMatrix(i, 0) = vRst!Sequencia
            .TextMatrix(i, 1) = IIf(IsNull(vRst!Email), "", vRst!Email)
            .TextMatrix(i, 2) = IIf(IsNull(vRst!NOME_CONTATO), "", vRst!NOME_CONTATO)
            .TextMatrix(i, 3) = vRst!Descricao
            .TextMatrix(i, 4) = IIf(IsNull(vRst!Dt_Email), "", vRst!Dt_Email)
        
            If .TextMatrix(i, 4) = "" Then
               For ii = 0 To 4
                    .row = i
                    .col = ii
                    .CellFontBold = True
                    .CellForeColor = vbRed
               Next
            Else
                If DateAdd("m", -6, Now) > CDate(.TextMatrix(i, 4)) Then
                    For ii = 0 To 4
                         .row = i
                         .col = ii
                         .CellFontBold = True
                         .CellForeColor = vbRed
                    Next
                End If
            End If
        End With
        vRst.MoveNext
    Next
    Set vRst = Nothing
End Sub

Sub Pegar_nome_Cliente()

    vBanco.Parameters.Remove "Cod_Cliente"
    vBanco.Parameters.Add "Cod_Cliente", txtCodCliente, 1
    Criar_Cursor vBanco.Parameters, "vCursor"
    
    vBanco.ExecuteSQL "Begin Producao.PCK_CAD_EMAIL.PR_SELECT_NOME_CLIENTE(:vCursor, :Cod_Cliente); END;"

    Set vRst = vBanco.Parameters("vCursor").Value
    
    If vRst.EOF = False Then Me.lblNomeCliente = vRst!NOME_CLIENTE
    
    Set vRst = Nothing

End Sub

Sub Botoes(pNovo As Boolean, pAlterar As Boolean, pDesfazer As Boolean, pGravar As Boolean, pExcluir As Boolean, psememail As Boolean)

    cmdNovo.Enabled = pNovo
    cmdAlterar.Enabled = pAlterar
    cmdLimpar.Enabled = pDesfazer
    cmdAdd.Enabled = pGravar
    cmdExcluir.Enabled = pExcluir
    cmdSemEmail.Enabled = psememail

End Sub

Function isEmail(ByVal pEmail As String) As Boolean
        
    Dim Conta As Integer, Flag As Integer, cValido As String
    
    isEmail = False
    
    If Len(pEmail) < 3 Then
        Exit Function
    End If

    
    'Verifica se existe caracter inv�lido
    
    For Conta = 1 To Len(pEmail)
        cValido = Mid(pEmail, Conta, 1)
        If Not (LCase(cValido) Like "[a-z]" Or cValido = _
            "@" Or cValido = "." Or cValido = "-" Or _
            cValido = "_" Or cValido Like "[0-9]") Then
            Exit Function
        End If
    Next
  

    'Verifica a exist�ncia de (@)
    
    If InStr(pEmail, "@") = 0 Then
        Exit Function
    Else
        Flag = 0
        
        For Conta = 1 To Len(pEmail)
            If Mid(pEmail, Conta, 1) = "@" Then
                Flag = Flag + 1
            End If
        Next
        
        If Flag > 1 Then Exit Function
    End If
  
    If Left(pEmail, 1) = "@" Then
        Exit Function
    ElseIf Right(pEmail, 1) = "@" Then
        Exit Function
    ElseIf InStr(pEmail, ".@") > 0 Then
        Exit Function
    ElseIf InStr(pEmail, "@.") > 0 Then
        Exit Function
    End If
  
  
    'Verifica a exist�ncia de (.)
    
    If InStr(pEmail, ".") = 0 Then
        Exit Function
    ElseIf Left(pEmail, 1) = "." Then
        Exit Function
    ElseIf Right(pEmail, 1) = "." Then
        Exit Function
    ElseIf InStr(pEmail, "..") > 0 Then
        Exit Function
    End If
    
  
    isEmail = True

End Function

Sub Montar_Grid(rst As Object)
    With msfEmail
        .Rows = rst.RecordCount + 1
        .TextMatrix(0, 0) = "Seq"
        .TextMatrix(0, 1) = "E-mail"
        .TextMatrix(0, 2) = "Contato"
        .TextMatrix(0, 3) = "Departamento"
        .TextMatrix(0, 4) = "Dt Cadastro"
        .ColAlignment(0) = vbCenter
        .ColWidth(0) = 500
        .ColWidth(1) = 3000
        .ColWidth(2) = 2000
        .ColWidth(3) = 2000
        .ColWidth(4) = 1200

    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmVDA640.Carregar_Email
    frmVDA640.Tag = ""
End Sub

Private Sub msfEmail_Click()
    If msfEmail.TextMatrix(0, 0) = "" Or vLoad = True Then Exit Sub
    msfEmail_EnterCell
End Sub

Private Sub msfEmail_EnterCell()
    If msfEmail.Text = "" Or vLoad = True Then Exit Sub
    
    txtSeq = msfEmail.TextMatrix(msfEmail.row, 0)
    txtEmail = msfEmail.TextMatrix(msfEmail.row, 1)
    txtContato = msfEmail.TextMatrix(msfEmail.row, 2)
    cboDepto = msfEmail.TextMatrix(msfEmail.row, 3)
    txtDtCadastro = msfEmail.TextMatrix(msfEmail.row, 4)
        
    If Trim(txtDtCadastro) = "" Then txtDtCadastro = txtDtAtual
        
    Frame1.Enabled = False
    Frame1.Caption = ""
    Botoes False, True, True, False, True, False
End Sub
'TI-6746
Sub EnviarParaAprovacaoCAD060(pEmail As String, pCodCliente As Long)

On Error GoTo Trata_Erro

    vBanco.Parameters.Remove "PM_COD_CLIENTE"
    vBanco.Parameters.Remove "PM_EMAIL"
    vBanco.Parameters.Remove "PM_COD_USUARIO"
    
    vBanco.Parameters.Add "PM_COD_CLIENTE", pCodCliente, 1
    vBanco.Parameters.Add "PM_EMAIL", pEmail, 1
    vBanco.Parameters.Add "PM_COD_USUARIO", vCod_Usuario, 1
    
    vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_CAD_EMAIL.PR_INS_CLIENTE_WEB(:PM_COD_CLIENTE,:PM_EMAIL, :PM_COD_USUARIO); END;"
    
Trata_Erro:

End Sub
'FIM TI-6746
