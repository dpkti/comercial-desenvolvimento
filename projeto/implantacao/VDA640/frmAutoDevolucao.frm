VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAutoDevolucao 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Autoriza��o para Devolu��o"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8010
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2925
   ScaleWidth      =   8010
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin Bot�o.cmd cmdSalvar 
      Height          =   705
      Left            =   5580
      TabIndex        =   18
      ToolTipText     =   "Gravar Devolu��o"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAutoDevolucao.frx":0000
      PICN            =   "frmAutoDevolucao.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComCtl2.DTPicker txtEmissao 
      Height          =   315
      Left            =   4050
      TabIndex        =   17
      Top             =   1710
      Width           =   1305
      _ExtentX        =   2302
      _ExtentY        =   556
      _Version        =   393216
      Format          =   16515073
      CurrentDate     =   38401
   End
   Begin VB.ComboBox cboTransportadora 
      Height          =   315
      Left            =   30
      Style           =   2  'Dropdown List
      TabIndex        =   15
      Top             =   1080
      Width           =   3930
   End
   Begin VB.TextBox txtMensagem 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   585
      Left            =   30
      ScrollBars      =   2  'Vertical
      TabIndex        =   10
      Top             =   2310
      Width           =   7950
   End
   Begin VB.TextBox txtNFDevolucao 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2130
      MaxLength       =   6
      TabIndex        =   7
      Top             =   1710
      Width           =   1185
   End
   Begin VB.TextBox txtVlrNF 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   30
      MaxLength       =   14
      TabIndex        =   5
      Top             =   1680
      Width           =   1200
   End
   Begin VB.Frame fra 
      Caption         =   "Frete"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   585
      Left            =   6060
      TabIndex        =   2
      Top             =   1470
      Width           =   1905
      Begin VB.OptionButton optCIF 
         Appearance      =   0  'Flat
         Caption         =   "CIF"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1110
         TabIndex        =   4
         Top             =   270
         Width           =   630
      End
      Begin VB.OptionButton optFOB 
         Appearance      =   0  'Flat
         Caption         =   "FOB"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   60
         TabIndex        =   3
         Top             =   270
         Value           =   -1  'True
         Width           =   810
      End
   End
   Begin VB.TextBox txtTransportadora 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      MaxLength       =   50
      TabIndex        =   0
      Top             =   1080
      Width           =   3930
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   12
      Top             =   765
      Width           =   7950
      _ExtentX        =   14023
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   705
      Left            =   30
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAutoDevolucao.frx":0CF6
      PICN            =   "frmAutoDevolucao.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   705
      Left            =   6390
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAutoDevolucao.frx":19EC
      PICN            =   "frmAutoDevolucao.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerarArqHtml 
      Height          =   705
      Left            =   7200
      TabIndex        =   19
      ToolTipText     =   "Gerar Arquivo HTML"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAutoDevolucao.frx":26E2
      PICN            =   "frmAutoDevolucao.frx":26FE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Transportadora:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   30
      TabIndex        =   16
      Top             =   840
      Width           =   1305
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Mensagem:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   30
      TabIndex        =   11
      Top             =   2100
      Width           =   930
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Emiss�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   4065
      TabIndex        =   9
      Top             =   1500
      Width           =   690
   End
   Begin VB.Label lblNota_Fiscal 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "NF Devolu��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   2130
      TabIndex        =   8
      Top             =   1500
      Width           =   1170
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Valor NF:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   60
      TabIndex        =   6
      Top             =   1470
      Width           =   735
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Nome Transportadora:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   4050
      TabIndex        =   1
      Top             =   840
      Width           =   1845
   End
End
Attribute VB_Name = "frmAutoDevolucao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cboTransportadora_Click()
    txtTransportadora = ""
End Sub

Private Sub cmdGerarArqHtml_Click()
Dim strhtml As String
Dim strFRETE As String


If optFOB.Value = True Then
   strFRETE = "Frete por conta do Cliente"
Else
   strFRETE = "Frete por Conta da DPK"
End If

strhtml = strhtml & "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 3.2 Final//EN'>" & _
                    "<HTML>" & _
                    "<HEAD>" & _
                    "<META HTTP-EQUIV='Content-Type' CONTENT='text/html;charset=windows-1252'>" & _
                    "<TITLE>Autoriza��o de Devolu��o</TITLE>" & _
                    "</HEAD>" & _
                    "<BODY>" & _
                    "<TABLE WIDTH=100% border=1 cellpadding=4 cellspacing=0>" & _
                    "          <TR><TD ROWSPAN=5 vAlign=center align=middle><IMG src='http://dpknet/images/logodpk.jpg'></TD><TD colspan=3 align=middle><FONT FACE='Arial' SIZE=2><B>COMERCIAL AUTOMOTIVA LTDA</B></FONT></TD></TR>" & _
                    "          <TR><TD colspan=3 align=middle><FONT FACE='Arial' SIZE=1>SERVICO DE ATENDIMENTO AO CLIENTE</FONT></TD></TR>" & _
                    "          <TR><TD colspan=3 align=middle><FONT FACE='Arial' SIZE=1>FONE: (19) 3772-8790  FAX: (19) 3772-8602</FONT></TD></TR>" & _
                    "          <TR><TD colspan=3 align=middle><FONT FACE='Arial' SIZE=1>EMAIL: CA@DPK.COM.BR</FONT></TD></TR>" & _
                    "          <TR><TD colspan=3 align=middle><FONT FACE='Arial' SIZE=3><B>AUTORIZA��O PARA DEVOLU��O</B></FONT></TD></TR>" & _
                    "       <TR>" & _
                    "            <TD colspan=2><FONT FACE='Arial' SIZE=1>Campinas, " & FormatDateTime(Date, vbLongDate) & "</FONT></TD>" & _
                    "            <TD colspan=2 align=right><FONT FACE='Arial' SIZE=1>N� Atendimento: " & Mid(frmVDA640.lblNumAtendimento, InStr(1, frmVDA640.lblNumAtendimento, "-") + 2) & "</FONT></TD>" & _
                    "        </TR>" & _
                    "        <TR>" & _
                    "            <TD><FONT FACE='Arial' SIZE=2><B>Transportadora:</B></FONT></TD>" & _
                    "            <TD><FONT FACE='Arial' SIZE=1>" & IIf(cboTransportadora.ListIndex > 0, cboTransportadora, txtTransportadora) & "</FONT></TD>" & _
                    "        </TR>" & _
                    "        <TR>" & _
                    "            <TD><FONT FACE='Arial' SIZE=2><B>Frete:</B></FONT></TD>" & _
                    "            <TD><FONT FACE='Arial' SIZE=1>" & strFRETE & "</FONT></TD>" & _
                    "        </TR>"

strhtml = strhtml & "       <TR>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Rementente:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & frmVDA640.txtRazao_Social & "</FONT></TD>" & _
                    "       </TR>" & _
                    "       <TR>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Cidade:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & frmVDA640.txtCidade & "</FONT></TD>" & _
                    "       </TR>" & _
                    "       <TR>" & _
                    "           <TD width=25%><FONT FACE='Arial' SIZE=2><B>UF:</B></FONT></TD>" & _
                    "           <TD width=25%><FONT FACE='Arial' SIZE=1>" & frmVDA640.txtUF & "</FONT></TD>" & _
                    "           <TD width=25%><FONT FACE='Arial' SIZE=2><B>Valor NF R$:</B></FONT></TD>" & _
                    "           <TD width=25%><FONT FACE='Arial' SIZE=1>" & Format(txtVlrNF, "Standard") & "</FONT></TD>" & _
                    "       </TR>" & _
                    "       <TR>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>NF. Devolu��o:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & txtNFDevolucao & "</FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Loja/NF Origem:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & frmVDA640.txtDeposito & " / " & frmVDA640.txtNota_Fiscal & "</FONT></TD>" & _
                    "       </TR>"
                    
strhtml = strhtml & "       <TR>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Emiss�o:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & txtEmissao & "</FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Motivo:</B></FONT></TD>" & _
                    "       </TR>" & _
                    "       <TR>" & _
                    "           <TD><FONT FACE='Arial' SIZE=2><B>Atendente:</B></FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & frmVDA640.cboAtendente & "</FONT></TD>" & _
                    "           <TD><FONT FACE='Arial' SIZE=1>" & frmVDA640.cmbAssunto & "/" & frmVDA640.cmbTopico & "/" & frmVDA640.cmbDetalhe & "</FONT></TD>" & _
                    "       </TR>" & _
                    "       <TR><TD colspan=4><FONT FACE='Arial' SIZE=2><B>Mensagem: </B></FONT>" & txtMensagem & "</TD></TR>" & _
                    "       <TR><TD colspan=4><HR></TD></TR>" & _
                    "       <TR><TD colspan=4><FONT FACE='Arial' SIZE=3><B><CENTER>Para garantir o recebimento da devolu��o na DPK, esta autoriza��o dever� permanecer anexada �<BR>Nota Fiscal de devolu��o.</CENTER></B></FONT></TD></TR>" & _
                    "       <TR><TD colspan=4><FONT FACE='Arial' SIZE=5><B><CENTER>ATEN��O</CENTER></B></FONT></TD></TR>" & _
                    "       <TR><TD colspan=4><FONT FACE='Arial' SIZE=3><B><CENTER>Nos casos de envio de produtos em garantia dos seguintes fornecedores: COFAP, VARGA, SACHS,<BR>ATE, � indispens�vel o acompanhamento do Certificado de Garantia preenchido, sob pena de<BR>recusa ou improced�ncia da reivindica��o. Para os demais fornecedores, informar o defeito e a<BR>data da venda.</CENTER></B></FONT></TD></TR>" & _
                    "       <TR><TD colspan=4><FONT FACE='Arial' SIZE=4><B><CENTER>As devolu��es s� ser�o aceitas mediante autoriza��o da Central de<BR>Atendimento e avalia��o f�sica dos produtos devolvidos e sua<BR>embalagens</CENTER></B></FONT></TD></TR>" & _
                    "       <TR><TD colspan=4 align=middle><IMG alt='' hspace=0 src='H:\ORACLE\SISTEMAS\VB\32BITS\VDA640\Logo_sac.jpg' height=90 width=150 align=baseline border=0></CENTER></TD></TR>" & _
                    "</TABLE>" & _
                    "</BODY>" & _
                    "</HTML>"

        'Gera arquivo "C:\Devolucao.HTML"
        Open "C:\Devolucao.HTML" For Output As #1
         
        Print #1, strhtml
        
        Close #1

    MsgBox "Arquivo C:\Devolucao.html Gerado com Sucesso", vbInformation, "Aten��o"

End Sub

Private Sub cmdSalvar_Click()
    If txtVlrNF = "" Then
        MsgBox "Informe o Valor da Nota Fiscal.", vbInformation, "Aten��o"
        txtVlrNF.SetFocus
        Exit Sub
    End If
    
    If txtNFDevolucao = "" Then
        MsgBox "Informe o N�mero da Nota de Devolu��o.", vbInformation, "Aten��o"
        txtNFDevolucao.SetFocus
        Exit Sub
    End If
    
    If txtEmissao.Value = "" Then
        MsgBox "Informe a Data de Emiss�o.", vbInformation, "Aten��o"
        txtEmissao.SetFocus
        Exit Sub
    End If
    
    'Gravar no banco os dados
    vBanco.Parameters.Remove "PM_NUM_ATENDIMENTO"
    vBanco.Parameters.Remove "PM_COD_CLIENTE"
    vBanco.Parameters.Remove "PM_NF_ORIGEM"
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Remove "PM_NF_DEVOLUCAO"
    vBanco.Parameters.Remove "PM_COD_TRANSP"
    vBanco.Parameters.Remove "PM_NOME_TRANSP"
    vBanco.Parameters.Remove "PM_FRETE"
    vBanco.Parameters.Remove "PM_VALOR"
    vBanco.Parameters.Remove "PM_DTEMISSAO"
    
    vBanco.Parameters.Add "PM_NUM_ATENDIMENTO", Val(Mid(frmVDA640.lblNumAtendimento, InStr(1, frmVDA640.lblNumAtendimento, "-") + 2)), 1
    vBanco.Parameters.Add "PM_COD_CLIENTE", frmVDA640.txtCodigo_Cliente, 1
    vBanco.Parameters.Add "PM_NF_ORIGEM", frmVDA640.txtNota_Fiscal, 1
    vBanco.Parameters.Add "PM_COD_LOJA", frmVDA640.txtDeposito, 1
    vBanco.Parameters.Add "PM_NF_DEVOLUCAO", txtNFDevolucao, 1
    If cboTransportadora.ListIndex <> -1 Then
       vBanco.Parameters.Add "PM_COD_TRANSP", IIf(cboTransportadora.ListIndex > 0, cboTransportadora.ItemData(cboTransportadora.ListIndex), 0), 1
    Else
       vBanco.Parameters.Add "PM_COD_TRANSP", Null, 1
    End If
    vBanco.Parameters.Add "PM_NOME_TRANSP", txtTransportadora, 1
    vBanco.Parameters.Add "PM_DTEMISSAO", CDate(txtEmissao.Value), 1
    
    If optFOB.Value = True Then
       vBanco.Parameters.Add "PM_FRETE", 1, 1
    Else
       vBanco.Parameters.Add "PM_FRETE", 2, 1
    End If
    
    vBanco.Parameters.Add "PM_VALOR", txtVlrNF, 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_INSERT_DEVOLUCAO(:PM_NUM_ATENDIMENTO, :PM_COD_CLIENTE, :PM_NF_ORIGEM, :PM_COD_LOJA, :PM_NF_DEVOLUCAO, :PM_COD_TRANSP, :PM_NOME_TRANSP, :PM_FRETE, :PM_VALOR, :PM_DTEMISSAO)")
        
    If vErro <> "" Then
        MsgBox vErro
        Exit Sub
    Else
        If cboTransportadora <> "<Nenhum>" And cboTransportadora <> "" Then
            If MsgBox("Deseja gerar Solicita��o de Coleta para esta Devolu��o ?", vbQuestion + vbYesNo) = vbYes Then
                Gravar_Coleta
            End If
        End If
    End If
    
    cmdImprimir.Enabled = True
    cmdGerarArqHtml.Enabled = True
    
End Sub

Private Sub Gravar_Coleta()

    'Atualizar a tabela CACLIE.DEVOLUCAO_COLETA
    
    vBanco.Parameters.Remove "PM_NUM_TRANSP"
    vBanco.Parameters.Add "PM_NUM_TRANSP", cboTransportadora.ItemData(cboTransportadora.ListIndex), 1
    
    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", frmVDA640.txtDeposito, 1
    
    vBanco.Parameters.Remove "PM_DIA"
    vBanco.Parameters.Add "PM_DIA", Formata_Data(txtEmissao), 1
    
    vBanco.Parameters.Remove "PM_ERRO"
    vBanco.Parameters.Add "PM_ERRO", "", 2
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA640.PR_UPDATE_COLETA(:PM_NUM_TRANSP, :PM_DIA, :PM_cod_LOJA, :PM_ERRO)")
        
    If Trim(vErro) <> "" Or vBanco.Parameters("PM_ERRO") <> "" Then
        MsgBox vErro & vBanco.Parameters("PM_ERRO")
        Exit Sub
    Else
        MsgBox "Coleta gerada com sucesso !"
    End If

End Sub

Private Sub cmdImprimir_Click()
    
    Atribuir_Campo "NumAtendimento", "N� Atendimento: " & Mid(frmVDA640.lblNumAtendimento, InStr(1, frmVDA640.lblNumAtendimento, "-") + 2), 1
    
    Atribuir_Campo "Remetente", frmVDA640.txtRazao_Social, 2
    Atribuir_Campo "Transportadora", IIf(cboTransportadora.ListIndex > 0, cboTransportadora, txtTransportadora), 2
    Atribuir_Campo "ValorNF", Format(txtVlrNF, "Standard"), 2
    Atribuir_Campo "NFDevolucao", txtNFDevolucao, 2
    Atribuir_Campo "Emissao", txtEmissao, 2
    Atribuir_Campo "Mensagem", txtMensagem, 2

    Atribuir_Campo "LOJA", frmVDA640.txtDeposito & " / " & frmVDA640.txtNota_Fiscal, 2
    Atribuir_Campo "UF", frmVDA640.txtUF, 2
    Atribuir_Campo "Atendente", frmVDA640.cboAtendente, 2
    Atribuir_Campo "DescMotivo", frmVDA640.cmbAssunto & "/" & frmVDA640.cmbTopico & "/" & frmVDA640.cmbDetalhe, 2
    Atribuir_Campo "Cidade", frmVDA640.txtCidade, 2
    'Atribuir_Campo "NFOrigem", frmVDA640.txtNota_Fiscal, 2
    
    If optFOB.Value = True Then
       Atribuir_Campo "FRETE", "Frete por conta do Cliente", 2
    Else
       Atribuir_Campo "FRETE", "Frete por Conta da DPK", 2
    End If
    
      
    rptDevolucao.Show 1

End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.width - Me.width) / 2

    Preencher_Transportadora

    txtEmissao.Value = Date

    '-- BUSCAR OS USU�RIOS CADASTRADOS NA INTRANET
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     'Ler dados
     vBanco.Parameters.Remove "PM_NUM_ATENDIMENTO"
     vBanco.Parameters.Add "PM_NUM_ATENDIMENTO", Val(Mid(frmVDA640.lblNumAtendimento, InStr(1, frmVDA640.lblNumAtendimento, "-") + 2)), 1
     
     vSql = "Producao.PCK_VDA640.PR_SELECT_DEVOLUCAO(:vCursor, :PM_NUM_ATENDIMENTO)"
     
     If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
         Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
         Exit Sub
     Else
         Set vObjOracle = vBanco.Parameters("VCURSOR").Value
     End If
     
     If vObjOracle.EOF = False Then
        If IsNull(vObjOracle!nome_transp) Then
            cboTransportadora.ListIndex = -1
        Else
            cboTransportadora = Trim(vObjOracle!nome_transp)
        End If
        
        txtTransportadora = IIf(IsNull(vObjOracle!Nome_transportadora), "", Trim(vObjOracle!Nome_transportadora))
        
        If Not IsNull(vObjOracle!dt_emissao) Then
            txtEmissao = vObjOracle!dt_emissao
        End If
        'Conforme combinado com Natalia
        'A linha abaixo estava comentada conforme comentario acima.
        'txtMensagem = vObjOracle!Mensagem
        txtNFDevolucao = vObjOracle!NF_DEVOLUCAO
        txtVlrNF = vObjOracle!VL_NOTA
        If vObjOracle!TIPO_FRETE = 1 Then
           optFOB.Value = True
        Else
           optCIF.Value = True
        End If
        
        txtEmissao.Enabled = False
    End If

End Sub

Function Atribuir_Campo(vCampo As String, vValor As String, Secao As Integer)
    Dim i As Integer
    For i = 1 To rptDevolucao.Sections(Secao).Controls.Count
        If UCase(rptDevolucao.Sections(Secao).Controls(i).Name) = "LBL" & UCase(vCampo) Then
            rptDevolucao.Sections(Secao).Controls(i).Caption = vValor
            Exit For
        End If
    Next
End Function

Function Atribuir_Campo_Coleta(vCampo As String, vValor As String, Secao As Integer)
    Dim i As Integer
    For i = 1 To rptColeta.Sections(Secao).Controls.Count
        If UCase(rptColeta.Sections(Secao).Controls(i).Name) = "LBL" & UCase(vCampo) Then
            rptColeta.Sections(Secao).Controls(i).Caption = vValor
            Exit For
        End If
    Next
End Function

Sub Preencher_Transportadora()
     Criar_Cursor vBanco.Parameters, "vCursor"
     
     vSql = "Producao.PCK_VDA640.PR_SELECT_TRANSP(:vCursor)"
     
     If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
         Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_USUARIO, entre em contato com o SUPORTE!")
         Exit Sub
     Else
         Set vObjOracle = vBanco.Parameters("VCURSOR").Value
     End If

    cboTransportadora.AddItem "<Nenhum>"

    While vObjOracle.EOF = False
        cboTransportadora.AddItem Trim(vObjOracle!nome_transp)
        cboTransportadora.ItemData(cboTransportadora.NewIndex) = Trim(vObjOracle!COD_Transp)
        vObjOracle.MoveNext
    Wend

End Sub

Private Sub txtEmissao_Change()
    If CDate(txtEmissao.Value) > Format(Date, "DD/MM/YY") Then
        MsgBox "A data deve ser menor ou igual a data de hoje.", vbInformation
        txtEmissao.SetFocus
    End If
End Sub

Private Sub txtNFDevolucao_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtTransportadora_Change()
    cboTransportadora.ListIndex = -1
End Sub

Private Sub txtVlrNF_KeyPress(KeyAscii As Integer)
    If KeyAscii = 44 Then
        KeyAscii = 0
    Else
        KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVlrNF)
    End If
End Sub

'Sub DefinirImpressoraPadrao(pImpressora As String)
'
'    Dim osinfo As OSVERSIONINFO
'    Dim retvalue As Integer
'
'    osinfo.dwOSVersionInfoSize = 148
'    osinfo.szCSDVersion = Space$(128)
'    retvalue = GetVersionExA(osinfo)
'
'    If osinfo.dwPlatformId = VER_PLATFORM_WIN32_WINDOWS Then
'
'        Call Win95SetDefaultPrinter(pImpressora)
'
'    Else
'
'        Call WinNTSetDefaultPrinter(pImpressora)
'
'    End If
'
'    Dim vAux As Printer
'
'    For Each vAux In Printers
'
'        If UCase(vAux.DeviceName) = UCase(pImpressora) Then
'
'            Set Printer = vAux
'
'        End If
'
'    Next
'
'End Sub TI-6746

