VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{20C62CAE-15DA-101B-B9A8-444553540000}#1.1#0"; "MSMAPI32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmVDA640 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "VDA640 - ATENDIMENTO AO CLIENTE"
   ClientHeight    =   8130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11865
   Icon            =   "frmVDA640.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8130
   ScaleWidth      =   11865
   WindowState     =   2  'Maximized
   Begin VB.Frame Frame7 
      Caption         =   "N� Atendimento"
      Height          =   1335
      Left            =   10110
      TabIndex        =   139
      Top             =   810
      Visible         =   0   'False
      Width           =   1695
      Begin VB.ListBox lstNotas 
         Appearance      =   0  'Flat
         Height          =   1005
         Left            =   90
         TabIndex        =   140
         Top             =   240
         Visible         =   0   'False
         Width           =   1515
      End
   End
   Begin VB.TextBox txtNumNota 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10950
      TabIndex        =   138
      TabStop         =   0   'False
      Top             =   450
      Width           =   870
   End
   Begin VB.PictureBox PicEmailDesatualizado 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   165
      Left            =   11520
      Picture         =   "frmVDA640.frx":23D2
      ScaleHeight     =   135
      ScaleWidth      =   165
      TabIndex        =   130
      ToolTipText     =   "Coment�rios"
      Top             =   2520
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.TextBox txtEmailDesatualizado 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8940
      MultiLine       =   -1  'True
      TabIndex        =   129
      Top             =   2490
      Visible         =   0   'False
      Width           =   2805
   End
   Begin VB.Timer timer_color 
      Enabled         =   0   'False
      Left            =   30
      Top             =   540
   End
   Begin MSMAPI.MAPIMessages MAPIMensagem 
      Left            =   1110
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      AddressEditFieldCount=   1
      AddressModifiable=   0   'False
      AddressResolveUI=   0   'False
      FetchSorted     =   0   'False
      FetchUnreadOnly =   0   'False
   End
   Begin MSMAPI.MAPISession MAPISessao 
      Left            =   510
      Top             =   30
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DownloadMail    =   -1  'True
      LogonUI         =   -1  'True
      NewSession      =   0   'False
   End
   Begin VB.TextBox txtNumAtendimento 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   10965
      TabIndex        =   116
      TabStop         =   0   'False
      Top             =   60
      Width           =   870
   End
   Begin VB.Frame fraCliente 
      Caption         =   "Dados do Cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1680
      Left            =   60
      TabIndex        =   54
      Top             =   990
      Width           =   11760
      Begin VB.ListBox lstEmail 
         Appearance      =   0  'Flat
         Height          =   930
         Left            =   8670
         Style           =   1  'Checkbox
         TabIndex        =   125
         Top             =   660
         Width           =   3015
      End
      Begin VB.TextBox txtTelefone2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7260
         Locked          =   -1  'True
         TabIndex        =   48
         Top             =   1275
         Width           =   1365
      End
      Begin VB.TextBox txtDDD2 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6675
         Locked          =   -1  'True
         TabIndex        =   47
         Top             =   1275
         Width           =   510
      End
      Begin VB.TextBox txtFilial 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   765
         Locked          =   -1  'True
         TabIndex        =   46
         Top             =   1275
         Width           =   3300
      End
      Begin VB.TextBox txtTelefone1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7260
         Locked          =   -1  'True
         TabIndex        =   44
         Top             =   930
         Width           =   1365
      End
      Begin VB.TextBox txtDDD1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6675
         Locked          =   -1  'True
         TabIndex        =   43
         Top             =   930
         Width           =   510
      End
      Begin VB.TextBox txtCod_Representante 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   765
         Locked          =   -1  'True
         TabIndex        =   40
         Top             =   930
         Width           =   735
      End
      Begin VB.TextBox txtNome_Representante 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1575
         Locked          =   -1  'True
         TabIndex        =   41
         Top             =   930
         Width           =   4080
      End
      Begin VB.TextBox txtUF 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   8220
         Locked          =   -1  'True
         TabIndex        =   38
         Top             =   585
         Width           =   390
      End
      Begin VB.TextBox txtCidade 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5895
         Locked          =   -1  'True
         TabIndex        =   36
         Top             =   585
         Width           =   2010
      End
      Begin VB.TextBox txtTipo_Fiel 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3615
         Locked          =   -1  'True
         TabIndex        =   34
         Top             =   585
         Width           =   1620
      End
      Begin VB.TextBox txtCGC 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   750
         TabIndex        =   3
         Top             =   585
         Width           =   1950
      End
      Begin VB.TextBox txtContato 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   9360
         TabIndex        =   31
         Top             =   240
         Width           =   1650
      End
      Begin VB.TextBox txtRazao_Social 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3915
         TabIndex        =   2
         ToolTipText     =   "D� 2 clicks para pesquisar pela raz�o social"
         Top             =   240
         Width           =   4680
      End
      Begin VB.TextBox txtCodigo_Cliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   750
         TabIndex        =   1
         Top             =   240
         Width           =   915
      End
      Begin Bot�o.cmd cmdDuplicatas 
         Height          =   330
         Left            =   1710
         TabIndex        =   123
         TabStop         =   0   'False
         Top             =   240
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   582
         BTYPE           =   3
         TX              =   "Duplicatas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA640.frx":2735
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEnviarCliente 
         Height          =   480
         Left            =   11070
         TabIndex        =   127
         TabStop         =   0   'False
         ToolTipText     =   "Enviar e-mail para Cliente"
         Top             =   150
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   847
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA640.frx":2751
         PICN            =   "frmVDA640.frx":276D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblFilial 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Filial:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   360
         TabIndex        =   45
         Top             =   1365
         Width           =   360
      End
      Begin VB.Label lblTelefone 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Telefones:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5790
         TabIndex        =   42
         Top             =   1020
         Width           =   870
      End
      Begin VB.Label lblRepresentante 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Repres.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   45
         TabIndex        =   39
         Top             =   1020
         Width           =   675
      End
      Begin VB.Label lblUF 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   7965
         TabIndex        =   37
         Top             =   675
         Width           =   270
      End
      Begin VB.Label lblCidade 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5310
         TabIndex        =   35
         Top             =   660
         Width           =   600
      End
      Begin VB.Label lblTipo_Fiel 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tipo Fiel:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   2865
         TabIndex        =   33
         Top             =   675
         Width           =   735
      End
      Begin VB.Label lblCGC 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "CGC:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   315
         TabIndex        =   32
         Top             =   675
         Width           =   390
      End
      Begin VB.Label lblContato 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   8625
         TabIndex        =   30
         Top             =   330
         Width           =   720
      End
      Begin VB.Label lblRazao_Social 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Raz�o Social:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   2880
         TabIndex        =   29
         Top             =   330
         Width           =   1035
      End
      Begin VB.Label lblCodigo_Cliente 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "C�digo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   0
         Top             =   330
         Width           =   615
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   53
      Top             =   900
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   50
      Top             =   7800
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8229
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "21/3/2011"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "15:27"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSobre 
      Height          =   825
      Left            =   8775
      TabIndex        =   51
      TabStop         =   0   'False
      ToolTipText     =   "Sobre"
      Top             =   30
      Width           =   600
      _ExtentX        =   1058
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Sobre"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":3047
      PICN            =   "frmVDA640.frx":3063
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   825
      Left            =   9390
      TabIndex        =   52
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   30
      Width           =   630
      _ExtentX        =   1111
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":3D3D
      PICN            =   "frmVDA640.frx":3D59
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCadastros 
      Height          =   825
      Left            =   2835
      TabIndex        =   56
      TabStop         =   0   'False
      ToolTipText     =   "Cadastro de Informa��es"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Cadastro"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":4A33
      PICN            =   "frmVDA640.frx":4A4F
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdPendencias 
      Height          =   825
      Left            =   1170
      TabIndex        =   57
      TabStop         =   0   'False
      ToolTipText     =   "Pend�ncias para Retorno"
      Top             =   30
      Width           =   930
      _ExtentX        =   1640
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Pend�ncias"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":5729
      PICN            =   "frmVDA640.frx":5745
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdLimpar 
      Height          =   825
      Left            =   3600
      TabIndex        =   58
      TabStop         =   0   'False
      ToolTipText     =   "Limpar a tela"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Limpar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":5B97
      PICN            =   "frmVDA640.frx":5BB3
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAgenda 
      Height          =   825
      Left            =   2115
      TabIndex        =   59
      TabStop         =   0   'False
      ToolTipText     =   "Agendar contatos extras"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Agenda"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":5ECD
      PICN            =   "frmVDA640.frx":5EE9
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stb 
      Height          =   4965
      Left            =   30
      TabIndex        =   55
      Top             =   2730
      Width           =   11760
      _ExtentX        =   20743
      _ExtentY        =   8758
      _Version        =   393216
      Tab             =   1
      TabHeight       =   617
      ShowFocusRect   =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Liga��es Anteriores"
      TabPicture(0)   =   "frmVDA640.frx":633B
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "picFecharTool"
      Tab(0).Control(1)=   "txtTipComentarios"
      Tab(0).Control(2)=   "FraFinalizados"
      Tab(0).Control(3)=   "FraEmAberto"
      Tab(0).Control(4)=   "cmdAtualizar"
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Atendimento"
      TabPicture(1)   =   "frmVDA640.frx":6357
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "FraAtendimento"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Faturamento"
      TabPicture(2)   =   "frmVDA640.frx":6373
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label28"
      Tab(2).Control(1)=   "MSFlexGrid2"
      Tab(2).Control(2)=   "Frame6"
      Tab(2).ControlCount=   3
      Begin VB.PictureBox picFecharTool 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   165
         Left            =   -66150
         Picture         =   "frmVDA640.frx":638F
         ScaleHeight     =   135
         ScaleWidth      =   165
         TabIndex        =   113
         ToolTipText     =   "Coment�rios"
         Top             =   1260
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.TextBox txtTipComentarios 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0FFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   -73410
         MultiLine       =   -1  'True
         TabIndex        =   112
         ToolTipText     =   "Coment�rios"
         Top             =   1200
         Visible         =   0   'False
         Width           =   7515
      End
      Begin VB.Frame FraFinalizados 
         Height          =   2295
         Left            =   -74850
         TabIndex        =   104
         Top             =   2550
         Width           =   10665
         Begin VB.PictureBox PicFecharFim 
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            ForeColor       =   &H80000008&
            Height          =   165
            Left            =   8490
            Picture         =   "frmVDA640.frx":66F2
            ScaleHeight     =   135
            ScaleWidth      =   165
            TabIndex        =   114
            ToolTipText     =   "Coment�rios"
            Top             =   930
            Visible         =   0   'False
            Width           =   195
         End
         Begin VB.TextBox txtTipComentariosFim 
            Appearance      =   0  'Flat
            BackColor       =   &H00C0FFFF&
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   1230
            MultiLine       =   -1  'True
            TabIndex        =   115
            ToolTipText     =   "Coment�rios"
            Top             =   870
            Visible         =   0   'False
            Width           =   7515
         End
         Begin MSFlexGridLib.MSFlexGrid mfgFinalizado 
            Height          =   2010
            Left            =   60
            TabIndex        =   105
            Top             =   210
            Width           =   10515
            _ExtentX        =   18547
            _ExtentY        =   3545
            _Version        =   393216
            Cols            =   9
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            HighLight       =   2
            AllowUserResizing=   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Label lblQtd_Finalizado 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   1215
            TabIndex        =   107
            Top             =   -30
            Width           =   120
         End
         Begin VB.Label lblFinalisado 
            Appearance      =   0  'Flat
            Caption         =   "Finalizados:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   75
            TabIndex        =   106
            Top             =   -30
            Width           =   1140
         End
      End
      Begin VB.Frame FraEmAberto 
         Height          =   2175
         Left            =   -74850
         TabIndex        =   100
         Top             =   360
         Width           =   11505
         Begin MSFlexGridLib.MSFlexGrid mfgAberto 
            Height          =   1890
            Left            =   30
            TabIndex        =   101
            Top             =   210
            Width           =   11370
            _ExtentX        =   20055
            _ExtentY        =   3334
            _Version        =   393216
            Cols            =   10
            FixedCols       =   0
            BackColorBkg    =   -2147483633
            HighLight       =   2
            AllowUserResizing=   1
            Appearance      =   0
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin VB.Image imgComentarios 
            Height          =   90
            Left            =   1470
            Picture         =   "frmVDA640.frx":6A55
            Top             =   120
            Visible         =   0   'False
            Width           =   105
         End
         Begin VB.Label lblQtd_Aberto 
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "0"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   1245
            TabIndex        =   103
            Top             =   0
            Width           =   120
         End
         Begin VB.Label lblAberto 
            Appearance      =   0  'Flat
            Caption         =   "Em Aberto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   90
            TabIndex        =   102
            Top             =   0
            Width           =   1155
         End
      End
      Begin Threed.SSFrame FraAtendimento 
         Height          =   4515
         Left            =   90
         TabIndex        =   75
         Top             =   390
         Width           =   11655
         _Version        =   65536
         _ExtentX        =   20558
         _ExtentY        =   7964
         _StockProps     =   14
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.ComboBox cmbAssuntoCompleto 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmVDA640.frx":6D9C
            Left            =   90
            List            =   "frmVDA640.frx":6D9E
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   136
            Top             =   2280
            Visible         =   0   'False
            Width           =   1245
         End
         Begin VB.ComboBox cmbTopicoCompleto 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3180
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   135
            Top             =   2280
            Visible         =   0   'False
            Width           =   1245
         End
         Begin VB.ComboBox cmbDetalheCompleto 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6300
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   134
            Top             =   2280
            Visible         =   0   'False
            Width           =   1245
         End
         Begin TabDlg.SSTab SSTab 
            Height          =   1305
            Left            =   3600
            TabIndex        =   132
            TabStop         =   0   'False
            Top             =   120
            Width           =   7995
            _ExtentX        =   14102
            _ExtentY        =   2302
            _Version        =   393216
            Tabs            =   2
            TabsPerRow      =   2
            TabHeight       =   388
            WordWrap        =   0   'False
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            TabCaption(0)   =   "Coment�rios"
            TabPicture(0)   =   "frmVDA640.frx":6DA0
            Tab(0).ControlEnabled=   -1  'True
            Tab(0).Control(0)=   "txtComentarios"
            Tab(0).Control(0).Enabled=   0   'False
            Tab(0).ControlCount=   1
            TabCaption(1)   =   "Hist�rico"
            TabPicture(1)   =   "frmVDA640.frx":6DBC
            Tab(1).ControlEnabled=   0   'False
            Tab(1).Control(0)=   "LsvHistorico"
            Tab(1).ControlCount=   1
            Begin VB.TextBox txtComentarios 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   1020
               Left            =   60
               MaxLength       =   2000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   11
               Top             =   240
               Width           =   7875
            End
            Begin MSComctlLib.ListView LsvHistorico 
               Height          =   1005
               Left            =   -74940
               TabIndex        =   133
               TabStop         =   0   'False
               Top             =   240
               Width           =   7875
               _ExtentX        =   13891
               _ExtentY        =   1773
               View            =   3
               LabelEdit       =   1
               LabelWrap       =   -1  'True
               HideSelection   =   0   'False
               FullRowSelect   =   -1  'True
               GridLines       =   -1  'True
               _Version        =   393217
               ForeColor       =   -2147483640
               BackColor       =   12648447
               BorderStyle     =   1
               Appearance      =   0
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               NumItems        =   3
               BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  Text            =   "Seq"
                  Object.Width           =   882
               EndProperty
               BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   1
                  Text            =   "Atendente"
                  Object.Width           =   5292
               EndProperty
               BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
                  SubItemIndex    =   2
                  Text            =   "Data"
                  Object.Width           =   2999
               EndProperty
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Atendente"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   645
            Left            =   2850
            TabIndex        =   110
            Top             =   3810
            Width           =   3405
            Begin VB.ComboBox cboAtendente 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   90
               Sorted          =   -1  'True
               Style           =   2  'Dropdown List
               TabIndex        =   131
               Top             =   240
               Width           =   3240
            End
            Begin VB.Label lblAtendente 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   345
               Left            =   90
               TabIndex        =   111
               Top             =   240
               Visible         =   0   'False
               Width           =   3195
            End
         End
         Begin VB.Frame FraCodAtendimento 
            Caption         =   "Atendimento"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   645
            Left            =   6270
            TabIndex        =   108
            Top             =   3810
            Width           =   2985
            Begin VB.Label lblNumAtendimento 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H000000FF&
               Height          =   345
               Left            =   120
               TabIndex        =   109
               Top             =   210
               Width           =   2745
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Enviar e-mail:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   645
            Left            =   30
            TabIndex        =   88
            Top             =   3150
            Visible         =   0   'False
            Width           =   2610
            Begin VB.TextBox txtEmail_Para 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   630
               TabIndex        =   25
               Top             =   225
               Width           =   240
            End
            Begin VB.TextBox txtEmail_Copia 
               Appearance      =   0  'Flat
               BeginProperty Font 
                  Name            =   "Courier New"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Left            =   1470
               TabIndex        =   26
               Top             =   225
               Width           =   210
            End
            Begin Bot�o.cmd cmdEnviar_Email_ 
               Height          =   330
               Left            =   1770
               TabIndex        =   27
               TabStop         =   0   'False
               ToolTipText     =   "Enviar e-mail"
               Top             =   210
               Width           =   735
               _ExtentX        =   1296
               _ExtentY        =   582
               BTYPE           =   3
               TX              =   "E-mail"
               ENAB            =   -1  'True
               BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               COLTYPE         =   1
               FOCUSR          =   0   'False
               BCOL            =   16777215
               BCOLO           =   16777215
               FCOL            =   0
               FCOLO           =   0
               MCOL            =   12632256
               MPTR            =   1
               MICON           =   "frmVDA640.frx":6DD8
               UMCOL           =   -1  'True
               SOFT            =   0   'False
               PICPOS          =   0
               NGREY           =   0   'False
               FX              =   3
               HAND            =   0   'False
               CHECK           =   0   'False
               VALUE           =   0   'False
            End
            Begin VB.Label lblEmail_Para 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               Caption         =   "Para:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   45
               TabIndex        =   90
               Top             =   315
               Width           =   555
            End
            Begin VB.Label lblEmail_Copia 
               Alignment       =   1  'Right Justify
               Appearance      =   0  'Flat
               Caption         =   "C/C:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   285
               Left            =   900
               TabIndex        =   89
               Top             =   270
               Width           =   555
            End
         End
         Begin VB.TextBox txtProcedimento 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   960
            Left            =   30
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   24
            Top             =   2850
            Width           =   11535
         End
         Begin VB.ComboBox cmbDetalhe 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   6300
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   23
            Top             =   2280
            Width           =   3015
         End
         Begin VB.ComboBox cmbTopico 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   3180
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   22
            Top             =   2280
            Width           =   3015
         End
         Begin VB.CheckBox chkComplemento 
            Appearance      =   0  'Flat
            Caption         =   "Falta Complemento"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   5100
            TabIndex        =   17
            Top             =   1500
            Width           =   1905
         End
         Begin VB.Frame Frame1 
            Caption         =   "Contato de:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   1320
            Left            =   1785
            TabIndex        =   87
            Top             =   120
            Width           =   1770
            Begin VB.OptionButton optCliente 
               Appearance      =   0  'Flat
               Caption         =   "Cliente"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   8
               Top             =   270
               Value           =   -1  'True
               Width           =   1095
            End
            Begin VB.OptionButton optRepresentante 
               Appearance      =   0  'Flat
               Caption         =   "Representante"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   9
               Top             =   510
               Width           =   1545
            End
            Begin VB.OptionButton optTelevendas 
               Appearance      =   0  'Flat
               Caption         =   "Televendas"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   10
               Top             =   750
               Width           =   1320
            End
            Begin VB.OptionButton optOutros 
               Appearance      =   0  'Flat
               Caption         =   "Outros"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   12
               Top             =   990
               Width           =   960
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Situa��o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   1320
            Left            =   60
            TabIndex        =   86
            Top             =   120
            Width           =   1680
            Begin VB.OptionButton optLiberado 
               Appearance      =   0  'Flat
               Caption         =   "Liberado"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   120
               TabIndex        =   7
               Top             =   990
               Width           =   1320
            End
            Begin VB.OptionButton optSolucionado 
               Appearance      =   0  'Flat
               Caption         =   "Solucionado"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   6
               Top             =   750
               Width           =   1365
            End
            Begin VB.OptionButton optRetornado 
               Appearance      =   0  'Flat
               Caption         =   "Retornado"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   5
               Top             =   510
               Width           =   1275
            End
            Begin VB.OptionButton optPendente 
               Appearance      =   0  'Flat
               Caption         =   "Pendente"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Left            =   135
               TabIndex        =   4
               Top             =   270
               Value           =   -1  'True
               Width           =   1275
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Prazo para:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   1200
            Left            =   9630
            TabIndex        =   76
            Top             =   1590
            Width           =   1905
            Begin VB.TextBox txtRetorno 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000004&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   315
               Left            =   990
               Locked          =   -1  'True
               TabIndex        =   79
               Top             =   240
               Width           =   420
            End
            Begin VB.TextBox txtSolucao 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000004&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   990
               Locked          =   -1  'True
               TabIndex        =   78
               Top             =   570
               Width           =   420
            End
            Begin VB.TextBox txtLiberacao 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000004&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   990
               Locked          =   -1  'True
               TabIndex        =   77
               Top             =   885
               Width           =   420
            End
            Begin VB.Label lblRetorno 
               Appearance      =   0  'Flat
               Caption         =   "Retorno:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   0
               Left            =   135
               TabIndex        =   85
               Top             =   270
               Width           =   780
            End
            Begin VB.Label lblSolucao 
               Appearance      =   0  'Flat
               Caption         =   "Solu��o:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   0
               Left            =   135
               TabIndex        =   84
               Top             =   585
               Width           =   780
            End
            Begin VB.Label lblLiberacao 
               Appearance      =   0  'Flat
               Caption         =   "Libera��o:"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   0
               Left            =   135
               TabIndex        =   83
               Top             =   900
               Width           =   870
            End
            Begin VB.Label lblDias_Retorno 
               Appearance      =   0  'Flat
               Caption         =   "dias"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   1
               Left            =   1485
               TabIndex        =   82
               Top             =   270
               Width           =   285
            End
            Begin VB.Label lblDias_Solucao 
               Appearance      =   0  'Flat
               Caption         =   "dias"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   1
               Left            =   1485
               TabIndex        =   81
               Top             =   585
               Width           =   330
            End
            Begin VB.Label lblDias_Liberacao 
               Appearance      =   0  'Flat
               Caption         =   "dias"
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   9
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   240
               Index           =   1
               Left            =   1485
               TabIndex        =   80
               Top             =   900
               Width           =   330
            End
         End
         Begin VB.TextBox txtNota_Fiscal 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   750
            MaxLength       =   6
            TabIndex        =   14
            Top             =   1680
            Width           =   1095
         End
         Begin VB.TextBox txtDeposito 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   60
            MaxLength       =   2
            TabIndex        =   13
            Top             =   1680
            Width           =   510
         End
         Begin VB.TextBox txtComissao 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1920
            MaxLength       =   5
            TabIndex        =   15
            Top             =   1680
            Width           =   780
         End
         Begin VB.CheckBox chkProcede 
            Appearance      =   0  'Flat
            Caption         =   "Procede"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   5100
            TabIndex        =   18
            Top             =   1740
            Width           =   1125
         End
         Begin VB.CheckBox chkDevolucao 
            Appearance      =   0  'Flat
            Caption         =   "Gerou Devolu��o"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   7110
            TabIndex        =   20
            Top             =   1710
            Width           =   1785
         End
         Begin VB.CheckBox chkProrrogacao 
            Appearance      =   0  'Flat
            Caption         =   "Gerou Prorroga��o"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   7110
            TabIndex        =   19
            Top             =   1470
            Width           =   1875
         End
         Begin VB.TextBox txtAbatimento 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2820
            MaxLength       =   14
            TabIndex        =   16
            Top             =   1680
            Width           =   1200
         End
         Begin Bot�o.cmd cmdGravar_Atendimento 
            Height          =   630
            Left            =   10830
            TabIndex        =   28
            TabStop         =   0   'False
            ToolTipText     =   "Gravar Informa��es"
            Top             =   3840
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1111
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmVDA640.frx":6DF4
            PICN            =   "frmVDA640.frx":6E10
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdAdicionar 
            Height          =   630
            Left            =   10110
            TabIndex        =   99
            TabStop         =   0   'False
            ToolTipText     =   "Limpar Informa��es"
            Top             =   3840
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1111
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmVDA640.frx":7AEA
            PICN            =   "frmVDA640.frx":7B06
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdDuplEmAtraso 
            Height          =   570
            Left            =   60
            TabIndex        =   124
            TabStop         =   0   'False
            Top             =   3870
            Visible         =   0   'False
            Width           =   2745
            _ExtentX        =   4842
            _ExtentY        =   1005
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmVDA640.frx":7E20
            PICN            =   "frmVDA640.frx":7E3C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   1
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdEnviar_Email 
            Height          =   630
            Left            =   9390
            TabIndex        =   128
            TabStop         =   0   'False
            ToolTipText     =   "Enviar e-mail Interno"
            Top             =   3840
            Width           =   690
            _ExtentX        =   1217
            _ExtentY        =   1111
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   16777215
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmVDA640.frx":8716
            PICN            =   "frmVDA640.frx":8732
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.ComboBox cmbAssunto 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            ItemData        =   "frmVDA640.frx":900C
            Left            =   90
            List            =   "frmVDA640.frx":900E
            Sorted          =   -1  'True
            Style           =   2  'Dropdown List
            TabIndex        =   21
            Top             =   2280
            Width           =   3015
         End
         Begin VB.Label lblProcedimento 
            Appearance      =   0  'Flat
            Caption         =   "Procedimento:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   240
            Left            =   60
            TabIndex        =   98
            Top             =   2640
            Width           =   1230
         End
         Begin VB.Label lblDetalhe 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Detalhe:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   6300
            TabIndex        =   97
            Top             =   2070
            Width           =   690
         End
         Begin VB.Label lblTopico 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "T�pico:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   3165
            TabIndex        =   96
            Top             =   2070
            Width           =   615
         End
         Begin VB.Label lblAssunto 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Assunto:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   90
            TabIndex        =   95
            Top             =   2070
            Width           =   720
         End
         Begin VB.Label lblNota_Fiscal 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "NF:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   750
            TabIndex        =   94
            Top             =   1470
            Width           =   270
         End
         Begin VB.Label lblDeposito 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Dep.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   75
            TabIndex        =   93
            Top             =   1470
            Width           =   450
         End
         Begin VB.Label lblComissao 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Comiss�o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   1920
            TabIndex        =   92
            Top             =   1470
            Width           =   795
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            AutoSize        =   -1  'True
            Caption         =   "Vl.Abatimento:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   210
            Left            =   2820
            TabIndex        =   91
            Top             =   1470
            Width           =   1230
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Cabe�alho da Nota"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   1995
         Left            =   -74865
         TabIndex        =   60
         Top             =   540
         Width           =   11490
         Begin VB.TextBox Text29 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   4410
            TabIndex        =   71
            Top             =   675
            Width           =   510
         End
         Begin VB.TextBox Text28 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   2970
            TabIndex        =   69
            Top             =   675
            Width           =   510
         End
         Begin VB.TextBox Text27 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   855
            TabIndex        =   67
            Top             =   675
            Width           =   1185
         End
         Begin VB.TextBox Text26 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   3600
            TabIndex        =   65
            Top             =   270
            Width           =   1140
         End
         Begin VB.TextBox Text24 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   1575
            TabIndex        =   63
            Top             =   270
            Width           =   1140
         End
         Begin VB.TextBox Text23 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Left            =   630
            TabIndex        =   61
            Top             =   270
            Width           =   420
         End
         Begin VB.Label Label30 
            Appearance      =   0  'Flat
            Caption         =   "Nat. Op:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   2
            Left            =   3645
            TabIndex        =   72
            Top             =   765
            Width           =   735
         End
         Begin VB.Label Label30 
            Appearance      =   0  'Flat
            Caption         =   "Nat. Op:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   1
            Left            =   2205
            TabIndex        =   70
            Top             =   765
            Width           =   735
         End
         Begin VB.Label Label30 
            Appearance      =   0  'Flat
            Caption         =   "Emiss�o:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   0
            Left            =   135
            TabIndex        =   68
            Top             =   765
            Width           =   735
         End
         Begin VB.Label Label29 
            Appearance      =   0  'Flat
            Caption         =   "Pedido:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   2925
            TabIndex        =   66
            Top             =   360
            Width           =   690
         End
         Begin VB.Label Label18 
            Appearance      =   0  'Flat
            Caption         =   "NF:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   1260
            TabIndex        =   64
            Top             =   360
            Width           =   375
         End
         Begin VB.Label Label17 
            Appearance      =   0  'Flat
            Caption         =   "Dep.:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Left            =   135
            TabIndex        =   62
            Top             =   360
            Width           =   510
         End
      End
      Begin Bot�o.cmd cmdAtualizar 
         Height          =   690
         Left            =   -64020
         TabIndex        =   49
         TabStop         =   0   'False
         ToolTipText     =   "Gravar Informa��es"
         Top             =   4140
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmVDA640.frx":9010
         PICN            =   "frmVDA640.frx":902C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid2 
         Height          =   1680
         Left            =   -74865
         TabIndex        =   73
         Top             =   2790
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   2963
         _Version        =   393216
         Cols            =   7
         BackColorBkg    =   -2147483633
         HighLight       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label28 
         Appearance      =   0  'Flat
         Caption         =   "Itens do Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   -74820
         TabIndex        =   74
         Top             =   2565
         Width           =   1455
      End
   End
   Begin MSComctlLib.ImageList imlBotoes 
      Left            =   -60
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVDA640.frx":9D06
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmVDA640.frx":A158
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin Bot�o.cmd cmdImprimirDevolucao 
      Height          =   825
      Left            =   4305
      TabIndex        =   118
      TabStop         =   0   'False
      ToolTipText     =   "Reimprimir Devolu��o"
      Top             =   30
      Width           =   840
      _ExtentX        =   1482
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Devolu��o"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":A472
      PICN            =   "frmVDA640.frx":A48E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEstatisticas 
      Height          =   825
      Left            =   5880
      TabIndex        =   119
      TabStop         =   0   'False
      ToolTipText     =   "Estat�sticas"
      Top             =   30
      Width           =   870
      _ExtentX        =   1535
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Estatistica"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":AD68
      PICN            =   "frmVDA640.frx":AD84
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdColeta 
      Height          =   825
      Left            =   5160
      TabIndex        =   120
      TabStop         =   0   'False
      ToolTipText     =   "Coleta"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Coleta"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":B65E
      PICN            =   "frmVDA640.frx":B67A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsColetas 
      Height          =   825
      Left            =   7530
      TabIndex        =   121
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Coletas"
      Top             =   30
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Coletas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":BF54
      PICN            =   "frmVDA640.frx":BF70
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdTxt 
      Height          =   825
      Left            =   6765
      TabIndex        =   122
      TabStop         =   0   'False
      ToolTipText     =   "Gerar relat�rios txt"
      Top             =   30
      Width           =   750
      _ExtentX        =   1323
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Relat�rio"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":CC4A
      PICN            =   "frmVDA640.frx":CC66
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEmail 
      Height          =   825
      Left            =   8205
      TabIndex        =   126
      TabStop         =   0   'False
      ToolTipText     =   "Cadastro de Email"
      Top             =   30
      Width           =   540
      _ExtentX        =   953
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Email"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVDA640.frx":D940
      PICN            =   "frmVDA640.frx":D95C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "N� Nota.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   10125
      TabIndex        =   137
      Top             =   540
      Width           =   780
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "N� Atend.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   10050
      TabIndex        =   117
      Top             =   150
      Width           =   900
   End
   Begin VB.Image Image1 
      Height          =   1035
      Left            =   -30
      MouseIcon       =   "frmVDA640.frx":E636
      MousePointer    =   99  'Custom
      Picture         =   "frmVDA640.frx":E940
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -90
      Width           =   1275
   End
End
Attribute VB_Name = "frmVDA640"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Type POINTAPI
    x As Long
    y As Long
End Type

Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Declare Function ScreenToClient Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long) As Long

Private Const LB_ITEMFROMPOINT = &H1A9

Dim vPosicao As Byte 'Indica o status do atendimento 1-optPendente, 2-optRetornado, 3-optLiberado, 4-optSolucionado

Private TimerID As Long
Public vNovo As Boolean 'Se true significa que o usuario clicou direto na guia e quer adicionar um atendimento

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)


Private Sub cboAtendente_Click()
    If cboAtendente.Tag = "" Then
        cboAtendente.Tag = Me.cboAtendente
    Else
        If UCase(cboAtendente) <> UCase(cboAtendente.Tag) Then
            If MsgBox("Confirma a mudan�a de Atendente ?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
               cboAtendente = cboAtendente.Tag
            Else
               cboAtendente.Tag = cboAtendente
            End If
        End If
    End If
End Sub

Private Sub chkComplemento_Click()
    If chkComplemento.Value = 1 Or chkComplemento.Value = True Then
        optPendente.Enabled = False
        optRetornado.Enabled = False
        optSolucionado.Enabled = False
        optLiberado.Enabled = False
    Else
        If optPendente.Value = True Or optPendente.Value = 1 Then
            Status_Opt "P"
        ElseIf optRetornado.Value = True Or optRetornado.Value = 1 Then
            Status_Opt "R"
        ElseIf optLiberado.Value = True Or optLiberado.Value = 1 Then
            Status_Opt "P"
        ElseIf optSolucionado.Value = True Or optSolucionado.Value = 1 Then
            Status_Opt "P"
        End If
    End If
End Sub

Private Sub cmbAssunto_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim vObjProced As Object
          Dim vObj As Object
          Dim vStatus As Byte
          
2         cmdImprimirDevolucao.Enabled = stb.Tab = 1 And InStr(1, UCase(cmbAssunto), "DEVOLU��O") > 0 And optSolucionado.Value = True

3         txtRetorno = 0
4         txtSolucao = 0
5         txtLiberacao = 0
              
6         cmbTopico.Enabled = True
7         cmbDetalhe.Enabled = True
          
8         cmbTopico.Clear
9         cmbDetalhe.Clear
              
10        txtProcedimento.Text = ""
              
11        If cmbAssunto.ListIndex = -1 Then Exit Sub
              
    'Eduardo - 06/10/2006
          
12        Criar_Cursor vBanco.Parameters, "vCursor"
         
13        vBanco.Parameters.Remove "PM_CODASSUNTO"
14        vBanco.Parameters.Remove "PM_TOPICO"
15        vBanco.Parameters.Remove "PM_DETALHE"

16        vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
17        vBanco.Parameters.Add "PM_TOPICO", 0, 1
18        vBanco.Parameters.Add "PM_DETALHE", 0, 1
19        vSql = "Producao.PCK_VDA640.PR_BUSCA_TOPICO_ATEND(:PM_CODASSUNTO, :vCursor)"
                  
          'Buscar T�picos
20        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
21           Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TOPICO_ATEND, entre em contato com o SUPORTE!")
22           Exit Sub
23        Else
24           Set vObj = vBanco.Parameters("VCURSOR").Value
25        End If

26        If vObj.EOF = False Then
27           Preenche_Combo cmbTopico, vObj
28        Else
29           cmbTopico.Enabled = False
30           cmbDetalhe.Enabled = False
31        End If
          
32        If Val(Right(Me.lblNumAtendimento, 6)) = 0 Then
'18           If cmbTopico.ListIndex = -1 Then
'19              vBanco.Parameters.Add "PM_TOPICO", 0, 1
'20           Else
'21              vBanco.Parameters.Add "PM_TOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
'22           End If
'23           If cmbDetalhe.ListIndex = -1 Then
'24              vBanco.Parameters.Add "PM_DETALHE", 0, 1
'25           Else
'26              vBanco.Parameters.Add "PM_DETALHE", cmbDetalhe.ItemData(cmbDetalhe.ListIndex), 1
'27           End If
33        Else
             vBanco.Parameters.Remove "PM_TOPICO"
             vBanco.Parameters.Remove "PM_DETALHE"
34           vBanco.Parameters.Add "PM_TOPICO", Val(0 & vObjOracle!cod_topico), 1
35           vBanco.Parameters.Add "PM_DETALHE", Val(0 & vObjOracle!cod_detalhe), 1
36        End If
          
37        vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_CODASSUNTO,:PM_TOPICO,:PM_DETALHE,:VCURSOR)"
              
38        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
39           Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PROCEDIMENTO, entre em contato com o SUPORTE!")
40           Exit Sub
41        Else
42           Set vObj = vBanco.Parameters("VCURSOR").Value
43        End If
          
'13        If cmbAssunto.Text <> "" Then
'15           Criar_Cursor vBanco.Parameters, "vCursor"
'40           vSql = "Producao.PCK_VDA640.PR_BUSCA_TOPICO_ATEND(:PM_CODASSUNTO, :vCursor)"
'
'41           If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
'42              Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TOPICO_ATEND, entre em contato com o SUPORTE!")
'43              Exit Sub
'44           Else
'45              Set vObjAssunto = vBanco.Parameters("VCURSOR").Value
'46           End If
'
'47           If vObjAssunto.EOF = False Then
'48              Preenche_Combo cmbTopico, vObjAssunto
'49           Else
'50              cmbTopico.Enabled = False
'51              cmbDetalhe.Enabled = False
'52           End If
'32        End If
             
          '-- BUSCA O PROCEDIMENTO RELACIONADO COM O ASSUNTO, T�PICO E DETALHE ESCOLHIDOS
'34        vBanco.Parameters.Remove "PM_DETALHE"
'35        vBanco.Parameters.Remove "PM_TOPICO"
              
          'Se estiver adicionando um novo
'36        If vNum_Atendimento = 0 Then
'37           vBanco.Parameters.Add "PM_ASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
'38        Else
'39           vBanco.Parameters.Add "PM_ASSUNTO", Val(vObjOracle!Cod_assunto), 1
'40        End If
              
'41        If vNum_Atendimento = 0 Then
'42           If cmbTopico.ListIndex = -1 Then
'43              vBanco.Parameters.Add "PM_TOPICO", 0, 1
'44              vBanco.Parameters.Add "PM_DETALHE", 0, 1
'45           Else
'46              vBanco.Parameters.Add "PM_TOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
'47              If cmbDetalhe.ListIndex = -1 Then
'48                 vBanco.Parameters.Add "PM_DETALHE", 0, 1
'49              Else
'50                 vBanco.Parameters.Add "PM_DETALHE", cmbDetalhe.ItemData(cmbDetalhe.ListIndex), 1
'51              End If
'52           End If
'53        Else
'54           vBanco.Parameters.Add "PM_TOPICO", Val(0 & vObjOracle!cod_topico), 1
'55           vBanco.Parameters.Add "PM_DETALHE", Val(0 & vObjOracle!cod_detalhe), 1
'56        End If
              
'57        Criar_Cursor vBanco.Parameters, "vCursor"
              
'58        vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_ASSUNTO,:PM_TOPICO,:PM_DETALHE,:VCURSOR)"
              
'59        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
'60           Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PROCEDIMENTO, entre em contato com o SUPORTE!")
'61           Exit Sub
'62        Else
'63           Set vObjAssunto = vBanco.Parameters("VCURSOR").Value
'64        End If
              
44        If vObj.RecordCount > 0 Then
45           If vObj("Status") = 0 Then
46              txtProcedimento.Text = vObj!PROC_RECLAMACAO
47              txtRetorno.Text = vObj!DIAS_RETORNO
48              If txtRetorno.Text = 0 Then
49                  optRetornado.Enabled = False
50              Else
51                  If vPosicao <= 2 Then optRetornado.Enabled = True
52              End If
53              txtSolucao.Text = vObj!DIAS_SOLUCAO

                optSolucionado.Enabled = IIf(txtSolucao = 0, False, True)

59              txtLiberacao.Text = vObj!DIAS_LIBERACAO
60              cmdGravar_Atendimento.Enabled = True
61          Else
62              cmdGravar_Atendimento.Enabled = False
63              txtProcedimento.Text = ""
64          End If
65        Else
66           cmdGravar_Atendimento.Enabled = False
67           txtProcedimento.Text = ""
68        End If

Trata_Erro:
69    If Err.Number <> 0 Then
70      MsgBox "Sub cmbAssunto_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
71    End If

End Sub



Private Sub cmbDetalhe_Click()

1         On Error GoTo Trata_Erro

          Dim vObjDetalhe As Object

2         txtProcedimento.Text = ""
          
         '-- BUSCA O PROCEDIMENTO RELACIONADO COM O ASSUNTO, T�PICO E DETALHE ESCOLHIDOS
3         vBanco.Parameters.Remove "PM_CODASSUNTO"
4         vBanco.Parameters.Remove "PM_CODTOPICO"
5         vBanco.Parameters.Remove "PM_CODDETALHE"

6         If cmbAssunto.Text <> "" And cmbTopico.Text <> "" Then
7             vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
8             vBanco.Parameters.Add "PM_CODTOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
9             If cmbDetalhe = "" Then
10                vBanco.Parameters.Add "PM_CODDETALHE", 0, 1
11            Else
12                vBanco.Parameters.Add "PM_CODDETALHE", cmbDetalhe.ItemData(cmbDetalhe.ListIndex), 1
13            End If
14            Criar_Cursor vBanco.Parameters, "vCursor"
              
15            vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_CODASSUNTO,:PM_CODTOPICO,:PM_CODDETALHE,:VCURSOR)"
              
16            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
17                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PROCEDIMENTO, entre em contato com o SUPORTE!")
18                Exit Sub
19            Else
20                Set vObjDetalhe = vBanco.Parameters("VCURSOR").Value
21            End If
              
22            If vObjDetalhe.RecordCount > 0 Then
23               If vObjDetalhe("Status") = 0 Then
24                  txtProcedimento.Text = vObjDetalhe!PROC_RECLAMACAO
25                  txtRetorno.Text = vObjDetalhe!DIAS_RETORNO
26                  optRetornado.Enabled = IIf(txtRetorno = 0, False, True)
27                  txtSolucao.Text = vObjDetalhe!DIAS_SOLUCAO
28                  optSolucionado.Enabled = IIf(txtSolucao = 0, False, True)
29                  txtLiberacao.Text = vObjDetalhe!DIAS_LIBERACAO
30                  cmdGravar_Atendimento.Enabled = True
31               Else
32                  cmdGravar_Atendimento.Enabled = False
33               End If
34            Else
35               cmdGravar_Atendimento.Enabled = False
36            End If
37        Else
38            Call vVB_Generica_001.Informar("O assunto ou t�pico ainda n�o foi escolhido!")
39            cmbAssunto.SetFocus
40            txtProcedimento.Text = ""
41        End If
          
Trata_Erro:
42        If Err.Number <> 0 Then
43            MsgBox "Sub cmbDetalhe_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
44        End If
End Sub

Private Sub cmbTopico_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim vObjTopico As Object

2         cmbDetalhe.Enabled = True
3         cmbDetalhe.Clear
4         txtProcedimento.Text = ""
          
         '-- CARREGA OS DETALHES
5         vBanco.Parameters.Remove "PM_CODASSUNTO"
6         vBanco.Parameters.Remove "PM_CODTOPICO"
         vBanco.Parameters.Remove "PM_CODDETALHE"
          
          vBanco.Parameters.Add "PM_CODDETALHE", 0, 1

7         If cmbAssunto.Text <> "" And cmbTopico.Text <> "" Then
              
8             vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
              
9             vBanco.Parameters.Add "PM_CODTOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
              
10            Criar_Cursor vBanco.Parameters, "vCursor"
              
11            vSql = "Producao.PCK_VDA640.PR_BUSCA_DETALHE_ATEND(:PM_CODASSUNTO, :PM_CODTOPICO, :vCursor)"
              
12            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
13                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_DETALHE_ATEND, entre em contato com o SUPORTE!")
14                Exit Sub
15            Else
16                Set vObjTopico = vBanco.Parameters("VCURSOR").Value
17            End If
18            If vObjTopico.EOF = False Then
19                Preenche_Combo cmbDetalhe, vObjTopico
21            Else
22                cmbDetalhe.Enabled = False
23                cmbDetalheCompleto.Enabled = False
24            End If
25        End If
          
         '-- BUSCA O PROCEDIMENTO RELACIONADO COM O ASSUNTO E T�PICO ESCOLHIDOS
'26        vBanco.Parameters.Remove "PM_CODASSUNTO"
'27        vBanco.Parameters.Remove "PM_CODTOPICO"
'28        vBanco.Parameters.Remove "PM_CODDETALHE"
          
29        If cmbAssunto.Text <> "" Then
'30            vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
              
'31            If cmbTopico.ListIndex = -1 Then
'32                vBanco.Parameters.Add "PM_CODTOPICO", 0, 1
'33            Else
'34                vBanco.Parameters.Add "PM_CODTOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
'35            End If
              
'36            vBanco.Parameters.Add "PM_CODDETALHE", 0, 1
              
'37            Criar_Cursor vBanco.Parameters, "vCursor"
              
'38            vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_CODASSUNTO,:PM_CODTOPICO,:PM_CODDETALHE,:VCURSOR)"
38            vSql = "Producao.PCK_VDA640.PR_BUSCA_PROCEDIMENTO(:PM_CODASSUNTO,:PM_CODTOPICO,:PM_DETALHE,:VCURSOR)"
              
39            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
40                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PROCEDIMENTO, entre em contato com o SUPORTE!")
41                Exit Sub
42            Else
43                Set vObjTopico = vBanco.Parameters("VCURSOR").Value
44            End If
              
45            If vObjTopico.RecordCount > 0 Then
                  If vObjTopico("Status") = 0 Then
46                   txtProcedimento.Text = vObjTopico!PROC_RECLAMACAO
47                   txtRetorno.Text = vObjTopico!DIAS_RETORNO
48                   If txtRetorno.Text = 0 Then
49                      optRetornado.Enabled = False
50                   Else
51                      If vPosicao <= 2 Then optRetornado.Enabled = True
52                   End If
                  
53                   txtSolucao.Text = vObjTopico!DIAS_SOLUCAO
                     optSolucionado.Enabled = IIf(txtSolucao = 0, False, True)

59                   txtLiberacao.Text = vObjTopico!DIAS_LIBERACAO
60                   cmdGravar_Atendimento.Enabled = True
                  Else
                     cmdGravar_Atendimento.Enabled = False
                  End If
61            Else
62                cmdGravar_Atendimento.Enabled = False
63            End If
64        Else
65            Call vVB_Generica_001.Informar("O assunto ainda n�o foi escolhido!")
66            cmbAssunto.SetFocus
67            txtProcedimento.Text = ""
68        End If
Trata_Erro:
69        If Err.Number <> 0 Then
70            MsgBox "Sub cmbTopico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
71        End If
End Sub

Private Sub cmdAdd_Click()
    Limpar_Tab_Atendimento
End Sub

Private Sub cmdAdicionar_Click()
    vNum_Atendimento = 0
    Limpar_Tab_Atendimento
End Sub

Private Sub cmdAgenda_Click()
    
    frmCalendario.vQuemChamou = "Botao"
    
    frmCalendario.Show
    StayOnTop frmCalendario

End Sub

Private Sub cmdAtualizar_Click()

    On Erro GoTo Trata_Erro

1         If txtCodigo_Cliente.Text <> "" Then
             '-- BUSCA OS ATENDIMENTOS REGISTRADOS PARA O CLIENTE, QUE N�O EST�O FINALIZADOS
2             vBanco.Parameters.Remove "PM_CODCLIENTE"
3             vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
4             vBanco.Parameters.Remove "PM_SITUACAO"
5             vBanco.Parameters.Add "PM_SITUACAO", "A", 1
              
6             Criar_Cursor vBanco.Parameters, "vCursor"
              
7             vSql = "Producao.PCK_VDA640.PR_BUSCA_LIGACOES(:vCursor,:PM_CODCLIENTE,:PM_SITUACAO)"
              
8             If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
9                 Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_LIGACOES, entre em contato com o SUPORTE!")
10                Exit Sub
11            Else
12                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
13            End If
              
14            vData = Format(Date, "DD/MM/YY")
              
15            If Not vObjOracle.EOF Then
16                vCount_Abertos = vObjOracle.RecordCount
17                lblQtd_Aberto.Caption = vCount_Abertos
                  
18                mfgAberto.Clear
19                mfgAberto.Rows = 2
20                Call Inicia_Grid_Aberto
                  
21                mfgAberto.Rows = vObjOracle.RecordCount + 1
                  
22                For i = 1 To vObjOracle.RecordCount
23                    mfgAberto.TextMatrix(i, 0) = vObjOracle.Fields(0)
24                    mfgAberto.TextMatrix(i, 4) = vObjOracle.Fields(1)
25                    mfgAberto.row = i
26                    mfgAberto.col = 1
27                    mfgAberto.CellFontBold = True
28                    mfgAberto.TextMatrix(i, 2) = vObjOracle.Fields(2)
29                    mfgAberto.TextMatrix(i, 3) = IIf(IsNull(vObjOracle.Fields(3)), "", vObjOracle.Fields(3))
30                    mfgAberto.TextMatrix(i, 4) = IIf(IsNull(vObjOracle.Fields(4)), "", vObjOracle.Fields(4))
31                    mfgAberto.TextMatrix(i, 5) = vObjOracle.Fields(5)
32                    mfgAberto.TextMatrix(i, 6) = IIf(Left(Trim(vObjOracle.Fields(6)), 3) = 0, "", vObjOracle.Fields(6))
33                    mfgAberto.TextMatrix(i, 7) = IIf(Left(Trim(vObjOracle.Fields(7)), 3) = 0, "", vObjOracle.Fields(7))
                      
34                    If vObjOracle.Fields(8) = "P" Then
35                        mfgAberto.TextMatrix(i, 8) = "Pendente"
36                    ElseIf vObjOracle.Fields(8) = "R" Then
37                        mfgAberto.TextMatrix(i, 8) = "Retornado"
38                    ElseIf vObjOracle.Fields(8) = "S" Then
39                        mfgAberto.TextMatrix(i, 8) = "Solucionado"
40                    ElseIf vObjOracle.Fields(8) = "L" Then
41                        mfgAberto.TextMatrix(i, 8) = "Liberado"
42                    End If
                      
43                    Pintar_Linha mfgAberto, 9, vbBlack, i
                      
44                    If Not IsNull(vObjOracle!DT_PREV_RETORNO) Then
45                        If IsNull(vObjOracle!dt_retorno) And _
                             CDate(vObjOracle!DT_PREV_RETORNO) < vData Then
46                            Pintar_Linha mfgAberto, 9, vbRed, i
47                        End If
48                    End If
49                    If Not IsNull(vObjOracle!DT_PREV_SOLUCAO) Then
50                        If IsNull(vObjOracle!dt_solucao) And _
                             CDate(vObjOracle!DT_PREV_SOLUCAO) < vData Then
51                            Pintar_Linha mfgAberto, 9, vbRed, i
52                        End If
53                    End If
54                    If Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
55                        If IsNull(vObjOracle!dt_liberacao) And _
                             CDate(vObjOracle!DT_PREV_LIBERACAO) < vData Then
56                            Pintar_Linha mfgAberto, 9, vbRed, i
57                        End If
58                    End If
59                    If IsNull(vObjOracle!dt_complemento) Then
60                        Pintar_Linha mfgAberto, 9, vbRed, i
61                    End If
62                    vObjOracle.MoveNext
63                Next
64            End If
             
             '-- BUSCA OS ATENDIMENTOS REGISTRADOS PARA O CLIENTE, QUE N�O EST�O FINALIZADOS
65            vBanco.Parameters.Remove "PM_CODCLIENTE"
66            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
67            vBanco.Parameters.Remove "PM_SITUACAO"
68            vBanco.Parameters.Add "PM_SITUACAO", "F", 1
              
69            Criar_Cursor vBanco.Parameters, "vCursor"
              
70            vSql = "Producao.PCK_VDA640.PR_BUSCA_LIGACOES(:vCursor,:PM_CODCLIENTE,:PM_SITUACAO)"
              
71            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
72                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_LIGACOES, entre em contato com o SUPORTE!")
73                Exit Sub
74            Else
75                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
76            End If
              
77            If Not vObjOracle.EOF Then
78                vCount_Finalizados = vObjOracle.RecordCount
79                lblQtd_Finalizado.Caption = vCount_Finalizados
80                Preenche_Grid_Finalizado vObjOracle, vCount_Finalizados
81            End If
82            fl_Novo = "S"
83            vNum_Atendimento = 0
84        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub cmdAtualizar_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdColeta_Click()
    frmCritColeta.Show 1
End Sub

Private Sub cmdConsColetas_Click()
    frmConsColeta.Show 1
End Sub

Private Sub cmdDuplicatas_Click()
    
    Shell "H:\Oracle\sistemas\vb\32bits\rec301.exe VDA640;" & txtCodigo_Cliente, vbNormalFocus
    
End Sub


Private Sub cmdEmail_Click()
    
    If Trim(txtCodigo_Cliente) = "" Then
        MsgBox "Informe o C�digo do Cliente.", vbInformation, "Aten��o"
        txtCodigo_Cliente.SetFocus
        Exit Sub
    End If
    
    lstEmail.BackColor = vbWhite
    timer_color.Interval = 0
    timer_color.Enabled = False
    txtEmailDesatualizado.Visible = False
    PicEmailDesatualizado.Visible = False
    
    frmCadEmail.Show , frmVDA640
    
End Sub

Private Sub cmdEnviar_Email_Click()

'Elisson 2275
    Dim ListaEmail As String
    Dim codLoja As Integer
    
    If IsNumeric(txtDeposito.Text) Then
        frmSeleciona.CodigoLoja = CInt(txtDeposito.Text)
    Else
        frmSeleciona.CodigoLoja = 0
    End If
        
    frmSeleciona.Show 1
    
    ListaEmail = frmSeleciona.ListaEmail
        
        
1         On Error GoTo Erro

2         MAPISessao.SignOn

3         If Err <> 0 Then
4             MsgBox "Falha de conex�o: " + Error$
5         Else
6             MAPIMensagem.SessionID = MAPISessao.SessionID
7         End If

8         MAPIMensagem.Compose
          'Elisson SDS 2275
'9         MAPIMensagem.MsgSubject = "Servico de Atendimento ao Cliente - " & txtCodigo_Cliente
'10        MAPIMensagem.MsgNoteText = "C�digo do Cliente: " & txtCodigo_Cliente & vbCrLf & "N�mero Atendimento: " & lblNumAtendimento & vbCrLf & "Motivo da Ocorr�ncia: " & cmbAssunto & " / " & cmbTopico & " / " & cmbDetalhe & vbCrLf & "PRAZO PARA LIBERA��O: " & txtLiberacao & " Dias " & vbCrLf & txtComentarios
9         MAPIMensagem.MsgSubject = TITULO_EMAIL & " - " & txtCodigo_Cliente
10        MAPIMensagem.MsgNoteText = "C�digo do Cliente: " & txtCodigo_Cliente & vbCrLf & _
                                     "N�mero Atendimento: " & lblNumAtendimento & vbCrLf & _
                                     "Motivo da Ocorr�ncia: " & cmbAssunto & " / " & cmbTopico & " / " & cmbDetalhe & vbCrLf & _
                                     "Contato: " & txtContato.Text & vbCrLf & _
                                     "Relato do Cliente:" & vbCrLf & txtComentarios.Text
'Elisson SDS 2275
'11        MAPIMensagem.RecipDisplayName = vEmailAtendente
11        MAPIMensagem.RecipDisplayName = ListaEmail


12        MAPIMensagem.Send True
13        MAPISessao.SignOff

14        Exit Sub

Erro:

15        MAPISessao.SignOff

16        If Err.Number = 32003 Then
17            MsgBox "Login cancelado", vbExclamation
18        ElseIf Err.Number = 32001 Then
19            MsgBox "Envio de email cancelado !", vbExclamation
20        ElseIf Err.Number = 32025 Or Err.Number = 32014 Then
21            MsgBox "Endere�o invalido !", vbExclamation
22        Else
23            MsgBox "N�o foi possivel enviar o email" & vbLf & vbLf & Err.Number & " - " & Err.Description & vbCrLf & "Linha:" & Erl, vbExclamation
24        End If

End Sub

Private Sub cmdEnviarCliente_Click()
          
        On Error GoTo Trata_Erro
          
          Dim vMensagem As String
          Dim vEmail As String
          Dim vNome() As String
          Dim vNomeCompleto As String
          Dim olApp As Outlook.Application
          Dim olNS As NameSpace
          Dim olMessage As MailItem

1         Set olApp = New Outlook.Application
2         Set olNS = olApp.GetNamespace("MAPI")

3         If lstEmail.ListCount = 0 Then
4             MsgBox "Selecione um email.", vbInformation, "Aten��o"
5             Exit Sub
6         End If
          
7         For i = 0 To Me.lstEmail.ListCount - 1
8             If Me.lstEmail.Selected(i) = True Then
9                 If vEmail = "" Then
10                    vEmail = Me.lstEmail.List(i)
11                Else
12                    vEmail = vEmail & ";" & lstEmail.List(i)
13                End If
14            End If
15        Next
16        vNome = Split(vAtendente, " ")
          
17        For i = 0 To UBound(vNome)
18            If vNomeCompleto = "" Then
19                vNomeCompleto = UCase(Left(vNome(i), 1)) & LCase(Mid(vNome(i), 2))
20            Else
21                vNomeCompleto = vNomeCompleto & " " & UCase(Left(vNome(i), 1)) & LCase(Mid(vNome(i), 2))
22            End If
23        Next
          
24        vMensagem = "<HTML><BODY>"
25        vMensagem = vMensagem & "<IMG alt='' hspace=0 src='H:\ORACLE\SISTEMAS\VB\32BITS\VDA640\Logo_Dpk.Gif' height=70 width=70 align=baseline border=0><BR><BR>"
26        vMensagem = vMensagem & "Prezado Cliente, <BR><BR> "
27        vMensagem = vMensagem & "Referente a sua solicitacao, segue abaixo o retorno de sua ocorr�ncia.<BR><BR>"
28        vMensagem = vMensagem & "C�digo do Cliente: <b>" & txtCodigo_Cliente & "</b><BR><BR>"
29        vMensagem = vMensagem & "N�mero Atendimento: " & lblNumAtendimento & "<BR><BR>"
30        vMensagem = vMensagem & "Motivo da Ocorr�ncia: " & cmbAssunto & " / " & cmbTopico & " / " & cmbDetalhe & "<BR><BR>"
          'Elisson SDS2275
'31        vMensagem = vMensagem & "Prazo para Libera��o: <b>" & txtLiberacao & " Dias " & "</b><BR><BR>"
32        vMensagem = vMensagem & "Coment�rios: " & txtComentarios & "<BR><BR>"
33        vMensagem = vMensagem & vNomeCompleto & "<BR><BR>"
34        vMensagem = vMensagem & "Qualquer d�vida, favor entrar em contato conosco.<BR><BR>"
35        vMensagem = vMensagem & "<IMG alt='' hspace=0 src='H:\ORACLE\SISTEMAS\VB\32BITS\VDA640\Logo_sac.jpg' height=80 width=120 align=baseline border=0>"
36        vMensagem = vMensagem & "</BODY></HTML>"
          
37        Set olMessage = olApp.CreateItem(olMailItem)
38        olMessage.To = vEmail
          'Elisson SDS2275
'39        olMessage.Subject = "Servico de Atendimento ao Cliente"
39        olMessage.Subject = TITULO_EMAIL
40        olMessage.HTMLBody = vMensagem

41        olMessage.Display 1

42        Set olMessage = Nothing
43        Set olNS = Nothing
44        Set olApp = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdEnviarCliente_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub cmdEstatisticas_Click()
    
    frmConsultar.Show
    StayOnTop frmConsultar
End Sub

Private Sub cmdGravar_Atendimento_Click()
    
1         On Error GoTo Vda640_Erro
    
    Dim vData As String
    Dim vDt_Complemento As Date

    '29/09/2006
2   If cmbAssuntoCompleto.Visible = False Then
3     If cmbAssunto.Text = "" Then
4        MsgBox "Informe o Assunto.", vbInformation, "Aten��o"
5        cmbAssunto.SetFocus
6        Exit Sub
7     End If
8     If cmbTopico.Enabled = True And Me.cmbTopico = "" And txtProcedimento = "" Then
9        MsgBox "Informe o T�pico.", vbInformation, "Aten��o"
10       cmbTopico.SetFocus
11       Exit Sub
12    End If
13    If Me.cmbDetalhe.Enabled = True And cmbDetalhe = "" And txtProcedimento = "" Then
14       MsgBox "Informe o Detalhe.", vbInformation, "Aten��o"
15       cmbDetalhe.SetFocus
16       Exit Sub
17    End If
18  End If

19    If Me.cboAtendente.ListIndex = -1 Then
20       MsgBox "Informe o Atendente>", vbInformation, "Aten��o"
21       cboAtendente.SetFocus
22       Exit Sub
23    End If

    
24    If (txtProcedimento.Text <> "" Or chkComplemento.Value = 1 Or chkComplemento = True) And txtComentarios.Text <> "" Then
25       vData = Format(Date, "DD/MM/YY")
        
26       If vNum_Atendimento <> 0 Then
27           vBanco.Parameters.Remove "PM_NUMERO"
28           vBanco.Parameters.Add "PM_NUMERO", vNum_Atendimento, 1
29           fl_Novo = "N"
30      Else
31           vBanco.Parameters.Remove "PM_NUMERO"
32           vBanco.Parameters.Add "PM_NUMERO", 0, 1
33           fl_Novo = "S"
34      End If
        
35      Pegar_VL_Parametro
        
36      vBanco.Parameters.Remove "PM_CODCLIENTE"
37      vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
38      vBanco.Parameters.Remove "PM_CODLOJA"
39      vBanco.Parameters.Add "PM_CODLOJA", txtDeposito.Text, 1
40      vBanco.Parameters.Remove "PM_NUMNOTA"
41      vBanco.Parameters.Add "PM_NUMNOTA", txtNota_Fiscal.Text, 1
42      vBanco.Parameters.Remove "PM_DTATENDIMENTO"
43      vBanco.Parameters.Add "PM_DTATENDIMENTO", vData, 1
44      vBanco.Parameters.Remove "PM_CODUSUARIO"

'23      vBanco.Parameters.Add "PM_CODUSUARIO", vCod_Usuario, 1
45      vBanco.Parameters.Add "PM_CODUSUARIO", fGet_CodUsuario(cboAtendente.ItemData(cboAtendente.ListIndex)), 1
        
46      If chkComplemento.Value = 1 Then    '-- Falta complemento
47          vBanco.Parameters.Remove "PM_DTPREV_RETORNO"
48          vBanco.Parameters.Add "PM_DTPREV_RETORNO", Null, 1
49          vBanco.Parameters.Remove "PM_DTPREV_SOLUCAO"
50          vBanco.Parameters.Add "PM_DTPREV_SOLUCAO", Null, 1
51          vBanco.Parameters.Remove "PM_DTPREV_LIBERACAO"
52          vBanco.Parameters.Add "PM_DTPREV_LIBERACAO", Null, 1
53          vBanco.Parameters.Remove "PM_DTCOMPLEMENTO"
54          vBanco.Parameters.Add "PM_DTCOMPLEMENTO", Null, 1
55          vBanco.Parameters.Remove "PM_DISPENSA_RETORNO"
56          vBanco.Parameters.Add "PM_DISPENSA_RETORNO", "N", 1
57      Else
58          If fl_Novo = "S" Then   '-- Atendimento novo que n�o falta complemento
59              vBanco.Parameters.Remove "PM_DTCOMPLEMENTO"
60              vBanco.Parameters.Add "PM_DTCOMPLEMENTO", vData, 1
61              vDt_Complemento = vData
62          Else                    '-- Atendimento existente e n�o falta complemento
               '-- Verifica se j� existe data para o falta_complemento ou se ser� gravada hoje
63              vBanco.Parameters.Remove "PM_NUMERO"
64              vBanco.Parameters.Add "PM_NUMERO", vNum_Atendimento, 1
                                
65              Criar_Cursor vBanco.Parameters, "vCursor"
                
66              vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
                
67              If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
68                  Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
69                  Exit Sub
70              Else
71                  Set vObjOracle = vBanco.Parameters("VCURSOR").Value
72              End If
                
73              If IsNull(vObjOracle!dt_complemento) Then
74                  vBanco.Parameters.Remove "PM_DTCOMPLEMENTO"
75                  vBanco.Parameters.Add "PM_DTCOMPLEMENTO", vData, 1
76                  vDt_Complemento = vData
77              Else
78                  vBanco.Parameters.Remove "PM_DTCOMPLEMENTO"
79                  vBanco.Parameters.Add "PM_DTCOMPLEMENTO", vObjOracle!dt_complemento, 1
80                  vDt_Complemento = vObjOracle!dt_complemento
81              End If
82          End If
83          If txtRetorno.Text = 0 Then
84              vBanco.Parameters.Remove "PM_DTPREV_RETORNO"
85              vBanco.Parameters.Add "PM_DTPREV_RETORNO", Null, 1
86              vBanco.Parameters.Remove "PM_DISPENSA_RETORNO"
87              vBanco.Parameters.Add "PM_DISPENSA_RETORNO", "N", 1
88          Else
89              If fl_Novo = "S" Then
90                 vBanco.Parameters.Remove "PM_DTPREV_RETORNO"
91                 vBanco.Parameters.Add "PM_DTPREV_RETORNO", fDataDiasUteis(vDt_Complemento, Val(txtRetorno)), 1 'DateAdd("d", CDbl(txtRetorno.Text), vDt_Complemento), 1
                                    
92              Else
93                  vBanco.Parameters.Remove "PM_DTPREV_RETORNO"
94                  vBanco.Parameters.Add "PM_DTPREV_RETORNO", Format(fDataDiasUteis(CDate(vObjOracle!DT_ATENDIMENTO), Val(txtRetorno.Text)), "DD/MM/YY"), 1 'Format(DateAdd("d", CDbl(txtRetorno.Text), CDate(vObjOracle!DT_ATENDIMENTO)), "DD/MM/YY"), 1
95              End If
                
96              vBanco.Parameters.Remove "PM_DISPENSA_RETORNO"
97              vBanco.Parameters.Add "PM_DISPENSA_RETORNO", "N", 1
98          End If
            
99          If txtSolucao.Text = 0 Then
100             vBanco.Parameters.Remove "PM_DTPREV_SOLUCAO"
101             vBanco.Parameters.Add "PM_DTPREV_SOLUCAO", Null, 1
102         Else
103             If fl_Novo = "S" Then
104                 vBanco.Parameters.Remove "PM_DTPREV_SOLUCAO"
105                 vBanco.Parameters.Add "PM_DTPREV_SOLUCAO", fDataDiasUteis(vDt_Complemento, Val(txtSolucao.Text)), 1 'Format(DateAdd("d", CDbl(txtSolucao.Text), vDt_Complemento), "DD/MM/YY"), 1
106             Else
107                 vBanco.Parameters.Remove "PM_DTPREV_SOLUCAO"
108                 vBanco.Parameters.Add "PM_DTPREV_SOLUCAO", fDataDiasUteis(CDate(vObjOracle!DT_ATENDIMENTO), Val(txtSolucao.Text)), 1 'Format(DateAdd("d", CDbl(txtSolucao.Text), CDate(vObjOracle!DT_ATENDIMENTO)), "DD/MM/YY"), 1
109             End If
110         End If
            
111         If fl_Novo = "S" Then
112            vBanco.Parameters.Remove "PM_DTPREV_LIBERACAO"
113            vBanco.Parameters.Add "PM_DTPREV_LIBERACAO", fDataDiasUteis(vDt_Complemento, Val(txtLiberacao.Text)), 1 'DateAdd("d", CDbl(txtLiberacao.Text), vDt_Complemento), 1
114         Else
115             vBanco.Parameters.Remove "PM_DTPREV_LIBERACAO"
116             vBanco.Parameters.Add "PM_DTPREV_LIBERACAO", fDataDiasUteis(CDate(vObjOracle!DT_ATENDIMENTO), Val(txtLiberacao.Text)), 1 'Format(DateAdd("d", CDbl(txtLiberacao.Text), CDate(vObjOracle!DT_ATENDIMENTO)), "DD/MM/YY"), 1
117         End If
118     End If
        
119     If optPendente.Value = True Then
120         vBanco.Parameters.Remove "PM_SITUACAO"
121         vBanco.Parameters.Add "PM_SITUACAO", "P", 1
122         vBanco.Parameters.Remove "PM_DTRETORNO"
123         vBanco.Parameters.Add "PM_DTRETORNO", Null, 1
124         vBanco.Parameters.Remove "PM_DTSOLUCAO"
125         vBanco.Parameters.Add "PM_DTSOLUCAO", Null, 1
126         vBanco.Parameters.Remove "PM_DTLIBERACAO"
127         vBanco.Parameters.Add "PM_DTLIBERACAO", Null, 1
128     ElseIf optRetornado.Value = True Then
129         vBanco.Parameters.Remove "PM_SITUACAO"
130         vBanco.Parameters.Add "PM_SITUACAO", "R", 1
131         If fl_Novo = "S" Then
132             vBanco.Parameters.Remove "PM_DTRETORNO"
133             vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
134             vBanco.Parameters.Remove "PM_DTSOLUCAO"
135             vBanco.Parameters.Add "PM_DTSOLUCAO", Null, 1
136             vBanco.Parameters.Remove "PM_DTLIBERACAO"
137             vBanco.Parameters.Add "PM_DTLIBERACAO", Null, 1
138         Else
139             If IsNull(vObjOracle!dt_retorno) Then
140                vBanco.Parameters.Remove "PM_DTRETORNO"
141                vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
142             Else
143                 vBanco.Parameters.Remove "PM_DTRETORNO"
144                 vBanco.Parameters.Add "PM_DTRETORNO", vObjOracle!dt_retorno, 1
145             End If
146             vBanco.Parameters.Remove "PM_DTSOLUCAO"
147             vBanco.Parameters.Add "PM_DTSOLUCAO", Null, 1
148             vBanco.Parameters.Remove "PM_DTLIBERACAO"
149             vBanco.Parameters.Add "PM_DTLIBERACAO", Null, 1
150         End If
151     ElseIf optSolucionado.Value = True Then
152         vBanco.Parameters.Remove "PM_SITUACAO"
153         vBanco.Parameters.Add "PM_SITUACAO", "S", 1
            
154         If fl_Novo = "S" Then
155             vBanco.Parameters.Remove "PM_DTSOLUCAO"
156             vBanco.Parameters.Add "PM_DTSOLUCAO", vData, 1
            
157             vBanco.Parameters.Remove "PM_DTRETORNO"
158             vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
159         Else
160             If IsNull(vObjOracle!dt_solucao) Then
161                 vBanco.Parameters.Remove "PM_DTSOLUCAO"
162                 vBanco.Parameters.Add "PM_DTSOLUCAO", vData, 1
163             Else
164                 vBanco.Parameters.Remove "PM_DTSOLUCAO"
165                   vBanco.Parameters.Add "PM_DTSOLUCAO", vObjOracle!dt_solucao, 1
166             End If
167             If IsNull(vObjOracle!dt_retorno) Then
168                 vBanco.Parameters.Remove "PM_DTRETORNO"
169                 vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
170             Else
171                 vBanco.Parameters.Remove "PM_DTRETORNO"
172                  vBanco.Parameters.Add "PM_DTRETORNO", vObjOracle!dt_retorno, 1
173             End If
174         End If
175         vBanco.Parameters.Remove "PM_DTLIBERACAO"
176         vBanco.Parameters.Add "PM_DTLIBERACAO", Null, 1
177     ElseIf optLiberado.Value = True Then
178         vBanco.Parameters.Remove "PM_SITUACAO"
179         vBanco.Parameters.Add "PM_SITUACAO", "L", 1
            
180         If fl_Novo = "S" Then
181            vBanco.Parameters.Remove "PM_DTLIBERACAO"
182            vBanco.Parameters.Add "PM_DTLIBERACAO", vData, 1
                
183            vBanco.Parameters.Remove "PM_DTSOLUCAO"
184            vBanco.Parameters.Add "PM_DTSOLUCAO", vData, 1
            
185             vBanco.Parameters.Remove "PM_DTRETORNO"
186             vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
            
187         Else
188             If IsNull(vObjOracle!dt_liberacao) Then
189                 vBanco.Parameters.Remove "PM_DTLIBERACAO"
190                 vBanco.Parameters.Add "PM_DTLIBERACAO", vData, 1
191             Else
192                 vBanco.Parameters.Remove "PM_DTLIBERACAO"
193                   vBanco.Parameters.Add "PM_DTLIBERACAO", vObjOracle!dt_liberacao, 1
194             End If
                    
195             If IsNull(vObjOracle!dt_solucao) Then
196                 vBanco.Parameters.Remove "PM_DTSOLUCAO"
197                 vBanco.Parameters.Add "PM_DTSOLUCAO", vData, 1
198             Else
199                 vBanco.Parameters.Remove "PM_DTSOLUCAO"
200                   vBanco.Parameters.Add "PM_DTSOLUCAO", vObjOracle!dt_solucao, 1
201             End If
            
202             If IsNull(vObjOracle!dt_retorno) Then
203                 vBanco.Parameters.Remove "PM_DTRETORNO"
204                 vBanco.Parameters.Add "PM_DTRETORNO", vData, 1
205             Else
206                 vBanco.Parameters.Remove "PM_DTRETORNO"
207                   vBanco.Parameters.Add "PM_DTRETORNO", vObjOracle!dt_retorno, 1
208             End If
209            End If
210     End If
        
211     vBanco.Parameters.Remove "PM_NOMECONTATO"
212     vBanco.Parameters.Add "PM_NOMECONTATO", txtContato.Text, 1
        
213     If chkComplemento.Value = 0 Then
214         vBanco.Parameters.Remove "PM_FALTA_COMPLEMENTO"
215         vBanco.Parameters.Add "PM_FALTA_COMPLEMENTO", "N", 1
216     Else
217         vBanco.Parameters.Remove "PM_FALTA_COMPLEMENTO"
218         vBanco.Parameters.Add "PM_FALTA_COMPLEMENTO", "S", 1
219     End If
        
220     If chkProcede.Value = 0 Then
221         vBanco.Parameters.Remove "PM_PROCEDE"
222         vBanco.Parameters.Add "PM_PROCEDE", "N", 1
223     Else
224         vBanco.Parameters.Remove "PM_PROCEDE"
225         vBanco.Parameters.Add "PM_PROCEDE", "S", 1
226     End If
        
227     If chkDevolucao.Value = 0 Then
228         vBanco.Parameters.Remove "PM_DEVOLUCAO"
229         vBanco.Parameters.Add "PM_DEVOLUCAO", "N", 1
230     Else
231         vBanco.Parameters.Remove "PM_DEVOLUCAO"
232         vBanco.Parameters.Add "PM_DEVOLUCAO", "S", 1
233     End If
        
234     If chkProrrogacao.Value = 0 Then
235         vBanco.Parameters.Remove "PM_PRORROGACAO"
236         vBanco.Parameters.Add "PM_PRORROGACAO", "N", 1
237     Else
238         vBanco.Parameters.Remove "PM_PRORROGACAO"
239         vBanco.Parameters.Add "PM_PRORROGACAO", "S", 1
240     End If
        
241     vBanco.Parameters.Remove "PM_DESC_COMISSAO"
242     vBanco.Parameters.Add "PM_DESC_COMISSAO", txtComissao.Text, 1
243     If txtAbatimento.Text = "" Then
244         vBanco.Parameters.Remove "PM_ABATIMENTO"
245         vBanco.Parameters.Add "PM_ABATIMENTO", Null, 1
246     Else
247         vBanco.Parameters.Remove "PM_ABATIMENTO"
248         vBanco.Parameters.Add "PM_ABATIMENTO", CDbl(txtAbatimento.Text), 1
249     End If
        
250     If optCliente.Value = True Then
251         vBanco.Parameters.Remove "PM_CONTATO"
252         vBanco.Parameters.Add "PM_CONTATO", "C", 1
253     ElseIf optRepresentante.Value = True Then
254         vBanco.Parameters.Remove "PM_CONTATO"
255         vBanco.Parameters.Add "PM_CONTATO", "R", 1
256     ElseIf optTelevendas.Value = True Then
257         vBanco.Parameters.Remove "PM_CONTATO"
258         vBanco.Parameters.Add "PM_CONTATO", "T", 1
259     Else
260         vBanco.Parameters.Remove "PM_CONTATO"
261         vBanco.Parameters.Add "PM_CONTATO", "O", 1
262     End If
        
263     vBanco.Parameters.Remove "PM_COMENTARIOS"
264     vBanco.Parameters.Add "PM_COMENTARIOS", txtComentarios.Text, 1
265     vBanco.Parameters.Remove "PM_CODASSUNTO"

266     If cmbAssuntoCompleto.Visible = False Then
267         If cmbAssunto = "" Then
268             vBanco.Parameters.Add "PM_CODASSUNTO", 0, 1
269         Else
270             vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssunto.ItemData(cmbAssunto.ListIndex), 1
271         End If
            
272         vBanco.Parameters.Remove "PM_CODTOPICO"
273         If cmbTopico = "" Then
274             vBanco.Parameters.Add "PM_CODTOPICO", 0, 1
275         Else
276             vBanco.Parameters.Add "PM_CODTOPICO", cmbTopico.ItemData(cmbTopico.ListIndex), 1
277         End If
278         vBanco.Parameters.Remove "PM_CODDETALHE"
279         If cmbDetalhe.ListIndex = -1 Then
280            vBanco.Parameters.Add "PM_CODDETALHE", 0, 1
281         Else
282            vBanco.Parameters.Add "PM_CODDETALHE", cmbDetalhe.ItemData(cmbDetalhe.ListIndex), 1
283         End If

284     Else
285         If cmbAssuntoCompleto = "" Then
286             vBanco.Parameters.Add "PM_CODASSUNTO", 0, 1
287         Else
288             vBanco.Parameters.Add "PM_CODASSUNTO", cmbAssuntoCompleto.ItemData(cmbAssuntoCompleto.ListIndex), 1
289         End If
            
290         vBanco.Parameters.Remove "PM_CODTOPICO"
291         If cmbTopicoCompleto = "" Then
292             vBanco.Parameters.Add "PM_CODTOPICO", 0, 1
293         Else
294             vBanco.Parameters.Add "PM_CODTOPICO", cmbTopicoCompleto.ItemData(cmbTopicoCompleto.ListIndex), 1
295         End If
296         vBanco.Parameters.Remove "PM_CODDETALHE"
297         If cmbDetalheCompleto.ListIndex = -1 Then
298            vBanco.Parameters.Add "PM_CODDETALHE", 0, 1
299         Else
300            vBanco.Parameters.Add "PM_CODDETALHE", cmbDetalheCompleto.ItemData(cmbDetalheCompleto.ListIndex), 1
301         End If
302     End If

303     vBanco.Parameters.Remove "PM_NOVO"
304     vBanco.Parameters.Add "PM_NOVO", fl_Novo, 1
        
        'Eduardo - Atendente
305     vBanco.Parameters.Remove "PM_NUM_ATEND_NOVO"
306     vBanco.Parameters.Add "PM_NUM_ATEND_NOVO", 0, 2
        
307     Criar_Cursor vBanco.Parameters, "vCursor"
        
308     vSql = "Producao.PCK_VDA640.PR_ATUALIZA_ATENDIMENTO(:PM_NUMERO, :PM_CODCLIENTE, :PM_CODLOJA, " & _
               ":PM_NUMNOTA, TO_DATE(:PM_DTATENDIMENTO, 'DD/MM/RR'), :PM_CODUSUARIO, TO_DATE(:PM_DTPREV_RETORNO, 'DD/MM/RR'), " & _
               "TO_DATE(:PM_DTPREV_SOLUCAO, 'DD/MM/RR'), TO_DATE(:PM_DTPREV_LIBERACAO, 'DD/MM/RR'), " & _
               "TO_DATE(:PM_DTCOMPLEMENTO, 'DD/MM/RR'), TO_DATE(:PM_DTRETORNO, 'DD/MM/RR'), " & _
               "TO_DATE(:PM_DTSOLUCAO, 'DD/MM/RR'), TO_DATE(:PM_DTLIBERACAO, 'DD/MM/RR'), " & _
               ":PM_NOMECONTATO, :PM_FALTA_COMPLEMENTO, :PM_PROCEDE, :PM_DEVOLUCAO, :PM_PRORROGACAO, " & _
               ":PM_DISPENSA_RETORNO, :PM_DESC_COMISSAO, :PM_ABATIMENTO, :PM_SITUACAO, :PM_CONTATO, :PM_COMENTARIOS, " & _
               ":PM_CODASSUNTO, :PM_CODTOPICO, :PM_CODDETALHE, :PM_NOVO, :PM_NUM_ATEND_NOVO)"
               
309     vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
               
310     If vErro <> "" Then
311         Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATUALIZA_ATENDIMENTO. Entre em contato com o SUPORTE.")
312         Exit Sub
313     Else
314         vBanco.Parameters.Remove "PM_COD_ERRO"
315         vBanco.Parameters.Add "PM_COD_ERRO", 0, 2
            
316         vBanco.Parameters.Remove "PM_TXT_ERRO"
317         vBanco.Parameters.Add "PM_TXT_ERRO", "", 2
            
318         If vBanco.Parameters("pm_numero").Value = 0 Then
319             vBanco.Parameters.Remove "PM_NUM_ATEND"
320             vBanco.Parameters.Add "PM_NUM_ATEND", vBanco.Parameters("PM_NUM_ATEND_NOVO").Value, 1
                
321             vBanco.Parameters.Remove "Pm_Num_Atendente"
322             vBanco.Parameters.Add "PM_NUM_ATENDENTE", vCod_Usuario, 1
323             vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA640.PR_ATENDIMENTO_USUARIO(:PM_NUM_ATEND, :PM_NUM_ATENDENTE, :PM_COD_ERRO, :PM_TXT_ERRO)")

324             If vErro <> "" And vBanco.Parameters("PM_COD_ERRO").Value <> 0 Then
325                 Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATENDIMENTO_USUARIO. Entre em contato com o SUPORTE.")
326                 Exit Sub
327             End If
                'Coloquei isso para dar um tempo de 1,5s entre cada registro

328             Call Sleep(1000)

329         Else
330             vBanco.Parameters.Remove "PM_NUM_ATEND"
331             vBanco.Parameters.Add "PM_NUM_ATEND", vNum_Atendimento, 1
332         End If
            
333         vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.PCK_VDA640.PR_ATENDIMENTO_USUARIO(:PM_NUM_ATEND, :PM_CODUSUARIO, :PM_COD_ERRO, :PM_TXT_ERRO)")

334         If vErro <> "" And vBanco.Parameters("PM_COD_ERRO").Value <> 0 Then
335             Call vVB_Generica_001.Informar("Erro no processo de grava��o da PR_ATENDIMENTO_USUARIO. Entre em contato com o SUPORTE.")
336             Exit Sub
337         End If

338         Call vVB_Generica_001.Informar("Atendimento conclu�do com sucesso!")
            
339         mfgAberto.Clear
340         mfgAberto.Rows = 2
            
341         Call Inicia_Grid_Aberto
            
342         Call Formatar_Grid_Finalizado
            
343         Limpar_Tab_Atendimento
344     End If
345       Else
346     Call vVB_Generica_001.Informar("Atendimento incompleto!")
347       End If
    
   '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � PRAZO
348       vBanco.Parameters.Remove "PM_CODIGO"
349       vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
350       vBanco.Parameters.Remove "PM_TIPO"
351       vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
352       vBanco.Parameters.Remove "PM_ROWNUM"
353       vBanco.Parameters.Add "PM_ROWNUM", 1, 1
    
354       Criar_Cursor vBanco.Parameters, "vCursor"
    
355       vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_PRAZO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
    
356       If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
357         Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_PRAZO, entre em contato com o SUPORTE!")
358         Exit Sub
359       Else
360         Set vObjOracle = vBanco.Parameters("VCURSOR").Value
361       End If
    
   '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � AGENDA
362       vBanco.Parameters.Remove "PM_CODIGO"
363       vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
364       vBanco.Parameters.Remove "PM_TIPO"
365       vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
366       vBanco.Parameters.Remove "PM_ROWNUM"
367       vBanco.Parameters.Add "PM_ROWNUM", 1, 1
    
368       Criar_Cursor vBanco.Parameters, "vCursor"
    
369       vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_AGENDA(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
    
370       If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
371             Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_AGENDA, entre em contato com o SUPORTE!")
372             Exit Sub
373       Else
374             Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
375       End If
    
   '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � COMPLEMENTO
376       vBanco.Parameters.Remove "PM_CODIGO"
377       vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
378       vBanco.Parameters.Remove "PM_TIPO"
379       vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
380       vBanco.Parameters.Remove "PM_ROWNUM"
381       vBanco.Parameters.Add "PM_ROWNUM", 1, 1
    
382       Criar_Cursor vBanco.Parameters, "vCursor"
    
383       vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_COMPLEMENTO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
    
384       If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
385             Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_AGENDA, entre em contato com o SUPORTE!")
386             Exit Sub
387       Else
388             Set vObjOracle3 = vBanco.Parameters("VCURSOR").Value
389       End If
    
390       If vObjOracle.RecordCount <> 0 Or vObjOracle2.RecordCount <> 0 Or _
                vObjOracle3.RecordCount <> 0 Then
391             Set cmdPendencias.PictureNormal = imlBotoes.ListImages(2).Picture
392       Else
393             Set cmdPendencias.PictureNormal = imlBotoes.ListImages(1).Picture
394       End If
395       stb.Tab = 0
396       txtCodigo_Cliente_LostFocus
          
Vda640_Erro:
397   If Err.Number <> 0 Then
398     MsgBox "Sub: CmdGravar_Atendimento_Click " & vbCrLf & "C�digo Erro: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
399   End If
End Sub

Private Sub Exibir_Historico()

    On Error GoTo Trata_Erro
    
          Dim Litem As ListItem
          Dim vRst_Atend As Object
          
1         vBanco.Parameters.Remove "NUM_ATENDIMENTO"
2         vBanco.Parameters.Add "NUM_ATENDIMENTO", Right(lblNumAtendimento, 6), 1
          
3         Criar_Cursor vBanco.Parameters, "vCursor"
          
4         vSql = "Producao.PCK_VDA640.PR_SELECT_ATENDIMENTO_USUARIO(:vCursor, :NUM_ATENDIMENTO)"
          
5         If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
6             Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_ATENDIMENTO_USUARIO, entre em contato com o SUPORTE!")
7             Exit Sub
8         Else
9             Set vRst_Atend = vBanco.Parameters("VCURSOR").Value
10        End If
             
11        LsvHistorico.ListItems.Clear
12        For i = 1 To vRst_Atend.RecordCount
13            Set Litem = LsvHistorico.ListItems.Add
14            Litem = i
15            Litem.SubItems(1) = vRst_Atend!Nome_Usuario
16            Litem.SubItems(2) = vRst_Atend!dt_troca
17            vRst_Atend.MoveNext
18        Next
             
19        LsvHistorico.SortKey = 0
20        LsvHistorico.SortOrder = lvwDescending
21        LsvHistorico.Sorted = True
             
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Exibir_Historico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
             
End Sub

Private Sub cmdImprimirDevolucao_Click()
    If txtNota_Fiscal = "" Then
        MsgBox "Informe o N�mero da Nota de Origem na tela de Atendimento.", vbInformation, "Aten��o"
        txtNota_Fiscal.SetFocus
        Exit Sub
    End If
    If lblNumAtendimento.Caption = "" Then
        MsgBox "Grave o Atendimento primeiro.", vbInformation, "Aten��o"
        Exit Sub
    End If
    frmAutoDevolucao.Show 1
End Sub

Private Sub cmdLimpar_Click()
1     On Error GoTo Erro
    
          cmdLimpar.Tag = "Limpando"

2         txtCodigo_Cliente.Text = ""
3         txtRazao_Social.Text = ""
4         txtContato.Text = ""
5         txtCGC.Text = ""
6         txtCidade.Text = ""
7         txtUF.Text = ""
8         txtTipo_Fiel.Text = ""
9         txtCod_Representante.Text = ""
10        txtNome_Representante.Text = ""
11        txtDDD1.Text = ""
12        txtTelefone1.Text = ""
13        txtDDD2.Text = ""
14        txtTelefone2.Text = ""
15        txtFilial.Text = ""
16        lblQtd_Aberto.Caption = ""
17        lblQtd_Finalizado.Caption = ""
18        cmdAgenda.Enabled = False
19        cmbAssunto.Enabled = True
         
         'FraAtendimento.Enabled = True
20        Frame4.Enabled = True
21        Frame1.Enabled = True
22        txtComentarios.Locked = False
23        txtDeposito.Locked = False
24        txtNota_Fiscal.Locked = False
25        txtComissao.Locked = False
26        txtAbatimento.Locked = False
27        chkComplemento.Enabled = True
28        chkProcede.Enabled = True
29        chkProrrogacao.Enabled = True
30        chkDevolucao.Enabled = True
          
31        stb.TabEnabled(1) = False
          
32        mfgAberto.Clear
33        mfgAberto.Rows = 2
34        Call Inicia_Grid_Aberto

37        Call Formatar_Grid_Finalizado
           
38        optPendente.Value = True
39        optCliente.Value = True
40        txtComentarios.Text = ""
41        txtDeposito.Text = ""
42        txtNota_Fiscal.Text = ""
43        txtComissao.Text = ""
44        txtAbatimento.Text = ""
45        chkComplemento.Value = 0
46        chkProcede.Value = 0
47        chkDevolucao.Value = 0
48        chkProrrogacao.Value = 0
49        cmbAssunto.ListIndex = -1
50        cmbTopico.ListIndex = -1
51        cmbDetalhe.ListIndex = -1
52        txtProcedimento.Text = ""
53        txtRetorno.Text = ""
54        txtSolucao.Text = ""
55        txtLiberacao.Text = ""

56        txtEmail_Para.Text = ""
57        txtEmail_Copia.Text = ""
58        txtNumAtendimento = ""
59        optPendente.Enabled = True
60        optRetornado.Enabled = True
61        optSolucionado.Enabled = True
62        optLiberado.Enabled = True
63        chkComplemento.Enabled = True
          
64        stb.Tab = 0
          
65        txtNota_Fiscal.ForeColor = vbBlack
66        txtNota_Fiscal.FontBold = False
                      
          lstEmail.Clear
          
          timer_color.Interval = 0
          timer_color.Enabled = False
          lstEmail.BackColor = vbWhite
          txtEmailDesatualizado.Visible = False
          PicEmailDesatualizado.Visible = False
          
67        txtCodigo_Cliente.SetFocus

Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub cmdLimpar_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

    cmdLimpar.Tag = ""

End Sub

Private Sub cmdPendencias_Click()

    If Form_Aberto("frmPendencias") Then
        frmPendencias.Visible = True
    Else
        frmPendencias.Show
        StayOnTop frmPendencias
    End If
End Sub

Private Sub cmdCadastros_Click()

    frmCadastro.Show

End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        vVB_Generica_001.ExcluiBind vBanco
        
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        Set vVB_Generica_001.vBanco = Nothing
        Set vVB_Generica_001.vSessao = Nothing
        
        If TimerID <> 0 Then KillTimer 0, TimerID
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub cmdTxt_Click()
    frmRelatorios.Show
End Sub



Private Sub Form_Load()
          
1         On Error GoTo Trata_Erro
          
          Dim ObjAtendPend As Object
          
2         frmLogo.Show 1
          
3         Tempo
          
4         Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
          
5         txtTipComentarios.Visible = False
6         picFecharTool.Visible = False
          
7         txtTipComentariosFim.Visible = False
8         PicFecharFim.Visible = False
             
'9         vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "VDA640", True, Me)
         
          vErro = vVB_Generica_001.ConectaOracle("SDPK", "VDA640", True, Me)
          'vErro = vVB_Generica_001.ConectaOracle("DESENV", "PRODUCAO", True, Me)

10        If vErro <> "" Then
11            Call vVB_Generica_001.ProcessaErro(vErro)
12            End
13        End If

14        vCD = vVB_Generica_001.vCD
15        vTipoCD = vVB_Generica_001.vTipoCD
16        Set vSessao = vVB_Generica_001.vSessao
17        Set vBanco = vVB_Generica_001.vBanco
          
18        dlgLogin.Show 1

19        Me.Visible = True

20        Me.Refresh

21        Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))
          
22        If vObjOracle.EOF Then
          
23            Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
24            End
          
25        End If
          
26        Call DefinirTelaSobre
          
27        stb.TabEnabled(1) = False
28        stb.TabVisible(2) = False
          
         '-- CARREGA OS ASSUNTOS
29        Criar_Cursor vBanco.Parameters, "vCursor"
          
30        vSql = "Producao.PCK_VDA640.PR_BUSCA_ASSUNTO(:vCursor)"
          
31        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
32            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ASSUNTO, entre em contato com o SUPORTE!")
33            Exit Sub
34        Else
35            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
36        End If
          
37        Preenche_Combo cmbAssunto, vObjOracle

38        Preenche_Combo_Completo
          
         '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � PRAZO
39        vBanco.Parameters.Remove "PM_CODIGO"
40        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
41        vBanco.Parameters.Remove "PM_TIPO"
42        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
43        vBanco.Parameters.Remove "PM_ROWNUM"
44        vBanco.Parameters.Add "PM_ROWNUM", 1, 1
          
45        Criar_Cursor vBanco.Parameters, "vCursor"
          
46        vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_PRAZO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
          
47        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
48            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_PRAZO, entre em contato com o SUPORTE!")
49            Exit Sub
50        Else
51            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
52        End If
          
         '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � AGENDA
53        vBanco.Parameters.Remove "PM_CODIGO"
54        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
55        vBanco.Parameters.Remove "PM_TIPO"
56        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
57        vBanco.Parameters.Remove "PM_ROWNUM"
58        vBanco.Parameters.Add "PM_ROWNUM", 1, 1
          
59        Criar_Cursor vBanco.Parameters, "vCursor"
          
60        vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_AGENDA(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
          
61        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
62            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_AGENDA, entre em contato com o SUPORTE!")
63            Exit Sub
64        Else
65            Set vObjOracle2 = vBanco.Parameters("VCURSOR").Value
66        End If
          
         '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � COMPLEMENTO
67        vBanco.Parameters.Remove "PM_CODIGO"
68        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
69        vBanco.Parameters.Remove "PM_TIPO"
70        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
71        vBanco.Parameters.Remove "PM_ROWNUM"
72        vBanco.Parameters.Add "PM_ROWNUM", 1, 1
          
73        Criar_Cursor vBanco.Parameters, "vCursor"
          
74        vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_COMPLEMENTO(:vCursor, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
          
75        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
76            Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_AGENDA, entre em contato com o SUPORTE!")
77            Exit Sub
          
78        Else
79            Set vObjOracle3 = vBanco.Parameters("VCURSOR").Value
80        End If
          
          'ATENDIMENTOS PENDENTES
          'Consultar os Atendimentos com Data Prev Retorno, Data Prev Solucao, Data Prev Liberacao menores que a data atual.
81        vBanco.Parameters.Remove "PM_CODIGO"
82        vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
83        vBanco.Parameters.Remove "PM_TIPO"
84        vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
85        vBanco.Parameters.Remove "PM_DT_ATUAL"
86        vBanco.Parameters.Add "PM_DT_ATUAL", Format(Now, "DD/MM/YY"), 1
87        vBanco.Parameters.Remove "PM_ROWNUM"
88        vBanco.Parameters.Add "PM_ROWNUM", 1, 1
          
89        vSql = "Producao.PCK_VDA640.PR_SELECT_ATENDIMENTO_PENDENTE(:vCursor, :PM_CODIGO, :PM_TIPO, TO_DATE(:PM_DT_ATUAL,'DD/MM/RR'), :PM_ROWNUM)"
          
90        Criar_Cursor vBanco.Parameters, "vCursor"
          
91        If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
92            Call vVB_Generica_001.Informar("Erro no processamento da PR_SELECT_ATENDIMENTO_PENDENTE, entre em contato com o SUPORTE!")
93            Exit Sub
94        Else
95            Set ObjAtendPend = vBanco.Parameters("VCURSOR").Value
96        End If
          
97        If ObjAtendPend.RecordCount > 0 Or vObjOracle2.RecordCount > 0 Or vObjOracle3.RecordCount <> 0 Then
98            Set cmdPendencias.PictureNormal = imlBotoes.ListImages(2).Picture
99        Else
100           Set cmdPendencias.PictureNormal = imlBotoes.ListImages(1).Picture
101       End If
          
102       Me.Caption = Me.Caption & " - " & vAtendente
          
103       cmdEstatisticas.Visible = vTipo_Atendente = "C"
104       cmdColeta.Visible = vTipo_Atendente = "C"
          
105       Call Inicia_Grid_Aberto
          
106       Preenche_Combo_Atendente
          
107       cboAtendente = vAtendente
          
108       stb.Tab = 0

Trata_Erro:
109       If Err.Number <> 0 Then
110     MsgBox "Sub FrmVDA640_LOAD" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
111       End If
End Sub

Private Sub Image1_Click()

    Shell "C:\Arquivos de programas\Internet Explorer\IEXPLORE.EXE intranet.dpk.com.br", vbNormalFocus

End Sub



Private Sub lstEmail_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    Dim tPOINT As POINTAPI
    Dim iIndex As Long

    'get the Mouse Cursor Position
    Call GetCursorPos(tPOINT)
    'Convert the Coords to be Relative to the Listbox
    Call ScreenToClient(lstEmail.hwnd, tPOINT)
    'Find which Item the Mouse is Over
    iIndex = SendMessage(lstEmail.hwnd, LB_ITEMFROMPOINT, 0&, ByVal ((tPOINT.x And &HFF) Or (&H10000 * (tPOINT.y And &HFF))))
    If iIndex >= 0 Then
        'Extract the List Index
        iIndex = iIndex And &HFF
        'set the Lists ToolTipText
        lstEmail.ToolTipText = Trim(Mid(lstEmail.List(iIndex), InStr(1, lstEmail.List(iIndex), " ") + 50))
    End If
End Sub

Private Sub lstNotas_DblClick()
    Dim i As Integer
    For i = 0 To lstNotas.ListCount - 1
        If lstNotas.Selected(i) = True Then
            txtNumAtendimento = lstNotas.List(i)
            txtNumAtendimento.SetFocus
            txtNumNota = ""
            lstNotas.Clear
            lstNotas.Visible = False
            Frame7.Visible = False
            Exit For
        End If
    Next
End Sub

Private Sub mfgAberto_Click()
    If mfgAberto.col = 0 Then
        txtTipComentarios.Visible = True
        picFecharTool.Visible = True
        txtTipComentarios = mfgAberto.TextMatrix(mfgAberto.row, 9)
    End If
End Sub

Private Sub mfgAberto_dblClick()
          
1         On Error GoTo Trata_Erro
          
2         vPosicao = 0
          
3         vNovo = False
          
4         mfgAberto.col = 1
          
5         If mfgAberto.Text <> "" Then
             
6             Frame4.Enabled = True
7             Frame1.Enabled = True
8             txtComentarios.Locked = False
9             txtDeposito.Locked = False
10            txtNota_Fiscal.Locked = False
11            txtComissao.Locked = False
12            txtAbatimento.Locked = False
13            chkComplemento.Enabled = True
14            chkProcede.Enabled = True
15            chkProrrogacao.Enabled = True
16            chkDevolucao.Enabled = True

17            vBanco.Parameters.Remove "PM_NUMERO"
18            vBanco.Parameters.Add "PM_NUMERO", mfgAberto.Text, 1
19            vNum_Atendimento = mfgAberto.Text
                  
20            lblNumAtendimento.Caption = mfgAberto.TextMatrix(mfgAberto.row, 0) & " - " & mfgAberto.TextMatrix(mfgAberto.row, 1)
              
              'lblAtendente.Caption = mfgAberto.TextMatrix(mfgAberto.Row, 2)
21            cboAtendente.Tag = mfgAberto.TextMatrix(mfgAberto.row, 2) 'Guarda o anterior
22            cboAtendente = mfgAberto.TextMatrix(mfgAberto.row, 2)
                  
23            Criar_Cursor vBanco.Parameters, "vCursor"
              
24            vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
              
25            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
26                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
27                Exit Sub
28            Else
29                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
30            End If
              
31            If vObjOracle.RecordCount > 0 Then
32                If vObjOracle!Situacao = "P" Then
33                    vPosicao = 1
34                    optPendente.Enabled = True: optPendente.Value = True
35                    optRetornado.Enabled = True
36                    optLiberado.Enabled = True
37                    optSolucionado.Enabled = True

38                ElseIf vObjOracle!Situacao = "R" Then
39                    vPosicao = 2
40                    optPendente.Enabled = False
41                    optRetornado.Enabled = True: optRetornado.Value = True
42                    optLiberado.Enabled = True
43                    optSolucionado.Enabled = True
                      
44                    chkComplemento.Enabled = False
                  
45                ElseIf vObjOracle!Situacao = "S" Then
46                    vPosicao = 3
47                    optPendente.Enabled = False
48                    optRetornado.Enabled = False
49                    optSolucionado.Enabled = True: optSolucionado.Value = True
50                    optLiberado.Enabled = True
                      
51                    chkComplemento.Enabled = False
                  
52                ElseIf vObjOracle!Situacao = "L" Then
53                    vPosicao = 4
54                    optPendente.Enabled = False
55                    optRetornado.Enabled = False
56                    optSolucionado.Enabled = False
57                    optLiberado.Enabled = True: optLiberado.Value = True
                      
58                    chkComplemento.Enabled = False
59                End If
                  
60                If vObjOracle!CONTATO = "C" Then
61                    optCliente.Value = True
62                ElseIf vObjOracle!CONTATO = "R" Then
63                    optRepresentante.Value = True
64                ElseIf vObjOracle!CONTATO = "T" Then
65                    optTelevendas.Value = True
66                ElseIf vObjOracle!CONTATO = "O" Then
67                    optOutros.Value = True
68                End If
                  
69                txtComentarios.Text = IIf(IsNull(vObjOracle!COMENTARIOS), "", vObjOracle!COMENTARIOS)
                  
70                Inserir_Nome_Atendente_Descricao
                  
71                txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
72                txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
                  txtNota_Fiscal_LostFocus
73                txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
74                txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
                  
75                If vObjOracle!FL_PROCEDE = "S" Then
76                    chkProcede.Value = 1
77                Else
78                    chkProcede.Value = 0
79                End If
              
80                If vObjOracle!FL_DEVOLUCAO = "S" Then
81                    chkDevolucao.Value = 1
82                Else
83                    chkDevolucao.Value = 0
84                End If
              
85                If vObjOracle!FL_PRORROGACAO = "S" Then
86                    chkProrrogacao.Value = 1
87                Else
88                    chkProrrogacao.Value = 0
89                End If
              
                  'cmbAssunto = Trim(vObjOracle!COD_ASSUNTO) & " - " & vObjOracle!DESC_ASSUNTO
90                cmbAssunto = vObjOracle!DESC_ASSUNTO
                  
91                If Val(Trim(0 & vObjOracle!cod_topico)) = 0 Then
92                    cmbTopico.ListIndex = -1
93                Else
94                    cmbTopico = vObjOracle!DESC_TOPICO
95                End If
                  
96                If Val(Trim(0 & vObjOracle!cod_detalhe)) = 0 Then
97                    cmbDetalhe.ListIndex = -1
98                Else
99                    cmbDetalhe.Text = vObjOracle!DESC_DETALHE
100               End If
                  
101               txtProcedimento.Text = "" & vObjOracle!PROC_RECLAMACAO
102               txtRetorno.Text = "" & vObjOracle!DIAS_RETORNO
                  
103               If Val(txtRetorno.Text) = 0 Then
104                   optRetornado.Enabled = False
105               Else
106                   If vPosicao <= 2 Then optRetornado.Enabled = True
107               End If
                  
108               txtLiberacao.Text = Val(0 & vObjOracle!DIAS_LIBERACAO)
109               If txtLiberacao.Text = 0 Then
110                   optLiberado.Enabled = False
111               Else
112                   If vPosicao <= 3 Then optLiberado.Enabled = True
113               End If
                  
114               txtSolucao.Text = Val(0 & vObjOracle!DIAS_SOLUCAO)
115               If txtSolucao.Text = 0 Then
116                   optSolucionado.Enabled = False
117               Else
118                   If vPosicao = 4 Then optSolucionado.Enabled = True
119               End If
                  
120               If vObjOracle!FL_DISPENSA_RETORNO = "S" Then
121                   optDispensa_Retorno.Value = True
122               End If
              
123               If vObjOracle!fl_falta_complemento = "S" Then
124                   chkComplemento.Value = 1
125                   optPendente.Enabled = False
126                   optRetornado.Enabled = False
127                   optSolucionado.Enabled = False
128                   optLiberado.Enabled = False
129               Else
130                   chkComplemento.Value = 0
131               End If
              
132               If txtComentarios.Enabled = True Then txtComentarios.SetFocus
                  
133               fl_Novo = "N"
                  
134               cmdAgenda.Enabled = True
135               cmbAssunto.Enabled = False
                  
136               stb.Tab = 1
              
137           Else
138               Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
              
139               cmdAgenda.Enabled = False
140           End If
141       End If
Trata_Erro:
142       If Err.Number = 383 Then
143           Resume Next
144       ElseIf Err.Number <> 0 Then
145           MsgBox "Sub mfgAberto_dblClick" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
146       End If
End Sub



Private Sub mfgFinalizado_Click()
    
    If mfgFinalizado.TextMatrix(1, 1) = "" Then Exit Sub
    PicFecharFim.Visible = True
    txtTipComentariosFim.Visible = True
    txtTipComentariosFim = mfgFinalizado.TextMatrix(mfgFinalizado.row, 9)

End Sub

Private Sub mfgFinalizado_dblClick()
          
1         On Error GoTo Trata_Erro
          
2         vNovo = False
          
3         mfgFinalizado.col = 1
4         If mfgFinalizado.Text <> "" Then
5             Frame4.Enabled = False
6             Frame1.Enabled = False
7             txtComentarios.Locked = True
8             txtDeposito.Locked = True
9             txtNota_Fiscal.Locked = True
10            txtComissao.Locked = True
11            txtAbatimento.Locked = True
12            chkComplemento.Enabled = False
13            chkProcede.Enabled = False
14            chkProrrogacao.Enabled = False
15            chkDevolucao.Enabled = False
              
16            cmdGravar_Atendimento.Enabled = False
              
17            vBanco.Parameters.Remove "PM_NUMERO"
18            vBanco.Parameters.Add "PM_NUMERO", mfgFinalizado.Text, 1
19            vNum_Atendimento = mfgFinalizado.Text
              
20            Criar_Cursor vBanco.Parameters, "vCursor"
              
21            vSql = "Producao.PCK_VDA640.PR_BUSCA_ATENDIMENTO(:vCursor,:PM_NUMERO)"
              
22            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
23                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_ATENDIMENTO, entre em contato com o SUPORTE!")
24                Exit Sub
25            Else
26                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
27            End If
              
28            If Not vObjOracle.EOF Then
29                If vObjOracle!Situacao = "P" Then
30                    optPendente.Value = True
31                ElseIf vObjOracle!Situacao = "R" Then
32                    optRetornado.Value = True
33                    optPendente.Enabled = False
34                    chkComplemento.Enabled = False
                  
35                ElseIf vObjOracle!Situacao = "S" Then
36                    optSolucionado.Value = True
37                    optPendente.Enabled = False
38                    optRetornado.Enabled = False
39                    chkComplemento.Enabled = False
                  
40                ElseIf vObjOracle!Situacao = "L" Then
41                    optLiberado.Value = True
42                    optPendente.Enabled = False
43                    optRetornado.Enabled = False
44                    optSolucionado.Enabled = False
45                    chkComplemento.Enabled = False
46                End If
                  
47                If vObjOracle!CONTATO = "C" Then
48                    optCliente.Value = True
49                ElseIf vObjOracle!CONTATO = "R" Then
50                    optRepresentante.Value = True
51                ElseIf vObjOracle!CONTATO = "T" Then
52                    optTelevendas.Value = True
53                ElseIf vObjOracle!CONTATO = "O" Then
54                    optOutros.Value = True
55                End If
                  
56                txtComentarios.Text = IIf(IsNull(vObjOracle!COMENTARIOS), "", vObjOracle!COMENTARIOS)

57                txtDeposito.Text = IIf(IsNull(vObjOracle!COD_LOJA), "", vObjOracle!COD_LOJA)
58                txtNota_Fiscal.Text = IIf(IsNull(vObjOracle!NUM_NOTA), "", vObjOracle!NUM_NOTA)
59                txtComissao.Text = IIf(IsNull(vObjOracle!PC_DESC_COMISSAO), "", vObjOracle!PC_DESC_COMISSAO)
60                txtAbatimento.Text = IIf(IsNull(vObjOracle!VL_ABATIMENTO), "", vObjOracle!VL_ABATIMENTO)
                  
61                lblNumAtendimento.Caption = mfgFinalizado.TextMatrix(mfgFinalizado.row, 0) & " - " & mfgFinalizado.TextMatrix(mfgFinalizado.row, 1)
                  
                  
62                If vObjOracle!FL_PROCEDE = "S" Then
63                    chkProcede.Value = 1
64                Else
65                    chkProcede.Value = 0
66                End If
              
67                If vObjOracle!FL_DEVOLUCAO = "S" Then
68                    chkDevolucao.Value = 1
69                Else
70                    chkDevolucao.Value = 0
71                End If
              
72                If vObjOracle!FL_PRORROGACAO = "S" Then
73                    chkProrrogacao.Value = 1
74                Else
75                    chkProrrogacao.Value = 0
76                End If
              
77                cmbAssuntoCompleto.Text = vObjOracle!DESC_ASSUNTO

78                If Trim(vObjOracle!cod_topico) = 0 Then
79                    cmbTopicoCompleto.ListIndex = -1
80                Else
81                    cmbTopicoCompleto.Text = vObjOracle!DESC_TOPICO
82                End If

83                If Trim(vObjOracle!cod_detalhe) = 0 Then
84                    cmbDetalheCompleto.ListIndex = -1
85                Else
86                    cmbDetalheCompleto.Text = vObjOracle!DESC_DETALHE
87                End If
                  
                 '29/09/2006
88                cmbAssuntoCompleto.width = cmbAssunto.width
89                cmbTopicoCompleto.width = cmbTopico.width
90                cmbDetalheCompleto.width = cmbDetalhe.width
                
91                cmbAssunto.Visible = False
92                cmbTopico.Visible = False
93                cmbDetalhe.Visible = False

94                cmbAssuntoCompleto.Visible = True
95                cmbTopicoCompleto.Visible = True
96                cmbDetalheCompleto.Visible = True
                
'97                cmbAssuntoCompleto = vObjOracle!DESC_ASSUNTO
                  
'98                If IsNull(vObjOracle!DESC_DETALHE) Then
'99                   cmbTopicoCompleto.ListIndex = -1
'100               Else
'101                  cmbTopicoCompleto = vObjOracle!DESC_TOPICO
'102               End If
                  
'103               If IsNull(vObjOracle!DESC_DETALHE) Then
'104                  cmbDetalheCompleto.ListIndex = -1
'105               Else
'106                  cmbDetalheCompleto = vObjOracle!DESC_DETALHE
'107               End If
                  '29/09/2006
                  
97                txtProcedimento.Text = vObjOracle!PROC_RECLAMACAO
98                txtRetorno.Text = vObjOracle!DIAS_RETORNO
99                If txtRetorno.Text = 0 Then
100                   optRetornado.Enabled = False
101               Else
102                   optRetornado.Enabled = True
103               End If
                  
104               txtSolucao.Text = vObjOracle!DIAS_SOLUCAO
105               If txtSolucao.Text = 0 Then
106                   optSolucionado.Enabled = False
107               Else
108                   optSolucionado.Enabled = True
109               End If
                  
110               txtLiberacao.Text = vObjOracle!DIAS_LIBERACAO
111               If txtLiberacao.Text = 0 Then
112                   optLiberado.Enabled = False
113               Else
114                   optLiberado.Enabled = True
115               End If
                  
116               If vObjOracle!fl_falta_complemento = "S" Then
117                   chkComplemento.Value = 1
118                   optPendente.Enabled = False
119                   optRetornado.Enabled = False
120                   optSolucionado.Enabled = False
121                   optLiberado.Enabled = False
122               Else
123                   chkComplemento.Value = 0
124               End If
              
125               If txtComentarios.Enabled = True Then txtComentarios.SetFocus
                  
126               fl_Novo = "N"
                  
127               cmdAgenda.Enabled = True
128               cmbAssunto.Enabled = False
                  
129               stb.Tab = 1
130           Else
131               Call vVB_Generica_001.Informar("Problemas com o atendimento escolhido!")
              
132               cmdAgenda.Enabled = False
133           End If
134       End If
          
Trata_Erro:
135       If Err.Number <> 0 Then
136           If Err.Number = 383 Then
137               Resume Next
138           Else
139               MsgBox "Sub Exibir_Historico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
140           End If
141       End If
End Sub

Private Sub optLiberado_Click()
    cmdImprimirDevolucao.Enabled = False
    chkComplemento.Value = False
    chkComplemento.Enabled = Not optLiberado.Value
End Sub

Private Sub optPendente_Click()
    cmdImprimirDevolucao.Enabled = False
    chkComplemento.Enabled = optPendente.Value
End Sub

Private Sub optRetornado_Click()
    chkComplemento.Value = False
    chkComplemento.Enabled = Not optRetornado.Value
End Sub

Private Sub optSolucionado_Click()
    chkComplemento.Value = False
    chkComplemento.Enabled = Not optSolucionado.Value

    cmdImprimirDevolucao.Enabled = stb.Tab = 1 And InStr(1, UCase(cmbAssunto), "DEVOLU��O") > 0 And optSolucionado.Value = True

End Sub

Private Sub PicEmailDesatualizado_Click()
    txtEmailDesatualizado = ""
    txtEmailDesatualizado.Visible = False
    PicEmailDesatualizado.Visible = False
End Sub

Private Sub PicFecharFim_Click()
    txtTipComentariosFim = ""
    txtTipComentariosFim.Visible = False
    PicFecharFim.Visible = False
End Sub

Private Sub picFecharTool_Click()
    txtTipComentarios = ""
    txtTipComentarios.Visible = False
    picFecharTool.Visible = False
End Sub



Private Sub stb_Click(PreviousTab As Integer)
          
1         On Error GoTo Trata_Erro
          
2         cmdImprimirDevolucao.Enabled = stb.Tab = 1 And InStr(1, UCase(cmbAssunto), "DEVOLU��O") > 0 And optSolucionado.Value = True
          
3         If stb.Tab = 0 Then
4            vNum_Atendimento = 0
5            cmbAssuntoCompleto.Visible = False
6            cmbTopicoCompleto.Visible = False
7            cmbDetalheCompleto.Visible = False
             
8            cmbAssunto.Visible = True
9            cmbTopico.Visible = True
10           cmbDetalhe.Visible = True

11           cmdGravar_Atendimento.Enabled = True
             
12        End If
          
13        Me.SSTab.Tab = 0
              
14        If stb.Tab = 1 And vNum_Atendimento = 0 Then
15            Frame4.Enabled = True
16            Frame1.Enabled = True
17            txtComentarios.Locked = False
18            txtDeposito.Locked = False
19            txtNota_Fiscal.Locked = False
20            txtComissao.Locked = False
21            txtAbatimento.Locked = False
22            chkComplemento.Enabled = True
23            chkProcede.Enabled = True
24            chkProrrogacao.Enabled = True
25            chkDevolucao.Enabled = True
              
26        ElseIf stb.Tab = 1 And vNum_Atendimento <> 0 Then
27            If Trim(Me.txtDeposito) <> "" And Trim(Me.txtNota_Fiscal) <> "" Then
28                txtNota_Fiscal_LostFocus
29            End If
30        End If
          
31        If vNum_Atendimento = 0 Then
32            Limpar_Tab_Atendimento
              
33            Inserir_Nome_Atendente_Descricao
              
34            vNum_Atendimento = 0
35        End If

Trata_Erro:
36        If Err.Number <> 0 Then
37      MsgBox "Sub Stb_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
38        End If

End Sub



Private Sub txtAbatimento_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 44 Then
        KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtAbatimento)
    Else
        KeyAscii = 0
    End If
End Sub

Private Sub txtCGC_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCGC_LostFocus()

    On Error GoTo Trata_Erro

          Dim Valido As Boolean
          
1         If txtCGC.Text <> "" Then
             '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO CGC DO CLIENTE
2             vBanco.Parameters.Remove "PM_CGC"
3             vBanco.Parameters.Add "PM_CGC", txtCGC.Text, 1
              
4             Criar_Cursor vBanco.Parameters, "vCursor"
              
5             vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,NULL,:PM_CGC,NULL)"
              
6             If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
7                 Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
8                 Exit Sub
9             Else
10                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
11            End If
              
12            If Not vObjOracle.EOF Then
13                txtCodigo_Cliente.Text = vObjOracle!cod_cliente
14                Call txtCodigo_Cliente_LostFocus
15            Else
16                MsgBox "Cliente n�o encontrado.", vbInformation, "Aten��o"
17            End If
                  
18        End If
          
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub TxtCGC_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
          
End Sub

Private Sub txtCodigo_Cliente_Change()
    vNum_Atendimento = 0
    txtTipComentarios.Visible = False
    picFecharTool.Visible = False

    txtTipComentariosFim.Visible = False
    PicFecharFim.Visible = False
    
    If Trim(txtDeposito) <> "" And Trim(Me.txtNota_Fiscal) <> "" And txtCodigo_Cliente.Tag <> "Click" Then
        txtNota_Fiscal_LostFocus
    End If
End Sub

Private Sub txtCodigo_Cliente_Click()
    txtCodigo_Cliente.Tag = "Click"
    Call cmdLimpar_Click
    txtCodigo_Cliente.Tag = ""

    txtTipComentariosFim.Visible = False
    PicFecharFim.Visible = False
End Sub

Private Sub txtCodigo_Cliente_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Public Sub txtCodigo_Cliente_LostFocus()

1         On Error GoTo Trata_Erro

2         If Me.Tag <> "" Then Exit Sub

         '-- CARREGA OS DADOS DO CLIENTE NA TELA ATRAV�S DO C�DIGO DO CLIENTE
3         If txtCodigo_Cliente.Text <> "" Then
              
4             vBanco.Parameters.Remove "PM_CODCLIENTE"
5             vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
              
6             vSql = "Producao.PCK_VDA640.PR_BUSCA_CLIENTE(:vCursor,:PM_CODCLIENTE,NULL,NULL)"
              
7             If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
8                 Unload frmAguardar
9                 Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_CLIENTE, entre em contato com o SUPORTE!")
10                Exit Sub
11            Else
12                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
13            End If
              
14            If Not vObjOracle.EOF Then
15                txtRazao_Social.Text = vObjOracle!NOME_CLIENTE
16                txtContato.Text = IIf(IsNull(vObjOracle!NOME_CONTATO), "", vObjOracle!NOME_CONTATO)
17                txtCGC.Text = vObjOracle!CGC
18                txtCidade.Text = vObjOracle!NOME_CIDADE
19                txtUF.Text = vObjOracle!COD_UF
20                txtCod_Representante.Text = vObjOracle!COD_REPRES
21                txtNome_Representante.Text = vObjOracle!NOME_REPRES
22                txtDDD1.Text = vObjOracle!DDD1
23                txtTelefone1.Text = vObjOracle!FONE1
24                txtDDD2.Text = IIf(IsNull(vObjOracle!DDD2), 0, vObjOracle!DDD2)
25                txtTelefone2.Text = IIf(IsNull(vObjOracle!FONE2), 0, vObjOracle!FONE2)
26                txtFilial.Text = vObjOracle!COD_FILIAL & " - " & vObjOracle!NOME_FILIAL
27            Else
28                Unload frmAguardar
29                MsgBox "Cliente n�o encontrado.", vbInformation, "Aten��o"
30                txtCodigo_Cliente.SetFocus
31                Exit Sub
32            End If
              
             '-- BUSCA O TIPO_FIEL DO CLIENTE
33            vBanco.Parameters.Remove "PM_CODCLIENTE"
34            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
              
35            Criar_Cursor vBanco.Parameters, "vCursor"
              
36            vSql = "Producao.PCK_VDA640.PR_BUSCA_TP_FIEL(:vCursor,:PM_CODCLIENTE)"
              
37            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
38                Unload frmAguardar
39                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_TP_FIEL, entre em contato com o SUPORTE!")
40                Exit Sub
41            Else
42                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
43            End If
              
44            If Not vObjOracle.EOF Then
45                txtTipo_Fiel.Text = vObjOracle!DESC_FIEL
46            End If

             '-- BUSCA OS ATENDIMENTOS REGISTRADOS PARA O CLIENTE, QUE N�O EST�O FINALIZADOS
47            vBanco.Parameters.Remove "PM_CODCLIENTE"
48            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
49            vBanco.Parameters.Remove "PM_SITUACAO"
50            vBanco.Parameters.Add "PM_SITUACAO", "A", 1
              
51            Criar_Cursor vBanco.Parameters, "vCursor"
              
52            vSql = "Producao.PCK_VDA640.PR_BUSCA_LIGACOES(:vCursor,:PM_CODCLIENTE,:PM_SITUACAO)"
              
53            If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
54                Unload frmAguardar
55                Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_LIGACOES, entre em contato com o SUPORTE!")
56                Exit Sub
57            Else
58                Set vObjOracle = vBanco.Parameters("VCURSOR").Value
59            End If
              
60            vData = Format(Date, "DD/MM/YY")
              
61            mfgAberto.Clear
62            mfgAberto.Rows = 2
63            Call Inicia_Grid_Aberto
              
64            If Not vObjOracle.EOF Then
65                vCount_Abertos = vObjOracle.RecordCount
66                lblQtd_Aberto.Caption = vCount_Abertos
                  
67                mfgAberto.Rows = vObjOracle.RecordCount + 1
68                mfgAberto.row = 1
69                mfgAberto.col = 0
70                For i = 1 To vObjOracle.RecordCount
                      
71                    mfgAberto.col = 0
72                    mfgAberto.row = i
                      
73                    Set mfgAberto.CellPicture = imgComentarios.Picture
74                    mfgAberto.CellPictureAlignment = 0
75                    mfgAberto.TextMatrix(i, 0) = vObjOracle.Fields(0)
76                    mfgAberto.col = 1
77                    mfgAberto.Text = vObjOracle.Fields(1)
78                    mfgAberto.CellFontBold = True

79                    mfgAberto.TextMatrix(i, 2) = vObjOracle.Fields(2)
80                    mfgAberto.TextMatrix(i, 3) = IIf(IsNull(vObjOracle.Fields(3)), "", vObjOracle.Fields(3))
81                    mfgAberto.TextMatrix(i, 4) = IIf(IsNull(vObjOracle.Fields(4)), "", vObjOracle.Fields(4))
82                    mfgAberto.TextMatrix(i, 5) = vObjOracle.Fields(5)
83                    mfgAberto.TextMatrix(i, 6) = IIf(Left(Trim(vObjOracle.Fields(6)), 3) = 0, "", vObjOracle.Fields(6))
84                    mfgAberto.TextMatrix(i, 7) = IIf(Left(Trim(vObjOracle.Fields(7)), 3) = 0, "", vObjOracle.Fields(7))
85                    If vObjOracle.Fields(8) = "P" Then
86                        mfgAberto.TextMatrix(i, 8) = "Pendente"
87                    ElseIf vObjOracle.Fields(8) = "R" Then
88                        mfgAberto.TextMatrix(i, 8) = "Retornado"
89                    ElseIf vObjOracle.Fields(8) = "S" Then
90                        mfgAberto.TextMatrix(i, 8) = "Solucionado"
91                    ElseIf vObjOracle.Fields(8) = "L" Then
92                        mfgAberto.TextMatrix(i, 8) = "Liberado"
93                    End If
                      
94                    Pintar_Linha mfgAberto, 9, vbBlack, i
                      
95                    If IsNull(vObjOracle!dt_complemento) Then
96                        Pintar_Linha mfgAberto, 9, vbRed, i
97                    End If
98                    If Not IsNull(vObjOracle!DT_PREV_RETORNO) Then
99                        If IsNull(vObjOracle!dt_retorno) And _
                             CDate(vObjOracle!DT_PREV_RETORNO) < vData Then
100                           Pintar_Linha mfgAberto, 9, vbRed, i
101                       End If
102                   End If
103                   If Not IsNull(vObjOracle!DT_PREV_SOLUCAO) Then
104                       If IsNull(vObjOracle!dt_solucao) And _
                             CDate(vObjOracle!DT_PREV_SOLUCAO) < vData Then
105                           Pintar_Linha mfgAberto, 9, vbRed, i
106                       End If
107                   End If
108                   If Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
109                       If IsNull(vObjOracle!dt_liberacao) And _
                             CDate(vObjOracle!DT_PREV_LIBERACAO) < vData Then
110                           Pintar_Linha mfgAberto, 9, vbRed, i
111                       End If
112                   End If
                                      
113                   mfgAberto.TextMatrix(mfgAberto.row, 9) = vObjOracle!COMENTARIOS
                                      
114                   vObjOracle.MoveNext
                  
115               Next
116           End If
             
             
      'Eduardo 22/06/05
      'Atendimentos Inseridos na INTRANET
117           Criar_Cursor vBanco.Parameters, "vCursor"
              
118           vSql = "Producao.PCK_VDA640.PR_BUSCA_LIGACOES_INTRANET(:vCursor,:PM_CODCLIENTE)"
              
119           If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
120               Unload frmAguardar
121               Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_LIGACOES_INTRANET, entre em contato com o SUPORTE!")
122               Exit Sub
123           Else
124               Set vObjOracle = vBanco.Parameters("VCURSOR").Value
125           End If
              
126           vData = Format(Date, "DD/MM/YY")
              
127           If Not vObjOracle.EOF Then
                  
128               If mfgAberto.Rows = 1 Or mfgAberto.Rows = 0 Then
129                   mfgAberto.Rows = IIf(vObjOracle.RecordCount <= 1, 2, vObjOracle.RecordCount)
130               Else
131                   mfgAberto.Rows = mfgAberto.Rows + vObjOracle.RecordCount
132               End If
                  
133               vCount_Abertos = vCount_Abertos + vObjOracle.RecordCount
134               lblQtd_Aberto.Caption = vCount_Abertos
                  
135               For i = 1 To vObjOracle.RecordCount
                      
                      'Pegar numero da proxima linha em branco
136                   For ii = 1 To mfgAberto.Rows - 1
137                       If mfgAberto.TextMatrix(ii, 0) = "" Then
138                           mfgAberto.row = ii
139                           Exit For
140                       End If
141                   Next
                      
                      
142                   If mfgAberto.TextMatrix(1, 1) = "" Then mfgAberto.row = 1
                      
143                   mfgAberto.col = 0
                      
144                   Set mfgAberto.CellPicture = imgComentarios.Picture
145                   mfgAberto.CellPictureAlignment = 0
146                   mfgAberto.TextMatrix(i, 0) = vObjOracle.Fields(0)
147                   mfgAberto.col = 1
148                   mfgAberto.Text = vObjOracle.Fields(1)
149                   mfgAberto.CellFontBold = True
                      
150                   mfgAberto.TextMatrix(i, 2) = vObjOracle.Fields(2)
151                   mfgAberto.TextMatrix(i, 3) = IIf(IsNull(vObjOracle.Fields(3)), "", vObjOracle.Fields(3))
152                   mfgAberto.TextMatrix(i, 4) = IIf(IsNull(vObjOracle.Fields(4)), "", vObjOracle.Fields(4))
153                   mfgAberto.TextMatrix(i, 5) = vObjOracle.Fields(5)
154                   mfgAberto.TextMatrix(i, 6) = IIf(Left(Trim(vObjOracle.Fields(6)), 3) = 0, "", vObjOracle.Fields(6))
155                   mfgAberto.TextMatrix(i, 7) = IIf(Left(Trim(vObjOracle.Fields(7)), 3) = 0, "", vObjOracle.Fields(7))
156                   If vObjOracle.Fields(8) = "P" Then
157                       mfgAberto.TextMatrix(i, 8) = "Pendente"
158                   ElseIf vObjOracle.Fields(8) = "R" Then
159                       mfgAberto.TextMatrix(i, 8) = "Retornado"
160                   ElseIf vObjOracle.Fields(8) = "S" Then
161                       mfgAberto.TextMatrix(i, 8) = "Solucionado"
162                   ElseIf vObjOracle.Fields(8) = "L" Then
163                       mfgAberto.TextMatrix(i, 8) = "Liberado"
164                   End If
                      
165                   Pintar_Linha mfgAberto, 9, vbBlue, i
                      
166                   If IsNull(vObjOracle!dt_complemento) Then
167                       Pintar_Linha mfgAberto, 9, &H80&, i
168                   End If
169                   If Not IsNull(vObjOracle!DT_PREV_RETORNO) Then
170                       If IsNull(vObjOracle!dt_retorno) And _
                             CDate(vObjOracle!DT_PREV_RETORNO) < vData Then
171                           Pintar_Linha mfgAberto, 9, vbMagenta, i
172                       End If
173                   End If
174                   If Not IsNull(vObjOracle!DT_PREV_SOLUCAO) Then
175                       If IsNull(vObjOracle!dt_solucao) And _
                             CDate(vObjOracle!DT_PREV_SOLUCAO) < vData Then
176                           Pintar_Linha mfgAberto, 9, vbMagenta, i
177                       End If
178                   End If
179                   If Not IsNull(vObjOracle!DT_PREV_LIBERACAO) Then
180                       If IsNull(vObjOracle!dt_liberacao) And _
                             CDate(vObjOracle!DT_PREV_LIBERACAO) < vData Then
181                           Pintar_Linha mfgAberto, 9, vbMagenta, i
182                       End If
183                   End If
                                      
184                   mfgAberto.TextMatrix(mfgAberto.row, 9) = vObjOracle!COMENTARIOS
                                      
185                   vObjOracle.MoveNext
                  
186               Next
187           End If
      'FIM 22/06/05
             
             '-- BUSCA OS ATENDIMENTOS REGISTRADOS PARA O CLIENTE, QUE EST�O FINALIZADOS
188           vBanco.Parameters.Remove "PM_CODCLIENTE"
189           vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
190           vBanco.Parameters.Remove "PM_SITUACAO"
191           vBanco.Parameters.Add "PM_SITUACAO", "F", 1
              
192           Criar_Cursor vBanco.Parameters, "vCursor"
              
193           vSql = "Producao.PCK_VDA640.PR_BUSCA_LIGACOES(:vCursor,:PM_CODCLIENTE,:PM_SITUACAO)"
              
194           If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
195               Unload frmAguardar
196               Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_LIGACOES, entre em contato com o SUPORTE!")
197               Exit Sub
198           Else
199               Set vObjOracle = vBanco.Parameters("VCURSOR").Value
200           End If
              
201           If Not vObjOracle.EOF Then
202               vCount_Finalizados = vObjOracle.RecordCount
203               lblQtd_Finalizado.Caption = vCount_Finalizados
204               Preenche_Grid_Finalizado vObjOracle, vCount_Finalizados
205           End If

             '-- CARREGA O �CONE DE PEND�NCIA REFERENTE � AGENDA DO CLIENTE
206           vBanco.Parameters.Remove "PM_CODIGO"
207           vBanco.Parameters.Add "PM_CODIGO", vCod_Usuario, 1
208           vBanco.Parameters.Remove "PM_TIPO"
209           vBanco.Parameters.Add "PM_TIPO", vTipo_Atendente, 1
210           vBanco.Parameters.Remove "PM_CODCLIENTE"
211           vBanco.Parameters.Add "PM_CODCLIENTE", txtCodigo_Cliente.Text, 1
212           vBanco.Parameters.Remove "PM_ROWNUM"
213           vBanco.Parameters.Add "PM_ROWNUM", 0, 1
              
214           Criar_Cursor vBanco.Parameters, "vCursor"
              
215           vSql = "Producao.PCK_VDA640.PR_BUSCA_PENDENCIA_CLIENTE(:vCursor, :PM_CODCLIENTE, :PM_CODIGO, :PM_TIPO, :PM_ROWNUM)"
              
216           If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
217               Unload frmAguardar
218               Call vVB_Generica_001.Informar("Erro no processamento da PR_BUSCA_PENDENCIA_PRAZO, entre em contato com o SUPORTE!")
219               Exit Sub
220           Else
221               Set vObjOracle = vBanco.Parameters("VCURSOR").Value
222           End If
          
223           If vObjOracle.RecordCount <> 0 Then
224               Set cmdAgenda.PictureNormal = imlBotoes.ListImages(2).Picture
225           Else
226               Set cmdAgenda.PictureNormal = imlBotoes.ListImages(1).Picture
227           End If
              
              
228           fl_Novo = "S"
          
229           stb.TabEnabled(1) = True

230           Unload frmAguardar
              
231           Carregar_Email
              
232       End If

Trata_Erro:
233       If Err.Number <> 0 Then
234     MsgBox "Sub txtCodigo_Cliente_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
235       End If

End Sub



Private Sub txtComentarios_KeyPress(KeyAscii As Integer)
    If KeyAscii = 123 Or KeyAscii = 59 Then
        KeyAscii = 0
    End If
End Sub

Private Sub txtComissao_KeyPress(KeyAscii As Integer)
    If KeyAscii <> 8 Then
        If KeyAscii <> 44 Then
            If Trim(txtNota_Fiscal) = "" Or Trim(txtDeposito) = "" Then
                MsgBox "Preencha o n�mero da Nota Fiscal.", vbInformation, "Aten��o"
                txtNota_Fiscal.SetFocus
                KeyAscii = 0
                Exit Sub
            End If
                
            KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtComissao)
        Else
            KeyAscii = 0
        End If
    End If
End Sub

Private Sub txtDeposito_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtLiberacao_Change()
     optLiberado.Enabled = Not Val(txtLiberacao) = 0
End Sub

Private Sub txtNota_Fiscal_Change()
    txtNota_Fiscal.ForeColor = vbBlack
    txtNota_Fiscal.FontBold = False
    cmdDuplEmAtraso.Visible = False
End Sub

Private Sub txtNota_Fiscal_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Public Sub txtNota_Fiscal_LostFocus()
          
    On Error GoTo Trata_Erro
          
1         txtNota_Fiscal.ForeColor = vbBlack
2         txtNota_Fiscal.FontBold = False
          
3         cmdDuplEmAtraso.Visible = False
          
4         If Trim(txtCodigo_Cliente) = "" And cmdLimpar.Tag = "" Then
5            MsgBox "Informe o c�digo do cliente.", vbInformation, "Aten��o"
6            Exit Sub
7         End If
          
          
8         If txtNota_Fiscal <> "" Then
9             vBanco.Parameters.Remove "PM_NF"
10            vBanco.Parameters.Add "PM_NF", txtNota_Fiscal, 1
11            vBanco.Parameters.Remove "PM_DEPOSITO"
12            vBanco.Parameters.Add "PM_DEPOSITO", txtDeposito, 1
13            vBanco.Parameters.Remove "PM_COD_CLIENTE"
14            vBanco.Parameters.Add "PM_COD_CLIENTE", 0, 2
              
15            vSql = "Producao.PCK_VDA640.PR_SELECT_NOTA(:PM_NF, :PM_DEPOSITO, :PM_COD_CLIENTE)"
          
16            vVB_Generica_001.ExecutaPl vBanco, vSql
          
17            If vBanco.Parameters("PM_COD_CLIENTE").Value = 0 Then
18                MsgBox "Nota Fiscal n�o Cadastrada.", , "Aten��o"
19                txtNota_Fiscal.SetFocus
20                Exit Sub
21            ElseIf Val(vBanco.Parameters("PM_COD_CLIENTE").Value) <> Val(txtCodigo_Cliente) And txtCodigo_Cliente <> "" Then
22                MsgBox "Nota Fiscal n�o pertence a este Cliente.", , "Aten��o"
23                If txtNota_Fiscal.Enabled = True Then txtNota_Fiscal.SetFocus
24                Exit Sub
25            End If
              
26            vBanco.Parameters.Remove "PM_QTD"
27            vBanco.Parameters.Add "PM_QTD", 0, 2
              
28            vBanco.Parameters.Remove "PM_COD_CLI"
29            vBanco.Parameters.Add "PM_COD_CLI", txtCodigo_Cliente, 2
              
30            vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA640.pr_count_dupl_em_atraso(:PM_DEPOSITO, :PM_NF, :PM_COD_CLI, :PM_QTD)"

31            If Val(vBanco.Parameters("PM_QTD").Value) > 0 Then
32                txtNota_Fiscal.ForeColor = vbRed
33                txtNota_Fiscal.FontBold = True
                      
34                cmdDuplEmAtraso.Visible = True
35                cmdDuplEmAtraso.Caption = "Existe Fatura em Atraso para esta NF."
36                Exit Sub
                      
37            Else
38                txtNota_Fiscal.ForeColor = vbBlack
39                txtNota_Fiscal.FontBold = False
40            End If
                  
      'Eduardo - 02/12/05 - Solicitacao da Natalia
      'Exibir uma mensagem quando faltam menos de 15 dias para vencer a Duplicata.
41            vVB_Generica_001.ExecutaPl vBanco, "PRODUCAO.PCK_VDA640.pr_count_dupl_a_Vencer(:PM_DEPOSITO, :PM_NF, :PM_COD_CLI, :PM_QTD)"

42            If Val(vBanco.Parameters("PM_QTD").Value) > 0 Then
43                txtNota_Fiscal.ForeColor = vbBlue
44                txtNota_Fiscal.FontBold = True
                      
45                cmdDuplEmAtraso.Visible = True
46                cmdDuplEmAtraso.Caption = "Fatura a vencer em menos de 15 dias."
                      
47            Else
48                txtNota_Fiscal.ForeColor = vbBlack
49                txtNota_Fiscal.FontBold = False
50            End If
51        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub txtNota_Fiscal_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub txtNumAtendimento_GotFocus()
    txtNumNota = ""
    lstNotas.Clear
    lstNotas.Visible = False
    Frame7.Visible = False
End Sub

Private Sub txtNumAtendimento_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtNumAtendimento_LostFocus()

    On Error GoTo Trata_Erro
          
1         If txtNumAtendimento = "" Then Exit Sub
          
2         vBanco.Parameters.Remove "PM_COD_ATENDIMENTO"
3         vBanco.Parameters.Add "PM_COD_ATENDIMENTO", Val(txtNumAtendimento), 1
4         vBanco.Parameters.Remove "PM_COD_CLIENTE"
5         vBanco.Parameters.Add "PM_COD_CLIENTE", 0, 2
              
6         vSql = "Producao.PCK_VDA640.PR_SELECT_COD_CLIENTE(:PM_COD_ATENDIMENTO, :PM_COD_CLIENTE)"

7         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

8         If vErro <> "" Then
9             vVB_Generica_001.Informar vErro
10        End If
          
11        If vBanco.Parameters("PM_COD_CLIENTE") <> 0 Then
12            txtCodigo_Cliente = vBanco.Parameters("PM_COD_CLIENTE").Value
13            txtCodigo_Cliente_LostFocus
14            For i = 1 To mfgAberto.Rows - 1
15                If mfgAberto.TextMatrix(i, 1) = txtNumAtendimento Then
16                    mfgAberto.row = i
17                    mfgAberto_dblClick
18                    Exit Sub
19                End If
20            Next
21            For i = 1 To mfgFinalizado.Rows - 1
22                If mfgFinalizado.TextMatrix(i, 1) = txtNumAtendimento Then
23                    mfgFinalizado.row = i
24                    mfgFinalizado_dblClick
25                    Exit Sub
26                End If
27            Next
              
28        Else
29            MsgBox "N�mero do atendimento n�o encontrado.", vbInformation, "Aten��o"
30        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub txtNumAtendimento_Lostfocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub txtNumNota_GotFocus()
    txtNumAtendimento = ""
End Sub

Private Sub txtNumNota_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtNumNota_LostFocus()
    'Dim vObjOracle As Object
    Dim i As Integer
    
    On Error GoTo Trata_Erro
    
    If txtNumNota = "" Then Exit Sub
    
    Criar_Cursor vBanco.Parameters, "vCursor"
    vBanco.Parameters.Remove "PM_NUM_NOTA"
    vBanco.Parameters.Add "PM_NUM_NOTA", Val(txtNumNota), 1
    vBanco.Parameters.Remove "PM_COD_CLIENTE"
    vBanco.Parameters.Add "PM_COD_CLIENTE", 0, 2
        
    frmAguardar.Show
    StayOnTop frmAguardar
    frmAguardar.Refresh
    vSql = "Producao.PCK_VDA640.PR_SELECT_COD_CLIENTE_POR_NOTA(:vCursor, :PM_NUM_NOTA, :PM_COD_CLIENTE)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        vVB_Generica_001.Informar vErro
    End If
    
    Set vObjOracle = vBanco.Parameters("vcursor").Value
    If vObjOracle.EOF = False Then
        For i = 0 To vObjOracle.RecordCount - 1
            lstNotas.AddItem vObjOracle!Num_Atendimento
            vObjOracle.MoveNext
        Next
        lstNotas.Visible = True
        Frame7.Visible = True
        Unload frmAguardar
        Exit Sub
    End If
    Unload frmAguardar
    
    
    'If vBanco.Parameters("PM_NUM_NOTA") <> 0 Then
    'txtCodigo_Cliente = vBanco.Parameters("PM_COD_CLIENTE").Value
    'txtCodigo_Cliente_LostFocus
    'For i = 1 To mfgAberto.Rows - 1
    'If mfgAberto.TextMatrix(i, 1) = txtNumNota Then
    'mfgAberto.Row = i
    'mfgAberto_dblClick
    'Exit Sub
    'End If
    'Next
    'For i = 1 To mfgFinalizado.Rows - 1
    'If mfgFinalizado.TextMatrix(i, 1) = txtNumNota Then
    'mfgFinalizado.Row = i
    'mfgFinalizado_dblClick
    'Exit Sub
    'End If
    'Next
    'Else
    'MsgBox "N�mero do atendimento n�o encontrado.", vbInformation, "Aten��o"
    'End If
    
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub txtNumNota_LostFocus" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub txtRazao_Social_DblClick()
    frmClientes.Show
    StayOnTop frmClientes
End Sub


Sub Tempo()
    Static running As Boolean
    Dim pause As Long

    If running Then
        ' Stop the timer.
        KillTimer 0, TimerID
    Else
        ' Start the timer.
        pause = CLng(1800000)
        TimerID = SetTimer(0, 0, pause, AddressOf MyTimer)
    End If
    running = Not running
    
End Sub
' Kill the timer if it is running.
Private Sub Form_Unload(Cancel As Integer)
    If TimerID <> 0 Then KillTimer 0, TimerID
    
    cmdSair_Click
    vVB_Generica_001.ExcluiBind vBanco
    
End Sub

Sub Limpar_Tab_Atendimento()

    On Error GoTo Trata_Erro

1         cmbAssunto.ListIndex = -1
2         cmbTopico.Clear
3         cmbDetalhe.Clear

          'lblAtendente.Caption = ""
4         cboAtendente.Tag = ""
5         cboAtendente.ListIndex = -1

6         optPendente.Enabled = True: optPendente.Value = True
7         optRetornado.Enabled = True
8         optLiberado.Enabled = True
9         optSolucionado.Enabled = True
          
10        optCliente.Enabled = True: optCliente.Value = True
11        optRepresentante.Enabled = True
12        optTelevendas.Enabled = True
13        optOutros.Enabled = True

14        txtComentarios.Enabled = True: txtComentarios = ""
          'Eduardo - 15/05/2006
15        LsvHistorico.ListItems.Clear


16        txtDeposito.Enabled = True: txtDeposito = ""
17        txtNota_Fiscal.Enabled = True: txtNota_Fiscal = ""
18        txtComissao.Enabled = True: txtComissao = ""
19        txtAbatimento.Enabled = True: txtAbatimento = ""
          
20        chkComplemento.Enabled = True: chkComplemento.Value = False
21        chkProcede.Enabled = True: chkProcede.Value = False
22        chkProrrogacao.Enabled = True: chkProrrogacao.Value = False
23        chkDevolucao.Enabled = True: chkDevolucao.Value = False
          
24        cmbAssunto.Enabled = True: cmbAssunto.ListIndex = -1
25        cmbTopico.Enabled = True: cmbTopico.ListIndex = -1
26        cmbDetalhe.Enabled = True: cmbDetalhe.ListIndex = -1
          
27        txtProcedimento.Enabled = True: txtProcedimento = ""
28        txtEmail_Para.Enabled = True: txtEmail_Para = ""
29        txtEmail_Copia.Enabled = True: txtEmail_Copia = ""
          
30        txtRetorno = 0
31        txtSolucao = 0
32        txtLiberacao = 0
          
33        lblNumAtendimento.Caption = ""

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Limpar_Tab_Atendimento" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Sub Status_Opt(Status As String)
    If Status = "P" Then
        optPendente.Enabled = True: optPendente.Value = True
        optRetornado.Enabled = True
        optLiberado.Enabled = True
        optSolucionado.Enabled = True

    ElseIf Status = "R" Then
        optPendente.Enabled = False
        optRetornado.Enabled = True: optRetornado.Value = True
        optLiberado.Enabled = True
        optSolucionado.Enabled = True
        
    ElseIf Status = "S" Then
        optPendente.Enabled = False
        optRetornado.Enabled = False
        optSolucionado.Enabled = True: optSolucionado.Value = True
        optLiberado.Enabled = True
        
    ElseIf Status = "L" Then
        optPendente.Enabled = False
        optRetornado.Enabled = False
        optSolucionado.Enabled = False
        optLiberado.Enabled = True: optLiberado.Value = True
    End If
    txtRetorno_Change
    txtSolucao_Change
    txtLiberacao_Change
End Sub

Private Sub txtRetorno_Change()
    optRetornado.Enabled = (Val(txtRetorno) <> 0 And vPosicao <= 2)
End Sub

Private Sub txtSolucao_Change()
    optSolucionado.Enabled = (Not Val(txtSolucao) = 0 And vPosicao <= 3)
End Sub


Sub Inserir_Nome_Atendente_Descricao()
    If Me.cboAtendente = "" Then cboAtendente = vAtendente
        
    'Eduardo - 15/05/2006
    Exibir_Historico
        
    Dim vConsultar As String
    vConsultar = UCase(vAtendente) & " - " & Format(Now, "DD/MM/YY")
    
    'Elisson SDS 2275
    'If InStr(1, UCase(txtComentarios), vConsultar) <= 0 Then
       'txtComentarios = txtComentarios & vbCrLf & vConsultar
    'End If
    
End Sub

Sub Preenche_Grid_Finalizado(rst As Object, ByVal Registros As Integer)
          
    On Error GoTo Trata_Erro
          
1         Call Formatar_Grid_Finalizado

2         mfgFinalizado.Rows = Registros + 1
          
3         For i = 1 To Registros
4             With mfgFinalizado
5                 .TextMatrix(i, 0) = rst.Fields(0) & ""
6                 .col = 0
7                 .row = i
8                 Set mfgFinalizado.CellPicture = imgComentarios.Picture
9                 mfgFinalizado.CellPictureAlignment = 0
10                .TextMatrix(i, 1) = rst.Fields(1) & ""
11                .TextMatrix(i, 2) = rst.Fields(2) & ""
12                .TextMatrix(i, 3) = rst.Fields(3) & ""
13                .TextMatrix(i, 4) = rst.Fields(4) & ""
14                .TextMatrix(i, 5) = Trim(rst.Fields(5)) & ""
15                .TextMatrix(i, 6) = Trim(rst.Fields(6)) & ""
16                .TextMatrix(i, 7) = Trim(rst.Fields(7)) & ""
17                .TextMatrix(i, 8) = rst.Fields(8) & ""
18                .TextMatrix(i, 9) = rst.Fields(9) & ""
19            End With
20            rst.MoveNext
21        Next

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Preenche_Grid_Finalizado" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub
Sub Formatar_Grid_Finalizado()
    
    mfgFinalizado.Clear
    mfgFinalizado.Rows = 2
    
    mfgFinalizado.Cols = 10
    
    With mfgFinalizado
        .row = 0
        .ColWidth(0) = 1000
        .ColAlignment(0) = 1
        .TextMatrix(0, 0) = "DATA"
        
        .ColWidth(1) = 1100
        .ColAlignment(1) = 1
        .TextMatrix(0, 1) = "N�M.ATEND."
        
        .ColWidth(2) = 2000
        .ColAlignment(2) = 0
        .TextMatrix(0, 2) = "ATENDENTE"
        
        .ColWidth(3) = 1000
        .ColAlignment(3) = 1
        .TextMatrix(0, 3) = "DEP�SITO"
        
        .ColAlignment(4) = 4
        .ColWidth(4) = 1300
        .TextMatrix(0, 4) = "NOTA FISCAL"
        
        .ColWidth(5) = 1500
        .ColAlignment(5) = 1
        .TextMatrix(0, 5) = "ASSUNTO"
        
        .ColWidth(6) = 1500
        .ColAlignment(6) = 1
        .TextMatrix(0, 6) = "T�PICO"
        
        .ColWidth(7) = 1500
        .ColAlignment(7) = 1
        .TextMatrix(0, 7) = "DETALHE"
        
        .ColWidth(8) = 0
        .TextMatrix(0, 8) = "Situacao"
        
        .ColWidth(9) = 0
        .TextMatrix(0, 9) = "COMENTARIOS"
        
    End With
End Sub



Function BuscaUSERNAME() As String

    On Error GoTo TrataErro

    Dim sBuffer As String
    Dim lSize As Long

    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    GetUserName sBuffer, lSize

    If lSize > 0 Then
        BuscaUSERNAME = Left$(sBuffer, lSize - 1)
    End If
    Exit Function

TrataErro:
    BuscaUSERNAME = "Erro"

End Function

Sub Carregar_Email()
          
1         On Error GoTo Trata_Erro
          
          Dim vEmailDesatualizado As Boolean
          Dim vRstEmail As Object
          'Dim vrstData As Object
              
          Dim vrstData As Object
2         Set vrstData = vBanco.CreateDynaset("Select to_char(sysdate,'DD/MM/RR') data from dual", &O0)
              
3         Criar_Cursor vBanco.Parameters, "vCursor"
          
4         vBanco.Parameters.Remove "PM_COD_CLIENTE"
5         vBanco.Parameters.Add "PM_COD_CLIENTE", Val(txtCodigo_Cliente), 1
              
6         vSql = "Producao.PCK_VDA640.PR_SELECT_EMAIL_CLIENTE(:vCursor, :PM_COD_CLIENTE)"

7         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
          
8         lstEmail.Clear
          
9         Set vRstEmail = vBanco.Parameters("vCursor").Value
          
10        For i = 1 To vRstEmail.RecordCount
11            If Not IsNull(vRstEmail!Email) Then
12                lstEmail.AddItem IIf(IsNull(vRstEmail!Email), "", vRstEmail!Email) & Space(50) & vRstEmail!Descricao
              End If
13            If IsNull(vRstEmail!Dt_Email) Then
14               vEmailDesatualizado = True
15            Else
16               If DateAdd("m", -6, CDate(vrstData!Data)) > CDate(vRstEmail!Dt_Email) Then
17                  vEmailDesatualizado = True
18               End If
20            End If
21            vRstEmail.MoveNext
22        Next
          
23        If lstEmail.ListCount = 1 Then lstEmail.Selected(0) = True
              
24        If vEmailDesatualizado = True Then
25            txtEmailDesatualizado.Visible = True
26            txtEmailDesatualizado = "Confirme os Emails deste Cliente!"
27            PicEmailDesatualizado.Visible = True
28            lstEmail.ForeColor = vbRed
29        Else
30            txtEmailDesatualizado.Visible = False
32            PicEmailDesatualizado.Visible = False
33            lstEmail.ForeColor = vbBlack
34        End If
Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub Carregar_Email" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
37        End If
          
End Sub

Sub Preenche_Combo_Atendente()
          
    On Error GoTo Trata_Erro
          
1         vSql = "Producao.PCK_VDA640.Pr_Select_Atendente(:vCursor)"
          
2         If vVB_Generica_001.ExecutaPl(vBanco, vSql) <> "" Then
3             Call vVB_Generica_001.Informar("Erro no processamento da Pr_Select_Atendente, entre em contato com o SUPORTE!")
4             Exit Sub
5         Else
6             Set vObjOracle = vBanco.Parameters("VCURSOR").Value
7         End If
          
8         For i = 1 To vObjOracle.RecordCount
9             cboAtendente.AddItem vObjOracle.Fields(1).Value
10            cboAtendente.ItemData(cboAtendente.NewIndex) = vObjOracle.Fields(0).Value
11            vObjOracle.MoveNext
12        Next
          
13        vObjOracle.Close
14        Set vObjOracle = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Preenche_Combo_Atendente" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Function fDataDiasUteis(pDtInicio As Date, pNumDias As Byte) As Date

    Dim vDt As Date
    Dim vDtFim As Date
    Dim vDias As Byte
    Dim vDiaSemana As Byte
    
    vDtFim = DateAdd("d", pNumDias, pDtInicio)
    vDt = pDtInicio
    
    Do While vDt <= vDtFim
        
        vDiaSemana = Weekday(vDt)
        
        If vDiaSemana = 1 Then
            vDtFim = DateAdd("d", 1, vDtFim)
            vDias = vDias + 1
            
        ElseIf vDiaSemana = 7 Then
            vDtFim = DateAdd("d", 1, vDtFim)
            vDias = vDias + 1
        End If
        
        vDt = DateAdd("d", 1, vDt)
        
    Loop

    fDataDiasUteis = DateAdd("d", pNumDias + vDias, pDtInicio)

End Function
