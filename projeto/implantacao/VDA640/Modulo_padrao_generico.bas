Attribute VB_Name = "moduloPadrao"
Option Explicit

Public vNomeSistema As String
Public vDescricaoSistema As String
Public vResposta
Public vUsuarioRede As String
Public vCD As Integer
Public vTipoCD As String
Public vUsuarioBanco As String
Public vAnalistaResponsavel As String
Public vAnalistaBackup As String
Public vVB_Generica_001 As New clsVB_Generica_001
Public vVB_Venda_001 As New clsVB_Venda_001
Public vVB_PAAC_001 As New clsVB_PAAC_001
Public vVB_Cobranca_001 As New clsVB_Cobranca_001
Public vVB_Autolog_001 As New clsVB_Autolog_001
Public vErro As String
Public vSelect As String
Public vSql As String
Public i As Double
Public j As Double
Public Const COLORONCOLOR = 3
Public Const HALFTONE = 4
Public Const SW_SHOWMAXIMIZED = 3
Public Const SW_SHOWNORMAL = 1
Public Const NERR_BASE = 2100
Public Const NERR_InvalidComputer = (NERR_BASE + 251)
Public Const NERR_UseNotFound = (NERR_BASE + 150)
Public vFlCDDPK As Boolean
Public vOwners(10) As String
Public vCDConectado As Integer      'TABELA DEPOSITO
Public vTipoCDConectado As String   'TABELA DEPOSITO
Public vUsuario As Usuario
Public Const NERR_Success = 0
Public Const CP_ACP = 0
Public Const NORMAL_PRIORITY_CLASS = &H20&
Public Const INFINITE = -1&
Public Const VCONSTANTE = &H1
Public Const vCorDivergente As Long = &HC0C0FF
Public Const vCorNormal As Long = &H80000005
Public Const BIF_RETURNONLYFSDIRS = 1
Public Const MAX_PATH = 260
Public vFormPrincipal As Form
Public vUsuarioDialogConexao As String

Public vRetornoCodTransportadora As Double
Public vRetornoNomeTransportadora As String
Public vRetornoCodFornecedor As Double
Public vRetornoNomeFornecedor As String
Public vRetornoCodCliente As Double
Public vRetornoNomeCliente As String
Public vRetornoCodBanco As Double
Public vRetornoNomeBanco As String
Public vRetornoCodDPK As Double
Public vShell
Public vInformacoesUsuario As USER_INFO_3


Public Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (pDest As Any, pSource As Any, ByVal dwLength As Long)
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Sub CoTaskMemFree Lib "ole32.dll" (ByVal hMem As Long)
Public Declare Function SHGetPathFromIDList Lib "shell32" (ByVal pidList As Long, ByVal lpBuffer As String) As Long
Public Declare Function lSTRCat Lib "kernel32" Alias "lstrcatA" (ByVal lpString1 As String, ByVal lpString2 As String) As Long
Public Declare Function SHBrowseForFolder Lib "shell32" (lpbi As BrowseInfo) As Long
Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Long
Public Declare Function SetStretchBltMode Lib "gdi32" (ByVal hdc As Long, ByVal nStretchMode As Long) As Long
Public Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long
Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Declare Function NetUserGetInfo Lib "netapi32" (lpServer As Any, UserName As Byte, ByVal Level As Long, lpBuffer As Long) As Long
Public Declare Function NetApiBufferFree Lib "netapi32" (ByVal buffer As Long) As Long
Public Declare Function lstrlenW Lib "kernel32" (lpString As Any) As Long
Public Declare Function WideCharToMultiByte Lib "kernel32" (ByVal CodePage As Long, ByVal dwFlags As Long, lpWideCharStr As Any, ByVal cchWideChar As Long, lpMultiByteStr As Any, ByVal cchMultiByte As Long, ByVal lpDefaultChar As String, ByVal lpUsedDefaultChar As Long) As Long
Public Declare Function CreateProcessA Lib "kernel32" (ByVal lpApplicationName As String, ByVal lpCommandLine As String, ByVal lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, ByVal lpCurrentDirectory As String, lpStartupInfo As STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) As Long
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)
Public Declare Function lOpen Lib "kernel32" Alias "_lopen" (ByVal lpPathName As String, ByVal iReadWrite As Long) As Long
Public Declare Function lClose Lib "kernel32" Alias "_lclose" (ByVal hFile As Long) As Long
Public Declare Function MessageBox Lib "user32" Alias "MessageBoxA" (ByVal hwnd As Long, ByVal lpText As String, ByVal lpCaption As String, ByVal wType As Long) As Long
Public Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As Long, lpExitCode As Long) As Long


Public Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type

Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Type POINTAPI
    x As Long
    y As Long
End Type

Public Type BrowseInfo
    hwndOwner As Long
    pIDLRoot As Long
    pszDisplayName As Long
    lpszTitle As Long
    ulFlags As Long
    lpfnCallback As Long
    lParam As Long
    iImage As Long
End Type

Public Type Usuario
    Login As String
    Senha As String
    Permissao As Double
    Cod_Usuario As Double
    Nome_Usuario As String
    Tipo_Usuario As String
    Tipo_CD As String
    Loja As String
    Cod_Representante As Double
    Tipo_Informe As String
    Fl_Informe As String
    Tipo_Representante As String
    Email As String
    Perfil As String
End Type

Private Type USER_INFO_3
    usri3_name As Long
    usri3_password As Long
    usri3_password_age As Long
    usri3_priv As Long
    usri3_home_dir As Long
    usri3_comment As Long
    usri3_flags As Long
    usri3_script_path As Long
    usri3_auth_flags As Long
    usri3_full_name As Long
    usri3_usr_comment As Long
    usri3_parms As Long
    usri3_workstations As Long
    usri3_last_logon As Long
    usri3_last_logoff As Long
    usri3_acct_expires As Long
    usri3_max_storage As Long
    usri3_units_per_week As Long
    usri3_logon_hours As Byte
    usri3_bad_pw_count As Long
    usri3_num_logons As Long
    usri3_logon_server As String
    usri3_country_code As Long
    usri3_code_page As Long
    usri3_user_id As Long
    usri3_primary_group_id As Long
    usri3_profile As Long
    usri3_home_dir_drive As Long
    usri3_password_expired As Long
End Type

Public Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessID As Long
    dwThreadID As Long
End Type

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Public Function BrowseForFolder(hwndOwner As Long, sPrompt As String) As String

    Dim iNull As Integer
    Dim lpIDList As Long
    Dim lResult As Long
    Dim sPath As String
    Dim udtBI As BrowseInfo

    With udtBI

        .hwndOwner = hwndOwner
        .lpszTitle = lSTRCat(sPrompt, "")
        .ulFlags = BIF_RETURNONLYFSDIRS

    End With

    lpIDList = SHBrowseForFolder(udtBI)

    If lpIDList Then

        sPath = String$(MAX_PATH, 0)
        lResult = SHGetPathFromIDList(lpIDList, sPath)
        Call CoTaskMemFree(lpIDList)
        iNull = InStr(sPath, vbNullChar)
        If iNull Then sPath = Left$(sPath, iNull - 1)

    End If

    BrowseForFolder = sPath

End Function

Sub Aguardar()

    frmAguardar.Show
    frmAguardar.Refresh

End Sub

Function LerINI(ByVal pArquivo As String, ByVal pSecao As String, ByVal pChave As String)
    
    Dim RetVal As String, Worked As Integer
    
    If Dir(pArquivo) = "" Then
    
        Call Informar(pArquivo & " n�o encontrado")
        Exit Function
    
    End If
    
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(pSecao, pChave, "", RetVal, Len(RetVal), pArquivo)
    
    If Worked = 0 Then
        
        LerINI = ""
    
    Else
        
        LerINI = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    
    End If

End Function

Function GravarINI(ByVal pArquivo As String, ByVal pSecao As String, ByVal pChave As String, ByVal pValor As String) As Integer
    
    WritePrivateProfileString pSecao, pChave, pValor, pArquivo
    GravarINI = 1

End Function

' C�digo comentado por Gustavo Scalzilli - 26/06/2009
' Motivo: somente 1 dos arquivos padr�es encontrados na rede utilizava esse processo. Os demais sempre apresentaram erro ao referenciar tal Sub.
'         Estamos a espera da utiliza��o para sabermos como proceder para sua libera��o.
' Sub ProcessaErro()
'
'    Screen.MousePointer = vbDefault
'
'    If Err.Number <> 0 Then
'
'        frmErro.Caption = "Aten��o"
'        frmErro.pnlErro.Text = "Ocorreu o erro: " & CStr(Err.Number) & " - " & CStr(Err.Description)
'        Unload frmAguarde
'        frmErro.Show 1
'
'    End If
'
' End Sub

Sub Informar(pMensagem As String)
    
    MsgBox pMensagem, vbInformation + vbOKOnly, "Aten��o"

End Sub
Sub Perguntar(pMensagem As String)
    
    vResposta = MsgBox(pMensagem, vbExclamation + vbYesNo, "Aten��o")

End Sub
