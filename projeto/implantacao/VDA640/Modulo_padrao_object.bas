Attribute VB_Name = "modPadrao_object"
Option Explicit

Public vSessao As Object        'Vari�vel de sess�o do Oracle
Public vBanco As Object         'Vari�vel de banco do Oracle
Public vNomeBanco As String     'Nome do banco do arquivo .ini
Public vNomeFantasia As String  'Nome fantasia do CD conectado

Public vLinhaArquivo
Public vObjOracle As Object
Public vUsuarioBanco As String

Sub ConectaOracle(pUsuarioSenha As String, Optional pStatusBar As Boolean, Optional pForm As Form)

On Error GoTo Erro
    
    Dim vArquivoINI As String
    
    vArquivoINI = Dir("C:\" & App.Title & ".ini")
    
    If vArquivoINI = "" Then
    
        'CRIAR ARQUIVO INI
        vArquivoINI = "C:\" & App.Title & ".ini"
        Call GravarINI(vArquivoINI, "CONEXAO", "BANCO", "PRODUCAO")
        Call GravarINI(vArquivoINI, "LOG", "CAMINHO1", "")
        Call GravarINI(vArquivoINI, "IMPRESSORA", "TIPO", "ZEBRA")
    
    End If
    
    'LER ARQUIVO INI
    vArquivoINI = "C:\" & App.Title & ".ini"
    vNomeBanco = LerINI(vArquivoINI, "CONEXAO", "BANCO")
    
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    Set vBanco = vSessao.OpenDatabase(vNomeBanco, pUsuarioSenha, 0&)
    
    If pStatusBar = True Then
    
        vUsuarioBanco = Mid(pUsuarioSenha, 1, InStr(1, pUsuarioSenha, "/") - 1)
        pForm.stbBarra.Panels(2) = vUsuarioBanco
    
    End If
    
    Exit Sub

Erro:

     MsgBox "Sub ConectaOralce" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, vbCritical, "Aten��o"

    'Call ProcessaErro

End Sub

Sub Sair()

    Call Perguntar("Deseja sair do sistema")
    
    If vResposta = vbYes Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If
        
End Sub

Sub DefinirTelaSobre()

    vNomeSistema = UCase(App.Title)
    vDescricaoSistema = vObjOracle.Fields(0)
    vAnalistaResponsavel = vObjOracle.Fields(1)
    vAnalistaBackup = vObjOracle.Fields(2)
    
    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)
    
End Sub
