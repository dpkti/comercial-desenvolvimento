Attribute VB_Name = "Module1"
Global dbOra  As Object         'Para Abrir o Bco.
Global db As Object             'Database
Global db_CONS As Object        'Variavel Bco. de Consulta
Global Ora_Consulta As Object
Global cod_errora As Integer    'Cod. Erro PL-SQL
Global txt_errora As String     'Texto Erro PL-SQL
Global CmdSql As String         'String para o SQL
Global CmdPl As String          'String para o PL-SQL

Sub Data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub



