VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmVolta 
   Caption         =   "Volta Posi��o do Pedido"
   ClientHeight    =   4140
   ClientLeft      =   2700
   ClientTop       =   1770
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4140
   ScaleWidth      =   6690
   Begin VB.TextBox txtSequencia 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4710
      MaxLength       =   1
      TabIndex        =   7
      Top             =   1920
      Width           =   255
   End
   Begin VB.TextBox txtPedido 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3270
      MaxLength       =   7
      TabIndex        =   6
      Top             =   1920
      Width           =   1215
   End
   Begin VB.TextBox txtLoja 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3270
      MaxLength       =   2
      TabIndex        =   4
      Top             =   1440
      Width           =   495
   End
   Begin Threed.SSFrame SSFrame1 
      Height          =   855
      Left            =   855
      TabIndex        =   0
      Top             =   240
      Width           =   4965
      _Version        =   65536
      _ExtentX        =   8758
      _ExtentY        =   1508
      _StockProps     =   14
      Caption         =   "Tipo de Pedido"
      ForeColor       =   255
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSOption SSOption3 
         Height          =   375
         Left            =   3915
         TabIndex        =   3
         Top             =   360
         Width           =   825
         _Version        =   65536
         _ExtentX        =   1455
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "VDR"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSOption SSOption2 
         Height          =   375
         Left            =   2295
         TabIndex        =   2
         Top             =   360
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Digita��o"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSOption SSOption1 
         Height          =   375
         Left            =   315
         TabIndex        =   1
         Top             =   360
         Width           =   1695
         _Version        =   65536
         _ExtentX        =   2990
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Telemarketing"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSGrid.Grid GrdVDR 
      Height          =   1215
      Left            =   855
      TabIndex        =   12
      Top             =   1440
      Visible         =   0   'False
      Width           =   4950
      _Version        =   65536
      _ExtentX        =   8731
      _ExtentY        =   2143
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSGrid.Grid Grid1 
      Height          =   1215
      Left            =   870
      TabIndex        =   11
      Top             =   1440
      Visible         =   0   'False
      Width           =   4935
      _Version        =   65536
      _ExtentX        =   8705
      _ExtentY        =   2143
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   615
      Left            =   3555
      TabIndex        =   9
      Top             =   3015
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   1085
      _StockProps     =   78
      Caption         =   "Sair"
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   615
      Left            =   1890
      TabIndex        =   8
      Top             =   3000
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   1085
      _StockProps     =   78
      Caption         =   "Altera Posi��o"
   End
   Begin VB.Label Label1 
      Caption         =   "PEDIDO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1950
      TabIndex        =   10
      Top             =   1920
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "LOJA"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1950
      TabIndex        =   5
      Top             =   1440
      Width           =   855
   End
End
Attribute VB_Name = "frmVolta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SSTab1_DblClick()

End Sub


Private Sub Form_Load()
    SSOption2.Value = True
   'Conexao Access
    Set dbaccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")
    
    Set dbaccess1 = OpenDatabase(Path_drv & "\DADOS\" & "DIGITA.MDB")
    
    Set DBACCESS2 = OpenDatabase(Path_drv & "\DADOS\" & "VDR.MDB")

    txtLoja = 1
    txtSequencia = 0
    Label2.Visible = True
    Label1.Visible = True
    txtLoja.Visible = True
    txtPedido.Visible = True
    txtSequencia.Visible = True
    Grid1.Visible = False
    GrdVDR.Visible = False
    SSCommand1.Visible = True
  
End Sub

Private Sub Form_Terminate()
    Call SSCommand1_Click
End Sub


Private Sub GrdVDR_DblClick()
   Dim SQL As String
   
   GrdVDR.Col = 0
   lngCod_loja = GrdVDR.Text
   GrdVDR.Col = 1
   lngNum_Pedido = GrdVDR.Text
   GrdVDR.Col = 2
   lngSeq_Pedido = GrdVDR.Text
      
    SQL = "Update pedido set"
    SQL = SQL & " fl_pedido_finalizado = 'N'"
    SQL = SQL & " Where cod_Loja =" & lngCod_loja
    SQL = SQL & " and seq_pedido =" & lngSeq_Pedido
    SQL = SQL & " and num_pendente =" & lngNum_Pedido
      
    DBACCESS2.Execute SQL
    FreeLocks
    
    MsgBox "Pedido alterado, favor liberar novamente", vbInformation, "Aten��o"
    Call SSOption3_Click(1)
End Sub

Private Sub Grid1_DblClick()
   Dim SQL As String
   
   Grid1.Col = 0
   lngCod_loja = Grid1.Text
   Grid1.Col = 1
   lngNum_Pedido = Grid1.Text
   Grid1.Col = 2
   lngSeq_Pedido = Grid1.Text
      
    SQL = "Update pedido set"
    SQL = SQL & " fl_pedido_finalizado = 'N'"
    SQL = SQL & " Where cod_Loja =" & lngCod_loja
    SQL = SQL & " and seq_pedido =" & lngSeq_Pedido
    SQL = SQL & " and num_pendente =" & lngNum_Pedido
      
    dbaccess.Execute SQL
    FreeLocks
    
    MsgBox "Pedido alterado, favor liberar novamente", vbInformation, "Aten��o"
    Call SSOption1_Click(1)
    
End Sub

Private Sub SSCommand1_Click()
    Dim SQL As String
    Dim ss As Snapshot
    
    If txtLoja = "" Then
        MsgBox "Selecione uma loja para fazer a consulta", vbInformation, "Aten��o"
        Exit Sub
    ElseIf txtPedido = "" Then
        MsgBox "Selecione um pedido para fazer a consulta", vbInformation, "Aten��o"
        Exit Sub
    ElseIf txtSequencia = "" Then
        MsgBox "Selecione uma sequencia para fazer a consulta", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If SSOption2.Value = True Then
        SQL = "Select num_pendente"
        SQL = SQL & " From pedido"
        SQL = SQL & " Where fl_pedido_finalizado='T'"
        SQL = SQL & " and cod_loja = " & txtLoja
        SQL = SQL & " and num_pendente = " & txtPedido
        SQL = SQL & " and seq_pedido = " & txtSequencia
      
        Set ss = dbaccess1.CreateSnapshot(SQL)
        FreeLocks
      
        If ss.EOF And ss.BOF Then
            MsgBox "Pedido n�o localizado", vbInformation, "Aten��o"
            txtPedido.Text = ""
            Exit Sub
        Else
                  SQL = "Update pedido set"
            SQL = SQL & " fl_pedido_finalizado = 'N'"
            SQL = SQL & " Where cod_Loja =" & txtLoja
            SQL = SQL & " and seq_pedido =" & txtSequencia
            SQL = SQL & " and num_pendente =" & txtPedido
            
            dbaccess1.Execute SQL
            FreeLocks
            MsgBox "Pedido alterado, favor liberar novamente", vbInformation, "Aten��o"
            txtPedido.Text = ""
        End If
        ss.Close
    End If
  
End Sub

Private Sub SSCommand2_Click()
  dbaccess.Close
  dbaccess1.Close
  DBACCESS2.Close
  Unload Me
End Sub


Private Sub SSOption1_Click(Value As Integer)
  Dim SQL  As String
  Dim ss As Snapshot
  Dim i As Long
 
  Label2.Visible = False
  Label1.Visible = False
  txtLoja.Visible = False
  txtPedido.Visible = False
  txtSequencia.Visible = False
  SSCommand1.Visible = False
  GrdVDR.Visible = False
  
  SQL = "Select num_pendente,"
  SQL = SQL & " seq_pedido,"
  SQL = SQL & " cod_Loja,"
  SQL = SQL & " cod_cliente,"
  SQL = SQL & " dt_digitacao,"
  SQL = SQL & " qtd_itens"
  SQL = SQL & " From pedido"
  SQL = SQL & " Where fl_pedido_finalizado='T'"
  
  Set ss = dbaccess.CreateSnapshot(SQL)
  FreeLocks
  
  If ss.EOF And ss.BOF Then
    MsgBox "N�o h� pedido de telemarketing para ser alterado", vbInformation, "Aten��o"
    Grid1.Visible = False
    SSOption1.Value = 0
    SSOption2.Value = 1
    SSOption3.Value = 0
    Call SSOption2_Click(1)
    Exit Sub
  Else
      'carrega dados
    ss.MoveLast
    ss.MoveFirst
    With Grid1
        .Cols = 6
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 400
        .ColWidth(1) = 900
        .ColWidth(2) = 500
        .ColWidth(3) = 800
        .ColWidth(4) = 1000
        .ColWidth(5) = 800
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "Pedido"
        .Col = 2
        .Text = "Seq."
        .Col = 3
        .Text = "Cliente"
        .Col = 4
        .Text = "Dt.Dig."
        .Col = 5
        .Text = "Qt.Itens"

        ss.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!cod_loja
            .Col = 1
            .Text = ss!num_pendente
            .Col = 2
            .Text = ss!seq_pedido
            .Col = 3
            .Text = ss!cod_cliente
            .Col = 4
            If IsNull(ss!DT_DIGITACAO) Then
              .Text = ""
            Else
              .Text = ss!DT_DIGITACAO
            End If
            .Col = 5
            .Text = ss!qtd_itens
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    ss.Close
    Grid1.Visible = True
  End If
  
  
  
End Sub

Private Sub SSOption2_Click(Value As Integer)
  Label2.Visible = True
  Label1.Visible = True
  txtLoja.Visible = True
  txtPedido.Visible = True
  txtSequencia.Visible = True
  Grid1.Visible = False
  GrdVDR.Visible = False
  SSCommand1.Visible = True
End Sub


Private Sub SSOption3_Click(Value As Integer)
    Dim SQL  As String
    Dim ss As Snapshot
    Dim i As Long
 
    Label2.Visible = False
    Label1.Visible = False
    txtLoja.Visible = False
    txtPedido.Visible = False
    txtSequencia.Visible = False
    SSCommand1.Visible = False
    Grid1.Visible = False
  
    SQL = "Select num_pendente,"
    SQL = SQL & " seq_pedido,"
    SQL = SQL & " cod_Loja,"
    SQL = SQL & " cod_cliente,"
    SQL = SQL & " dt_digitacao,"
    SQL = SQL & " qtd_itens"
    SQL = SQL & " From pedido"
    SQL = SQL & " Where fl_pedido_finalizado='T'"
  
    Set ss = DBACCESS2.CreateSnapshot(SQL)
    FreeLocks
  
    If ss.EOF And ss.BOF Then
        MsgBox "N�o h� pedido de VDR para ser alterado", vbInformation, "Aten��o"
        GrdVDR.Visible = False
        SSOption1.Value = 0
        SSOption2.Value = 1
        SSOption3.Value = 0
        Call SSOption2_Click(1)
        Exit Sub
    Else
       'carrega dados
        ss.MoveLast
        ss.MoveFirst
        With GrdVDR
            .Cols = 6
            .Rows = ss.RecordCount + 1
            .ColWidth(0) = 400
            .ColWidth(1) = 900
            .ColWidth(2) = 500
            .ColWidth(3) = 800
            .ColWidth(4) = 1000
            .ColWidth(5) = 800
        
            .Row = 0
            .Col = 0
            .Text = "Loja"
            .Col = 1
            .Text = "Pedido"
            .Col = 2
            .Text = "Seq."
            .Col = 3
            .Text = "Cliente"
            .Col = 4
            .Text = "Dt.Dig."
            .Col = 5
            .Text = "Qt.Itens"
        
            ss.MoveFirst
            For i = 1 To .Rows - 1
                .Row = i
                
                .Col = 0
                .Text = ss!cod_loja
                .Col = 1
                .Text = ss!num_pendente
                .Col = 2
                .Text = ss!seq_pedido
                .Col = 3
                .Text = ss!cod_cliente
                .Col = 4
                If IsNull(ss!DT_DIGITACAO) Then
                    .Text = ""
                Else
                    .Text = ss!DT_DIGITACAO
                End If
                .Col = 5
                .Text = ss!qtd_itens
                
                ss.MoveNext
            Next
            .Row = 1
        End With
    
        ss.Close
        GrdVDR.Visible = True
    End If

End Sub

Private Sub txtLoja_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtPedido_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtSequencia_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


