VERSION 5.00
Begin VB.Form frmTransferencia 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Transfer�ncia "
   ClientHeight    =   1665
   ClientLeft      =   2730
   ClientTop       =   2985
   ClientWidth     =   4035
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1665
   ScaleWidth      =   4035
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton CmdSair 
      Caption         =   "Sair"
      Height          =   435
      Left            =   2130
      TabIndex        =   3
      Top             =   1140
      Width           =   645
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   435
      Left            =   1110
      TabIndex        =   2
      Top             =   1140
      Width           =   645
   End
   Begin VB.ComboBox cboDepTransfer 
      ForeColor       =   &H00800000&
      Height          =   315
      ItemData        =   "frmTrans.frx":0000
      Left            =   930
      List            =   "frmTrans.frx":0002
      TabIndex        =   1
      Top             =   600
      Width           =   2085
   End
   Begin VB.Label lblTransf 
      Caption         =   "Escolha o dep�sito para finalizar a transfer�ncia:"
      ForeColor       =   &H00800000&
      Height          =   225
      Left            =   150
      TabIndex        =   0
      Top             =   180
      Width           =   3735
   End
End
Attribute VB_Name = "frmTransferencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cboDepTransfer_Click()
    If Mid(cboDepTransfer.Text, 1, 2) = Mid(frmVisPedido.lblDeposito(17).Caption, 1, 2) Then
        MsgBox "O dep�sito de transfer�ncia deve ser diferente do escolhido para o pedido."
        cboDepTransfer.SetFocus
    End If
End Sub


Private Sub cmdOK_Click()
    Dim vRst_Transf As Recordset
    
    If Val(cboDepTransfer.Text) = Val(frmVisPedido.lblDeposito(17).Caption) Then
        MsgBox "O dep�sito de transfer�ncia deve ser diferente do escolhido para o pedido."
        cboDepTransfer.SetFocus
        Exit Sub
    End If
    
    Set vRst_Transf = dbAccess2.CreateSnapshot("Select * from transferencia where cod_dep_origem = " & Val(cboDepTransfer) & " and (cod_dep_destino=" & Val(frmVisPedido.lblDeposito(17).Caption) & " OR COD_DEP_DESTINO = 99)")
            
    If vRst_Transf.RecordCount > 0 Then
        MsgBox "Devido a restri��o Fiscal, n�o � poss�vel efetuar transfer�ncia de " & cboDepTransfer & " para " & frmVisPedido.lblDeposito(17).Caption, vbInformation, "Aten��o"
        cboDepTransfer.ListIndex = -1
        Set vRst_Transf = Nothing
        Exit Sub
    End If
    Set vRst_Transf = Nothing
    
    DepTransf = Val(cboDepTransfer.Text)
    Unload Me
    
End Sub


Private Sub cmdSair_Click()
    DepTransf = 0
    Unload Me
End Sub

Private Sub Form_Load()
    
    Dim ss2 As Snapshot
    
   'Combo de Transfer�ncia
   'carrega deposito default e depositos "visao"
           SQL2 = "Select distinct(a.cod_loja) as cod_loja,b.nome_fantasia as nome_fantasia"
    SQL2 = SQL2 & " from deposito_visao a, loja b, R_UF_DEPOSITO c"
    SQL2 = SQL2 & " where a.cod_loja=b.cod_loja and a.nome_programa = 'FIL010' and "
    SQL2 = SQL2 & " a.cod_loja <> c.cod_loja "

    Set ss2 = dbAccess2.CreateSnapshot(SQL2)
    FreeLocks

    If ss2.EOF Then
        MsgBox "Tabela DEPOSITO_VISAO est� vazia ligue para Depto.Sistemas", vbExclamation, "Aten��o"
        ss2.Close
        Exit Sub
    End If

    ss2.MoveLast
    ss2.MoveFirst

    For i = 1 To ss2.RecordCount
        cboDepTransfer.AddItem Format(ss2!cod_loja, "00") & "-" & ss2!nome_fantasia
        ss2.MoveNext
    Next

    cboDepTransfer.Text = deposito_default
End Sub
