Attribute VB_Name = "Module1"
Public dbaccess As Database
Public SQL As String
Public path_drv As String
Public SS As Snapshot
    

Sub IniciaGrid2()
    With frmItens.grdItem
        
        .Cols = 17
        .Row = 0
        .RowHeight(0) = 400
        
        .Col = 0: .Text = "ITEM": .ColWidth(0) = 500: .FixedAlignment(0) = 2
        
        .Col = 1: .Text = "SIT.": .ColWidth(1) = 500:  .FixedAlignment(1) = 2
        .ColAlignment(1) = 2
        
        .Col = 2: .Text = "TRIB.": .ColWidth(2) = 500:  .FixedAlignment(2) = 2
        .ColAlignment(2) = 2
        
        .Col = 3: .Text = "FORN.": .ColWidth(3) = 600: .FixedAlignment(3) = 2
        .ColAlignment(3) = 2
        
        .Col = 4: .Text = "F�BR.": .ColWidth(4) = 1200:  .FixedAlignment(4) = 2
        .ColAlignment(4) = 2
        
        .Col = 5: .Text = "QTD. SOLIC.": .ColWidth(5) = 700:  .FixedAlignment(5) = 2
        .ColAlignment(5) = 2
        
        .Col = 6: .Text = "QTD. ATEND.": .ColWidth(6) = 700: .FixedAlignment(6) = 2
        .ColAlignment(6) = 2
        
        .Col = 7: .Text = "PRE�O UNIT�RIO": .ColWidth(7) = 900:  .FixedAlignment(7) = 2
        .ColAlignment(7) = 2
        
        .Col = 8: .Text = "% PER.": .ColWidth(8) = 500:  .FixedAlignment(8) = 2
        .ColAlignment(8) = 2
        
        .Col = 9: .Text = "% ADIC.": .ColWidth(9) = 500: .FixedAlignment(9) = 2
        .ColAlignment(9) = 2
        
        .Col = 10: .Text = "% UF": .ColWidth(10) = 500:  .FixedAlignment(10) = 2
        .ColAlignment(10) = 2
        
        .Col = 11: .Text = "PRE�O L�QUIDO": .ColWidth(11) = 900:  .FixedAlignment(11) = 2
        .ColAlignment(11) = 2
        
        .Col = 12: .Text = "VALOR L�QUIDO": .ColWidth(12) = 900: .FixedAlignment(12) = 2
        .ColAlignment(12) = 2
        
        .Col = 13: .Text = "TABELA": .ColWidth(13) = 700:  .FixedAlignment(13) = 2
        .ColAlignment(13) = 2
        
        .Col = 14: .Text = "% IPI": .ColWidth(14) = 500:  .FixedAlignment(14) = 2
        .ColAlignment(14) = 2
        
        .Col = 15: .Text = "% COM": .ColWidth(15) = 500: .FixedAlignment(15) = 2
        .ColAlignment(15) = 2
        
        .Col = 16: .Text = "DPK": .ColWidth(16) = 900: .FixedAlignment(16) = 2
        .ColAlignment(16) = 2
        
        .FixedRows = 1
        
    End With
End Sub

Sub LimpaGrid()
    With frmTrans.grdConsulta
        .Rows = 2
        .Row = 1
        For I = 0 To .Cols - 1
            .Col = I: .Text = ""
        Next I
    End With
    
    Call IniciaGrid
    
End Sub

Sub IniciaGrid()
    With frmTrans.grdConsulta
        
        .Cols = 8
        .Row = 0
        .RowHeight(0) = 400
        
        .Col = 0: .Text = "N�MERO TEMP.": .ColWidth(0) = 950: .FixedAlignment(0) = 2
        
        .Col = 1: .Text = "LOJA COT.": .ColWidth(1) = 800:  .FixedAlignment(1) = 2
        .ColAlignment(1) = 2
        
        .Col = 2: .Text = "COTA��O": .ColWidth(2) = 950:  .FixedAlignment(2) = 2
        .ColAlignment(2) = 2
        
        .Col = 3: .Text = "LOJA TRANSF.": .ColWidth(3) = 800:  .FixedAlignment(3) = 2
        .ColAlignment(3) = 2
        
        .Col = 4: .Text = "PEDIDO TRANSF.": .ColWidth(4) = 1150: .FixedAlignment(4) = 2
        .ColAlignment(4) = 2
        
        .Col = 5: .Text = "LOJA VENDA": .ColWidth(5) = 800:  .FixedAlignment(5) = 2
        .ColAlignment(5) = 2
        
        .Col = 6: .Text = "PEDIDO VENDA": .ColWidth(6) = 1150: .FixedAlignment(6) = 2
        .ColAlignment(6) = 2
        
        .Col = 7: .Text = "VALOR TOTAL": .ColWidth(7) = 1000: .FixedAlignment(7) = 2
        .ColAlignment(7) = 1
        
        .FixedRows = 1
        
    End With

End Sub

Sub LimpaGrid2()
    With frmItens.grdItem
        .Rows = 2
        .Row = 1
        For I = 0 To .Cols - 1
            .Col = I: .Text = ""
        Next I
    End With
    
    Call IniciaGrid2
    
End Sub


