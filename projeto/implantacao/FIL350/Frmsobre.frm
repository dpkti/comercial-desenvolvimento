VERSION 4.00
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SOBRE"
   ClientHeight    =   3225
   ClientLeft      =   2715
   ClientTop       =   2550
   ClientWidth     =   6450
   ClipControls    =   0   'False
   Height          =   3630
   Icon            =   "Frmsobre.frx":0000
   Left            =   2655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3225
   ScaleWidth      =   6450
   Top             =   2205
   Width           =   6570
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   2730
      Width           =   975
   End
   Begin VB.Image Image1 
      Height          =   1380
      Left            =   2580
      Picture         =   "Frmsobre.frx":0442
      Top             =   45
      Width           =   1650
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "   Responsável:  Mariangela      SUPORTE (suppaac@dpk.com.br)"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   495
      Left            =   1695
      TabIndex        =   1
      Top             =   2070
      Width           =   3135
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Sistema: FIL350 - Consulta Pedidos de Transferência"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   345
      Left            =   585
      TabIndex        =   0
      Top             =   1575
      Width           =   5370
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_Creatable = False
Attribute VB_Exposed = False



Option Explicit

Private Sub cmdOK_Click()
  Unload Me
End Sub


