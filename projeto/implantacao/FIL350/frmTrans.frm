VERSION 4.00
Begin VB.Form frmTrans 
   Caption         =   "Consulta Pedidos de Transfer�ncia"
   ClientHeight    =   5700
   ClientLeft      =   495
   ClientTop       =   720
   ClientWidth     =   8655
   Height          =   6105
   Icon            =   "frmTrans.frx":0000
   Left            =   435
   LinkTopic       =   "Form1"
   ScaleHeight     =   5700
   ScaleWidth      =   8655
   Top             =   375
   Width           =   8775
   Begin VB.TextBox txtVlTotal 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      ForeColor       =   &H00000080&
      Height          =   315
      Left            =   7560
      TabIndex        =   8
      Top             =   5235
      Width           =   945
   End
   Begin VB.TextBox txtPedidos 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      ForeColor       =   &H00000080&
      Height          =   315
      Left            =   7920
      TabIndex        =   6
      Top             =   4830
      Width           =   585
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   675
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      _Version        =   65536
      _ExtentX        =   15266
      _ExtentY        =   1191
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand sscmdSobre 
         Height          =   555
         Left            =   1980
         TabIndex        =   3
         Top             =   45
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmTrans.frx":0442
      End
      Begin Threed.SSCommand sscmdSair 
         Height          =   555
         Left            =   840
         TabIndex        =   2
         Top             =   45
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmTrans.frx":0894
      End
      Begin Threed.SSCommand sscmdConsulta 
         Height          =   555
         Left            =   180
         TabIndex        =   1
         Top             =   45
         Width           =   555
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "frmTrans.frx":0BAE
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Valor Total dos Pedidos de Transfer�ncia (sem pedido de venda):"
      ForeColor       =   &H00800000&
      Height          =   225
      Left            =   2760
      TabIndex        =   7
      Top             =   5310
      Width           =   4755
   End
   Begin VB.Label Label1 
      Caption         =   "Quantidade de Pedidos de Transfer�ncia:"
      ForeColor       =   &H00800000&
      Height          =   225
      Left            =   4440
      TabIndex        =   5
      Top             =   4920
      Width           =   3075
   End
   Begin MSGrid.Grid grdConsulta 
      Height          =   3945
      Left            =   90
      TabIndex        =   4
      Top             =   765
      Width           =   8430
      _Version        =   65536
      _ExtentX        =   14870
      _ExtentY        =   6959
      _StockProps     =   77
      BackColor       =   16777215
   End
End
Attribute VB_Name = "frmTrans"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Toolbar1_ButtonClick(ByVal Button As Button)

End Sub


Private Sub Form_Load()
    '*************************
    'Define drive de execu��o
     path_drv = Left(App.Path, 2)
    'path_drv = "C:"
     
    '**************************
    'busca informacoes sobre os representantes que pertencem ao paac
     Set dbaccess = OpenDatabase(path_drv & "\DADOS\BASE_DPK.MDB")
    
    '*************************
End Sub


Private Sub grdConsulta_DblClick()

    Dim loja
    Dim pedido
    
    grdConsulta.Col = 6
    If grdConsulta.Text = "" Or grdConsulta.Text = "0" Then
        'sele��o de itens do pedido
               SQL = "select a.situacao as situacao,"
         SQL = SQL & " b.cod_tributacao as cod_trib,"
         SQL = SQL & " a.num_item_nota as num_item_nota,"
         SQL = SQL & " a.num_item_pedido as num_item_pedido,"
         SQL = SQL & " b.cod_fornecedor as cod_fornecedor,"
         SQL = SQL & " b.cod_fabrica as cod_fabrica,"
         SQL = SQL & " a.qtd_solicitada as qtd_solicitada,"
         SQL = SQL & " a.qtd_atendida as qtd_atendida,"
         SQL = SQL & " a.preco_unitario as preco_unitario,"
         SQL = SQL & " a.pc_desc1 as pc_desc1,"
         SQL = SQL & " a.pc_desc2 as pc_desc2,"
         SQL = SQL & " a.pc_desc3 as pc_desc3,"
         SQL = SQL & " c.pr_liquido as preco_liquido,"
         SQL = SQL & " c.vl_liquido as valor_liquido,"
         SQL = SQL & " a.tabela_venda as tabela_venda,"
         SQL = SQL & " a.pc_ipi as pc_ipi,"
         SQL = SQL & " (a.pc_comiss +  a.pc_comisstlmk) as comissao,"
         SQL = SQL & " a.cod_dpk as cod_dpk"
         SQL = SQL & " from itpednota_venda a, item_cadastro b, v_pedliq_venda c"
         grdConsulta.Col = 4
         SQL = SQL & " where a.num_pedido = " & Mid(grdConsulta.Text, 1, InStr(grdConsulta.Text, "-") - 1)
         SQL = SQL & " and a.seq_pedido = " & Mid(grdConsulta.Text, InStr(grdConsulta.Text, "-") + 1, 1)
         grdConsulta.Col = 3
         SQL = SQL & " and a.cod_loja = " & grdConsulta.Text
         SQL = SQL & " and a.cod_dpk = b.cod_dpk and"
         SQL = SQL & " a.num_pedido=c.num_pedido and"
         SQL = SQL & " a.seq_pedido=c.seq_pedido and"
         SQL = SQL & " a.cod_loja = c.cod_loja and "
         SQL = SQL & " a.num_item_pedido= c.num_item_pedido"
         
         Set SS = dbaccess.CreateSnapshot(SQL)
            
         If SS.EOF Then
            MsgBox "Pedido sem itens para consulta! ", vbInformation, "ATEN��O"
            Exit Sub
         Else
         
             Call LimpaGrid2
             
            'Enquanto houver registros para esta consulta, executa o loop
             With frmItens.grdItem
                 Do While Not SS.EOF
                 
                     liLinha = liLinha + 1
                     
                     .Rows = liLinha + 1
                     .Row = liLinha
                         
                     If liLinha < 20 Then
                         .Refresh
                     End If
                  
                    'Posiciona dados no grid
                     .Col = 0:  .Text = liLinha
                     .Col = 1:  .Text = SS.Fields!situacao
                     .Col = 2:  .Text = SS.Fields!cod_trib
                     .Col = 3:  .Text = SS.Fields!cod_fornecedor
                     .Col = 4:  .Text = SS.Fields!cod_fabrica
                     .Col = 5:  .Text = SS.Fields!qtd_solicitada
                     .Col = 6:  .Text = SS.Fields!qtd_atendida
                     .Col = 7:  .Text = SS.Fields!preco_unitario
                     .Col = 8:  .Text = SS.Fields!pc_desc1
                     .Col = 9:  .Text = SS.Fields!pc_desc2
                     .Col = 10: .Text = SS.Fields!pc_desc3
                     .Col = 11: .Text = SS.Fields!preco_liquido
                     .Col = 12: .Text = SS.Fields!valor_liquido
                     .Col = 13: .Text = SS.Fields!tabela_venda
                     .Col = 14: .Text = SS.Fields!pc_ipi
                     .Col = 15: .Text = SS.Fields!comissao
                     .Col = 16: .Text = SS.Fields!cod_dpk
                     SS.MoveNext
                 Loop
             
             End With
             frmItens.grdItem.Visible = True
             frmItens.Show vbModal
         End If
    Else
        grdConsulta.Col = 5
        loja = grdConsulta.Text
        grdConsulta.Col = 6
        pedido = grdConsulta.Text
    
        MsgBox "Pedido de Venda do dep�sito " & loja & " N�m. " & pedido & " referente a esta transfer�ncia j� foi gerado. Consulte pelo Sistema de Consulta Pedido-NF!"
    End If
End Sub

Private Sub sscmdConsulta_Click()
    Dim Vl_Total As Double
    Dim Qtde_Pedidos As Integer

    Screen.MousePointer = 11
    Call LimpaGrid
     
    SQL = "Select a.cod_loja_cotacao, a.num_cotacao, " & _
          "a.cod_loja_transf, a.num_pedido, a.seq_pedido, a.cod_loja_venda, " & _
          "a.num_pedido_venda, a.seq_pedido_venda, a.num_cotacao_temp, " & _
          "b.vl_contabil  " & _
          "from r_cotacao_transf a, pednota_venda b " & _
          "where a.num_pedido=b.num_pedido and a.cod_loja_transf=b.cod_loja " & _
          "order by a.cod_loja_cotacao, a.num_cotacao_temp"
           
    Set SS = dbaccess.CreateSnapshot(SQL)
    FreeLocks
    
    Qtde_Pedidos = SS.RecordCount
     
   'Enquanto houver registros para esta consulta, executa o loop
    With grdConsulta
        Do While Not SS.EOF
        
            liLinha = liLinha + 1
            
            .Rows = liLinha + 1
            .Row = liLinha
                
            If liLinha < 20 Then
                .Refresh
            End If
         
            If SS!cod_loja_venda = "" Or SS!cod_loja_venda = Null Or SS!cod_loja_venda = 0 Then
                Vl_Total = Vl_Total + SS.Fields!vl_contabil.Value
            End If
            
           'Posiciona dados no grid
            .Col = 0:  .Text = SS.Fields!num_cotacao_temp
            .Col = 1:  .Text = SS.Fields!cod_loja_cotacao
            .Col = 2:  .Text = SS.Fields!num_cotacao
            .Col = 3:  .Text = SS.Fields!cod_loja_transf
            .Col = 4:  .Text = SS.Fields!num_pedido & "-" & SS.Fields!seq_pedido
            .Col = 5:  .Text = IIf(SS.Fields!cod_loja_venda = 0, "", SS.Fields!cod_loja_venda)
            If SS.Fields!num_pedido_venda = 0 Then
                .Col = 6:  .Text = ""
            Else
                .Col = 6:  .Text = SS.Fields!num_pedido_venda & "-" & SS.Fields!seq_pedido_venda
            End If
            .Col = 7:  .Text = Format(SS.Fields!vl_contabil, "#.00")
            
            SS.MoveNext
        Loop
    
    End With
    txtPedidos = Qtde_Pedidos
    txtVlTotal = Vl_Total
    Screen.MousePointer = 0
End Sub

Private Sub sscmdSair_Click()
    dbaccess.Close
    End
End Sub

Private Sub sscmdSobre_Click()
    frmSobre.Show vbModal
End Sub


