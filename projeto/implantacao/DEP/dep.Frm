VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmIdentifica 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FIL210 - Identifica Dep�sito do Pedido"
   ClientHeight    =   1380
   ClientLeft      =   2535
   ClientTop       =   3075
   ClientWidth     =   4695
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "dep.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1380
   ScaleWidth      =   4695
   Begin MSComDlg.CommonDialog CD 
      Left            =   3960
      Top             =   150
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Par�metros"
      Height          =   3585
      Left            =   60
      TabIndex        =   4
      Top             =   1410
      Width           =   4575
      Begin VB.TextBox txtDirOrigemConsultas 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         TabIndex        =   13
         Top             =   3120
         Width           =   4005
      End
      Begin VB.TextBox txtDirOrigemPedidos 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         TabIndex        =   11
         Top             =   2460
         Width           =   4005
      End
      Begin VB.TextBox txtDirDestinoConsultas 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         TabIndex        =   9
         Top             =   1830
         Width           =   4005
      End
      Begin VB.TextBox txtDirDestinoTransfer 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Top             =   1110
         Width           =   4005
      End
      Begin VB.TextBox txtDrive 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   120
         MaxLength       =   2
         TabIndex        =   5
         Text            =   "P:"
         Top             =   450
         Width           =   345
      End
      Begin Bot�o.cmd cmdDirDestTransf 
         Height          =   315
         Left            =   4140
         TabIndex        =   15
         Top             =   1110
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "dep.frx":0CCA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDirDestCons 
         Height          =   315
         Left            =   4140
         TabIndex        =   16
         Top             =   1830
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "dep.frx":0CE6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDirOriPed 
         Height          =   315
         Left            =   4140
         TabIndex        =   17
         Top             =   2460
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "dep.frx":0D02
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDirOriCons 
         Height          =   315
         Left            =   4140
         TabIndex        =   18
         Top             =   3120
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "dep.frx":0D1E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravar 
         Height          =   585
         Left            =   3780
         TabIndex        =   19
         Top             =   210
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "dep.frx":0D3A
         PICN            =   "dep.frx":0D56
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label6 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Diret�rio de Origem das Consultas:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2910
         Width           =   4035
      End
      Begin VB.Label Label5 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Diret�rio de Origem dos Pedidos:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2250
         Width           =   4035
      End
      Begin VB.Label Label4 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Diret�rio de Destino das Consultas:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1620
         Width           =   4035
      End
      Begin VB.Label Label3 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Diret�rio de Destino das Transfer�ncias:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   900
         Width           =   4035
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Drive onde est�os os arquivos:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   2265
      End
   End
   Begin Bot�o.cmd cmdIniciar 
      Height          =   825
      Left            =   90
      TabIndex        =   1
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Iniciar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dep.frx":1A30
      PICN            =   "dep.frx":1A4C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   3480
      Top             =   180
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   825
      Left            =   2070
      TabIndex        =   2
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dep.frx":2726
      PICN            =   "dep.frx":2742
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdParametros 
      Height          =   825
      Left            =   1080
      TabIndex        =   3
      Top             =   60
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   1455
      BTYPE           =   3
      TX              =   "Parametros"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dep.frx":341C
      PICN            =   "dep.frx":3438
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblMensagem 
      Alignment       =   2  'Center
      BackColor       =   &H00800000&
      Caption         =   "Iniciando Processo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Left            =   30
      TabIndex        =   0
      Top             =   990
      Width           =   4635
   End
End
Attribute VB_Name = "frmIdentifica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDirDestCons_Click()
    CD.ShowOpen
    txtDirDestinoConsultas = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdDirDestTransf_Click()
    CD.ShowOpen
    txtDirDestinoTransfer = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdDirOriCons_Click()
    CD.ShowOpen
    txtDirOrigemConsultas = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdDirOriPed_Click()
    CD.ShowOpen
    txtDirOrigemPedidos = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdGravar_Click()
          
1         On Error GoTo Trata_Erro
              
2         If Trim(txtDrive) = "" Then
3             MsgBox "Informe o Drive.", vbInformation, "Aten��o"
4             txtDrive.SetFocus
5             Exit Sub
6         End If
7         If Trim(txtDirDestinoTransfer) = "" Then
8             MsgBox "Informe o caminho do Destino das Transfer�ncias.", vbInformation, "Aten��o"
9             txtDirDestinoTransfer.SetFocus
10            Exit Sub
11        End If
          
12        If Trim(txtDirDestinoConsultas) = "" Then
13            MsgBox "Informe o caminho do destino das Consultas.", vbInformation, "Aten��o"
14            txtDirDestinoConsultas.SetFocus
15            Exit Sub
16        End If
17        If Trim(txtDirOrigemPedidos) = "" Then
18            MsgBox "Informe o caminho da Origem dos Pedidos.", vbInformation, "Aten��o"
19            txtDirOrigemPedidos.SetFocus
20            Exit Sub
21        End If
22        If Trim(txtDirOrigemConsultas) = "" Then
23            MsgBox "Informe o caminho da Origem das Consultas.", vbInformation, "Aten��o"
24            txtDirOrigemConsultas.SetFocus
25            Exit Sub
26        End If
          
27        Gravar_Parametros "Drive", txtDrive
28        Gravar_Parametros "DestinoTransfer", txtDirDestinoTransfer
29        Gravar_Parametros "DestinoConsultas", txtDirDestinoConsultas
30        Gravar_Parametros "OrigemPedidos", txtDirOrigemPedidos
31        Gravar_Parametros "OrigemConsultas", txtDirOrigemConsultas
          
32        EnviarEmail "Drive:" & txtDrive & vbCrLf & "DestinoTransfer:" & txtDirDestinoTransfer & vbCrLf & "DestinoConsultas:" & txtDirDestinoConsultas & vbCrLf & "OrigemPedidos:" & txtDirOrigemPedidos & vbCrLf & "OrigemConsultas:" & txtDirOrigemConsultas
          
Trata_Erro:
33        If Err.Number <> 0 Then
34            EnviarEmail "cmdGravar_Click -C�digo:" & Err.Number & " -Descricao:" & Err.Description & " -Linha:" & Erl
35            MsgBox "cmdGravar_Click -C�digo:" & Err.Number & " -Descricao:" & Err.Description & " -Linha:" & Erl
36        End If
          
End Sub

Private Sub cmdIniciar_Click()
1     On Error GoTo Trata_erros

      Dim TMP_LINHA As String
      
2     Set vFso = New FileSystemObject
      Dim vFile As File

3     If txtDrive.Tag = "SAIR" Then
4       Me.lblMensagem.Caption = "Avise o Suporte."
5       Exit Sub
6     End If

7     lblMensagem.Caption = "Buscando Pedidos..."
8     Me.Refresh
          
9     If Dir(txtDirOrigemPedidos & "*.TXT") <> "" Then  'EXISTE PEDIDO
10        Do While Dir(txtDirOrigemPedidos & "*.TXT") <> ""

11           STRPEDIDO = Dir(txtDirOrigemPedidos & "*.TXT")
             
12           If Left(UCase(STRPEDIDO), 8) = "PEDCONTR" Then
13              If Dir(txtDirOrigemPedidos & "CONTROLE\") = STRPEDIDO Then
14                 Kill txtDirOrigemPedidos & "CONTROLE\" & STRPEDIDO
15              End If
16              FileCopy txtDirOrigemPedidos & STRPEDIDO, txtDirOrigemPedidos & "CONTROLE\" & STRPEDIDO
17              Kill txtDirOrigemPedidos & STRPEDIDO
18              Kill txtDirOrigemPedidos & Mid(STRPEDIDO, 1, InStrRev(STRPEDIDO, ".") - 1) & ".HDR"

             'Dicas
19           ElseIf Left(UCase(STRPEDIDO), 8) = "PEDDICAS" Then
20              If Dir(txtDirOrigemPedidos & "DICAS\") = STRPEDIDO Then
21                 Kill txtDirOrigemPedidos & "DICAS\" & STRPEDIDO
22              End If
23              FileCopy txtDirOrigemPedidos & STRPEDIDO, txtDirOrigemPedidos & "DICAS\" & STRPEDIDO
24              Kill txtDirOrigemPedidos & STRPEDIDO
25              Kill txtDirOrigemPedidos & Mid(STRPEDIDO, 1, InStrRev(STRPEDIDO, ".") - 1) & ".HDR"

             'Visitas
26           ElseIf Left(UCase(STRPEDIDO), 8) = "PEDVISIT" Then
27              If Dir(txtDirOrigemPedidos & "VISITAS\") = STRPEDIDO Then
28                 Kill txtDirOrigemPedidos & "VISITAS\" & STRPEDIDO
29              End If
30              FileCopy txtDirOrigemPedidos & STRPEDIDO, txtDirOrigemPedidos & "VISITAS\" & STRPEDIDO
31              Kill txtDirOrigemPedidos & STRPEDIDO
32              Kill txtDirOrigemPedidos & Mid(STRPEDIDO, 1, InStrRev(STRPEDIDO, ".") - 1) & ".HDR"


33           ElseIf Left(UCase(STRPEDIDO), 8) = "PEDINDIC" Then
34              If Dir(txtDirOrigemPedidos & "CONTROLE\") = STRPEDIDO Then
35                 Kill txtDirOrigemPedidos & "CONTROLE\" & STRPEDIDO
36              End If
37              FileCopy txtDirOrigemPedidos & STRPEDIDO, txtDirOrigemPedidos & "CONTROLE\" & STRPEDIDO
38              Kill txtDirOrigemPedidos & STRPEDIDO
39              Kill txtDirOrigemPedidos & Mid(STRPEDIDO, 1, InStrRev(STRPEDIDO, ".") - 1) & ".HDR"
40           Else
             
                'IDENTIFICA DIR + NOME DO PEDIDO DE ORIGEM
41              STRPEDIDO = txtDirOrigemPedidos & Dir(txtDirOrigemPedidos & "*.TXT")
42              NUM_PEDIDO = ""
43              SEQ_PEDIDO = ""
44              LOJA_DESTINO = ""
45              NUM_PEDIDO = Mid(STRPEDIDO, 7, InStr(STRPEDIDO, ".") - 8)
46              SEQ_PEDIDO = Right(Mid(STRPEDIDO, 1, InStr(STRPEDIDO, ".") - 1), 1)
              
47              Open STRPEDIDO For Input As #1
48              Line Input #1, TMP_LINHA
49              POS_SEP = InStr(TMP_LINHA, "|")
                  
50              If Mid(TMP_LINHA, 1, 8) = "TRANSFER" Then
51                  Close #1
52                  Processa_Generica "TRANSFER", STRPEDIDO
53              Else
54                  LOJA_DESTINO = Mid(TMP_LINHA, 2, 2)
55                  Close #1
                  
56                  Processa_Generica Val(LOJA_DESTINO), STRPEDIDO
                 
57               'If txtDrive.Tag = "SAIR" Then
58               '   lblMensagem.Caption = "Avise o Suporte"
59               '   Exit Sub
60               'End If
61              End If
62          End If
Pr�ximo: 'Vai para o proximo arquivo pois este est� em branco
63        Loop
64    End If

65    lblMensagem = "Verificando Consulta..."
66    Me.Refresh

67    CONSULTA = Dir(txtDirOrigemConsultas & "CON*.CON")
          
68    If CONSULTA = "" Then    ' N�O EXISTE CONSULTA
69       Exit Sub
70    Else                     ' EXISTE CONSULTA
71       NUMPAAC = Mid(CONSULTA, 1, 7)
72       ARQDEL = Dir(txtDirDestinoConsultas & NUMPAAC & "*.CON")
73       If ARQDEL = "" Then
74          vFso.MoveFile txtDirOrigemConsultas & CONSULTA, txtDirDestinoConsultas & CONSULTA
75          vFso.MoveFile txtDirOrigemConsultas & Mid(CONSULTA, 1, 13) & ".HDR", txtDirDestinoConsultas & Mid(CONSULTA, 1, InStrRev(CONSULTA, ".") - 1) & ".HDR"
76       Else
77          Kill txtDirDestinoConsultas & ARQDEL
78          Kill txtDirDestinoConsultas & Mid(ARQDEL, 1, InStrRev(ARQDEL, ".") - 1) & ".HDR"
79          vFso.MoveFile txtDirOrigemConsultas & CONSULTA, txtDirDestinoConsultas & CONSULTA
80          vFso.MoveFile txtDirOrigemConsultas & Mid(CONSULTA, 1, InStrRev(CONSULTA, ".") - 1) & ".HDR", txtDirDestinoConsultas & Mid(CONSULTA, 1, InStrRev(CONSULTA, ".") - 1) & ".HDR"
81       End If
82    End If

83    Exit Sub

Trata_erros:
84        If Err = 70 Or Err = 75 Or Err = 53 Then
85            Resume Next
86        Else
'64            MsgBox "Sub Command1_Click" & vbCrLf & "C�digo:" & Err & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "STRPEDIDO:" & STRPEDIDO & vbCrLf & "Num_Pedido:" & NUM_PEDIDO & "Seq_pedido:" & SEQ_PEDIDO
87            EnviarEmail "Este arquivo foi renomeado e o programa est� rodando normalmente." & vbCrLf & "Sub Command1_Click" & vbCrLf & "C�digo:" & Err & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "STRPEDIDO:" & STRPEDIDO & vbCrLf & "Num_Pedido:" & NUM_PEDIDO & "Seq_pedido:" & SEQ_PEDIDO
88            Close #1
89            Name STRPEDIDO As STRPEDIDO & ".ERRO"
90            Name Mid(STRPEDIDO, 1, Len(STRPEDIDO) - 3) & "HDR" As Mid(STRPEDIDO, 1, Len(STRPEDIDO) - 3) & "HDR.ERRO"
91            GoTo Pr�ximo
92        End If

End Sub

Private Sub cmdParametros_Click()
    If Me.Height = 1755 Then
        Me.Height = 5535
        lblMensagem = "Processo Parado! Altere os Par�metros."
        lblMensagem.FontSize = 10
        Timer1.Enabled = False
    Else
        Me.Height = 1755
        lblMensagem.FontSize = 12
        lblMensagem = "Iniciando Processamento..."
        Timer1.Enabled = True
    End If
End Sub

Private Sub cmdSair_Click()
    On Error Resume Next
  
    ExcluiBind
  
    Set vBanco = Nothing
    Set vSessao = Nothing
    Set vRst = Nothing
    Close
    
    End
    
End Sub

Private Sub Form_Load()
                
    If App.PrevInstance = True Then
        MsgBox "Este programa j� est� aberto."
        End
    End If
                
    Me.Caption = Me.Caption & " - " & App.Major & "." & App.Minor & "." & App.Revision
                   
    Me.Visible = True
                   
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    Set vBanco = vSessao.OpenDatabase("PRODUCAO", "FIL210/DPKFIL210", 0&)
                   
                   
End Sub


Private Sub Form_Unload(Cancel As Integer)
    
    ExcluiBind
    
    Set vBanco = Nothing
    Set vSessao = Nothing
End Sub

Private Sub Timer1_Timer()
    
    Carregar_Parametros
    
    Call cmdIniciar_Click
    
    lblMensagem.Caption = "Aguardando nova busca..."
    Me.Refresh
End Sub


Sub Processa_Generica(pDeposito As String, pPedido)
1         On Error GoTo Trata_erros
                
          Dim gFso As FileSystemObject
          Dim vDirDeposito As String
          Dim vDirOrigemArquivo As String
          Dim vRst As Object
          Dim vArquivo As String
                  
2         Set gFso = New FileSystemObject
                  
3         If pDeposito <> "TRANSFER" Then

6             Set vRst = vBanco.createdynaset("Select * from Loja where cod_loja =" & Val(pDeposito), 0&)
                  
7             If IsNull(vRst!nome_fantasia) Then
8                 MsgBox "Dep�sito n�o est� cadastrado na tabela LOJA." & vbCrLf & "Pedido: " & STRPEDIDO & vbCrLf & "Avise o Suporte", vbInformation, "Aten��o"
9                 Me.txtDrive.Tag = "SAIR"
10                Exit Sub
11            End If
                  
12            lblMensagem.Caption = vRst!nome_fantasia
13        Else
14            Me.lblMensagem.Caption = "TRANSFER�NCIA"
15        End If
                
18        Me.Refresh
                
19        Select Case Format(pDeposito, "00")
              Case "01"
20                vDirDeposito = "\IN\CPS\"
21            Case "04"
22                vDirDeposito = "\IN\GO\"
23            Case "05"
24                vDirDeposito = "\IN\FOR\"
25            Case "07"
26                vDirDeposito = "\IN\BH\"
27            Case "08"
28                vDirDeposito = "\IN\POA\"
29            Case "09"
30                vDirDeposito = "\IN\DF\"
31            Case "9"
32                vDirDeposito = "\IN\DF\"
33            Case "10"
34                vDirDeposito = "\IN\RJ\"
35            Case "11"
36                vDirDeposito = "\IN\BA\"
37            Case "12"
38                vDirDeposito = "\IN\RE\"
39            Case "13"
40                vDirDeposito = "\IN\MS\"
41            Case "14"
42                vDirDeposito = "\IN\CUR\"
43            Case "TRANSFER"
44                vDirDeposito = Mid(txtDirDestinoTransfer, 3)
45            Case Else
46                vDirDeposito = "\IN\CD" & pDeposito & "\"
47         End Select
                
48         If gFso.FolderExists(txtDrive & vDirDeposito) = False Then
              
49            If Dir(pPedido & ".ERRO") <> "" Then Kill pPedido & ".ERRO"
50            If Dir(Mid(pPedido, 1, Len(pPedido) - 3) & "HDR" & ".ERRO") <> "" Then Kill Mid(pPedido, 1, Len(pPedido) - 3) & "HDR" & ".ERRO"
              
51            EnviarEmail "N�o existe a pasta " & txtDrive & vDirDeposito & " no Servidor." & vbCrLf & "Verifique!!!!!" & vbCrLf & "O pedido " & Mid(pPedido, 7, InStr(pPedido, ".") - 8) & " - " & Right(Mid(pPedido, 1, InStr(pPedido, ".") - 1), 1) & " foi renomeado para " & pPedido & ".ERRO"

52            Name pPedido As pPedido & ".ERRO"
53            Name Mid(pPedido, 1, Len(pPedido) - 3) & "HDR" As Mid(pPedido, 1, Len(pPedido) - 3) & "HDR" & ".ERRO"

54            'txtDrive.Tag = "SAIR"
55            Exit Sub
56        End If
                
          '**************************************************************************
          'COPIA CORPO DO E-MAIL (PEDIDO)
          'DO DIRET�RIO DE ORIGEM H:\COM\ENTRADA\PEDIDOS PARA O DIRETORIO DE DESTINO
          '**************************************************************************
57        vArquivo = NUM_PEDIDO & SEQ_PEDIDO & ".TXT"
58        If Dir(txtDrive & vDirDeposito & vArquivo) <> "" Then Kill txtDrive & vDirDeposito & vArquivo
59        vFso.MoveFile txtDirOrigemPedidos & vArquivo, txtDrive & vDirDeposito & vArquivo
                
          'COPIA CABE�ALHO DO E-MAIL (ENDERE�OS)
60        vArquivo = NUM_PEDIDO & SEQ_PEDIDO & ".HDR"
61        If Dir(txtDrive & vDirDeposito & vArquivo) <> "" Then Kill txtDrive & vDirDeposito & vArquivo
62        vFso.MoveFile txtDirOrigemPedidos & vArquivo, txtDrive & vDirDeposito & vArquivo

Trata_erros:
63        If Err.Number <> 0 Then
64           If Err.Number = 70 Or Err.Number = 75 Then
65              Name txtDirOrigemPedidos & vArquivo As txtDirOrigemPedidos & vArquivo & ".ERRO"
66              Resume Next
             'EDUARDO - 20/03/2007
67           ElseIf Err.Number = 53 Then
68              If Dir(STRPEDIDO) <> "" Then
69                  EnviarEmail "Sub Processa Generica - DEP:" & pDeposito & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & CABEC & " - " & NUM_PEDIDO & SEQ_PEDIDO & ".HDR"
70                  Name STRPEDIDO As STRPEDIDO & ".ERRO"
71              End If
72              Resume Next
73           Else
74             Gravar_Erro "Sub Processa Generica - DEP:" & pDeposito & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & CABEC & " - " & NUM_PEDIDO & SEQ_PEDIDO & ".HDR"
75             EnviarEmail "Sub Processa Generica - DEP:" & pDeposito & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & CABEC & " - " & NUM_PEDIDO & SEQ_PEDIDO & ".HDR"
76             txtDrive.Tag = "SAIR"
77           End If
78        End If
End Sub

Public Sub Carregar_Parametros()
    
    vCodSistema = Pegar_Cod_Sistema
    
    txtDrive = "" & Pegar_VL_Parametro(vCodSistema, "DRIVE")
    txtDirDestinoTransfer = "" & Pegar_VL_Parametro(vCodSistema, "DESTINOTRANSFER")
    txtDirDestinoConsultas = "" & Pegar_VL_Parametro(vCodSistema, "DESTINOCONSULTAS")
    txtDirOrigemPedidos = "" & Pegar_VL_Parametro(vCodSistema, "ORIGEMPEDIDOS")
    txtDirOrigemConsultas = "" & Pegar_VL_Parametro(vCodSistema, "ORIGEMCONSULTAS")
    
End Sub

