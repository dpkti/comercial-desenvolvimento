VERSION 5.00
Begin VB.Form PCK_FRETE 
   Caption         =   "Previs�o de Frete"
   ClientHeight    =   2370
   ClientLeft      =   3765
   ClientTop       =   4155
   ClientWidth     =   4950
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2370
   ScaleWidth      =   4950
   Begin VB.CommandButton Command2 
      Caption         =   "Sair"
      Height          =   495
      Left            =   3360
      TabIndex        =   1
      Top             =   795
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Iniciar"
      Height          =   495
      Left            =   285
      TabIndex        =   0
      Top             =   885
      Width           =   1215
   End
   Begin VB.Label Label1 
      ForeColor       =   &H00000080&
      Height          =   480
      Left            =   180
      TabIndex        =   2
      Top             =   1470
      Width           =   4410
   End
End
Attribute VB_Name = "PCK_FRETE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TOTAL_FATURA_DIA As Double 'Eduardo - 11/09/2006

Private Sub Command1_Click()
    
   Label1.Caption = "Iniciando Execu��o...."
   PCK_FRETE.Refresh
   
   Screen.MousePointer = 13
        
   TOTAL_FATURA_DIA = 0 'Eduardo - 11/09/2006
        
   Nome_Arquivo = "C:\FRETE\FR" & Format(Date, "dd-mm") & ".txt"
   Nome_Arquivo_Log = "C:\FRETE\FRL" & Format(Date, "dd-mm") & ".txt"
        
   Open Nome_Arquivo_Log For Output As #4
        
   Print #4, "Data de Inicio Execu��o: "; Format(Date, "long date")
   Print #4, "Hora de Inicio Execu��o: "; Format(Time, "long time")
        
   Open Nome_Arquivo For Output As #2
     
   Print #2, "----------------------------------------------------------------------------------------------------------------"
   Print #2, "PCK_FRETE - PREVISAO DE CUSTOS DE FRETE - Data Emiss�o: " & Date
   Print #2, "----------------------------------------------------------------------------------------------------------------"
   Print #2, "VENDA"
   Print #2, "----------------------------------------------------------------------------------------------------------------"
   Print #2, "Dt Faturamento        Vl Faturado        Previs�o Frete        Vl Frete Recuperado       Vl Custo      % Frete"
   Print #2, " "
   
   'Seleciona Datas de Execu��o do Relat�rio
   lstrSQL = "Select TO_CHAR(DT_INI_FECH_DIA,'DD-MON-RR') AS DT, DT_FATURAMENTO "
   lstrSQL = lstrSQL & "From  Datas "

   Set ObjCons = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
   
   DT_INICIO_MES = "01" & Mid(ObjCons!DT, 3)
   
    'Valores sem Retira para C�lculo do % Frete
    
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABILR, to_char(dt_emissao_nota,'DD/MM/RR'), COUNT(*) TOT_NF "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D , Transportadora T "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' And "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota< DT_INI_FECH_DIA+1 And "
    lstrSQL = lstrSQL & " p.Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=1 and p.cod_transp=t.cod_transp and "
    lstrSQL = lstrSQL & " fl_retira_local='N' "
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
    
    Set OBJRET = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
    
        
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL, to_char(dt_emissao_nota,'DD/MM/RR'), COUNT(*) TOT_NF "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' And "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota< DT_INI_FECH_DIA+1 And "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=1 "
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
           
    Set OBJTOT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)

    linha = Space(3) & ObjCons!DT_FATURAMENTO & Space(7)
   
    linha = linha & Space(14 - Len(Format(Val(TOTAL_FATURA_DIA), "###,###,###.00"))) & Format(TOTAL_FATURA_DIA, "###,###,###.00")
       
    'Valor Frete Recuperado
       
    lstrSQL = "Select Sum(NVL(Vl_Frete,0)) VL_FRETE, to_char(dt_emissao_nota,'DD/MM/RR')"
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=1 "
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
           
    Set ObjFr = Oradatabase.dbcreatedynaset(lstrSQL, 0&)

    'Notas Fiscais
    
    lstrSQL = "Select  /*+ INDEX(I PK_CIDADE) */ cod_loja_nota, num_nota, cod_uf,to_char(dt_emissao_nota,'DD/MM/RR') dt_emissao_nota, "
    lstrSQL = lstrSQL & " c.cod_cidade , P.peso_bruto, P.vl_contabil, P.COD_TRANSP"
    lstrSQL = lstrSQL & " From pednota_venda p, cliente c, cidade i, datas d"
    lstrSQL = lstrSQL & " Where c.cod_cliente=p.cod_cliente and"
    lstrSQL = lstrSQL & " c.cod_cidade = i.cod_cidade and"
    lstrSQL = lstrSQL & " p.dt_emissao_nota >= '" & DT_INICIO_MES & "' and"
    lstrSQL = lstrSQL & " p.dt_emissao_nota <DT_INI_FECH_DIA+1 and"
    lstrSQL = lstrSQL & " p.situacao+0=0 and tp_transacao=1 and tp_dpkblau IN (0,4) and "
    lstrSQL = lstrSQL & " P.FL_GER_NFIS||'' = 'S' and"
    lstrSQL = lstrSQL & " P.COD_TRANSP <> 999 "
    lstrSQL = lstrSQL & " Order by dt_emissao_nota "

    Set ObjCons = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
   
    data_nf = ObjCons!dt_emissao_nota
    
    Do While Not ObjCons.EOF
       Label1.Caption = "C�lculos do dia: " & data_nf & " da NF: " & ObjCons!COD_LOJA_NOTA & " - " & ObjCons!NUM_NOTA
       PCK_FRETE.Refresh
       If data_nf = ObjCons!dt_emissao_nota Then
           Call PR_CALCULO_FRETE(ObjCons!COD_LOJA_NOTA, ObjCons!NUM_NOTA, ObjCons!COD_UF, ObjCons!COD_CIDADE, ObjCons!PESO_BRUTO, ObjCons!VL_CONTABIL, ObjCons!COD_TRANSP, OBJTOT!TOT_NF)
           ObjCons.MoveNext
       Else
           linha = Space(3) & data_nf & Space(7)
           linha = linha & Space(14 - Len(Format(Val(OBJTOT!VL_CONTABIL), "###,###,###.00"))) & Format(OBJTOT!VL_CONTABIL, "###,###,###.00")
           W_VALOR_TOTAL_FATURADO_MES = W_VALOR_TOTAL_FATURADO_MES + Val(OBJTOT!VL_CONTABIL)
           linha = linha & Space(8) & Space(14 - Len(Format(Val(W_VALOR_TOT_FRETE), "###,###,###.00"))) & Format(W_VALOR_TOT_FRETE, "###,###,###.00")
           'Frete Recuperado
           linha = linha & Space(8) & Space(14 - Len(Format(Val(ObjFr!VL_FRETE), "###,###,###.00"))) & Format(ObjFr!VL_FRETE, "###,###,###.00")
           W_TOTAL_RECUPERADO = W_TOTAL_RECUPERADO + Val(ObjFr!VL_FRETE)
           'Custo Total
           W_CUSTO_FRETE = W_VALOR_TOT_FRETE - Val(ObjFr!VL_FRETE)
           linha = linha & Space(8) & Space(14 - Len(Format(Val(W_CUSTO_FRETE), "###,###,###.00"))) & Format(W_CUSTO_FRETE, "###,###,###.00")
           W_CUSTO_TOTAL_FRETE = W_CUSTO_TOTAL_FRETE + W_CUSTO_FRETE
           '% Frete
           linha = linha & Space(4) & Space(8 - Len(Format(Val((W_CUSTO_FRETE / OBJRET!VL_CONTABILR) * 100), "###,###,###.00"))) & Format((W_CUSTO_FRETE / OBJRET!VL_CONTABILR) * 100, "###,###,###.00")
          
           Print #2, linha
           data_nf = ObjCons!dt_emissao_nota
           linha = ""
           W_VALOR_TOTAL_FRETE_MES = W_VALOR_TOTAL_FRETE_MES + Val(W_VALOR_TOT_FRETE)
           W_VALOR_TOT_FRETE = 0
           OBJTOT.MoveNext
           ObjFr.MoveNext
           OBJRET.MoveNext
       End If
    Loop
      
   'ultimo dia do m�s
    linha = Space(3) & data_nf & Space(7)
    linha = linha & Space(14 - Len(Format(Val(OBJTOT!VL_CONTABIL), "###,###,###.00"))) & Format(OBJTOT!VL_CONTABIL, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(Val(W_VALOR_TOT_FRETE), "###,###,###.00"))) & Format(W_VALOR_TOT_FRETE, "###,###,###.00")
    'Frete Recuperado
    linha = linha & Space(8) & Space(14 - Len(Format(Val(ObjFr!VL_FRETE), "###,###,###.00"))) & Format(ObjFr!VL_FRETE, "###,###,###.00")
    W_TOTAL_RECUPERADO = W_TOTAL_RECUPERADO + Val(ObjFr!VL_FRETE)
    'Custo Total
    W_CUSTO_FRETE = W_VALOR_TOT_FRETE - ObjFr!VL_FRETE
    linha = linha & Space(8) & Space(14 - Len(Format(Val(W_CUSTO_FRETE), "###,###,###.00"))) & Format(W_CUSTO_FRETE, "###,###,###.00")
    W_CUSTO_TOTAL_FRETE = W_CUSTO_TOTAL_FRETE + W_CUSTO_FRETE
    '% Frete
    linha = linha & Space(4) & Space(8 - Len(Format(Val((W_CUSTO_FRETE / OBJRET!VL_CONTABILR) * 100), "###,###,###.00"))) & Format(Abs((W_CUSTO_FRETE / OBJRET!VL_CONTABILR) * 100), "###,###,###.00")
    Print #2, linha
    data_nf = ObjCons!dt_emissao_nota
    linha = ""
    W_VALOR_TOTAL_FRETE_MES = W_VALOR_TOTAL_FRETE_MES + Val(W_VALOR_TOT_FRETE)
    W_VALOR_TOT_FRETE = 0
      
    'Busca Total Faturado no M�s
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=1 "
           
    Set OBJTOT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
   
   
    Print #2, " "
    Print #2, "----------------------------------------------------------------------------------------------------------------"
    Print #2, " "
      
    linha = Space(18) & Space(14 - Len(Format(OBJTOT!VL_CONTABIL, "###,###,###.00"))) & Format(OBJTOT!VL_CONTABIL, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_VALOR_TOTAL_FRETE_MES, "###,###,###.00"))) & Format(W_VALOR_TOTAL_FRETE_MES, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_TOTAL_RECUPERADO, "###,###,###.00"))) & Format(W_TOTAL_RECUPERADO, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_CUSTO_TOTAL_FRETE, "###,###,###.00"))) & Format(W_CUSTO_TOTAL_FRETE, "###,###,###.00")
    
    'Busca Total Faturado no M�s Sem retira para % Frete
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D, transportadora T "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " P.Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=1 and "
    lstrSQL = lstrSQL & " p.cod_transp=t.cod_transp and t.fl_retira_local='N' "
           
    Set OBJTOT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
    
    linha = linha & Space(5) & Space(8 - Len(Format((W_CUSTO_TOTAL_FRETE / OBJTOT!VL_CONTABIL) * 100, "###,###,###.00"))) & Format(Abs((W_CUSTO_TOTAL_FRETE / OBJTOT!VL_CONTABIL) * 100), "###,###,###.00")
    
    Print #2, linha
      
    '*************************************************************************************************************************************
    '** INICIO TRANSFERENCIA *************************************************************************************************************
    W_VALOR_TOT_FRETE = 0
    W_VALOR_TOTAL_FRETE_MES = 0
    W_TOTAL_RECUPERADO = 0
    W_CUSTO_TOTAL_FRETE = 0
    W_CUSTO_FRETE = 0
    
    Print #2, " "
    Print #2, "----------------------------------------------------------------------------------------------------------------"
    Print #2, "TRANSFERENCIA"
    Print #2, "----------------------------------------------------------------------------------------------------------------"
    Print #2, "Dt Faturamento        Vl Faturado        Previs�o Frete        Vl Frete Recuperado       Vl Custo      % Frete"
    Print #2, " "
    
    'Valores sem Retira para C�lculo % Frete
    
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABILRT, to_char(dt_emissao_nota,'DD/MM/RR'), count(*) tot_nft"
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D, Transportadora T "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " p.Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=2 AND COD_NOPE='A30' and "
    lstrSQL = lstrSQL & " p.cod_transp=t.cod_transp and t.fl_retira_local='N' "
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
    
    Set OBJRETT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
    
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL, to_char(dt_emissao_nota,'DD/MM/RR'), count(*) tot_nft"
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=2 AND COD_NOPE='A30'"
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
           
    Set OBJtotT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)

'    Linha = Space(3) & ObjCons!DT_FATURAMENTO & Space(7)
   
    linha = linha & Space(14 - Len(Format(Val(TOTAL_FATURA_DIA), "###,###,###.00"))) & Format(TOTAL_FATURA_DIA, "###,###,###.00")
       
    'Valor Frete Recuperado
       
    lstrSQL = "Select Sum(NVL(Vl_Frete,0)) VL_FRETE, to_char(dt_emissao_nota,'DD/MM/RR')"
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=2 AND COD_NOPE='A30'"
    lstrSQL = lstrSQL & " group by to_char(dt_emissao_nota,'DD/MM/RR') order by to_char(dt_emissao_nota,'DD/MM/RR')"
           
    Set OBJfrT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)

    'Notas Fiscais
    
    lstrSQL = "Select  /*+ INDEX(I PK_CIDADE) */ cod_loja_nota, num_nota, cod_uf,to_char(dt_emissao_nota,'DD/MM/RR') dt_emissao_nota, "
    lstrSQL = lstrSQL & " c.cod_cidade , P.peso_bruto, P.vl_contabil, P.COD_TRANSP"
    lstrSQL = lstrSQL & " From pednota_venda p, cliente c, cidade i, datas d"
    lstrSQL = lstrSQL & " Where c.cod_cliente=p.cod_cliente and"
    lstrSQL = lstrSQL & " c.cod_cidade = i.cod_cidade and"
    lstrSQL = lstrSQL & " p.dt_emissao_nota >= '" & DT_INICIO_MES & "' and"
    lstrSQL = lstrSQL & " p.dt_emissao_nota <DT_INI_FECH_DIA+1 and"
    lstrSQL = lstrSQL & " p.situacao+0=0 and tp_transacao=2 and tp_dpkblau IN (0,4) and "
    lstrSQL = lstrSQL & " P.FL_GER_NFIS||'' = 'S' and"
    lstrSQL = lstrSQL & " P.COD_TRANSP <> 999 AND COD_NOPE='A30'"
    lstrSQL = lstrSQL & " Order by dt_emissao_nota "

    Set ObjConsT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
   
    data_nf = ObjConsT!dt_emissao_nota
    
    Do While Not ObjConsT.EOF
       Label1.Caption = "C�lculos do dia: " & data_nf & " da NF: " & ObjConsT!COD_LOJA_NOTA & " - " & ObjConsT!NUM_NOTA
       PCK_FRETE.Refresh
       If data_nf = ObjConsT!dt_emissao_nota Then
           Call PR_CALCULO_FRETE(ObjConsT!COD_LOJA_NOTA, ObjConsT!NUM_NOTA, ObjConsT!COD_UF, ObjConsT!COD_CIDADE, ObjConsT!PESO_BRUTO, ObjConsT!VL_CONTABIL, ObjConsT!COD_TRANSP, OBJtotT!TOT_NFt)
           ObjConsT.MoveNext
       Else
           linha = Space(3) & data_nf & Space(7)
           linha = linha & Space(14 - Len(Format(Val(OBJtotT!VL_CONTABIL), "###,###,###.00"))) & Format(OBJtotT!VL_CONTABIL, "###,###,###.00")
           W_VALOR_TOTAL_FATURADO_MES = W_VALOR_TOTAL_FATURADO_MES + Val(OBJtotT!VL_CONTABIL)
           linha = linha & Space(8) & Space(14 - Len(Format(Val(W_VALOR_TOT_FRETE), "###,###,###.00"))) & Format(W_VALOR_TOT_FRETE, "###,###,###.00")
           'Frete Recuperado
           linha = linha & Space(8) & Space(14 - Len(Format(Val(OBJfrT!VL_FRETE), "###,###,###.00"))) & Format(OBJfrT!VL_FRETE, "###,###,###.00")
           W_TOTAL_RECUPERADO = W_TOTAL_RECUPERADO + Val(OBJfrT!VL_FRETE)
           'Custo Total
           W_CUSTO_FRETE = W_VALOR_TOT_FRETE - Val(OBJfrT!VL_FRETE)
           linha = linha & Space(8) & Space(14 - Len(Format(Val(W_CUSTO_FRETE), "###,###,###.00"))) & Format(W_CUSTO_FRETE, "###,###,###.00")
           W_CUSTO_TOTAL_FRETE = W_CUSTO_TOTAL_FRETE + W_CUSTO_FRETE
           '% Frete
           linha = linha & Space(4) & Space(8 - Len(Format(Val((W_CUSTO_FRETE / OBJRETT!VL_CONTABILRT) * 100), "###,###,###.00"))) & Format((W_CUSTO_FRETE / OBJRETT!VL_CONTABILRT) * 100, "###,###,###.00")
          
           Print #2, linha
           data_nf = ObjConsT!dt_emissao_nota
           linha = ""
           W_VALOR_TOTAL_FRETE_MES = W_VALOR_TOTAL_FRETE_MES + Val(W_VALOR_TOT_FRETE)
           W_VALOR_TOT_FRETE = 0
           OBJtotT.MoveNext
           OBJfrT.MoveNext
           OBJRETT.MoveNext
       End If
    Loop
      
   'ultimo dia do m�s
    linha = Space(3) & data_nf & Space(7)
    linha = linha & Space(14 - Len(Format(Val(OBJtotT!VL_CONTABIL), "###,###,###.00"))) & Format(OBJtotT!VL_CONTABIL, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(Val(W_VALOR_TOT_FRETE), "###,###,###.00"))) & Format(W_VALOR_TOT_FRETE, "###,###,###.00")
    'Frete Recuperado
    linha = linha & Space(8) & Space(14 - Len(Format(Val(OBJfrT!VL_FRETE), "###,###,###.00"))) & Format(OBJfrT!VL_FRETE, "###,###,###.00")
    W_TOTAL_RECUPERADO = W_TOTAL_RECUPERADO + Val(OBJfrT!VL_FRETE)
    'Custo Total
    W_CUSTO_FRETE = W_VALOR_TOT_FRETE - OBJfrT!VL_FRETE
    linha = linha & Space(8) & Space(14 - Len(Format(Val(W_CUSTO_FRETE), "###,###,###.00"))) & Format(W_CUSTO_FRETE, "###,###,###.00")
    W_CUSTO_TOTAL_FRETE = W_CUSTO_TOTAL_FRETE + W_CUSTO_FRETE
    '% Frete
    linha = linha & Space(4) & Space(8 - Len(Format(Val((W_CUSTO_FRETE / OBJRETT!VL_CONTABILRT) * 100), "###,###,###.00"))) & Format(Abs((W_CUSTO_FRETE / OBJRETT!VL_CONTABILRT) * 100), "###,###,###.00")
    Print #2, linha
    data_nf = ObjCons!dt_emissao_nota
    linha = ""
    W_VALOR_TOTAL_FRETE_MES = W_VALOR_TOTAL_FRETE_MES + Val(W_VALOR_TOT_FRETE)
    W_VALOR_TOT_FRETE = 0
      
    'Busca Total Faturado no M�s
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=2 AND COD_NOPE='A30'"
           
    Set OBJtotT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
   
    Print #2, " "
    Print #2, "----------------------------------------------------------------------------------------------------------------"
    Print #2, " "
      
    linha = Space(18) & Space(14 - Len(Format(OBJtotT!VL_CONTABIL, "###,###,###.00"))) & Format(OBJtotT!VL_CONTABIL, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_VALOR_TOTAL_FRETE_MES, "###,###,###.00"))) & Format(W_VALOR_TOTAL_FRETE_MES, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_TOTAL_RECUPERADO, "###,###,###.00"))) & Format(W_TOTAL_RECUPERADO, "###,###,###.00")
    linha = linha & Space(8) & Space(14 - Len(Format(W_CUSTO_TOTAL_FRETE, "###,###,###.00"))) & Format(W_CUSTO_TOTAL_FRETE, "###,###,###.00")
    
    'Busca Total Faturado no M�s
    lstrSQL = "Select Sum(NVL(Vl_Contabil,0)) VL_CONTABIL "
    lstrSQL = lstrSQL & " From Pednota_Venda P, Datas D, transportadora T "
    lstrSQL = lstrSQL & " Where Dt_Emissao_Nota>='" & DT_INICIO_MES & "' and "
    lstrSQL = lstrSQL & " Dt_Emissao_Nota<DT_INI_FECH_DIA+1 and "
    lstrSQL = lstrSQL & " P.Situacao+0=0 and"
    lstrSQL = lstrSQL & " Fl_Ger_Nfis||''='S' and"
    lstrSQL = lstrSQL & " TP_DpkBlau IN (0,4) and"
    lstrSQL = lstrSQL & " Tp_Transacao=2 AND COD_NOPE='A30' and "
    lstrSQL = lstrSQL & " p.cod_transp=t.cod_transp and fl_retira_local='N' "
           
    Set OBJtotT = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
        
    linha = linha & Space(5) & Space(8 - Len(Format((W_CUSTO_TOTAL_FRETE / OBJTOT!VL_CONTABIL) * 100, "###,###,###.00"))) & Format(Abs((W_CUSTO_TOTAL_FRETE / OBJTOT!VL_CONTABIL) * 100), "###,###,###.00")
    
    Print #2, linha
    
    '** FIM TRANSFERENCIA ****************************************************************************************************************
    '*************************************************************************************************************************************
    
    Close #2
   
    Print #4, "Data de T�rmino Execu��o: "; Format(Date, "long date")
    Print #4, "Hora de T�rmino Execu��o: "; Format(Time, "long time")
    
    Close #4
   
   FileCopy Nome_Arquivo, "C:\COM\SAIDA\PEDIDOS\" & Mid(Nome_Arquivo, 10)
   
   '********************************************************************
   '*** Gera��o e-mail para envio do arquivo
   Open "C:\frete\MSG" & Format(Date, "DD-MM") & ".msg" For Output As #6
   Print #6, "Date: "; Date
   Print #6, "TO: fretes@dpk.com.br "
   Print #6, "From: mkt@dpk.com.br"
   Print #6, "Sbject: Relatorio Previsao Frete"
   Print #6, "File: " & Mid(Nome_Arquivo, 10)
   '**** Fim da Gera��o e-mail
   '********************************************************************
   
   Close #6
    
   FileCopy "C:\frete\MSG" & Format(Date, "DD-MM") & ".MSG", "C:\COM\SAIDA\MSG\MSG" & Format(Date, "DD-MM") & ".MSG"
  
   Label1 = "Finalizado"
   
   Screen.MousePointer = 0
   
End Sub

Private Sub PR_CALCULO_FRETE(W_COD_LOJA_NOTA, W_NUM_NOTA, W_COD_UF, W_COD_CIDADE, W_PESO_BRUTO, W_VL_CONTABIL, W_COD_TRANSP, TOT_NF)
    W_VALOR_CALC1 = 0
    W_VALOR_CALC2 = 0
    W_VALOR_CALC3 = 0
    W_VALOR_CALC4 = 0
    W_VALOR_CALC5 = 0
    W_VALOR_CALC6 = 0
    W_VALOR_CALC7 = 0
    W_VALOR_CALC8 = 0
    W_VALOR_CALC9 = 0
    W_VALOR_TOT_FRETE_NF = 0

    lstrSQL = " Select FL_PESO_VOL, FL_VALOR, FL_SEC, FL_CAT, FL_ITR, FL_DESPACHO, FL_PEDAGIO,"
    lstrSQL = lstrSQL & " FL_ADEME, FL_CONTRATO, VL_FRETE_PESO, VL_FRETE_VALOR, VL_SEC, VL_CAT,"
    lstrSQL = lstrSQL & " VL_ITR , VL_DESPACHO, VL_ADEME, VL_CONTRATO, VL_PEDAGIO, t.VIA_TRANSP, "
    lstrSQL = lstrSQL & " FRETE_MINIMO "
    lstrSQL = lstrSQL & " From FRETE.CALCULO_FRETE F, transportadora T"
    lstrSQL = lstrSQL & " Where F.COD_TRANSP=" & W_COD_TRANSP & " AND"
    lstrSQL = lstrSQL & " F.COD_UF= '" & W_COD_UF & "' AND"
    lstrSQL = lstrSQL & " F.COD_LOJA= " & W_COD_LOJA_NOTA & " AND"
    lstrSQL = lstrSQL & " F.COD_CIDADE IN (0," & W_COD_CIDADE & ") AND "
    lstrSQL = lstrSQL & " F.COD_TRANSP=T.COD_TRANSP "
    lstrSQL = lstrSQL & " Order by COD_LOJA DESC, f.COD_TRANSP DESC, f.COD_CIDADE DESC,COD_UF DESC "
           
    Set ObjCalc = Oradatabase.dbcreatedynaset(lstrSQL, 0&)

    If Not ObjCalc.EOF Then
       If ObjCalc!VIA_TRANSP = "O" Then     'ONIBUS PARA CALCULO DO FRETE PESO
          lstrSQL = " Select valor_frete, PESO_FINAL "
          lstrSQL = lstrSQL & " from frete.calculo_frete_onibus "
          lstrSQL = lstrSQL & " Where cod_transp = " & W_COD_TRANSP & "   and"
          lstrSQL = lstrSQL & " (cod_cidade is null or cod_cidade = " & W_COD_CIDADE & "   ) and"
          lstrSQL = lstrSQL & " ((peso_final>= " & W_PESO_BRUTO & "    and peso_inicial < " & W_PESO_BRUTO & "   ) or"
          lstrSQL = lstrSQL & " (peso_final=0 and peso_inicial<= " & W_PESO_BRUTO & ")) "
          lstrSQL = lstrSQL & " order by cod_transp, cod_cidade, peso_final desc"
           
          Set ObjCalcO = Oradatabase.dbcreatedynaset(lstrSQL, 0&)
          
          If Not ObjCalcO.EOF Then
             If ObjCalcO!Peso_Final = 0 Then
                W_VALOR_CALC1 = PM_VL_CONTABIL * ObjCalcO!Valor_Frete / 100
             Else
                W_VALOR_CALC1 = ObjCalcO!Valor_Frete
             End If
          Else
            W_VALOR_CALC1 = 0
          End If
          
       Else
          If ObjCalc!FL_PESO_VOL = 1 Then   'FRETE PESO QUANDO N�O � �NIBUS
             W_VALOR_CALC1 = PM_PESO_BRUTO * ObjCalc!VL_FRETE_PESO
          End If
       End If
       If ObjCalc!FL_VALOR = 1 Then         'VALOR
          W_VALOR_CALC2 = (W_VL_CONTABIL * ObjCalc!VL_FRETE_VALOR) / 100
       End If
       If ObjCalc!FL_SEC = 1 Then           'SEC
          W_VALOR_CALC3 = ObjCalc!VL_SEC
       End If
       If ObjCalc!FL_CAT = 1 Then           'CAT
          W_VALOR_CALC4 = ObjCalc!VL_CAT
       End If
       If ObjCalc!FL_ITR = 1 Then           'ITR
          W_VALOR_CALC5 = ObjCalc!VL_ITR
       End If
       If ObjCalc!FL_DESPACHO = 1 Then      'DESPACHO
          W_VALOR_CALC6 = ObjCalc!VL_DESPACHO
       End If
       If ObjCalc!FL_PEDAGIO = 1 Then       'PEDAGIO
          W_FRACAO_IDEAL = W_PESO_BRUTO / 100
          If W_FRACAO_IDEAL > Int(W_FRACAO_IDEAL) Then
             W_FRACAO_IDEAL = Int(W_FRACAO_IDEAL) + 1
          End If
          W_VALOR_CALC7 = ObjCalc!VL_PEDAGIO * W_FRACAO_IDEAL
       End If
       If ObjCalc!FL_ADEME = 1 Then         'ADEME
          W_VALOR_CALC8 = (PM_VL_CONTABIL * ObjCalc!VL_ADEME) / 100
       End If
       If ObjCalc!FL_CONTRATO = 1 Then      'CONTRATO
          W_VALOR_CALC9 = (ObjCalc!VL_CONTRATO / 21) / TOT_NF
       End If
       
       W_VALOR_TOT_FRETE_NF = W_VALOR_CALC1 + W_VALOR_CALC2 + W_VALOR_CALC3 + W_VALOR_CALC4 + W_VALOR_CALC5 + W_VALOR_CALC6 + W_VALOR_CALC7 + W_VALOR_CALC8 + W_VALOR_CALC9
       W_VALOR_TOT_FRETE = Val(W_VALOR_TOT_FRETE) + W_VALOR_CALC1 + W_VALOR_CALC2 + W_VALOR_CALC3 + W_VALOR_CALC4 + W_VALOR_CALC5 + W_VALOR_CALC6 + W_VALOR_CALC7 + W_VALOR_CALC8 + W_VALOR_CALC9
    Else
       W_VALOR_TOT_FRETE = Val(W_VALOR_TOT_FRETE) + 0
    End If
      
    'Compara com Valor de Frete Minimo
    If IsNull(ObjCalc!FRETE_MINIMO) Then
       FRETE_MINIMO = 0
    Else
       FRETE_MINIMO = Val(ObjCalc!FRETE_MINIMO)
    End If
    
    If W_VALOR_TOT_FRETE_NF < FRETE_MINIMO Then
       W_VALOR_TOT_FRETE = W_VALOR_TOT_FRETE + Val(ObjCalc!FRETE_MINIMO)
    End If
    
End Sub


Private Sub Command2_Click()
End
End Sub


Private Sub Form_Load()
    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR", 0, "Aten��o"
      End
    End If
        
   Set Orasession = CreateObject("oracleinprocserver.xorasession")
'   Set Oradatabase = Orasession.OpenDatabase("DESENV", "producao/des", 0&)
   Set Oradatabase = Orasession.OpenDatabase("batch", "conssic/batch", 0&)

'   Set Oradatabase = Orasession.OpenDatabase("PRODUCAO", "PRODUCAO/DPK", 0&)
End Sub


