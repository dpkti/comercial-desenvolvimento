﻿using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using Business;
using Entities;

namespace MonitorarPedidos
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            txtUsuario.Select();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            btnEntrar.Select();
            this.Refresh();
            UsuarioBE oUser = new UsuarioBE();
            oUser.LOGIN = txtUsuario.Text;
            oUser.SENHA = txtSenha.Text;
            lblErro.Text = String.Empty;
            try
            {

                string nomeAplicacao = ConfigurationManager.AppSettings["nomeAplicacao"].ToString();
                CentroDistribuicaoBE oCentro = ServicoBO.ObterCentroDistribuicao();

                if (oCentro == null)
                {
                    lblErro.Text = "CD não configurado.";
                    lblErro.ForeColor = Color.Red;
                    return;
                }

                oCentro.COD_LOJA_EXIBICAO = ServicoBO.BuscarNomeExibicaoCDs(oCentro.COD_LOJA);
                oUser = ServicoBO.ValidaUsuario(oUser, nomeAplicacao, oCentro.COD_LOJA);


                if (oUser != null)
                {
                    if (oUser.PERMISSAO_ACESSO)
                    {
                        frmPrincipal frmPrincipal = new frmPrincipal(oUser, oCentro);
                        this.Visible = false;
                        this.Hide();

                        frmPrincipal.ShowDialog();
                        this.Close();
                    }
                    else
                        MessageBox.Show(ResourceMensagemMonitor.UsuarioSemPermissaoDeAcesso, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBox.Show(ResourceMensagemMonitor.UsuarioSenhaInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                lblErro.Text = ex.Message;
                lblErro.ForeColor = Color.Red;
            }
        }


    }
}
