﻿using System;
using System.Configuration;
using System.Drawing;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using Business;
using Entities;

namespace MonitorarPedidos
{
    public partial class frmPrincipal : Form
    {
        PedidoSAP oPedidoSAP;

        public frmPrincipal()
        {
            InitializeComponent();

            InicializaUserControls();
        }
        MonitoraPedidoUC UCmonitorarPedidoPendente;
        MonitoraPedidoUC UCmonitorarPedidoProcessado;
        MonitoraPedidoUC UCmonitorarPedidoNaoCancelado;
        public bool _EmExecucao;

        private void InicializaUserControls()
        {
            oPedidoSAP = new PedidoSAP();
            UCmonitorarPedidoPendente = new MonitoraPedidoUC(TipoUserControl.PedidosPendentes, _CDBase);
            UCmonitorarPedidoPendente.ExecutaRotina += new MonitoraPedidoUC.ExecutarRotina(oPedidoSAP.BuscarPedidos);

            tabPendentes.Controls.Add(UCmonitorarPedidoPendente);

            UCmonitorarPedidoProcessado = new MonitoraPedidoUC(TipoUserControl.PedidosProcessados, _CDBase);
            UCmonitorarPedidoProcessado.ExecutaRotina += new MonitoraPedidoUC.ExecutarRotina(oPedidoSAP.BuscarPedidos);

            tabProcessados.Controls.Add(UCmonitorarPedidoProcessado);

            UCmonitorarPedidoNaoCancelado = new MonitoraPedidoUC(TipoUserControl.PedidosNaoCancelados, _CDBase);
            UCmonitorarPedidoNaoCancelado.ExecutaRotina += new MonitoraPedidoUC.ExecutarRotina(oPedidoSAP.BuscarPedidos);

            tabNaoCancelados.Controls.Add(UCmonitorarPedidoNaoCancelado);

        }

        public UsuarioBE _UsuarioLogado { get; set; }
        public CentroDistribuicaoBE _CDBase { get; set; }
        Ping pinger;
        bool controleExibirNotifySAP;
        public frmPrincipal(UsuarioBE user, CentroDistribuicaoBE oCentro)
        {
            InitializeComponent();

            _CDBase = oCentro;

            InicializaUserControls();

            _UsuarioLogado = user;

            lblUsuarioLogado.Text = _UsuarioLogado.NOME_USUARIO;

            UCmonitorarPedidoProcessado.CarregaComboCD(user.listLojas);

            pinger = new Ping();

            pinger.PingCompleted += new PingCompletedEventHandler(pi_PingCompleted);

            tStatusSAP.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["intervalPingSAP"].ToString());
            tPendentes.Interval = Convert.ToDouble(ConfigurationManager.AppSettings["intervalMonitPendentes"].ToString());
            controleExibirNotifySAP = true;
            pinger.SendAsync(ConfigurationManager.AppSettings["ipToPingSAP"].ToString(), "");

        }

        public void pi_PingCompleted(object sender, PingCompletedEventArgs e)
        {
            string tituloNotify = ResourceMensagemMonitor.tituloNotifyConexaoSAP;
            string mensagemNotify = String.Empty;
            ToolTipIcon tipIcon = ToolTipIcon.Info;
            try
            {
                PingReply reply = e.Reply;
                if (reply.Status == IPStatus.Success)
                {
                    mensagemNotify = String.Format(ResourceMensagemMonitor.SapAtivo, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    lblStatusSAP.ForeColor = Color.Green;

                    notifySAP.Visible = true;
                    tipIcon = ToolTipIcon.Info;

                }
                else
                {
                    mensagemNotify = String.Format(ResourceMensagemMonitor.SAPForaDoAr, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                    lblStatusSAP.ForeColor = Color.Red;
                    tipIcon = ToolTipIcon.Error;
                    controleExibirNotifySAP = true;
                }
            }
            catch (Exception ex)
            {
                mensagemNotify = String.Format(ResourceMensagemMonitor.SAPForaDoAr, DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                tipIcon = ToolTipIcon.Error;
                lblStatusSAP.ForeColor = Color.Red;
                controleExibirNotifySAP = true;
            }
            finally
            {
                lblStatusSAP.Text = mensagemNotify;
                if (controleExibirNotifySAP)
                    notifySAP.ShowBalloonTip(3000, tituloNotify, mensagemNotify, tipIcon);
                controleExibirNotifySAP = false;
            }
        }

        private void tPendente_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // monitorPedidoUCPendentes.Execu
            //  tPendentes.Enabled = false;
            //_EmExecucao = true;
            if (!_EmExecucao)
                UCmonitorarPedidoPendente.Start();

            //_EmExecucao = false;
            //tPendentes.Enabled = true;
        }

        private void timerCustom1_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            pinger.SendAsync(ConfigurationManager.AppSettings["ipToPingSAP"].ToString(), "");
        }

        private void btnCancelaPedido_Click_1(object sender, EventArgs e)
        {
            bool possuiPermissao;
            try
            {
                //verifica se o usuário logada possui permissão para cancelar o pedido
                possuiPermissao = ServicoBO.VerificarPermissaoCancelPedido(_UsuarioLogado.COD_USUARIO, _CDBase.COD_LOJA, _CDBase.TipoBanco);

                //caso não possua permissão uma mensagem será enviada
                if (!possuiPermissao)
                {
                    MessageBox.Show("O usuário não possui permissão para cancelar pedidos.");
                }
                else
                {
                    frmCancelamento frmCancelamento = new frmCancelamento(_CDBase, _UsuarioLogado);
                    frmCancelamento.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            notifySAP.Dispose();
        }




    }
}
