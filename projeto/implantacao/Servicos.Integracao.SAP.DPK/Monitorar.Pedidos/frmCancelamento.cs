﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Business;
using Entities;

namespace MonitorarPedidos
{
    public partial class frmCancelamento : Form
    {
        public CentroDistribuicaoBE _CDDirH { get; set; }
        public UsuarioBE oUser { get; set; }
        private PedidoCancelamentoDH pedidoCancelamentoDH;

        public frmCancelamento(CentroDistribuicaoBE oCentro, UsuarioBE user)
        {
            _CDDirH = oCentro;
            oUser = user;
            InitializeComponent();
        }

        private void frmCancelamento_Load(object sender, EventArgs e)
        {
            cmbDeposito.Items.Add(_CDDirH);
            cmbDeposito.SelectedItem = _CDDirH;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BuscarPedido()
        {
            string mensagemErro = string.Empty;
            if (!txtPedido.Text.Equals(string.Empty) || !txtNF.Text.Equals(string.Empty))
            {
                try
                {
                    PedidoSAP pedidoSAP = new PedidoSAP();
                    PedidoCancelamentoDH pedidoConsultado = pedidoSAP.ConsultarPedidoASerCancelado(_CDDirH.COD_LOJA.ToString(), txtPedido.Text, txtDigitoPedido.Text, txtNF.Text);

                    if (pedidoConsultado.SITUACAO != 0)
                    {
                        MessageBox.Show(ResourceMensagemMonitor.PedidoJaCancelado, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        LimparCampos();
                    }
                    else
                    {
                        if (pedidoConsultado != null && !pedidoConsultado.NUM_PEDIDO.Equals(0))
                        {
                            txtFilial.Text = pedidoConsultado.SIGLA_FILIAL.ToString();
                            txtDivisao.Text = pedidoConsultado.DIVISAO;
                            txtDataNota.Text = pedidoConsultado.DT_EMISSAO_NOTA.ToString("dd/MM/yyyy");
                            txtRepresentante.Text = pedidoConsultado.COD_REPRES.ToString();
                            txtDigitoRepresentante.Text = "0";
                            txtDigitoPedido.Text = "0";
                            txtNomeRepresentante.Text = pedidoConsultado.PSEUDO_REPR;
                            txtCodigoVendedor.Text = pedidoConsultado.COD_VEND.ToString();
                            txtNomeVendedor.Text = pedidoConsultado.PSEUDO_VEND;
                            txtNomeCliente.Text = pedidoConsultado.NOME_CLIENTE;
                            txtCodigoCliente.Text = pedidoConsultado.COD_CLIENTE.ToString();
                            txtCidadeCliente.Text = pedidoConsultado.CID_CLIENTE;
                            txtEstadoCliente.Text = pedidoConsultado.UF_CLIENTE;
                            txtCGC.Text = pedidoConsultado.CGC_CLIENTE.ToString();
                            txtCodigoTransportadora.Text = pedidoConsultado.COD_TRANSP.ToString();
                            txtNomeTransportadora.Text = pedidoConsultado.NOME_TRANSP;
                            txtVia.Text = pedidoConsultado.VIA_TRANSP;
                            txtPeso.Text = pedidoConsultado.PESO_BRUTO.ToString();
                            txtCodigoPlanoPgto.Text = pedidoConsultado.COD_PLANO.ToString();
                            txtDescricaoPlanoPgto.Text = pedidoConsultado.DESC_PLANO;
                            txtCodNaturezaOp.Text = pedidoConsultado.COD_NOPE.ToString();
                            txtDescricaoNaturezaOp.Text = pedidoConsultado.DESC_NATUREZA;
                            txtValorContabil.Text = pedidoConsultado.VL_CONTABIL.ToString();
                            txtDifIcm.Text = pedidoConsultado.FL_DIF_ICM.ToString();
                            txtDesc.Text = pedidoConsultado.PC_DESCONTO.ToString();
                            txtAcrescimo.Text = pedidoConsultado.PC_ACRESCIMO.ToString();
                            txtItens.Text = pedidoConsultado.QTD_ITEM_PEDIDO.ToString();
                            txtMensagemAviso.Text = pedidoConsultado.MENS_PEDIDO;
                            txtMensagemNota.Text = pedidoConsultado.MENS_NOTA;
                            txtPedido.Text = pedidoConsultado.NUM_PEDIDO.ToString();
                            txtNF.Text = pedidoConsultado.NUM_NOTA == 0 ? string.Empty : pedidoConsultado.NUM_NOTA.ToString();

                            pedidoCancelamentoDH = pedidoConsultado;

                            VisibleTxtBoxCodCancelamento(true);
                            txtCodCancelamento.Text = string.Empty;
                            txtDescCancelamento.Text = string.Empty;
                        }
                        else
                        {
                            pedidoCancelamentoDH = null;
                            VisibleTxtBoxCodCancelamento(false);
                            if (!txtPedido.Text.Equals(string.Empty))
                            {
                                mensagemErro = ResourceMensagemMonitor.NumPedidoNaoExiste;
                            }
                            else
                            {
                                mensagemErro = ResourceMensagemMonitor.NumNotaFiscalNaoExiste;
                            }
                            LimparCampos();
                        }
                    }
                }
                catch (Exception)
                {
                    mensagemErro = ResourceMensagemMonitor.ErroCarregarPedido;
                }
            }

            if (!mensagemErro.Equals(string.Empty))
            {
                MessageBox.Show(mensagemErro, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LimparCampos();
            }
        }

        private void txtPedido_Leave(object sender, EventArgs e)
        {
            if (!txtPedido.Text.Equals(string.Empty))
            {
                txtNF.Text = string.Empty;
                BuscarPedido();
            }
        }

        private void txtNF_Leave(object sender, EventArgs e)
        {
            if (!txtNF.Text.Equals(string.Empty))
            {
                txtPedido.Text = string.Empty;
                BuscarPedido();
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparCampos();
        }

        private void txtCodCancelamento_Leave(object sender, EventArgs e)
        {
            BuscarCodCancelamento();
        }

        private void txtCodCancelamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt16(e.KeyChar) == 13)
            {
                BuscarCodCancelamento();
            }
        }

        private void BuscarCodCancelamento()
        {
            if (string.IsNullOrEmpty(txtCodCancelamento.Text))
            {
                txtDescCancelamento.Text = string.Empty;
                return;
            }

            try
            {
                string desc = ServicoBO.BuscarDescricaoCancelamento(Convert.ToInt32(txtCodCancelamento.Text));

                if (string.IsNullOrEmpty(desc))
                {
                    MessageBox.Show(ResourceMensagemMonitor.CodCancelamentoInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDescCancelamento.Text = string.Empty;
                }
                else
                {
                    txtDescCancelamento.Text = desc;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ResourceMensagemMonitor.ErroCarregarCodCancelamento, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDescCancelamento.Text = string.Empty;
            }
        }

        private void txtPedido_TextChanged(object sender, EventArgs e)
        {
            somenteNumeros(txtPedido);
        }

        private void txtNF_TextChanged(object sender, EventArgs e)
        {
            somenteNumeros(txtNF);
        }

        private void txtCodCancelamento_TextChanged(object sender, EventArgs e)
        {
            somenteNumeros(txtCodCancelamento);
        }

        /// <summary>
        /// Remove letras digitadas no textbox
        /// </summary>
        /// <param name="textBox"></param>
        private void somenteNumeros(TextBox textBox)
        {
            if (Regex.IsMatch(textBox.Text, "\\D"))
            {
                int num = textBox.SelectionStart;//textBox.GetCharIndexFromPosition(pos);

                textBox.Text = Regex.Replace(textBox.Text, "\\D", string.Empty);

                textBox.SelectionStart = num - 1;
                textBox.SelectionLength = 0;
            }
        }

        /// <summary>
        /// Limpa os campos da tela de cancelamento
        /// </summary>
        private void LimparCampos()
        {
            txtFilial.Text = string.Empty;
            txtDivisao.Text = string.Empty;
            txtDataNota.Text = string.Empty;
            txtRepresentante.Text = string.Empty;
            txtDigitoRepresentante.Text = "0";
            txtDigitoPedido.Text = "0";
            txtNomeRepresentante.Text = string.Empty;
            txtCodigoVendedor.Text = string.Empty;
            txtNomeVendedor.Text = string.Empty;
            txtNomeCliente.Text = string.Empty;
            txtCodigoCliente.Text = string.Empty;
            txtCidadeCliente.Text = string.Empty;
            txtEstadoCliente.Text = string.Empty;
            txtCGC.Text = string.Empty;
            txtCodigoTransportadora.Text = string.Empty;
            txtNomeTransportadora.Text = string.Empty;
            txtVia.Text = string.Empty;
            txtPeso.Text = string.Empty;
            txtCodigoPlanoPgto.Text = string.Empty;
            txtDescricaoPlanoPgto.Text = string.Empty;
            txtCodNaturezaOp.Text = string.Empty;
            txtDescricaoNaturezaOp.Text = string.Empty;
            txtValorContabil.Text = string.Empty;
            txtDifIcm.Text = string.Empty;
            txtDesc.Text = string.Empty;
            txtAcrescimo.Text = string.Empty;
            txtItens.Text = string.Empty;
            txtMensagemAviso.Text = string.Empty;
            txtMensagemNota.Text = string.Empty;
            txtPedido.Text = string.Empty;
            txtNF.Text = string.Empty;

            VisibleTxtBoxCodCancelamento(false);

            pedidoCancelamentoDH = null;
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (pedidoCancelamentoDH == null)
            {
                MessageBox.Show(ResourceMensagemMonitor.PedidoConsultadoNull, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtDescCancelamento.Text.Equals(string.Empty))
            {
                MessageBox.Show(ResourceMensagemMonitor.CodCancelamentoBranco, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DialogResult result = MessageBox.Show(ResourceMensagemMonitor.ConfirmarCancelamentoPedido, "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


            try
            {
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    PedidoSAP pedidoSAP = new PedidoSAP();
                    pedidoCancelamentoDH.COD_LOJA = _CDDirH.COD_LOJA;
                    pedidoCancelamentoDH.COD_CANCEL = Convert.ToInt32(txtCodCancelamento.Text);
                    pedidoCancelamentoDH.SITUACAO = 9;
                    pedidoSAP.BuscarControlePedido(pedidoCancelamentoDH);
                    CancelamentoOut cancOut = pedidoSAP.CancelarPedido(pedidoCancelamentoDH, _CDDirH, oUser);

                    if (cancOut.canceladoComSucesso)
                    {
                        MessageBox.Show(ResourceMensagemMonitor.PedidoCanceladoComSucesso, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        LimparCampos();
                    }
                    else
                    {
                        string msg = ResourceMensagemMonitor.PedidoNaoCancSAP + "\n\n";
                        foreach (string texto in cancOut.messages)
                        {
                            msg += texto + "\n";
                        }
                        MessageBox.Show(msg, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        LimparCampos();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void VisibleTxtBoxCodCancelamento(bool opcao)
        {
            txtDescCancelamento.Visible = opcao;
            txtCodCancelamento.Visible = opcao;
        }

    }
}
