﻿namespace MonitorarPedidos
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tabControlGeral = new System.Windows.Forms.TabControl();
            this.tabPendentes = new System.Windows.Forms.TabPage();
            this.tabNaoCancelados = new System.Windows.Forms.TabPage();
            this.tabProcessados = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.tPendentes = new CommonServico.TimerCustom();
            this.lblUsuarioLogado = new System.Windows.Forms.Label();
            this.lblStatusSAP = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tStatusSAP = new CommonServico.TimerCustom();
            this.notifySAP = new System.Windows.Forms.NotifyIcon(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCancelaPedido = new System.Windows.Forms.Button();
            this.tabControlGeral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tPendentes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStatusSAP)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlGeral
            // 
            this.tabControlGeral.Controls.Add(this.tabPendentes);
            this.tabControlGeral.Controls.Add(this.tabNaoCancelados);
            this.tabControlGeral.Controls.Add(this.tabProcessados);
            this.tabControlGeral.Location = new System.Drawing.Point(12, 45);
            this.tabControlGeral.Name = "tabControlGeral";
            this.tabControlGeral.SelectedIndex = 0;
            this.tabControlGeral.Size = new System.Drawing.Size(850, 447);
            this.tabControlGeral.TabIndex = 2;
            // 
            // tabPendentes
            // 
            this.tabPendentes.Location = new System.Drawing.Point(4, 22);
            this.tabPendentes.Name = "tabPendentes";
            this.tabPendentes.Padding = new System.Windows.Forms.Padding(3);
            this.tabPendentes.Size = new System.Drawing.Size(842, 421);
            this.tabPendentes.TabIndex = 0;
            this.tabPendentes.Text = "Pedidos Pendentes";
            this.tabPendentes.UseVisualStyleBackColor = true;
            // 
            // tabNaoCancelados
            // 
            this.tabNaoCancelados.Location = new System.Drawing.Point(4, 22);
            this.tabNaoCancelados.Name = "tabNaoCancelados";
            this.tabNaoCancelados.Padding = new System.Windows.Forms.Padding(3);
            this.tabNaoCancelados.Size = new System.Drawing.Size(842, 421);
            this.tabNaoCancelados.TabIndex = 2;
            this.tabNaoCancelados.Text = "Pedidos não cancelados";
            this.tabNaoCancelados.UseVisualStyleBackColor = true;
            // 
            // tabProcessados
            // 
            this.tabProcessados.Location = new System.Drawing.Point(4, 22);
            this.tabProcessados.Name = "tabProcessados";
            this.tabProcessados.Padding = new System.Windows.Forms.Padding(3);
            this.tabProcessados.Size = new System.Drawing.Size(842, 421);
            this.tabProcessados.TabIndex = 1;
            this.tabProcessados.Text = "Pedidos Processados";
            this.tabProcessados.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // tPendentes
            // 
            this.tPendentes.Enabled = true;
            this.tPendentes.Interval = 10000D;
            this.tPendentes.SynchronizingObject = this;
            this.tPendentes.Elapsed += new System.Timers.ElapsedEventHandler(this.tPendente_Elapsed);
            // 
            // lblUsuarioLogado
            // 
            this.lblUsuarioLogado.AutoSize = true;
            this.lblUsuarioLogado.Location = new System.Drawing.Point(644, 21);
            this.lblUsuarioLogado.Name = "lblUsuarioLogado";
            this.lblUsuarioLogado.Size = new System.Drawing.Size(35, 13);
            this.lblUsuarioLogado.TabIndex = 3;
            this.lblUsuarioLogado.Text = "label1";
            // 
            // lblStatusSAP
            // 
            this.lblStatusSAP.AutoSize = true;
            this.lblStatusSAP.Location = new System.Drawing.Point(76, 496);
            this.lblStatusSAP.Name = "lblStatusSAP";
            this.lblStatusSAP.Size = new System.Drawing.Size(0, 13);
            this.lblStatusSAP.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 496);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Status SAP:";
            // 
            // tStatusSAP
            // 
            this.tStatusSAP.Enabled = true;
            this.tStatusSAP.Interval = 10000D;
            this.tStatusSAP.SynchronizingObject = this;
            this.tStatusSAP.Elapsed += new System.Timers.ElapsedEventHandler(this.timerCustom1_Elapsed);
            // 
            // notifySAP
            // 
            this.notifySAP.Icon = ((System.Drawing.Icon)(resources.GetObject("notifySAP.Icon")));
            this.notifySAP.Text = "Monitoramento de pedidos";
            this.notifySAP.Visible = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCancelaPedido);
            this.panel2.Controls.Add(this.lblUsuarioLogado);
            this.panel2.Location = new System.Drawing.Point(8, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(846, 37);
            this.panel2.TabIndex = 24;
            // 
            // btnCancelaPedido
            // 
            this.btnCancelaPedido.Image = global::MonitorarPedidos.Properties.Resources.ico24_cancelar;
            this.btnCancelaPedido.Location = new System.Drawing.Point(4, 3);
            this.btnCancelaPedido.Name = "btnCancelaPedido";
            this.btnCancelaPedido.Size = new System.Drawing.Size(160, 31);
            this.btnCancelaPedido.TabIndex = 0;
            this.btnCancelaPedido.Text = "Cancelamento de pedido";
            this.btnCancelaPedido.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancelaPedido.UseVisualStyleBackColor = true;
            this.btnCancelaPedido.Click += new System.EventHandler(this.btnCancelaPedido_Click_1);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(872, 512);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblStatusSAP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControlGeral);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Monitorar comunicação de pedidos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            this.tabControlGeral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tPendentes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tStatusSAP)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlGeral;
        private System.Windows.Forms.TabPage tabPendentes;
        private System.Windows.Forms.Button button1;
        private CommonServico.TimerCustom tPendentes;
        private System.Windows.Forms.TabPage tabProcessados;
        private System.Windows.Forms.Label lblUsuarioLogado;
        private System.Windows.Forms.Label lblStatusSAP;
        private System.Windows.Forms.Label label4;
        private CommonServico.TimerCustom tStatusSAP;
        private System.Windows.Forms.NotifyIcon notifySAP;
        private System.Windows.Forms.TabPage tabNaoCancelados;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnCancelaPedido;
    }
}

