﻿namespace MonitorarPedidos
{
    partial class frmCancelamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCancelamento));
            this.gbPrincipal = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDescCancelamento = new System.Windows.Forms.TextBox();
            this.txtCodCancelamento = new System.Windows.Forms.TextBox();
            this.txtMensagemNota = new System.Windows.Forms.TextBox();
            this.lblMensagemNota = new System.Windows.Forms.Label();
            this.txtMensagemAviso = new System.Windows.Forms.TextBox();
            this.lblMensagemAviso = new System.Windows.Forms.Label();
            this.txtItens = new System.Windows.Forms.TextBox();
            this.lblItens = new System.Windows.Forms.Label();
            this.txtAcrescimo = new System.Windows.Forms.TextBox();
            this.lblAcrescimo = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.txtDifIcm = new System.Windows.Forms.TextBox();
            this.lblDifIcm = new System.Windows.Forms.Label();
            this.txtValorContabil = new System.Windows.Forms.TextBox();
            this.lblValorContabil = new System.Windows.Forms.Label();
            this.txtDescricaoNaturezaOp = new System.Windows.Forms.TextBox();
            this.txtCodNaturezaOp = new System.Windows.Forms.TextBox();
            this.lblNaturezaOperacao = new System.Windows.Forms.Label();
            this.txtDescricaoPlanoPgto = new System.Windows.Forms.TextBox();
            this.txtCodigoPlanoPgto = new System.Windows.Forms.TextBox();
            this.lblPlanoPgto = new System.Windows.Forms.Label();
            this.lblKg = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.lblPeso = new System.Windows.Forms.Label();
            this.txtVia = new System.Windows.Forms.TextBox();
            this.lblVia = new System.Windows.Forms.Label();
            this.txtNomeTransportadora = new System.Windows.Forms.TextBox();
            this.txtCodigoTransportadora = new System.Windows.Forms.TextBox();
            this.lblTransportadora = new System.Windows.Forms.Label();
            this.lblTraco = new System.Windows.Forms.Label();
            this.txtEstadoCliente = new System.Windows.Forms.TextBox();
            this.txtCidadeCliente = new System.Windows.Forms.TextBox();
            this.txtCGC = new System.Windows.Forms.TextBox();
            this.lblCGC = new System.Windows.Forms.Label();
            this.txtNomeCliente = new System.Windows.Forms.TextBox();
            this.txtCodigoCliente = new System.Windows.Forms.TextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.txtNomeVendedor = new System.Windows.Forms.TextBox();
            this.txtCodigoVendedor = new System.Windows.Forms.TextBox();
            this.lblVendedor = new System.Windows.Forms.Label();
            this.txtNomeRepresentante = new System.Windows.Forms.TextBox();
            this.txtDigitoRepresentante = new System.Windows.Forms.TextBox();
            this.txtRepresentante = new System.Windows.Forms.TextBox();
            this.lblRepresentante = new System.Windows.Forms.Label();
            this.txtDataNota = new System.Windows.Forms.TextBox();
            this.lblDataNota = new System.Windows.Forms.Label();
            this.txtDivisao = new System.Windows.Forms.TextBox();
            this.lblDivisao = new System.Windows.Forms.Label();
            this.txtFilial = new System.Windows.Forms.TextBox();
            this.lblFilial = new System.Windows.Forms.Label();
            this.txtNF = new System.Windows.Forms.TextBox();
            this.lblNF = new System.Windows.Forms.Label();
            this.txtDigitoPedido = new System.Windows.Forms.TextBox();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.lblPedido = new System.Windows.Forms.Label();
            this.cmbDeposito = new System.Windows.Forms.ComboBox();
            this.lblDeposito = new System.Windows.Forms.Label();
            this.gbBotoes = new System.Windows.Forms.GroupBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.gbPrincipal.SuspendLayout();
            this.gbBotoes.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbPrincipal
            // 
            this.gbPrincipal.Controls.Add(this.label1);
            this.gbPrincipal.Controls.Add(this.txtDescCancelamento);
            this.gbPrincipal.Controls.Add(this.txtCodCancelamento);
            this.gbPrincipal.Controls.Add(this.txtMensagemNota);
            this.gbPrincipal.Controls.Add(this.lblMensagemNota);
            this.gbPrincipal.Controls.Add(this.txtMensagemAviso);
            this.gbPrincipal.Controls.Add(this.lblMensagemAviso);
            this.gbPrincipal.Controls.Add(this.txtItens);
            this.gbPrincipal.Controls.Add(this.lblItens);
            this.gbPrincipal.Controls.Add(this.txtAcrescimo);
            this.gbPrincipal.Controls.Add(this.lblAcrescimo);
            this.gbPrincipal.Controls.Add(this.txtDesc);
            this.gbPrincipal.Controls.Add(this.lblDesconto);
            this.gbPrincipal.Controls.Add(this.txtDifIcm);
            this.gbPrincipal.Controls.Add(this.lblDifIcm);
            this.gbPrincipal.Controls.Add(this.txtValorContabil);
            this.gbPrincipal.Controls.Add(this.lblValorContabil);
            this.gbPrincipal.Controls.Add(this.txtDescricaoNaturezaOp);
            this.gbPrincipal.Controls.Add(this.txtCodNaturezaOp);
            this.gbPrincipal.Controls.Add(this.lblNaturezaOperacao);
            this.gbPrincipal.Controls.Add(this.txtDescricaoPlanoPgto);
            this.gbPrincipal.Controls.Add(this.txtCodigoPlanoPgto);
            this.gbPrincipal.Controls.Add(this.lblPlanoPgto);
            this.gbPrincipal.Controls.Add(this.lblKg);
            this.gbPrincipal.Controls.Add(this.txtPeso);
            this.gbPrincipal.Controls.Add(this.lblPeso);
            this.gbPrincipal.Controls.Add(this.txtVia);
            this.gbPrincipal.Controls.Add(this.lblVia);
            this.gbPrincipal.Controls.Add(this.txtNomeTransportadora);
            this.gbPrincipal.Controls.Add(this.txtCodigoTransportadora);
            this.gbPrincipal.Controls.Add(this.lblTransportadora);
            this.gbPrincipal.Controls.Add(this.lblTraco);
            this.gbPrincipal.Controls.Add(this.txtEstadoCliente);
            this.gbPrincipal.Controls.Add(this.txtCidadeCliente);
            this.gbPrincipal.Controls.Add(this.txtCGC);
            this.gbPrincipal.Controls.Add(this.lblCGC);
            this.gbPrincipal.Controls.Add(this.txtNomeCliente);
            this.gbPrincipal.Controls.Add(this.txtCodigoCliente);
            this.gbPrincipal.Controls.Add(this.lblCliente);
            this.gbPrincipal.Controls.Add(this.txtNomeVendedor);
            this.gbPrincipal.Controls.Add(this.txtCodigoVendedor);
            this.gbPrincipal.Controls.Add(this.lblVendedor);
            this.gbPrincipal.Controls.Add(this.txtNomeRepresentante);
            this.gbPrincipal.Controls.Add(this.txtDigitoRepresentante);
            this.gbPrincipal.Controls.Add(this.txtRepresentante);
            this.gbPrincipal.Controls.Add(this.lblRepresentante);
            this.gbPrincipal.Controls.Add(this.txtDataNota);
            this.gbPrincipal.Controls.Add(this.lblDataNota);
            this.gbPrincipal.Controls.Add(this.txtDivisao);
            this.gbPrincipal.Controls.Add(this.lblDivisao);
            this.gbPrincipal.Controls.Add(this.txtFilial);
            this.gbPrincipal.Controls.Add(this.lblFilial);
            this.gbPrincipal.Controls.Add(this.txtNF);
            this.gbPrincipal.Controls.Add(this.lblNF);
            this.gbPrincipal.Controls.Add(this.txtDigitoPedido);
            this.gbPrincipal.Controls.Add(this.txtPedido);
            this.gbPrincipal.Controls.Add(this.lblPedido);
            this.gbPrincipal.Controls.Add(this.cmbDeposito);
            this.gbPrincipal.Controls.Add(this.lblDeposito);
            this.gbPrincipal.Location = new System.Drawing.Point(12, 12);
            this.gbPrincipal.Name = "gbPrincipal";
            this.gbPrincipal.Size = new System.Drawing.Size(945, 460);
            this.gbPrincipal.TabIndex = 0;
            this.gbPrincipal.TabStop = false;
            this.gbPrincipal.Text = "Pedido/Nota Fiscal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 427);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 58;
            this.label1.Text = "Cod. Motivo Cancel.:";
            // 
            // txtDescCancelamento
            // 
            this.txtDescCancelamento.Location = new System.Drawing.Point(196, 424);
            this.txtDescCancelamento.Name = "txtDescCancelamento";
            this.txtDescCancelamento.ReadOnly = true;
            this.txtDescCancelamento.Size = new System.Drawing.Size(648, 20);
            this.txtDescCancelamento.TabIndex = 57;
            this.txtDescCancelamento.Visible = false;
            // 
            // txtCodCancelamento
            // 
            this.txtCodCancelamento.Location = new System.Drawing.Point(116, 424);
            this.txtCodCancelamento.MaxLength = 2;
            this.txtCodCancelamento.Name = "txtCodCancelamento";
            this.txtCodCancelamento.Size = new System.Drawing.Size(64, 20);
            this.txtCodCancelamento.TabIndex = 56;
            this.txtCodCancelamento.Visible = false;
            this.txtCodCancelamento.TextChanged += new System.EventHandler(this.txtCodCancelamento_TextChanged);
            this.txtCodCancelamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCancelamento_KeyPress);
            this.txtCodCancelamento.Leave += new System.EventHandler(this.txtCodCancelamento_Leave);
            // 
            // txtMensagemNota
            // 
            this.txtMensagemNota.Enabled = false;
            this.txtMensagemNota.Location = new System.Drawing.Point(116, 398);
            this.txtMensagemNota.Name = "txtMensagemNota";
            this.txtMensagemNota.Size = new System.Drawing.Size(728, 20);
            this.txtMensagemNota.TabIndex = 55;
            // 
            // lblMensagemNota
            // 
            this.lblMensagemNota.AutoSize = true;
            this.lblMensagemNota.Location = new System.Drawing.Point(22, 401);
            this.lblMensagemNota.Name = "lblMensagemNota";
            this.lblMensagemNota.Size = new System.Drawing.Size(88, 13);
            this.lblMensagemNota.TabIndex = 54;
            this.lblMensagemNota.Text = "Mensagem Nota:";
            // 
            // txtMensagemAviso
            // 
            this.txtMensagemAviso.Enabled = false;
            this.txtMensagemAviso.Location = new System.Drawing.Point(116, 372);
            this.txtMensagemAviso.Name = "txtMensagemAviso";
            this.txtMensagemAviso.Size = new System.Drawing.Size(728, 20);
            this.txtMensagemAviso.TabIndex = 53;
            // 
            // lblMensagemAviso
            // 
            this.lblMensagemAviso.AutoSize = true;
            this.lblMensagemAviso.Location = new System.Drawing.Point(19, 375);
            this.lblMensagemAviso.Name = "lblMensagemAviso";
            this.lblMensagemAviso.Size = new System.Drawing.Size(91, 13);
            this.lblMensagemAviso.TabIndex = 52;
            this.lblMensagemAviso.Text = "Mensagem Aviso:";
            // 
            // txtItens
            // 
            this.txtItens.Enabled = false;
            this.txtItens.Location = new System.Drawing.Point(472, 334);
            this.txtItens.Name = "txtItens";
            this.txtItens.Size = new System.Drawing.Size(71, 20);
            this.txtItens.TabIndex = 51;
            // 
            // lblItens
            // 
            this.lblItens.AutoSize = true;
            this.lblItens.Location = new System.Drawing.Point(442, 337);
            this.lblItens.Name = "lblItens";
            this.lblItens.Size = new System.Drawing.Size(33, 13);
            this.lblItens.TabIndex = 50;
            this.lblItens.Text = "Itens:";
            // 
            // txtAcrescimo
            // 
            this.txtAcrescimo.Enabled = false;
            this.txtAcrescimo.Location = new System.Drawing.Point(363, 334);
            this.txtAcrescimo.Name = "txtAcrescimo";
            this.txtAcrescimo.Size = new System.Drawing.Size(64, 20);
            this.txtAcrescimo.TabIndex = 49;
            // 
            // lblAcrescimo
            // 
            this.lblAcrescimo.AutoSize = true;
            this.lblAcrescimo.Location = new System.Drawing.Point(319, 337);
            this.lblAcrescimo.Name = "lblAcrescimo";
            this.lblAcrescimo.Size = new System.Drawing.Size(46, 13);
            this.lblAcrescimo.TabIndex = 48;
            this.lblAcrescimo.Text = "Acresc.:";
            // 
            // txtDesc
            // 
            this.txtDesc.Enabled = false;
            this.txtDesc.Location = new System.Drawing.Point(241, 334);
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(64, 20);
            this.txtDesc.TabIndex = 47;
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.Location = new System.Drawing.Point(205, 337);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(38, 13);
            this.lblDesconto.TabIndex = 46;
            this.lblDesconto.Text = "Desc.:";
            // 
            // txtDifIcm
            // 
            this.txtDifIcm.Enabled = false;
            this.txtDifIcm.Location = new System.Drawing.Point(116, 334);
            this.txtDifIcm.Name = "txtDifIcm";
            this.txtDifIcm.Size = new System.Drawing.Size(64, 20);
            this.txtDifIcm.TabIndex = 45;
            // 
            // lblDifIcm
            // 
            this.lblDifIcm.AutoSize = true;
            this.lblDifIcm.Location = new System.Drawing.Point(51, 337);
            this.lblDifIcm.Name = "lblDifIcm";
            this.lblDifIcm.Size = new System.Drawing.Size(48, 13);
            this.lblDifIcm.TabIndex = 44;
            this.lblDifIcm.Text = "Dif. ICM:";
            // 
            // txtValorContabil
            // 
            this.txtValorContabil.Enabled = false;
            this.txtValorContabil.Location = new System.Drawing.Point(672, 299);
            this.txtValorContabil.Name = "txtValorContabil";
            this.txtValorContabil.Size = new System.Drawing.Size(111, 20);
            this.txtValorContabil.TabIndex = 43;
            // 
            // lblValorContabil
            // 
            this.lblValorContabil.AutoSize = true;
            this.lblValorContabil.Location = new System.Drawing.Point(587, 302);
            this.lblValorContabil.Name = "lblValorContabil";
            this.lblValorContabil.Size = new System.Drawing.Size(75, 13);
            this.lblValorContabil.TabIndex = 42;
            this.lblValorContabil.Text = "Valor Contábil:";
            // 
            // txtDescricaoNaturezaOp
            // 
            this.txtDescricaoNaturezaOp.Enabled = false;
            this.txtDescricaoNaturezaOp.Location = new System.Drawing.Point(196, 295);
            this.txtDescricaoNaturezaOp.Name = "txtDescricaoNaturezaOp";
            this.txtDescricaoNaturezaOp.Size = new System.Drawing.Size(347, 20);
            this.txtDescricaoNaturezaOp.TabIndex = 41;
            // 
            // txtCodNaturezaOp
            // 
            this.txtCodNaturezaOp.Enabled = false;
            this.txtCodNaturezaOp.Location = new System.Drawing.Point(116, 295);
            this.txtCodNaturezaOp.Name = "txtCodNaturezaOp";
            this.txtCodNaturezaOp.Size = new System.Drawing.Size(64, 20);
            this.txtCodNaturezaOp.TabIndex = 40;
            // 
            // lblNaturezaOperacao
            // 
            this.lblNaturezaOperacao.AutoSize = true;
            this.lblNaturezaOperacao.Location = new System.Drawing.Point(51, 298);
            this.lblNaturezaOperacao.Name = "lblNaturezaOperacao";
            this.lblNaturezaOperacao.Size = new System.Drawing.Size(59, 13);
            this.lblNaturezaOperacao.TabIndex = 39;
            this.lblNaturezaOperacao.Text = "Nat. Oper.:";
            // 
            // txtDescricaoPlanoPgto
            // 
            this.txtDescricaoPlanoPgto.Enabled = false;
            this.txtDescricaoPlanoPgto.Location = new System.Drawing.Point(196, 256);
            this.txtDescricaoPlanoPgto.Name = "txtDescricaoPlanoPgto";
            this.txtDescricaoPlanoPgto.Size = new System.Drawing.Size(347, 20);
            this.txtDescricaoPlanoPgto.TabIndex = 38;
            // 
            // txtCodigoPlanoPgto
            // 
            this.txtCodigoPlanoPgto.Enabled = false;
            this.txtCodigoPlanoPgto.Location = new System.Drawing.Point(116, 256);
            this.txtCodigoPlanoPgto.Name = "txtCodigoPlanoPgto";
            this.txtCodigoPlanoPgto.Size = new System.Drawing.Size(64, 20);
            this.txtCodigoPlanoPgto.TabIndex = 37;
            // 
            // lblPlanoPgto
            // 
            this.lblPlanoPgto.AutoSize = true;
            this.lblPlanoPgto.Location = new System.Drawing.Point(48, 259);
            this.lblPlanoPgto.Name = "lblPlanoPgto";
            this.lblPlanoPgto.Size = new System.Drawing.Size(62, 13);
            this.lblPlanoPgto.TabIndex = 36;
            this.lblPlanoPgto.Text = "Plano Pgto:";
            // 
            // lblKg
            // 
            this.lblKg.AutoSize = true;
            this.lblKg.Location = new System.Drawing.Point(850, 216);
            this.lblKg.Name = "lblKg";
            this.lblKg.Size = new System.Drawing.Size(20, 13);
            this.lblKg.TabIndex = 35;
            this.lblKg.Text = "Kg";
            // 
            // txtPeso
            // 
            this.txtPeso.Enabled = false;
            this.txtPeso.Location = new System.Drawing.Point(780, 213);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(64, 20);
            this.txtPeso.TabIndex = 34;
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Location = new System.Drawing.Point(749, 216);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(34, 13);
            this.lblPeso.TabIndex = 33;
            this.lblPeso.Text = "Peso:";
            // 
            // txtVia
            // 
            this.txtVia.Enabled = false;
            this.txtVia.Location = new System.Drawing.Point(672, 213);
            this.txtVia.Name = "txtVia";
            this.txtVia.Size = new System.Drawing.Size(64, 20);
            this.txtVia.TabIndex = 32;
            // 
            // lblVia
            // 
            this.lblVia.AutoSize = true;
            this.lblVia.Location = new System.Drawing.Point(637, 216);
            this.lblVia.Name = "lblVia";
            this.lblVia.Size = new System.Drawing.Size(25, 13);
            this.lblVia.TabIndex = 31;
            this.lblVia.Text = "Via:";
            // 
            // txtNomeTransportadora
            // 
            this.txtNomeTransportadora.Enabled = false;
            this.txtNomeTransportadora.Location = new System.Drawing.Point(196, 213);
            this.txtNomeTransportadora.Name = "txtNomeTransportadora";
            this.txtNomeTransportadora.Size = new System.Drawing.Size(347, 20);
            this.txtNomeTransportadora.TabIndex = 30;
            // 
            // txtCodigoTransportadora
            // 
            this.txtCodigoTransportadora.Enabled = false;
            this.txtCodigoTransportadora.Location = new System.Drawing.Point(116, 213);
            this.txtCodigoTransportadora.Name = "txtCodigoTransportadora";
            this.txtCodigoTransportadora.Size = new System.Drawing.Size(64, 20);
            this.txtCodigoTransportadora.TabIndex = 29;
            // 
            // lblTransportadora
            // 
            this.lblTransportadora.AutoSize = true;
            this.lblTransportadora.Location = new System.Drawing.Point(28, 216);
            this.lblTransportadora.Name = "lblTransportadora";
            this.lblTransportadora.Size = new System.Drawing.Size(82, 13);
            this.lblTransportadora.TabIndex = 28;
            this.lblTransportadora.Text = "Transportadora:";
            // 
            // lblTraco
            // 
            this.lblTraco.AutoSize = true;
            this.lblTraco.Location = new System.Drawing.Point(377, 178);
            this.lblTraco.Name = "lblTraco";
            this.lblTraco.Size = new System.Drawing.Size(10, 13);
            this.lblTraco.TabIndex = 27;
            this.lblTraco.Text = "-";
            // 
            // txtEstadoCliente
            // 
            this.txtEstadoCliente.Enabled = false;
            this.txtEstadoCliente.Location = new System.Drawing.Point(393, 175);
            this.txtEstadoCliente.Name = "txtEstadoCliente";
            this.txtEstadoCliente.Size = new System.Drawing.Size(54, 20);
            this.txtEstadoCliente.TabIndex = 26;
            // 
            // txtCidadeCliente
            // 
            this.txtCidadeCliente.Enabled = false;
            this.txtCidadeCliente.Location = new System.Drawing.Point(196, 175);
            this.txtCidadeCliente.Name = "txtCidadeCliente";
            this.txtCidadeCliente.Size = new System.Drawing.Size(175, 20);
            this.txtCidadeCliente.TabIndex = 25;
            // 
            // txtCGC
            // 
            this.txtCGC.Enabled = false;
            this.txtCGC.Location = new System.Drawing.Point(672, 152);
            this.txtCGC.Name = "txtCGC";
            this.txtCGC.Size = new System.Drawing.Size(140, 20);
            this.txtCGC.TabIndex = 24;
            // 
            // lblCGC
            // 
            this.lblCGC.AutoSize = true;
            this.lblCGC.Location = new System.Drawing.Point(630, 155);
            this.lblCGC.Name = "lblCGC";
            this.lblCGC.Size = new System.Drawing.Size(32, 13);
            this.lblCGC.TabIndex = 23;
            this.lblCGC.Text = "CGC:";
            // 
            // txtNomeCliente
            // 
            this.txtNomeCliente.Enabled = false;
            this.txtNomeCliente.Location = new System.Drawing.Point(196, 149);
            this.txtNomeCliente.Name = "txtNomeCliente";
            this.txtNomeCliente.Size = new System.Drawing.Size(347, 20);
            this.txtNomeCliente.TabIndex = 22;
            // 
            // txtCodigoCliente
            // 
            this.txtCodigoCliente.Enabled = false;
            this.txtCodigoCliente.Location = new System.Drawing.Point(116, 149);
            this.txtCodigoCliente.Name = "txtCodigoCliente";
            this.txtCodigoCliente.Size = new System.Drawing.Size(64, 20);
            this.txtCodigoCliente.TabIndex = 21;
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Location = new System.Drawing.Point(50, 152);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(42, 13);
            this.lblCliente.TabIndex = 20;
            this.lblCliente.Text = "Cliente:";
            // 
            // txtNomeVendedor
            // 
            this.txtNomeVendedor.Enabled = false;
            this.txtNomeVendedor.Location = new System.Drawing.Point(752, 111);
            this.txtNomeVendedor.Name = "txtNomeVendedor";
            this.txtNomeVendedor.Size = new System.Drawing.Size(175, 20);
            this.txtNomeVendedor.TabIndex = 19;
            // 
            // txtCodigoVendedor
            // 
            this.txtCodigoVendedor.Enabled = false;
            this.txtCodigoVendedor.Location = new System.Drawing.Point(672, 111);
            this.txtCodigoVendedor.Name = "txtCodigoVendedor";
            this.txtCodigoVendedor.Size = new System.Drawing.Size(64, 20);
            this.txtCodigoVendedor.TabIndex = 18;
            // 
            // lblVendedor
            // 
            this.lblVendedor.AutoSize = true;
            this.lblVendedor.Location = new System.Drawing.Point(606, 114);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(56, 13);
            this.lblVendedor.TabIndex = 17;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // txtNomeRepresentante
            // 
            this.txtNomeRepresentante.Enabled = false;
            this.txtNomeRepresentante.Location = new System.Drawing.Point(262, 111);
            this.txtNomeRepresentante.Name = "txtNomeRepresentante";
            this.txtNomeRepresentante.Size = new System.Drawing.Size(281, 20);
            this.txtNomeRepresentante.TabIndex = 16;
            // 
            // txtDigitoRepresentante
            // 
            this.txtDigitoRepresentante.Enabled = false;
            this.txtDigitoRepresentante.Location = new System.Drawing.Point(208, 111);
            this.txtDigitoRepresentante.Name = "txtDigitoRepresentante";
            this.txtDigitoRepresentante.Size = new System.Drawing.Size(35, 20);
            this.txtDigitoRepresentante.TabIndex = 15;
            // 
            // txtRepresentante
            // 
            this.txtRepresentante.Enabled = false;
            this.txtRepresentante.Location = new System.Drawing.Point(116, 111);
            this.txtRepresentante.Name = "txtRepresentante";
            this.txtRepresentante.Size = new System.Drawing.Size(86, 20);
            this.txtRepresentante.TabIndex = 14;
            // 
            // lblRepresentante
            // 
            this.lblRepresentante.AutoSize = true;
            this.lblRepresentante.Location = new System.Drawing.Point(30, 114);
            this.lblRepresentante.Name = "lblRepresentante";
            this.lblRepresentante.Size = new System.Drawing.Size(80, 13);
            this.lblRepresentante.TabIndex = 13;
            this.lblRepresentante.Text = "Representante:";
            // 
            // txtDataNota
            // 
            this.txtDataNota.Enabled = false;
            this.txtDataNota.Location = new System.Drawing.Point(672, 78);
            this.txtDataNota.Name = "txtDataNota";
            this.txtDataNota.Size = new System.Drawing.Size(100, 20);
            this.txtDataNota.TabIndex = 12;
            // 
            // lblDataNota
            // 
            this.lblDataNota.AutoSize = true;
            this.lblDataNota.Location = new System.Drawing.Point(603, 81);
            this.lblDataNota.Name = "lblDataNota";
            this.lblDataNota.Size = new System.Drawing.Size(59, 13);
            this.lblDataNota.TabIndex = 11;
            this.lblDataNota.Text = "Data Nota:";
            // 
            // txtDivisao
            // 
            this.txtDivisao.Enabled = false;
            this.txtDivisao.Location = new System.Drawing.Point(377, 74);
            this.txtDivisao.Name = "txtDivisao";
            this.txtDivisao.Size = new System.Drawing.Size(86, 20);
            this.txtDivisao.TabIndex = 10;
            // 
            // lblDivisao
            // 
            this.lblDivisao.AutoSize = true;
            this.lblDivisao.Location = new System.Drawing.Point(328, 81);
            this.lblDivisao.Name = "lblDivisao";
            this.lblDivisao.Size = new System.Drawing.Size(45, 13);
            this.lblDivisao.TabIndex = 9;
            this.lblDivisao.Text = "Divisão:";
            // 
            // txtFilial
            // 
            this.txtFilial.Enabled = false;
            this.txtFilial.Location = new System.Drawing.Point(116, 67);
            this.txtFilial.Name = "txtFilial";
            this.txtFilial.Size = new System.Drawing.Size(86, 20);
            this.txtFilial.TabIndex = 8;
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Location = new System.Drawing.Point(61, 74);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(30, 13);
            this.lblFilial.TabIndex = 7;
            this.lblFilial.Text = "Filial:";
            // 
            // txtNF
            // 
            this.txtNF.Location = new System.Drawing.Point(672, 31);
            this.txtNF.MaxLength = 13;
            this.txtNF.Name = "txtNF";
            this.txtNF.Size = new System.Drawing.Size(140, 20);
            this.txtNF.TabIndex = 6;
            this.txtNF.TextChanged += new System.EventHandler(this.txtNF_TextChanged);
            this.txtNF.Leave += new System.EventHandler(this.txtNF_Leave);
            // 
            // lblNF
            // 
            this.lblNF.AutoSize = true;
            this.lblNF.Location = new System.Drawing.Point(603, 34);
            this.lblNF.Name = "lblNF";
            this.lblNF.Size = new System.Drawing.Size(63, 13);
            this.lblNF.TabIndex = 5;
            this.lblNF.Text = "Nota Fiscal:";
            // 
            // txtDigitoPedido
            // 
            this.txtDigitoPedido.Location = new System.Drawing.Point(510, 31);
            this.txtDigitoPedido.Name = "txtDigitoPedido";
            this.txtDigitoPedido.ReadOnly = true;
            this.txtDigitoPedido.Size = new System.Drawing.Size(33, 20);
            this.txtDigitoPedido.TabIndex = 4;
            this.txtDigitoPedido.Text = "0";
            // 
            // txtPedido
            // 
            this.txtPedido.Location = new System.Drawing.Point(377, 31);
            this.txtPedido.MaxLength = 18;
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.Size = new System.Drawing.Size(111, 20);
            this.txtPedido.TabIndex = 3;
            this.txtPedido.TextChanged += new System.EventHandler(this.txtPedido_TextChanged);
            this.txtPedido.Leave += new System.EventHandler(this.txtPedido_Leave);
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Location = new System.Drawing.Point(328, 34);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(43, 13);
            this.lblPedido.TabIndex = 2;
            this.lblPedido.Text = "Pedido:";
            // 
            // cmbDeposito
            // 
            this.cmbDeposito.DisplayMember = "COD_LOJA_EXIBICAO";
            this.cmbDeposito.Enabled = false;
            this.cmbDeposito.FormattingEnabled = true;
            this.cmbDeposito.Location = new System.Drawing.Point(116, 31);
            this.cmbDeposito.Name = "cmbDeposito";
            this.cmbDeposito.Size = new System.Drawing.Size(169, 21);
            this.cmbDeposito.TabIndex = 1;
            this.cmbDeposito.ValueMember = "COD_LOJA";
            // 
            // lblDeposito
            // 
            this.lblDeposito.AutoSize = true;
            this.lblDeposito.Location = new System.Drawing.Point(61, 34);
            this.lblDeposito.Name = "lblDeposito";
            this.lblDeposito.Size = new System.Drawing.Size(52, 13);
            this.lblDeposito.TabIndex = 0;
            this.lblDeposito.Text = "Depósito:";
            // 
            // gbBotoes
            // 
            this.gbBotoes.Controls.Add(this.btnSair);
            this.gbBotoes.Controls.Add(this.btnLimpar);
            this.gbBotoes.Controls.Add(this.btnConfirmar);
            this.gbBotoes.Location = new System.Drawing.Point(595, 478);
            this.gbBotoes.Name = "gbBotoes";
            this.gbBotoes.Size = new System.Drawing.Size(362, 72);
            this.gbBotoes.TabIndex = 1;
            this.gbBotoes.TabStop = false;
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSair.Location = new System.Drawing.Point(237, 10);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(107, 55);
            this.btnSair.TabIndex = 2;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpar.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar.Image")));
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLimpar.Location = new System.Drawing.Point(130, 10);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(101, 55);
            this.btnLimpar.TabIndex = 1;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnConfirmar.Location = new System.Drawing.Point(7, 10);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(117, 55);
            this.btnConfirmar.TabIndex = 0;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            // 
            // frmCancelamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(988, 571);
            this.Controls.Add(this.gbBotoes);
            this.Controls.Add(this.gbPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmCancelamento";
            this.Text = "Cancelamento Nota Fiscal";
            this.Load += new System.EventHandler(this.frmCancelamento_Load);
            this.gbPrincipal.ResumeLayout(false);
            this.gbPrincipal.PerformLayout();
            this.gbBotoes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbPrincipal;
        private System.Windows.Forms.Label lblDeposito;
        private System.Windows.Forms.ComboBox cmbDeposito;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.TextBox txtDigitoPedido;
        private System.Windows.Forms.TextBox txtNF;
        private System.Windows.Forms.Label lblNF;
        private System.Windows.Forms.TextBox txtDivisao;
        private System.Windows.Forms.Label lblDivisao;
        private System.Windows.Forms.TextBox txtFilial;
        private System.Windows.Forms.Label lblFilial;
        private System.Windows.Forms.TextBox txtDataNota;
        private System.Windows.Forms.Label lblDataNota;
        private System.Windows.Forms.TextBox txtRepresentante;
        private System.Windows.Forms.Label lblRepresentante;
        private System.Windows.Forms.TextBox txtDigitoRepresentante;
        private System.Windows.Forms.TextBox txtNomeRepresentante;
        private System.Windows.Forms.TextBox txtNomeVendedor;
        private System.Windows.Forms.TextBox txtCodigoVendedor;
        private System.Windows.Forms.Label lblVendedor;
        private System.Windows.Forms.TextBox txtNomeCliente;
        private System.Windows.Forms.TextBox txtCodigoCliente;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.TextBox txtCGC;
        private System.Windows.Forms.Label lblCGC;
        private System.Windows.Forms.TextBox txtEstadoCliente;
        private System.Windows.Forms.TextBox txtCidadeCliente;
        private System.Windows.Forms.Label lblTraco;
        private System.Windows.Forms.TextBox txtNomeTransportadora;
        private System.Windows.Forms.TextBox txtCodigoTransportadora;
        private System.Windows.Forms.Label lblTransportadora;
        private System.Windows.Forms.TextBox txtVia;
        private System.Windows.Forms.Label lblVia;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblKg;
        private System.Windows.Forms.TextBox txtDescricaoPlanoPgto;
        private System.Windows.Forms.TextBox txtCodigoPlanoPgto;
        private System.Windows.Forms.Label lblPlanoPgto;
        private System.Windows.Forms.TextBox txtDescricaoNaturezaOp;
        private System.Windows.Forms.TextBox txtCodNaturezaOp;
        private System.Windows.Forms.Label lblNaturezaOperacao;
        private System.Windows.Forms.Label lblValorContabil;
        private System.Windows.Forms.TextBox txtValorContabil;
        private System.Windows.Forms.TextBox txtDifIcm;
        private System.Windows.Forms.Label lblDifIcm;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.TextBox txtAcrescimo;
        private System.Windows.Forms.Label lblAcrescimo;
        private System.Windows.Forms.TextBox txtItens;
        private System.Windows.Forms.Label lblItens;
        private System.Windows.Forms.Label lblMensagemAviso;
        private System.Windows.Forms.TextBox txtMensagemAviso;
        private System.Windows.Forms.TextBox txtMensagemNota;
        private System.Windows.Forms.Label lblMensagemNota;
        private System.Windows.Forms.TextBox txtDescCancelamento;
        private System.Windows.Forms.TextBox txtCodCancelamento;
        private System.Windows.Forms.GroupBox gbBotoes;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Label label1;
    }
}