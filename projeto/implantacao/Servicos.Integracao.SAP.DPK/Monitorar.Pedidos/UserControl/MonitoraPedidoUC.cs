﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Business;
using Entities;
using UtilGeralDPA;

namespace MonitorarPedidos
{
    public enum TipoUserControl
    {
        PedidosProcessados = 1,
        PedidosPendentes = 2,
        PedidosNaoCancelados = 3
    }

    public partial class MonitoraPedidoUC : UserControl, IMessageDPA
    {
        private bool _EmLiberacao;
        private int codLojaPesquisa;
        private CentroDistribuicaoBE _CentroBase;
        WorkerDPA uk;

        public delegate void ExecutarRotina(WorkerDPA uk, object filtroIn);

        public event ExecutarRotina ExecutaRotina;

        public TipoUserControl TipoUC { get; set; }

        public object FiltroIn { get; set; }

        public MonitoraPedidoUC()
        {
            InitializeComponent();
        }

        public MonitoraPedidoUC(TipoUserControl tipo, CentroDistribuicaoBE cd)
        {
            InitializeComponent();
            _CentroBase = cd;

            dgvPedidos.AutoGenerateColumns = false;
            uk = new WorkerDPA(this);

            uk.Rotina += new WorkerDPA.ExecutarRotina(Executa);

            TipoUC = tipo;


            lblMensagem.Text = String.Empty;

            TrataUserControls();

        }

        private void TrataUserControls()
        {
            switch (TipoUC)
            {
                case TipoUserControl.PedidosProcessados:
                    pnlFiltro.Visible = true;
                    pnlRefreshAutomatico.Visible = false;
                    dgvPedidos.Columns[7].Visible = false;
                    break;

                case TipoUserControl.PedidosPendentes:
                    //não visualiza o filtro e muda a visibilidade da coluna "LIBERAR"
                    pnlFiltro.Visible = false;
                    pnlRefreshAutomatico.Visible = true;
                    dgvPedidos.Columns[7].Visible = true;
                    btnHabilitaRefresh.Text = ResourceMensagemMonitor.PararRefreshAutomatico;
                    break;

                case TipoUserControl.PedidosNaoCancelados:
                    ddlCd.Visible = false;
                    label2.Visible = false;
                    pnlRefreshAutomatico.Visible = false;
                    dgvPedidos.Columns[7].Visible = false;  
                    break;
                default:       
                    break;
            }

            txtDataFim.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtDataInicio.Text = DateTime.Now.ToString("dd/MM/yyyy");
            _RefreshAutomatico = true;
        }

        public void addMessageComplete(string texto)
        {

            ((frmPrincipal)this.Parent.Parent.Parent)._EmExecucao = false;
        }

        private void ExibeMensagem(string texto, Color cor)
        {
            lblMensagem.Text = texto;
            lblMensagem.ForeColor = cor;
        }

        public void addMessage(BuscaPedidoOut objMensagem)
        {
            if (objMensagem.Status == StatusBuscaPedidoOut.Processando)
            {
                ExibeMensagem(objMensagem.Mensagem, Color.Black);
                HabilitaBotoes(false);
            }
            else if (objMensagem.Status == StatusBuscaPedidoOut.Sucesso)
            {
                foreach (PedidoDH item in objMensagem.ListPedidosDH)
                {
                    PreencheDataGridView(item);
                }
                ExibeMensagem(objMensagem.Mensagem, Color.Black);
                HabilitaBotoes(true);
            }
            else if (objMensagem.Status == StatusBuscaPedidoOut.Erro)
            {
                ExibeMensagem(objMensagem.Mensagem, Color.Red);
                HabilitaBotoes(true);
            }
        }
        private void HabilitaBotoes(bool habilita)
        {
            btnHabilitaRefresh.Enabled = habilita;
            btnBuscar.Enabled = habilita;
        }

        public void CarregaComboCD(System.Collections.Generic.List<CentroDistribuicaoBE> list)
        {
            foreach (CentroDistribuicaoBE item in list)
            {
                ddlCd.Items.Add(item);
            }

            if (list.Count == 0)
            {
                ddlCd.Items.Add(_CentroBase);
                ddlCd.SelectedItem = _CentroBase;
            }
            else
                ddlCd.SelectedItem = list[0];

        }

        public void Start()
        {

            try
            {
                if (!_EmLiberacao && _RefreshAutomatico)
                {
                    lblMensagem.Text = String.Empty;
                    dgvPedidos.Rows.Clear();
                    ((frmPrincipal)this.Parent.Parent.Parent)._EmExecucao = true;
                    uk.execute();
                }
            }
            catch (Exception ex)
            {
                lblMensagem.Text = ex.Message;
            }
        }

        protected void Executa(BackgroundWorker g, object filtroIn)
        {
            FiltroBuscaPedidoIn f = new FiltroBuscaPedidoIn();
            CultureInfo culture = new CultureInfo("pt-BR");

            switch (TipoUC)
            {
                case TipoUserControl.PedidosProcessados:
                    f.CodLoja = codLojaPesquisa;
                    f.DataInicio = Convert.ToDateTime(txtDataInicio.Text, culture);
                    f.DataFim = Convert.ToDateTime(txtDataFim.Text, culture);
                    f.Status = ((char)EnumeradoresAplicacao.StatusPedido.Sucesso).ToString();
                    break;

                case TipoUserControl.PedidosPendentes:
                    string[] status = ConfigurationManager.AppSettings["statusMonitoramentoPendentes"].ToString().Split(',');
                    f.CodLoja = _CentroBase.COD_LOJA;
                    f.ListStatus = status;
                    break;

                case TipoUserControl.PedidosNaoCancelados:
                    f.CodLoja = _CentroBase.COD_LOJA;
                    f.DataInicio = Convert.ToDateTime(txtDataInicio.Text, culture);
                    f.DataFim = Convert.ToDateTime(txtDataFim.Text, culture);
                    f.Status = ((char)EnumeradoresAplicacao.StatusPedido.Cancelamento_Negado).ToString();
                    break;
                default:
                    break;
            }

            f.CodLojaBase = _CentroBase.COD_LOJA;
            ExecutaRotina(uk, f);
        }

        private void PreencheDataGridView(PedidoDH objMensagem)
        {
            int contador = dgvPedidos.Rows.Add();

            dgvPedidos.Rows[contador].Cells[0].Value = objMensagem.ID_CONTROLE_PEDIDO;
            dgvPedidos.Rows[contador].Cells[1].Value = objMensagem.NUMERO_PEDIDO;
            dgvPedidos.Rows[contador].Cells[2].Value = objMensagem.NOME_CLIENTE;
            dgvPedidos.Rows[contador].Cells[3].Value = objMensagem.NOME_TRANSPORTADORA;
            dgvPedidos.Rows[contador].Cells[4].Value = objMensagem.VALOR.ToString();
            dgvPedidos.Rows[contador].Cells[5].Value = objMensagem.DATA_PEDIDO.ToString();
            dgvPedidos.Rows[contador].Cells[6].Value = objMensagem.STATUS_DESCRICAO.ToString();
            dgvPedidos.Rows[contador].Cells[8].Value = objMensagem.STATUS.ToString();

            if (TipoUC == TipoUserControl.PedidosPendentes)
            {
                dgvPedidos.Rows[contador].Cells[7].Value = ResourceMensagemMonitor.gridLiberar;

                //só deixa liberar caso o status seja Erro
                if (objMensagem.STATUS != ((char)EnumeradoresAplicacao.StatusPedido.Erro_SAP).ToString())
                    dgvPedidos.Rows[contador].Cells[7].ReadOnly = true;
            }
            else
            {


                dgvPedidos.Rows[contador].Cells[1].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);
                dgvPedidos.Rows[contador].Cells[2].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);
                dgvPedidos.Rows[contador].Cells[3].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);
                dgvPedidos.Rows[contador].Cells[4].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);
                dgvPedidos.Rows[contador].Cells[5].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);
                dgvPedidos.Rows[contador].Cells[6].Style.BackColor = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? Color.Yellow : Color.White);

                dgvPedidos.Rows[contador].Cells[1].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);
                dgvPedidos.Rows[contador].Cells[2].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);
                dgvPedidos.Rows[contador].Cells[3].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);
                dgvPedidos.Rows[contador].Cells[4].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);
                dgvPedidos.Rows[contador].Cells[5].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);
                dgvPedidos.Rows[contador].Cells[6].ToolTipText = (objMensagem.TEM_DIF_ESTOQUE.Equals("S") ? ResourceMensagemMonitor.DiferencaQuantidades : String.Empty);


                dgvPedidos.Rows[contador].Cells[7].ReadOnly = true;
                dgvPedidos.Rows[contador].Cells[7].Value = String.Empty;
            }

            dgvPedidos.Sort(dgvPedidos.Columns[1], ListSortDirection.Descending);
        }

        private void dgvPedidos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            _EmLiberacao = true;

            if (e.ColumnIndex == 6)
            {
                string idControlePedido = Convert.ToString(dgvPedidos.Rows[e.RowIndex].Cells[0].Value);

                if (TipoUC == TipoUserControl.PedidosPendentes)
                    codLojaPesquisa = _CentroBase.COD_LOJA;

                frmHistorico frmHistorico = new frmHistorico(idControlePedido, _CentroBase.COD_LOJA, codLojaPesquisa);
                frmHistorico.ShowDialog();
            }
            else if (e.ColumnIndex == 7)
            {
                string status = Convert.ToString(dgvPedidos.Rows[e.RowIndex].Cells[8].Value);
                if (status == ((char)EnumeradoresAplicacao.StatusPedido.Erro_SAP).ToString())
                {
                    if (MessageBox.Show(ResourceMensagemMonitor.ConfirmarOperacao, "Confirmar liberação", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                    {
                        try
                        {
                            PedidoSAP oPedidoSAP = new PedidoSAP();
                            string idControlePedido = Convert.ToString(dgvPedidos.Rows[e.RowIndex].Cells[0].Value);
                            string nomeUsuario = ((frmPrincipal)this.Parent.Parent.Parent)._UsuarioLogado.NOME_USUARIO;

                            oPedidoSAP.AtualizaStatusControlePedido(idControlePedido, ((char)EnumeradoresAplicacao.StatusPedido.Pendente).ToString(), ((char)EnumeradoresAplicacao.StatusPedido.Erro_SAP).ToString());

                            Logger.LogInfo(String.Format(ResourceMensagemMonitor.PedidoLiberadoPeloUsuario, idControlePedido, nomeUsuario));

                            dgvPedidos.Rows.RemoveAt(e.RowIndex);
                        }
                        catch (Exception ex)
                        {
                            Logger.LogError(ex);
                            MessageBox.Show(ResourceMensagemMonitor.ErroLiberacaoPedido);
                        }
                    }
                }
            }
            _EmLiberacao = false;
        }

        private void dgvPedidos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                string idControlePedido = Convert.ToString(dgvPedidos.Rows[e.RowIndex].Cells[0].Value);

                if (TipoUC == TipoUserControl.PedidosPendentes)
                    codLojaPesquisa = _CentroBase.COD_LOJA;

                frmHistorico frmHistorico = new frmHistorico(idControlePedido, _CentroBase.COD_LOJA, codLojaPesquisa);
                frmHistorico.ShowDialog();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {

            codLojaPesquisa = (TipoUC == TipoUserControl.PedidosProcessados ? ((CentroDistribuicaoBE)ddlCd.SelectedItem).COD_LOJA : _CentroBase.COD_LOJA);
            dgvPedidos.Rows.Clear();

            if (ValidaCampos())
                this.Start();
        }

        private bool ValidaCampos()
        {
            DateTime dataInicio = new DateTime();
            DateTime dataFinal = new DateTime();
            string mensagem = String.Empty;

            if (String.IsNullOrEmpty(txtDataInicio.Text.Replace('/', ' ').Trim()))
                mensagem = String.Format(ResourceMensagemMonitor.CampoObrigatorioNaoPreenchido, "Data Início");
            else
                if (!DateTime.TryParse(txtDataInicio.Text, out dataInicio))
                    mensagem += (String.IsNullOrEmpty(mensagem) ? String.Format(ResourceMensagemMonitor.CampoInvalido, "Data Início") : "\r\n" + String.Format(ResourceMensagemMonitor.CampoInvalido, "Data Início"));

            if (String.IsNullOrEmpty(txtDataFim.Text.Replace('/', ' ').Trim()))
                mensagem += (String.IsNullOrEmpty(mensagem) ? String.Format(ResourceMensagemMonitor.CampoObrigatorioNaoPreenchido, "Data Fim") : "\r\n" + String.Format(ResourceMensagemMonitor.CampoObrigatorioNaoPreenchido, "Data Fim"));
            else
                if (!DateTime.TryParse(txtDataFim.Text, out dataFinal))
                    mensagem += (String.IsNullOrEmpty(mensagem) ? String.Format(ResourceMensagemMonitor.CampoInvalido, "Data Fim") : "\r\n" + String.Format(ResourceMensagemMonitor.CampoInvalido, "Data Fim"));



            if (dataInicio.Date > DateTime.MinValue && dataFinal.Date > DateTime.MinValue)
            {
                if (dataFinal.Date < dataInicio.Date)
                {
                    mensagem = String.Format(ResourceMensagemMonitor.CampoMaiorQue, "Data Início", "Data Fim");
                }
            }

            lblMensagem.Text = mensagem;
            lblMensagem.ForeColor = Color.Red;

            return String.IsNullOrEmpty(mensagem);
        }

        private bool _RefreshAutomatico;
        private void btnHabilitaRefresh_Click(object sender, EventArgs e)
        {
            if (_RefreshAutomatico)
            {
                btnHabilitaRefresh.Text = ResourceMensagemMonitor.IniciaRefreshAutomatico;

            }
            else
            {
                btnHabilitaRefresh.Text = ResourceMensagemMonitor.PararRefreshAutomatico;
            }
            _RefreshAutomatico = !_RefreshAutomatico;

        }

        private void MonitoraPedidoUC_Load(object sender, EventArgs e)
        {
            if (TipoUC == TipoUserControl.PedidosPendentes || TipoUC == TipoUserControl.PedidosNaoCancelados)
                this.Start();
        }
    }
}
