﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace MonitorarPedidos
{
    [RunInstaller(true)]
    public partial class MonitorarPedidosInstaller : System.Configuration.Install.Installer
    {
        public MonitorarPedidosInstaller()
        {
            InitializeComponent();
        }
    }
}
