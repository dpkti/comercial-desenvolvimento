﻿using System.Collections.Generic;
using System.Windows.Forms;
using Business;
using Entities;

namespace MonitorarPedidos
{
    public partial class frmHistorico : Form
    {
        public frmHistorico()
        {
            InitializeComponent();

            dgvHistorico.AutoGenerateColumns = false;

        }

        public frmHistorico(string idControlePedido, int codLojaBase, int codLoja)
        {
            InitializeComponent();

            PedidoSAP oPedido = new PedidoSAP();
            List<HistoricoPedidoDH> listHistorico = oPedido.BuscarHistoricoPedido(idControlePedido, codLojaBase, codLoja);

            if (listHistorico.Count > 0)
            {
                CarregaGridView(listHistorico);
                this.Text = "Histórico do pedido: " + listHistorico[0].ID_CONTROLE_PEDIDO;
            }


            dgvHistorico.AutoGenerateColumns = false;

        }
        private void CarregaGridView(List<HistoricoPedidoDH> listHistorico)
        {
            int linha = 0;
            foreach (HistoricoPedidoDH item in listHistorico)
            {
                linha = dgvHistorico.Rows.Add();

                dgvHistorico.Rows[linha].Cells[0].Value = item.DESCRICAO;
                dgvHistorico.Rows[linha].Cells[1].Value = item.DATA_LOG;
            }

        }

    }
}
