﻿using System.ServiceProcess;

namespace ServicoComunicarPedido
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
#if DEBUG
                ComunicarPedido servicoComunicar = new ComunicarPedido();
                servicoComunicar.StartDebug(new string[1]);

                ProcessarNfeStatus servicoProcessarNfeStatus = new ProcessarNfeStatus();
                servicoProcessarNfeStatus.StartDebug(new string[1]);

                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
                ServiceBase[] servicesToRun;
                servicesToRun = new ServiceBase[] { new SincronizarLegado(), new ProcessarNfeStatus() };
                ServiceBase.Run(servicesToRun);
#endif
            }
            else   // codigo original
            {

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                                    {
                                        new ComunicarPedido(),
                                        new ProcessarNfeStatus()
                                    };
                ServiceBase.Run(ServicesToRun);
            }
        }

    }
}
