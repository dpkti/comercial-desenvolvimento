﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Business;
using CommonServico;
using Entities;
using UtilGeralDPA;

namespace ServicoComunicarPedido
{
    [RunInstaller(true)]
    partial class ProcessarNfeStatus : ServiceBase
    {
        public CentroDistribuicaoBE _CentroDistribuicao
        {
            private set;
            get;
        }

        private TimerCustom timerProcessarNfeStatus = null;
        private bool servicoExecutandoNfeStatus = false;

        private TimerCustom timerProcessarExpurgo = null;
        private bool servicoExecutandoExpurgo = false;

        public ProcessarNfeStatus()
        {
            InitializeComponent();
        }

#if DEBUG
        public void StartDebug(string[] args)
        {
            OnStart(args);
        }
#endif

        protected override void OnStart(string[] args)
        {
            int segundosExecucao;
            String segundosParametro;

            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                // Obtém configurações do centro de distribuição.
                _CentroDistribuicao = ServicoBO.ObterCentroDistribuicao();

                // Se não encontrou o CD correto aborta o início do serviço.
                if (_CentroDistribuicao == null)
                {
                    this.Stop();
                    return;
                }

                //cria os diretorios da aplicacao
                if (!ServicoBO.CriarDiretoriosPadroesNfeStatus())
                {
                    this.Stop();
                    return;
                }

                // Serviço NFE STATUS

                segundosParametro = ConfigurationAccess.ObterConfiguracao(Parametros.INTERVALO_EXECUCAO_PROCESSAR_NFESTATUS_SEG);

                if (!int.TryParse(segundosParametro, out segundosExecucao))
                {
                    // se falhar o try parse, valor default para iniciar o serv
                    segundosExecucao = 2;
                }

                Logger.LogInfo(String.Format("O intervalo de execução do serviço ProcessarNfeStatus está parametrizado para {0} segundo(s).", segundosExecucao.ToString()));

                segundosExecucao = segundosExecucao * 1000;

                timerProcessarNfeStatus = new TimerCustom(segundosExecucao);
                timerProcessarNfeStatus.Elapsed += Timer_Tick_NfeStatus;

                iniciarServicoNfeStatus();

                Logger.LogInfo("Serviço - ProcessarNfeStatus - iniciado com sucesso!");


                // Serviço Expurgo

                segundosParametro = ConfigurationAccess.ObterConfiguracao(Parametros.INTERVALO_EXECUCAO_EXPURGO_SEG);

                if (!int.TryParse(segundosParametro, out segundosExecucao))
                {
                    // se falhar o try parse, valor default para iniciar o serv
                    segundosExecucao = 2;
                }

                Logger.LogInfo(String.Format("O intervalo de execução do serviço ProcessarExpurgo está parametrizado para {0} segundo(s).", segundosExecucao.ToString()));

                segundosExecucao = segundosExecucao * 1000;

                timerProcessarExpurgo = new TimerCustom(segundosExecucao);
                timerProcessarExpurgo.Elapsed += Timer_Tick_Expurgo;

                iniciarServicoExpurgo();

                Logger.LogInfo("Serviço - ProcessarExpurgo - iniciado com sucesso!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        private void Timer_Tick_NfeStatus(object sender, ElapsedEventArgs e)
        {
            // Setando cultura da aplicação
            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                if (!servicoExecutandoNfeStatus)
                {
                    pararServicoNfeStatus();

                    ServicoBO.IniciaProcessoNfeStatus(_CentroDistribuicao);

                    iniciarServicoNfeStatus();
                }
                else
                {
                    Logger.LogInfo("O serviço - ProcessarNfeStatus - já está em execução.");
                }
            }
            catch (Exception ex)
            {
                if (servicoExecutandoNfeStatus)
                {
                    iniciarServicoNfeStatus();
                }

                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        /// <summary>
        /// Inicia o timer do serviço e atualiza a variável de controle
        /// </summary>
        private void pararServicoNfeStatus()
        {
            servicoExecutandoNfeStatus = true;
            timerProcessarNfeStatus.Stop();
        }

        /// <summary>
        /// Para o timer do serviõ e atualiza a variável de control
        /// </summary>
        private void iniciarServicoNfeStatus()
        {
            servicoExecutandoNfeStatus = false;
            timerProcessarNfeStatus.Start();
        }

        private void Timer_Tick_Expurgo(object sender, ElapsedEventArgs e)
        {
            // Setando cultura da aplicação
            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                if (!servicoExecutandoExpurgo)
                {
                    pararServicoExpurgo();

                    ServicoBO.IniciaProcessoExpurgo(_CentroDistribuicao);

                    iniciarServicoExpurgo();
                }
                else
                {
                    Logger.LogInfo("O serviço - ProcessarExpurgo - já está em execução.");
                }
            }
            catch (Exception ex)
            {
                if (servicoExecutandoExpurgo)
                {
                    iniciarServicoExpurgo();
                }

                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        /// <summary>
        /// Inicia o timer do serviço e atualiza a variável de controle
        /// </summary>
        private void pararServicoExpurgo()
        {
            servicoExecutandoExpurgo = true;
            timerProcessarExpurgo.Stop();
        }

        /// <summary>
        /// Para o timer do serviõ e atualiza a variável de control
        /// </summary>
        private void iniciarServicoExpurgo()
        {
            servicoExecutandoExpurgo = false;
            timerProcessarExpurgo.Start();
        }
    }
}
