﻿using System.ServiceProcess;
namespace ServicoComunicarPedido
{
    partial class ComunicarPedidoInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            this.serviceInstaller2 = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;
            // 
            // serviceInstaller1
            // 
            this.serviceInstaller1.Description = "Serviço de comunicação de pedidos entre SAP e legado DPK";
            this.serviceInstaller1.DisplayName = "DPK - Comunicação de Pedido SAP";
            this.serviceInstaller1.ServiceName = "DPK_ComunicacaoPedidoSAP";
            //       this.serviceInstaller1.ServicesDependedOn = new string[] {
            //  "DPK - Processamento NFeStatus"};

            this.serviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // serviceInstaller2
            // 
            this.serviceInstaller2.Description = "Serviço de Processamento NFeStatus";
            this.serviceInstaller2.DisplayName = "DPK - Processamento NFeStatus";
            this.serviceInstaller2.ServiceName = "DPK_ProcessamentoNfeStatus";
            this.serviceInstaller2.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ComunicarPedidoInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstaller1,
            this.serviceInstaller1,
            this.serviceInstaller2});

        }

        #endregion

        protected override void OnCommitted(System.Collections.IDictionary savedState)
        {
            new ServiceController(serviceInstaller1.ServiceName).Start();
        }

        //protected override void OnBeforeUninstall(IDictionary savedState)
        //{
        //    base.OnBeforeUninstall(savedState);
        //    new ServiceController(serviceInstaller1.ServiceName).Stop();
        //}

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;
        private ServiceInstaller serviceInstaller2;
    }
}