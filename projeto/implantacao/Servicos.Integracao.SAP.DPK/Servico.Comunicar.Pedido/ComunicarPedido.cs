﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Business;
using CommonServico;
using Entities;
using UtilGeralDPA;

namespace ServicoComunicarPedido
{
    [RunInstaller(true)]
    public partial class ComunicarPedido : ServiceBase
    {
        public CentroDistribuicaoBE _CentroDistribuicao
        {
            private set;
            get;
        }


        /// <summary>
        /// Propriedade para armazenar o tipo do banco de dados do CD.
        /// </summary>
        public EnumeradoresAplicacao.TipoBanco TipoBanco
        {
            private set;
            get;
        }

        public ComunicarPedido()
        {
            InitializeComponent();
        }

#if DEBUG
        public void StartDebug(string[] args)
        {
            OnStart(args);
        }
#endif

        private TimerCustom timer = null;
        private bool servicoExecutando = false;

        protected override void OnStart(string[] args)
        {
            int segundosExecucao;
            String segundosParametro;

            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                // Obtém configurações do centro de distribuição.
                _CentroDistribuicao = ServicoBO.ObterCentroDistribuicao();

                // Se não encontrou o CD correto aborta o início do serviço.
                if (_CentroDistribuicao == null)
                {
                    this.Stop();
                    return;
                }

                // Busca o intervalo, em segundos, no qual o serviço será executado
                segundosParametro = ConfigurationAccess.ObterConfiguracao(Parametros.INTERVALO_EXECUCAO_COMUNICAR_PEDIDO_SEG);

                if (!int.TryParse(segundosParametro, out segundosExecucao))
                {
                    // se falhar o try parse, valor default para iniciar o serv
                    segundosExecucao = 2;
                }

                Logger.LogInfo(String.Format("O intervalo de execução do processo ComunicarPedido está parametrizado para {0} segundo(s).", segundosExecucao.ToString()));

                segundosExecucao = segundosExecucao * 1000;

                timer = new TimerCustom(segundosExecucao);
                timer.Elapsed += Timer_Tick;

                iniciarServico();

                Logger.LogInfo("Serviço - ComunicarPedido - iniciado com sucesso!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }




        private void Timer_Tick(object sender, ElapsedEventArgs e)
        {
            // Setando cultura da aplicação
            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                if (!servicoExecutando)
                {
                    pararServico();

                    ServicoBO.IniciaProcessoComunicacaoPedido(_CentroDistribuicao);

                    iniciarServico();
                }
                else
                {
                    Logger.LogInfo("O serviço - ComunicarPedido - já está em execução.");
                }
            }
            catch (Exception ex)
            {
                if (servicoExecutando)
                {
                    iniciarServico();
                }

                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        /// <summary>
        /// Inicia o timer do serviço e atualiza a variável de controle
        /// </summary>
        private void pararServico()
        {
            servicoExecutando = true;
            timer.Stop();
        }

        /// <summary>
        /// Para o timer do serviõ e atualiza a variável de control
        /// </summary>
        private void iniciarServico()
        {
            servicoExecutando = false;
            timer.Start();
        }


        protected override void OnStop()
        {
        }
    }
}
