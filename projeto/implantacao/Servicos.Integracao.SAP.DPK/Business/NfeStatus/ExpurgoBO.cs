﻿using Entities;
using Business.ServicoVenda;
using System.Collections.Generic;
using System;
using System.Configuration;
using System.Linq;
using Business.Pedido;

namespace Business.NfeStatus
{
    public class ExpurgoBO : BaseBO<PedidoCancelamentoDH>
    {
        private CentroDistribuicaoBE centro;

        public ExpurgoBO(CentroDistribuicaoBE centro)
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.SemArquivo;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.ExpurgoBE;

            this.centro = centro;
        }

        protected override void ProcessarInterno(PedidoCancelamentoDH pedido)
        {

            dtpSD_CancVenda request = new dtpSD_CancVenda();
            PopularRequestCancel(pedido, request);
            base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(request);
            CancelamentoPedidoBO cancelamentoPedido = new CancelamentoPedidoBO(pedido, centro);
            cancelamentoPedido.Processar(request);
            base.processarOut.LOG_GERAL = cancelamentoPedido.processarOut.LOG_GERAL;
        }

        public void PopularRequestCancel(PedidoCancelamentoDH pedido, dtpSD_CancVenda request)
        {
            request.Protocolo = new dtpPI_ItfProtocolo();
            request.Protocolo.Centro = pedido.LOJA_COD_SAP;
            request.Protocolo.Usuario = string.Empty;
            request.Protocolo.Hostname = Environment.MachineName;
            request.Protocolo.NmAplicacao = ConfigurationManager.AppSettings["nomeAplicacaoRequest"].ToString();
            request.Empresa = ObterParametroSistemaPorNomeParametro("EMPRESA").VL_PARAMETRO;
            request.Filial = pedido.LOJA_COD_SAP;
            request.Cliente = pedido.CLIENTE_COD_SAP.ToString();
            request.ReferenciaPOS = new dtpSD_POS();
            request.ReferenciaPOS.NumeroPedido = pedido.ID_CONTROLE_PEDIDO_SAP;
            request.ReferenciaNF = new dtpSD_NF();
            request.ReferenciaNF.TipoNF = ConfigurationManager.AppSettings["TipoNF"].ToString();
            request.ReferenciaNF.FilialOrig = pedido.LOJA_COD_SAP;
            request.ReferenciaNF.NroNfCliente = pedido.NUM_PEDIDO.ToString();
            request.ReferenciaNF.NroNfReferencia = String.Empty;
        }

        /// <summary>
        /// vxavier CiT (11/10/2012) - ObterParametroSistemaPorNomeParametro
        /// Obtem o parametro pelo nome 
        /// </summary>
        /// <param name="nomeParametro">nome do Parametro</param>
        /// <returns></returns>
        public ParametroSistemaBE ObterParametroSistemaPorNomeParametro(string nomeParametro)
        {
            ParametroSistemaBE entity = new ParametroSistemaBE();

            if (centro.ListParametrosSistema != null)
            {
                entity = centro.ListParametrosSistema.Single(item => item.NM_PARAMETRO == nomeParametro);
            }
            return entity;
        }
    }
}
