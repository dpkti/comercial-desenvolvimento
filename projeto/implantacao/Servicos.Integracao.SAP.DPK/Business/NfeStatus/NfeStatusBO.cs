﻿using System;
using System.Linq;
using System.Transactions;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class NfeStatusBO : BaseBO<dtpSD_status_nfe>
    {
        private CentroDistribuicaoBE centroDistribuicao;

        public NfeStatusBO(string nomeArquivo, dtpSD_status_nfe nfeStatus, CentroDistribuicaoBE centroDistribuicao)
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.NfeStatusBE;
            base.processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;
            base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(nfeStatus);

            this.centroDistribuicao = centroDistribuicao;
        }

        protected override void ProcessarInterno(dtpSD_status_nfe nfeStatus)
        {
            if (nfeStatus.Status.Equals(ObtemValorStatusSAP(EnumeradoresAplicacao.StatusNfeSAP.Aprovado)))
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    AtualizarCabecalho(nfeStatus);
                    AtualizarItem(nfeStatus);
                    EfetivarPedido(nfeStatus.Orcamento, nfeStatus.ChaveAcesso, centroDistribuicao.COD_LOJA);

                    ts.Complete();
                }
            }
            else if (nfeStatus.Status.Equals(ObtemValorStatusSAP(EnumeradoresAplicacao.StatusNfeSAP.Recusado)) || nfeStatus.Status.Equals(ObtemValorStatusSAP(EnumeradoresAplicacao.StatusNfeSAP.Rejeitado)))
            {
                CancelarPedido(nfeStatus);
            }
            else
            {
                processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroSapNFE, "Código de status não suportado: " + nfeStatus.Status);
            }
        }

        private void AtualizarItem(dtpSD_status_nfe nfeStatus)
        {
            int count = 1;
            foreach (dtpSD_status_nfeItem item in nfeStatus.Item)
            {
                NfeStatusDALC.Instance.AtualizarItem(nfeStatus.Orcamento, nfeStatus.NumeroNFe, count++, item.ValorRetido, item.Material, Convert.ToInt32(item.Quantidade));
                decimal valorLiquido = item.ValorSubtotal - item.ValorRetido - item.Despesas; // Valor Líquido composto: ISA-845
                NfeStatusDALC.Instance.InsFiscalSaida(nfeStatus.Orcamento, item.Material, valorLiquido, item.ValorRetido, item.ValorIPI, item.Despesas);
            }
        }

        /// <summary>
        /// Método responsável por cancelar o pedido
        /// </summary>
        /// <param name="pedido">pedido a ser cancelado</param>
        /// <remarks>
        /// vxavier CiT - 15/10/12 - [ISA - 823]
        /// </remarks>
        private void CancelarPedido(dtpSD_status_nfe pedido)
        {
            char letraStatus = (char)EnumeradoresAplicacao.StatusPedido.Expurgado;
            processarOut.IncluirLogErro(Entities.EnumeradoresAplicacao.CodigoErro.ErroSapNFE, pedido.Mensagem);
            NfeStatusDALC.Instance.AtualizarControlePedidoStatus(pedido.Orcamento, letraStatus.ToString());
        }

        /// <summary>
        /// Método faz transação e chama a DALC para efetivar o pedido
        /// </summary>
        /// <param name="codLoja">Código da Loja</param>
        /// <param name="codPedido">Código do Pedido</param>
        /// <remarks> oliveira@ - CiT (15/10/2012)
        /// </remarks>
        private void EfetivarPedido(string idControlePedido, string chaveNFE, int codLoja)
        {
            NfeStatusDALC.Instance.EfetivarPedido(idControlePedido, chaveNFE, codLoja);
        }

        private string ObtemValorStatusSAP(EnumeradoresAplicacao.StatusNfeSAP status)
        {
            int valor = 0;

            switch (status)
            {
                case EnumeradoresAplicacao.StatusNfeSAP.Aprovado:
                    valor = 1;
                    break;
                case EnumeradoresAplicacao.StatusNfeSAP.Recusado:
                    valor = 2;
                    break;
                case EnumeradoresAplicacao.StatusNfeSAP.Rejeitado:
                    valor = 3;
                    break;
            }

            return valor.ToString();
        }

        /// <summary>
        /// carlosj CiT (15/10/2012) - Esse método irá atualizar um determinado pedido na tabela PEDNOTA_VENDA com os dados do objeto mtpSD_NfeStatus_req.
        /// </summary>
        /// <param name="nfeItemStatus"></param>
        private void AtualizarCabecalho(dtpSD_status_nfe nfeStatus)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            decimal valorIPI = nfeStatus.Item.Sum(i => i.ValorIPI);
            decimal valorContabil = nfeStatus.Item.Sum(i => i.ValorSubtotal) + valorIPI; // Incluir valor de IPI - ISA-845
            decimal valorBaseMajorada = nfeStatus.Item.Sum(i => i.BaseMajorada);
            decimal valorIcmRetido = nfeStatus.Item.Sum(i => i.ValorRetido);

            NfeStatusDALC.Instance.AtualizarCabecalho(valorIPI, valorContabil, nfeStatus.NumeroNFe, valorBaseMajorada, valorIcmRetido, nfeStatus.Centro, nfeStatus.Orcamento);

            Logger.LogFinalMetodo(dataInicio);

        }

    }
}
