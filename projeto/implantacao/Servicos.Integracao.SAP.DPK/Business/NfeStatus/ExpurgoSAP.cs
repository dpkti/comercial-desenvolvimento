﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using DALC;
using Entities;
using UtilGeralDPA;
namespace Business.NfeStatus
{
    public class ExpurgoSAP
    {
        public void Processar(CentroDistribuicaoBE centroDistribuicao)
        {
            ExpurgoBO expurgoBO;

            List<PedidoCancelamentoDH> listPedidoCancelamentoDH = this.ConsultarPedidosExpurgados(centroDistribuicao.COD_LOJA);

            Int32 codCancel;

            // Obtém o valor de cod. do cancelamento do app.config
            if (!Int32.TryParse(ConfigurationManager.AppSettings["codCancelExpurgo"], out codCancel))
            {
                // Valor Default
                codCancel = 34;
            }

            foreach (var pedido in listPedidoCancelamentoDH)
            {
                expurgoBO = new ExpurgoBO(centroDistribuicao);
                pedido.COD_CANCEL = codCancel;
                expurgoBO.Processar(pedido);
                LogBO.InserirLog(expurgoBO.processarOut);
            }
        }

        private List<PedidoCancelamentoDH> ConsultarPedidosExpurgados(int codLoja)
        {
            List<PedidoCancelamentoDH> listPedidoBE = new List<PedidoCancelamentoDH>();

            try
            {
                listPedidoBE = NfeStatusDALC.Instance.ConsultarPedidosExpurgados(codLoja);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return listPedidoBE;
        }
    }
}
