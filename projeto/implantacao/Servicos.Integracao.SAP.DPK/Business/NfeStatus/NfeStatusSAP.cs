﻿
using System;
using Entities;
using UtilGeralDPA;
namespace Business.NfeStatus
{
    public class NfeStatusSAP : BusinessGenericBase<dtpSD_status_nfe>
    {
        private CentroDistribuicaoBE centroDistribuicao;

        public NfeStatusSAP(CentroDistribuicaoBE CentroDistribuicaoBE)
        {
            // Obtém as configurações de arquivo utilizadas para a classe.
            base.caminhoArquivoEntrada = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_NFESTATUS);
            base.caminhoArquivoProcessado = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_NFESTATUS_PROCESSADOS);
            base.gravarArquivoProcessado = Convert.ToBoolean(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.GRAVAR_ARQUIVOS_NFESTATUS_PROCESSADOS));
            base.caminhoArquivoInvalido = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_NFESTATUS);
            base.prefixoNomenclaturaArquivo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.PREFIXO_ARQUIVO_NFESTATUS);

            this.centroDistribuicao = CentroDistribuicaoBE;
        }

        public override ProcessarOUT Processar(dtpSD_status_nfe nfeStatus, string nomeArquivo)
        {
            NfeStatusBO nfeStatosBo = new NfeStatusBO(nomeArquivo, nfeStatus, centroDistribuicao);

            nfeStatosBo.Processar(nfeStatus);

            if (nfeStatosBo.processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
            {
                Logger.LogInfo(string.Format(ResourceMensagem.ProcessamentoEntidadeRealizadoSucesso, base.entidade.ToString(), nfeStatus.Orcamento));
            }
            else
            {
                Logger.LogInfo(string.Format(ResourceMensagem.ProcessamentoEntidadeComFalha, base.entidade.ToString(), nfeStatus.Orcamento));
            }

            return nfeStatosBo.processarOut;
        }
    }
}
