﻿using System;
using DALC;
using Entities;
using Entities.XSD.Fornecedor;
using UtilGeralDPA;

namespace Business
{
    /// <summary>
    /// Classe responsável pelas regras de negócios específicas do Fornecedor.
    /// </summary>
    /// <remarks>
    /// Criado por: Fabio Pallini - Ci&T
    /// Criado em: 17/09/12
    /// </remarks>
    public class FornecedorBO : BaseBO<dtpMM_Fornecedor>
    {

         /// <summary>
        /// construtor
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <param name="fornecedor"></param>
        public FornecedorBO(string nomeArquivo, dtpMM_Fornecedor fornecedor)
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.FornecedorBE;
            base.processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;
            base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(fornecedor);
        }
        

        protected override void ProcessarInterno(dtpMM_Fornecedor fornecedor)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
          
            bool fornecedorExistente = FornecedorDPKDALC.Instance.VerificarExistenciaFornecedorDPK(fornecedor.Fornecedor);

            if (ValidarRegrasNegocios(fornecedor))
            {
                // Inserção do fornecedor
                if (!fornecedorExistente)
                {
                    FornecedorDPKDALC.Instance.InserirDadosFornecedor(fornecedor);
                    //Logger.LogInfo(String.Format("Fornecedor adicionado, código SAP {0}.", fornecedor.Fornecedor));
                }
                // Atualizar fornecedor existente.
                else
                {
                    FornecedorDPKDALC.Instance.AtualizarDadosFornecedor(fornecedor);
                    //Logger.LogInfo(String.Format("Fornecedor atualizado, código SAP {0}.", fornecedor.Fornecedor));
                }
            }
            else
            {
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            Logger.LogFinalMetodo(dataInicio);

            return;
        }

        private bool ValidarRegrasNegocios(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(fornecedor);

            //Validação do campo BAIRRO
            if (ValidaBairro(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo CNPJ
            if (ValidaCNPJ_CPF(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo CEP
            if (ValidaCEP(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo CtaReconciliacao
            if (ValidaCtaReconciliacao(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo CXPOSTAL
            if (ValidaCxPostal(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo DOMFISCAL
            //if (ValidaDomFiscal(fornecedor) == false)
            //{
            //    retorno = false;
            //}

            //Validação do campo FAX
            if (ValidaFax(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo INSCRESTADUAL
            if (ValidaInscrEstadual(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo NOME
            if (ValidaNome(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo NOME
            //if (ValidaNumAntigoFornecedor(fornecedor) == false)
            //{
            //    retorno = false;
            //}

            //Validação do campo RUA
            if (ValidaRua(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo SETOR
            if (ValidaSetor(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo TELEFONE
            if (ValidaTelefone(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação do campo TERMOPESQUISA
            if (ValidaTermoPesquisa(fornecedor) == false)
            {
                retorno = false;
            }

            //Validação dos campos CARACTERISTICAS
            if (ValidaCaracteristicas(fornecedor) == false)
            {
                retorno = false;
            }

            if (retorno == false)
            {
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo Telefone de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaTelefone(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            bool isDdd;
            bool isTelefone;
            if (!string.IsNullOrEmpty(fornecedor.Telefone))
            {
                if (fornecedor.Telefone.Contains(ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE)))
                {
                    string[] telefonecompleto;
                    Int64 ddd;
                    telefonecompleto = fornecedor.Telefone.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None);
                    if (telefonecompleto.Length > 2)
                    {
                        base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD/Telefone"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                        retorno = false;
                    }
                    else
                    {
                        isDdd = Int64.TryParse(telefonecompleto[0], out ddd);
                        if (isDdd)
                        {
                            if (ddd > 9999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                            retorno = false;
                        }
                        Int64 telefone;
                        isTelefone = Int64.TryParse(telefonecompleto[1], out telefone);
                        if (isTelefone)
                        {
                            if (telefone > 999999999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Telefone"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Telefone"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                            retorno = false;
                        }
                    }

                }
                else
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Telefone"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo TermoPesquisa de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaTermoPesquisa(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.TermoPesquisa))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "TermoPesquisa"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else if (fornecedor.TermoPesquisa.Length > 10)
            {
                fornecedor.TermoPesquisa = fornecedor.TermoPesquisa.Substring(0, 10);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Setor de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaSetor(dtpMM_Fornecedor fornecedor)
        {
            if (fornecedor.Setor == null)
            {
                fornecedor.Setor = "C";
            }
            else
            {
                switch (fornecedor.Setor)
                {
                    case "23": fornecedor.Setor = "A";
                        break;

                    case "30": fornecedor.Setor = "B";
                        break;

                    default: fornecedor.Setor = "C";
                        break;
                }
            }
            return true;
        }

        /// <summary>
        /// método para validar o campo Rua de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaRua(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.Rua))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "Rua"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else if (fornecedor.Rua.Length > 33)
            {
                fornecedor.Rua = fornecedor.Rua.Substring(0, 33);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo NumAntigoFornec de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaNumAntigoFornecedor(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.NumAntigoFornec))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "NumAntigoFornec"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else if (fornecedor.NumAntigoFornec.Length > 3)
            {
               base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "NumAntigoFornec"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Nome de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaNome(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.Nome))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "Nome"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else if (fornecedor.Nome.Length > 33)
            {
                fornecedor.Nome = fornecedor.Nome.Substring(0, 33);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo InscrEstadual de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaInscrEstadual(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.InscrEstadual))
            {
                processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorEmBranco, "InscrEstadual"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                //retorno = false;
            }
            else
            {
                fornecedor.InscrEstadual = UtilGeralDPA.General.RetornaApenasAlfanumericos((fornecedor.InscrEstadual));
                if (fornecedor.InscrEstadual.Length > 14 || (!fornecedor.InscrEstadual.ToUpper().Contains("ISENTO") && FornecedorSAP.ContemLetras(fornecedor.InscrEstadual)))
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto,string.Format(ResourceMensagem.ValorForaDominio, "InscrEstadual"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Fax de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaFax(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            bool isDdd;
            bool isTelefone;
            if (!string.IsNullOrEmpty(fornecedor.Fax))
            {
                if (fornecedor.Fax.Contains(ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE)))
                {
                    string[] telefonecompleto;
                    Int64 ddd;
                    telefonecompleto = fornecedor.Fax.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None); ;
                    if (telefonecompleto.Length > 2)
                    {
                        base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                        retorno = false;
                    }
                    else
                    {
                        isDdd = Int64.TryParse(telefonecompleto[0], out ddd);
                        if (isDdd)
                        {
                            if (ddd > 9999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                            retorno = false;
                        }
                        Int64 telefone;
                        isTelefone = Int64.TryParse(telefonecompleto[1], out telefone);
                        if (isTelefone)
                        {
                            if (telefone > 999999999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                            retorno = false;
                        }
                        if (retorno == true)
                        {
                            fornecedor.Fax = String.Empty;
                            fornecedor.Fax = ddd.ToString() + telefone.ToString();
                        }
                    }

                }
                else
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo DomFiscal de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaDomFiscal(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.DomFiscal))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "DomFiscal"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo CxPostal de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCxPostal(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.CxPostal))
            {
                processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorEmBranco, "CxPostal"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                
                //retorno = false;
            }
            else if (FornecedorSAP.ContemLetras(fornecedor.CxPostal))
            {
                processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto,string.Format(ResourceMensagem.ValorForaDominio,"CxPostal"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                
                retorno = false;
            }
            else
            {
                fornecedor.CxPostal = UtilGeralDPA.General.RetornaApenasAlfanumericos((fornecedor.CxPostal));
                if (fornecedor.CxPostal.Length > 5)
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto,string.Format(ResourceMensagem.ValorForaDominio, "CxPostal"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo CtaReconciliacao de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCtaReconciliacao(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(fornecedor.CtaReconciliacao))
            {

                processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorEmBranco, "CtaReconciliacao"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                //retorno = false;
            }
            else if (FornecedorSAP.ContemLetras(fornecedor.CtaReconciliacao))
            {
                processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorForaDominio, "CtaReconciliacao"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else
            {
                fornecedor.CtaReconciliacao = fornecedor.CtaReconciliacao.TrimEnd();
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo CEP de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCEP(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            if (string.IsNullOrEmpty(fornecedor.CodPostal))
            {
                processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorEmBranco, "CodPostal"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                //retorno = false;
            }
            else if (FornecedorSAP.ContemLetras(fornecedor.CodPostal))
            {
                LogDetalheBE logDetalheBE = new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, "CodPostal", EnumeradoresAplicacao.TipoEntidade.FornecedorBE);

                retorno = false;
            }
            else
            {
                fornecedor.CodPostal = UtilGeralDPA.General.RetornaApenasAlfanumericos((fornecedor.CodPostal.TrimEnd()));
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo CNPJ de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCNPJ_CPF(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            string cnpjTransportadora = fornecedor.CNPJ;
            bool isCPF;
            if (string.IsNullOrEmpty(cnpjTransportadora))
            {
                if (string.IsNullOrEmpty(fornecedor.CPF))
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "CNPJ/CPF"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
                else
                {
                    Int64 cpfTransportadora;
                    isCPF = Int64.TryParse(UtilGeralDPA.General.RetornaApenasAlfanumericos(fornecedor.CPF), out cpfTransportadora);
                    if (isCPF && cpfTransportadora <= 99999999999999)
                    {
                        fornecedor.CPF = cpfTransportadora.ToString();
                    }
                    else
                    {
                        base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "CPF"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                        retorno = false;
                    }
                }
            }
            else
            {
                fornecedor.CNPJ = UtilGeralDPA.General.RetornaApenasAlfanumericos(cnpjTransportadora);
                if (fornecedor.CNPJ.Length > 14 || FornecedorSAP.ContemLetras(fornecedor.CNPJ))
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "CNPJ"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    retorno = false;
                }
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo bairro de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaBairro(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            if (string.IsNullOrEmpty(fornecedor.Bairro))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "Bairro"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (fornecedor.Bairro.Length > 20)
            {
                fornecedor.Bairro = fornecedor.Bairro.Substring(0, 20);
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo caracteristicas de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCaracteristicas(dtpMM_Fornecedor fornecedor)
        {
            bool retorno = true;
            string caracteristica = string.Empty;

            //validação BAIXAR_PEDIDO
            if (string.IsNullOrEmpty(FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_BAIXAR_PEDIDO, fornecedor)))
            {

                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "Bairro"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação DIFERENCA_ICM
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DESCONTO_DIFERENCA_ICM, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "DESCONTO_DIFERENCA_ICM"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("S") || caracteristica.Equals("N")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "DESCONTO_DIFERENCA_ICM"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação DIVISAO
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DIVISAO, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "DIVISAO"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("B") || caracteristica.Equals("D")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "DIVISAO"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação DPK
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DPK, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "DPK"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("S") || caracteristica.Equals("N")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "DPK"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação EDI
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_EDI, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "EDI"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("S") || caracteristica.Equals("N")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "EDI"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação DESCONTO_ADICIONAL
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_PERMITIR_DESCONTO_ADICIONAL, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "PERMITIR_DESCONTO_ADICIONAL"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("S") || caracteristica.Equals("N")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "PERMITIR_DESCONTO_ADICIONAL"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //validação DESCONTO_ADICIONAL
            caracteristica = FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_SITUACAO, fornecedor);
            if (string.IsNullOrEmpty(caracteristica))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "SITUACAO"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }
            else if (!(caracteristica.Equals("0") || caracteristica.Equals("9")))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "SITUACAO"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                retorno = false;
            }

            //if (string.IsNullOrEmpty(FornecedorSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_SIGLA, fornecedor)))
            //{
            //    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "Caracteristica_Fornecedor_SIGLA", EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
            //    
            //    retorno = false;
            //}

            return retorno;
        }
    }
}