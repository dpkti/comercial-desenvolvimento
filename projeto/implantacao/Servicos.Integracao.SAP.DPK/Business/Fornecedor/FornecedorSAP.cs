﻿using System;
using System.Data.Common;
using System.Linq;
using Entities;
using Entities.XSD.Fornecedor;
using UtilGeralDPA;

namespace Business
{
    /// <summary>
    /// Classe utilizada para representar Fornecedor DPK e Transportador DPK.
    /// </summary>
    /// <remarks>
    /// Criado por: Fabio Pallini - Ci&T
    /// Criado em: 17/09/12
    /// </remarks>
    public class FornecedorSAP : BusinessGenericBase<dtpMM_Fornecedor>
    {
        /// <summary>
        /// Construtor.
        /// </summary>
        public FornecedorSAP()
        {
            // Obtém as configurações de arquivo utilizadas para a classe.
            base.caminhoArquivoEntrada = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_FORNECEDOR);
            base.caminhoArquivoProcessado = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_FORNECEDOR_PROCESSADOS);
            base.gravarArquivoProcessado = Convert.ToBoolean(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.GRAVAR_ARQUIVOS_FORNECEDOR_PROCESSADOS));
            base.caminhoArquivoInvalido = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_FORNECEDOR);
            base.prefixoNomenclaturaArquivo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.PREFIXO_ARQUIVO_FORNECEDOR);
        }

        public override ProcessarOUT Processar(dtpMM_Fornecedor fornecedor, string nomeArquivo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            TransportadoraBO transportadoraBO = new TransportadoraBO(nomeArquivo, fornecedor);
            FornecedorBO fornecedorBO = new FornecedorBO(nomeArquivo, fornecedor);
            string fornecedorDPK = GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DPK, fornecedor);

            // Fornecedor
            if (fornecedor.GrpContas == EnumeradoresAplicacao.GrupoContaFornecedor.ZFIS.ToString() || fornecedor.GrpContas == EnumeradoresAplicacao.GrupoContaFornecedor.ZJUR.ToString())
            {
                base.entidade = EnumeradoresAplicacao.TipoEntidade.FornecedorBE;
            }
            // Transportadora
            else if (fornecedor.GrpContas == EnumeradoresAplicacao.GrupoContaFornecedor.ZTPJ.ToString() || fornecedor.GrpContas == EnumeradoresAplicacao.GrupoContaFornecedor.ZTPF.ToString())
            {
                base.entidade = EnumeradoresAplicacao.TipoEntidade.TransportadoraBE;
            }

            // Fornecedor
            if (entidade.Equals(EnumeradoresAplicacao.TipoEntidade.FornecedorBE))
            {
                // Só processa fornecedor se a característica DPK estiver ligada com "S"
                if (fornecedorDPK.ToUpper().Equals("S"))
                {
                    fornecedorBO.Processar(fornecedor);

                    if (fornecedorBO.processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
                    {
                        Logger.LogInfo("Fornecedor DPK " + fornecedor.Fornecedor + " processado com sucesso.");
                    }
                    else
                    {
                        Logger.LogInfo("Fornecedor DPK " + fornecedor.Fornecedor + " não processado com sucesso devido a informações incorretas no XML.");
                    }
                }
                else
                {
                    fornecedorBO.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ArquivoIgnorado, "Caracteristica/DPK = N"), EnumeradoresAplicacao.TipoEntidade.FornecedorBE);
                    Logger.LogWarning("Fornecedor " + fornecedor.Fornecedor + " não é fornecedor DPK e será ignorado.");
                }

                return fornecedorBO.processarOut;

            }
            // Transportadora
            else if (entidade.Equals(EnumeradoresAplicacao.TipoEntidade.TransportadoraBE))
            {

                // Só processa fornecedor se a característica DPK estiver ligada com "S"
                if (fornecedorDPK.ToUpper().Equals("S"))
                {

                    transportadoraBO.Processar(fornecedor);

                    if (transportadoraBO.processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
                    {
                        Logger.LogInfo("Transportadora DPK " + fornecedor.Fornecedor + " processado com sucesso.");
                    }
                    else
                    {
                        Logger.LogInfo("Transportadora DPK " + fornecedor.Fornecedor + " não processado com sucesso devido a informações incorretas no XML.");
                    }
                }
                else
                {
                    transportadoraBO.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ArquivoIgnorado, "Caracteristica/DPK = N"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                    Logger.LogInfo("Transportadora " + fornecedor.Fornecedor + " não é fornecedor DPK e será ignorado.");
                }

                return transportadoraBO.processarOut;
            }
            else
            {
                string msg = "Erro ao tratar grupo de contas - " + fornecedor.GrpContas;
                transportadoraBO.processarOut.IncluirLogAviso(msg, EnumeradoresAplicacao.TipoEntidade.Indefinido);
                return transportadoraBO.processarOut;
            }

        }


        /// <summary>
        /// Método responsável por obter um valor de caracteristica de um fornecedor pelo seu nome.
        /// </summary>
        /// <param name="nomeCaracteristica">Nome da característica.</param>
        /// <param name="fornecedor">Objeto fornecedor.</param>
        /// <returns>Valor em texto da característica.</returns>
        public static string GetValorCaracteristica(string nomeCaracteristica, dtpMM_Fornecedor fornecedor)
        {
            string retorno = string.Empty;

            if (fornecedor.Caracteristicas != null)
            {
                retorno = (from c in fornecedor.Caracteristicas
                           where c != null &&
                                 c.Fornecedor != null &&
                                 c.Nome != null &&
                                 c.Valor != null &&
                                 c.Nome.Equals(nomeCaracteristica) &&
                                 c.Fornecedor.Equals(fornecedor.Fornecedor)
                           select c.Valor.Trim()).FirstOrDefault();
                if (retorno == null)
                {
                    retorno = string.Empty;
                }

            }
            return retorno;
        }

        public static bool ContemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }
    }
}