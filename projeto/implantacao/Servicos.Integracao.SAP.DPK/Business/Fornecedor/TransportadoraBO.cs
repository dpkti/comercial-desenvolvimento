﻿using System;
using DALC;
using Entities;
using Entities.XSD.Fornecedor;
using UtilGeralDPA;

namespace Business
{
    class TransportadoraBO : BaseBO<dtpMM_Fornecedor>
    {

        /// <summary>
        /// construtor
        /// </summary>
        /// <param name="nomeArquivo"></param>
        /// <param name="transportadora"></param>
        public TransportadoraBO(string nomeArquivo, dtpMM_Fornecedor transportadora)
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.TransportadoraBE;
            base.processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;
            base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(transportadora);
        }


        /// <summary>
        /// Classe responsável pelas regras de negócios específicas da Transportadora.
        /// </summary>
        /// <remarks>
        /// Criado por: vxavier - Ci&T
        /// Criado em: 21/09/12
        /// </remarks>
        protected override void ProcessarInterno(dtpMM_Fornecedor transportadora)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            ValidarRegrasNegocio(transportadora);

            if (base.processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
            {
                bool transportadoraExistente = TransportadoraDPKDALC.Instance.VerificarExistenciaTransportadoraDPK(transportadora.Fornecedor);

                // Inserção da transportadora
                if (!transportadoraExistente)
                {
                    TransportadoraDPKDALC.Instance.InserirDadosTransportadora(transportadora);
                    //Logger.LogInfo(String.Format("Transportadora adicionado, código SAP {0}.", transportadora.Fornecedor));
                }
                // Atualizar fornecedor existente.
                else
                {
                    TransportadoraDPKDALC.Instance.AtualizarDadosTransportadora(transportadora);
                    //Logger.LogInfo(String.Format("Transportadora atualizado, código SAP {0}.", transportadora.Fornecedor));
                }
            }
            else
            {
                //vxavier 24/09/2012
                Logger.LogInfo(String.Format("Cadastro NÃO atualizado para Transportador SAP {0} devido a inconsistências nos dados da transportadora.", transportadora.Fornecedor));
            }
            Logger.LogFinalMetodo(dataInicio);

            // return processarOUT;

        }

        private void ValidarRegrasNegocio(dtpMM_Fornecedor transportadora)
        {
            //bool retorno = true;
            // processarOUT.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            //  processarOUT.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(transportadora);

            //Validação do campo Fornecedor
            if (ValidaFornecedor(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro; 
            }

            //Validação do campo Nome
            if (ValidaNome(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo Termo Pesquisa
            if (ValidaTermoPesquisa(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo Rua
            if (ValidaRua(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo CEP
            if (ValidaCEP(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo BAIRRO
            if (ValidaBairro(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo DDD/Telefone
            if (ValidaTelefone(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo Fax
            if (ValidaFax(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo CNPJ/CPF
            //if (ValidaCNPJCPF(transportadora) == false)
            //{
            //    retorno = false;
            //}

            //Validação do campo DomFiscal
            if (ValidaDomFiscal(transportadora) == false)
            {
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //Validação do campo Tipo Transporte
            if (ValidaTpTransporte(transportadora) == false)
            {
                //retorno = false;
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }

            //seta Status do processarOUT
            // processarOUT.STATUS = retorno ? EnumeradoresAplicacao.Status.Sucesso : EnumeradoresAplicacao.Status.Erro;
        }

        /// <summary>
        /// método para validar o campo Rua de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaRua(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (String.IsNullOrEmpty(transportadora.Rua))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "Rua"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }
            else if (transportadora.Rua.Length > 33)
            {
                transportadora.Rua = transportadora.Rua.Substring(0, 33);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Termo Pesquisa de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaTermoPesquisa(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (String.IsNullOrEmpty(transportadora.TermoPesquisa))
            {
                base.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ValorEmBranco, "TermoPesquisa"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
            }
            else if (transportadora.TermoPesquisa.Length > 15)
            {
                transportadora.TermoPesquisa = transportadora.TermoPesquisa.Substring(0, 15);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Nome de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaNome(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (String.IsNullOrEmpty(transportadora.Nome))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "Nome"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }
            else if (transportadora.Nome.Length > 33)
            {
                transportadora.Nome = transportadora.Nome.Substring(0, 33);
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Fornecedor de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaFornecedor(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (String.IsNullOrEmpty(transportadora.Fornecedor))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "Fornecedor"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo CEP de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCEP(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;
            if (string.IsNullOrEmpty(transportadora.CodPostal))
            {
                base.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.CampoValorEmBranco, "CodPostal"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
            }
            else if (FornecedorSAP.ContemLetras(transportadora.CodPostal))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "CodPostal"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }
            else
            {
                transportadora.CodPostal = UtilGeralDPA.General.RetornaApenasAlfanumericos((transportadora.CodPostal.TrimEnd()));
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo bairro de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaBairro(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;
            if (string.IsNullOrEmpty(transportadora.Bairro))
            {
                base.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.CampoValorEmBranco, "Bairro"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
            }
            else if (transportadora.Bairro.Length > 20)
            {
                transportadora.Bairro = transportadora.Bairro.Substring(0, 20);
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo telefone de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaTelefone(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;
            bool isDdd;
            bool isTelefone;
            if (!string.IsNullOrEmpty(transportadora.Telefone))
            {
                if (transportadora.Telefone.Contains(ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE)))
                {
                    string[] telefonecompleto;
                    Int64 ddd;
                    telefonecompleto = transportadora.Telefone.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None);
                    if (telefonecompleto.Length > 2)
                    {
                        base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD/Telefone"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                        retorno = false;
                    }
                    else
                    {
                        isDdd = Int64.TryParse(telefonecompleto[0], out ddd);
                        if (isDdd)
                        {
                            if (ddd > 9999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "DDD"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                            retorno = false;
                        }
                        Int64 telefone;
                        isTelefone = Int64.TryParse(telefonecompleto[1], out telefone);
                        if (isTelefone)
                        {
                            if (telefone > 999999999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Telefone"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Telefone"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                            retorno = false;
                        }
                    }

                }
                else
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorEmBranco, "Telefone"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                    retorno = false;
                }
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo fax de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaFax(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;
            bool isDdd;
            bool isTelefone;
            if (!string.IsNullOrEmpty(transportadora.Fax))
            {
                if (transportadora.Fax.Contains(ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE)))
                {
                    string[] telefonecompleto;
                    Int64 ddd;
                    telefonecompleto = transportadora.Fax.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None);
                    if (telefonecompleto.Length > 2)
                    {
                        base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                        retorno = false;
                    }
                    else
                    {
                        isDdd = Int64.TryParse(telefonecompleto[0], out ddd);
                        if (isDdd)
                        {
                            if (ddd > 9999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                            retorno = false;
                        }
                        Int64 telefone;
                        isTelefone = Int64.TryParse(telefonecompleto[1], out telefone);
                        if (isTelefone)
                        {
                            if (telefone > 999999999)
                            {
                                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                                retorno = false;
                            }
                        }
                        else
                        {
                            base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                            retorno = false;
                        }
                        if (retorno == true)
                        {
                            transportadora.Fax = String.Empty;
                            transportadora.Fax = ddd.ToString() + telefone.ToString();
                        }
                    }

                }
                else
                {
                    base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Fax"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                    retorno = false;
                }
            }
            return retorno;
        }

        /// <summary>
        /// método para validar o campo CNPJ/CPF de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaCNPJCPF(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;
            bool isCPF;
            bool isCNPJ;

            if (string.IsNullOrEmpty(transportadora.CNPJ))
            {
                if (string.IsNullOrEmpty(transportadora.CPF))
                {
                    base.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.CampoValorEmBranco, "CNPJ/CPF"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);

                }
                else
                {
                    Int64 cpfTransportadora;
                    isCPF = Int64.TryParse(UtilGeralDPA.General.RetornaApenasAlfanumericos(transportadora.CPF), out cpfTransportadora);
                    if (isCPF && cpfTransportadora <= 99999999999999)
                    {
                        transportadora.CPF = cpfTransportadora.ToString();
                    }
                    else
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "CPF"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                        retorno = false;
                    }
                }
            }
            else
            {
                Int64 cnpjTransportadora;
                isCNPJ = Int64.TryParse(UtilGeralDPA.General.RetornaApenasAlfanumericos(transportadora.CNPJ), out cnpjTransportadora);
                if (isCNPJ && cnpjTransportadora <= 99999999999999)
                {
                    transportadora.CNPJ = cnpjTransportadora.ToString();
                }
                else
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "CNPJ"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                    retorno = false;
                }
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo Dominio Fiscal de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaDomFiscal(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(transportadora.DomFiscal))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "DomFiscal"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }

            return retorno;
        }

        /// <summary>
        /// método para validar o campo TpTransporte de acordo com a regra de negócios
        /// </summary>
        /// <param name="fornecedor">referência para o objeto fornecedor(dtpMM_Fornecedor)</param>
        /// <param name="processarOut">referencia para o objeto processaroOut(ProcessarOUT)</param>
        /// <returns>TRUE se está de acordo com a regra de negócios e FALSE se falhou em alguma regra de negócios </returns>
        private bool ValidaTpTransporte(dtpMM_Fornecedor transportadora)
        {
            bool retorno = true;

            if (string.IsNullOrEmpty(transportadora.TpTransporte))
            {
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "TpTransporte"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }
            else if (!(transportadora.TpTransporte.ToUpper().Equals("CIF") || transportadora.TpTransporte.ToUpper().Equals("FOB")))
            {
                processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorForaDominio, "TpTransporte"), EnumeradoresAplicacao.TipoEntidade.TransportadoraBE);
                retorno = false;
            }

            return retorno;
        }
    }
}
