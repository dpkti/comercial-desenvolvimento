﻿
namespace Business
{
    public class BusinessFactory
    {
        public static BusinessBase GetEntidadeNegocio(string nomeArquivo)
        {
            BusinessBase oEntidadeNegocio = null;

            // ClienteSAP
            if (nomeArquivo.StartsWith(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Entities.Parametros.DIR_ARQUIVOS_CLIENTE)))
            {
                oEntidadeNegocio = new ClienteSAP();
            }
            // FornecedorSAP
            else if (nomeArquivo.StartsWith(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Entities.Parametros.DIR_ARQUIVOS_FORNECEDOR)))
            {
                oEntidadeNegocio = new FornecedorSAP();
            }
            //ProdutoSAP
            else if (nomeArquivo.StartsWith(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Entities.Parametros.DIR_ARQUIVOS_MATERIAL)))
            {
                oEntidadeNegocio = new MaterialSAP();
            }

            return oEntidadeNegocio;
        }
    }

}