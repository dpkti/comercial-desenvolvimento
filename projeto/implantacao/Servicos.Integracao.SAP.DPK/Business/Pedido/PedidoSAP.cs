﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Business.Pedido;
using Business.ServicoVenda;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class PedidoSAP
    {
        /// <summary>
        /// Propriedade que guarda a lista de parametros do sistema
        /// </summary>
        List<ParametroSistemaBE> ListParametrosSistema
        {
            get;
            set;
        }

        /// <summary>
        /// Método do Processamento dos Pedidos. Empilhamento e Envio de Pendentes.
        /// </summary>
        /// <param name="codLoja"></param>
        public void Processar(CentroDistribuicaoBE centroDistribuicao)
        {

            //seta propriedade com a lista de parametros
            ListParametrosSistema = centroDistribuicao.ListParametrosSistema;

            ProcessarOUT resultadoProcessamento = null;

            //Empilha os pedidos que estão pendentes de envio
            resultadoProcessamento = EmpilharPedidosLiberados(centroDistribuicao.COD_LOJA);

            // Salva o log somente em casos de erro de empilhamento.
            if (resultadoProcessamento.STATUS == EnumeradoresAplicacao.Status.Erro)
            {
                LogBO.InserirLog(resultadoProcessamento);
            }

            // *************** Separar em duas threads?

            //Envia pedido a pedido para o SAP
            EnviarPedidosPendentes(centroDistribuicao.COD_LOJA);
        }

        /// <summary>
        /// carlosj CiT (05/10/2012) - EmpilharPedidosLiberados
        /// Esse método privado irá selecionar todos os pedidos que passaram pelo fluxo de liberação (bloqueios), 
        /// usando o método ConsultarPedidosLiberados da PedidoDALC e incluí-los na pilha usando o método 
        /// InserirPedidoPendente da mesma DALC
        /// <param name="cod_loja">código da loja</param>
        /// <returns>Resultado do empilhamento.</returns>
        /// </summary>
        private ProcessarOUT EmpilharPedidosLiberados(int cod_loja)
        {
            EmpilhamentoPedidoBO empilhamento = new EmpilhamentoPedidoBO();

            empilhamento.Processar(cod_loja);

            return empilhamento.processarOut;
        }

        protected void EnviarPedidosPendentes(int codLoja)
        {
            // Tratar exceção da consulta
            List<PedidoBE> lista = PedidoDALC.Instance.ConsultarPedidosPendentes(codLoja);

            foreach (PedidoBE ped in lista)
            {
                EnviarPedidoSAP(ped);
            }
        }

        /// <summary>
        /// Método responsável por enviar os pedidos em estado Pendente ou Aguardando.
        /// </summary>
        /// <param name="entity">objeto tipo PedidoBE</param>
        private void EnviarPedidoSAP(PedidoBE entity)
        {
            //instancia da classe EnvioPedidoBO 
            EnvioPedidoBO envioPedidoBO = new EnvioPedidoBO(entity, ListParametrosSistema);

            // Criar o objeto de request.
            dtpSD_Venda request = new dtpSD_Venda();

            //instancia request com os paremetros que vao ser consumidos pelo SAP
            envioPedidoBO.Processar(request);

            LogMethodEnviarPedidoSAP(envioPedidoBO);

            ProcessarRetornoPedido(envioPedidoBO.retornoVenda, entity);
        }

        private void LogMethodEnviarPedidoSAP(EnvioPedidoBO envioPedidoBO)
        {
            if (!envioPedidoBO.processarOut.verificaContemErros() && !envioPedidoBO.processarOut.verificaContemAvisos())
            {
                envioPedidoBO.processarOut.IncluirLogSucesso("Sucesso no envio do pedido.");
            }
            else if (!envioPedidoBO.processarOut.verificaContemErros())
            {
                envioPedidoBO.processarOut.IncluirLogSucesso("Sucesso no envio do pedido com avisos.");
            }

            LogBO.InserirLog(envioPedidoBO.processarOut);

            envioPedidoBO.LogControlePedido(envioPedidoBO.processarOut.LOG_GERAL.arquivoBE.ID);
        }

        protected void ProcessarRetornoPedido(dtpSD_RetornoVenda retornoVenda, PedidoBE pedido)
        {
            RetornoPedidoBO retornoPedidoBO = new RetornoPedidoBO(pedido);

            retornoPedidoBO.Processar(retornoVenda);

            if (!retornoPedidoBO.processarOut.verificaContemErros() && !retornoPedidoBO.processarOut.verificaContemAvisos())
            {
                retornoPedidoBO.processarOut.IncluirLogSucesso("Sucesso na resposta do pedido.");
            }
            else if (!retornoPedidoBO.processarOut.verificaContemErros())
            {
                retornoPedidoBO.processarOut.IncluirLogSucesso("Sucesso na resposta do pedido com avisos.");
            }

            LogBO.InserirLog(retornoPedidoBO.processarOut);

            retornoPedidoBO.LogControlePedido(retornoPedidoBO.processarOut.LOG_GERAL.arquivoBE.ID);
        }

        public void AtualizarControlePedidoSap(PedidoBE oPedido)
        {
            PedidoDALC.Instance.AtualizarControlePedidoSap(oPedido);
        }

        #region Monitoramento de pedidos

        /// <summary>
        ///  Realiza busca de todos os pedidos com o status = pendente
        /// </summary>
        /// <param name="oWorker">backgroundWorker customizado</param>
        /// <param name="filtroIn">Filtro da busca de pedidos</param>
        public void BuscarPedidos(WorkerDPA oWorker, object filtroIn)
        {
            if (filtroIn != null)
            {
                FiltroBuscaPedidoIn pesquisaFiltro = (FiltroBuscaPedidoIn)filtroIn;
                BuscaPedidoOut oBuscaOut = new BuscaPedidoOut();
                try
                {
                    oBuscaOut.Status = StatusBuscaPedidoOut.Processando;
                    oBuscaOut.Mensagem = ResourceMensagem.OperacaoEmProcessamento;
                    oWorker.GetBackgroundWorker.ReportProgress(1, oBuscaOut);

                    //string dbLink = ServicoBO.VerificaDBLinkCD(pesquisaFiltro.CodLojaBase, pesquisaFiltro.CodLoja);
                    List<PedidoDH> list = null;
                    oBuscaOut.ListPedidosDH = new List<PedidoDH>();
                    if (pesquisaFiltro.ListStatus.Length == 0)
                    {
                        list = PedidoDALC.Instance.BuscarPedidos(pesquisaFiltro.CodLojaBase, pesquisaFiltro.CodLoja, pesquisaFiltro.Status, pesquisaFiltro.DataInicio, pesquisaFiltro.DataFim);

                        oBuscaOut.ListPedidosDH = list;
                        oBuscaOut.QuantidadeRegistros = list.Count;
                    }
                    else
                    {
                        foreach (string item in pesquisaFiltro.ListStatus)
                        {
                            list = PedidoDALC.Instance.BuscarPedidos(pesquisaFiltro.CodLojaBase, pesquisaFiltro.CodLoja, item, pesquisaFiltro.DataInicio, pesquisaFiltro.DataFim);

                            oBuscaOut.ListPedidosDH.AddRange(list);
                            oBuscaOut.QuantidadeRegistros = oBuscaOut.ListPedidosDH.Count;
                        }
                    }

                    oBuscaOut.Status = StatusBuscaPedidoOut.Sucesso;
                    oBuscaOut.Mensagem = (oBuscaOut.QuantidadeRegistros == 0 ? ResourceMensagem.NenhumRegistroEncontrado : String.Format(ResourceMensagem.QuantidadePedidosEncontrados, oBuscaOut.QuantidadeRegistros));
                }
                catch (DALCException ex)
                {
                    Logger.LogError(ex);
                    oBuscaOut.Status = StatusBuscaPedidoOut.Erro;
                    oBuscaOut.Mensagem = String.Format(ResourceMensagem.ErroConectarBanco, pesquisaFiltro.CodLoja.ToString().PadLeft(2, '0'));
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    oBuscaOut.Status = StatusBuscaPedidoOut.Erro;
                    oBuscaOut.Mensagem = ResourceMensagem.ErroBuscaPedidos;
                }
                finally
                {
                    oWorker.GetBackgroundWorker.ReportProgress(1, oBuscaOut);

                }

            }
        }

        /// <summary>
        /// Busca o histórico de logo de um pedido enviado para o SAP
        /// </summary>
        /// <param name="idControlePedido">chave - ID_CONTROLE_PEDIDO</param>
        /// <param name="codLoja">código do CD a ser pesquisado</param>
        /// <returns>lista de logs de um determinado pedido</returns>
        public List<HistoricoPedidoDH> BuscarHistoricoPedido(string idControlePedido, int codLojaBase, int codLoja)
        {
            //string dbLink = ServicoBO.VerificaDBLinkCD(codLoja);

            return PedidoDALC.Instance.BuscarHistoricoPedido(idControlePedido, codLojaBase, codLoja);
        }

        /// <summary>
        /// Atualiza o campo STATUS do controle_pedido_sap
        /// </summary>
        /// <param name="idControlePedido">chave da tabela - ID_CONTROLE_PEDIDO</param>
        /// <param name="status">status a ser atualizado</param>
        public void AtualizaStatusControlePedido(string idControlePedido, string status, string statusAnterior)
        {
            PedidoDALC.Instance.AtualizaStatusControlePedido(idControlePedido, status, statusAnterior);
        }

        #endregion

        #region Cancelamento de pedidos

        public PedidoCancelamentoDH ConsultarPedidoASerCancelado(string codLoja, string numPedido, string seqPedido, string numNota)
        {
            return PedidoDALC.Instance.ConsultarPedidoASerCancelado(codLoja, numPedido, seqPedido, numNota);
        }

        public void BuscarControlePedido(PedidoCancelamentoDH pedido)
        {
            try
            {
                PedidoDALC.Instance.BuscarControlePedido(pedido);
            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);

                if (ex.ErroDeConexao)
                {
                    throw new Exception(String.Format(ResourceMensagem.ErroConectarBanco, pedido.COD_LOJA.ToString().PadLeft(2, '0')));
                }
                else
                {
                    throw new Exception(ResourceMensagem.ErroPadrao);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);
            }

        }

        /// <summary>
        /// Método responsável por cancelar o pedido
        /// </summary>
        /// <param name="pedido">Pedido a ser cancelado</param>
        /// <param name="centro">Centro que está cancelando o pedido</param>
        /// <param name="oUser">Usuário que está cancelando o pedido</param>
        /// <remarks>vxavier CiT - 11/10/12</remarks>
        public CancelamentoOut CancelarPedido(PedidoCancelamentoDH pedido, CentroDistribuicaoBE centro, UsuarioBE oUser)
        {
            dtpSD_CancVenda request = new dtpSD_CancVenda();

            CancelamentoPedidoBO cancelamentoBo = new CancelamentoPedidoBO(pedido, centro);

            PopularRequestCancel(pedido, centro, request, oUser, cancelamentoBo);

            cancelamentoBo.Processar(request);

            //tratamento de mensagens de erro
            if (cancelamentoBo.processarOut.verificaContemErros())
            {
                cancelamentoBo.cancelamentoOut.canceladoComSucesso = false;
                cancelamentoBo.cancelamentoOut.messages.Clear();

                if (cancelamentoBo.processarOut.LOG_GERAL.listLogDetalhe.Any(x => x.COD_ERRO == EnumeradoresAplicacao.CodigoErro.ErroBanco))
                {
                    //string logDet = (from x in cancelamentoBo.processarOut.LOG_GERAL.listLogDetalhe where x.COD_ERRO == EnumeradoresAplicacao.CodigoErro.ErroBanco select x.DESCRICAO).FirstOrDefault();
                    cancelamentoBo.cancelamentoOut.messages.Add(String.Format(ResourceMensagem.ErroConectarBanco, centro.COD_LOJA));
                }
                else if (cancelamentoBo.processarOut.LOG_GERAL.listLogDetalhe.Any(x => x.COD_ERRO == EnumeradoresAplicacao.CodigoErro.ErroComunicacaoPedidoSAP))
                {

                    cancelamentoBo.cancelamentoOut.messages.Add(ResourceMensagem.ErroComunicacaoSAP);
                }
                else
                {
                    cancelamentoBo.cancelamentoOut.messages.Add(ResourceMensagem.ErroPadrao);
                }
            }

            foreach (var msg in cancelamentoBo.cancelamentoOut.messages)
            {
                cancelamentoBo.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroSapNFE, msg);
            }

            LogBO.InserirLog(cancelamentoBo.processarOut);

            return cancelamentoBo.cancelamentoOut;
        }

        public void PopularRequestCancel(PedidoCancelamentoDH pedido, CentroDistribuicaoBE centro, dtpSD_CancVenda request, UsuarioBE oUser, CancelamentoPedidoBO cancelamentoBo)
        {
            request.Protocolo = new dtpPI_ItfProtocolo();
            request.Protocolo.Centro = pedido.LOJA_COD_SAP;
            request.Protocolo.Usuario = oUser.LOGIN;
            request.Protocolo.Hostname = Environment.MachineName;
            request.Protocolo.NmAplicacao = ConfigurationManager.AppSettings["nomeAplicacao"].ToString();
            request.Empresa = cancelamentoBo.ObterParametroSistemaPorNomeParametro("EMPRESA", centro.ListParametrosSistema).VL_PARAMETRO;
            request.Filial = pedido.LOJA_COD_SAP;
            request.Cliente = pedido.CLIENTE_COD_SAP.ToString();
            request.ReferenciaPOS = new dtpSD_POS();
            request.ReferenciaPOS.NumeroPedido = pedido.ID_CONTROLE_PEDIDO_SAP;
            request.ReferenciaNF = new dtpSD_NF();
            request.ReferenciaNF.TipoNF = ConfigurationManager.AppSettings["TipoNF"].ToString();
            request.ReferenciaNF.FilialOrig = pedido.LOJA_COD_SAP;
            request.ReferenciaNF.NroNfCliente = pedido.NUM_PEDIDO.ToString();
            request.ReferenciaNF.NroNfReferencia = String.Empty;
        }

        #endregion
    }
}
