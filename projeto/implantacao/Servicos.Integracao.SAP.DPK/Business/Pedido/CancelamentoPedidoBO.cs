﻿using System;
using System.Collections.Generic;
using System.Linq;
using Business.ServicoVenda;
using DALC;
using Entities;

namespace Business.Pedido
{
    public class CancelamentoPedidoBO : BaseBO<ServicoVenda.dtpSD_CancVenda>
    {
        public CancelamentoOut cancelamentoOut;
        private PedidoCancelamentoDH Pedido;
        private CentroDistribuicaoBE Centro;


        public CancelamentoPedidoBO(PedidoCancelamentoDH pedido, CentroDistribuicaoBE centro)
        {
            this.Pedido = pedido;
            this.Centro = centro;
            this.cancelamentoOut = new CancelamentoOut();
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.SemArquivo;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.CancelamentoBE;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
        }

        protected override void ProcessarInterno(dtpSD_CancVenda item)
        {
            if (!string.IsNullOrEmpty(Pedido.STATUS) && (Pedido.STATUS.Equals("F") || Pedido.STATUS.Equals("X") || Pedido.STATUS.Equals("N")))
            {
                //se status for faturado ou expurgo
                dtpSD_RetornoCanc resposta = CancelarPedidoSAP(item);

                base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(resposta);

                bool statusErro = resposta.Status[0].msgs.Any(x => x.Tipo.Equals("E"));
                bool pedidoJaCanceladoNoSAP = resposta.Status[0].msgs.Any(x => x.Tipo.Equals("E") && x.ID.Equals("902"));

                if (pedidoJaCanceladoNoSAP)
                {
                    Pedido.SITUACAO = 9;
                    cancelamentoOut.canceladoComSucesso = true;
                    base.processarOut.IncluirLogAviso(ResourceMensagem.PedidoJaCanceladoNoSap);
                    CancelarPedidoLegado();
                }
                else if (resposta.Status[0].Status.Equals("P"))
                {
                    string msgConcat = string.Empty;
                    foreach (dtpPI_ItfMensagem m in resposta.Status[0].msgs)
                    {
                        msgConcat += m.ID + " - " + m.Msg + "\n";
                    }

                    msgConcat = "Etapa: " + resposta.Status[0].Etapa
                                        + ", Tipo: " + resposta.Status[0].Tipo
                                        + ", Documento: " + resposta.Status[0].Documento + "\n"
                                        + msgConcat;

                    cancelamentoOut.messages.Add(msgConcat);

                    cancelamentoOut.canceladoComSucesso = false;
                    string statusPedido = "N"; //negado
                    NfeStatusDALC.Instance.AtualizarControlePedidoStatus(Pedido.ID_CONTROLE_PEDIDO_SAP, statusPedido);
                }
                else if (statusErro)
                {
                    foreach (dtpPI_ItfMensagem msg in resposta.Status[0].msgs)
                    {
                        string erro = msg.ID + " - " + msg.Msg;
                        cancelamentoOut.messages.Add(erro);
                        cancelamentoOut.canceladoComSucesso = false;
                    }
                }
                else if (resposta.Status[0].Status.Equals("C"))
                {
                    Pedido.SITUACAO = 9;
                    CancelarPedidoLegado();
                    cancelamentoOut.canceladoComSucesso = true;
                }
            }
            else if (string.IsNullOrEmpty(Pedido.STATUS) || Pedido.STATUS.Equals("P") || Pedido.STATUS.Equals("E"))
            {
                Pedido.SITUACAO = 9;
                cancelamentoOut.canceladoComSucesso = true;
                CancelarPedidoLegado();
            }
            else
            {
                cancelamentoOut.messages.Add("O status atual do pedido não permite cancelamento. Status: " + Pedido.STATUS);
            }
        }

        private void CancelarPedidoLegado()
        {
            string nomeProc = String.Empty;
            if (Centro.TipoBanco.Equals(EnumeradoresAplicacao.TipoBanco.Unico))
            {
                nomeProc = "PRODUCAO." + Parametros.PR_CANCELA;
            }
            else if (Centro.TipoBanco.Equals(EnumeradoresAplicacao.TipoBanco.Multiplo))
            {
                nomeProc = "DEP" + String.Format(Centro.COD_LOJA.ToString(), "00") + "." + Parametros.PR_CANCELA;
            }
            PedidoDALC.Instance.CancelarPedido(Pedido, nomeProc);
        }

        private dtpSD_RetornoCanc CancelarPedidoSAP(dtpSD_CancVenda item)
        {
            itfSD_Venda_OutClient client = new itfSD_Venda_OutClient();

            dtpSD_RetornoCanc response = client.itfSD_Canc_Out(item);

            return response;
        }

        /// <summary>
        /// vxavier CiT (11/10/2012) - ObterParametroSistemaPorNomeParametro
        /// Obtem o parametro pelo nome 
        /// </summary>
        /// <param name="nomeParametro">nome do Parametro</param>
        /// <returns></returns>
        public ParametroSistemaBE ObterParametroSistemaPorNomeParametro(string nomeParametro, List<ParametroSistemaBE> ListParametrosSistema)
        {
            ParametroSistemaBE entity = new ParametroSistemaBE();

            if (ListParametrosSistema != null)
            {
                entity = ListParametrosSistema.Single(item => item.NM_PARAMETRO == nomeParametro);
            }
            return entity;
        }
    }
}
