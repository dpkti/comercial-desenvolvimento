﻿using System;
using System.Collections.Generic;
using DALC;
using Entities;
namespace Business
{
    public class EmpilhamentoPedidoBO : BaseBO<Int32>
    {
        public EmpilhamentoPedidoBO()
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.SemArquivo;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.PedidoBE;
        }

        protected override void ProcessarInterno(Int32 codLoja)
        {
            List<PedidoBE> listPedidoBE = null;

            listPedidoBE = PedidoDALC.Instance.ConsultarPedidosLiberados(codLoja);

            foreach (PedidoBE entity in listPedidoBE)
            {
                PedidoDALC.Instance.InserirPedidoPendente(entity);
            }
        }
    }

}
