﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Business.ServicoVenda;
using DALC;
using Entities;

namespace Business
{
    public class EnvioPedidoBO : BaseBO<ServicoVenda.dtpSD_Venda>
    {
        #region attributes

        public dtpSD_RetornoVenda retornoVenda { get; set; }

        private PedidoBE pedido;

        private decimal totalProduto { get; set; }

        /// <summary>
        /// Propriedade que guarda a lista de parametros do sistema
        /// </summary>
        List<ParametroSistemaBE> ListParametrosSistema { get; set; }

        #endregion

        public EnvioPedidoBO(PedidoBE pedido, List<ParametroSistemaBE> listParametrosSistema)
        {
            this.pedido = pedido;
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.SemArquivo;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.PedidoBE;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            this.ListParametrosSistema = listParametrosSistema;
        }

        protected override void ProcessarInterno(ServicoVenda.dtpSD_Venda item)
        {
            PopularObjeto(item);

            processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(item);

            // Ser conter erros devemos logar e não enviar para o SAP.
            if (!processarOut.verificaContemErros())
            {
                //atualiza no bco o status do pedido para aguardando
                AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Aguardando);

                //instancia objeto itfSD_Venda_OutClient (webservice que ira enviar para o SAP)
                itfSD_Venda_OutClient client = new itfSD_Venda_OutClient();

                //Chamada do SAP
                retornoVenda = client.itfSD_Venda_Out(item);
            }
        }

        public void LogControlePedido(long idLogSincArquivo)
        {
            PedidoDALC.Instance.InserirLogControlePedido(pedido.ID_CONTROLE_PEDIDO, idLogSincArquivo);
        }

        /// <summary>
        /// vxavier CiT (05/10/2012) 
        /// Este método atualiza o status do pedido.
        /// </summary>
        /// <param name="pedido">Pedidos a ser Enviado</param>
        private void AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido statusPedido)
        {
            pedido.STATUS = statusPedido.ToString();
            if (statusPedido.Equals(EnumeradoresAplicacao.StatusPedido.Sucesso))
            {
                pedido.DT_ENVIO = DateTime.Now;
            }
            PedidoDALC.Instance.AtualizarControlePedidoSap(pedido);
        }

        private void PopularObjeto(ServicoVenda.dtpSD_Venda item)
        {
            totalProduto = getTotalProduto();

            item.Protocolo = new ServicoVenda.dtpPI_ItfProtocolo();
            item.Protocolo.Centro = pedido.COD_SAP_LOJA.ToString();
            item.Protocolo.Usuario = string.Empty;
            item.Protocolo.Hostname = Environment.MachineName;
            item.Protocolo.NmAplicacao = ConfigurationManager.AppSettings["nomeAplicacaoRequest"].ToString();

            item.ReferenciaPOS = new ServicoVenda.dtpSD_POS();
            item.ReferenciaPOS.NumeroPedido = pedido.ID_CONTROLE_PEDIDO;

            item.Processamento = new ServicoVenda.dtpSD_Proc();
            item.Processamento.NaturezaOper = pedido.NATUREZA_OP_SAP; //Verificar

            item.Cabecalho = new ServicoVenda.dtpSD_DadosCabecalho();
            item.Cabecalho.AreaVendas = new ServicoVenda.dtpSD_AreaVendas();
            item.Cabecalho.AreaVendas.OrgVendas = ObterParametroSistemaPorNomeParametro("EMPRESA").VL_PARAMETRO; //Verificar - ParametroSistema
            item.Cabecalho.AreaVendas.CanalDistrib = ObterParametroSistemaPorNomeParametro("CANAL_DIST").VL_PARAMETRO; //verificar - ParametroSistema
            item.Cabecalho.AreaVendas.SetorAtividade = ObterParametroSistemaPorNomeParametro("SETOR_ATIV_VENDA").VL_PARAMETRO; //verificar - ParametroSistema
            item.Cabecalho.AreaVendas.EcritorioVendas = getFormattedCodFilial(); //Verificar 
            item.Cabecalho.AreaVendas.EquipeVendas = string.Empty;

            item.Cabecalho.Cliente = new ServicoVenda.dtpSD_Cliente();
            item.Cabecalho.Cliente.EmissorOrdem = pedido.COD_SAP_CLIENTE.ToString();
            item.Cabecalho.Cliente.RecebMercadoria = pedido.COD_SAP_CLIENTE.ToString();
            item.Cabecalho.Cliente.ClienteRetira = pedido.FLAG_RETIRA;
            //item.Cabecalho.Cliente.RecebFatura = string.Empty; //DETEC diz para não enviar
            //item.Cabecalho.Cliente.RecebMercadoriaAd = string.Empty; //DETEC diz para não enviar


            item.Cabecalho.DadosAdicionais = new ServicoVenda.dtpSD_AdCab();
            item.Cabecalho.DadosAdicionais.EnviadoPor = string.Empty;
            item.Cabecalho.DadosAdicionais.Veiculo = string.Empty;
            item.Cabecalho.DadosAdicionais.Montadora = string.Empty;
            item.Cabecalho.DadosAdicionais.KM = string.Empty;
            item.Cabecalho.DadosAdicionais.Ano = string.Empty;
            item.Cabecalho.DadosAdicionais.Placa = string.Empty;
            item.Cabecalho.DadosAdicionais.NumeroSinistro = string.Empty;
            item.Cabecalho.DadosAdicionais.NroApolice = string.Empty;
            item.Cabecalho.DadosAdicionais.NroAtendimentoGarantia = string.Empty;
            item.Cabecalho.DadosAdicionais.JurosDiasAtraso = getJurosDiasAtraso();
            if (item.Cabecalho.DadosAdicionais.JurosDiasAtraso != 0)
            {
                item.Cabecalho.DadosAdicionais.JurosDiasAtrasoSpecified = true;
            }
            item.Cabecalho.DadosAdicionais.Banco = pedido.COD_BANCO.ToString();
            //item.Cabecalho.DadosAdicionais.TxMulta = 0.0M;
            //item.Cabecalho.DadosAdicionais.TxJuros = 0.0M; //DETEC manda enviar em branco
            //item.Cabecalho.DadosAdicionais.DescPagtoAntecip = 0.0M; //DETEC manda enviar em branco

            item.Item = getItensPedido();
        }

        private ServicoVenda.dtpSD_DadosItem[] getItensPedido()
        {
            ServicoVenda.dtpSD_DadosItem[] itens = new ServicoVenda.dtpSD_DadosItem[1];
            itens[0] = new ServicoVenda.dtpSD_DadosItem();
            itens[0].TpDocVendas = pedido.TP_DOC_SAP; //Verificar
            itens[0].MotivoOrdem = getMotivoOrdem();
            itens[0].NroReferencia = pedido.ID_CONTROLE_PEDIDO;
            itens[0].MotivoBloq = string.Empty;
            itens[0].Finalidade = getFinalidade();
            itens[0].ItmVenda = new ServicoVenda.dtpSD_Item[pedido.ListaItensPedido.Count];
            itens[0].Parceiros = getParceiros(pedido);

            int i = 0;

            foreach (ItemBE item in pedido.ListaItensPedido)
            {
                itens[0].ItmVenda[i] = new ServicoVenda.dtpSD_Item();
                itens[0].ItmVenda[i].NrItem = item.NUM_ITEM_PEDIDO.ToString();
                itens[0].ItmVenda[i].Centro = pedido.COD_SAP_LOJA.ToString();
                itens[0].ItmVenda[i].Deposito = "0001";
                itens[0].ItmVenda[i].CodMaterial = item.COD_DPK.ToString();
                // Quantidade atendida é multiplicado por mil para atender formatação do SAP. Últimos 3 dígitos são descartados pois seriam a parte fracionária.
                itens[0].ItmVenda[i].QtdMaterial = (item.QTD_ATENDIDA * 1000).ToString();
                itens[0].ItmVenda[i].LocExpedicao = getLocExpedicao();
                itens[0].ItmVenda[i].Itinerario = pedido.COD_TRANSPORTADORA.ToString();
                itens[0].ItmVenda[i].Condicoes = getCondicoes(item);
                //itens[0].ItmVenda[i].DtCarregamento //No DETEC manda enviar em branco
                //itens[0].ItmVenda[i].HrCarregamento //No DETEC manda enviar em branco

                i++;
            }

            itens[0].RegVendas = getRegVendas();
            if (pedido.MENS_PEDIDO != null)
            {
                itens[0].Texto = pedido.MENS_PEDIDO.Length > 30 ? pedido.MENS_PEDIDO.Substring(0, 30) : pedido.MENS_PEDIDO;
            }
            itens[0].Mensagem = pedido.MENS_NOTA;

            itens[0].Comercial = new ServicoVenda.dtpSD_Comercial();
            itens[0].Comercial.CondPgto = pedido.COD_PLANO.ToString();
            itens[0].Comercial.Incoterm = getFretePago();
            itens[0].Comercial.DescIncoterm = string.Empty;
            //itens[0].Comercial.MeioPagto  //No DETEC este campo não foi tratado

            return itens;
        }

        private ServicoVenda.dtpSD_Parceiro[] getParceiros(PedidoBE ped)
        {
            PedidoDALC pedidoDalc = new PedidoDALC();
            ServicoVenda.dtpSD_Parceiro[] itemParceiro = new ServicoVenda.dtpSD_Parceiro[2];
            itemParceiro[0] = new ServicoVenda.dtpSD_Parceiro();

            CentroDistribuicaoBE oCentro = ServicoBO.ObterCentroDistribuicao();

            foreach (KeyValuePair<string, string> par in pedidoDalc.BuscaFlagRedespacho(ped))
            {
                if (par.Value == "S")
                {
                    itemParceiro[0].FuncaoParceiro = "SP";
                    itemParceiro[0].CodParceiro = pedidoDalc.BuscaTranspParametro(ped, oCentro); //PEGA O COD_SAP2 

                    itemParceiro[1] = new ServicoVenda.dtpSD_Parceiro();
                    itemParceiro[1].FuncaoParceiro = "ZH";
                    itemParceiro[1].CodParceiro = par.Key; //PEGA O COD_SAP NA PAR.KEY
                }
                else//FL_REDESPACHO == "N"
                {
                    itemParceiro[0].FuncaoParceiro = "SP";
                    itemParceiro[0].CodParceiro = par.Key; //PEGA O COD_SAP NA PAR.KEY
                }

            }

            return itemParceiro;
        }

        private ServicoVenda.dtpSD_Condicao[] getCondicoes(ItemBE item)
        {
            List<ServicoVenda.dtpSD_Condicao> listaCondicao = new List<ServicoVenda.dtpSD_Condicao>();
            ServicoVenda.dtpSD_Condicao condicao;

            condicao = new ServicoVenda.dtpSD_Condicao();
            condicao.Condicao = "ZPR0";
            condicao.Valor = item.PRECO_UNITARIO;
            listaCondicao.Add(condicao);

            if (item.PC_DESC1 != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZD01";
                condicao.Valor = item.PC_DESC1;
                listaCondicao.Add(condicao);
            }

            if (item.PC_DESC2 != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZD02";
                condicao.Valor = item.PC_DESC2;
                listaCondicao.Add(condicao);
            }

            if (item.PC_DESC3 != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZD03";
                condicao.Valor = item.PC_DESC3;
                listaCondicao.Add(condicao);
            }

            if (pedido.PC_DESC_SUFRAMA != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZD04";
                condicao.Valor = pedido.PC_DESC_SUFRAMA;
                listaCondicao.Add(condicao);
            }

            if (item.PC_DIF_ICM != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZD05";
                condicao.Valor = item.PC_DIF_ICM;
                listaCondicao.Add(condicao);
            }

            condicao = new ServicoVenda.dtpSD_Condicao();
            condicao.Condicao = "ZACR";
            condicao.Valor = pedido.PC_DESCONTO - pedido.PC_ACRESCIMO;
            listaCondicao.Add(condicao);

            if (item.PC_COMISS != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZCRG";
                condicao.Valor = item.PC_COMISS;
                listaCondicao.Add(condicao);
            }

            if (item.PC_COMISS_TLMK != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZCVE";
                condicao.Valor = item.PC_COMISS_TLMK;
                listaCondicao.Add(condicao);
            }

            if (pedido.PC_SEGURO != 0)
            {
                condicao = new ServicoVenda.dtpSD_Condicao();
                condicao.Condicao = "ZSEG";
                condicao.Valor = pedido.PC_SEGURO;
                listaCondicao.Add(condicao);
            }

            condicao = new ServicoVenda.dtpSD_Condicao();
            condicao.Condicao = "ZFRE";
            condicao.Valor = (pedido.VL_FRETE * item.VALOR_LIQUIDO) / totalProduto;
            listaCondicao.Add(condicao);

            condicao = new ServicoVenda.dtpSD_Condicao();
            condicao.Condicao = "ZDAC";
            condicao.Valor = (pedido.VL_DESP_ACESS * item.VALOR_LIQUIDO) / totalProduto;
            listaCondicao.Add(condicao);

            condicao = new ServicoVenda.dtpSD_Condicao();
            condicao.Condicao = "PR00";
            condicao.Valor = item.VALOR_LIQUIDO;
            listaCondicao.Add(condicao);

            return listaCondicao.ToArray();
        }

        private decimal getTotalProduto()
        {
            decimal somaLiquido = 0M;
            foreach (ItemBE itens in pedido.ListaItensPedido)
            {
                somaLiquido += itens.VALOR_LIQUIDO;
            }
            return somaLiquido;
        }

        private string getFretePago()
        {
            string retorno;

            switch (pedido.FRETE_PAGO)
            {
                case "C":
                    retorno = "CIF";
                    break;

                case "F":
                    retorno = "FOB";
                    break;

                case "D":
                    retorno = "DIV";
                    break;

                default:
                    retorno = "";
                    break;
            }
            return retorno;
        }

        private string getRegVendas()
        {
            return (new PedidoDALC().BuscarRegiaoVendas(pedido)).ToString();
        }

        private string getLocExpedicao()
        {
            string retorno = string.Empty;
            if (pedido.FLAG_RETIRA.Equals("N") || pedido.FLAG_RETIRA.Equals("E"))
            {
                retorno = pedido.COD_SAP_LOJA.ToString();
            }
            else
            {
                retorno = "R" + pedido.COD_SAP_LOJA.ToString().Substring(1, 3);
            }
            return retorno;
        }

        private string getFinalidade()
        {
            // Padrão R: revenda
            string retorno = "R";

            if (pedido.FL_CONS_FINAL.Equals("S"))
            {
                // Se o consumidor é final: C
                retorno = "C";
            }

            return retorno;
        }

        private string getMotivoOrdem()
        {
            string retorno = string.Empty;
            if (pedido.COD_VDR == 1)
            {
                retorno = ObterParametroSistemaPorNomeParametro("MTV_ORDEM_VDR").VL_PARAMETRO; //Verificar - ParametroSistema
            }
            else
            {
                retorno = pedido.MTV_ORDEM; //informaremos o campo MTV_ORDEM da tabela  NATUREZA_OPER_S
            }
            return retorno;
        }

        private decimal getJurosDiasAtraso()
        {
            return new PedidoDALC().BuscarJurosAtraso(pedido);
        }

        private string getFormattedCodFilial()
        {
            return "8" + pedido.COD_FILIAL.ToString();
        }

        /// <summary>
        /// carlosj CiT (08/10/2012) - ObterParametroSistemaPorNomeParametro
        /// Obtem o parametro pelo nome 
        /// </summary>
        /// <param name="nomeParametro">nome do Parametro</param>
        /// <returns></returns>
        private ParametroSistemaBE ObterParametroSistemaPorNomeParametro(string nomeParametro)
        {
            ParametroSistemaBE entity = new ParametroSistemaBE();

            if (ListParametrosSistema != null)
            {
                entity = ListParametrosSistema.Single(item => item.NM_PARAMETRO == nomeParametro);
            }
            return entity;
        }

    }

}
