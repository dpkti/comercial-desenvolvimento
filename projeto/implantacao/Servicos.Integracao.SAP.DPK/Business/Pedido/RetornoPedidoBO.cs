﻿using System;
using System.Linq;
using Business.ServicoVenda;
using DALC;
using Entities;

namespace Business
{
    public class RetornoPedidoBO : BaseBO<dtpSD_RetornoVenda>
    {
        private PedidoBE Pedido { get; set; }

        public RetornoPedidoBO(PedidoBE pedido)
        {
            this.Pedido = pedido;
            processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.SemArquivo;
            processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.PedidoBE;
            processarOut.LOG_GERAL.arquivoBE.nomeArquivo = string.Empty;
            processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
        }

        protected override void ProcessarInterno(dtpSD_RetornoVenda retornoVenda)
        {
            processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(retornoVenda);

            Pedido.NR_ORDEM_SAP = retornoVenda.Documentos[0].Ordem;
            Pedido.NR_REMESSA_SAP = retornoVenda.Documentos[0].Remessa;
            Pedido.NR_FATURA_SAP = retornoVenda.Documentos[0].Fatura;

            Pedido.TEM_DIF_ESTOQUE = getDifEstoqueValue(retornoVenda.Quantidades);
            if (Pedido.TEM_DIF_ESTOQUE.Equals("X"))
            {
                Pedido.TEM_DIF_ESTOQUE = "S";
                AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Expurgado);
                processarOut.IncluirLogAviso("O pedido teve seu status atualizado para X (Expurgo) pois todos os itens estão com a quantidade 0.", EnumeradoresAplicacao.TipoEntidade.PedidoBE);
            }
            //verifica remessa
            else if (!ValidarRemessa(retornoVenda))
            {
                AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Erro_SAP);
                base.processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ItemEmBranco, "Remessa"));
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }
            else
            {
                bool invalidaPedido = false;

                //caso tenha achado um item com erro, seta a variavel invalidaPedido para true
                invalidaPedido = (retornoVenda.Msgs.Any(msg => msg.Tipo == ServicoBO.GetValorCodStatusSap(EnumeradoresAplicacao.Status.Erro) && !(msg.ID == "901")));
                
                if (invalidaPedido)
                {
                    AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Erro_SAP);
                    processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroComunicacaoPedidoSAP, "Pedido invalidado no SAP");
                }
                else
                {
                    AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Sucesso);
                }
            }

            IncluirMsgRetornoBO(retornoVenda);
        }

        /// <summary>
        /// carlosj CiT (09/10/2012) - ValidarRemessa
        /// </summary>
        /// <param name="retornoVenda"></param>
        /// <returns></returns>
        private bool ValidarRemessa(dtpSD_RetornoVenda retornoVenda)
        {
            //caso tenha achado um item com erro, seta a variavel invalidaPedido para true
            bool contemErro = (retornoVenda.Msgs.Any(msg => msg.Tipo == ServicoBO.GetValorCodStatusSap(EnumeradoresAplicacao.Status.Erro)));

            if (!contemErro && string.IsNullOrEmpty(retornoVenda.Documentos[0].Remessa))
            {
                return false;
            }
            else if(contemErro)
            {
                if (retornoVenda.Msgs.Any(msg => msg.Tipo == ServicoBO.GetValorCodStatusSap(EnumeradoresAplicacao.Status.Erro) && msg.ID == "901"))
                {
                    AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido.Sucesso);
                    return true;
                }
            }
            return true;
        }

        private void IncluirMsgRetornoBO(dtpSD_RetornoVenda retornoVenda)
        {
            foreach (dtpPI_ItfMensagem msg in retornoVenda.Msgs)
            {
                string message = "XmlMsg-" + msg.ID + ": " +  msg.Msg;
                if (msg.Tipo == ServicoBO.GetValorCodStatusSap(EnumeradoresAplicacao.Status.Erro))
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.RetornoComunicacaoSAP, message);
                }
                else if (msg.Tipo == ServicoBO.GetValorCodStatusSap(EnumeradoresAplicacao.Status.Sucesso))
                {
                    processarOut.IncluirLogSucesso(message, EnumeradoresAplicacao.TipoEntidade.PedidoBE);
                }
                else
                {
                    processarOut.IncluirLogAviso(message, EnumeradoresAplicacao.TipoEntidade.PedidoBE);
                }
            }
        }

        private string getDifEstoqueValue(dtpSD_Quantidades[] quantidadesItens)
        {
            string retorno = "N";
            int countZeros = 0;
            foreach (dtpSD_Quantidades item in quantidadesItens)
            {
                if (item.QtdDesejada != item.QtdConfirmada)
                {
                    retorno = "S";
                }
                if (item.QtdConfirmada == 0)
                {
                    countZeros++;
                }
            }

            if (countZeros == quantidadesItens.Length)
            {
                retorno = "X";
            }

            return retorno;
        }

        /// <summary>
        /// vxavier CiT (05/10/2012) 
        /// ecampos Método replicado da classe EnvioPedidoBO
        /// Este método atualiza o status do pedido.
        /// </summary>
        /// <param name="pedido">Pedidos a ser Enviado</param>
        private void AtualizarStatusPedido(EnumeradoresAplicacao.StatusPedido statusPedido)
        {
            Pedido.STATUS = statusPedido.ToString();
            if (statusPedido.Equals(EnumeradoresAplicacao.StatusPedido.Sucesso))
            {
                Pedido.DT_ENVIO = DateTime.Now;
            }
            PedidoDALC.Instance.AtualizarControlePedidoSap(Pedido);
        }

        public void LogControlePedido(long idLogSincArquivo)
        {
            PedidoDALC.Instance.InserirLogControlePedido(Pedido.ID_CONTROLE_PEDIDO, idLogSincArquivo);
        }
    }
}
