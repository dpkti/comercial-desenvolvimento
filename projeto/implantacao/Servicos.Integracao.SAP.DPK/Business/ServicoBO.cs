﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Xml.Serialization;
using Business.NfeStatus;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class ServicoBO
    {

        private static List<ParametroSistemaBE> lstParam;
        public static List<ParametroSistemaBE> ListParamSistema
        {
            get { return lstParam; }
            set { lstParam = value; }
        }

        /// <summary>
        /// Método responsável por criar os diretórios necessários da aplicação se não existirem.
        /// </summary>
        /// <returns>Indicador de sucesso.</returns>
        /// <remarks>vxavier - Ci&T - 31/08/2012</remarks>
        public static Boolean CriarDiretoriosPadroes()
        {
            try
            {

                // Diretórios de Cliente
                DirectoryInfo dirArquivosInvalidosCliente = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_CLIENTE));
                DirectoryInfo dirArquivosCliente = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_CLIENTE));
                DirectoryInfo dirArquivosClientesProcessados = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_CLIENTE_PROCESSADOS));

                // Diretórios de Fornecedor
                DirectoryInfo dirArquivosInvalidosFornecedor = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_FORNECEDOR));
                DirectoryInfo dirArquivosFornecedor = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_FORNECEDOR));
                DirectoryInfo dirArquivosFornecedorProcessados = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_FORNECEDOR_PROCESSADOS));

                // Diretórios de Material
                DirectoryInfo dirArquivosInvalidosMaterial = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_MATERIAL));
                DirectoryInfo dirArquivosMaterial = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_MATERIAL));
                DirectoryInfo dirArquivosMaterialProcessados = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_MATERIAL_PROCESSADOS));

                // Criando Diretórios Cliente
                if (!dirArquivosInvalidosCliente.Exists)
                {
                    dirArquivosInvalidosCliente.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosInvalidosCliente.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosCliente.Exists)
                {
                    dirArquivosCliente.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosCliente.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosClientesProcessados.Exists)
                {
                    dirArquivosClientesProcessados.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosClientesProcessados.FullName + "\" criado com sucesso");
                }

                // Criando Diretórios Fornecedor
                if (!dirArquivosInvalidosFornecedor.Exists)
                {
                    dirArquivosInvalidosFornecedor.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosInvalidosFornecedor.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosFornecedor.Exists)
                {
                    dirArquivosFornecedor.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosFornecedor.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosFornecedorProcessados.Exists)
                {
                    dirArquivosFornecedorProcessados.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosFornecedorProcessados.FullName + "\" criado com sucesso");
                }

                // Criando Diretórios Material
                if (!dirArquivosInvalidosMaterial.Exists)
                {
                    dirArquivosInvalidosMaterial.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosInvalidosMaterial.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosMaterial.Exists)
                {
                    dirArquivosMaterial.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosMaterial.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosMaterialProcessados.Exists)
                {
                    dirArquivosMaterialProcessados.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosMaterialProcessados.FullName + "\" criado com sucesso");
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.LogError("Erro ao criar os diretórios padrões da aplicação, favor verificar app.config.");
                Logger.LogError(e);
                return false;
            }
        }

        /// <summary>
        /// Método responsável por criar os diretórios necessários da aplicação se não existirem.
        /// </summary>
        /// <returns>Indicador de sucesso.</returns>
        /// <remarks>vxavier - Ci&T - 31/08/2012</remarks>
        public static Boolean CriarDiretoriosPadroesNfeStatus()
        {
            try
            {
                // Diretórios de NfeStatus
                DirectoryInfo dirArquivosInvalidosNfeStatus = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_NFESTATUS));
                DirectoryInfo dirArquivosNfeStatus = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_NFESTATUS));
                DirectoryInfo dirArquivosNfeStatusProcessados = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_NFESTATUS_PROCESSADOS));

                // Criando Diretórios NfeStatus
                if (!dirArquivosInvalidosNfeStatus.Exists)
                {
                    dirArquivosInvalidosNfeStatus.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosInvalidosNfeStatus.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosNfeStatus.Exists)
                {
                    dirArquivosNfeStatus.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosNfeStatus.FullName + "\" criado com sucesso");
                }

                if (!dirArquivosNfeStatusProcessados.Exists)
                {
                    dirArquivosNfeStatusProcessados.Create();
                    Logger.LogInfo("Diretorio \"" + dirArquivosNfeStatusProcessados.FullName + "\" criado com sucesso");
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.LogError("Erro ao criar os diretórios padrões da aplicação, favor verificar app.config.");
                Logger.LogError(e);
                return false;
            }
        }

        public static void IniciaProcessoSincronismo()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            ProcessarArquivosDiretorio();

            Logger.LogFinalMetodo(dataInicio);
        }

        public static void IniciaProcessoComunicacaoPedido(CentroDistribuicaoBE centroDistribuicao)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            PedidoSAP oPedidoSAP = new PedidoSAP();
            oPedidoSAP.Processar(centroDistribuicao);

            Logger.LogFinalMetodo(dataInicio);
        }

        public static void IniciaProcessoNfeStatus(CentroDistribuicaoBE centroDistribuicao)
        {
            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                List<String> arquivosProcessar = new List<string>();

                string mascara = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.PREFIXO_ARQUIVO_NFESTATUS) +
                    ObterParametroSistemaPorNomeParametro("COD_LOJA", centroDistribuicao.ListParametrosSistema) + "*" +
                    UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.SUFIXO_NOMENCLATURA_ARQUIVO);

                arquivosProcessar.AddRange(Directory.GetFiles(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_NFESTATUS), mascara).ToArray());

                NfeStatus.NfeStatusSAP oEntidadeNegocio;

                foreach (String arquivo in arquivosProcessar.Distinct())
                {
                    oEntidadeNegocio = new NfeStatus.NfeStatusSAP(centroDistribuicao);
                    oEntidadeNegocio.Read(arquivo);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        public static void IniciaProcessoExpurgo(CentroDistribuicaoBE centroDistribuicao)
        {
            DateTime inicio = Logger.LogInicioMetodo();

            ExpurgoSAP oEntidadeNegocio = new ExpurgoSAP();
            oEntidadeNegocio.Processar(centroDistribuicao);

            Logger.LogFinalMetodo(inicio);
        }

        /// <summary>
        /// carlosj CiT 26/09/2012 - Verifica as conexões das lojas e
        /// retorna apenas as lojas conectadas
        /// </summary>
        /// <returns>retorna as lojas conectadas</returns>
        public static List<LojaConexaoBE> ObterLojasConectadas()
        {
            List<LojaConexaoBE> listLojaConexaoBE = DALC.CommonDPKDALC.ObterConexaoLojas();
            Ping pinger = null;

            foreach (LojaConexaoBE item in listLojaConexaoBE)
            {
                pinger = new Ping();

                PingReply reply = pinger.Send(item.IP, item.TIME_OUT);

                item.CONECTADO = (reply.Status == IPStatus.Success);
            }

            // filtra a lista de retorno deixando apenas as lojas conectadas
            listLojaConexaoBE = listLojaConexaoBE.FindAll(val => { return val.CONECTADO == true; });

            return listLojaConexaoBE;
        }

        /// <summary>
        ///  carlosj CiT 08/10/2012 - ObterParametrosSistema
        /// </summary>
        /// <param name="codLoja"></param>
        /// <returns>um array com os parametros do sistema</returns>
        public static List<ParametroSistemaBE> ObterParametrosSistema(int codLoja)
        {
            List<ParametroSistemaBE> listParametrosSistemaBE = ListParamSistema;

            if (listParametrosSistemaBE == null)
            {
                listParametrosSistemaBE = DALC.CommonDPKDALC.ObterParametrosSistema(codLoja);
            }

            return listParametrosSistemaBE;
        }

        private static void ProcessarArquivosDiretorio()
        {
            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                List<String> arquivosProcessar = new List<string>();

                // Adiciona pasta de arquivos de cliente.
                arquivosProcessar.AddRange(Directory.GetFiles(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_CLIENTE)).ToArray());

                // Adiciona pasta de arquivos de fornecedores.
                arquivosProcessar.AddRange(Directory.GetFiles(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_FORNECEDOR)).ToArray());

                // Adiciona pasta de arquivos de materiais.
                arquivosProcessar.AddRange(Directory.GetFiles(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_MATERIAL)).ToArray());

                foreach (String arquivo in arquivosProcessar.Distinct())
                {
                    BusinessBase oEntidadeNegocio = BusinessFactory.GetEntidadeNegocio(arquivo);

                    oEntidadeNegocio.Read(arquivo);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        /// <summary>
        /// carlosj CiT 20/09/2012 - Obtem objeto cliente e retorna um string do xml 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string ConstruirNoXML(object entity)
        {
            string xml = string.Empty;

            XmlSerializer serializer = new XmlSerializer(entity.GetType());

            StringWriter otext = new StringWriter();

            serializer.Serialize(otext, entity);
            xml = otext.ToString();
            return xml;
        }

        /// <summary>
        /// Obtem o codigo da loja atraves do arquivo de configuracao do CD (H:\Oracle\Dados\32Bits\CD.txt)
        /// </summary>
        public static CentroDistribuicaoBE ObterCentroDistribuicao()
        {
            int codLoja;
            CentroDistribuicaoBE oCD = null;
            try
            {
                string caminhoArquivo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.CAMINHO_COMPLETO_ARQUIVO_CONFIGURACAO_CD);

                // verifica se o arquivo existe
                if (System.IO.File.Exists(caminhoArquivo))
                {
                    using (StreamReader objReader = new StreamReader(caminhoArquivo))
                    {
                        string linhaArquivo = string.Empty;

                        linhaArquivo = objReader.ReadLine();

                        if (linhaArquivo != null)
                        {
                            // tenta converter os dois primeiros caracteres da linha do arquivo em inteiro
                            if (Int32.TryParse(linhaArquivo.Substring(0, 2), out codLoja))
                            {
                                oCD = new CentroDistribuicaoBE();
                                // Define o número do CD.
                                oCD.COD_LOJA = codLoja;

                                // Define o tipo de banco do CD.
                                switch (linhaArquivo.Substring(2, 1))
                                {
                                    case "U":
                                        oCD.TipoBanco = EnumeradoresAplicacao.TipoBanco.Unico;
                                        break;
                                    case "M":
                                        oCD.TipoBanco = EnumeradoresAplicacao.TipoBanco.Multiplo;
                                        break;
                                }

                                // Se o banco é válido busca os parâmetros dele e retorna o objeto completo.
                                if (oCD.COD_LOJA > 0 && oCD.TipoBanco != EnumeradoresAplicacao.TipoBanco.Nenhum)
                                {
                                    oCD.ListParametrosSistema = ObterParametrosSistema(oCD.COD_LOJA);
                                }
                            }
                        }
                    }
                }
                if (oCD == null)
                {
                    Logger.LogError(String.Format(ResourceMensagem.arquivoCdTxtNaoIdentificado, UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.CAMINHO_COMPLETO_ARQUIVO_CONFIGURACAO_CD)));
                }
                else
                {
                    Logger.LogInfo(String.Format(ResourceMensagem.arquivoCdTxtEncontrado, oCD.COD_LOJA.ToString(), oCD.TipoBanco.ToString(), UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.CAMINHO_COMPLETO_ARQUIVO_CONFIGURACAO_CD)));
                }
            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);

            }
            // Se houve alguma falha na leitura do arquivo de CD retornará nulo.
            return oCD;
        }

        /// <summary>
        /// Método responsável por converter os status dos retornos do enumerador para o código equivalente no SAP.
        /// </summary>
        /// <param name="status">Enumerador de status.</param>
        /// <returns>String do tipo de código equivalente no SAP.</returns>
        public static string GetValorCodStatusSap(EnumeradoresAplicacao.Status status)
        {
            switch (status)
            {
                case EnumeradoresAplicacao.Status.Aviso:
                    return "W";
                case EnumeradoresAplicacao.Status.Erro:
                    return "E";
                case EnumeradoresAplicacao.Status.Sucesso:
                    return "S";
                default:
                    return "";
            }
        }

        public static string ObterParametroSistemaPorNomeParametro(string nomeParametro, List<ParametroSistemaBE> listParametros)
        {
            return listParametros.Single(item => item.NM_PARAMETRO == nomeParametro).VL_PARAMETRO;
        }

        #region monitoramento de pedidos

        /// <summary>
        /// Valida o usuário que entra na aplicação, carregando todos os CD's disponíveis a ele
        /// </summary>
        /// <param name="oUsuarioLogado">objeto do usuario logado</param>
        /// <param name="nomeAplicacao">nome da aplicação de monitoramento de pedidos - parametrizado no app.config</param>
        /// <returns></returns>
        public static UsuarioBE ValidaUsuario(UsuarioBE oUsuarioLogado, string nomeAplicacao, int codLoja)
        {
            UsuarioBE oUser = new UsuarioBE();
            try
            {
                oUser = CommonDPKDALC.ValidaUsuario(oUsuarioLogado);

                if (oUser != null)
                    oUser.listLojas = CommonDPKDALC.BuscarCDUsuario(codLoja, nomeAplicacao, oUser.COD_USUARIO);

            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);
                throw new Exception(String.Format(ResourceMensagem.ErroConectarBanco, codLoja));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);

            }
            return oUser;
        }



        /// <summary>
        /// Verifica se o usuário possui permissão para cancelar pedidos
        /// </summary>
        /// <param name="codUsuario">Código do usuário a ser verificado.</param>
        /// <returns>Permissão ou não para cancelamento do pedido</returns>
        public static bool VerificarPermissaoCancelPedido(Int64 codUsuario, int codLoja, EnumeradoresAplicacao.TipoBanco tipoBanco)
        {
            return CommonDPKDALC.VerificarPermissaoCancelPedido(codUsuario, codLoja, tipoBanco);
        }

        /// <summary>
        /// Busca o nome de exibição para os CDs
        /// </summary>
        /// <param name="cod_loja">Código da loja.</param>
        /// <returns>Nome de exibição para o CD</returns>
        public static string BuscarNomeExibicaoCDs(int cod_loja)
        {
            return CommonDPKDALC.BuscarNomeExibicaoCDs(cod_loja);
        }

        /// <summary>
        /// Método responsável por buscar a descrição do cancelamento do pedido
        /// </summary>
        /// <param name="cod_cancel">Código Cancel</param>
        /// <returns>Descrição do cancelamento</returns>
        public static string BuscarDescricaoCancelamento(int cod_cancel)
        {
            return CommonDPKDALC.BuscarDescricaoCancelamento(cod_cancel);
        }


        #endregion
    }
}
