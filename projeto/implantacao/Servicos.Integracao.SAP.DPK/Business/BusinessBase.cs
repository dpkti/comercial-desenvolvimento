﻿namespace Business
{

    public abstract class BusinessBase
    {
        /// <summary>
        /// Caminho do arquivo XML que será lido
        /// </summary>
        /// <param name="caminhoArquivo"></param>
        public abstract void Read(string caminhoArquivo);
    }

}