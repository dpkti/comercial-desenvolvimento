﻿using System;
using System.ServiceModel;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public abstract class BaseBO<T>
    {
        private ProcessarOUT _processarOut = new ProcessarOUT();

        public ProcessarOUT processarOut
        {
            get
            {
                return _processarOut;
            }
        }

        public void Processar(T item)
        {
            try
            {
                ProcessarInterno(item);
            }
            catch (DALCException ex)
            {
                if (ex.ErroDeConexao)
                {
                    _processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.ManterPasta;
                }

                _processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroBanco, ex.Message, processarOut.entidade);

                Logger.LogError(ex);
            }
            catch (Exception ex)
            {
                if (ex is CommunicationException || ex is TimeoutException)
                {
                    // Exceção timeout do SAP
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroComunicacaoPedidoSAP, ex.Message, processarOut.entidade);
                }
                else
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroDeSistema, ex.Message, processarOut.entidade);
                }

                Logger.LogError(ex);
            }
        }

        protected abstract void ProcessarInterno(T item);
    }
}
