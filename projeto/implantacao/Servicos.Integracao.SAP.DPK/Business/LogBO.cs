﻿using System;
using System.Transactions;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public static class LogBO
    {
        /// <summary>
        /// Método responsável por inserir logs dos arquivos processados e validados.
        /// </summary>
        /// <param name="entity">Lista de objetos de log preenchido.</param>
        /// <returns>ID do log.</returns>
        /// <remarks>vxavier - CiT (20/09/2012)</remarks>
        public static long InserirLog(ProcessarOUT processarOut)
        {
            long retorno = long.MinValue;

            if (processarOut != null)
            {
                try
                {
                    using (TransactionScope ts = new TransactionScope())
                    {
                        retorno = LogDALC.InserirArquivoXml(processarOut.LOG_GERAL.arquivoBE);
                        processarOut.LOG_GERAL.arquivoBE.ID = retorno;

                        foreach (LogDetalheBE log in processarOut.LOG_GERAL.listLogDetalhe)
                        {
                            LogDALC.InserirLog(log, retorno);
                        }

                        ts.Complete();
                    }

                    Logger.LogInfo("Arquivo " + processarOut.LOG_GERAL.arquivoBE.nomeArquivo + " inserido na tabela de log.");
                }
                catch (Exception ex)
                {
                    Logger.LogError("Erro ao inserir arquivo xml: " + processarOut.LOG_GERAL.arquivoBE.nomeArquivo + System.Environment.NewLine);
                    Logger.LogError(ex);
                }
            }

            return retorno;
        }
    }
}
