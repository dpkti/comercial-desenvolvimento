﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public abstract class BusinessGenericBase<T> : BusinessBase
    {
        protected string caminhoArquivoEntrada;
        protected string caminhoArquivoInvalido;
        protected string caminhoArquivoProcessado;
        protected bool gravarArquivoProcessado;
        protected string prefixoNomenclaturaArquivo;
        protected EnumeradoresAplicacao.TipoEntidade entidade;
        protected EnumeradoresAplicacao.AcaoArquivo acaoArquivo;

        public abstract ProcessarOUT Processar(T objetoParaIncluir, string nomeArquivo);

        /// <summary>
        /// Recebe o caminho onde está localizado o arquivo
        /// </summary>
        /// <param name="caminhoArquivo"></param>
        public override void Read(string caminhoArquivo)
        {
            XmlReader reader = null;
            XmlSerializer serializer = null;
            StreamReader lerArquivo = null;
            T obj = default(T);

            FileInfo detalhesArquivo;
            string nomeArquivo;
            string caminhoDestinoArquivoProcessado;

            // Pegar informações do arquivo.
            detalhesArquivo = new FileInfo(caminhoArquivo);
            nomeArquivo = detalhesArquivo.Name;

            // Instancia processarOut que será usado em exceções de XML e Nome inválido.
            ProcessarOUT processarOut = new ProcessarOUT()
            {
                acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados,
                STATUS = EnumeradoresAplicacao.Status.Sucesso,
            };
            processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;

            //verifica se o nome do arquivo é válido
            if (!ValidarNomenclaturaArquivo(nomeArquivo))
            {
                processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverInvalidos;
                processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.LeituraDeArquivo, "Nome do arquivo inválido.", EnumeradoresAplicacao.TipoEntidade.Indefinido);
                Logger.LogInfo("Nome do arquivo inválido: " + caminhoArquivo);
            }
            else
            {
                try
                {
                    lerArquivo = File.OpenText(caminhoArquivo);
                    reader = XmlReader.Create(lerArquivo, new XmlReaderSettings() { IgnoreComments = true });

                    serializer = new XmlSerializer(typeof(T));

                    obj = (T)serializer.Deserialize(reader);

                    lerArquivo.Close();
                    lerArquivo.Dispose();
                    serializer = null;
                    reader.Close();

                    // Executa o processamento do arquivo lido.
                    processarOut = Processar(obj, nomeArquivo);
                }
                catch (Exception ex)
                {
                    lerArquivo.Close();
                    lerArquivo.Dispose();
                    serializer = null;
                    reader.Close();

                    Type exType = ex.GetType();

                    if (exType == typeof(System.InvalidOperationException) ||
                        exType == typeof(System.Xml.XmlException))
                    {
                        processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverInvalidos;
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.LeituraDeArquivo, "Erro na deserialização do arquivo.", EnumeradoresAplicacao.TipoEntidade.Indefinido);
                    }
                    else
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.ErroDeSistema, ex.Message, EnumeradoresAplicacao.TipoEntidade.Indefinido);
                    }

                    Logger.LogInfo("Erro na leitura do arquivo: " + caminhoArquivo);
                    Logger.LogError(ex);
                }
            }

            if (processarOut.acaoArquivo == EnumeradoresAplicacao.AcaoArquivo.MoverInvalidos)
            {
                MoverArquivoInvalido(caminhoArquivo);
            }
            else if (processarOut.acaoArquivo == EnumeradoresAplicacao.AcaoArquivo.MoverProcessados && gravarArquivoProcessado)
            {
                caminhoDestinoArquivoProcessado = caminhoArquivoProcessado.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar + nomeArquivo;

                //verifica se o arquivo existe na pasta de arquivos processados e deleto, se existir
                if (File.Exists(caminhoDestinoArquivoProcessado))
                {
                    File.Delete(caminhoDestinoArquivoProcessado);
                }

                //move o arquivo para pasta de arquivos processados
                File.Move(caminhoArquivo, caminhoDestinoArquivoProcessado);
            }
            else if (processarOut.acaoArquivo != EnumeradoresAplicacao.AcaoArquivo.ManterPasta)
            {
                // Deleta arquivo processado com sucesso.
                File.Delete(caminhoArquivo);
            }

            // Se não contém erros ou avisos adiciona o log de sucesso.
            if (processarOut.LOG_GERAL.listLogDetalhe.Count == 0)
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Sucesso, EnumeradoresAplicacao.CodigoErro.Sucesso, ResourceMensagem.ProcessamentoArquivoRealizadoSucesso, processarOut.entidade));
            }
            // Se contém avisos adiciono o sucesso com mensagem de aviso.
            else if (!processarOut.verificaContemErros() && !processarOut.verificaContemSucesso() && processarOut.verificaContemAvisos())
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Sucesso, EnumeradoresAplicacao.CodigoErro.Sucesso, ResourceMensagem.ProcessamentoArquivoRealizadoSucessoComAvisos, processarOut.entidade));
            }

            // Salva o log no banco de dados.
            LogBO.InserirLog(processarOut);
        }

        /// <summary>
        /// Método responsável por mover arquivo para pasta de inválidos.
        /// </summary>
        /// <param name="caminhoArquivo">Caminho completo do arquivo inválido que deve ser movido.</param>
        /// <returns>Indicador de sucesso.</returns>
        private void MoverArquivoInvalido(string caminhoArquivo)
        {
            try
            {
                string nomeArquivo = new FileInfo(caminhoArquivo).Name;

                string caminhoArquivoarquivoInvalido = caminhoArquivoInvalido.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar + nomeArquivo;

                //verifica se o arquivo existe na pasta de arquivos invalidos e deleto, se exister
                if (File.Exists(caminhoArquivoarquivoInvalido))
                {
                    File.Delete(caminhoArquivoarquivoInvalido);
                }

                //move o arquivo para pasta de arquivos invalidos
                File.Move(caminhoArquivo, caminhoArquivoarquivoInvalido);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        /// <summary>
        ///  carlosj CiT 24/09/2012 - Valida a nomenclatura do arquivo conforme Key existente no app.config 
        /// </summary>
        /// <param name="nomeArquivo">nome do arquivo para ser validado</param>
        /// <param name="obj">entidade que está chamando o método</param>
        /// <returns>Se a nomenclatura estiver OK retorna True</returns>
        private bool ValidarNomenclaturaArquivo(string nomeArquivo)
        {
            DateTime inicio = Logger.LogInicioMetodo();

            string[] arraySufixo = null;
            string keySufixo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.SUFIXO_NOMENCLATURA_ARQUIVO);
            bool valido = false;

            try
            {
                if (string.IsNullOrEmpty(prefixoNomenclaturaArquivo) || string.IsNullOrEmpty(keySufixo))
                {
                    throw new Exception("A Key 'SufixoNomenclaturaArquivo' e 'SufixoNomenclaturaArquivo' é solicitada na classe ServicoBO, no método 'ValidarNomenclaturaArquivo'. Uma, outra ou ambas não existem no app.config.");
                }
                else if (nomeArquivo.ToUpper().StartsWith(prefixoNomenclaturaArquivo.ToUpper()))
                {
                    arraySufixo = keySufixo.Split(new char[] { '|' });

                    foreach (string item in arraySufixo)
                    {
                        if (nomeArquivo.ToUpper().EndsWith(item.ToUpper()))
                        {
                            valido = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }

            return valido;
        }
    }
}