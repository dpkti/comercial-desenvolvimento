﻿
using System.ComponentModel;
using Entities;
namespace Business
{
    public class WorkerDPA
    {
        private BackgroundWorker _myWorker = new BackgroundWorker();

        public delegate void ExecutarRotina(BackgroundWorker bug, object filtroIn);

        /// <summary>
        /// Evento genérico
        /// </summary>
        public event ExecutarRotina Rotina;

        private IMessageDPA _mens;

        /// <summary>
        /// retorna a instância do backgroundWorker atual
        /// </summary>
        public BackgroundWorker GetBackgroundWorker
        {
            get { return _myWorker; }

        }

        /// <summary>
        /// Construtor - define todos os métodos/eventos que o BWCustomizado terá.
        /// </summary>
        /// <param name="mens"></param>
        public WorkerDPA(IMessageDPA mens)
        {
            _myWorker.WorkerReportsProgress = true;
            _myWorker.ProgressChanged += new ProgressChangedEventHandler(_myWorker_ProgressChanged);
            _myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_myWorker_RunWorkerCompleted);
            _myWorker.DoWork += new DoWorkEventHandler(_myWorker_DoWork);
            _mens = mens;
        }

        /// <summary>
        /// Executa a rotina previamente definida para o evento de início de processamento. - Dispara o método RunWorkerAsync()
        /// </summary>
        public void execute()
        {
            if (!_myWorker.IsBusy)
                _myWorker.RunWorkerAsync();
        }

        private void _myWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Rotina(_myWorker, null);
        }

        private void _myWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _mens.addMessageComplete((string)e.Result);
        }

        private void _myWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _mens.addMessage(((BuscaPedidoOut)(e.UserState)));
        }



    }
}
