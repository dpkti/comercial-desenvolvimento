﻿using System;
using DALC;
using Entities;
using Entities.XSD.Cliente;
using UtilGeralDPA;

namespace Business
{

    public class ClienteBO : BaseBO<dtpSD_cliente>
    {
        public ClienteBO(dtpSD_Retconsultacliente retornoCliente, string nomeArquivo)
        {
            processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
            processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.ClienteBE;
            processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;
            processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(retornoCliente);
        }

        /// <summary>
        /// Recebe cliente do SAP e verifica se ele é cliente DPK, e se é uma nova alteração
        /// </summary>
        /// <param name="retornoCliente"></param>
        /// <returns>Retorna True caso consiga completar o fluxo inteiro, false caso não consiga</returns>
        /// <remarks>KEVLIN TOSHINARI OSSADA - Ci&T - 29/08/2012</remarks>
        protected override void ProcessarInterno(dtpSD_cliente retornoCliente)
        {
            long codClienteSap;
            ClienteBE oClienteDPK;
            DateTime dataInicio = Logger.LogInicioMetodo();

            codClienteSap = Convert.ToInt64(retornoCliente.NoCliente_SAP);
            oClienteDPK = ClienteDPKDALC.Instance.BuscaClienteDPK(codClienteSap);

            //Verificar se o cliente é da DPK
            if (oClienteDPK != null)
            {
                retornoCliente.NoCliente_POS = oClienteDPK.COD_CLIENTE;

                //Verificar a data de alteração do cliente
                if (oClienteDPK.DT_ATUALIZACAO < retornoCliente.DtAlteracaoSAP)
                {
                    //verifica as regras de negocio (se xml está ok)
                    ValidarRegrasNegocio(retornoCliente);

                    if (processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
                    {
                        //Atualizar base SDPK com os dados do cliente
                        ClienteDPKDALC.Instance.AtualizarDadosCliente(retornoCliente);

                        //logar CNPJ e COD_SAP no debug.trace
                        Logger.LogInfo(String.Format("Cadastro atualizado para cliente SAP {0}, código DPK {1}.", retornoCliente.NoCliente_SAP, oClienteDPK.COD_CLIENTE));
                    }
                    else
                    {
                        Logger.LogInfo(String.Format("Cadastro NÃO atualizado para cliente SAP {0} devido a inconsistências nos dados do cliente.", retornoCliente.NoCliente_SAP));
                    }
                }
                else
                {
                    processarOut.IncluirLogAviso(String.Format("Data de alteração do cliente SAP {0} ({1}) é menor ou igual do que a data de alteração do legado ({2}). Ignorando informações do SAP.", retornoCliente.NoCliente_SAP, retornoCliente.DtAlteracaoSAP, oClienteDPK.DT_ATUALIZACAO), EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                    Logger.LogWarning(String.Format("Data de alteração do cliente SAP {0} ({1}) é menor ou igual do que a data de alteração do legado ({2}). Ignorando informações do SAP.", retornoCliente.NoCliente_SAP, retornoCliente.DtAlteracaoSAP, oClienteDPK.DT_ATUALIZACAO));
                }
            }
            else
            {
                processarOut.IncluirLogAviso(String.Format("Cliente SAP {0} não é um cliente DPK.", retornoCliente.NoCliente_SAP), EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                Logger.LogWarning(String.Format("Cliente SAP {0} não é um cliente DPK.", retornoCliente.NoCliente_SAP));
            }

            Logger.LogFinalMetodo(dataInicio);
        }


        /// <summary>
        /// carlosj CiT (21/09/2012) 
        /// Concatena o endereco completo (rua, numero, complemento ) e o coloca na proprieda rua
        /// </summary>
        /// <param name="oCliente"></param>
        private void ConcatenarEnderecos(dtpSD_cliente oCliente)
        {

            for (int i = 0; i < oCliente.Enderecos.Length; i++)
            {
                if (!String.IsNullOrEmpty(oCliente.Enderecos[i].Complemento))
                {
                    oCliente.Enderecos[i].Rua = string.Format("{0}, {1}, {2}", oCliente.Enderecos[i].Rua, oCliente.Enderecos[i].NoResidencia, oCliente.Enderecos[i].Complemento);
                }
                else
                {
                    oCliente.Enderecos[i].Rua = string.Format("{0}, {1}", oCliente.Enderecos[i].Rua, oCliente.Enderecos[i].NoResidencia);
                }
            }
        }


        /// <summary>
        /// carlosj CiT (20/09/2012) - Valida os dados do cliente
        /// </summary>
        /// <param name="oCliente"></param>
        /// <returns></returns>
        private void ValidarRegrasNegocio(dtpSD_cliente oCliente)
        {
            bool retorno = true;
            string mensagem = string.Empty;

            processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(oCliente);

            //Verifica se o objeto cliente nao esta nulo
            if (oCliente != null)
            {
                //Tipo cliente
                oCliente.SubTpCliente = oCliente.SubTpCliente.Length > 2 ? oCliente.SubTpCliente.Substring(0, 2) : oCliente.SubTpCliente;

                //Nome cliente
                if (string.IsNullOrEmpty(oCliente.Name1))
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'Nome Cliente' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                    retorno = false;
                }

                //Verifica se existe fiscal
                if (oCliente.Fiscal != null)
                {
                    //Mensagem fiscal
                    if (string.IsNullOrEmpty(oCliente.Fiscal[0].MsgFiscal1))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'Mensagem Fiscal' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }
                }
                else
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Não existe 'Mensagem Fiscal'.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                    retorno = false;
                }

                //verifica se existe endereco
                if (oCliente.Enderecos != null)
                {

                    //Verifica se existe fiscal
                    if (oCliente.Fiscal != null)
                    {
                        if ((oCliente.Fiscal[0].MsgFiscal1.Equals("S")) || (oCliente.Enderecos[0].Regiao.Equals("SC")))
                        {
                            oCliente.Fiscal[0].MsgFiscal1 = "53";
                        }
                        else
                            if (oCliente.Fiscal[0].MsgFiscal2.Equals("S"))
                            {
                                oCliente.Fiscal[0].MsgFiscal1 = "56";
                            }
                            else
                            {
                                oCliente.Fiscal[0].MsgFiscal1 = string.Empty;
                            }
                    }

                    //verifica se a cidade está preenchido
                    if (string.IsNullOrEmpty(oCliente.Enderecos[0].Cidade))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, "Campo 'Cidade' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }


                    //verifica se bairro está preenchido
                    if (string.IsNullOrEmpty(oCliente.Enderecos[0].Bairro))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'Bairro' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }

                    //Rua do endereco do cliente
                    if (string.IsNullOrEmpty(oCliente.Enderecos[0].Rua))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'Rua' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }

                    //Número endereço de residência
                    if (string.IsNullOrEmpty(oCliente.Enderecos[0].NoResidencia))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'Número Endereço de Residência' não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }

                    //validaEnderecos adicionais
                    for (int i = 1; i < oCliente.Enderecos.Length; i++)
                    {
                        //Tipo de endereco
                        if (String.IsNullOrEmpty(oCliente.Enderecos[i].TpEndereco))
                        {
                            mensagem = string.Format("Campo 'Tipo Endereço Adicional' {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        //Grupo Conta Cliente
                        if (String.IsNullOrEmpty(oCliente.Enderecos[i].GrpContaClient))
                        {
                            mensagem = string.Format("Campo 'Grupo Conta Cliente' do Endereço Adicional {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        //verifica se a cidade está preenchido
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].Cidade))
                        {
                            mensagem = string.Format("Campo 'Cidade' do  Endereço Adicional  {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        //verifica se bairro está preenchido
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].Bairro))
                        {
                            mensagem = string.Format("Campo 'Bairro' do  Endereço Adicional  {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        //Rua do endereco do cliente
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].Rua))
                        {
                            mensagem = string.Format("Campo 'Rua' do Endereço Adicional  {0}  não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        //Número endereço de residência
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].NoResidencia))
                        {
                            mensagem = string.Format("Campo 'Número' do  Endereço Adicional {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].CEP))
                        {
                            mensagem = string.Format("Campo 'CEP' do Endereço Adicional  {0}  não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }

                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].Regiao))
                        {
                            mensagem = string.Format("Campo 'Região (UF)' do  Endereço Adicional  {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].DDDFone))
                        {
                            mensagem = string.Format("Campo 'DDD' do  Endereço Adicional  {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }
                        if (string.IsNullOrEmpty(oCliente.Enderecos[i].Telefone))
                        {
                            mensagem = string.Format("Campo 'Telefone' do  Endereço Adicional  {0} não está preenchido.", i + 1);
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }
                    }//fim da verificacao dos enderecos adicionais


                    //concatena os endereços colocando na propriedade rua
                    ConcatenarEnderecos(oCliente);


                    //CEP do endereço principal
                    if (string.IsNullOrEmpty(oCliente.Enderecos[0].CEP))
                    {
                        processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Campo 'CEP' do Endereço de Residência não está preenchido.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                        retorno = false;
                    }

                }
                else //fim da verificacao de endereco
                {
                    // Endereço vazio
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, "Campos de Endereço não preenchidos.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                    retorno = false;
                }


                //CONTATOS
                if (oCliente.Contatos != null)
                {
                    for (int cont = 0; cont < oCliente.Contatos.Length; cont++)
                    {

                        if (string.IsNullOrEmpty(oCliente.Contatos[cont].Nome))
                        {
                            //a mensagem varia caso seja o contato principal
                            mensagem = (cont > 0) ? string.Format("Campo Nome do Contato {0} ", cont + 1) : "Campo 'Nome' do Contato principal não está preenchido.";
                            processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, mensagem, EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                            retorno = false;
                        }
                    }

                }
                else //CONTATO VAZIO
                {
                    processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Cliente sem Contato.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                    retorno = false;
                }

            }//fim da verificacao do cliente
            else
            {
                processarOut.IncluirLogErro(EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, "Cliente não encontrado.", EnumeradoresAplicacao.TipoEntidade.ClienteBE);
                retorno = false;
            }

            //NoIDFiscal3
            if (string.IsNullOrEmpty(oCliente.NoIDFiscal3))
            {
                oCliente.NoIDFiscal3 = "ISENTO";
            }

            //seta Status do processarOUT
            processarOut.STATUS = retorno ? EnumeradoresAplicacao.Status.Sucesso : EnumeradoresAplicacao.Status.Erro;
        }
    }
}