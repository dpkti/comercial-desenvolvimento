﻿using System;
using Entities;
using Entities.XSD.Cliente;
using UtilGeralDPA;

namespace Business
{
    /// <summary>
    /// Classe utilizada para representar Clientes DPK e Representantes DPK.
    /// </summary>
    public class ClienteSAP : BusinessGenericBase<dtpSD_Retconsultacliente>
    {
        /// <summary>
        /// Construtor.
        /// </summary>
        public ClienteSAP()
        {
            // Obtém as configurações de arquivo utilizadas para a classe.
            base.caminhoArquivoEntrada = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_CLIENTE);
            base.caminhoArquivoProcessado = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_CLIENTE_PROCESSADOS);
            base.gravarArquivoProcessado = Convert.ToBoolean(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.GRAVAR_ARQUIVOS_CLIENTE_PROCESSADOS));
            base.caminhoArquivoInvalido = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_CLIENTE);
            base.prefixoNomenclaturaArquivo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.PREFIXO_ARQUIVO_CLIENTE);
        }

        public override ProcessarOUT Processar(dtpSD_Retconsultacliente retornoCliente, string nomeArquivo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            dtpSD_cliente[] listaClientes = retornoCliente.item;
            ClienteBO clienteBo = new ClienteBO(retornoCliente, nomeArquivo);

            foreach (var cliente in listaClientes)
            {
                // Representante
                if (cliente.GrpContaClient == EnumeradoresAplicacao.GrupoContaCliente.ZREP.ToString())
                {
                    //RepresentanteBO.Processar(cliente);
                    clienteBo.processarOut.IncluirLogAviso("Representante DPK ainda não implementado. Ignorando entrada. Número SAP: " + cliente.NoCliente_SAP, EnumeradoresAplicacao.TipoEntidade.Indefinido);
                    Logger.LogInfo("Representante DPK ainda não implementado. Ignorando arquivo.");
                }
                // Cliente
                else
                {
                    clienteBo.Processar(cliente);
                }
            }

            Logger.LogFinalMetodo(dataInicio);

            return clienteBo.processarOut;
        }
    }
}
