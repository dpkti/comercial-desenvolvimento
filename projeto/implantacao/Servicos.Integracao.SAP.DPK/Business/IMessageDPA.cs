﻿
using Entities;
namespace Business
{
    /// <summary>
    /// Interface para adicionar mensagem de report de processamento
    /// </summary>
    public interface IMessageDPA
    {
        void addMessage(BuscaPedidoOut objMensagem);

        void addMessageComplete(string texto);
    }
}
