﻿using System;
using System.Collections.Generic;
using System.Linq;
using DALC;
using Entities;
using Entities.XSD.Material;
using UtilGeralDPA;

namespace Business
{
    public class ProdutoBO : BaseBO<dtpMM_Material>
    {
        public ProdutoBO(string nomeArquivo, dtpMM_Material material)
        {
            base.processarOut.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
            base.processarOut.STATUS = EnumeradoresAplicacao.Status.Sucesso;
            base.processarOut.entidade = EnumeradoresAplicacao.TipoEntidade.MaterialBE;
            base.processarOut.LOG_GERAL.arquivoBE.nomeArquivo = nomeArquivo;
            base.processarOut.LOG_GERAL.arquivoBE.xml = ServicoBO.ConstruirNoXML(material);
        }

        protected override void ProcessarInterno(dtpMM_Material material)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool materialExistente = MaterialDPKDALC.Instance.VerificarExistenciaMaterialDPK(material.Material);

            ValidaRegraNegocios(material);

            if (processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
            {
                // Inserir material
                if (!materialExistente)
                {
                    MaterialDPKDALC.Instance.InserirDadosMaterial(material);
                }
                // Atualizar material existente.
                else
                {
                    MaterialDPKDALC.Instance.AtualizarDadosMaterial(material);
                }

                List<LojaConexaoBE> listLojasConectadas = ServicoBO.ObterLojasConectadas();
                foreach (LojaConexaoBE lj in listLojasConectadas)
                {
                    try
                    {
                        MaterialDPKDALC.Instance.InserirAtualizarDadosMaterialPorOWNER(material, lj.COD_LOJA.ToString());
                    }
                    catch (Exception)
                    {

                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Aviso, EnumeradoresAplicacao.CodigoErro.FalhaComunicacaoOutrosCDs, string.Format(ResourceMensagem.ErroComunicaoBDloja, lj.COD_LOJA.ToString()), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                }
            }

            Logger.LogFinalMetodo(dataInicio);
        }

        /// <summary>
        /// Validação da Regra de negócios  todos os campos obrigatórios 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaRegraNegocios(dtpMM_Material material)
        {
            ValidaDadosRegistroInfo(material);

            ValidaCaracteristicas(material);

            ValidaGrupoSubGrupo(material);

            ValidaUndMedVenda(material);

            ValidaCodEAN(material);

            ValidaDenomMaterial(material);

            ValidaPesoBruto(material);

            ValidaVolume(material);

            ValidaOrigMaterial(material);

            ValidaDtCriacao(material);

            ValidaDadosDeCentro(material);

            ValidaQtdFornMin(material);

            ValidaSetorAtiv(material);

            if (processarOut.verificaContemErros())
            {
                processarOut.STATUS = EnumeradoresAplicacao.Status.Erro;
            }
        }

        /// <summary>
        /// Valida se ja existe Cod_Fornecedor + Cod_Fabrica
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaFornecedorFabrica(dtpMM_Material material)
        {
            string cod_dpk = MaterialDPKDALC.Instance.VerificarExistenciaFornecedorFabrica(material.Registros[0].CodFornecedor, material.Registros[0].MaterialFornec);
            if (!string.IsNullOrEmpty(cod_dpk))
            {
                if (!cod_dpk.Equals(material.Material))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ExisteChaveComposta, "Cod_Fornecedor - Cod_Fabrica", "Cod_Fornecedor + Cod_Fabrica"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
        }

        /// <summary>
        /// Valida os campo áreaVenda -> QtdFornMin
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaQtdFornMin(dtpMM_Material material)
        {
            if (material.areaVendas.Length == 0)
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorMaiorQue, "QtdFornMin", "1 (um)"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (string.IsNullOrEmpty(material.areaVendas[0].QtdFornMin))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorMaiorQue, "QtdFornMin", "1 (um)"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Valida os campo áreaVenda -> SetorAtiv
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaSetorAtiv(dtpMM_Material material)
        {
            if ((material.areaVendas.Length > 0 && string.IsNullOrEmpty(material.areaVendas[0].SetorAtiv)))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "SetorAtiv"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (!(material.areaVendas[0].SetorAtiv.Equals("LE") || material.areaVendas[0].SetorAtiv.Equals("PE") || material.areaVendas[0].SetorAtiv.Equals("AC")))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorIgual, "SetorAtiv", "LE, PE ou AC"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Valida os campos CARACTERISTICA
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaCaracteristicas(dtpMM_Material material)
        {
            ValidaNovoItemSubstituido(material);

            ValidaCodLinha(material);

            //CODIGO_MASCARADO
            string caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_CODIGO_MASCARADO, material);
            if (string.IsNullOrEmpty(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "CODIGO_MASCARADO"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }

            //COD_TRIB_IPI
            caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_TRIB_IPI, material);
            if (string.IsNullOrEmpty(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "COD_TRIB_IPI"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }

            //COD_TRIB
            caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_TRIB, material);
            if (string.IsNullOrEmpty(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "COD_TRIB"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }

            //PC_IPI
            caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_PC_IPI, material);
            if (string.IsNullOrEmpty(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "PC_IPI"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Validação se o código NOVO_ITEM_SUBSTITUIDO existe na tabela como chave primária
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaNovoItemSubstituido(dtpMM_Material material)
        {
            string caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_NOVO_ITEM_SUBSTITUIDO, material);
            if (!string.IsNullOrEmpty(caracteristica) && !MaterialDPKDALC.Instance.VerificarExistenciaMaterialDPK(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ItemValorInconsistente, "NOVO_ITEM_SUBSTITUIDO"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Validação se o campo COD_LINHA existe na PRODUÇÃO.LINHA
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaCodLinha(dtpMM_Material material)
        {
            string caracteristica = MaterialSAP.GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_LINHA, material);
            if (string.IsNullOrEmpty(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ItemEmBranco, "COD_LINHA"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (!MaterialDPKDALC.Instance.VerificarExistenciaLinhaDPK(caracteristica))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ItemValorInconsistente, "COD_LINHA"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Verifica se há apenas 1 registroInfo e os campos de RegistroInfo
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaDadosRegistroInfo(dtpMM_Material material)
        {
            //Valida se há 1 RegistroInfo
            if (material.Registros == null || material.Registros.Length != 1)
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ObrigatoriedadeMaxima, "Registros", "1 <item>"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                return;
            }

            //Validação de MaterialFornec
            if (string.IsNullOrEmpty(material.Registros[0].MaterialFornec))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "MaterialFornec", "Registros"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (material.Registros[0].MaterialFornec.Length > 19)
            {
                material.Registros[0].MaterialFornec = material.Registros[0].MaterialFornec.Substring(0, 19);
            }

            //Validação de QtdMin
            if (string.IsNullOrEmpty(material.Registros[0].QtdMin))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "QtdMin", "Registros"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }

            //Validação de CodFornecedor
            if (string.IsNullOrEmpty(material.Registros[0].CodFornecedor))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.CampoValorEmBranco, "CodFornecedor"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (!MaterialDPKDALC.Instance.VerificarExistenciaFornecedorDPK(material.Registros[0].CodFornecedor))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ItemValorInconsistente, "CodFornecedor"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }

            ValidaFornecedorFabrica(material);
        }

        /// <summary>
        /// Validação de atributos de CENTRO
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaDadosDeCentro(dtpMM_Material material)
        {
            if (material.Centro.Length <= 0)
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.TagNaoExiste, "Centro"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else
            {
                if (string.IsNullOrEmpty(material.Centro[0].Ncm))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "Ncm"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
                else if (ContemLetras(material.Centro[0].Ncm))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "Ncm"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
                else
                {
                    material.Centro[0].Ncm = UtilGeralDPA.General.RetornaApenasAlfanumericos(material.Centro[0].Ncm);
                    if (material.Centro[0].Ncm.Length > 10)
                    {
                        material.Centro[0].Ncm = material.Centro[0].Ncm.Substring(0, 10);
                    }
                }

                if (!string.IsNullOrEmpty(material.Centro[0].EstoqSegMin))
                {
                    decimal result;
                    string[] arrayEstoqSegMin = material.Centro[0].EstoqSegMin.Split('.');
                    if (decimal.TryParse(material.Centro[0].EstoqSegMin, out result))
                    {
                        if (arrayEstoqSegMin.Length != 2)
                        {
                            processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "EstoqSegMin"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                        }
                        else if (arrayEstoqSegMin[0].Length != 10 || arrayEstoqSegMin[1].Length != 3)
                        {
                            processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "EstoqSegMin"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                        }
                    }
                    else
                    {
                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "EstoqSegMin"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                }
            }
        }

        /// <summary>
        /// Validação do atributo DtCriacao
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaDtCriacao(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.DtCriacao))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "DtCriacao"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Validação de atributo OrigMaterial
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaOrigMaterial(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.OrigMaterial))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "OrigMaterial"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Validação de atributo Volume
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaVolume(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.Volume))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "Volume"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else
            {
                decimal result;
                string[] arrayVolume = material.Volume.Split('.');
                if (decimal.TryParse(material.Volume, out result))
                {
                    if (arrayVolume.Length != 2)
                    {
                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Volume"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                    else if (arrayVolume[0].Length != 10 || arrayVolume[1].Length != 3)
                    {
                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Volume"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                }
                else
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "Volume"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
        }

        /// <summary>
        /// Validação de atributo PesoBruto
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaPesoBruto(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.PesoBruto))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "PesoBruto"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else
            {
                decimal result;
                string[] arrayPeso = material.PesoBruto.Split('.');
                if (decimal.TryParse(material.PesoBruto, out result))
                {
                    if (arrayPeso.Length != 2)
                    {
                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "PesoBruto"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                    else if (arrayPeso[0].Length != 10 || arrayPeso[1].Length != 3)
                    {
                        processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "PesoBruto"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                    }
                }
                else
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "PesoBruto"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
        }

        /// <summary>
        /// Validação do atributo CODEAN
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaCodEAN(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.CodEAN))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "CodEAN"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (!string.IsNullOrEmpty(material.CodEAN))
            {
                if (ContemLetras(material.CodEAN))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorInconsistente, "CodEAN"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
                else if (material.CodEAN.Length > 13)
                {
                    material.CodEAN = material.CodEAN.Substring(0, 13);
                }

                string codDPK = MaterialDPKDALC.Instance.VerificarExistenciaEANDPK(material.CodEAN);
                if (!string.IsNullOrEmpty(codDPK) && !codDPK.Equals(material.Material))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.JaExiste, "O código EAN"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
        }

        /// <summary>
        /// Validação se UndMedVenda e verificar se ele existe na tabela UNIDADE do owner PRODUCAO
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaUndMedVenda(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.UndMedVenda))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "UndMedVenda"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (material.UndMedVenda.Length == 2)
            {
                material.UndMedVenda = material.UndMedVenda.Substring(0, 2);
                if (!MaterialDPKDALC.Instance.VerificarExistenciaUnidadeDPK(material.UndMedVenda))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "UndMedVenda"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
            else
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioFormatoIncorreto, string.Format(ResourceMensagem.ValorInconsistente, "UndMedVenda"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
        }

        /// <summary>
        /// Validação de atributo DenomMaterial
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaDenomMaterial(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.DenomMaterial))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "DenomMaterial"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
        }

        /// <summary>
        /// Validação se o atributo GrpMercadorias existe na tabela Grupo e Subgrupo do owner produção
        /// </summary>
        /// <param name="material"></param>
        /// <param name="processarOut"></param>
        private void ValidaGrupoSubGrupo(dtpMM_Material material)
        {
            if (string.IsNullOrEmpty(material.GrpMercadorias))
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.CampoObrigatorioEmBranco, string.Format(ResourceMensagem.ValorEmBranco, "GrpMercadorias"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else if (material.GrpMercadorias.Length < 4)
            {
                processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.ValorForaDominio, string.Format(ResourceMensagem.ValorForaDominio, "GrpMercadorias"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
            }
            else
            {
                string grupo = material.GrpMercadorias.Substring(0, 2);
                string subgrupo = material.GrpMercadorias.Substring(2, 2);

                if (!MaterialDPKDALC.Instance.VerificarExistenciaGrupoSubgrupoDPK(grupo, subgrupo))
                {
                    processarOut.LOG_GERAL.listLogDetalhe.Add(new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, EnumeradoresAplicacao.CodigoErro.DadoInconsistente, string.Format(ResourceMensagem.ValorInconsistente, "GrpMercadorias"), EnumeradoresAplicacao.TipoEntidade.MaterialBE));
                }
            }
        }

        /// <summary>
        /// Verifica se uma string contém letras
        /// </summary>
        /// <param name="texto">string a ser validada</param>
        /// <returns>TRUE se contém letras e FALSE se não contém letras</returns>
        public bool ContemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
                return true;
            else
                return false;
        }
    }
}
