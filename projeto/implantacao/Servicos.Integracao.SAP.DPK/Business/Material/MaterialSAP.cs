﻿using System;
using System.Linq;
using Entities;
using Entities.XSD.Material;
using UtilGeralDPA;

namespace Business
{
    public class MaterialSAP : BusinessGenericBase<dtpMM_Material>
    {

        public MaterialSAP()
        {
            // Obtém as configurações de arquivo utilizadas para a classe.
            base.caminhoArquivoEntrada = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_MATERIAL);
            base.caminhoArquivoProcessado = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_MATERIAL_PROCESSADOS);
            base.gravarArquivoProcessado = Convert.ToBoolean(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.GRAVAR_ARQUIVOS_MATERIAL_PROCESSADOS));
            base.caminhoArquivoInvalido = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_ARQUIVOS_INVALIDOS_MATERIAL);
            base.prefixoNomenclaturaArquivo = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.PREFIXO_ARQUIVO_MATERIAL);
        }

        public override ProcessarOUT Processar(dtpMM_Material material, string nomeArquivo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            string materialDPK = GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_DPK, material);
            ProdutoBO produtoBO = new ProdutoBO(nomeArquivo, material);

            if (materialDPK.ToUpper().Equals("S"))
            {
                produtoBO.Processar(material);
                if (produtoBO.processarOut.STATUS == EnumeradoresAplicacao.Status.Sucesso)
                {
                    Logger.LogInfo(string.Format(ResourceMensagem.ProcessamentoEntidadeRealizadoSucesso, material.ToString(), material.Material));
                }
                else
                {
                    Logger.LogInfo(string.Format(ResourceMensagem.ProcessamentoEntidadeComFalha, material.ToString(), material.Material));
                }
            }
            else
            {
                produtoBO.processarOut.IncluirLogAviso(string.Format(ResourceMensagem.ArquivoIgnorado, "Caracteristica/DPK = N"), EnumeradoresAplicacao.TipoEntidade.MaterialBE);

                Logger.LogInfo(string.Format(ResourceMensagem.ArquivoIgnorado, "Material " + material.Material + " não é fornecedor DPK"));
            }

            return produtoBO.processarOut;
        }

        /// <summary>
        /// Método responsável por obter um valor de caracteristica de um material pelo seu nome.
        /// </summary>
        /// <param name="nomeCaracteristica">Nome da característica.</param>
        /// <param name="fornecedor">Objeto fornecedor.</param>
        /// <returns>Valor em texto da característica.</returns>
        public static string GetValorCaracteristica(string nomeCaracteristica, dtpMM_Material material)
        {
            string retorno = string.Empty;

            if (material.Caracteristicas != null)
            {
                retorno = (from c in material.Caracteristicas
                           where c != null &&
                                 c.Material != null &&
                                 c.Nome != null &&
                                 c.Valor != null &&
                                 c.Nome.Equals(nomeCaracteristica) &&
                                 c.Material.Equals(material.Material)
                           select c.Valor.Trim()).FirstOrDefault();
                if (retorno == null)
                {
                    retorno = string.Empty;
                }

            }
            return retorno;
        }
    }
}
