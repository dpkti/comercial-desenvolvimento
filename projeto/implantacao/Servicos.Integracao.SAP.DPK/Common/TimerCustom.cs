﻿using System;

namespace CommonServico
{
    public class TimerCustom : System.Timers.Timer
    {
        private Boolean process = false;
        private Boolean init = false;

        public TimerCustom(double interval) : base(interval) { }
        public TimerCustom() : base() { }

        public void StopProcess()
        {
            if (!init)
            {
                base.Stop();
            }
            process = false;
        }

        public void StartProcess()
        {
            if (!process)
            {
                base.Start();
            }
            process = true;
        }

        public void BeginProcess()
        {
            base.Stop();
            init = true;
        }

        public void EndProcess()
        {
            init = false;
            if (process)
            {
                base.Start();
            }
        }
    }
}
