﻿
using System;
namespace Entities
{
    public class FiltroBuscaPedidoIn
    {
        private string _status;
        public int CodLoja { get; set; }
        public string Status
        {
            get { return _status; }
            set
            {
                if (value.Length == 1)
                    this._status = value;
                else
                {
                    switch ((EnumeradoresAplicacao.StatusPedido)Enum.Parse(typeof(EnumeradoresAplicacao.StatusPedido), value))
                    //((string)value)
                    {
                        case (EnumeradoresAplicacao.StatusPedido.Aguardando): this._status = "W";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Cancelado): this._status = "C";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Cancelamento_Negado): this._status = "N";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Erro_SAP): this._status = "E";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Expurgado): this._status = "X";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Faturado): this._status = "F";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Pendente): this._status = "P";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Sucesso): this._status = "S";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string[] ListStatus { get; set; }
        public int CodLojaBase { get; set; }
        public FiltroBuscaPedidoIn()
        {
            this.ListStatus = new string[0];
        }

    }
}
