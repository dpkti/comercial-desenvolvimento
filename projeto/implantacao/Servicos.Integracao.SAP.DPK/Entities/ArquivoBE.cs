﻿
namespace Entities
{
    /// <summary>
    ///  carlosj CiT 20/09/2012 - ArquivoBE
    /// </summary>
    public class ArquivoBE
    {
        public long ID { get; set; }
        public string nomeArquivo { get; set; }
        public string xml { get; set; }
    }
}
