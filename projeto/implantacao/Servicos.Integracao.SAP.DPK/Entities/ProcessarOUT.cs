﻿using System.Linq;

namespace Entities
{
    /// <summary>
    ///  carlosj CiT 20/09/2012 - Classe ProcessarOUT
    /// </summary>
    public class ProcessarOUT
    {
        /// <summary>
        /// carlosj CiT 20/09/2012 - construtor
        /// </summary>
        public ProcessarOUT()
        {
            this.LOG_GERAL = new LogGeralBE();
            this.acaoArquivo = EnumeradoresAplicacao.AcaoArquivo.MoverProcessados;
        }

        public EnumeradoresAplicacao.Status STATUS { get; set; }
        public LogGeralBE LOG_GERAL { get; set; }
        public EnumeradoresAplicacao.AcaoArquivo acaoArquivo { get; set; }
        public EnumeradoresAplicacao.TipoEntidade entidade { get; set; }

        /// <summary>
        /// Verifica se entre os itens listados em listLogDetalhe existe algum com status Erro
        /// </summary>
        /// <returns>TRUE se encontou item de erro e FALSE se não contém item de erro</returns>
        public bool verificaContemErros()
        {
            return this.LOG_GERAL.listLogDetalhe.Count(x => x.STATUS == EnumeradoresAplicacao.Status.Erro) > 0 ? true : false;
        }

        /// <summary>
        /// Verifica se entre os itens listados em listLogDetalhe existe algum com status Erro
        /// </summary>
        /// <returns>TRUE se encontou item de erro e FALSE se não contém item de erro</returns>
        public bool verificaContemAvisos()
        {
            return this.LOG_GERAL.listLogDetalhe.Count(x => x.STATUS == EnumeradoresAplicacao.Status.Aviso) > 0 ? true : false;
        }

        /// <summary>
        /// Verifica se entre os itens listados em listLogDetalhe existe algum com status Erro
        /// </summary>
        /// <returns>TRUE se encontou item de erro e FALSE se não contém item de erro</returns>
        public bool verificaContemSucesso()
        {
            return this.LOG_GERAL.listLogDetalhe.Count(x => x.STATUS == EnumeradoresAplicacao.Status.Sucesso) > 0 ? true : false;
        }

        public void IncluirLogErro(EnumeradoresAplicacao.CodigoErro cod_erro, string descricao, EnumeradoresAplicacao.TipoEntidade entidade)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, cod_erro, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

        public void IncluirLogAviso(string descricao, EnumeradoresAplicacao.TipoEntidade entidade)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Aviso, EnumeradoresAplicacao.CodigoErro.Sucesso, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

        public void IncluirLogSucesso(string descricao, EnumeradoresAplicacao.TipoEntidade entidade)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Sucesso, EnumeradoresAplicacao.CodigoErro.Sucesso, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

        public void IncluirLogErro(EnumeradoresAplicacao.CodigoErro cod_erro, string descricao)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Erro, cod_erro, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

        public void IncluirLogAviso(string descricao)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Aviso, EnumeradoresAplicacao.CodigoErro.Sucesso, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

        public void IncluirLogSucesso(string descricao)
        {
            LogDetalheBE log = new LogDetalheBE(EnumeradoresAplicacao.Status.Sucesso, EnumeradoresAplicacao.CodigoErro.Sucesso, descricao, entidade);
            this.LOG_GERAL.listLogDetalhe.Add(log);
        }

    }

}
