﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    /// <summary>
    /// carlosj  CiT 20/09/2012 - LogDetalheBE
    /// </summary>
    public class LogDetalheBE
    {
        public EnumeradoresAplicacao.Status STATUS { get; set; }
        public EnumeradoresAplicacao.CodigoErro COD_ERRO { get; set; }
        public string DESCRICAO { get; set; }
        public EnumeradoresAplicacao.TipoEntidade TIPO_ENTIDADE { get; set; }
       
        //carlosj  CiT 20/09/2012 - Construtor da classe 
        public LogDetalheBE(EnumeradoresAplicacao.Status status, EnumeradoresAplicacao.CodigoErro cod_Erro, string descricao,
                     EnumeradoresAplicacao.TipoEntidade tipoEntidade)
        {
            this.STATUS = status;
            this.COD_ERRO = cod_Erro;
            this.DESCRICAO = descricao;
            this.TIPO_ENTIDADE = tipoEntidade;
        }
    }
}
