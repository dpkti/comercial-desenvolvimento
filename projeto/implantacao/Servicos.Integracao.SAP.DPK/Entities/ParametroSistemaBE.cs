﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    /// <summary>
    /// carlosj CiT (08/10/2012)
    ///  Classe que representa a tbl 'SAP.PARAMETROS_SISTEMA'
    /// </summary>
    public class ParametroSistemaBE
    {
        /// <summary>
        /// Código da loja
        /// </summary>
        public int COD_LOJA { get; set;}
        
        /// <summary>
        /// Nome do Parametro
        /// </summary>
        public string NM_PARAMETRO { get; set; }
        
        /// <summary>
        /// Valor Parametro
        /// </summary>
        public string VL_PARAMETRO { get; set; }

    }
}
