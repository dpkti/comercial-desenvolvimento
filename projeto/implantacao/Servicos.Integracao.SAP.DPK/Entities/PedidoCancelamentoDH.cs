﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class PedidoCancelamentoDH
    {
        public Int64 NUM_NOTA { get; set; }
        public Int64 NUM_PEDIDO { get; set; }
        public int SEQ_PEDIDO { get; set; }
        public string SIGLA_FILIAL { get; set; }
        public DateTime DT_EMISSAO_NOTA { get; set; }
        public Int64 COD_TRANSP { get; set; }
        public string NOME_TRANSP { get; set; }
        public string VIA_TRANSP { get; set; }
        public string DIVISAO { get; set; }
        public string ORIGEM { get; set; }
        public Int64 FILIAL_REPR { get; set; }
        public Int64 COD_REPRES { get; set; }
        public string PSEUDO_REPR { get; set; }
        public Int64 COD_VEND { get; set; }
        public string PSEUDO_VEND { get; set; }
        public Int64 COD_CLIENTE { get; set; }
        public string NOME_CLIENTE { get; set; }
        public Int64 CGC_CLIENTE { get; set; }
        public string CID_CLIENTE { get; set; }
        public string UF_CLIENTE { get; set; }
        public Int32 COD_PLANO { get; set; }
        public string DESC_PLANO { get; set; }
        public string COD_NOPE { get; set; }
        public string DESC_NATUREZA { get; set; }
        public Decimal PC_ACRESCIMO { get; set; }
        public Decimal PC_DESCONTO { get; set; }
        public Decimal PESO_BRUTO { get; set; }
        public DateTime DT_PEDIDO { get; set; }
        public Int32 QTD_ITEM_PEDIDO { get; set; }
        public Decimal VL_CONTABIL { get; set; }
        public string FL_DIF_ICM { get; set; }
        public string MENS_PEDIDO { get; set; }
        public string MENS_NOTA { get; set; }
        public int COD_CANCEL { get; set; }
        public int SITUACAO { get; set; }
        public int COD_LOJA { get; set; }
        public Int64 CLIENTE_COD_SAP { get; set; }
        public string ID_CONTROLE_PEDIDO_SAP { get; set; }
        public string STATUS { get; set; }
        public string LOJA_COD_SAP { get; set; }
    }
}
