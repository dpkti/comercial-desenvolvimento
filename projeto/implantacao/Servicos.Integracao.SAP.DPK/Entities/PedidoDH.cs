﻿
using System;
namespace Entities
{
    public class PedidoDH
    {
        public string NUMERO_PEDIDO { get; set; }
        public string NOME_CLIENTE { get; set; }
        public string ID_CONTROLE_PEDIDO { get; set; }
        public string NOME_TRANSPORTADORA { get; set; }
        public double VALOR { get; set; }
        public DateTime DATA_PEDIDO { get; set; }
        public string STATUS { get; set; }
        public string STATUS_DESCRICAO { get; set; }
        public string TEM_DIF_ESTOQUE { get; set; }
        public int COD_LOJA { get; set; }
        public DateTime DATA_ENVIO { get; set; }

    }
}
