﻿
using System.Collections.Generic;
namespace Entities
{
    public class BuscaPedidoOut
    {
        public List<PedidoDH> ListPedidosDH { get; set; }
        public StatusBuscaPedidoOut Status { get; set; }
        public string Mensagem { get; set; }
        public int QuantidadeRegistros { get; set; }
    }
    public enum StatusBuscaPedidoOut
    {
        Erro,
        Processando,
        Sucesso
    }
}
