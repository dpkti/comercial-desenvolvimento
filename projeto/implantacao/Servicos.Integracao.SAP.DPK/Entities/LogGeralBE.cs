﻿using System.Collections.Generic;

namespace Entities
{
    /// <summary>
    ///  carlosj CiT 20/09/2012 - LogGeralBE 
    /// </summary>
    public class LogGeralBE
    {
        public List<LogDetalheBE> listLogDetalhe { get; set; }
        public ArquivoBE arquivoBE { get; set; }

        /// <summary>
        /// carlosj CiT 20/09/2012 - construtor
        /// </summary>
        public LogGeralBE()
        {
            this.listLogDetalhe = new List<LogDetalheBE>();
            this.arquivoBE = new ArquivoBE();
        }
    }
}
