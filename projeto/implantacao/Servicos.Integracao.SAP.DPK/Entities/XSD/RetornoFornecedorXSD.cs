﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3634
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=2.0.50727.1432.
// 

namespace Entities.XSD.Fornecedor
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:dpaschoal:com:br:interface:fornecedores")]
    [System.Xml.Serialization.XmlRootAttribute("mtpMM_CadFornec", Namespace = "urn:dpaschoal:com:br:interface:fornecedores", IsNullable = false)]
    public partial class dtpMM_Fornecedor : object, System.ComponentModel.INotifyPropertyChanged
    {

        private string fornecedorField;

        private string empresaField;

        private string orgComprasField;

        private string grpContasField;

        private string nomeField;

        private string termoPesquisaField;

        private string nome3Field;

        private string ruaField;

        private string cxPostalField;

        private string cidadeField;

        private string codPostalField;

        private string bairroField;

        private string paisField;

        private string regiaoField;

        private string idiomaField;

        private string telefoneField;

        private string faxField;

        private string cNPJField;

        private string cPFField;

        private string pessoaFisicaField;

        private string inscrEstadualField;

        private string inscrMunicipalField;

        private string domFiscalField;

        private string setorField;

        private dtpMM_Banco bancoField;

        private string ctaReconciliacaoField;

        private string numAntigoFornecField;

        private string chvCondPgtoField;

        private string moedaField;

        private string chvCondPgto2Field;

        private string tpTransporteField;

        private string tpTransporte2Field;

        private decimal vlrMinPedField;

        private bool vlrMinPedFieldSpecified;

        private string grpCompradoresField;

        private string prazoEntregaField;

        private dtpMM_Caracteristicas[] caracteristicasField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Fornecedor
        {
            get
            {
                return this.fornecedorField;
            }
            set
            {
                this.fornecedorField = value;
                this.RaisePropertyChanged("Fornecedor");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Empresa
        {
            get
            {
                return this.empresaField;
            }
            set
            {
                this.empresaField = value;
                this.RaisePropertyChanged("Empresa");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string OrgCompras
        {
            get
            {
                return this.orgComprasField;
            }
            set
            {
                this.orgComprasField = value;
                this.RaisePropertyChanged("OrgCompras");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string GrpContas
        {
            get
            {
                return this.grpContasField;
            }
            set
            {
                this.grpContasField = value;
                this.RaisePropertyChanged("GrpContas");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
                this.RaisePropertyChanged("Nome");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TermoPesquisa
        {
            get
            {
                return this.termoPesquisaField;
            }
            set
            {
                this.termoPesquisaField = value;
                this.RaisePropertyChanged("TermoPesquisa");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Nome3
        {
            get
            {
                return this.nome3Field;
            }
            set
            {
                this.nome3Field = value;
                this.RaisePropertyChanged("Nome3");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Rua
        {
            get
            {
                return this.ruaField;
            }
            set
            {
                this.ruaField = value;
                this.RaisePropertyChanged("Rua");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CxPostal
        {
            get
            {
                return this.cxPostalField;
            }
            set
            {
                this.cxPostalField = value;
                this.RaisePropertyChanged("CxPostal");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Cidade
        {
            get
            {
                return this.cidadeField;
            }
            set
            {
                this.cidadeField = value;
                this.RaisePropertyChanged("Cidade");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CodPostal
        {
            get
            {
                return this.codPostalField;
            }
            set
            {
                this.codPostalField = value;
                this.RaisePropertyChanged("CodPostal");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Bairro
        {
            get
            {
                return this.bairroField;
            }
            set
            {
                this.bairroField = value;
                this.RaisePropertyChanged("Bairro");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Pais
        {
            get
            {
                return this.paisField;
            }
            set
            {
                this.paisField = value;
                this.RaisePropertyChanged("Pais");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Regiao
        {
            get
            {
                return this.regiaoField;
            }
            set
            {
                this.regiaoField = value;
                this.RaisePropertyChanged("Regiao");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Idioma
        {
            get
            {
                return this.idiomaField;
            }
            set
            {
                this.idiomaField = value;
                this.RaisePropertyChanged("Idioma");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Telefone
        {
            get
            {
                return this.telefoneField;
            }
            set
            {
                this.telefoneField = value;
                this.RaisePropertyChanged("Telefone");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
                this.RaisePropertyChanged("Fax");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CNPJ
        {
            get
            {
                return this.cNPJField;
            }
            set
            {
                this.cNPJField = value;
                this.RaisePropertyChanged("CNPJ");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CPF
        {
            get
            {
                return this.cPFField;
            }
            set
            {
                this.cPFField = value;
                this.RaisePropertyChanged("CPF");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string PessoaFisica
        {
            get
            {
                return this.pessoaFisicaField;
            }
            set
            {
                this.pessoaFisicaField = value;
                this.RaisePropertyChanged("PessoaFisica");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string InscrEstadual
        {
            get
            {
                return this.inscrEstadualField;
            }
            set
            {
                this.inscrEstadualField = value;
                this.RaisePropertyChanged("InscrEstadual");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string InscrMunicipal
        {
            get
            {
                return this.inscrMunicipalField;
            }
            set
            {
                this.inscrMunicipalField = value;
                this.RaisePropertyChanged("InscrMunicipal");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string DomFiscal
        {
            get
            {
                return this.domFiscalField;
            }
            set
            {
                this.domFiscalField = value;
                this.RaisePropertyChanged("DomFiscal");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Setor
        {
            get
            {
                return this.setorField;
            }
            set
            {
                this.setorField = value;
                this.RaisePropertyChanged("Setor");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public dtpMM_Banco Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
                this.RaisePropertyChanged("Banco");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CtaReconciliacao
        {
            get
            {
                return this.ctaReconciliacaoField;
            }
            set
            {
                this.ctaReconciliacaoField = value;
                this.RaisePropertyChanged("CtaReconciliacao");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string NumAntigoFornec
        {
            get
            {
                return this.numAntigoFornecField;
            }
            set
            {
                this.numAntigoFornecField = value;
                this.RaisePropertyChanged("NumAntigoFornec");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ChvCondPgto
        {
            get
            {
                return this.chvCondPgtoField;
            }
            set
            {
                this.chvCondPgtoField = value;
                this.RaisePropertyChanged("ChvCondPgto");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Moeda
        {
            get
            {
                return this.moedaField;
            }
            set
            {
                this.moedaField = value;
                this.RaisePropertyChanged("Moeda");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ChvCondPgto2
        {
            get
            {
                return this.chvCondPgto2Field;
            }
            set
            {
                this.chvCondPgto2Field = value;
                this.RaisePropertyChanged("ChvCondPgto2");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TpTransporte
        {
            get
            {
                return this.tpTransporteField;
            }
            set
            {
                this.tpTransporteField = value;
                this.RaisePropertyChanged("TpTransporte");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string TpTransporte2
        {
            get
            {
                return this.tpTransporte2Field;
            }
            set
            {
                this.tpTransporte2Field = value;
                this.RaisePropertyChanged("TpTransporte2");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public decimal VlrMinPed
        {
            get
            {
                return this.vlrMinPedField;
            }
            set
            {
                this.vlrMinPedField = value;
                this.RaisePropertyChanged("VlrMinPed");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VlrMinPedSpecified
        {
            get
            {
                return this.vlrMinPedFieldSpecified;
            }
            set
            {
                this.vlrMinPedFieldSpecified = value;
                this.RaisePropertyChanged("VlrMinPedSpecified");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string GrpCompradores
        {
            get
            {
                return this.grpCompradoresField;
            }
            set
            {
                this.grpCompradoresField = value;
                this.RaisePropertyChanged("GrpCompradores");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string PrazoEntrega
        {
            get
            {
                return this.prazoEntregaField;
            }
            set
            {
                this.prazoEntregaField = value;
                this.RaisePropertyChanged("PrazoEntrega");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public dtpMM_Caracteristicas[] Caracteristicas
        {
            get
            {
                return this.caracteristicasField;
            }
            set
            {
                this.caracteristicasField = value;
                this.RaisePropertyChanged("Caracteristicas");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:dpaschoal:com:br:interface:fornecedores")]
    public partial class dtpMM_Banco : object, System.ComponentModel.INotifyPropertyChanged
    {

        private string codBancoField;

        private string contaCorrenteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CodBanco
        {
            get
            {
                return this.codBancoField;
            }
            set
            {
                this.codBancoField = value;
                this.RaisePropertyChanged("CodBanco");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string ContaCorrente
        {
            get
            {
                return this.contaCorrenteField;
            }
            set
            {
                this.contaCorrenteField = value;
                this.RaisePropertyChanged("ContaCorrente");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "urn:dpaschoal:com:br:interface:fornecedores")]
    public partial class dtpMM_Caracteristicas : object, System.ComponentModel.INotifyPropertyChanged
    {

        private string fornecedorField;

        private string nomeField;

        private string valorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Fornecedor
        {
            get
            {
                return this.fornecedorField;
            }
            set
            {
                this.fornecedorField = value;
                this.RaisePropertyChanged("Fornecedor");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
                this.RaisePropertyChanged("Nome");
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
                this.RaisePropertyChanged("Valor");
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}