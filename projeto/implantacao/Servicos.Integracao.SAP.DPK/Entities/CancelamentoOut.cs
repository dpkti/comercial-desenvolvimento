﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class CancelamentoOut
    {
        public List<string> messages;
        public bool canceladoComSucesso;

        public CancelamentoOut()
        {
            this.messages = new List<string>();
            canceladoComSucesso = false;
        }


    }
}
