﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ItemBE
    {
        public Int16 NUM_ITEM_PEDIDO { get; set; }   //ITPEDNOTA_VENDA
        public Int64 COD_DPK { get; set; }           //ITPEDNOTA_VENDA
        public Int64 QTD_ATENDIDA { get; set; }      //ITPEDNOTA_VENDA
        public Decimal PRECO_UNITARIO { get; set; }  //ITPEDNOTA_VENDA
        public Decimal PC_DESC1 { get; set; }        //ITPEDNOTA_VENDA
        public Decimal PC_DESC2 { get; set; }        //ITPEDNOTA_VENDA
        public Decimal PC_DESC3 { get; set; }        //ITPEDNOTA_VENDA
        public Decimal PC_DIF_ICM { get; set; }      //ITPEDNOTA_VENDA
        public Decimal PC_COMISS { get; set; }       //ITPEDNOTA_VENDA
        public Decimal PC_COMISS_TLMK { get; set; }  //ITPEDNOTA_VENDA
        public Decimal VALOR_LIQUIDO { get; set; }   //Valor Liquido do item
    }
}
