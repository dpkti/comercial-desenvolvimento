﻿
using System.Collections.Generic;

namespace Entities
{
    public class UsuarioBE
    {
        public List<CentroDistribuicaoBE> listLojas { get; set; }

        public string NOME_USUARIO { get; set; }
        public string SENHA { get; set; }
        public string LOGIN { get; set; }
        public bool PERMISSAO_ACESSO { get; set; }
        public long COD_USUARIO { get; set; }
    }
}
