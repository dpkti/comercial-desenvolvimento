﻿namespace Entities
{
    public static class Parametros
    {
        // Conexões.
        public const string CONN_ORA_DPK = "ORA_DPK_CONN";

        // Procedures - PCK_INT_SAP
        public const string PR_SEL_CLIENTE_DPK = "SAP.PCK_INT_SAP.PR_SEL_CLIENTE_DPK";
        public const string PR_UPD_CLIENTE = "SAP.PCK_INT_SAP.PR_UPD_CLIENTE";
        public const string PR_UPD_CLIENTE_ENDERECO = "SAP.PCK_INT_SAP.PR_UPD_CLIENTE_ENDERECO";
        public const string PR_SEL_FORNECEDOR_DPK = "SAP.PCK_INT_SAP.PR_SEL_FORNECEDOR_DPK";
        public const string PR_INS_FORNECEDOR = "SAP.PCK_INT_SAP.PR_INS_FORNECEDOR";
        public const string PR_UPD_FORNECEDOR = "SAP.PCK_INT_SAP.PR_UPD_FORNECEDOR";
        public const string PR_INS_LOG = "SAP.PCK_INT_SAP.PR_INS_LOG_SINC_LEGADO";
        public const string PR_INS_ARQUIVO = "SAP.PCK_INT_SAP.PR_INS_LOG_ARQUIVO";
        public const string PR_INS_TRANSPORTADORA = "SAP.PCK_INT_SAP.PR_INS_TRANSPORTADORA";
        public const string PR_UPD_TRANSPORTADORA = "SAP.PCK_INT_SAP.PR_UPD_TRANSPORTADORA";
        public const string PR_SEL_TRANSPORTADORA_DPK = "SAP.PCK_INT_SAP.PR_SEL_TRANSPORTADORA_DPK";
        public const string PR_SEL_LINHA = "SAP.PCK_INT_SAP.PR_SEL_LINHA";
        public const string PR_SEL_MATERIAL_DPK = "SAP.PCK_INT_SAP.PR_SEL_MATERIAL_DPK";
        public const string PR_SEL_GRUPO_SUBGRUPO = "SAP.PCK_INT_SAP.PR_SEL_GRUPO_SUBGRUPO";
        public const string PR_SEL_UNIDADE = "SAP.PCK_INT_SAP.PR_SEL_UNIDADE";
        public const string PR_SEL_R_ITEM_EAN = "SAP.PCK_INT_SAP.PR_SEL_R_ITEM_EAN";
        public const string PR_INS_MATERIAL = "SAP.PCK_INT_SAP.PR_INS_MATERIAL_DPK";
        public const string PR_UPD_MATERIAL = "SAP.PCK_INT_SAP.PR_UPD_MATERIAL";
        public const string PR_INS_UPD_MATERIAL_DPK_DEPXX = "SAP.PCK_INT_SAP.PR_INS_UPD_MATERIAL_DPK_DEPXX";
        public const string PR_SEL_LOJA_CONEXAO = "SAP.PCK_INT_SAP.PR_SEL_LOJA_CONEXAO";
        public const string PR_SEL_FORNEC_FABRICA = "SAP.PCK_INT_SAP.PR_SEL_FORNEC_FABRICA";

        // Procedures - PCK_INT_SAP_PEDIDO
        public const string PR_LOG_CONTROLE_PEDIDO_SAP = "SAP.PCK_INT_SAP_PEDIDO.PR_LOG_CONTROLE_PEDIDO_SAP";
        public const string PR_SEL_PEDIDOS_PENDENTES = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PEDIDOS_PENDENTES";
        public const string PR_INS_CONTROLE_PEDIDO_SAP = "SAP.PCK_INT_SAP_PEDIDO.PR_INS_CONTROLE_PEDIDO_SAP";
        public const string PR_UPD_CONTROLE_PEDIDO_SAP = "SAP.PCK_INT_SAP_PEDIDO.PR_UPD_CONTROLE_PEDIDO_SAP";
        public const string PR_SEL_PEDIDOS_LIBERADOS = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PEDIDOS_LIBERADOS";
        public const string PR_SEL_TAXA_JUROS = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_TAXA_JUROS";
        public const string PR_SEL_COD_REGIONAL = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_COD_REGIONAL";
        public const string PR_SEL_FL_REDESPACHO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_FL_REDESPACHO";
        public const string PR_SEL_COD_TRANSPORTADORA = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_COD_TRANSPORTADORA";
        public const string PR_SEL_ITENS_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_ITENS_PEDIDO";
        public const string PR_SEL_PARAM_SISTEMA = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PARAM_SISTEMA";
        public const string PR_SEL_USUARIO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_USUARIO";
        public const string PR_SEL_CONTROLE_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_CONTROLE_PEDIDO";
        public const string PR_SEL_HIST_CONTROLE_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_HIST_CONTROLE_PEDIDO";
        public const string PR_UPD_STATUS_CONTROLE_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_UPD_STATUS_CONTROLE_PEDIDO";
        public const string PR_SEL_DEPOSITO_VISAO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_DEPOSITO_VISAO";
        public const string PR_SEL_PERMISSAO_CANCELAMENTO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PERMISSAO_CANCELAMENTO";
        public const string PR_SEL_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PEDIDO";
        public const string PR_SEL_NOME_EXIBICAO_LOJA = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_NOME_EXIBICAO_LOJA";
        public const string PR_SEL_DESCRICAO_CANCEL = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_DESCRICAO_CANCEL";
        public const string PR_UPD_CONTROLE_PEDIDO_STATUS = "SAP.PCK_INT_SAP_PEDIDO.PR_UPD_CONTROLE_PEDIDO_STATUS";
        public const string PR_UPD_PEDNOTA_VENDA = "SAP.PCK_INT_SAP_PEDIDO.PR_UPD_PEDNOTA_VENDA";
        public const string PR_EFETIVAR_PEDIDO = "SAP.PCK_INT_SAP_PEDIDO.PR_EFETIVAR_PEDIDO";
        public const string PR_SEL_PEDIDOS_EXPURGADOS = "SAP.PCK_INT_SAP_PEDIDO.PR_SEL_PEDIDOS_EXPURGADOS";
        public const string PR_UPD_ITPEDNOTA_VENDA = "SAP.PCK_INT_SAP_PEDIDO.PR_UPD_ITPEDNOTA_VENDA";
        public const string PR_INS_FISCAL_SAIDA = "SAP.PCK_INT_SAP_PEDIDO.PR_INS_FISCAL_SAIDA";


        // Procedures com owners diferentes para cada CD
        public const string PR_CANCELA = "PR_CANCELA";

        // Constantes globais.
        public const string NOME_APLICACAO = "nomeAplicacaoEventViewer";
        public const string INTERVALO_EXECUCAO_SEG = "IntervaloExecucaoSegundo";

        public const string INTERVALO_EXECUCAO_COMUNICAR_PEDIDO_SEG = "IntervaloExecucaoComunicarPedidoSegundo";
        public const string INTERVALO_EXECUCAO_PROCESSAR_NFESTATUS_SEG = "IntervaloExecucaoProcessarNfeSegundo";
        public const string INTERVALO_EXECUCAO_EXPURGO_SEG = "IntervaloExecucaoExpurgoSegundo";

        public const string SEPARADOR_TELEFONE = "SeparadorTelefone";
        public const string SUFIXO_NOMENCLATURA_ARQUIVO = "SufixoNomenclaturaArquivo";
        public const string ERROS_GERENCIAVEIS_CONEXAO = "erros_gerenciaveis_conexao";

        // Constantes do Serviço de Pedidos
        public const string CAMINHO_COMPLETO_ARQUIVO_CONFIGURACAO_CD = "caminhoCompletoArquivoConfiguracaoCD";

        // Constantes da classe ClienteSAP.     
        public const string DIR_ARQUIVOS_INVALIDOS_CLIENTE = "DiretorioArquivosInvalidosCliente";
        public const string DIR_ARQUIVOS_CLIENTE = "DiretorioArquivosCliente";
        public const string DIR_ARQUIVOS_CLIENTE_PROCESSADOS = "DiretorioArquivosClienteProcessados";
        public const string GRAVAR_ARQUIVOS_CLIENTE_PROCESSADOS = "GravarArquivosClienteProcessados";
        public const string PREFIXO_ARQUIVO_CLIENTE = "PrefixoArquivoCliente";

        // Constantes da classe ProdutoSAP.
        public const string DIR_ARQUIVOS_INVALIDOS_MATERIAL = "DiretorioArquivosInvalidosMaterial";
        public const string DIR_ARQUIVOS_MATERIAL = "DiretorioArquivosMaterial";
        public const string DIR_ARQUIVOS_MATERIAL_PROCESSADOS = "DiretorioArquivosMaterialProcessados";
        public const string GRAVAR_ARQUIVOS_MATERIAL_PROCESSADOS = "GravarArquivosMaterialProcessados";
        public const string PREFIXO_ARQUIVO_MATERIAL = "PrefixoArquivoMaterial";

        // Constantes da classe FornecedorSAP.
        public const string DIR_ARQUIVOS_INVALIDOS_FORNECEDOR = "DiretorioArquivosInvalidosFornecedor";
        public const string DIR_ARQUIVOS_FORNECEDOR = "DiretorioArquivosFornecedor";
        public const string DIR_ARQUIVOS_FORNECEDOR_PROCESSADOS = "DiretorioArquivosFornecedorProcessados";
        public const string GRAVAR_ARQUIVOS_FORNECEDOR_PROCESSADOS = "GravarArquivosFornecedorProcessados";
        public const string PREFIXO_ARQUIVO_FORNECEDOR = "PrefixoArquivoFornecedor";

        // Constantes da classe NfeStatusSAP.
        public const string DIR_ARQUIVOS_INVALIDOS_NFESTATUS = "DiretorioArquivosInvalidosNfeStatus";
        public const string DIR_ARQUIVOS_NFESTATUS = "DiretorioArquivosNfeStatus";
        public const string DIR_ARQUIVOS_NFESTATUS_PROCESSADOS = "DiretorioArquivosNfeStatusProcessados";
        public const string GRAVAR_ARQUIVOS_NFESTATUS_PROCESSADOS = "GravarArquivosNfeStatusProcessados";
        public const string PREFIXO_ARQUIVO_NFESTATUS = "PrefixoArquivoNfeStatus";

        // Caracteristicas da classe FornecedorSAP.
        public const string CARACTERISTICA_FORNECEDOR_BAIXAR_PEDIDO = "BAIXAR_PEDIDO";
        public const string CARACTERISTICA_FORNECEDOR_DPK = "DPK";
        public const string CARACTERISTICA_FORNECEDOR_DESCONTO_DIFERENCA_ICM = "DESCONTO_DIFERENCA_ICM";
        public const string CARACTERISTICA_FORNECEDOR_PERMITIR_DESCONTO_ADICIONAL = "PERMITIR_DESCONTO_ADICIONAL";
        public const string CARACTERISTICA_FORNECEDOR_SIGLA = "SIGLA";
        public const string CARACTERISTICA_FORNECEDOR_EDI = "EDI";
        public const string CARACTERISTICA_FORNECEDOR_DIVISAO = "DIVISAO";
        public const string CARACTERISTICA_FORNECEDOR_SITUACAO = "SITUACAO";

        // Caracteristicas da classe MaterialSAP.
        public const string CARACTERISTICA_MATERIAL_NOVO_ITEM_SUBSTITUIDO = "NOVO_ITEM_SUBSTITUIDO";
        public const string CARACTERISTICA_MATERIAL_COD_LINHA = "COD_LINHA";
        public const string CARACTERISTICA_MATERIAL_CODIGO_MASCARADO = "CODIGO_MASCARADO";
        public const string CARACTERISTICA_MATERIAL_COD_TRIB_IPI = "COD_TRIB_IPI";
        public const string CARACTERISTICA_MATERIAL_COD_TRIB = "COD_TRIB";
        public const string CARACTERISTICA_MATERIAL_DPK = "DPK";
        public const string CARACTERISTICA_MATERIAL_PC_IPI = "PC_IPI";
    }
}
