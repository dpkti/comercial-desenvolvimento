﻿
using System;
namespace Entities
{
    public class HistoricoPedidoDH
    {
        public string ID_CONTROLE_PEDIDO { get; set; }
        public string NUMERO_PEDIDO { get; set; }
        public string DESCRICAO { get; set; }
        public DateTime DATA_LOG { get; set; }
    }
}
