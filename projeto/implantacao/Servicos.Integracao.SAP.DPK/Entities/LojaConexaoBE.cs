﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    /// <summary>
    ///  carlosj CiT 26/09/2012 - LojaConexaoBE 
    ///  representa a tabela 'PRODUCAO.LOJA_CONEXAO' 
    /// </summary>
    public class LojaConexaoBE
    {
        public string IP { get; set;}
        public Int32 COD_LOJA { get; set; }
        public Int32 TIME_OUT { get; set; }
        public bool CONECTADO { get; set; }

        /// <summary>
        /// carlosj CiT 26/09/2012 - construtor
        /// </summary>
        public LojaConexaoBE() 
        {
            IP = string.Empty;
            COD_LOJA = 0;
            TIME_OUT = 0;
            CONECTADO = false;
        } 
    }
}
