﻿
using System.Collections.Generic;
namespace Entities
{
    public class CentroDistribuicaoBE
    {
        public int COD_LOJA { get; set; }

        public string COD_LOJA_EXIBICAO { get; set; }

        public Entities.EnumeradoresAplicacao.TipoBanco TipoBanco { get; set; }

        public List<Entities.ParametroSistemaBE> ListParametrosSistema { get; set; }

        // Construtor.
        public CentroDistribuicaoBE()
        {
            TipoBanco = EnumeradoresAplicacao.TipoBanco.Nenhum;
            COD_LOJA = 0;
            COD_LOJA_EXIBICAO = string.Empty;
        }
    }
}
