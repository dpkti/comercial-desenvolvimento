﻿
namespace Entities
{
    public static class EnumeradoresAplicacao
    {
        public enum GrupoContaCliente
        {
            ZREP,
            ZCPF,
            ZCPJ

        }

        public enum GrupoContaFornecedor
        {
            ZFIS,
            ZJUR,
            ZTPJ,
            ZTPF
        }


        /// <summary>
        /// carlosj CiT (20/09/2012)
        /// </summary>
        public enum TipoEntidade
        {
            Indefinido = 0,
            ClienteBE = 1,
            FornecedorBE = 2,
            TransportadoraBE = 3,
            MaterialBE = 4,
            PedidoBE = 5,
            NfeStatusBE = 6,
            ExpurgoBE = 7,
            CancelamentoBE = 8,
        }

        /// <summary>
        /// carlosj CiT (20/09/2012)
        /// </summary>
        public enum CodigoErro
        {
            Sucesso = 0,
            CampoObrigatorioEmBranco = 1,
            CampoObrigatorioFormatoIncorreto = 2,
            ErroBanco = 3,
            FormatoIncorretoInscrição = 4,
            ValorForaDominio = 5,
            CampoFormatoIncorreto = 6,
            DadoInconsistente = 7,
            FalhaComunicacaoOutrosCDs = 8,
            LeituraDeArquivo = 9,
            RetornoComunicacaoSAP = 10,
            ErroComunicacaoPedidoSAP = 11,
            ErroDeSistema = 12,
            ErroSapNFE = 13
        }

        /// <summary>
        /// carlosj CiT (20/09/2012)
        /// </summary>
        public enum Status
        {
            Sucesso = 1,
            Erro = 2,
            Aviso = 3
        }

        public enum AcaoArquivo
        {
            SemArquivo,
            MoverProcessados,
            MoverInvalidos,
            ManterPasta,
            DeletarArquivo,
        }

        public enum TipoBanco
        {
            Nenhum,
            Multiplo = 'M',
            Unico = 'U'
        }

        public enum StatusPedido
        {
            Pendente = 'P',
            Aguardando = 'W',
            Erro_SAP = 'E',
            Sucesso = 'S',
            Cancelado = 'C',
            Faturado = 'F',
            Cancelamento_Negado = 'N',
            Expurgado = 'X',
        }

        public enum StatusNfeSAP
        {
            Aprovado = 1,
            Recusado = 2,
            Rejeitado = 3,
        }
    }
}
