﻿
using System;
using System.Collections.Generic;

namespace Entities
{
    public class PedidoBE
    {
        private string _status;

        public PedidoBE()
        {
            DT_ENVIO = DateTime.MinValue;
        }

        public string ID_CONTROLE_PEDIDO { get; set; }       //Tabela Controle_Pedido_Sap
        public int COD_LOJA { get; set; }                    //Tabela Controle_Pedido_Sap
        public Int64 NUM_PEDIDO { get; set; }                //Tabela Controle_Pedido_Sap
        public int SEQ_PEDIDO { get; set; }                  //Tabela Controle_Pedido_Sap
        public DateTime DT_ENVIO { get; set; }               //Tabela Controle_Pedido_Sap
        public string NR_REMESSA_SAP { get; set; }           //Tabela Controle_Pedido_Sap
        public string NR_FATURA_SAP { get; set; }            //Tabela Controle_Pedido_Sap
        public string NR_ORDEM_SAP { get; set; }             //Tabela Controle_Pedido_Sap
        public string STATUS
        {
            get { return _status; }
            set
            {
                if (value.Length == 1)
                    this._status = value;
                else
                {
                    switch ((EnumeradoresAplicacao.StatusPedido)Enum.Parse(typeof(EnumeradoresAplicacao.StatusPedido), value))
                    //((string)value)
                    {
                        case (EnumeradoresAplicacao.StatusPedido.Aguardando): this._status = "W";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Cancelado): this._status = "C";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Cancelamento_Negado): this._status = "N";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Erro_SAP): this._status = "E";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Expurgado): this._status = "X";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Faturado): this._status = "F";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Pendente): this._status = "P";
                            break;
                        case (EnumeradoresAplicacao.StatusPedido.Sucesso): this._status = "S";
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        //Tabela Controle_Pedido_Sap
        public string TEM_DIF_ESTOQUE { get; set; }          //Tabela Controle_Pedido_Sap
        public Int32 COD_FILIAL { get; set; }                //Tabela PEDNOTA_VENDA
        public string NATUREZA_OP_SAP { get; set; }          //Tabela NATUREZA_OPER_SAP
        public Int64 COD_SAP_CLIENTE { get; set; }           //Tabela Cliente
        public Int64 COD_SAP_LOJA { get; set; }              //Tabela Loja
        public string FLAG_RETIRA { get; set; }              //Tabela Transportadora
        public Int32 COD_BANCO { get; set; }                 //Tabela PEDNOTA_VENDA
        public string TP_DOC_SAP { get; set; }               //Tabela NATUREZA_OPER_SAP
        public int COD_VDR { get; set; }                     //Tabela PEDNOTA_VENDA
        public string FL_CONS_FINAL { get; set; }            //Tabela Cliente
        public Int32 COD_TRANSPORTADORA { get; set; }        //Tabela Tansportadora
        public Decimal PC_DESC_SUFRAMA { get; set; }         //Tabela PEDNOTA_VENDA
        public Decimal PC_DESCONTO { get; set; }             //Tabela PEDNOTA_VENDA
        public Decimal PC_ACRESCIMO { get; set; }            //Tabela PEDNOTA_VENDA
        public Decimal PC_SEGURO { get; set; }               //Tabela PEDNOTA_VENDA
        public Decimal VL_FRETE { get; set; }                //Tabela PEDNOTA_VENDA
        public Decimal VL_DESP_ACESS { get; set; }           //Tabela PEDNOTA_VENDA
        public string MENS_PEDIDO { get; set; }              //Tabela PEDNOTA_VENDA
        public string MENS_NOTA { get; set; }                //Tabela PEDNOTA_VENDA
        public Int32 COD_PLANO { get; set; }                 //Tabela PEDNOTA_VENDA
        public string FRETE_PAGO { get; set; }               //Tabela PEDNOTA_VENDA
        public List<ItemBE> ListaItensPedido;                //Lista de Itens do pedido 
        public string MTV_ORDEM { get; set; }                //Tabela NATUREZA_OPER_SAP
    }
}
