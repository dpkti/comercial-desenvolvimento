﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;
using System.Timers;
using Business;
using CommonServico;
using Entities;
using UtilGeralDPA;

namespace ServicoSincronizarLegado
{
    [RunInstaller(true)]
    public partial class SincronizarLegado : ServiceBase
    {
        public SincronizarLegado()
        {
            InitializeComponent();
        }

#if DEBUG
        public void StartDebug(string[] args)
        {
            OnStart(args);
        }
#endif

        private TimerCustom timer = null;
        private bool servicoExecutando = false;


        protected override void OnStart(string[] args)
        {
            int segundosExecucao;
            String segundosParametro;

            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                // Busca o intervalo, em segundos, no qual o serviço será executado
                segundosParametro = ConfigurationAccess.ObterConfiguracao(Parametros.INTERVALO_EXECUCAO_SEG);

                //cria os diretorios da aplicacao
                if (!ServicoBO.CriarDiretoriosPadroes())
                {
                    this.Stop();
                    return;
                }

                if (!int.TryParse(segundosParametro, out segundosExecucao))
                {
                    // se falhar o try parse, valor default para iniciar o serv
                    segundosExecucao = 2;
                }

                Logger.LogInfo(String.Format("O intervalo de execução está parametrizado para {0} segundo(s).", segundosExecucao.ToString()));

                segundosExecucao = segundosExecucao * 1000;

                timer = new TimerCustom(segundosExecucao);
                timer.Elapsed += Timer_Tick;

                iniciarServico();

                Logger.LogInfo("Serviço - SincronizarLegado - iniciado com sucesso!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        private void Timer_Tick(object sender, ElapsedEventArgs e)
        {
            // Setando cultura da aplicação
            CultureInfo ci = new CultureInfo("EN-US");
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            DateTime inicio = Logger.LogInicioMetodo();

            try
            {
                if (!servicoExecutando)
                {
                    pararServico();

                    ServicoBO.IniciaProcessoSincronismo();

                    iniciarServico();
                }
                else
                {
                    Logger.LogInfo("O serviço - SincronizarLegado - já está em execução.");
                }
            }
            catch (Exception ex)
            {
                if (servicoExecutando)
                {
                    iniciarServico();
                }

                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(inicio);
            }
        }

        /// <summary>
        /// Inicia o timer do serviço e atualiza a variável de controle
        /// </summary>
        private void pararServico()
        {
            servicoExecutando = true;
            timer.Stop();
        }

        /// <summary>
        /// Para o timer do serviõ e atualiza a variável de control
        /// </summary>
        private void iniciarServico()
        {
            servicoExecutando = false;
            timer.Start();
        }


        protected override void OnStop()
        {
        }
    }
}
