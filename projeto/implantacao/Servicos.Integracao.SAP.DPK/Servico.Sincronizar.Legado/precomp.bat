:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    if exist "%temp%\getadmin.vbs" ( del "%temp%\getadmin.vbs" )
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------

SET PATH=%PATH%;C:\Arquivos de programas\Microsoft Visual Studio 8\SDK\v2.0\Bin;C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727;C:\Arquivos de programas\Microsoft Visual Studio 8\VC\bin;C:\Arquivos de programas\Microsoft Visual Studio 8\Common7\IDE;C:\Arquivos de programas\Microsoft Visual Studio 8\VC\vcpackages;

:PARAR_RETORNO
NET STOP "DPK - Sincronismo Legado"
if ERRORLEVEL 1 goto DESINSTALA

:DESINSTALA
installutil.exe /u ServicoSincronizarLegado.exe
if ERRORLEVEL 1 goto EXIT

:EXIT
SET ERRORLEVEL = 0