﻿using System.ServiceProcess;

namespace ServicoSincronizarLegado
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
#if DEBUG
                SincronizarLegado servicoSincronizar = new SincronizarLegado();
                servicoSincronizar.StartDebug(new string[1]);

                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
                ServiceBase[] servicesToRun;
                servicesToRun = new ServiceBase[] { new SincronizarLegado() };
                ServiceBase.Run(servicesToRun);
#endif
            }
            else   // codigo original
            {

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                                    {
                                        new SincronizarLegado(),
                                    };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
