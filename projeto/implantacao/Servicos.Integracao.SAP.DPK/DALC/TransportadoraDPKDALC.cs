﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Entities;
using Entities.XSD.Fornecedor;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;

namespace DALC
{
    /// <summary>
    /// Classe responsável por fazer o acesso a banco de dados para classe transportadora.
    /// </summary>
    public class TransportadoraDPKDALC
    {
        #region Singleton
        private static TransportadoraDPKDALC instance;
        public static TransportadoraDPKDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TransportadoraDPKDALC();
                }
                return instance;
            }
        }
        #endregion


        /// <summary>
        /// carlosj CiT (24/09/2012) - verifica se a Transportadora já está cadastrada
        /// </summary>
        /// <param name="cod_sap"></param>
        /// <returns></returns>
        public bool VerificarExistenciaTransportadoraDPK(string cod_sap)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_TRANSPORTADORA_DPK))
                {
                    db.AddInParameter(cmd, "p_cod_sap", DbType.String, cod_sap);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        /// <summary>
        /// carlosj CiT (24/09/2012) - Insere dados da Transportadora
        /// </summary>
        /// <param name="transportadora"></param>
        public void InserirDadosTransportadora(dtpMM_Fornecedor transportadora)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_TRANSPORTADORA))
                {

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    //Tipo transporte
                    db.AddInParameter(cmd, "P_TP_FRETE", DbType.String, transportadora.TpTransporte);

                    //adiciona os parametros no cmd
                    AdicionarParametrosComuns(db, cmd, transportadora);

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Erro ao inserir Transportadora.");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                    Logger.LogInfo(string.Format("A Transportadora {0} foi inserida com sucesso", transportadora.Nome));
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        ///  carlosj CiT (24/09/2012) - Altera os dados da Transportadora
        /// </summary>
        /// <param name="transportadora"></param>
        public void AtualizarDadosTransportadora(dtpMM_Fornecedor transportadora)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_TRANSPORTADORA))
                {

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    //adiciona os parametros no cmd
                    AdicionarParametrosComuns(db, cmd, transportadora);

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Erro ao inserir Transportadora.");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                    Logger.LogInfo(string.Format("A Transportadora {0} foi atualizada com sucesso", transportadora.Nome));
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
        }


        /// <summary>
        /// carlosj CiT (24/09/2012) - Adiciona os parametros comuns
        /// </summary>
        /// <param name="db"></param>
        /// <param name="cmd"></param>
        /// <param name="transportadora"></param>
        private void AdicionarParametrosComuns(Database db, DbCommand cmd, dtpMM_Fornecedor transportadora)
        {
            //parametros de saida
            db.AddOutParameter(cmd, "P_CODERRO", DbType.Int64, 8);
            db.AddOutParameter(cmd, "P_TXTERRO", DbType.String, 100);



            //Nome transportadora
            db.AddInParameter(cmd, "P_NOME_TRANSP", DbType.String, transportadora.Nome);

            //Sigla
            db.AddInParameter(cmd, "P_SIGLA", DbType.String, transportadora.TermoPesquisa);

            //Endereco
            db.AddInParameter(cmd, "P_ENDERECO", DbType.String, transportadora.Rua);

            //CEP
            db.AddInParameter(cmd, "P_CEP", DbType.String, transportadora.CodPostal);

            //Bairro
            db.AddInParameter(cmd, "P_BAIRRO", DbType.String, transportadora.Bairro);

            string[] dddFone = transportadora.Telefone.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None);

            if (dddFone.LongCount() > 1)
            {
                //DDD
                db.AddInParameter(cmd, "P_DDD", DbType.String, dddFone[0]);
                //FONE
                db.AddInParameter(cmd, "P_FONE", DbType.String, dddFone[1]);
            }
            else
            {
                //DDD
                db.AddInParameter(cmd, "P_DDD", DbType.String, null);
                //FONE
                db.AddInParameter(cmd, "P_FONE", DbType.String, null);
            }

            //FAX
            db.AddInParameter(cmd, "P_FAX", DbType.String, transportadora.Fax);

            //CGC
            if (!string.IsNullOrEmpty(transportadora.CNPJ.Trim()))
            {
                db.AddInParameter(cmd, "P_TP_DOCTO", DbType.Int16, 1);
                db.AddInParameter(cmd, "P_CGC", DbType.Int64, Convert.ToUInt64(transportadora.CNPJ));
            }
            else
            {
                db.AddInParameter(cmd, "P_TP_DOCTO", DbType.Int16, 2);
                db.AddInParameter(cmd, "P_CGC", DbType.Int64, Convert.ToUInt64(transportadora.CPF));
            }

            //DomFiscal
            db.AddInParameter(cmd, "P_DOM_FISCAL", DbType.String, transportadora.DomFiscal);

            //Tipo transporte
            db.AddInParameter(cmd, "P_COD_SAP", DbType.String, transportadora.Fornecedor);

        }

    }
}
