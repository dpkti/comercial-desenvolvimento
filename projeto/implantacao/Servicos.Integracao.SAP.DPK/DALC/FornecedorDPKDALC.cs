﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;
using Entities.XSD.Fornecedor;

namespace DALC
{
    /// <summary>
    /// Classe responsável por fazer o acesso a banco de dados para classe fornecedores.
    /// </summary>
    public class FornecedorDPKDALC
    {

        #region Singleton
        private static FornecedorDPKDALC instance;
        public static FornecedorDPKDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new FornecedorDPKDALC();
                }
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Método responsável por verificar se o fornecedor já é cadastrado no banco.
        /// </summary>
        /// <param name="cod_sap">Código SAP a ser verificado.</param>
        /// <returns>Indicador se fornecedor existe.</returns>
        /// <remarks>
        ///  carlosj - CiT (12/09/2012)
        /// </remarks>
        public bool VerificarExistenciaFornecedorDPK(string cod_sap)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_FORNECEDOR_DPK))
                {
                    db.AddInParameter(cmd, "p_cod_sap", DbType.String, cod_sap);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        /// <summary>
        /// Método responsável por atualizar um fornecedor.
        /// </summary>
        /// <param name="entity">Objeto fornecedor preenchido.</param>
        /// <remarks>carlosj - CiT (12/09/2012)</remarks>
        public void AtualizarDadosFornecedor(dtpMM_Fornecedor entity)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_FORNECEDOR))
                {
                    LoadParametros(db, cmd, entity);

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Fornecedor não encontrado para atualização.");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                }

                Logger.LogInfo("Fornecedor Atualizado COD SAP " + entity.Fornecedor + ".");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        /// Método responsável por inserir um fornecedor.
        /// </summary>
        /// <param name="entity">Objeto fornecedor preenchido.</param>
        /// <remarks>carlosj - CiT (12/09/2012)</remarks>
        public void InserirDadosFornecedor(dtpMM_Fornecedor entity)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_FORNECEDOR))
                {

                    LoadParametros(db, cmd, entity);

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Não foi possivel Inserir fornecedor.");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                }

                Logger.LogInfo("Fornecedor Inserido COD SAP " + entity.Fornecedor + ".");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        /// Método responsável por obter um valor de caracteristica de um fornecedor pelo seu nome.
        /// </summary>
        /// <param name="nomeCaracteristica">Nome da característica.</param>
        /// <param name="fornecedor">Objeto fornecedor.</param>
        /// <returns>Valor em texto da característica.</returns>
        /// <remarks>
        /// Criado por: Fabio Pallini - Ci&T
        /// Criado em: 17/09/12
        /// </remarks>
        public static string GetValorCaracteristica(string nomeCaracteristica, dtpMM_Fornecedor fornecedor)
        {
            string retorno = string.Empty;

            if (fornecedor.Caracteristicas != null)
            {
                retorno = (from c in fornecedor.Caracteristicas
                           where c != null &&
                                 c.Fornecedor != null &&
                                 c.Nome != null &&
                                 c.Valor != null &&
                                 c.Nome.Equals(nomeCaracteristica) &&
                                 c.Fornecedor.Equals(fornecedor.Fornecedor)
                           select c.Valor.Trim()).FirstOrDefault();
                if (retorno == null)
                {
                    retorno = string.Empty;
                }

            }
            return retorno;
        }

        /// <summary>
        /// Método responsável por retornar número de telefone e DDD.
        /// Caracter de separação definido no App.Config
        /// </summary>
        /// <param name="telefoneCompleto">Telefone completo para ser separado.</param>
        /// <param name="ddd">DDD separado.</param>
        /// <param name="telefone">Teledone separado.</param>
        /// <remarks>
        /// Criado por: Fabio Pallini - Ci&T
        /// Criado em: 17/09/12
        /// </remarks>
        private static void RetornarTelefoneSeparado(string telefoneCompleto, ref string ddd, ref string telefone)
        {
            string caracterSeparador = ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE);
            if (string.IsNullOrEmpty(caracterSeparador))
            {
                caracterSeparador = "-"; // Valor Default
            }

            if (telefoneCompleto.Contains(caracterSeparador))
            {
                ddd = General.RetornaApenasAlfanumericos(telefoneCompleto.Substring(0, telefoneCompleto.IndexOf(caracterSeparador)));
                telefone = General.RetornaApenasAlfanumericos(telefoneCompleto.Substring(telefoneCompleto.IndexOf(caracterSeparador)));
            }
            else
            {
                ddd = "0";
                telefone = General.RetornaApenasAlfanumericos(telefoneCompleto);
            }
        }

        /// <summary>
        /// Método responsável por carregar os parâmetros das procs de atualização/inserção de fornecedores.
        /// </summary>
        /// <param name="db">Banco de dados</param>
        /// <param name="cmd">Comando</param>
        /// <param name="entity">Entidade</param>
        /// <remarks>oliveira - Ci&T - 17/09/2012</remarks>
        private static void LoadParametros(Database db, DbCommand cmd, dtpMM_Fornecedor entity)
        {
            //CGC e TIPO_DOCTO -verifica se o a propriedade CNPJ da entity está vazia para setar o valor do parametro 
            string cpfOrCnpj = string.Empty;
            int tipodoc = 0;
            if (!string.IsNullOrEmpty(entity.CNPJ))
            {
                cpfOrCnpj = entity.CNPJ;
                tipodoc = 1;
            }
            else if (!string.IsNullOrEmpty(entity.CPF))
            {
                cpfOrCnpj = entity.CPF;
                tipodoc = 2;
            }
            db.AddInParameter(cmd, "P_TP_DOCUMENTO", DbType.Int16, Convert.ToInt16(tipodoc));

            db.AddInParameter(cmd, "P_CGC", DbType.Int64, cpfOrCnpj);

            //Inscricao Etadual
            db.AddInParameter(cmd, "P_INSCR_ESTADUAL", DbType.String, entity.InscrEstadual);

            //Nome fornecedor
            db.AddInParameter(cmd, "P_NOME_FORNEC", DbType.String, entity.Nome);

            //Sigla
            db.AddInParameter(cmd, "P_SIGLA", DbType.String, entity.TermoPesquisa);

            //Endereco
            db.AddInParameter(cmd, "P_ENDERECO", DbType.String, entity.Rua);

            //Bairro
            db.AddInParameter(cmd, "P_BAIRRO", DbType.String, entity.Bairro);

            //Dominio Fiscal
            db.AddInParameter(cmd, "P_DOM_FISCAL", DbType.String, entity.DomFiscal);

            //Cep
            if (entity.CodPostal == null)
            {
                db.AddInParameter(cmd, "P_CEP", DbType.Int64, DBNull.Value);
            }
            else
            {
                db.AddInParameter(cmd, "P_CEP", DbType.Int64, Convert.ToInt64(entity.CodPostal));
            }

            //Caixa Postal
            if (entity.CxPostal == null)
            {
                db.AddInParameter(cmd, "P_CXPOSTAL", DbType.Int32, DBNull.Value);
            }
            else
            {
                db.AddInParameter(cmd, "P_CXPOSTAL", DbType.Int32, Convert.ToInt32(entity.CxPostal));
            }

            //Fax
            if (entity.Fax == null)
            {
                db.AddInParameter(cmd, "P_FAX", DbType.Int64, DBNull.Value);
            }
            else
            {
                db.AddInParameter(cmd, "P_FAX", DbType.Int64, Convert.ToInt64(entity.Fax));
            }

            //Telefone
            db.AddInParameter(cmd, "P_DDD", DbType.Int16, Convert.ToInt16(entity.Telefone.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None)[0]));
            db.AddInParameter(cmd, "P_FONE", DbType.Int64, Convert.ToInt64(entity.Telefone.Split(new string[] { ConfigurationAccess.ObterConfiguracao(Parametros.SEPARADOR_TELEFONE) }, StringSplitOptions.None)[1]));



            //db.AddInParameter(cmd, "P_TELEX", DbType.String, null); // nao possui no sap

            //db.AddInParameter(cmd, "P_NOME_CONTATO", DbType.String, null); // nao possui no sap

            //db.AddInParameter(cmd, "P_COD_COMPRADOR", DbType.String, null); //nao possui no sap

            //db.AddInParameter(cmd, "P_FL_GERENCIAL", DbType.String, null); //nao possui no sap

            //db.AddInParameter(cmd, "P_FL_SERVICO", DbType.String, null); //nao possui no sap

            //db.AddInParameter(cmd, "P_TIPO_NOTA", DbType.String, null); // nao possui no sap

            //db.AddInParameter(cmd, "P_TP_BLAU", DbType.String, null); //nao possui este campo

            // db.AddInParameter(cmd, "P_COD_FORNEC_MATRIZ", DbType.String, null);//nao possui no sap

            //Divisao
            db.AddInParameter(cmd, "P_DIVISAO", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DIVISAO, entity));

            //Classificacao
            db.AddInParameter(cmd, "P_CLASSIFICACAO", DbType.String, entity.Setor);

            //Situacao
            db.AddInParameter(cmd, "P_SITUACAO", DbType.Int16, Convert.ToInt16(GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_SITUACAO, entity)));

            //Conta Credito
            db.AddInParameter(cmd, "P_CONTA_CREDITO", DbType.Int64, Convert.ToInt64(entity.CtaReconciliacao));

            //FLAG EDI
            db.AddInParameter(cmd, "P_FL_EDI", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_EDI, entity));

            //Baixa Por Pedido
            db.AddInParameter(cmd, "P_FL_BAIXA_POR_PEDIDO", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_BAIXAR_PEDIDO, entity));

            //FL_DIF_ICMS
            db.AddInParameter(cmd, "P_FL_DIF_ICMS", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_DESCONTO_DIFERENCA_ICM, entity));

            //FL_ADICIONAL
            db.AddInParameter(cmd, "P_FL_ADICIONAL", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_FORNECEDOR_PERMITIR_DESCONTO_ADICIONAL, entity));

            //Codigo SAP do fornecedor
            db.AddInParameter(cmd, "P_COD_SAP", DbType.String, entity.Fornecedor);

            //parametros de saida
            db.AddOutParameter(cmd, "P_CODERRO", DbType.Int64, 8);
            db.AddOutParameter(cmd, "P_TXTERRO", DbType.String, 100);
        }
    }
}
