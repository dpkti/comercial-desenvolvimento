﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;

namespace DALC
{
    public class PedidoDALC
    {
        #region Singleton
        private static PedidoDALC instance;
        public static PedidoDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new PedidoDALC();
                }
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Consulta os pedidos que foram liberados dos bloqueiosc (crédito, política, OP...)
        /// </summary>
        /// <param name="codLoja">código da loja que está executando o serviço</param>
        /// <returns>Lista de pedidos liberados no BD legado para envio ao SAP</returns>
        public List<Entities.PedidoBE> ConsultarPedidosLiberados(int codLoja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<Entities.PedidoBE> listaPedidos = new List<PedidoBE>();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PEDIDOS_LIBERADOS))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, codLoja);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            PedidoBE pedido = new PedidoBE();
                            pedido.COD_LOJA = codLoja;
                            pedido.NUM_PEDIDO = Convert.ToInt32(reader["NUM_PEDIDO"]);
                            pedido.SEQ_PEDIDO = Convert.ToInt32(reader["SEQ_PEDIDO"]);

                            listaPedidos.Add(pedido);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaPedidos;
        }

        /// <summary>
        /// Método responsável por buscar os pedidos pendentes de envio para o SAP
        /// na tabela de controle. Status = "P".
        /// </summary>
        /// <returns>Entidade com os dados do pedido.</returns>
        /// <remarks>
        /// Criado por: Fabio Pallini - Ci&T
        /// Criado em: 05/10/12
        /// </remarks>
        public List<PedidoBE> ConsultarPedidosPendentes(int codLoja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<PedidoBE> mapper = MapBuilder<PedidoBE>.BuildAllProperties();
            List<PedidoBE> listaPedido = new List<PedidoBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PEDIDOS_PENDENTES))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, codLoja);
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            var pedido = new PedidoBE();
                            pedido = mapper.MapRow(reader);
                            pedido.ListaItensPedido = ConsultarItensPedido(codLoja, pedido.NUM_PEDIDO, pedido.SEQ_PEDIDO);
                            listaPedido.Add(pedido);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaPedido;
        }

        /// <summary>
        /// Método responsável por buscar os itens do pedido de envio para o SAP
        /// </summary>
        /// <returns>Lista Entidade dos itens.</returns>
        /// <remarks>
        /// Criado por: vxavier - Ci&T
        /// Criado em: 08/10/12
        /// </remarks>
        private List<ItemBE> ConsultarItensPedido(int codLoja, Int64 numPedido, int seqPedido)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<ItemBE> mapper = MapBuilder<ItemBE>.BuildAllProperties();
            List<ItemBE> listaItens = new List<ItemBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITENS_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, codLoja);
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, numPedido);
                    db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, seqPedido);
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            var item = new ItemBE();
                            item = mapper.MapRow(reader);
                            listaItens.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaItens;
        }

        /// <summary>
        /// Método responsável por atualizar os dados do Controle Pedido SAP
        /// </summary>
        /// <param name="item">Controle do Pedido SAP</param>
        /// <remarks>
        ///  vxavier@ - CiT (05/10/2012)
        /// </remarks>
        public void AtualizarControlePedidoSap(Entities.PedidoBE item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_CONTROLE_PEDIDO_SAP))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, item.ID_CONTROLE_PEDIDO);
                    if (item.DT_ENVIO.Equals(DateTime.MinValue))
                    {
                        db.AddInParameter(cmd, "P_DT_ENVIO", DbType.String, string.Empty);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_DT_ENVIO", DbType.DateTime, item.DT_ENVIO);
                    }
                    db.AddInParameter(cmd, "P_STATUS", DbType.String, item.STATUS);
                    db.AddInParameter(cmd, "P_NR_REMESSA_SAP", DbType.String, item.NR_REMESSA_SAP);
                    db.AddInParameter(cmd, "P_NR_FATURA_SAP", DbType.String, item.NR_FATURA_SAP);
                    db.AddInParameter(cmd, "P_NR_ORDEM_SAP", DbType.String, item.NR_ORDEM_SAP);
                    db.AddInParameter(cmd, "P_TEM_DIF_ESTOQ", DbType.String, item.TEM_DIF_ESTOQUE);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por inserir os dados do Controle Pedido SAP
        /// </summary>
        /// <param name="item">Objeto Entidade PedidoBE</param>
        /// <remarks>
        ///  oliveira@ - CiT (05/10/2012)
        /// </remarks>
        public void InserirPedidoPendente(Entities.PedidoBE item)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_CONTROLE_PEDIDO_SAP))
                {

                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, item.COD_LOJA);
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, item.NUM_PEDIDO);
                    db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int32, item.SEQ_PEDIDO);
                    if (item.DT_ENVIO.Equals(DateTime.MinValue))
                    {
                        db.AddInParameter(cmd, "P_DT_ENVIO", DbType.String, string.Empty);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_DT_ENVIO", DbType.DateTime, item.DT_ENVIO);
                    }

                    db.ExecuteNonQuery(cmd);

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        /// [ISA - 757] - Método que busca a taxa de juros por dia atrasado
        /// </summary>
        /// <param name="item">Objeto PedidoBE</param>
        /// <returns>taxa de juros por atraso</returns>
        /// <remarks>
        ///  vxavier@ - CiT (05/10/2012)
        /// </remarks>
        public Decimal BuscarJurosAtraso(Entities.PedidoBE item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Decimal retorno;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_TAXA_JUROS))
                {
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, item.NUM_PEDIDO);
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, item.COD_LOJA);
                    db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, item.SEQ_PEDIDO);
                    db.AddOutParameter(cmd, "P_VALOR_3", DbType.Decimal, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToDecimal(db.GetParameterValue(cmd, "P_VALOR_3"));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return retorno;
        }

        /// <summary>
        /// [ISA - 719] - Método que busca o código da região de vendas
        /// </summary>
        /// <param name="item">Objeto PedidoBE</param>
        /// <returns>código da região</returns>
        /// <remarks>
        ///  vxavier@ - CiT (05/10/2012)
        /// </remarks>
        public Int32 BuscarRegiaoVendas(Entities.PedidoBE item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Int32 retorno;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_COD_REGIONAL))
                {
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, item.NUM_PEDIDO);
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, item.COD_LOJA);
                    db.AddOutParameter(cmd, "P_COD_REGIONAL", DbType.Int32, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt32(db.GetParameterValue(cmd, "P_COD_REGIONAL"));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return retorno;
        }

        /// <summary>
        /// [ISA - 720] - Método que Busca Flag de parceiro com o COD_SAP
        /// </summary>
        /// <param name="item">Objeto PedidoBE</param>
        /// <returns>Dicionario com apenas 1 conjunto com a Flag_redespacho e Cod_sap da transportadora</returns>
        /// <remarks>
        ///  oliveira@ - CiT (08/10/2012)
        /// </remarks>
        public Dictionary<string, string> BuscaFlagRedespacho(Entities.PedidoBE item)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Dictionary<string, string> dicRetorno = null;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_FL_REDESPACHO))
                {

                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, item.NUM_PEDIDO);
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, item.COD_LOJA);
                    db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, item.SEQ_PEDIDO);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            dicRetorno = new Dictionary<string, string>();
                            dicRetorno.Add(reader["COD_SAP"].ToString(), reader["FL_REDESPACHO"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return dicRetorno;
        }

        /// <summary>
        /// [ISA - 720] - Método que Busca CodSAP se a Flag de Redespacho estiver "N"
        /// </summary>
        /// <param name="item">Objeto PedidoBE</param>
        /// <returns>Cod_SAP</returns>
        /// <remarks>
        ///  oliveira@ - CiT (08/10/2012)
        /// </remarks>
        public String BuscaTranspParametro(Entities.PedidoBE item, CentroDistribuicaoBE oCentro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            string retorno = string.Empty;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_COD_TRANSPORTADORA))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, oCentro.COD_LOJA);
                    db.AddInParameter(cmd, "P_TIPO_BANCO", DbType.String, ((char)oCentro.TipoBanco).ToString());

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            retorno = reader["COD_SAP2"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return retorno;
        }

        /// <summary>
        /// Método responsável por fazer a entrada na tabela de relacionamento SAP.CONTROLE_PEDIDO_SAP_LOG.
        /// </summary>
        /// <param name="idControlePedido">ID na tabela controle pedido.</param>
        /// <param name="idLogSincLegado">ID na tabela Sinc Arquivo.</param>
        public void InserirLogControlePedido(string idControlePedido, long idLogSincArquivo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_LOG_CONTROLE_PEDIDO_SAP))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_ID_LOG_SINC_ARQUIVO", DbType.Int64, idLogSincArquivo);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        #region monitoramento de pedidos

        /// <summary>
        /// Busca os pedidos na tabela controle_pedido_sap - monitoramento de pedidos
        /// </summary>
        /// <param name="codLojaDestino">CD a ser efetuado a busca</param>
        /// <param name="status">filtro por status</param>
        /// <param name="dataIni">filtro de periodo de data de envio - data inicial</param>
        /// <param name="dataFim">filtro de periodo de data de envio - data final</param>
        /// <param name="dbLink">dbLink para conexão com CDs fora do CD em que está rodando a aplicação</param>
        /// <returns>lista de pedidos a serem exibidos no monitor de pedidos</returns>
        public List<PedidoDH> BuscarPedidos(int codLojaOrigem, int codLojaDestino, string status, DateTime dataIni, DateTime dataFim)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<PedidoDH> mapper = MapBuilder<PedidoDH>.BuildAllProperties();
            List<PedidoDH> listPedido = null;
            PedidoDH pedido = null;
            IDataReader reader = null;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CONTROLE_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA_ORIGEM", DbType.Int32, codLojaOrigem);
                    db.AddInParameter(cmd, "P_COD_LOJA_DESTINO", DbType.Int32, codLojaDestino);
                    db.AddInParameter(cmd, "P_STATUS", DbType.String, status);

                    if (dataIni.Date > DateTime.MinValue && dataFim.Date > DateTime.MinValue)
                    {
                        db.AddInParameter(cmd, "P_DATA_INI", DbType.DateTime, dataIni);
                        db.AddInParameter(cmd, "P_DATA_FIM", DbType.DateTime, dataFim);
                    }

                    reader = db.ExecuteReader(cmd);
                    listPedido = new List<PedidoDH>();

                    while (reader.Read())
                    {
                        pedido = mapper.MapRow(reader);

                        listPedido.Add(pedido);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogFinalMetodo(dataInicio);
            }

            return listPedido;
        }

        /// <summary>
        /// Busca o histórico de log de um determinado pedido (por id_controle_pedido)
        /// </summary>
        /// <param name="idControlePedido">chave da tabela - id_controle_pedido - sap.pedido_controle_sap</param>
        /// <param name="dbLink">dbLink para conexão com CDs fora do CD em que está rodando a aplicação</param>
        /// <returns>lista de histórico de log de um pedido</returns>
        public List<HistoricoPedidoDH> BuscarHistoricoPedido(string idControlePedido, int codLojaBase, int codLojaDestino)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<HistoricoPedidoDH> mapper = MapBuilder<HistoricoPedidoDH>.BuildAllProperties();
            List<HistoricoPedidoDH> listPedido = null;
            HistoricoPedidoDH pedido = null;
            IDataReader reader = null;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_HIST_CONTROLE_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_COD_LOJA_ORIGEM", DbType.Int32, codLojaBase);
                    db.AddInParameter(cmd, "P_COD_LOJA_DESTINO", DbType.Int32, codLojaDestino);

                    reader = db.ExecuteReader(cmd);
                    listPedido = new List<HistoricoPedidoDH>();

                    while (reader.Read())
                    {
                        pedido = mapper.MapRow(reader);

                        listPedido.Add(pedido);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogFinalMetodo(dataInicio);
            }

            return listPedido;
        }

        /// <summary>
        /// Atualiza o status de um controle_pedido filtrando pelo status anterior e chave da tabela    
        /// </summary>
        /// <param name="idControlePedido">chave da tabela - id_controle_pedido - sap.pedido_controle_sap</param>
        /// <param name="status">status a ser atualizado</param>
        /// <param name="statusAnterior">status anterior a ser filtrado</param>
        public void AtualizaStatusControlePedido(string idControlePedido, string status, string statusAnterior)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_STATUS_CONTROLE_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_STATUS", DbType.String, status);
                    db.AddInParameter(cmd, "P_STATUS_ANTERIOR", DbType.String, statusAnterior);


                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        #endregion

        #region cancelamento de pedidos

        /// <summary>
        /// Método responsável por buscar o pedido a ser cancelado
        /// vxavier CiT 10/10/2012 - [ISA - 763] Criar BO/DALC/procedure para carregar dados na tela
        /// </summary>
        /// <param name="codLoja">Código da Loja</param>
        /// <param name="numPedido">Número do Pedido</param>
        /// <param name="seqPedido">Sequencia do Pedido</param>
        /// <param name="numNota">Número Nota</param>
        /// <returns>Pedido para cancelamento</returns>
        public PedidoCancelamentoDH ConsultarPedidoASerCancelado(string codLoja, string numPedido, string seqPedido, string numNota)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<PedidoCancelamentoDH> mapper = MapBuilder<PedidoCancelamentoDH>.MapAllProperties().DoNotMap(x => x.COD_LOJA).DoNotMap(y => y.COD_CANCEL).DoNotMap(k => k.SITUACAO).DoNotMap(w => w.STATUS).DoNotMap(j => j.ID_CONTROLE_PEDIDO_SAP).Build();
            PedidoCancelamentoDH pedido = new PedidoCancelamentoDH();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, Convert.ToInt32(codLoja));

                    if (string.IsNullOrEmpty(numPedido))
                    {
                        db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, DBNull.Value);
                        db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, DBNull.Value);
                        db.AddInParameter(cmd, "P_NUM_NOTA", DbType.Int64, Convert.ToInt64(numNota));
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, Convert.ToInt64(numPedido));
                        db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, Convert.ToInt16(seqPedido));
                        db.AddInParameter(cmd, "P_NUM_NOTA", DbType.Int64, DBNull.Value);
                    }


                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            pedido = mapper.MapRow(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return pedido;
        }

        /// <summary>
        /// Método responsável por cancelar o pedido
        /// [ISA - 767] - Criar BO/DALC para chamada da procedure PR_CANCELA
        /// </summary>
        /// <param name="pedido">Pedido a ser Cancelado</param>
        /// <param name="banco">CD de onde esta sendo cancelado o pedido</param>
        /// <remarks>vxavier CiT 11/10/12</remarks>
        public void CancelarPedido(PedidoCancelamentoDH pedido, string nomeProcedure)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(nomeProcedure))
                {
                    db.AddInParameter(cmd, "p_cod_loja", DbType.Int16, pedido.COD_LOJA);
                    db.AddInParameter(cmd, "p_num_pedido", DbType.Int64, pedido.NUM_PEDIDO);
                    db.AddInParameter(cmd, "p_seq_pedido", DbType.Int16, pedido.SEQ_PEDIDO);
                    db.AddInParameter(cmd, "p_situacao", DbType.Int16, pedido.SITUACAO);
                    db.AddInParameter(cmd, "p_cod_cancel", DbType.Int32, pedido.COD_CANCEL);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por busca o status e o id de controle do pedido
        /// </summary>
        /// <param name="pedido">Pedidos a ser cancelado</param>
        /// <remarks>vxavier CiT - 11/10/12</remarks>
        public void BuscarControlePedido(PedidoCancelamentoDH pedido)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CONTROLE_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, pedido.COD_LOJA);
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, pedido.NUM_PEDIDO);
                    db.AddInParameter(cmd, "P_SEQ_PEDIDO", DbType.Int16, pedido.SEQ_PEDIDO);
                    db.AddOutParameter(cmd, "P_STATUS", DbType.String, 50);
                    db.AddOutParameter(cmd, "P_ID_CONTROLE_PEDIDO_SAP", DbType.String, 50);

                    db.ExecuteNonQuery(cmd);

                    pedido.STATUS = db.GetParameterValue(cmd, "P_STATUS").ToString();
                    pedido.ID_CONTROLE_PEDIDO_SAP = db.GetParameterValue(cmd, "P_ID_CONTROLE_PEDIDO_SAP").ToString();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        #endregion
    }
}
