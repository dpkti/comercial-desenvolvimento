﻿using System;
using System.Data;
using System.Data.Common;
using System.Transactions;
using Entities;
using Entities.XSD.Cliente;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;

namespace DALC
{
    public class ClienteDPKDALC
    {
        #region Singleton
        private static ClienteDPKDALC instance;
        public static ClienteDPKDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ClienteDPKDALC();
                }
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Obtem o ID do cliente no legado e a ultima data de alteração
        /// </summary>
        /// <param name="cicCgc">CicCgc do cliente para pesquisar</param>
        /// <returns>Entidade com os campos esperados(CICCGC e ultima Data Alteração)</returns>
        /// KEVLIN TOSHINARI OSSADA - CIT - 29/08/2012
        public ClienteBE BuscaClienteDPK(long cod_sap)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<ClienteBE> mapper = MapBuilder<ClienteBE>.BuildAllProperties();
            ClienteBE clienteDPK = null;
            IDataReader reader = null;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CLIENTE_DPK))
                {
                    db.AddInParameter(cmd, "p_cod_sap", DbType.Double, cod_sap);

                    reader = db.ExecuteReader(cmd);

                    if (reader.Read())
                    {
                        clienteDPK = mapper.MapRow(reader);
                    }
                    else
                    {
                        Logger.LogWarning(String.Format("Código SAP {0} não encontrado.", cod_sap));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogFinalMetodo(dataInicio);
            }

            return clienteDPK;
        }

        /// <summary>
        /// Método para atualizar um cliente no legado.
        /// Criado por: Fabio Pallini - Ci&T
        /// Criado em: 30/08/12
        /// </summary>
        /// <param name="oCliente">Objeto Cliente preenchido.</param>
        /// <returns>Indicador de sucesso da atualização.</returns>
        public bool AtualizarDadosCliente(dtpSD_cliente oCliente)
        {
            bool retorno = false;

            try
            {
                using (TransactionScope ts = new TransactionScope())
                {

                    // Atualiza o cabeçalho do cliente, inclusive o endereço 1 (endereço principal).
                    AtualizaDadosCliente(oCliente);

                    // Se o cliente tiver mais de 1 endereço (ex: cobrança, entrega), chama a proc que atualiza tabela endereço.
                    if (oCliente.Enderecos.Length > 1)
                    {
                        for (int i = 1; i < oCliente.Enderecos.Length; i++)
                        {
                            AtualizaEnderecosCliente(oCliente.Enderecos[i], Int32.Parse(oCliente.NoCliente_POS), oCliente.Name1, oCliente.NoIDFiscal3);
                        }
                    }

                    ts.Complete();
                    retorno = true;
                }
            }
            catch (Exception ex)
            {
                throw new DALCException(ex);
            }

            return retorno;
        }


        /// <summary>
        /// [ISA - 366] - Criar método no DALC para gravar dados do Cliente (objeto dtpSD_cliente)
        /// </summary>
        /// <param name="oCliente"></param>
        /// vxavier - Ci&T - 29/08/2012 
        private void AtualizaDadosCliente(dtpSD_cliente oCliente)
        {
            string cnpjOrCpf = "";
            DateTime dataFundOrNasc = new DateTime();

            if (!String.IsNullOrEmpty(oCliente.NoIDFiscal1))
            {
                cnpjOrCpf = oCliente.NoIDFiscal1;
                dataFundOrNasc = oCliente.DataFundacao;
            }

            if (!String.IsNullOrEmpty(oCliente.NoIDFiscal2))
            {
                cnpjOrCpf = oCliente.NoIDFiscal2;
                dataFundOrNasc = oCliente.DtNascimento;
            }

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_CLIENTE))
            {
                //codigo do cliente
                db.AddInParameter(cmd, "PCODCLIENTE", DbType.Int64, Convert.ToInt64(oCliente.NoCliente_POS));
                //nome do cliente
                db.AddInParameter(cmd, "PNOMECLIENTE", DbType.String, oCliente.Name1);

                //endereco do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].Rua))
                {
                    db.AddInParameter(cmd, "PENDERECO", DbType.String, oCliente.Enderecos[0].Rua);
                }
                else
                {
                    db.AddInParameter(cmd, "PENDERECO", DbType.String, null);
                }

                //bairro do cliente
                db.AddInParameter(cmd, "PBAIRRO", DbType.String, oCliente.Enderecos[0].Bairro);

                //cep do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].CEP))
                {
                    db.AddInParameter(cmd, "PCEP", DbType.Int64, Convert.ToInt64(UtilGeralDPA.General.RetornaApenasAlfanumericos(oCliente.Enderecos[0].CEP)));
                }
                else
                {
                    db.AddInParameter(cmd, "PCEP", DbType.Int64, null);
                }

                //cidade do cliente
                db.AddInParameter(cmd, "PNOME_CIDADE", DbType.String, oCliente.Enderecos[0].Cidade);

                //estado do cliente
                db.AddInParameter(cmd, "PCOD_UF", DbType.String, oCliente.Enderecos[0].Regiao);

                //DDD do telefone do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].DDDFone))
                {
                    db.AddInParameter(cmd, "PDDD1", DbType.Int32, Convert.ToInt32(oCliente.Enderecos[0].DDDFone));
                }
                else
                {
                    db.AddInParameter(cmd, "PDDD1", DbType.Int32, null);
                }

                //telefone do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].Telefone))
                {
                    db.AddInParameter(cmd, "PFONE1", DbType.Int64, Convert.ToInt64(oCliente.Enderecos[0].Telefone));
                }
                else
                {
                    db.AddInParameter(cmd, "PFONE1", DbType.Int64, null);
                }

                //DDD do celular do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].DDDFax))
                {
                    db.AddInParameter(cmd, "PDDD2", DbType.Int32, Convert.ToInt32(oCliente.Enderecos[0].DDDFax));
                }
                else
                {
                    db.AddInParameter(cmd, "PDDD2", DbType.Int32, null);
                }

                //celular do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].Celular))
                {
                    db.AddInParameter(cmd, "PFONE2", DbType.Int64, Convert.ToInt64(oCliente.Enderecos[0].Celular));
                }
                else
                {
                    db.AddInParameter(cmd, "PFONE2", DbType.Int64, null);
                }

                //fax do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].TelFax))
                {
                    db.AddInParameter(cmd, "PFAX", DbType.Int64, Convert.ToInt64(oCliente.Enderecos[0].TelFax));
                }
                else
                {
                    db.AddInParameter(cmd, "PFAX", DbType.Int64, null);
                }

                //telex do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].Telex))
                {
                    db.AddInParameter(cmd, "PTELEX", DbType.Int32, Convert.ToInt32(oCliente.Enderecos[0].Telex));
                }
                else
                {
                    db.AddInParameter(cmd, "PTELEX", DbType.Int32, null);
                }

                //CXPOSTAL do cliente
                if (!String.IsNullOrEmpty(oCliente.Enderecos[0].CxPostal))
                {
                    db.AddInParameter(cmd, "PCXPOSTAL", DbType.Int32, Convert.ToInt32(oCliente.Enderecos[0].CxPostal));
                }
                else
                {
                    db.AddInParameter(cmd, "PCXPOSTAL", DbType.Int32, null);
                }

                //nome do contato do cliente
                db.AddInParameter(cmd, "PNOME_CONTATO", DbType.String, oCliente.Contatos[0].Nome);

                //codigo tare
                if (!String.IsNullOrEmpty(oCliente.Fiscal[0].CodTARE))
                {
                    db.AddInParameter(cmd, "PCOD_TARE", DbType.Int64, Convert.ToInt64(oCliente.Fiscal[0].CodTARE));
                }
                else
                {
                    db.AddInParameter(cmd, "PCOD_TARE", DbType.Int64, null);
                }

                //tipo cliente
                db.AddInParameter(cmd, "PCOD_TIPO_CLIENTE", DbType.String, oCliente.SubTpCliente);

                //data de nascimento do cliente ou data de fundacao da empresa
                db.AddInParameter(cmd, "PDT_FUNDACAO", DbType.Date, dataFundOrNasc);

                //suframa
                db.AddInParameter(cmd, "PINSCR_SUFRAMA", DbType.String, oCliente.NoIDFiscal4);

                //inscricao estadual
                db.AddInParameter(cmd, "PINSCR_ESTADUAL", DbType.String, "ISENTO");


                //mensagem fiscal
                if (!string.IsNullOrEmpty(oCliente.Fiscal[0].MsgFiscal1))
                {
                    db.AddInParameter(cmd, "PCOD_MENSAGEM_FISCAL", DbType.Int16, Convert.ToInt16(oCliente.Fiscal[0].MsgFiscal1));
                }
                else
                {
                    db.AddInParameter(cmd, "PCOD_MENSAGEM_FISCAL", DbType.Int16, null);
                }

                //tipo da empresa
                db.AddInParameter(cmd, "PTP_EMPRESA", DbType.String, oCliente.Fiscal[0].TpEmpresa);

                //data do cadastro
                db.AddInParameter(cmd, "PDT_CADASTRO", DbType.Date, oCliente.DataCadastro);

                //data do recadastro
                db.AddInParameter(cmd, "PDT_RECADASTRO", DbType.Date, oCliente.DataReCadastro);

                //data da atualizacao do sap
                db.AddInParameter(cmd, "PDT_ATUALIZACAO", DbType.DateTime, DateTime.Now);

                //codigo do banco
                if (!String.IsNullOrEmpty(oCliente.Bancos[0].CodBanco))
                {
                    db.AddInParameter(cmd, "PCOD_BANCO", DbType.Int16, Int16.Parse(oCliente.Bancos[0].CodBanco));
                }
                else
                {
                    db.AddInParameter(cmd, "PCOD_BANCO", DbType.Int16, null);
                }

                //parametros de saida
                db.AddOutParameter(cmd, "PCODERRO", DbType.Int64, 8);
                db.AddOutParameter(cmd, "PTXTERRO", DbType.String, 100);

                string mensagemErro = string.Empty;
                long codError = 0;

                if (db.ExecuteNonQuery(cmd) > 0)
                {
                    codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "PCODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "PCODERRO").ToString());
                    mensagemErro = db.GetParameterValue(cmd, "PTXTERRO").ToString();
                }
                else
                {
                    throw new DALCException("Cliente não encontrado para atualização.");
                }

                if (codError != 0)
                {
                    throw new DALCException(codError, mensagemErro);
                }
            }

            Logger.LogInfo("O cliente atualizado possui CNPJ/CPF: " + cnpjOrCpf + ", COD. SAP: " + oCliente.NoCliente_SAP + " e COD DPK: " + oCliente.NoCliente_POS);
        }

        /// <summary>
        /// [ISA - 366] - Criar método no DALC para gravar dados do Cliente (objeto dtpSD_cliente)
        /// </summary>
        /// <param name="oEndereco"></param>
        /// <param name="noCliente_POS"></param>
        /// <param name="nomeCliente"></param>
        /// <param name="NoIDFiscal3"></param>
        /// vxavier - Ci&T - 30/08/2012 - 
        private void AtualizaEnderecosCliente(dtpSD_Endereco oEndereco, int noCliente_POS, string nomeCliente, string NoIDFiscal3)
        {
            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_CLIENTE_ENDERECO))
            {
                db.AddInParameter(cmd, "P_CODCLIENTE", DbType.Int64, Convert.ToInt64(noCliente_POS));
                db.AddInParameter(cmd, "P_TIPO", DbType.Int16, oEndereco.TpEndereco.Equals("ZP") ? 1 : 2);
                db.AddInParameter(cmd, "P_INSCR_ESTADUAL", DbType.String, NoIDFiscal3);
                db.AddInParameter(cmd, "P_NOME_CLIENTE", DbType.String, nomeCliente);
                db.AddInParameter(cmd, "P_NM_CIDADE", DbType.String, oEndereco.Cidade);
                db.AddInParameter(cmd, "P_COD_UF", DbType.String, oEndereco.Regiao);
                db.AddInParameter(cmd, "P_BAIRRO", DbType.String, oEndereco.Bairro);

                //o endereco ja chega concatenado na propriedade rua
                db.AddInParameter(cmd, "P_ENDERECO", DbType.String, oEndereco.Rua);

                if (!String.IsNullOrEmpty(oEndereco.CEP))
                {
                    db.AddInParameter(cmd, "P_CEP", DbType.Int32, Convert.ToInt32(UtilGeralDPA.General.RetornaApenasAlfanumericos(oEndereco.CEP)));
                }
                else
                {
                    db.AddInParameter(cmd, "P_CEP", DbType.Int32, null);
                }

                if (!String.IsNullOrEmpty(oEndereco.CxPostal))
                {
                    db.AddInParameter(cmd, "P_CXPOSTAL", DbType.Int64, Convert.ToInt64(oEndereco.CxPostal));
                }
                else
                {
                    db.AddInParameter(cmd, "P_CXPOSTAL", DbType.Int64, null);
                }

                if (!String.IsNullOrEmpty(oEndereco.DDDFone))
                {
                    db.AddInParameter(cmd, "P_DDDFONE", DbType.Int64, Convert.ToInt64(oEndereco.DDDFone));
                }
                else
                {
                    db.AddInParameter(cmd, "P_DDDFONE", DbType.Int64, null);
                }

                if (!String.IsNullOrEmpty(oEndereco.Telefone))
                {
                    db.AddInParameter(cmd, "P_TELEFONE", DbType.Int64, Convert.ToInt64(oEndereco.Telefone));
                }
                else
                {
                    db.AddInParameter(cmd, "P_TELEFONE", DbType.Int64, null);
                }

                if (!String.IsNullOrEmpty(oEndereco.TelFax))
                {
                    db.AddInParameter(cmd, "P_TELFAX", DbType.Double, Convert.ToDouble(oEndereco.TelFax));
                }
                else
                {
                    db.AddInParameter(cmd, "P_TELFAX", DbType.Double, null);
                }

                //parametros de saida
                db.AddOutParameter(cmd, "P_CODERRO", DbType.Int64, 8);
                db.AddOutParameter(cmd, "P_TXTERRO", DbType.String, 100);

                string mensagemErro = string.Empty;
                long codError = 0;

                if (db.ExecuteNonQuery(cmd) > 0)
                {
                    codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                    mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                }
                else
                {
                    throw new DALCException("Cliente não encontrado para atualização.");
                }

                if (codError != 0)
                {
                    throw new DALCException(codError, mensagemErro);
                }
            }

            Logger.LogInfo("Atualizado endereço do cliente Cod DPK: " + noCliente_POS);
        }
    }
}