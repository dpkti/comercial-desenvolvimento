﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;

namespace DALC
{
    public static class CommonDPKDALC
    {

        public static List<LojaConexaoBE> ObterConexaoLojas()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<LojaConexaoBE> mapper = MapBuilder<LojaConexaoBE>.BuildAllProperties();
            IDataReader reader = null;
            LojaConexaoBE lojaConexaoBE = null;
            List<LojaConexaoBE> listLojaConexaoBE = new List<LojaConexaoBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_LOJA_CONEXAO))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        lojaConexaoBE = new LojaConexaoBE();
                        lojaConexaoBE = mapper.MapRow(reader);
                        listLojaConexaoBE.Add(lojaConexaoBE);
                    }

                }
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listLojaConexaoBE;
        }

        /// <summary>
        ///  carlosj CiT 08/10/2012 - ObterParametrosSistema
        /// </summary>
        /// <param name="codLoja"></param>
        /// <returns>um array com os parametros do sistema</returns>
        public static List<ParametroSistemaBE> ObterParametrosSistema(int codLoja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();


            IRowMapper<ParametroSistemaBE> mapper = MapBuilder<ParametroSistemaBE>.BuildAllProperties();
            IDataReader reader = null;
            ParametroSistemaBE entity = null;
            List<ParametroSistemaBE> listParametroSistemaBE = new List<ParametroSistemaBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PARAM_SISTEMA))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, codLoja);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        entity = new ParametroSistemaBE();
                        entity = mapper.MapRow(reader);
                        listParametroSistemaBE.Add(entity);
                    }

                }
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listParametroSistemaBE;
        }

        #region monitoramento de pedidos

        public static UsuarioBE ValidaUsuario(UsuarioBE oUser)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();


            IDataReader reader = null;
            UsuarioBE usuarioLogado = null;
            IRowMapper<UsuarioBE> mapper = MapBuilder<UsuarioBE>.MapAllProperties().DoNotMap(x => x.listLojas).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_USUARIO))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, oUser.LOGIN);
                    db.AddInParameter(cmd, "P_SENHA", DbType.String, oUser.SENHA);

                    reader = db.ExecuteReader(cmd);
                    if (reader.Read())
                    {
                        usuarioLogado = mapper.MapRow(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);

                //no data found - usuário não encontrado
                if (((OracleException)ex).Code == 1403)
                    usuarioLogado = null;
                else
                    new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                Logger.LogFinalMetodo(dataInicio);
            }

            return usuarioLogado;
        }

        public static List<CentroDistribuicaoBE> BuscarCDUsuario(int codLoja, string nomeAplicacao, long codUsuario)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            IRowMapper<CentroDistribuicaoBE> mapper = MapBuilder<CentroDistribuicaoBE>.MapAllProperties().DoNotMap(x => x.TipoBanco).Build();
            List<CentroDistribuicaoBE> listPedido = null;
            CentroDistribuicaoBE pedido = null;
            IDataReader reader = null;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_DEPOSITO_VISAO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, codLoja);
                    db.AddInParameter(cmd, "P_NOME_APLICACAO", DbType.String, nomeAplicacao);
                    db.AddInParameter(cmd, "P_COD_USUARIO", DbType.Int64, codUsuario);

                    reader = db.ExecuteReader(cmd);
                    listPedido = new List<CentroDistribuicaoBE>();

                    while (reader.Read())
                    {
                        pedido = mapper.MapRow(reader);

                        listPedido.Add(pedido);
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogFinalMetodo(dataInicio);
            }

            return listPedido;
        }

        /// <summary>
        /// Método responsável por verificar se o usuario possui permissão para cancelar pedido.
        /// </summary>
        /// <param name="codUsuario">Código usuario a ser verificado.</param>
        /// <returns>Indicador se o usuário possui permissão.</returns>
        /// <remarks>
        ///  vxavier@ - CiT (10/10/2012)
        /// </remarks>
        public static bool VerificarPermissaoCancelPedido(Int64 codUsuario, int codLoja, EnumeradoresAplicacao.TipoBanco tipoBanco)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool possuiPermissao = false;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                //  IDataReader reader = null;

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PERMISSAO_CANCELAMENTO))
                {

                    db.AddInParameter(cmd, "P_COD_USUARIO", DbType.String, codUsuario.ToString());
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, codLoja);
                    db.AddInParameter(cmd, "P_TIPO_BANCO", DbType.String, ((char)tipoBanco).ToString());

                    //caso o retorno seja maior que 0 é porque possui permissao
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            possuiPermissao = (int.Parse(reader[0].ToString()) > 0);
                        }
                    }

                }

                return possuiPermissao; 
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar o nome de exibicao da loja.
        /// </summary>
        /// <param name="cod_loja">Código Loja.</param>
        /// <returns>Nome de Exibicao da loja.</returns>
        /// <remarks>
        ///  vxavier - CiT (10/10/2012)
        /// </remarks>
        public static string BuscarNomeExibicaoCDs(int cod_loja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            string retorno = String.Empty;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_NOME_EXIBICAO_LOJA))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, cod_loja);
                    db.AddOutParameter(cmd, "P_NOME_EXIBICAO", DbType.String, 50);

                    db.ExecuteNonQuery(cmd);

                    retorno = db.GetParameterValue(cmd, "P_NOME_EXIBICAO").ToString();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        /// <summary>
        /// Método responsável por buscar a descrição do cancelamento do pedido 
        /// </summary>
        /// <param name="cod_cancel">Código Cancel</param>
        /// <returns>Descrição do cancelamento</returns>
        /// <remarks>vxavier CiT - 10/10/12</remarks>
        public static string BuscarDescricaoCancelamento(int cod_cancel)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            string retorno = String.Empty;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_DESCRICAO_CANCEL))
                {
                    db.AddInParameter(cmd, "P_COD_CANCEL", DbType.Int16, cod_cancel);
                    db.AddOutParameter(cmd, "P_DESCRICAO", DbType.String, 50);

                    db.ExecuteNonQuery(cmd);

                    retorno = db.GetParameterValue(cmd, "P_DESCRICAO").ToString();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        #endregion

    }
}
