﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Entities;
using Entities.XSD.Material;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;


namespace DALC
{
    public class MaterialDPKDALC
    {

        #region Singleton
        private static MaterialDPKDALC instance;
        public static MaterialDPKDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MaterialDPKDALC();
                }
                return instance;
            }
        }
        #endregion


        /// <summary>
        /// Método responsável por verificar se o produto já é cadastrado no banco.
        /// </summary>
        /// <param name="cod_sap">Código SAP a ser verificado.</param>
        /// <returns>Indicador se produto existe.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public bool VerificarExistenciaMaterialDPK(string cod_dpk)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_MATERIAL_DPK))
                {
                    db.AddInParameter(cmd, "p_cod_dpk", DbType.String, cod_dpk);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }


        /// <summary>
        /// Método responsável por verificar se a linha já está cadastrada no banco.
        /// </summary>
        /// <param name="cod_linha">Código linha a ser verificado.</param>
        /// <returns>Indicador se linha existe.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public bool VerificarExistenciaLinhaDPK(string cod_linha)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_LINHA))
                {
                    db.AddInParameter(cmd, "p_cod_linha", DbType.String, cod_linha);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }


        /// <summary>
        /// Método responsável por verificar se o fornecedor já é cadastrado no banco.
        /// </summary>
        /// <param name="cod_sap">Código SAP a ser verificado.</param>
        /// <returns>Indicador se fornecedor existe.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public bool VerificarExistenciaFornecedorDPK(string cod_sap)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_FORNECEDOR_DPK))
                {
                    db.AddInParameter(cmd, "p_cod_sap", DbType.String, cod_sap);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }


        /// <summary>
        /// Método responsável por verificar se o grupo e sub-grupo já estão cadastrados no banco.
        /// </summary>
        /// <param name="cod_grupo">Código grupo a ser verificado.</param>
        /// <param name="cod_subGrupo">Código Sub-Grupo a ser verificado.</param>
        /// <returns>Indicador se Grupo e Sub-grupo existem.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public bool VerificarExistenciaGrupoSubgrupoDPK(string cod_grupo, string cod_subGrupo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GRUPO_SUBGRUPO))
                {
                    db.AddInParameter(cmd, "p_cod_grupo", DbType.String, cod_grupo);
                    db.AddInParameter(cmd, "p_cod_subgrupo", DbType.String, cod_subGrupo);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        /// <summary>
        /// Método responsável por verificar se a unidade já está cadastrado no banco.
        /// </summary>
        /// <param name="cod_unidade">unidade a ser verificado.</param>
        /// <returns>Indicador se a unidade existe.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public bool VerificarExistenciaUnidadeDPK(string cod_unidade)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            bool retorno = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_UNIDADE))
                {
                    db.AddInParameter(cmd, "p_cod_unidade", DbType.String, cod_unidade);
                    db.AddOutParameter(cmd, "EXISTE", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = Convert.ToInt64(db.GetParameterValue(cmd, "EXISTE").ToString()) > 0;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }


        /// <summary>
        /// Método responsável por verificar se codigo EAN já está cadastrado no banco.
        /// </summary>
        /// <param name="cod_ean">codigo EAN a ser verificado.</param>
        /// <returns>Indicador se o codigo EAN existe.</returns>
        /// <remarks>
        ///  oliveira@ - CiT (25/09/2012)
        /// </remarks>
        public string VerificarExistenciaEANDPK(string cod_ean)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            string retorno = string.Empty;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_R_ITEM_EAN))
                {
                    db.AddInParameter(cmd, "p_cod_ean", DbType.Int64, Convert.ToInt64(cod_ean));
                    db.AddOutParameter(cmd, "P_COD_DPK", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = db.GetParameterValue(cmd, "P_COD_DPK").ToString();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }

        /// <summary>
        /// Método responsável por verificar se codigo fornecedor junto com codigo fabrica já está cadastrado no banco.
        /// </summary>
        /// <param name="cod_sap_fornecedor">codigo sap do fornecedor a ser verificado.</param>
        /// <param name="cod_fabrica">codigo fabrica a ser verificado.</param>
        /// <returns>Indicador se o codigo fornecedor junto com o codigo fabrica existe.</returns>
        /// <remarks>
        ///  vxavier@ - CiT (01/10/2012)
        /// </remarks>
        public string VerificarExistenciaFornecedorFabrica(string cod_sap_fornecedor, string cod_fabrica)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            string retorno = string.Empty;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_FORNEC_FABRICA))
                {
                    db.AddInParameter(cmd, "P_COD_SAP_FORNECEDOR", DbType.String, cod_sap_fornecedor);
                    db.AddInParameter(cmd, "P_COD_FABRICA", DbType.String, cod_fabrica);
                    db.AddOutParameter(cmd, "P_COD_DPK", DbType.Int64, 8);

                    db.ExecuteNonQuery(cmd);

                    retorno = db.GetParameterValue(cmd, "P_COD_DPK").ToString();
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return retorno;
        }


        /// <summary>
        /// Método responsável por atualizar um produto.
        /// </summary>
        /// <param name="entity">Objeto produto preenchido.</param>
        /// <remarks>oliveira@ - CiT (25/09/2012)</remarks>
        public void AtualizarDadosMaterial(dtpMM_Material entity)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_MATERIAL))
                {
                    //parametros que não são default para todos os metodos
                    //min_estoque
                    db.AddInParameter(cmd, "P_MIN_ESTOQUE", DbType.Decimal, Convert.ToDecimal(entity.Centro[0].EstoqSegMin));

                    //cod_ean
                    db.AddInParameter(cmd, "P_COD_EAN", DbType.Int64, Convert.ToInt64(entity.CodEAN));

                    LoadParametrosComuns(db, cmd, entity);

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Produto não encontrado para atualização.");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                }

                Logger.LogInfo("Produto Atualizado COD SAP " + entity.Material + ".");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }


        /// <summary>
        /// Método responsável por inserir um produto.
        /// </summary>
        /// <param name="entity">Objeto produto preenchido.</param>
        /// <remarks>oliveira@ - CiT (25/09/2012)</remarks>
        public void InserirDadosMaterial(dtpMM_Material entity)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_MATERIAL))
                {
                    //parametros que não são default para todos os metodos
                    //cod_loja
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.String, "01");//default 01 = Campinas

                    //min_estoque
                    db.AddInParameter(cmd, "P_MIN_ESTOQUE", DbType.Decimal, Convert.ToDecimal(entity.Centro[0].EstoqSegMin));

                    //cod_ean
                    db.AddInParameter(cmd, "P_COD_EAN", DbType.Int64, Convert.ToInt64(entity.CodEAN));

                    LoadParametrosComuns(db, cmd, entity);

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Não foi possivel Inserir");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                }

                Logger.LogInfo("Produto Inserido COD SAP " + entity.Material + ".");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        /// Método responsável por inserir um produto por OWNER.
        /// </summary>
        /// <param name="entity">Objeto produto preenchido.</param>
        /// <param name="owner">Numero do CD que será o OWNER da procedure</param>
        /// <remarks>oliveira@ - CiT (26/09/2012)</remarks>
        public void InserirAtualizarDadosMaterialPorOWNER(dtpMM_Material entity, string owner)
        {

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_UPD_MATERIAL_DPK_DEPXX))
                {

                    //OWNER
                    db.AddInParameter(cmd, "P_LOJA", DbType.String, owner);

                    LoadParametrosComuns(db, cmd, entity);

                    string mensagemErro = string.Empty;
                    long codError = 0;

                    if (db.ExecuteNonQuery(cmd) > 0)
                    {
                        codError = String.IsNullOrEmpty(db.GetParameterValue(cmd, "P_CODERRO").ToString()) ? 0 : Int64.Parse(db.GetParameterValue(cmd, "P_CODERRO").ToString());
                        mensagemErro = db.GetParameterValue(cmd, "P_TXTERRO").ToString();
                    }
                    else
                    {
                        throw new DALCException("Não foi possivel Inserir");
                    }

                    if (codError != 0)
                    {
                        throw new DALCException(codError, mensagemErro);
                    }

                }

                Logger.LogInfo("Produto Inserido COD SAP " + entity.Material + ".");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }

        }

        /// <summary>
        /// Método responsável por obter um valor de caracteristica de um material pelo seu nome.
        /// </summary>
        /// <param name="nomeCaracteristica">Nome da característica.</param>
        /// <param name="fornecedor">Objeto material.</param>
        /// <returns>Valor em texto da característica.</returns>
        /// <remarks>oliveira@ - CiT (25/09/2012)</remarks>
        public static string GetValorCaracteristica(string nomeCaracteristica, dtpMM_Material material)
        {
            string retorno = string.Empty;

            if (material.Caracteristicas != null)
            {
                retorno = (from c in material.Caracteristicas
                           where c != null &&
                                 c.Material != null &&
                                 c.Nome != null &&
                                 c.Valor != null &&
                                 c.Nome.Equals(nomeCaracteristica) &&
                                 c.Material.Equals(material.Material)
                           select c.Valor.Trim()).FirstOrDefault();
                if (retorno == null)
                {
                    retorno = string.Empty;
                }

            }
            return retorno;
        }

        /// <summary>
        /// Método responsável por carregar os parâmetros das procedures de atualização/inserção de produtos.
        /// </summary>
        /// <param name="db1">Banco de dados</param>
        /// <param name="cmd">Comando</param>
        /// <param name="entity">Entidade</param>
        /// <remarks>oliveira@ - CiT (25/09/2012)</remarks>
        private static void LoadParametrosComuns(Database db, DbCommand cmd, dtpMM_Material entity)
        {
            string Grupo = string.Empty;

            //cod_dpk
            db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, Convert.ToInt64(entity.Material));

            //COD_FABRICA
            db.AddInParameter(cmd, "P_COD_FABRICA", DbType.String, entity.Registros[0].MaterialFornec);

            //COD_FABRICA_EDI
            db.AddInParameter(cmd, "P_COD_FABRICA_EDI", DbType.String, entity.Registros[0].MaterialFornec);

            //Desc_Item
            db.AddInParameter(cmd, "P_DESC_ITEM", DbType.String, entity.DenomMaterial);

            //cod_linha,
            db.AddInParameter(cmd, "P_COD_LINHA", DbType.Int64, Convert.ToInt64(GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_LINHA, entity)));

            //cod_grupo,
            Grupo = entity.GrpMercadorias;
            db.AddInParameter(cmd, "P_COD_GRUPO", DbType.Int64, Convert.ToInt64(Grupo.Substring(0, 2)));

            //cod_subgrupo,
            db.AddInParameter(cmd, "P_COD_SUBGRUPO", DbType.Int64, Convert.ToInt64(Grupo.Substring(Grupo.Length - 2, 2)));

            //dt_cadastramento
            db.AddInParameter(cmd, "P_DT_CADASTRAMENTO", DbType.DateTime, Convert.ToDateTime(entity.DtCriacao));

            //mascarado
            db.AddInParameter(cmd, "P_MASCARADO", DbType.String, GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_CODIGO_MASCARADO, entity));

            //class_fiscal
            db.AddInParameter(cmd, "P_CLASS_FISCAL", DbType.Int64, Convert.ToInt64(entity.Centro[0].Ncm));

            //SETOR_ATIV
            //db.AddInParameter(cmd, "P_SETOR_ATIV", DbType.String, entity.areaVendas[0].SetorAtiv);

            //peso,
            db.AddInParameter(cmd, "P_PESO", DbType.Double, Convert.ToDouble(entity.PesoBruto));

            //volume,
            db.AddInParameter(cmd, "P_VOLUME", DbType.Double, Convert.ToDouble(entity.Volume));

            //cod_unidade,
            db.AddInParameter(cmd, "P_COD_UNIDADE", DbType.String, entity.UndMedVenda);

            //cod_tributacao,
            db.AddInParameter(cmd, "P_COD_TRIBUTACAO", DbType.Int16, Convert.ToInt16(GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_TRIB, entity)));

            //cod_tributacao_ipi,
            db.AddInParameter(cmd, "P_COD_TRIBUTACAO_IPI", DbType.Int16, Convert.ToInt16(GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_COD_TRIB_IPI, entity)));

            //pc_ipi,
            db.AddInParameter(cmd, "P_PC_IPI", DbType.Decimal, Convert.ToDecimal(GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_PC_IPI, entity)));
            //db.AddInParameter(cmd, "P_PC_IPI", DbType.Decimal, Convert.ToDecimal(GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_PC_IPI, entity), new System.Globalization.CultureInfo("en-US")));

            //(de/para) COD_FORNECEDOR -> P_COD_SAP_FORNECEDOR
            db.AddInParameter(cmd, "P_COD_SAP_FORNECEDOR", DbType.String, entity.Registros[0].CodFornecedor);

            //cod_procedencia,
            db.AddInParameter(cmd, "P_COD_PROCEDENCIA", DbType.Int64, Convert.ToInt64(entity.OrigMaterial));

            //cod_dpk_ant,
            string novoItemSub = GetValorCaracteristica(Parametros.CARACTERISTICA_MATERIAL_NOVO_ITEM_SUBSTITUIDO, entity);
            if (string.IsNullOrEmpty(novoItemSub))
            {
                db.AddInParameter(cmd, "P_COD_DPK_ANT", DbType.Int64, null);
            }
            else
            {
                db.AddInParameter(cmd, "P_COD_DPK_ANT", DbType.Int64, Convert.ToInt64(novoItemSub));
            }

            //qtd_minforn,
            db.AddInParameter(cmd, "P_QTD_MINFORN", DbType.Decimal, Convert.ToDecimal(entity.Registros[0].QtdMin));

            //qtd_minvda
            db.AddInParameter(cmd, "P_QTD_MINVDA", DbType.Double, Convert.ToDouble(entity.areaVendas[0].QtdFornMin));

            db.AddInParameter(cmd, "P_SETOR_ATIV", DbType.String, entity.areaVendas[0].SetorAtiv);


            //parametros de saida
            db.AddOutParameter(cmd, "P_CODERRO", DbType.Int64, 8);
            db.AddOutParameter(cmd, "P_TXTERRO", DbType.String, 100);
        }

    }
}
