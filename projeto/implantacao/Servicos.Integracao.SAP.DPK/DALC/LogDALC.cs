﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DALC
{
    /// <summary>
    /// Classe responsável por fazer o acesso a banco de dados para classe LOGBE.
    /// </summary>
    public static class LogDALC
    {
        /// <summary>
        /// Método responsável por inserir um Log.
        /// </summary>
        /// <param name="entity">Objeto log preenchido.</param>
        /// <remarks>vxavier - CiT (20/09/2012)</remarks>
        public static void InserirLog(LogDetalheBE log, long idArquivoXml)
        {
            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_LOG))
            {
                db.AddInParameter(cmd, "P_TIPO_ENTIDADE", DbType.Int16, Convert.ToInt16(log.TIPO_ENTIDADE));
                if (log.DESCRICAO.Length > 300)
                {
                    log.DESCRICAO = log.DESCRICAO.Substring(0, 300);
                }
                db.AddInParameter(cmd, "P_DESCRICAO", DbType.String, log.DESCRICAO);
                db.AddInParameter(cmd, "P_STATUS", DbType.Int16, Convert.ToInt16(log.STATUS));
                db.AddInParameter(cmd, "P_COD_ERRO", DbType.Int16, Convert.ToInt16(log.COD_ERRO));
                db.AddInParameter(cmd, "P_ID_LOG_ARQUIVO", DbType.Int64, idArquivoXml);

                db.ExecuteNonQuery(cmd);
            }
        }

        /// <summary>
        /// Método responsável por inserir um Arquivo XML.
        /// </summary>
        /// <param name="entity">Objeto log preenchido.</param>
        /// <remarks>vxavier - CiT (21/09/2012)</remarks>
        public static long InserirArquivoXml(ArquivoBE arq)
        {
            long idArquivoXml = 0;

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_ARQUIVO))
            {

                db.AddInParameter(cmd, "P_NOME_ARQUIVO", DbType.String, arq.nomeArquivo);

                // Insere o XML CLOB
                OracleCommand oracmd = new OracleCommand();
                OracleParameter op = oracmd.CreateParameter();
                op.OracleType = OracleType.Clob;
                op.Direction = ParameterDirection.Input;
                if (arq.xml != null)
                {
                    op.Value = arq.xml;
                }
                else
                {
                    op.Value = string.Empty;
                }
                op.ParameterName = "P_ARQ_XML";
                cmd.Parameters.Add(op);

                db.AddOutParameter(cmd, "P_ID_ARQUIVO", DbType.Int64, 8);

                db.ExecuteNonQuery(cmd);

                idArquivoXml = Convert.ToInt64(db.GetParameterValue(cmd, "P_ID_ARQUIVO"));
            }

            return idArquivoXml;
        }
    }
}
