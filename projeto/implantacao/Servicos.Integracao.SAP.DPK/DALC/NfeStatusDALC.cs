﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using UtilGeralDPA;

namespace DALC
{
    public class NfeStatusDALC
    {

        #region Singleton
        private static NfeStatusDALC instance;
        public static NfeStatusDALC Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NfeStatusDALC();
                }
                return instance;
            }
        }
        #endregion

        /// <summary>
        /// Método responsável por atualizar o status na tabela Controle Pedido SAP
        /// </summary>
        /// <param name="idControlePedido">Id do Controle do Pedido SAP</param>
        /// <remarks>
        ///  vxavier@ - CiT (15/10/2012)
        /// </remarks>
        public void AtualizarControlePedidoStatus(string idControlePedido, string status)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_CONTROLE_PEDIDO_STATUS))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_STATUS", DbType.String, status);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }


        public void EfetivarPedido(string idControlePedido, string chaveNFE, int codLoja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_EFETIVAR_PEDIDO))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_CHAVE_NFE", DbType.String, chaveNFE);
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, codLoja);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método que consulta os pedidos expurgados
        /// </summary>
        /// <param name="codLoja">Código Loja</param>
        /// <returns>lista de pedidos expurgados</returns>
        /// <remarks>
        /// vxavier CiT - 15/10/12 - [ISA - 826]
        /// </remarks>
        public List<PedidoCancelamentoDH> ConsultarPedidosExpurgados(int codLoja)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<PedidoCancelamentoDH> listaPedidos = new List<PedidoCancelamentoDH>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PEDIDOS_EXPURGADOS))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int16, codLoja);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            PedidoCancelamentoDH pedido = new PedidoCancelamentoDH();
                            pedido.ID_CONTROLE_PEDIDO_SAP = reader["ID_CONTROLE_PEDIDO"].ToString();
                            pedido.COD_LOJA = Convert.ToInt16(reader["COD_LOJA"]);
                            pedido.NUM_PEDIDO = Convert.ToInt32(reader["NUM_PEDIDO"]);
                            pedido.SEQ_PEDIDO = Convert.ToInt32(reader["SEQ_PEDIDO"]);
                            pedido.STATUS = reader["STATUS"].ToString();
                            pedido.CLIENTE_COD_SAP = reader["COD_SAP_CLIENTE"] is DBNull ? 0 : Convert.ToInt64(reader["COD_SAP_CLIENTE"]);
                            pedido.LOJA_COD_SAP = reader["COD_SAP_LOJA"].ToString();
                            listaPedidos.Add(pedido);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaPedidos;
        }

        /// <summary>
        /// carlosj CiT (15/10/2012) - Esse método irá atualizar um determinado pedido na tabela PEDNOTA_VENDA.
        /// </summary>
        /// <param name="valorIPI">Somatória de Item[1..n]/ ValorIPI</param>
        /// <param name="valorContabil">Somatória de Item[1..n]/SubTotal</param>
        /// <param name="numNotaFE">NumeroNfe</param>
        /// <param name="valorBaseMajorada">Somatória de Item[1..n]/BaseMajorada</param>
        /// <param name="valorIcmRetido">Somatória de Item[1..n]/ValorRetido</param>
        /// <param name="idControlePedido">chave da tabela SAP.CONTROLE_PEDIDO_SAP</param>
        public void AtualizarCabecalho(decimal valorIPI, decimal valorContabil, string numNotaFE, decimal valorBaseMajorada, decimal valorIcmRetido, string centro, string idControlePedido)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_PEDNOTA_VENDA))
            {
                try
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, idControlePedido);
                    db.AddInParameter(cmd, "P_NUM_NOTA", DbType.String, numNotaFE);
                    db.AddInParameter(cmd, "P_VL_IPI", DbType.Decimal, valorIPI);
                    db.AddInParameter(cmd, "P_VL_CONTABIL", DbType.Decimal, valorContabil);
                    db.AddInParameter(cmd, "P_VL_BASEMAJ", DbType.Decimal, valorBaseMajorada);
                    db.AddInParameter(cmd, "P_VL_ICMRETIDO", DbType.Decimal, valorIcmRetido);

                    db.ExecuteNonQuery(cmd);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    throw new DALCException(ex);
                }
                finally
                {
                    Logger.LogFinalMetodo(dataInicio);
                }

            }

            Logger.LogInfo("Atualizado cabeçalho da NFE " + numNotaFE);
        }

        /// <summary>
        /// ecampos CiT (15/10/2012) - Esse método irá atualizar um determinado item na tabela ITPEDNOTA_VENDA.
        /// </summary>
        /// <param name="id_ControlePedido">chave combinada</param>
        /// <param name="NumNfe">Número da Nota Fiscal</param>
        /// <param name="numItem">Posição do item na Nota</param>
        /// <param name="icmRetido">Valor do Item/ValorRetido</param>
        /// <param name="codDPK">Codigo DPK = Material</param>
        public void AtualizarItem(string id_ControlePedido, string NumNfe, int numItem, decimal icmRetido, string codDPK, int qtdAtendida)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_ITPEDNOTA_VENDA))
                {
                    db.AddInParameter(cmd, "C_ID_CONTROLE_PEDIDO", DbType.String, id_ControlePedido);
                    db.AddInParameter(cmd, "I_NUM_NOTA", DbType.Int32, Convert.ToInt32(NumNfe));
                    db.AddInParameter(cmd, "I_NUM_ITEM_NOTA", DbType.Int16, numItem);
                    db.AddInParameter(cmd, "I_VL_ICMRETIDO", DbType.Decimal, icmRetido);
                    db.AddInParameter(cmd, "I_COD_DPK", DbType.Int32, Convert.ToInt32(codDPK));
                    db.AddInParameter(cmd, "I_QTD_ATENDIDA", DbType.Int32, Convert.ToInt32(qtdAtendida));

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// ecampos CiT (15/10/2012) - Esse método irá realizar a inserção ou atualização da tabela FISCAL_SAIDA.
        /// </summary>
        public void InsFiscalSaida(string id_ControlePedido, string codDPK, decimal vlLiquido, decimal icmRetido, decimal vlIPI, decimal despesas)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_FISCAL_SAIDA))
                {
                    db.AddInParameter(cmd, "P_ID_CONTROLE_PEDIDO", DbType.String, id_ControlePedido);
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, Convert.ToInt64(codDPK));
                    db.AddInParameter(cmd, "P_VL_LIQUIDO", DbType.Decimal, vlLiquido);
                    db.AddInParameter(cmd, "P_ICMRETIDO", DbType.Decimal, icmRetido);
                    db.AddInParameter(cmd, "P_VL_IPI", DbType.Decimal, vlIPI);
                    db.AddInParameter(cmd, "P_VL_DESPESAS", DbType.Decimal, despesas);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }
    }
}
