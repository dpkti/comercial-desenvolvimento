VERSION 5.00
Begin VB.Form frmTran 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PROCESSA ARQUIVO BRINDE - GERA PEDIDO"
   ClientHeight    =   3105
   ClientLeft      =   1095
   ClientTop       =   1440
   ClientWidth     =   9105
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3105
   ScaleWidth      =   9105
   Begin VB.CommandButton Command1 
      Caption         =   "Gera Pedidos"
      Height          =   735
      Left            =   7560
      TabIndex        =   2
      Top             =   1080
      Width           =   1095
   End
   Begin VB.ListBox lstPedido 
      ForeColor       =   &H00800000&
      Height          =   1035
      ItemData        =   "frmTran.frx":0000
      Left            =   1320
      List            =   "frmTran.frx":0002
      TabIndex        =   0
      Top             =   840
      Visible         =   0   'False
      Width           =   5655
   End
   Begin VB.Label lblStatus 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   1
      Top             =   2400
      Width           =   615
   End
End
Attribute VB_Name = "frmTran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
 Call Verifica_Arquivo
End Sub

Private Sub Form_Load()
  
  On Error GoTo Trata_Erro
  
  
  If App.PrevInstance Then
    MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
    End
  End If
  'producao
   strDiretorio = "h:\oracle\dados\32bits\arquivo\"
   strDiretorio_Salva = "h:\oracle\dados\32bits\arquivo\salva\"
  'desenv
  'strDiretorio = "f:\oracle\dados\32bits\arquivo\"
  'strDiretorio_Salva = "f:\oracle\dados\32bits\arquivo\salva\"
  
  
  Set orasession = CreateObject("oracleinprocserver.xorasession")
  Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA840/PROD", 0&)
  'Set oradatabase = orasession.OpenDatabase("desenv", "PRODUCAO/DES", 0&)
  
  Set OraParameters = oradatabase.Parameters
  OraParameters.Remove "vCursor"
  OraParameters.Add "vCursor", 0, 2
  OraParameters("vCursor").serverType = ORATYPE_CURSOR
  OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
  OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
  
  OraParameters.Remove "vErro"
  OraParameters.Add "vErro", 0, 2
  OraParameters("vErro").serverType = ORATYPE_NUMBER

  
  
 Exit Sub
Trata_Erro:
   Call Process_Line_Errors(SQL)
End Sub



Public Sub Verifica_Arquivo()
  Dim TMP_LINHA As String
  Dim strChar As String
  Dim strLinha As String
  Dim strTp_Linha As String * 1
  Dim SQL1 As String
  Dim dDigitacao As Date
  Dim dProxSeg As Date
  Dim lngLoja As Long
  Dim lngCod_Cliente As Long
  Dim lngCod_DPK As Long
  Dim lngQtde As Long
  Dim dblPreco As Double
  
  On Error GoTo Trata_Erro
  
  lblStatus = "VERIFICANDO ARQUIVO DE BRINDES"
  Screen.MousePointer = 11
  lstPedido.Visible = False
  DoEvents
 'montar SQL
  V_SQL = "Begin producao.pck_vda840.pr_deposito(:vCursor,:vErro);END;"

  
  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
  
  If Dir(strDiretorio & "cesta" & Format(ss!cod_loja, "00") & ".TXT") = "" Then ' N�O EXISTE TRANSFERENCIA
   lblStatus = "N�O H� ARQUIVO: " & strDiretorio & "cesta" & Format(ss!cod_loja, "00") & ".TXT" & " PARA PROCESSAR BRINDES"
   DoEvents
   Exit Sub
  Else
    Do While Dir(strDiretorio & "*.TXT") <> ""
      strArquivo = strDiretorio & Dir(strDiretorio & "*.TXT")
      
      lblStatus = "PROCESSANDO ARQUIVO DE BRINDES: " & strArquivo
      DoEvents
      lstPedido.Visible = True
      lngCod_Cliente = 0
      Open strArquivo For Input As #1
        Line Input #1, strLinha
        Do While Not EOF(1)
inicio:
               If lngCod_Cliente <> 0 Then
                 Line Input #1, strLinha
               End If
               lngLoja = Mid(strLinha, 1, 2)
               lngCod_Cliente = Mid(strLinha, 3, 6)
               'eoliveira CiT 24/10/2012
               'altera��o do C�digo DPK
               lngCod_DPK = Mid(strLinha, 9, 18)
               lngQtde = Mid(strLinha, 27, 6)
               dblPreco = Mid(strLinha, 33, 13)
               
               'verifica cliente
               OraParameters.Remove "cliente"
               OraParameters.Add "cliente", lngCod_Cliente, 1
               V_SQL = "Begin producao.pck_vda840.pr_cliente(:vCursor,:cliente,:vErro);END;"

               oradatabase.ExecuteSQL V_SQL
  
               Set ss = oradatabase.Parameters("vCursor").Value
  
               If oradatabase.Parameters("vErro").Value <> 0 Then
                 Line Input #1, strLinha
                 lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " N�O CADASTRADO, PEDIDO N�O GERADO"
                 GoTo inicio
               End If
               If ss!situacao <> 0 Then
                 Line Input #1, strLinha
                 lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " DESATIVADO, PEDIDO N�O GERADO"
                 GoTo inicio
               End If
              
               'verifica dpk
               OraParameters.Remove "loja"
               OraParameters.Add "loja", lngLoja, 1
               OraParameters.Remove "dpk"
               OraParameters.Add "dpk", lngCod_DPK, 1
               V_SQL = "Begin producao.pck_vda840.pr_item(:vCursor,:loja,:dpk,:vErro);END;"

               oradatabase.ExecuteSQL V_SQL
  
               Set ss = oradatabase.Parameters("vCursor").Value
  
               If oradatabase.Parameters("vErro").Value <> 0 Then
                 Line Input #1, strLinha
                 lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ITEM N�O ENCONTRADO"
                 GoTo inicio
               End If
               
               If ss!situacao <> 0 Then
                 Line Input #1, strLinha
                 lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ITEM DESATIVADO"
                 GoTo inicio
               End If
               lblStatus = "PROCESSANDO O PEDIDO PARA O CLIENTE: " & lngCod_Cliente
               
               'executa procedure
               OraParameters.Remove "loja"
               OraParameters.Add "loja", lngLoja, 1
               OraParameters.Remove "cliente"
               OraParameters.Add "cliente", lngCod_Cliente, 1
               OraParameters.Remove "dpk"
               OraParameters.Add "dpk", lngCod_DPK, 1
               OraParameters.Remove "qtd"
               OraParameters.Add "qtd", lngQtde, 1
               OraParameters.Remove "preco"
               OraParameters.Add "preco", dblPreco, 1
                OraParameters.Remove "pedido"
               OraParameters.Add "pedido", 0, 2
               
               V_SQL = "Begin producao.pck_vda840.pr_brindes(:loja,:cliente,:dpk,:qtd,:preco, :pedido,:vErro);END;"

               oradatabase.ExecuteSQL V_SQL
               If oradatabase.Parameters("vErro").Value <> 0 Then
                 lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO N�O GERADO, ERRO: " & oradatabase.Parameters("vErro").Value
                 GoTo inicio
               End If
               
               
               lblStatus = "PROCESSADO O PEDIDO : " & oradatabase.Parameters("pedido").Value & " PARA O CLIENTE: " & lngCod_Cliente
               
               lstPedido.AddItem "CLIENTE: " & lngCod_Cliente & " PEDIDO GERADO: " & oradatabase.Parameters("pedido").Value
               DoEvents
               
               
         Loop
         
         lblStatus = ""
         Call Salva_Arquivo(strArquivo)
    Loop
    
  End If
  
  MsgBox "ARQUIVO PROCESSADO COM SUCESSO", vbInformation, "Aten��o"
  Screen.MousePointer = 0
  Exit Sub
Trata_Erro:
  If Err = 62 Then
    Resume Next
  End If
  
End Sub

Private Sub List1_Click()

End Sub

Public Sub Salva_Arquivo(strArquivo As String)
  Close #1
  FileCopy strArquivo, strDiretorio_Salva & Right(strArquivo, InStr(strArquivo, "\arquivo") - 12)
  
  Kill strArquivo
  
End Sub


Private Sub Timer1_Timer()

End Sub


