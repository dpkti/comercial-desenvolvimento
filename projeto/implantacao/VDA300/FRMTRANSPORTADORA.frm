VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmTransportadora 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transportadora"
   ClientHeight    =   2145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8880
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2145
   ScaleWidth      =   8880
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   7800
      TabIndex        =   7
      Top             =   1560
      Width           =   975
   End
   Begin VB.TextBox txtMsgNota 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1320
      MaxLength       =   60
      TabIndex        =   5
      Top             =   1080
      Width           =   7455
   End
   Begin Threed.SSFrame frmeTransportadora 
      Height          =   975
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8775
      _Version        =   65536
      _ExtentX        =   15478
      _ExtentY        =   1720
      _StockProps     =   14
      Caption         =   "Transportadora"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton optCIF 
         Caption         =   "CIF"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   7100
         TabIndex        =   4
         Top             =   480
         Width           =   615
      End
      Begin VB.OptionButton optFOBD 
         Caption         =   "FOBD"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   7850
         TabIndex        =   3
         Top             =   480
         Width           =   855
      End
      Begin VB.OptionButton optFOB 
         Caption         =   "FOB"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   6250
         TabIndex        =   2
         Top             =   480
         Width           =   735
      End
      Begin VB.ComboBox cmbTransportadora 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   6015
      End
   End
   Begin VB.Label lblMsgNota 
      AutoSize        =   -1  'True
      Caption         =   "Msg Nota"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Width           =   825
   End
End
Attribute VB_Name = "frmTransportadora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmTransportadora
' Author    : c.samuel.oliveira
' Date      : 10/07/2018
' Purpose   : TI-5912
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmTransportadora
' Author    : c.alexandre.ferreira
' Date      : 06/10/16
' Purpose   : TI-4839 - retirar DIGITA.mdb
'---------------------------------------------------------------------------------------


'PSERGIO 11/09/2015 - TI-3023 - Frete DPK
Private Sub cmdOk_Click()
    
    On Error GoTo TrataErro
    
    If cmbTransportadora.Text = "" Then
        MsgBox "Favor entrar com a transportadora", vbInformation
        cmbTransportadora.SetFocus
        Exit Sub
    End If
    
    'SQL = " update pedido set COD_TRANSP = " & Mid(cmbTransportadora.Text, 1, 4) & "," 'TI-4839
    SQL = "Begin"
    SQL = SQL & " update VENDAS.PEDIDO_OUTSAI set COD_TRANSP = " & Mid(cmbTransportadora.Text, 1, 4) & ","
    If optCIF.Value Then
        SQL = SQL & " FRETE_PAGO = 'C',"
    ElseIf optFOB.Value Then
        SQL = SQL & " FRETE_PAGO = 'F',"
    ElseIf optFOBD.Value Then
        SQL = SQL & " FRETE_PAGO = 'D',"
    End If
    SQL = SQL & " MENS_NOTA = '" & Mid(txtMsgNota.Text, 1, 60) & "'"
    SQL = SQL & " where num_pendente = " & lngNUM_PEDIDO
    SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
    SQL = SQL & " and cod_loja = " & lngCod_Loja & ";"
    'TI-4839  - in�cio
    SQL = SQL & " COMMIT;"
    SQL = SQL & "EXCEPTION"
    SQL = SQL & " WHEN OTHERS THEN"
    SQL = SQL & " ROLLBACK;"
    SQL = SQL & " :cod_errora := SQLCODE;"
    SQL = SQL & " :txt_errora := SQLERRM;"
    SQL = SQL & "END;"
    oradatabase.Parameters.Remove "cod_errora"
    oradatabase.Parameters.Add "cod_errora", 0, 2
    oradatabase.Parameters.Remove "txt_errora"
    oradatabase.Parameters.Add "txt_errora", "", 2
    'dbAccess.Execute SQL, dbFailOnError
    oradatabase.ExecuteSQL SQL
    'TI-4839 Fim

    FreeLocks
     
    Call MergeItinerarioPedido(cmbTransportadora.ItemData(cmbTransportadora.ListIndex))
    
    Unload Me
    
    Exit Sub

TrataErro:

    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    Else
        Call Process_Line_Errors(SQL)
    End If
    
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub Form_Load()
    
    Dim ss As Object
    Dim mensagemNota As String
    Dim CodTranspFrete As Long
    
    Call CalculaTotalNota
    
    Call PrrencheListasTransportadoras
    
    SQL = "SELECT P.COD_TRANSP, P.FRETE_PAGO, P.MENS_NOTA"
    'SQL = SQL & " FROM PEDIDO P" '    TI-4839
    SQL = SQL & " FROM VENDAS.PEDIDO_OUTSAI P"
    SQL = SQL & " Where P.NUM_PENDENTE = " & lngNUM_PEDIDO
    SQL = SQL & " AND P.SEQ_PEDIDO = " & lngSEQ_PEDIDO
    SQL = SQL & " AND P.COD_LOJA = " & lngCod_Loja
    'Set ss = dbAccess.CreateSnapshot(SQL)        '    TI-4839
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    FreeLocks
    If Not ss.EOF Then
        If Not IsNull(ss!MENS_NOTA) Then
            mensagemNota = ss!MENS_NOTA
        Else
            mensagemNota = ""
        End If
        If ((Not IsNull(ss!COD_TRANSP)) And (Not IsNull(ss!FRETE_PAGO))) Then
            CodTranspFrete = CLng(ss!COD_TRANSP)
            If ss!FRETE_PAGO = "F" Then
                tipoFrete = "FOB"
                optFOB.Value = True
                optCIF.Value = False
                optFOBD.Value = False
            ElseIf ss!FRETE_PAGO = "C" Then
                tipoFrete = "CIF"
                optFOB.Value = False
                optCIF.Value = True
                optFOBD.Value = False
            ElseIf ss!FRETE_PAGO = "D" Then
                tipoFrete = "CIFD"
                optFOB.Value = False
                optCIF.Value = False
                optFOBD.Value = True
            End If
        Else
            CodTranspFrete = 0
            tipoFrete = ""
        End If
    Else
        mensagemNota = ""
    End If
    
    If CodTranspFrete <> 0 And tipoFrete <> "" Then
        Call CarregaComboTransportadora(tipoFrete, Format(CodTranspFrete, "0000"))
    End If
    
    ss.Close
    
    txtMsgNota.Text = mensagemNota
    
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub cmbTransportadora_Click()
    Dim codTransp As String
    Dim ehRetira As Boolean
    
    If tipoFrete <> "FOB" Then
        codTransp = Mid(cmbTransportadora.Text, 1, 4)
        ehRetira = VerificaTransportadoraRetira(codTransp)
        If ehRetira Then
            tipoFrete = "FOB"
            Call CarregaComboTransportadora(tipoFrete, codTransp)
            optFOB.Value = True
            optCIF.Value = False
            optFOBD.Value = False
            Call MsgNota
        End If
    End If
    
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub CarregaComboTransportadora(tipoFrete As String, Optional codTransp As String = Empty)

    On Error GoTo Trata_Erro

    Dim i As Long
    
    'TI-5912
    If BuscarPlano Then
        cmbTransportadora.Clear
        For i = 1 To UBound(listaTransportadorasPLANO)
            cmbTransportadora.AddItem listaTransportadorasPLANO(i).codTransp & " - " & listaTransportadorasPLANO(i).Nome
        Next i
        If (Not IsEmpty(codTransp)) Then
            For i = 0 To cmbTransportadora.ListCount - 1
                If Mid(cmbTransportadora.List(i), 1, 4) = codTransp Then
                    cmbTransportadora.ListIndex = i
                End If
            Next i
        End If
        
        Exit Sub
    End If
    'FIM TI-5912
    
    If tipoFrete = "CIFD" Then
        cmbTransportadora.Clear
        For i = 1 To UBound(listaTransportadorasCIFD)
            If listaTransportadorasCIFD(i).FLRetira Then
                cmbTransportadora.AddItem listaTransportadorasCIFD(i).codTransp & " - " & listaTransportadorasCIFD(i).Nome
            Else
                cmbTransportadora.AddItem listaTransportadorasCIFD(i).codTransp & " - " & Mid(listaTransportadorasCIFD(i).Nome, 1, 25) & " " _
                & listaTransportadorasCIFD(i).Prazo & "D Frete: " & CDbl(listaTransportadorasCIFD(i).VlFrete) & " CIF: " & CDbl(listaTransportadorasCIFD(i).FatMin)
                cmbTransportadora.ItemData(cmbTransportadora.NewIndex) = listaTransportadorasCIFD(i).CodItTransp
            End If
        Next i
        If (Not IsEmpty(codTransp)) Then
            For i = 0 To cmbTransportadora.ListCount - 1
                If Mid(cmbTransportadora.List(i), 1, 4) = codTransp Then
                    cmbTransportadora.ListIndex = i
                End If
            Next i
        End If
    End If
    
    If tipoFrete = "CIF" Then
        cmbTransportadora.Clear
        For i = 1 To UBound(listaTransportadorasCIF)
            cmbTransportadora.AddItem listaTransportadorasCIF(i).codTransp & " - " & listaTransportadorasCIF(i).Nome
            cmbTransportadora.ItemData(cmbTransportadora.NewIndex) = 0
        Next i
        If (Not IsEmpty(codTransp)) Then
            For i = 0 To cmbTransportadora.ListCount - 1
                If Mid(cmbTransportadora.List(i), 1, 4) = codTransp Then
                    cmbTransportadora.ListIndex = i
                End If
            Next i
        End If
    End If
    
    If tipoFrete = "FOB" Then
        cmbTransportadora.Clear
        For i = 1 To UBound(listaTransportadorasFOB)
            cmbTransportadora.AddItem listaTransportadorasFOB(i).codTransp & " - " & listaTransportadorasFOB(i).Nome
        Next i
        If (Not IsEmpty(codTransp)) Then
            For i = 0 To cmbTransportadora.ListCount - 1
                If Mid(cmbTransportadora.List(i), 1, 4) = codTransp Then
                    cmbTransportadora.ListIndex = i
                End If
            Next i
        End If
    End If
    
    Exit Sub
    
Trata_Erro:
    MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
    
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub optCIF_Click()
    If tipoFrete <> "CIF" Then
        tipoFrete = "CIF"
        Call CarregaComboTransportadora("CIF")
        'mensagem da nota
        Call MsgNota
    End If
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub optFOB_Click()
    If tipoFrete <> "FOB" Then
        tipoFrete = "FOB"
        Call CarregaComboTransportadora(tipoFrete)
        'mensagem da nota
        Call MsgNota
    End If
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub optFOBD_Click()
    If tipoFrete <> "CIFD" Then
        tipoFrete = "CIFD"
        Call CarregaComboTransportadora("CIFD")
        'mensagem da nota
        Call MsgNota
    End If
End Sub

'PSERGIO CIT - 11/09/2015 - TI-3023 - Frete DPK
Private Sub MsgNota()
          
          On Error GoTo Trata_Erro

          Dim bTam As Byte
          bTam = Len(txtMsgNota.Text)

          If bTam <= 5 Then
              If optCIF.Value Then
                  txtMsgNota.Text = "CIF. "
              ElseIf optFOB.Value Then
                  txtMsgNota.Text = "FOB. "
              ElseIf optFOBD.Value Then
                  txtMsgNota.Text = "CIFD."
              End If
          Else
              If optCIF.Value Then
                  txtMsgNota.Text = "CIF. " & Mid$(txtMsgNota.Text, 6)
              ElseIf optFOB.Value Then
                  txtMsgNota.Text = "FOB. " & Mid$(txtMsgNota.Text, 6)
              ElseIf optFOBD.Value Then
                  txtMsgNota.Text = "CIFD." & Mid$(txtMsgNota.Text, 6)
              End If
          End If

Trata_Erro:
          If Err.Number <> 0 Then
               MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
          End If

End Sub
