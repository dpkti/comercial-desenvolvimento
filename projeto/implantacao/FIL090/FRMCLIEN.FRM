VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmCliente 
   BorderStyle     =   0  'None
   Caption         =   "Dados Cadastrais"
   ClientHeight    =   5505
   ClientLeft      =   1125
   ClientTop       =   1665
   ClientWidth     =   9465
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5505
   ScaleWidth      =   9465
   ShowInTaskbar   =   0   'False
   Begin Threed.SSFrame SSFrame1 
      Height          =   600
      Left            =   90
      TabIndex        =   3
      Top             =   45
      Width           =   9330
      _Version        =   65536
      _ExtentX        =   16457
      _ExtentY        =   1058
      _StockProps     =   14
      ForeColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShadowStyle     =   1
      Begin VB.TextBox txtCgc 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   7380
         MaxLength       =   14
         TabIndex        =   2
         Top             =   180
         Width           =   1815
      End
      Begin VB.TextBox txtCodigo 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   945
         MaxLength       =   6
         TabIndex        =   0
         Top             =   180
         Width           =   735
      End
      Begin VB.TextBox txtNome 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1755
         TabIndex        =   1
         Top             =   180
         Width           =   4425
      End
      Begin VB.Label lblCliente 
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   135
         TabIndex        =   5
         Top             =   270
         Width           =   855
      End
      Begin VB.Label lblCgc 
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6885
         TabIndex        =   4
         Top             =   270
         Width           =   495
      End
   End
   Begin Threed.SSFrame SSFrame2 
      Height          =   1575
      Left            =   90
      TabIndex        =   40
      Top             =   3915
      Visible         =   0   'False
      Width           =   9240
      _Version        =   65536
      _ExtentX        =   16298
      _ExtentY        =   2778
      _StockProps     =   14
      Caption         =   "Caracter�stica"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblMsg 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4230
         TabIndex        =   56
         Top             =   1035
         Width           =   4815
      End
      Begin VB.Label Label3 
         Caption         =   "Msg.: "
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3690
         TabIndex        =   55
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblConceito 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7230
         TabIndex        =   54
         Top             =   675
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Conceito"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5910
         TabIndex        =   53
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblSituacao 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7230
         TabIndex        =   48
         Top             =   315
         Width           =   1815
      End
      Begin VB.Label lblSit 
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5910
         TabIndex        =   47
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblCategoria 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   46
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label lblCa 
         Caption         =   "Categoria"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   765
         Width           =   855
      End
      Begin VB.Label lblClass 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   44
         Top             =   1080
         Width           =   375
      End
      Begin VB.Label lblCl 
         Caption         =   "Classif."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1125
         Width           =   735
      End
      Begin VB.Label lblTpdpk 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   42
         Top             =   360
         Width           =   2895
      End
      Begin VB.Label lblTP1 
         Caption         =   "Tipo DPK"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   135
         TabIndex        =   41
         Top             =   405
         Width           =   855
      End
   End
   Begin MSGrid.Grid Grid1 
      Height          =   255
      Left            =   2160
      TabIndex        =   50
      Top             =   45
      Visible         =   0   'False
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   450
      _StockProps     =   77
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "FRMCLIEN.frx":0000
   End
   Begin Threed.SSCommand ssDupl 
      Height          =   375
      Left            =   4500
      TabIndex        =   49
      Top             =   3015
      Visible         =   0   'False
      Width           =   1575
      _Version        =   65536
      _ExtentX        =   2778
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "Saldo Duplicatas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label4 
      Caption         =   "Tipo Fiel"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   6705
      TabIndex        =   66
      Top             =   1800
      Visible         =   0   'False
      Width           =   825
   End
   Begin VB.Label lbl_Tipo_Fiel 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7605
      TabIndex        =   65
      Top             =   1755
      Visible         =   0   'False
      Width           =   1770
   End
   Begin VB.Label lbl_CodMensFiscal 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2205
      TabIndex        =   64
      Top             =   3555
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lbl_CodMens 
      Caption         =   "C�d. Mensagem Fiscal"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   135
      TabIndex        =   63
      Top             =   3600
      Visible         =   0   'False
      Width           =   2040
   End
   Begin VB.Label lblEmail 
      Caption         =   "E-mail"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   135
      TabIndex        =   62
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblEnd_Email 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   61
      Top             =   1755
      Visible         =   0   'False
      Width           =   5100
   End
   Begin VB.Label lblInsSuf 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   7560
      TabIndex        =   60
      Top             =   2475
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblIS 
      Caption         =   "Inscr. SUFRAMA:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   5940
      TabIndex        =   59
      Top             =   2565
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.Label lblLimCredito 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7560
      TabIndex        =   58
      Top             =   3510
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lbllm 
      Caption         =   "Limite Cr�dito"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   6300
      TabIndex        =   57
      Top             =   3600
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblDtcadastro 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1215
      TabIndex        =   52
      Top             =   3195
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDtcad 
      Caption         =   "Dt.Cadastro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   105
      TabIndex        =   51
      Top             =   3240
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblDvencer 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7560
      TabIndex        =   39
      Top             =   2970
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblDVencido 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   7560
      TabIndex        =   38
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblVcido 
      Caption         =   "Vencido"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   6300
      TabIndex        =   37
      Top             =   3330
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblV 
      Caption         =   "� Vencer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   6300
      TabIndex        =   36
      Top             =   3060
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblUltc 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1215
      TabIndex        =   35
      Top             =   2835
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblUc 
      Caption         =   "Ult.Compra"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   105
      TabIndex        =   34
      Top             =   2880
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblIe 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7605
      TabIndex        =   33
      Top             =   720
      Visible         =   0   'False
      Width           =   1785
   End
   Begin VB.Label lblIee 
      Caption         =   "I.E"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7245
      TabIndex        =   32
      Top             =   765
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Label lblDrdpk 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6795
      TabIndex        =   31
      Top             =   2115
      Visible         =   0   'False
      Width           =   2595
   End
   Begin VB.Label lblCrdpk 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6120
      TabIndex        =   30
      Top             =   2115
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblrep1 
      Caption         =   "Rep. DPK"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5175
      TabIndex        =   29
      Top             =   2160
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblDtransp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1575
      TabIndex        =   28
      Top             =   2475
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCtransp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   27
      Top             =   2475
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblDbco 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1575
      TabIndex        =   26
      Top             =   2115
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblCbco 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   25
      Top             =   2115
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblTra 
      Caption         =   "Transp."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   24
      Top             =   2520
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblbco 
      Caption         =   "Banco"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   23
      Top             =   2160
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblFax 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7605
      TabIndex        =   22
      Top             =   1395
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblfx 
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   7155
      TabIndex        =   21
      Top             =   1440
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label lblContato 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5220
      TabIndex        =   20
      Top             =   1395
      Visible         =   0   'False
      Width           =   1590
   End
   Begin VB.Label lblCont 
      Caption         =   "Contato"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   4455
      TabIndex        =   19
      Top             =   1440
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblFone2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3240
      TabIndex        =   18
      Top             =   1395
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDdd2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2700
      TabIndex        =   17
      Top             =   1395
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblFone1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1575
      TabIndex        =   16
      Top             =   1395
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDdd1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   15
      Top             =   1395
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblFone 
      Caption         =   "Fone"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   135
      TabIndex        =   14
      Top             =   1440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7605
      TabIndex        =   13
      Top             =   1035
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCpx 
      Caption         =   "Caixa Postal"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6435
      TabIndex        =   12
      Top             =   1080
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Label lblCep 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4635
      TabIndex        =   11
      Top             =   990
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblUf 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4140
      TabIndex        =   10
      Top             =   990
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label lblCidade 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   9
      Top             =   990
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label lblBairro 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4635
      TabIndex        =   8
      Top             =   720
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblEnd 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1035
      TabIndex        =   7
      Top             =   720
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.Label lblEnde 
      Caption         =   "Endereco"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   6
      Top             =   765
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "frmCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False




Private Sub FORM_Load()
Move 0, 0

End Sub

Private Sub Grid1_DblClick()
    
  Dim cod As Double
    
   Grid1.Col = 1
   cod = Grid1.Text
   Grid1.Visible = False
   SSFRAME1.Visible = True
     
   Call SELECTS(1, " a.cod_cliente= " & cod)
   
   Set accessdynaset = accessdatabase.CreateDynaset(sel1)
    
   If accessdynaset.EOF Then
     MsgBox "N�O LOCALIZOU COD:" & cod, 0, "ATEN��O"
   End If
   
   Call SELECTS(3, " a.cod_cliente= " & cod)
                 
   Set access3dynaset = accessdatabase.CreateDynaset(sel3)
         
   Call SELECTS(5, " a.cod_cliente= " & cod)
                 
   Set access5dynaset = accessdatabase.CreateDynaset(sel5)
         
   Call SELECTS(12, " a.cod_cliente= " & cod)
                 
   Set access12dynaset = accessdatabase.CreateDynaset(sel12)
  
    Call DADOS_TELA
  
  Exit Sub

  
End Sub



Private Sub ssDupl_Click()
 
   frmCliente.ssDupl.Enabled = False
  
   Call SELECTS(4, " a.cod_cliente= " & frmCliente.txtcodigo.Text & " and a.dt_vencimento >= b.dt_faturamento")
   
   Set accessdynaset = accessdatabase.CreateDynaset(sel4)
   
   If accessdynaset.EOF Then
      frmCliente.lblDvencer.Caption = 0
   Else
      frmCliente.lblDvencer.Caption = IIf(IsNull(accessdynaset.Fields("valor").Value), "", Format(accessdynaset.Fields("valor").Value, "###,###,##0.00"))
      vencer = IIf(IsNull(accessdynaset.Fields("valor").Value), 0, accessdynaset.Fields("valor").Value)
   End If
   
   Call SELECTS(4, " a.cod_cliente= " & frmCliente.txtcodigo.Text & " and a.dt_vencimento < b.dt_faturamento")
   
   Set accessdynaset = accessdatabase.CreateDynaset(sel4)
   
   If accessdynaset.EOF Then
      frmCliente.lblDVencido.Caption = 0
   Else
      frmCliente.lblDVencido.Caption = IIf(IsNull(accessdynaset.Fields("valor").Value), "", Format(accessdynaset.Fields("valor").Value, "###,###,##0.00"))
      vencido = IIf(IsNull(accessdynaset.Fields("valor").Value), 0, accessdynaset.Fields("valor").Value)
   End If
   
   
   Call SELECTS(13, frmCliente.txtcodigo.Text)
   
   Set accessdynaset = accessdatabase.CreateDynaset(sel13)
   
   saldo = accessdynaset.Fields("limite_credito").Value

   
   limite = Format(saldo, "###,###,##0.00")
   If limite < 0 Then
     frmCliente.lblLimCredito.Caption = 0
   Else
     frmCliente.lblLimCredito.Caption = Format(saldo, "###,###,##0.00")
   End If
   
   If vencido = 0 Then
     frmCliente.lblDVencido.ForeColor = &H800000
   Else
     frmCliente.lblDVencido.ForeColor = &HFF&
   End If
   
   limite = 0
   saldo = 0
   vencer = 0
   vencido = 0
   saldo_pedidos = 0
   
   
   frmCliente.lblV.Visible = True
   frmCliente.lblVcido.Visible = True
   frmCliente.lblDvencer.Visible = True
   frmCliente.lblDVencido.Visible = True
   frmCliente.lbllm.Visible = True
   frmCliente.lblLimCredito.Visible = True
   
    
   
End Sub


Private Sub txtCgc_Change()

If txtCgc <> "" Then
    txtNome.Enabled = False
    txtcodigo.Enabled = False
Else
    txtNome.Enabled = True
    txtcodigo.Enabled = True
End If

End Sub

Private Sub txtCgc_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodigo_DblClick()
    frmClienteFimPedido.Show vbModal
    If txtResposta <> "0" Then
        txtcodigo.Text = txtResposta
        txtcodigo.DataChanged = True
    End If
End Sub


Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodigo_LostFocus()
    If txtcodigo <> "" Then
        txtNome.Enabled = False
        txtCgc.Enabled = False
    Else
        txtNome.Enabled = True
        txtCgc.Enabled = True
    End If
End Sub

Private Sub txtNome_Change()
txtNome = UCase(txtNome)

If txtNome <> "" Then
    txtcodigo.Enabled = False
    txtCgc.Enabled = False
Else
    txtcodigo.Enabled = True
    txtCgc.Enabled = True
End If

End Sub


Private Sub txtNome_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


