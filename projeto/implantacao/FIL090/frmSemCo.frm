VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmSemCo 
   Caption         =   "Consulta de Clientes"
   ClientHeight    =   5490
   ClientLeft      =   1395
   ClientTop       =   1470
   ClientWidth     =   7725
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5490
   ScaleWidth      =   7725
   WindowState     =   2  'Maximized
   Begin VB.TextBox txtMenor 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   3360
      MaxLength       =   8
      TabIndex        =   1
      Top             =   930
      Width           =   1575
   End
   Begin VB.TextBox txtMaior 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   5400
      TabIndex        =   0
      Top             =   930
      Width           =   1575
   End
   Begin Threed.SSCommand cmdSair 
      Height          =   615
      Left            =   6120
      TabIndex        =   5
      Top             =   6000
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      Picture         =   "frmSemCo.frx":0000
   End
   Begin Threed.SSCommand cmdDados 
      Height          =   615
      Left            =   2520
      TabIndex        =   4
      Top             =   6000
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      ForeColor       =   8388608
      Picture         =   "frmSemCo.frx":031A
   End
   Begin Threed.SSCommand cmdVoltar 
      Height          =   615
      Left            =   4320
      TabIndex        =   3
      Top             =   6000
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1085
      _StockProps     =   78
      Picture         =   "frmSemCo.frx":076C
   End
   Begin MSGrid.Grid grdSelect 
      Height          =   4335
      Left            =   480
      TabIndex        =   2
      Top             =   1440
      Width           =   8655
      _Version        =   65536
      _ExtentX        =   15266
      _ExtentY        =   7646
      _StockProps     =   77
      BackColor       =   16777215
      Cols            =   3
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Clientes sem compras no per�odo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   360
      Left            =   2505
      TabIndex        =   8
      Top             =   240
      Width           =   4275
   End
   Begin VB.Label Label2 
      Caption         =   "Per�odo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2280
      TabIndex        =   7
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5040
      TabIndex        =   6
      Top             =   960
      Width           =   255
   End
End
Attribute VB_Name = "frmSemCo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdDados_Click()
    Dim dataMenor
    Dim dataMaior

    linha4 = accessdynaset.RecordCount

   'Limpar o grid
    Do While linha4 > 0 And linha4 <> 1
       grdSelect.RemoveItem linha4
       linha4 = linha4 - 1
    Loop

    grdSelect.Visible = False
   
    dataMenor = Format(txtMenor.Text, "m/d/yy")
    dataMaior = Format(txtMaior.Text, "m/d/yy")
    lstrsql = "SELECT DISTINCT a.cod_cliente, a.nome_cliente, b.dt_ult_compra " & _
              "FROM cliente AS a INNER JOIN clie_credito AS b ON a.cod_cliente = b.cod_cliente " & _
              "Where a.cod_cliente = b.cod_cliente And " & _
              "(b.dt_ult_compra < #" & dataMenor & "# or b.dt_ult_compra > #" & dataMaior & "#) and " & _
              "a.situacao = 0 ORDER BY a.nome_cliente"
 
    Set accessdynaset = accessdatabase.CreateDynaset(lstrsql, 0&)

    Screen.MousePointer = 11
    
    grdSelect.Row = 0
    grdSelect.Col = 0
    grdSelect.ColWidth(0) = 1000
    grdSelect.FixedAlignment(0) = 2
    grdSelect.Text = "C�d. Cliente"
    grdSelect.Col = 1
    grdSelect.ColWidth(1) = 5500
    grdSelect.FixedAlignment(1) = 2
    grdSelect.Text = "Nome Cliente"
    grdSelect.Col = 2
    grdSelect.ColWidth(2) = 1200
    grdSelect.FixedAlignment(2) = 2
    grdSelect.Text = "Dt.Ult.Compra"
    
    If accessdynaset.EOF() Then
        MsgBox "N�o h� registros para este per�odo."
        linha4 = 0
    Else
        linha4 = accessdynaset.RecordCount
    End If
    
   
    Do While Not accessdynaset.EOF()
       DoEvents
       grdSelect.AddItem accessdynaset.Fields("COD_CLIENTE") & Chr(9) & accessdynaset.Fields("NOME_CLIENTE") & _
       Chr(9) & accessdynaset.Fields("DT_ULT_COMPRA")
       accessdynaset.MoveNext
       grdSelect.Visible = True
    Loop
    
    If accessdynaset.EOF() And linha4 > 0 Then
        grdSelect.RemoveItem 1
    End If
Screen.MousePointer = 0

    
End Sub

Private Sub cmdSair_Click()
    End
End Sub


Private Sub cmdVoltar_Click()
    Unload Me
    MDIForm1.Show
End Sub


Private Sub FORM_Load()
'Carregando o campo da Data de Faturamento (data atual)
    grdSelect.Visible = False
    lstrsql = " Select dt_faturamento from DATAS"
    Set accessdynaset = accessdatabase.CreateDynaset(lstrsql, 0&)
    txtMaior.Text = accessdynaset.Fields("dt_faturamento")
    

End Sub

Private Sub grdSelect_DblClick()
    frmCliente.Refresh
    Dim LINHA_SELECIONADA As String
    
    LINHA_SELECIONADA = grdSelect.Row
    
    grdSelect.Col = 0
    frmCliente.txtcodigo.Text = grdSelect.Text
    
    Unload Me

End Sub

