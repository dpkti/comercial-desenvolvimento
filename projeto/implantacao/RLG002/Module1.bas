Attribute VB_Name = "Module1"
Option Explicit
Public vDeposito As Integer
Public Function TotalizarColunaGrid(pGrid As MSFlexGrid, pColuna As Integer, pFormato As String, Optional pDestacar As Boolean) As String

    On Error GoTo Erro

    Dim vControle As Integer

    pGrid.row = pGrid.Rows - 1

    If pGrid.TextMatrix(pGrid.row, 1) <> "TOTAL" Then

        pGrid.Rows = pGrid.Rows + 2
        pGrid.row = pGrid.Rows - 1
        pGrid.TextMatrix(pGrid.row, 1) = "TOTAL"

    End If

    Dim vTotal As Double

    For i = 1 To pGrid.Rows - 3

        If pGrid.TextMatrix(i, pColuna) <> "" Then
        
            vTotal = vTotal + CDbl(pGrid.TextMatrix(i, pColuna))
        
        End If

    Next

    pGrid.TextMatrix(pGrid.Rows - 1, pColuna) = Format(vTotal, pFormato)
    TotalizarColunaGrid = Format(vTotal, pFormato)

    If pGrid.ColWidth(pColuna) < pGrid.Parent.TextWidth(pGrid.TextMatrix(pGrid.Rows - 1, pColuna)) + 200 Then

        pGrid.ColWidth(9) = pGrid.Parent.TextWidth(pGrid.TextMatrix(pGrid.Rows - 1, pColuna)) + 200

    End If

    If pDestacar = True Then

        For vControle = 1 To pGrid.Cols - 1

            pGrid.col = vControle
            pGrid.CellBackColor = &HC0FFFF

        Next

    Else

        For vControle = 1 To pGrid.Cols - 1

            pGrid.col = vControle
            pGrid.CellBackColor = &H80000005

        Next

    End If

    pGrid.col = pGrid.FixedCols
    pGrid.row = pGrid.FixedRows

    Exit Function

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "TotalizarColunaGrid")

End Function
