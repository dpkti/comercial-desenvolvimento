VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFRAME.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "Msflxgrd.ocx"
Begin VB.Form frmRLG002 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:  FORM PRINCIPAL PADR�O (800x600)  :."
   ClientHeight    =   3705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7890
   ControlBox      =   0   'False
   Icon            =   "frmRLG002.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3705
   ScaleWidth      =   7890
   Begin VB.Timer Timer1 
      Interval        =   2000
      Left            =   7425
      Top             =   3240
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   960
      Left            =   7200
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   45
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRLG002.frx":0CCA
      PICN            =   "frmRLG002.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fra 
      Height          =   3075
      Left            =   45
      Top             =   -45
      Width           =   7080
      _ExtentX        =   12488
      _ExtentY        =   5424
      Caption         =   "Roteiros gerados"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin MSFlexGridLib.MSFlexGrid grdRoteiro 
         Height          =   2490
         Left            =   90
         TabIndex        =   0
         TabStop         =   0   'False
         Top             =   495
         Visible         =   0   'False
         Width           =   6900
         _ExtentX        =   12171
         _ExtentY        =   4392
         _Version        =   393216
         RowHeightMin    =   340
         BackColorBkg    =   14737632
         HighLight       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblDeposito 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   855
         TabIndex        =   15
         Top             =   225
         Width           =   105
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Dep�sito:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   90
         TabIndex        =   14
         Top             =   225
         Width           =   690
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "da gera��o do roteiro."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   90
         TabIndex        =   4
         Top             =   2250
         Width           =   6900
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Esta janela ir� fechar automaticamente no final"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   90
         TabIndex        =   3
         Top             =   1935
         Width           =   6900
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "O roteiro que est� sendo gerado ser� exibido e atualizado automaticamente."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   90
         TabIndex        =   2
         Top             =   990
         Width           =   6900
         WordWrap        =   -1  'True
      End
      Begin VB.Label lbl 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Aguarde..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   90
         TabIndex        =   1
         Top             =   675
         Width           =   6900
      End
   End
   Begin CoolXPFrame.xpFrame fraEstimativa 
      Height          =   645
      Left            =   45
      Top             =   3015
      Width           =   2760
      _ExtentX        =   4868
      _ExtentY        =   1138
      Caption         =   "Liga��es a gerar (Estimado)"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.Label lblDias 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2115
         TabIndex        =   7
         Top             =   225
         Width           =   105
      End
      Begin VB.Label lblRepresentantes 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   2160
         TabIndex        =   10
         Top             =   405
         Width           =   105
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Vendedores:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1215
         TabIndex        =   9
         Top             =   405
         Width           =   915
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Base (dias):"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   1215
         TabIndex        =   6
         Top             =   225
         Width           =   855
      End
      Begin VB.Label lblEstimativa 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   8
         Top             =   270
         Width           =   1020
      End
   End
   Begin CoolXPFrame.xpFrame fraGeradas 
      Height          =   645
      Left            =   2880
      Top             =   3015
      Width           =   1365
      _ExtentX        =   2408
      _ExtentY        =   1138
      Caption         =   "Lig. geradas"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.Label lblGeradas 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   90
         TabIndex        =   11
         Top             =   270
         Width           =   1200
      End
   End
   Begin CoolXPFrame.xpFrame xpFrame1 
      Height          =   645
      Left            =   4320
      Top             =   3015
      Width           =   2805
      _ExtentX        =   4948
      _ExtentY        =   1138
      Caption         =   "Perc. conclu�do (Estimado)"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Pode variar para mais ou menos de 100%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   1125
         TabIndex        =   13
         Top             =   225
         Width           =   1605
      End
      Begin VB.Label lblPercentual 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "0 %"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   12
         Top             =   270
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmRLG002"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()

    If vClsDPK001.Perguntar("Confirma sa�da do sistema?") = vbYes Then

        Call vClsDPK001.Sair

    End If

End Sub
Private Sub Form_Load()

    On Error GoTo Erro

    If Command$ = "" Then

        Call vClsDPK001.Informar("Este sistema � integrado ao RLG001. N�o � poss�vel execut�-lo manualmente.")
        End

    End If
    
    Me.Top = 360
    Me.Left = 75

    Call vClsDPK001.ConectarOracle("PRODUCAO", "RLG001", "DPK9475")

    If vTipoCDConectado = "U" Then

        vOwners(1) = "VENDAS"

    ElseIf vTipoCDConectado = "M" Then

        vOwners(1) = "DEP" & Format(vCD, "00")

    End If

    Me.Caption = ".:  " & UCase(vObjOracle(0).Fields(0)) & " - " & UCase(vObjOracle(0).Fields(1) & "   [" & vObjOracle(0).Fields(2) & "]") & "  :."
    
    If InStr(1, Command$, "|") > 0 Then

        vDeposito = Val(Trim(Mid(Command$, 1, InStr(1, Command$, "|"))))
        lblDias.Caption = Trim(Mid(Command$, InStr(1, Command$, "|") + 1))

    Else

        vDeposito = Command$
        lblDias.Caption = "21"

    End If

    Set vObjOracle(0) = vClsTABELAS001.TabelaLoja(vDeposito)
    lblDeposito = vObjOracle(0).Fields(0)
    lblDeposito.Refresh

    Call Timer1_Timer
    
    Exit Sub

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "Form_Load")

End Sub
Private Sub Form_Unload(Cancel As Integer)

    Cancel = 1

End Sub

Private Sub Timer1_Timer()

    On Error GoTo Erro

    If Dir("C:\RLG001\RLG001.TXT") <> "" Then
    
        lblGeradas.ForeColor = &HFF&
        lblPercentual.ForeColor = &HFF&
        lblGeradas.Refresh
        lblPercentual.Refresh
        
        Call vClsDPK001.ExcluiBind
        
        vBanco.Parameters.Add "PACAO", 4, 1
        vBanco.Parameters.Add "PCODLOJA", vDeposito, 1
        vBanco.Parameters.Add "PCODVEND", 0, 1
        
        vBanco.Parameters.Add "PRETORNO1", 0, 2
        vBanco.Parameters("PRETORNO1").serverType = 102
        vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
        vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "PCODERRO", 0, 2
        vBanco.Parameters.Add "PMSGERRO", "", 2
        
        vSql = _
            vOwners(1) & ".PCK_RLG001.PR_ACOMPANHAMENTO(" & _
               ":PACAO, " & _
               ":PCODLOJA, " & _
               ":PCODVEND, " & _
               ":PRETORNO1, " & _
               ":PCODERRO, " & _
               ":PMSGERRO)"
        
        Call vClsDPK001.ExecutaPL(vSql)
    
        If vBanco.Parameters("PCODERRO") <> 0 Then
    
            Call vClsDPK001.TratarErro(CStr(vBanco.Parameters("PCODERRO")), CStr(vBanco.Parameters("PMSGERRO")), True, "cmbDeposito_Click")
            Exit Sub
    
        End If
        
        Set vObjOracle(0) = vBanco.Parameters("PRETORNO1").Value
        
        If Not vObjOracle(0).EOF Then
        
            grdRoteiro.Visible = True
            Call vClsDPK001.PreencheGrid(vObjOracle(0), grdRoteiro, True, vObjOracle(0).Fields.Count + 1)
            lblGeradas = TotalizarColunaGrid(grdRoteiro, 3, "0", True)
            lblGeradas.Refresh
        
        Else
        
            lblGeradas = 0
            lblGeradas.Refresh
            grdRoteiro.Visible = False
        
        End If

        'Buscando estimativa de liga��es a serem geradas
        Call vClsDPK001.ExcluiBind
    
        vBanco.Parameters.Add "PACAO", 5, 1
        vBanco.Parameters.Add "PCODLOJA", vDeposito, 1
        vBanco.Parameters.Add "PCODVEND", 0, 1
        
        vBanco.Parameters.Add "PRETORNO1", 0, 2
        vBanco.Parameters("PRETORNO1").serverType = 102
        vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
        vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0
    
        vBanco.Parameters.Add "PCODERRO", 0, 2
        vBanco.Parameters.Add "PMSGERRO", "", 2
        
        vSql = _
            vOwners(1) & ".PCK_RLG001.PR_ACOMPANHAMENTO(" & _
               ":PACAO, " & _
               ":PCODLOJA, " & _
               ":PCODVEND, " & _
               ":PRETORNO1, " & _
               ":PCODERRO, " & _
               ":PMSGERRO)"
        
        Call vClsDPK001.ExecutaPL(vSql)
    
        If vBanco.Parameters("PCODERRO") <> 0 Then
    
            Call vClsDPK001.TratarErro(CStr(vBanco.Parameters("PCODERRO")), CStr(vBanco.Parameters("PMSGERRO")), True, "cmbDeposito_Click")
            Exit Sub
    
        End If
        
        Set vObjOracle(0) = vBanco.Parameters("PRETORNO1").Value
        
        If Not vObjOracle(0).EOF Then
        
            lblRepresentantes.Caption = vObjOracle(0).Fields(0)
            lblRepresentantes.Refresh
            
            lblEstimativa.Caption = vObjOracle(0).Fields(0) * vObjOracle(0).Fields(1) * Val(lblDias)
            lblEstimativa.Refresh
            
            lblPercentual = Round(Val(lblGeradas) / Val(lblEstimativa) * 100, 0) & " %"
            lblPercentual.Refresh
            
        End If
        
        lblGeradas.ForeColor = &H80000008
        lblPercentual.ForeColor = &H80000008
        lblGeradas.Refresh
        lblPercentual.Refresh
        
    Else
    
        Call vClsDPK001.Sair
    
    End If

    Exit Sub

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "Timer1_Timer")

End Sub
