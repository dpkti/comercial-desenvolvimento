﻿using System;

namespace Entities
{
    public class Cidade
    {
        public Int32 codigo { get; set; }
        public Int32 cdOrigem { get; set; }
        public string ufDestino { get; set; }
    }
}
