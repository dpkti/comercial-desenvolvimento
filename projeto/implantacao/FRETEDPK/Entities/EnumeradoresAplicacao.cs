﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class EnumeradoresAplicacao
    {
        public enum StatusTransportadoraItinerario
        {
            InicioValidacaoArquivo,
            FimValidacaoArquivo,
            InicioGeracaoCSV,
            FimGeracaoCSV,
            InicioInsercaoItinerarioTransportadora,
            FimInsercaoItinerarioTransportadora,
            Erro
        }

        public enum ColunaPropriedade
        {
            DataVigencia = 0,
            Canal = 1,
            CodTransportadora = 2,
            Prioridade = 3,
            CodItinerario = 4,
            CdOrigem = 5,
            CodCidadeDestino = 6,
            FaturamentoMinimo = 7,
            PrazoEntrega = 8,
            TipoCobranca = 9,
            VlFaixaInicial = 10,
            VlFaixaFinal = 11,
            ValorFrete = 12,
            PercentualFrete = 13
        }

        public enum TipoCobranca
        {
            Valor = 1,
            Percentual = 2
        }


    }
}
