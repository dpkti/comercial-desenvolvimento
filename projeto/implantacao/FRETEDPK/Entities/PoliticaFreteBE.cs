﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class FiltroGrid
    {
        public string cdItinerario { get; set; }
        public string cdTransp { get; set; }
        public Int32 cdOrigem { get; set; }
        public string ufDestino { get; set; }
        public Int32 cdCidadeDestino { get; set; }

    }  
}
