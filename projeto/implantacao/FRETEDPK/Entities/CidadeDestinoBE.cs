﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class CidadeDestinoBE
    {
        public int CODCIDADE { get; set; }
        public string CIDADE { get; set; }
        public string UF { get; set; }
    }
}
