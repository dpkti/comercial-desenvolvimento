﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class GerenteBE
    {
        public Int64 CPF { get; set; }
        public string NOME { get; set; }
        public double LIMITE { get; set; }
        public double SALDO { get; set; }
    }

    public class GerenteRegional
    {
        public long CPF { get; set; }
        public string NOME { get; set; }
    }

    public class GerenteLimiteBE
    {
        public Int64 CPF { get; set; }
        public string TIPO { get; set; }
        public string NOME { get; set; }
        public string FILIAL { get; set; }
        public int COD_FILIAL { get; set; }
        public double LIMITE { get; set; }
        public double SALDO { get; set; }
    }

    public class HistoricoGerente
    {
        public DateTime DATA { get; set; }
        public string NOME { get; set; }
        public string FILIAL { get; set; }
        public string GERENTEREGIONAL { get; set; }
        public double LIMITEANT { get; set; }
        public double SALDOANT { get; set; }
        public double LIMITE { get; set; }
        public double SALDO { get; set; }
        public string USUARIO { get; set; }
        public string OPERACAO { get; set; }
    }

    public class FiltroHistGerente
    {
        public Int64 CPF { get; set; }
        public string TIPO { get; set; }
        public string DTINI { get; set; }
        public string DTFIM { get; set; }

    }

    public class FiltroPedidoGerente
    {
        public Int64 cpfGerenteRegional { get; set; }
        public Int64 cpfGerente { get; set; }
        public string dtIni { get; set; }
        public string dtFim { get; set; }
    }

    public class ComboGerente
    {
        public long CPF { get; set; }
        public string DESCNOME { get; set; }
        public string NOME { get; set; }
        public string TIPO { get; set; }
    }

    public class ComboGerenteLimite : ComboGerente
    {
        public long CPFREGIONAL { get; set; }
    }

    public class ComboFilial
    {
        public int CDFILIAL { get; set; }
        public string FILIAL { get; set; }
    }

    public class InserirAlterarGerente
    {
        public long CPF_REGIONAL { get; set; }
        public long CPF { get; set; }
        public string TIPO { get; set; }
        public string NOME_REGIONAL { get; set; }
        public string NOME_GERENTE { get; set; }
        public int CDFILIAL { get; set; }
    }

    public class FiltroGerenteLimite
    {
        public Int64 gerente { get; set; }
        public string tipo { get; set; }
        public int filial { get; set; }
        public double limite { get; set; }
        public double saldo { get; set; }
        public Int64 gerenteRegional { get; set; }
        public string loginAlteracao { get; set; }
    }

    public enum TipoAcao
    {
        Inserir = 'I',
        Atualizar = 'A',
        Delete = 'D'
    }

    public class Gerente
    {
        public Int64 cpf { get; set; }
        public string nome { get; set; }
        public string tipo { get; set; }
        public int situacao { get; set; }
    }
        
}
