﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Gerente
    {
        public Int64 cpf { get; set; }
        public string nome { get; set; }
        public string tipo { get; set; }
        public int filial { get; set; }
        public Int64 gerenteRegional { get; set; }
        public Decimal limite { get; set; }
        public Decimal saldo { get; set; }
    }
}
