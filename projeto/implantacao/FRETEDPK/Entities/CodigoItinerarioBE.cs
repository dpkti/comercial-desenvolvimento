﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class CodigoItinerarioBE
    {
        public int COD_ITINERARIO { get; set; }
        public string ITINERARIO { get; set; }
    }

    public class CodigoTransportadoraBE
    {
        public int COD_TRANSP { get; set; }
        public string NOME_TRANSP { get; set; }
    }

}
