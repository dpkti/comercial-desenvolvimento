﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class UfDestinoBE
    {
        public string CDUF { get; set; }
        public string UF { get; set; }
    }
}
