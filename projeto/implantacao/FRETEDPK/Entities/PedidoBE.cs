﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class PedidoBE
    {
        public string nomeGerenteRegional { get; set; }
        public string nomeGerente { get; set; }
        public string filial { get; set; }
        public int codLoja { get; set; }
        public string numPedido { get; set; }
        public string seqPedido { get; set; }
        public string numNota { get; set; }
        public string dataEmissao { get; set; }
        public string dataLiberacao { get; set; }
        public Double valorNota { get; set; }
        public Double vlFreteCalculado { get; set; }
        public Double vlPagoGerente { get; set; }
    }
}
