﻿
using System;
namespace Entities
{
    public class Itinerario
    {
        public Int64 codigo { get; set; }
        public string cdOrigem { get; set; }
        public string ufOrigem { get; set; }
        public string cidadeDestino { get; set; }
        public string ufDestino { get; set; }
    }

    public class FiltroItinerario
    {
        public int cdOrigem { get; set; }
        public int codCidadeDestino { get; set; }
        public string ufDestino { get; set; }
    }

    public class ResultItinerario
    {
        public Int64 codigo { get; set; }
        public int cdOrigem { get; set; }
        public Int32 codCidadeDestino { get; set; }
        public string ufDestino { get; set; }
        public string nomeCidadeDestino { get; set; }
        public string cdOrigemDescricao { get; set; }
    }
}
