﻿
using System;
namespace Entities
{
    public class ItinerarioTransportadora
    {
        public int codigoTransportadora { get; set; }
        public int prioridade { get; set; }
        public Double faturamentoMinimo { get; set; }
        public int prazoEntrega { get; set; }
        public int tipoCobranca { get; set; }
        public Double valorFaixaInicial { get; set; }
        public Double valorFaixaFinal { get; set; }
        public Double valorFrete { get; set; }
        public Double percentualFrete { get; set; }
        public Int64 codigoItiTransp { get; set; }
        public DateTime dataVigencia { get; set; }
        public Int32 codItinerario { get; set; }
        public int canal { get; set; }
        public bool excluir { get; set; }
        public int linha { get; set; }
    }

    public class ItinerarioTransportadoraResultado : ItinerarioTransportadoraBasic
    {

        public string codigoTransportadora { get; set; }
    }

    public class Transportadora
    {
        public int codTransportadora { get; set; }
        public Int64 cnpj { get; set; }
        public string nome { get; set; }
    }

    public class ItinerarioTransportadoraBasic
    {
        public DateTime dataVigencia { get; set; }
        public Int32 canal { get; set; }
        public int prioridade { get; set; }
        public Int32 codigoItinerario { get; set; }
        public int cdOrigem { get; set; }
        public string ufDestino { get; set; }
        public string cidadeDestino { get; set; }
        public Double faturamentoMinimo { get; set; }
        public int prazoEntrega { get; set; }
        public int tipoCobranca { get; set; }
        public Double valorFaixaInicial { get; set; }
        public Double valorFaixaFinal { get; set; }
        public Double valorFrete { get; set; }
        public Double percentualFrete { get; set; }
    }

    public class ItinerarioTransportadoraValidacao : ItinerarioTransportadoraBasic
    {
        public int codigoTransportadora { get; set; }
        public int cidadeDestino { get; set; }
        public Int32 linha { get; set; }
        public int canal { get; set; }
    }

    public class FiltroItinerarioTransportadora
    {
        public int codItinerario { get; set; }
        public int canal { get; set; }
        public DateTime dataVigencia { get; set; }
    }
}
