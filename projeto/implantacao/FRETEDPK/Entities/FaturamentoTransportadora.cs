﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
        public class FaturamentoTransportadoraGrid
        {
            public string MesAno{ get; set; }
            public string Itinerario{ get; set; }
            public string Transportadora { get; set; }
            public Double ValorFreteRecuperado{ get; set; }
            
        }

        public class FiltroGridTransportadora
        {
            public DateTime dataInicial { get; set; }
            public DateTime dataFinal { get; set; }
            public string cdItinerario { get; set; }
            public int cdTransp { get; set; }
            public Int32 cdOrigem { get; set; }
            public string ufDestino { get; set; }
            public Int32 cdCidadeDestino { get; set; }
        }

        public class TransportadoraCombo
        {
            public int codTransportadora { get; set; }
            public string nome { get; set; }
        }

}
