﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class GlobalBE
    {
        public static List<CidadeDestinoBE> listaCidade = null;
        public static List<UfDestinoBE> listaUF = null;
        public static List<CdOrigemBE> listaCdOrigem = null;
        public static List<CodigoItinerarioBE> listaCdItinerario = null;
        public static List<ComboGerente> listaGerente = null;
        public static List<ComboFilial> listaFilial = null;

        public static UsuarioBE usuarioLogado = null;
    }
}
