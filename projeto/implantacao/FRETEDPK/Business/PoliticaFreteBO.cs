﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;
using System.ComponentModel;
using UtilGeralDPA;
using System.IO;
using Excel;
using System.Diagnostics;
using System.Globalization;
using System.Transactions;

namespace Business
{
    public class PoliticaFreteBO
    {

        /// <summary>
        /// Método responsável por buscar os códigos Itinerario.
        /// </summary>
        /// <returns>Descrição do Itinerario</returns>
        public static List<CodigoItinerarioBE> BuscarDescricaoItinerario()
        {
            return PoliticaFreteDALC.BuscarCodigosItinerarios();
        }

        /// <summary>
        /// Método responsável por buscar os códigos de Canal.
        /// </summary>
        /// <returns>Descrição do Canal</returns>
        public static List<CodigoCanalBE> BuscarDescricaoCanal(string login)
        {
            return PoliticaFreteDALC.BuscarCodigosCanal(login);//TI-5946
        }

        /// <summary>
        /// Método responsável por buscar Trasnportadores para popular a grid.
        /// </summary>
        /// <returns>lista de transportadores</returns>
        public static List<ItinerarioTransportadoraResultado> BuscarTransportadora(FiltroGrid ofiltro)
        {
            return PoliticaFreteDALC.BuscaTranportadoraGrid(ofiltro);
        }

        /// <summary>
        /// Método responsável por buscar as transportadoras.
        /// </summary>
        /// <returns>lista de transportadoras</returns>
        public static List<CodigoTransportadoraBE> BuscarDescricaoTransportadora()
        {
            return PoliticaFreteDALC.BuscarCodigosTransportadoras();

        }

        /// <summary>
        /// Método responsável por gerar arquivo CSV a partir do arquivo excel importado
        /// </summary>
        /// <param name="bgWorkerPoliticaFrete">background para mostrar em qual passo esta</param>
        /// <param name="listaItinerarioTransportadora">lista de grupo e subgrupo</param>
        /// <returns>sucesso ou erro</returns>
        public static bool GerarArquivoCSV(BackgroundWorker bgWorkerPoliticaFrete, List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora)
        {
            bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioGeracaoCSV);
            DateTime dataInicio = Logger.LogInicioMetodo();
            string caminhoArquivoCSV = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_TEMP_SQL_LOADER_ITINERARIO) + "ITINERARIOTRANSPORTADORACSV.csv";
            string caminhoArquivoCTL = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_TEMP_SQL_LOADER_ITINERARIO) + "ITINERARIOTRANSPORTADORACTL.ctl";
            string caminhoArquivoLog = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_TEMP_SQL_LOADER_ITINERARIO) + "logSQLLoader.log";
            string caminhoBatSqlLoader = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_TEMP_SQL_LOADER_ITINERARIO) + "\\ITINERARIOTRANSPORTADORASQLLoader.bat";
            string usuarioConexaoBanco = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.USERID_SQL_LOADER_ITINERARIO);
            try
            {
                CriarDiretorio();
                GerarArquivoCSV(listaItinerarioTransportadora, caminhoArquivoCSV);
                GerarSqlLoader(caminhoArquivoCSV, caminhoArquivoCTL);
                GerarBatSqlLoader(usuarioConexaoBanco, caminhoArquivoCTL, caminhoArquivoLog, caminhoBatSqlLoader);
                PoliticaFreteDALC.LimparStageItinerarioTransportadora();
                ProcessStartInfo psi = new ProcessStartInfo(caminhoBatSqlLoader);

                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = new Process();
                process.EnableRaisingEvents = false;
                process.StartInfo = psi;

                process.Start();
                process.WaitForExit();
                process.Close();

                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.FimGeracaoCSV);

                return true;
            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.Erro);
                throw new Exception("Falha ao processar o arquivo.", ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Gera bat para o SQLLoader
        /// </summary>
        /// <param name="usuarioConexaoBanco">usuário de conexão</param>
        /// <param name="caminhoArquivoCTL">caminho do arquivo CLT</param>
        /// <param name="caminhoArquivoLog">caminho do arquivo de log</param>
        /// <param name="caminhoBatSqlLoader">caminho do bat</param>
        private static void GerarBatSqlLoader(string usuarioConexaoBanco, string caminhoArquivoCTL, string caminhoArquivoLog, string caminhoBatSqlLoader)
        {
            string cmd = @"sqlldr userid=" + usuarioConexaoBanco + " control=" + caminhoArquivoCTL + " log=" + caminhoArquivoLog;

            //Cria arquivo CTL com os comandos de SqlLoader
            using (StreamWriter sw = new StreamWriter(caminhoBatSqlLoader))
            {
                sw.WriteLine(cmd);
                //sw.WriteLine("pause"); //para adicionar uma pause para testes
            }
        }

        /// <summary>
        /// Gera arquivo para o SQLLoader
        /// </summary>
        /// <param name="caminhoArquivoCSV">caminho do arquivo CSV</param>
        /// <param name="caminhoArquivoCTL">caminho do arquivo CLT</param>
        private static void GerarSqlLoader(string caminhoArquivoCSV, string caminhoArquivoCTL)
        {
            string cmd = @"OPTIONS 
                            (SKIP = 0, 
                             ERRORS = 1000, 
                             ROWS = 66796, 
                             DIRECT = FALSE, 
                             PARALLEL = FALSE 
                            ) 
                           LOAD DATA
                           INFILE '" + caminhoArquivoCSV +
                         @"'APPEND INTO TABLE PRODUCAO.STG_ITINERARIO_TRANSPORTADORA
                           FIELDS TERMINATED BY ','
                           TRAILING NULLCOLS
                           (
                           DT_VIGENCIA            DATE EXTERNAL,
                           CANAL                  INTEGER EXTERNAL,
                           COD_TRANSP             INTEGER EXTERNAL,
                           PRIOR_TRANSP           INTEGER EXTERNAL,
                           COD_ITINERARIO         INTEGER EXTERNAL,
                           FAT_MINIMO             DECIMAL EXTERNAL,
                           PRAZO_ENTREGA          INTEGER EXTERNAL,
                           TP_COBRANCA            INTEGER EXTERNAL,
                           VL_FAIXA_INI           DECIMAL EXTERNAL,
                           VL_FAIXA_FINAL         DECIMAL EXTERNAL,
                           VL_FRETE               DECIMAL EXTERNAL,
                           PERC_FRETE             DECIMAL EXTERNAL)";

            //Cria arquivo CTL com os comandos de SqlLoader
            using (StreamWriter sw = new StreamWriter(caminhoArquivoCTL))
            {
                sw.WriteLine(cmd);
            }
        }

        /// <summary>
        /// Auxilia a geração do arquivo csv
        /// </summary>
        /// <param name="separator">separado dos campos CSV</param>
        /// <param name="ItinerarioTransportadoraValidacao">Objeto com itinerario transportadora</param>
        /// <returns></returns>
        private static string ToCsvFields(string separator, ItinerarioTransportadoraValidacao itinerarioTransportadora)
        {
            StringBuilder linie = new StringBuilder();
            linie.Append(String.Format("{0:dd-MMM-yyyy}", itinerarioTransportadora.dataVigencia) + separator + itinerarioTransportadora.canal.ToString()
                + separator + itinerarioTransportadora.codigoTransportadora.ToString() + separator + itinerarioTransportadora.prioridade.ToString()
                + separator + itinerarioTransportadora.codigoItinerario.ToString() + separator + itinerarioTransportadora.faturamentoMinimo.ToString()
                + separator + itinerarioTransportadora.prazoEntrega.ToString() + separator + itinerarioTransportadora.tipoCobranca.ToString()
                + separator + itinerarioTransportadora.valorFaixaInicial.ToString() + separator + itinerarioTransportadora.valorFaixaFinal.ToString()
                + separator + itinerarioTransportadora.valorFrete.ToString() + separator + itinerarioTransportadora.percentualFrete.ToString());
            return linie.ToString();
        }

        /// <summary>
        /// Auxilia a geração do arquivo csv
        /// </summary>
        /// <param name="separator">separador das colunas no arquivo csv</param>
        /// <param name="listaItinerarioTransportadora">lista de itinerário transportadoras</param>
        /// <returns>string de arquivo csv</returns>
        private static string ToCsv(string separator, List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora)
        {
            StringBuilder csvdata = new StringBuilder();

            foreach (var item in listaItinerarioTransportadora)
                csvdata.AppendLine(ToCsvFields(separator, item));

            return csvdata.ToString();
        }

        /// <summary>
        /// Transforma a lista de grupo e subgrupo em string separada por virgula
        /// </summary>
        /// <param name="listaItinerarioTransportadora">lista de itinerário transportadoras</param>
        /// <param name="caminhoArquivoCSV">caminho que deverá ser gravado o csv</param>
        private static void GerarArquivoCSV(List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora, string caminhoArquivoCSV)
        {
            //Cria uma string em formatação CSV com os dados do objeto item
            String csv = ToCsv(",", listaItinerarioTransportadora);

            //Cria arquivo CSV com os dados a serem gravados no banco
            using (StreamWriter sw = new StreamWriter(caminhoArquivoCSV))
            {
                sw.WriteLine(csv);
            }
        }

        /// <summary>
        /// Cria diretório para o SQLLoader
        /// </summary>
        /// <returns></returns>
        private static Boolean CriarDiretorio()
        {
            try
            {
                DirectoryInfo dirArquivoSQLLoader = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(Parametros.DIR_TEMP_SQL_LOADER_ITINERARIO));

                if (!dirArquivoSQLLoader.Exists)
                {
                    dirArquivoSQLLoader.Create();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Método responsável por validar o arquivo excel importado
        /// </summary>
        /// <param name="caminhoArquivo">caminho do arquivo excel</param>
        /// <param name="bgWorkerPoliticaFrete">backgroundworker para mostrar o passo em que esta</param>
        /// <param name="retLogGeraArq">log de erro</param>
        /// <returns>lista com o itinerario transportadora</returns>
        public static List<ItinerarioTransportadoraValidacao> ValidarArquivoExcel(string caminhoArquivo, BackgroundWorker bgWorkerPoliticaFrete, out string retLogGeraArq)
        {
            bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioValidacaoArquivo);
            DateTime dataInicio = Logger.LogInicioMetodo();
            string caminhoOrigem;
            string arquivo;
            string arqListErro = string.Empty;
            string arqListSuc = string.Empty;
            List<String> listaErro = new List<String>();
            List<String> listaSucesso = new List<String>();
            bool erro = false;
            int count = 0;
            
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora = new List<ItinerarioTransportadoraValidacao>();

                FileStream stream = File.Open(arquivo, FileMode.Open, FileAccess.Read);

                string extension = Path.GetExtension(arquivo);
                IExcelDataReader excelReader;
                if (extension.Equals(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;

                while (excelReader.Read())
                {
                    if (count > 0 && (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.DataVigencia) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodTransportadora) != null
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.Prioridade) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodItinerario) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CdOrigem) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodCidadeDestino) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.FaturamentoMinimo) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PrazoEntrega) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.TipoCobranca) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaInicial) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaFinal) != null 
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.ValorFrete) != null
                                      || excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PercentualFrete) != null))
                    {
                        ItinerarioTransportadoraValidacao oItiTransp = new ItinerarioTransportadoraValidacao();
                        oItiTransp = ValidarTipagem(excelReader, listaErro);

                        if (oItiTransp == null)
                        {
                            erro = true;
                            break;
                        }

                        if (!ValidaDataVigencia(excelReader.Depth, oItiTransp.dataVigencia, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarCanal(excelReader.Depth, oItiTransp.canal, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarTransportadora(excelReader.Depth, oItiTransp.codigoTransportadora, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarItinerario(excelReader.Depth, oItiTransp, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarItigerarioVigenciaFutura(excelReader.Depth, oItiTransp.codigoItinerario, oItiTransp.canal, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarPrioridadeTransportadora(excelReader.Depth, listaItinerarioTransportadora, oItiTransp, listaErro))
                        {
                            erro = true;
                        }

                        if (!ValidarFaixasPrazo(excelReader.Depth, listaItinerarioTransportadora, oItiTransp, listaErro))
                        {
                            erro = true;
                        }

                        if (!erro)
                        {
                            listaItinerarioTransportadora.Add(oItiTransp);
                        }
                        else
                        {
                            break;
                        }
                    }
                    count++;
                }

                excelReader.Close();

                if (!erro)
                {
                    if (!ValidarPrioridadeSequencial(listaItinerarioTransportadora, listaErro))
                    {
                        erro = true;
                    }

                    if (!ValidarDataVigenteItinerario(listaItinerarioTransportadora, listaErro))
                    {
                        erro = true;
                    }
                }

                if (erro)
                {
                    arqListErro = gravarLista(listaErro, caminhoArquivo, "Erro");
                }
                else
                {
                    arqListErro = string.Empty;
                }

                retLogGeraArq = arqListErro;

                File.Delete(arquivo);

                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.FimValidacaoArquivo);

                return listaItinerarioTransportadora; 
   
            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.Erro);
                throw new Exception("Falha ao processar o arquivo.", ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método que valida se todos as linhas de uma mesmo itinerário possui a mesma data de vigência
        /// </summary>
        /// <param name="listaItinerarioTransportadora"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarDataVigenteItinerario(List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora, List<string> listaErro)
        {
            bool dataVigenteItinerario;
            dataVigenteItinerario = true;

            var listaItinerario = (from i in listaItinerarioTransportadora
                                  select new { i.codigoItinerario, i.canal }).Distinct().ToList();

            foreach (var itinerario in listaItinerario)
            {
                var listaDatas = (from ld in listaItinerarioTransportadora
                                  where ld.codigoItinerario == itinerario.codigoItinerario && ld.canal == itinerario.canal
                                  select new { ld.dataVigencia }).Distinct().ToList();

                if (listaDatas.Count > 1)
                {
                    dataVigenteItinerario = false;
                    listaErro.Add(String.Format("Itinerario: {0} - Canal: {1} - Erro, o campo DATA_VIGENCIA deve ser o mesmo para todas as transportadoras em um mesmo itinerário. - Em {2}", itinerario.codigoItinerario, itinerario.canal, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                else
                {
                    dataVigenteItinerario = true;
                }
            }

            return dataVigenteItinerario;
        }

        /// <summary>
        /// Método responsável por validar se a prioridade de um itinerário esta sequencial
        /// </summary>
        /// <param name="listaItinerarioTransportadora"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarPrioridadeSequencial(List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora, List<string> listaErro)
        {
            bool prioridateSequencial;
            int prioridademax;
            prioridateSequencial = true;

            var listaItinerario = (from i in listaItinerarioTransportadora
                                  select new { i.codigoItinerario, i.canal }).Distinct().ToList();
                
            foreach(var itinerario in listaItinerario)
            {
                var listaTransportadoraItinerario = (from transp in listaItinerarioTransportadora
                                                     where transp.codigoItinerario == itinerario.codigoItinerario && transp.canal == itinerario.canal
                                                     select transp).ToList();
                prioridademax = listaTransportadoraItinerario.Max(x => x.prioridade);
                                                     
                var listaTranspItinerarioSemRepetidas = (from transp in listaItinerarioTransportadora
                                                         where transp.codigoItinerario == itinerario.codigoItinerario && transp.canal == itinerario.canal    
                                                         select new {transp.codigoTransportadora}).Distinct().ToList();
                if (listaTranspItinerarioSemRepetidas.Count != prioridademax)
                {
                    prioridateSequencial = false;
                    listaErro.Add(String.Format("Itinerario: {0} - Canal: {1} - Erro, o campo PRIORIDADE_TRANSPORTADORA deve ser sequêncial para transportadoras diferentes em um mesmo itinerário. - Em {2}",itinerario.codigoItinerario, itinerario.canal, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                else
                {
                    prioridateSequencial = true;
                }
            }

            return prioridateSequencial;
        }

        /// <summary>
        /// Método responsável por validar se a data vigência é maior que a data atual
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="dataVigencia"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaDataVigencia(int posicao, DateTime dataVigencia, List<string> listaErro)
        {
            bool dataValida;

            if (dataVigencia <= DateTime.Now)
            {
                dataValida = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, campo DATA_VIGENCIA deve ser maior que a data atual. - Em {1}", posicao, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }
            else
            {
                dataValida = true;
            }

            return dataValida;
        }

        /// <summary>
        /// Método responsável por validar se não existe faixa duplicadas de uma transportadora para um mesmo itinerário
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="listaItinerarioTransportadora"></param>
        /// <param name="oItiTransp"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarFaixasPrazo(int posicao, List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora, ItinerarioTransportadoraValidacao oItiTransp, List<string> listaErro)
        {
            bool naoExisteFaixasDuplicadas;

            if (listaItinerarioTransportadora.Count == 0)
            {
                naoExisteFaixasDuplicadas = true;
            }
            else
            {
                var listaFaixasDuplicadas = (from itiTranspFaixas in listaItinerarioTransportadora
                                             where itiTranspFaixas.canal == oItiTransp.canal
                                             && itiTranspFaixas.codigoItinerario == oItiTransp.codigoItinerario
                                             && (itiTranspFaixas.valorFaixaInicial == oItiTransp.valorFaixaInicial || itiTranspFaixas.valorFaixaFinal == oItiTransp.valorFaixaFinal
                                             || (oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal <= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaFinal && oItiTransp.valorFaixaFinal <= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaFinal && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaFinal))
                                             && itiTranspFaixas.codigoTransportadora == oItiTransp.codigoTransportadora
                                             && itiTranspFaixas.prazoEntrega == oItiTransp.prazoEntrega
                                             select itiTranspFaixas).ToList();

                if (listaFaixasDuplicadas.Count > 0)
                {
                    naoExisteFaixasDuplicadas = false;
                    listaErro.Add(String.Format("Linha: {0} - Erro, informação duplicada para o campo VL_FAIXA_INICIAL e/ou VL_FAIXA_FINAL na linha {1}. - Em {2}", posicao, listaFaixasDuplicadas.FirstOrDefault().linha, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                else
                {
                    naoExisteFaixasDuplicadas = true;
                }
            }

            return naoExisteFaixasDuplicadas;
        }

        /// <summary>
        /// Método responsável por verificar se existe prioridades duplicadas para um mesmo itinerário e transportadoras diferentes
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="listaItinerarioTransportadora"></param>
        /// <param name="oItiTransp"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        public static bool ValidarPrioridadeTransportadora(int posicao, List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora, ItinerarioTransportadoraValidacao oItiTransp, List<string> listaErro)
        {
            bool naoExistePrioridadeDuplicada;

            if (listaItinerarioTransportadora.Count == 0)
            {
                naoExistePrioridadeDuplicada = true;
            }
            else
            {
                var listaPrioridade = (from itiTranspPri in listaItinerarioTransportadora
                                      where itiTranspPri.canal == oItiTransp.canal
                                          && itiTranspPri.codigoItinerario == oItiTransp.codigoItinerario
                                          && itiTranspPri.prioridade == oItiTransp.prioridade
                                          && itiTranspPri.codigoTransportadora != oItiTransp.codigoTransportadora
                                      select itiTranspPri).ToList();

                if (listaPrioridade.Count > 0)
                {
                    naoExistePrioridadeDuplicada = false;
                    listaErro.Add(String.Format("Linha: {0} - Erro, informação duplicada para o campo PRIORIDADE_TRANSPORTADORA na linha {1}. - Em {2}", posicao, listaPrioridade.FirstOrDefault().linha, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                else
                {
                    naoExistePrioridadeDuplicada = true;
                }
            }

            return naoExistePrioridadeDuplicada;
        }

        /// <summary>
        /// Método responsável por verificar se já existe um itinerário com data de vigência futura
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="codigoItinerario"></param>
        /// <param name="canal"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarItigerarioVigenciaFutura(int posicao, int codigoItinerario, int canal, List<string> listaErro)
        {
            DateTime? dataVigenciaFutura = new DateTime();
            bool naoExisteDataVigenciaFutura; 

            dataVigenciaFutura = PoliticaFreteDALC.ValidarItigerarioVigenciaFutura(codigoItinerario, canal);

            if (dataVigenciaFutura == null)
            {
                naoExisteDataVigenciaFutura = true;
            }
            else
            {
                naoExisteDataVigenciaFutura = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, já existe vigência futura, para o itinerário {1} e canal {2}. - Em {3}", posicao, codigoItinerario, canal, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }

            return naoExisteDataVigenciaFutura;
        }

        /// <summary>
        /// Método responsável por verificar se existe o itinerário
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="oItiTransp"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarItinerario(int posicao, ItinerarioTransportadoraValidacao oItiTransp, List<string> listaErro)
        {
            bool existeItinerario = true;

            if (oItiTransp.codigoItinerario == 0)
            {
                oItiTransp.codigoItinerario = PoliticaFreteDALC.BuscarItinerario(oItiTransp.cdOrigem, oItiTransp.cidadeDestino);

                if (oItiTransp.codigoItinerario == 0)
                {
                    existeItinerario = false;
                    listaErro.Add(String.Format("Linha: {0} - Erro, não existe itinerário para CD_ORIGEM ({1}) - COD_CIDADE_DESTINO ({2}) - Em {3}", posicao, oItiTransp.cdOrigem, oItiTransp.cidadeDestino, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
            }
            else
            {
                existeItinerario = PoliticaFreteDALC.ValidarItinerario(oItiTransp.codigoItinerario);

                if (!existeItinerario)
                {
                    listaErro.Add(String.Format("Linha: {0} - Erro, não existe itinerário - {1} cadastrado - Em {2}", posicao, oItiTransp.codigoItinerario, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
            }

            return existeItinerario;
        }

        /// <summary>
        /// Método responsável por verificar se existe o canal
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="canal"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarCanal(int posicao, int canal, List<string> listaErro)
        {
            bool canalExiste = PoliticaFreteDALC.ValidarCanal(canal);

            if (!canalExiste)
            {
                listaErro.Add(String.Format("Linha: {0} - Erro, CANAL - {1} inválido (20-DPK e 30-AutoZ). - Em {2}", posicao, canal, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }

            return canalExiste;
        }

        /// <summary>
        /// Método responsável por verificar o código da transportadora
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="codTransportadora"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        public static bool ValidarTransportadora(int posicao, int codTransportadora, List<String> listaErro)
        {
            bool transportadoraExiste = PoliticaFreteDALC.ValidarTransportadora(codTransportadora);

            if (!transportadoraExiste)
            {
                listaErro.Add(String.Format("Linha: {0} - Erro, COD_TRANSPORTADORA - {1} não existe na base ou esta desativada ou não consta na tabela EDI.R_TRANSP_CGC. - Em {2}", posicao, codTransportadora, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }

            return transportadoraExiste;
        }

        /// <summary>
        /// Método responsável por validar a tipagem dos campos do excel importado
        /// </summary>
        /// <param name="excelReader"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static ItinerarioTransportadoraValidacao ValidarTipagem(IExcelDataReader excelReader, List<String> listaErro)
        {
            DateTime dataVigencia;
            int canal;
            int codTransportadora;
            int prioridade;
            int itinerario;
            int cdOrigem;
            int codCidadeDestino;
            Double faturamentoMinimo;
            int prazoEntrega;
            int tipoCobranca;
            Double vlFaixaInicial;
            Double vlFaixaFinal;
            Double valorFrete;
            Double percentualFrete;
            bool dataVigenciaValida;
            bool canalValido;
            bool codTransportadoraValido;
            bool prioridadeValida;
            bool itinerarioValido;
            bool cdOrigemValido;
            bool codCidadeDestinoValido;
            bool faturamentoMinimoValido;
            bool prazoEntregaValido;
            bool tipoCobrancaValido;
            bool vlFaixaInicialValido;
            bool vlFaixaFinalValido;
            bool valorFreteValido;
            bool percentualFreteValido;
            bool erro;
            ItinerarioTransportadoraValidacao oItiTransp = new ItinerarioTransportadoraValidacao();
            erro = false;
            oItiTransp.linha = excelReader.Depth;

            try
            {
                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.DataVigencia) != null)
                {            
                    dataVigenciaValida = DateTime.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.DataVigencia).ToString(), out dataVigencia);

                    if (!dataVigenciaValida)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, DATA_VIGENCIA em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                    else
                    {
                        oItiTransp.dataVigencia = dataVigencia;
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, DATA_VIGENCIA é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.Canal) != null)
                {
                    canalValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.Canal).ToString(), out canal);

                    if (canalValido && canal > 0)
                    {
                        oItiTransp.canal = canal;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, CANAL em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, CANAL é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodTransportadora) != null)
                {
                    codTransportadoraValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodTransportadora).ToString(), out codTransportadora);

                    if (codTransportadoraValido && codTransportadora > 0)
                    {
                        oItiTransp.codigoTransportadora = codTransportadora;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, COD_TRANSPORTADORA em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, COD_TRANSPORTADORA é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.Prioridade) != null)
                {
                    prioridadeValida = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.Prioridade).ToString(), out prioridade);

                    if (!prioridadeValida)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, PRIORIDADE_TRANSPORTADORA em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                    else
                    {
                        if (prioridade <= 0)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Linha: {0} - Erro, PRIORIDADE_TRANSPORTADORA deve ser maior que zero. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        else
                        {
                            oItiTransp.prioridade = prioridade;
                        }
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, PRIORIDADE_TRANSPORTADORA é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodItinerario) != null)
                {
                    itinerarioValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodItinerario).ToString(), out itinerario);

                    if (!itinerarioValido)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, COD_ITINERARIO em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                    else
                    {
                        oItiTransp.codigoItinerario = itinerario;
                    }
                }
                else
                {
                    if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CdOrigem) != null)
                    {
                        cdOrigemValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CdOrigem).ToString(), out cdOrigem);

                        if (!cdOrigemValido)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Linha: {0} - Erro, CD_ORIGEM em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        else
                        {
                            oItiTransp.cdOrigem = cdOrigem;
                        }
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, CD_ORIGEM é obrigatório quando COD_ITINERARIO esta em branco. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }

                    if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodCidadeDestino) != null)
                    {
                        codCidadeDestinoValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.CodCidadeDestino).ToString(), out codCidadeDestino);

                        if (!codCidadeDestinoValido)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Linha: {0} - Erro, COD_CIDADE_DESTINO em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        else
                        {
                            oItiTransp.cidadeDestino = codCidadeDestino;
                        }
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, COD_CIDADE_DESTINO é obrigatório quando COD_ITINERARIO esta em branco. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.FaturamentoMinimo) != null)
                {
                    faturamentoMinimoValido = Double.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.FaturamentoMinimo).ToString(), out faturamentoMinimo);

                    if (faturamentoMinimoValido && faturamentoMinimo >= 0 && faturamentoMinimo <= 999999999999.99)
                    {
                        oItiTransp.faturamentoMinimo = faturamentoMinimo;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, FATURAMENTO_MINIMO em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, FATURAMENTO_MINIMO é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PrazoEntrega) != null)
                {
                    prazoEntregaValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PrazoEntrega).ToString(), out prazoEntrega);

                    if (prazoEntregaValido && prazoEntrega > 0)
                    {
                        oItiTransp.prazoEntrega = prazoEntrega;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, PRAZO_ENTREGA em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, PRAZO_ENTREGA é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.TipoCobranca) != null)
                {
                    tipoCobrancaValido = Int32.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.TipoCobranca).ToString(), out tipoCobranca);
                }
                else
                {
                    tipoCobrancaValido = false;
                }
                if (!tipoCobrancaValido)
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, TIPO_COBRANCA em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                else
                {
                    tipoCobranca = Convert.ToInt32((excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.TipoCobranca)));

                    oItiTransp.tipoCobranca = tipoCobranca;

                    if ((tipoCobranca != (int)EnumeradoresAplicacao.TipoCobranca.Valor) && (tipoCobranca != (int)EnumeradoresAplicacao.TipoCobranca.Percentual))
                    {
                        erro = true;
                        tipoCobrancaValido = false;
                        listaErro.Add(String.Format("Linha: {0} - Erro, TIPO_COBRANCA deve ser 1 ou 2. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaInicial) != null)
                {
                    vlFaixaInicialValido = Double.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaInicial).ToString(), out vlFaixaInicial);

                    if (vlFaixaInicialValido && vlFaixaInicial >= 0 && vlFaixaInicial <= 999999999999.99)
                    {
                        oItiTransp.valorFaixaInicial = vlFaixaInicial;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, VL_FAIXA_INICIAL em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    vlFaixaInicialValido = false;
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, VL_FAIXA_INICIAL é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaFinal) != null)
                {
                    vlFaixaFinalValido = Double.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.VlFaixaFinal).ToString(), out vlFaixaFinal);

                    if (vlFaixaFinalValido && vlFaixaFinal >= 0 && vlFaixaFinal <=999999999999.99)
                    {
                        oItiTransp.valorFaixaFinal = vlFaixaFinal;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, VL_FAIXA_FINAL em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    vlFaixaFinalValido = false;
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, VL_FAIXA_FINAL é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                if (tipoCobrancaValido)
                {
                    tipoCobranca = Convert.ToInt32((excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.TipoCobranca)));

                    if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.ValorFrete) != null)
                    {
                        valorFreteValido = Double.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.ValorFrete).ToString(), out valorFrete);
                        if (tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Valor && valorFreteValido && valorFrete >= 0 && valorFrete<=999999999999.99)
                        {
                            oItiTransp.valorFrete = valorFrete;
                        }
                        else
                        {
                            valorFreteValido = false;
                        }
                    }
                    else
                    {
                        valorFreteValido = false;
                    }

                    if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PercentualFrete) != null)
                    {
                        percentualFreteValido = Double.TryParse(excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PercentualFrete).ToString(), out percentualFrete);
                        if (tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Percentual && percentualFreteValido && percentualFrete >= 0 && percentualFrete <= 99.99)
                        {
                            oItiTransp.percentualFrete = percentualFrete;
                        }
                        else
                        {
                            percentualFreteValido = false;
                        }
                    }
                    else
                    {
                        percentualFreteValido = false;
                    }
                    
                    if (tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Valor && !valorFreteValido)
                    {
                        if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.ValorFrete) != null)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Linha: {0} - Erro, VALOR_FRETE em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        else
                        {
                            erro = true;
                            listaErro.Add(String.Format("Linha: {0} - Erro, VALOR_FRETE obrigatório quando TIPO_COBRANCA igual a 1. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                    }
                    else
                    {
                        if (tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Percentual && !percentualFreteValido)
                        {
                            if (excelReader.GetValue((int)EnumeradoresAplicacao.ColunaPropriedade.PercentualFrete) != null)
                            {
                                erro = true;
                                listaErro.Add(String.Format("Linha: {0} - Erro, PERCENTUAL_FRETE em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                            }
                            else
                            {
                                erro = true;
                                listaErro.Add(String.Format("Linha: {0} - Erro, PERCENTUAL_FRETE obrigatório quando TIPO_COBRANCA igual a 2. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                            }
                        }
                    }
                }

                if (vlFaixaInicialValido && vlFaixaFinalValido)
                {
                    if (oItiTransp.valorFaixaFinal <= oItiTransp.valorFaixaInicial)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, VL_FAIXA_FINAL não pode ser menor ou igual que VL_FAIXA_INICIAL. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }


                if (erro)
                {
                    return null;
                }
                else
                {
                    return oItiTransp;
                }
            }catch(Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro na importação do Excel", ex);
            }
        }

        /// <summary>
        /// Método responsável por gravar lista de erro do arquivo importado
        /// </summary>
        /// <param name="listaErro"></param>
        /// <param name="nomeArquivo"></param>
        /// <param name="tipolog"></param>
        /// <returns></returns>
        private static String gravarLista(List<string> listaErro, string nomeArquivo, string tipolog)
        {
            String nomeLog = String.Empty;

            try
            {
                if (listaErro != null && !String.IsNullOrEmpty(nomeArquivo))
                {
                    nomeLog = String.Format("LOG_{0}_{1}_{2}.txt",
                        tipolog.ToString(),
                        Path.GetFileName(nomeArquivo).ToString(),
                        DateTime.Now.ToString("ddMMyyyy"));

                    nomeLog = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\Log.PainelFrete\" + nomeLog;

                    if (File.Exists(nomeLog))
                    {
                        File.Delete(nomeLog);
                    }

                    using (var arqGravar = File.CreateText(nomeLog))
                    {
                        arqGravar.WriteLine("<---Log de Importação Itinerario Transportadora--->");
                        listaErro.ForEach(i => arqGravar.WriteLine(i));
                        arqGravar.WriteLine("<---Fim arquivo Log--->");
                        arqGravar.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro ao criar arquivo de log: " + ex.Message);
            }

            return nomeLog;
        }

        /// <summary>
        /// Método responsável por inserir o itinerário transportadora no banco
        /// </summary>
        /// <param name="bgWorkerPoliticaFrete"></param>
        /// <param name="qtdItiTransp"></param>
        /// <returns></returns>
        public static bool InserirItinerarioTransportadora(BackgroundWorker bgWorkerPoliticaFrete, out int qtdItiTransp)
        {
            bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioInsercaoItinerarioTransportadora);
            try
            {
                qtdItiTransp = PoliticaFreteDALC.InserirItinerarioTransportadora();
                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.FimInsercaoItinerarioTransportadora);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerPoliticaFrete.ReportProgress(1, EnumeradoresAplicacao.StatusTransportadoraItinerario.Erro);
                throw new Exception("Erro ao inserir tabela PRODUCAO.ITINERARIO_TRANSPORTADORA: " + ex.Message);
            }
        }

        /// <summary>
        /// Método responsável por buscar as transportadoras de um determinado itinerário e canal
        /// </summary>
        /// <param name="oFiltro"></param>
        /// <returns></returns>
        public static List<ItinerarioTransportadora> BuscarItinerarioTransportadora(FiltroItinerarioTransportadora oFiltro)
        {
            return PoliticaFreteDALC.BuscarItinerarioTransportadora(oFiltro);
        }

        /// <summary>
        /// Método responsável por buscar o itinerário correspondente ao cd origem e cidade destino
        /// </summary>
        /// <param name="cdOrigem"></param>
        /// <param name="cidadeDestino"></param>
        /// <returns></returns>
        public static int BuscarItinerario(int cdOrigem, int cidadeDestino)
        {
            return PoliticaFreteDALC.BuscarItinerario(cdOrigem, cidadeDestino);
        }

        public static bool ValidarPrioridadeTransportadora(List<ItinerarioTransportadora> listaItinerarioTransportadora, int prioridade, int codigoTransportadora)
        {
            bool naoExistePrioridadeDuplicada;

            if (listaItinerarioTransportadora.Count == 0)
            {
                naoExistePrioridadeDuplicada = true;
            }
            else
            {
                var listaPrioridade = (from itiTranspPri in listaItinerarioTransportadora
                                       where itiTranspPri.prioridade == prioridade
                                           && itiTranspPri.codigoTransportadora != codigoTransportadora
                                       select itiTranspPri).ToList();

                if (listaPrioridade.Count > 0)
                {
                    return false;
                }
                else
                {
                    naoExistePrioridadeDuplicada = true;
                }
            }

            return naoExistePrioridadeDuplicada;
        }

        public static bool ValidarPrioridadeSequencial(List<ItinerarioTransportadora> listaItinerarioTransportadora)
        {
            bool prioridateSequencial;
            int prioridademax;
            prioridateSequencial = true;

            var listaItinerario = (from i in listaItinerarioTransportadora
                                   select new { i.codigoItiTransp, i.canal }).Distinct().ToList();

            foreach (var itinerario in listaItinerario)
            {
                var listaTransportadoraItinerario = (from transp in listaItinerarioTransportadora
                                                     where transp.codigoItiTransp == itinerario.codigoItiTransp && transp.canal == itinerario.canal
                                                     select transp).ToList();
                prioridademax = listaTransportadoraItinerario.Max(x => x.prioridade);

                var listaTranspItinerarioSemRepetidas = (from transp in listaItinerarioTransportadora
                                                         where transp.codigoItiTransp == itinerario.codigoItiTransp && transp.canal == itinerario.canal
                                                         select new { transp.codigoTransportadora }).Distinct().ToList();
                if (listaTranspItinerarioSemRepetidas.Count != prioridademax)
                {
                    prioridateSequencial = false;
                }
                else
                {
                    prioridateSequencial = true;
                }
            }

            return prioridateSequencial;
        }

        public static DateTime? ValidarItigerarioVigenciaFutura(FiltroItinerarioTransportadora oFiltro)
        {
            DateTime? dataVigenciaFutura = new DateTime();

            dataVigenciaFutura = PoliticaFreteDALC.ValidarItigerarioVigenciaFutura(oFiltro.codItinerario, oFiltro.canal);

            return dataVigenciaFutura;
        }

        public static void DeletarInserirVigenciaFutura(FiltroItinerarioTransportadora oFiltro, List<ItinerarioTransportadora> listaItinerarioTransportadora)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                PoliticaFreteDALC.DeletarVigenciaFutura(oFiltro);
                foreach (var oItinerarioTransportadora in listaItinerarioTransportadora)
                {
                    PoliticaFreteDALC.InserirVigenciaFutura(oItinerarioTransportadora);
                }
                ts.Complete();
            }
        }

        public static void DeletarVigenciaFutura(FiltroItinerarioTransportadora oFiltro)
        {
            PoliticaFreteDALC.DeletarVigenciaFutura(oFiltro);
        }

        public static void InserirItinerarioTransportadora(List<ItinerarioTransportadora> listaItinerarioTransportadora)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                foreach (var oItinerarioTransportadora in listaItinerarioTransportadora)
                {
                    PoliticaFreteDALC.InserirVigenciaFutura(oItinerarioTransportadora);
                }
                ts.Complete();
            }
        }

        public static bool ValidarFaixasPrazo(List<ItinerarioTransportadora> listaItinerarioTransportadora, ItinerarioTransportadora oItiTransp)
        {
            bool naoExisteFaixasDuplicadas;

            if (listaItinerarioTransportadora.Count == 0)
            {
                naoExisteFaixasDuplicadas = true;
            }
            else
            {
                var listaFaixasDuplicadas = (from itiTranspFaixas in listaItinerarioTransportadora
                                             where itiTranspFaixas.canal == oItiTransp.canal
                                             && itiTranspFaixas.codItinerario == oItiTransp.codItinerario
                                             && (itiTranspFaixas.valorFaixaInicial == oItiTransp.valorFaixaInicial || itiTranspFaixas.valorFaixaFinal == oItiTransp.valorFaixaFinal
                                             || (oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal <= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaFinal && oItiTransp.valorFaixaFinal <= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial >= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaFinal && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaFinal)
                                             || (oItiTransp.valorFaixaInicial <= itiTranspFaixas.valorFaixaInicial && oItiTransp.valorFaixaFinal >= itiTranspFaixas.valorFaixaFinal))
                                             && itiTranspFaixas.codigoTransportadora == oItiTransp.codigoTransportadora
                                             && itiTranspFaixas.prazoEntrega == oItiTransp.prazoEntrega
                                             select itiTranspFaixas).ToList();

                if (listaFaixasDuplicadas.Count > 0)
                {
                    naoExisteFaixasDuplicadas = false;
                }
                else
                {
                    naoExisteFaixasDuplicadas = true;
                }
            }

            return naoExisteFaixasDuplicadas;
        }

        /// <summary>
        /// Método responsável por buscar a cidade através do código do itinerario
        /// </summary>
        /// <param name="codItinerario">código itinerario</param>
        /// <returns>cidade</returns>
        public static Cidade BuscarCidade(Int32 codItinerario)
        {
            return PoliticaFreteDALC.BuscarCidade(codItinerario);
        }


        /// <summary>
        /// Método responsável por verificar se existe politica para um determinado itinerário
        /// </summary>
        /// <param name="oFiltro"></param>
        /// <returns></returns>
        public static bool ExistePolitica(FiltroItinerarioTransportadora oFiltro)
        {
            return PoliticaFreteDALC.ExistePolitica(oFiltro);
        }
    }
}
