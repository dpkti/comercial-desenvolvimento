﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using System.Windows.Forms;
using DALC;

namespace Business
{
    public class PainelParamentroBO
    {
        /// <summary>
        /// Método responsável por buscar os CD Origem.
        /// </summary>
        /// <returns>Descrição do CD</returns>
        public static List<CdOrigemBE> BuscarCdOrigem()
        {
            List<CdOrigemBE> lista = null;
            lista = CadastroItinerarioDALC.BuscarCdOrigem();
            CdOrigemBE cdOrigem = new CdOrigemBE();
            cdOrigem.CD = 0;
            cdOrigem.CDORIGEM = "Selecione";
            lista.Insert(0,cdOrigem);            
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar UF Destino.
        /// </summary>
        /// <returns>Descrição da UF</returns>
        public static List<UfDestinoBE> BuscarUfDestino()
        {
            List<UfDestinoBE> lista = null;
            lista = CadastroItinerarioDALC.BuscarUfDestino();
            UfDestinoBE ufDestino = new UfDestinoBE();
            ufDestino.CDUF = null;
            ufDestino.UF = "Selecione";
            lista.Insert(0, ufDestino);
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar as Cidades de Destino.
        /// </summary>
        /// <returns>Descrição da Cidades</returns>
        public static List<CidadeDestinoBE> BuscarCidadeDestino()
        {
            List<CidadeDestinoBE> lista = null;
            lista = CadastroItinerarioDALC.BuscarCidadeDestino();
            CidadeDestinoBE cidadeDestino = new CidadeDestinoBE();
            cidadeDestino.CODCIDADE = 0;
            cidadeDestino.CIDADE = "Selecione";
            cidadeDestino.UF = null;
            lista.Insert(0, cidadeDestino);
            return lista;
        }
        /// <summary>
        /// Método responsável por buscar as Itinerario.
        /// </summary>
        /// <returns>Descrição da Cidades</returns>
        public static List<CodigoItinerarioBE> BuscarCodigoItinerario()
        {
            List<CodigoItinerarioBE> lista = null;
            lista = PoliticaFreteDALC.BuscarCodigosItinerarios();
            CodigoItinerarioBE cdItinerario = new CodigoItinerarioBE();
            cdItinerario.COD_ITINERARIO = 0;
            cdItinerario.ITINERARIO = "Selecione";
            lista.Insert(0, cdItinerario);
            return lista;
        }
        /// <summary>
        /// Método responsável por buscar Gerentes.
        /// </summary>
        /// <returns>CPF e Nome</returns>
        public static List<ComboGerente> BuscarGerente()
        {
            List<ComboGerente> lista = null;
            lista = GerenteDALC.BuscarGerente();
            ComboGerente gerente = new ComboGerente();
            gerente.CPF = 0;
            gerente.DESCNOME = "Selecione";
            lista.Insert(0, gerente);
            return lista;
        }
        /// <summary>
        /// Método responsável por buscar Filial.
        /// </summary>
        /// <returns>cod e nome filial</returns>
        public static List<ComboFilial> BuscarFilial()
        {
            List<ComboFilial> lista = null;
            lista = GerenteDALC.BuscarFilial();
            ComboFilial filial = new ComboFilial();
            filial.CDFILIAL = 0;
            filial.FILIAL = "Selecione";
            lista.Insert(0, filial);
            return lista;
        }
    }
}
