﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class FaturamentoTransportadoraBO
    {

        /// <summary>
        /// Método responsável por buscar Trasnportadoras por Itinerario para popular a grid.
        /// </summary>
        /// <returns>lista de transportadoras</returns>
        public static List<FaturamentoTransportadoraGrid> BuscarTransportadoraItinerario(FiltroGridTransportadora ofiltro)
        {
            return FaturamentoTransportadoraDALC.BuscaTranportadoraItinerarioGrid(ofiltro);
        }


        /// <summary>
        /// Método responsável por buscar Trasnportadores para popular a grid.
        /// </summary>
        /// <returns>lista de transportadores</returns>
        public static List<FaturamentoTransportadoraGrid> BuscarTransportadora(FiltroGridTransportadora ofiltro)
        {
            //return     F.BuscaTranportadoraGrid(ofiltro);
            return FaturamentoTransportadoraDALC.BuscaTranportadoraGrid(ofiltro);
        }

        /// <summary>
        /// Método responsável por buscar as transportadoras.
        /// </summary>
        /// <returns>lista de transportadoras</returns>
        public static List<CodigoTransportadoraBE> BuscarDescricaoTransportadora()
        {
            return PoliticaFreteDALC.BuscarCodigosTransportadoras();

        }


    }
}
