﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class CadastroItinerarioBO
    {
        /// <summary>
        /// Método responsável por verificar se o itinerário ja existe na base de dados
        /// </summary>
        /// <param name="cdOrigem">Cd Origem</param>
        /// <param name="codCidadeDestino">Cidade Destino</param>
        /// <returns>retorn true se e existe e false caso não exista</returns>
        public static bool VerificarItinerario(int cdOrigem, int codCidadeDestino)
        {
            return CadastroItinerarioDALC.VerificarItinerario(cdOrigem, codCidadeDestino);
        }

        /// <summary>
        /// Método responsável por inserir um novo itinerário
        /// </summary>
        /// <param name="cdOrigem">Cd Origem</param>
        /// <param name="codCidadeDestino">Cidade Destino</param>
        public static void InserirItinerario(int cdOrigem, int codCidadeDestino)
        {
            CadastroItinerarioDALC.InserirItinerario(cdOrigem, codCidadeDestino);
        }

        /// <summary>
        /// Método responsável por consultar os itinerários existentes
        /// </summary>
        /// <param name="oFiltro">Filtro de consulta: cd origem, cidade destino e uf destino</param>
        /// <returns>lista com os itinerarios cadastrados</returns>
        public static List<Itinerario> ConsultarItinerario(FiltroItinerario oFiltro)
        {
            return CadastroItinerarioDALC.ConsultarItinerario(oFiltro);
        }

        /// <summary>
        /// Método responsável por consultar um itinerário específico
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário</param>
        /// <returns>Itinerário a ser alterado</returns>
        public static ResultItinerario ConsultarItinerario(Int32 codigoItinerario)
        {
            return CadastroItinerarioDALC.ConsultarItinerario(codigoItinerario);
        }

        /// <summary>
        /// Método responsável por realizar update na tabel itinerário
        /// </summary>
        /// <param name="codItinerario">código do itinerário</param>
        /// <param name="cdOrigem">cd origem</param>
        /// <param name="codCidadeDestino">cidade destino</param>
        public static void AlterarItinerario(int codItinerario, int cdOrigem, int codCidadeDestino)
        {
            CadastroItinerarioDALC.AlterarItinerario(codItinerario, cdOrigem, codCidadeDestino);
        }

        /// <summary>
        /// Método responsável por consultar as transportadoras de um determinado itinerário
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário</param>
        /// <returns>Lista com as transportadoras</returns>
        public static List<Transportadora> ConsultarTransportadoras(int codigoItinerario)
        {
            return CadastroItinerarioDALC.ConsultarTransportadoras(codigoItinerario);
        }
    }
}
