﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;
using System.Data;
using System.IO;
using Excel;
using UtilGeralDPA;

namespace Business
{
    public class GerenteBO
    {
        /// <summary>
        /// Método responsável por buscar os Gerentes regionais.
        /// </summary>
        /// <returns>Gerentes regionais, limite e saldo</returns>
        public static List<GerenteBE> BuscaGerenteRegionalGrid()
        {
            List<GerenteBE> lista = null;
            lista = GerenteDALC.BuscarGerenteRegionalGrid();
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar historico gerente para popular a grid.
        /// </summary>
        /// <returns>lista de log de alterações</returns>
        public static List<HistoricoGerente> BuscarHistoricoGerente(FiltroHistGerente ofiltro)
        {
            return GerenteDALC.BuscaHistoricoGerente(ofiltro);
        }

        /// <summary>
        /// Método responsável por gravar Gerente.
        /// </summary>
        public static void GravarGerente(FiltroGerenteLimite oFiltro)
        {
            GerenteDALC.GravarGerente(oFiltro);
        }
        /// <summary>
        /// Método responsável por validar se gerente já pertence a outro time de gerente regional
        /// </summary>
        /// <returns>true/false</returns>
        public static bool ValidaGerenteRegional(FiltroGerenteLimite oFiltro)
        {
            Boolean valida;
            valida = GerenteDALC.ValidaGerenteRegional(oFiltro);
            return valida;
        }

        /// <summary>
        /// Método responsável por buscar os Gerentes Limite.
        /// </summary>
        /// <returns>Gerentes, limite e saldo</returns>
        public static List<GerenteLimiteBE> BuscarGerenteLimite(InserirAlterarGerente gerenteRegional)
        {
            List<GerenteLimiteBE> lista = new List<GerenteLimiteBE>();
            lista = GerenteDALC.BuscarGerenteLimite(gerenteRegional);
            return lista;
        }

        /// <summary>
        /// Método responsável por excluir Limite de gerente.
        /// </summary>
        public static void ExcluiLimiteGerente(InserirAlterarGerente oGerenteRegional, UsuarioBE oUser)
        {
            GerenteDALC.ExcluiLimiteGerente(oGerenteRegional, oUser);
        }

        /// <summary>
        /// Método responsável por Verificar se gerente já está castrado.
        /// </summary>
        /// <returns>true/false</returns>
        public static bool ValidaGerenteFilial(FiltroGerenteLimite oFiltro)
        {
            Boolean valida;
            valida = GerenteDALC.ValidaGerenteFilial(oFiltro);
            return valida;
        }

        /// <summary>
        /// Método responsável por buscar os gerentes.
        /// </summary>
        /// <returns>lista de gerentes</returns>
        public static List<ComboGerenteLimite> BuscarGerenteLimite()
        {
            return GerenteDALC.BuscarGerenteLimite();
        }

        /// <summary>
        /// Método responsável por buscar os gerentes regionais
        /// </summary>
        /// <returns>lista de gerentes regionais</returns>
        public static List<ComboGerente> BuscarGerenteRegional()
        {
            return GerenteDALC.BuscarGerenteRegional();
        }

        /// <summary>
        /// Método responsável por buscar os pedidos liberados por gerente
        /// </summary>
        /// <param name="oFiltro">filtro de busca</param>
        /// <returns></returns>
        public static List<PedidoBE> ConsultarPedidosLiberadosGerente(FiltroPedidoGerente oFiltro)
        {
            return GerenteDALC.ConsultarPedidosLiberadosGerente(oFiltro);
        }

        /// <summary>
        /// Método responsável por verificar se o gerente existe na base
        /// </summary>
        /// <param name="cpf">cpf do gerente</param>
        /// <param name="tipo">tipo do gerente</param>
        /// <returns></returns>
        public static bool VerificaGerenteExiste(Int64 cpf, string tipo)
        {
            return GerenteDALC.VerificaGerenteExiste(cpf, tipo);
        }

        /// <summary>
        /// Método responsável por verificar se o gerente possui gerentes relacionados
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public static bool VerificaGerenteRelacionados(Int64 cpf)
        {
            return GerenteDALC.VerificaGerenteRelacionados(cpf);
        }

        /// <summary>
        /// Método responsável por realizar o atualizar/inserir o gerente na base
        /// </summary>
        /// <param name="oGerente"></param>
        public static void CadastrarGerente(Gerente oGerente)
        {
            GerenteDALC.CadastrarGerente(oGerente);
        }

        /// <summary>
        /// Método responsável por buscar os gerentes
        /// </summary>
        /// <param name="cpf"></param>
        /// <param name="nome"></param>
        /// <returns></returns>
        public static List<Gerente> BuscarGerente(string cpf, string nome)
        {
            return GerenteDALC.BuscarGerente(cpf, nome);
        }
        //TI-5946
        /// <summary>
        /// Método responsável por carregar o histórico de alterações de limite 
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns></returns>
        public static List<HistoricoGerente> BuscarHistoricoLimite(long cpf)
        {
            return GerenteDALC.BuscarHistoricoLimite(cpf);
        }
        /// <summary>
        /// Carregar datatable com o arquivo excel 
        /// </summary>
        /// <param name="caminhoArquivo"></param>
        /// <returns></returns>
        public static List<FiltroGerenteLimite> CarregarExcel(List<ComboGerente> lstGerente, List<GerenteBE> lstRegional, List<ComboFilial> lstFilial, string caminhoArquivo, out string retLogGeraArq)
        {
            string caminhoOrigem;
            string arquivo = "";
            string arqListErro = string.Empty;
            bool erro = false;
            IExcelDataReader excelReader = null;
            DataTable dt = new DataTable();
            int count = 0;
            List<FiltroGerenteLimite> lst = new List<FiltroGerenteLimite>();
            List<String> listaErro = new List<String>();

            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                FileStream stream = File.Open(arquivo, FileMode.Open, FileAccess.Read);

                string extension = Path.GetExtension(arquivo);
                if (extension.Equals(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;

                while (excelReader.Read())
                {
                    if (count > 0 && (excelReader.GetValue(0) != null  || excelReader.GetValue(1) != null
                        || excelReader.GetValue(2) != null || excelReader.GetValue(3) != null || excelReader.GetValue(4) != null))
                    {
                        FiltroGerenteLimite oFiltro = new FiltroGerenteLimite();
                        oFiltro = ValidarTipagem(excelReader, listaErro);

                        if (oFiltro == null) 
                        {
                            erro = true;
                            break;
                        }

                        var gerente = lstGerente.Find(g => g.CPF == oFiltro.gerente);
                        var gerenteRegional = lstRegional.Find(r => r.CPF == oFiltro.gerenteRegional);
                        var filial = lstFilial.Find(f => f.CDFILIAL == oFiltro.filial);

                        if (gerente == null)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Gerente {0} não encontrado. em {1}", oFiltro.gerente, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        if (filial == null)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Filial {0} não encontrada. em {1}", oFiltro.filial, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }
                        if (gerente == null)
                        {
                            erro = true;
                            listaErro.Add(String.Format("Gerente Regional {0} não encontrado. em {1}", oFiltro.gerenteRegional, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                        }

                        if (!erro) lst.Add(oFiltro);
                    }

                    count++;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (erro)
                {
                    arqListErro = gravarLista(listaErro, caminhoArquivo, "Erro");
                }
                else
                {
                    arqListErro = string.Empty;
                }

                retLogGeraArq = arqListErro;

                excelReader.Close();

                File.Delete(arquivo);
            }

            return lst;
        }
        /// <summary>
        /// Método responsável por validar a tipagem dos campos do excel importado 
        /// </summary>
        /// <param name="excelReader"></param>
        /// <returns></returns>
        private static FiltroGerenteLimite ValidarTipagem(IExcelDataReader excelReader, List<String> listaErro)
        {
            long gerente;
            int filial;
            long regional;
            double limite;
            string tipo;
            bool erro = false;

            FiltroGerenteLimite gerenteLimite = new FiltroGerenteLimite();

            try
            {
                gerente = long.TryParse(excelReader.GetValue(0).ToString(), out gerente) ? gerente : 0;
                if(gerente == 0)
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, campo CPF inválido. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                filial = int.TryParse(excelReader.GetValue(1).ToString(), out filial) ? filial : 0;
                if (filial == 0)
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, campo COD_FILIAL inválido. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                regional = long.TryParse(excelReader.GetValue(2).ToString(), out regional) ? regional : 0;
                if (regional == 0)
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, campo GERENTE_REGIONAL inválido. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                limite = double.TryParse(excelReader.GetValue(3).ToString(), out limite) ? limite : 0;
                if (limite == 0)
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, campo LIMITE inválido. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }
                tipo = excelReader.GetValue(4).ToString();

                gerenteLimite.gerente = gerente;
                gerenteLimite.filial = filial;
                gerenteLimite.gerenteRegional = regional;
                gerenteLimite.limite = limite;
                gerenteLimite.saldo = limite;
                gerenteLimite.tipo = tipo;
                gerenteLimite.loginAlteracao = GlobalBE.usuarioLogado.LOGIN;

                if (erro)
                {
                    return null;
                }
                else
                {
                    return gerenteLimite;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro na importação do Excel gerente limite", ex);
            }
        }
        /// <summary>
        /// Método responsável por gravar lista de erro do arquivo importado
        /// </summary>
        /// <param name="listaErro"></param>
        /// <param name="nomeArquivo"></param>
        /// <param name="tipolog"></param>
        /// <returns></returns>
        private static string gravarLista(List<string> listaErro, string nomeArquivo, string tipolog)
        {
            String nomeLog = String.Empty;

            try
            {
                if (listaErro != null && !String.IsNullOrEmpty(nomeArquivo))
                {
                    nomeLog = String.Format("LOG_{0}_{1}_{2}.txt",
                        tipolog.ToString(),
                        Path.GetFileName(nomeArquivo).ToString(),
                        DateTime.Now.ToString("ddMMyyyy"));

                    nomeLog = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\Log.PainelFrete\" + nomeLog;

                    if (File.Exists(nomeLog))
                    {
                        File.Delete(nomeLog);
                    }

                    using (var arqGravar = File.CreateText(nomeLog))
                    {
                        arqGravar.WriteLine("<---Log de Importação Itinerario Transportadora--->");
                        listaErro.ForEach(i => arqGravar.WriteLine(i));
                        arqGravar.WriteLine("<---Fim arquivo Log--->");
                        arqGravar.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro ao criar arquivo de log: " + ex.Message);
            }

            return nomeLog;
        }
        //FIM TI-5946
    }
}
