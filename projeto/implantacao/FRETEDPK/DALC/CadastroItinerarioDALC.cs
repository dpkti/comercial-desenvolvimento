﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class CadastroItinerarioDALC
    {
        /// <summary>
        /// Método responsável por buscar CD.
        /// </summary>
        /// <returns>Descrição CD Origem.</returns>
        public static List<CdOrigemBE> BuscarCdOrigem()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<CdOrigemBE> mapper = MapBuilder<CdOrigemBE>.BuildAllProperties();
            IDataReader reader = null;
            CdOrigemBE cdOrigem = null;
            List<CdOrigemBE> listaCdOrigem = new List<CdOrigemBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CDORIGEM))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        cdOrigem = new CdOrigemBE();
                        cdOrigem = mapper.MapRow(reader);
                        listaCdOrigem.Add(cdOrigem);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCdOrigem;
        }

        /// <summary>
        /// Método responsável por buscar UF.
        /// </summary>
        /// <returns>descrição da uf.</returns>
        public static List<UfDestinoBE> BuscarUfDestino()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<UfDestinoBE> mapper = MapBuilder<UfDestinoBE>.BuildAllProperties();
            IDataReader reader = null;
            UfDestinoBE ufDestino = null;
            List<UfDestinoBE> listaUfDestino = new List<UfDestinoBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_UFDESTINO))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        ufDestino = new UfDestinoBE();
                        ufDestino = mapper.MapRow(reader);
                        listaUfDestino.Add(ufDestino);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaUfDestino;
        }

        /// <summary>
        /// Método responsável por buscar Cidade Destino.
        /// </summary>
        /// <returns>descrição da Cidade.</returns>
        public static List<CidadeDestinoBE> BuscarCidadeDestino()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<CidadeDestinoBE> mapper = MapBuilder<CidadeDestinoBE>.BuildAllProperties();
            IDataReader reader = null;
            CidadeDestinoBE cidadeDestino = null;
            List<CidadeDestinoBE> listaCidadeDestino = new List<CidadeDestinoBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CIDADEDESTINO))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        cidadeDestino = new CidadeDestinoBE();
                        cidadeDestino = mapper.MapRow(reader);
                        listaCidadeDestino.Add(cidadeDestino);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCidadeDestino;
        }

        /// <summary>
        /// Método responsável por verificar se o itinerário ja existe na base de dados
        /// </summary>
        /// <param name="cdOrigem">Cd Origem</param>
        /// <param name="codCidadeDestino">Cidade Destino</param>
        /// <returns>retorn true se e existe e false caso não exista</returns>
        public static bool VerificarItinerario(int cdOrigem, int codCidadeDestino)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existeItinerario;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_ITINERARIO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, cdOrigem);
                    db.AddInParameter(cmd, "P_COD_CID_DEST", DbType.Int32, codCidadeDestino);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 20);
                    db.ExecuteNonQuery(cmd);

                    if (Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) == 0)
                    {
                        existeItinerario = false;
                    }
                    else
                    {
                        existeItinerario = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return existeItinerario;
        }

        /// <summary>
        /// Método responsável por inserir um novo itinerário
        /// </summary>
        /// <param name="cdOrigem">Cd Origem</param>
        /// <param name="codCidadeDestino">Cidade Destino</param>
        public static void InserirItinerario(int cdOrigem, int codCidadeDestino)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_ITINERARIO))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, cdOrigem);
                    db.AddInParameter(cmd, "P_COD_CID_DEST", DbType.Int32, codCidadeDestino);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por consultar todos os itinerários de acordo com o filtro
        /// </summary>
        /// <param name="oFiltro">Filtro de consulta: cd origem, cidade destino e uf destino</param>
        /// <returns>Lista de Itinerários</returns>
        public static List<Itinerario> ConsultarItinerario(FiltroItinerario oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<Itinerario> listaItinerario = new List<Itinerario>();
            
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GRD_ITINERARIO))
                {
                    if (oFiltro.cdOrigem != 0)
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, oFiltro.cdOrigem);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, DBNull.Value);
                    }

                    if (oFiltro.codCidadeDestino != 0)
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, oFiltro.codCidadeDestino);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, DBNull.Value);
                    }
                    if (!String.IsNullOrEmpty(oFiltro.ufDestino))
                    {
                        db.AddInParameter(cmd, "P_UF_DESTINO", DbType.String, oFiltro.ufDestino);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_UF_DESTINO", DbType.String, DBNull.Value);
                    }

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            Itinerario oItinerario = new Itinerario();

                            oItinerario.codigo = Convert.ToInt64(reader["COD_ITINERARIO"]);
                            oItinerario.cdOrigem = Convert.ToString(reader["CD_ORIGEM"]);
                            oItinerario.ufOrigem = Convert.ToString(reader["UF_ORIGEM"]);
                            oItinerario.cidadeDestino = Convert.ToString(reader["CIDADE_DESTINO"]);
                            oItinerario.ufDestino = Convert.ToString(reader["UF_DESTINO"]);

                            listaItinerario.Add(oItinerario);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return listaItinerario;
        }
        /// <summary>
        /// Método responsável por consultar um itinerário específico
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário que irá buscar</param>
        /// <returns>Itinerário encontrado</returns>
        public static ResultItinerario ConsultarItinerario(Int32 codigoItinerario)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            ResultItinerario oItinerario = new ResultItinerario();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITINERARIO_COD))
                {
                    db.AddInParameter(cmd, "P_CODIGO_ITINERARIO", DbType.Int32, codigoItinerario);
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        reader.Read();
                        oItinerario.codigo = Convert.ToInt64(reader["COD_ITINERARIO"]);
                        oItinerario.cdOrigem = Convert.ToInt32(reader["COD_LOJA_ORIGEM"]);
                        oItinerario.codCidadeDestino = Convert.ToInt32(reader["COD_CID_DEST"]);
                        oItinerario.ufDestino = reader["UF_DESTINO"].ToString();
                        oItinerario.nomeCidadeDestino = reader["NOME_CIDADE"].ToString();
                        oItinerario.cdOrigemDescricao = reader["LOJA_DESCRICAO"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return oItinerario;
        }

        /// <summary>
        /// Método responsável por atualizar um itinerário existente
        /// </summary>
        /// <param name="codItinerario">código do itinerário</param>
        /// <param name="cdOrigem">cd origem</param>
        /// <param name="codCidadeDestino">código da cidade destino</param>
        public static void AlterarItinerario(int codItinerario, int cdOrigem, int codCidadeDestino)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_UPD_ITINERARIO))
                {
                    db.AddInParameter(cmd, "P_CODIGO_ITINERARIO", DbType.String, codItinerario);
                    db.AddInParameter(cmd, "P_COD_LOJA_ORIGEM", DbType.String, cdOrigem);
                    db.AddInParameter(cmd, "P_COD_CID_DEST", DbType.String, codCidadeDestino);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por consultar as transportadoras de um determinado itinerário
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário</param>
        /// <returns>Lista com as transportadoras</returns>
        public static List<Transportadora> ConsultarTransportadoras(int codigoItinerario)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<Transportadora> listaTransportadora = new List<Transportadora>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITINERARIO_TRANSP))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, codigoItinerario);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            Transportadora oTransportadora = new Transportadora();
                            oTransportadora.codTransportadora = Convert.ToInt16(reader["COD_TRANSP"]);
                            oTransportadora.cnpj = Convert.ToInt64(reader["CGC_TRANSP"]);
                            oTransportadora.nome = Convert.ToString(reader["NOME_TRANSP"]);
                            listaTransportadora.Add(oTransportadora);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return listaTransportadora;
        }
    }
}
