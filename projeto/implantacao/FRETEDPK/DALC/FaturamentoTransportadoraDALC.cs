﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilGeralDPA;
using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class FaturamentoTransportadoraDALC
    {
        
        /// <summary>
        /// Método responsável por buscar transportadoras.
        /// </summary>
        /// <returns>Lista de Transportadoras com frete.</returns>
        public static List<FaturamentoTransportadoraGrid> BuscaTranportadoraGrid(FiltroGridTransportadora ofiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<FaturamentoTransportadoraGrid> listaTransp = new List<FaturamentoTransportadoraGrid>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GRD_TRANSP))
                {

                    db.AddInParameter(cmd, "P_DT_INICIAL", DbType.Date, Convert.ToDateTime(ofiltro.dataInicial));
                    db.AddInParameter(cmd, "P_DT_FINAL", DbType.Date, Convert.ToDateTime(ofiltro.dataFinal));
                    

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            FaturamentoTransportadoraGrid item = new FaturamentoTransportadoraGrid();

                            item.MesAno = reader["mes_ano"].ToString();
                            item.Itinerario = reader["cod_itinerario"].ToString() + " - " + reader["cod_loja"].ToString() + " - " + reader["nome_cidade"].ToString() + " - " + reader["cod_uf"].ToString();
                            item.Transportadora = reader["cod_transp"].ToString() + " - " + reader["cgc_transp"].ToString() + " - " + reader["nome_transp"].ToString();
                            item.ValorFreteRecuperado = Convert.ToDouble(reader["valor_frete"]);

                            listaTransp.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaTransp;
        }

        /// <summary>
        /// Método responsável por buscar transportadoras.
        /// </summary>
        /// <returns>Lista de Transportadoras com frete.</returns>
        public static List<FaturamentoTransportadoraGrid> BuscaTranportadoraItinerarioGrid(FiltroGridTransportadora ofiltro)

        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<FaturamentoTransportadoraGrid> listaTransp = new List<FaturamentoTransportadoraGrid>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GRD_TRANSP_ITI))
                {

                    db.AddInParameter(cmd, "P_DT_INICIAL", DbType.Date, Convert.ToDateTime(ofiltro.dataInicial));
                    db.AddInParameter(cmd, "P_DT_FINAL", DbType.Date, Convert.ToDateTime(ofiltro.dataFinal));

                    if (ofiltro.cdOrigem == 0)
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, ofiltro.cdOrigem);
                    }

                    db.AddInParameter(cmd, "P_COD_UF", DbType.String, ofiltro.ufDestino);

                    if (ofiltro.cdCidadeDestino == 0)
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, ofiltro.cdCidadeDestino);
                    }

                    if (ofiltro.cdTransp == 0)
                    {
                        db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, Convert.ToInt32(ofiltro.cdTransp));
                    }


                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            FaturamentoTransportadoraGrid item = new FaturamentoTransportadoraGrid();

                            item.MesAno = reader["mes_ano"].ToString();
                            item.Itinerario = reader["cod_itinerario"].ToString() + " - " + reader["cod_loja"].ToString() + " - " + reader["nome_cidade"].ToString() + " - " + reader["cod_uf"].ToString();
                            item.Transportadora = reader["cod_transp"].ToString() + " - " + reader["cgc_transp"].ToString() + " - " + reader["nome_transp"].ToString();
                            item.ValorFreteRecuperado = Convert.ToDouble(reader["valor_frete"]);

                            listaTransp.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaTransp;
        }
 
    }
}
