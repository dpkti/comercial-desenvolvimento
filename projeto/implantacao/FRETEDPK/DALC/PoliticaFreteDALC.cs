﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
namespace DALC
{
    public class PoliticaFreteDALC
    {
        /// <summary>
        /// Método responsável por buscar Itinerarios.
        /// </summary>
        /// <returns>Códigos de itinerario.</returns>
        public static List<CodigoItinerarioBE> BuscarCodigosItinerarios()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<CodigoItinerarioBE> mapper = MapBuilder<CodigoItinerarioBE>.BuildAllProperties();
            IDataReader reader = null;
            CodigoItinerarioBE codigoItinerario = null;
            List<CodigoItinerarioBE> listaCodigosItinerarios = new List<CodigoItinerarioBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITINERARIO))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        codigoItinerario = new CodigoItinerarioBE();
                        codigoItinerario = mapper.MapRow(reader);
                        listaCodigosItinerarios.Add(codigoItinerario);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCodigosItinerarios;
        }

        /// <summary>
        /// Método responsável por buscar Canal.
        /// </summary>
        /// <returns>Códigos de Canal.</returns>
        public static List<CodigoCanalBE> BuscarCodigosCanal(string login)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<CodigoCanalBE> mapper = MapBuilder<CodigoCanalBE>.BuildAllProperties();
            IDataReader reader = null;
            CodigoCanalBE codigoCanal = null;
            List<CodigoCanalBE> listaCodigosCanal = new List<CodigoCanalBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CANAL))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, login);//TI-5946
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        codigoCanal = new CodigoCanalBE();
                        codigoCanal = mapper.MapRow(reader);
                        listaCodigosCanal.Add(codigoCanal);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCodigosCanal;
        }

        /// <summary>
        /// Método responsável por buscar Transportadora para popular a grid.
        /// </summary>
        /// <Parametros> filtros de pesquisa da tela </Parametros>   
        /// <returns>lista com transpotadoras.</returns>
        public static List<ItinerarioTransportadoraResultado> BuscaTranportadoraGrid(FiltroGrid oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<ItinerarioTransportadoraResultado> listaItinerarioTransp = new List<ItinerarioTransportadoraResultado>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_TRANSPORTADORA))
                {
                    if (string.IsNullOrEmpty(oFiltro.cdItinerario))
                    {
                        db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, Convert.ToInt32(oFiltro.cdItinerario));
                    }

                    if (string.IsNullOrEmpty(oFiltro.cdTransp))
                    {
                        db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, Convert.ToInt32(oFiltro.cdTransp));
                    }

                    if (oFiltro.cdOrigem == 0)
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CD_ORIGEM", DbType.Int32, oFiltro.cdOrigem);
                    }

                    if (string.IsNullOrEmpty(oFiltro.ufDestino))
                    {
                        db.AddInParameter(cmd, "P_UF_DESTINO", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_UF_DESTINO", DbType.String, oFiltro.ufDestino);
                    }

                    if (oFiltro.cdCidadeDestino == 0)
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CIDADE_DESTINO", DbType.Int32, oFiltro.cdCidadeDestino);
                    }


                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            ItinerarioTransportadoraResultado item = new ItinerarioTransportadoraResultado();
                            item.dataVigencia = Convert.ToDateTime(reader["dataVigencia"]);
                            item.canal = Convert.ToInt32(reader["canal"]);
                            item.codigoTransportadora = reader["codigoTransportadora"].ToString();
                            item.prioridade = Convert.ToInt32(reader["prioridade"]);
                            item.codigoItinerario = Convert.ToInt32(reader["codigoItinerario"]);
                            item.cdOrigem = Convert.ToInt32(reader["cdOrigem"]);
                            item.cidadeDestino = reader["cidadeDestino"].ToString();
                            item.ufDestino = reader["ufDestino"].ToString();
                            item.faturamentoMinimo = Convert.ToDouble(reader["faturamentoMinimo"]);
                            item.prazoEntrega = Convert.ToInt32(reader["prazoEntrega"]);
                            item.tipoCobranca = Convert.ToInt32(reader["tipoCobranca"]);
                            item.valorFaixaInicial = Convert.ToDouble(reader["valorFaixaInicial"]);
                            item.valorFaixaFinal = Convert.ToDouble(reader["valorFaixaFinal"]);
                            item.valorFrete = Convert.ToDouble(reader["valorFrete"]);
                            item.percentualFrete = Convert.ToDouble(reader["percentualFrete"]);

                            listaItinerarioTransp.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaItinerarioTransp;
        }

        /// <summary>
        /// Método responsável por buscar Transportadora.
        /// </summary>
        /// <returns>Lista de Transportadoras.</returns>
        public static List<CodigoTransportadoraBE> BuscarCodigosTransportadoras()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<CodigoTransportadoraBE> mapper = MapBuilder<CodigoTransportadoraBE>.BuildAllProperties();
            IDataReader reader = null;
            CodigoTransportadoraBE codigoTransportadora = null;
            List<CodigoTransportadoraBE> listaCodigosTransportadoras = new List<CodigoTransportadoraBE>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_TRANSP))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        codigoTransportadora = new CodigoTransportadoraBE();
                        codigoTransportadora = mapper.MapRow(reader);
                        listaCodigosTransportadoras.Add(codigoTransportadora);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCodigosTransportadoras;
        }

        /// <summary>
        /// Método responsável por validar se uma transportadora existe na base de dados
        /// </summary>
        /// <param name="codTransportadora">código transportadora</param>
        /// <returns>se existe ou não na base</returns>
        public static bool ValidarTransportadora(int codTransportadora)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existeTransportadora;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_TRANSP))
                {
                    db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, codTransportadora);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 20);
                    db.ExecuteNonQuery(cmd);

                    if (Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) == 0)
                    {
                        existeTransportadora = false;
                    }
                    else
                    {
                        existeTransportadora = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return existeTransportadora;
        }

        /// <summary>
        /// Método responsável por validar se o canal existe
        /// </summary>
        /// <param name="canal">código do canal</param>
        /// <returns>se existe ou não</returns>
        public static bool ValidarCanal(int canal)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existeCanal;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_CANAL))
                {
                    db.AddInParameter(cmd, "P_COD_CANAL", DbType.Int16, canal);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 20);
                    db.ExecuteNonQuery(cmd);

                    if (Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) == 0)
                    {
                        existeCanal = false;
                    }
                    else
                    {
                        existeCanal = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return existeCanal;
        }

        /// <summary>
        /// Método responsável por buscar o código do itinerário
        /// </summary>
        /// <param name="cdOrigem">código do cd origem</param>
        /// <param name="cidadeDestino">código da cidade destino</param>
        /// <returns>código do itinerário</returns>
        public static Int32 BuscarItinerario(int cdOrigem, int cidadeDestino)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            Int32 codItinerario;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITINERARIO_POR_CID_CD))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA_ORIGEM", DbType.Int32, cdOrigem);
                    db.AddInParameter(cmd, "P_COD_CID_DEST", DbType.Int32, cidadeDestino);
                    db.AddOutParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, 20);
                    db.ExecuteNonQuery(cmd);

                    codItinerario = Convert.ToInt32(db.GetParameterValue(cmd, "P_COD_ITINERARIO"));  
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return codItinerario;
        }

        /// <summary>
        /// Método responsável por validar se um código de itinerário existe ou não
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário</param>
        /// <returns>existe ou não</returns>
        public static bool ValidarItinerario(Int32 codigoItinerario)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existeItinerario;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_ITI_POR_COD))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, codigoItinerario);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 20);
                    db.ExecuteNonQuery(cmd);

                    if (Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) == 0)
                    {
                        existeItinerario = false;
                    }
                    else
                    {
                        existeItinerario = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return existeItinerario;
        }

        /// <summary>
        /// Método responsável por verificar se existe um itinerário com vigência futura
        /// </summary>
        /// <param name="codigoItinerario">código do itinerário</param>
        /// <param name="canal">canal do itinerário</param>
        /// <returns>existe ou não itinerário com vigência futura</returns>
        public static DateTime ? ValidarItigerarioVigenciaFutura(int codigoItinerario, int canal)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            DateTime ? dataVigênciaFutura;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_ITI_FUTURO))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, codigoItinerario);
                    db.AddInParameter(cmd, "P_CANAL", DbType.Int32, canal);
                    db.AddOutParameter(cmd, "P_DATA", DbType.Date, 20);
                    db.ExecuteNonQuery(cmd);

                    if (db.GetParameterValue(cmd, "P_DATA") != DBNull.Value)
                    {
                        dataVigênciaFutura = Convert.ToDateTime(db.GetParameterValue(cmd, "P_DATA"));
                    }
                    else
                    {
                        dataVigênciaFutura = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return dataVigênciaFutura;
        }

        /// <summary>
        /// Método responsável por limpar a tabela temporária
        /// </summary>
        public static void LimparStageItinerarioTransportadora()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_DEL_STG_ITI_TRANSP))
                {
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por inserir novos itinerarios transportadora vindo do excel
        /// </summary>
        /// <returns></returns>
        public static int InserirItinerarioTransportadora()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            int qtdInserida;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_ITINERARIO_TRANSP_EXCEL))
                {
                    db.AddOutParameter(cmd, "P_QTE_INSERIDA", DbType.Int32, 8);
                    db.ExecuteNonQuery(cmd);

                    qtdInserida = Convert.ToInt32(db.GetParameterValue(cmd, "P_QTE_INSERIDA"));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return qtdInserida;
        }

        /// <summary>
        /// Método responsável por buscar as transportadoras de um itinerário específico
        /// </summary>
        /// <param name="oFiltro">filtro de busca</param>
        /// <returns>lista de transportadoras</returns>
        public static List<ItinerarioTransportadora> BuscarItinerarioTransportadora(FiltroItinerarioTransportadora oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<ItinerarioTransportadora> listaItinerarioTransp = new List<ItinerarioTransportadora>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_ITINERARIO_TRANSP_IN))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, oFiltro.codItinerario);
                    db.AddInParameter(cmd, "P_COD_CANAL", DbType.Int32, oFiltro.canal);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            ItinerarioTransportadora item = new ItinerarioTransportadora();
                            item.codigoItiTransp = Convert.ToInt64(reader["COD_ITI_TRANSP"]);
                            item.dataVigencia = Convert.ToDateTime(reader["DT_VIGENCIA"]);
                            item.codigoTransportadora = Convert.ToInt32(reader["COD_TRANSP"]);
                            item.prioridade = Convert.ToInt32(reader["PRIOR_TRANSP"]);
                            item.codItinerario = Convert.ToInt32(reader["COD_ITINERARIO"]);
                            item.faturamentoMinimo = Convert.ToDouble(reader["FAT_MINIMO"]);
                            item.prazoEntrega = Convert.ToInt32(reader["PRAZO_ENTREGA"]);
                            item.tipoCobranca = Convert.ToInt32(reader["TP_COBRANCA"]);
                            item.canal = Convert.ToInt32(reader["CANAL"]);
                            item.valorFaixaInicial = Convert.ToDouble(reader["VL_FAIXA_INI"]);
                            item.valorFaixaFinal = Convert.ToDouble(reader["VL_FAIXA_FINAL"]);
                            item.valorFrete = Convert.ToDouble(reader["VL_FRETE"]);
                            item.percentualFrete = Convert.ToDouble(reader["PERC_FRETE"]);
                            item.excluir = false;

                            listaItinerarioTransp.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaItinerarioTransp;
        }

        public static void DeletarVigenciaFutura(FiltroItinerarioTransportadora oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_DEL_ITI_TRANSP))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, oFiltro.codItinerario);
                    db.AddInParameter(cmd, "P_COD_CANAL", DbType.Int32, oFiltro.canal);
                    db.AddInParameter(cmd, "P_DATA_VIGENCIA", DbType.DateTime, oFiltro.dataVigencia);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static void InserirVigenciaFutura(ItinerarioTransportadora oItinerarioTransportadora)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_ITINERARIO_TRANSP))
                {
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.DateTime, oItinerarioTransportadora.dataVigencia);
                    db.AddInParameter(cmd, "P_COD_TRANSP", DbType.Int32, oItinerarioTransportadora.codigoTransportadora);
                    db.AddInParameter(cmd, "P_PRIOR_TRANSP", DbType.Int32, oItinerarioTransportadora.prioridade);
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, oItinerarioTransportadora.codItinerario);
                    db.AddInParameter(cmd, "P_FAT_MINIMO", DbType.Double, oItinerarioTransportadora.faturamentoMinimo);
                    db.AddInParameter(cmd, "P_PRAZO_ENTREGA", DbType.Int32, oItinerarioTransportadora.prazoEntrega);
                    db.AddInParameter(cmd, "P_TP_COBRANCA", DbType.Int16, oItinerarioTransportadora.tipoCobranca);
                    db.AddInParameter(cmd, "P_CANAL", DbType.Int32, oItinerarioTransportadora.canal);
                    db.AddInParameter(cmd, "P_VL_FAIXA_INI", DbType.Double, oItinerarioTransportadora.valorFaixaInicial);
                    db.AddInParameter(cmd, "P_VL_FAIXA_FINAL", DbType.Double, oItinerarioTransportadora.valorFaixaFinal);
                    db.AddInParameter(cmd, "P_VL_FRETE", DbType.Double, oItinerarioTransportadora.valorFrete);
                    db.AddInParameter(cmd, "P_PERC_FRETE", DbType.Double, oItinerarioTransportadora.percentualFrete);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar a cidade através do código do itinerário
        /// </summary>
        /// <param name="codItinerario">código itinerário</param>
        /// <returns>cidade</returns>
        public static Cidade BuscarCidade(Int32 codItinerario)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            Cidade cidade = new Cidade();
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_CID_UF_CD))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, codItinerario);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        reader.Read();
                        cidade.cdOrigem = Convert.ToInt32(reader["COD_LOJA_ORIGEM"]);
                        cidade.codigo = Convert.ToInt32(reader["COD_CIDADE"]);
                        cidade.ufDestino = reader["COD_UF"].ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return cidade;
        }

        /// <summary>
        /// Método responsável por verificar se existe politica para um determinado itinerário
        /// </summary>
        /// <param name="oFiltro"></param>
        /// <returns>se possui ou não política</returns>
        public static bool ExistePolitica(FiltroItinerarioTransportadora oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existePolitica;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_EXISTE_POLITICA))
                {
                    db.AddInParameter(cmd, "P_COD_ITINERARIO", DbType.Int32, oFiltro.codItinerario);
                    db.AddInParameter(cmd, "P_CANAL", DbType.Int32, oFiltro.canal);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int32, 12);
                    db.ExecuteNonQuery(cmd);

                    if (Convert.ToInt32(db.GetParameterValue(cmd, "P_EXISTE")) > 0)
                    {
                        existePolitica = true;
                    }
                    else
                    {
                        existePolitica = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return existePolitica;
        }
    }
}
