﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class GerenteDALC
    {
        /// <summary>
        /// Método responsável por buscar Gerente.
        /// </summary>
        /// <returns>cpf e nome</returns>
        public static List<ComboGerente> BuscarGerente()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<ComboGerente> mapper = MapBuilder<ComboGerente>.BuildAllProperties();
            IDataReader reader = null;
            ComboGerente gerente = null;
            List<ComboGerente> listaGerente = new List<ComboGerente>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTE))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        gerente = new ComboGerente();
                        gerente = mapper.MapRow(reader);
                        listaGerente.Add(gerente);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaGerente;
        }
        /// <summary>
        /// Método responsável por buscar Filial.
        /// </summary>
        /// <returns>Cod e nome filial</returns>
        public static List<ComboFilial> BuscarFilial()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
            IRowMapper<ComboFilial> mapper = MapBuilder<ComboFilial>.BuildAllProperties();
            IDataReader reader = null;
            ComboFilial filial = null;
            List<ComboFilial> listaFilial = new List<ComboFilial>();

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_FILIAL))
                {

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        filial = new ComboFilial();
                        filial = mapper.MapRow(reader);
                        listaFilial.Add(filial);
                    }
                }

            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaFilial;
        }
        /// <summary>
        /// Método responsável por buscar historico de gerente.
        /// </summary>
        /// <Parametros> filtros de pesquisa da tela </Parametros>   
        /// <returns>lista com log de alterações.</returns>
        public static List<HistoricoGerente> BuscaHistoricoGerente(FiltroHistGerente oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<HistoricoGerente> listaHistoricoGerente = new List<HistoricoGerente>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_HISTORICO_GERENTE))
                {
                    
                    if (oFiltro.CPF == 0)
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, DBNull.Value);
                        db.AddInParameter(cmd, "P_TIPO", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, oFiltro.CPF);
                        db.AddInParameter(cmd, "P_TIPO", DbType.String, oFiltro.TIPO);
                    }
                    if (string.IsNullOrEmpty(oFiltro.DTINI))
                    {
                        db.AddInParameter(cmd, "P_DT_INICIO", DbType.Date, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_DT_INICIO", DbType.Date, Convert.ToDateTime(oFiltro.DTINI).ToShortDateString());
                    }
                    if (string.IsNullOrEmpty(oFiltro.DTFIM))
                    {
                        db.AddInParameter(cmd, "P_DT_FIM", DbType.Date, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_DT_FIM", DbType.Date, Convert.ToDateTime(oFiltro.DTFIM).ToShortDateString());
                    }
                                        
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            HistoricoGerente item = new HistoricoGerente();
                            item.DATA = Convert.ToDateTime(reader["DATA"]);
                            item.NOME = reader["NOME"].ToString();
                            item.FILIAL = reader["FILIAL"].ToString();
                            item.GERENTEREGIONAL = reader["GERENTEREGIONAL"].ToString();
                            item.LIMITEANT = string.IsNullOrEmpty(reader["LIMITEANT"].ToString()) ? 0 : Convert.ToDouble(reader["LIMITEANT"]);
                            item.SALDOANT = string.IsNullOrEmpty(reader["SALDOANT"].ToString()) ? 0 : Convert.ToDouble(reader["SALDOANT"]);
                            item.LIMITE = Convert.ToDouble(reader["LIMITE"]);
                            item.SALDO = Convert.ToDouble(reader["SALDO"]);
                            item.USUARIO = reader["USUARIO"].ToString();
                            if (reader["OPERACAO"].ToString() == "D")
                            {
                                item.OPERACAO = "EXCLUÍDO";
                            }
                            else if (reader["OPERACAO"].ToString() == "A")
                            {
                                item.OPERACAO = "ALTERADO";
                            }
                            else if (reader["OPERACAO"].ToString() == "I")
                            {
                                item.OPERACAO = "NOVO";
                            }
                            
                            listaHistoricoGerente.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaHistoricoGerente;
        }

        /// <summary>
        /// Método responsável por buscar historico de gerente regional.
        /// </summary>
        /// <returns>lista com gerentes regionais.</returns>

        public static List<GerenteBE> BuscarGerenteRegionalGrid()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<GerenteBE> listaGerente = new List<GerenteBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTE_REGIONAL))
                {
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            GerenteBE item = new GerenteBE();
                            item.CPF = Convert.ToInt64(reader["CPF"]);
                            item.NOME = reader["NOME"].ToString();
                            item.LIMITE = string.IsNullOrEmpty(reader["LIMITE"].ToString()) ? 0 : Convert.ToDouble(reader["LIMITE"]);
                            item.SALDO = string.IsNullOrEmpty(reader["SALDO"].ToString()) ? 0 : Convert.ToDouble(reader["SALDO"]);
                            listaGerente.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaGerente; ;
        }

         /// <summary>
        /// Método responsável por gravar alterar limite gerente.
        /// </summary>
        public static void GravarGerente(FiltroGerenteLimite oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_INS_GERENTELIMITE))
                {
                    db.AddInParameter(cmd, "P_CPF", DbType.Int64, oFiltro.gerente);
                    db.AddInParameter(cmd, "P_TIPO", DbType.String, oFiltro.tipo);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int64, oFiltro.filial);
                    db.AddInParameter(cmd, "P_GERENTE_REGIONAL", DbType.Int64, oFiltro.gerenteRegional);
                    db.AddInParameter(cmd, "P_LIMITE", DbType.Double, oFiltro.limite);
                    db.AddInParameter(cmd, "P_SALDO", DbType.Double, oFiltro.saldo);
                    db.AddInParameter(cmd, "P_LOGIN_ALTERACAO", DbType.String, oFiltro.loginAlteracao);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }


        /// <summary>
        /// Método responsável por validar se gerente já pertence a outro gerente regional.
        /// </summary>
        public static bool ValidaGerenteRegional(FiltroGerenteLimite oFiltro)
        {
            Boolean valida;
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_GERENTE_LIMITE))
            {
                db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, oFiltro.gerente);
                db.AddInParameter(cmd, "P_CPF_GERENTE_REGIONAL", DbType.Int64, oFiltro.gerenteRegional);
                db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16,1);
                db.ExecuteNonQuery(cmd);

                valida = Convert.ToInt16(db.GetParameterValue(cmd,"P_EXISTE")) > 0 ? false : true;
                
            }
            
            return valida;
        }

        /// <summary>
        /// Método responsável por buscar gerente limite por gerente regional.
        /// </summary>
        /// <returns>lista com gerentes limite.</returns>
        public static List<GerenteLimiteBE> BuscarGerenteLimite(InserirAlterarGerente gerenteRegional)
        {
             DateTime dataInicio = Logger.LogInicioMetodo();
            List<GerenteLimiteBE> lista = new List<GerenteLimiteBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_LISTA_GERENTE_LIMITE))
                {
                    
                    if (gerenteRegional.CPF_REGIONAL == 0)
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE_REGIONAL", DbType.Int64, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE_REGIONAL", DbType.Int64, gerenteRegional.CPF_REGIONAL);
                    }
                                        
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            GerenteLimiteBE item = new GerenteLimiteBE();
                            item.CPF = Convert.ToInt64(reader["CPF"]);
                            item.TIPO = reader["TIPO"].ToString();
                            item.NOME = reader["NOME"].ToString();
                            item.FILIAL = reader["FILIAL"].ToString();
                            item.COD_FILIAL = Convert.ToInt16(reader["COD_FILIAL"]);
                            item.LIMITE = Convert.ToDouble(reader["LIMITE"]);
                            item.SALDO = Convert.ToDouble(reader["SALDO"]);
                            
                            lista.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return lista;
        }

        /// <summary>
        /// Método responsável por excluir limite de gerente.
        /// </summary>
        public static void ExcluiLimiteGerente(InserirAlterarGerente oGerenteRegional, UsuarioBE oUser)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_DEL_LIMITE_GERENTE))
                {
                    db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, oGerenteRegional.CPF);
                    db.AddInParameter(cmd, "P_TIPO", DbType.String, oGerenteRegional.TIPO);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int64, oGerenteRegional.CDFILIAL);
                    db.AddInParameter(cmd, "P_LOGIN_ALTERACAO", DbType.String, oUser.LOGIN);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por validar se gerente já foi gravado.
        /// </summary>
        public static bool ValidaGerenteFilial(FiltroGerenteLimite oFiltro)
        {
            Boolean valida;
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_VALIDA_GERENTE_FILIAL))
            {
                db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, oFiltro.gerente);
                db.AddInParameter(cmd, "P_FILIAL", DbType.Int64, oFiltro.filial);
                db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16,1);
                db.ExecuteNonQuery(cmd);

                valida = Convert.ToInt16(db.GetParameterValue(cmd,"P_EXISTE")) > 0 ? false : true;
                
            }
            
            return valida;
        }

        /// <summary>
        /// Método responsável por buscar os gerentes.
        /// </summary>
        /// <returns>lista com gerentes.</returns>
        public static List<ComboGerenteLimite> BuscarGerenteLimite()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<ComboGerenteLimite> listaGerente = new List<ComboGerenteLimite>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTE_LIMITE))
                {
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            ComboGerenteLimite gerente = new ComboGerenteLimite();
                            gerente.CPF = Convert.ToInt64(reader["CPF"]);
                            gerente.NOME = reader["NOME"].ToString();
                            gerente.CPFREGIONAL = Convert.ToInt64(reader["GERENTE_REGIONAL"]);
                            listaGerente.Add(gerente);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaGerente; ;
        }

        /// <summary>
        /// Método responsável por buscar os gerentes regionais.
        /// </summary>
        /// <returns>lista com gerentes regionais.</returns>
        public static List<ComboGerente> BuscarGerenteRegional()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<ComboGerente> listaGerente = new List<ComboGerente>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTE_REGIONAL_REL))
                {
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            ComboGerente gerente = new ComboGerente();
                            gerente.CPF = Convert.ToInt64(reader["CPF"]);
                            gerente.NOME = reader["NOME"].ToString();
                            listaGerente.Add(gerente);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return listaGerente;
        }

        /// <summary>
        /// Método responsável por buscar pedidos liberados por gerente.
        /// </summary>
        /// <Parametros> filtros de pesquisa da tela </Parametros>   
        /// <returns>lista com os pedidos.</returns>
        public static List<PedidoBE> ConsultarPedidosLiberadosGerente(FiltroPedidoGerente oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<PedidoBE> listaPedidosLiberados = new List<PedidoBE>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_PEDIDOS_LIB_GERENTE))
                {

                    if (oFiltro.cpfGerenteRegional == 0)
                    {
                        db.AddInParameter(cmd, "P_CPF_REGIONAL", DbType.Int64, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CPF_REGIONAL", DbType.Int64, oFiltro.cpfGerenteRegional);
                    }
                    if (oFiltro.cpfGerente == 0)
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, oFiltro.cpfGerente);
                    }
                    if (string.IsNullOrEmpty(oFiltro.dtIni))
                    {
                        db.AddInParameter(cmd, "P_DATA_INI", DbType.Date, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_DATA_INI", DbType.Date, Convert.ToDateTime(oFiltro.dtIni).ToShortDateString());
                    }
                    if (string.IsNullOrEmpty(oFiltro.dtFim))
                    {
                        db.AddInParameter(cmd, "P_DATA_FIM", DbType.Date, DBNull.Value);
                    }
                    else
                    {
                        TimeSpan time = new TimeSpan(0, 23, 59, 59);
                        db.AddInParameter(cmd, "P_DATA_FIM", DbType.Date, Convert.ToDateTime(oFiltro.dtFim).Add(time));
                    }

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            PedidoBE pedido = new PedidoBE();
                            pedido.nomeGerenteRegional = reader["NOME_GERENTE_REGIONAL"].ToString();
                            pedido.nomeGerente = reader["NOME_GERENTE"].ToString();
                            pedido.filial = reader["FILIAL"].ToString();
                            pedido.codLoja = Convert.ToInt16(reader["COD_LOJA"]);
                            pedido.numPedido = reader["NUM_PEDIDO"].ToString();
                            pedido.seqPedido = reader["SEQ_PEDIDO"].ToString();
                            if (reader["NUM_NOTA"] != DBNull.Value)
                            {
                                pedido.numNota = reader["NUM_NOTA"].ToString();
                                pedido.dataEmissao = reader["DT_EMISSAO_NOTA"].ToString();
                            }
                            pedido.dataLiberacao = reader["DT_LIBERACAO"].ToString();
                            pedido.valorNota = Convert.ToDouble(reader["VL_CONTABIL"]);
                            pedido.vlFreteCalculado = Convert.ToDouble(reader["VL_FRETE_CALCULADO"]);
                            pedido.vlPagoGerente = Convert.ToDouble(reader["VL_PAGO_GERENTE"]);

                            listaPedidosLiberados.Add(pedido);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaPedidosLiberados;
        }

        /// <summary>
        /// Método responsável por verificar se o gerente + tipo existe na base de dados
        /// </summary>
        /// <param name="cpf">cpf do gerente</param>
        /// <param name="tipo">tipo do gerente</param>
        /// <returns></returns>
        public static bool VerificaGerenteExiste(Int64 cpf, string tipo)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            bool existe = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_EXISTE_GERENTE))
                {

                    db.AddInParameter(cmd, "P_CPF", DbType.Int64, cpf);
                    db.AddInParameter(cmd, "P_TIPO", DbType.String, tipo);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 8);

                    db.ExecuteNonQuery(cmd);

                    existe = Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) > 0 ? true : false;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return existe;
        }

        /// <summary>
        /// Método responsável por verificar se o gerente regionar possui gerentes relacionados a ele
        /// </summary>
        /// <param name="cpf">CPF do gerente</param>
        /// <returns></returns>
        public static bool VerificaGerenteRelacionados(Int64 cpf)
        {
            Boolean existe;
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTES_RELACIONADOS))
                {
                    db.AddInParameter(cmd, "P_CPF", DbType.Int64, cpf);
                    db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 8);
                    db.ExecuteNonQuery(cmd);

                    existe = Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return existe;
        }

        /// <summary>
        /// Método responsável por realizar o atualizar/inserir o gerente na base
        /// </summary>
        /// <param name="oGerente"></param>
        public static void CadastrarGerente(Gerente oGerente)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_MERGE_GERENTE))
                {
                    db.AddInParameter(cmd, "P_CPF", DbType.Int64, oGerente.cpf);
                    db.AddInParameter(cmd, "P_NOME", DbType.String, oGerente.nome);
                    db.AddInParameter(cmd, "P_TIPO", DbType.String, oGerente.tipo);
                    db.AddInParameter(cmd, "P_SITUACAO", DbType.Int16, oGerente.situacao);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar os gerentes
        /// </summary>
        /// <param name="cpf"></param>
        /// <param name="nome"></param>
        /// <returns></returns>
        public static List<Gerente> BuscarGerente(string cpf, string nome)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<Gerente> listaGerentes = new List<Gerente>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_GERENTE_CAD))
                {

                    if (String.IsNullOrEmpty(cpf))
                    {
                        db.AddInParameter(cmd, "P_CPF", DbType.Int64, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_CPF", DbType.Int64, Convert.ToInt64(cpf));
                    }
                    if (String.IsNullOrEmpty(nome))
                    {
                        db.AddInParameter(cmd, "P_NOME", DbType.String, DBNull.Value);
                    }
                    else
                    {
                        db.AddInParameter(cmd, "P_NOME", DbType.String, nome);
                    }

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            Gerente oGerente = new Gerente();
                            oGerente.cpf = Convert.ToInt64(reader["CPF"]);
                            oGerente.nome = reader["NOME"].ToString();
                            oGerente.tipo = reader["TIPO"].ToString();
                            oGerente.situacao = Convert.ToInt16(reader["SITUACAO"]);
                            listaGerentes.Add(oGerente);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaGerentes;
        }

        /// <summary>
        /// Método responsável por carregar o histórico de alterações de limite TI-5946
        /// </summary>
        /// <returns>lista com histórico de limite.</returns>
        public static List<HistoricoGerente> BuscarHistoricoLimite(long cpf)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            List<HistoricoGerente> lista = new List<HistoricoGerente>();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Parametros.CONN_ORA_DPK);
                using (DbCommand cmd = db.GetStoredProcCommand(Parametros.PR_SEL_HISTORICO_LIMITE))
                {

                    if (cpf == 0)
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, DBNull.Value);
                    else
                        db.AddInParameter(cmd, "P_CPF_GERENTE", DbType.Int64, cpf);

                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        while (reader.Read())
                        {
                            HistoricoGerente item = new HistoricoGerente();
                            item.DATA = Convert.ToDateTime(reader["DATA"]);
                            item.NOME = reader["NOME"].ToString();
                            item.FILIAL = reader["FILIAL"].ToString();
                            item.GERENTEREGIONAL = reader["GERENTEREGIONAL"].ToString();
                            item.LIMITEANT = string.IsNullOrEmpty(reader["LIMITEANT"].ToString()) ? 0 : Convert.ToDouble(reader["LIMITEANT"]);
                            item.SALDOANT = string.IsNullOrEmpty(reader["SALDOANT"].ToString()) ? 0 : Convert.ToDouble(reader["SALDOANT"]);
                            item.LIMITE = Convert.ToDouble(reader["LIMITE"]);
                            item.SALDO = Convert.ToDouble(reader["SALDO"]);
                            item.USUARIO = reader["USUARIO"].ToString();
                            if (reader["OPERACAO"].ToString() == "D")
                                item.OPERACAO = "EXCLUÍDO";
                            else if (reader["OPERACAO"].ToString() == "A")
                                item.OPERACAO = "ALTERADO";
                            else if (reader["OPERACAO"].ToString() == "I")
                                item.OPERACAO = "NOVO";

                            lista.Add(item);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return lista;
        }

    }
}
