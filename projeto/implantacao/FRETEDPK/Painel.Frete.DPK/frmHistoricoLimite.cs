﻿using System;
using System.Collections.Generic;
using Entities;
using Business;
using System.Windows.Forms;
using System.Data;
using UtilGeralDPA;
//TI-5946
namespace PainelFreteDPK
{
    public partial class frmHistoricoLimite : Form
    {
        public frmHistoricoLimite(long cpf)
        {
            InitializeComponent();
            CarregarHistorico(cpf);
        }
        private void CarregarHistorico(long cpf)
        {
            try
            {
                grdHistoricoLimite.AutoGenerateColumns = false;
                List<HistoricoGerente> lista = GerenteBO.BuscarHistoricoLimite(cpf);
                grdHistoricoLimite.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();

                //Adding the Columns
                foreach (DataGridViewColumn column in grdHistoricoLimite.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in grdHistoricoLimite.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                if (grdHistoricoLimite.Rows.Count < 100)
                {
                    for (int j = grdHistoricoLimite.Rows.Count; j <= (grdHistoricoLimite.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= grdHistoricoLimite.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }

}
