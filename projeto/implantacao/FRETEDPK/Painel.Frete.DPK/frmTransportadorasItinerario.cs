﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace PainelFreteDPK
{
    public partial class frmTransportadorasItinerario : Form
    {
        public frmTransportadorasItinerario(Int32 codigoItinerario)
        {
            InitializeComponent();
            lbCodigoItinerarioAlt.Text = codigoItinerario.ToString();
            CarregarItinerário(codigoItinerario);
            CarregarTransportadora(codigoItinerario);
        }

        private void CarregarTransportadora(int codigoItinerario)
        {
            List<Transportadora> listaTransportadoras = new List<Transportadora>();
            listaTransportadoras = CadastroItinerarioBO.ConsultarTransportadoras(codigoItinerario);
            dgvTransportadoras.DataSource = listaTransportadoras;
        }

        private void CarregarItinerário(Int32 codigoItinerario)
        {
            ResultItinerario oItinerario = CadastroItinerarioBO.ConsultarItinerario(codigoItinerario);
            lbCdOrigemAlt.Text = oItinerario.cdOrigemDescricao;
            lbCiadeDestinoAlt.Text = oItinerario.nomeCidadeDestino;
            lbUfDestinoAlt.Text = oItinerario.ufDestino;
        }
    }
}
