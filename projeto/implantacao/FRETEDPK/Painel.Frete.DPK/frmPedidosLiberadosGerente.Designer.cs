﻿namespace PainelFreteDPK
{
    partial class frmPedidosLiberadosGerente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPedidosLiberados = new System.Windows.Forms.DataGridView();
            this.lbGerenteRegional = new System.Windows.Forms.Label();
            this.cmbGerenteRegional = new System.Windows.Forms.ComboBox();
            this.cmbGerente = new System.Windows.Forms.ComboBox();
            this.lbGerente = new System.Windows.Forms.Label();
            this.dtpDataFim = new System.Windows.Forms.DateTimePicker();
            this.dtpDataInicio = new System.Windows.Forms.DateTimePicker();
            this.lblDtFim = new System.Windows.Forms.Label();
            this.lblDtInicio = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.bgwPedidos = new System.ComponentModel.BackgroundWorker();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.nomeGerenteRegional = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomeGerenteFilial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codFilial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codLoja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.seqPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroNota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtEmissao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataLiberacaoPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlNota = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlFrete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vlCobradoGerente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidosLiberados)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPedidosLiberados
            // 
            this.dgvPedidosLiberados.AllowUserToAddRows = false;
            this.dgvPedidosLiberados.AllowUserToDeleteRows = false;
            this.dgvPedidosLiberados.AllowUserToOrderColumns = true;
            this.dgvPedidosLiberados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPedidosLiberados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nomeGerenteRegional,
            this.nomeGerenteFilial,
            this.codFilial,
            this.codLoja,
            this.numPedido,
            this.seqPedido,
            this.numeroNota,
            this.dtEmissao,
            this.DataLiberacaoPedido,
            this.vlNota,
            this.vlFrete,
            this.vlCobradoGerente});
            this.dgvPedidosLiberados.Location = new System.Drawing.Point(12, 102);
            this.dgvPedidosLiberados.Name = "dgvPedidosLiberados";
            this.dgvPedidosLiberados.ReadOnly = true;
            this.dgvPedidosLiberados.RowTemplate.Height = 24;
            this.dgvPedidosLiberados.Size = new System.Drawing.Size(1296, 484);
            this.dgvPedidosLiberados.TabIndex = 0;
            // 
            // lbGerenteRegional
            // 
            this.lbGerenteRegional.AutoSize = true;
            this.lbGerenteRegional.Location = new System.Drawing.Point(9, 29);
            this.lbGerenteRegional.Name = "lbGerenteRegional";
            this.lbGerenteRegional.Size = new System.Drawing.Size(124, 17);
            this.lbGerenteRegional.TabIndex = 1;
            this.lbGerenteRegional.Text = "Gerente Regional:";
            // 
            // cmbGerenteRegional
            // 
            this.cmbGerenteRegional.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGerenteRegional.FormattingEnabled = true;
            this.cmbGerenteRegional.Location = new System.Drawing.Point(139, 26);
            this.cmbGerenteRegional.Name = "cmbGerenteRegional";
            this.cmbGerenteRegional.Size = new System.Drawing.Size(424, 24);
            this.cmbGerenteRegional.TabIndex = 2;
            this.cmbGerenteRegional.SelectedIndexChanged += new System.EventHandler(this.cmbGerenteRegional_SelectedIndexChanged);
            // 
            // cmbGerente
            // 
            this.cmbGerente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGerente.FormattingEnabled = true;
            this.cmbGerente.Location = new System.Drawing.Point(658, 26);
            this.cmbGerente.Name = "cmbGerente";
            this.cmbGerente.Size = new System.Drawing.Size(424, 24);
            this.cmbGerente.TabIndex = 4;
            this.cmbGerente.SelectedIndexChanged += new System.EventHandler(this.cmbGerente_SelectedIndexChanged);
            // 
            // lbGerente
            // 
            this.lbGerente.AutoSize = true;
            this.lbGerente.Location = new System.Drawing.Point(588, 29);
            this.lbGerente.Name = "lbGerente";
            this.lbGerente.Size = new System.Drawing.Size(64, 17);
            this.lbGerente.TabIndex = 3;
            this.lbGerente.Text = "Gerente:";
            // 
            // dtpDataFim
            // 
            this.dtpDataFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFim.Location = new System.Drawing.Point(305, 68);
            this.dtpDataFim.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataFim.Name = "dtpDataFim";
            this.dtpDataFim.ShowCheckBox = true;
            this.dtpDataFim.Size = new System.Drawing.Size(132, 22);
            this.dtpDataFim.TabIndex = 27;
            // 
            // dtpDataInicio
            // 
            this.dtpDataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataInicio.Location = new System.Drawing.Point(87, 68);
            this.dtpDataInicio.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataInicio.Name = "dtpDataInicio";
            this.dtpDataInicio.ShowCheckBox = true;
            this.dtpDataInicio.Size = new System.Drawing.Size(132, 22);
            this.dtpDataInicio.TabIndex = 26;
            // 
            // lblDtFim
            // 
            this.lblDtFim.AutoSize = true;
            this.lblDtFim.Location = new System.Drawing.Point(238, 69);
            this.lblDtFim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDtFim.Name = "lblDtFim";
            this.lblDtFim.Size = new System.Drawing.Size(68, 17);
            this.lblDtFim.TabIndex = 29;
            this.lblDtFim.Text = "Data Fim:";
            // 
            // lblDtInicio
            // 
            this.lblDtInicio.AutoSize = true;
            this.lblDtInicio.Location = new System.Drawing.Point(10, 69);
            this.lblDtInicio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDtInicio.Name = "lblDtInicio";
            this.lblDtInicio.Size = new System.Drawing.Size(78, 17);
            this.lblDtInicio.TabIndex = 28;
            this.lblDtInicio.Text = "Data Início:";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(1200, 58);
            this.btnConsultar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(109, 39);
            this.btnConsultar.TabIndex = 30;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(1085, 58);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(109, 39);
            this.btnLimpar.TabIndex = 31;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // bgwPedidos
            // 
            this.bgwPedidos.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPedidos_DoWork);
            this.bgwPedidos.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwPedidos_RunWorkerCompleted);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Location = new System.Drawing.Point(1193, 591);
            this.btnExportarExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(115, 39);
            this.btnExportarExcel.TabIndex = 32;
            this.btnExportarExcel.Text = "Exportar Excel";
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // nomeGerenteRegional
            // 
            this.nomeGerenteRegional.DataPropertyName = "nomeGerenteRegional";
            this.nomeGerenteRegional.HeaderText = "Gerente Regional";
            this.nomeGerenteRegional.Name = "nomeGerenteRegional";
            this.nomeGerenteRegional.ReadOnly = true;
            this.nomeGerenteRegional.Width = 170;
            // 
            // nomeGerenteFilial
            // 
            this.nomeGerenteFilial.DataPropertyName = "nomeGerente";
            this.nomeGerenteFilial.HeaderText = "Gerente";
            this.nomeGerenteFilial.Name = "nomeGerenteFilial";
            this.nomeGerenteFilial.ReadOnly = true;
            this.nomeGerenteFilial.Width = 300;
            // 
            // codFilial
            // 
            this.codFilial.DataPropertyName = "filial";
            this.codFilial.HeaderText = "Filial";
            this.codFilial.Name = "codFilial";
            this.codFilial.ReadOnly = true;
            this.codFilial.Width = 200;
            // 
            // codLoja
            // 
            this.codLoja.DataPropertyName = "codLoja";
            this.codLoja.HeaderText = "Código Loja";
            this.codLoja.Name = "codLoja";
            this.codLoja.ReadOnly = true;
            this.codLoja.Width = 120;
            // 
            // numPedido
            // 
            this.numPedido.DataPropertyName = "numPedido";
            this.numPedido.HeaderText = "Número Pedido";
            this.numPedido.Name = "numPedido";
            this.numPedido.ReadOnly = true;
            this.numPedido.Width = 150;
            // 
            // seqPedido
            // 
            this.seqPedido.DataPropertyName = "seqPedido";
            this.seqPedido.HeaderText = "Seq. Pedido";
            this.seqPedido.Name = "seqPedido";
            this.seqPedido.ReadOnly = true;
            this.seqPedido.Width = 110;
            // 
            // numeroNota
            // 
            this.numeroNota.DataPropertyName = "numNota";
            this.numeroNota.HeaderText = "Número Nota";
            this.numeroNota.Name = "numeroNota";
            this.numeroNota.ReadOnly = true;
            this.numeroNota.Width = 120;
            // 
            // dtEmissao
            // 
            this.dtEmissao.DataPropertyName = "dataEmissao";
            this.dtEmissao.HeaderText = "Data Emissão";
            this.dtEmissao.Name = "dtEmissao";
            this.dtEmissao.ReadOnly = true;
            this.dtEmissao.Width = 160;
            // 
            // DataLiberacaoPedido
            // 
            this.DataLiberacaoPedido.DataPropertyName = "dataLiberacao";
            this.DataLiberacaoPedido.HeaderText = "Data Liberação";
            this.DataLiberacaoPedido.Name = "DataLiberacaoPedido";
            this.DataLiberacaoPedido.ReadOnly = true;
            this.DataLiberacaoPedido.Width = 200;
            // 
            // vlNota
            // 
            this.vlNota.DataPropertyName = "valorNota";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.vlNota.DefaultCellStyle = dataGridViewCellStyle1;
            this.vlNota.HeaderText = "Valor Nota";
            this.vlNota.Name = "vlNota";
            this.vlNota.ReadOnly = true;
            // 
            // vlFrete
            // 
            this.vlFrete.DataPropertyName = "vlFreteCalculado";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.vlFrete.DefaultCellStyle = dataGridViewCellStyle2;
            this.vlFrete.HeaderText = "Valor Frete";
            this.vlFrete.Name = "vlFrete";
            this.vlFrete.ReadOnly = true;
            this.vlFrete.Width = 110;
            // 
            // vlCobradoGerente
            // 
            this.vlCobradoGerente.DataPropertyName = "vlPagoGerente";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.vlCobradoGerente.DefaultCellStyle = dataGridViewCellStyle3;
            this.vlCobradoGerente.HeaderText = "Valor Desc. Gerente";
            this.vlCobradoGerente.Name = "vlCobradoGerente";
            this.vlCobradoGerente.ReadOnly = true;
            this.vlCobradoGerente.Width = 160;
            // 
            // frmPedidosLiberadosGerente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1321, 637);
            this.Controls.Add(this.btnExportarExcel);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.dtpDataFim);
            this.Controls.Add(this.dtpDataInicio);
            this.Controls.Add(this.lblDtFim);
            this.Controls.Add(this.lblDtInicio);
            this.Controls.Add(this.cmbGerente);
            this.Controls.Add(this.lbGerente);
            this.Controls.Add(this.cmbGerenteRegional);
            this.Controls.Add(this.lbGerenteRegional);
            this.Controls.Add(this.dgvPedidosLiberados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPedidosLiberadosGerente";
            this.Text = "Relatório Pedidos Liberados Gerente";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidosLiberados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPedidosLiberados;
        private System.Windows.Forms.Label lbGerenteRegional;
        private System.Windows.Forms.ComboBox cmbGerenteRegional;
        private System.Windows.Forms.ComboBox cmbGerente;
        private System.Windows.Forms.Label lbGerente;
        private System.Windows.Forms.DateTimePicker dtpDataFim;
        private System.Windows.Forms.DateTimePicker dtpDataInicio;
        private System.Windows.Forms.Label lblDtFim;
        private System.Windows.Forms.Label lblDtInicio;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnLimpar;
        private System.ComponentModel.BackgroundWorker bgwPedidos;
        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeGerenteRegional;
        private System.Windows.Forms.DataGridViewTextBoxColumn nomeGerenteFilial;
        private System.Windows.Forms.DataGridViewTextBoxColumn codFilial;
        private System.Windows.Forms.DataGridViewTextBoxColumn codLoja;
        private System.Windows.Forms.DataGridViewTextBoxColumn numPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn seqPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroNota;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtEmissao;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataLiberacaoPedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlNota;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlFrete;
        private System.Windows.Forms.DataGridViewTextBoxColumn vlCobradoGerente;
    }
}