﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;
using UtilGeralDPA;

namespace PainelFreteDPK
{
    public partial class frmPedidosLiberadosGerente : Form
    {
        private List<ComboGerenteLimite> listaGerente = new List<ComboGerenteLimite>();
        private List<ComboGerente> listaGerenteRegional = new List<ComboGerente>();
        private FiltroPedidoGerente oFiltro;
        private List<PedidoBE> listaPedidosLiberados;

        public frmPedidosLiberadosGerente()
        {
            InitializeComponent();
            CarregarComboGerente();
            CarregarComboGerenteRegional();
        }

        private void CarregarComboGerente()
        {
            cmbGerente.DataSource = null;
            cmbGerente.Items.Clear();
            cmbGerente.DisplayMember = "NOME";
            cmbGerente.ValueMember = "CPF";

            try
            {
                listaGerente = GerenteBO.BuscarGerenteLimite();
                ComboGerenteLimite gerenteLimite = new ComboGerenteLimite();
                gerenteLimite.CPF = 0;
                gerenteLimite.NOME = "Selecione";
                listaGerente.Insert(0, gerenteLimite);
                cmbGerente.DataSource = listaGerente;
                cmbGerente.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarGerente, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboGerenteRegional()
        {
            cmbGerenteRegional.DataSource = null;
            cmbGerenteRegional.Items.Clear();
            cmbGerenteRegional.DisplayMember = "NOME";
            cmbGerenteRegional.ValueMember = "CPF";

            try
            {
                listaGerenteRegional = GerenteBO.BuscarGerenteRegional();
                ComboGerente gerenteLimite = new ComboGerente();
                gerenteLimite.CPF = 0;
                gerenteLimite.NOME = "Selecione";
                listaGerenteRegional.Insert(0, gerenteLimite);
                cmbGerenteRegional.DataSource = listaGerenteRegional;
                cmbGerenteRegional.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarGerente, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cmbGerenteRegional_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboGerenteLimite gerente = new ComboGerenteLimite();
            gerente.CPF = 0;
            gerente.NOME = "Selecione";
            Int64 cpfGerenteSelecionado = ((ComboGerenteLimite)cmbGerente.SelectedItem).CPF;
            cmbGerente.DataSource = null;
            cmbGerente.Items.Clear();
            cmbGerente.DisplayMember = "NOME";
            cmbGerente.ValueMember = "CPF";

            if (cmbGerenteRegional.SelectedIndex == 0 || cmbGerenteRegional.SelectedIndex == -1)
            {
                cmbGerente.DataSource = listaGerente;
                cmbGerente.SelectedIndex = 0;
            }
            else
            {
                List<ComboGerenteLimite> listaGerenteAxu = new List<ComboGerenteLimite>();
                Int64 cpfGerente = ((ComboGerente)cmbGerenteRegional.SelectedItem).CPF;
                listaGerenteAxu = (from l in listaGerente where (l.CPFREGIONAL == cpfGerente || cpfGerente == 0) select l).ToList();
                listaGerenteAxu.Insert(0, gerente);
                cmbGerente.DataSource = listaGerenteAxu;
                var listaGerenteSelecionado = (from l in listaGerenteAxu where (l.CPF == cpfGerenteSelecionado) select l).ToList();
                if (listaGerenteSelecionado.ToList().Count == 0)
                {
                    cmbGerente.SelectedIndex = 0;
                }
                else
                {
                    cmbGerente.SelectedValue = cpfGerenteSelecionado;
                }
            }
        }

        private void cmbGerente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbGerente.SelectedIndex != -1 && cmbGerente.SelectedIndex != 0)
            {
                Int64 cpfGerenteRegional = ((ComboGerenteLimite)cmbGerente.SelectedItem).CPFREGIONAL;
                cmbGerenteRegional.SelectedValue = cpfGerenteRegional;
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            oFiltro = new FiltroPedidoGerente();
            if (dtpDataInicio.Checked == true)
            {
                oFiltro.dtIni = dtpDataInicio.Value.ToShortDateString();
            }
            else
            {
                oFiltro.dtIni = null;
            }
            if (dtpDataInicio.Checked == false && dtpDataFim.Checked == true ||
               dtpDataInicio.Checked == true && dtpDataFim.Checked == false)
            {
                MessageBox.Show("Para os filtros de data serem válidos, é necessario marcar Data Início e Data Fim.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (dtpDataInicio.Value.Date > dtpDataFim.Value.Date)
            {
                MessageBox.Show("Data Fim deve ser maior que a Data Início.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (dtpDataFim.Checked == true)
            {
                oFiltro.dtFim = dtpDataFim.Value.ToShortDateString();
            }
            else
            {
                oFiltro.dtFim = null;
            }

            oFiltro.cpfGerente = ((ComboGerenteLimite)cmbGerente.SelectedItem).CPF;
            oFiltro.cpfGerenteRegional = ((ComboGerente)cmbGerenteRegional.SelectedItem).CPF;

            btnConsultar.Text = "Carregando...";
            btnConsultar.Enabled = false;
            btnLimpar.Enabled = false;
            btnExportarExcel.Enabled = false;
            bgwPedidos.RunWorkerAsync();
        }

        private void bgwPedidos_DoWork(object sender, DoWorkEventArgs e)
        {
            listaPedidosLiberados = new List<PedidoBE>();
            listaPedidosLiberados = GerenteBO.ConsultarPedidosLiberadosGerente(oFiltro);
        }

        private void bgwPedidos_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnConsultar.Text = "Consultar";
            btnConsultar.Enabled = true;
            btnLimpar.Enabled = true;

            dgvPedidosLiberados.DataSource = listaPedidosLiberados;
            if (listaPedidosLiberados.Count == 0)
            {
                btnExportarExcel.Enabled = false;
                string mensagem = "Nenhum pedido encontrado.";
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                btnExportarExcel.Enabled = true;
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            cmbGerente.SelectedIndex = 0;
            cmbGerenteRegional.SelectedIndex = 0;
            dtpDataInicio.Text = DateTime.Now.ToShortDateString();
            dtpDataFim.Text = DateTime.Now.ToShortDateString();
            listaPedidosLiberados = null;
            dgvPedidosLiberados.AutoGenerateColumns = false;
            dgvPedidosLiberados.DataSource = listaPedidosLiberados;
        }

        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();

                //Adding the Columns
                foreach (DataGridViewColumn column in dgvPedidosLiberados.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in dgvPedidosLiberados.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (cell.Value != null)
                        {
                            dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                        }
                        else
                        {
                            dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = "";
                        }
                    }
                }

                if (dgvPedidosLiberados.Rows.Count < 100)
                {
                    for (int j = dgvPedidosLiberados.Rows.Count; j <= (dgvPedidosLiberados.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= dgvPedidosLiberados.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
