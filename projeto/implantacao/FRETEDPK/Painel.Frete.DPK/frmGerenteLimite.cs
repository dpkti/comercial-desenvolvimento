﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace PainelFreteDPK
{
    public partial class frmGerenteLimite : Form
    {
        GerenteLimiteUC oGerenteLimiteUC;
        InserirAlterarGerente oGerenteRegional;
        List<GerenteLimiteBE> listaGerentes = new List<GerenteLimiteBE>();
        public frmGerenteLimite(InserirAlterarGerente gerenteRegional, GerenteLimiteUC gerenteLimiteUC)
        {
            try
            {
                oGerenteRegional = gerenteRegional;
                oGerenteLimiteUC = gerenteLimiteUC;

                InitializeComponent();
                lblGerenteRegionalTxt.Text = gerenteRegional.NOME_REGIONAL;

            
                listaGerentes = GerenteBO.BuscarGerenteLimite(gerenteRegional);
                grdGerenteLimite.AutoGenerateColumns = false;
                grdGerenteLimite.DataSource = listaGerentes;
                if (listaGerentes.Count == 0)
                {
                    string mensagem = "Nenhum Limite encontrado.";
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                CarregarGerente();//TI-5946
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo frmGerenteLimite.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void grdGerenteLimite_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                switch (e.Button)
                {
                    case MouseButtons.Right:

                        if (grdGerenteLimite.SelectedRows.Count <= 0) return;

                        ContextMenu myContextMenu = new ContextMenu();

                        MenuItem menuItem0 = new MenuItem("Alterar Limite");
                        MenuItem menuItem1 = new MenuItem("Excluir Gerente");
                        MenuItem menuItem2 = new MenuItem("Histórico Limite");//TI-5946

                        myContextMenu.MenuItems.Add(menuItem0);
                        myContextMenu.MenuItems.Add(menuItem1);
                        myContextMenu.MenuItems.Add(menuItem2);//TI-5946

                        if (grdGerenteLimite.SelectedRows.Count == 1)
                        {
                            myContextMenu.MenuItems.Add(menuItem0);
                            myContextMenu.MenuItems.Add(menuItem1);
                            myContextMenu.MenuItems.Add(menuItem2);//TI-5946

                            menuItem0.Click += new EventHandler(delegate(Object o, EventArgs a)
                            {
                                oGerenteRegional.CDFILIAL = int.Parse(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["COD_FILIAL"].Value.ToString());
                                oGerenteRegional.CPF = Int64.Parse(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["CPF"].Value.ToString());
                                oGerenteRegional.TIPO = grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["TIPO"].Value.ToString();
                                oGerenteRegional.NOME_GERENTE = grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["NOME"].Value.ToString();
                                frmInserirAtualizarGerente frmInserirAtualizarGerente = new frmInserirAtualizarGerente(oGerenteRegional, oGerenteLimiteUC, this, TipoAcao.Atualizar);
                                frmInserirAtualizarGerente.Text = "Alterar Limite Gerente";
                                frmInserirAtualizarGerente.StartPosition = FormStartPosition.CenterParent;
                                frmInserirAtualizarGerente.ShowDialog();
                            });

                            menuItem1.Click += new EventHandler(delegate(Object o, EventArgs a)
                           {
                               oGerenteRegional.CDFILIAL = int.Parse(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["COD_FILIAL"].Value.ToString());
                               oGerenteRegional.CPF = Int64.Parse(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["CPF"].Value.ToString());
                               oGerenteRegional.TIPO = grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["TIPO"].Value.ToString();

                               if (MessageBox.Show("Confirma a exclusão do limite?", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                               {
                                   GerenteBO.ExcluiLimiteGerente(oGerenteRegional, GlobalBE.usuarioLogado);
                                   MessageBox.Show("Limite excluido com sucesso.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                   this.AtualizaGrid();
                                   oGerenteLimiteUC.AtualizaGrid();

                               }
                               else
                               {
                                   return;
                               }
                           });

                            //TI-5946
                            menuItem2.Click += new EventHandler(delegate (Object o, EventArgs a)
                            {
                                oGerenteRegional.CPF = Int64.Parse(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["CPF"].Value.ToString());

                                frmHistoricoLimite frmHistoricoLimite = new frmHistoricoLimite(oGerenteRegional.CPF);
                                frmHistoricoLimite.Text = "Histórico de Limite";
                                frmHistoricoLimite.StartPosition = FormStartPosition.CenterParent;
                                frmHistoricoLimite.ShowDialog();
                            });
                            //FIM TI-5946
                        }

                        myContextMenu.Show(grdGerenteLimite, e.Location, LeftRightAlignment.Right);

                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo grdGerenteLimite_MouseUp.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            
        }

        private void grdGerenteLimite_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    grdGerenteLimite.ClearSelection();
                    grdGerenteLimite.Rows[e.RowIndex].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo grdGerenteLimite_CellMouseClick.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
                
        }
        
        public void AtualizaGrid()
        {
            try
            {
                listaGerentes = GerenteBO.BuscarGerenteLimite(oGerenteRegional);
                grdGerenteLimite.AutoGenerateColumns = false;
                grdGerenteLimite.DataSource = listaGerentes;
                grdGerenteLimite.Update();

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo AtualizarGrid.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        //TI-5946
        public void CarregarGerente()
        {
            try
            {
                cmbFilial.DisplayMember = "Key";
                cmbFilial.ValueMember = "Key";
                cmbFilial.Items.Add("Todos");
                listaGerentes.GroupBy(o => o.NOME).OrderBy(x => x.Key).ToList().ForEach(
                    c =>
                    {
                        cmbFilial.Items.Add(c.Key);
                    });
                cmbFilial.SelectedItem = "Todos";

            }
            catch (Exception){ return;  }
        }

        private void cmbFilial_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdGerenteLimite.AutoGenerateColumns = false;

                if (cmbFilial.SelectedItem.ToString() == "Todos")
                {
                    grdGerenteLimite.DataSource = listaGerentes;
                    grdGerenteLimite.Update();
                }
                else
                {
                    var lst = listaGerentes.Where(t => t.NOME.ToUpper().Equals(cmbFilial.SelectedItem.ToString().ToUpper()));
                    grdGerenteLimite.DataSource = lst.ToList();
                    grdGerenteLimite.Update();
                }
            }
            catch (Exception) { return; }
        }

        //FIM TI-5946
    }
}
