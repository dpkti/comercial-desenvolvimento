﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Entities;
using Business;
using System.Diagnostics;

namespace PainelFreteDPK
{
    public partial class frmImportarExcel : Form
    {
        private bool erro;
        private int qtdInserido;

        public frmImportarExcel()
        {
            InitializeComponent();
            lbQtdInserido.Visible = false;
            lbQtdInseridoAlt.Visible = false;
            txtCaminhoArquivo.Enabled = false;
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            this.oFDPlanilha.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";
            this.oFDPlanilha.ShowDialog();
            this.txtCaminhoArquivo.Text = this.oFDPlanilha.FileName;
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            if (File.Exists(this.txtCaminhoArquivo.Text))
            {
                btnAbrir.Enabled = false;
                btnImportar.Enabled = false;
                lbStatus.Visible = true;
                lbStatusProc.Visible = true;
                btnImportar.Enabled = false;
                btnAbrir.Enabled = false;
                bgWorkerPoliticaFrete.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show(ResourcePainelFreteDPK.Erro_ArquivoInvalido, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void bgWorkerPoliticaFrete_DoWork(object sender, DoWorkEventArgs e)
        {
            erro = false;
            string arqErro = string.Empty;
            List<ItinerarioTransportadoraValidacao> listaItinerarioTransportadora;
            string retLogGeraArq = string.Empty;
            int qtdItiTransp;

            try
            {
                listaItinerarioTransportadora = PoliticaFreteBO.ValidarArquivoExcel(txtCaminhoArquivo.Text, bgWorkerPoliticaFrete, out retLogGeraArq);
                if (!retLogGeraArq.Equals(string.Empty))
                {
                    erro = true;
                    if (MessageBox.Show("Ocorreram problemas durante a inserção, deseja ver Log?",
                               "Politica Frete - Transportadora Itinerário", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        Process.Start("notepad.exe", retLogGeraArq);
                    }
                }
                else
                {
                    bool sucessoCSV = PoliticaFreteBO.GerarArquivoCSV(bgWorkerPoliticaFrete, listaItinerarioTransportadora);
                    bool sucessoInsercaoItiTransp = PoliticaFreteBO.InserirItinerarioTransportadora(bgWorkerPoliticaFrete, out qtdItiTransp);

                    if (sucessoCSV && sucessoInsercaoItiTransp)
                    {
                        qtdInserido = qtdItiTransp;
                        MessageBox.Show("O arquivo foi processado com sucesso!", "Politica Frete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        erro = true;
                        MessageBox.Show("Falha ao processar o arquivo.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                this.erro = true;
            }
        }

        private void bgWorkerPoliticaFrete_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch ((EnumeradoresAplicacao.StatusTransportadoraItinerario)e.UserState)
            {
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioValidacaoArquivo:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Validando Arquivo Excel...";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.FimValidacaoArquivo:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Validação Arquivo Excel.";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioGeracaoCSV:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Gerando Arquivo CSV...";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.FimGeracaoCSV:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Geração Arquivo CSV.";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.InicioInsercaoItinerarioTransportadora:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Inserindo Tabela ITINERARIO_TRANSPORTADORA...";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.FimInsercaoItinerarioTransportadora:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Inserção Tabela ITINERARIO_TRANSPORTADORA.";
                    break;
                case EnumeradoresAplicacao.StatusTransportadoraItinerario.Erro:
                    this.erro = true;
                    break;
            }
        }

        private void bgWorkerPoliticaFrete_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.erro)
            {
                lbStatus.Visible = true;
                lbStatusProc.Visible = true;
                lbQtdInserido.Visible = false;
                lbQtdInseridoAlt.Visible = false;
                lbStatus.Text = "Erro no processamento.";
                lbStatus.ForeColor = Color.Red;
            }
            else
            {
                lbStatus.Visible = false;
                lbStatusProc.Visible = false;
                lbQtdInseridoAlt.Text = qtdInserido.ToString();
                lbQtdInserido.Visible = true;
                lbQtdInseridoAlt.Visible = true;
            }

            btnAbrir.Enabled = true;
            btnImportar.Enabled = true;
        }
    }
}
