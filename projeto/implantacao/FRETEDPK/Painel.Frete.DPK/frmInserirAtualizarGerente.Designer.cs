﻿namespace PainelFreteDPK
{
    partial class frmInserirAtualizarGerente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFilial = new System.Windows.Forms.Label();
            this.lblGerente = new System.Windows.Forms.Label();
            this.lblLimite = new System.Windows.Forms.Label();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.txtSaldo = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.cmbGerente = new System.Windows.Forms.ComboBox();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.lblGerenteRegional = new System.Windows.Forms.Label();
            this.txtGerenteRegional = new System.Windows.Forms.TextBox();
            this.txtLimite = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Location = new System.Drawing.Point(11, 57);
            this.lblFilial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(30, 13);
            this.lblFilial.TabIndex = 1;
            this.lblFilial.Text = "Filial:";
            // 
            // lblGerente
            // 
            this.lblGerente.AutoSize = true;
            this.lblGerente.Location = new System.Drawing.Point(11, 25);
            this.lblGerente.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGerente.Name = "lblGerente";
            this.lblGerente.Size = new System.Drawing.Size(48, 13);
            this.lblGerente.TabIndex = 2;
            this.lblGerente.Text = "Gerente:";
            // 
            // lblLimite
            // 
            this.lblLimite.AutoSize = true;
            this.lblLimite.Location = new System.Drawing.Point(317, 21);
            this.lblLimite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLimite.Name = "lblLimite";
            this.lblLimite.Size = new System.Drawing.Size(37, 13);
            this.lblLimite.TabIndex = 3;
            this.lblLimite.Text = "Limite:";
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Location = new System.Drawing.Point(317, 51);
            this.lblSaldo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(37, 13);
            this.lblSaldo.TabIndex = 4;
            this.lblSaldo.Text = "Saldo:";
            // 
            // txtSaldo
            // 
            this.txtSaldo.Enabled = false;
            this.txtSaldo.Location = new System.Drawing.Point(368, 47);
            this.txtSaldo.Margin = new System.Windows.Forms.Padding(2);
            this.txtSaldo.Name = "txtSaldo";
            this.txtSaldo.ReadOnly = true;
            this.txtSaldo.Size = new System.Drawing.Size(159, 20);
            this.txtSaldo.TabIndex = 9;
            this.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(357, 82);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(82, 32);
            this.btnSalvar.TabIndex = 3;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // cmbGerente
            // 
            this.cmbGerente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGerente.FormattingEnabled = true;
            this.cmbGerente.Location = new System.Drawing.Point(83, 21);
            this.cmbGerente.Margin = new System.Windows.Forms.Padding(2);
            this.cmbGerente.Name = "cmbGerente";
            this.cmbGerente.Size = new System.Drawing.Size(229, 21);
            this.cmbGerente.TabIndex = 0;
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(83, 54);
            this.cmbFilial.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(229, 21);
            this.cmbFilial.TabIndex = 1;
            // 
            // lblGerenteRegional
            // 
            this.lblGerenteRegional.AutoSize = true;
            this.lblGerenteRegional.Location = new System.Drawing.Point(11, 92);
            this.lblGerenteRegional.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGerenteRegional.Name = "lblGerenteRegional";
            this.lblGerenteRegional.Size = new System.Drawing.Size(93, 13);
            this.lblGerenteRegional.TabIndex = 13;
            this.lblGerenteRegional.Text = "Gerente Regional:";
            // 
            // txtGerenteRegional
            // 
            this.txtGerenteRegional.Enabled = false;
            this.txtGerenteRegional.Location = new System.Drawing.Point(108, 92);
            this.txtGerenteRegional.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerenteRegional.Name = "txtGerenteRegional";
            this.txtGerenteRegional.ReadOnly = true;
            this.txtGerenteRegional.Size = new System.Drawing.Size(204, 20);
            this.txtGerenteRegional.TabIndex = 14;
            // 
            // txtLimite
            // 
            this.txtLimite.Location = new System.Drawing.Point(368, 21);
            this.txtLimite.MaxLength = 12;
            this.txtLimite.Name = "txtLimite";
            this.txtLimite.Size = new System.Drawing.Size(158, 20);
            this.txtLimite.TabIndex = 2;
            this.txtLimite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLimite.TextChanged += new System.EventHandler(this.txtLimite_TextChanged);
            this.txtLimite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLimite_KeyPress);
            this.txtLimite.Leave += new System.EventHandler(this.txtLimite_Leave);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(444, 82);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(82, 32);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmInserirAtualizarGerente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(538, 129);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.txtLimite);
            this.Controls.Add(this.txtGerenteRegional);
            this.Controls.Add(this.lblGerenteRegional);
            this.Controls.Add(this.cmbFilial);
            this.Controls.Add(this.cmbGerente);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.txtSaldo);
            this.Controls.Add(this.lblSaldo);
            this.Controls.Add(this.lblLimite);
            this.Controls.Add(this.lblGerente);
            this.Controls.Add(this.lblFilial);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "frmInserirAtualizarGerente";
            this.Text = "Inserir/Atualizar Gerente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFilial;
        private System.Windows.Forms.Label lblGerente;
        private System.Windows.Forms.Label lblLimite;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.TextBox txtSaldo;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.ComboBox cmbGerente;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label lblGerenteRegional;
        private System.Windows.Forms.TextBox txtGerenteRegional;
        private System.Windows.Forms.TextBox txtLimite;
        private System.Windows.Forms.Button btnCancelar;
    }
}