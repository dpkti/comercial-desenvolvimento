﻿namespace PainelFreteDPK
{
    partial class FaturamentoTransportadoraUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdFaturamento = new System.Windows.Forms.DataGridView();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itinerario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transportadora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFreteRecuperado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdConsultar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.dtpDataFim = new System.Windows.Forms.DateTimePicker();
            this.dtpDataInicio = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTransportadora = new System.Windows.Forms.ComboBox();
            this.lblTransportadora = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbCDOrigem = new System.Windows.Forms.ComboBox();
            this.cmbCidadeDestino = new System.Windows.Forms.ComboBox();
            this.cmbUfDestino = new System.Windows.Forms.ComboBox();
            this.bgWorkerFaturamentoTransportadora = new System.ComponentModel.BackgroundWorker();
            this.btnExportar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdFaturamento)).BeginInit();
            this.SuspendLayout();
            // 
            // grdFaturamento
            // 
            this.grdFaturamento.AllowUserToAddRows = false;
            this.grdFaturamento.AllowUserToDeleteRows = false;
            this.grdFaturamento.AllowUserToOrderColumns = true;
            this.grdFaturamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFaturamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.data,
            this.itinerario,
            this.transportadora,
            this.valorFreteRecuperado});
            this.grdFaturamento.Location = new System.Drawing.Point(15, 94);
            this.grdFaturamento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grdFaturamento.Name = "grdFaturamento";
            this.grdFaturamento.ReadOnly = true;
            this.grdFaturamento.RowTemplate.Height = 24;
            this.grdFaturamento.Size = new System.Drawing.Size(1296, 525);
            this.grdFaturamento.TabIndex = 0;
            // 
            // data
            // 
            this.data.DataPropertyName = "MesAno";
            this.data.HeaderText = "Mês/Ano";
            this.data.Name = "data";
            this.data.ReadOnly = true;
            // 
            // itinerario
            // 
            this.itinerario.DataPropertyName = "Itinerario";
            this.itinerario.HeaderText = "Itinerário";
            this.itinerario.Name = "itinerario";
            this.itinerario.ReadOnly = true;
            this.itinerario.Width = 280;
            // 
            // transportadora
            // 
            this.transportadora.DataPropertyName = "Transportadora";
            this.transportadora.HeaderText = "Transportadora";
            this.transportadora.Name = "transportadora";
            this.transportadora.ReadOnly = true;
            this.transportadora.Width = 300;
            // 
            // valorFreteRecuperado
            // 
            this.valorFreteRecuperado.DataPropertyName = "ValorFreteRecuperado";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "C2";
            dataGridViewCellStyle1.NullValue = null;
            this.valorFreteRecuperado.DefaultCellStyle = dataGridViewCellStyle1;
            this.valorFreteRecuperado.HeaderText = "Valor de Frete Recuperado";
            this.valorFreteRecuperado.Name = "valorFreteRecuperado";
            this.valorFreteRecuperado.ReadOnly = true;
            this.valorFreteRecuperado.Width = 180;
            // 
            // cmdConsultar
            // 
            this.cmdConsultar.Location = new System.Drawing.Point(1202, 44);
            this.cmdConsultar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmdConsultar.Name = "cmdConsultar";
            this.cmdConsultar.Size = new System.Drawing.Size(109, 39);
            this.cmdConsultar.TabIndex = 44;
            this.cmdConsultar.Text = "Consultar";
            this.cmdConsultar.UseVisualStyleBackColor = true;
            this.cmdConsultar.Click += new System.EventHandler(this.cmdConsultar_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(1087, 44);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(109, 39);
            this.btnLimpar.TabIndex = 45;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // dtpDataFim
            // 
            this.dtpDataFim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataFim.Location = new System.Drawing.Point(927, 49);
            this.dtpDataFim.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataFim.Name = "dtpDataFim";
            this.dtpDataFim.ShowCheckBox = true;
            this.dtpDataFim.Size = new System.Drawing.Size(132, 22);
            this.dtpDataFim.TabIndex = 43;
            // 
            // dtpDataInicio
            // 
            this.dtpDataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataInicio.Location = new System.Drawing.Point(723, 50);
            this.dtpDataInicio.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataInicio.Name = "dtpDataInicio";
            this.dtpDataInicio.ShowCheckBox = true;
            this.dtpDataInicio.Size = new System.Drawing.Size(132, 22);
            this.dtpDataInicio.TabIndex = 42;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(857, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 33;
            this.label3.Text = "Data Fim:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(639, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 17);
            this.label4.TabIndex = 32;
            this.label4.Text = "Data Início:";
            // 
            // cmbTransportadora
            // 
            this.cmbTransportadora.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbTransportadora.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbTransportadora.FormattingEnabled = true;
            this.cmbTransportadora.Location = new System.Drawing.Point(127, 49);
            this.cmbTransportadora.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbTransportadora.Name = "cmbTransportadora";
            this.cmbTransportadora.Size = new System.Drawing.Size(459, 24);
            this.cmbTransportadora.TabIndex = 41;
            // 
            // lblTransportadora
            // 
            this.lblTransportadora.AutoSize = true;
            this.lblTransportadora.Location = new System.Drawing.Point(12, 53);
            this.lblTransportadora.Name = "lblTransportadora";
            this.lblTransportadora.Size = new System.Drawing.Size(111, 17);
            this.lblTransportadora.TabIndex = 36;
            this.lblTransportadora.Text = "Transportadora:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(639, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 17);
            this.label5.TabIndex = 43;
            this.label5.Text = "Cidade Destino:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(262, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 42;
            this.label1.Text = "UF Destino:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 41;
            this.label2.Text = "CD Origem:";
            // 
            // cmbCDOrigem
            // 
            this.cmbCDOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCDOrigem.FormattingEnabled = true;
            this.cmbCDOrigem.Location = new System.Drawing.Point(99, 11);
            this.cmbCDOrigem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCDOrigem.Name = "cmbCDOrigem";
            this.cmbCDOrigem.Size = new System.Drawing.Size(156, 24);
            this.cmbCDOrigem.TabIndex = 38;
            // 
            // cmbCidadeDestino
            // 
            this.cmbCidadeDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCidadeDestino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCidadeDestino.FormattingEnabled = true;
            this.cmbCidadeDestino.Location = new System.Drawing.Point(754, 11);
            this.cmbCidadeDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCidadeDestino.Name = "cmbCidadeDestino";
            this.cmbCidadeDestino.Size = new System.Drawing.Size(305, 24);
            this.cmbCidadeDestino.TabIndex = 40;
            this.cmbCidadeDestino.SelectedIndexChanged += new System.EventHandler(this.cmbCidadeDestino_SelectedIndexChanged);
            // 
            // cmbUfDestino
            // 
            this.cmbUfDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUfDestino.FormattingEnabled = true;
            this.cmbUfDestino.Location = new System.Drawing.Point(351, 11);
            this.cmbUfDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbUfDestino.Name = "cmbUfDestino";
            this.cmbUfDestino.Size = new System.Drawing.Size(235, 24);
            this.cmbUfDestino.TabIndex = 39;
            this.cmbUfDestino.SelectedIndexChanged += new System.EventHandler(this.cmbUfDestino_SelectedIndexChanged);
            // 
            // bgWorkerFaturamentoTransportadora
            // 
            this.bgWorkerFaturamentoTransportadora.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerFaturamentoTransportadora_DoWork);
            this.bgWorkerFaturamentoTransportadora.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerFaturamentoTransportadora_RunWorkerCompleted);
            // 
            // btnExportar
            // 
            this.btnExportar.Location = new System.Drawing.Point(1202, 623);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(109, 39);
            this.btnExportar.TabIndex = 46;
            this.btnExportar.Text = "Exportar Excel";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // FaturamentoTransportadoraUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbCDOrigem);
            this.Controls.Add(this.cmbCidadeDestino);
            this.Controls.Add(this.cmbUfDestino);
            this.Controls.Add(this.cmbTransportadora);
            this.Controls.Add(this.lblTransportadora);
            this.Controls.Add(this.dtpDataFim);
            this.Controls.Add(this.dtpDataInicio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.cmdConsultar);
            this.Controls.Add(this.grdFaturamento);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FaturamentoTransportadoraUC";
            this.Size = new System.Drawing.Size(1327, 670);
            ((System.ComponentModel.ISupportInitialize)(this.grdFaturamento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdFaturamento;
        private System.Windows.Forms.Button cmdConsultar;
        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.Windows.Forms.DataGridViewTextBoxColumn itinerario;
        private System.Windows.Forms.DataGridViewTextBoxColumn transportadora;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFreteRecuperado;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.DateTimePicker dtpDataFim;
        private System.Windows.Forms.DateTimePicker dtpDataInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTransportadora;
        private System.Windows.Forms.Label lblTransportadora;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCDOrigem;
        private System.Windows.Forms.ComboBox cmbCidadeDestino;
        private System.Windows.Forms.ComboBox cmbUfDestino;
        private System.ComponentModel.BackgroundWorker bgWorkerFaturamentoTransportadora;
        private System.Windows.Forms.Button btnExportar;
    }
}
