﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;
using System.Reflection;
using System.Globalization;
using UtilGeralDPA;

namespace PainelFreteDPK
{
    public partial class frmNovaPoliticaFrete : Form
    {
        List<CidadeDestinoBE> listaCidade = null;
        Int32 codItinerario;
        FiltroItinerarioTransportadora oFiltro = new FiltroItinerarioTransportadora();
        bool alteracao;
        public PoliticaFreteUC politicaFreteUC = new PoliticaFreteUC();
        bool teveInsercaoAlteracao = false;
        List<CodigoTransportadoraBE> listaCodigosTransportadora = new List<CodigoTransportadoraBE>();//TI-5946

        public frmNovaPoliticaFrete()
        {
            InitializeComponent();
            alteracao = false;
        }

        public frmNovaPoliticaFrete(DateTime dataVigencia, Int32 codItinerario, Int32 canal)
        {
            InitializeComponent();
            oFiltro.dataVigencia = dataVigencia;
            oFiltro.codItinerario = codItinerario;
            oFiltro.canal = canal;
            alteracao = true;
        }

        private void frmNovaPoliticaFrete_Load(object sender, EventArgs e)
        {
            if (!alteracao)
            {
                CarregarTela();
            }
            else
            {
                CarregarTelaAlteracao();
            }
        }

        private void CarregarTelaAlteracao()
        {
            try
            {
                CarregarComboCdOrigem();
                CarregarComboUfDestino();
                listaCidade = GlobalBE.listaCidade;
                CarregarComboCodigoCanal();
                Cidade cid = PoliticaFreteBO.BuscarCidade(oFiltro.codItinerario);
                cmbCDOrigem.SelectedValue = cid.cdOrigem;
                cmbUfDestino.SelectedValue = cid.ufDestino;
                cmbCidadeDestino.SelectedValue = cid.codigo;
                cmbCanal.SelectedValue = oFiltro.canal;
                btnLimpar.Visible = false;
                btnConsultar.Location = new Point(1171, 57);
                btnConsultar.Visible = false;
                Consultar();
            }
            catch (Exception ex)
            {
                string mensagem = String.Format("Erro ao preencher tela.");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
            listaCodigosTransportadora = PoliticaFreteBO.BuscarDescricaoTransportadora();//TI-5946
        }

        private void Consultar()
        {
            try
            {
                if (ValidarTela())
                {
                    List<ItinerarioTransportadora> listaItiTransp = new List<ItinerarioTransportadora>();
                    int cdOrigem = Convert.ToInt32(cmbCDOrigem.SelectedValue);
                    int cidadeDestino = Convert.ToInt32(cmbCidadeDestino.SelectedValue);
                    this.codItinerario = PoliticaFreteBO.BuscarItinerario(cdOrigem, cidadeDestino);
                    grdPoliticaFrete.AutoGenerateColumns = false;

                    if (this.codItinerario == 0)
                    {
                        string mensagem = "Não existe itinerário para este CD origem/cidade destino";
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        lbCodItinerarioAlt.Text = this.codItinerario.ToString();
                        lbCodigoItinerario.Visible = true;
                        lbCodItinerarioAlt.Visible = true;
                        oFiltro = new FiltroItinerarioTransportadora();
                        oFiltro.canal = Convert.ToInt32(cmbCanal.SelectedValue);
                        oFiltro.codItinerario = this.codItinerario;
                        listaItiTransp = PoliticaFreteBO.BuscarItinerarioTransportadora(oFiltro);

                        lblDataVigencia.Visible = true;
                        dtpDataVigencia.Visible = true;
                        dtpDataVigencia.Enabled = true;
                        btnSalvar.Enabled = true;
                        grdPoliticaFrete.Enabled = true;
                        cmbCanal.Enabled = false;
                        cmbCDOrigem.Enabled = false;
                        cmbCidadeDestino.Enabled = false;
                        cmbUfDestino.Enabled = false;

                        if (listaItiTransp.Count > 0)
                        {
                            if (listaItiTransp.FirstOrDefault().dataVigencia > DateTime.Now)
                            {
                                dtpDataVigencia.Text = listaItiTransp.FirstOrDefault().dataVigencia.ToShortDateString();
                            }
                            else
                            {
                                dtpDataVigencia.Text = "";
                            }
                            
                            grdPoliticaFrete.DataSource = ToDataTable(listaItiTransp);
                        }
                        else
                        {
                            grdPoliticaFrete.DataSource = ToDataTable(listaItiTransp);
                        }
                    }
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                string mensagem = String.Format("Erro ao consultar itinerário transportadoras.");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            //put a breakpoint here and check datatable
            return dataTable;
        }

        private bool ValidarTela()
        {
            if (cmbCDOrigem.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "CD Origem");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbUfDestino.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "UF Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbCidadeDestino.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Cidade Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbCanal.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Canal");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void CarregarTela()
        {
            try
            {
                CarregarComboCdOrigem();
                CarregarComboUfDestino();
                cmbCidadeDestino.Enabled = false;
                listaCidade = GlobalBE.listaCidade;
                CarregarComboCodigoCanal();
                btnSalvar.Enabled = false;
                lblDataVigencia.Visible = false;
                dtpDataVigencia.Visible = false;
                dtpDataVigencia.Text = DateTime.Now.AddDays(1).ToShortDateString();
                grdPoliticaFrete.Enabled = false;
            }
            catch (Exception ex)
            {
                string mensagem = String.Format("Erro ao preencher tela.");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CarregarComboCdOrigem()
        {
            List<CdOrigemBE> listaCdOrigem = null;
            cmbCDOrigem.DataSource = null;
            cmbCDOrigem.Items.Clear();
            cmbCDOrigem.DisplayMember = "CDORIGEM";
            cmbCDOrigem.ValueMember = "CD";
            try
            {
                listaCdOrigem = GlobalBE.listaCdOrigem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            cmbCDOrigem.DataSource = listaCdOrigem;
            cmbCDOrigem.SelectedIndex = 0;
        }

        private void CarregarComboUfDestino()
        {
            List<UfDestinoBE> listaUfDestino = null;
            cmbUfDestino.DataSource = null;
            cmbUfDestino.Items.Clear();
            cmbUfDestino.DisplayMember = "UF";
            cmbUfDestino.ValueMember = "CDUF";

            try
            {
                listaUfDestino = GlobalBE.listaUF;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            cmbUfDestino.DataSource = listaUfDestino;
            cmbUfDestino.SelectedIndex = 0;
        }

        private void CarregarComboCodigoCanal()
        {
            cmbCanal.Items.Clear();

            cmbCanal.DisplayMember = "CANAL";
            cmbCanal.ValueMember = "COD_CANAL";

            List<CodigoCanalBE> listaCodigosCanal = null;

            try
            {
                listaCodigosCanal = Business.PoliticaFreteBO.BuscarDescricaoCanal(GlobalBE.usuarioLogado.LOGIN);//TI-5946
                CodigoCanalBE oCanal = new CodigoCanalBE();
                oCanal.COD_CANAL = 0;
                oCanal.CANAL = "Selecione";
                listaCodigosCanal.Insert(0, oCanal);

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCodCanal, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            cmbCanal.DataSource = listaCodigosCanal;
        }

        private void cmbUfDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            CidadeDestinoBE cidadeDestino = new CidadeDestinoBE();
            cidadeDestino.CODCIDADE = 0;
            cidadeDestino.CIDADE = "Selecione";
            cmbCidadeDestino.DataSource = null;
            cmbCidadeDestino.Items.Clear();
            cmbCidadeDestino.DisplayMember = "CIDADE";
            cmbCidadeDestino.ValueMember = "CODCIDADE";

            if (cmbUfDestino.SelectedIndex == 0 || cmbUfDestino.SelectedIndex == -1)
            {
                cmbCidadeDestino.Enabled = false;
                List<CidadeDestinoBE> listaCidadeVazia = new List<CidadeDestinoBE>();
                listaCidadeVazia.Insert(0, cidadeDestino);
                cmbCidadeDestino.DataSource = listaCidadeVazia;
                cmbCidadeDestino.SelectedIndex = 0;
            }
            else
            {
                List<CidadeDestinoBE> listaCidadeDestino;
                string uf = ((UfDestinoBE)cmbUfDestino.SelectedItem).CDUF;
                var listatmp = from l in listaCidade where (l.UF == uf || uf == null) select l;
                listaCidadeDestino = listatmp.ToList();
                listaCidadeDestino.Insert(0, cidadeDestino);
                cmbCidadeDestino.DataSource = listaCidadeDestino;
                cmbCidadeDestino.SelectedIndex = 0;
                cmbCidadeDestino.Enabled = true;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            List<String> listaErro = new List<String>();
            int linha = 0;
            bool excluir = false;
            List<ItinerarioTransportadora> listaItinerarioTransportadora = new List<ItinerarioTransportadora>();
            bool existeVigenciaItinerario;
       
            try
            {
                foreach (DataGridViewRow dr in grdPoliticaFrete.Rows)
                {
                    linha++;
                    if (((dr.Cells["codigoTransportadora"].Value != DBNull.Value && dr.Cells["codigoTransportadora"].Value != null) || (dr.Cells["prioridade"].Value != DBNull.Value && dr.Cells["prioridade"].Value != null)
                        || (dr.Cells["faturamentoMinimo"].Value != DBNull.Value && dr.Cells["faturamentoMinimo"].Value != null) || (dr.Cells["PrazoEntrega"].Value != DBNull.Value && dr.Cells["PrazoEntrega"].Value != null)
                        || (dr.Cells["tipoCobranca"].Value != DBNull.Value && dr.Cells["tipoCobranca"].Value != null) || (dr.Cells["valorFaixaInicial"].Value != DBNull.Value && dr.Cells["valorFaixaInicial"].Value != null)
                        || (dr.Cells["valorFaixaFinal"].Value != DBNull.Value && dr.Cells["valorFaixaFinal"].Value != null) || (dr.Cells["valorFrete"].Value != DBNull.Value && dr.Cells["valorFrete"].Value != null)
                        || (dr.Cells["percentualFrete"].Value != DBNull.Value && dr.Cells["percentualFrete"].Value != null)) && (dr.Cells["Excluir"].Value == DBNull.Value || !Convert.ToBoolean(dr.Cells["Excluir"].Value)))
                    {
                        ItinerarioTransportadora oItiTransp = new ItinerarioTransportadora();
                        oItiTransp.canal = Convert.ToInt32(cmbCanal.SelectedValue);
                        oItiTransp.codItinerario = this.codItinerario;
                        oItiTransp.dataVigencia = Convert.ToDateTime(dtpDataVigencia.Text);
                        foreach (DataGridViewCell dc in dr.Cells)
                        {
                            if (!ValidarItinerario(linha, dc, oItiTransp, listaItinerarioTransportadora))
                            {
                                return;
                            }
                        }

                        if (!PoliticaFreteBO.ValidarFaixasPrazo(listaItinerarioTransportadora, oItiTransp))
                        {
                            string mensagem = String.Format("Linha: {0} - Já existe uma política de frete para o campo VL_FAIXA_INICIAL e VL_FAIXA_FINAL para esta transportadora.", linha);
                            MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        listaItinerarioTransportadora.Add(oItiTransp);
                    }
                    else
                    {
                        if (dr.Cells["Excluir"].Value != DBNull.Value && dr.Cells["Excluir"].Value != null && Convert.ToBoolean(dr.Cells["Excluir"].Value))
                        {
                            excluir = true;
                        }
                    }
                }

                if (!PoliticaFreteBO.ValidarPrioridadeSequencial(listaItinerarioTransportadora))
                {
                    string mensagem = String.Format("O campo Prioridade Transportadora deve ser sequêncial para transportadoras diferentes em um mesmo itinerário.");
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                existeVigenciaItinerario = PoliticaFreteBO.ExistePolitica(oFiltro);

                if (existeVigenciaItinerario)
                {
                    if (Convert.ToDateTime(dtpDataVigencia.Text) <= DateTime.Now)
                    {
                        string mensagem = String.Format("O campo Data Vigência deve ser maior que a data atual.");
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    if (Convert.ToDateTime(dtpDataVigencia.Text) < DateTime.Now.Date)
                    {
                        string mensagem = String.Format("O campo Data Vigência deve ser maior ou igual que a data atual.");
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                DateTime? VigenciaFutura = PoliticaFreteBO.ValidarItigerarioVigenciaFutura(oFiltro);

                if (listaItinerarioTransportadora.Count == 0 && excluir && VigenciaFutura != null)
                {
                    string mensagemExcluir = String.Format("Deseja excluir todas as politicas de frete deste itinerário com data de vigência para {0}?", dtpDataVigencia.Text);
                    DialogResult result = MessageBox.Show(mensagemExcluir, ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        oFiltro.dataVigencia = Convert.ToDateTime(dtpDataVigencia.Text);
                        PoliticaFreteBO.DeletarVigenciaFutura(oFiltro);
                        teveInsercaoAlteracao = true;
                        MessageBox.Show("Sucesso ao excluir política de frete.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        if (alteracao)
                        {
                            this.Close();
                            return;
                        }
                        else
                        {
                            teveInsercaoAlteracao = true;
                            Limpar();
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    if (listaItinerarioTransportadora.Count == 0 && !excluir && VigenciaFutura == null)
                    {
                        MessageBox.Show("Não houve inserção de política de frete.", ResourcePainelFreteDPK.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (VigenciaFutura != null)
                {
                    DateTime dataVigencia = Convert.ToDateTime(VigenciaFutura);
                    string mensagem = String.Format("Existe vigência futura para o dia {0}. Deseja substituir?", dataVigencia.ToShortDateString());
                    DialogResult result = MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        oFiltro.dataVigencia = dataVigencia;
                        PoliticaFreteBO.DeletarInserirVigenciaFutura(oFiltro, listaItinerarioTransportadora);
                        if (alteracao)
                        {
                            teveInsercaoAlteracao = true;
                            MessageBox.Show("Sucesso ao alterar política de frete.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Close();
                            return;
                        }
                        else
                        {
                            teveInsercaoAlteracao = true;
                            MessageBox.Show("Sucesso ao inserir nova política de frete.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Limpar();
                            return;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    string mensagemInserir = String.Format("Deseja inserir novas politicas de frete para o itinerário {0} com data de vigência para {1}?", oFiltro.codItinerario.ToString(), dtpDataVigencia.Text);
                    DialogResult result = MessageBox.Show(mensagemInserir, ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        teveInsercaoAlteracao = true;
                        PoliticaFreteBO.InserirItinerarioTransportadora(listaItinerarioTransportadora);
                        MessageBox.Show("Sucesso ao inserir nova política de frete.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Limpar();
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                string mensagem = String.Format("Erro ao inserir/alterar política de frente.");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private bool ValidarItinerario(int linha, DataGridViewCell dc, ItinerarioTransportadora oItiTransp, List<ItinerarioTransportadora> listaItinerarioTransportadora)
        {
            List<String> listaErro = new List<String>();

            //codigoTransportadora
            if (dc.ColumnIndex == 0)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()))
                {
                    if (!PoliticaFreteBO.ValidarTransportadora(linha, Convert.ToInt32(dc.Value), listaErro))
                    {
                        string mensagem = String.Format("Linha: {0} - Campo Código Transportadora Inválido.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        oItiTransp.codigoTransportadora = Convert.ToInt32(dc.Value);                       
                    }
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Código Transportadora Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //prioridade
            if (dc.ColumnIndex == 2)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()))
                {
                    if (!PoliticaFreteBO.ValidarPrioridadeTransportadora(listaItinerarioTransportadora, Convert.ToInt32(dc.Value), oItiTransp.codigoTransportadora))
                    {
                        string mensagem = String.Format("Linha: {0} - Informação duplicada para o campo Prioridade Transportadora.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                    else
                    {
                        oItiTransp.prioridade = Convert.ToInt32(dc.Value);
                    }
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Prioridade Transportadora Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false; ;
                }
            }

            //faturamentoMinimo
            if (dc.ColumnIndex == 3)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) >= 0 && Convert.ToDouble(dc.Value) <= 999999999999.99)
                {
                    oItiTransp.faturamentoMinimo = Convert.ToDouble(dc.Value);
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Faturamento Mínimo Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //PrazoEntrega
            if (dc.ColumnIndex == 4)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()))
                {
                    oItiTransp.prazoEntrega = Convert.ToInt32(dc.Value);
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Prazo Entrega Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //tipoCobranca
            if (dc.ColumnIndex == 5)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) &&
                    (Convert.ToInt32(dc.Value) == (int)EnumeradoresAplicacao.TipoCobranca.Percentual || Convert.ToInt32(dc.Value) == (int)EnumeradoresAplicacao.TipoCobranca.Valor))
                {
                    oItiTransp.tipoCobranca = Convert.ToInt32(dc.Value);
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Tipo Cobrança Inválido. Deve ser 1 ou 2.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //valorFaixaInicial
            if (dc.ColumnIndex == 6)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) >= 0 && Convert.ToDouble(dc.Value) <= 999999999999.99)
                {
                    oItiTransp.valorFaixaInicial = Convert.ToDouble(dc.Value);
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Valor Faixa Inicial Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //valorFaixaFinal
            if (dc.ColumnIndex == 7)
            {
                if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) > 0 && Convert.ToDouble(dc.Value) <= 999999999999.99)
                {
                    oItiTransp.valorFaixaFinal = Convert.ToDouble(dc.Value);
                }
                else
                {
                    string mensagem = String.Format("Linha: {0} - Campo Valor Faixa Final Inválido.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                if (oItiTransp.valorFaixaInicial >= oItiTransp.valorFaixaFinal)
                {
                    string mensagem = String.Format("Linha: {0} - Valor da Faixa Inicial deve ser menor que o Valor da Faixa Final.", linha);
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            //valorFrete
            if (dc.ColumnIndex == 8)
            {
                if (oItiTransp.tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Valor)
                {
                    if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) >= 0 & Convert.ToDouble(dc.Value) <= 999999999999.99)
                    {
                        oItiTransp.valorFrete = Convert.ToDouble(dc.Value);
                        oItiTransp.percentualFrete = 0;
                    }
                    else
                    {
                        string mensagem = String.Format("Linha: {0} - Campo Valor Frete Inválido.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) != 0)
                    {
                        string mensagem = String.Format("Linha: {0} - Campo Valor Frete Inválido.\n Para Tipo Cobrança igual 2 campo Valor de Frete deve ser 0.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }

            //percentualFrete
            if (dc.ColumnIndex == 9)
            {
                if (oItiTransp.tipoCobranca == (int)EnumeradoresAplicacao.TipoCobranca.Percentual)
                {
                    if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) >= 0 && Convert.ToDouble(dc.Value) < 100)
                    {
                        oItiTransp.percentualFrete = Convert.ToDouble(dc.Value);
                        oItiTransp.valorFrete = 0;
                    }
                    else
                    {
                        string mensagem = String.Format("Linha: {0} - Campo Percentual Frete Inválido.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    if (dc.Value != DBNull.Value && dc.Value != null && !String.IsNullOrEmpty(dc.Value.ToString()) && Convert.ToDouble(dc.Value) != 0)
                    {
                        string mensagem = String.Format("Linha: {0} - Campo Percentual Frete Inválido.\n Para Tipo Cobrança igual 1 campo Percentual de Frete deve ser 0.", linha);
                        MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
            }

            return true;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void Limpar()
        {
            cmbCanal.Enabled = true;
            cmbCanal.SelectedIndex = 0;
            cmbCDOrigem.Enabled = true;
            cmbCDOrigem.SelectedIndex = 0;
            cmbUfDestino.Enabled = true;
            cmbUfDestino.SelectedIndex = 0;
            cmbCidadeDestino.Enabled = false;
            cmbCidadeDestino.SelectedIndex = 0;
            grdPoliticaFrete.Enabled = false;
            grdPoliticaFrete.DataSource = null;
            grdPoliticaFrete.Rows.Clear();
            btnSalvar.Enabled = false;
            lblDataVigencia.Visible = false;
            dtpDataVigencia.Visible = false;
            grdPoliticaFrete.Enabled = false;
            dtpDataVigencia.Text = "";
            lbCodigoItinerario.Visible = false;
            lbCodItinerarioAlt.Visible = false;
        }

        private void grdPoliticaFrete_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (e.ColumnIndex == 3 && e.RowIndex != this.grdPoliticaFrete.NewRowIndex && e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = d.ToString("N2");
            }

            if (e.ColumnIndex == 6 && e.RowIndex != this.grdPoliticaFrete.NewRowIndex && e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = d.ToString("N2");
            }

            if (e.ColumnIndex == 7 && e.RowIndex != this.grdPoliticaFrete.NewRowIndex && e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = d.ToString("N2");
            }

            if (e.ColumnIndex == 8 && e.RowIndex != this.grdPoliticaFrete.NewRowIndex && e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = d.ToString("N2");
            }

            if (e.ColumnIndex == 9 && e.RowIndex != this.grdPoliticaFrete.NewRowIndex && e.Value != null && !String.IsNullOrEmpty(e.Value.ToString()))
            {
                double d = double.Parse(e.Value.ToString());
                e.Value = d.ToString("N2");
            }
        }

        private void grdPoliticaFrete_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (grdPoliticaFrete.CurrentCell.ColumnIndex == 0 || grdPoliticaFrete.CurrentCell.ColumnIndex == 2 || grdPoliticaFrete.CurrentCell.ColumnIndex == 4)
            {
                e.Control.KeyPress -= new KeyPressEventHandler(Inteiro_KeyPress);
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Inteiro_KeyPress);
                }

            }

            if (grdPoliticaFrete.CurrentCell.ColumnIndex == 3
                || grdPoliticaFrete.CurrentCell.ColumnIndex == 6 || grdPoliticaFrete.CurrentCell.ColumnIndex == 7
                || grdPoliticaFrete.CurrentCell.ColumnIndex == 8 || grdPoliticaFrete.CurrentCell.ColumnIndex == 9)
            {
                e.Control.KeyPress -= new KeyPressEventHandler(Double_KeyPress);
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Double_KeyPress);
                }
            }

        }

        private void Inteiro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Double_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar == ',')
                {
                    DataGridViewTextBoxEditingControl ctrl = sender as DataGridViewTextBoxEditingControl;
                    if (ctrl != null)
                    {
                        string s = ctrl.Text + ',';
                        decimal d;
                        e.Handled = !decimal.TryParse(s, out d);
                    }
                }
                else if (e.KeyChar != '\b') //allow the backspace key
                {
                    e.Handled = true;
                }

            }
        }

        private void frmNovaPoliticaFrete_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (teveInsercaoAlteracao)
            {
                politicaFreteUC.Consultar();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //TI-5946
        private void grdPoliticaFrete_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int codigo;
            CodigoTransportadoraBE transp = new CodigoTransportadoraBE();

            if (grdPoliticaFrete.Rows.Count > 0)
            {
                for (int i = 0; i < grdPoliticaFrete.Rows.Count; i++)
                {
                    if (grdPoliticaFrete.Rows[i].Cells[0].Value != null)
                    {
                        codigo = int.TryParse(grdPoliticaFrete.Rows[i].Cells[0].Value.ToString(), out codigo) ? codigo : 0;
                        transp = listaCodigosTransportadora.Find(o => o.COD_TRANSP == codigo);
                        if (transp != null) grdPoliticaFrete.Rows[i].Cells[1].Value = transp.NOME_TRANSP;
                    }
                }
            }
        }
        //FIM TI-5946
    }
}
