﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace PainelFreteDPK
{
    public partial class frmHistoricoGerente : Form
    {
        public frmHistoricoGerente()
        {
            InitializeComponent();
            CarregarComboGerente(cmbGerente);

        }

        private void CarregarComboGerente(ComboBox cmbGerente)
        {
            List<ComboGerente> listaGerente = null;
            cmbGerente.DataSource = null;
            cmbGerente.Items.Clear();
            cmbGerente.DisplayMember = "DESCNOME";
            cmbGerente.ValueMember = "CPF";

            try
            {
                listaGerente = GlobalBE.listaGerente;
                cmbGerente.DataSource = listaGerente;
                cmbGerente.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarGerente, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                FiltroHistGerente oFiltro = new FiltroHistGerente();
                oFiltro.CPF = ((ComboGerente)cmbGerente.SelectedItem).CPF;
                oFiltro.TIPO = ((ComboGerente)cmbGerente.SelectedItem).TIPO;
                if (dtpDataInicio.Checked == true)
                {
                    oFiltro.DTINI = dtpDataInicio.Value.ToShortDateString();
                }
                else
                {
                    oFiltro.DTINI = null;
                }
                if (dtpDataInicio.Checked == false && dtpDataFim.Checked == true ||
                   dtpDataInicio.Checked == true && dtpDataFim.Checked == false)
                {
                    MessageBox.Show("Para os filtros de data serem válidos, é necessario marcar Data Início e Data Fim.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (dtpDataInicio.Value.Date > dtpDataFim.Value.Date)
                {
                    MessageBox.Show("Data Fim deve ser maior que a Data Início.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (dtpDataFim.Checked == true)
                {
                    oFiltro.DTFIM = dtpDataFim.Value.ToShortDateString();
                }
                else
                {
                    oFiltro.DTFIM = null;
                }

                grdHistoricoGerente.AutoGenerateColumns = false;
                List<HistoricoGerente> lista = GerenteBO.BuscarHistoricoGerente(oFiltro);
                grdHistoricoGerente.DataSource = lista;
                if (lista.Count == 0)
                {
                    string mensagem = "Nenhum histórico encontrado.";
                    MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo btnConsultar_Click.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            try
            {
                cmbGerente.SelectedIndex = 0;
                dtpDataInicio.Text = DateTime.Now.ToShortDateString();
                dtpDataFim.Text = DateTime.Now.ToShortDateString();
                List<HistoricoGerente> lista = null;
                grdHistoricoGerente.AutoGenerateColumns = false;
                grdHistoricoGerente.DataSource = lista;
                grdHistoricoGerente.Update();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo btnLimpar_Click.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
    }
}
