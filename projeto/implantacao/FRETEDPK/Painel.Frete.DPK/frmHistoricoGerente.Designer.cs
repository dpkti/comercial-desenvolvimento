﻿namespace PainelFreteDPK
{
    partial class frmHistoricoGerente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdHistoricoGerente = new System.Windows.Forms.DataGridView();
            this.DATA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GERENTEREGIONAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIMITEANT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALDOANT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIMITE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USUARIO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbGerente = new System.Windows.Forms.ComboBox();
            this.lblGerente = new System.Windows.Forms.Label();
            this.dtpDataFim = new System.Windows.Forms.DateTimePicker();
            this.dtpDataInicio = new System.Windows.Forms.DateTimePicker();
            this.lblDtFim = new System.Windows.Forms.Label();
            this.lblDtInicio = new System.Windows.Forms.Label();
            this.btnLimpar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricoGerente)).BeginInit();
            this.SuspendLayout();
            // 
            // grdHistoricoGerente
            // 
            this.grdHistoricoGerente.AllowUserToAddRows = false;
            this.grdHistoricoGerente.AllowUserToDeleteRows = false;
            this.grdHistoricoGerente.AllowUserToOrderColumns = true;
            this.grdHistoricoGerente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdHistoricoGerente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DATA,
            this.NOME,
            this.FILIAL,
            this.GERENTEREGIONAL,
            this.LIMITEANT,
            this.SALDOANT,
            this.LIMITE,
            this.SALDO,
            this.OPERACAO,
            this.USUARIO});
            this.grdHistoricoGerente.Location = new System.Drawing.Point(12, 73);
            this.grdHistoricoGerente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grdHistoricoGerente.Name = "grdHistoricoGerente";
            this.grdHistoricoGerente.ReadOnly = true;
            this.grdHistoricoGerente.RowTemplate.Height = 24;
            this.grdHistoricoGerente.Size = new System.Drawing.Size(1296, 541);
            this.grdHistoricoGerente.TabIndex = 0;
            // 
            // DATA
            // 
            this.DATA.DataPropertyName = "DATA";
            this.DATA.HeaderText = "Data";
            this.DATA.Name = "DATA";
            this.DATA.ReadOnly = true;
            // 
            // NOME
            // 
            this.NOME.DataPropertyName = "NOME";
            this.NOME.HeaderText = "Gerente";
            this.NOME.Name = "NOME";
            this.NOME.ReadOnly = true;
            this.NOME.Width = 200;
            // 
            // FILIAL
            // 
            this.FILIAL.DataPropertyName = "FILIAL";
            this.FILIAL.HeaderText = "Filial";
            this.FILIAL.Name = "FILIAL";
            this.FILIAL.ReadOnly = true;
            this.FILIAL.Width = 200;
            // 
            // GERENTEREGIONAL
            // 
            this.GERENTEREGIONAL.DataPropertyName = "GERENTEREGIONAL";
            this.GERENTEREGIONAL.HeaderText = "Gerente Regional";
            this.GERENTEREGIONAL.Name = "GERENTEREGIONAL";
            this.GERENTEREGIONAL.ReadOnly = true;
            this.GERENTEREGIONAL.Width = 200;
            // 
            // LIMITEANT
            // 
            this.LIMITEANT.DataPropertyName = "LIMITEANT";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N2";
            this.LIMITEANT.DefaultCellStyle = dataGridViewCellStyle1;
            this.LIMITEANT.HeaderText = "Limite Anterior";
            this.LIMITEANT.Name = "LIMITEANT";
            this.LIMITEANT.ReadOnly = true;
            // 
            // SALDOANT
            // 
            this.SALDOANT.DataPropertyName = "SALDOANT";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.Format = "N2";
            this.SALDOANT.DefaultCellStyle = dataGridViewCellStyle2;
            this.SALDOANT.HeaderText = "Saldo Anterior";
            this.SALDOANT.Name = "SALDOANT";
            this.SALDOANT.ReadOnly = true;
            // 
            // LIMITE
            // 
            this.LIMITE.DataPropertyName = "LIMITE";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            this.LIMITE.DefaultCellStyle = dataGridViewCellStyle3;
            this.LIMITE.HeaderText = "Limite";
            this.LIMITE.Name = "LIMITE";
            this.LIMITE.ReadOnly = true;
            // 
            // SALDO
            // 
            this.SALDO.DataPropertyName = "SALDO";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            this.SALDO.DefaultCellStyle = dataGridViewCellStyle4;
            this.SALDO.HeaderText = "Saldo";
            this.SALDO.Name = "SALDO";
            this.SALDO.ReadOnly = true;
            // 
            // OPERACAO
            // 
            this.OPERACAO.DataPropertyName = "OPERACAO";
            this.OPERACAO.HeaderText = "Operação";
            this.OPERACAO.Name = "OPERACAO";
            this.OPERACAO.ReadOnly = true;
            // 
            // USUARIO
            // 
            this.USUARIO.DataPropertyName = "USUARIO";
            this.USUARIO.HeaderText = "Usuário";
            this.USUARIO.Name = "USUARIO";
            this.USUARIO.ReadOnly = true;
            this.USUARIO.Width = 150;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(1199, 30);
            this.btnConsultar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(109, 39);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbGerente
            // 
            this.cmbGerente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGerente.FormattingEnabled = true;
            this.cmbGerente.Location = new System.Drawing.Point(79, 29);
            this.cmbGerente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbGerente.Name = "cmbGerente";
            this.cmbGerente.Size = new System.Drawing.Size(412, 24);
            this.cmbGerente.TabIndex = 0;
            // 
            // lblGerente
            // 
            this.lblGerente.AutoSize = true;
            this.lblGerente.Location = new System.Drawing.Point(9, 32);
            this.lblGerente.Name = "lblGerente";
            this.lblGerente.Size = new System.Drawing.Size(64, 17);
            this.lblGerente.TabIndex = 21;
            this.lblGerente.Text = "Gerente:";
            // 
            // dtpDataFim
            // 
            this.dtpDataFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFim.Location = new System.Drawing.Point(886, 32);
            this.dtpDataFim.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataFim.Name = "dtpDataFim";
            this.dtpDataFim.ShowCheckBox = true;
            this.dtpDataFim.Size = new System.Drawing.Size(132, 22);
            this.dtpDataFim.TabIndex = 2;
            // 
            // dtpDataInicio
            // 
            this.dtpDataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataInicio.Location = new System.Drawing.Point(629, 32);
            this.dtpDataInicio.Margin = new System.Windows.Forms.Padding(4);
            this.dtpDataInicio.Name = "dtpDataInicio";
            this.dtpDataInicio.ShowCheckBox = true;
            this.dtpDataInicio.Size = new System.Drawing.Size(132, 22);
            this.dtpDataInicio.TabIndex = 1;
            // 
            // lblDtFim
            // 
            this.lblDtFim.AutoSize = true;
            this.lblDtFim.Location = new System.Drawing.Point(809, 35);
            this.lblDtFim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDtFim.Name = "lblDtFim";
            this.lblDtFim.Size = new System.Drawing.Size(68, 17);
            this.lblDtFim.TabIndex = 25;
            this.lblDtFim.Text = "Data Fim:";
            // 
            // lblDtInicio
            // 
            this.lblDtInicio.AutoSize = true;
            this.lblDtInicio.Location = new System.Drawing.Point(537, 32);
            this.lblDtInicio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDtInicio.Name = "lblDtInicio";
            this.lblDtInicio.Size = new System.Drawing.Size(78, 17);
            this.lblDtInicio.TabIndex = 24;
            this.lblDtInicio.Text = "Data Início:";
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(1085, 30);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(109, 39);
            this.btnLimpar.TabIndex = 3;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // frmHistoricoGerente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1321, 637);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.dtpDataFim);
            this.Controls.Add(this.dtpDataInicio);
            this.Controls.Add(this.lblDtFim);
            this.Controls.Add(this.lblDtInicio);
            this.Controls.Add(this.lblGerente);
            this.Controls.Add(this.cmbGerente);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.grdHistoricoGerente);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "frmHistoricoGerente";
            this.Text = "Histórico Gerente               ";
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricoGerente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdHistoricoGerente;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cmbGerente;
        private System.Windows.Forms.Label lblGerente;
        private System.Windows.Forms.DateTimePicker dtpDataFim;
        private System.Windows.Forms.DateTimePicker dtpDataInicio;
        private System.Windows.Forms.Label lblDtFim;
        private System.Windows.Forms.Label lblDtInicio;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn GERENTEREGIONAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIMITEANT;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALDOANT;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIMITE;
        private System.Windows.Forms.DataGridViewTextBoxColumn SALDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn USUARIO;
    }
}