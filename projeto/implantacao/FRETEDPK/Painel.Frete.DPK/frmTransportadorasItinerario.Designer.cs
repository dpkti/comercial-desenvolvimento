﻿namespace PainelFreteDPK
{
    partial class frmTransportadorasItinerario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvTransportadoras = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cnpj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbCdOrigem = new System.Windows.Forms.Label();
            this.lbCdOrigemAlt = new System.Windows.Forms.Label();
            this.lbCidadeDestino = new System.Windows.Forms.Label();
            this.lbCiadeDestinoAlt = new System.Windows.Forms.Label();
            this.lbUfDestino = new System.Windows.Forms.Label();
            this.lbUfDestinoAlt = new System.Windows.Forms.Label();
            this.lbCodigoItinerario = new System.Windows.Forms.Label();
            this.lbCodigoItinerarioAlt = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransportadoras)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTransportadoras
            // 
            this.dgvTransportadoras.AllowUserToAddRows = false;
            this.dgvTransportadoras.AllowUserToDeleteRows = false;
            this.dgvTransportadoras.AllowUserToOrderColumns = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTransportadoras.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTransportadoras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTransportadoras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Cnpj,
            this.Nome});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTransportadoras.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvTransportadoras.Location = new System.Drawing.Point(23, 65);
            this.dgvTransportadoras.Name = "dgvTransportadoras";
            this.dgvTransportadoras.ReadOnly = true;
            this.dgvTransportadoras.RowTemplate.Height = 24;
            this.dgvTransportadoras.Size = new System.Drawing.Size(904, 285);
            this.dgvTransportadoras.TabIndex = 0;
            // 
            // Codigo
            // 
            this.Codigo.DataPropertyName = "codTransportadora";
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // Cnpj
            // 
            this.Cnpj.DataPropertyName = "cnpj";
            this.Cnpj.HeaderText = "Cnpj";
            this.Cnpj.Name = "Cnpj";
            this.Cnpj.ReadOnly = true;
            this.Cnpj.Width = 150;
            // 
            // Nome
            // 
            this.Nome.DataPropertyName = "nome";
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            this.Nome.Width = 300;
            // 
            // lbCdOrigem
            // 
            this.lbCdOrigem.AutoSize = true;
            this.lbCdOrigem.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCdOrigem.Location = new System.Drawing.Point(232, 24);
            this.lbCdOrigem.Name = "lbCdOrigem";
            this.lbCdOrigem.Size = new System.Drawing.Size(91, 17);
            this.lbCdOrigem.TabIndex = 1;
            this.lbCdOrigem.Text = "CD Origem:";
            // 
            // lbCdOrigemAlt
            // 
            this.lbCdOrigemAlt.AutoSize = true;
            this.lbCdOrigemAlt.Location = new System.Drawing.Point(329, 24);
            this.lbCdOrigemAlt.Name = "lbCdOrigemAlt";
            this.lbCdOrigemAlt.Size = new System.Drawing.Size(0, 17);
            this.lbCdOrigemAlt.TabIndex = 2;
            // 
            // lbCidadeDestino
            // 
            this.lbCidadeDestino.AutoSize = true;
            this.lbCidadeDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCidadeDestino.Location = new System.Drawing.Point(461, 24);
            this.lbCidadeDestino.Name = "lbCidadeDestino";
            this.lbCidadeDestino.Size = new System.Drawing.Size(123, 17);
            this.lbCidadeDestino.TabIndex = 3;
            this.lbCidadeDestino.Text = "Cidade Destino:";
            // 
            // lbCiadeDestinoAlt
            // 
            this.lbCiadeDestinoAlt.AutoSize = true;
            this.lbCiadeDestinoAlt.Location = new System.Drawing.Point(590, 24);
            this.lbCiadeDestinoAlt.Name = "lbCiadeDestinoAlt";
            this.lbCiadeDestinoAlt.Size = new System.Drawing.Size(0, 17);
            this.lbCiadeDestinoAlt.TabIndex = 4;
            // 
            // lbUfDestino
            // 
            this.lbUfDestino.AutoSize = true;
            this.lbUfDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUfDestino.Location = new System.Drawing.Point(800, 24);
            this.lbUfDestino.Name = "lbUfDestino";
            this.lbUfDestino.Size = new System.Drawing.Size(93, 17);
            this.lbUfDestino.TabIndex = 5;
            this.lbUfDestino.Text = "UF Destino:";
            // 
            // lbUfDestinoAlt
            // 
            this.lbUfDestinoAlt.AutoSize = true;
            this.lbUfDestinoAlt.Location = new System.Drawing.Point(892, 24);
            this.lbUfDestinoAlt.Name = "lbUfDestinoAlt";
            this.lbUfDestinoAlt.Size = new System.Drawing.Size(0, 17);
            this.lbUfDestinoAlt.TabIndex = 6;
            // 
            // lbCodigoItinerario
            // 
            this.lbCodigoItinerario.AutoSize = true;
            this.lbCodigoItinerario.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodigoItinerario.Location = new System.Drawing.Point(23, 24);
            this.lbCodigoItinerario.Name = "lbCodigoItinerario";
            this.lbCodigoItinerario.Size = new System.Drawing.Size(133, 17);
            this.lbCodigoItinerario.TabIndex = 7;
            this.lbCodigoItinerario.Text = "Código Itinerário:";
            // 
            // lbCodigoItinerarioAlt
            // 
            this.lbCodigoItinerarioAlt.AutoSize = true;
            this.lbCodigoItinerarioAlt.Location = new System.Drawing.Point(163, 24);
            this.lbCodigoItinerarioAlt.Name = "lbCodigoItinerarioAlt";
            this.lbCodigoItinerarioAlt.Size = new System.Drawing.Size(0, 17);
            this.lbCodigoItinerarioAlt.TabIndex = 8;
            // 
            // frmTransportadorasItinerario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 376);
            this.Controls.Add(this.lbCodigoItinerarioAlt);
            this.Controls.Add(this.lbCodigoItinerario);
            this.Controls.Add(this.lbUfDestinoAlt);
            this.Controls.Add(this.lbUfDestino);
            this.Controls.Add(this.lbCiadeDestinoAlt);
            this.Controls.Add(this.lbCidadeDestino);
            this.Controls.Add(this.lbCdOrigemAlt);
            this.Controls.Add(this.lbCdOrigem);
            this.Controls.Add(this.dgvTransportadoras);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTransportadorasItinerario";
            this.Text = "Transportadoras Itinerário";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransportadoras)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTransportadoras;
        private System.Windows.Forms.Label lbCdOrigem;
        private System.Windows.Forms.Label lbCdOrigemAlt;
        private System.Windows.Forms.Label lbCidadeDestino;
        private System.Windows.Forms.Label lbCiadeDestinoAlt;
        private System.Windows.Forms.Label lbUfDestino;
        private System.Windows.Forms.Label lbUfDestinoAlt;
        private System.Windows.Forms.Label lbCodigoItinerario;
        private System.Windows.Forms.Label lbCodigoItinerarioAlt;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cnpj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
    }
}