﻿namespace PainelFreteDPK
{
    partial class frmImportarExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTipoArquivo = new System.Windows.Forms.Label();
            this.lbArquivo = new System.Windows.Forms.Label();
            this.btnImportar = new System.Windows.Forms.Button();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.txtCaminhoArquivo = new System.Windows.Forms.TextBox();
            this.oFDPlanilha = new System.Windows.Forms.OpenFileDialog();
            this.lbStatus = new System.Windows.Forms.Label();
            this.lbStatusProc = new System.Windows.Forms.Label();
            this.bgWorkerPoliticaFrete = new System.ComponentModel.BackgroundWorker();
            this.lbQtdInserido = new System.Windows.Forms.Label();
            this.lbQtdInseridoAlt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbTipoArquivo
            // 
            this.lbTipoArquivo.AutoSize = true;
            this.lbTipoArquivo.Location = new System.Drawing.Point(625, 95);
            this.lbTipoArquivo.Name = "lbTipoArquivo";
            this.lbTipoArquivo.Size = new System.Drawing.Size(267, 17);
            this.lbTipoArquivo.TabIndex = 9;
            this.lbTipoArquivo.Text = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx\"";
            // 
            // lbArquivo
            // 
            this.lbArquivo.AutoSize = true;
            this.lbArquivo.Location = new System.Drawing.Point(53, 73);
            this.lbArquivo.Name = "lbArquivo";
            this.lbArquivo.Size = new System.Drawing.Size(60, 17);
            this.lbArquivo.TabIndex = 8;
            this.lbArquivo.Text = "Arquivo:";
            // 
            // btnImportar
            // 
            this.btnImportar.Location = new System.Drawing.Point(898, 101);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(100, 28);
            this.btnImportar.TabIndex = 7;
            this.btnImportar.Text = "Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(898, 67);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(100, 28);
            this.btnAbrir.TabIndex = 6;
            this.btnAbrir.Text = "Abrir...";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // txtCaminhoArquivo
            // 
            this.txtCaminhoArquivo.Location = new System.Drawing.Point(119, 70);
            this.txtCaminhoArquivo.Name = "txtCaminhoArquivo";
            this.txtCaminhoArquivo.Size = new System.Drawing.Size(773, 22);
            this.txtCaminhoArquivo.TabIndex = 5;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbStatus.Location = new System.Drawing.Point(357, 159);
            this.lbStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(182, 29);
            this.lbStatus.TabIndex = 11;
            this.lbStatus.Text = "Processando...";
            this.lbStatus.Visible = false;
            // 
            // lbStatusProc
            // 
            this.lbStatusProc.AutoSize = true;
            this.lbStatusProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusProc.Location = new System.Drawing.Point(51, 159);
            this.lbStatusProc.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbStatusProc.Name = "lbStatusProc";
            this.lbStatusProc.Size = new System.Drawing.Size(311, 29);
            this.lbStatusProc.TabIndex = 10;
            this.lbStatusProc.Text = "Status do Processamento:";
            this.lbStatusProc.Visible = false;
            // 
            // bgWorkerPoliticaFrete
            // 
            this.bgWorkerPoliticaFrete.WorkerReportsProgress = true;
            this.bgWorkerPoliticaFrete.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerPoliticaFrete_DoWork);
            this.bgWorkerPoliticaFrete.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerPoliticaFrete_ProgressChanged);
            this.bgWorkerPoliticaFrete.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerPoliticaFrete_RunWorkerCompleted);
            // 
            // lbQtdInserido
            // 
            this.lbQtdInserido.AutoSize = true;
            this.lbQtdInserido.Location = new System.Drawing.Point(16, 249);
            this.lbQtdInserido.Name = "lbQtdInserido";
            this.lbQtdInserido.Size = new System.Drawing.Size(140, 17);
            this.lbQtdInserido.TabIndex = 12;
            this.lbQtdInserido.Text = "Quantidade inserido:";
            // 
            // lbQtdInseridoAlt
            // 
            this.lbQtdInseridoAlt.AutoSize = true;
            this.lbQtdInseridoAlt.Location = new System.Drawing.Point(158, 249);
            this.lbQtdInseridoAlt.Name = "lbQtdInseridoAlt";
            this.lbQtdInseridoAlt.Size = new System.Drawing.Size(0, 17);
            this.lbQtdInseridoAlt.TabIndex = 13;
            // 
            // frmImportarExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 275);
            this.Controls.Add(this.lbQtdInseridoAlt);
            this.Controls.Add(this.lbQtdInserido);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.lbStatusProc);
            this.Controls.Add(this.lbTipoArquivo);
            this.Controls.Add(this.lbArquivo);
            this.Controls.Add(this.btnImportar);
            this.Controls.Add(this.btnAbrir);
            this.Controls.Add(this.txtCaminhoArquivo);
            this.Name = "frmImportarExcel";
            this.Text = "Manutenção Politica Frete - Importação Planilha";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTipoArquivo;
        private System.Windows.Forms.Label lbArquivo;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.TextBox txtCaminhoArquivo;
        private System.Windows.Forms.OpenFileDialog oFDPlanilha;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbStatusProc;
        private System.ComponentModel.BackgroundWorker bgWorkerPoliticaFrete;
        private System.Windows.Forms.Label lbQtdInserido;
        private System.Windows.Forms.Label lbQtdInseridoAlt;
    }
}