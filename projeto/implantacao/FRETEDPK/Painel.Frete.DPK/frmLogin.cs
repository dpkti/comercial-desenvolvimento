﻿using System;
using System.Windows.Forms;
using Entities;
using UtilGeralDPA;
using Business;
using System.Globalization;

namespace PainelFreteDPK
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            txtUsuario.Select();
            this.CenterToScreen();

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ",";
            customCulture.NumberFormat.NumberGroupSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            string versao = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersao.Text = "Versão: " + versao.Substring(0, versao.LastIndexOf('.'));
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            btnEntrar.Select();
            this.Refresh();

            Boolean acessoPainel;
            UsuarioBE oUser = new UsuarioBE();
            oUser.LOGIN = txtUsuario.Text;
            oUser.SENHA = txtSenha.Text;
            lblErro.Text = String.Empty;
            GlobalBE.usuarioLogado = oUser;

            try
            {
                string nomeAplicacao = ConfigurationAccess.ObterConfiguracao("nomeAplicacao").ToString();

                lblErro.Text = "Validando o usuário...";
                lblErro.Update();

                if (string.IsNullOrEmpty(oUser.LOGIN) || string.IsNullOrEmpty(oUser.SENHA))
                {
                    lblErro.Text = "";
                    lblErro.Update();
                    MessageBox.Show(ResourcePainelFreteDPK.Erro_UsuarioSenhaInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    oUser = LoginBO.ValidaUsuario(oUser);

                    if (oUser != null)
                    {
                        acessoPainel = LoginBO.ValidaAcessoPainel(oUser.LOGIN);
                        if (oUser.PERMISSAO_ACESSO && acessoPainel == true)
                        {
                            lblErro.Text = "Validação OK";
                            lblErro.Update();

                            this.Hide();

                            frmPainelParametros frmPrincipal = new frmPainelParametros(oUser);

                            lblErro.Text = "";
                            lblErro.Update();

                            frmPrincipal.ShowDialog();
                        }
                        else
                        {
                            lblErro.Text = "";
                            lblErro.Update();
                            MessageBox.Show(ResourcePainelFreteDPK.Erro_UsuarioSemPermissaoDeAcesso, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);    
                        }
                    }
                    else
                    {
                        lblErro.Text = "";
                        lblErro.Update();
                        MessageBox.Show(ResourcePainelFreteDPK.Erro_UsuarioSenhaInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

