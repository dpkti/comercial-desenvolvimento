﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;
using System.Diagnostics;

namespace PainelFreteDPK
{
    public partial class GerenteLimiteUC : UserControl
    {
        List<GerenteBE> listaGerentes = new List<GerenteBE>();
        public GerenteLimiteUC()
        {
            InitializeComponent();
        }

        private void GerenteLimiteUC_Load(object sender, EventArgs e)
        {
            try
            {
                listaGerentes = GerenteBO.BuscaGerenteRegionalGrid();
                grdGerenteLimite.AutoGenerateColumns = false;
                grdGerenteLimite.DataSource = listaGerentes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo GerenteLimiteUC_Load.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        public void AtualizaGrid()
        {
            try
            {
                listaGerentes = GerenteBO.BuscaGerenteRegionalGrid();
                grdGerenteLimite.DataSource = listaGerentes;
                grdGerenteLimite.Update();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo AtualizaGrid.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void grdGerenteLimite_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
                {
                    grdGerenteLimite.ClearSelection();
                    grdGerenteLimite.Rows[e.RowIndex].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo grdGerenteLimite_CellMouseClick.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnHistorico_Click(object sender, EventArgs e)
        {
            try
            {
                frmHistoricoGerente oFrmHistoricoGerente = new frmHistoricoGerente();
                oFrmHistoricoGerente.StartPosition = FormStartPosition.CenterParent;
                oFrmHistoricoGerente.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo btnHistorico_Click.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void grdGerenteLimite_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                switch (e.Button)
                {
                    case MouseButtons.Right:

                        if (grdGerenteLimite.SelectedRows.Count <= 0) return;

                        ContextMenu myContextMenu = new ContextMenu();

                        MenuItem menuItem0 = new MenuItem("Inserir Gerente");
                        MenuItem menuItem1 = new MenuItem("Listar Gerentes");

                        myContextMenu.MenuItems.Add(menuItem0);

                        if (grdGerenteLimite.SelectedRows.Count == 1)
                        {
                            myContextMenu.MenuItems.Add(menuItem0);
                            myContextMenu.MenuItems.Add(menuItem1);
                            InserirAlterarGerente gerenteRegional = new InserirAlterarGerente();
                            gerenteRegional.CPF_REGIONAL = Convert.ToInt64(grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["CPF"].Value);
                            gerenteRegional.NOME_REGIONAL = grdGerenteLimite.Rows[grdGerenteLimite.SelectedRows[0].Index].Cells["NOME"].Value.ToString();
                            menuItem0.Click += new EventHandler(delegate (Object o, EventArgs a)
                            {
                                frmInserirAtualizarGerente frmInserirAtualizarGerente = new frmInserirAtualizarGerente(gerenteRegional, this, null, TipoAcao.Inserir);
                                frmInserirAtualizarGerente.Text = "Inserir Gerente";
                                frmInserirAtualizarGerente.StartPosition = FormStartPosition.CenterParent;
                                frmInserirAtualizarGerente.ShowDialog();
                            });

                            menuItem1.Click += new EventHandler(delegate (Object o, EventArgs a)
                            {
                                frmGerenteLimite frmGerenteLimite = new frmGerenteLimite(gerenteRegional, this);
                                frmGerenteLimite.StartPosition = FormStartPosition.CenterParent;
                                frmGerenteLimite.ShowDialog();
                            });
                        }

                        myContextMenu.Show(grdGerenteLimite, e.Location, LeftRightAlignment.Right);

                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.Titulo_MessageBoxErro, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnPedidosLiberados_Click(object sender, EventArgs e)
        {
            frmPedidosLiberadosGerente frmPedidosLiberadosGerente = new frmPedidosLiberadosGerente();
            frmPedidosLiberadosGerente.StartPosition = FormStartPosition.CenterParent;
            frmPedidosLiberadosGerente.ShowDialog();
        }

        private void btnCadGerente_Click(object sender, EventArgs e)
        {
            frmCadastroGerente frmCadastroGerente = new frmCadastroGerente();
            frmCadastroGerente.oGerenteLimiteUC = this;
            frmCadastroGerente.StartPosition = FormStartPosition.CenterParent;
            frmCadastroGerente.ShowDialog();
        }
        //TI-5946
        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    lbArquivo.Text = openFileDialog1.FileName;
                    string retLogGeraArq = string.Empty;
                    var lstGerente = PainelParamentroBO.BuscarGerente();
                    var lstFilial = PainelParamentroBO.BuscarFilial();
                    var excel = GerenteBO.CarregarExcel(lstGerente, listaGerentes, lstFilial, lbArquivo.Text, out retLogGeraArq);
                    if (!retLogGeraArq.Equals(string.Empty))
                    {
                        if (MessageBox.Show("Ocorreram problemas durante a inserção, deseja ver Log?",
                                   "Politica Frete - Gerente Limite", MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            Process.Start("notepad.exe", retLogGeraArq);
                        }
                    }
                    else
                    {
                        excel.ForEach(o => {
                            GerenteBO.GravarGerente(o);
                        });
                        lbArquivo.Text = string.Empty;
                        MessageBox.Show("Limite de Gerente gravado com sucesso.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo btnImport_Click.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }
        //TI-5946
    }
}
