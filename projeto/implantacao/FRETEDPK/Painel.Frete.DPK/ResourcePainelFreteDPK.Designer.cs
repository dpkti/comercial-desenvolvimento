﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PainelFreteDPK {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ResourcePainelFreteDPK {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResourcePainelFreteDPK() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("PainelFreteDPK.ResourcePainelFreteDPK", typeof(ResourcePainelFreteDPK).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campo {0} está inválido..
        /// </summary>
        internal static string Erro_CampoInvalido {
            get {
                return ResourceManager.GetString("Erro_CampoInvalido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} maior que {1}..
        /// </summary>
        internal static string Erro_CampoMaiorQue {
            get {
                return ResourceManager.GetString("Erro_CampoMaiorQue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campo {0} não preenchido..
        /// </summary>
        internal static string Erro_CampoObrigatorioNaoPreenchido {
            get {
                return ResourceManager.GetString("Erro_CampoObrigatorioNaoPreenchido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informação não encontrada..
        /// </summary>
        internal static string Erro_InformacaoNaoEncontrada {
            get {
                return ResourceManager.GetString("Erro_InformacaoNaoEncontrada", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pelo menos um filtro de busca deve ser preenchido..
        /// </summary>
        internal static string Erro_PreenchimentoTodosCampos {
            get {
                return ResourceManager.GetString("Erro_PreenchimentoTodosCampos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preencher somente CGC OU Código Rastreamento..
        /// </summary>
        internal static string Erro_RastreamentoCgcPreenchido {
            get {
                return ResourceManager.GetString("Erro_RastreamentoCgcPreenchido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preencher somente Código Rastreamento OU Cod. Fabrica OU EAN..
        /// </summary>
        internal static string Erro_RastreamentoFabricaEanPreenchido {
            get {
                return ResourceManager.GetString("Erro_RastreamentoFabricaEanPreenchido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuário não tem permissão para acessar o sistema..
        /// </summary>
        internal static string Erro_UsuarioSemPermissaoDeAcesso {
            get {
                return ResourceManager.GetString("Erro_UsuarioSemPermissaoDeAcesso", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar itenerario..
        /// </summary>
        internal static string ErroCarregarCodItenerario
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar Itinerarios.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar Canal..
        /// </summary>
        internal static string ErroCarregarCodCanal
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar combo de Canal.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar filial..
        /// </summary>
        internal static string ErroCarregarFilial
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar combo Filial.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar gerente..
        /// </summary>
        internal static string ErroCarregarGerente
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar combo Gerente.", resourceCulture);
            }
        }
        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar uf destino..
        /// </summary>
        internal static string ErroCarregarUfDestino
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar Uf Destino.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar uf destino..
        /// </summary>
        internal static string ErroCarregarCidadeDestino
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar Cidade Destino.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar CD..
        /// </summary>
        internal static string ErroCarregarCdOrigem
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar códigos CD's.", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Usuário e/ou senha inválido(s)..
        /// </summary>
        internal static string Erro_UsuarioSenhaInvalido {
            get {
                return ResourceManager.GetString("Erro_UsuarioSenhaInvalido", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Erro no banco de dados..
        /// </summary>
        internal static string ErroBanco {
            get {
                return ResourceManager.GetString("ErroBanco", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Item não processado..
        /// </summary>
        internal static string MensagemItemProcessado {
            get {
                return ResourceManager.GetString("MensagemItemProcessado", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} resultado(s) encontrado(s)..
        /// </summary>
        internal static string MensagemResultadoBusca {
            get {
                return ResourceManager.GetString("MensagemResultadoBusca", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nenhum arquivo recebido desta interface..
        /// </summary>
        internal static string MensagemSemArquivoInterface {
            get {
                return ResourceManager.GetString("MensagemSemArquivoInterface", resourceCulture);
            }
        }


        internal static string Titulo_MessageBoxConfirmacao
        {
            get
            {
                return ResourceManager.GetString("Titulo_MessageBoxConfirmacao", resourceCulture);
            }
        }

        internal static string Titulo_MessageBoxAviso
        {
            get
            {
                return ResourceManager.GetString("Titulo_MessageBoxAviso", resourceCulture);
            }
        }

        /// <summary>
        ///  Looks up a localized string similar to Painel de Parâmetros Frete - Erro.
        /// </summary>
        internal static string Titulo_MessageBoxErro {
            get {
                return ResourceManager.GetString("Titulo_MessageBoxErro", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Itinerário já existente.
        /// </summary>
        internal static string Erro_ItinerarioExiste
        {
            get
            {
                return ResourceManager.GetString("Erro_ItinerarioExiste", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Itinerário já existente.
        /// </summary>
        internal static string Erro_InserirItinerario
        {
            get
            {
                return ResourceManager.GetString("Erro_InserirItinerario", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to sucesso inserção de itinerario.
        /// </summary>
        internal static string Sucesso_Itinerario_Insercao
        {
            get
            {
                return ResourceManager.GetString("Sucesso_Itinerario_Insercao", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to sucesso na alteração do itinerário.
        /// </summary>
        internal static string Sucesso_Itinerario_Alteracao
        {
            get
            {
                return ResourceManager.GetString("Sucesso_Itinerario_Alteracao", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to titulo sucesso.
        /// </summary>
        internal static string Titulo_MessageBoxSucesso
        {
            get
            {
                return ResourceManager.GetString("Titulo_MessageBoxSucesso", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro_ArquivoInvalido.
        /// </summary>
        internal static string Erro_ArquivoInvalido
        {
            get
            {
                return ResourceManager.GetString("Erro_ArquivoInvalido", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Erro ao carregar transportadora..
        /// </summary>
        internal static string ErroCarregarCodTransportadora
        {
            get
            {
                return ResourceManager.GetString("Falha ao carregar Transportadoras.", resourceCulture);
            }
        }
    }
}
