﻿namespace PainelFreteDPK
{
    partial class frmHistoricoLimite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdHistoricoLimite = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Limite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricoLimite)).BeginInit();
            this.SuspendLayout();
            // 
            // grdHistoricoLimite
            // 
            this.grdHistoricoLimite.AllowUserToAddRows = false;
            this.grdHistoricoLimite.AllowUserToDeleteRows = false;
            this.grdHistoricoLimite.AllowUserToOrderColumns = true;
            this.grdHistoricoLimite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdHistoricoLimite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.Usuario,
            this.Filial,
            this.Limite});
            this.grdHistoricoLimite.Location = new System.Drawing.Point(11, 11);
            this.grdHistoricoLimite.Margin = new System.Windows.Forms.Padding(2);
            this.grdHistoricoLimite.Name = "grdHistoricoLimite";
            this.grdHistoricoLimite.ReadOnly = true;
            this.grdHistoricoLimite.RowTemplate.Height = 24;
            this.grdHistoricoLimite.Size = new System.Drawing.Size(506, 325);
            this.grdHistoricoLimite.TabIndex = 2;
            // 
            // Data
            // 
            this.Data.DataPropertyName = "data";
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // Usuario
            // 
            this.Usuario.DataPropertyName = "usuario";
            this.Usuario.HeaderText = "Responsável";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            this.Usuario.Width = 200;
            // 
            // Filial
            // 
            this.Filial.DataPropertyName = "FILIAL";
            this.Filial.HeaderText = "Filial";
            this.Filial.Name = "Filial";
            this.Filial.ReadOnly = true;
            this.Filial.Width = 50;
            // 
            // Limite
            // 
            this.Limite.DataPropertyName = "LIMITE";
            dataGridViewCellStyle4.Format = "N2";
            this.Limite.DefaultCellStyle = dataGridViewCellStyle4;
            this.Limite.HeaderText = "Limite";
            this.Limite.Name = "Limite";
            this.Limite.ReadOnly = true;
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Location = new System.Drawing.Point(430, 337);
            this.btnExportarExcel.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(86, 32);
            this.btnExportarExcel.TabIndex = 11;
            this.btnExportarExcel.Text = "Exportar Excel";
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // frmHistoricoLimite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 371);
            this.Controls.Add(this.btnExportarExcel);
            this.Controls.Add(this.grdHistoricoLimite);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmHistoricoLimite";
            this.Text = "Histórico de Limite";
            ((System.ComponentModel.ISupportInitialize)(this.grdHistoricoLimite)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView grdHistoricoLimite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Limite;
        private System.Windows.Forms.Button btnExportarExcel;
    }
}