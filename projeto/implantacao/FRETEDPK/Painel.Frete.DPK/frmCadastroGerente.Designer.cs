﻿namespace PainelFreteDPK
{
    partial class frmCadastroGerente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCPF = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.lbNome = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.rbFilial = new System.Windows.Forms.RadioButton();
            this.rbCordenador = new System.Windows.Forms.RadioButton();
            this.rbRegional = new System.Windows.Forms.RadioButton();
            this.rbDesativado = new System.Windows.Forms.RadioButton();
            this.rbAtivo = new System.Windows.Forms.RadioButton();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.GbTipo = new System.Windows.Forms.GroupBox();
            this.GbSituacao = new System.Windows.Forms.GroupBox();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.GbTipo.SuspendLayout();
            this.GbSituacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCPF
            // 
            this.lbCPF.AutoSize = true;
            this.lbCPF.Location = new System.Drawing.Point(13, 41);
            this.lbCPF.Name = "lbCPF";
            this.lbCPF.Size = new System.Drawing.Size(38, 17);
            this.lbCPF.TabIndex = 0;
            this.lbCPF.Text = "CPF:";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(94, 38);
            this.txtCPF.MaxLength = 11;
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(138, 22);
            this.txtCPF.TabIndex = 1;
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPF_KeyPress);
            // 
            // lbNome
            // 
            this.lbNome.AutoSize = true;
            this.lbNome.Location = new System.Drawing.Point(16, 83);
            this.lbNome.Name = "lbNome";
            this.lbNome.Size = new System.Drawing.Size(49, 17);
            this.lbNome.TabIndex = 2;
            this.lbNome.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Location = new System.Drawing.Point(94, 80);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(419, 22);
            this.txtNome.TabIndex = 3;
            // 
            // rbFilial
            // 
            this.rbFilial.AutoSize = true;
            this.rbFilial.Location = new System.Drawing.Point(8, 21);
            this.rbFilial.Name = "rbFilial";
            this.rbFilial.Size = new System.Drawing.Size(57, 21);
            this.rbFilial.TabIndex = 5;
            this.rbFilial.TabStop = true;
            this.rbFilial.Text = "Filial";
            this.rbFilial.UseVisualStyleBackColor = true;
            // 
            // rbCordenador
            // 
            this.rbCordenador.AutoSize = true;
            this.rbCordenador.Location = new System.Drawing.Point(76, 21);
            this.rbCordenador.Name = "rbCordenador";
            this.rbCordenador.Size = new System.Drawing.Size(112, 21);
            this.rbCordenador.TabIndex = 6;
            this.rbCordenador.TabStop = true;
            this.rbCordenador.Text = "Coordenador";
            this.rbCordenador.UseVisualStyleBackColor = true;
            // 
            // rbRegional
            // 
            this.rbRegional.AutoSize = true;
            this.rbRegional.Location = new System.Drawing.Point(195, 21);
            this.rbRegional.Name = "rbRegional";
            this.rbRegional.Size = new System.Drawing.Size(85, 21);
            this.rbRegional.TabIndex = 7;
            this.rbRegional.TabStop = true;
            this.rbRegional.Text = "Regional";
            this.rbRegional.UseVisualStyleBackColor = true;
            // 
            // rbDesativado
            // 
            this.rbDesativado.AutoSize = true;
            this.rbDesativado.Location = new System.Drawing.Point(86, 17);
            this.rbDesativado.Name = "rbDesativado";
            this.rbDesativado.Size = new System.Drawing.Size(100, 21);
            this.rbDesativado.TabIndex = 10;
            this.rbDesativado.TabStop = true;
            this.rbDesativado.Text = "Desativado";
            this.rbDesativado.UseVisualStyleBackColor = true;
            // 
            // rbAtivo
            // 
            this.rbAtivo.AutoSize = true;
            this.rbAtivo.Location = new System.Drawing.Point(14, 17);
            this.rbAtivo.Name = "rbAtivo";
            this.rbAtivo.Size = new System.Drawing.Size(60, 21);
            this.rbAtivo.TabIndex = 9;
            this.rbAtivo.TabStop = true;
            this.rbAtivo.Text = "Ativo";
            this.rbAtivo.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(473, 250);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(134, 39);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.Location = new System.Drawing.Point(171, 250);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(134, 39);
            this.btnPesquisar.TabIndex = 12;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(322, 250);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(134, 39);
            this.btnSalvar.TabIndex = 13;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // GbTipo
            // 
            this.GbTipo.Controls.Add(this.rbFilial);
            this.GbTipo.Controls.Add(this.rbCordenador);
            this.GbTipo.Controls.Add(this.rbRegional);
            this.GbTipo.Location = new System.Drawing.Point(19, 118);
            this.GbTipo.Name = "GbTipo";
            this.GbTipo.Size = new System.Drawing.Size(289, 62);
            this.GbTipo.TabIndex = 14;
            this.GbTipo.TabStop = false;
            this.GbTipo.Text = "Tipo";
            // 
            // GbSituacao
            // 
            this.GbSituacao.Controls.Add(this.rbAtivo);
            this.GbSituacao.Controls.Add(this.rbDesativado);
            this.GbSituacao.Location = new System.Drawing.Point(19, 189);
            this.GbSituacao.Name = "GbSituacao";
            this.GbSituacao.Size = new System.Drawing.Size(200, 55);
            this.GbSituacao.TabIndex = 15;
            this.GbSituacao.TabStop = false;
            this.GbSituacao.Text = "Situação";
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(19, 250);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(134, 39);
            this.btnLimpar.TabIndex = 16;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // frmCadastroGerente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 303);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.GbSituacao);
            this.Controls.Add(this.GbTipo);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnPesquisar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.lbNome);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.lbCPF);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmCadastroGerente";
            this.Text = "Cadastro Gerente";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCadastroGerente_FormClosing);
            this.GbTipo.ResumeLayout(false);
            this.GbTipo.PerformLayout();
            this.GbSituacao.ResumeLayout(false);
            this.GbSituacao.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbCPF;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.Label lbNome;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.RadioButton rbFilial;
        private System.Windows.Forms.RadioButton rbCordenador;
        private System.Windows.Forms.RadioButton rbRegional;
        private System.Windows.Forms.RadioButton rbDesativado;
        private System.Windows.Forms.RadioButton rbAtivo;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.GroupBox GbTipo;
        private System.Windows.Forms.GroupBox GbSituacao;
        private System.Windows.Forms.Button btnLimpar;
    }
}