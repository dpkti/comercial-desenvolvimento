﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace PainelFreteDPK
{
    public partial class CadastroItinerarioUC : UserControl
    {
        private FiltroItinerario oFiltro;
        private List<Itinerario> listaItinerarios;

        public CadastroItinerarioUC()
        {
            InitializeComponent();
            CarregarComboCdOrigem(cmbCDOrigem);
            CarregarComboUfDestino(cmbUFDestino);
        }

        private void grdItinerario_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                grdItinerario.ClearSelection();
                grdItinerario.Rows[e.RowIndex].Selected = true;
            }

        }

        private void grdItinerario_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:

                    if (grdItinerario.SelectedRows.Count <= 0) return;

                    ContextMenu myContextMenu = new ContextMenu();

                    MenuItem menuItem0 = new MenuItem("Alterar Itinerário");
                    MenuItem menuItem1 = new MenuItem("Transportadoras Itinerário");

                    myContextMenu.MenuItems.Add(menuItem0);

                    if (grdItinerario.SelectedRows.Count == 1)
                    {
                        myContextMenu.MenuItems.Add(menuItem0);
                        myContextMenu.MenuItems.Add(menuItem1);

                        menuItem0.Click += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            frmItinerarioAlteracao frmItinerarioAlteracao = new frmItinerarioAlteracao(Convert.ToInt32(grdItinerario.SelectedRows[0].Cells[0].Value));
                            frmItinerarioAlteracao.StartPosition = FormStartPosition.CenterParent;
                            frmItinerarioAlteracao.ShowDialog();
                        });

                        menuItem1.Click += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            frmTransportadorasItinerario frmTranspotadoraItinerario = new frmTransportadorasItinerario(Convert.ToInt32(grdItinerario.SelectedRows[0].Cells[0].Value));
                            frmTranspotadoraItinerario.StartPosition = FormStartPosition.CenterParent;
                            frmTranspotadoraItinerario.ShowDialog();
                        });
                    }

                    myContextMenu.Show(grdItinerario, e.Location, LeftRightAlignment.Right);

                    break;
            }

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            //Creating DataTable
            DataTable dt = new DataTable();

            //Adding the Columns
            foreach (DataGridViewColumn column in grdItinerario.Columns)
            {
                dt.Columns.Add(column.HeaderText);
            }

            //Adding the Rows
            foreach (DataGridViewRow row in grdItinerario.Rows)
            {
                dt.Rows.Add();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                }
            }

            if (grdItinerario.Rows.Count < 100)
            {
                for (int j = grdItinerario.Rows.Count; j <= (grdItinerario.Rows.Count + 100); j++)
                {
                    dt.Rows.Add();
                    for (int i = 0; i <= grdItinerario.ColumnCount - 1; i++)
                    {
                        dt.Rows[j][i] = "";
                    }
                }
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            DataSet ds = new DataSet("Teste");
            ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
            ds.Tables.Add(dt);
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
            }

        }
        private void CarregarComboCdOrigem(ComboBox cmbCDOrigem)
        {
            List<CdOrigemBE> listaCdOrigem = null;
            cmbCDOrigem.DataSource = null;
            cmbCDOrigem.Items.Clear();
            cmbCDOrigem.DisplayMember = "CDORIGEM";
            cmbCDOrigem.ValueMember = "CD";
            
            try
            {
                listaCdOrigem = GlobalBE.listaCdOrigem;
                cmbCDOrigem.DataSource = listaCdOrigem;
                cmbCDOrigem.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregarComboUfDestino(ComboBox cmbUFDestino)
        {
            List<UfDestinoBE> listaUfDestino = null;
            cmbUFDestino.DataSource = null;
            cmbUFDestino.Items.Clear();
            cmbUFDestino.DisplayMember = "UF";
            cmbUFDestino.ValueMember = "CDUF";
            
            try
            {
                listaUfDestino = GlobalBE.listaUF;
                cmbUFDestino.DataSource = listaUfDestino;
                cmbUFDestino.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregarComboCidadeDestino(ComboBox cmbCidadeDestino, string uf)
        {
            try
            {
                cmbCidadeDestino.DataSource = null;
                cmbCidadeDestino.Items.Clear();
                DoubleBuffered = true;
                cmbCidadeDestino.DisplayMember = "CIDADE";
                cmbCidadeDestino.ValueMember = "CODCIDADE";
                List<CidadeDestinoBE> listaCidadeDestino = null;

                var listatmp = from l in GlobalBE.listaCidade where (l.UF == uf|| l.CODCIDADE == 0 || uf == null) select l;
                listaCidadeDestino = listatmp.ToList();                       

                cmbCidadeDestino.DataSource = listaCidadeDestino;
                DoubleBuffered = true;
                if (string.IsNullOrEmpty(uf))
                {
                    cmbCidadeDestino.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
        }

        private void cmbCidadeDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCidadeDestino.SelectedIndex != 0 && cmbCidadeDestino.SelectedIndex != -1)
            {
                string uf;
                uf = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).UF;
                cmbUFDestino.SelectedValue = uf;
            }
        }

        private void cmbUFDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cduf = null;
            CidadeDestinoBE cdcidade = new CidadeDestinoBE();
            if (cmbUFDestino.SelectedIndex != 0 && cmbUFDestino.SelectedIndex != -1)
            {
                cduf = ((UfDestinoBE)cmbUFDestino.SelectedItem).CDUF;
            }
            else if (cmbCidadeDestino.Items.Count > 0)
            {
                cmbCidadeDestino.SelectedIndex = 0;
            }
            if (cmbCidadeDestino.Items.Count > 0)
                {
                    cdcidade = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem);
                }
            if (cmbCidadeDestino.SelectedIndex == 0 || cmbCidadeDestino.SelectedIndex == -1 || cdcidade.UF != cduf)
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                DoubleBuffered = true;
            }
            else
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                cmbCidadeDestino.SelectedValue = cdcidade.CODCIDADE;
                DoubleBuffered = true;
            }
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            frmItinerarioAlteracao oFrmItinerarioAlteracao = new frmItinerarioAlteracao();
            oFrmItinerarioAlteracao.StartPosition = FormStartPosition.CenterParent;
            oFrmItinerarioAlteracao.ShowDialog();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            oFiltro = new FiltroItinerario();
            oFiltro.cdOrigem = ((CdOrigemBE)cmbCDOrigem.SelectedItem).CD;
            if (((CidadeDestinoBE)cmbCidadeDestino.SelectedItem) == null)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Cidade Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            oFiltro.codCidadeDestino = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).CODCIDADE;
            oFiltro.ufDestino = ((UfDestinoBE)cmbUFDestino.SelectedItem).CDUF;
            btnConsultar.Text = "Carregando...";
            btnConsultar.Enabled = false;
            btnExportar.Enabled = false;
            bgWorkerCadastroItinerario.RunWorkerAsync();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            cmbCDOrigem.SelectedIndex = 0;
            cmbUFDestino.SelectedIndex = 0;
            cmbCidadeDestino.SelectedIndex = 0;
        }

        private void bgWorkerCadastroItinerario_DoWork(object sender, DoWorkEventArgs e)
        {
            listaItinerarios = new List<Itinerario>();
            listaItinerarios = CadastroItinerarioBO.ConsultarItinerario(oFiltro);
        }

        private void bgWorkerCadastroItinerario_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnConsultar.Text = "Consultar";
            btnConsultar.Enabled = true;

            grdItinerario.DataSource = listaItinerarios;
            if (listaItinerarios.Count > 0)
            {
                btnExportar.Enabled = true;
            }
            else
            {
                btnExportar.Enabled = false;
                string mensagem = "Nenhum itinerário encontrado.";
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
