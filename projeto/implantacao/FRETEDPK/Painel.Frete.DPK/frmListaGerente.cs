﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;

namespace PainelFreteDPK
{
    public partial class frmListaGerente : Form
    {
        public frmCadastroGerente oFrmCadastroGerente = new frmCadastroGerente(); 

        public frmListaGerente(List<Gerente> listaGerentes)
        {
            InitializeComponent();

            dvGerentes.DataSource = listaGerentes;
        }

        private void dvGerentes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                Gerente obj = (Gerente)((DataGridView)sender).Rows[e.RowIndex].DataBoundItem;
                oFrmCadastroGerente.PreencherTxtBox(obj);
                this.Close();
            }
        }
    }
}
