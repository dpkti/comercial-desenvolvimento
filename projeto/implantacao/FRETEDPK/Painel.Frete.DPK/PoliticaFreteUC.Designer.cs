﻿namespace PainelFreteDPK
{
    partial class PoliticaFreteUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.grdPoliticaFrete = new System.Windows.Forms.DataGridView();
            this.dataVigencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.canal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoTransportadora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prioridade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoItinerario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdOrigem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cidadeDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ufDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.faturamentoMinimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prazoEntrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoCobranca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFaixaInicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFaixaFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFrete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentualFrete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnImportarExcel = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbCidadeDestino = new System.Windows.Forms.ComboBox();
            this.cmbUFDestino = new System.Windows.Forms.ComboBox();
            this.cmbCDOrigem = new System.Windows.Forms.ComboBox();
            this.txtCodigoTransportadora = new System.Windows.Forms.TextBox();
            this.lblUFDestino = new System.Windows.Forms.Label();
            this.lblCidadeDestino = new System.Windows.Forms.Label();
            this.lblCDOrigem = new System.Windows.Forms.Label();
            this.lblCodigoTransportadora = new System.Windows.Forms.Label();
            this.lblItinerario = new System.Windows.Forms.Label();
            this.txtCdItinerario = new System.Windows.Forms.TextBox();
            this.bgWorkerPoliticaFrete = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.grdPoliticaFrete)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Location = new System.Drawing.Point(1196, 620);
            this.btnExportarExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(115, 39);
            this.btnExportarExcel.TabIndex = 10;
            this.btnExportarExcel.Text = "Exportar Excel";
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // grdPoliticaFrete
            // 
            this.grdPoliticaFrete.AllowUserToAddRows = false;
            this.grdPoliticaFrete.AllowUserToDeleteRows = false;
            this.grdPoliticaFrete.AllowUserToOrderColumns = true;
            this.grdPoliticaFrete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPoliticaFrete.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataVigencia,
            this.canal,
            this.codigoTransportadora,
            this.prioridade,
            this.codigoItinerario,
            this.cdOrigem,
            this.cidadeDestino,
            this.ufDestino,
            this.faturamentoMinimo,
            this.prazoEntrega,
            this.tipoCobranca,
            this.valorFaixaInicial,
            this.valorFaixaFinal,
            this.valorFrete,
            this.percentualFrete});
            this.grdPoliticaFrete.Location = new System.Drawing.Point(15, 98);
            this.grdPoliticaFrete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grdPoliticaFrete.Name = "grdPoliticaFrete";
            this.grdPoliticaFrete.ReadOnly = true;
            this.grdPoliticaFrete.RowTemplate.Height = 24;
            this.grdPoliticaFrete.Size = new System.Drawing.Size(1296, 514);
            this.grdPoliticaFrete.TabIndex = 32;
            this.grdPoliticaFrete.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdPoliticaFrete_CellMouseClick);
            this.grdPoliticaFrete.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdPoliticaFrete_MouseUp);
            // 
            // dataVigencia
            // 
            this.dataVigencia.DataPropertyName = "dataVigencia";
            this.dataVigencia.HeaderText = "Data Vigência";
            this.dataVigencia.Name = "dataVigencia";
            this.dataVigencia.ReadOnly = true;
            this.dataVigencia.Width = 125;
            // 
            // canal
            // 
            this.canal.DataPropertyName = "canal";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.canal.DefaultCellStyle = dataGridViewCellStyle23;
            this.canal.HeaderText = "Canal";
            this.canal.Name = "canal";
            this.canal.ReadOnly = true;
            // 
            // codigoTransportadora
            // 
            this.codigoTransportadora.DataPropertyName = "codigoTransportadora";
            this.codigoTransportadora.HeaderText = "Transportadora";
            this.codigoTransportadora.Name = "codigoTransportadora";
            this.codigoTransportadora.ReadOnly = true;
            this.codigoTransportadora.Width = 350;
            // 
            // prioridade
            // 
            this.prioridade.DataPropertyName = "prioridade";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.prioridade.DefaultCellStyle = dataGridViewCellStyle24;
            this.prioridade.HeaderText = "Prioridade";
            this.prioridade.Name = "prioridade";
            this.prioridade.ReadOnly = true;
            // 
            // codigoItinerario
            // 
            this.codigoItinerario.DataPropertyName = "codigoItinerario";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.codigoItinerario.DefaultCellStyle = dataGridViewCellStyle25;
            this.codigoItinerario.HeaderText = "Código Itinerário";
            this.codigoItinerario.Name = "codigoItinerario";
            this.codigoItinerario.ReadOnly = true;
            this.codigoItinerario.Width = 140;
            // 
            // cdOrigem
            // 
            this.cdOrigem.DataPropertyName = "cdOrigem";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.cdOrigem.DefaultCellStyle = dataGridViewCellStyle26;
            this.cdOrigem.HeaderText = "CD Origem";
            this.cdOrigem.Name = "cdOrigem";
            this.cdOrigem.ReadOnly = true;
            this.cdOrigem.Width = 150;
            // 
            // cidadeDestino
            // 
            this.cidadeDestino.DataPropertyName = "cidadeDestino";
            this.cidadeDestino.HeaderText = "Cidade Destino";
            this.cidadeDestino.Name = "cidadeDestino";
            this.cidadeDestino.ReadOnly = true;
            this.cidadeDestino.Width = 180;
            // 
            // ufDestino
            // 
            this.ufDestino.DataPropertyName = "ufDestino";
            this.ufDestino.HeaderText = "UF Destino";
            this.ufDestino.Name = "ufDestino";
            this.ufDestino.ReadOnly = true;
            this.ufDestino.Width = 115;
            // 
            // faturamentoMinimo
            // 
            this.faturamentoMinimo.DataPropertyName = "faturamentoMinimo";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle27.Format = "N2";
            dataGridViewCellStyle27.NullValue = null;
            this.faturamentoMinimo.DefaultCellStyle = dataGridViewCellStyle27;
            this.faturamentoMinimo.HeaderText = "Faturamento Mínimo";
            this.faturamentoMinimo.Name = "faturamentoMinimo";
            this.faturamentoMinimo.ReadOnly = true;
            this.faturamentoMinimo.Width = 160;
            // 
            // prazoEntrega
            // 
            this.prazoEntrega.DataPropertyName = "prazoEntrega";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.prazoEntrega.DefaultCellStyle = dataGridViewCellStyle28;
            this.prazoEntrega.HeaderText = "Prazo Entrega";
            this.prazoEntrega.Name = "prazoEntrega";
            this.prazoEntrega.ReadOnly = true;
            // 
            // tipoCobranca
            // 
            this.tipoCobranca.DataPropertyName = "tipoCobranca";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.tipoCobranca.DefaultCellStyle = dataGridViewCellStyle29;
            this.tipoCobranca.HeaderText = "Tipo Cobrança";
            this.tipoCobranca.Name = "tipoCobranca";
            this.tipoCobranca.ReadOnly = true;
            this.tipoCobranca.Width = 120;
            // 
            // valorFaixaInicial
            // 
            this.valorFaixaInicial.DataPropertyName = "valorFaixaInicial";
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle30.Format = "N2";
            dataGridViewCellStyle30.NullValue = null;
            this.valorFaixaInicial.DefaultCellStyle = dataGridViewCellStyle30;
            this.valorFaixaInicial.HeaderText = "Valor Faixa Inicial";
            this.valorFaixaInicial.Name = "valorFaixaInicial";
            this.valorFaixaInicial.ReadOnly = true;
            this.valorFaixaInicial.Width = 125;
            // 
            // valorFaixaFinal
            // 
            this.valorFaixaFinal.DataPropertyName = "valorFaixaFinal";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle31.Format = "N2";
            dataGridViewCellStyle31.NullValue = null;
            this.valorFaixaFinal.DefaultCellStyle = dataGridViewCellStyle31;
            this.valorFaixaFinal.HeaderText = "Valor Faixa Final";
            this.valorFaixaFinal.Name = "valorFaixaFinal";
            this.valorFaixaFinal.ReadOnly = true;
            this.valorFaixaFinal.Width = 125;
            // 
            // valorFrete
            // 
            this.valorFrete.DataPropertyName = "valorFrete";
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle32.Format = "N2";
            dataGridViewCellStyle32.NullValue = null;
            this.valorFrete.DefaultCellStyle = dataGridViewCellStyle32;
            this.valorFrete.HeaderText = "Valor Frete";
            this.valorFrete.Name = "valorFrete";
            this.valorFrete.ReadOnly = true;
            // 
            // percentualFrete
            // 
            this.percentualFrete.DataPropertyName = "percentualFrete";
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle33.Format = "N2";
            dataGridViewCellStyle33.NullValue = null;
            this.percentualFrete.DefaultCellStyle = dataGridViewCellStyle33;
            this.percentualFrete.HeaderText = "Percentual Frete";
            this.percentualFrete.Name = "percentualFrete";
            this.percentualFrete.ReadOnly = true;
            // 
            // btnImportarExcel
            // 
            this.btnImportarExcel.Location = new System.Drawing.Point(1080, 620);
            this.btnImportarExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnImportarExcel.Name = "btnImportarExcel";
            this.btnImportarExcel.Size = new System.Drawing.Size(109, 39);
            this.btnImportarExcel.TabIndex = 9;
            this.btnImportarExcel.Text = "Importar Excel";
            this.btnImportarExcel.UseVisualStyleBackColor = true;
            this.btnImportarExcel.Click += new System.EventHandler(this.btnImportarExcel_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(963, 620);
            this.btnInserir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(109, 39);
            this.btnInserir.TabIndex = 8;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(1085, 53);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(109, 39);
            this.btnLimpar.TabIndex = 6;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(1202, 53);
            this.btnConsultar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(109, 39);
            this.btnConsultar.TabIndex = 7;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbCidadeDestino
            // 
            this.cmbCidadeDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCidadeDestino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCidadeDestino.FormattingEnabled = true;
            this.cmbCidadeDestino.Location = new System.Drawing.Point(787, 55);
            this.cmbCidadeDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCidadeDestino.Name = "cmbCidadeDestino";
            this.cmbCidadeDestino.Size = new System.Drawing.Size(271, 24);
            this.cmbCidadeDestino.TabIndex = 5;
            this.cmbCidadeDestino.SelectedIndexChanged += new System.EventHandler(this.cmbCidadeDestino_SelectedIndexChanged);
            // 
            // cmbUFDestino
            // 
            this.cmbUFDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUFDestino.FormattingEnabled = true;
            this.cmbUFDestino.Location = new System.Drawing.Point(395, 53);
            this.cmbUFDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbUFDestino.Name = "cmbUFDestino";
            this.cmbUFDestino.Size = new System.Drawing.Size(244, 24);
            this.cmbUFDestino.TabIndex = 4;
            this.cmbUFDestino.SelectedIndexChanged += new System.EventHandler(this.cmbUFDestino_SelectedIndexChanged);
            // 
            // cmbCDOrigem
            // 
            this.cmbCDOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCDOrigem.FormattingEnabled = true;
            this.cmbCDOrigem.Location = new System.Drawing.Point(395, 21);
            this.cmbCDOrigem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCDOrigem.Name = "cmbCDOrigem";
            this.cmbCDOrigem.Size = new System.Drawing.Size(159, 24);
            this.cmbCDOrigem.TabIndex = 2;
            // 
            // txtCodigoTransportadora
            // 
            this.txtCodigoTransportadora.Location = new System.Drawing.Point(163, 54);
            this.txtCodigoTransportadora.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCodigoTransportadora.MaxLength = 4;
            this.txtCodigoTransportadora.Name = "txtCodigoTransportadora";
            this.txtCodigoTransportadora.Size = new System.Drawing.Size(112, 22);
            this.txtCodigoTransportadora.TabIndex = 3;
            this.txtCodigoTransportadora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoTransportadora_KeyPress);
            // 
            // lblUFDestino
            // 
            this.lblUFDestino.AutoSize = true;
            this.lblUFDestino.Location = new System.Drawing.Point(306, 58);
            this.lblUFDestino.Name = "lblUFDestino";
            this.lblUFDestino.Size = new System.Drawing.Size(82, 17);
            this.lblUFDestino.TabIndex = 22;
            this.lblUFDestino.Text = "UF Destino:";
            // 
            // lblCidadeDestino
            // 
            this.lblCidadeDestino.AutoSize = true;
            this.lblCidadeDestino.Location = new System.Drawing.Point(673, 58);
            this.lblCidadeDestino.Name = "lblCidadeDestino";
            this.lblCidadeDestino.Size = new System.Drawing.Size(108, 17);
            this.lblCidadeDestino.TabIndex = 21;
            this.lblCidadeDestino.Text = "Cidade Destino:";
            // 
            // lblCDOrigem
            // 
            this.lblCDOrigem.AutoSize = true;
            this.lblCDOrigem.Location = new System.Drawing.Point(309, 21);
            this.lblCDOrigem.Name = "lblCDOrigem";
            this.lblCDOrigem.Size = new System.Drawing.Size(81, 17);
            this.lblCDOrigem.TabIndex = 20;
            this.lblCDOrigem.Text = "CD Origem:";
            // 
            // lblCodigoTransportadora
            // 
            this.lblCodigoTransportadora.AutoSize = true;
            this.lblCodigoTransportadora.Location = new System.Drawing.Point(11, 59);
            this.lblCodigoTransportadora.Name = "lblCodigoTransportadora";
            this.lblCodigoTransportadora.Size = new System.Drawing.Size(159, 17);
            this.lblCodigoTransportadora.TabIndex = 18;
            this.lblCodigoTransportadora.Text = "Código Transportadora:";
            // 
            // lblItinerario
            // 
            this.lblItinerario.AutoSize = true;
            this.lblItinerario.Location = new System.Drawing.Point(11, 21);
            this.lblItinerario.Name = "lblItinerario";
            this.lblItinerario.Size = new System.Drawing.Size(67, 17);
            this.lblItinerario.TabIndex = 19;
            this.lblItinerario.Text = "Itinerário:";
            // 
            // txtCdItinerario
            // 
            this.txtCdItinerario.Location = new System.Drawing.Point(85, 21);
            this.txtCdItinerario.Margin = new System.Windows.Forms.Padding(4);
            this.txtCdItinerario.MaxLength = 10;
            this.txtCdItinerario.Name = "txtCdItinerario";
            this.txtCdItinerario.Size = new System.Drawing.Size(189, 22);
            this.txtCdItinerario.TabIndex = 1;
            this.txtCdItinerario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCdItinerario_KeyPress);
            // 
            // bgWorkerPoliticaFrete
            // 
            this.bgWorkerPoliticaFrete.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerPoliticaFrete_DoWork);
            this.bgWorkerPoliticaFrete.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerPoliticaFrete_RunWorkerCompleted);
            // 
            // PoliticaFreteUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtCdItinerario);
            this.Controls.Add(this.btnExportarExcel);
            this.Controls.Add(this.grdPoliticaFrete);
            this.Controls.Add(this.btnImportarExcel);
            this.Controls.Add(this.btnInserir);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.cmbCidadeDestino);
            this.Controls.Add(this.cmbUFDestino);
            this.Controls.Add(this.cmbCDOrigem);
            this.Controls.Add(this.txtCodigoTransportadora);
            this.Controls.Add(this.lblUFDestino);
            this.Controls.Add(this.lblCidadeDestino);
            this.Controls.Add(this.lblCDOrigem);
            this.Controls.Add(this.lblCodigoTransportadora);
            this.Controls.Add(this.lblItinerario);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "PoliticaFreteUC";
            this.Size = new System.Drawing.Size(1327, 670);
            ((System.ComponentModel.ISupportInitialize)(this.grdPoliticaFrete)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.DataGridView grdPoliticaFrete;
        private System.Windows.Forms.Button btnImportarExcel;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cmbCidadeDestino;
        private System.Windows.Forms.ComboBox cmbUFDestino;
        private System.Windows.Forms.ComboBox cmbCDOrigem;
        private System.Windows.Forms.TextBox txtCodigoTransportadora;
        private System.Windows.Forms.Label lblUFDestino;
        private System.Windows.Forms.Label lblCidadeDestino;
        private System.Windows.Forms.Label lblCDOrigem;
        private System.Windows.Forms.Label lblCodigoTransportadora;
        private System.Windows.Forms.Label lblItinerario;
        private System.Windows.Forms.TextBox txtCdItinerario;
        private System.ComponentModel.BackgroundWorker bgWorkerPoliticaFrete;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataVigencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn canal;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoTransportadora;
        private System.Windows.Forms.DataGridViewTextBoxColumn prioridade;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoItinerario;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdOrigem;
        private System.Windows.Forms.DataGridViewTextBoxColumn cidadeDestino;
        private System.Windows.Forms.DataGridViewTextBoxColumn ufDestino;
        private System.Windows.Forms.DataGridViewTextBoxColumn faturamentoMinimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn prazoEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoCobranca;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFaixaInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFaixaFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFrete;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentualFrete;
    }
}
