﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace PainelFreteDPK
{
    public partial class frmItinerarioAlteracao : Form
    {
        List<CidadeDestinoBE> listaCidade = null;
        bool insercao;
        Int32 codItinerario;
       
        public frmItinerarioAlteracao(Int32 codigoItinerario)
        {
            this.Text = "Alterar Itinerário";
            this.Refresh();
            InitializeComponent();
            codItinerario = codigoItinerario;
            CarregaDadosAlteracao(codItinerario);
            insercao = false;
        }

        public frmItinerarioAlteracao()
        {
            this.Text = "Inserir Itinerário";
            this.Refresh();
            InitializeComponent();
            CarregarTela();
            insercao = true;
        }


        public void CarregaDadosAlteracao(Int32 codigoItinerario)
        {
            lbCodItinerario.Text = codigoItinerario.ToString();
            CarregarComboCdOrigem();
            CarregarComboUfDestino();
            CarregarComboCidadeDestino();
            ResultItinerario oItinerario = new ResultItinerario();
            oItinerario = CadastroItinerarioBO.ConsultarItinerario(codigoItinerario);
            cmbCDOrigem.SelectedValue = oItinerario.cdOrigem;
            cmbUfDestino.SelectedValue = oItinerario.ufDestino;
            cmbCidadeDestino.SelectedValue = oItinerario.codCidadeDestino;
        }

        private void CarregarComboCidadeDestino()
        {
            cmbCidadeDestino.DisplayMember = "CIDADE";
            cmbCidadeDestino.ValueMember = "CODCIDADE";
            listaCidade = GlobalBE.listaCidade;
            cmbCidadeDestino.DataSource = listaCidade;
        }

        private void CarregarTela()
        {
            CarregarComboCdOrigem();
            CarregarComboUfDestino();
            cmbCidadeDestino.Enabled = false;
            lbTextCodItinerario.Visible = false;
            lbCodItinerario.Visible = false;
            listaCidade = GlobalBE.listaCidade;
        }

        private void CarregarComboCdOrigem()
        {
            List<CdOrigemBE> listaCdOrigem = null;
            cmbCDOrigem.DataSource = null;
            cmbCDOrigem.Items.Clear();
            cmbCDOrigem.DisplayMember = "CDORIGEM";
            cmbCDOrigem.ValueMember = "CD";
            try
            {
                listaCdOrigem = GlobalBE.listaCdOrigem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            cmbCDOrigem.DataSource = listaCdOrigem;
            cmbCDOrigem.SelectedIndex = 0;
        }

        private void CarregarComboUfDestino()
        {
            List<UfDestinoBE> listaUfDestino = null;
            cmbUfDestino.DataSource = null;
            cmbUfDestino.Items.Clear();
            cmbUfDestino.DisplayMember = "UF";
            cmbUfDestino.ValueMember = "CDUF";

            try
            {
                listaUfDestino = GlobalBE.listaUF;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            cmbUfDestino.DataSource = listaUfDestino;
            cmbUfDestino.SelectedIndex = 0;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidarTela())
                {
                    CdOrigemBE oCdOrigem;
                    CidadeDestinoBE oCidadeDestino;
                    Itinerario oItinerario = new Itinerario();
                    oCdOrigem = ((CdOrigemBE)cmbCDOrigem.SelectedItem);
                    oCidadeDestino = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem);
                    if (ValidarItinerario(oCdOrigem.CD, oCidadeDestino.CODCIDADE))
                    {
                        if (insercao)
                        {
                            CadastroItinerarioBO.InserirItinerario(oCdOrigem.CD, oCidadeDestino.CODCIDADE);
                            MessageBox.Show(ResourcePainelFreteDPK.Sucesso_Itinerario_Insercao, ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            CadastroItinerarioBO.AlterarItinerario(codItinerario, oCdOrigem.CD, oCidadeDestino.CODCIDADE);
                            MessageBox.Show(ResourcePainelFreteDPK.Sucesso_Itinerario_Alteracao, ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        this.Close();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ResourcePainelFreteDPK.Erro_InserirItinerario, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private bool ValidarItinerario(int cdOrigem, int codCidadeDestino)
        {

            bool existeItinerario = CadastroItinerarioBO.VerificarItinerario(cdOrigem, codCidadeDestino);
            if (existeItinerario)
            {
                MessageBox.Show(ResourcePainelFreteDPK.Erro_ItinerarioExiste, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
            return true;      
        }

        private bool ValidarTela()
        {
            if (cmbCDOrigem.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "CD Origem");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbUfDestino.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "UF Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            if (cmbCidadeDestino.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Cidade Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void cmbUfDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            CidadeDestinoBE cidadeDestino = new CidadeDestinoBE();
            cidadeDestino.CODCIDADE = 0;
            cidadeDestino.CIDADE = "Selecione";
            cmbCidadeDestino.DataSource = null;
            cmbCidadeDestino.Items.Clear();
            cmbCidadeDestino.DisplayMember = "CIDADE";
            cmbCidadeDestino.ValueMember = "CODCIDADE";

            if (cmbUfDestino.SelectedIndex == 0 || cmbUfDestino.SelectedIndex == -1)
            {
                cmbCidadeDestino.Enabled = false;
                List<CidadeDestinoBE> listaCidadeVazia = new List<CidadeDestinoBE>();
                listaCidadeVazia.Insert(0, cidadeDestino);
                cmbCidadeDestino.DataSource = listaCidadeVazia;
                cmbCidadeDestino.SelectedIndex = 0;
            }
            else
            {
                List<CidadeDestinoBE> listaCidadeDestino;
                string uf = ((UfDestinoBE)cmbUfDestino.SelectedItem).CDUF;
                var listatmp = from l in listaCidade where (l.UF == uf || uf == null) select l;
                listaCidadeDestino = listatmp.ToList();
                listaCidadeDestino.Insert(0, cidadeDestino);
                cmbCidadeDestino.DataSource = listaCidadeDestino;
                cmbCidadeDestino.SelectedIndex = 0;
                cmbCidadeDestino.Enabled = true;
            }
        }
    }
}
