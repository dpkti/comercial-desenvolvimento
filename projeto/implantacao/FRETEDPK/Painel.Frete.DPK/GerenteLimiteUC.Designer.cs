﻿namespace PainelFreteDPK
{
    partial class GerenteLimiteUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnHistorico = new System.Windows.Forms.Button();
            this.grdGerenteLimite = new System.Windows.Forms.DataGridView();
            this.CPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Limite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SaldoDisponivel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnPedidosLiberados = new System.Windows.Forms.Button();
            this.btnCadGerente = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lbArquivo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdGerenteLimite)).BeginInit();
            this.SuspendLayout();
            // 
            // btnHistorico
            // 
            this.btnHistorico.Location = new System.Drawing.Point(883, 10);
            this.btnHistorico.Margin = new System.Windows.Forms.Padding(2);
            this.btnHistorico.Name = "btnHistorico";
            this.btnHistorico.Size = new System.Drawing.Size(100, 32);
            this.btnHistorico.TabIndex = 5;
            this.btnHistorico.Text = "Histórico";
            this.btnHistorico.UseVisualStyleBackColor = true;
            this.btnHistorico.Click += new System.EventHandler(this.btnHistorico_Click);
            // 
            // grdGerenteLimite
            // 
            this.grdGerenteLimite.AllowUserToAddRows = false;
            this.grdGerenteLimite.AllowUserToDeleteRows = false;
            this.grdGerenteLimite.AllowUserToOrderColumns = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdGerenteLimite.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grdGerenteLimite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGerenteLimite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CPF,
            this.NOME,
            this.Limite,
            this.SaldoDisponivel});
            this.grdGerenteLimite.Location = new System.Drawing.Point(11, 45);
            this.grdGerenteLimite.Margin = new System.Windows.Forms.Padding(2);
            this.grdGerenteLimite.Name = "grdGerenteLimite";
            this.grdGerenteLimite.ReadOnly = true;
            this.grdGerenteLimite.RowHeadersWidth = 20;
            this.grdGerenteLimite.RowTemplate.Height = 24;
            this.grdGerenteLimite.Size = new System.Drawing.Size(972, 486);
            this.grdGerenteLimite.TabIndex = 4;
            this.grdGerenteLimite.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdGerenteLimite_CellMouseClick);
            this.grdGerenteLimite.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdGerenteLimite_MouseUp);
            // 
            // CPF
            // 
            this.CPF.DataPropertyName = "CPF";
            this.CPF.HeaderText = "cpf";
            this.CPF.Name = "CPF";
            this.CPF.ReadOnly = true;
            this.CPF.Visible = false;
            // 
            // NOME
            // 
            this.NOME.DataPropertyName = "NOME";
            this.NOME.HeaderText = "Gerente Regional";
            this.NOME.Name = "NOME";
            this.NOME.ReadOnly = true;
            this.NOME.Width = 300;
            // 
            // Limite
            // 
            this.Limite.DataPropertyName = "LIMITE";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.Limite.DefaultCellStyle = dataGridViewCellStyle5;
            this.Limite.HeaderText = "Limite";
            this.Limite.Name = "Limite";
            this.Limite.ReadOnly = true;
            this.Limite.Width = 200;
            // 
            // SaldoDisponivel
            // 
            this.SaldoDisponivel.DataPropertyName = "SALDO";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.SaldoDisponivel.DefaultCellStyle = dataGridViewCellStyle6;
            this.SaldoDisponivel.HeaderText = "Saldo Disponível";
            this.SaldoDisponivel.Name = "SaldoDisponivel";
            this.SaldoDisponivel.ReadOnly = true;
            this.SaldoDisponivel.Width = 200;
            // 
            // btnPedidosLiberados
            // 
            this.btnPedidosLiberados.Location = new System.Drawing.Point(764, 10);
            this.btnPedidosLiberados.Margin = new System.Windows.Forms.Padding(2);
            this.btnPedidosLiberados.Name = "btnPedidosLiberados";
            this.btnPedidosLiberados.Size = new System.Drawing.Size(115, 32);
            this.btnPedidosLiberados.TabIndex = 6;
            this.btnPedidosLiberados.Text = "Pedidos Liberados";
            this.btnPedidosLiberados.UseVisualStyleBackColor = true;
            this.btnPedidosLiberados.Click += new System.EventHandler(this.btnPedidosLiberados_Click);
            // 
            // btnCadGerente
            // 
            this.btnCadGerente.Location = new System.Drawing.Point(660, 10);
            this.btnCadGerente.Margin = new System.Windows.Forms.Padding(2);
            this.btnCadGerente.Name = "btnCadGerente";
            this.btnCadGerente.Size = new System.Drawing.Size(100, 32);
            this.btnCadGerente.TabIndex = 7;
            this.btnCadGerente.Text = "Cadastro Gerente";
            this.btnCadGerente.UseVisualStyleBackColor = true;
            this.btnCadGerente.Click += new System.EventHandler(this.btnCadGerente_Click);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(555, 10);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 32);
            this.btnImport.TabIndex = 33;
            this.btnImport.Text = "Importar";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.InitialDirectory = "c:\\";
            this.openFileDialog1.Title = "Selecionar Arquivo";
            // 
            // lbArquivo
            // 
            this.lbArquivo.AutoSize = true;
            this.lbArquivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbArquivo.Location = new System.Drawing.Point(8, 29);
            this.lbArquivo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbArquivo.Name = "lbArquivo";
            this.lbArquivo.Size = new System.Drawing.Size(0, 13);
            this.lbArquivo.TabIndex = 49;
            // 
            // GerenteLimiteUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lbArquivo);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.btnCadGerente);
            this.Controls.Add(this.btnPedidosLiberados);
            this.Controls.Add(this.btnHistorico);
            this.Controls.Add(this.grdGerenteLimite);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "GerenteLimiteUC";
            this.Size = new System.Drawing.Size(995, 544);
            this.Load += new System.EventHandler(this.GerenteLimiteUC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdGerenteLimite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnHistorico;
        private System.Windows.Forms.DataGridView grdGerenteLimite;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn Limite;
        private System.Windows.Forms.DataGridViewTextBoxColumn SaldoDisponivel;
        private System.Windows.Forms.Button btnPedidosLiberados;
        private System.Windows.Forms.Button btnCadGerente;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lbArquivo;
    }
}
