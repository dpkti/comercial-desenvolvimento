﻿namespace PainelFreteDPK
{
    partial class frmNovaPoliticaFrete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdPoliticaFrete = new System.Windows.Forms.DataGridView();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblDataVigencia = new System.Windows.Forms.Label();
            this.lblCanal = new System.Windows.Forms.Label();
            this.cmbCanal = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCDOrigem = new System.Windows.Forms.ComboBox();
            this.cmbCidadeDestino = new System.Windows.Forms.ComboBox();
            this.cmbUfDestino = new System.Windows.Forms.ComboBox();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.dtpDataVigencia = new System.Windows.Forms.DateTimePicker();
            this.lbCodigoItinerario = new System.Windows.Forms.Label();
            this.lbCodItinerarioAlt = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.codigoTransportadora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prioridade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.faturamentoMinimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrazoEntrega = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoCobranca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFaixaInicial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFaixaFinal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valorFrete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentualFrete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Excluir = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdPoliticaFrete)).BeginInit();
            this.SuspendLayout();
            // 
            // grdPoliticaFrete
            // 
            this.grdPoliticaFrete.AllowUserToDeleteRows = false;
            this.grdPoliticaFrete.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPoliticaFrete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdPoliticaFrete.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPoliticaFrete.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigoTransportadora,
            this.transp,
            this.prioridade,
            this.faturamentoMinimo,
            this.PrazoEntrega,
            this.tipoCobranca,
            this.valorFaixaInicial,
            this.valorFaixaFinal,
            this.valorFrete,
            this.percentualFrete,
            this.Excluir});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdPoliticaFrete.DefaultCellStyle = dataGridViewCellStyle11;
            this.grdPoliticaFrete.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdPoliticaFrete.Location = new System.Drawing.Point(9, 81);
            this.grdPoliticaFrete.Margin = new System.Windows.Forms.Padding(2);
            this.grdPoliticaFrete.Name = "grdPoliticaFrete";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdPoliticaFrete.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.grdPoliticaFrete.RowHeadersWidth = 20;
            this.grdPoliticaFrete.RowTemplate.Height = 24;
            this.grdPoliticaFrete.Size = new System.Drawing.Size(972, 392);
            this.grdPoliticaFrete.TabIndex = 29;
            this.grdPoliticaFrete.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPoliticaFrete_CellEndEdit);
            this.grdPoliticaFrete.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdPoliticaFrete_CellFormatting);
            this.grdPoliticaFrete.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.grdPoliticaFrete_EditingControlShowing);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(878, 44);
            this.btnConsultar.Margin = new System.Windows.Forms.Padding(2);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(82, 32);
            this.btnConsultar.TabIndex = 6;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(813, 480);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(2);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(82, 32);
            this.btnSalvar.TabIndex = 5;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblDataVigencia
            // 
            this.lblDataVigencia.AutoSize = true;
            this.lblDataVigencia.Location = new System.Drawing.Point(628, 491);
            this.lblDataVigencia.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDataVigencia.Name = "lblDataVigencia";
            this.lblDataVigencia.Size = new System.Drawing.Size(77, 13);
            this.lblDataVigencia.TabIndex = 37;
            this.lblDataVigencia.Text = "Data Vigência:";
            // 
            // lblCanal
            // 
            this.lblCanal.AutoSize = true;
            this.lblCanal.Location = new System.Drawing.Point(529, 46);
            this.lblCanal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCanal.Name = "lblCanal";
            this.lblCanal.Size = new System.Drawing.Size(37, 13);
            this.lblCanal.TabIndex = 39;
            this.lblCanal.Text = "Canal:";
            // 
            // cmbCanal
            // 
            this.cmbCanal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCanal.FormattingEnabled = true;
            this.cmbCanal.Location = new System.Drawing.Point(615, 44);
            this.cmbCanal.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCanal.Name = "cmbCanal";
            this.cmbCanal.Size = new System.Drawing.Size(92, 21);
            this.cmbCanal.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(529, 12);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 45;
            this.label5.Text = "Cidade Destino:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(243, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 44;
            this.label4.Text = "UF Destino:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 43;
            this.label3.Text = "CD Origem:";
            // 
            // cmbCDOrigem
            // 
            this.cmbCDOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCDOrigem.FormattingEnabled = true;
            this.cmbCDOrigem.Location = new System.Drawing.Point(85, 10);
            this.cmbCDOrigem.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCDOrigem.Name = "cmbCDOrigem";
            this.cmbCDOrigem.Size = new System.Drawing.Size(114, 21);
            this.cmbCDOrigem.TabIndex = 1;
            // 
            // cmbCidadeDestino
            // 
            this.cmbCidadeDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCidadeDestino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCidadeDestino.FormattingEnabled = true;
            this.cmbCidadeDestino.Location = new System.Drawing.Point(615, 10);
            this.cmbCidadeDestino.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCidadeDestino.Name = "cmbCidadeDestino";
            this.cmbCidadeDestino.Size = new System.Drawing.Size(180, 21);
            this.cmbCidadeDestino.TabIndex = 3;
            // 
            // cmbUfDestino
            // 
            this.cmbUfDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUfDestino.FormattingEnabled = true;
            this.cmbUfDestino.Location = new System.Drawing.Point(309, 10);
            this.cmbUfDestino.Margin = new System.Windows.Forms.Padding(2);
            this.cmbUfDestino.Name = "cmbUfDestino";
            this.cmbUfDestino.Size = new System.Drawing.Size(175, 21);
            this.cmbUfDestino.TabIndex = 2;
            this.cmbUfDestino.SelectedIndexChanged += new System.EventHandler(this.cmbUfDestino_SelectedIndexChanged);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(793, 44);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(82, 32);
            this.btnLimpar.TabIndex = 5;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // dtpDataVigencia
            // 
            this.dtpDataVigencia.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataVigencia.Location = new System.Drawing.Point(709, 489);
            this.dtpDataVigencia.Name = "dtpDataVigencia";
            this.dtpDataVigencia.Size = new System.Drawing.Size(100, 20);
            this.dtpDataVigencia.TabIndex = 47;
            this.dtpDataVigencia.Value = new System.DateTime(2015, 8, 27, 0, 0, 0, 0);
            // 
            // lbCodigoItinerario
            // 
            this.lbCodigoItinerario.AutoSize = true;
            this.lbCodigoItinerario.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodigoItinerario.Location = new System.Drawing.Point(20, 49);
            this.lbCodigoItinerario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCodigoItinerario.Name = "lbCodigoItinerario";
            this.lbCodigoItinerario.Size = new System.Drawing.Size(104, 13);
            this.lbCodigoItinerario.TabIndex = 48;
            this.lbCodigoItinerario.Text = "Código Itinerário:";
            this.lbCodigoItinerario.Visible = false;
            // 
            // lbCodItinerarioAlt
            // 
            this.lbCodItinerarioAlt.AutoSize = true;
            this.lbCodItinerarioAlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodItinerarioAlt.Location = new System.Drawing.Point(124, 49);
            this.lbCodItinerarioAlt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCodItinerarioAlt.Name = "lbCodItinerarioAlt";
            this.lbCodItinerarioAlt.Size = new System.Drawing.Size(0, 13);
            this.lbCodItinerarioAlt.TabIndex = 49;
            this.lbCodItinerarioAlt.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(899, 480);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(82, 32);
            this.btnCancelar.TabIndex = 50;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // codigoTransportadora
            // 
            this.codigoTransportadora.DataPropertyName = "codigoTransportadora";
            dataGridViewCellStyle2.Format = "N0";
            dataGridViewCellStyle2.NullValue = null;
            this.codigoTransportadora.DefaultCellStyle = dataGridViewCellStyle2;
            this.codigoTransportadora.HeaderText = "Código Transportadora";
            this.codigoTransportadora.MaxInputLength = 4;
            this.codigoTransportadora.Name = "codigoTransportadora";
            // 
            // transp
            // 
            this.transp.HeaderText = "Transportadora";
            this.transp.Name = "transp";
            // 
            // prioridade
            // 
            this.prioridade.DataPropertyName = "prioridade";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.prioridade.DefaultCellStyle = dataGridViewCellStyle3;
            this.prioridade.HeaderText = "Prioridade Transportadora";
            this.prioridade.MaxInputLength = 1;
            this.prioridade.Name = "prioridade";
            this.prioridade.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // faturamentoMinimo
            // 
            this.faturamentoMinimo.DataPropertyName = "faturamentoMinimo";
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.faturamentoMinimo.DefaultCellStyle = dataGridViewCellStyle4;
            this.faturamentoMinimo.HeaderText = "Faturamento Mínimo (R$)";
            this.faturamentoMinimo.MaxInputLength = 15;
            this.faturamentoMinimo.Name = "faturamentoMinimo";
            // 
            // PrazoEntrega
            // 
            this.PrazoEntrega.DataPropertyName = "prazoEntrega";
            dataGridViewCellStyle5.Format = "N0";
            dataGridViewCellStyle5.NullValue = null;
            this.PrazoEntrega.DefaultCellStyle = dataGridViewCellStyle5;
            this.PrazoEntrega.HeaderText = "Prazo Entrega";
            this.PrazoEntrega.MaxInputLength = 3;
            this.PrazoEntrega.Name = "PrazoEntrega";
            // 
            // tipoCobranca
            // 
            this.tipoCobranca.DataPropertyName = "tipoCobranca";
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.tipoCobranca.DefaultCellStyle = dataGridViewCellStyle6;
            this.tipoCobranca.HeaderText = "Tipo Cobrança";
            this.tipoCobranca.MaxInputLength = 1;
            this.tipoCobranca.Name = "tipoCobranca";
            // 
            // valorFaixaInicial
            // 
            this.valorFaixaInicial.DataPropertyName = "valorFaixaInicial";
            dataGridViewCellStyle7.Format = "0.00";
            dataGridViewCellStyle7.NullValue = null;
            this.valorFaixaInicial.DefaultCellStyle = dataGridViewCellStyle7;
            this.valorFaixaInicial.HeaderText = "Valor Faixa Inicial (R$)";
            this.valorFaixaInicial.MaxInputLength = 15;
            this.valorFaixaInicial.Name = "valorFaixaInicial";
            this.valorFaixaInicial.Width = 120;
            // 
            // valorFaixaFinal
            // 
            this.valorFaixaFinal.DataPropertyName = "valorFaixaFinal";
            dataGridViewCellStyle8.Format = "0.00";
            dataGridViewCellStyle8.NullValue = null;
            this.valorFaixaFinal.DefaultCellStyle = dataGridViewCellStyle8;
            this.valorFaixaFinal.HeaderText = "Valor Faixa Final (R$)";
            this.valorFaixaFinal.MaxInputLength = 15;
            this.valorFaixaFinal.Name = "valorFaixaFinal";
            this.valorFaixaFinal.Width = 120;
            // 
            // valorFrete
            // 
            this.valorFrete.DataPropertyName = "valorFrete";
            dataGridViewCellStyle9.Format = "0.00";
            dataGridViewCellStyle9.NullValue = null;
            this.valorFrete.DefaultCellStyle = dataGridViewCellStyle9;
            this.valorFrete.HeaderText = "Valor Frete (R$)";
            this.valorFrete.MaxInputLength = 15;
            this.valorFrete.Name = "valorFrete";
            // 
            // percentualFrete
            // 
            this.percentualFrete.DataPropertyName = "percentualFrete";
            dataGridViewCellStyle10.Format = "0.00";
            dataGridViewCellStyle10.NullValue = null;
            this.percentualFrete.DefaultCellStyle = dataGridViewCellStyle10;
            this.percentualFrete.HeaderText = "Percentual Frete (%)";
            this.percentualFrete.MaxInputLength = 5;
            this.percentualFrete.Name = "percentualFrete";
            // 
            // Excluir
            // 
            this.Excluir.DataPropertyName = "excluir";
            this.Excluir.HeaderText = "Excluir";
            this.Excluir.Name = "Excluir";
            // 
            // frmNovaPoliticaFrete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 518);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lbCodItinerarioAlt);
            this.Controls.Add(this.lbCodigoItinerario);
            this.Controls.Add(this.dtpDataVigencia);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbCDOrigem);
            this.Controls.Add(this.cmbCidadeDestino);
            this.Controls.Add(this.cmbUfDestino);
            this.Controls.Add(this.cmbCanal);
            this.Controls.Add(this.lblCanal);
            this.Controls.Add(this.lblDataVigencia);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.grdPoliticaFrete);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "frmNovaPoliticaFrete";
            this.Text = "Politica Frete";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmNovaPoliticaFrete_FormClosing);
            this.Load += new System.EventHandler(this.frmNovaPoliticaFrete_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdPoliticaFrete)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.DataGridView grdPoliticaFrete;
        private System.Windows.Forms.Label lblDataVigencia;
        private System.Windows.Forms.Label lblCanal;
        private System.Windows.Forms.ComboBox cmbCanal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCDOrigem;
        private System.Windows.Forms.ComboBox cmbCidadeDestino;
        private System.Windows.Forms.ComboBox cmbUfDestino;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.DateTimePicker dtpDataVigencia;
        private System.Windows.Forms.Label lbCodigoItinerario;
        private System.Windows.Forms.Label lbCodItinerarioAlt;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoTransportadora;
        private System.Windows.Forms.DataGridViewTextBoxColumn transp;
        private System.Windows.Forms.DataGridViewTextBoxColumn prioridade;
        private System.Windows.Forms.DataGridViewTextBoxColumn faturamentoMinimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrazoEntrega;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoCobranca;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFaixaInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFaixaFinal;
        private System.Windows.Forms.DataGridViewTextBoxColumn valorFrete;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentualFrete;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Excluir;
    }
}