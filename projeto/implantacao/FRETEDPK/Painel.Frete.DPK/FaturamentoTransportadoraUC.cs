﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using UtilGeralDPA;
using Business;

namespace PainelFreteDPK
{
    public partial class FaturamentoTransportadoraUC : UserControl
    {
        private FiltroGridTransportadora ofiltro = new FiltroGridTransportadora();
        private List<FaturamentoTransportadoraGrid> listaTransp;

        List<CidadeDestinoBE> listaCidade = null;

        public FaturamentoTransportadoraUC()
        {
            InitializeComponent();
            CarregarTela();
        }

        private void cmdConsultar_Click(object sender, EventArgs e)
        {

            if (ValidarTela())
            {

                ofiltro.dataInicial = dtpDataInicio.Value.Date;
                ofiltro.dataFinal = dtpDataFim.Value.Date; 
                ofiltro.cdOrigem = ((CdOrigemBE)cmbCDOrigem.SelectedItem).CD;
                ofiltro.ufDestino = ((UfDestinoBE)cmbUfDestino.SelectedItem).CDUF;
                ofiltro.cdCidadeDestino = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).CODCIDADE;
                ofiltro.cdTransp = ((CodigoTransportadoraBE)cmbTransportadora.SelectedItem).COD_TRANSP;

                cmdConsultar.Text = "Carregando...";
                cmdConsultar.Enabled = false;

                bgWorkerFaturamentoTransportadora.RunWorkerAsync();
            }
            else
            {
                return;
            }
  
        }

        private void CarregarComboCodigoTransportadora(ComboBox cmbTransportadora)
        {
            cmbTransportadora.Items.Clear();

            cmbTransportadora.DisplayMember = "NOME_TRANSP";
            cmbTransportadora.ValueMember = "COD_TRANSP";

            List<CodigoTransportadoraBE> listaCodigosTransportadora = null;

            try
            {
                listaCodigosTransportadora = Business.PoliticaFreteBO.BuscarDescricaoTransportadora();
                CodigoTransportadoraBE transportadora = new CodigoTransportadoraBE();
                transportadora.COD_TRANSP = 0;
                transportadora.NOME_TRANSP = "Selecione";
                listaCodigosTransportadora.Insert(0, transportadora);
                cmbTransportadora.DataSource = listaCodigosTransportadora;
                cmbTransportadora.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCodTransportadora, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregarTela()
        {
            CarregarComboCdOrigem(cmbCDOrigem);
            CarregarComboUfDestino(cmbUfDestino);

            CarregarComboCodigoTransportadora(cmbTransportadora);

            listaCidade = GlobalBE.listaCidade;
        }

        private void CarregarComboCidadeDestino()
        {
            cmbCidadeDestino.DisplayMember = "CIDADE";
            cmbCidadeDestino.ValueMember = "CODCIDADE";
            listaCidade = GlobalBE.listaCidade;
            cmbCidadeDestino.DataSource = listaCidade;
        }

        private void CarregarComboCdOrigem(ComboBox cmbCDOrigem)
        {
            List<CdOrigemBE> listaCdOrigem = null;
            cmbCDOrigem.DataSource = null;
            cmbCDOrigem.Items.Clear();
            cmbCDOrigem.DisplayMember = "CDORIGEM";
            cmbCDOrigem.ValueMember = "CD";

            try
            {
                listaCdOrigem = GlobalBE.listaCdOrigem;
                cmbCDOrigem.DataSource = listaCdOrigem;
                cmbCDOrigem.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregarComboUfDestino(ComboBox cmbUFDestino)
        {
            List<UfDestinoBE> listaUfDestino = null;
            cmbUFDestino.DataSource = null;
            cmbUFDestino.Items.Clear();
            cmbUFDestino.DisplayMember = "UF";
            cmbUFDestino.ValueMember = "CDUF";

            try
            {
                listaUfDestino = GlobalBE.listaUF;
                cmbUFDestino.DataSource = listaUfDestino;
                cmbUFDestino.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregarComboCidadeDestino(ComboBox cmbCidadeDestino, string uf)
        {
            try
            {
                cmbCidadeDestino.DataSource = null;
                cmbCidadeDestino.Items.Clear();
                DoubleBuffered = true;
                cmbCidadeDestino.DisplayMember = "CIDADE";
                cmbCidadeDestino.ValueMember = "CODCIDADE";
                List<CidadeDestinoBE> listaCidadeDestino = null;

                var listatmp = from l in GlobalBE.listaCidade where (l.UF == uf || l.CODCIDADE == 0 || uf == null) select l;
                listaCidadeDestino = listatmp.ToList();

                cmbCidadeDestino.DataSource = listaCidadeDestino;
                DoubleBuffered = true;
                if (string.IsNullOrEmpty(uf))
                {
                    cmbCidadeDestino.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void cmbUfDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cduf = null;
            CidadeDestinoBE cdcidade = new CidadeDestinoBE();
            if (cmbUfDestino.SelectedIndex != 0 && cmbUfDestino.SelectedIndex != -1)
            {
                cduf = ((UfDestinoBE)cmbUfDestino.SelectedItem).CDUF;
            }
            else if (cmbCidadeDestino.Items.Count > 0)
            {
                cmbCidadeDestino.SelectedIndex = 0;
            }
            if (cmbCidadeDestino.Items.Count > 0)
            {
                cdcidade = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem);
            }
            if (cmbCidadeDestino.SelectedIndex == 0 || cmbCidadeDestino.SelectedIndex == -1 || cdcidade.UF != cduf)
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                DoubleBuffered = true;
            }
            else
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                cmbCidadeDestino.SelectedValue = cdcidade.CODCIDADE;
                DoubleBuffered = true;
            }
        }

        private bool ValidarTela()
        {
            if (cmbCDOrigem.SelectedIndex == 0)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "CD Origem");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (((CidadeDestinoBE)cmbCidadeDestino.SelectedItem) == null)
            {
                string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Cidade Destino");
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpDataInicio.Value.Date > dtpDataFim.Value.Date)
            {
                string mensagem = "Data Fim deve ser maior que a Data Início.";
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

                return true;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            cmbCDOrigem.SelectedValue = 0;
            cmbUfDestino.SelectedIndex = 0;
            cmbCidadeDestino.SelectedValue = 0;
            cmbTransportadora.SelectedValue = 0;
            dtpDataInicio.Text = DateTime.Now.ToShortDateString();
            dtpDataFim.Text = DateTime.Now.ToShortDateString();

        }

        private void cmbCidadeDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCidadeDestino.SelectedIndex != 0 && cmbCidadeDestino.SelectedIndex != -1)
            {
                string uf;
                uf = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).UF;
                cmbUfDestino.SelectedValue = uf;
            }

        }

        private void bgWorkerFaturamentoTransportadora_DoWork(object sender, DoWorkEventArgs e)
        {

            try
               {
                   
                  listaTransp = FaturamentoTransportadoraBO.BuscarTransportadoraItinerario(ofiltro);
                   
               }
               catch (Exception ex)
               {
                    Logger.LogError(ex);
                    MessageBox.Show(ResourcePainelFreteDPK.ErroBanco, ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
               }
        }

        private void bgWorkerFaturamentoTransportadora_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            cmdConsultar.Text = "Consultar";
            cmdConsultar.Enabled = true;

            grdFaturamento.DataSource = listaTransp;

            if (listaTransp.Count == 0) 
             {
                string mensagem = "Nenhum faturamento encontrado.";
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();

                //Adding the Columns
                foreach (DataGridViewColumn column in grdFaturamento.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in grdFaturamento.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                if (grdFaturamento.Rows.Count < 100)
                {
                    for (int j = grdFaturamento.Rows.Count; j <= (grdFaturamento.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= grdFaturamento.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
