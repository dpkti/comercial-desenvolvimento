﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;
using UtilGeralDPA;


namespace PainelFreteDPK
{
    public partial class frmPainelParametros : Form
    {
        private PoliticaFreteUC oPoliticaFreteUC;
        private CadastroItinerarioUC oCadastroItinerarioUC;
        private GerenteLimiteUC oGerenteLimiteUC;
        private FaturamentoTransportadoraUC oFaturamentoTransportadoraUC;
        private UsuarioBE oUsuario;
        private LogoDPKUC oLogoDPKUC;

        public frmPainelParametros(UsuarioBE oUser)
        {
            InitializeComponent();
            this.oUsuario = oUser;
            CarregaCDOrigem();
            CarregaCidadeDestino();
            CarregaUFDestino();
            CarregaFilial();
            CarregaGerente();
            this.oPoliticaFreteUC = new PoliticaFreteUC();
            this.oCadastroItinerarioUC = new CadastroItinerarioUC();
            this.oGerenteLimiteUC = new GerenteLimiteUC();
            this.oFaturamentoTransportadoraUC = new FaturamentoTransportadoraUC();
            this.oLogoDPKUC = new LogoDPKUC();
        }

        private void CarregaGerente()
        {
            try
            {
                GlobalBE.listaGerente = Business.PainelParamentroBO.BuscarGerente();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarGerente, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregaFilial()
        {
            try
            {
                GlobalBE.listaFilial = Business.PainelParamentroBO.BuscarFilial();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnCadastroItinerario_Click(object sender, EventArgs e)
        {
            MudaCorBotao(btnCadastroItinerario);
            this.pnlParametros.Controls.Clear();
            this.pnlParametros.Controls.Add(oCadastroItinerarioUC);
        }

        private void btnManutencaoTransportadora_Click(object sender, EventArgs e)
        {
            
            try
            {
                MudaCorBotao(btnManutencaoTransportadora);
                this.pnlParametros.Controls.Clear();
                this.pnlParametros.Controls.Add(oLogoDPKUC);
                string CAD020 = ConfigurationAccess.ObterConfiguracao("diretorioCAD020").ToString();
                string nomeAplicacao = ConfigurationAccess.ObterConfiguracao("nomeAplicacao").ToString();

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = CAD020;
                proc.StartInfo.Arguments = nomeAplicacao + oUsuario.COD_USUARIO;
                proc.Start();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show(String.Format("{0}", "Erro ao abrir o CAD020."),PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void btnManutencaoPoliticaFrete_Click(object sender, EventArgs e)
        {
            MudaCorBotao(btnManutencaoPoliticaFrete);
            this.pnlParametros.Controls.Clear();
            this.pnlParametros.Controls.Add(oPoliticaFreteUC);
        }

        private void btnGerenteLimite_Click(object sender, EventArgs e)
        {
            MudaCorBotao(btnGerenteLimite);
            this.pnlParametros.Controls.Clear();
            this.pnlParametros.Controls.Add(oGerenteLimiteUC);  
        }

        private void btnFaturamentoTransportadora_Click(object sender, EventArgs e)
        {
            MudaCorBotao(btnFaturamentoTransportadora);
            this.pnlParametros.Controls.Clear();
            this.pnlParametros.Controls.Add(oFaturamentoTransportadoraUC);
        }

        private void btnSair_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Deseja Encerrar a Aplicação?", "Sair", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Environment.Exit(0);
            }
        }

        private void CarregaCidadeDestino()
        {
            try
            {
                GlobalBE.listaCidade = Business.PainelParamentroBO.BuscarCidadeDestino();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCidadeDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void CarregaCdItinerario()
        {
            try
            {
                GlobalBE.listaCdItinerario = Business.PainelParamentroBO.BuscarCodigoItinerario();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCodItenerario, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }
        private void CarregaUFDestino()
        {
            try
            {
                GlobalBE.listaUF = Business.PainelParamentroBO.BuscarUfDestino();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }
        private void CarregaCDOrigem()
        {
            try
            {
                GlobalBE.listaCdOrigem = Business.PainelParamentroBO.BuscarCdOrigem();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void frmPainelParametros_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Deseja Encerrar a Aplicação?", "Sair", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Environment.Exit(0);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void MudaCorBotao(Button botao)
        {
            this.btnManutencaoTransportadora.BackColor = System.Drawing.Color.LightGray;
            this.btnCadastroItinerario.BackColor = System.Drawing.Color.LightGray;
            this.btnManutencaoPoliticaFrete.BackColor = System.Drawing.Color.LightGray;
            this.btnGerenteLimite.BackColor = System.Drawing.Color.LightGray;
            this.btnFaturamentoTransportadora.BackColor = System.Drawing.Color.LightGray;
            
            botao.BackColor = System.Drawing.Color.DarkGray;

        }

        private void frmPainelParametros_Load(object sender, EventArgs e)
        {
            this.pnlParametros.Controls.Clear();
            this.pnlParametros.Controls.Add(oLogoDPKUC);
        }
    
    }
}
