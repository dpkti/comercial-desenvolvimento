﻿namespace PainelFreteDPK
{
    partial class frmPainelParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnManutencaoTransportadora = new System.Windows.Forms.Button();
            this.btnCadastroItinerario = new System.Windows.Forms.Button();
            this.btnManutencaoPoliticaFrete = new System.Windows.Forms.Button();
            this.btnGerenteLimite = new System.Windows.Forms.Button();
            this.btnFaturamentoTransportadora = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.pnlParametros = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // btnManutencaoTransportadora
            // 
            this.btnManutencaoTransportadora.BackColor = System.Drawing.Color.LightGray;
            this.btnManutencaoTransportadora.Location = new System.Drawing.Point(17, 12);
            this.btnManutencaoTransportadora.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnManutencaoTransportadora.Name = "btnManutencaoTransportadora";
            this.btnManutencaoTransportadora.Size = new System.Drawing.Size(201, 36);
            this.btnManutencaoTransportadora.TabIndex = 0;
            this.btnManutencaoTransportadora.Text = "Manutenção Transportadora";
            this.btnManutencaoTransportadora.UseVisualStyleBackColor = false;
            this.btnManutencaoTransportadora.Click += new System.EventHandler(this.btnManutencaoTransportadora_Click);
            // 
            // btnCadastroItinerario
            // 
            this.btnCadastroItinerario.BackColor = System.Drawing.Color.LightGray;
            this.btnCadastroItinerario.Location = new System.Drawing.Point(242, 12);
            this.btnCadastroItinerario.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCadastroItinerario.Name = "btnCadastroItinerario";
            this.btnCadastroItinerario.Size = new System.Drawing.Size(201, 36);
            this.btnCadastroItinerario.TabIndex = 1;
            this.btnCadastroItinerario.Text = "Cadastro Itinerário";
            this.btnCadastroItinerario.UseVisualStyleBackColor = false;
            this.btnCadastroItinerario.Click += new System.EventHandler(this.btnCadastroItinerario_Click);
            // 
            // btnManutencaoPoliticaFrete
            // 
            this.btnManutencaoPoliticaFrete.BackColor = System.Drawing.Color.LightGray;
            this.btnManutencaoPoliticaFrete.Location = new System.Drawing.Point(467, 12);
            this.btnManutencaoPoliticaFrete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnManutencaoPoliticaFrete.Name = "btnManutencaoPoliticaFrete";
            this.btnManutencaoPoliticaFrete.Size = new System.Drawing.Size(201, 36);
            this.btnManutencaoPoliticaFrete.TabIndex = 2;
            this.btnManutencaoPoliticaFrete.Text = "Manutenção Política Frete";
            this.btnManutencaoPoliticaFrete.UseVisualStyleBackColor = false;
            this.btnManutencaoPoliticaFrete.Click += new System.EventHandler(this.btnManutencaoPoliticaFrete_Click);
            // 
            // btnGerenteLimite
            // 
            this.btnGerenteLimite.BackColor = System.Drawing.Color.LightGray;
            this.btnGerenteLimite.Location = new System.Drawing.Point(692, 12);
            this.btnGerenteLimite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGerenteLimite.Name = "btnGerenteLimite";
            this.btnGerenteLimite.Size = new System.Drawing.Size(201, 36);
            this.btnGerenteLimite.TabIndex = 3;
            this.btnGerenteLimite.Text = "Gerente (limite)";
            this.btnGerenteLimite.UseVisualStyleBackColor = false;
            this.btnGerenteLimite.Click += new System.EventHandler(this.btnGerenteLimite_Click);
            // 
            // btnFaturamentoTransportadora
            // 
            this.btnFaturamentoTransportadora.BackColor = System.Drawing.Color.LightGray;
            this.btnFaturamentoTransportadora.Location = new System.Drawing.Point(917, 12);
            this.btnFaturamentoTransportadora.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFaturamentoTransportadora.Name = "btnFaturamentoTransportadora";
            this.btnFaturamentoTransportadora.Size = new System.Drawing.Size(201, 36);
            this.btnFaturamentoTransportadora.TabIndex = 4;
            this.btnFaturamentoTransportadora.Text = "Faturamento Transportadora";
            this.btnFaturamentoTransportadora.UseVisualStyleBackColor = false;
            this.btnFaturamentoTransportadora.Click += new System.EventHandler(this.btnFaturamentoTransportadora_Click);
            // 
            // btnSair
            // 
            this.btnSair.BackColor = System.Drawing.Color.LightGray;
            this.btnSair.Location = new System.Drawing.Point(1142, 12);
            this.btnSair.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(201, 36);
            this.btnSair.TabIndex = 6;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = false;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // pnlParametros
            // 
            this.pnlParametros.Location = new System.Drawing.Point(17, 54);
            this.pnlParametros.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlParametros.Name = "pnlParametros";
            this.pnlParametros.Size = new System.Drawing.Size(1327, 670);
            this.pnlParametros.TabIndex = 7;
            // 
            // frmPainelParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1360, 735);
            this.Controls.Add(this.pnlParametros);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnFaturamentoTransportadora);
            this.Controls.Add(this.btnGerenteLimite);
            this.Controls.Add(this.btnManutencaoPoliticaFrete);
            this.Controls.Add(this.btnCadastroItinerario);
            this.Controls.Add(this.btnManutencaoTransportadora);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "frmPainelParametros";
            this.Text = "Painel de Parâmetros - Frete DPK";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPainelParametros_FormClosing);
            this.Load += new System.EventHandler(this.frmPainelParametros_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnManutencaoTransportadora;
        private System.Windows.Forms.Button btnCadastroItinerario;
        private System.Windows.Forms.Button btnManutencaoPoliticaFrete;
        private System.Windows.Forms.Button btnGerenteLimite;
        private System.Windows.Forms.Button btnFaturamentoTransportadora;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Panel pnlParametros;
    }
}