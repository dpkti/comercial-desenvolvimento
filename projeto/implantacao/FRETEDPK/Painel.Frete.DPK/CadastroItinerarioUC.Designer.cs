﻿namespace PainelFreteDPK
{
    partial class CadastroItinerarioUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbCDOrigem = new System.Windows.Forms.ComboBox();
            this.cmbCidadeDestino = new System.Windows.Forms.ComboBox();
            this.grdItinerario = new System.Windows.Forms.DataGridView();
            this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdOrigem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ufOrigem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cidadeDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ufDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCidadeDestino = new System.Windows.Forms.Label();
            this.lblUFDestino = new System.Windows.Forms.Label();
            this.lblCDOrigem = new System.Windows.Forms.Label();
            this.cmbUFDestino = new System.Windows.Forms.ComboBox();
            this.bgWorkerCadastroItinerario = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.grdItinerario)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExportar
            // 
            this.btnExportar.Enabled = false;
            this.btnExportar.Location = new System.Drawing.Point(1187, 620);
            this.btnExportar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(124, 39);
            this.btnExportar.TabIndex = 7;
            this.btnExportar.Text = "Exportar Excel";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(1072, 620);
            this.btnInserir.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(109, 39);
            this.btnInserir.TabIndex = 6;
            this.btnInserir.Text = "Inserir";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(1085, 28);
            this.btnLimpar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(109, 39);
            this.btnLimpar.TabIndex = 4;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(1200, 28);
            this.btnConsultar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(109, 39);
            this.btnConsultar.TabIndex = 5;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbCDOrigem
            // 
            this.cmbCDOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCDOrigem.FormattingEnabled = true;
            this.cmbCDOrigem.Location = new System.Drawing.Point(103, 36);
            this.cmbCDOrigem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCDOrigem.Name = "cmbCDOrigem";
            this.cmbCDOrigem.Size = new System.Drawing.Size(152, 24);
            this.cmbCDOrigem.TabIndex = 1;
            // 
            // cmbCidadeDestino
            // 
            this.cmbCidadeDestino.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbCidadeDestino.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCidadeDestino.DisplayMember = "(none)";
            this.cmbCidadeDestino.FormattingEnabled = true;
            this.cmbCidadeDestino.Location = new System.Drawing.Point(779, 36);
            this.cmbCidadeDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbCidadeDestino.Name = "cmbCidadeDestino";
            this.cmbCidadeDestino.Size = new System.Drawing.Size(280, 24);
            this.cmbCidadeDestino.TabIndex = 3;
            this.cmbCidadeDestino.SelectedIndexChanged += new System.EventHandler(this.cmbCidadeDestino_SelectedIndexChanged);
            // 
            // grdItinerario
            // 
            this.grdItinerario.AllowUserToAddRows = false;
            this.grdItinerario.AllowUserToDeleteRows = false;
            this.grdItinerario.AllowUserToOrderColumns = true;
            this.grdItinerario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdItinerario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codigo,
            this.cdOrigem,
            this.ufOrigem,
            this.cidadeDestino,
            this.ufDestino});
            this.grdItinerario.Location = new System.Drawing.Point(15, 83);
            this.grdItinerario.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grdItinerario.Name = "grdItinerario";
            this.grdItinerario.ReadOnly = true;
            this.grdItinerario.RowTemplate.Height = 24;
            this.grdItinerario.Size = new System.Drawing.Size(1296, 530);
            this.grdItinerario.TabIndex = 17;
            this.grdItinerario.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdItinerario_CellMouseClick);
            this.grdItinerario.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdItinerario_MouseUp);
            // 
            // codigo
            // 
            this.codigo.DataPropertyName = "codigo";
            this.codigo.HeaderText = "Código Itinerário";
            this.codigo.Name = "codigo";
            this.codigo.ReadOnly = true;
            this.codigo.Width = 140;
            // 
            // cdOrigem
            // 
            this.cdOrigem.DataPropertyName = "cdOrigem";
            this.cdOrigem.HeaderText = "CD Origem";
            this.cdOrigem.Name = "cdOrigem";
            this.cdOrigem.ReadOnly = true;
            this.cdOrigem.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cdOrigem.Width = 150;
            // 
            // ufOrigem
            // 
            this.ufOrigem.DataPropertyName = "ufOrigem";
            this.ufOrigem.HeaderText = "UF Origem";
            this.ufOrigem.Name = "ufOrigem";
            this.ufOrigem.ReadOnly = true;
            this.ufOrigem.Width = 110;
            // 
            // cidadeDestino
            // 
            this.cidadeDestino.DataPropertyName = "cidadeDestino";
            this.cidadeDestino.HeaderText = "Cidade Destino";
            this.cidadeDestino.Name = "cidadeDestino";
            this.cidadeDestino.ReadOnly = true;
            this.cidadeDestino.Width = 300;
            // 
            // ufDestino
            // 
            this.ufDestino.DataPropertyName = "ufDestino";
            this.ufDestino.HeaderText = "UF Destino";
            this.ufDestino.Name = "ufDestino";
            this.ufDestino.ReadOnly = true;
            this.ufDestino.Width = 101;
            // 
            // lblCidadeDestino
            // 
            this.lblCidadeDestino.AutoSize = true;
            this.lblCidadeDestino.Location = new System.Drawing.Point(664, 39);
            this.lblCidadeDestino.Name = "lblCidadeDestino";
            this.lblCidadeDestino.Size = new System.Drawing.Size(108, 17);
            this.lblCidadeDestino.TabIndex = 16;
            this.lblCidadeDestino.Text = "Cidade Destino:";
            // 
            // lblUFDestino
            // 
            this.lblUFDestino.AutoSize = true;
            this.lblUFDestino.Location = new System.Drawing.Point(281, 39);
            this.lblUFDestino.Name = "lblUFDestino";
            this.lblUFDestino.Size = new System.Drawing.Size(82, 17);
            this.lblUFDestino.TabIndex = 15;
            this.lblUFDestino.Text = "UF Destino:";
            // 
            // lblCDOrigem
            // 
            this.lblCDOrigem.AutoSize = true;
            this.lblCDOrigem.Location = new System.Drawing.Point(12, 39);
            this.lblCDOrigem.Name = "lblCDOrigem";
            this.lblCDOrigem.Size = new System.Drawing.Size(81, 17);
            this.lblCDOrigem.TabIndex = 14;
            this.lblCDOrigem.Text = "CD Origem:";
            // 
            // cmbUFDestino
            // 
            this.cmbUFDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUFDestino.FormattingEnabled = true;
            this.cmbUFDestino.Location = new System.Drawing.Point(370, 36);
            this.cmbUFDestino.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbUFDestino.Name = "cmbUFDestino";
            this.cmbUFDestino.Size = new System.Drawing.Size(272, 24);
            this.cmbUFDestino.TabIndex = 2;
            this.cmbUFDestino.SelectedIndexChanged += new System.EventHandler(this.cmbUFDestino_SelectedIndexChanged);
            // 
            // bgWorkerCadastroItinerario
            // 
            this.bgWorkerCadastroItinerario.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerCadastroItinerario_DoWork);
            this.bgWorkerCadastroItinerario.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerCadastroItinerario_RunWorkerCompleted);
            // 
            // CadastroItinerarioUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.btnInserir);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnConsultar);
            this.Controls.Add(this.cmbCDOrigem);
            this.Controls.Add(this.cmbCidadeDestino);
            this.Controls.Add(this.grdItinerario);
            this.Controls.Add(this.lblCidadeDestino);
            this.Controls.Add(this.lblUFDestino);
            this.Controls.Add(this.lblCDOrigem);
            this.Controls.Add(this.cmbUFDestino);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CadastroItinerarioUC";
            this.Size = new System.Drawing.Size(1327, 670);
            ((System.ComponentModel.ISupportInitialize)(this.grdItinerario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cmbCDOrigem;
        private System.Windows.Forms.ComboBox cmbCidadeDestino;
        private System.Windows.Forms.DataGridView grdItinerario;
        private System.Windows.Forms.Label lblCidadeDestino;
        private System.Windows.Forms.Label lblUFDestino;
        private System.Windows.Forms.Label lblCDOrigem;
        private System.Windows.Forms.ComboBox cmbUFDestino;
        private System.ComponentModel.BackgroundWorker bgWorkerCadastroItinerario;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdOrigem;
        private System.Windows.Forms.DataGridViewTextBoxColumn ufOrigem;
        private System.Windows.Forms.DataGridViewTextBoxColumn cidadeDestino;
        private System.Windows.Forms.DataGridViewTextBoxColumn ufDestino;
    }
}
