﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using System.Globalization;
using Business;
using System.Threading;

namespace PainelFreteDPK
{
    public partial class frmInserirAtualizarGerente : Form
    {
        InserirAlterarGerente gerenteRegional;
        GerenteLimiteUC gerenteLimiteUC;
        frmGerenteLimite frmgerenteLimite;
        TipoAcao acao;
        public frmInserirAtualizarGerente(InserirAlterarGerente oGerenteRegional, GerenteLimiteUC oGerenteLimiteUC, frmGerenteLimite oFrmGerenteLimite, TipoAcao tpAcao)
        {
            try
            {
                acao = tpAcao;
                gerenteLimiteUC = oGerenteLimiteUC == null ? null : oGerenteLimiteUC;
                frmgerenteLimite = oFrmGerenteLimite == null ? null : oFrmGerenteLimite;
                gerenteRegional = oGerenteRegional;
                InitializeComponent();
                CarregarComboGerente(cmbGerente);
                CarregarComboFilial(cmbFilial);
                if (tpAcao == TipoAcao.Inserir)
                {
                    cmbGerente.Enabled = true;
                    cmbFilial.Enabled = true;
                }
                else if (tpAcao == TipoAcao.Atualizar)
                {
                    cmbGerente.SelectedIndex = cmbGerente.FindStringExact(oGerenteRegional.NOME_GERENTE + " - " + oGerenteRegional.TIPO);
                    cmbFilial.SelectedValue = oGerenteRegional.CDFILIAL;
                    cmbFilial.Enabled = false;
                    cmbGerente.Enabled = false;
                }

                txtGerenteRegional.Text = gerenteRegional.NOME_REGIONAL;
            }
            
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo frmInserirAtualizarGerente.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {
            List<ComboFilial> listaFilial = null;
            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "FILIAL";
            cmbFilial.ValueMember = "CDFILIAL";

            try
            {
                listaFilial = GlobalBE.listaFilial;
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboGerente(ComboBox cmbGerente)
        {
            List<ComboGerente> listaGerente = null;
            cmbGerente.DataSource = null;
            cmbGerente.Items.Clear();
            cmbGerente.DisplayMember = "DESCNOME";
            cmbGerente.ValueMember = "CPF";

            try
            {
                listaGerente = GlobalBE.listaGerente;
                cmbGerente.DataSource = listaGerente;
                cmbGerente.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarGerente, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FiltroGerenteLimite oFiltro = new FiltroGerenteLimite();
                if (cmbGerente.SelectedIndex == 0 || cmbGerente.SelectedIndex == -1)
                {
                    string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Gerente");
                    MessageBox.Show(mensagem, PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    oFiltro.gerente = ((ComboGerente)cmbGerente.SelectedItem).CPF;
                    oFiltro.tipo = ((ComboGerente)cmbGerente.SelectedItem).TIPO;
                }

                if (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1)
                {
                    string mensagem = String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Filial");
                    MessageBox.Show(mensagem, PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    oFiltro.filial = ((ComboFilial)cmbFilial.SelectedItem).CDFILIAL;
                }

                oFiltro.limite = string.IsNullOrEmpty(txtLimite.Text) ? 0 : Convert.ToDouble(txtLimite.Text);
                oFiltro.saldo = oFiltro.limite;
                oFiltro.gerenteRegional = gerenteRegional.CPF_REGIONAL;
                                
                oFiltro.loginAlteracao = GlobalBE.usuarioLogado.LOGIN;

                if (!GerenteBO.ValidaGerenteFilial(oFiltro) && acao == TipoAcao.Inserir)
                {
                    MessageBox.Show("O gerente já está cadastrado para essa filial.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                if (!GerenteBO.ValidaGerenteRegional(oFiltro))
                {
                    MessageBox.Show("O gerente já está associado a outro gerente regional.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                GerenteBO.GravarGerente(oFiltro);

                MessageBox.Show("Limite de Gerente gravado com sucesso.", PainelFreteDPK.ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Close();
                if (gerenteLimiteUC != null)
                {
                    gerenteLimiteUC.AtualizaGrid();
                }

                if (frmgerenteLimite != null)
                {
                    frmgerenteLimite.AtualizaGrid();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao validar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
            
        }

        private void txtLimite_TextChanged(object sender, EventArgs e)
        {

            txtSaldo.Text = txtLimite.Text;
        }

        private void txtLimite_Leave(object sender, EventArgs e)
        {
            try
            {
                double valor = 0.0d;
                if (Double.TryParse(txtLimite.Text, NumberStyles.Currency, null, out valor))
                {
                    txtLimite.Text = valor.ToString("N2");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtLimite_Leave.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void txtLimite_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == '.' || e.KeyChar == ',')
                {
                    e.KeyChar = ',';
                    if (txtLimite.Text.Contains(","))
                        e.Handled = true;
                }
                else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtLimite_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
