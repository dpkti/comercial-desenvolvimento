﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace PainelFreteDPK
{
    public partial class frmCadastroGerente : Form
    {
        public GerenteLimiteUC oGerenteLimiteUC = new GerenteLimiteUC();

        public frmCadastroGerente()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            if (ValidaTela())
            {
                Gerente oGerente = PreencherObjeto();

                bool existeGerente = GerenteBO.VerificaGerenteExiste(oGerente.cpf, oGerente.tipo);
                string mensagem = "";

                if (existeGerente)
                {
                    mensagem = String.Format("Deseja alterar os dados do gerente {0}?", oGerente.nome);
                    DialogResult result = MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        if (oGerente.tipo.Equals("R") && oGerente.situacao == 9)
                        {
                            bool pussuiGerenteRelacionados = GerenteBO.VerificaGerenteRelacionados(oGerente.cpf);

                            if (pussuiGerenteRelacionados)
                            {
                                MessageBox.Show("Para alterar a situação de um gerente regional é necessário remover o vínculo com os outros gerentes.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                            else
                            {
                                GerenteBO.CadastrarGerente(oGerente);
                                MessageBox.Show("Sucesso ao alterar dados do gerente.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                LimparTela();
                            }
                        }
                        else
                        {
                            GerenteBO.CadastrarGerente(oGerente);
                            MessageBox.Show("Sucesso ao alterar dados do gerente.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LimparTela();
                        }
                    }
                }
                else
                {
                    mensagem = String.Format("Deseja cadastrar o gerente {0}?", oGerente.nome);
                    DialogResult result = MessageBox.Show(mensagem, ResourcePainelFreteDPK.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        GerenteBO.CadastrarGerente(oGerente);
                        MessageBox.Show("Sucesso ao inserir dados do gerente.", ResourcePainelFreteDPK.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LimparTela();
                    }
                }
                oGerenteLimiteUC.AtualizaGrid();
                GlobalBE.listaGerente = Business.PainelParamentroBO.BuscarGerente();
            }
        }

        private Gerente PreencherObjeto()
        {
            Gerente oGerente = new Gerente();
            oGerente.cpf = Convert.ToInt64(txtCPF.Text);
            oGerente.nome = txtNome.Text;
            string tipo = "";

            if (rbFilial.Checked)
            {
                tipo = "F";
            }
            else
            {
                if (rbCordenador.Checked)
                {
                    tipo = "C";
                }
                else
                {
                    tipo = "R";
                }
            }

            oGerente.tipo = tipo;

            int situacao;
            if (rbAtivo.Checked)
            {
                situacao = 0;
            }
            else
            {
                situacao = 9;
            }

            oGerente.situacao = situacao;
            return oGerente;
        }

        private bool ValidaTela()
        {
            if (!ValidaCPF(txtCPF.Text))
            {
                MessageBox.Show(String.Format(ResourcePainelFreteDPK.Erro_CampoInvalido, "CPF"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (txtNome.Text.Length > 50)
            {
                MessageBox.Show("O nome deve possuir no máximo 50 caracteres.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (String.IsNullOrEmpty(txtNome.Text))
            {
                MessageBox.Show(String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Nome"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!rbFilial.Checked && !rbCordenador.Checked && !rbRegional.Checked)
            {
                MessageBox.Show(String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Tipo"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!rbAtivo.Checked && !rbDesativado.Checked)
            {
                MessageBox.Show(String.Format(ResourcePainelFreteDPK.Erro_CampoObrigatorioNaoPreenchido, "Situação"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool ValidaCPF(string cpf)
        {
            if (cpf.Length > 11)
                return false;

            while (cpf.Length != 11)
                cpf = '0' + cpf;

            bool igual = true;
            for (int i = 1; i < 11 && igual; i++)
                if (cpf[i] != cpf[0])
                    igual = false;

            if (igual || cpf == "12345678909")
                return false;

            int[] numeros = new int[11];

            for (int i = 0; i < 11; i++)
                numeros[i] = int.Parse(cpf[i].ToString());

            int soma = 0;
            for (int i = 0; i < 9; i++)
                soma += (10 - i) * numeros[i];

            int resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[9] != 0)
                    return false;
            }
            else if (numeros[9] != 11 - resultado)
                return false;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += (11 - i) * numeros[i];

            resultado = soma % 11;

            if (resultado == 1 || resultado == 0)
            {
                if (numeros[10] != 0)
                    return false;
            }
            else
                if (numeros[10] != 11 - resultado)
                    return false;

            return true;
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {

                e.Handled = true;

            }
        }

        private void LimparTela()
        {
            txtCPF.Text = "";
            txtNome.Text = "";
            rbAtivo.Checked = false;
            rbCordenador.Checked = false;
            rbDesativado.Checked = false;
            rbFilial.Checked = false;
            rbRegional.Checked = false;
            txtCPF.Enabled = true;
            GbTipo.Enabled = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCPF.Text) && String.IsNullOrEmpty(txtNome.Text))
            {
                MessageBox.Show("Para realizar a pesquisa é necessário preencher o CPF ou Nome.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                List<Gerente> listaGerente = new List<Gerente>();
                listaGerente = GerenteBO.BuscarGerente(txtCPF.Text, txtNome.Text);
                if (listaGerente.Count == 0)
                {
                    MessageBox.Show("Gerente não encontrado.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    if (listaGerente.Count == 1)
                    {
                        Gerente oGerente = new Gerente();
                        oGerente = listaGerente.FirstOrDefault();
                        PreencherTxtBox(oGerente);
                    }
                    else
                    {
                        frmListaGerente ofrmListaGerente = new frmListaGerente(listaGerente);
                        ofrmListaGerente.oFrmCadastroGerente = this;
                        ofrmListaGerente.StartPosition = FormStartPosition.CenterParent;
                        ofrmListaGerente.ShowDialog();
                    }
                }
            }
        }

        public void PreencherTxtBox(Gerente oGerente)
        {
            txtCPF.Text = oGerente.cpf.ToString();
            txtNome.Text = oGerente.nome;
            switch (oGerente.tipo)
            {
                case "R":
                    rbCordenador.Checked = false;
                    rbFilial.Checked = false;
                    rbRegional.Checked = true;
                    break;
                case "F":
                    rbCordenador.Checked = false;
                    rbFilial.Checked = true;
                    rbRegional.Checked = false;
                    break;
                case "C":
                    rbCordenador.Checked = true;
                    rbFilial.Checked = false;
                    rbRegional.Checked = false;
                    break;
            }
            switch (oGerente.situacao)
            {
                case 0:
                    rbAtivo.Checked = true;
                    rbDesativado.Checked = false;
                    break;
                case 9:
                    rbAtivo.Checked = false;
                    rbDesativado.Checked = true;
                    break;
            }
            txtCPF.Enabled = false;
            GbTipo.Enabled = false;
        }

        private void frmCadastroGerente_FormClosing(object sender, FormClosingEventArgs e)
        {
            oGerenteLimiteUC.AtualizaGrid();
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparTela();
        }
    }
}
