﻿namespace PainelFreteDPK
{
    partial class frmItinerarioAlteracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTextCodItinerario = new System.Windows.Forms.Label();
            this.lbCodItinerario = new System.Windows.Forms.Label();
            this.cmbUfDestino = new System.Windows.Forms.ComboBox();
            this.cmbCidadeDestino = new System.Windows.Forms.ComboBox();
            this.cmbCDOrigem = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTextCodItinerario
            // 
            this.lbTextCodItinerario.AutoSize = true;
            this.lbTextCodItinerario.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTextCodItinerario.Location = new System.Drawing.Point(22, 15);
            this.lbTextCodItinerario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTextCodItinerario.Name = "lbTextCodItinerario";
            this.lbTextCodItinerario.Size = new System.Drawing.Size(104, 13);
            this.lbTextCodItinerario.TabIndex = 0;
            this.lbTextCodItinerario.Text = "Código Itinerário:";
            // 
            // lbCodItinerario
            // 
            this.lbCodItinerario.AutoSize = true;
            this.lbCodItinerario.Location = new System.Drawing.Point(127, 15);
            this.lbCodItinerario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbCodItinerario.Name = "lbCodItinerario";
            this.lbCodItinerario.Size = new System.Drawing.Size(0, 13);
            this.lbCodItinerario.TabIndex = 1;
            // 
            // cmbUfDestino
            // 
            this.cmbUfDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUfDestino.FormattingEnabled = true;
            this.cmbUfDestino.Location = new System.Drawing.Point(288, 42);
            this.cmbUfDestino.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbUfDestino.Name = "cmbUfDestino";
            this.cmbUfDestino.Size = new System.Drawing.Size(175, 21);
            this.cmbUfDestino.TabIndex = 2;
            this.cmbUfDestino.SelectedIndexChanged += new System.EventHandler(this.cmbUfDestino_SelectedIndexChanged);
            // 
            // cmbCidadeDestino
            // 
            this.cmbCidadeDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCidadeDestino.FormattingEnabled = true;
            this.cmbCidadeDestino.Location = new System.Drawing.Point(565, 42);
            this.cmbCidadeDestino.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbCidadeDestino.Name = "cmbCidadeDestino";
            this.cmbCidadeDestino.Size = new System.Drawing.Size(180, 21);
            this.cmbCidadeDestino.TabIndex = 3;
            // 
            // cmbCDOrigem
            // 
            this.cmbCDOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCDOrigem.FormattingEnabled = true;
            this.cmbCDOrigem.Location = new System.Drawing.Point(87, 41);
            this.cmbCDOrigem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbCDOrigem.Name = "cmbCDOrigem";
            this.cmbCDOrigem.Size = new System.Drawing.Size(114, 21);
            this.cmbCDOrigem.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 44);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "CD Origem:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(222, 45);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "UF Destino:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(479, 45);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Cidade Destino:";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(608, 69);
            this.btnSalvar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(66, 28);
            this.btnSalvar.TabIndex = 8;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(679, 69);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(66, 28);
            this.btnCancelar.TabIndex = 9;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmItinerarioAlteracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 105);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbCDOrigem);
            this.Controls.Add(this.cmbCidadeDestino);
            this.Controls.Add(this.cmbUfDestino);
            this.Controls.Add(this.lbCodItinerario);
            this.Controls.Add(this.lbTextCodItinerario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmItinerarioAlteracao";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTextCodItinerario;
        private System.Windows.Forms.Label lbCodItinerario;
        private System.Windows.Forms.ComboBox cmbUfDestino;
        private System.Windows.Forms.ComboBox cmbCidadeDestino;
        private System.Windows.Forms.ComboBox cmbCDOrigem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCancelar;
    }
}