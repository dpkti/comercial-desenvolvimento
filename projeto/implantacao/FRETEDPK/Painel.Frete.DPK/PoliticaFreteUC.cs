﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;
using UtilGeralDPA;

namespace PainelFreteDPK
{
    public partial class PoliticaFreteUC : UserControl
    {
        FiltroGrid ofiltro;
        List<ItinerarioTransportadoraResultado> listaTransp;

        public PoliticaFreteUC()
        {
            InitializeComponent();
            CarregarComboCdOrigem(cmbCDOrigem);
            CarregarComboUfDestino(cmbUFDestino);
            btnExportarExcel.Enabled = false;
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            frmNovaPoliticaFrete politicaFrete = new frmNovaPoliticaFrete();
            politicaFrete.politicaFreteUC = this;
            politicaFrete.StartPosition = FormStartPosition.CenterParent;
            politicaFrete.ShowDialog();

        }

        private void grdPoliticaFrete_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right && e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                grdPoliticaFrete.ClearSelection();
                grdPoliticaFrete.Rows[e.RowIndex].Selected = true;
            }

        }

        private void CarregarComboCodigoItinerario(ComboBox cmbItinerario)
        {
            cmbItinerario.Items.Clear();

            cmbItinerario.DisplayMember = "ITINERARIO";
            cmbItinerario.ValueMember = "COD_ITINERARIO";

            List<CodigoItinerarioBE> listaCodigosItinerario = null;

            try
            {
                listaCodigosItinerario = GlobalBE.listaCdItinerario;
                cmbItinerario.DataSource = listaCodigosItinerario;
                cmbItinerario.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCodItenerario, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }
        private void grdPoliticaFrete_MouseUp(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:

                    if (grdPoliticaFrete.SelectedRows.Count <= 0 || Convert.ToDateTime(grdPoliticaFrete.SelectedRows[0].Cells["dataVigencia"].Value) <= DateTime.Now) return;

                    ContextMenu myContextMenu = new ContextMenu();

                    MenuItem menuItem0 = new MenuItem("Alterar");

                    myContextMenu.MenuItems.Add(menuItem0);

                    if (grdPoliticaFrete.SelectedRows.Count == 1)
                    {
                        myContextMenu.MenuItems.Add(menuItem0);

                        menuItem0.Click += new EventHandler(delegate(Object o, EventArgs a)
                        {
                            frmNovaPoliticaFrete frmNovaPoliticaFrete = new frmNovaPoliticaFrete(Convert.ToDateTime(grdPoliticaFrete.SelectedRows[0].Cells["dataVigencia"].Value), Convert.ToInt32(grdPoliticaFrete.SelectedRows[0].Cells["codigoItinerario"].Value), Convert.ToInt32(grdPoliticaFrete.SelectedRows[0].Cells["canal"].Value));
                            frmNovaPoliticaFrete.politicaFreteUC = this;
                            frmNovaPoliticaFrete.StartPosition = FormStartPosition.CenterParent;
                            frmNovaPoliticaFrete.ShowDialog();
                        });

                    }

                    myContextMenu.Show(grdPoliticaFrete, e.Location, LeftRightAlignment.Right);

                    break;
            }

        }

        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();

                //Adding the Columns
                foreach (DataGridViewColumn column in grdPoliticaFrete.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in grdPoliticaFrete.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                if (grdPoliticaFrete.Rows.Count < 100)
                {
                    for (int j = grdPoliticaFrete.Rows.Count; j <= (grdPoliticaFrete.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= grdPoliticaFrete.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch(Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void CarregarComboCdOrigem(ComboBox cmbCDOrigem)
        {
            List<CdOrigemBE> listaCdOrigem = null;
            cmbCDOrigem.DataSource = null;
            cmbCDOrigem.Items.Clear();
            cmbCDOrigem.DisplayMember = "CDORIGEM";
            cmbCDOrigem.ValueMember = "CD";

            try
            {
                listaCdOrigem = GlobalBE.listaCdOrigem;
                cmbCDOrigem.DataSource = listaCdOrigem;
                cmbCDOrigem.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarCdOrigem, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }
        private void CarregarComboUfDestino(ComboBox cmbUFDestino)
        {
            List<UfDestinoBE> listaUfDestino = null;
            cmbUFDestino.DataSource = null;
            cmbUFDestino.Items.Clear();
            cmbUFDestino.DisplayMember = "UF";
            cmbUFDestino.ValueMember = "CDUF";

            try
            {
                listaUfDestino = GlobalBE.listaUF;
                cmbUFDestino.DataSource = listaUfDestino;
                cmbUFDestino.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboCidadeDestino(ComboBox cmbCidadeDestino, string uf)
        {
            try
            {
                cmbCidadeDestino.DataSource = null;
                cmbCidadeDestino.Items.Clear();
                DoubleBuffered = true;
                cmbCidadeDestino.DisplayMember = "CIDADE";
                cmbCidadeDestino.ValueMember = "CODCIDADE";
                List<CidadeDestinoBE> listaCidadeDestino = null;
                var listatmp = from l in GlobalBE.listaCidade where (l.UF == uf || l.CODCIDADE == 0 || uf == null) select l;
                listaCidadeDestino = listatmp.ToList();
                cmbCidadeDestino.DataSource = listaCidadeDestino;
                DoubleBuffered = true;
                if (string.IsNullOrEmpty(uf))
                {
                    cmbCidadeDestino.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourcePainelFreteDPK.ErroCarregarUfDestino, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void cmbCidadeDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCidadeDestino.SelectedIndex != 0 && cmbCidadeDestino.SelectedIndex != -1)
            {
                string uf;
                uf = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).UF;
                cmbUFDestino.SelectedValue = uf;
            }
        }

        private void cmbUFDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cduf = null;
            CidadeDestinoBE cdcidade = new CidadeDestinoBE();
            if (cmbUFDestino.SelectedIndex != 0 && cmbUFDestino.SelectedIndex != -1)
            {
                cduf = ((UfDestinoBE)cmbUFDestino.SelectedItem).CDUF;
            }
            else if (cmbCidadeDestino.Items.Count > 0)
            {
                cmbCidadeDestino.SelectedIndex = 0;
            }
            if (cmbCidadeDestino.Items.Count > 0)
            {
                cdcidade = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem);
            }
            if (cmbCidadeDestino.SelectedIndex == 0 || cmbCidadeDestino.SelectedIndex == -1 || cdcidade.UF != cduf)
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                DoubleBuffered = true;
            }
            else
            {
                CarregarComboCidadeDestino(cmbCidadeDestino, cduf);
                cmbCidadeDestino.SelectedValue = cdcidade.CODCIDADE;
                DoubleBuffered = true;
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            cmbCDOrigem.SelectedIndex = 0;
            cmbUFDestino.SelectedIndex = 0;
            cmbCidadeDestino.SelectedIndex = 0;
            txtCdItinerario.Text = "";
            txtCodigoTransportadora.Text = "";
        }

        private void txtCdItinerario_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            e.Handled = !Char.IsDigit(e.KeyChar) && e.KeyChar != Delete;
        }

        private void txtCodigoTransportadora_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            e.Handled = !Char.IsDigit(e.KeyChar) && e.KeyChar != Delete;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        public void Consultar()
        {
            try
            {
                if (cmbCidadeDestino.SelectedIndex == -1)
                {
                    MessageBox.Show("Cidade selecionada inválida.", ResourcePainelFreteDPK.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                ofiltro = new FiltroGrid();

                ofiltro.cdItinerario = txtCdItinerario.Text;
                ofiltro.cdTransp = txtCodigoTransportadora.Text;
                ofiltro.cdOrigem = ((CdOrigemBE)cmbCDOrigem.SelectedItem).CD;
                ofiltro.ufDestino = ((UfDestinoBE)cmbUFDestino.SelectedItem).CDUF;
                ofiltro.cdCidadeDestino = ((CidadeDestinoBE)cmbCidadeDestino.SelectedItem).CODCIDADE;

                btnConsultar.Text = "Carregando...";
                btnConsultar.Enabled = false;
                btnExportarExcel.Enabled = false;
                btnLimpar.Enabled = false;
                btnInserir.Enabled = false;
                btnImportarExcel.Enabled = false;

                bgWorkerPoliticaFrete.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao consultar itinerários transportadoras.", ResourcePainelFreteDPK.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void btnImportarExcel_Click(object sender, EventArgs e)
        {
            frmImportarExcel oFrmImportarExcel = new frmImportarExcel();
            oFrmImportarExcel.StartPosition = FormStartPosition.CenterParent;
            oFrmImportarExcel.ShowDialog();
        }

        private void bgWorkerPoliticaFrete_DoWork(object sender, DoWorkEventArgs e)
        {
            listaTransp = new List<ItinerarioTransportadoraResultado>();
            listaTransp = PoliticaFreteBO.BuscarTransportadora(ofiltro);
        }

        private void bgWorkerPoliticaFrete_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            grdPoliticaFrete.DataSource = listaTransp;
            if (listaTransp.Count == 0)
            {
                string mensagem = "Nenhuma politica encontrada.";
                MessageBox.Show(mensagem, ResourcePainelFreteDPK.MensagemResultadoBusca, MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnExportarExcel.Enabled = false;
            }
            else
            {
                btnExportarExcel.Enabled = true;
            }

            btnConsultar.Text = "Consultar";
            btnConsultar.Enabled = true;
            btnLimpar.Enabled = true;
            btnInserir.Enabled = true;
            btnImportarExcel.Enabled = true;
            
        }
    }
}
