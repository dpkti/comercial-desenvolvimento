﻿namespace PainelFreteDPK
{
    partial class frmGerenteLimite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grdGerenteLimite = new System.Windows.Forms.DataGridView();
            this.CPF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COD_FILIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Limite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Saldo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblGerenteRegional = new System.Windows.Forms.Label();
            this.lblGerenteRegionalTxt = new System.Windows.Forms.Label();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.lblFilial = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdGerenteLimite)).BeginInit();
            this.SuspendLayout();
            // 
            // grdGerenteLimite
            // 
            this.grdGerenteLimite.AllowUserToAddRows = false;
            this.grdGerenteLimite.AllowUserToDeleteRows = false;
            this.grdGerenteLimite.AllowUserToOrderColumns = true;
            this.grdGerenteLimite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdGerenteLimite.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CPF,
            this.COD_FILIAL,
            this.NOME,
            this.Tipo,
            this.Filial,
            this.Limite,
            this.Saldo});
            this.grdGerenteLimite.Location = new System.Drawing.Point(19, 35);
            this.grdGerenteLimite.Margin = new System.Windows.Forms.Padding(2);
            this.grdGerenteLimite.Name = "grdGerenteLimite";
            this.grdGerenteLimite.ReadOnly = true;
            this.grdGerenteLimite.RowTemplate.Height = 24;
            this.grdGerenteLimite.Size = new System.Drawing.Size(771, 472);
            this.grdGerenteLimite.TabIndex = 1;
            this.grdGerenteLimite.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdGerenteLimite_CellMouseClick);
            this.grdGerenteLimite.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdGerenteLimite_MouseUp);
            // 
            // CPF
            // 
            this.CPF.DataPropertyName = "CPF";
            this.CPF.HeaderText = "CPF";
            this.CPF.Name = "CPF";
            this.CPF.ReadOnly = true;
            this.CPF.Visible = false;
            // 
            // COD_FILIAL
            // 
            this.COD_FILIAL.DataPropertyName = "COD_FILIAL";
            this.COD_FILIAL.HeaderText = "COD_FILIAL";
            this.COD_FILIAL.Name = "COD_FILIAL";
            this.COD_FILIAL.ReadOnly = true;
            this.COD_FILIAL.Visible = false;
            // 
            // NOME
            // 
            this.NOME.DataPropertyName = "NOME";
            this.NOME.HeaderText = "Gerente";
            this.NOME.Name = "NOME";
            this.NOME.ReadOnly = true;
            this.NOME.Width = 210;
            // 
            // Tipo
            // 
            this.Tipo.DataPropertyName = "tipo";
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            // 
            // Filial
            // 
            this.Filial.DataPropertyName = "FILIAL";
            this.Filial.HeaderText = "Filial";
            this.Filial.Name = "Filial";
            this.Filial.ReadOnly = true;
            this.Filial.Width = 200;
            // 
            // Limite
            // 
            this.Limite.DataPropertyName = "LIMITE";
            dataGridViewCellStyle5.Format = "N2";
            this.Limite.DefaultCellStyle = dataGridViewCellStyle5;
            this.Limite.HeaderText = "Limite";
            this.Limite.Name = "Limite";
            this.Limite.ReadOnly = true;
            // 
            // Saldo
            // 
            this.Saldo.DataPropertyName = "SALDO";
            dataGridViewCellStyle6.Format = "N2";
            this.Saldo.DefaultCellStyle = dataGridViewCellStyle6;
            this.Saldo.HeaderText = "Saldo";
            this.Saldo.Name = "Saldo";
            this.Saldo.ReadOnly = true;
            // 
            // lblGerenteRegional
            // 
            this.lblGerenteRegional.AutoSize = true;
            this.lblGerenteRegional.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGerenteRegional.Location = new System.Drawing.Point(19, 11);
            this.lblGerenteRegional.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGerenteRegional.Name = "lblGerenteRegional";
            this.lblGerenteRegional.Size = new System.Drawing.Size(110, 13);
            this.lblGerenteRegional.TabIndex = 8;
            this.lblGerenteRegional.Text = "Gerente Regional:";
            // 
            // lblGerenteRegionalTxt
            // 
            this.lblGerenteRegionalTxt.AutoSize = true;
            this.lblGerenteRegionalTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGerenteRegionalTxt.Location = new System.Drawing.Point(126, 11);
            this.lblGerenteRegionalTxt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGerenteRegionalTxt.Name = "lblGerenteRegionalTxt";
            this.lblGerenteRegionalTxt.Size = new System.Drawing.Size(0, 13);
            this.lblGerenteRegionalTxt.TabIndex = 9;
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(552, 11);
            this.cmbFilial.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(238, 21);
            this.cmbFilial.TabIndex = 10;
            this.cmbFilial.SelectedIndexChanged += new System.EventHandler(this.cmbFilial_SelectedIndexChanged);
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.lblFilial.Location = new System.Drawing.Point(483, 14);
            this.lblFilial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(56, 13);
            this.lblFilial.TabIndex = 11;
            this.lblFilial.Text = "Gerente:";
            // 
            // frmGerenteLimite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 518);
            this.Controls.Add(this.cmbFilial);
            this.Controls.Add(this.lblFilial);
            this.Controls.Add(this.lblGerenteRegionalTxt);
            this.Controls.Add(this.lblGerenteRegional);
            this.Controls.Add(this.grdGerenteLimite);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "frmGerenteLimite";
            this.Text = "Gerente Limite";
            ((System.ComponentModel.ISupportInitialize)(this.grdGerenteLimite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grdGerenteLimite;
        private System.Windows.Forms.Label lblGerenteRegional;
        private System.Windows.Forms.Label lblGerenteRegionalTxt;
        private System.Windows.Forms.DataGridViewTextBoxColumn CPF;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_FILIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Limite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Saldo;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label lblFilial;
    }
}