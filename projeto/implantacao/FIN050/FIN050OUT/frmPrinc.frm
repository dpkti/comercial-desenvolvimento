VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmPrincipal 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIL050(OUT) - Enviar Pedidos e Consultas"
   ClientHeight    =   2820
   ClientLeft      =   2865
   ClientTop       =   2220
   ClientWidth     =   5580
   Icon            =   "frmPrinc.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2820
   ScaleWidth      =   5580
   Begin VB.CheckBox chkDicas 
      Appearance      =   0  'Flat
      Caption         =   "Envio de Dicas"
      Enabled         =   0   'False
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   1290
      TabIndex        =   7
      Top             =   960
      Width           =   2295
   End
   Begin Bot�o.cmd sscmdIniciar 
      Height          =   525
      Left            =   1380
      TabIndex        =   4
      Top             =   1680
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   "Iniciar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrinc.frx":0442
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      Height          =   1005
      Left            =   30
      Pattern         =   "*.EXE"
      TabIndex        =   3
      Top             =   30
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tarefas a serem executadas"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   1560
      Left            =   1170
      TabIndex        =   0
      Top             =   30
      Width           =   3345
      Begin VB.CheckBox chkVisitas 
         Appearance      =   0  'Flat
         Caption         =   "Envio de Visitas"
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   1230
         Width           =   2295
      End
      Begin VB.CheckBox chkPedido 
         Appearance      =   0  'Flat
         Caption         =   "Envio de Pedidos"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   630
         Width           =   2355
      End
      Begin VB.CheckBox chkConsulta 
         Appearance      =   0  'Flat
         Caption         =   "Envio de Consultas de Pedidos"
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   330
         Width           =   2985
      End
   End
   Begin Bot�o.cmd sscmdSair 
      Height          =   525
      Left            =   2940
      TabIndex        =   5
      Top             =   1680
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrinc.frx":045E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblMensagem 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   525
      Left            =   0
      TabIndex        =   6
      Top             =   2250
      Width           =   5565
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strEmail_To As String
Dim strEmail_From As String
Dim vTipo As String
Dim SQL As String
Dim SQL2 As String
Dim num_arq As Integer
Dim i As Integer
Dim Consulta As String
Dim Fl_Aberto As String
Dim SS3 As Object
Dim vRep As Object
Dim vCodPaacRepres As Long
Dim sDicas As Object
Dim sVisitaPromotor As Object

Private Sub Form_Load()
1         On Error GoTo Trata_Erro
          
2         If App.PrevInstance = True Then
3             MsgBox "Este programa j� est� aberto.", vbInformation, "Aten��o"
4             End
5         End If
          
6         Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
          
          '*************************
          'Define drive de execu��o
7         Path_drv = Pegar_Drive
8         Path_drv = "C:"
          
9         strPath = Path_drv & "\DADOS\"
10        STRPATH2 = Path_drv & "\COM\"
          '*************************
          
11        File1.Path = Path_drv & "\PGMS\"
          
12        Screen.MousePointer = 11
13        sscmdIniciar.Enabled = False
14        sscmdSair.Enabled = False
          
          '**************************
          'busca informacoes sobre FTP origem/destino
15        Set dbaccess2 = OpenDatabase(STRPATH2 & "DBMAIL.MDB")
          
16        SQL = "SELECT DESTINO as EMAIL_TO,"
17        SQL = SQL & " USUARIO as EMAIL_FROM FROM USUARIO "
18        Set SS = dbaccess2.CreateSnapshot(SQL)
19        FreeLocks
20        strEmail_To = SS!email_to
21        strEmail_From = SS!email_from
22        SS.Close
23        dbaccess2.Close
            
          'Luciano Carlos de Jesus - 14/07/2008
          'Monta arquivo de dicas para envio via FTP
24        If GeraDicas = True Then
25            chkDicas.Value = 1
26            chkDicas.Enabled = False
27        Else
28            chkDicas.Value = 0
29            chkDicas.Enabled = False
30        End If
               
          'Luciano Carlos de Jesus - 24/08/2008
          'Gerar arquivo de visita de promotor
31        If GeraVisitaPromotor = True Then
32            chkVisitas.Value = 1
33            chkVisitas.Enabled = False
34        Else
35            chkVisitas.Value = 0
36            chkVisitas.Enabled = False
37        End If

          '******************************************
          'monta arquivo de consulta de pedidos em aberto
          
38        Set dbaccess = OpenDatabase(strPath & "base_dpk.MDB")
          
39        SQL = "SELECT a.COD_LOJA, a.NUM_PEDIDO, a.SEQ_PEDIDO, a.COD_FILIAL, b.dt_real"
40        SQL = SQL & " FROM PEDNOTA_VENDA a, DATAS b"
41        SQL = SQL & " WHERE FL_GER_NFIS='N' and "
42        SQL = SQL & " SITUACAO=0 "
          
43        Set SS = dbaccess.CreateSnapshot(SQL)
44        FreeLocks
          
45        SQL2 = " SELECT iif(a.COD_LOJA_VENDA=0,iif(a.COD_LOJA_TRANSF=0,a.COD_LOJA_COTACAO,a.COD_LOJA_TRANSF),a.COD_LOJA_VENDA) AS COD_LOJA_TRANSF, " & _
          " iif(a.NUM_PEDIDO_VENDA=0,iif(a.NUM_PEDIDO=0,a.NUM_COTACAO_TEMP,a.NUM_PEDIDO),a.NUM_PEDIDO_VENDA) as NUM_PEDIDO_TRANSF, " & _
          " b.COD_FILIAL, c.DT_REAL, A.NUM_PEDIDO, A.COD_LOJA_TRANSF, A.SEQ_PEDIDO "
46        SQL2 = SQL2 & " FROM R_COTACAO_TRANSF a, DEPOSITO b, DATAS c"
47        SQL2 = SQL2 & " WHERE COD_LOJA_VENDA = 0 AND NUM_PEDIDO_VENDA = 0 "
          
48        Set SS2 = dbaccess.CreateSnapshot(SQL2)
49        FreeLocks
          
50        num_arq = FreeFile
              
51        Fl_Aberto = "N"
52        If Not SS2.EOF Then
53            SS2.MoveLast
54            SS2.MoveFirst
55        End If
          
56        Do While Not SS2.EOF
              'Verifica se j� existe o num_pedido de transferencia
57            If Len(SS2!num_pedido) > 0 Or SS2!num_pedido > 0 Then
58                SQL = "SELECT p.COD_LOJA, p.NUM_PEDIDO, p.SEQ_PEDIDO "
59                SQL = SQL & " FROM PEDNOTA_VENDA p, R_COTACAO_TRANSF r"
60                SQL = SQL & " WHERE p.cod_loja=r.cod_loja_transf and "
61                SQL = SQL & " p.num_pedido = r.num_pedido and"
62                SQL = SQL & " p.seq_pedido = r.seq_pedido and "
63                SQL = SQL & " p.cod_loja=" & SS2!cod_loja_transf & " and"
64                SQL = SQL & " p.num_pedido = " & SS2!num_pedido & " and"
65                SQL = SQL & " p.seq_pedido = " & SS2!SEQ_PEDIDO & " and "
                  'SQL = SQL & " p.FL_GER_NFIS='N' and "
66                SQL = SQL & " p.SITUACAO=0 "
                  
                  'Eduardo - 17/02/06
67                SQL = SQL & " and r.cod_loja_venda <> 0 and r.num_pedido_venda <> 0"
68                Set SS3 = dbaccess.CreateSnapshot(SQL)
69                FreeLocks
70            End If
71            If SS3.EOF = True Then
72                If Fl_Aberto = "N" Then
73                    Consulta = Path_drv & "\COM\SAIDA\PEDIDOS\CON" & Format(SS2!cod_filial, "0000") & Format(Time, "hhmmss") & ".CON"
74                    Open Consulta For Output As #num_arq
75                    Fl_Aberto = "S"
76                End If
77                Print #num_arq, Format(SS2!cod_loja_transf, "00"); Format(SS2!num_pedido_transf, "0000000"); "0"
78            End If
79            SS2.MoveNext
80        Loop
              
81        If Not SS.EOF Then
82            SS.MoveLast
83            SS.MoveFirst
84            If Fl_Aberto = "N" Then
85                Consulta = Path_drv & "\COM\SAIDA\PEDIDOS\CON" & Format(SS!cod_filial, "0000") & Format(Time, "hhmmss") & ".CON"
86                Open Consulta For Output As #num_arq
87                Fl_Aberto = "S"
88            End If
              
89            For i = 1 To SS.RecordCount
90                Print #num_arq, Format(SS!cod_loja, "00"); Format(SS!num_pedido, "0000000"); Format(SS!SEQ_PEDIDO, "0")
91                SS.MoveNext
92            Next
93        End If
94        Close #num_arq
95        num_arq = FreeFile
96        If Fl_Aberto = "S" Then
97            Open Path_drv & "\COM\SAIDA\MSG\CON" & Mid(Consulta, 25, 10) & ".HDR" For Output As #num_arq
                  
98            Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
99            Print #num_arq, "TO        =" & strEmail_To
100           Print #num_arq, "FROM      =" & strEmail_From
101           Print #num_arq, "DESCRICAO =" & "#CON"; Mid(Consulta, 25, 10)
102           Print #num_arq, "FILE      =" & "CON" & Mid(Consulta, 25, 10) & ".CON"
              
103           Close #num_arq
                  
104       End If
              
105       SS.Close
106       SS2.Close
         '*************************
              
         'Verifica as tarefas a serem executadas e apresenta ao usu�rio
107       chkConsulta.Value = 0
108       chkPedido.Value = 0
              
109       If Trim(Dir(Path_drv & "\COM\SAIDA\MSG\PED*.HDR")) <> "" And _
              Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\PED*.TXT")) <> "" Then
110           chkPedido.Value = 1
111       End If

112       If Trim(Dir(Path_drv & "\COM\SAIDA\MSG\CON*.HDR")) <> "" And _
              Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\CON*.CON")) <> "" Then
113           chkConsulta.Value = 1
114       End If

          'Eduardo - 25/04/2006
115       If Trim(Dir(Path_drv & "\COM\SAIDA\MSG\PED*.HDR")) <> "" And _
              Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\PED*.TXT")) <> "" Or _
              Trim(Dir(Path_drv & "\COM\SAIDA\MSG\CON*.HDR")) <> "" And _
              Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\CON*.CON")) <> "" Then
              
116           Set vRep = dbaccess.OpenRecordset("Select cod_filial, cod_repres from Deposito")
              
			  'Login - RLIMA (Ci&T) Data - 15/06/2012
			  'Jira - RTI-137
			  'Descri��o - Definindo o de tipo de usu�rio igual a Gerente.
117           If UCase(Left(strEmail_From, 2)) = "PA" Then
118               vTipo = "P"
119               vCodPaacRepres = vRep!cod_filial
120           ElseIf UCase(Left(strEmail_From, 2)) = "RE" Then
121               vCodPaacRepres = vRep!cod_repres
122               vTipo = "R"
              ElseIf UCase(Left(strEmail_From, 2)) = "GE" Then
                  vCodPaacRepres = vRep!cod_repres
                  vTipo = "G"
123           End If
              
124           If Dir(Path_drv & "\COMUNICA\OUT\PEDCONTR" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT") <> "" Then Kill Path_drv & "\COMUNICA\OUT\PEDCONTR" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT"
              
              '13/03/2008
125           If Dir(Path_drv & "\COMUNICA\OUT\PEDINDIC" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT") <> "" Then Kill Path_drv & "\COMUNICA\OUT\PEDINDIC" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT"
              
              
126           Data_Ult_Atualizacao Format(vCodPaacRepres, "0000"), vTipo
127           Gerar_Controle strEmail_To, strEmail_From, Format(vCodPaacRepres, "0000"), vTipo
128           Get_File_Version Path_drv & "\PGMS", Format(vCodPaacRepres, "0000"), vTipo
129           Registros Format(vCodPaacRepres, "0000"), vTipo
130           Registros_Diferenca strEmail_From, Format(vCodPaacRepres, "0000"), vTipo
              
              '13/03/2008
131           If Dir("C:\PK_BASE_DPK.TXT") <> "" Then
132              Gerar_HDR_Indice strEmail_To, strEmail_From, Format(vCodPaacRepres, "0000"), vTipo
133              FileCopy "C:\PK_BASE_DPK.TXT", Path_drv & "\COMUNICA\OUT\PEDINDIC" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT"
134              Kill "C:\PK_BASE_DPK.TXT"
135           End If
              
136           Set vRep = Nothing
137       End If

138       chkConsulta.Enabled = False
139       chkPedido.Enabled = False
140       sscmdIniciar.Enabled = True
          
141       dbaccess.Close
          
142       lblMensagem = "Clique no bot�o iniciar para executar as tarefas marcadas acima ou verificar se existem tarefas a receber!"
143       Screen.MousePointer = 0

Trata_Erro:
144       If Err.Number = 94 Then
145           Resume Next
146       ElseIf Err.Number <> 0 Then
147           MsgBox "Sub FrmPrincipal_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "C�digo:" & Err.Description & vbCrLf & "Linha: " & Erl
148       End If
End Sub

'*******************************************************************************************************************
'Luciano Carlos de Jesus - 14/07/2008
'Monta arquivo de dicas para envio via FTP

Private Function GeraDicas() As Boolean
          Dim tipo As String
          Dim sequencia As String
          Dim codcliente As String
          Dim dt_dica As String
          Dim dt_alteracao As String
          Dim cod_vend As String
          Dim dica As String
          Dim vcount As Integer
          
1         On errot GoTo TrataErro
          
2         GeraDicas = False
          
3         Set dbaccess = OpenDatabase(strPath & "BASE_DPK.MDB")
          
4         SQL = "SELECT controle_dicas.FL_TIPO, controle_dicas.SEQ_ALTERACAO, controle_dicas.DT_DICA, controle_dicas.SEQUENCIA, "
5         SQL = SQL & " controle_dicas.COD_CLIENTE, controle_dicas.DT_ALTERACAO, controle_dicas.COD_VEND, controle_dicas.DICA"
6         SQL = SQL & " FROM controle_dicas;"

7         Set sDicas = dbaccess.CreateDynaset(SQL)
8         vcount = sDicas.RecordCount
9         FreeLocks
          
10        If sDicas.EOF = False Then
11            GeraDicas = True
12            Set vRep = dbaccess.OpenRecordset("Select cod_filial, cod_repres from Deposito")
			  'Login - RLIMA (Ci&T) Data - 15/06/2012
			  'Jira - RTI-137
			  'Descri��o - Definindo o de tipo de usu�rio igual a Gerente.
13            If UCase(Left(strEmail_From, 2)) = "PA" Then
14                vTipo = "P"
15                vCodPaacRepres = vRep!cod_filial
16            ElseIf UCase(Left(strEmail_From, 2)) = "RE" Then
17                vCodPaacRepres = vRep!cod_repres
18                vTipo = "R"
              ElseIf UCase(Left(strEmail_From, 2)) = "GE" Then
                  vCodPaacRepres = vRep!cod_repres
                  vTipo = "G"
19            End If
20            vRep.Close
              
              'Abre arquivo
21            num_arq = FreeFile
22            Consulta = Path_drv & "\COMUNICA\OUT\PEDDICAS" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT"
23            Open Consulta For Output As #num_arq
              
24            Do While Not sDicas.EOF
25                Select Case sDicas!FL_TIPO
                      Case 1
26                        tipo = "I"
27                    Case 2
28                        tipo = "U"
29                    Case 3
30                        tipo = "D"
31                End Select
32                sequencia = Format(sDicas!sequencia, "000000000")
33                codcliente = Format(sDicas!cod_cliente, "000000")
34                dt_dica = Format(sDicas!dt_dica, "YYYYMMDDHHMMSS")
35                dt_alteracao = Format(sDicas!dt_alteracao, "YYYYMMDDHHMMSS")
36                cod_vend = Format(sDicas!cod_vend, "000000")
37                If UCase(sDicas!dica) <> "" Then
38                    dica = Replace(UCase(sDicas!dica), vbCrLf, " ")
39                Else
40                    dica = ""
41                End If

42                sDicas.MoveNext
43                Print #num_arq, tipo & sequencia & codcliente & dt_dica & dt_alteracao & cod_vend & dica
44            Loop
45            Close #num_arq
              
              'Gera arquivo HDR
46            Gerar_Dicas strEmail_To, strEmail_From, Format(vCodPaacRepres, "0000"), vTipo

47            If FileLen(Consulta) = 0 Then
48               Kill Consulta
49               Kill Path_drv & "\COMUNICA\OUT\PEDDICAS" & vTipo & Format(vCodPaacRepres, "0000") & ".HDR"
50            End If

51        End If
52        Set sDicas = Nothing
          
TrataErro:
53        If Err.Number = 94 Then
54            Resume Next
55        ElseIf Err.Number <> 0 Then
56            MsgBox "Rotina GeraDicas. Codigo erro: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & er1
57        End If
End Function

Private Function GeraVisitaPromotor() As Boolean
          
1         On Error GoTo Trata_Erro
          
          Dim vArq As Integer
          
2         GeraVisitaPromotor = False
          
3         Set dbaccess = OpenDatabase(strPath & "BASE_DPK.MDB")

4         SQL = "SELECT VISITA_PROMOTOR.CGC, VISITA_PROMOTOR.DT_VISITA_AGENDA, VISITA_PROMOTOR.COD_PROMOTOR, "
5         SQL = SQL & " VISITA_PROMOTOR.DT_RESULTADO, VISITA_PROMOTOR.COD_STATUS, VISITA_PROMOTOR.COMENTARIO_PROMOTOR"
6         SQL = SQL & " FROM DATAS LEFT JOIN VISITA_PROMOTOR ON DATAS.DT_REAL = VISITA_PROMOTOR.DT_RESULTADO;"
7         Set sVisitaPromotor = dbaccess.CreateDynaset(SQL)
          
8         vcount = sVisitaPromotor.RecordCount
9         FreeLocks
          
10        If sVisitaPromotor.EOF = False Then
11            GeraVisitaPromotor = True
12            Set vRep = dbaccess.OpenRecordset("Select cod_filial, cod_repres from Deposito")
			  'Login - RLIMA (Ci&T) Data - 15/06/2012
			  'Jira - RTI-137
			  'Descri��o - Definindo o de tipo de usu�rio igual a Gerente.
13            If UCase(Left(strEmail_From, 2)) = "PA" Then
14                vTipo = "P"
15                vCodPaacRepres = vRep!cod_filial
16            ElseIf UCase(Left(strEmail_From, 2)) = "RE" Then
17                vCodPaacRepres = vRep!cod_repres
18                vTipo = "R"
              ElseIf UCase(Left(strEmail_From, 2)) = "GE" Then
                  vCodPaacRepres = vRep!cod_repres
                  vTipo = "G"
19            End If
20            vRep.Close
21            Set vRep = Nothing
              
              'Abre arquivo
22            vArq = FreeFile
23            Consulta = Path_drv & "\COMUNICA\OUT\PEDVISIT" & vTipo & Format(vCodPaacRepres, "0000") & ".TXT"
24            Open Consulta For Output As #vArq
              
              
              Dim vCgc As String
              Dim vDtVisita As String
              Dim vCodPromotor As String
              Dim vDtResult As String
              Dim vCodStatus As String
              Dim vComenPromotor As String
              
25            Do While Not sVisitaPromotor.EOF
26                If IsNull(sVisitaPromotor!cgc) Then Exit Do
27                vCgc = Format(sVisitaPromotor!cgc, "00000000000000")
28                vDtVisita = Format(sVisitaPromotor!dt_visita_agenda, "YYYYMMDD")
29                vCodPromotor = Format(sVisitaPromotor!Cod_Promotor, "0000")
30                vDtResult = Format(sVisitaPromotor!Dt_resultado, "YYYYMMDD")
31                vCodStatus = Format(sVisitaPromotor!Cod_Status, "00")
32                vComenPromotor = "" & sVisitaPromotor!Comentario_Promotor

33                sVisitaPromotor.MoveNext
34                Print #vArq, vCgc & vDtVisita & vCodPromotor & vDtResult & vCodStatus & vComenPromotor
35            Loop
36            Close #vArq
              
              'Gera arquivo HDR
37            Gerar_Visita strEmail_To, strEmail_From, Format(vCodPaacRepres, "0000"), vTipo

              If FileLen(Consulta) = 0 Then
                 Kill Consulta
                 Kill "\COMUNICA\OUT\PEDVISIT" & vTipo & Format(vCodPaacRepres, "0000") & ".HDR"
              End If

38        End If
39        Set sVisitaPromotor = Nothing

Trata_Erro:
40        If Err.Number <> 0 Then
41            MsgBox "Sub GeraVisitaPromotor" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
42        End If
End Function


Private Sub sscmdIniciar_Click()
          Dim Guarda    'Verifica se existe DBF
          Dim Guarda2   'Verifica se existe MDB
          Dim Guarda3   'Verifica se existe TXT ou CON
          Dim Guarda4   'Verifica se existe ZIP
          Dim Guarda5   'Verifica se existe Carga.zip
          Dim Guarda6   'Verifica se existe Pedido para enviar
          Dim Guarda7   'Verifica os HDR referente aos arquivos a serem enviados
          
1         On Error GoTo Trata_Erro
          
2        lblMensagem = "As tarefas est�o sendo executadas. Aguarde..."
          
         '*********************
         'Faz o processo de busca de pedidos e consultas a serem enviadas
         
3         copiarDE = ""
4         copiarPARA = ""

         'Verifica se existe pedidos a serem enviados para o dep�sito
5         Guarda6 = Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\PED*.TXT"))
6         If Guarda6 <> "" Then
7             Do While Guarda6 <> ""
8                 copiarDE = Path_drv & "\COM\SAIDA\PEDIDOS\" & Guarda6
9                 copiarPARA = Path_drv & "\COMUNICA\OUT\" & Guarda6
10                FileCopy copiarDE, copiarPARA
11                Kill Path_drv & "\COM\SAIDA\PEDIDOS\" & Guarda6
                  
12                Nome = Mid(Guarda6, 1, InStr(Guarda6, ".") - 1)
13                Guarda7 = Nome & ".HDR"
14                copiarDE = Path_drv & "\COM\SAIDA\MSG\" & Guarda7
15                copiarPARA = Path_drv & "\COMUNICA\OUT\" & Guarda7
16                FileCopy copiarDE, copiarPARA
17                Kill Path_drv & "\COM\SAIDA\MSG\" & Guarda7
                  
18                Guarda6 = Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\PED*.TXT"))
19            Loop
20        End If
         'T�rmino dos pedidos
          
         'Verifica se existe consultas a serem enviados para o dep�sito
21        Guarda6 = Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\CON*.CON"))
22        If Guarda6 <> "" Then
23            Do While Guarda6 <> ""
24                copiarDE = Path_drv & "\COM\SAIDA\PEDIDOS\" & Guarda6
25                copiarPARA = Path_drv & "\COMUNICA\OUT\" & Guarda6
26                FileCopy copiarDE, copiarPARA
27                Kill Path_drv & "\COM\SAIDA\PEDIDOS\" & Guarda6
                  
28                Nome = Mid(Guarda6, 1, InStr(Guarda6, ".") - 1)
29                Guarda7 = Nome & ".HDR"
30                copiarDE = Path_drv & "\COM\SAIDA\MSG\" & Guarda7
31                copiarPARA = Path_drv & "\COMUNICA\OUT\" & Guarda7
32                FileCopy copiarDE, copiarPARA
33                Kill Path_drv & "\COM\SAIDA\MSG\" & Guarda7
                  
34                Guarda6 = Trim(Dir(Path_drv & "\COM\SAIDA\PEDIDOS\CON*.CON"))
35            Loop
36        End If
         'T�rmino das consultas
         
         'T�rmino da verifica��o de pedidos
         '**********************
         Set dbaccess = OpenDatabase(strPath & "base_dpk.MDB")
         dbaccess.Execute "DELETE FROM CONTROLE"
         dbaccess.Close
         Set dbaccess = Nothing
         
         '**********************
         'Faz a chamada do sistema do Marcos Ap. onde executa o envio e recebimento de mensagens
         'da/para a m�quina FTP
37        Shell (Path_drv & "\PGMS\FTPCLIEN.EXE /AUTO"), 1
         '**********************
38        End
         'Ap�s o t�rmino do FTPCLIENT ele ir� chamar o Fil050IN onde far� o tratamento
         'de tudo o que recebeu no Comunica\In da sua caixa do FTP

Trata_Erro:
39        If Err.Number <> 0 Then
40            MsgBox "Sub FrmPrincipal_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "C�digo:" & Err.Description & vbCrLf & "Linha: " & Erl
41        End If
             
End Sub

Private Sub sscmdSair_Click()
    End
End Sub

Sub Gerar_Controle(pTo As String, pFrom As String, pRepres As String, pTipo As String)
    
    Dim vArq As Integer
    
    vArq = FreeFile
         
    Open Path_drv & "\COMUNICA\OUT\PEDCONTR" & pTipo & pRepres & ".HDR" For Output As #vArq
            
    Print #vArq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
    Print #vArq, "TO        =" & pTo
    Print #vArq, "FROM      =" & pFrom
    Print #vArq, "DESCRICAO =" & "#PEDCONTR" & pTipo & pRepres
    Print #vArq, "FILE      =" & "PEDCONTR" & pTipo & pRepres & ".TXT"
            
    Close #vArq
    
End Sub

Sub Gerar_Dicas(pTo As String, pFrom As String, pRepres As String, pTipo As String)
    Dim vArq As Integer
    
    vArq = FreeFile
    Open Path_drv & "\COMUNICA\OUT\PEDDICAS" & pTipo & pRepres & ".HDR" For Output As #vArq
            
    Print #vArq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
    Print #vArq, "TO        =" & pTo
    Print #vArq, "FROM      =" & pFrom
    Print #vArq, "DESCRICAO =" & "#PEDDICAS" & pTipo & pRepres
    Print #vArq, "FILE      =" & "PEDDICAS" & pTipo & pRepres & ".TXT"
            
    Close #vArq
End Sub

Sub Gerar_Visita(pTo As String, pFrom As String, pRepres As String, pTipo As String)
    Dim vArq As Integer
    
    vArq = FreeFile
    Open Path_drv & "\COMUNICA\OUT\PEDVISIT" & pTipo & pRepres & ".HDR" For Output As #vArq
            
    Print #vArq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
    Print #vArq, "TO        =" & pTo
    Print #vArq, "FROM      =" & pFrom
    Print #vArq, "DESCRICAO =" & "#PEDVISIT" & pTipo & pRepres
    Print #vArq, "FILE      =" & "PEDVISIT" & pTipo & pRepres & ".TXT"
            
    Close #vArq
End Sub


Sub Gerar_HDR_Indice(pTo As String, pFrom As String, pRepres As String, pTipo As String)
    
    Dim vArq As Integer
    
    vArq = FreeFile
         
    Open Path_drv & "\COMUNICA\OUT\PEDINDIC" & pTipo & pRepres & ".HDR" For Output As #vArq
            
    Print #vArq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
    Print #vArq, "TO        =" & pTo
    Print #vArq, "FROM      =" & pFrom
    Print #vArq, "DESCRICAO =" & "#PEDINDIC" & pTipo & pRepres
    Print #vArq, "FILE      =" & "PEDINDIC" & pTipo & pRepres & ".TXT"
            
    Close #vArq
    
End Sub

Sub Registros(pRepres As String, pTipo As String)
    Dim vArq As Integer
       
    vArq = FreeFile
    
    Open Path_drv & "\COMUNICA\OUT\PEDCONTR" & pTipo & pRepres & ".TXT" For Append As #vArq
    
    Print #vArq, ""
    Print #vArq, "[TABELAS]"
    
    For i = 0 To dbaccess.TableDefs.count - 1
        If Left(UCase(dbaccess.TableDefs(i).Name), 4) <> "MSYS" Then
            Print #vArq, dbaccess.TableDefs(i).Name & " = " & dbaccess.TableDefs(i).RecordCount
        End If
    Next
    
    Close #vArq
    
End Sub

Sub Data_Ult_Atualizacao(pRepres As String, pTipo As String)
    Dim vArq As Integer
    Dim vUltAtu As Object
    
    vArq = FreeFile
    
    Open Path_drv & "\COMUNICA\OUT\PEDCONTR" & pTipo & pRepres & ".TXT" For Append As #vArq
    
    Print #vArq, ""
    Print #vArq, "[ATUALIZACAO]"
    
    Set vUltAtu = dbaccess.OpenRecordset("Select * from Datas")
    
    Print #vArq, vUltAtu!dt_faturamento
    
    Close #vArq
    
End Sub

Sub Registros_Diferenca(pFrom As String, pRepres As String, pTipo As String)
    Dim vArq As Integer
    Dim vUltAtu As Object
    
    vArq = FreeFile
    
    Open Path_drv & "\COMUNICA\OUT\PEDCONTR" & pTipo & pRepres & ".TXT" For Append As #vArq
    
    Print #vArq, ""
    Print #vArq, "[DIFERENCA]"
    
    Set vUltAtu = dbaccess.OpenRecordset("SELECT controle.NOME_TAB as NOME_TAB, First(controle.REG_ENV) AS REG_ENV, First(controle.REG_REC) AS REG_REC, First(controle.REG_BASE) AS REG_BASE From Controle GROUP BY controle.NOME_TAB ORDER BY controle.NOME_TAB")
    
    While vUltAtu.EOF = False
        If vUltAtu!reg_env <> vUltAtu!reg_rec Or vUltAtu!reg_env <> vUltAtu!reg_base Then
            Print #vArq, vUltAtu!nome_tab & " = " & vUltAtu!reg_env & ";" & vUltAtu!reg_rec & ";" & vUltAtu!reg_base
        End If
        vUltAtu.MoveNext
    Wend
    Close #vArq
    Set vUltAtu = Nothing
End Sub
