VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Begin VB.Form frmPrincipal 
   Caption         =   "FIL050IN"
   ClientHeight    =   3195
   ClientLeft      =   1470
   ClientTop       =   2280
   ClientWidth     =   5205
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmPrinc.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3195
   ScaleWidth      =   5205
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Tarefas a serem executadas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   1770
      Left            =   900
      TabIndex        =   0
      Top             =   60
      Width           =   3345
      Begin VB.CheckBox chkTabCompleta 
         Appearance      =   0  'Flat
         Caption         =   "Trocar Tabela Completa"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   210
         TabIndex        =   7
         Top             =   1200
         Width           =   3045
      End
      Begin VB.CheckBox chkCarga 
         Appearance      =   0  'Flat
         Caption         =   "Carga de Sistemas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   210
         TabIndex        =   3
         Top             =   900
         Width           =   2265
      End
      Begin VB.CheckBox chkRetorno 
         Appearance      =   0  'Flat
         Caption         =   "Retorno de Pedidos / Dicas"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   210
         TabIndex        =   2
         Top             =   630
         Width           =   3075
      End
      Begin VB.CheckBox chkAtualiza 
         Appearance      =   0  'Flat
         Caption         =   "Atualiza��o de Base Di�ria"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   210
         TabIndex        =   1
         Top             =   360
         Width           =   3075
      End
   End
   Begin VB.Data DataMDB 
      Connect         =   "Access"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   60
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   30
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.FileListBox File1 
      Height          =   870
      Left            =   60
      TabIndex        =   8
      Top             =   360
      Visible         =   0   'False
      Width           =   675
   End
   Begin MSComctlLib.StatusBar SSPanel 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   6
      Top             =   2820
      Width           =   5205
      _ExtentX        =   9181
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8652
         EndProperty
      EndProperty
   End
   Begin Bot�o.cmd sscmdIniciar 
      Height          =   495
      Left            =   900
      TabIndex        =   4
      Top             =   1950
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Iniciar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   49152
      FCOLO           =   49152
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrinc.frx":0442
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   60
      Top             =   1290
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin Bot�o.cmd sscmdSair 
      Height          =   495
      Left            =   3300
      TabIndex        =   5
      Top             =   1950
      Width           =   945
      _ExtentX        =   1667
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPrinc.frx":045E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblMensagem 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Atualizando Base, Por Favor Aguarde..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   285
      Left            =   180
      TabIndex        =   9
      Top             =   2490
      Visible         =   0   'False
      Width           =   4785
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPrincipal
' Author    : c.samuel.oliveira
' Date      : 10/02/16
' Purpose   : TI-3952
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmPrincipal
' Author    : C.SAMUEL.OLIVEIRA
' Date      : 13/10/15
' Purpose   : TI-3030
'---------------------------------------------------------------------------------------

Option Explicit

Private Const INFINITE = &HFFFF
Private Const SYNCHRONIZE = &H100000

Private Declare Sub WaitForSingleObject Lib "kernel32.dll" (ByVal hHandle As Long, ByVal dwMilliseconds As Long)
Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDA As Long, ByVal bIH As Integer, ByVal dwPID As Long) As Long
Private Declare Sub CloseHandle Lib "kernel32.dll" (ByVal hObject As Long)

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Public Sub RunCmd(CmdPath As String, _
   Optional WindowStyle As VbAppWinStyle = vbNormalFocus)

   Dim hProcess As Long

   On Error GoTo Err_RunCmd

   hProcess = OpenProcess(SYNCHRONIZE, 0, Shell(CmdPath, WindowStyle))

   If hProcess Then
       WaitForSingleObject hProcess, INFINITE
       CloseHandle hProcess
   End If

   Exit Sub

Err_RunCmd:

   Err.Clear

End Sub

Private Sub Form_Load()
   
    Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
   
    '--------------------------------------------
    '-- Variaveis de Lican�a para uso do BWZIP
    MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    '--------------------------------------------
   
    vPath = Pegar_Drive
    'vPath = "C:"
     
    frmPrincipal.WindowState = 0
    Screen.MousePointer = 11
    sscmdIniciar.Enabled = False
    sscmdSair.Enabled = False
        
   'Verifica as tarefas a serem executadas e apresenta ao usu�rio
    chkRetorno.Value = 0
    'chkRetornoDica.Value = 0
    chkAtualiza.Value = 0
    chkCarga.Value = 0
    chkTabCompleta.Value = 0
        
    If Trim(Dir(vPath & "\COMUNICA\IN\*.CON")) <> "" Or _
       Trim(Dir(vPath & "\COMUNICA\IN\*.TXT")) <> "" Then
        chkRetorno.Value = 1
        'chkRetornoDica.Value = 1
    End If
    
    If Trim(Dir(vPath & "\COMUNICA\IN\ATU*.ZIP")) <> "" Then
        chkAtualiza.Value = 1
    End If
    
    If Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP")) <> "" Then
        chkCarga.Value = 1
    End If
    
    If Trim(Dir(vPath & "\CARGA\*.DBF")) <> "" And Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP")) <> "" Then
        chkTabCompleta.Value = 1
    End If
    
    chkRetorno.Enabled = False
    'chkRetornoDica.Enabled = False
    chkAtualiza.Enabled = False
    chkCarga.Enabled = False
    chkTabCompleta.Enabled = False
    
    If chkRetorno.Value = 0 And chkAtualiza.Value = 0 And chkCarga.Value = 0 And chkTabCompleta.Value = 0 Then
        sscmdIniciar.Enabled = False
        sscmdSair.Enabled = True
    
        SSPanel.Panels(1) = "Clique no bot�o sair!"
        Screen.MousePointer = 0
    
    Else
        sscmdIniciar.Enabled = True
        sscmdSair.Enabled = False
    
        SSPanel.Panels(1) = "Clique no bot�o iniciar para executar as tarefas marcadas acima ou verificar se existem tarefas a receber!"
        Screen.MousePointer = 0
    End If
End Sub

Private Sub sscmdIniciar_Click()
    Dim Guarda    'Verifica se existe DBF
    Dim Guarda2   'Verifica se existe MDB
    Dim Guarda3   'Verifica se existe TXT ou CON
    Dim Guarda4   'Verifica se existe ZIP
    Dim Guarda5   'Verifica se existe Carga.zip
    Dim Guarda6   'Verifica se existe Pedido para enviar
    
    Dim vNomeComEntradaArquivo As String
    Dim Nome_Arquivo_Atu As String
    
    On Error GoTo trataerro
    
        
    SSPanel.Panels(1) = "As tarefas recebidas est�o sendo executadas. Aguarde..."
    
   '*********************
   'Voltou do FTPCLIENT e ir� fazer o processo de verifica��o e execu��o de tudo que veio
   'para o Comunica\In e estava na caixa do FTP
   
   'Verificar se existe carga de sistemas a ser executada e faz�-la
    Guarda5 = Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP"))
    If Guarda5 <> "" Then
        RunCmd vPath & "\carga\fil240.exe", 1
        If Guarda5 <> "" Then
            Kill vPath & "\COMUNICA\IN\" & Guarda5
        End If
    End If
   'T�rmino da etapa de carga de sistemas
     
   'Verificar se existe atualiza��o a ser executada e faz�-la
    Guarda4 = Trim(Dir(vPath & "\COMUNICA\IN\ATU*.ZIP"))
    If Guarda4 <> "" Then
    
        'Luciano Carlos de Jesus - 24-06-2008
        'Solicita��o de Romulo suporte DPK para deletar todas a atualiza��es ou DBF's que n�o foram feitas por algum
        'motivo.
        'Nesta altera��o primeiro vou verificar se o arquivo .ZIP se encontra no diret�rio \COM\ENTRADA\ARQUIVOS
        'Primeiro
        'Se existir verifico se o nome do arquivo � igual ao nome que esta vindo do diret�rio \COMUNICA\IN
        'Se for o mesmo ent�o deleto o que esta no diretorio \COM\ENTRADA\ARQUIVOS.
        
        'Segundo
        'Verifico se no diretorio \COM\ENTRADA\ARQUIVOS existem DBF's.
        'Se exitir ent�o deleto todos, para come�ar o processo normal.
        
        vNomeComEntradaArquivo = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\ATU*.ZIP"))
        If vNomeComEntradaArquivo <> "" Then
            If UCase(Guarda4) = UCase(vNomeComEntradaArquivo) Then
                MsgBox "Por algum motivo sua ATUALIZA��O DIARIA foi interrompida." & vbCrLf & _
                "O Processo de atualiza��o ser� reiniciado.", vbInformation, "Aten��o"
            
                Kill vPath & "\COM\ENTRADA\ARQUIVOS\" & vNomeComEntradaArquivo
            End If
            
            Do While Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF")) <> ""
                Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
                Kill vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda
                Guarda = ""
            Loop
        Else
            If Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF")) <> "" Then
                MsgBox "Por algum motivo sua ATUALIZA��O DIARIA foi interrompida." & vbCrLf & _
                "O Processo de atualiza��o ser� reiniciado.", vbInformation, "Aten��o"
                
                Do While Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF")) <> ""
                    Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
                    Kill vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda
                    Guarda = ""
                Loop
            End If
        End If
        Guarda = Empty

        Do While Guarda4 <> ""
            FileCopy vPath & "\comunica\in\" & Guarda4, vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda4
            Kill vPath & "\COMUNICA\IN\" & Guarda4
            Guarda4 = Trim(Dir(vPath & "\COMUNICA\IN\ATU*.ZIP"))
        Loop
           
       'Deletar todas as consultas e retornos de pedidos pois s�o anteriores � atualiza��o
        Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.TXT"))
        Do While Guarda3 <> ""
            Kill vPath & "\COMUNICA\IN\" & Guarda3
            Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.TXT"))
        Loop
        
        Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.CON"))
        Do While Guarda3 <> ""
            Kill vPath & "\COMUNICA\IN\" & Guarda3
            Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.CON"))
        Loop
        
        Do While Dir(vPath & "\COM\ENTRADA\ARQUIVOS\ATU*.ZIP") <> ""
            DoEvents
            
            'Nome do Arquivo de Atualiza��o Compactado que ser� Descompactado
            Nome_Arquivo_Atu = Dir(vPath & "\COM\ENTRADA\ARQUIVOS\ATU*.ZIP")
           
            'BWZIP
            MaqZip1.ZipUncompress vPath & "\com\entrada\arquivos\" & Nome_Arquivo_Atu, "*", vPath & "\com\entrada\arquivos\", "-o"
    
            Kill vPath & "\com\entrada\arquivos\" & Nome_Arquivo_Atu
    
            DoEvents
            Nome_Arquivo_Atu = ""
            
           'Verifica se existe DBF para continuar o processo pois s� assim se tem a certeza de que o
           'Unzipa j� terminou a descompacta��o.
            Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
            Do While Guarda = ""
                Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
            Loop
            
            If Guarda <> "" Then
               'Roda a convers�o de arquivos para colocar todos os DBF's dentro do Base_atu.mdb
                RunCmd vPath & "\PGMS\fil260.exe", 1
                DoEvents
                
               'Fica em loop at� sumir todos os DBF's pois s� ent�o a convers�o foi conclu�da.
                Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
                Do While Guarda <> ""
                    Guarda = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.DBF"))
                Loop
                
                'TI-3030
                Do While Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.TXT") <> ""
                    Guarda = Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.TXT")
                    If Dir(vPath & "\DADOS\" & Guarda) <> "" Then Kill vPath & "\DADOS\" & Guarda
                    FileCopy vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda, vPath & "\DADOS\" & Guarda
                    If Dir(vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda) <> "" Then Kill vPath & "\COM\ENTRADA\ARQUIVOS\" & Guarda
                Loop
                'FIM TI-3030
                
               'Se n�o tiver nenhum DBF e tiver um MDB continua o processo pois o Fil040
               'que � chamado dentro do Fil260 ainda n�o terminou o processo
                Guarda2 = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.MDB"))
                Do While Guarda2 <> ""
                    Guarda2 = Trim(Dir(vPath & "\COM\ENTRADA\ARQUIVOS\*.MDB"))
                Loop
                
                If Err <> 0 And Err <> 53 Then
                    MsgBox ("Erro ao encerrar o Sistema: " & Error$)
                    End
                End If
            End If
            DoEvents
        Loop
        Guarda4 = Trim(Dir(vPath & "\COMUNICA\IN\*.ZIP"))
        Do While Guarda4 <> ""
            Kill vPath & "\COMUNICA\IN\" & Guarda4
            Guarda4 = Trim(Dir(vPath & "\COMUNICA\IN\*.ZIP"))
        Loop
    End If
   'T�rmino da etapa de Atualiza��o

    CriarBancoMIX 'TI-3952
    
   'Verifica se existe consulta e retorno de pedidos para pegar e executa
    Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.TXT"))
    If Guarda3 <> "" Then
        Do While Guarda3 <> ""
            FileCopy vPath & "\comunica\in\" & Guarda3, vPath & "\COM\ENTRADA\PEDIDOS\" & Guarda3
            Kill vPath & "\comunica\in\" & Guarda3
            Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.TXT"))
        Loop
    End If
                                  
    Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.CON"))
    If Guarda3 <> "" Then
        Do While Guarda3 <> ""
            FileCopy vPath & "\comunica\in\" & Guarda3, vPath & "\COM\ENTRADA\PEDIDOS\" & Guarda3
            Kill vPath & "\comunica\in\" & Guarda3
            Guarda3 = Trim(Dir(vPath & "\COMUNICA\IN\*.CON"))
        Loop
    End If
   'Se existir arquivos para serem atualizados roda o fil040.exe
    If Trim(Dir(vPath & "\COM\ENTRADA\PEDIDOS\*.TXT")) <> "" Or _
       Trim(Dir(vPath & "\COM\ENTRADA\PEDIDOS\*.CON")) <> "" Then
        RunCmd vPath & "\pgms\fil040.EXE", 1
        DoEvents
    End If
   'T�rmino das consultas e retornos
   
   'Deleta todos os cabe�alhos (.HDR) que foram recebidos com os arquivos
    Do While Trim(Dir(vPath & "\COMUNICA\IN\*.HDR")) <> ""
        Kill vPath & "\COMUNICA\IN\*.HDR"
    Loop
    
        
    '************************************************************
    '30/08/2006
    'Verificar se existem arquivos .DBF no diretorio CARGA
    'Se existir mov�-los para a pasta vPath\TMP\GENERICA
    'Apagar do banco BASE_DPK os registros conforme o nome do DBF
    'Inserir nesta tabela vazia todos os registros do DBF
    '************************************************************
    If chkTabCompleta.Value = 1 Then
        lblMensagem.Visible = True
        Me.Refresh
        TabelaCompleta
        lblMensagem.Visible = False
        Me.Refresh
    End If
    '*************************************************************
    
    
    SSPanel.Panels(1) = "Todas as tarefas foram conclu�das. Clique no bot�o sair!"
    sscmdIniciar.Enabled = False
    sscmdSair.Enabled = True
    
    chkRetorno.Value = 0
    chkAtualiza.Value = 0
    chkCarga.Value = 0
    chkTabCompleta.Value = 0

    chkRetorno.Enabled = True
    chkAtualiza.Enabled = True
    chkCarga.Enabled = True
    chkTabCompleta.Enabled = True
    
Exit Sub

trataerro:
    If Err = 53 Then
        Resume Next
    End If
End Sub

Private Sub sscmdSair_Click()
    End
End Sub


Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "DPK": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function


Sub TabelaCompleta()
    
    On Error GoTo Trata_Erro
    
    Dim vNomeArqDbf As String
    Dim Rst As Recordset
    Dim RstMDB As Recordset
    Dim dbBASE_DPK As Database
    Dim vSql As String
    Dim vNomeTabela As String
    
    If Dir(vPath & "\TMP", vbDirectory) = "" Then
        ChDir vPath
        MkDir vPath & "\TMP"
    End If
    If Dir(vPath & "\TMP\GENERICA", vbDirectory) = "" Then
        MkDir vPath & "\TMP\GENERICA"
    End If

    Do While Dir(vPath & "\TMP\GENERICA\*.dbf", vbArchive) <> ""
        vNomeArqDbf = Dir(vPath & "\TMP\GENERICA\*.dbf")
        Kill vPath & "\TMP\GENERICA\" & vNomeArqDbf
    Loop

    'APAGA QUALQUER ARQUIVO DBT DO GENERICA ANTES DE QUALQUER A��O
    Do While Dir(vPath & "\TMP\GENERICA\*.DBT", vbArchive) <> ""
        vNomeArqDbf = Dir(vPath & "\TMP\GENERICA\*.DBT")
        Kill vPath & "\TMP\GENERICA\" & vNomeArqDbf
    Loop
    
    'TI-3030
    Do While Dir(vPath & "\TMP\GENERICA\*.TXT", vbArchive) <> ""
        vNomeArqDbf = Dir(vPath & "\TMP\GENERICA\*.TXT")
        Kill vPath & "\TMP\GENERICA\" & vNomeArqDbf
    Loop
    'FIM TI-3030
    
    Do While Dir(vPath & "\CARGA\*.DBF") <> ""
        vNomeArqDbf = Dir(vPath & "\CARGA\*.dbf")
        vNomeArqDbf = Mid(vNomeArqDbf, 1, InStr(vNomeArqDbf, ".") - 1)
        FileCopy vPath & "\CARGA\" & vNomeArqDbf & ".DBF", vPath & "\TMP\GENERICA\" & vNomeArqDbf & ".DBF"
        Kill vPath & "\CARGA\" & vNomeArqDbf & ".DBF"
    Loop

    Do While Dir(vPath & "\CARGA\*.DBT") <> ""
        vNomeArqDbf = Dir(vPath & "\CARGA\*.DBT")
        vNomeArqDbf = Mid(vNomeArqDbf, 1, InStr(vNomeArqDbf, ".") - 1)
        FileCopy vPath & "\CARGA\" & vNomeArqDbf & ".DBT", vPath & "\TMP\GENERICA\" & vNomeArqDbf & ".DBT"
        Kill vPath & "\CARGA\" & vNomeArqDbf & ".DBT"
    Loop
    
    'TI-3030
    Do While Dir(vPath & "\CARGA\*.TXT") <> ""
        vNomeArqDbf = Dir(vPath & "\CARGA\*.TXT")
        FileCopy vPath & "\CARGA\" & vNomeArqDbf, vPath & "\TMP\GENERICA\" & vNomeArqDbf
        If Dir(vPath & "\CARGA\" & vNomeArqDbf) <> "" Then Kill vPath & "\CARGA\" & vNomeArqDbf
        If Dir(vPath & "\DADOS\" & vNomeArqDbf) <> "" Then Kill vPath & "\DADOS\" & vNomeArqDbf
        FileCopy vPath & "\TMP\GENERICA\" & vNomeArqDbf, vPath & "\DADOS\" & vNomeArqDbf
    Loop
    'FIM TI-3030
    
    Me.Refresh

    Set dbBASE_DPK = OpenDatabase(vPath & "\DADOS\BASE_DPK.MDB", False, False)

    Do While Dir(vPath & "\TMP\GENERICA\*.dbf") <> ""
        vNomeArqDbf = Dir(vPath & "\TMP\GENERICA\*.dbf")
        vNomeArqDbf = Mid(vNomeArqDbf, 1, InStr(vNomeArqDbf, ".") - 1)
        vSql = ""
        Select Case vNomeArqDbf
            Case "ALIQUOTA" '1
                vNomeTabela = "ALIQUOTA_ME"
                vSql = "INSERT INTO ALIQUOTA_ME (COD_LOJA, CGC, PC_ICM) "
                vSql = vSql & " Select COD_LOJA, CGC, PC_ICM "
                vSql = vSql & " FROM ALIQUOTA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                Me.Refresh
            Case "DEPOSITO" '2.1
                vNomeTabela = "DEPOSITO"
                vSql = "INSERT INTO DEPOSITO(COD_LOJA, COD_FILIAL, COD_UF, COD_REPRES, FL_VDR) "
                vSql = vSql & " SELECT COD_LOJA, COD_FILIAL, COD_UF, COD_REPRES, FL_VDR "
                vSql = vSql & " FROM DEPOSITO "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "ANTECIPA" '2
                vNomeTabela = "ANTECIPACAO_TRIBUTARIA"
                vSql = "INSERT INTO antecipacao_tributaria(cod_uf_origem,cod_uf_destino,pc_margem_lucro) "
                vSql = vSql & " SELECT cod_uf_ori,cod_uf_des,pc_margem "
                vSql = vSql & " FROM ANTECIPA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "APLICACA" '3
                vNomeTabela = "APLICACAO"
                vSql = "INSERT into APLICACAO (COD_DPK, COD_CATEGORIA, COD_MONTADORA, SEQUENCIA, COD_ORIGINAL, DESC_APLICACAO) "
                vSql = vSql & " SELECT cod_dpk,cod_catego,cod_montad, sequencia,cod_origin,desc_aplic "
                vSql = vSql & " FROM APLICACA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "BANCO" '4
                vNomeTabela = "BANCO"
                vSql = "INSERT INTO banco(COD_BANCO,SIGLA)  "
                vSql = vSql & " SELECT cod_banco,sigla "
                vSql = vSql & " FROM banco "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CANCEL_P" '5
                vNomeTabela = "CANCEL_PEDNOTA"
                vSql = "INSERT INTO cancel_pednota(COD_CANCEL,DESC_CANCEL)"
                vSql = vSql & " SELECT cod_cancel,desc_cance "
                vSql = vSql & " FROM cancel_p"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CATEG_S" '6
                vNomeTabela = "CATEG_SINAL"
                vSql = "INSERT INTO categ_sinal (CATEGORIA, SINAL) "
                vSql = vSql & " SELECT categoria,sinal "
                vSql = vSql & " FROM categ_s "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CESTAI" '7
                vNomeTabela = "CESTA_ITEM"
                vSql = "INSERT INTO CESTA_ITEM (COD_LOJA, COD_DPK, TP_CESTA) "
                vSql = vSql & " SELECT COD_LOJA, COD_DPK, TP_CESTA "
                vSql = vSql & " FROM CESTAI "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CESTAV" '8
                vNomeTabela = "CESTA_VENDA"
                vSql = "INSERT INTO CESTA_VENDA (COD_LOJA, TP_CESTA, NOME_CESTA, DT_VIGENCIA) "
                vSql = vSql & " SELECT COD_LOJA, TP_CESTA, NOMECEST, DTVIGENC "
                vSql = vSql & " FROM CESTAV "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CIDADE" '9
                vNomeTabela = "CIDADE"
                vSql = "INSERT INTO cidade(cod_cidade,nome_cidade,tp_cidade,cod_uf,"
                vSql = vSql & " cod_regiao,cod_subregiao,caracteristica)"
                vSql = vSql & " SELECT cod_cidade,nome_cidad,tp_cidade,cod_uf,"
                vSql = vSql & " cod_regiao,cod_subreg,caracteris"
                vSql = vSql & " FROM cidade "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLASSANT" '10
                vNomeTabela = "CLASS_ANTEC_ENTRADA"
                vSql = "INSERT INTO CLASS_ANTEC_ENTRADA(class_fiscal,cod_uf_origem,cod_uf_destino,cod_trib, pc_margem_lucro) "
                vSql = vSql & " SELECT class_fisc,cod_uf_ori,cod_uf_des,cod_trib,pc_marg_lu "
                vSql = vSql & " FROM CLASSANT "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIE_CAR" '11
                vNomeTabela = "CLIENTE_CARACTERISTICA"
                vSql = "INSERT INTO CLIENTE_CARACTERISTICA (CARACTERISTICA, DESC_CARACTERISTICA, FL_DESC_AUTOMATICO, FL_RED_COMIS)"
                vSql = vSql & " SELECT CARACTERIS, DESC_CARAC, FL_DESC_AU, FL_RED_COM "
                vSql = vSql & " FROM CLIE_CAR "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIE_CRE" '12
                vNomeTabela = "CLIE_CREDITO"
                vSql = "INSERT INTO CLIE_CREDITO (COD_CLIENTE,DT_ULT_COMPRA,NOTA_CREDITO, LIMITE_CREDITO)"
                vSql = vSql & " SELECT COD_CLIENT, DT_ULT_COM, NOTA_CREDI, LIMITE_CRE "
                vSql = vSql & " FROM CLIE_CRE "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIE_END" '13
                vNomeTabela = "CLIE_ENDERECO"
                vSql = "INSERT INTO CLIE_ENDERECO (COD_CLIENTE,TP_ENDERECO,SEQUENCIA, ENDERECO, COD_CIDADE,BAIRRO, CEP, DDD, FONE, FAX, CXPOSTAL,TP_DOCTO, CGC, INSCR_ESTADUAL,NOME_CLIENTE )"
                vSql = vSql & " SELECT COD_CLIENT, TP_ENDEREC, SEQUENCIA, ENDERECO, COD_CIDADE, BAIRRO, CEP, DDD, FONE, FAX, CXPOSTAL, TP_DOCTO, CGC, INSCR_ESTA, NOME_CLIEN "
                vSql = vSql & " FROM CLIE_END "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIE_INT" '14
                vNomeTabela = "CLIENTE_INTERNET"
                vSql = "INSERT INTO CLIENTE_INTERNET (COD_CLIENTE,E_MAIL,HOMEPAGE)"
                vSql = vSql & " SELECT COD_CLIENT, E_MAIL, HOMEPAGE "
                vSql = vSql & " FROM CLIE_INT "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIE_MEN" '15
                vNomeTabela = "CLIE_MENSAGEM"
                vSql = "INSERT INTO clie_mensagem(COD_MENSAGEM,DESC_MENS,FL_BLOQUEIO)"
                vSql = vSql & " SELECT cod_mensag,desc_mens, fl_bloquei "
                vSql = vSql & " FROM clie_men "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
              Me.Refresh
            Case "CLIENTE" '16
                vNomeTabela = "CLIENTE"
                vSql = "INSERT INTO CLIENTE(COD_CLIENTE, CGC, NOME_CLIENTE, NOME_CONTATO, CLASSIFICACAO, ENDERECO, COD_CIDADE, BAIRRO, DDD1, FONE1, DDD2, FONE2, CEP, CXPOSTAL, TELEX,FAX, INSCR_ESTADUAL, INSCR_SUFRAMA, COD_TIPO_CLIENTE, DT_CADASTR, COD_TRANSP, COD_REPR_VEND,COD_BANCO, SITUACAO, COD_MENSAGEM, FL_CONS_FINAL, CARACTERISTICA, COD_MENSAGEM_FISCAL, TP_EMPRESA)"
                vSql = vSql & " SELECT COD_CLIENT, CGC, NOME_CLIEN, NOME_CONTA, CLASSIFICA, ENDERECO, COD_CIDADE, BAIRRO, DDD1, FONE1, DDD2, FONE2, CEP, CXPOSTAL, TELEX,FAX, INSCR_ESTA, INSCR_SUFR, COD_TIPO_C, DT_CADASTR, COD_TRANSP, COD_REPR_V,COD_BANCO, SITUACAO, COD_MENSAG, FL_CONS_FI, CARACTERIS, COD_MENS_F, TP_EMPRESA "
                vSql = vSql & " FROM CLIENTE "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "CLIEACUM" '17
                vNomeTabela = "CLIENTE_ACUMULADO"
                vSql = "INSERT INTO CLIENTE_ACUMULADO(COD_CLIENTE, ANO_MES1, VALOR1, ANO_MES2, VALOR2, ANO_MES3, VALOR3, ANO_MES4, VALOR4)"
                vSql = vSql & " SELECT COD_CLIENT, ANO_MES1, VALOR1, ANO_MES2, VALOR2, ANO_MES3, VALOR3, ANO_MES4, VALOR4 "
                vSql = vSql & " FROM CLIEACUM "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "DATAS" '18
                vNomeTabela = "DATAS"
                vSql = "INSERT INTO datas(DT_REAL,DT_FATURAMENTO,DT_INI_FECH_MENSAL,DT_FIN_FECH_MENSAL,"
                vSql = vSql & "           DT_FERIADO_1,DT_FERIADO_2,FL_LIBER,DT_FORA_SEMANA,DT_INI_FECH_DIA)"
                vSql = vSql & " SELECT   DT_REAL, DT_FATURAM, DT_INI_FEC, DT_FIN_FEC, DT_FERI_1, "
                vSql = vSql & "          DT_FERI_2,FL_LIBER,DT_FORA_SE,DT_INI_FD"
                vSql = vSql & " FROM datas "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "DEP_VISA" '19
                vNomeTabela = "DEPOSITO_VISAO"
                vSql = "INSERT INTO DEPOSITO_VISAO(NOME_PROGRAMA, COD_LOJA)"
                vSql = vSql & " SELECT NOME_PROGR, COD_LOJA "
                vSql = vSql & " FROM DEP_VISA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "DOLAR_DI" '20
                vNomeTabela = "DOLAR_DIARIO"
                vSql = "INSERT INTO dolar_diario(DATA_USS,VALOR_USS)"
                vSql = vSql & " SELECT data_uss,valor_uss"
                vSql = vSql & " FROM dolar_di"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "DUPLICAT" '21
                vNomeTabela = "DUPLICATAS"
                vSql = "INSERT INTO DUPLICATAS(COD_LOJA, NUM_FATURA, NUM_ORDEM, SITUACAO_PAGTO, TP_DUPLICATA, COD_CLIENTE, COD_BANCO, TP_PAGAMENTO, DT_VENCIMENTO, VL_ABERTO, NUM_DUPL_BANCO) "
                vSql = vSql & " SELECT COD_LOJA, NUM_FATURA, NUM_ORDEM, SITUACAO_P, TP_DUPLICA, COD_CLIENT, COD_BANCO, TP_PAGAMEN, DT_VENCIME, VL_ABERTO, NUM_DUPL_B "
                vSql = vSql & " FROM DUPLICAT "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "EMBALAD" '22
                vNomeTabela = "EMBALADOR"
                vSql = "insert into EMBALADOR(COD_EMBAL,NOME_EMBAL)"
                vSql = vSql & " SELECT cod_embal,nome_embal "
                vSql = vSql & " FROM EMBALAD "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FILIAL" '23
                vNomeTabela = "FILIAL"
                vSql = "INSERT INTO filial(COD_FILIAL, SIGLA,TP_FILIAL,COD_FRANQUEADOR,COD_REGIONAL)"
                vSql = vSql & " SELECT cod_filial,sigla,tp_filial,cod_franqu,cod_region"
                vSql = vSql & " FROM Filial "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FORNECED" '24
                vNomeTabela = "FORNECEDOR"
                vSql = "INSERT INTO fornecedor(COD_FORNECEDOR,SIGLA,DIVISAO,CLASSIFICACAO,SITUACAO)"
                vSql = vSql & " SELECT cod_fornec,sigla,divisao,classifica,situacao"
                vSql = vSql & " FROM forneced "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FORN_ESP" '25
                vNomeTabela = "FORNECEDOR_ESPECIFICO"
                vSql = "INSERT INTO fornecedor_especifico(COD_FORNECEDOR,COD_GRUPO,COD_SUBGRUPO,COD_DPK, "
                vSql = vSql & " fl_dif_icms,fl_adicional,TP_DIF_ICMS) "
                vSql = vSql & " SELECT cod_fornec,cod_grupo, cod_subgru, cod_dpk, fl_dif_icm, "
                vSql = vSql & " fl_adicion,tp_dif_icm "
                vSql = vSql & " FROM forn_esp "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FRETE_EN" '26
                vNomeTabela = "FRETE_ENTREGA"
                vSql = "insert into FRETE_ENTREGA(COD_LOJA, COD_TRANSP, COD_UF, COD_REGIAO, COD_SUBREGIAO, COD_CIDADE, TP_FRETE, VL_FRETE_ENTREGA, SITUACAO)"
                vSql = vSql & " SELECT COD_LOJA, COD_TRANSP, COD_UF, COD_REGIAO, COD_SUBREG, COD_CIDADE, TP_FRETE, VL_FRETE_E, SITUACAO "
                vSql = vSql & " FROM FRETE_EN "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FRETE_UF" '27
                vNomeTabela = "FRETE_UF"
                vSql = "insert into frete_uf(COD_LOJA, COD_UF, COD_REGIAO, COD_SUBREGIAO, COD_CIDADE, VL_FRETE)"
                vSql = vSql & " SELECT COD_LOJA, COD_UF, COD_REGIAO, COD_SUBREG, COD_CIDADE, VL_FRETE "
                vSql = vSql & " FROM FRETE_UF "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FRETEUFB" '28
                vNomeTabela = "FRETE_UF_BLOQ"
                vSql = "insert into frete_uf_bloq(COD_UF,COD_TRANSP,FL_BLOQUEIO)"
                vSql = vSql & " SELECT cod_uf, cod_transp, fl_bloquei "
                vSql = vSql & " FROM freteufb "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "FRETEUFT" '29
                vNomeTabela = "FRETE_UF_TRANSP"
                vSql = "insert into frete_uf_TRANSP(COD_LOJA,COD_TRANSP,COD_UF, VL_FRETE)"
                vSql = vSql & " SELECT COD_LOJA, COD_TRANSP, COD_UF, VL_FRETE "
                vSql = vSql & " FROM FRETEUFT "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "GRUPO" '30
                vNomeTabela = "GRUPO"
                vSql = "INSERT INTO grupo(COD_GRUPO,DESC_GRUPO)"
                vSql = vSql & " SELECT cod_grupo,desc_grupo"
                vSql = vSql & " FROM grupo"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "ITEM_ANA" '31
                vNomeTabela = "ITEM_ANALITICO"
                vSql = "INSERT INTO ITEM_ANALITICO (COD_LOJA, COD_DPK, CATEGORIA, CLASS_ABC, CLASS_ABCF, CLASS_VENDA, QTD_MES_ANT, CUSTO_MEDIO, CUSTO_MEDIO_ANT ) "
                vSql = vSql & " SELECT COD_LOJA, COD_DPK, CATEGORIA, CLASS_ABC, CLASS_ABCF, CLASS_VEND, QTD_MES_AN, CUSTO_MEDI, CUSTO_MED_ "
                vSql = vSql & " FROM ITEM_ANA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
              Me.Refresh
            Case "ITEM_CAD" '32
                vNomeTabela = "ITEM_CADASTRO"
                vSql = "insert into item_cadastro(cod_dpk,cod_fornecedor,cod_fabrica,desc_item,"
                vSql = vSql & "                   cod_linha,cod_grupo,cod_subgrupo,dt_cadastramento,"
                vSql = vSql & "                   mascarado,class_fiscal,peso,volume,cod_unidade,"
                vSql = vSql & "                   cod_tributacao,cod_tributacao_ipi,pc_ipi,cod_procedencia,"
                vSql = vSql & "                   cod_dpk_ant,qtd_minforn,qtd_minvda)"
                vSql = vSql & " SELECT cod_dpk,cod_fornec,cod_fabric,desc_item,"
                vSql = vSql & "        cod_linha,cod_grupo,cod_subgru,dt_cadastr,"
                vSql = vSql & "        mascarado,class_fisc,peso,volume,cod_unidad,"
                vSql = vSql & "        cod_tribut,cod_trib_i,pc_ipi,cod_proced,"
                vSql = vSql & "        cod_dpk_an,qtd_minfor,qtd_minvda"
                vSql = vSql & " FROM item_cad "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
             Case "ITEM_EST" '33
                vNomeTabela = "ITEM_ESTOQUE"
                vSql = "INSERT INTO ITEM_ESTOQUE (COD_LOJA, COD_DPK, QTD_ATUAL, QTD_RESERV, QTD_PENDENTE, QTD_MAXVDA, CUEMA, DT_CUEMA, VL_ULT_COMPRA, DT_ULT_COMPRA, SITUACAO) "
                vSql = vSql & " SELECT COD_LOJA, COD_DPK, QTD_ATUAL, QTD_RESERV, QTD_PENDEN, QTD_MAXVDA, CUEMA, DT_CUEMA, VL_ULT_COM, DT_ULT_COM, SITUACAO "
                vSql = vSql & " FROM ITEM_EST"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "ITEM_GLO" '34
                vNomeTabela = "ITEM_GLOBAL"
                vSql = "INSERT INTO item_global(COD_DPK,SEQUENCIA_TAB)"
                vSql = vSql & " SELECT cod_dpk,sequencia"
                vSql = vSql & " FROM item_glo"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "ITEM_PRE" '35
                vNomeTabela = "ITEM_PRECO"
                vSql = "INSERT INTO ITEM_PRECO(COD_LOJA, COD_DPK, PRECO_VENDA, PRECO_VENDA_ANT, PRECO_OF, PRECO_OF_ANT, PRECO_SP, PRECO_SP_ANT)"
                vSql = vSql & " SELECT COD_LOJA, COD_DPK, PRECO_VEND, PRECO_V_AN, PRECO_OF, PRECO_O_AN, PRECO_SP, PRECO_S_AN "
                vSql = vSql & " FROM ITEM_PRE "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "ITPEDNOT" '36
                vNomeTabela = "itpednota_venda"
                vSql = "INSERT INTO itpednota_venda(COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_ITEM_PEDIDO, COD_LOJA_NOTA, NUM_NOTA, NUM_ITEM_NOTA, COD_DPK, QTD_SOLICITADA, QTD_ATENDIDA, PRECO_UNITARIO, TABELA_VENDA, PC_DESC1, PC_DESC2, PC_DESC3, PC_DIFICM, PC_IPI, PC_COMISS, PC_COMISSTLMK, COD_TRIB, COD_TRIBIPI, SITUACAO)"
                vSql = vSql & " SELECT COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_ITEM_P, COD_LOJA_N, NUM_NOTA, NUM_ITEM_N, COD_DPK, QTD_SOLICI, QTD_ATENDI, PRECO_UNIT, TABELA_VEN, PC_DESC1, PC_DESC2, PC_DESC3, PC_DIFICM, PC_IPI, PC_COMISS, PC_COMISST, COD_TRIB, COD_TRIBIP, SITUACAO "
                vSql = vSql & " FROM ITPEDNOT "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "LOJA" '37
                vNomeTabela = "LOJA"
                vSql = "insert into LOJA(COD_LOJA , CGC, RAZAO_SOCIAL, NOME_FANTASIA, ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, DT_CADASTR, INSCR_ESTADUAL, REPART_FISCAL)"
                vSql = vSql & " SELECT COD_LOJA, CGC, RAZAO_SOCI, NOME_FANTA, ENDERECO, BAIRRO, COD_CIDADE,CEP, DDD, FONE, FAX, DT_CADASTR, INSCR_ESTA, REPART_FIS "
                vSql = vSql & " FROM LOJA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "MONTADOR" '38
                vNomeTabela = "MONTADORA"
                vSql = "insert into montadora(COD_MONTADORA,DESC_MONTADORA)"
                vSql = vSql & " SELECT cod_montad,desc_monta"
                vSql = vSql & " FROM montador "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "NATU_OPE" '39
                vNomeTabela = "NATUREZA_OPERACAO"
                vSql = "insert into natureza_operacao(cod_natureza,desc_natureza,desc_abreviada,cod_cfo_dest1, "
                vSql = vSql & "                   cod_cfo_dest2,cod_cfo_dest3, cod_cfo_oper, cod_transac,"
                vSql = vSql & "                   cod_trib_icms1, cod_trib_icms2 ,cod_trib_icms3,  "
                vSql = vSql & "                   cod_trib_icms4,cod_trib_icms5,cod_trib_icms6,cod_trib_ipi1,"
                vSql = vSql & "                   cod_trib_ipi2, cod_trib_ipi3,cod_trib_ipi4 , cod_mensagem1,  "
                vSql = vSql & "                   cod_mensagem2,fl_livro, fl_cuema,fl_estoque,fl_estorno ,"
                vSql = vSql & "                   fl_tributacao,fl_fornecedor,fL_vlcontabil,"
                vSql = vSql & "                   fl_nfitem,fl_quantidade , fl_cod_merc,fl_pcdesc, "
                vSql = vSql & "                   fl_contas_pagar, fl_tipo_nota,fl_centro_custo, fl_contabil, "
                vSql = vSql & "                   fl_base_red_icms,fl_consistencia, fl_ipi_incid_icm,fl_movimentacao)"
                vSql = vSql & " SELECT cod_nature , desc_natur, desc_abrev,cod_dest1, "
                vSql = vSql & "        cod_dest2,cod_dest3, cod_cfo_op,cod_transa, "
                vSql = vSql & "        cod_icms1, cod_icms2 ,cod_icms3,cod_icms4,  "
                vSql = vSql & "        cod_icms5, cod_icms6,cod_ipi1,cod_ipi2, cod_ipi3,  "
                vSql = vSql & "        cod_ipi4, cod_mens1,cod_mens2, fl_livro, "
                vSql = vSql & "        fl_cuema, fl_estoque,fl_estorno,fl_tributa,"
                vSql = vSql & "        fl_fornece,FL_vlconta,FL_nfitem,fl_quantid,  "
                vSql = vSql & "        fl_cod_mer,fl_pcdesc, fl_contas_,fl_tipo_no,  "
                vSql = vSql & "        fl_centro_,fl_contabi,fl_base_re,fl_consist, "
                vSql = vSql & "        fl_ipi_inc,fl_movimen "
                vSql = vSql & " FROM natu_ope"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "PEDNOTA" '40
                vNomeTabela = "PEDNOTA_VENDA"
                vSql = "INSERT INTO PEDNOTA_VENDA(COD_LOJA , NUM_PEDIDO, SEQ_PEDIDO, COD_LOJA_NOTA, NUM_NOTA, NUM_PENDENTE, COD_FILIAL, TP_PEDIDO, TP_DPKBLAU, "
                vSql = vSql & "TP_TRANSACAO , DT_DIGITACAO, DT_PEDIDO, DT_EMISSAO_NOTA, DT_SSM, DT_BLOQ_POLITICA, DT_BLOQ_CREDITO, DT_BLOQ_FRETE,"
                vSql = vSql & "COD_NOPE , COD_CLIENTE, COD_FORNECEDOR, COD_END_ENTREGA, COD_END_COBRANCA, COD_TRANSP, COD_REPRES, COD_VEND,"
                vSql = vSql & "COD_PLANO , COD_BANCO, COD_CFO_DEST, COD_CFO_OPER, FRETE_PAGO, PESO_BRUTO, QTD_SSM, QTD_ITEM_PEDIDO, QTD_ITEM_NOTA,"
                vSql = vSql & "VL_CONTABIL , VL_IPI, VL_BASEICM1, VL_BASEICM2, VL_BASE_1, VL_BASE_2, VL_BASE_3, VL_BASEISEN, VL_BASE_5, VL_BASE_6,"
                vSql = vSql & "VL_BASEOUTR , VL_BASEMAJ, VL_ICMRETIDO, VL_BASEIPI, VL_BISENIPI, VL_BOUTRIPI, VL_FRETE, VL_DESP_ACESS, PC_DESCONTO,"
                vSql = vSql & "PC_DESC_SUFRAMA , PC_ACRESCIMO, PC_SEGURO, PC_ICM1, PC_ICM2, PC_ALIQ_INTERNA, COD_CANCEL, FL_GER_SSM, FL_GER_NFIS,"
                vSql = vSql & "FL_PENDENCIA , FL_DESP_ACESS, FL_DIF_ICM, SITUACAO, BLOQ_PROTESTO, BLOQ_JUROS, BLOQ_CHEQUE, BLOQ_COMPRA, BLOQ_DUPLICATA,"
                vSql = vSql & "BLOQ_LIMITE , BLOQ_SALDO, BLOQ_CLIE_NOVO, BLOQ_DESCONTO, BLOQ_ACRESCIMO, BLOQ_VLFATMIN, BLOQ_ITEM_DESC1, BLOQ_ITEM_DESC2, BLOQ_ITEM_DESC3, BLOQ_FRETE, MENS_PEDIDO, MENS_NOTA, COD_VDR)"
                vSql = vSql & " SELECT COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, COD_LOJA_N, NUM_NOTA, NUM_PENDEN, COD_FILIAL, TP_PEDIDO, TP_DPKBLAU, TP_TRANSAC, "
                vSql = vSql & " DT_DIGITAC, DT_PEDIDO, DT_EMISSAO,DT_SSM, DT_POLITIC, DT_CREDITO, DT_FRETE, COD_NOPE, COD_CLIENT, COD_FORNEC, COD_ENTREG, "
                vSql = vSql & " COD_COBRAN, COD_TRANSP, COD_REPRES, COD_VEND, COD_PLANO, COD_BANCO, COD_DEST, COD_OPER, FRETE_PAGO, PESO_BRUTO, QTD_SSM, "
                vSql = vSql & " QTD_PEDIDO, QTD_NOTA, VL_CONTABI, VL_IPI, VL_ICM1, VL_ICM2, VL_BASE_1, VL_BASE_2, VL_BASE_3, VL_BASEISE, VL_BASE_5, VL_BASE_6, "
                vSql = vSql & " VL_BASEOUT, VL_BASEMAJ, VL_ICMRETI, VL_BASEIPI, VL_BISENIP, VL_BOUTRIP, VL_FRETE, VL_DESP_AC, PC_DESCONT, PC_DESC_SU, PC_ACRESCI, "
                vSql = vSql & " PC_SEGURO, PC_ICM1, PC_ICM2, PC_ALIQ_IN, COD_CANCEL, FL_SSM, FL_NFIS, FL_PENDENC, FL_DESP_AC, "
                vSql = vSql & " FL_DIF_ICM, SITUACAO, BLOQ_PROTE, BLOQ_JUROS, BLOQ_CHEQU, BLOQ_COMPR, BLOQ_DUPLI, BLOQ_LIMIT, BLOQ_SALDO, BLOQ_CLIE_, BLOQ_DESCO, BLOQ_ACRES, BLOQ_VLFAT, BLOQ_DESC1, BLOQ_DESC2, BLOQ_DESC3, BLOQ_FRETE, MENS_PEDID, MENS_NOTA, COD_VDR "
                vSql = vSql & " FROM PEDNOTA "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "PLANO_PG" '41
                vNomeTabela = "PLANO_PGTO"
                vSql = "INSERT INTO plano_pgto(cod_plano,desc_plano,prazo_medio,pc_acres_fin_dpk,"
                vSql = vSql & "                pc_desc_fin_dpk,situacao,fl_avista, "
                vSql = vSql & "                pc_desc_fin_vdr,pc_acres_fin_blau)"
                vSql = vSql & " SELECT COD_PLANO , DESC_PLANO, PRAZO_MEDI, PC_ACRES_F, PC_DESC_FI, "
                vSql = vSql & " SITUACAO,FL_AVISTA,PC_D_VDR,PC_A_BLA"
                vSql = vSql & " FROM PLANO_PG"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_CLIE_R" '42
                vNomeTabela = "R_CLIE_REPRES"
                vSql = "INSERT INTO R_CLIE_REPRES(COD_CLIENTE, COD_REPRES)  "
                vSql = vSql & " SELECT  COD_CLIENT, COD_REPRES "
                vSql = vSql & " FROM R_CLIE_R "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "RED_BASE" '43
                vNomeTabela = "CLASS_FISCAL_RED_BASE"
                vSql = "INSERT INTO CLASS_FISCAL_RED_BASE(COD_UF_ORIGEM, COD_UF_DESTINO, CLASS_FISCAL)  "
                vSql = vSql & " SELECT COD_UF_ORI, COD_UF_DES, CLASS_FISC "
                vSql = vSql & " FROM RED_BASE "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_DPK_EQ" '44
                vNomeTabela = "R_DPK_EQUIV"
                vSql = "INSERT into R_DPK_EQUIV(COD_DPK,COD_DPK_EQ) "
                vSql = vSql & " SELECT cod_dpk,cod_dpk_eq "
                vSql = vSql & " FROM R_DPK_EQ "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_FABRIC" '45
                vNomeTabela = "R_FABRICA_DPK"
                vSql = "INSERT INTO R_FABRICA_DPK(COD_FABRICA, COD_DPK, FL_TIPO, DT_CADASTRO, SIGLA, COD_FORNECEDOR)"
                vSql = vSql & " SELECT COD_FABRIC, COD_DPK, FL_TIPO, DT_CADASTR, SIGLA, COD_FORNEC"
                vSql = vSql & " FROM R_FABRIC"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_FILDEP" '46
                vNomeTabela = "R_FILDEP"
                vSql = "insert into R_FILDEP(COD_LOJA,COD_FILIAL)"
                vSql = vSql & " SELECT COD_LOJA,COD_FILIAL "
                vSql = vSql & " FROM r_FILDEP "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_PED_CO" '47
                vNomeTabela = "R_PEDIDO_CONF"
                vSql = "insert into R_PEDIDO_CONF(COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_CAIXA, COD_CONFERENTE, COD_EMBALADOR, COD_EXPEDIDOR, FL_PEND_ETIQ, QTD_VOLUME_PARC)"
                vSql = vSql & " SELECT COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_CAIXA, COD_CONFER, COD_EMBALA, COD_EXPEDI, FL_PEND_ET, QTD_VOLUME "
                vSql = vSql & " FROM R_PED_CO"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_REPCGC" '47
                vNomeTabela = "R_REPCGC"
                vSql = "insert into R_REPCGC(COD_REPRES,CGC)"
                vSql = vSql & " SELECT COD_REPRES, CGC "
                vSql = vSql & " FROM R_REPCGC"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_REPVEN" '48
                vNomeTabela = "R_REPVEN"
                vSql = "insert into R_REPVEN(COD_REPRES,COD_VEND)"
                vSql = vSql & " SELECT COD_REPRES, COD_VEND "
                vSql = vSql & " FROM R_REPVEN "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "R_UF_DEP" '49
                vNomeTabela = "R_UF_DEPOSITO"
                vSql = "INSERT INTO R_UF_DEPOSITO(COD_UF, COD_LOJA)"
                vSql = vSql & " SELECT cod_uf,cod_loja"
                vSql = vSql & " FROM r_uf_dep "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "REP_ENDC" '50
                vNomeTabela = "REPR_END_CORRESP"
                vSql = "INSERT INTO REPR_END_CORRESP(COD_REPRES, ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, CELULAR, CXPOSTAL)"
                vSql = vSql & " SELECT COD_REPRES, ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, CELULAR, CXPOSTAL "
                vSql = vSql & " FROM REP_ENDC "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "REPRESEN" '51
                vNomeTabela = "REPRESENTACAO"
                vSql = "INSERT INTO REPRESENTACAO(CGC, RAZAO_SOCIAL, NUM_CORI, ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, CELULAR, CXPOSTAL)"
                vSql = vSql & " SELECT CGC, RAZAO_SOCI, NUM_CORI,ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, CELULAR, CXPOSTAL "
                vSql = vSql & " FROM REPRESEN "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "REPRES" '52
                vNomeTabela = "REPRESENTANTE"
                vSql = "INSERT INTO REPRESENTANTE(cic , cod_repres, nome_repres, pseudonimo, endereco, bairro, cod_cidade, cep, ddd, fone, fax, ramal, cod_filial, cic_gerente, situacao, tipo, seq_franquia, dt_cadastr, dt_desligamento)"
                vSql = vSql & " SELECT CIC, COD_REPRES, NOME_REPRE, PSEUDONIMO, ENDERECO, BAIRRO, COD_CIDADE, CEP, DDD, FONE, FAX, RAMAL, COD_FILIAL, CIC_GERENT, SITUACAO, TIPO, SEQ_FRANQU, DT_CADASTR, DT_DESLIGA "
                vSql = vSql & " FROM REPRES "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "RESULTAD" '53
                vNomeTabela = "RESULTADO"
                vSql = "INSERT INTO resultado(cod_resultado, desc_resultado) "
                vSql = vSql & " SELECT cod_result, desc_resul "
                vSql = vSql & " FROM RESULTAD "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                Me.Refresh
            Case "ROMANEIO" '54
                vNomeTabela = "ROMANEIO"
                vSql = "INSERT INTO ROMANEIO(COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, DT_COLETA, DT_DESPACHO, NUM_ROMANEIO, NUM_CONHECIMENTO, NUM_CARRO, QTD_VOLUME ) "
                vSql = vSql & " SELECT COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, DT_COLETA,DT_DESPACH, NUM_ROMANE, NUM_CONHEC, NUM_CARRO, QTD_VOLUME "
                vSql = vSql & " FROM ROMANEIO "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                Me.Refresh
            Case "SALDO_PE" '55
                vNomeTabela = "SALDO_PEDIDOS"
                vSql = "insert into saldo_pedidos(cod_loja,cod_cliente,saldo_pedidos)"
                vSql = vSql & " SELECT cod_loja,cod_client,saldo_pedi "
                vSql = vSql & " FROM saldo_pe"
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                Me.Refresh
            Case "SUBGRUPO" '56
                vNomeTabela = "SUBGRUPO"
                vSql = "insert into subgrupo(cod_grupo,cod_subgrupo,desc_subgrupo)"
                vSql = vSql & " SELECT cod_grupo,cod_subgru,desc_subgr"
                vSql = vSql & " FROM subgrupo "
                vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "SUBST_TR" '57
                  vNomeTabela = "SUBST_TRIBUTARIA"
                  vSql = "INSERT INTO subst_tributaria(class_fiscal,cod_uf_origem,cod_uf_destino,"
                  vSql = vSql & "                      cod_trib_revendedor,cod_trib_inscrito, "
                  vSql = vSql & "                      cod_trib_isento,fator_revendedor,"
                  vSql = vSql & "                      fator_inscrito,fator_isento,fl_cred_suspenso,"
                  vSql = vSql & "                      pc_margem_lucro, FL_RED_ICMS)"
                  vSql = vSql & " SELECT class_fisc,cod_origem,cod_destin,"
                  vSql = vSql & "        cod_revend,cod_inscri,cod_isento,"
                  vSql = vSql & "        fator_reve,fator_insc,fator_isen,"
                  vSql = vSql & "        fl_cred_su,pc_margem_,FL_RED_ICM"
                  vSql = vSql & " FROM subst_tr"
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TABELAD" '58
                  vNomeTabela = "TABELA_DESCPER"
                  vSql = "insert into tabela_descper(sequencia,tp_tabela,ocorr_preco,cod_filial,cod_fornecedor,"
                  vSql = vSql & "                    cod_grupo,cod_subgrupo,cod_dpk,situacao,pc_desc_periodo1,"
                  vSql = vSql & "                    pc_desc_periodo2,pc_desc_periodo3) "
                  vSql = vSql & " SELECT sequencia,tp_tabela,ocorr_prec,cod_filial,cod_fornec,"
                  vSql = vSql & "        cod_grupo,cod_subgru,cod_dpk,situacao,pc_desc1,"
                  vSql = vSql & "        pc_desc2,pc_desc3"
                  vSql = vSql & " FROM tabelad "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TABELAV" '59
                  vNomeTabela = "TABELA_VENDA"
                  vSql = "insert into tabela_venda(tabela_venda,tp_tabela,ocorr_preco,dt_vigencia1,dt_vigencia2,"
                  vSql = vSql & "                  dt_vigencia3,dt_vigencia4,dt_vigencia5)"
                  vSql = vSql & " SELECT tabela_ven,tp_tabela,ocorr_prec,dt_vig1,"
                  vSql = vSql & "        dt_vig2,dt_vig3,dt_vig4,dt_vig5"
                  vSql = vSql & " FROM tabelav "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TAXA" '60
                  vNomeTabela = "TAXA"
                  vSql = "insert into TAXA(COD_LOJA, VALOR_2, VALOR_6, VALOR_7, DATA_1, DATA_2, DATA_3, DATA_4, DATA_5, VALOR_13)"
                  vSql = vSql & " SELECT COD_LOJA, VALOR_2, VALOR_6, VALOR_7, DATA_1,DATA_2,DATA_3, DATA_4, DATA_5, VALOR_13 "
                  vSql = vSql & " FROM TAXA "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TIPO_CLI" '61
                  vNomeTabela = "TIPO_CLIENTE"
                  vSql = "insert into tipo_cliente(cod_tipo_cli,desc_tipo_cli,cod_segmento)"
                  vSql = vSql & " SELECT cod_tipo_c,desc_tipo,cod_segmen"
                  vSql = vSql & " FROM tipo_cli "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TP_BLAU" '62
                  vNomeTabela = "TIPO_CLIENTE_BLAU"
                  vSql = "insert into tipo_cliente_blau (cod_tipo_cli,desc_tipo_cli)"
                  vSql = vSql & " SELECT cod_tipo_c,desc_tipo_"
                  vSql = vSql & " FROM tp_blau "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TRANSPOR" '63
                  vNomeTabela = "TRANSPORTADORA"
                  vSql = "insert into transportadora(cod_transp,nome_transp,cod_cidade,situacao) "
                  vSql = vSql & " SELECT cod_transp,nome_trans,cod_cidade,situacao"
                  vSql = vSql & " FROM transpor"
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF" '64
                  vNomeTabela = "UF"
                  vSql = "insert into uf(cod_uf,desc_uf,dt_aliq_interna)"
                  vSql = vSql & " SELECT cod_uf,desc_uf,dt_aliq_in"
                  vSql = vSql & " FROM uf "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF_CATEG" '65
                vNomeTabela = "UF_CATEG"
                  vSql = "insert into uf_categ(cod_uf,categoria,pc_desc_adic,pc_adic_maximo,pc_desc_comis,"
                  vSql = vSql & "              desc_comis_maximo, caracteristica )  "
                  vSql = vSql & " SELECT cod_uf,categoria, pc_desc_ad, pc_adic_ma,pc_desc_co, desc_comis, caracteris "
                  vSql = vSql & " FROM uf_categ "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF_DEP" '66
                  vNomeTabela = "UF_DEPOSITO"
                  vSql = "insert into UF_DEPOSITO(COD_LOJA, COD_UF_DESTINO, PC_FRETE, INSCR_ESTADUAL_ST)"
                  vSql = vSql & " SELECT COD_LOJA, COD_UF_DES, PC_FRETE, INSCR_ESTA "
                  vSql = vSql & " FROM UF_DEP "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF_DPK" '67
                  vNomeTabela = "UF_DPK"
                  vSql = "insert into uf_dpk(cod_uf_origem,cod_uf_destino,cod_dpk,pc_desc)"
                  vSql = vSql & " SELECT cod_uf_ori,cod_uf_des,cod_dpk,pc_desc"
                  vSql = vSql & " FROM uf_dpk "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF_OR_DE" '68
                  vNomeTabela = "UF_ORIGEM_DESTINO"
                  vSql = "insert into uf_origem_destino(cod_uf_origem,cod_uf_destino,pc_icm,pc_dificm,PC_ICM_GERENCIAL)"
                  vSql = vSql & " SELECT cod_uf_ori,cod_uf_des,pc_icm,pc_dificm, PC_ICM_GER"
                  vSql = vSql & " FROM uf_or_de"
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "UF_TPCLI" '69
                  vNomeTabela = "UF_TPCLIENTE"
                  vSql = "insert into uf_tpcliente(cod_uf_origem,cod_uf_destino,tp_cliente,cod_tributacao,"
                  vSql = vSql & "                  pc_desc,cod_tributacao_final,pc_desc_diesel) "
                  vSql = vSql & " SELECT cod_origem, cod_destin, tp_cliente, cod_tribut,"
                  vSql = vSql & "        pc_desc, cod_final,pc_desc_di "
                  vSql = vSql & " from uf_tpcli"
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VCLIFIEL" '70
                  vNomeTabela = "V_CLIENTE_FIEL"
                  vSql = "INSERT INTO V_CLIENTE_FIEL(COD_CLIENTE, TP_FIEL, DESC_FIEL)  "
                  vSql = vSql & " SELECT COD_CLIENT, TP_FIEL, DESC_FIEL "
                  vSql = vSql & " FROM VCLIFIEL "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "V_PEDLIQ" '71
                  vNomeTabela = "v_pedliq_venda"
                  vSql = "INSERT INTO v_pedliq_venda(COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_ITEM_PEDIDO, SITUACAO, PR_LIQUIDO, VL_LIQUIDO, PR_LIQUIDO_SUFRAMA, VL_LIQUIDO_SUFRAMA)  "
                  vSql = vSql & " SELECT COD_LOJA, NUM_PEDIDO, SEQ_PEDIDO, NUM_ITEM_P, SITUACAO, PR_LIQUIDO, VL_LIQUIDO, PR_SUFRAMA, VL_SUFRAMA "
                  vSql = vSql & " FROM V_PEDLIQ "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VLIMITAD" '72
                  vNomeTabela = "VENDA_LIMITADA"
                  vSql = "INSERT INTO VENDA_LIMITADA(COD_UF,TP_CLIENTE,COD_LOJA_OBRIGATORIA)  "
                  vSql = vSql & " SELECT cod_uf,tp_cliente,cod_loja_o "
                  vSql = vSql & " FROM vlimitad "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh       'VDR
            Case "VDR_CLIE" '73
                  vNomeTabela = "VDR_CLIENTE_CATEG_VDR"
                  vSql = "INSERT INTO VDR_CLIENTE_CATEG_VDR(COD_CLIENTE, CATEGORIA)  "
                  vSql = vSql & " SELECT COD_CLIENT, CATEGORIA "
                  vSql = vSql & " FROM VDR_CLIE "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VDR_CONT" '74
                  vNomeTabela = "VDR_CONTROLE_VDR"
                  vSql = "INSERT INTO VDR_CONTROLE_VDR(COD_FORNECEDOR, COD_GRUPO, COD_SUBGRUPO, COD_TIPO_CLIENTE, COD_VDR, LINHA_PRODUTO)  "
                  vSql = vSql & " SELECT COD_FORN, COD_GRUP, COD_SUBG, COD_TIPO, COD_VDR, LINHA_PR  "
                  vSql = vSql & " FROM VDR_CONT "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VDR_DESC" '75
                  vNomeTabela = "VDR_DESCONTO_CATEG"
                  vSql = "INSERT INTO VDR_DESCONTO_CATEG(CATEGORIA, PC_DESC)  "
                  vSql = vSql & " SELECT CATEGORI, PC_DESC "
                  vSql = vSql & " FROM VDR_DESC "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VDR_ITEM" '76
                  vNomeTabela = "VDR_ITEM_PRECO"
                  vSql = "INSERT INTO VDR_ITEM_PRECO(COD_LOJA, COD_DPK, PRECO_VENDA, PRECO_VENDA_ANT )  "
                  vSql = vSql & " SELECT COD_LOJA, COD_DPK, PRECO_VE, PRECO_AN "
                  vSql = vSql & " FROM VDR_ITEM "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VDR_RCLI" '77
                  vNomeTabela = "VDR_R_CLIE_LINHA_PRODUTO"
                  vSql = "INSERT INTO VDR_R_CLIE_LINHA_PRODUTO(COD_CLIENTE, LINHA_PRODUTO)  "
                  vSql = vSql & " SELECT COD_CLIE, LINHA_PR "
                  vSql = vSql & " FROM VDR_RCLI "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "VDR_TABE" '78
                  vNomeTabela = "VDR_TABELA_VENDA"
                  vSql = "INSERT INTO VDR_TABELA_VENDA(DIVISAO, TP_TABELA, OCORR_PRECO, TABELA_VENDA, DT_VIGENCIA1, DT_VIGENCIA2, DT_VIGENCIA3, DT_VIGENCIA4, DT_VIGENCIA5 ) "
                  vSql = vSql & " SELECT DIVISAO, TP_TABEL, OCORR_PR, TABELA_V, DT_VIGE1, DT_VIGE2, DT_VIGE3, DT_VIGE4, DT_VIGE5 "
                  vSql = vSql & " FROM VDR_TABE "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            Case "TRANSFER" '79
                  vNomeTabela = "TRANSFERENCIA"
                  vSql = "INSERT INTO TRANSFERENCIA(COD_DEP_ORIGEM, COD_DEP_DESTINO) "
                  vSql = vSql & " SELECT DEP_ORIG,DEP_DEST "
                  vSql = vSql & " FROM TRANSFER "
                  vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                  Me.Refresh
            '07/07/2008 - rotina para quadrante
            Case "RCLI_QUA" '80
                 vNomeTabela = "R_CLIENTE_QUADRANTE"
                 If Dir(vPath & "\TMP\GENERICA\RCLI_QUA.DBF") <> "" Then
                    vSql = "INSERT INTO R_CLIENTE_QUADRANTE(COD_CLIE, ANO_MES, COD_QUAD) "
                    vSql = vSql & " SELECT COD_CLIE, ANO_MES, COD_QUAD FROM RCLI_QUAD "
                    vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                 End If
            Case "RACA_QUA" '81
                 vNomeTabela = "R_ACAO_QUADRANTE"
                 If Dir(vPath & "\TMP\GENERICA\RACA_QUA.DBF") <> "" Then
                    vSql = "INSERT INTO R_ACAO_QUADRANTE(COD_LOJA, COD_QUAD, TIPO, SEQ, NUM_PRIO, DES_ACAO) "
                    vSql = vSql & " SELECT COD_LOJA, COD_QUAD, 0 & TIPO, SEQ, NUM_PRIO, DES_ACAO FROM RACA_QUA"
                    vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                 End If
            Case "DICAS" '82
                 vNomeTabela = "DICAS"
                 If Dir(vPath & "\TMP\GENERICA\DICAS.DBF") <> "" Then
                    vSql = "INSERT INTO DICAS(SEQUENCIA, COD_CLIENTE, DT_DICA, COD_VEND, DICA) "
                    vSql = vSql & " SELECT SEQ, COD_CLI, DT_DICA, COD_VEND, DICA FROM DICAS"
                    vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                 End If
            
            '22/07/2008 - Rotina para Status_Visita promotor
            Case "STAT_VIS" '83
                 vNomeTabela = "STATUS_VISITA"
                 If Dir(vPath & "\TMP\GENERICA\STAT_VIS.DBF") <> "" Then
                    vSql = "INSERT INTO STATUS_VISITA(COD_STATUS, STATUS) "
                    vSql = vSql & " SELECT COD_STAT, STATUS FROM STAT_VIS"
                    vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
                 End If
            '23/07/2008 - Rotina para Visita promotor
            'Case "VIS_PROM" '84
            '     vNomeTabela = "VISITA_PROMOTOR"
            '     If Dir(vPath & "\TMP\GENERICA\VIS_PROM.DBF") <> "" Then
            '        vSql = "INSERT INTO VISITA_PROMOTOR(CGC, DT_VISITA_AGENDA, COD_PROMOTOR, DT_RESULTADO, COD_STATUS, COMENTARIO_GERENTE, COMENTARIO_PROMOTOR) "
            '        vSql = vSql & " SELECT CGC, DT_VISIT, COD_PROM, DT_RESUL, COD_STAT, COM_GERE, COM_PROM FROM VIS_PROM"
            '        vSql = vSql & " IN '" & vPath & "\TMP\GENERICA' 'dBASE IV;'"
            '     End If
            Case Else
                GoTo Final
        End Select
        
        If vSql <> "" Then
            SSPanel.Panels(1) = vNomeTabela
            dbBASE_DPK.Execute "DELETE FROM " & vNomeTabela
            DoEvents
            dbBASE_DPK.Execute vSql
            DoEvents
            Me.Refresh
Final:
            Kill vPath & "\TMP\GENERICA\" & vNomeArqDbf & ".DBF"
            If Dir(vPath & "\TMP\GENERICA\" & vNomeArqDbf & ".DBT") <> "" Then
                Kill vPath & "\TMP\GENERICA\" & vNomeArqDbf & ".DBT"
            End If
            Me.Refresh
        End If

        DoEvents
        Me.Refresh
        DoEvents
    Loop
    
    dbBASE_DPK.Close
    Set dbBASE_DPK = Nothing
    
    SSPanel.Panels(1) = "Aguarde... Compactando Base..."

    If Dir(vPath & "\DADOS\BASE_DPK1.MDB") <> "" Then
       Kill vPath & "\DADOS\BASE_DPK1.MDB"
    End If

    Me.Refresh

    DBEngine.CompactDatabase vPath & "\DADOS\BASE_DPK.MDB", vPath & "\DADOS\BASE_DPK1.MDB"
    Kill vPath & "\DADOS\BASE_DPK.MDB"
    Name vPath & "\DADOS\BASE_DPK1.MDB" As vPath & "\DADOS\BASE_DPK.MDB"
    
    CriarBancoMIX 'TI-3952
    
Trata_Erro:
    If Err.Number = 3078 Then
        Resume Next
    ElseIf Err.Number = 3192 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub TabelaCompleta" & vbCrLf & "Tabela: " & vNomeTabela & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

'TI-3952
Private Sub CriarBancoMIX()

On Error GoTo Trata_Erro
Dim Banco As Database
Dim NomeBanco As String
Dim SQL As String
Dim F As Long, sLine As String, A(0 To 4) As String
Dim G As Long, rLine As String, B(0 To 2) As String
Dim rs As Recordset

    '1) Criando o banco de dados
    NomeBanco = "MIX.MDB"
    If Dir(vPath & "\DADOS\MIX_PRODUTO.TXT") = "" Then Exit Sub
    If Dir(vPath & "\DADOS\MIX_ITEM.TXT") = "" Then Exit Sub
    If Dir(vPath & "\TMP\GENERICA\" & NomeBanco) <> "" Then Kill vPath & "\TMP\GENERICA\" & NomeBanco
    Set Banco = CreateDatabase(vPath & "\TMP\GENERICA\" & NomeBanco, dbLangGeneral)
    
    '2) Criando a Tabela:
    SQL = "CREATE TABLE MIX_Produto "
    SQL = SQL & " (NUM_MIX LONG,COD_LOJA INTEGER,DT_INICIAL DATETIME,DT_FINAL DATETIME,DESCRICAO TEXT(30),CONSTRAINT PK_MIX_PRODUTO PRIMARY KEY(NUM_MIX));"
    Banco.Execute SQL
    
    SQL = "CREATE TABLE MIX_Item "
    SQL = SQL & " (NUM_MIX LONG,COD_DPK LONG,QTD_DPK LONG,CONSTRAINT PK_MIX_ITEM PRIMARY KEY(NUM_MIX,COD_DPK));"
    Banco.Execute SQL
    
    Banco.Close

    '3) Carregando tabelas com os arquivo txt
    Set Banco = DBEngine(0).OpenDatabase(vPath & "\TMP\GENERICA\" & NomeBanco)
    F = FreeFile
    Open vPath & "\DADOS\MIX_PRODUTO.TXT" For Input As F
    
    Set rs = Banco.OpenRecordset("MIX_Produto", dbOpenTable)
    
    Do While Not EOF(F)
      Line Input #F, sLine
      ParseToArray sLine, A()
      rs.AddNew
        rs(0) = Val(A(1))
        rs(1) = Val(A(0))
        rs(2) = IIf(IsDate(A(2)), A(2), Format(CDate(Now), "DD/MM/YYYY"))
        rs(3) = IIf(IsDate(A(3)), A(3), Format(CDate(Now), "DD/MM/YYYY"))
        rs(4) = A(4)
      rs.Update
    Loop
    
    Close #F
    rs.Close

    '/////////////////////////////////////////////////////////////////////
    G = FreeFile
    Open vPath & "\DADOS\MIX_ITEM.TXT" For Input As G
    
    Set rs = Banco.OpenRecordset("MIX_Item", dbOpenTable)
    
    Do While Not EOF(G)
      Line Input #G, rLine
      ParseToArray rLine, B()
      rs.AddNew
        rs(0) = Val(B(0))
        rs(1) = Val(B(1))
        rs(2) = Val(B(2))
      rs.Update
    Loop
    
    Close #G
    
    rs.Close
    Banco.Close

    '4)move o banco para a pasta DADOS, depois apaga da temp
    If Dir(vPath & "\DADOS\" & NomeBanco) <> "" Then Kill vPath & "\DADOS\" & NomeBanco
    FileCopy vPath & "\TMP\GENERICA\" & NomeBanco, vPath & "\DADOS\" & NomeBanco
    If Dir(vPath & "\TMP\GENERICA\" & NomeBanco) <> "" Then Kill vPath & "\TMP\GENERICA\" & NomeBanco
    
    Exit Sub

Trata_Erro:
    If Err.Number = 3078 Then
        Resume Next
    ElseIf Err.Number = 3192 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub CriarBancoMIX" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub


Sub ParseToArray(sLine As String, A() As String)

Dim P As Long, LastPos As Long, I As Long

    P = InStr(sLine, ",")
    
    Do While P
        A(I) = Mid$(sLine, LastPos + 1, P - LastPos - 1)
        LastPos = P
        I = I + 1
        P = InStr(LastPos + 1, sLine, ",", vbBinaryCompare)
    Loop
    
    A(I) = Mid$(sLine, LastPos + 1)

End Sub
'FIM TI-3952
