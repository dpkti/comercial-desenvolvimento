VERSION 4.00
Begin VB.Form frmLog 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Log de An�lise OP"
   ClientHeight    =   2115
   ClientLeft      =   1185
   ClientTop       =   1515
   ClientWidth     =   7005
   Height          =   2520
   Left            =   1125
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2115
   ScaleWidth      =   7005
   Top             =   1170
   Width           =   7125
   Begin VB.TextBox txtNF 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5400
      MaxLength       =   9
      TabIndex        =   3
      Top             =   360
      Width           =   1455
   End
   Begin VB.ComboBox cboDeposito 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   1440
      TabIndex        =   1
      Top             =   360
      Width           =   2295
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   495
      Left            =   3480
      TabIndex        =   5
      Top             =   1080
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "Sair"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   1560
      TabIndex        =   4
      Top             =   1080
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "Consultar"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "NF/PEDIDO-SEQ"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   3840
      TabIndex        =   2
      Top             =   360
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "DEP�SITO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   1095
   End
End
Attribute VB_Name = "frmLog"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim ss As Object
  Dim i As Long
  Dim SQL As String
  
  
  If cboDeposito.ListCount = 0 Then
    SQL = "Select to_char(cod_loja,'0000')||'-'"
    SQL = SQL & "||nome_fantasia loja"
    SQL = SQL & " From loja"
    SQL = SQL & " Where cod_loja not in (3,6)"
    SQL = SQL & " Order by cod_loja"
  
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
    If ss.EOF And ss.BOF Then
      MsgBox "Falha na consulta de loja", vbInformation, "Aten��o"
      Exit Sub
    End If
  
    For i = 1 To ss.RecordCount
      cboDeposito.AddItem ss!loja
      ss.MoveNext
    Next
  End If
End Sub


Private Sub SSCommand1_Click()
  Dim ss As Snapshot
  Dim SQL As String
  Dim i As Long

  If cboDeposito = "" Then
    MsgBox "Para executar a consulta � preciso escolher um dep�sito", vbExclamation, "Aten��o"
    Exit Sub
  ElseIf txtNF = "" Or txtNF = "0" Then
    MsgBox "Para executar a consulta � preciso escolher uma NF", vbExclamation, "Aten��o"
    Exit Sub
  End If

  'montar comando para atualizar oracle
    SQL = "Select data, tp_altera��o, usuario"
    SQL = SQL & " From controle"
    SQL = SQL & " Where num_nota = " & CLng(txtNF)
    SQL = SQL & " and cod_loja = " & CLng(Mid(Trim(cboDeposito), 1, 4))
    
    Set ss = dbAccess.CreateSnapshot(SQL)
    FreeLocks
    
      
    If ss.EOF And ss.BOF Then
      MsgBox "ESTA NF N�O PASSOU PELA AN�LISE DE OP", vbInformation, "ATEN��O"
      Exit Sub
    End If
    
    ss.MoveLast
    ss.MoveFirst
    
    For i = 1 To ss.RecordCount
      MsgBox ss!TP_ALTERA��O & _
             " == DATA: " & ss!Data & _
             " == FEITA PELO USU�RIO: " & ss!usuario & _
             "     ", vbInformation, "ATEN��O"
      
      ss.MoveNext
    Next
    
    ss.Close
    
End Sub

Private Sub SSCommand2_Click()
  Unload Me
End Sub


Private Sub txtNF_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


