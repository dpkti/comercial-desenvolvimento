VERSION 4.00
Begin VB.Form frmManut 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Manuten��o / Cancelamento de Pedido"
   ClientHeight    =   4230
   ClientLeft      =   390
   ClientTop       =   2040
   ClientWidth     =   9000
   Height          =   4635
   Left            =   330
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4230
   ScaleWidth      =   9000
   Top             =   1695
   Width           =   9120
   Begin VB.CommandButton Command2 
      Caption         =   "&Limpa Tela"
      Height          =   375
      Left            =   4440
      TabIndex        =   4
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   6120
      TabIndex        =   5
      Top             =   3480
      Width           =   1215
   End
   Begin VB.TextBox txtNrpedido 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5160
      MaxLength       =   7
      TabIndex        =   0
      Top             =   360
      Width           =   975
   End
   Begin VB.TextBox txtSequencia 
      Alignment       =   2  'Center
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   6240
      MaxLength       =   1
      TabIndex        =   1
      Top             =   360
      Width           =   255
   End
   Begin VB.ComboBox cboDeposito 
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   1560
      TabIndex        =   6
      Top             =   360
      Width           =   2055
   End
   Begin VB.CommandButton Command5 
      Caption         =   "&Manuten��o"
      Height          =   375
      Left            =   1200
      TabIndex        =   2
      Top             =   3480
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Cancelar Pedido"
      Height          =   375
      Left            =   2760
      TabIndex        =   3
      Top             =   3480
      Width           =   1455
   End
   Begin VB.Label lblMsg 
      AutoSize        =   -1  'True
      ForeColor       =   &H00800000&
      Height          =   435
      Left            =   3120
      TabIndex        =   22
      Top             =   2640
      Width           =   405
   End
   Begin VB.Label lblCod_Cliente 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   21
      Top             =   960
      Width           =   855
   End
   Begin VB.Label lblNome_Cliente 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2520
      TabIndex        =   20
      Top             =   960
      Width           =   3975
   End
   Begin VB.Label lblCidade 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   19
      Top             =   1320
      Width           =   2760
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      BorderWidth     =   2
      X1              =   4440
      X2              =   4560
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Label lblUf 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4680
      TabIndex        =   18
      Top             =   1320
      Width           =   390
   End
   Begin VB.Label Label5 
      Caption         =   "Filial"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   6600
      TabIndex        =   17
      Top             =   960
      Width           =   525
   End
   Begin VB.Label lblFilial 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7200
      TabIndex        =   16
      Top             =   960
      Width           =   1455
   End
   Begin VB.Label lblPedido 
      Caption         =   "Nr.Pedido"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4080
      TabIndex        =   15
      Top             =   360
      Width           =   855
   End
   Begin VB.Label lblDeposito 
      Caption         =   "Dep�sito"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   360
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "Valor Cont�bil"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   240
      TabIndex        =   13
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label lblVl_Contabil 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   1560
      TabIndex        =   12
      Top             =   2280
      Width           =   1695
   End
   Begin VB.Label lblQtdItens 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4920
      TabIndex        =   11
      Top             =   2280
      Width           =   495
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      Caption         =   "Qtde  Itens"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Index           =   10
      Left            =   3720
      TabIndex        =   10
      Top             =   2280
      Width           =   960
   End
   Begin VB.Label Label7 
      Caption         =   "Natureza OP."
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   9
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label lblNatureza 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   8
      Top             =   1800
      Width           =   495
   End
   Begin VB.Label lblDados 
      AutoSize        =   -1  'True
      Caption         =   "Cliente"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   960
      Width           =   600
   End
End
Attribute VB_Name = "frmManut"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub cboDeposito_Click()
  cboDeposito.DataChanged = True
End Sub


Private Sub cboDeposito_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboDeposito_LostFocus()
  If cboDeposito.Text = "" Then
    MsgBox "Selecione um dep�sito", vbExclamation, "Aten��o"
    cboDeposito.SetFocus
    Exit Sub
  End If
  
  If cboDeposito.DataChanged Then
    Call txtSEQUENCIA_LostFocus
  End If
  
End Sub


Private Sub cmdSair_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
Dim response As String
   
On Error GoTo TrataErro

   If txtNrpedido.Text = "" Then
     MsgBox "Selecione um Pedido para Cancelar", vbInformation, "Aten��o"
     Init_tela
     Exit Sub
   End If
   
   If txtSEQUENCIA.Text = "" Then
     Call txtSEQUENCIA_LostFocus
   End If

  If dn.EOF Then
    Exit Sub
  End If
  If CLng(dn!cod_filial) = CLng(Mid(FILIAL_PED, 1, 3)) Or CLng(dn!cod_loja) = CLng(Mid(deposito_default, 1, 3)) Then

    If dn!situacao = "9" Then
      MsgBox "O pedido j� est� cancelado", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf dn!situacao = "7" Then
      MsgBox "Pedido com Problema de Finaliza��o", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf dn!fl_ger_nfis = "S" Then
      MsgBox "Pedido com Nota Fiscal j� emitida", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf (dn!situacao = "0" Or dn!situacao = "8") And dn!fl_ger_nfis = "N" Then
      response = MsgBox("Confirma Cancelamento do Pedido ?", vbYesNo, "Aten��o")
      If response = vbNo Then
         Exit Sub
      End If
      frmCancela.Show vbModal
      If cod_cancel = "0" Then
        Exit Sub
      Else
           'Dispara procedure no Oracle
            SQL = "BEGIN PR_CANCELA (:loja,:NUM,:SEQ,9,:cod_cancel);"
      SQL = SQL & " COMMIT;"
      SQL = SQL & "EXCEPTION"
      SQL = SQL & " WHEN OTHERS THEN"
      SQL = SQL & " ROLLBACK;"
      SQL = SQL & " :cod_errora := SQLCODE;"
      SQL = SQL & " :txt_errora := SQLERRM;"
      SQL = SQL & " END;"
    
      oradatabase.Parameters.Remove "loja"
      oradatabase.Parameters.Add "loja", Mid(cboDeposito.Text, 1, 3), 1
      oradatabase.Parameters.Remove "num"
      oradatabase.Parameters.Add "num", txtNrpedido.Text, 1
      oradatabase.Parameters.Remove "SEQ"
      oradatabase.Parameters.Add "SEQ", txtSEQUENCIA.Text, 1
      oradatabase.Parameters.Remove "cod_cancel"
      oradatabase.Parameters.Add "cod_cancel", cod_cancel, 1
      oradatabase.Parameters.Remove "cod_errora"
      oradatabase.Parameters.Add "cod_errora", 0, 2
      oradatabase.Parameters.Remove "txt_errora"
      oradatabase.Parameters.Add "txt_errora", "", 2
    
    
       oradatabase.ExecuteSQL SQL
     
       If oradatabase.Parameters("cod_errora") <> 0 Then
          MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
          Screen.MousePointer = vbDefault
          Exit Sub
       Else
               SQL = "delete *from pedido"
         SQL = SQL & " where num_pendente = " & txtNrpedido.Text
         SQL = SQL & " and seq_pedido = " & txtSEQUENCIA.Text
         SQL = SQL & " and cod_loja = " & Mid(cboDeposito.Text, 1, 3)
   
         dbAccess.Execute SQL, dbFailOnError
         FreeLocks
         
         MsgBox "Cancelamento Realizado", vbInformation, "Aten��o"
         Init_tela
       End If
        
      End If
      
    End If
  
  Else
    MsgBox "Pedido n�o pertence a esta filial ou dep�sito", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
    
  Exit Sub
    
TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
  

End Sub

Private Sub Command2_Click()
  Call Init_tela
End Sub

Private Sub Command5_Click()
  
  On Error GoTo TrataErro
  
  Dim preco_liquido As Double
  Dim TRIB As Byte


  If txtNrpedido.Text = "" Then
    MsgBox "Selecione um Pedido para colocar em Manuten��o", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
  
  If txtSEQUENCIA.Text = "" Then
     Call txtSEQUENCIA_LostFocus
   End If
  
  If dn.EOF Then
    Exit Sub
  End If
  
  If CLng(dn!cod_filial) = CLng(Mid(FILIAL_PED, 1, 3)) Or CLng(dn!cod_loja) = CLng(Mid(deposito_default, 1, 3)) Then

    If dn!situacao = "9" Then
      MsgBox "O pedido est� cancelado", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf dn!situacao = "8" Then
      MsgBox "O pedido j� est� em Manuten��o", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf dn!situacao = "7" Then
      MsgBox "Pedido com Problema de Finaliza��o", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    ElseIf dn!fl_ger_ssm = "S" Then
      MsgBox "Pedido com SSM j� emitida", vbInformation, "Aten��o"
      Init_tela
      Exit Sub
    End If

    SQL = "delete *from pedido"
    SQL = SQL & " where num_pendente = " & txtNrpedido.Text
    SQL = SQL & " and seq_pedido = " & txtSEQUENCIA.Text
    SQL = SQL & " and cod_loja = " & Mid(cboDeposito.Text, 1, 3)
     
    
    lblMsg = "Deletando Pedido no Access .."
    dbAccess.Execute SQL, dbFailOnError
    FreeLocks
    
    
  
    SQL = "insert into pedido("
    SQL = SQL & "num_pendente,seq_pedido,cod_loja,num_pedido,"
    SQL = SQL & " fl_pedido_finalizado,fl_dif_icm,fl_pendencia,"
    SQL = SQL & " cod_cliente,cod_uf,cod_end_entrega,cod_banco,"
    SQL = SQL & " cod_transp,cod_nope,cod_repres,cod_vend,tp_pedido,"
    SQL = SQL & " pc_desc_suframa,cod_plano,pc_desconto,pc_acrescimo,"
    SQL = SQL & " dt_digitacao,dt_pedido,vl_ipi,vl_contabil,frete_pago,"
    SQL = SQL & " mens_pedido,mens_nota,qtd_itens,qtd_item_pedido)"
    SQL = SQL & " values( " & dn!num_pedido & "," & dn!seq_pedido & ","
    SQL = SQL & dn!cod_loja & "," & dn!num_pedido & ", 'N','" & dn!fl_dif_icm & "','"
    SQL = SQL & dn!fl_pendencia & "'," & dn!cod_cliente & ",'" & dn!cod_uf & "',"
    SQL = SQL & dn!cod_end_entrega & "," & dn!cod_banco & "," & dn!cod_transp & ",'"
    SQL = SQL & dn!cod_nope & "'," & dn!cod_repres & "," & dn!cod_vend & ","
    SQL = SQL & dn!tp_pedido & "," & dn!PC_DESC_SUFRAMA & "," & dn!cod_plano & ","
    SQL = SQL & FmtBR(dn!PC_DESCONTO) & "," & FmtBR(dn!PC_ACRESCIMO) & ","
    SQL = SQL & "'" & Format$(dn!dt_digitacao, "mm/dd/yy hh:nn:ss") & "',"
    SQL = SQL & "'" & Format$(dn!dt_pedido, "mm/dd/yy") & "',"
    SQL = SQL & FmtBR(dn!vl_ipi) & "," & FmtBR(dn!vl_contabil) & ","
    SQL = SQL & "'" & dn!frete_pago & "',"
    SQL = SQL & "'" & dn!mens_pedido & "',"
    SQL = SQL & "'" & dn!mens_nota & "',"
    SQL = SQL & dn!qtd_item_pedido & "," & dn!qtd_item_pedido & ")"
  
    lblMsg = "Gravando Pedido no Access .."
    dbAccess.Execute SQL, dbFailOnError
    FreeLocks
      
    SQL = "Select a.num_pedido,a.seq_pedido,a.cod_loja,"
    SQL = SQL & " a.num_item_pedido,a.cod_dpk,"
    SQL = SQL & " b.cod_fornecedor,b.cod_fabrica,"
    SQL = SQL & " a.cod_trib,a.cod_tribipi, a.qtd_solicitada,"
    SQL = SQL & " nvl(a.tabela_venda,' ') tabela, nvl(a.preco_unitario,0) preco_unitario,"
    SQL = SQL & " nvl(a.pc_desc1,0) pc_desc1, nvl(a.pc_desc2,0) pc_desc2, nvl(a.pc_desc3,0) pc_desc3,"
    SQL = SQL & " nvl(a.pc_dificm,0) pc_dif_icm, nvl(a.pc_ipi,0) pc_ipi, a.situacao"
    SQL = SQL & " from itpednota_venda a, item_cadastro b"
    SQL = SQL & " Where a.num_pedido = :num"
    SQL = SQL & " and a.seq_pedido = :seq"
    SQL = SQL & " and a.cod_loja = :loja"
    SQL = SQL & " and a.cod_dpk = b.cod_dpk"
  
    oradatabase.Parameters.Remove "num"
    oradatabase.Parameters.Add "num", txtNrpedido.Text, 1
    oradatabase.Parameters.Remove "seq"
    oradatabase.Parameters.Add "seq", txtSEQUENCIA.Text, 1
    oradatabase.Parameters.Remove "loja"
    oradatabase.Parameters.Add "loja", Mid(cboDeposito.Text, 1, 3), 1
  
    lblMsg = "Buscando Itens no Oracle .."
    Set dn2 = oradatabase.dbcreatedynaset(SQL, 8&)
  
    If dn2.EOF Then
      MsgBox "Problema na Busca dos itens do Pedido. Ligue para o Depto Sistemas", _
            vbExclamation, "Aten��o"
      Call Init_tela
      Exit Sub
    End If
  
    dn2.MoveFirst
    Do
        
           
        preco_liquido = FmtBR(Format$(dn2!preco_unitario * _
                                 (1 - dn2!pc_desc1 / 100) * _
                                 (1 - dn2!pc_desc2 / 100) * _
                                 (1 - dn2!pc_desc3 / 100) * _
                                 (1 - dn!PC_DESC_SUFRAMA / 100) * _
                                 (1 - dn2!pc_dif_icm / 100) * _
                                 (1 + dn!PC_ACRESCIMO / 100) * _
                                 (1 - dn!PC_DESCONTO / 100), "##########.00"))

      If dn2!cod_trib = "3" Or dn2!cod_trib = "6" Or dn2!cod_trib = "1" Then
        TRIB = "1"
      Else
        TRIB = dn2!cod_trib
      End If

      SQL = "Insert into item_pedido("
      SQL = SQL & " num_pendente,seq_pedido,cod_loja,num_item_pedido,"
      SQL = SQL & " cod_dpk,cod_fornecedor,cod_fabrica,cod_tributacao,"
      SQL = SQL & " cod_tributacao_ipi,qtd_solicitada,tabela_compra,"
      SQL = SQL & " preco_unitario, preco_liquido,pc_desconto1,"
      SQL = SQL & " pc_desconto2,pc_desconto3,pc_desc_icm,pc_ipi,situacao)"
      SQL = SQL & " values(" & dn2!num_pedido & "," & dn2!seq_pedido & ","
      SQL = SQL & dn2!cod_loja & "," & dn2!num_item_pedido & "," & dn2!cod_dpk & ","
      SQL = SQL & dn2!cod_fornecedor & ",'" & dn2!cod_fabrica & "'," & TRIB & ","
      SQL = SQL & dn2!cod_tribipi & "," & dn2!qtd_solicitada & ",'" & dn2!tabela & "',"
      SQL = SQL & FmtBR(dn2!preco_unitario) & "," & FmtBR(preco_liquido) & ","
      SQL = SQL & FmtBR(dn2!pc_desc1) & "," & FmtBR(dn2!pc_desc2) & ","
      SQL = SQL & FmtBR(dn2!pc_desc3) & "," & FmtBR(dn2!pc_dif_icm) & ","
      SQL = SQL & FmtBR(dn2!pc_ipi) & "," & dn2!situacao & ")"
    
      lblMsg = "Gravando Item no Access .."
      dbAccess.Execute SQL, dbFailOnError
      FreeLocks
      
    
     dn2.MoveNext
   Loop Until dn2.EOF
     
 
         'Dispara procedure no Oracle
            SQL = "BEGIN PR_CANCELA (:loja,:NUM,:SEQ,8,0);"
      SQL = SQL & " COMMIT;"
      SQL = SQL & "EXCEPTION"
      SQL = SQL & " WHEN OTHERS THEN"
      SQL = SQL & " ROLLBACK;"
      SQL = SQL & " :cod_errora := SQLCODE;"
      SQL = SQL & " :txt_errora := SQLERRM;"
      SQL = SQL & " END;"
    
      oradatabase.Parameters.Remove "loja"
      oradatabase.Parameters.Add "loja", Mid(cboDeposito.Text, 1, 3), 1
      oradatabase.Parameters.Remove "num"
      oradatabase.Parameters.Add "num", txtNrpedido.Text, 1
      oradatabase.Parameters.Remove "SEQ"
      oradatabase.Parameters.Add "SEQ", txtSEQUENCIA.Text, 1
      oradatabase.Parameters.Remove "cod_errora"
      oradatabase.Parameters.Add "cod_errora", 0, 2
      oradatabase.Parameters.Remove "txt_errora"
      oradatabase.Parameters.Add "txt_errora", "", 2
    
    
       oradatabase.ExecuteSQL SQL
     
       If oradatabase.Parameters("cod_errora") <> 0 Then
          MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
          Screen.MousePointer = vbDefault
          Exit Sub
       Else
         MsgBox "Pedido pronto para manuten��o", vbInformation, "Aten��o"
         Init_tela
       End If
   
      lblMsg = ""
  Else
    MsgBox "Pedido n�o pertence a esta filial ou dep�sito", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
    
  Exit Sub
    
TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If

End Sub

Private Sub Form_Load()
    
    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim ss1 As Object
    Dim i As Byte
    
 
    'mouse pointer
    Screen.MousePointer = vbHourglass
    
    'carrega deposito default e depositos "visao"
             SQL = "Select to_char(a.cod_loja,'09') || '-' || b.nome_fantasia deposito"
       SQL = SQL & " from deposito_visao a, loja b"
       SQL = SQL & " where a.cod_loja=b.cod_loja and a.nome_programa = 'VDA310'"
       
       Set ss1 = oradatabase.dbcreatedynaset(SQL, 0&)
    
       If ss1.EOF Then
         MsgBox "Tabela DEPOSITO_VISAO est� vazia ligue para Depto.Sistemas", vbExclamation, "Aten��o"
         Screen.MousePointer = 0
         Exit Sub
       End If
       
       For i = 1 To ss1.RecordCount
         frmManut.cboDeposito.AddItem ss1!deposito
         ss1.MoveNext
       Next
          
         frmManut.cboDeposito.Text = deposito_default
           
         If frmManut.cboDeposito.ListCount = 1 Then
           frmManut.cboDeposito.Enabled = False
         Else
           frmManut.cboDeposito.Enabled = True
         End If

    'mouse pointer
    Screen.MousePointer = vbDefault
  
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 75 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If

End Sub


Private Sub lblSituacao_Click()

End Sub

Private Sub msg_Click()

End Sub

Private Sub txtNrpedido_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtSEQUENCIA_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtSEQUENCIA_LostFocus()
  
  On Error GoTo TrataErro
  
  If txtNrpedido.Text = "" Then
    MsgBox "Selecione um Pedido para colocar em Manuten��o", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
  
  If txtSEQUENCIA.Text = "" Then
    txtSEQUENCIA.Text = "0"
  End If
  
  SQL = "Select a.cod_cliente,"
  SQL = SQL & " a.num_nota,"
  SQL = SQL & " a.num_pedido,a.seq_pedido,a.cod_loja,"
  SQL = SQL & " c.nome_cliente,"
  SQL = SQL & " a.fl_ger_ssm,"
  SQL = SQL & " a.fl_ger_nfis,"
  SQL = SQL & " a.fl_dif_icm,"
  SQL = SQL & " a.fl_pendencia,"
  SQL = SQL & " nvl(a.cod_end_entrega,0) cod_end_entrega,"
  SQL = SQL & " a.cod_banco,"
  SQL = SQL & " a.cod_transp,"
  SQL = SQL & " a.cod_filial,"
  SQL = SQL & " nvl(a.cod_repres,0) cod_repres,"
  SQL = SQL & " nvl(a.cod_vend,0) cod_vend,"
  SQL = SQL & " a.tp_pedido,"
  SQL = SQL & " a.pc_desc_suframa,"
  SQL = SQL & " a.cod_plano,"
  SQL = SQL & " a.pc_desconto,"
  SQL = SQL & " a.pc_acrescimo,"
  SQL = SQL & " to_char(a.dt_digitacao,'dd/mm/yy hh24:mi:ss') dt_digitacao,"
  SQL = SQL & " to_char(a.dt_pedido,'dd/mm/yy') dt_pedido,"
  SQL = SQL & " nvl(a.vl_ipi,0) vl_ipi,"
  SQL = SQL & " a.frete_pago,"
  SQL = SQL & " nvl(a.mens_pedido,' ') mens_pedido,"
  SQL = SQL & " nvl(a.mens_nota,' ') mens_nota,"
  SQL = SQL & " qtd_item_pedido,"
  SQL = SQL & " a.tp_dpkblau,"
  SQL = SQL & " a.tp_transacao,"
  SQL = SQL & " a.cod_filial,"
  SQL = SQL & " b.sigla,"
  SQL = SQL & " d.nome_cidade,"
  SQL = SQL & " d.cod_uf,"
  SQL = SQL & " a.cod_nope,"
  SQL = SQL & " nvl(a.vl_contabil,0) vl_contabil,"
  SQL = SQL & " a.qtd_item_pedido,"
  SQL = SQL & " a.situacao "
  SQL = SQL & " from pednota_venda a, filial b, cliente c, cidade d"
  SQL = SQL & " Where num_pedido = :num"
  SQL = SQL & " and seq_pedido = :seq"
  SQL = SQL & " and cod_loja = :loja"
  SQL = SQL & " and c.cod_cidade = d.cod_cidade"
  SQL = SQL & " and a.cod_filial = b.cod_filial "
  SQL = SQL & " and a.cod_cliente = c.cod_cliente"
  
  oradatabase.Parameters.Remove "num"
  oradatabase.Parameters.Add "num", txtNrpedido.Text, 1
  oradatabase.Parameters.Remove "seq"
  oradatabase.Parameters.Add "seq", txtSEQUENCIA.Text, 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", Mid(cboDeposito.Text, 1, 3), 1
  
  Set dn = oradatabase.dbcreatedynaset(SQL, 8&)
  
  If dn.EOF Then
    MsgBox "Pedido n�o localizado, verifique", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
  
  If dn!tp_dpkblau <> "0" Then
    MsgBox "Este pedido pertence a divis�o BLAU", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
  
  If dn!tp_transacao = "0" Then
    MsgBox "Este pedido n�o � de Venda", vbInformation, "Aten��o"
    Init_tela
    Exit Sub
  End If
  
  
  lblCod_Cliente.Caption = dn!cod_cliente
  lblNome_Cliente.Caption = dn!nome_cliente
  lblFilial.Caption = dn!cod_filial & " - " & dn!sigla
  lblCidade.Caption = dn!nome_cidade
  lblUf.Caption = dn!cod_uf
  lblNatureza.Caption = dn!cod_nope
  lblVl_Contabil.Caption = Format$(dn!vl_contabil, "standard")
  lblQtdItens.Caption = dn!qtd_item_pedido
  Exit Sub
  
TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
  
End Sub

Public Sub Init_tela()

  cboDeposito.Text = deposito_default
  txtNrpedido.Text = ""
  txtSEQUENCIA.Text = ""
  lblCod_Cliente.Caption = ""
  lblNome_Cliente.Caption = ""
  lblFilial.Caption = ""
  lblCidade.Caption = ""
  lblUf.Caption = ""
  lblNatureza.Caption = ""
  lblVl_Contabil.Caption = ""
  lblQtdItens.Caption = ""
  
  
  txtNrpedido.SetFocus
  
End Sub


