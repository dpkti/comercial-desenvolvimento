VERSION 4.00
Begin VB.Form frmAltera 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Volta situa��o de OP j� liberada"
   ClientHeight    =   4140
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   6690
   Height          =   4545
   Left            =   1080
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   6690
   Top             =   1170
   Width           =   6810
   Begin VB.CommandButton Command1 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   3360
      TabIndex        =   6
      Top             =   2280
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      Caption         =   "&Alterar"
      Height          =   375
      Left            =   1680
      TabIndex        =   5
      Top             =   2280
      Width           =   1335
   End
   Begin VB.ComboBox cboDeposito 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   1200
      TabIndex        =   2
      Top             =   840
      Width           =   2055
   End
   Begin VB.TextBox txtSequencia 
      Alignment       =   2  'Center
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   5520
      MaxLength       =   1
      TabIndex        =   1
      Top             =   840
      Width           =   255
   End
   Begin VB.TextBox txtNrpedido 
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4440
      MaxLength       =   7
      TabIndex        =   0
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblDeposito 
      Caption         =   "Dep�sito"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   840
      Width           =   855
   End
   Begin VB.Label lblPedido 
      Caption         =   "Nr.Pedido"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3480
      TabIndex        =   3
      Top             =   840
      Width           =   855
   End
End
Attribute VB_Name = "frmAltera"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Unload Me
End Sub


Private Sub Command5_Click()
  Dim SQL As String
  Dim ss As Object


  If txtSequencia = "" Then
    txtSequencia = 0
  End If

  If txtNrpedido = "" Then
    MsgBox "Selecione um pedido", vbInformation
    Exit Sub
  ElseIf Mid(Trim(cboDeposito), 1, 2) = "" Then
    MsgBox "Selecione um dep�sito", vbInformation
    Exit Sub
  End If

  SQL = "Select tp_pgto"
  SQL = SQL & " From cobranca.boleto_op"
  SQL = SQL & " Where "
  SQL = SQL & " seq_pedido = :seq and"
  SQL = SQL & " num_pedido = :num and"
  SQL = SQL & " cod_loja = :loja"
  
  oradatabase.Parameters.Remove "seq"
  oradatabase.Parameters.Add "seq", txtSequencia, 1
  oradatabase.Parameters.Remove "num"
  oradatabase.Parameters.Add "num", txtNrpedido, 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", Mid(Trim(cboDeposito), 1, 2), 1

  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.RecordCount = 0 Then
    MsgBox "Este pedido n�o existe cadastrado como OP", vbInformation
    txtSequencia = ""
    txtNrpedido = ""
    Exit Sub
  Else
    If ss!TP_PGTO = "O" Then
       SQL = "BEGIN"
       SQL = SQL & " update cobranca.boleto_op set"
       SQL = SQL & " situacao = 0"
       SQL = SQL & " Where seq_pedido = :seq and"
       SQL = SQL & " num_pedido = :num and"
       SQL = SQL & " cod_loja = :loja;"
       SQL = SQL & " COMMIT;"
       SQL = SQL & " EXCEPTION"
       SQL = SQL & " WHEN OTHERS THEN"
       SQL = SQL & " ROLLBACK;"
       SQL = SQL & " :cod_errora := SQLCODE;"
       SQL = SQL & " :txt_errora := SQLERRM;"
       SQL = SQL & " END;"
       
       oradatabase.Parameters.Remove "seq"
       oradatabase.Parameters.Add "seq", txtSequencia, 1
       oradatabase.Parameters.Remove "num"
       oradatabase.Parameters.Add "num", txtNrpedido, 1
       oradatabase.Parameters.Remove "loja"
       oradatabase.Parameters.Add "loja", Mid(Trim(cboDeposito), 1, 2), 1
       oradatabase.Parameters.Remove "cod_errora"
       oradatabase.Parameters.Add "cod_errora", 0, 2
       oradatabase.Parameters.Remove "txt_errora"
       oradatabase.Parameters.Add "txt_errora", "", 2
   
       oradatabase.ExecuteSQL SQL
       If oradatabase.Parameters("cod_errora").Value <> 0 Then
         MsgBox "Problema na atualiza��o, ligue para o Depto de Sistemas", vbInformation
         Exit Sub
       Else
         MsgBox "Altera��o efetuada com sucesso, volte para a tela de OP para visualizar o pedido", vbInformation
         Unload Me
       End If
    Else
      MsgBox "Este pedido n�o foi liberado como OP, portanto n�o poder� ser alterado", vbInformation
      Exit Sub
    End If
  End If

End Sub


Private Sub Form_Load()
      
    On Error GoTo TrataErro

    Dim ss As Snapshot
    Dim ss1 As Object
    Dim i As Byte
    Dim SQL As String
    
 
    'mouse pointer
    Screen.MousePointer = vbHourglass
    
    'carrega deposito default e depositos "visao"
             SQL = "Select to_char(a.cod_loja,'09') || '-' || a.nome_fantasia deposito"
       SQL = SQL & " from loja a, deposito b"
       SQL = SQL & " where a.cod_loja = b.cod_loja"
       
       Set ss1 = oradatabase.dbcreatedynaset(SQL, 0&)
    
       If ss1.EOF Then
         MsgBox "Tabela DEPOSITO_VISAO est� vazia ligue para Depto.Sistemas", vbExclamation, "Aten��o"
         Screen.MousePointer = 0
         Exit Sub
       End If
       
       For i = 1 To ss1.RecordCount
         frmAltera.cboDeposito.AddItem ss1!deposito
         frmAltera.cboDeposito.Text = ss1!deposito
         ss1.MoveNext
       Next
          
        
           
         If frmAltera.cboDeposito.ListCount = 1 Then
           frmAltera.cboDeposito.Enabled = False
         Else
           frmAltera.cboDeposito.Enabled = True
         End If

    'mouse pointer
    Screen.MousePointer = vbDefault
  
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3189 Or Err = 75 Then
      Resume
    ElseIf Err = 7 Then
      Exit Sub
    Else
      Call Process_Line_Errors(SQL)
    End If

End Sub



Private Sub txtNrpedido_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtSequencia_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtSequencia_LostFocus()
 
  
  If txtSequencia = "" Then
    txtSequencia = 0
  End If
  
  

End Sub


