Attribute VB_Name = "Module2"
'Declarar variaveis globais
Global lngNUM_COTACAO As Long   'NUMERO DE COTACAO
Public lngNUM_PEDIDO As Long    'NUMERO DE PEDIDO
Public lngSEQ_PEDIDO As Long
Public lngNUM_NOTA As Long
Public dn As Object
Public dn2 As Object
Public cod_cancel As Long

Global sCOD_VEND As String        'CODIGO DE VENDEDOR
Global strPath As String        'PATH DOS DADOS NA REDE
Global strPathUsuario As String 'PATH DO USUARIO
Global dbOracle As Database     'BANCO DE DADOS ORACLE
Global dbAccess As Database     'BANCO DE DADOS ACCESS
Global lngCOD_CLIENTE As Long   'CODIGO DO CLIENTE - DPK 24 HORAS
Global lngCODIGO As Long        'CODIGO ( cotacao ou pedido ) - DPK 24 HORAS
Public ssItemCot As Object
Public sngDesc_uf As Single
Public qtd As Byte
Public tp_item As Byte           ' indica se o item ser� digitado preco liquido
Public ssItem As Object
Public strIEfim As String
Public strISFim As String
Public lngCod_Loja As Long
Public lngFilial As Long
Public tp_cliente As String
Public ssDescontos As Object
Public Tribut As Integer
Public Preco_Bruto As Double
Public class_fiscal As String
Public sPC_DESCONTO_PLANO As Single
Public sPC_ACRESCIMO_PLANO As Single
Public lngQtd_inf As Long
Public lngQtd_dig As Long
Public item As Boolean
Public tela As String
Public strDt_Tabela As String
Public ckicm As Boolean


Public tp_tabela As Integer
Public ocorr_preco As Integer
Public SQL As String
Public deposito_default As String
Public uf_destino As String
Public uf_origem As String
Public FILIAL_PED As String
Public tipo_cliente As String     'categoria do cliente

Public qtd_max As Long
Public qtd_min As Long


' vetores
Public arrmontadora() As Integer
Public arrgrupo() As Integer
Public arrsubgrupo() As Integer

Public montadora As Integer
Public grupo As Integer
Public subgrupo As Integer


Public cont As Long

'VARIAVEIS UTILIZADAS PARA PESQUISA
Public CONTINUAPESQUISA As Integer
Public OCORRENCIA As Integer
Public CONTADOROCORRENCIA As Long
Public TEXTOPARAPROCURA As String
Public INICIOPESQUISA As Long
Public INILINHA As Long

Public tot_grdfabr As Integer

Public counter As Long                   'contador de registros da tabela de venda
Public tabela(1 To 2, 0 To 2) As String  'nome da tabela (ocorrencia,tipo tabela)
Public data_vig(1 To 2, 0 To 2, 1 To 5) As Date ' data de vigencia
Public seq_atu(0 To 2) As Byte            ' sequencia do desconto atual
Public seq_ant(0 To 2) As Byte            ' sequencia do desconto anterior
Public i As Byte                          ' �ndice que indica tipo de tabela (0,1,2)
Public j As Byte                          ' �ndica que indica ocorrencia de preco (1,2)
Public w As Byte                          ' �ndice que indica ocorrencia da data (1 a 5)
Public ocor_atu(0 To 2) As Byte           ' ocorrencia de preco atual
Public ocor_ant(0 To 2) As Byte           'ocorrencia de preco anterior

Public forn As Integer
Public pesq As String
Public data_real As Date
Public contador As Long


Global Const Branco = &HFFFFFF
Global Const Vermelho = &HC0C0FF
Global Const Cinza = &H8000000F
Public Sub TABELA_VENDA(dt_pedido As String)
      
  Dim SQL As String
      
 
'CARREGA TABELA DE VEND
      SQL = "select tabela_venda, tp_tabela, ocorr_preco,"
SQL = SQL & " decode(nvl(dt_vigencia1,0),'0','0',to_char(to_date(dt_vigencia1,'YYMMDD'),'DD/MM/YY')) dt_vigencia1,"
SQL = SQL & " decode(nvl(dt_vigencia2,0),'0','0',to_char(to_date(dt_vigencia2,'YYMMDD'),'DD/MM/YY')) dt_vigencia2,"
SQL = SQL & " decode(nvl(dt_vigencia3,0),'0','0',to_char(to_date(dt_vigencia3,'YYMMDD'),'DD/MM/YY')) dt_vigencia3,"
SQL = SQL & " decode(nvl(dt_vigencia4,0),'0','0',to_char(to_date(dt_vigencia4,'YYMMDD'),'DD/MM/YY')) dt_vigencia4,"
SQL = SQL & " decode(nvl(dt_vigencia5,0),'0','0',to_char(to_date(dt_vigencia5,'YYMMDD'),'DD/MM/YY')) dt_vigencia5"
SQL = SQL & " from tabela_venda where divisao='D' and tabela_venda = rpad(:tabela,6)"
SQL = SQL & " order by ocorr_preco"

 
 
 oradatabase.Parameters.Remove "tabela"
 oradatabase.Parameters.Add "tabela", FRMITENS.txtTabela.Text, 1
 
 Set oradynaset = oradatabase.dbcreatedynaset(SQL, 8&)
  
 If oradynaset.EOF Then
   MsgBox "Tabela n�o existe", vbInformation, "Aten��o"
   FRMITENS.txtTabela.Text = ""
   Exit Sub
 ElseIf dt_pedido < oradynaset!dt_vigencia1 Then
    If oradynaset!dt_vigencia2 = "0" Then
      MsgBox "A Tabela " & FRMITENS.txtTabela.Text & " entrar� em vig�ncia a partir de " & oradynaset!dt_vigencia1
      FRMITENS.txtTabela.SetFocus
      Exit Sub
    Else
      If dt_pedido < oradynaset!dt_vigencia2 Then
        If oradynaset!dt_vigencia3 = "0" Then
          MsgBox "A Tabela " & FRMITENS.txtTabela.Text & " entrar� em vig�ncia a partir de " & oradynaset!dt_vigencia2
          FRMITENS.txtTabela.SetFocus
          Exit Sub
        Else
          If dt_pedido < oradynaset!dt_vigencia3 Then
            If oradynaset!dt_vigencia4 = "0" Then
            MsgBox "A Tabela " & FRMITENS.txtTabela.Text & " entrar� em vig�ncia a partir de " & oradynaset!dt_vigencia3
            FRMITENS.txtTabela.SetFocus
            Exit Sub
          Else
            If dt_pedido < oradynaset!dt_vigencia4 Then
              If oradynaset!dt_vigencia5 = "0" Then
              MsgBox "A Tabela " & FRMITENS.txtTabela.Text & " entrar� em vig�ncia a partir de " & oradynaset!dt_vigencia4
              FRMITENS.txtTabela.SetFocus
              Exit Sub
            Else
              If dt_pedido < oradynaset!dt_vigencia5 Then
                MsgBox "A Tabela " & FRMITENS.txtTabela.Text & " entrar� em vig�ncia a partir de " & oradynaset!dt_vigencia5
                FRMITENS.txtTabela.SetFocus
                Exit Sub
               End If
             End If
           End If
         End If
       End If
     End If
   End If
 End If
End If

tp_tabela = oradynaset!tp_tabela
ocorr_preco = oradynaset!ocorr_preco


End Sub


 Sub ReSequenciar()
    Dim SQL As String
    Dim ss As Snapshot
    Dim ss2 As Snapshot
    Dim PLSQL As String
    Dim i As Integer
    Dim ITEM_COT As Integer
    Dim qtd As Long
    
        'carregar dpk e sequencias
        SQL = "select COD_DPK,NUM_ITEM_PEDIDO from ITEM_PEDIDO where "
        SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
        SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
        SQL = SQL & " and COD_LOJA = " & lngCod_Loja
        SQL = SQL & " order by NUM_ITEM_PEDIDO"
        Set ss = dbAccess.CreateSnapshot(SQL)
        FreeLocks
        ss.MoveFirst
        ss.MoveLast
        If (ss.EOF And ss.BOF) Or ss.RecordCount = 0 Then
          Exit Sub
        End If
        ss.MoveFirst
        i = 1
        Do
            If i <> ss("NUM_ITEM_PEDIDO") Then
                SQL = "update ITEM_PEDIDO "
                SQL = SQL & "set NUM_ITEM_PEDIDO = " & i
                SQL = SQL & " where SEQ_PEDIDO = " & lngSEQ_PEDIDO
                SQL = SQL & " and NUM_PENDENTE = " & lngNUM_PEDIDO
                SQL = SQL & " and COD_LOJA = " & lngCod_Loja
                SQL = SQL & " and NUM_ITEM_PEDIDO = " & ss("NUM_ITEM_PEDIDO")
                'atualizar
                dbAccess.Execute SQL, dbFailOnError
                FreeLocks
            End If
            i = i + 1
            ss.MoveNext
        Loop Until ss.EOF
        ss.Close
        
        SQL = "select max(num_item_pedido) as qtd"
        SQL = SQL & " from item_pedido"
        SQL = SQL & " where "
        SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
        SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
        SQL = SQL & " and COD_LOJA = " & lngCod_Loja

        Set ss2 = dbAccess.CreateSnapshot(SQL)

        qtd = ss2!qtd
       
        SQL = "update pedido set qtd_itens = " & qtd
        SQL = SQL & " where "
        SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
        SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
        SQL = SQL & " and COD_LOJA = " & lngCod_Loja
                  
        'atualizar
         dbAccess.Execute SQL, dbFailOnError
         FreeLocks

         ss2.Close
End Sub





Public Sub Tipo_Item()
  
  If frmFimPedido.lstNatureza.Text <> "B02" And _
     frmFimPedido.lstNatureza.Text <> "B20" And _
     frmFimPedido.lstNatureza.Text <> "B22" And _
     frmFimPedido.lstNatureza.Text <> "B24" Then
     FRMITENS.lblTabela.Visible = False
     FRMITENS.txtTabela.Visible = False
     FRMITENS.lblPeriodo.Visible = False
     FRMITENS.txtPeriodo.Visible = False
     FRMITENS.lblAdicional.Visible = False
     FRMITENS.txtAdicional.Visible = False
     FRMITENS.lblUf.Visible = False
     FRMITENS.txtUf.Visible = False
     FRMITENS.lbldificm.Visible = False
     FRMITENS.lblpcIcm.Visible = False
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = True
     FRMITENS.chkSemanaPassada.Visible = False
     tp_item = 2
  Else
     FRMITENS.lblTabela.Visible = True
     FRMITENS.txtTabela.Visible = True
     FRMITENS.lblPeriodo.Visible = True
     FRMITENS.txtPeriodo.Visible = True
     FRMITENS.lblAdicional.Visible = True
     FRMITENS.txtAdicional.Visible = True
     FRMITENS.lblUf.Visible = True
     FRMITENS.txtUf.Visible = True
     FRMITENS.lbldificm.Visible = True
     FRMITENS.lblpcIcm.Visible = True
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = False
     FRMITENS.chkSemanaPassada.Visible = True
     tp_item = 1
End If
     
 



End Sub

Public Sub Descontos()
  
 Dim SQL As String
 Dim ss As Object
 Dim ss2 As Object
 Dim pc_icm As Double
 Dim ssUF As Object
  
  'Busca desconto de per�odo
  
   'consulta descontos
          SQL = "Select cod_filial, "
    SQL = SQL & " cod_dpk,cod_subgrupo,cod_grupo, "
    SQL = SQL & " to_char(pc_desc_periodo3,'99.99') pc_desc_periodo3 "
    SQL = SQL & " from TABELA_DESCPER "
    SQL = SQL & " where SITUACAO = 0 and  "
    SQL = SQL & " sequencia = :seq and"
    SQL = SQL & " tp_tabela= :tp_tabela and"
    SQL = SQL & " ocorr_preco = :ocorr_preco and"
    SQL = SQL & " cod_fornecedor = :forn and"
    SQL = SQL & " cod_filial = 0 and"
    SQL = SQL & " cod_grupo in (0,:grupo) and"
    SQL = SQL & " cod_subgrupo in (0,:subgrupo) and"
    SQL = SQL & " cod_dpk in (0,:dpk) "
    SQL = SQL & " Union "
    SQL = SQL & " Select cod_filial,"
    SQL = SQL & " cod_dpk,cod_subgrupo,cod_grupo, "
    SQL = SQL & " to_char(pc_desc_periodo3,'99.99') pc_desc_periodo3 "
    SQL = SQL & " from TABELA_DESCPER "
    SQL = SQL & " where SITUACAO = 0 and"
    SQL = SQL & " sequencia = :seq and"
    SQL = SQL & " tp_tabela= :tp_tabela and"
    SQL = SQL & " ocorr_preco = :ocorr_preco and"
    SQL = SQL & " cod_fornecedor = :forn and"
    SQL = SQL & " cod_filial = :filial and"
    SQL = SQL & " cod_grupo in (0,:grupo) and"
    SQL = SQL & " cod_subgrupo in (0,:subgrupo) and"
    SQL = SQL & " cod_dpk in (0,:dpk) "
    SQL = SQL & " order by 1 desc,2 desc,3 desc,4 desc "
    
   
    oradatabase.Parameters.Remove "seq"
    If FRMITENS.chkSemanaPassada.Value = vbUnchecked Then
      oradatabase.Parameters.Add "seq", 1, 1
      strSEQUENCIA = "1"
    Else
      strSEQUENCIA = "2"
      oradatabase.Parameters.Add "seq", 2, 1
    End If
    oradatabase.Parameters.Remove "tp_tabela"
    oradatabase.Parameters.Add "tp_tabela", tp_tabela, 1
    oradatabase.Parameters.Remove "ocorr_preco"
    oradatabase.Parameters.Add "ocorr_preco", ocorr_preco, 1
    oradatabase.Parameters.Remove "forn"
    oradatabase.Parameters.Add "forn", CLng(FRMITENS.txtCOD_FORNECEDOR.Text), 1
    oradatabase.Parameters.Remove "filial"
    oradatabase.Parameters.Add "filial", lngFilial, 1
    oradatabase.Parameters.Remove "grupo"
    oradatabase.Parameters.Add "grupo", grupo, 1
    oradatabase.Parameters.Remove "subgrupo"
    oradatabase.Parameters.Add "subgrupo", subgrupo, 1
    oradatabase.Parameters.Remove "dpk"
    oradatabase.Parameters.Add "dpk", CLng(FRMITENS.txtCOD_DPK.Text), 1

        
       
    'oradatabase.ExecuteSQL ("alter session set sql_trace=true")
    Set ssDescontos = oradatabase.dbcreatedynaset(SQL, 8&)
    'oradatabase.ExecuteSQL ("alter session set sql_trace=false")

  
    
  If ssDescontos.EOF Then
    FRMITENS.txtPeriodo.Text = FmtBR(0#)
  Else
    FRMITENS.txtPeriodo.Text = FmtBR(ssDescontos!pc_desc_periodo3)
  End If
  
 'Busca desconto de UF
    'desconto de UF
    SQL = "select PC_DESC from VENDAS.UF_DPK where "
    SQL = SQL & " COD_DPK = :dpk and "
    SQL = SQL & " COD_UF_ORIGEM =  :uf_origem and "
    SQL = SQL & " COD_UF_DESTINO =  :uf_destino "
    
    oradatabase.Parameters.Remove "dpk"
    oradatabase.Parameters.Remove "uf_origem"
    oradatabase.Parameters.Remove "uf_destino"
    oradatabase.Parameters.Add "dpk", FRMITENS.txtCOD_DPK.Text, 1
    oradatabase.Parameters.Add "uf_origem", uf_origem, 1
    oradatabase.Parameters.Add "uf_destino", uf_destino, 1
        
    Set ssUF = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If ssUF.EOF And ssUF.BOF Then
        sngDesc_uf = 0
        FRMITENS.txtUf.Text = FmtBR(0#)
    Else
        If Tribut = 1 And tp_cliente = "ISENTO" Then
          sngDesc_uf = 0
          FRMITENS.txtUf.Text = FmtBR(0#)
        Else
          sngDesc_uf = CSng(ssUF!PC_DESC)
          FRMITENS.txtUf.Text = FmtBR(ssUF!PC_DESC)
        End If
    End If

    
     'Busca Diferen�a de ICMS
      Call GetDifIcm

End Sub

Public Sub GetDesc_Suframa()
Dim ss As Object


        SQL = "Select nvl(pc_icm,0) pc_icm "
  SQL = SQL & " from uf_origem_destino"
  SQL = SQL & " where cod_uf_origem = :uf_origem"
  SQL = SQL & " and cod_uf_destino = :uf_destino"

 oradatabase.Parameters.Remove "uf_origem"
 oradatabase.Parameters.Add "uf_origem", uf_origem, 1
 oradatabase.Parameters.Remove "uf_destino"
 oradatabase.Parameters.Add "uf_destino", uf_destino, 1

 Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
 
 If ss.EOF Then
   frmFimPedido.lblDescSuframa.Caption = FmtBR(0#)
   FRMITENS.lblDescSuframa.Caption = FmtBR(0#)
   
 Else
   frmFimPedido.lblDescSuframa.Caption = FmtBR(ss!pc_icm)
   
 End If


End Sub

Public Sub GetDifIcm()

    'On Error GoTo TrataErro

    Dim strDifIcm As String
    Dim ss As Object
    Dim ss2 As Object
    Dim cod_dpk As Long
    
    
    If tela = "V" Or tela = "A" Then
      If frmVisPedido.chkDifIcm.Value = vbuncheked Then
        ckicm = False
      Else
        ckicm = True
      End If
    Else
      If frmFimPedido.chkDifIcm.Value = vbuncheked Then
        ckicm = False
      Else
        ckicm = True
      End If
    End If
   
    If ckicm = False Or FRMITENS.txtCOD_DPK.Text = "" Then
        FRMITENS.lblpcIcm.Caption = "0"
        strDifIcm = 0
    Else
        'dados da uf
              SQL = "select PC_ICM,PC_DIFICM "
        SQL = SQL & " from UF_ORIGEM_DESTINO "
        SQL = SQL & " where COD_UF_ORIGEM = :uf_origem and"
        SQL = SQL & " COD_UF_DESTINO = :uf_destino"
                
        oradatabase.Parameters.Remove "uf_origem"
        oradatabase.Parameters.Add "uf_origem", uf_origem, 1
        oradatabase.Parameters.Remove "uf_destino"
        oradatabase.Parameters.Add "uf_destino", uf_destino, 1
        
        Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
        
        If IsNull(ss!pc_icm) Then
          MsgBox "Problema na consulta. UF ORIGEM: " _
                 & uf_origem & " - UF DESTINO:  " & uf_destino & " . Ligue para Depto.Sistemas" _
                 , vbCritical, "Aten��o"
          End
        End If
          
        pc_icm = ss!pc_icm
        
        'Verifica desconto suframa
        If strISFim <> "" And tp_cliente = "REVENDEDOR" Then
          FRMITENS.lblDescSuframa.Caption = FmtBR(pc_icm)
        Else
          FRMITENS.lblDescSuframa.Caption = FmtBR(0)
        End If
        
        'calcula diferen�a de ICM
        If ckicm = False Then
                strDifIcm = 0
        Else
                If strIEfim <> "ISENTO" And _
                    IIf(ssItem!COD_TRIBUTACAO = "8" And ss!pc_icm = "12", False, True) And _
                    ssItem!COD_TRIBUTACAO <> "1" Then
                    If ssItem!COD_TRIBUTACAO = "8" Then
                        strDifIcm = 5.38
                    Else
                        strDifIcm = ss!PC_DIFICM
                    End If
                ElseIf strIEfim = "ISENTO" Then
                    strDifIcm = 0
                ElseIf strIEfim <> "ISENTO" And IIf(ssItem!COD_TRIBUTACAO = "8" And ss!pc_icm = "12", True, False) And _
                    ssItem!COD_TRIBUTACAO <> "1" Then
                    If ssItem!COD_TRIBUTACAO = "8" Then
                        strDifIcm = 0
                    Else
                        strDifIcm = ss!PC_DIFICM
                    End If
                Else
                          SQL = " Select cod_trib_revendedor,"
                    SQL = SQL & " cod_trib_inscrito,"
                    SQL = SQL & " cod_trib_isento"
                    SQL = SQL & " From compras.subst_tributaria"
                    SQL = SQL & " Where class_fiscal = :class and "
                    SQL = SQL & " cod_uf_origem = :uf_origem and"
                    SQL = SQL & " cod_uf_destino = :uf_destino"
                      
                    oradatabase.Parameters.Remove "class"
                    oradatabase.Parameters.Add "class", class_fiscal, 1
                    oradatabase.Parameters.Remove "uf_origem"
                    oradatabase.Parameters.Add "uf_origem", uf_origem, 1
                    oradatabase.Parameters.Remove "uf_destino"
                    
                    If strISFim <> "" And uf_origem <> uf_destino Then
                      oradatabase.Parameters.Add "uf_destino", "SU", 1
                    Else
                      oradatabase.Parameters.Add "uf_destino", uf_destino, 1
                    End If
                    
                    Set ss2 = oradatabase.dbcreatedynaset(SQL, 8&)
                    
                    If ss2.EOF Then
                       strDifIcm = 0
                       
                    ElseIf tp_cliente = "REVENDEDOR" Then
                      If ss2!COD_TRIB_REVENDEDOR <> 0 Then
                       strDifIcm = 0
                      Else
                       strDifIcm = ss!PC_DIFICM
                      End If
                    ElseIf tp_cliente = "CONS.FINAL" Then
                      If ss2!COD_TRIB_INSCRITO <> 0 Then
                       strDifIcm = 0
                      Else
                       strDifIcm = ss!PC_DIFICM
                      End If
                    ElseIf tp_cliente = "ISENTO" Then
                      If ss2!COD_TRIB_ISENTO <> 0 Then
                       strDifIcm = 0
                      Else
                       strDifIcm = ss!PC_DIFICM
                      End If
                    End If
               End If
            End If
        FRMITENS.lblpcIcm.Caption = strDifIcm
      End If
    
    
   If uf_origem = uf_destino Then
     FRMITENS.lblpcIcm.Caption = "0"
     strDifIcm = 0
   End If

    Exit Sub

TrataErro:
   If Err = 3186 Or Err = 3188 Or Err = 3260 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
   
End Sub



Public Sub Calcula_Preco()

  If FRMITENS.lblpcIcm.Caption = "" Then
     FRMITENS.lblpcIcm.Caption = 0
  End If
    
  If FRMITENS.lblDescSuframa.Caption = "" Then
     FRMITENS.lblDescSuframa.Caption = 0
  End If
    
  If FRMITENS.txtAdicional.Text = "" Then
     FRMITENS.txtAdicional.Text = 0
  End If
    
    FRMITENS.txtPrLiquido.Text = FmtBR(Format$(Preco_Bruto * _
                                  (1 + CDbl(sPC_ACRESCIMO_PLANO) / 100) * _
                                  (1 - CDbl(sPC_DESCONTO_PLANO) / 100) * _
                                  (1 - CDbl(FRMITENS.lblDescSuframa.Caption) / 100) * _
                                  (1 - CDbl(FRMITENS.lblpcIcm.Caption) / 100) * _
                                  (1 - CDbl(FRMITENS.txtPeriodo.Text) / 100) * _
                                  (1 - CDbl(FRMITENS.txtAdicional.Text) / 100) * _
                                  (1 - sngDesc_uf / 100), "##########.00"))
End Sub

Public Sub Preco_Venda()
  Dim ss As Object
  
  
  
  SQL = "Select "
  If tp_tabela = 0 And ocorr_preco = 1 Then
    SQL = SQL & " nvl(preco_venda,0)  preco"
  ElseIf tp_tabela = 0 And ocorr_preco = 2 Then
    SQL = SQL & " nvl(preco_venda_ant,0) preco"
  ElseIf tp_tabela = 1 And ocorr_preco = 1 Then
    SQL = SQL & " nvl(preco_of,0) preco"
  ElseIf tp_tabela = 1 And ocorr_preco = 2 Then
    SQL = SQL & " nvl(preco_of,0) preco"
  ElseIf tp_tabela = 2 And ocorr_preco = 1 Then
    SQL = SQL & " nvl(preco_sp,0) preco"
  ElseIf tp_tabela = 2 And ocorr_preco = 2 Then
    SQL = SQL & " nvl(preco_sp_ant,0) preco"
  End If
  SQL = SQL & " From item_preco "
  SQL = SQL & " Where cod_dpk = :dpk and"
  SQL = SQL & " cod_loja = :loja"
  
  
  oradatabase.Parameters.Remove "dpk"
  oradatabase.Parameters.Add "dpk", FRMITENS.txtCOD_DPK.Text, 1
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", lngCod_Loja, 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
  
  
  If ss.EOF Or ss!PRECO = 0 Then
    MsgBox "N�o h� pre�o de venda cadastrado para este item, consulte o Depto. Compras", vbExclamation, "Aten��o"
    item = False
    Exit Sub
  Else
        Preco_Bruto = ss!PRECO
  End If
    
  
End Sub



Public Sub Tipo_Item2()
  
  If frmVisPedido.lblNatureza.Caption <> "B02" And _
     frmVisPedido.lblNatureza.Caption <> "B20" And _
     frmVisPedido.lblNatureza.Caption <> "B22" And _
     frmVisPedido.lblNatureza.Caption <> "B24" Then
     FRMITENS.lblTabela.Visible = False
     FRMITENS.txtTabela.Visible = False
     FRMITENS.lblPeriodo.Visible = False
     FRMITENS.txtPeriodo.Visible = False
     FRMITENS.lblAdicional.Visible = False
     FRMITENS.txtAdicional.Visible = False
     FRMITENS.lblUf.Visible = False
     FRMITENS.txtUf.Visible = False
     FRMITENS.lbldificm.Visible = False
     FRMITENS.lblpcIcm.Visible = False
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = True
     FRMITENS.chkSemanaPassada.Visible = False
     tp_item = 2
  Else
     FRMITENS.lblTabela.Visible = True
     FRMITENS.txtTabela.Visible = True
     FRMITENS.lblPeriodo.Visible = True
     FRMITENS.txtPeriodo.Visible = True
     FRMITENS.lblAdicional.Visible = True
     FRMITENS.txtAdicional.Visible = True
     FRMITENS.lblUf.Visible = True
     FRMITENS.txtUf.Visible = True
     FRMITENS.lbldificm.Visible = True
     FRMITENS.lblpcIcm.Visible = True
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = False
     FRMITENS.chkSemanaPassada.Visible = True
     tp_item = 1
End If
     
End Sub

Public Sub Tipo_Item3()
  
  If frmVisPedido.lblNatureza.Caption <> "B02" And _
     frmVisPedido.lblNatureza.Caption <> "B20" And _
     frmVisPedido.lblNatureza.Caption <> "B22" And _
     frmVisPedido.lblNatureza.Caption <> "B24" Then
     FRMITENS.lblTabela.Visible = False
     FRMITENS.txtTabela.Visible = False
     FRMITENS.lblPeriodo.Visible = False
     FRMITENS.txtPeriodo.Visible = False
     FRMITENS.lblAdicional.Visible = False
     FRMITENS.txtAdicional.Visible = False
     FRMITENS.lblUf.Visible = False
     FRMITENS.txtUf.Visible = False
     FRMITENS.lbldificm.Visible = False
     FRMITENS.lblpcIcm.Visible = False
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = False
     FRMITENS.chkSemanaPassada.Visible = False
     tp_item = 2
  Else
     FRMITENS.txtCOD_DPK.Enabled = False
     FRMITENS.txtCOD_FORNECEDOR.Enabled = False
     FRMITENS.txtCOD_FABRICA.Enabled = False
     FRMITENS.txtDescricao.Enabled = False
     FRMITENS.lblTabela.Visible = True
     FRMITENS.txtTabela.Visible = True
     FRMITENS.lblPeriodo.Visible = True
     FRMITENS.txtPeriodo.Visible = True
     FRMITENS.lblAdicional.Visible = True
     FRMITENS.txtAdicional.Visible = True
     FRMITENS.lblUf.Visible = True
     FRMITENS.txtUf.Visible = True
     FRMITENS.lbldificm.Visible = True
     FRMITENS.lblpcIcm.Visible = True
     FRMITENS.lblPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Visible = True
     FRMITENS.txtPrLiquido.Enabled = False
     FRMITENS.chkSemanaPassada.Visible = True
     tp_item = 1
End If

End Sub


