VERSION 5.00
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmVendas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acumulado de Compras"
   ClientHeight    =   4200
   ClientLeft      =   825
   ClientTop       =   2295
   ClientWidth     =   8010
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4200
   ScaleWidth      =   8010
   Begin GraphLib.Graph Graph1 
      Height          =   2055
      Left            =   1560
      TabIndex        =   15
      Top             =   1920
      Visible         =   0   'False
      Width           =   5415
      _Version        =   65536
      _ExtentX        =   9551
      _ExtentY        =   3625
      _StockProps     =   96
      BorderStyle     =   1
      RandomData      =   1
      ColorData       =   0
      ExtraData       =   0
      ExtraData[]     =   0
      FontFamily      =   4
      FontSize        =   4
      FontSize[0]     =   200
      FontSize[1]     =   150
      FontSize[2]     =   100
      FontSize[3]     =   100
      FontStyle       =   4
      GraphData       =   0
      GraphData[]     =   0
      LabelText       =   0
      LegendText      =   0
      PatternData     =   0
      SymbolData      =   0
      XPosData        =   0
      XPosData[]      =   0
   End
   Begin VB.Label Label1 
      Caption         =   "M�S-ANO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   14
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblMes4 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   13
      Top             =   1560
      Width           =   975
   End
   Begin VB.Label lblMes3 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   12
      Top             =   1200
      Width           =   975
   End
   Begin VB.Label lblMes2 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   11
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblValor4 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   10
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label lblValor3 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   9
      Top             =   1200
      Width           =   1215
   End
   Begin VB.Label lblQtd4 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   8
      Top             =   1560
      Width           =   735
   End
   Begin VB.Label lblQtd3 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   7
      Top             =   1200
      Width           =   735
   End
   Begin VB.Label lblQtd2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   6
      Top             =   840
      Width           =   735
   End
   Begin VB.Label lblValor2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   5
      Top             =   840
      Width           =   1215
   End
   Begin VB.Label lblQtd1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   4
      Top             =   480
      Width           =   735
   End
   Begin VB.Label Label4 
      Caption         =   "TOTAL PEDIDOS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5040
      TabIndex        =   3
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "TOTAL (R$)"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   2
      Top             =   120
      Width           =   1095
   End
   Begin VB.Label lblValor1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3360
      TabIndex        =   1
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label lblMes1 
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   0
      Top             =   480
      Width           =   975
   End
End
Attribute VB_Name = "frmVendas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub FORM_Load()
  Dim ss As Object
  Dim SQL As String
  Dim strMesFinal As String
  Dim i As Long
  
  On Error GoTo Trata_Erro
  
  
  V_SQL = "Begin producao.pck_vda020.pr_DATA_MES(:vCursor,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
  
  
  lblMes1 = ss!mes1
  lblMes2 = ss!mes2
  lblMes3 = ss!mes3
  lblMes4 = ss!mes4
  strMesFinal = ss!final
  
  
  OraParameters.Remove "cli"
  OraParameters.Add "cli", frmCliente.txtCodigo, 1
  OraParameters.Remove "dt1"
  OraParameters.Add "dt1", lblMes1, 1
  OraParameters.Remove "dt2"
  OraParameters.Add "dt2", lblMes2, 1
  OraParameters.Remove "dt3"
  OraParameters.Add "dt3", lblMes3, 1
  OraParameters.Remove "dt4"
  OraParameters.Add "dt4", lblMes4, 1
  OraParameters.Remove "dt_final"
  OraParameters.Add "dt_final", strMesFinal, 1
  
  
  V_SQL = "Begin producao.pck_vda020.pr_venda_mes(:vCursor,:cli,:dt1,:dt2,:dt3,:dt4,:dt_final,:vErro);END;"

  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
  
  If ss.EOF And ss.BOF Then
    lblValor1 = 0
    lblValor2 = 0
    lblValor3 = 0
    lblValor4 = 0
    
    lblQtd1 = 0
    lblQtd2 = 0
    lblQtd3 = 0
    lblQtd4 = 0
    Exit Sub
  Else
    lblValor1 = IIf(IsNull(ss!vl_1), 0, ss!vl_1)
    lblValor2 = IIf(IsNull(ss!vl_2), 0, ss!vl_2)
    lblValor3 = IIf(IsNull(ss!vl_3), 0, ss!vl_3)
    lblValor4 = IIf(IsNull(ss!vl_4), 0, ss!vl_4)
    
    lblQtd1 = IIf(IsNull(ss!qtd_1), 0, ss!qtd_1)
    lblQtd2 = IIf(IsNull(ss!qtd_2), 0, ss!qtd_2)
    lblQtd3 = IIf(IsNull(ss!qtd_3), 0, ss!qtd_3)
    lblQtd4 = IIf(IsNull(ss!qtd_4), 0, ss!qtd_4)
    
    Graph1.NumPoints = 4
    Graph1.GraphStyle = 0
    Graph1.ThisPoint = 1
    Graph1.GraphData = IIf(IsNull(ss!vl_4), 0, ss!vl_4)
    Graph1.ThisPoint = 2
    Graph1.GraphData = IIf(IsNull(ss!vl_3), 0, ss!vl_3)
    Graph1.ThisPoint = 3
    Graph1.GraphData = IIf(IsNull(ss!vl_2), 0, ss!vl_2)
    Graph1.ThisPoint = 4
    Graph1.GraphData = IIf(IsNull(ss!vl_1), 0, ss!vl_1)
    Graph1.LegendStyle = 0
    Graph1.ThisPoint = 1
    Graph1.LegendText = lblMes4
    Graph1.ThisPoint = 2
    Graph1.LegendText = lblMes3
    Graph1.ThisPoint = 3
    Graph1.LegendText = lblMes2
    Graph1.ThisPoint = 4
    Graph1.LegendText = lblMes1
    Graph1.DrawMode = 2
    Graph1.Visible = True

 End If

 
    
Exit Sub

Trata_Erro:
  Call Process_Line_Errors(SQL)
End Sub


