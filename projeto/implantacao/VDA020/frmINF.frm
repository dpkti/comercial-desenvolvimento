VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmINF 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "INFORMA��ES SOBRE AS COMPRAS DOS �LTIMOS 3 MESES"
   ClientHeight    =   4125
   ClientLeft      =   855
   ClientTop       =   2340
   ClientWidth     =   8025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4125
   ScaleWidth      =   8025
   Begin Threed.SSCommand SSCommand1 
      Height          =   375
      Left            =   6480
      TabIndex        =   2
      Top             =   240
      Visible         =   0   'False
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "Nova Consulta"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSGrid.Grid grdINF2 
      Height          =   2535
      Left            =   840
      TabIndex        =   1
      Top             =   960
      Visible         =   0   'False
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   4471
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
   Begin MSGrid.Grid grdINF 
      Height          =   2055
      Left            =   2040
      TabIndex        =   0
      Top             =   960
      Width           =   3855
      _Version        =   65536
      _ExtentX        =   6800
      _ExtentY        =   3625
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
End
Attribute VB_Name = "frmINF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub FORM_Load()
 Dim SQL As String
 Dim ss As Object
 Dim i As Integer
 
  Screen.MousePointer = 11
  grdINF.Visible = False
  SSCommand1.Visible = False
  
   'montar SQL
   
  OraParameters.Remove "cod"
  OraParameters.Add "cod", frmCliente.txtCodigo.Text, 1
  
   V_SQL = "Begin producao.pck_vda020.pr_venda_forn(:vCursor,:cod,:vErro);END;"

  
  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If

   
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o existe venda nos �ltimos 3 meses", vbInformation, "Aten��o"
        Exit Sub
        Unload Me
    End If
    
    'carrega dados
    With grdINF
        .Cols = 2
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 2800
        
        .Row = 0
        .Col = 0
        .Text = " COD"
        .Col = 1
        .Text = " FORN."
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!cod_fornecedor
            .Col = 1
            .Text = ss!sigla
             
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    grdINF.Visible = True
    Screen.MousePointer = 0

End Sub


Private Sub Grid1_RowColChange()

End Sub


Private Sub grdINF_DblClick()
 Dim SQL As String
 Dim ss As Object
 Dim i As Integer
 Dim lngForn As Long
 
  Screen.MousePointer = 11
  frmINF.grdINF2.Visible = False
  frmINF.grdINF.Visible = False
  frmINF.SSCommand1.Visible = True
  frmINF.grdINF.Col = 0
  lngForn = grdINF.Text
  
   'montar SQL
   
  OraParameters.Remove "cod"
  OraParameters.Add "cod", frmCliente.txtCodigo.Text, 1
  OraParameters.Remove "forn"
  OraParameters.Add "forn", lngForn, 1
  
   V_SQL = "Begin producao.pck_vda020.pr_venda_item(:vCursor,:cod,:forn,:vErro);END;"

  
  oradatabase.ExecuteSQL V_SQL
  
  Set ss = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(SQL)
    Exit Sub
  End If
 
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o existe venda nos �ltimos 3 meses", vbInformation, "Aten��o"
        Exit Sub
        Unload Me
    End If
    
    'carrega dados
    With grdINF2
        .Cols = 5
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 1300
        .ColWidth(2) = 1500
        .ColWidth(3) = 2000
        .ColWidth(4) = 800
        
        .Row = 0
        .Col = 0
        .Text = " COD"
        .Col = 1
        .Text = " FORN."
        .Col = 2
        .Text = "FABRICA"
        .Col = 3
        .Text = "DESCRI��O"
        .Col = 4
        .Text = "DPK"
        
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!cod_fornecedor
            .Col = 1
            .Text = ss!sigla
            .Col = 2
            .Text = ss!COD_FABRICA
            .Col = 3
            .Text = ss!DESC_ITEM
            .Col = 4
            .Text = ss!COD_DPK
             
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    grdINF2.Visible = True
    Screen.MousePointer = 0

End Sub

Private Sub SSCommand1_Click()
  frmINF.grdINF2.Visible = False
  frmINF.grdINF.Visible = True
  frmINF.SSCommand1.Visible = False
End Sub


