Attribute VB_Name = "Module2"
Option Explicit

 Sub Process_Line_Errors(ByRef SQL)
    Dim iFnum As Integer
    'On Error GoTo Handler_Process_Line_Errors
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
      Resume
    ElseIf Err.Number = 440 Then
      If (Err.Description Like "*02396*" Or _
          Err.Description Like "*01012*") Then
        Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA020/prod", 0&)
        MsgBox "Programa inativo, REPITA A OPERA��O", vbInformation, "Aten��o"
        Set OraParameters = oradatabase.Parameters
        OraParameters.Remove "vCursor"
        OraParameters.Add "vCursor", 0, 2
        OraParameters("vCursor").serverType = ORATYPE_CURSOR
        OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
        OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
 
        OraParameters.Remove "vErro"
        OraParameters.Add "vErro", 0, 2
        OraParameters("vErro").serverType = ORATYPE_NUMBER
  
        Exit Sub
      Else
        MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
      End If
    Else
      MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
     'cursor
     Screen.MousePointer = vbDefault
     'para a aplicacao
     End
    End If
     Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub


Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function


