--- PACKAGE DO SISTEMA VDA020
--- DATA: 02/08/2004
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\VDA020\VB\PADRAO\PCK_VDA020.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_VDA020 is

	Type tp_Cursor is Ref Cursor;
			  				
	PROCEDURE pr_CON_TOTALDUPL( PM_CODCLI  IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2);
			  					
			  					
	PROCEDURE pr_CON_SALDOPEDIDOS( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2);
			  					   
			  					   
	PROCEDURE pr_CON_REPR_CLIENTE( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2);
			  					   
			  					   
	PROCEDURE pr_CON_ULTIMACOMPRA( PM_CODCLI  IN NUMBER,
								   PM_MES     IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2);
			  					   
			  					   
	PROCEDURE pr_CON_DUPLVENCIDAS( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
		  						   PM_CODERRO OUT NUMBER,
		  						   PM_TXTERRO OUT VARCHAR2);
		  						   
		  						   
	PROCEDURE pr_CON_DUPLVENCER( PM_CODCLI  IN NUMBER,
								 PM_CURSOR1 IN OUT tp_Cursor,
		  						 PM_CODERRO OUT NUMBER,
		  						 PM_TXTERRO OUT VARCHAR2);
		  						 
		  						 
	PROCEDURE pr_CON_ENDERECOS( PM_CODCLI  IN NUMBER,
								PM_TPEND   IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2);
			  					
			  					
	PROCEDURE pr_ALT_DADOS( PM_CODCLI  IN NUMBER,
							PM_CODTRAN IN NUMBER,
							PM_DDD1    IN NUMBER,
							PM_FONE1   IN NUMBER,
							PM_DDD2    IN NUMBER,
							PM_FONE2   IN NUMBER,
							PM_FAX     IN NUMBER,
							PM_TELEX   IN NUMBER,
							PM_CONTATO IN VARCHAR2,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2);
			  				
			  					
	PROCEDURE pr_CON_TRANSPORTADORAS( PM_CODTRANS IN NUMBER,
									  PM_CURSOR1  IN OUT tp_Cursor,
			  						  PM_CODERRO  OUT NUMBER,
			  						  PM_TXTERRO  OUT VARCHAR2);
			  						  
			  						  
	PROCEDURE pr_CON_ACUMCOPRAS( PM_CODCLI  IN NUMBER,
								 PM_DATA1   IN VARCHAR2,
								 PM_DATA2   IN VARCHAR2,
								 PM_DATA3   IN VARCHAR2,
								 PM_DATA4   IN VARCHAR2,
								 PM_DTFINAL IN VARCHAR2,
								 PM_CURSOR1 IN OUT tp_Cursor,
			  					 PM_CODERRO OUT NUMBER,
								 PM_TXTERRO OUT VARCHAR2);
								 
								 
	PROCEDURE pr_CON_FORN3MESES( PM_CODCLI  IN NUMBER,
								 PM_CURSOR1 IN OUT tp_Cursor,
			  					 PM_CODERRO OUT NUMBER,
								 PM_TXTERRO OUT VARCHAR2);
								 
								 
	PROCEDURE pr_CON_PED3MESES( PM_CODCLI  IN NUMBER,
								PM_CODFORN IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2);
								
								
	PROCEDURE pr_CON_CLIENTECIDADE( PM_CODCIDADE IN NUMBER,
									PM_CURSOR1   IN OUT tp_Cursor,
			  						PM_CODERRO   OUT NUMBER,
									PM_TXTERRO   OUT VARCHAR2);
									
									
	PROCEDURE pr_CON_CLIENTESEMCOMPRA( PM_CODVEND IN NUMBER,
									   PM_CURSOR1 IN OUT tp_Cursor,
			  						   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2);
									   
									   
	PROCEDURE pr_CON_DICAS( PM_CODCLI  IN NUMBER,
							PM_CURSOR1 IN OUT tp_Cursor,
			  				PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE pr_ALT_DICAS( PM_CODCLI  IN NUMBER,
							PM_CODDICA IN NUMBER,
							PM_CODREPR IN NUMBER,
							PM_DICA    IN VARCHAR2,
							PM_ACT     IN VARCHAR2,
			  				PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
	--**************************						
	--- ANALISTA    : MARICI 	
	--- DATA	: 08/11/2002
	--**************************
	PROCEDURE PR_DATA(p_cursor IN OUT tp_cursor,
	                  p_erro   OUT    NUMBER);
		 
	PROCEDURE PR_CLIENTE(p_cursor      IN OUT tp_cursor,
	                     p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                     p_cgc	    IN     cliente.cgc%TYPE,
	                     p_erro        OUT    NUMBER);
		                      
	PROCEDURE PR_CLIENTE1(p_cursor       IN OUT tp_cursor,
	                      p_nome_cliente IN     cliente.nome_cliente%TYPE,
	                      p_erro        OUT     NUMBER);
		                       
	PROCEDURE PR_DUPL(p_cursor      IN OUT tp_cursor,
	                  p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                  p_erro        OUT    NUMBER);
		                      
		 
	PROCEDURE PR_PEDIDO(p_cursor      IN OUT tp_cursor,
	                   p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                   p_mes         IN     NUMBER,
	                   p_erro        OUT    NUMBER);

	PROCEDURE PR_DUPL1(p_cursor      IN OUT tp_cursor,
	                 p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                 p_erro        OUT    NUMBER);

	PROCEDURE PR_DUPL2(p_cursor      IN OUT tp_cursor,
	                 p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                 p_erro        OUT    NUMBER);
		                  
	PROCEDURE PR_ENDERECO(p_cursor      IN OUT tp_cursor,
	                     p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                     p_tp_endereco IN     clie_endereco.tp_endereco%TYPE,
	                     p_erro        OUT    NUMBER);
		                      
	PROCEDURE PR_TRANSPORTADORA(p_cursor      IN OUT tp_cursor,
	   		    p_cod_transp  IN     transportadora.cod_transp%TYPE,
	                 	    p_erro        OUT    NUMBER);
		                  
	PROCEDURE PR_CLIE_TRANSP(p_cursor      IN OUT tp_cursor,
			         p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                        p_erro        OUT    NUMBER);
		                         
	PROCEDURE PR_CLIE_SALDO(p_cursor      IN OUT tp_cursor,
			        p_cod_cliente IN     cliente.cod_cliente%TYPE,
	                       p_erro        OUT    NUMBER);
		                        
	PROCEDURE PR_CLIE_DICA(p_cursor      IN OUT tp_cursor,
			       p_cod_cliente IN     cliente.cod_cliente%TYPE,
			       p_erro        OUT    NUMBER);    
		 		       
	PROCEDURE PR_UF(p_cursor      IN OUT tp_cursor,
			p_erro        OUT    NUMBER);  
		 		
	PROCEDURE PR_CIDADE(p_cursor      IN OUT tp_cursor,
	   	    p_cod_uf      IN     uf.cod_uf%TYPE,
			    p_erro        OUT    NUMBER);  

	PROCEDURE PR_ATUDICA(p_cod_cliente IN     cliente.cod_cliente%TYPE,
	   	     p_sequencia   IN     vendas.dicas.sequencia%TYPE,
	   	     p_dica        IN     vendas.dicas.dica%TYPE,
	   	     p_cod_vend    IN     vendas.dicas.cod_vend%TYPE,
	   	     p_dt_dica     IN     vendas.dicas.dt_dica%TYPE,
	   	     p_tipo	   IN     NUMBER,
			     p_erro        OUT    NUMBER);  
		 		    
	PROCEDURE PR_ATUCLI(p_cod_cliente    IN     cliente.cod_cliente%TYPE,
	   	    p_cod_transp     IN     cliente.cod_transp%TYPE,
	   	    p_ddd1           IN     cliente.ddd1%TYPE,
	   	    p_ddd2           IN     cliente.ddd2%TYPE,
	   	    p_fone1          IN     cliente.fone1%TYPE,
	   	    p_fone2          IN     cliente.fone2%TYPE,
	   	    p_fax            IN     cliente.fax%TYPE,
	   	    p_telex          IN     cliente.telex%TYPE,
	   	    p_contato        IN     cliente.nome_contato%TYPE,
	                   p_erro           OUT    NUMBER);

	PROCEDURE PR_DATA_MES(p_cursor      IN OUT tp_cursor,
			      p_erro        OUT    NUMBER);  
		 		    
	PROCEDURE PR_VENDA_MES(p_cursor      IN OUT tp_cursor,
	   	       p_cod_cliente IN     cliente.cod_cliente%TYPE,
	   	       p_dt1         IN     VARCHAR2,
	   	       p_dt2         IN     VARCHAR2,
	   	       p_dt3         IN     VARCHAR2,
	   	       p_dt4         IN     VARCHAR2,
	   	       p_dt_final    IN     VARCHAR2,
			       p_erro        OUT    NUMBER);  
		 		       
	PROCEDURE PR_DUPL_CIDADE(p_cursor      IN OUT tp_cursor,
			         p_cod_cidade  IN     cidade.cod_cidade%TYPE,
	                        p_erro        OUT    NUMBER);
		 		       

	PROCEDURE PR_VENDA_FORN(p_cursor      IN OUT tp_cursor,
			        p_cod_cliente IN     cliente.cod_cliente%TYPE,
			        p_erro        OUT    NUMBER); 

	PROCEDURE PR_VENDA_ITEM(p_cursor      IN OUT tp_cursor,
			        p_cod_cliente IN     cliente.cod_cliente%TYPE,
			        p_cod_forn    IN     fornecedor.cod_fornecedor%TYPE,
			        p_erro        OUT    NUMBER);  		        

	PROCEDURE PR_CLIE_REPRES(p_cursor      IN OUT tp_cursor,
			         p_cod_cliente IN     cliente.cod_cliente%TYPE,
			         p_erro        OUT    NUMBER); 

	PROCEDURE PR_ESQUECERAM(p_cursor      IN OUT tp_cursor,
			        p_cod_vend   IN     representante.cod_repres%TYPE,
			        p_erro       OUT    NUMBER); 
			  					  
			  					  
End PCK_VDA020;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_VDA020 is


	PROCEDURE pr_CON_TOTALDUPL( PM_CODCLI  IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select 'a' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto -
		a.vl_abatimento - a.vl_devolucao - a.vl_pago),
		'FM999,999,999,999.00'),0)valor,
		nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
		from PRODUCAO.datas, cobranca.duplicatas a
		where a.dt_vencimento >= datas.dt_faturamento and
		a.situacao_pagto=0  and 
		a.cod_cliente = PM_CODCLI
		union
		select 'b' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto -
		a.vl_abatimento - a.vl_devolucao - a.vl_pago),
		'FM999,999,999,999.00'),0)valor,
		nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
		from PRODUCAO.datas, cobranca.duplicatas a
		where a.dt_vencimento < datas.dt_faturamento and
		a.situacao_pagto=0  and 
		a.cod_cliente = PM_CODCLI
		order by 1;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_TOTALDUPL;



	PROCEDURE pr_CON_SALDOPEDIDOS( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		Select nvl((0.75 * nvl(a.maior_saldev,0) * 
		(f.valor_uss/decode(nvl(b.valor_uss,0),0,f.valor_uss,
		b.valor_uss))) * d.pc_limite / 100,0) valor1,
		nvl(e.saldo_pedidos,0) saldo_pedidos
		from PRODUCAO.clie_credito a, PRODUCAO.dolar_diario b, PRODUCAO.datas c, 
		PRODUCAO.pont_credito d, PRODUCAO.v_saldo_pedidos e, PRODUCAO.dolar_diario f
		where b.data_uss(+) = a.dt_maior_saldev and
		f.data_uss(+) = c.dt_faturamento and
		a.cod_cliente = e.cod_cliente(+) and
		(a.nota_credito >= d.pc_nota_inic and a.nota_credito <= d.pc_nota_final) and
		a.cod_cliente=PM_CODCLI;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_SALDOPEDIDOS;
	
	
	PROCEDURE pr_CON_REPR_CLIENTE( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select a.cod_repres,b.pseudonimo,b.cod_filial,c.nome_filial
		from r_clie_repres a,
		representante b,
		filial c
		Where
		a.cod_cliente = PM_CODCLI and
		b.cod_filial=c.cod_filial and
		a.cod_repres = b.cod_repres
		Order by b.cod_filial,a.cod_repres;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_REPR_CLIENTE;
	
	
	PROCEDURE pr_CON_ULTIMACOMPRA( PM_CODCLI  IN NUMBER,
								   PM_MES     IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select
		substr(to_char(a.dt_digitacao,'DD/MM/RR'),1,8) "DT. PEDIDO",
		substr(to_char(a.num_pedido,'0999999'),2,7) "NUM. PEDIDO",
		a.seq_pedido "SEQ. PEDIDO",
		nvl(a.num_nota,0) "NUM. NF",
		nvl(b.desc_plano,' ') "DESC. PLANO",
		to_char(a.vl_contabil,'FM999,999,990.00') "VL. CONT�BIL",
		nvl(decode(a.cod_vend,null,d.pseudonimo,c.pseudonimo),' ') "RESP.",
		to_char(a.cod_loja,'09') || '-'|| f.nome_fantasia "DEP�SITO"
		from pednota_venda a,
		plano_pgto b,
		representante c,
		representante d,
		datas e,
		loja f
		where a.cod_cliente = PM_CODCLI and
		a.dt_digitacao >= add_months(e.dt_faturamento,PM_MES) and
		a.dt_digitacao < e.dt_faturamento + 1 and
		a.situacao=0 and
		a.cod_plano=b.cod_plano(+) and
		a.cod_vend=c.cod_repres(+) and
		a.cod_repres=d.cod_repres(+) and
		a.cod_loja = f.cod_loja(+)
		order by a.dt_digitacao desc;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_ULTIMACOMPRA;
	
	
	PROCEDURE pr_CON_DUPLVENCIDAS( PM_CODCLI  IN NUMBER,
								   PM_CURSOR1 IN OUT tp_Cursor,
		  						   PM_CODERRO OUT NUMBER,
		  						   PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select a.num_fatura "NUM. FATURA",
		to_char(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago,'FM999,999,999,999.00') "VALOR",
		a.num_ordem "NUM. ORDEM",
		TO_CHAR(a.dt_vencimento,'DD/MM/RR') "DT. VENCIMENTO",
		a.cod_banco "BANCO",
		nvl((datas.dt_faturamento - a.dt_vencimento),0) "ATRASO",
		nvl(a.tp_pagamento,' ') "SITUA��O",
		to_char(a.cod_loja,'09') ||'-'||c.nome_fantasia "DEP�SITO" 
		from PRODUCAO.datas, cobranca.duplicatas a, PRODUCAO.banco b, PRODUCAO.loja c
		where a.dt_vencimento < datas.dt_faturamento and
		a.situacao_pagto=0  and
		a.cod_banco=b.cod_banco(+) and
		a.cod_loja = c.cod_loja and
		a.cod_cliente = PM_CODCLI
		order by a.dt_vencimento,a.num_fatura;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_DUPLVENCIDAS;
	
	
	PROCEDURE pr_CON_DUPLVENCER( PM_CODCLI  IN NUMBER,
								 PM_CURSOR1 IN OUT tp_Cursor,
		  						 PM_CODERRO OUT NUMBER,
		  						 PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT
		a.num_fatura "NUM. FATURA",
		to_char(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago,'FM999,999,999,999.00') "VALOR",
		a.num_ordem "NUM. ORDEM",
		TO_CHAR(a.dt_vencimento,'DD/MM/RR') "DT. VENCIMENTO",
		a.cod_banco "BANCO",
		nvl((datas.dt_faturamento - a.dt_vencimento),0) "ATRASO",
		nvl(a.tp_pagamento,' ') "SITUA��O",
		to_char(a.cod_loja,'09') ||'-'||c.nome_fantasia "DEP�SITO" 
		from PRODUCAO.datas, cobranca.duplicatas a, PRODUCAO.banco b, PRODUCAO.loja c
		where a.dt_vencimento >= datas.dt_faturamento and
		a.situacao_pagto=0  and
		a.cod_banco=b.cod_banco(+) and
		a.cod_loja = c.cod_loja and
		a.cod_cliente = PM_CODCLI
		order by a.dt_vencimento,a.num_fatura;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_DUPLVENCER;
	
	
	PROCEDURE pr_CON_ENDERECOS( PM_CODCLI  IN NUMBER,
								PM_TPEND   IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select substr(to_char(a.tp_endereco),1,1) "TP. ENDERE�O",
		substr(to_char(a.sequencia),1,6) "SEQUENCIA",
		a.nome_cliente "NOME",
		a.endereco "ENDERE�O",
		b.nome_cidade "CIDADE",
		a.bairro "BAIRRO",
		substr(to_char(a.cep),1,8) "CEP",
		substr(to_char(a.ddd),1,4) "DDD",
		substr(to_char(a.fone),1,9) "FONE",
		substr(to_char(a.fax),1,13) "FAX",
		substr(to_char(a.cxpostal),1,5) "CX. POSTAL",
		substr(to_char(a.tp_docto),1,1) "TP. DOC.",
		substr(to_char(a.cgc),1,14) "CGC",
		a.inscr_estadual "INSCR. EST.",
		b.cod_uf "UF"
		from PRODUCAO.clie_endereco a, PRODUCAO.cidade b
		where a.cod_cidade=b.cod_cidade and
		a.cod_cliente = PM_CODCLI and a.tp_endereco=PM_TPEND;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_ENDERECOS;
	
	
	PROCEDURE pr_ALT_DADOS( PM_CODCLI  IN NUMBER,
							PM_CODTRAN IN NUMBER,
							PM_DDD1    IN NUMBER,
							PM_FONE1   IN NUMBER,
							PM_DDD2    IN NUMBER,
							PM_FONE2   IN NUMBER,
							PM_FAX     IN NUMBER,
							PM_TELEX   IN NUMBER,
							PM_CONTATO IN VARCHAR2,
			  				PM_CODERRO OUT NUMBER,
			  				PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		UPDATE PRODUCAO.CLIENTE SET
		COD_TRANSP = PM_CODTRAN,
		DDD1  = PM_DDD1,
		FONE1 = PM_FONE1,
		DDD2  = PM_DDD2,
		FONE2 = PM_FONE2,
		TELEX = PM_TELEX, 
		FAX   = PM_FAX,
		NOME_CONTATO = PM_CONTATO
		where COD_CLIENTE = PM_CODCLI;
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_ALT_DADOS;
	
	
	PROCEDURE pr_CON_TRANSPORTADORAS( PM_CODTRANS IN NUMBER,
									  PM_CURSOR1  IN OUT tp_Cursor,
			  						  PM_CODERRO  OUT NUMBER,
			  						  PM_TXTERRO  OUT VARCHAR2) AS
			  						  
		STRSQL varchar2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := ' Select cod_transp "C�DIGO", ';
		STRSQL := STRSQL || ' nvl(nome_transp,'' '') "TRANSP." ';
		STRSQL := STRSQL || ' from transportadora ';
		STRSQL := STRSQL || ' where situacao=0';
		
		IF PM_CODTRANS <> 0 THEN
			STRSQL := STRSQL || ' and cod_transp = ' || PM_CODTRANS;
		END IF;
		
		STRSQL := STRSQL || ' order by nome_transp ';
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_TRANSPORTADORAS;
	
	
	PROCEDURE pr_CON_ACUMCOPRAS( PM_CODCLI  IN NUMBER,
								 PM_DATA1   IN VARCHAR2,
								 PM_DATA2   IN VARCHAR2,
								 PM_DATA3   IN VARCHAR2,
								 PM_DATA4   IN VARCHAR2,
								 PM_DTFINAL IN VARCHAR2,
								 PM_CURSOR1 IN OUT tp_Cursor,
			  					 PM_CODERRO OUT NUMBER,
								 PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		Select 
		SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA1,VL_CONTABIL, 0)) vl_1,
				SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA1,1, 0)) qtd_1,
		SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA2,VL_CONTABIL, 0)) vl_2,
				SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA2,1, 0)) qtd_2,
		SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA3,VL_CONTABIL, 0)) vl_3,
				SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA3,1, 0)) qtd_3,
		SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA4,VL_CONTABIL, 0)) vl_4,
				SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),PM_DATA4,1, 0)) qtd_4
		from pednota_venda 
		WHERE TP_TRANSACAO=1
		AND SITUACAO=0
		AND FL_GER_NFIS||NULL='S'
		AND DT_EMISSAO_NOTA >= to_date(PM_DATA4,'mm-rrrr') 
		AND DT_EMISSAO_NOTA < to_date(PM_DTFINAL,'mm-rrrr')
		AND COD_CLIENTE=PM_CODCLI;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_ACUMCOPRAS;
	
	PROCEDURE pr_CON_FORN3MESES( PM_CODCLI  IN NUMBER,
								 PM_CURSOR1 IN OUT tp_Cursor,
			  					 PM_CODERRO OUT NUMBER,
								 PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select c.cod_fornecedor,e.sigla
		from pednota_venda a,
		itpednota_venda b,
		item_cadastro c,
		datas d,
		fornecedor e
		Where
		a.cod_cliente = PM_CODCLI and
		a.fl_ger_nfis||''='S' and
		a.dt_emissao_nota >= add_months(d.dt_faturamento,-3) and
		a.dt_emissao_nota < dt_faturamento + 1 and
		a.tp_transacao=1 and
		a.situacao =0 and
		b.situacao=0 and
		c.cod_fornecedor=e.cod_fornecedor and
		b.cod_dpk=c.cod_dpk and
		a.seq_pedido=b.seq_pedido and
		a.num_pedido=b.num_pedido and
		a.cod_loja = b.cod_loja
		Group by c.cod_fornecedor,E.SIGLA
		Order by E.SIGLA;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_FORN3MESES;
	
	
	PROCEDURE pr_CON_PED3MESES( PM_CODCLI  IN NUMBER,
								PM_CODFORN IN NUMBER,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
								PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select c.cod_fornecedor "C�D. FORN.",
		e.sigla "FORNECEDOR",
		c.cod_fabrica "C�D. F�BRICA",
		c.desc_item "ITEM",
		b.cod_dpk "C�D. DPK"
		from pednota_venda a,
		itpednota_venda b,
		item_cadastro c,
		datas d,
		fornecedor e
		Where
		c.cod_fornecedor+0 = PM_CODFORN and
		a.cod_cliente = PM_CODCLI and
		a.fl_ger_nfis||'' = 'S' and
		a.dt_emissao_nota >= add_months(d.dt_faturamento,-3) and
		a.dt_emissao_nota < dt_faturamento + 1 and
		a.tp_transacao=1 and
		a.situacao =0 and
		b.situacao=0 and
		c.cod_fornecedor=e.cod_fornecedor and
		b.cod_dpk=c.cod_dpk and
		a.seq_pedido=b.seq_pedido and
		a.num_pedido=b.num_pedido and
		a.cod_loja = b.cod_loja
		Group by c.cod_fornecedor,e.sigla,c.cod_fabrica,c.desc_item,
		b.cod_dpk
		Order by c.cod_fornecedor,e.sigla,c.cod_fabrica,c.desc_item,
		b.cod_dpk;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PED3MESES;
	
	
	PROCEDURE pr_CON_CLIENTECIDADE( PM_CODCIDADE IN NUMBER,
									PM_CURSOR1   IN OUT tp_Cursor,
			  						PM_CODERRO   OUT NUMBER,
									PM_TXTERRO   OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select 
		a.cod_cliente "C�D. CLIENTE", 
		a.nome_cliente "CLIENTE",
		nvl(SUM(b.vl_duplicata - b.vl_desconto - b.vl_abatimento -b.vl_devolucao - b.vl_pago),0) "DUPLICATA"
		from cliente a,cobranca.duplicatas b
		where a.cod_cidade = PM_CODCIDADE and 
		a.situacao=0 and 
		b.cod_cliente(+) = a.cod_cliente and
		b.situacao_pagto(+) = 0
		group by a.nome_cliente,a.cod_cliente
		order by "DUPLICATA" DESC,a.nome_cliente;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_CLIENTECIDADE;
	
		
	PROCEDURE pr_CON_CLIENTESEMCOMPRA( PM_CODVEND IN NUMBER,
									   PM_CURSOR1 IN OUT tp_Cursor,
			  						   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		Select a.cod_cliente "C�D. CLIENTE",
		a.nome_cliente "RAZ�O SOCIAL",
		b.nome_cidade "CIDADE",
		b.cod_uf "UF",
		a.classificacao "TP. FIEL",
		e.desc_caracteristica "TIPO",
		nvl(to_char(c.dt_ult_compra,'dd/mm/rr'),'00/00/00') "ULT. COMPRA",
		to_char(a.ddd1,'9999') "DDD", 
		to_char(a.Fone1,'999999999') "FONE",
		f.cod_repres "RESPONS�VEL"
		From cliente a, cidade b, clie_credito c, datas d, 
		cliente_caracteristica e, r_clie_repres f
		Where a.situacao = 0 and 
		f.cod_repres in (Select cod_repres
						 From r_repven
						 Where cod_vend = PM_CODVEND)
		and a.classificacao <> 'X' 
		and (c.dt_ult_compra >= add_months(d.dt_faturamento,-3) 
		and c.dt_ult_compra < to_date(d.dt_fin_fech_mensal,'dd-mon-rr')+1) 
		and a.caracteristica = e.caracteristica
		and a.cod_cliente = f.cod_cliente 
		and a.cod_cliente = c.cod_cliente
		and a.cod_cidade = b.cod_cidade 
		Order by f.cod_repres,c.dt_ult_compra;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_CLIENTESEMCOMPRA;
	
	PROCEDURE pr_CON_DICAS( PM_CODCLI  IN NUMBER,
							PM_CURSOR1 IN OUT tp_Cursor,
			  				PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select sequencia, to_char(a.dt_dica,'dd/mm/RR hh24:mi') dt_dica, a.cod_vend,
		a.dica, b.pseudonimo
		from vendas.dicas a, representante b
		where cod_cliente = PM_CODCLI 
		and a.cod_vend = b.cod_repres
		Order by dt_dica desc;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_DICAS;
	
	PROCEDURE pr_ALT_DICAS( PM_CODCLI  IN NUMBER,
							PM_CODDICA IN NUMBER,
							PM_CODREPR IN NUMBER,
							PM_DICA    IN VARCHAR2,
							PM_ACT     IN VARCHAR2,
			  				PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
		
			INSERT INTO VENDAS.DICAS VALUES
			((SELECT MAX(SEQUENCIA)+1 FROM VENDAS.DICAS),PM_CODCLI,SYSDATE,PM_CODREPR,PM_DICA);
		
		ELSIF PM_ACT = 'A' THEN
		
			UPDATE VENDAS.DICAS SET
			DT_DICA = SYSDATE,
			COD_VEND = PM_CODREPR,
			DICA = PM_DICA
			WHERE
			SEQUENCIA = PM_CODDICA;
		
		ELSIF PM_ACT = 'D' THEN
		
			DELETE VENDAS.DICAS WHERE SEQUENCIA = PM_CODDICA;
		
		END IF;
		COMMIT;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_ALT_DICAS;
	
	
	--**************************						
	--- ANALISTA : MARICI 	
	--- DATA	 : 08/11/2002
	--**************************
  PROCEDURE PR_DATA(p_cursor   IN OUT tp_cursor,
                    p_erro     OUT    NUMBER)
  IS
    
  BEGIN
  
    p_erro := 0;
    
    --Abrindo Cursor
    OPEN p_cursor FOR SELECT TO_CHAR(DT_FATURAMENTO,'DD/MM/RR') DT_FATURAMENTO,
			TO_CHAR(DT_REAL,'DD/MM/RR') DT_REAL FROM DATAS;
    
    
    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;

  
  END;
  
  PROCEDURE PR_CLIENTE(p_cursor      IN OUT tp_cursor,
                        p_cod_cliente IN     cliente.cod_cliente%TYPE,
                        p_cgc	      IN     cliente.cgc%TYPE,
                        p_erro        OUT    NUMBER)
                        
   IS
    
  BEGIN
  
    p_erro := 0;
    
    --Abrindo Cursor
    IF p_cod_cliente <> 0 THEN
      OPEN p_cursor FOR Select substr(to_char(a.cod_cliente,'FM9999990'),1,7)
		cod_cliente,
		a.nome_cliente nome_cliente, a.cgc cgc,
		nvl(a.endereco,' ') endereco, nvl(a.bairro,' ') bairro,
		b.nome_cidade nome_cidade, b.cod_uf cod_uf,
		NVL(substr(to_char(a.cep,'fm999999990'),1,9),0) cep,
		NVL(substr(to_char(a.cxpostal,'fm999999990'),1,6),0) cxpostal,
		NVL(substr(to_char(a.ddd1,'FM99990'),1,5),0) ddd1,
		NVL(substr(to_char(a.fone1,'FM999999990'),1,9),0) fone1,
		NVL(substr(to_char(a.ddd2,'FM99990'),1,5),0) ddd2,
		NVL(substr(to_char(a.fone2,'FM999999990'),1,9),0) fone2,
		NVL(substr(to_char(a.fax,'FM999999999990'),1,12),0) fax,
		NVL(a.nome_contato,' ') nome_contato,
		NVL(substr(to_char(a.cod_banco,'FM9990'),1,4),0) cod_banco,
		NVL(c.sigla,' ') sigla,
		NVL(substr(to_char(a.cod_transp,'FM99990'),1,5),0) cod_transp,
		NVL(d.nome_transp,0) nome_transp,
		NVL(substr(to_char(g.dt_ult_compra,'DD/MM/RR'),1,8),' ')dt_ult_compra,
		NVL(substr(to_char(a.dt_cadastr,'DD/MM/RR'),1,8),' ')dt_cadastr,
		NVL(h.desc_tipo_cli,' ')desc_tipo_cli1,
		NVL(i.desc_tipo_cli,' ')desc_tipo_cli2,
		NVL(a.classificacao,' ')classificacao,
		a.situacao situacao,
		a.fl_cons_final fl_cons_final,
		nvl(a.inscr_estadual,' ')inscr_estadual,
		nvl(a.inscr_suframa,' ')inscr_suframa,
		nvl(a.dt_tare,'') dt_tare,
		nvl(a.cod_tare,0) cod_tare,
		nvl(g.nota_credito,0) nota_credito,
		nvl(a.cod_mensagem,0) cod_mensagem,
		nvl(a.cod_mensagem_fiscal,0) cod_mensagem_fiscal,
		j.fl_bloqueio fl_bloqueio,
		nvl(j.desc_mens,' ') desc_mens,
		nvl(m.desc_mens,' ') desc_mens_fiscal,
		l.desc_caracteristica caracteristica
		From cliente a, cidade b, banco c, transportadora d,
		clie_credito g,
		tipo_cliente h, tipo_cliente_blau i, clie_mensagem j,
		cliente_caracteristica l, clie_mensagem m
		Where c.cod_banco(+)=a.cod_banco and
		d.cod_transp(+)=a.cod_transp and
		g.cod_cliente(+)=a.cod_cliente and
		l.caracteristica(+)=a.caracteristica and
		m.cod_mensagem(+)=a.cod_mensagem_fiscal and
		j.cod_mensagem(+)=a.cod_mensagem and
		h.cod_tipo_cli(+)=a.cod_tipo_cliente and
		i.cod_tipo_cli(+)=a.cod_tipo_clblau and
		b.cod_cidade(+)=a.cod_cidade and
		a.cod_cliente=p_cod_cliente;
      
    ELSE
    	OPEN p_cursor FOR Select substr(to_char(a.cod_cliente,'FM9999990'),1,7)
    		cod_cliente,
    		a.nome_cliente nome_cliente, a.cgc cgc,
    		nvl(a.endereco,' ') endereco, nvl(a.bairro,' ') bairro,
    		b.nome_cidade nome_cidade, b.cod_uf cod_uf,
    		NVL(substr(to_char(a.cep,'fm999999990'),1,9),0) cep,
    		NVL(substr(to_char(a.cxpostal,'fm999999990'),1,6),0) cxpostal,
    		NVL(substr(to_char(a.ddd1,'FM99990'),1,5),0) ddd1,
    		NVL(substr(to_char(a.fone1,'FM999999990'),1,9),0) fone1,
    		NVL(substr(to_char(a.ddd2,'FM99990'),1,5),0) ddd2,
    		NVL(substr(to_char(a.fone2,'FM999999990'),1,9),0) fone2,
    		NVL(substr(to_char(a.fax,'FM999999999990'),1,12),0) fax,
    		NVL(a.nome_contato,' ') nome_contato,
    		NVL(substr(to_char(a.cod_banco,'FM9990'),1,4),0) cod_banco,
    		NVL(c.sigla,' ') sigla,
    		NVL(substr(to_char(a.cod_transp,'FM99990'),1,5),0) cod_transp,
    		NVL(d.nome_transp,0) nome_transp,
    		NVL(substr(to_char(g.dt_ult_compra,'DD/MM/RR'),1,8),' ')dt_ult_compra,
    		NVL(substr(to_char(a.dt_cadastr,'DD/MM/RR'),1,8),' ')dt_cadastr,
    		NVL(h.desc_tipo_cli,' ')desc_tipo_cli1,
    		NVL(i.desc_tipo_cli,' ')desc_tipo_cli2,
    		NVL(a.classificacao,' ')classificacao,
    		a.situacao situacao,
    		a.fl_cons_final fl_cons_final,
    		nvl(a.inscr_estadual,' ')inscr_estadual,
    		nvl(a.inscr_suframa,' ')inscr_suframa,
    		nvl(a.dt_tare,'') dt_tare,
    		nvl(a.cod_tare,0) cod_tare,
    		nvl(g.nota_credito,0) nota_credito,
    		nvl(a.cod_mensagem,0) cod_mensagem,
    		nvl(a.cod_mensagem_fiscal,0) cod_mensagem_fiscal,
    		j.fl_bloqueio fl_bloqueio,
    		nvl(j.desc_mens,' ') desc_mens,
    		nvl(m.desc_mens,' ') desc_mens_fiscal,
    		l.desc_caracteristica caracteristica
    		From cliente a, cidade b, banco c, transportadora d,
    		clie_credito g,
    		tipo_cliente h, tipo_cliente_blau i, clie_mensagem j,
    		cliente_caracteristica l, clie_mensagem m
    		Where c.cod_banco(+)=a.cod_banco and
    		d.cod_transp(+)=a.cod_transp and
    		g.cod_cliente(+)=a.cod_cliente and
    		l.caracteristica(+)=a.caracteristica and
    		m.cod_mensagem(+)=a.cod_mensagem_fiscal and
    		j.cod_mensagem(+)=a.cod_mensagem and
    		h.cod_tipo_cli(+)=a.cod_tipo_cliente and
    		i.cod_tipo_cli(+)=a.cod_tipo_clblau and
    		b.cod_cidade(+)=a.cod_cidade and
		a.cgc=p_cgc;
    
    END IF;
    
    
    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;

  
  END;
                        

  PROCEDURE PR_CLIENTE1(p_cursor       IN OUT tp_cursor,
                         p_nome_cliente IN     cliente.nome_cliente%TYPE,
                         p_erro        OUT     NUMBER)

  IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select a.nome_cliente nome_cliente,
   		    a.cod_cliente cod_cliente,
   		    b.nome_cidade nome_cidade,
   		    b.cod_uf cod_uf
   		    from cliente a, cidade b
    		    where a.cod_cidade=b.cod_cidade and
    		     a.nome_cliente like p_nome_cliente;
      
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;

 PROCEDURE PR_DUPL(p_cursor      IN OUT tp_cursor,
                   p_cod_cliente IN     cliente.cod_cliente%TYPE,
 		   p_erro        OUT    NUMBER)
  IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select 'a' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto -
 			a.vl_abatimento - a.vl_devolucao - a.vl_pago),
 			'FM999,999,999,999.00'),0)valor,
 			nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
 			from datas, cobranca.duplicatas a
			where a.dt_vencimento >= datas.dt_faturamento and
			 a.situacao_pagto=0  and 
			 a.cod_cliente = p_cod_cliente
			 union
			select 'b' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto -
			a.vl_abatimento - a.vl_devolucao - a.vl_pago),
			'FM999,999,999,999.00'),0)valor,
			nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
			from datas, cobranca.duplicatas a
			where a.dt_vencimento < datas.dt_faturamento and
			a.situacao_pagto=0  and 
			a.cod_cliente = p_cod_cliente
			order by 1;
      
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
  
  END;                     
 
 PROCEDURE PR_PEDIDO(p_cursor      IN OUT tp_cursor,
                     p_cod_cliente IN     cliente.cod_cliente%TYPE,
                     p_mes         IN     NUMBER,
                     p_erro        OUT    NUMBER)
  IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select substr(to_char(a.dt_digitacao,'DD/MM/RR'),1,8) dt_pedido,
 		substr(to_char(a.num_pedido,'0999999'),2,7) num_pedido,
		 a.seq_pedido seq_pedido,
 		nvl(a.num_nota,0) num_nota,
 		nvl(b.desc_plano,' ') desc_plano,
 		 to_char(a.vl_contabil,'FM999,999,990.00') vl_contabil,
  		nvl(decode(a.cod_vend,null,d.pseudonimo,c.pseudonimo),' ') responsavel,
  		to_char(a.cod_loja,'09') || '-'|| f.nome_fantasia deposito 
  		from pednota_venda a,
  		plano_pgto b,
  		representante c,
		 representante d,
		 datas e,
		 loja f
		 where a.cod_cliente = p_cod_cliente and
		 a.dt_digitacao >= add_months(e.dt_faturamento,p_mes) and
		 a.dt_digitacao < e.dt_faturamento + 1 and
		 a.situacao=0 and
		 a.cod_plano=b.cod_plano(+) and
		 a.cod_vend=c.cod_repres(+) and
		 a.cod_repres=d.cod_repres(+) and
		 a.cod_loja = f.cod_loja(+)
		 order by a.dt_digitacao desc;

      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;                     
                    
  PROCEDURE PR_DUPL1(p_cursor      IN OUT tp_cursor,
                      p_cod_cliente IN     cliente.cod_cliente%TYPE,
                      p_erro        OUT    NUMBER)

  IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select a.num_fatura num_fatura,to_char(a.vl_duplicata - a.vl_desconto -
	 a.vl_abatimento - a.vl_devolucao - a.vl_pago,
	'FM999,999,999,999.00') valor,
	a.num_ordem num_ordem,
	 TO_CHAR(a.dt_vencimento,'DD/MM/RR') dt_vencimento,
	 a.cod_banco cod_banco,
	 b.sigla sigla,
	 to_char(a.cod_loja,'09') ||'-'|| c.nome_fantasia deposito 
	 from datas, cobranca.duplicatas a, banco b, loja c
	 where a.dt_vencimento >= datas.dt_faturamento and
	 a.situacao_pagto=0  and
	 a.cod_banco=b.cod_banco(+) and
	 a.cod_loja = c.cod_loja and
	 a.cod_cliente = p_cod_cliente
	 order by a.dt_vencimento,a.num_fatura;
           
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;                     


PROCEDURE PR_DUPL2(p_cursor      IN OUT tp_cursor,
                  p_cod_cliente IN     cliente.cod_cliente%TYPE,
                  p_erro        OUT    NUMBER)    


  IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select a.num_fatura num_fatura,to_char(a.vl_duplicata - a.vl_desconto -
		 a.vl_abatimento - a.vl_devolucao - a.vl_pago,
		 'FM999,999,999,999.00') valor,
		 a.num_ordem num_ordem,
		 TO_CHAR(a.dt_vencimento,'DD/MM/RR') dt_vencimento,
		 a.cod_banco cod_banco,
		 nvl((datas.dt_faturamento - a.dt_vencimento),0) atraso,
		nvl(a.tp_pagamento,' ') situacao,
		 to_char(a.cod_loja,'09') ||'-'||c.nome_fantasia deposito 
		 from datas, cobranca.duplicatas a, banco b, loja c
		 where a.dt_vencimento < datas.dt_faturamento and
		 a.situacao_pagto=0  and
		a.cod_banco=b.cod_banco(+) and
		a.cod_loja = c.cod_loja and
		a.cod_cliente = p_cod_cliente
		order by a.dt_vencimento,a.num_fatura;
           
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;                     

  PROCEDURE PR_ENDERECO(p_cursor      IN OUT tp_cursor,
                        p_cod_cliente IN     cliente.cod_cliente%TYPE,
                        p_tp_endereco IN     clie_endereco.tp_endereco%TYPE,
                        p_erro        OUT    NUMBER)
   IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select substr(to_char(a.tp_endereco),1,1) tp_endereco,
 		substr(to_char(a.sequencia),1,6) sequencia,
 		a.nome_cliente nome_cliente,
 		a.endereco endereco,
 		b.nome_cidade nome_cidade,
 		a.bairro bairro,
		substr(to_char(a.cep),1,8) cep,
		substr(to_char(a.ddd),1,4) ddd,
		substr(to_char(a.fone),1,9) fone,
		substr(to_char(a.fax),1,13) fax,
		substr(to_char(a.cxpostal),1,5) cxpostal,
		substr(to_char(a.tp_docto),1,1) tp_docto,
		substr(to_char(a.cgc),1,14) cgc,
		a.inscr_estadual inscr_estadual,
		b.cod_uf cod_uf
		from clie_endereco a, cidade b
		where a.cod_cidade=b.cod_cidade and
		a.cod_cliente = p_cod_cliente and a.tp_endereco = p_tp_endereco;
           
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;                      
    
  PROCEDURE PR_TRANSPORTADORA(p_cursor      IN OUT tp_cursor,
  			      p_cod_transp  IN     transportadora.cod_transp%TYPE,
                  	      p_erro        OUT    NUMBER)  
 
  IS
        
      BEGIN
      
        p_erro := 0;
        
        --Abrindo Cursor
        IF p_cod_transp = 0 THEN
          OPEN p_cursor FOR Select cod_transp cod_transp,
		 nvl(nome_transp,' ') nome_transp
		 From transportadora
		 where situacao=0
		 order by nome_transp;

        
        ELSE
	   OPEN p_cursor FOR Select cod_transp cod_transp,
		nvl(nome_transp,' ') nome_transp
		from transportadora
		where situacao=0 and cod_transp=p_cod_transp;
        
        END IF;
        
        
        
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
    
      
  END;                      
 
 PROCEDURE PR_CLIE_TRANSP(p_cursor      IN OUT tp_cursor,
 		          p_cod_cliente IN     cliente.cod_cliente%TYPE,
                          p_erro        OUT    NUMBER)
                    
 IS
        
      BEGIN
      
        p_erro := 0;
        
        --Abrindo Cursor
       
        OPEN p_cursor FOR Select nvl(a.cod_transp,0) cod_transp,
		nvl(nome_transp,' ') nome_transp,
		NVL(a.ddd1,0) ddd1,
		NVL(a.fone1,0) fone1,
		NVL(a.ddd2,0) ddd2,
		NVL(a.fone2,0) fone2,
		NVL(a.fax,0) fax,
		NVL(a.nome_contato,' ') nome_contato,
		NVL(a.telex,0) telex
		from cliente a, transportadora b
		where b.cod_transp(+)=a.cod_transp and a.cod_cliente=p_cod_cliente;
	 
        
        
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
    
      
  END;                                         
 
 PROCEDURE PR_CLIE_SALDO(p_cursor      IN OUT tp_cursor,
  		         p_cod_cliente IN     cliente.cod_cliente%TYPE,
                         p_erro        OUT    NUMBER)
 IS
         
       BEGIN
       
         p_erro := 0;
         
         --Abrindo Cursor
        
         OPEN p_cursor FOR Select nvl((0.75 * nvl(a.maior_saldev,0) * 
			(f.valor_uss/decode(nvl(b.valor_uss,0),0,f.valor_uss,
			b.valor_uss))) * d.pc_limite / 100,0) valor1,
			nvl(e.saldo_pedidos,0) saldo_pedidos
			from clie_credito a, dolar_diario b, datas c, 
			pont_credito d, v_saldo_pedidos e, dolar_diario f
			where b.data_uss(+) = a.dt_maior_saldev and
			f.data_uss(+) = c.dt_faturamento and
			a.cod_cliente = e.cod_cliente(+) and
			(a.nota_credito >= d.pc_nota_inic and 
			a.nota_credito <= d.pc_nota_final) and
			a.cod_cliente=p_cod_cliente;
         
         
         EXCEPTION
         WHEN OTHERS THEN
         p_erro := SQLCODE;
     
       
  END;                      
 
 PROCEDURE PR_CLIE_DICA(p_cursor      IN OUT tp_cursor,
  		        p_cod_cliente IN     cliente.cod_cliente%TYPE,
                        p_erro        OUT    NUMBER)                        

 IS
          
        BEGIN
        
          p_erro := 0;
          
          --Abrindo Cursor
         
          OPEN p_cursor FOR select sequencia, to_char(a.dt_dica,'dd/mm/RR hh24:mi') dt_dica, a.cod_vend,
 			a.dica, b.pseudonimo
 			from vendas.dicas a, representante b
 			where cod_cliente = p_cod_cliente and a.cod_vend = b.cod_repres
 			Order by dt_dica desc;
          
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
      
        
  END;                      
 
 PROCEDURE PR_UF(p_cursor      IN OUT tp_cursor,
 		 p_erro        OUT    NUMBER)
 		 
 IS         
        BEGIN
        
          p_erro := 0;
          
          --Abrindo Cursor
         
          OPEN p_cursor FOR Select cod_uf from uf order by cod_uf;
          
          
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
      
        
  END;                      		 
 
 PROCEDURE PR_CIDADE(p_cursor      IN OUT tp_cursor,
 		     p_cod_uf      IN     uf.cod_uf%TYPE,
 		     p_erro        OUT    NUMBER)
 
 IS         
         BEGIN
         
           p_erro := 0;
           
           --Abrindo Cursor
          
           OPEN p_cursor FOR Select nome_cidade,cod_cidade 
			From cidade
			 where cod_uf = p_cod_uf
			 order by nome_cidade;
           
           
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
       
         
  END;              
 
 PROCEDURE PR_ATUDICA(p_cod_cliente IN     cliente.cod_cliente%TYPE,
 		     p_sequencia   IN     vendas.dicas.sequencia%TYPE,
 		     p_dica        IN     vendas.dicas.dica%TYPE,
 		     p_cod_vend    IN     vendas.dicas.cod_vend%TYPE,
 		     p_dt_dica     IN     vendas.dicas.dt_dica%TYPE,
 		     p_tipo	   IN     NUMBER,
		     p_erro        OUT    NUMBER)
 
  IS         
          BEGIN
          
            p_erro := 0;
            
            IF p_tipo = 0 THEN
              Update vendas.dicas set
	       dt_dica = p_dt_dica,
	       cod_vend = p_cod_vend,
	       dica = p_dica
		where cod_cliente=p_cod_cliente and
		sequencia=p_sequencia;
		COMMIT;
            ELSIF p_tipo =1 THEN
              Delete from vendas.dicas 
 		where cod_cliente=p_cod_cliente and sequencia=p_sequencia;
 		COMMIT;
            ELSE
              Insert into vendas.dicas 
		values(vendas.seq_dicas.nextval,p_cod_cliente,p_dt_dica,p_cod_vend, p_dica);
		COMMIT;
            END IF;
            
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
   END;              
 
  PROCEDURE PR_ATUCLI(p_cod_cliente    IN     cliente.cod_cliente%TYPE,
	  	      p_cod_transp     IN     cliente.cod_transp%TYPE,
		      p_ddd1           IN     cliente.ddd1%TYPE,
		      p_ddd2           IN     cliente.ddd2%TYPE,
		      p_fone1          IN     cliente.fone1%TYPE,
		      p_fone2          IN     cliente.fone2%TYPE,
		      p_fax            IN     cliente.fax%TYPE,
		      p_telex          IN     cliente.telex%TYPE,
		      p_contato        IN     cliente.nome_contato%TYPE,
                      p_erro           OUT    NUMBER)
 
  IS         
           BEGIN
           
             p_erro := 0;
              Update cliente set cod_transp=p_cod_transp,
		 ddd1=nvl(p_ddd1,0),
		 ddd2=nvl(p_ddd2,0),
		 fone1=nvl(p_fone1,0),
		 fone2=nvl(p_fone2,0),
		 fax=nvl(p_fax,0),
		 telex=nvl(p_telex,0),
		 nome_contato=nvl(p_contato,' ')
		 where cod_cliente=p_cod_cliente; 
                 COMMIT;
               
             EXCEPTION
             WHEN OTHERS THEN
             p_erro := SQLCODE;
         
           
   END;             
 
 PROCEDURE PR_DATA_MES(p_cursor      IN OUT tp_cursor,
 		       p_erro        OUT    NUMBER)
 		      
 		      
 IS         
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
           
            OPEN p_cursor FOR SELECT TO_CHAR(DT_FATURAMENTO,'MM-RRRR') mes1, 
            TO_CHAR(ADD_MONTHS(DT_FATURAMENTO,-1),'MM-RRRR') mes2,
            TO_CHAR(ADD_MONTHS(DT_FATURAMENTO,-2),'MM-RRRR') mes3,
            TO_CHAR(ADD_MONTHS(DT_FATURAMENTO,-3),'MM-RRRR') mes4,
            TO_CHAR(ADD_MONTHS(DT_FATURAMENTO,1),'MM-RRRR') final 
            From DATAS;

            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
  END;              
 
 PROCEDURE PR_VENDA_MES(p_cursor      IN OUT tp_cursor,
 		       p_cod_cliente IN     cliente.cod_cliente%TYPE,
 		       p_dt1         IN     VARCHAR2,
		       p_dt2         IN     VARCHAR2,
	  	       p_dt3         IN     VARCHAR2,
		       p_dt4         IN     VARCHAR2,
		       p_dt_final    IN     VARCHAR2,
 		       p_erro        OUT    NUMBER)
 		       
 IS         
           BEGIN
           
             p_erro := 0;
             
             --Abrindo Cursor
            
             OPEN p_cursor FOR Select to_char(SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'),
             p_dt1,VL_CONTABIL, 0)),'999,999,999,999') vl_1,
             SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt1,1, 0)) qtd_1, 
             to_char(SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt2,VL_CONTABIL, 0)), 
             '999,999,999,999') vl_2, SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt2,1,
             0)) qtd_2, to_char(SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt3,
             VL_CONTABIL, 0)),'999,999,999,999') vl_3, 
             SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt3,1, 0)) qtd_3, 
             to_char(SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt4,
             VL_CONTABIL, 0)),'999,999,999,999') vl_4, 
             SUM(DECODE(to_char(dt_emissao_nota, 'mm-rrrr'), p_dt4,1, 0)) qtd_4
             from pednota_venda  
             WHERE TP_TRANSACAO=1 AND SITUACAO=0 AND
             FL_GER_NFIS||NULL='S' AND DT_EMISSAO_NOTA >= to_date(p_DT4,'mm-rrrr')  AND 
             DT_EMISSAO_NOTA < to_date(p_DT_FINAL,'mm-rrrr') AND COD_CLIENTE=p_cod_cliente;

             
             
             EXCEPTION
             WHEN OTHERS THEN
             p_erro := SQLCODE;
         
           
  END;              
 
 PROCEDURE PR_DUPL_CIDADE(p_cursor      IN OUT tp_cursor,
  		          p_cod_cidade  IN     cidade.cod_cidade%TYPE,
                          p_erro        OUT    NUMBER)
 
 IS         
            BEGIN
            
              p_erro := 0;
              
              --Abrindo Cursor
             
              OPEN p_cursor FOR select a.nome_cliente,  a.cod_cliente,  
              nvl(SUM(b.vl_duplicata - b.vl_desconto - b.vl_abatimento - b.vl_devolucao -
              b.vl_pago),0) nr_duplicata 
              from cliente a,cobranca.duplicatas b 
              where a.cod_cidade = p_cod_cidade and  a.situacao=0 and
              b.cod_cliente(+) = a.cod_cliente and b.situacao_pagto(+) = 0 
              group by a.nome_cliente,a.cod_cliente 
              order by nr_duplicata DESC,a.nome_cliente;

 
              
              
              EXCEPTION
              WHEN OTHERS THEN
              p_erro := SQLCODE;
          
            
 END;              
 
 PROCEDURE PR_VENDA_FORN(p_cursor      IN OUT tp_cursor,
  		         p_cod_cliente IN     cliente.cod_cliente%TYPE,
 		         p_erro        OUT    NUMBER)
  
 IS         
             BEGIN
             
               p_erro := 0;
               
               --Abrindo Cursor
              
               OPEN p_cursor FOR  select c.cod_fornecedor,e.sigla 
               from pednota_venda a, itpednota_venda b, item_cadastro c, 
               datas d, fornecedor e 
               Where a.cod_cliente = p_cod_cliente and 
               a.fl_ger_nfis||''='S' and a.dt_emissao_nota >= add_months(d.dt_faturamento,-3)
               and a.dt_emissao_nota < dt_faturamento + 1 and 
               a.tp_transacao=1 and a.situacao =0 and b.situacao=0 and
               c.cod_fornecedor=e.cod_fornecedor and b.cod_dpk=c.cod_dpk and 
               a.seq_pedido=b.seq_pedido and a.num_pedido=b.num_pedido and 
               a.cod_loja = b.cod_loja Group by c.cod_fornecedor,E.SIGLA 
               Order by E.SIGLA;

               
               
               EXCEPTION
               WHEN OTHERS THEN
               p_erro := SQLCODE;
           
             
 END;              
 
 PROCEDURE PR_VENDA_ITEM(p_cursor      IN OUT tp_cursor,
  		        p_cod_cliente IN     cliente.cod_cliente%TYPE,
  		        p_cod_forn    IN     fornecedor.cod_fornecedor%TYPE,
 		        p_erro        OUT    NUMBER)
 		        
  
  IS         
              BEGIN
              
                p_erro := 0;
                
                --Abrindo Cursor
               
                OPEN p_cursor FOR   select c.cod_fornecedor,e.sigla,c.cod_fabrica,
                c.desc_item,b.cod_dpk 
                from pednota_venda a, itpednota_venda b,
                item_cadastro c, datas d, fornecedor e
                Where c.cod_fornecedor+0 = p_cod_forn and
                a.cod_cliente = p_cod_cliente and
                a.fl_ger_nfis||'' = 'S' and
                a.dt_emissao_nota >= add_months(d.dt_faturamento,-3) and 
                a.dt_emissao_nota < dt_faturamento + 1 and 
                a.tp_transacao=1 and a.situacao =0 and 
                b.situacao=0 and c.cod_fornecedor=e.cod_fornecedor and
                b.cod_dpk=c.cod_dpk and a.seq_pedido=b.seq_pedido and
                a.num_pedido=b.num_pedido and a.cod_loja = b.cod_loja
                Group by c.cod_fornecedor,e.sigla,c.cod_fabrica,
                c.desc_item, b.cod_dpk
                Order by c.cod_fornecedor,e.sigla,c.cod_fabrica,
                c.desc_item, b.cod_dpk;

 
                
                
                EXCEPTION
                WHEN OTHERS THEN
                p_erro := SQLCODE;
            
              
 END;              
 
 PROCEDURE PR_CLIE_REPRES(p_cursor      IN OUT tp_cursor,
  		         p_cod_cliente IN     cliente.cod_cliente%TYPE,
 		         p_erro        OUT    NUMBER)
 
 IS         
               BEGIN
               
                 p_erro := 0;
                 
                 --Abrindo Cursor
                
                 OPEN p_cursor FOR select a.cod_repres,b.pseudonimo,b.cod_filial,
                 	c.nome_filial
			from r_clie_repres a,
			representante b,
			filial c
			Where
			a.cod_cliente = p_cod_cliente and
			b.cod_filial=c.cod_filial and
			a.cod_repres = b.cod_repres
			Order by b.cod_filial,a.cod_repres;
                 
                 
                 EXCEPTION
                 WHEN OTHERS THEN
                 p_erro := SQLCODE;
             
               
 END;              
 
 PROCEDURE PR_ESQUECERAM(p_cursor      IN OUT tp_cursor,
  		         p_cod_vend   IN     representante.cod_repres%TYPE,
 		         p_erro       OUT    NUMBER)
 
 
  IS         
                BEGIN
                
                  p_erro := 0;
                  
                  --Abrindo Cursor
                 
                  OPEN p_cursor FOR Select a.cod_cliente Cod,a.nome_cliente Razao_Social, 
		b.nome_cidade Cidade,b.cod_uf UF,
		 a.classificacao Fiel, e.desc_caracteristica tipo,
		 nvl(to_char(c.dt_ult_compra,'dd/mm/rr'),'00/00/00') Ult_Compra, 
		to_char(a.ddd1,'9999') DDD,  to_char(a.Fone1,'999999999') Fone , 
		f.cod_repres responsavel
		 From cliente a, cidade b, clie_credito c, datas d, 
		 cliente_caracteristica e, r_clie_repres f 
		Where a.situacao = 0 and  
		f.cod_repres in 
		(Select cod_repres
		 From r_repven
		 where cod_vend = p_cod_vend
		 Union
		 Select cod_repres
		 From representante
		Where cod_repres = p_cod_vend) and 
		a.classificacao <> 'X'  and 
		(c.dt_ult_compra >= add_months(d.dt_faturamento,-3)  and 
		c.dt_ult_compra < to_date(d.dt_fin_fech_mensal,'dd-mon-rr')+1 ) and 
		a.caracteristica = e.caracteristica and 
		a.cod_cliente = f.cod_cliente  and 
		a.cod_cliente = c.cod_cliente  and 
		a.cod_cidade = b.cod_cidade  
		Order by f.cod_repres,c.dt_ult_compra;
	
                  
                  
                  EXCEPTION
                  WHEN OTHERS THEN
                  p_erro := SQLCODE;
              
                
 END;              
 

End PCK_VDA020;
---------------
/