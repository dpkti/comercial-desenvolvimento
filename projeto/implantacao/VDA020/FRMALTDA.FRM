VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmAltdados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Altera��o de Dados"
   ClientHeight    =   4245
   ClientLeft      =   1350
   ClientTop       =   2175
   ClientWidth     =   6705
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4245
   ScaleWidth      =   6705
   Begin VB.TextBox txtContato 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   15
      TabIndex        =   8
      Top             =   2160
      Width           =   1695
   End
   Begin VB.CommandButton Command2 
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3360
      TabIndex        =   14
      Top             =   3480
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&OK"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1440
      TabIndex        =   13
      Top             =   3480
      Width           =   975
   End
   Begin VB.TextBox txtCodtransp 
      Alignment       =   1  'Right Justify
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   0
      Top             =   360
      Width           =   615
   End
   Begin VB.TextBox txtTelex 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   8
      TabIndex        =   6
      Top             =   2160
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.TextBox txtFax 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   14
      TabIndex        =   5
      Top             =   1680
      Width           =   1695
   End
   Begin VB.TextBox txtFone2 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   9
      TabIndex        =   4
      Top             =   1200
      Width           =   1095
   End
   Begin VB.TextBox txtDdd2 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   3
      Top             =   1200
      Width           =   495
   End
   Begin VB.TextBox txtFone1 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   9
      TabIndex        =   2
      Top             =   840
      Width           =   1095
   End
   Begin VB.TextBox txtDdd1 
      Enabled         =   0   'False
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1560
      MaxLength       =   4
      TabIndex        =   1
      Top             =   840
      Width           =   495
   End
   Begin MSGrid.Grid Grid1 
      Height          =   1815
      Left            =   3360
      TabIndex        =   15
      Top             =   1080
      Visible         =   0   'False
      Width           =   3255
      _Version        =   65536
      _ExtentX        =   5741
      _ExtentY        =   3201
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "FRMALTDA.frx":0000
   End
   Begin VB.Label lblTransp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   2400
      TabIndex        =   16
      Top             =   360
      Width           =   4095
   End
   Begin VB.Label Label5 
      Caption         =   "Contato"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   2160
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "Telex"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2160
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label Label3 
      Caption         =   "Fax"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   1680
      Width           =   495
   End
   Begin VB.Label Label2 
      Caption         =   "Fone"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   1080
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Transportadora"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   360
      Width           =   1335
   End
End
Attribute VB_Name = "frmAltdados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command2_Click()
  Unload frmAltdados
  frmCliente.SetFocus
End Sub


Private Sub Command1_Click()

On Error GoTo Trata_Erro
  
  OraParameters.Remove "cod"
  OraParameters.Remove "tran"
  OraParameters.Remove "ddd1"
  OraParameters.Remove "fone1"
  OraParameters.Remove "ddd2"
  OraParameters.Remove "fone2"
  OraParameters.Remove "fax"
  OraParameters.Remove "telex"
  OraParameters.Remove "contato"
  
  
  OraParameters.Add "cod", frmCliente.txtCodigo.Text, 1
  OraParameters.Add "tran", frmAltdados.txtCodtransp.Text, 1
  OraParameters.Add "ddd1", frmAltdados.txtDdd1.Text, 1
  OraParameters.Add "fone1", frmAltdados.txtFone1.Text, 1
  OraParameters.Add "ddd2", frmAltdados.txtDdd2.Text, 1
  OraParameters.Add "fone2", frmAltdados.txtFone2.Text, 1
  OraParameters.Add "fax", frmAltdados.txtFax.Text, 1
  OraParameters.Add "telex", frmAltdados.txtTelex.Text, 1
  OraParameters.Add "contato", frmAltdados.txtContato.Text, 1
  
  
  V_SQL = "Begin producao.pck_vda020.pr_atucli(:cod,:tran,:ddd1,:ddd2,:fone1,:fone2,:fax,:telex,:contato,:vErro);END;"

  
  oradatabase.ExecuteSQL V_SQL
  
    
  If oradatabase.Parameters("vErro").Value <> 0 Then
    frmTrataerro.Show
    Unload frmAltdados
    Exit Sub
  End If
  
  
  MsgBox "Altera��o efetuada com sucesso", 0, "ATEN��O"
  Call DADOS_ALT
  Unload frmAltdados
  frmCliente.SetFocus
  Exit Sub
  
Trata_Erro:
   Call Process_Line_Errors(SQL)
End Sub



Private Sub Grid1_DblClick()

  Dim cod As String
  
   Grid1.Col = 0
   cod = Grid1.Text
   frmAltdados.txtCodtransp.Text = cod
   Grid1.Visible = False
   frmAltdados.txtCodtransp.SetFocus
  
      
 
End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MDIForm1.ssMsg.Caption = "Pressione o bat�o esquerdo do mouse para ver lista de transportadora"
End Sub


Private Sub txtcodtransp_DblClick()

    botao = 10
    
   OraParameters.Remove "cod"
   OraParameters.Add "cod", 0, 1
   
   
     V_SQL = "Begin producao.pck_vda020.pr_transportadora(:vCursor,:cod,:vErro);END;"

  
     oradatabase.ExecuteSQL V_SQL
  
     Set oradynaset = oradatabase.Parameters("vCursor").Value
  
     If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(SQL)
        Exit Sub
     End If
    
    MDIForm1.ssMsg.Caption = "Aguarde Consultando .."
    'CHAMA PROCEDURE QUE MONTA O GRID
       Call CONS_GRID(1450, 250, 8500, 2800, 2, frmAltdados, sel10)
       MDIForm1.ssMsg.Caption = ""
       frmAltdados.Grid1.Move 3360, 1080, 3255, 1815
       frmAltdados.Grid1.Visible = True
       Grid1.Visible = True

End Sub

Private Sub txtcodtransp_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtcodtransp_LostFocus()


On Error GoTo ERROR_HANDLER

If Grid1.Visible Then
   Grid1.SetFocus
Else
   If frmAltdados.txtCodtransp.Text > 0 Then
     OraParameters.Remove "cod"
     OraParameters.Add "cod", txtCodtransp.Text, 1
   
   
     V_SQL = "Begin producao.pck_vda020.pr_transportadora(:vCursor,:cod,:vErro);END;"

  
     oradatabase.ExecuteSQL V_SQL
  
     Set oradynaset = oradatabase.Parameters("vCursor").Value
  
     If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(SQL)
        Exit Sub
     End If
    
     If oradynaset.EOF Then
       MsgBox "TRANSP.: " & txtCodtransp.Text & " N�O LOCALIZADA", 0, "ATEN��O"
       frmAltdados.txtCodtransp.SetFocus
     Else
       frmAltdados.txtCodtransp.Text = oradynaset.Fields("cod_transp").Value
       frmAltdados.lblTransp.Caption = oradynaset.Fields("nome_transp").Value
     End If
  Else
    MsgBox "Selecione uma transportadora"
  End If
    oradatabase.Parameters.Remove "cod"
End If
  Exit Sub
    
ERROR_HANDLER:
   Select Case Err.Number
     Case 13
       MsgBox "Selecione uma transportadora", 0, "ATEN��O"
       frmAltdados.txtCodtransp.SetFocus
       Exit Sub
     Case Else
       frmTrataerro.Show
   End Select
    
End Sub


Private Sub txtContato_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtDdd1_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtDdd1_LostFocus()

  If txtDdd1.Text = "" Then
    txtDdd1.Text = 0
  End If


End Sub


Private Sub txtDdd2_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtDdd2_LostFocus()
  
  If txtDdd2.Text = "" Then
    txtDdd2.Text = 0
  End If

End Sub


Private Sub txtFax_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtFax_LostFocus()

  If txtFax.Text = "" Then
    txtFax.Text = 0
  End If


End Sub


Private Sub txtFone1_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtFone1_LostFocus()

  If txtFone1.Text = "" Then
    txtFone1.Text = 0
  End If



End Sub


Private Sub txtFone2_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtFone2_LostFocus()

  If txtFone1.Text = "" Then
    txtFone1.Text = 0
  End If

End Sub


Private Sub txtTelex_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtTelex_LostFocus()

  If txtTelex.Text = "" Then
    txtTelex.Text = 0
  End If


End Sub


