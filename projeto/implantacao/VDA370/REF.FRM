VERSION 4.00
Begin VB.Form frmref 
   BorderStyle     =   0  'None
   ClientHeight    =   780
   ClientLeft      =   45
   ClientTop       =   1395
   ClientWidth     =   9495
   Height          =   1185
   Left            =   -15
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   780
   ScaleWidth      =   9495
   ShowInTaskbar   =   0   'False
   Top             =   1050
   Width           =   9615
   Begin VB.TextBox txtCgc 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   6600
      MaxLength       =   33
      TabIndex        =   3
      Top             =   360
      Width           =   1575
   End
   Begin VB.TextBox txtNome 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   33
      TabIndex        =   2
      Top             =   360
      Width           =   3495
   End
   Begin VB.TextBox txtCodigo 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1080
      MaxLength       =   6
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin MSGrid.Grid Grid1 
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   360
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      ScrollBars      =   2
      MouseIcon       =   "REF.frx":0000
   End
   Begin VB.Label Label2 
      Caption         =   "CGC"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   360
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "Cliente"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   735
   End
End
Attribute VB_Name = "frmref"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Private Sub txtCgccic_Change()

End Sub

Private Sub txtCgccic_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub FORM_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MDIForm1.ssmsg.Caption = ""
End Sub


Private Sub Grid1_DblClick()

   Grid1.Col = 1
   Grid1.Visible = False
   oradatabase.Parameters.Remove "cod"
   oradatabase.Parameters.Add "cod", Grid1.Text, 1
   Set oradynaset = oradatabase.dbcreatedynaset(sel1, 0&)
    
   If oradynaset.EOF Then
     MsgBox "N�O LOCALIZOU COD:" & cod, 0, "ATEN��O"
   End If
   
   frmref.txtCodigo.Visible = True
   frmref.txtNome.Visible = True
   frmref.txtCgc.Visible = True
   frmref.Label2.Visible = True
   
   Call DADOS_TELA

End Sub

Private Sub txtCgc_Change()

If txtCgc <> "" Then
    txtNome.Enabled = False
    txtCodigo.Enabled = False
Else
    txtNome.Enabled = True
    txtCodigo.Enabled = True
End If

End Sub

Private Sub txtCodigo_Change()

If txtCodigo <> "" Then
    txtNome.Enabled = False
    txtCgc.Enabled = False
Else
    txtNome.Enabled = True
    txtCgc.Enabled = True
End If

End Sub


Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtNome_Change()
  
  If txtNome <> "" Then
    txtCodigo.Enabled = False
    txtCgc.Enabled = False
Else
    txtCodigo.Enabled = True
    txtCgc.Enabled = True
End If

End Sub


Private Sub txtNome_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


