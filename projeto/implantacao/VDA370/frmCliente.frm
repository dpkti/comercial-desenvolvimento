VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCliente 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dados do Cliente"
   ClientHeight    =   7590
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9315
   Icon            =   "frmCliente.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   506
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   621
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Dados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   5040
      Left            =   90
      TabIndex        =   10
      Top             =   2160
      Width           =   9105
      Begin VB.TextBox txtUF 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6480
         TabIndex        =   44
         Top             =   225
         Width           =   510
      End
      Begin VB.TextBox txtCidade 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   43
         Top             =   225
         Width           =   4335
      End
      Begin VB.TextBox txtA_Ven 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4995
         TabIndex        =   41
         Top             =   2790
         Width           =   1500
      End
      Begin VB.TextBox txtVenc 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4995
         TabIndex        =   39
         Top             =   2430
         Width           =   1500
      End
      Begin VB.TextBox txtPag_Cart 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5805
         TabIndex        =   37
         Top             =   3645
         Width           =   690
      End
      Begin VB.TextBox txtPag_Juros 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5805
         TabIndex        =   35
         Top             =   3195
         Width           =   690
      End
      Begin VB.TextBox txtSit 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   33
         Top             =   4590
         Width           =   1800
      End
      Begin VB.TextBox txtMens 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   31
         Top             =   4140
         Width           =   7350
      End
      Begin VB.TextBox txtMai_Atr 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   28
         Top             =   3690
         Width           =   600
      End
      Begin VB.TextBox txtMed_Atr 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   25
         Top             =   3240
         Width           =   600
      End
      Begin VB.TextBox txtSaldo 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   23
         Top             =   2610
         Width           =   1800
      End
      Begin VB.TextBox txtMai_Ac 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   21
         Top             =   2025
         Width           =   1800
      End
      Begin VB.TextBox txtMai_Fat_2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3555
         TabIndex        =   20
         Top             =   1575
         Width           =   1185
      End
      Begin VB.TextBox txtMai_Fat_1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   18
         Top             =   1575
         Width           =   1800
      End
      Begin VB.TextBox txtPrazo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6480
         TabIndex        =   16
         Top             =   1125
         Width           =   2490
      End
      Begin VB.TextBox txtUlt_Fat_2 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3555
         TabIndex        =   15
         Top             =   1125
         Width           =   1185
      End
      Begin VB.TextBox txtUlt_Fat_1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   13
         Top             =   1125
         Width           =   1800
      End
      Begin VB.TextBox txtCadastrado 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1620
         TabIndex        =   11
         Top             =   675
         Width           =   1185
      End
      Begin VB.Label lblUF 
         Appearance      =   0  'Flat
         Caption         =   "UF"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   6165
         TabIndex        =   47
         Top             =   270
         Width           =   390
      End
      Begin VB.Label lblCidade 
         Appearance      =   0  'Flat
         Caption         =   "Cidade"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   46
         Top             =   315
         Width           =   1155
      End
      Begin VB.Label lblAVen 
         Appearance      =   0  'Flat
         Caption         =   "� Vencer"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4095
         TabIndex        =   42
         Top             =   2835
         Width           =   810
      End
      Begin VB.Label lblVenc 
         Appearance      =   0  'Flat
         Caption         =   "Vencido"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4095
         TabIndex        =   40
         Top             =   2475
         Width           =   840
      End
      Begin VB.Label lblPagCart 
         Appearance      =   0  'Flat
         Caption         =   "Pagamento Cart�rio"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4095
         TabIndex        =   38
         Top             =   3735
         Width           =   1650
      End
      Begin VB.Label lblPagJuros 
         Appearance      =   0  'Flat
         Caption         =   "Paga Juros"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   4095
         TabIndex        =   36
         Top             =   3285
         Width           =   1470
      End
      Begin VB.Label lblSit 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   34
         Top             =   4635
         Width           =   1335
      End
      Begin VB.Label lblMens 
         Appearance      =   0  'Flat
         Caption         =   "Mensagem"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   32
         Top             =   4140
         Width           =   1290
      End
      Begin VB.Label lblDias2 
         Appearance      =   0  'Flat
         Caption         =   "dias"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2295
         TabIndex        =   30
         Top             =   3735
         Width           =   660
      End
      Begin VB.Label lblMaiAtr 
         Appearance      =   0  'Flat
         Caption         =   "Maior Atraso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   29
         Top             =   3735
         Width           =   1335
      End
      Begin VB.Label lblDias 
         Appearance      =   0  'Flat
         Caption         =   "dias"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2295
         TabIndex        =   27
         Top             =   3285
         Width           =   660
      End
      Begin VB.Label lblMedAtr 
         Appearance      =   0  'Flat
         Caption         =   "M�dia Atraso"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   26
         Top             =   3285
         Width           =   1305
      End
      Begin VB.Label lblSaldo 
         Appearance      =   0  'Flat
         Caption         =   "Saldo Duplicata"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   24
         Top             =   2655
         Width           =   1290
      End
      Begin VB.Label lblMaiAc 
         Appearance      =   0  'Flat
         Caption         =   "Maior Ac�mulo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   22
         Top             =   2070
         Width           =   1335
      End
      Begin VB.Label lblMaiFat 
         Appearance      =   0  'Flat
         Caption         =   "Maior Fatura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   19
         Top             =   1620
         Width           =   1200
      End
      Begin VB.Label lblPrazo 
         Appearance      =   0  'Flat
         Caption         =   "Prazo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   5940
         TabIndex        =   17
         Top             =   1170
         Width           =   540
      End
      Begin VB.Label lblUltFat 
         Appearance      =   0  'Flat
         Caption         =   "�ltima Fatura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   14
         Top             =   1170
         Width           =   1335
      End
      Begin VB.Label lblCadastrado 
         Appearance      =   0  'Flat
         Caption         =   "Cadastrado em"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   12
         Top             =   720
         Width           =   1335
      End
   End
   Begin VB.Frame fraCliente 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1080
      Left            =   90
      TabIndex        =   3
      Top             =   945
      Width           =   9105
      Begin VB.TextBox txtCod_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   225
         TabIndex        =   6
         Top             =   540
         Width           =   780
      End
      Begin VB.TextBox txtNome_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1125
         TabIndex        =   5
         Top             =   540
         Width           =   5685
      End
      Begin VB.TextBox txtCGC_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6930
         TabIndex        =   4
         Top             =   540
         Width           =   2040
      End
      Begin VB.Label lblCod 
         Appearance      =   0  'Flat
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   225
         TabIndex        =   9
         Top             =   315
         Width           =   750
      End
      Begin VB.Label lblNome 
         Appearance      =   0  'Flat
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1125
         TabIndex        =   8
         Top             =   315
         Width           =   690
      End
      Begin VB.Label lblCGC 
         Appearance      =   0  'Flat
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   6930
         TabIndex        =   7
         Top             =   315
         Width           =   480
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7260
      Width           =   9315
      _ExtentX        =   16431
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16378
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliente.frx":23D2
      PICN            =   "frmCliente.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCons_Pagam 
      Height          =   690
      Left            =   810
      TabIndex        =   45
      TabStop         =   0   'False
      ToolTipText     =   "Pagamentos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliente.frx":30C8
      PICN            =   "frmCliente.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   690
      Left            =   1575
      TabIndex        =   48
      TabStop         =   0   'False
      ToolTipText     =   "Impress�o"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCliente.frx":39BE
      PICN            =   "frmCliente.frx":39DA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCons_Pagam_Click()

    vModo = 0
    frmPagamentos.Show
    
End Sub

Private Sub cmdImprimir_Click()

'Dim arq
'Dim strNomeImpressoraAtual As String
'Dim P As Object

    If vVB_Generica_001.Perguntar("Deseja imprimir Relat�rio de Refer�ncias Comerciais?") = 7 Then
        Exit Sub
    End If

    Call Aguardar
    Screen.MousePointer = 11
    
    'impress�o do relat�rio
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print Tab(30); "REFER�NCIAS COMERCIAIS"
    Printer.Print Tab(30); "-----------------------------------------"
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Cliente"
    Printer.Print Tab(9); "----------"
    Printer.Print Tab(9); "C�d."; Tab(20); "Nome"; Tab(80); "CGC"
    Printer.Print Tab(9); txtCod_Cli.Text; Tab(20); txtNome_Cli.Text; Tab(80); txtCGC_Cli.Text
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Cidade:"; Tab(30); txtCidade.Text; Tab(80); "UF:"; Tab(90); txtUF.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Cadastrado em:"; Tab(30); txtCadastrado.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "�ltima Fatura:"; Tab(30); txtUlt_Fat_1.Text; Tab(50); txtUlt_Fat_2.Text; Tab(80); "Prazo:"; Tab(90); txtPrazo.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Maior Fatura:"; Tab(30); txtMai_Fat_1.Text; Tab(50); txtMai_Fat_2.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Maior Ac�mulo:"; Tab(30); txtMai_Ac.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Saldo Duplicata:"; Tab(30); txtSaldo.Text; Tab(80); "Vencido:"; Tab(105); txtVenc.Text;
    Printer.Print Tab(80); "� Vencer:"; Tab(105); txtA_Ven.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "M�dia Atraso:"; Tab(30); txtMed_Atr.Text; Tab(40); " dias"; Tab(80); "Paga Juros:"; Tab(105); txtPag_Juros.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Maior Atraso:"; Tab(30); txtMai_Atr.Text; Tab(40); " dias"; Tab(80); "Pagamento Cart�rio:"; Tab(105); txtPag_Cart.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Mensagem:"; Tab(30); txtMens.Text;
    Printer.Print
    Printer.Print
    Printer.Print Tab(9); "Situa��o:"; Tab(30); txtSit.Text;
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print Tab(30); "Emiss�o:  "; Format(Now, "DD/MM/YY hh:mm")
    Printer.Print
    
    'Sa�da tamb�m para arquivo --> *** fun��o do vb4: n�o estava ativa ***
    'strNomeImpressoraAtual = ""
    'If vVB_Generica_001.Perguntar("Enviar dados para arquivo ?") <> 7 Then
    '    strNomeImpressoraAtual = Printer.DeviceName
    '    For Each P In Printers
    '        If P.DeviceName Like "*PDF LIVRE*" Then
    '            Set Printer = P
    '            Exit For
    '        End If
    '    Next P
    '    Printer.EndDoc
    '
    '    FileCopy "C:\TEMP\FORN.PRN", "C:\PDFLIV~1\temp.prn"
    '
    '    arq = InputBox("ENTRE COM O NOME DO ARQUIVO")
    '    If arq = "" Then arq = "PDFLIVRE"
    '
    '    Shell "c:\aladdin\WordPDF.bat C:\PDFLIV~1\temp.prn C:\PDFs\" & arq & ".pdf", vbMinimizedNoFocus
    '    FileCopy "C:\PDFs\" & arq & ".pdf", "F:\USR\MARICI\" & arq & ".PDF"
    '
    '    'retornar impressora padrao
    '    If strNomeImpressoraAtual <> "" Then
    '        For Each P In Printers
    '            If P.DeviceName = strNomeImpressoraAtual Then
    '                Set Printer = P
    '                Exit For
    '            End If
    '        Next P
    '    End If
    '    Printer.EndDoc
    'End If
    
    Printer.EndDoc
        
    Screen.MousePointer = 0
    Unload frmAguardar
    Call vVB_Generica_001.Informar("Impress�o efetuada com sucesso!")

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub


