create or replace package PRODUCAO.PCK_VDA370 is

  TYPE TP_CURSOR IS REF CURSOR;

  Procedure PR_SEL_COD_CLI (cCodCli IN OUT TP_CURSOR, pCodigo IN CLIENTE.COD_CLIENTE%TYPE);
  
  Procedure PR_SEL_NOM_CLI (cCodCli IN OUT TP_CURSOR, pNome IN CLIENTE.NOME_CLIENTE%TYPE);

  Procedure PR_SEL_CGC_CLI (cCodCli IN OUT TP_CURSOR, pCGC IN CLIENTE.CGC%TYPE);
  
  Procedure PR_SEL_FAT (cCodCli IN OUT TP_CURSOR, 
                        pCodigo IN COBRANCA.DUPLICATAS.COD_CLIENTE%TYPE,
                        pData IN COBRANCA.DUPLICATAS.DT_EMISSAO_DUPL%TYPE);
                        
  Procedure PR_SEL_DUPL (cCodCli IN OUT TP_CURSOR, 
                        pFatura IN COBRANCA.DUPLICATAS.NUM_FATURA%TYPE,
                        pLoja IN COBRANCA.DUPLICATAS.COD_LOJA%TYPE);     
                        
  Procedure PR_SEL_DOLAR (cCodCli IN OUT TP_CURSOR, 
                         pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);
                           
  Procedure PR_SEL_DATAS (cCodCli IN OUT TP_CURSOR, 
                         pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);
                         
  Procedure PR_SEL_ATRASO (cCodCli IN OUT TP_CURSOR, 
                           pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);
                           
  Procedure PR_SEL_JUROS (cCodCli IN OUT TP_CURSOR, 
                          pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);

  Procedure PR_SEL_JUROS_2 (cCodCli IN OUT TP_CURSOR, 
                            pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);
                            
  Procedure PR_SEL_CART (cCodCli IN OUT TP_CURSOR, 
                         pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE);  
                         
  Procedure PR_SEL_PAGAM (cCodCli IN OUT TP_CURSOR, 
                          pCod IN COBRANCA.DUPLICATAS.COD_CLIENTE%TYPE,
                          pMes IN NUMBER);                                                                                                                                                                                                      

end PCK_VDA370;
/
create or replace package body PRODUCAO.PCK_VDA370 is

  -- 1 - Consulta Cliente a partir do "C�digo"
  Procedure PR_SEL_COD_CLI (cCodCli IN OUT TP_CURSOR, pCodigo IN CLIENTE.COD_CLIENTE%TYPE) is
    Begin
       OPEN cCodCli FOR
       Select substr(to_char(a.cod_cliente,'FM9999990'),1,7) codigo,       
              a.nome_cliente nome, 
              a.cgc cgc,
              b.nome_cidade cidade, 
              b.cod_uf uf,
              NVL(substr(to_char(c.dt_ult_compra,'DD/MM/RR'),1,8),' ') ult_compra,
              NVL(substr(to_char(a.dt_cadastr,'DD/MM/RR'),1,8),' ') dt_cadastr,
              a.situacao situacao,
              nvl(a.cod_mensagem,0) cod_mens,
              nvl(d.desc_mens,' ') desc_mens
      From    cliente a, cidade b, clie_credito c, clie_mensagem d
      Where   c.cod_cliente(+) = a.cod_cliente and
              d.cod_mensagem(+) = a.cod_mensagem and
              b.cod_cidade(+) = a.cod_cidade and
              a.cod_cliente = pCodigo;        
  End;  

  -- 2 - Consulta Cliente a partir do "Nome"
  Procedure PR_SEL_NOM_CLI (cCodCli IN OUT TP_CURSOR, pNome IN CLIENTE.NOME_CLIENTE%TYPE) is
    Begin
       OPEN cCodCli FOR  
       Select a.nome_cliente cliente,
              a.cod_cliente cod_cliente,
              b.nome_cidade cidade,
              b.cod_uf uf
       From   cliente a, cidade b
       Where  a.cod_cidade=b.cod_cidade and
              a.nome_cliente like '%' || pNome || '%';
  End;
  
  -- 3 - Consulta Cliente a partir do "CGC"
  Procedure PR_SEL_CGC_CLI (cCodCli IN OUT TP_CURSOR, pCGC IN CLIENTE.CGC%TYPE) is
    Begin
       OPEN cCodCli FOR
       Select substr(to_char(a.cod_cliente,'FM9999990'),1,7) codigo,       
              a.nome_cliente nome, 
              a.cgc cgc,
              b.nome_cidade cidade, 
              b.cod_uf uf,
              NVL(substr(to_char(c.dt_ult_compra,'DD/MM/RR'),1,8),' ') ult_compra,
              NVL(substr(to_char(a.dt_cadastr,'DD/MM/RR'),1,8),' ') dt_cadastr,
              a.situacao situacao,
              nvl(a.cod_mensagem,0) cod_mens,
              nvl(d.desc_mens,' ') desc_mens
      From    cliente a, cidade b, clie_credito c, clie_mensagem d
      Where   c.cod_cliente(+) = a.cod_cliente and
              d.cod_mensagem(+) = a.cod_mensagem and
              b.cod_cidade(+) = a.cod_cidade and
              a.cgc = pCGC;        
  End;  
  
  -- 4 - Consulta Dados da Fatura     
  Procedure PR_SEL_FAT (cCodCli IN OUT TP_CURSOR, 
                        pCodigo IN COBRANCA.DUPLICATAS.COD_CLIENTE%TYPE,
                        pData IN COBRANCA.DUPLICATAS.DT_EMISSAO_DUPL%TYPE) is
    Begin
       OPEN cCodCli FOR
       Select a.num_fatura num_fatura,
              a.cod_loja cod_loja,
              nvl(b.desc_plano,' ') desc_plano
       From   cobranca.duplicatas a, plano_pgto b 
       Where  a.cod_cliente = pCodigo and 
              a.dt_emissao_dupl = to_date(pData,'DD/MM/RR') and
              b.cod_plano(+) = a.cod_plano
       Order by num_fatura;
   End;
   
   -- 5 - Consulta Dados da Duplicatas     
   Procedure PR_SEL_DUPL (cCodCli IN OUT TP_CURSOR, 
                          pFatura IN COBRANCA.DUPLICATAS.NUM_FATURA%TYPE,
                          pLoja IN COBRANCA.DUPLICATAS.COD_LOJA%TYPE) is
     Begin
       OPEN cCodCli FOR
       Select nvl(substr(to_char(sum(vl_duplicata),'999,999,999,990.00'),2,18),0) vl_duplicata
       From   cobranca.duplicatas a
       Where  num_fatura = rpad(pFatura,6) and
              a.cod_loja = pLoja
       Group by a.num_fatura
       Order by a.num_fatura;
   End;
   
   -- 6 - Consulta Dolar
   Procedure PR_SEL_DOLAR (cCodCli IN OUT TP_CURSOR, 
                           pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR
       Select nvl(substr(to_char((a.maior_fatura * 
              (b.valor_uss/decode(d.valor_uss,'',b.valor_uss,d.valor_uss))),'999,999,999,990.00'),2,18),0) maior_fatura,
              nvl(substr(to_char((a.maior_saldev * (b.valor_uss/decode(e.valor_uss,'',b.valor_uss,e.valor_uss))),'999,999,999,990.00'),2,18),0) maior_saldev,
              nvl(substr(to_char(a.atraso_medio,'990.00'),2,6),0) atraso_medio,
              NVL(substr(to_char(a.dt_maior_fatura,'DD/MM/RR'),1,8),' ') dt_maior_fatura
       From   dolar_diario e,dolar_diario d, DATAS c, dolar_diario b, clie_credito a
       Where a.cod_cliente = pCod and
             e.data_uss(+) = a.dt_maior_saldev and
             d.data_uss(+) = a.dt_maior_fatura and
             b.data_uss = c.dt_faturamento; 
   End;
  
   -- 7 - Consulta Datas
   Procedure PR_SEL_DATAS (cCodCli IN OUT TP_CURSOR, 
                           pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR   
       Select 'a' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),'FM999,999,999,999.00'),0)valor,
              nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
       From datas, cobranca.duplicatas a
       Where a.dt_vencimento >= datas.dt_faturamento and
             a.situacao_pagto=0 and 
             a.cod_cliente = pCod
       Union
       Select 'b' DUPL,nvl(to_char(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),'FM999,999,999,999.00'),0)valor,
               nvl(sum(a.vl_duplicata - a.vl_desconto - a.vl_abatimento - a.vl_devolucao - a.vl_pago),0)valor1
       From datas, cobranca.duplicatas a
       Where a.dt_vencimento < datas.dt_faturamento and
             a.situacao_pagto=0  and 
             a.cod_cliente = pCod
       Order by 1;
   End;

   -- 8 - Consulta Atraso
   Procedure PR_SEL_ATRASO (cCodCli IN OUT TP_CURSOR, 
                           pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR      
       Select nvl(to_char(max(dt_baixa_banco - dt_vencimento),'999'),0) maior_atraso
       From cobranca.duplicatas
       Where (dt_baixa_banco - dt_vencimento) > 0 and
             situacao_pagto=1 and cod_cliente = pCod;
   End;  

   -- 9 - Consulta Juros
   Procedure PR_SEL_JUROS (cCodCli IN OUT TP_CURSOR, 
                           pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR      
       Select tp_duplicata
       From cobranca.duplicatas a, datas b
       Where tp_duplicata=1 and
             situacao_pagto=0 and
             (dt_vencimento - dt_faturamento) > 15 and
             cod_cliente = pCod;
   End;
   
   -- 10 - Consulta Juros_2
   Procedure PR_SEL_JUROS_2 (cCodCli IN OUT TP_CURSOR, 
                            pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR    
       Select tp_duplicata
       From cobranca.duplicatas a, datas b
       Where tp_duplicata=1 and
             cod_cliente = pCod;
   End;

   -- 11 - Consulta Cart�rio
   Procedure PR_SEL_CART (cCodCli IN OUT TP_CURSOR, 
                          pCod IN CLIE_CREDITO.COD_CLIENTE%TYPE) is
     Begin
       OPEN cCodCli FOR    
       Select cod_cliente
       From cobranca.duplicatas 
       Where (tp_pagamento='C' or tp_pagamento='P') and
             cod_cliente = pCod;
   End;  
   
   -- 12 - Consulta Pagamentos
   Procedure PR_SEL_PAGAM (cCodCli IN OUT TP_CURSOR, 
                           pCod IN COBRANCA.DUPLICATAS.COD_CLIENTE%TYPE,
                           pMes IN NUMBER) is
     Begin
       OPEN cCodCli FOR   
       Select a.num_fatura num_fatura,
              a.num_ordem num_ordem,
              to_char(a.cod_loja,'09') ||'-'|| d.nome_fantasia deposito,
              a.tp_duplicata tp_duplicata,
              nvl(substr(to_char(1/(g.valor_uss/b.valor_uss),'9,999,990.000000'),2,18),0) vl_dolar,
              nvl(substr(to_char(a.vl_pago_dolar,'999,999,999,990.00'),2,18),0) vl_pago,
              nvl(substr(to_char(a.vl_duplicata,'999,999,999,990.00'),2,18),0) vl_duplicata,
              nvl(substr(to_char(a.pc_juro_dias_atraso,'90.00'),2,5),0) pc_juro_dias_atraso,
              a.dt_baixa_banco dt_baixa_banco,
              nvl((a.dt_baixa_banco - a.dt_vencimento),0) dias_atraso,
              nvl(a.tp_pagamento,' ') tp_pagamento,
              nvl(substr(to_char(a.vl_despesas,'999,999,999,990.00'),2,18),0) vl_despesas,
              nvl(substr(to_char(a.vl_juros,'999,999,999,990.00'),2,18),0) vl_juros
       From   cobranca.duplicatas a, dolar_diario b, DATAS c, 
              loja d,dolar_diario e, datas f,dolar_diario g 
       Where  a.cod_cliente = pCod and
              a.situacao_pagto=1 and
              a.dt_baixa_empresa >= add_months(c.dt_faturamento,pMes) and
              a.dt_baixa_empresa <= c.dt_faturamento and
              a.cod_loja = d.cod_loja and
              e.data_uss(+) = f.dt_faturamento and
              g.data_uss(+) = a.dt_emissao_dupl and
              a.dt_baixa_banco = b.data_uss(+)
       Order by a.dt_baixa_banco desc;
   End;   
  
end PCK_VDA370;
/
