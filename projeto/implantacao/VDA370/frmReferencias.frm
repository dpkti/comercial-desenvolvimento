VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmReferencias 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Refer�ncias Comerciais"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6855
   Icon            =   "frmReferencias.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   457
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraCliente 
      Caption         =   "Dados do Cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1845
      Left            =   90
      TabIndex        =   3
      Top             =   945
      Width           =   6675
      Begin VB.TextBox txtCGC_Cli 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1050
         MaxLength       =   14
         TabIndex        =   8
         Top             =   1260
         Width           =   2220
      End
      Begin VB.TextBox txtNome_Cli 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1050
         MaxLength       =   30
         TabIndex        =   5
         Top             =   765
         Width           =   5460
      End
      Begin VB.TextBox txtCod_Cli 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1050
         MaxLength       =   6
         TabIndex        =   4
         Top             =   300
         Width           =   915
      End
      Begin VB.Label lblCGC 
         Appearance      =   0  'Flat
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   9
         Top             =   1320
         Width           =   660
      End
      Begin VB.Label lblNome 
         Appearance      =   0  'Flat
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   7
         Top             =   825
         Width           =   690
      End
      Begin VB.Label lblCod 
         Appearance      =   0  'Flat
         Caption         =   "C�digo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   6
         Top             =   360
         Width           =   750
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   2865
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12039
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmReferencias.frx":23D2
      PICN            =   "frmReferencias.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   810
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Consultar Refer�ncias Comerciais"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmReferencias.frx":30C8
      PICN            =   "frmReferencias.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCancelCons 
      Height          =   690
      Left            =   1575
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Limpar Campos"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmReferencias.frx":3DBE
      PICN            =   "frmReferencias.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmReferencias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancelCons_Click()

    'limpa campos
    txtCod_Cli = ""
    txtCod_Cli.SetFocus
    txtNome_Cli = ""
    txtCGC_Cli = ""

End Sub

Private Sub cmdConsultar_Click()

Dim vFatura As String
Dim vCodLoja As Long
Dim vSaldo As Double
Dim vVencer As Double
Dim vVencido As Double
Dim vJuros1 As Boolean
Dim vJuros2 As Boolean
Dim vSituacao As String
     
    'verificar se pelo menos 1 campo foi preenchido
    If Trim(txtCod_Cli.Text) = "" And Trim(txtNome_Cli.Text) = "" And Trim(txtCGC_Cli.Text) = "" Then
        Call vVB_Generica_001.Informar("Digite um campo para consulta!")
        txtCod_Cli.SetFocus
        Exit Sub
    End If

    Call Aguardar
        
    '*envia par�metros para o banco e executa 1) PR_SEL_COD_CLI ou 3) PR_SEL_CGC_CLI
    If Trim(txtCod_Cli.Text) <> "" Then
        '= por C�digo
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCODIGO", txtCod_Cli.Text, 1
        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_COD_CLI(:vCursor,:vCODIGO)"
    ElseIf Trim(txtNome_Cli.Text) <> "" Then
        '= por Nome
        'envia par�metros para o banco e executa 2) PR_SEL_NOM_CLI
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
            
        vBanco.Parameters.Add "vNOME", frmReferencias.txtNome_Cli.Text, 1
        
        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_NOM_CLI(:vCursor,:vNOME)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
    
        'preenche grid Clientes
        If vObjOracle.EOF = True Then  'n�o achou
            Call vVB_Generica_001.Informar("Cliente n�o existe!")
            Exit Sub
        Else
            Call vVB_Generica_001.CarregaGridTabela(frmGrid_Clientes.mfgClientes, vObjOracle, 5)
        End If
        
        'exibe tela
        vModo = 0
        frmGrid_Clientes.Show 0
        
        Unload frmAguardar
        
        Exit Sub
    ElseIf Trim(txtCGC_Cli.Text) <> "" Then
        '= por CGC
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        vBanco.Parameters.Add "vCGC", txtCGC_Cli.Text, 1
        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_CGC_CLI(:vCursor,:vCGC)"
    End If
        
    'executa o comando, de acordo com o campo preenchido
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Unload frmAguardar
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If
    
    If vObjOracle.EOF = True Then    'n�o achou
        Call vVB_Generica_001.Informar("Este Cliente n�o existe!")
        Unload frmAguardar
        cmdCancelCons_Click
        txtCod_Cli.SetFocus
        Exit Sub
    Else
        'preenche demais campos
        txtCod_Cli.Text = vObjOracle!codigo
        txtNome_Cli.Text = vObjOracle!nome
        
        'preenche form frmCliente
        Call Carrega_Dados_Cliente
                
        Unload frmAguardar
        
    End If

End Sub

Public Sub Carrega_Dados_Cliente()

        'preenche form frmCliente
        frmCliente.txtCod_Cli = vObjOracle!codigo
        frmCliente.txtNome_Cli = vObjOracle!nome
        frmCliente.txtCGC_Cli = vObjOracle!cgc
        frmReferencias.txtCGC_Cli.Text = vObjOracle!cgc
        frmCliente.txtCidade = vObjOracle!cidade
        frmCliente.txtUF = vObjOracle!uf
        frmCliente.txtCadastrado = vObjOracle!dt_cadastr
        frmCliente.txtUlt_Fat_2 = vObjOracle!ult_compra
        frmCliente.txtMens = vObjOracle!desc_mens
        
        vSituacao = vObjOracle!situacao
        If vSituacao = "0" Then
            frmCliente.txtSit.ForeColor = &H800000
            frmCliente.txtSit = "ATIVO"
        Else
            frmCliente.txtSit.ForeColor = &HFF&
            frmCliente.txtSit = "DESATIVADO"
        End If
                
                
        vFatura = ""
        vCod_Loja = 0
        If Trim(frmCliente.txtUlt_Fat_2.Text) <> "" Then
            '*envia par�metros para o banco e executa 4) PR_SEL_FAT
            Call vVB_Generica_001.ExcluiBind(vBanco)
            vBanco.Parameters.Add "vCursor", 0, 3
            vBanco.Parameters("vCursor").ServerType = 102
            vBanco.Parameters("vCursor").DynasetOption = &H2&
            vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
            vBanco.Parameters.Add "vCODIGO", frmCliente.txtCod_Cli.Text, 1
            vBanco.Parameters.Add "vDATA", CDate(Format(frmCliente.txtUlt_Fat_2.Text, "dd/mm/yy")), 1
    
            vSql = "PRODUCAO.PCK_VDA370.PR_SEL_FAT(:vCursor,:vCODIGO,:vDATA)"
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro (vErro)
                Unload frmAguardar
                Exit Sub
            Else
                Set vObjOracle = vBanco.Parameters("vCursor").Value
            End If
            If vObjOracle.EOF = True Then    'n�o achou
                frmCliente.txtPrazo = ""
                vFatura = ""
                vCod_Loja = 0
            Else
                frmCliente.txtPrazo = vObjOracle!desc_plano
                vFatura = vObjOracle!num_fatura
                vCod_Loja = vObjOracle!cod_loja
            End If
        End If
        
        
        '*envia par�metros para o banco e executa 5) PR_SEL_DUPL
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Add "vFATURA", vFatura, 1
        vBanco.Parameters.Add "vLOJA", vCod_Loja, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_DUPL(:vCursor,:vFATURA,:vLOJA)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then    'n�o achou
            frmCliente.txtUlt_Fat_1 = "0.00"
        Else
            frmCliente.txtUlt_Fat_1 = vObjOracle!vl_duplicata
        End If
        
        
        '*envia par�metros para o banco e executa 6) PR_SEL_DOLAR
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_DOLAR(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then    'n�o achou
            frmCliente.txtMai_Fat_1.Text = "0.00"
            frmCliente.txtMai_Fat_2.Text = ""
            frmCliente.txtMai_Ac.Text = "0.00"
            frmCliente.txtMed_Atr = "0.00"
        Else
            frmCliente.txtMai_Fat_1.Text = vObjOracle!maior_fatura
            frmCliente.txtMai_Fat_2.Text = vObjOracle!dt_maior_fatura
            frmCliente.txtMai_Ac.Text = vObjOracle!maior_saldev
            frmCliente.txtMed_Atr = vObjOracle!atraso_medio
        End If
                
                
        vSaldo = 0
        vVencer = 0
        vVencido = 0
        '*envia par�metros para o banco e executa 7) PR_SEL_DATAS
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_DATAS(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then    'n�o achou
            frmCliente.txtA_Ven = "0.00"
            frmCliente.txtVenc = "0.00"
            frmCliente.txtSaldo = "0.00"
        Else
            frmCliente.txtA_Ven = vObjOracle!Valor
            vVencer = vVencer + vObjOracle!Valor
            vObjOracle.MoveNext
            frmCliente.txtVenc = vObjOracle!Valor
            vVencido = vVencido + vObjOracle!Valor
            
            vSaldo = Format(vVencer + vVencido, "###,###,###.00")
            If vSaldo = 0 Then
                frmCliente.txtSaldo = "0.00"
            Else
                frmCliente.txtSaldo = vSaldo
            End If
        End If
        
        
        '*envia par�metros para o banco e executa 8) PR_SEL_ATRASO
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_ATRASO(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then    'n�o achou
            frmCliente.txtMai_Atr = "0"
        Else
            frmCliente.txtMai_Atr = vObjOracle!maior_atraso
        End If
        
                
        '*envia par�metros para o banco e executa 9) PR_SEL_JUROS
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_JUROS(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        
        vJuros1 = True
        If vObjOracle.EOF = True Then    'n�o achou
            vJuros1 = True
        Else
            vJuros1 = False
        End If
        
        
        '*envia par�metros para o banco e executa 10) PR_SEL_JUROS_2
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_JUROS_2(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        
        vJuros2 = True
        If vObjOracle.EOF = True Then    'n�o achou
            vJuros2 = True
        Else
            vJuros2 = True '*** esta condi��o estava no vb4 e foi mantida ***
        End If
        
        If vJuros1 = True And vJuros2 = True Then
            frmCliente.txtPag_Juros = "NDA"
        Else
            frmCliente.txtPag_Juros = "N�O"
        End If
        
        
        '*envia par�metros para o banco e executa 11) PR_SEL_CART
        Call vVB_Generica_001.ExcluiBind(vBanco)
        vBanco.Parameters.Add "vCursor", 0, 3
        vBanco.Parameters("vCursor").ServerType = 102
        vBanco.Parameters("vCursor").DynasetOption = &H2&
        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
        
        vBanco.Parameters.Add "vCOD", frmCliente.txtCod_Cli.Text, 1

        vSql = "PRODUCAO.PCK_VDA370.PR_SEL_CART(:vCursor,:vCOD)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro (vErro)
            Unload frmAguardar
            Exit Sub
        Else
            Set vObjOracle = vBanco.Parameters("vCursor").Value
        End If
        If vObjOracle.EOF = True Then    'n�o achou
            frmCliente.txtPag_Cart = "N�O"
        Else
            frmCliente.txtPag_Cart = "SIM"
        End If
        
        vModo = 0
        frmCliente.Show

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Activate()

    txtCod_Cli.SetFocus

End Sub

Private Sub txtNome_Cli_KeyPress(KeyAscii As Integer)

    'coloca letra mai�scula para o campo
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    txtCod_Cli.Text = ""
    txtCGC_Cli.Text = ""

End Sub

Private Sub txtCod_Cli_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCod_Cli)
   'If Chr$(KeyAscii) = "," Then
   '     KeyAscii = Asc(".")
   'End If
   txtNome_Cli.Text = ""
   txtCGC_Cli.Text = ""

End Sub

Private Sub txtCGC_Cli_KeyPress(KeyAscii As Integer)
   
   KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCGC_Cli)
   'If Chr$(KeyAscii) = "," Then
   '     KeyAscii = Asc(".")
   'End If
   txtCod_Cli.Text = ""
   txtNome_Cli.Text = ""

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub
