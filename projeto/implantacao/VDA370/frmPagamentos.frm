VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmPagamentos 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pagamentos"
   ClientHeight    =   7230
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8295
   Icon            =   "frmPagamentos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   482
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   553
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraCliente 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1080
      Left            =   15
      TabIndex        =   9
      Top             =   945
      Width           =   8265
      Begin VB.TextBox txtCGC_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6120
         TabIndex        =   12
         Top             =   540
         Width           =   1995
      End
      Begin VB.TextBox txtNome_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   990
         TabIndex        =   11
         Top             =   540
         Width           =   5055
      End
      Begin VB.TextBox txtCod_Cli 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   176
         TabIndex        =   10
         Top             =   540
         Width           =   735
      End
      Begin VB.Label lblCGC 
         Appearance      =   0  'Flat
         Caption         =   "CGC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   6120
         TabIndex        =   15
         Top             =   315
         Width           =   480
      End
      Begin VB.Label lblNome 
         Appearance      =   0  'Flat
         Caption         =   "Nome"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   990
         TabIndex        =   14
         Top             =   315
         Width           =   690
      End
      Begin VB.Label lblCod 
         Appearance      =   0  'Flat
         Caption         =   "C�d."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   176
         TabIndex        =   13
         Top             =   315
         Width           =   480
      End
   End
   Begin VB.Frame fraPagamentos 
      Caption         =   "Selecione o intervalo de Pagamentos: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   750
      Left            =   45
      TabIndex        =   7
      Top             =   2205
      Width           =   8160
      Begin VB.OptionButton opt_todos 
         Appearance      =   0  'Flat
         Caption         =   "TODOS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   6705
         TabIndex        =   4
         Top             =   315
         Width           =   1050
      End
      Begin VB.OptionButton opt_6_meses 
         Appearance      =   0  'Flat
         Caption         =   "6 meses"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4455
         TabIndex        =   3
         Top             =   315
         Width           =   1050
      End
      Begin VB.OptionButton opt_3_meses 
         Appearance      =   0  'Flat
         Caption         =   "3 meses"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   2205
         TabIndex        =   2
         Top             =   315
         Width           =   1050
      End
      Begin VB.OptionButton opt_1_mes 
         Appearance      =   0  'Flat
         Caption         =   "1 m�s"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   135
         TabIndex        =   1
         Top             =   315
         Width           =   1050
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6900
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14579
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPagamentos.frx":23D2
      PICN            =   "frmPagamentos.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgPagamentos 
      Height          =   3720
      Left            =   60
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   3105
      Width           =   8130
      _ExtentX        =   14340
      _ExtentY        =   6562
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      BackColorBkg    =   -2147483633
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmPagamentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
    'carrega cabe�alho com os dados do Cliente
    txtCod_Cli = frmCliente.txtCod_Cli
    txtNome_Cli = frmCliente.txtNome_Cli
    txtCGC_Cli = frmCliente.txtCGC_Cli
        
    'deixa as op��es em branco, para sele��o
    opt_1_mes.Value = False
    opt_3_meses.Value = False
    opt_6_meses.Value = False
    opt_todos.Value = False
    
    'Grid
    Call Cabecalho
    mfgPagamentos.Rows = 2
    
    'linha em branco
    mfgPagamentos.Row = 1
    For i = 0 To mfgPagamentos.Cols - 1
        mfgPagamentos.Col = i
        mfgPagamentos.Text = ""
    Next i
        
End Sub

Private Sub Cabecalho()

    'define tam. das colunas
    mfgPagamentos.ColWidth(0) = 900
    mfgPagamentos.ColWidth(1) = 400
    mfgPagamentos.ColWidth(2) = 1500
    mfgPagamentos.ColWidth(3) = 1500
    mfgPagamentos.ColWidth(4) = 1300
    mfgPagamentos.ColWidth(5) = 900
    mfgPagamentos.ColWidth(6) = 800
    mfgPagamentos.ColWidth(7) = 500
    
    'cabe�alho
    mfgPagamentos.Row = 0
    mfgPagamentos.Col = 0
    mfgPagamentos.Text = "DUPLIC."
    mfgPagamentos.Col = 1
    mfgPagamentos.Text = "OR."
    mfgPagamentos.Col = 2
    mfgPagamentos.Text = "    DEP�SITO"
    mfgPagamentos.Col = 3
    mfgPagamentos.Text = "   VALOR PAGO"
    mfgPagamentos.Col = 4
    mfgPagamentos.Text = "  DT. BAIXA"
    mfgPagamentos.Col = 5
    mfgPagamentos.Text = " ATRASO"
    mfgPagamentos.Col = 6
    mfgPagamentos.Text = "JUROS"
    mfgPagamentos.Col = 7
    mfgPagamentos.Text = "SIT."
    
End Sub

Private Sub Carrega_Grid_Pagamentos()

Dim i As Integer
Dim vLinha As Integer
Dim v_dias_atraso As Double
Dim v_pc_juros As Double
Dim v_vl_duplicata As Double
Dim v_vl_juros As Double
Dim v_vl_despesas As Double
Dim v_vl_dolar As Double

    Call Aguardar

    'envia par�metros para o banco e executa comando PR_SEL_PAGAM
    Call vVB_Generica_001.ExcluiBind(vBanco)
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vBanco.Parameters.Add "vCOD", Trim(frmCliente.txtCod_Cli.Text), 1
    
    'este par�metro depende da op��o escolhida
    If opt_1_mes.Value = True Then
        vBanco.Parameters.Add "vMES", -1, 1
    ElseIf opt_3_meses.Value = True Then
        vBanco.Parameters.Add "vMES", -3, 1
    ElseIf opt_6_meses.Value = True Then
        vBanco.Parameters.Add "vMES", -6, 1
    ElseIf opt_todos.Value = True Then
        vBanco.Parameters.Add "vMES", -24, 1
    End If
    
    vSql = "PRODUCAO.PCK_VDA370.PR_SEL_PAGAM(:vCursor,:vCOD,:vMES)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    'Unload frmAguardar
    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro (vErro)
        Unload frmAguardar
        Exit Sub
    Else
        Set vObjOracle = vBanco.Parameters("vCursor").Value
    End If

    'preenche grid
    If vObjOracle.EOF = True Then  'n�o achou
        Call vVB_Generica_001.Informar("N�o existem Pagamentos para este Cliente no intervalo selecionado!")
        Call Cabecalho
                
        mfgPagamentos.Rows = 2
        
        'linha em branco
        mfgPagamentos.Row = 1
        For i = 0 To mfgPagamentos.Cols - 1
            mfgPagamentos.Col = i
            mfgPagamentos.Text = ""
        Next i
        
        Unload frmAguardar
        
        Exit Sub
    Else
        'Call vVB_Generica_001.CarregaGridTabela(mfgPagamentos, vObjOracle, 9)
                
        Call Cabecalho
        
        'preenche as linhas
        mfgPagamentos.Rows = vObjOracle.RecordCount + 1
        For vLinha = 1 To vObjOracle.RecordCount
            mfgPagamentos.Row = vLinha
            mfgPagamentos.Col = 0
            mfgPagamentos.ColAlignment(0) = 7
            mfgPagamentos.Text = vObjOracle!num_fatura
            mfgPagamentos.Col = 1
            mfgPagamentos.ColAlignment(1) = 4
            mfgPagamentos.Text = vObjOracle!num_ordem
            mfgPagamentos.Col = 2
            mfgPagamentos.ColAlignment(2) = 4
            mfgPagamentos.Text = vObjOracle!deposito
            mfgPagamentos.Col = 3
            mfgPagamentos.ColAlignment(3) = 6
            mfgPagamentos.Text = vObjOracle!vl_pago
            mfgPagamentos.Col = 4
            mfgPagamentos.ColAlignment(4) = 4
            mfgPagamentos.Text = vObjOracle!dt_baixa_banco
            
            mfgPagamentos.Col = 5
            mfgPagamentos.ColAlignment(5) = 4
            v_dias_atraso = vObjOracle!dias_atraso
            v_pc_juros = vObjOracle!pc_juro_dias_atraso
            v_vl_duplicata = vObjOracle!vl_duplicata
            v_vl_juros = vObjOracle!vl_juros
            v_vl_despesas = vObjOracle!vl_despesas
            v_vl_dolar = vObjOracle!vl_dolar
            If v_dias_atraso < 0 Then
                mfgPagamentos.Text = 0
            Else
                mfgPagamentos.Text = v_dias_atraso
            End If

            mfgPagamentos.Col = 6
            mfgPagamentos.ColAlignment(6) = 4
            If v_dias_atraso = 0 Then
                mfgPagamentos.Text = "N"
            ElseIf ((v_dias_atraso * (v_pc_juros / 100) * v_vl_duplicata) / v_vl_dolar) < 15 Then
                mfgPagamentos.Text = "S"
            ElseIf ((v_dias_atraso * (v_pc_juros / 100) * v_vl_duplicata) * 0.75) > (v_vl_juros + v_vl_despesas) Then
                mfgPagamentos.Text = "N"
            ElseIf ((v_dias_atraso * (v_pc_juros / 100) * v_vl_duplicata) * 0.75) <= (v_vl_juros + v_vl_despesas) Then
                mfgPagamentos.Text = "N"
            End If

            mfgPagamentos.Col = 7
            mfgPagamentos.ColAlignment(7) = 4
            mfgPagamentos.Text = vObjOracle!tp_pagamento
            
            vObjOracle.MoveNext
        Next
                
    End If
    
    Unload frmAguardar

End Sub

Private Sub opt_todos_Click()

    Call Carrega_Grid_Pagamentos

End Sub

Private Sub opt_1_mes_Click()

    Call Carrega_Grid_Pagamentos

End Sub

Private Sub opt_3_meses_Click()

    Call Carrega_Grid_Pagamentos

End Sub

Private Sub opt_6_meses_Click()

    Call Carrega_Grid_Pagamentos

End Sub
