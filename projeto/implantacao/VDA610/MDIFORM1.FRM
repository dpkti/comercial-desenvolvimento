VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "Estat�stica de Contatos - Telemarketing"
   ClientHeight    =   4140
   ClientLeft      =   1695
   ClientTop       =   1950
   ClientWidth     =   6690
   Icon            =   "MDIFORM1.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   6690
      _Version        =   65536
      _ExtentX        =   11800
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   1560
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDIFORM1.frx":030A
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1560
         TabIndex        =   3
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDIFORM1.frx":0624
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   840
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDIFORM1.frx":093E
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   120
         TabIndex        =   1
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "MDIFORM1.frx":0C58
      End
   End
   Begin VB.Menu mnuVendedor 
      Caption         =   "&Vendedor"
   End
   Begin VB.Menu mnuResultado 
      Caption         =   "&Resultado"
   End
   Begin VB.Menu mnuLigacoes 
      Caption         =   "&Liga��es"
      Visible         =   0   'False
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()
      On Error GoTo TrataErro

    Dim strLogin As String
    Dim ss As Object
    Dim i As Integer
    
    
    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
      End
    End If
    
     Call Get_CD(Mid(App.Path, 1, 1))

    If strTp_banco = "U" Then
      strTabela_Banco = "PRODUCAO."
    Else
      strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
     ' strTabela_Banco = "DEP" & Format("02", "00") & "."
    End If
        
    fl_banco = 0
    
    
    'path na rede
    Operacao = "C:\TESSW\OPERACAO.TXT"
    arq_output = "C:\TEMP.TXT"
    'strPath = "F:\oracle\dados\32bits\"
    strPath = "h:\oracle\dados\32Bits\"
    
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("producao", "VDA610/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("DESENV", "vda610/prod", 0&)
    
    'Conexao Access
    'Set dbaccess = OpenDatabase(strPath & "LIGFIL.MDB")
    'fl_banco = 1
        
        
    'carregar data de faturamento,uf origem, filial origem
          SQL = "select to_char(a.cod_loja,'09') ||'-'||b.nome_fantasia deposito_default,"
    SQL = SQL & " a.cod_filial      "
    SQL = SQL & " from " & strTabela_Banco & "deposito a, loja b "
    SQL = SQL & " where a.cod_loja=b.cod_loja"
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
    
     
    If ss.EOF And ss.BOF Then
        MsgBox "Problemas na consulta de deposito default. Ligue para Depto Sistemas", vbInformation, "Aten��o"
        Exit Sub
    Else
        Deposito_Default = ss!Deposito_Default
        Filial = ss!cod_filial
    End If
                        
    SQL = " Select dt_real"
    SQL = SQL & " from datas"
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
      MsgBox "Tabela de Datas sem registros, verifique", vbInformation, "Aten��o"
      Unload Me
    End If
      
    dt_real = ss!dt_real
    
    
    Set OraParameters = oradatabase.Parameters
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2
    OraParameters("vErro").serverType = ORATYPE_NUMBER
                    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
       Resume
    Else
       Call Process_Line_Errors(SQL)
    End If

End Sub

Private Sub mnuLigacoes_Click()
  Call SSCommand4_Click
End Sub

Private Sub mnuResultado_Click()
  Call SSCommand2_Click
End Sub

Private Sub mnuSair_Click()
  Call SSCommand3_Click
End Sub

Private Sub mnuSobre_Click()
  frmSobre.Show 1
End Sub

Private Sub mnuVendedor_Click()
  Call SSCommand1_Click
End Sub

Private Sub SSCommand1_Click()
  frmVend.Show 1
End Sub



Private Sub SSCommand4_Click()
  frmStatus.Show 1
End Sub


Private Sub SSCommand2_Click()
  frmContatos.Show 1
End Sub


Private Sub SSCommand3_Click()
  End
End Sub


