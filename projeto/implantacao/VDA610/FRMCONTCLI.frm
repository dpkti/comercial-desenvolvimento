VERSION 5.00
Object = "{28D47522-CF84-11D1-834C-00A0249F0C28}#1.0#0"; "GIF89.DLL"
Object = "{F5BE8BC2-7DE6-11D0-91FE-00C04FD701A5}#2.0#0"; "agentctl.dll"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmContCli 
   Caption         =   "Contatos por cliente"
   ClientHeight    =   6945
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14490
   LinkTopic       =   "Form1"
   ScaleHeight     =   6945
   ScaleWidth      =   14490
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboRegional 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   5880
      TabIndex        =   13
      Top             =   30
      Width           =   2535
   End
   Begin VB.TextBox txtNomeCliente 
      Appearance      =   0  'Flat
      Height          =   255
      Left            =   3000
      TabIndex        =   4
      Top             =   420
      Width           =   4095
   End
   Begin VB.TextBox txtCodCliente 
      Appearance      =   0  'Flat
      Height          =   255
      Left            =   1800
      MaxLength       =   6
      TabIndex        =   3
      Top             =   405
      Width           =   1095
   End
   Begin VB.TextBox txtDataIni 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2640
      MaxLength       =   8
      TabIndex        =   1
      Top             =   60
      Width           =   975
   End
   Begin VB.TextBox txtDataFim 
      Appearance      =   0  'Flat
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3960
      MaxLength       =   8
      TabIndex        =   2
      Top             =   60
      Width           =   975
   End
   Begin MSFlexGridLib.MSFlexGrid gridContatosCliente 
      Height          =   6135
      Left            =   -15
      TabIndex        =   7
      Top             =   840
      Visible         =   0   'False
      Width           =   14535
      _ExtentX        =   25638
      _ExtentY        =   10821
      _Version        =   393216
      FixedCols       =   0
      ForeColor       =   8388608
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      AllowUserResizing=   3
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   690
      Left            =   12945
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMCONTCLI.frx":0000
      PICN            =   "FRMCONTCLI.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   90
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Sobre"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMCONTCLI.frx":0CF6
      PICN            =   "FRMCONTCLI.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   12210
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Filtrar Contatos"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMCONTCLI.frx":19EC
      PICN            =   "FRMCONTCLI.frx":1A08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   8
      Top             =   750
      Width           =   11745
      _ExtentX        =   20717
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd Exporta_Excel 
      Height          =   690
      Left            =   13680
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Exportar Excel"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMCONTCLI.frx":26E2
      PICN            =   "FRMCONTCLI.frx":26FE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin GIF89LibCtl.Gif89a gifDPK 
      Height          =   1380
      Left            =   6345
      OleObjectBlob   =   "FRMCONTCLI.frx":3998
      TabIndex        =   15
      Top             =   2782
      Width           =   1800
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Regional"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   5160
      TabIndex        =   14
      Top             =   75
      Width           =   675
   End
   Begin VB.Label lbCliente 
      Caption         =   "Cliente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   11
      Top             =   420
      Width           =   735
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Per�odo de Contato"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   960
      TabIndex        =   10
      Top             =   60
      Width           =   1605
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3720
      TabIndex        =   9
      Top             =   60
      Width           =   135
   End
   Begin AgentObjectsCtl.Agent Agent1 
      Left            =   240
      Top             =   120
   End
End
Attribute VB_Name = "frmContCli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdConsultar_Click()
    Dim vTabContatos As Object
    Dim vDataIni As Date
    Dim vDataFim As Date
    On Error GoTo Trata_Erro
  
    If (txtDataIni.Text = "") And (txtDataFim.Text <> "") Then
        MsgBox "Selecione um per�odo.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    vDataIni = CDate(txtDataIni.Text)
    vDataFim = CDate(txtDataFim.Text)
    
    If vDataIni > vDataFim Then
        MsgBox "Data inicial n�o pode ser maior que a data final.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If cboRegional = "" Then
        MsgBox "Selecione uma regional", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    'FELIPE CASSIANI - TI-1395 - anima��o de espera para carregar dados
    gifDPK.FileName = "H:\ORACLE\DADOS\PADRAO\LOGOTIPO\DPK_logo.gif"
    gifDPK.Visible = True
        
    ' Verifica se foi selecionado um per�odo maior que uma semana
    'If vDataFim - vDataIni > 6 Then
    '    MsgBox "Per�odo informado maior que uma semana.", vbInformation, "Aten��o"
    'End If
    
    Screen.MousePointer = 11
    DoEvents
    
    Set vTabContatos = BuscaContatosPorCliente(vDataIni, vDataFim, Val(txtCodCliente.Text), CInt(Mid(cboRegional, 1, 4)))
    
    gifDPK.Visible = False
    
    If vTabContatos Is Nothing Then
        MsgBox "N�o h� contatos para o per�odo escolhido", vbInformation, "Aten��o"
        gridContatosCliente.Visible = False
        Screen.MousePointer = 0
        Exit Sub
    End If
        
    gridContatosCliente.Visible = True

    gridContatosCliente.Clear
    With gridContatosCliente
        .Cols = 11
        .Rows = vTabContatos.RecordCount + 1
        ' Tamanhos das colunas
        .ColWidth(0) = 1200
        .ColWidth(1) = 4000
        .ColWidth(2) = 1200
        .ColWidth(3) = 2000
        .ColWidth(4) = 500
        .ColWidth(5) = 500
        .ColWidth(6) = 500
        .ColWidth(7) = 500
        .ColWidth(8) = 500
        .ColWidth(9) = 800
        .ColWidth(10) = 1000
        
        ' Titulos das colunas
        .TextMatrix(0, 0) = "C�d. Cliente"
        .TextMatrix(0, 1) = "Nome"
        .TextMatrix(0, 2) = "C�d. Vend."
        .TextMatrix(0, 3) = "Nome Vend."
        .TextMatrix(0, 4) = "Seg."
        .TextMatrix(0, 5) = "Ter."
        .TextMatrix(0, 6) = "Qua."
        .TextMatrix(0, 7) = "Qui."
        .TextMatrix(0, 8) = "Sex."
        .TextMatrix(0, 9) = "Qtd. NF"
        .TextMatrix(0, 10) = "Tx. Conv."
    
        vTabContatos.MoveFirst
    
        For i = 1 To .Rows - 1
            .TextMatrix(i, 0) = vTabContatos!cod_CLIENTE
            .TextMatrix(i, 1) = vTabContatos!NOME_CLIENTE
            .TextMatrix(i, 2) = vTabContatos!COD_REPRES
            .TextMatrix(i, 3) = vTabContatos!PSEUDONIMO
            .TextMatrix(i, 4) = vTabContatos!Seg
            .TextMatrix(i, 5) = vTabContatos!Ter
            .TextMatrix(i, 6) = vTabContatos!Qua
            .TextMatrix(i, 7) = vTabContatos!Qui
            .TextMatrix(i, 8) = vTabContatos!Sex
            .TextMatrix(i, 9) = vTabContatos!Qtdnf
            .TextMatrix(i, 10) = Format(vTabContatos!Tx_Conversao, "0.00")
                        
            vTabContatos.MoveNext
        Next
    End With
    
    Set vTabContatos = Nothing
    ' Alinhamento do conte�do das celulas
    gridContatosCliente.ColAlignment(0) = 2
    gridContatosCliente.ColAlignment(1) = 2
    gridContatosCliente.ColAlignment(2) = 2
    gridContatosCliente.ColAlignment(3) = 2
    gridContatosCliente.ColAlignment(4) = 2
    gridContatosCliente.ColAlignment(5) = 2
    gridContatosCliente.ColAlignment(6) = 2
    gridContatosCliente.ColAlignment(7) = 2
    gridContatosCliente.ColAlignment(8) = 2
    gridContatosCliente.ColAlignment(9) = 2
    gridContatosCliente.ColAlignment(10) = 2
    
    Screen.MousePointer = 0
    
    Exit Sub
    
Trata_Erro:
    If Err.Number = -2147213310 Then
        Resume Next
    Else
       Call Process_Line_Errors(SQL)
    End If
End Sub

Private Sub cmdVoltar_Click()
    On Error GoTo Genio_Erro
    
    'Amelo - Melhoria Roteirizacao - TI-3848
    If flHabGenio = "S" Then
    Agent1.Characters.Unload "Genie"
    End If

    Unload Me

Genio_Erro:
    If Err.Number = -2147213310 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If
End Sub

Private Sub cmdImprimir_Click()
    On Error GoTo Trata_Erro
       
    If gridContatosCliente.Visible = True Then
        Dim iTamanho As Integer
        iTamanho = gridContatosCliente.Width
        gridContatosCliente.Width = Printer.Width
        Printer.PaintPicture gridContatosCliente.Picture, 0, 0
        Printer.EndDoc
        gridContatosCliente.Width = iTamanho
        MsgBox "Impress�o encerrada com sucesso", vbInformation, "Aten��o"
    Else
        MsgBox "Para gerar o arquivo � preciso fazer uma sele��o", _
           vbInformation, "Aten��o"
        Exit Sub
    End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub Exporta_Excel_Click()
' Rotina para Exportar relat�rio para Excel
    If cboRegional.Text = "" Then
        MsgBox "Selecionar os campos Regional", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf txtDataIni.Text = "" Then
        MsgBox "Preencher os Campo Periodo de Contato", vbExclamation, "Alerta"
        txtDataIni.SetFocus
    ElseIf txtDataFim.Text = "" Then
        MsgBox "Preencher os Campo Periodo de Contato", vbExclamation, "Alerta"
        txtDataFim.SetFocus
    ElseIf gridContatosCliente.Rows = 1 Then
        MsgBox "� necess�rio gerar o relat�rio para realizar a exporta��o.", vbExclamation, "Alerta"
    Else
    ' Definindo o diret�rio onde o arquivo ser� salvo
    'cdDiretorio.InitDir = "c:\"
    'cdDiretorio.Filter = "Microsoft Excel Workbooks (*.xls)*.xls"

On Error GoTo ErrHandler
'cdDiretorio.ShowSave

ErrHandler:
If Err.Number = 32755 Then
    Exit Sub
End If

' Definindo vari�veis para exportar para Excel
Dim appExcel As excel.Application
Dim lngRows As Long
Dim lngCols As Long
Dim sNomeArq As String

'Exportando Dados do Grid no Excel
        Screen.MousePointer = vbHourglass
        frmContCli.cmdConsultar.Enabled = False
        frmContCli.cmdImprimir.Enabled = False
        frmContCli.cmdVoltar.Enabled = False
        frmContCli.Exporta_Excel.Enabled = False


        Set appExcel = New excel.Application

        appExcel.Workbooks.Add

        For lngRows = 0 To gridContatosCliente.Rows - 1
            For lngCols = 1 To gridContatosCliente.Cols - 1
                appExcel.Cells(lngRows + 1, lngCols) = gridContatosCliente.TextMatrix(lngRows, lngCols)
            Next lngCols
        Next lngRows

        appExcel.Columns.AutoFit

' Verificando a vers�o do Excel Instalada no micro do usu�rio para salvar o Relat�rio com a mesma extens�o da vers�o
Dim a As String
a = appExcel.Version
If (appExcel.Version >= 12) Then
    sNomeArq = "C:\WINDOWS\TEMP\PLAN_TEMP.XLSX"
    appExcel.ActiveWorkbook.SaveAs sNomeArq 'cdDiretorio.FileName & ".xlsx"
Else
    sNomeArq = "C:\WINDOWS\TEMP\PLAN_TEMP.XLS"
    appExcel.ActiveWorkbook.SaveAs sNomeArq  'cdDiretorio.FileName & ".xls"
End If
'Encerrado Excel e finalizando a utiliza��o do mesmo na mem�ria
        Screen.MousePointer = vbDefault
        frmContCli.cmdConsultar.Enabled = True
        frmContCli.cmdImprimir.Enabled = True
        frmContCli.cmdVoltar.Enabled = True
        frmContCli.Exporta_Excel.Enabled = True

        appExcel.Quit
        Set appExcel = Nothing
        'MsgBox "A exporta��o foi conclu�da com �xito!", vbInformation, "Mensagem do Sistema"
        Shell "cmd.exe /c start " & sNomeArq, vbHide
    End If
End Sub

Private Sub Form_Load()
    txtDataFim.Text = Format(dt_real, "DD/MM/YY")
    txtDataIni.Text = Format(dt_real, "DD/MM/YY")
End Sub
Private Sub txtCodCliente_Change()
    If txtCodCliente.Text = "" Then
        txtNomeCliente.Text = ""
    End If
End Sub

Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
    'KeyAscii = VB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodCliente_LostFocus()
    If Val(txtCodCliente.Text) > 0 Then
        CarregaCliente
    End If
End Sub

Private Sub txtDataFim_Change()
    If txtDataFim.Text = "" Then
        gridContatosCliente.Visible = False
    End If
End Sub

Private Sub txtDataFim_KeyPress(KeyAscii As Integer)
   vVB_Generica_001.Data KeyAscii, txtDataFim
End Sub

Private Sub txtDataIni_Change()
    If txtDataIni.Text = "" Then
        gridContatosCliente.Visible = False
    End If
End Sub

Private Sub txtDataIni_KeyPress(KeyAscii As Integer)
   vVB_Generica_001.Data KeyAscii, txtDataIni
End Sub

Public Sub CarregaCliente()
    Dim vTabCliente As Object
    
    If Val(txtCodCliente.Text) <> 0 Then
        Set vTabCliente = BuscaClientePorCodigo(Val(txtCodCliente.Text))
    End If
    If Not vTabCliente Is Nothing Then
        txtNomeCliente.Text = vTabCliente!NOME_CLIENTE
        DoEvents
    Else
        MsgBox "Cliente n�o encontrado."
        txtCodCliente.SetFocus
    End If
    Set vTabCliente = Nothing
End Sub

Private Sub txtNomeCliente_DblClick()
    frmClienteFimPedido.Form_Que_Chamou = "frmContCli"
    frmClienteFimPedido.Show 1
    StayOnTop frmClienteFimPedido
End Sub

Private Sub txtNomeCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub

'FELIPE CASSIANI - TI1395
Private Sub cboRegional_Dropdown()
  Dim ss As Object
  Dim i As Long
  
  If cboRegional.ListCount = 0 Then
     vBanco.Parameters.Remove "tab"
     vBanco.Parameters.Add "tab", strTabela_Banco, 1
     
     vBanco.Parameters.Remove "vErro"
     vBanco.Parameters.Add "vErro", 0, 2
     
     Criar_Cursor
     vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.Pck_vda610.pr_regionais(:vCursor,:tab,:vErro)")
  
     Set ss = vBanco.Parameters("vCursor").Value
     vBanco.Parameters.Remove "vCursor"
     
     For i = 1 To ss.RecordCount
       cboRegional.AddItem Format(ss!cod_filial, "0000") & "-" & ss!nome_filial
       
       ss.MoveNext
     Next
  
  End If
End Sub
