VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmEstat 
   Caption         =   "Contatos"
   ClientHeight    =   6720
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11640
   LinkTopic       =   "Form1"
   ScaleHeight     =   6720
   ScaleWidth      =   11640
   StartUpPosition =   1  'CenterOwner
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11640
      _Version        =   65536
      _ExtentX        =   20532
      _ExtentY        =   2170
      _StockProps     =   15
      ForeColor       =   -2147483640
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin MSComDlg.CommonDialog cdDiretorio 
         Left            =   9960
         Top             =   240
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
         CancelError     =   -1  'True
      End
      Begin VB.ComboBox cboRegional 
         Height          =   315
         Left            =   2400
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   180
         Width           =   3015
      End
      Begin VB.ComboBox cboContato 
         Height          =   315
         Left            =   2400
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   660
         Width           =   3015
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   6
         Top             =   1125
         Width           =   11760
         _ExtentX        =   20743
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd Exporta_Excel 
         Height          =   690
         Left            =   7440
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Exportar Excel"
         Top             =   240
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEstat.frx":0000
         PICN            =   "frmEstat.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd1 
         Height          =   690
         Left            =   8280
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Imprimir"
         Top             =   240
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEstat.frx":12B6
         PICN            =   "frmEstat.frx":12D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd2 
         Height          =   690
         Left            =   0
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Voltar"
         Top             =   0
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEstat.frx":1FAC
         PICN            =   "frmEstat.frx":1FC8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand1 
         Height          =   690
         Left            =   6600
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Relat�rio"
         Top             =   240
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmEstat.frx":2CA2
         PICN            =   "frmEstat.frx":2CBE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label1 
         Caption         =   "Regional:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1080
         TabIndex        =   9
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Contato:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1200
         TabIndex        =   8
         Top             =   720
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   5295
      Left            =   120
      TabIndex        =   10
      Top             =   1320
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   9340
      _Version        =   393216
      AllowBigSelection=   0   'False
      FillStyle       =   1
      SelectionMode   =   2
      AllowUserResizing=   3
   End
End
Attribute VB_Name = "frmEstat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public strMes As String

Private Sub cmd1_Click()

' Validando se Combo Reginal e Contato est�o preenchidos para gerar o relatorio.
    If cboRegional.Text = "" And cboContato.Text = "" Then
        MsgBox "Selecionar os campos Regional e Contato", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboRegional.Text = "" And cboContato.Text <> "" Then
        MsgBox "Selcionar o Campo Regional", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboContato.Text = "" And cboRegional.Text <> "" Then
        MsgBox "Selcionar o Campo contato", vbExclamation, "Alerta"
        cboContato.SetFocus
    ElseIf MSFlexGrid1.Rows = 2 Then
        MsgBox "� necess�rio gerar o relat�rio para realizar a impress�o.", vbExclamation, "Alerta"
    Else

        ' Definindo Vari�veis para carregar dados no grid
        Dim i As Integer
        Dim j As Integer
        Dim k As Integer
        Dim pagina As Integer
        Dim Paginas As Integer
        Dim pRegional As String
        Dim pContato As String

        'William Leite
        Screen.MousePointer = vbHourglass
        frmEstat.SSCommand1.Enabled = False
        frmEstat.Exporta_Excel.Enabled = False
        frmEstat.cmd1.Enabled = False

        ' Atribuindo Valores as Vari�veis

        pRegional = cboRegional.Text
        pContato = cboContato.Text
        pagina = Printer.ScaleHeight - 1440

        'Definindo Tamanho da Folha: A4 e Orienta��o da P�gina: Paisagem
        Printer.PaperSize = vbPRPSA4
        Printer.Orientation = vbPRORLandscape
        Printer.FontName = "Calibri"
        Printer.FontSize = "8"
        Printer.CurrentX = 100
        Printer.CurrentY = 100


        'Impress�o do Cabe�alho com fonte em negrito
        Printer.FontBold = True
        Printer.Print
        Printer.Print
        Printer.Print Tab(5); "Regional"; Tab(19); pRegional; Tab(167); "Data e Hora" & "      "; Date & "  " & Now;
        Printer.Print Tab(5); "Contato"; Tab(19); pContato; Tab(167); "Pagina:"; Printer.Page
        Printer.Print
        For k = 5 To MSFlexGrid1.Cols
            If (k = 5) Then
                Printer.Print Tab(62); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            ElseIf (k = 7) Then
                Printer.Print Tab(86); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            ElseIf (k = 9) Then
                Printer.Print Tab(110); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            ElseIf (k = 11) Then
                Printer.Print Tab(134); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            ElseIf (k = 13) Then
                Printer.Print Tab(158); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            ElseIf (k = 15) Then
                Printer.Print Tab(180); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
            End If
        Next
        ' Impress�o do corpo do relat�rio
        For j = 0 To (MSFlexGrid1.Cols - 1)
            If (j = 1) Then
                Printer.Print Tab(5); MSFlexGrid1.TextMatrix(0, j);
            ElseIf (j = 2) Then
                Printer.Print Tab(12); MSFlexGrid1.TextMatrix(0, j);
            ElseIf (j = 3) Then
                Printer.Print Tab(47); MSFlexGrid1.TextMatrix(0, j);
            ElseIf (j = 4) Then
                Printer.Print Tab(52); MSFlexGrid1.TextMatrix(0, j);
            ElseIf (j = 5) Then
                Printer.Print Tab(62); "Contato";
            ElseIf (j = 6) Then
                Printer.Print Tab(70); "Fat.";
            ElseIf (j = 7) Then
                Printer.Print Tab(82); "Contato";
            ElseIf (j = 8) Then
                Printer.Print Tab(90); "Fat.";
            ElseIf (j = 9) Then
                Printer.Print Tab(106); "Contato";
            ElseIf (j = 10) Then
                Printer.Print Tab(114); "Fat.";
            ElseIf (j = 11) Then
                Printer.Print Tab(130); "Contato";
            ElseIf (j = 12) Then
                Printer.Print Tab(138); "Fat.";
            ElseIf (j = 13) Then
                Printer.Print Tab(154); "Contato";
            ElseIf (j = 14) Then
                Printer.Print Tab(162); "Fat.";
            ElseIf (j = 15) Then
                Printer.Print Tab(176); "Contato";
            ElseIf (j = 16) Then
                Printer.Print Tab(184); "Fat.";
            End If
        Next j
        Printer.Print Tab(5); "_____________________________________________________________________________________________________________________________________________________________________________________________________________";
        Printer.Print
        Printer.Print
        Printer.FontBold = False

        For i = 1 To MSFlexGrid1.Rows - 1
            'Impress�o do Cabe�alho ap�s Quebra de P�gina
            If (i Mod 47) = 0 And i <> 0 Then
                Printer.NewPage
                Printer.FontBold = True
                Printer.Print
                Printer.Print
                Printer.Print Tab(5); "Regional"; Tab(19); pRegional; Tab(167); "Data e Hora" & "      "; Date & "  " & Format(Now, "hh:mm");
                Printer.Print Tab(5); "Contato"; Tab(19); pContato; Tab(167); "Pagina:"; Printer.Page
                Printer.Print
                For k = 5 To MSFlexGrid1.Cols
                    If (k = 5) Then
                        Printer.Print Tab(62); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    ElseIf (k = 7) Then
                        Printer.Print Tab(86); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    ElseIf (k = 9) Then
                        Printer.Print Tab(110); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    ElseIf (k = 11) Then
                        Printer.Print Tab(134); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    ElseIf (k = 13) Then
                        Printer.Print Tab(158); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    ElseIf (k = 15) Then
                        Printer.Print Tab(180); Mid(MSFlexGrid1.TextMatrix(0, k), 6, Len(MSFlexGrid1.TextMatrix(0, k)));
                    End If
                Next
                ' Impress�o do Corpo do Relat�rio ap�s Quebra de P�gina
                For j = 0 To (MSFlexGrid1.Cols - 1)
                    If (j = 1) Then
                        Printer.Print Tab(5); MSFlexGrid1.TextMatrix(0, j);
                    ElseIf (j = 2) Then
                        Printer.Print Tab(12); MSFlexGrid1.TextMatrix(0, j);
                    ElseIf (j = 3) Then
                        Printer.Print Tab(47); MSFlexGrid1.TextMatrix(0, j);
                    ElseIf (j = 4) Then
                        Printer.Print Tab(52); MSFlexGrid1.TextMatrix(0, j);
                    ElseIf (j = 5) Then
                        Printer.Print Tab(62); "Contato";
                    ElseIf (j = 6) Then
                        Printer.Print Tab(70); "Fat.";
                    ElseIf (j = 7) Then
                        Printer.Print Tab(82); "Contato";
                    ElseIf (j = 8) Then
                        Printer.Print Tab(90); "Fat.";
                    ElseIf (j = 9) Then
                        Printer.Print Tab(106); "Contato";
                    ElseIf (j = 10) Then
                        Printer.Print Tab(114); "Fat.";
                    ElseIf (j = 11) Then
                        Printer.Print Tab(130); "Contato";
                    ElseIf (j = 12) Then
                        Printer.Print Tab(138); "Fat.";
                    ElseIf (j = 13) Then
                        Printer.Print Tab(154); "Contato";
                    ElseIf (j = 14) Then
                        Printer.Print Tab(162); "Fat.";
                    ElseIf (j = 15) Then
                        Printer.Print Tab(176); "Contato";
                    ElseIf (j = 16) Then
                        Printer.Print Tab(184); "Fat.";
                    End If
                Next j
                Printer.Print Tab(5); "_____________________________________________________________________________________________________________________________________________________________________________________________________________";
                Printer.Print
                Printer.Print
                Printer.FontBold = False
            End If
            ' Imprimindo valores no Relat�rio recebidos do Banco
            For j = 0 To (MSFlexGrid1.Cols - 1)
                If (j = 1) Then
                    Printer.Print Tab(5); MSFlexGrid1.TextMatrix(i, j);
                ElseIf (j = 2) Then
                    Printer.Print Tab(12); MSFlexGrid1.TextMatrix(i, j);
                ElseIf (j = 3) Then
                    Printer.Print Tab(47); MSFlexGrid1.TextMatrix(i, j);
                ElseIf (j = 4) Then
                    Printer.Print Tab(52); MSFlexGrid1.TextMatrix(i, j);
                ElseIf (j = 5) Then
                    Printer.Print Tab(62); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 6) Then
                    Printer.Print Tab(70); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                ElseIf (j = 7) Then
                    Printer.Print Tab(82); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 8) Then
                    Printer.Print Tab(90); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                ElseIf (j = 9) Then
                    Printer.Print Tab(106); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 10) Then
                    Printer.Print Tab(114); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                ElseIf (j = 11) Then
                    Printer.Print Tab(130); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 12) Then
                    Printer.Print Tab(138); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                ElseIf (j = 13) Then
                    Printer.Print Tab(154); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 14) Then
                    Printer.Print Tab(162); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                ElseIf (j = 15) Then
                    Printer.Print Tab(176); Format$((MSFlexGrid1.TextMatrix(i, j)), SpcRel(MSFlexGrid1.TextMatrix(i, j), True));
                ElseIf (j = 16) Then
                    Printer.Print Tab(184); Replace(Format$(Format(MSFlexGrid1.TextMatrix(i, j), "##,##0"), SpcRel(MSFlexGrid1.TextMatrix(i, j), False)), ",", ".");
                End If
            Next j
        Next i
        Printer.EndDoc
    End If
    Screen.MousePointer = vbDefault
    frmEstat.SSCommand1.Enabled = True
    frmEstat.Exporta_Excel.Enabled = True
    frmEstat.cmd1.Enabled = True


End Sub

Private Sub cmd2_Click()
    Unload Me
End Sub

Private Sub Exporta_Excel_Click()
    ' Rotina para Exportar relat�rio para Excel
    If cboRegional.Text = "" And cboContato.Text = "" Then
        MsgBox "Selecionar os campos Regional e Contato", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboRegional.Text = "" And cboContato.Text <> "" Then
        MsgBox "Selcionar o Campo Regional", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboContato.Text = "" And cboRegional.Text <> "" Then
        MsgBox "Selcionar o Campo contato", vbExclamation, "Alerta"
        cboContato.SetFocus
    ElseIf MSFlexGrid1.Rows = 2 Then
        MsgBox "� necess�rio gerar o relat�rio para realizar a exporta��o.", vbExclamation, "Alerta"
    Else
    ' Definindo o diret�rio onde o arquivo ser� salvo
    'cdDiretorio.InitDir = "c:\"
    'cdDiretorio.Filter = "Microsoft Excel Workbooks (*.xls)*.xls"

On Error GoTo ErrHandler
'cdDiretorio.ShowSave

ErrHandler:
If Err.Number = 32755 Then
    Exit Sub
End If

' Definindo vari�veis para exportar para Excel
Dim appExcel As excel.Application
Dim lngRows As Long
Dim lngCols As Long
Dim sNomeArq As String

'Exportando Dados do Grid no Excel
        Screen.MousePointer = vbHourglass
        frmEstat.SSCommand1.Enabled = False
        frmEstat.Exporta_Excel.Enabled = False
        frmEstat.cmd1.Enabled = False


        Set appExcel = New excel.Application

        appExcel.Workbooks.Add

        For lngRows = 0 To MSFlexGrid1.Rows - 1
            For lngCols = 1 To MSFlexGrid1.Cols - 1
                appExcel.Cells(lngRows + 1, lngCols) = MSFlexGrid1.TextMatrix(lngRows, lngCols)
            Next lngCols
        Next lngRows

        appExcel.Columns.AutoFit

' Verificando a vers�o do Excel Instalada no micro do usu�rio para salvar o Relat�rio com a mesma extens�o da vers�o
Dim a As String
a = appExcel.Version
If (appExcel.Version >= 12) Then
    sNomeArq = "C:\WINDOWS\TEMP\PLAN_TEMP.XLSX"
    appExcel.ActiveWorkbook.SaveAs sNomeArq 'cdDiretorio.FileName & ".xlsx"
Else
    sNomeArq = "C:\WINDOWS\TEMP\PLAN_TEMP.XLS"
    appExcel.ActiveWorkbook.SaveAs sNomeArq  'cdDiretorio.FileName & ".xls"
End If
'Encerrado Excel e finalizando a utiliza��o do mesmo na mem�ria
        Screen.MousePointer = vbDefault
        frmEstat.SSCommand1.Enabled = True
        frmEstat.Exporta_Excel.Enabled = True
        frmEstat.cmd1.Enabled = True

        appExcel.Quit
        Set appExcel = Nothing
        'MsgBox "A exporta��o foi conclu�da com �xito!", vbInformation, "Mensagem do Sistema"
        Shell "cmd.exe /c start " & sNomeArq, vbHide
    End If


End Sub

Private Sub Form_Load()
    Dim ss As Object
    Dim i As Long
    Dim pmBanco As String
  ctrl = False
  Screen.MousePointer = vbArrowHourglass
  
    If cboRegional.ListCount = 0 Then

        vBanco.Parameters.Remove "tab"
        vBanco.Parameters.Add "tab", strTabela_Banco, 2

        vBanco.Parameters.Remove "vErro"
        vBanco.Parameters.Add "vErro", 0, 1

        Criar_Cursor
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.Pck_vda610.pr_regionais(:vCursor,:tab,:vErro)")

        Set ss = vBanco.Parameters("vCursor").Value
        vBanco.Parameters.Remove "vCursor"

        For i = 1 To ss.RecordCount
            cboRegional.AddItem Format(ss!cod_filial, "0000") & "-" & ss!nome_filial
       
       ss.MoveNext
     Next
  End If
    cboContato.AddItem "Ativo"
    cboContato.AddItem "Receptivo"
         
    'William Leite - Tratamento Banco Multi
    If strTabela_Banco = "PRODUCAO." Then
        pmBanco = "HELPDESK."
    Else
        pmBanco = strTabela_Banco
    End If
    
    vBanco.Parameters.Remove "Pm_Cod_Sistema"
    vBanco.Parameters.Add "Pm_Cod_Sistema", 1104, 1
    
    vBanco.Parameters.Remove "Pm_Nome_Param"
    vBanco.Parameters.Add "Pm_Nome_Param", "QTD_MES", 2
    
    vBanco.Parameters.Remove "Pm_Dep"
    vBanco.Parameters.Add "Pm_Dep", pmBanco, 2

    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.Pck_vda610.Pr_Select_Vl_Parametro2(:vCursor, :Pm_Cod_Sistema, :Pm_Nome_Param, :Pm_Dep)")
    
End Sub


Public Sub SSCommand1_Click()
    Dim Regionaltxt As String
    Dim Contatotxt As String
    Dim j As Long
    Dim k As Integer
    Dim total As Single
    Dim sTotais() As Double
    'Valida��o dos Combos Regional e Contato
    If cboRegional.Text = "" And cboContato.Text = "" Then
        MsgBox "Selecionar os campos Regional e Contato", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboRegional.Text = "" And cboContato.Text <> "" Then
        MsgBox "Selcionar o Campo Regional", vbExclamation, "Alerta"
        cboRegional.SetFocus
    ElseIf cboContato.Text = "" And cboRegional.Text <> "" Then
        MsgBox "Selcionar o Campo contato", vbExclamation, "Alerta"
        cboContato.SetFocus
    Else
        Screen.MousePointer = vbArrowHourglass
        'Aguardando...
        'William Leite
        frmEstat.SSCommand1.Enabled = False
        frmEstat.Exporta_Excel.Enabled = False
        frmEstat.cmd1.Enabled = False

        Regionaltxt = Left(cboRegional.Text, 4)
        If cboContato.Text = "Ativo" Then
            Contatotxt = "1"
        Else
            Contatotxt = "2"
        End If
         ' Carregar dados do Banco de Dados para exibir no Grid
        ' Gera relat�rio
        ' Limpar grid
        Screen.MousePointer = 11
        DoEvents
             If MSFlexGrid1.Rows > 2 Then
                For i = 1 To ((MSFlexGrid1.Rows - 2))
                   MSFlexGrid1.RemoveItem (1)
                Next
                    MSFlexGrid1.Clear
             End If

        'WILLIAM LEITE
        vBanco.Parameters.Remove "pcd_filial"
        vBanco.Parameters.Add "pcd_filial", Regionaltxt, 1

        vBanco.Parameters.Remove "ptipo_ligacao"
        vBanco.Parameters.Add "ptipo_ligacao", Contatotxt, 1

        vBanco.Parameters.Remove "pmBanco"
        vBanco.Parameters.Add "pmBanco", strTabela_Banco, 2

        Criar_Cursor
        
        'William Leite
        vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.Pck_vda610.Pr_Est_contatos(:vCursor, :pcd_filial, :ptipo_ligacao, :pmBanco)")
        Set ss = vBanco.Parameters("vCursor").Value
        vBanco.Parameters.Remove "vCursor"

            MSFlexGrid1.Cols = (ss.Fields.Count + 1)
            MSFlexGrid1.AddItem " "
            For j = 1 To (MSFlexGrid1.Cols - 1)
                'If (j > 5) Then
                If (j > (MSFlexGrid1.Cols - 1)) Then
                    If (j Mod 2 = 0) Then
                        MSFlexGrid1.TextMatrix(x, j) = TMesGrid(ss(j - 1).Name, False)
                    Else
                        MSFlexGrid1.TextMatrix(x, j) = TMesGrid(ss(j - 1).Name, True)
                    End If
                Else
                    MSFlexGrid1.TextMatrix(x, j) = ss(j - 1).Name
                End If
             Next

        ReDim sTotais(ss.Fields.Count)

        If ss.RecordCount = 0 Then
            Screen.MousePointer = vbArrow
            
            frmEstat.SSCommand1.Enabled = True
            frmEstat.Exporta_Excel.Enabled = True
            frmEstat.cmd1.Enabled = True

            MsgBox "O relat�rio para Regional: " & cboRegional.Text & " e Contato: " & cboContato.Text & " n�o possui informa��es.", vbExclamation, "Alerta"
        Else
            For i = 1 To ss.RecordCount
                ' Ocultando a primeira coluna do Grid
                MSFlexGrid1.ColWidth(0) = 0

                ' Carregando dados no Grid

                For j = 1 To (MSFlexGrid1.Cols - 1)


                        If (j > 4) Then
                                sTotais(j) = sTotais(j) + ss(j - 1)
                                If (j Mod 2 = 0) Then
                                   If (ss(j - 1) <> 0) Then
                                        MSFlexGrid1.TextMatrix(i, j) = Format(Mid(ss(j - 1), 1, (Len(ss(j - 1)) - 3)), "##,##0")
                                    Else
                                        MSFlexGrid1.TextMatrix(i, j) = ss(j - 1)
                                    End If
                                 Else
                                    MSFlexGrid1.TextMatrix(i, j) = ss(j - 1)
                                End If
                        Else
                                MSFlexGrid1.TextMatrix(i, j) = ss(j - 1)
                        End If
                    Next

                MSFlexGrid1.AddItem " "
                ss.MoveNext
            Next

            For k = 5 To (MSFlexGrid1.Cols - 1)
                If (k Mod 2 = 0) Then
                    MSFlexGrid1.TextMatrix((MSFlexGrid1.Rows - 1), k) = Format(sTotais(k), "##,##0")
                Else
                    MSFlexGrid1.TextMatrix((MSFlexGrid1.Rows - 1), k) = sTotais(k)
                End If
            Next

            MSFlexGrid1.TextMatrix((MSFlexGrid1.Rows - 1), 1) = "Total"
        End If

        'Ajustando Tamanho das Colunas do Grid
        MSFlexGrid1.ColWidth(1) = 800
        MSFlexGrid1.ColWidth(2) = 3500
        MSFlexGrid1.ColWidth(3) = 500
        MSFlexGrid1.ColWidth(4) = 1000
        For j = 1 To (MSFlexGrid1.Cols - 1)
            If (j > 4) Then
                MSFlexGrid1.ColWidth(j) = 1550
            End If
        Next
        Screen.MousePointer = vbDefault
            'Mensagem informando o usu�rio que a quantidade de meses de acordo com o parametro no banco de dados excede
            'a quantidade de campos que podem ser impressos na Folha do Relat�rio que � Tamanho A4.
            If (ss.Fields.Count > 16) Then
                MsgBox "O relat�rio gerado cont�m mais de 5 meses. A impress�o contemplar� os 5 primeiros meses.", vbInformation, "Mensagem do Sistema"
            End If
    End If
    'William Leite
    Screen.MousePointer = vbDefault
    frmEstat.SSCommand1.Enabled = True
    frmEstat.Exporta_Excel.Enabled = True
    frmEstat.cmd1.Enabled = True
    DoEvents
End Sub
Private Function TMesGrid(mes As String, tipo As Boolean)
' Traduzindo as colunas do GRID para Portugu�s pois as mesmas retornam do Banco de Dados em Ingl�s
If tipo = True Then
    Select Case UCase(mes)
    Case "CONT_JANUARY"
        TMesGrid = "CONT_JANEIRO"
    Case "CONT_FEBRUARY"
        TMesGrid = "CONT_FEVEREIRO"
    Case "CONT_MARCH"
        TMesGrid = "CONT_MAR�O"
    Case "CONT_APRIL"
        TMesGrid = "CONT_ABRIL"
    Case "CONT_MAY"
        TMesGrid = "CONT_MAIO"
    Case "CONT_JUNE"
        TMesGrid = "CONT_JUNHO"
    Case "CONT_JULY"
        TMesGrid = "CONT_JULHO"
    Case "CONT_AUGUST"
        TMesGrid = "CONT_AGOSTO"
    Case "CONT_SEPTEMBER"
        TMesGrid = "CONT_SETEMBRO"
    Case "CONT_OCTOBER"
        TMesGrid = "CONT_OUTUBRO"
    Case "CONT_NOVEMBER"
        TMesGrid = "CONT_NOVEMBRO"
    Case "CONT_DECEMBER"
        TMesGrid = "CONT_DEZEMBRO"
    End Select
Else
    Select Case UCase(mes)
    Case "FAT_JANUARY"
        TMesGrid = "FAT_JANEIRO"
    Case "FAT_FEBRUARY"
        TMesGrid = "FAT_FEVEREIRO"
    Case "FAT_MARCH"
        TMesGrid = "FAT_MAR�O"
    Case "FAT_APRIL"
        TMesGrid = "FAT_ABRIL"
    Case "FAT_MAY"
        TMesGrid = "FAT_MAIO"
    Case "FAT_JUNE"
        TMesGrid = "FAT_JUNHO"
    Case "FAT_JULY"
        TMesGrid = "FAT_JULHO"
    Case "FAT_AUGUST"
        TMesGrid = "FAT_AGOSTO"
    Case "FAT_SEPTEMBER"
        TMesGrid = "FAT_SETEMBRO"
    Case "FAT_OCTOBER"
        TMesGrid = "FAT_OUTUBRO"
    Case "FAT_NOVEMBER"
        TMesGrid = "FAT_NOVEMBRO"
    Case "FAT_DECEMBER"
        TMesGrid = "FAT_DEZEMBRO"
    End Select
End If
End Function
Private Function SpcRel(campo As String, tpCampo As Boolean)
' C�digo que preenche os espa�os em branco na impress�o nas colunas Contatos e Faturamento alinhando � direita os dados
If tpCampo = True Then
    Select Case Len(campo)
    Case 1
        SpcRel = "@@@@@@@@@@@@@"
    Case 2
        SpcRel = "@@@@@@@@@@@@"
    Case 3
        SpcRel = "@@@@@@@@@@@"
    Case 4
        SpcRel = "@@@@@@@@@"
    Case 5
        SpcRel = "@@@@@@@@"
    Case 6
        SpcRel = "@@@@@@@"
    Case 7
        SpcRel = "@@@@@@"
    Case 8
        SpcRel = "@@@@@"
    Case 9
        SpcRel = "@@@@"
    Case 10
        SpcRel = "@@@"
    Case 11
        SpcRel = "@@"
    Case 12
        SpcRel = "@"
    Case Else
        SpcRel = ""
    End Select
Else
    Select Case Len(campo)
    Case 1
        SpcRel = "@@@@@@@@@@@@@@@@@@"
    Case 2
        SpcRel = "@@@@@@@@@@@@@@@@@"
    Case 3
        SpcRel = "@@@@@@@@@@@@@@@@"
    Case 4
        SpcRel = "@@@@@@@@@@@@@@"
    Case 5
        SpcRel = "@@@@@@@@@@@@@"
    Case 6
        SpcRel = "@@@@@@@@@@@@"
    Case 7
        SpcRel = "@@@@@@@@@@@"
    Case 8
        SpcRel = "@@@@@@@@@@"
    Case 9
        SpcRel = "@@@@@@@@@"
    Case 10
        SpcRel = "@@@@@@@@"
    Case 11
        SpcRel = "@@@@@@@"
    Case 12
        SpcRel = "@@@@@@"
    Case 13
        SpcRel = "@@@@@"
    End Select
End If
End Function
