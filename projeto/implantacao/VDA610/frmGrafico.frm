VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmGrafico 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gr�fico"
   ClientHeight    =   8205
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   Icon            =   "frmGrafico.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8205
   ScaleWidth      =   11880
   Begin MSComDlg.CommonDialog CD1 
      Left            =   1710
      Top             =   6510
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tipo do Gr�fico"
      Height          =   1245
      Left            =   30
      TabIndex        =   7
      Top             =   2640
      Width           =   1485
      Begin VB.OptionButton optPizza2d 
         Caption         =   "Pizza 2D"
         Height          =   195
         Left            =   90
         TabIndex        =   11
         Top             =   510
         Width           =   1185
      End
      Begin VB.OptionButton optCol2d 
         Caption         =   "Colunas 2D"
         Height          =   195
         Left            =   90
         TabIndex        =   10
         Top             =   990
         Width           =   1245
      End
      Begin VB.OptionButton optCol3D 
         Caption         =   "Colunas 3D"
         Height          =   195
         Left            =   90
         TabIndex        =   9
         Top             =   750
         Width           =   1185
      End
      Begin VB.OptionButton OptPizza3d 
         Caption         =   "Pizza 3D"
         Height          =   195
         Left            =   90
         TabIndex        =   8
         Top             =   270
         Value           =   -1  'True
         Width           =   1185
      End
   End
   Begin VB.ComboBox cboTlmk 
      Height          =   315
      Left            =   30
      TabIndex        =   5
      Text            =   "Combo1"
      Top             =   2220
      Width           =   2235
   End
   Begin VB.ListBox lstOpcoes 
      Height          =   1230
      ItemData        =   "frmGrafico.frx":23D2
      Left            =   30
      List            =   "frmGrafico.frx":23E8
      TabIndex        =   4
      Top             =   900
      Width           =   2235
   End
   Begin GraphLib.Graph Grafico 
      Height          =   6915
      Left            =   2310
      TabIndex        =   3
      Top             =   900
      Width           =   9555
      _Version        =   65536
      _ExtentX        =   16854
      _ExtentY        =   12197
      _StockProps     =   96
      BorderStyle     =   1
      GraphStyle      =   4
      GraphType       =   7
      NumPoints       =   2
      RandomData      =   1
      ColorData       =   0
      ExtraData       =   0
      ExtraData[]     =   0
      FontFamily      =   4
      FontSize        =   4
      FontSize[0]     =   200
      FontSize[1]     =   150
      FontSize[2]     =   100
      FontSize[3]     =   100
      FontStyle       =   4
      GraphData       =   0
      GraphData[]     =   0
      LabelText       =   0
      LegendText      =   0
      PatternData     =   0
      SymbolData      =   0
      XPosData        =   0
      XPosData[]      =   0
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20452
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrafico.frx":2479
      PICN            =   "frmGrafico.frx":2495
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdImprimir 
      Height          =   660
      Left            =   1395
      TabIndex        =   6
      ToolTipText     =   "Imprimir Gr�fico"
      Top             =   3930
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1164
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrafico.frx":316F
      PICN            =   "frmGrafico.frx":318B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCopiar 
      Height          =   660
      Left            =   705
      TabIndex        =   12
      ToolTipText     =   "Copiar Gr�fico"
      Top             =   3930
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1164
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrafico.frx":3E65
      PICN            =   "frmGrafico.frx":3E81
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   660
      Left            =   0
      TabIndex        =   13
      ToolTipText     =   "Salvar Gr�fico"
      Top             =   3930
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1164
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrafico.frx":4B5B
      PICN            =   "frmGrafico.frx":4B77
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAtualizarGrafico 
      Height          =   660
      Left            =   1590
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   3240
      Visible         =   0   'False
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1164
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrafico.frx":5851
      PICN            =   "frmGrafico.frx":586D
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmGrafico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TituloGrafico As String

Private Sub cboTlmk_Click()
    Preenche_Grafico
End Sub

Private Sub cmdAtualizarGrafico_Click()
    If lstOpcoes = "Liga��es Por Resultado" Then
       TituloGrafico = "Liga��es Por Resultado"
       If cboTlmk = "" Then
          MsgBox "Selecione um Representante.", vbInformation, "Aten��o"
          Exit Sub
       Else
          TituloGrafico = TituloGrafico & " - " & cboTlmk
       End If
    End If
    
    Grafico.Visible = True
    
    If optCol2d.Value = True Then
        Grafico.GraphType = 3
    ElseIf optCol3D.Value = True Then
        Grafico.GraphType = 4
    ElseIf optPizza2d.Value = True Then
        Grafico.GraphType = 1
    ElseIf OptPizza3d.Value = True Then
        Grafico.GraphType = 2
    End If
    
    Grafico.DrawMode = gphDraw
    Preenche_Grafico
End Sub

Private Sub cmdCopiar_Click()
    Grafico.DrawMode = gphBlit
    Grafico.DrawMode = gphCopy
End Sub

Private Sub cmdSalvar_Click()
    Dim Caminho As String
    CD1.ShowSave
    Caminho = CD1.FileName
    If Caminho <> "" Then
        Grafico.DrawMode = gphBlit
        Grafico.ImageFile = Caminho
        Grafico.DrawMode = gphWrite
    Else
        MsgBox "Defina um local e um nome para salvar o Gr�fico.", vbInformation, "Aten��o"
    End If
End Sub

Private Sub cmdImprimir_Click()
    Grafico.DrawMode = gphPrint
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    Me.WindowState = vbMaximized
    Me.Grafico.Visible = False
    
    cboTlmk.Clear
    
    cboTlmk.AddItem ""
    For i = 1 To frmVend.grid1.Rows - 2
        cboTlmk.AddItem frmVend.grid1.TextMatrix(i, 1)
        cboTlmk.ItemData(cboTlmk.NewIndex) = frmVend.grid1.TextMatrix(i, 0)
    Next

End Sub

Sub Preenche_Grafico()
    Dim Contador As Integer
    Dim Conta As Byte
    Dim Realizado As Byte
    Dim Legenda As String
    
    If frmVend.grid1.Rows <= 1 Then
        Grafico.NumPoints = 2
    Else
        Grafico.NumPoints = frmVend.grid1.Rows - 2
    End If
    
    Grafico.AutoInc = 0
    Grafico.ThisPoint = 1
    Grafico.IndexStyle = gphStandard
    Grafico.ThisPoint = 1
    Grafico.DataReset = gphAllData
    Grafico.DataReset = gphLabelText
    
    Grafico.FontUse = gphLabels
    Grafico.FontFamily = gphSwiss
    Grafico.FontSize = 120

    Grafico.FontUse = gphLegend
    Grafico.FontFamily = gphSwiss
    Grafico.FontSize = 90
    
    If lstOpcoes = "Liga��es Ativas/Receptivas" Then
        If cboTlmk = "" Then
            Col1 = 7
            col2 = 0
        Else
            Col1 = 5
            col2 = 6
        End If
        TituloGrafico = "Ativas / Receptivas"
        If cboTlmk <> "" Then TituloGrafico = TituloGrafico & " - " & cboTlmk
        
        Legenda = "Ativas"
    ElseIf lstOpcoes = "Liga��es Ativas" Then
        Col1 = 5
        col2 = 0
        Legenda = "Ativas"
        TituloGrafico = "Ativas"
        If cboTlmk <> "" Then TituloGrafico = TituloGrafico & " - " & cboTlmk
    
    ElseIf lstOpcoes = "Liga��es Receptivas" Then
        Col1 = 6
        col2 = 0
        Legenda = "Receptivas"
        TituloGrafico = "Receptivas"
        If cboTlmk <> "" Then TituloGrafico = TituloGrafico & " - " & cboTlmk
    ElseIf lstOpcoes = "Liga��es Por Resultado" Then
        Col1 = 0
        col2 = 0
        Legenda = ""
        TituloGrafico = "Liga��es por Resultado"
        If cboTlmk <> "" Then TituloGrafico = TituloGrafico & " - " & cboTlmk
        Ligacoes_Resultado
        Grafico.DrawMode = gphDraw
        
        Exit Sub
    ElseIf lstOpcoes = "Tempo M�dio por Liga��o" Then
        Col1 = 13
        col2 = 0
        Legenda = ""
        TituloGrafico = "Tempo M�dio por Liga��o"
        If cboTlmk <> "" Then
           MsgBox "Voc� n�o pode escolher um Vendedor para esta op��o.", vbInformation, "Aten��o"
           cboTlmk = ""
        End If
    ElseIf lstOpcoes = "Crescimento Sobre a Meta" Then
        Col1 = 12
        col2 = 0
        Legenda = ""
        TituloGrafico = "Crescimento Sobre a Meta"
        If cboTlmk <> "" Then
           MsgBox "Voc� n�o pode escolher um Vendedor para esta op��o.", vbInformation, "Aten��o"
           cboTlmk = ""
        End If
    End If
    
    Grafico.GraphTitle = TituloGrafico
    Cota = 3
    Realizado = 4
    For i = 1 To frmVend.grid1.Rows - 2
        If cboTlmk <> "" Then
           If frmVend.grid1.TextMatrix(i, 1) = cboTlmk Then
              
              If col2 > 0 Then
                 Grafico.NumPoints = 4
              Else
                 Grafico.NumPoints = 3
              End If
              Grafico.ThisPoint = 1
              
              Grafico.GraphData = frmVend.grid1.TextMatrix(i, Cota)
              Grafico.LegendText = "Cota"
              Grafico.LabelText = frmVend.grid1.TextMatrix(i, Cota)
              
              Grafico.ThisPoint = Grafico.ThisPoint + 1
              Grafico.GraphData = frmVend.grid1.TextMatrix(i, Realizado)
              Grafico.LegendText = "Realizado"
              Grafico.LabelText = frmVend.grid1.TextMatrix(i, Realizado)
              
              Grafico.ThisPoint = Grafico.ThisPoint + 1
              Grafico.GraphData = frmVend.grid1.TextMatrix(i, Col1)
              Grafico.LegendText = Legenda
              Grafico.LabelText = frmVend.grid1.TextMatrix(i, Col1)
              
              If col2 > 0 Then
                 Grafico.ThisPoint = Grafico.ThisPoint + 1
                 Grafico.GraphData = frmVend.grid1.TextMatrix(i, col2)
                 Grafico.LegendText = "Receptivas"
                 Grafico.LabelText = frmVend.grid1.TextMatrix(i, col2)
              End If
                
              Exit For
           End If
        Else
              Grafico.ThisPoint = i
              
              If lstOpcoes = "Tempo M�dio por Liga��o" Then
                 Grafico.GraphData = Left(frmVend.grid1.TextMatrix(i, Col1), 2) * 60 + Right(frmVend.grid1.TextMatrix(i, Col1), 2)
                 Grafico.LabelText = frmVend.grid1.TextMatrix(i, Col1)
              Else
                 Grafico.GraphData = frmVend.grid1.TextMatrix(i, Col1)
                 Grafico.LabelText = frmVend.grid1.TextMatrix(i, Col1)
              End If
              If cboTlmk = "" Then
                 Grafico.LegendText = frmVend.grid1.TextMatrix(i, 1)
              Else
                 Grafico.LegendText = cboTlmk
              End If
        End If
    Next
    Grafico.DrawMode = gphDraw
End Sub

Private Sub Form_Resize()
    If Me.Width > 2500 Then
        Grafico.Width = Me.Width - (lstOpcoes.Width + lstOpcoes.Width / 8)
        Grafico.Height = Me.Height - (cmdVoltar.Height + pkGradient1.Height + stbBarra.Height * 3)
        Me.Refresh
    End If
End Sub

Private Sub lstOpcoes_Click()
    cmdAtualizarGrafico_Click
End Sub

Private Sub optCol2d_Click()
    Grafico.GraphType = 3
    Grafico.DrawMode = gphDraw
End Sub

Private Sub optCol3D_Click()
    Grafico.GraphType = 4
    Grafico.DrawMode = gphDraw
End Sub


Private Sub optPizza2d_Click()
    Grafico.GraphType = 1
    Grafico.DrawMode = gphDraw
End Sub

Private Sub OptPizza3d_Click()
    Grafico.GraphType = 2
    Grafico.DrawMode = gphDraw
End Sub

Sub Ligacoes_Resultado()
    Dim ss As Object
    Dim i As Integer
  
    vBanco.Parameters.Remove "dt"
    vBanco.Parameters.Add "dt", frmVend.txtData, 1
    vBanco.Parameters.Remove "dt1"
    vBanco.Parameters.Add "dt1", frmVend.txtData1, 1
    vBanco.Parameters.Remove "vend"
    vBanco.Parameters.Add "vend", frmGrafico.cboTlmk.ItemData(frmGrafico.cboTlmk.ListIndex), 1
    vBanco.Parameters.Remove "vErro"
    vBanco.Parameters.Add "vErro", 0, 2
    
    Criar_Cursor
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA610.PR_SELECT_RESULTADO(:vCursor, :Dt, :Dt1, :Vend, :vErro)")
   
    Set ss = vBanco.Parameters("vCursor").Value
    
    vBanco.Parameters.Remove "vCursor"
    
    Grafico.NumPoints = ss.RecordCount
    Grafico.GraphTitle = TituloGrafico
    
    For i = 1 To ss.RecordCount
        Grafico.ThisPoint = i
        Grafico.GraphData = ss.Fields(2)
        Grafico.LabelText = ss.Fields(2)
        Grafico.LegendText = ss.Fields(1)
        ss.MoveNext
    Next
    
End Sub
