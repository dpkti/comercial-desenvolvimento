VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.MDIForm mdiVDA610 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "VDA610 - Estatistica de Contatos"
   ClientHeight    =   8145
   ClientLeft      =   60
   ClientTop       =   390
   ClientWidth     =   11535
   Icon            =   "mdiVDA610.frx":0000
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11535
      _Version        =   65536
      _ExtentX        =   20346
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin Bot�o.cmd cmdContatosCli 
         Height          =   1005
         Left            =   2670
         TabIndex        =   9
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "C. Cliente"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":23D2
         PICN            =   "mdiVDA610.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   1005
         Left            =   4830
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":2537
         PICN            =   "mdiVDA610.frx":2553
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   5910
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":322D
         PICN            =   "mdiVDA610.frx":3249
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   4
         Top             =   1125
         Width           =   11760
         _ExtentX        =   20743
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdVendedor 
         Height          =   1005
         Left            =   1590
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Help"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Vendedor"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":3F23
         PICN            =   "mdiVDA610.frx":3F3F
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdResultado 
         Height          =   1005
         Left            =   3750
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Help"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Resultado"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":4C19
         PICN            =   "mdiVDA610.frx":4C35
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLigacoes 
         Height          =   1005
         Left            =   9255
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Help"
         Top             =   60
         Visible         =   0   'False
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Liga��es"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":590F
         PICN            =   "mdiVDA610.frx":592B
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdContatos 
         Height          =   1005
         Left            =   10335
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Estat�stica Relat�rio Contatos"
         Top             =   60
         Visible         =   0   'False
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Contatos"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA610.frx":6205
         PICN            =   "mdiVDA610.frx":6221
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiVDA610.frx":6A3E
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7815
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7197
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "25/11/15"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "13:55"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "mdiVDA610"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd2_Click()

End Sub

Private Sub cmd1_Click()

End Sub

Private Sub cmdContatos_Click()
    frmEstat.Show 1
End Sub

Private Sub cmdContatosCli_Click()
    frmContCli.Show 1
End Sub

Private Sub cmdLigacoes_Click()
    frmStatus.Show 1
End Sub

Private Sub cmdResultado_Click()
    frmContatos.Show 1
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub cmdVendedor_Click()
  frmVend.Show 1
End Sub

Private Sub MDIForm_Load()

    frmLogo.Show 1
    
    Dim ss As Object
    
    Me.Top = 0
    Me.Left = 0
    
    If App.PrevInstance Then
       MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
       End
    End If
    
    Call Get_CD(Mid(App.Path, 1, 1))

   
    If strTp_banco = "U" Then
      strTabela_Banco = "PRODUCAO."
    Else
      strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
    End If
        
    fl_banco = 0
    
    'path na rede
    Operacao = "C:\TESSW\OPERACAO.TXT"
    arq_output = "C:\TEMP.TXT"
    'strPath = "F:\oracle\dados\32bits\"
    strPath = "h:\oracle\dados\32Bits\"
    
    'Conexao oracle
    'Producao
    'vErro = vVB_Generica_001.ConectaOracle("producao", "VDA610", True, Me)
    'Desenv
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "VDA610", True, Me)
    
    Set vBanco = vVB_Generica_001.vBanco
    
    'carregar data de faturamento,uf origem, filial origem
'          SQL = "select to_char(a.cod_loja,'09') ||'-'||b.nome_fantasia deposito_default,"
'    SQL = SQL & " a.cod_filial      "
'    SQL = SQL & " from " & strTabela_Banco & "deposito a, loja b "
'    SQL = SQL & " where a.cod_loja=b.cod_loja"
    
    vBanco.Parameters.Remove "Tabela_banco"
    vBanco.Parameters.Add "Tabela_Banco", strTabela_Banco, 1
    vBanco.Parameters.Remove "Erro"
    vBanco.Parameters.Add "Erro", 0, 2
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA610.Pr_Select_Dt_Faturamento(:vCursor,:Tabela_Banco, :Erro)")
    
    If vErro <> "" Then End
    
    Set ss = vBanco.Parameters("vCursor").Value
     
    vBanco.Parameters.Remove "vCursor"
    
    If ss.EOF And ss.BOF Then
        MsgBox "Problemas na consulta de deposito default. Ligue para Depto Sistemas", vbInformation, "Aten��o"
        Exit Sub
    Else
        Deposito_Default = ss!Deposito_Default
        Filial = ss!cod_filial
    End If
                        
''    SQL = " Select dt_real"
''    SQL = SQL & " from datas"
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA610.Pr_Select_Datas(:vCursor)")
    
    If vErro <> "" Then End
    
    Set ss = vBanco.Parameters("vCursor").Value
    vBanco.Parameters.Remove "vCursor"
    
    If ss.EOF And ss.BOF Then
      MsgBox "Tabela de Datas sem registros, verifique", vbInformation, "Aten��o"
      Unload Me
    End If
      
    dt_real = ss!dt_real

    If vErro <> "" Then End
    
    vCD = vVB_Generica_001.vCD
    vTipoCD = vVB_Generica_001.vTipoCD
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
    
    
    'BUSCA DIAS
    vBanco.Parameters.Remove "Erro"
    vBanco.Parameters.Add "Erro", 0, 2
    Criar_Cursor
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA610.Pr_DIAS(:vCursor,:Erro)")
    
    If vErro <> "" Then
      lngTotal_Dias = 20
      lngTotal_Uteis = Mid(dt_real, 1, 2)
    End If
    
    Set ss = vBanco.Parameters("vCursor").Value
    vBanco.Parameters.Remove "vCursor"
    
    If ss.EOF And ss.BOF Then
       lngTotal_Dias = 20
       lngTotal_Uteis = Mid(dt_real, 1, 2)
    Else
      For i = 1 To ss.RecordCount
       If ss(0) = 1 Then
         lngTotal_Dias = ss(1)
       Else
         lngTotal_Uteis = ss(1)
       End If
       ss.MoveNext
      Next
    
    End If
      
      
    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1
    
    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, App.Title)
    
    If vObjOracle.EOF Then
    
        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End
    
    End If
    'Amelo - Melhoria Roteirizacao - TI-3848
    SetaParametroHabilitaGenio
    
    'CBacci - 30/08/2012 - SDS2589 - Seta a variavel vParamBuscaMeta
    SetaParametroInteligMercado
    AjustaTelaMeta
        
    Call DefinirTelaSobre
    
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
       Resume
    ElseIf Err.Number <> 0 Then
       Call Process_Line_Errors(SQL)
    End If
    
End Sub

Private Sub AjustaTelaMeta()
    If vParamBuscaMeta = "N" Then
        cmdContatosCli.Visible = False
        cmdResultado.Left = cmdResultado.Left - 1080
        cmdLigacoes.Left = cmdLigacoes.Left - 1080
        cmdContatos.Left = cmdContatos.Left - 1080
        cmdSobre.Left = cmdSobre.Left - 1080
        cmdSair.Left = cmdSair.Left - 1080
    End If
End Sub
