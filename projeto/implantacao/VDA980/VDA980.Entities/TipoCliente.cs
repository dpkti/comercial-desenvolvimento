﻿using System;

namespace VDA980.Entities
{
    public class TipoCliente
    {
        public string CodTipoCliente { get; set; }
        public string Descricao { get; set; }
        public string CodSegmento { get; set; }
        public string NomeExibicao
        {
            get { return String.Format("{0} - {1}", CodTipoCliente, Descricao); }
        }
    }
}
