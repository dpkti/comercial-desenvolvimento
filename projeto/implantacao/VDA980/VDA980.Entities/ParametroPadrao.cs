﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace VDA980.Entities
{
    public enum TipoParametro
    {
        TIPO_ANALISE_MARGEM,
        MARGEM_PEDIDO,
        MARGEM_ITEM,
        NENHUM
    }


    public class ParametroPadrao
    {
        [DisplayName("Cód. Loja")]
        public int CodigoLoja { get; set; }

        [DisplayName("Tipo Análise")]
        public string NomeParametro { get; set; }

        [DisplayName("Margem(%)")]
        public string ValorParametro { get; set; }

        [DisplayName("Data Vigência")]
        public DateTime DataVigencia { get; set; }

        [Browsable(false)]
        public String DataVigenciaStr
        {
            get
            {
                string retorno = string.Empty;

                if (DataVigencia.Year != 1)
                {
                    retorno = DataVigencia.ToString("dd/MM/yyyy");
                }

                return retorno;
            }

            set
            {
                DateTime data;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out data))
                {
                    DataVigencia = data;
                }
                else
                {
                    DataVigencia = DateTime.Now;
                }
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public ParametroPadrao ClonarTipado()
        {
            return ((ParametroPadrao)this.Clone());
        }

        public ParametroPadrao()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }
        }

    }
}
