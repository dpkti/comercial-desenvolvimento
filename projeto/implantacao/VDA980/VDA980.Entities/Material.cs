﻿using System;
using System.ComponentModel;

namespace VDA980.Entities
{
    public class Material : ModeloBusca
    {
        [DisplayName("Código DPK")]
        public Int64 CodigoDPK { get; set; }
      
        [DisplayName("Cód. Fábrica")]
        public string CodFabrica { get; set; }

        [DisplayName("Descrição")]
        public string DescItem { get; set; }

        public Material()
        {
        }

        public override string RetornarCodigoTexto()
        {
            return CodigoDPK.ToString();
        }

        public override void SetarParametrosBusca(string id, string descricao)
        {
            long codDpk;

            if (long.TryParse(id, out codDpk))
            {
                CodigoDPK = codDpk;
            }
            else
            {
                CodigoDPK = 0;
            }

            if (!String.IsNullOrEmpty(descricao) && descricao.Trim().Length > 0)
            {
                DescItem = descricao;
            }
            else
            {
                DescItem = null;
            }

            if (!String.IsNullOrEmpty(id) && id.Trim().Length > 0)
            {
                CodFabrica = id;
            }
            else
            {
                CodFabrica = null;
            }
        }
    }
}
