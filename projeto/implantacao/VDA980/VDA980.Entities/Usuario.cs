﻿
namespace VDA980.Entities
{
    public class Usuario
    {
        public string Nome { get; set; }
        public string Senha { get; set; }
        public string Login { get; set; }
        public bool PermissaoAcesso { get; set; }
        public long CodUsuario { get; set; }
    }
}
