﻿using System;
using System.ComponentModel;

namespace VDA980.Entities
{
    public class Fornecedor : ModeloBusca
    {
        [DisplayName("Código")]
        public Int64 CodFornecedor { get; set; }
        [DisplayName("CNPJ")]
        public Int64 CNPJFornecedor { get; set; }
        [DisplayName("Nome")]
        public string NomeFornecedor { get; set; }

        public override string RetornarCodigoTexto()
        {
            return CNPJFornecedor.ToString();
        }

        public override void SetarParametrosBusca(string id, string descricao)
        {
            Int64 CNPJForn;

            if (long.TryParse(id, out CNPJForn))
            {
                CNPJFornecedor = CNPJForn;
            }
            else
            {
                CNPJFornecedor = 0;
            }

            if (!String.IsNullOrEmpty(descricao) && descricao.Trim().Length > 0)
            {
                NomeFornecedor = descricao;
            }
            else
            {
                NomeFornecedor = null;
            }
        }
    }
}
