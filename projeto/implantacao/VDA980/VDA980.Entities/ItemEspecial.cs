﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace VDA980.Entities
{
    public class ItemEspecial : ICloneable
    {
        [DisplayName("Cód. Loja")]
        public int CodigoLoja { get; set; }

        [DisplayName("Código DPK")]
        public Int64 CodigoDPK { get; set; }

        [DisplayName("Cód. Fabrica")]
        public string CodigoFabrica { get; set; }

        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        [DisplayName("Margem(%)")]
        public double Margem { get; set; }

        private DateTime dataVigencia;

        [DisplayName("Data Vigência")]
        public DateTime DataVigencia
        {
            get
            {
                return dataVigencia;
            }
            set
            {
                dataVigencia = value;
            }
        }

        [Browsable(false)]
        public String DataVigenciaStr
        {
            get
            {
                string retorno = string.Empty;

                if (DataVigencia.Year != 1)
                {
                    retorno = DataVigencia.ToString("dd/MM/yyyy");
                }

                return retorno;
            }

            set
            {
                DateTime data;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out data))
                {
                    DataVigencia = data;
                }
                else
                {
                    DataVigencia = DateTime.MinValue;
                }
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public ItemEspecial ClonarTipado()
        {
            return ((ItemEspecial)this.Clone());
        }

        public ItemEspecial()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }
        }
    }
}
