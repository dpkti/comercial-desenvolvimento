﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace VDA980.Entities
{
    public class TipologiaFornecedor
    {
        [DisplayName("Código Loja")]
        public int CodigoLoja { get; set; }

        [DisplayName("Tipo Cliente")]
        public string CodTipoCliente { get; set; }

        [DisplayName("CNPJ Forn.")]
        public Int64 CGCFornecedor { get; set; }

        [DisplayName("Fornecedor")]
        public string NomeFornecedor { get; set; }

        [DisplayName("Margem(%)")]
        public double Margem { get; set; }

        private DateTime dataVigencia;

        [DisplayName("Data Vigência")]
        public DateTime DataVigencia
        {
            get
            {
                return dataVigencia;
            }
            set
            {
                dataVigencia = value;
            }
        }

        [Browsable(false)]
        public String DataVigenciaStr
        {
            get
            {
                string retorno = string.Empty;

                if (DataVigencia.Year != 1)
                {
                    retorno = DataVigencia.ToString("dd/MM/yyyy");
                }

                return retorno;
            }

            set
            {
                DateTime data;
                if (DateTime.TryParseExact(value, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out data))
                {
                    DataVigencia = data;
                }
                else
                {
                    DataVigencia = DateTime.MinValue;
                }

            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public TipologiaFornecedor ClonarTipado()
        {
            return ((TipologiaFornecedor)this.Clone());
        }

        public TipologiaFornecedor()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }

            this.CodTipoCliente = "0";
        }
    }
}
