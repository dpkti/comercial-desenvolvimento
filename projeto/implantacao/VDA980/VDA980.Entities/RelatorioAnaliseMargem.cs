﻿using System;

namespace VDA980.Entities
{
    public class RelatorioAnaliseMargem
    {
        public int CodigoLoja { get; set; }
        public DateTime DataPedido { get; set; }
        public long NumeroPedido { get; set; }
        public int SequenciaPedido { get; set; }
        public int NumeroItemPedido { get; set; }
        public long CodigoDPK { get; set; }
        public int QuantidadeItem { get; set; }
        public string DescricaoItem { get; set; }
        public decimal PrecoVenda { get; set; }
        public decimal ValorIpi { get; set; }
        public decimal ValorIcmsST { get; set; }
        public decimal ValorIcmsNormal { get; set; }
        public decimal ValorPis { get; set; }
        public decimal ValorCofins { get; set; }
        public decimal ValorCustoMedio { get; set; }
        public decimal ValorLucroBruto { get; set; }
        public decimal PercentualMargemBruta { get; set; }
        public string DescricaoAnalise { get; set; }
        public decimal PercentualMargemPolitica { get; set; }
        public string FlagAnalise { get; set; }
        public DateTime? DataLiberacao { get; set; }
        public long CodigoUsuario { get; set; }
        public string NomeUsuario { get; set; }
    }

    public class RelatorioAnaliseMargemFiltro
    {
        public int CodigoLoja { get; set; }
        public int CodigoLojaDestino { get; set; }
        public DateTime? DataPedidoInicio { get; set; }
        public DateTime? DataPedidoFim { get; set; }
        public long NumeroPedido { get; set; }
        public int CodigoTipoAnalise { get; set; }
        
        public bool Bloqueado { get; set; }
        public bool Liberado { get; set; }

        public string FlagAnalise
        { 
            get
            {
                if (Bloqueado && !Liberado)
                {
                    return "S";
                }
                else if (!Bloqueado && Liberado)
                {
                    return "N";
                }
                else
                {
                    return null;
                }

            }
        }

        public RelatorioAnaliseMargemFiltro()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
                this.CodigoLojaDestino = Session.Loja.CodigoLoja;
                this.Bloqueado = true;
                this.Liberado = true;
            }
        }
    }
}
