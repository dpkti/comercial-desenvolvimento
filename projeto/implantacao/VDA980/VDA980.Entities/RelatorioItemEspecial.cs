﻿using System;

namespace VDA980.Entities
{
    public class RelatorioItemEspecial : ItemEspecial
    {
        public DateTime DataInclusao { get; set; }
        public string UsuarioInclusao { get; set; }
        public DateTime ? DataAlteracao { get; set; }
        public string UsuarioAlteracao { get; set; }
    }

    public class RelatorioItemEspecialFiltro
    {
        public int CodigoLoja { get; set; }
        public Int64 CodigoDPK { get; set; }
        public DateTime ? DataVigenciaIni { get; set; }
        public DateTime ? DataVigenciaFim { get; set; }

        public RelatorioItemEspecialFiltro()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }
        }
    }
}
