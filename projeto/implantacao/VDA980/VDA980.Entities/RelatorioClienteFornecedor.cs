﻿using System;

namespace VDA980.Entities
{
    public class RelatorioClienteFornecedor : ClienteFornecedor
    {
        public DateTime DataInclusao { get; set; }
        public string UsuarioInclusao { get; set; }
        public DateTime? DataAlteracao { get; set; }
        public string UsuarioAlteracao { get; set; }
        public long CodigoFornecedor { get; set; }
        public long CodigoCliente { get; set; }
    }

    public class RelatorioClienteFornecedorFiltro
    {
        public Int64 CGCFornecedor { get; set; }
        public Int64 CNPJCliente { get; set; }
        public DateTime? DataVigenciaIni { get; set; }
        public DateTime? DataVigenciaFim { get; set; }
    }
}
