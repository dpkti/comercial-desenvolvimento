﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace VDA980.Entities
{
    public class Relatorio
    {
        public object Source { get; set; }
        public string NomeDataSet { get; set; }
        public string NomeRelatorio { get; set; }
        public string ReportEmbeddedResource { get; set; }
        public List<KeyValuePair<string, object>> Parametros { get; set; }
    }
}
