﻿using System;
using System.ComponentModel;

namespace VDA980.Entities
{
    public class Cliente : ModeloBusca
    {
        [DisplayName("Código")]
        public Int64 CodCliente { get; set; }

        [DisplayName("CPF/CNPJ")]
        public Int64 CNPJCliente { get; set; }

        [DisplayName("Nome")]
        public string NomeCliente { get; set; }

        public override string RetornarCodigoTexto()
        {
            return this.CNPJCliente.ToString();
        }

        public override void SetarParametrosBusca(string id, string descricao)
        {
            Int64 CNPJClie;

            if (long.TryParse(id, out CNPJClie))
            {
                CNPJCliente = CNPJClie;
            }
            else
            {
                CNPJCliente = 0;
            }

            if (!String.IsNullOrEmpty(descricao) && descricao.Trim().Length > 0)
            {
                NomeCliente = descricao;
            }
            else
            {
                NomeCliente = null;
            }
        }

    }
}
