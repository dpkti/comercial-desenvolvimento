﻿using System;

namespace VDA980.Entities
{
    public class TipoAnalise
    {
        public int CodigoTipoAnalise { get; set; }
        public string Descricao { get; set; }
        public string NomeExibicao
        {
            get { return String.Format("{0} - {1}", CodigoTipoAnalise, Descricao); }
        }
    }
}
