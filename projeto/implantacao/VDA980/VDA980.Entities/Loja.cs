﻿using System;

namespace VDA980.Entities
{
    public class Loja
    {
        public int CodigoLoja { get; set; }
        public String CNPJ { get; set; }
        public String NomeFantasia { get; set; }
        public String TipoBanco { get; set; }
        public String NomeExibicao
        {
            get
            {
                return String.Format("{0} - {1}", CodigoLoja.ToString(), NomeFantasia);
            }
        }
    }
}
