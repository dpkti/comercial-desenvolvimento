﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VDA980.Entities
{
    public abstract class ModeloBusca
    {
        public abstract String RetornarCodigoTexto();

        public abstract void SetarParametrosBusca(string id, string descricao);
    }
}
