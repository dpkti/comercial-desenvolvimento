﻿using System;

namespace VDA980.Entities
{
    public class RelatorioTipologiaFornecedor : TipologiaFornecedor
    {
        public long CodigoFornecedor { get; set; }
        public DateTime DataInclusao { get; set; }
        public string UsuarioInclusao { get; set; }
        public DateTime ? DataAlteracao { get; set; }
        public string UsuarioAlteracao { get; set; }
    }

    public class RelatorioTipologiaFornecedorFiltro
    {
        public int CodigoLoja { get; set; }
        public string CodTipoCliente { get; set; }
        public Int64 CGCFornecedor { get; set; }
        public DateTime ? DataVigenciaIni { get; set; }
        public DateTime ? DataVigenciaFim { get; set; }

        public RelatorioTipologiaFornecedorFiltro()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }

            this.CodTipoCliente = "0";
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public RelatorioTipologiaFornecedorFiltro ClonarTipado()
        {
            return ((RelatorioTipologiaFornecedorFiltro)this.Clone());
        }
    }
}
