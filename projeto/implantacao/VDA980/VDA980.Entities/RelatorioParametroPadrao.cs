﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace VDA980.Entities
{
    public class RelatorioParametroPadrao : ParametroPadrao
    {
        public string ValorParametro
        {
            set
            {
                Decimal margem;
                if (Decimal.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out margem))
                {
                    MargemDecimal = margem;
                }
            }
        }

        public Decimal MargemDecimal { get; set; }

        public DateTime ? DataAtualizacao { get; set; }
        public string Usuario { get; set; }
    }

    public class RelatorioParametroPadraoFiltro
    {
        public int CodigoLoja { get; set; }
        public string NomeParametro { get; set; }
        public bool ParametroPedido { get; set; }
        public bool ParametroItem { get; set; }
        public DateTime ? DataVigenciaIni { get; set; }
        public DateTime ? DataVigenciaFim { get; set; }

        public RelatorioParametroPadraoFiltro()
        {
            if (Session.Loja != null)
            {
                this.CodigoLoja = Session.Loja.CodigoLoja;
            }

            this.ParametroItem = true;
            this.ParametroPedido = true;
        }
    }
}
