﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class LojaBLL:BaseBLL<LojaBLL>
    {
        private List<Loja> listaLoja = null;

        public List<Loja> ListarTodos()
        {
            try
            {
                //cache da pesquisa de lojas
                if (listaLoja == null)
                {
                    listaLoja = LojaDALC.Instance().SelecionarTodos();

                    if (Session.Loja != null && Session.Loja.CodigoLoja != 1)
                    {
                        listaLoja = listaLoja.Where(i => i.CodigoLoja == Session.Loja.CodigoLoja).ToList();
                    }
                }

                return listaLoja;
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar lojas.", ex);
                return listaLoja;
            }
        }

        public Loja ValidarLoja()
        {
            try
            {
                Loja oLoja = new Loja();

                string pathCDTXT = @"H:\ORACLE\DADOS\32BITS\CD.TXT";
                if (ConfigurationManager.AppSettings.AllKeys.Contains("pathCD"))
                    pathCDTXT = ConfigurationManager.AppSettings["pathCD"].ToString();

                string param = String.Empty;

                using (StreamReader reader = File.OpenText(pathCDTXT))
                {
                    param = reader.ReadToEnd();
                    reader.Close();
                }

                if (String.IsNullOrEmpty(param))
                {
                    return null;
                }

                oLoja.CodigoLoja = int.Parse(param.Substring(0, 2)); 
                oLoja.TipoBanco = param.Substring(2, 1);

                return oLoja;
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao ler arquivo CD.txt.", ex);
                return null;
            }
        }
    }
}
