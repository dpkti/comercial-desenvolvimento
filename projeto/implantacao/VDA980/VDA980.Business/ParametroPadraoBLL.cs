﻿using System;
using System.Collections.Generic;
using System.Linq;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class ParametroPadraoBLL : BaseBLL<ParametroPadraoBLL>
    {
        public ParametroPadrao ObterParametroVigente(Loja loja, TipoParametro tipoParametro)
        {
            ParametroPadrao parametroRetorno = null;
            int codigoLoja;
            string nomeParametro;

            try
            {
                if (loja != null)
                {
                    nomeParametro = tipoParametro.ToString();
                    codigoLoja = loja.CodigoLoja;

                    var listaParametro = ParametroPadraoDALC.Instance().SelectByFilter(new ParametroPadrao() { CodigoLoja = codigoLoja, NomeParametro = nomeParametro });

                    if (listaParametro != null && listaParametro.Count > 0)
                    {
                        parametroRetorno = listaParametro.FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao obter parâmentro de margem.", ex);
            }

            return parametroRetorno;
        }

        public List<ParametroPadrao> ListarTodos(ParametroPadrao parametro)
        {
            List<ParametroPadrao> retorno = null;

            try
            {
                retorno = ParametroPadraoDALC.Instance().SelectByFilter(new ParametroPadrao() { CodigoLoja = parametro.CodigoLoja, NomeParametro = parametro.NomeParametro });
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar os parâmentros de margem.", ex);
            }

            return retorno;
        }

        public List<ParametroPadrao> ListarTodos(Loja loja, TipoParametro tipoParametro)
        {
            List<ParametroPadrao> parametroRetorno = null;
            int codigoLoja;
            string nomeParametro;

            try
            {
                if (loja != null)
                {
                    nomeParametro = tipoParametro.ToString();
                    codigoLoja = loja.CodigoLoja;

                    parametroRetorno = ParametroPadraoDALC.Instance().SelectByFilter(new ParametroPadrao() { CodigoLoja = codigoLoja, NomeParametro = nomeParametro });

                    foreach (var item in parametroRetorno)
                    {
                        item.ValorParametro = item.ValorParametro.Replace(".", ",");
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar os parâmentros de margem.", ex);
            }

            return parametroRetorno;
        }

        public List<TipoAnalise> ListarTipoAnalise()
        {
            List<TipoAnalise> listaRetorno = null;

            try
            {
                listaRetorno = ParametroPadraoDALC.Instance().SelectTipoAnalise();

                TipoAnalise tipoAnaliseTodos = new TipoAnalise();
                tipoAnaliseTodos.CodigoTipoAnalise = 0;
                tipoAnaliseTodos.Descricao = "Todos";

                listaRetorno.Insert(0, tipoAnaliseTodos);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar tipo de análises.", ex);
            }

            return listaRetorno;
        }

        public void Gravar(ParametroPadrao tipoAnalise)
        {
            try
            {
                if (!String.IsNullOrEmpty(tipoAnalise.ValorParametro))
                {
                    tipoAnalise.ValorParametro = tipoAnalise.ValorParametro.Replace(",", ".");
                }

                ParametroPadraoDALC.Instance().Merge(tipoAnalise, Session.Usuario.Login);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao salvar parâmetro padrão de margem.", ex);
            }
        }

        public static string NomeTipoAnalise(ParametroPadrao parametro)
        {
            string retorno = String.Empty;

            if (parametro != null)
            {
                if (parametro.ValorParametro.ToUpper().Trim() == "P")
                {
                    retorno = "pedido";
                }

                if (parametro.ValorParametro.ToUpper().Trim() == "I")
                {
                    retorno = "item";
                }
            }

            return retorno;
        }

        public List<RelatorioParametroPadrao> SelectByFilterRelatorio(RelatorioParametroPadraoFiltro filtro)
        {
            try
            {
                if (filtro.ParametroItem && filtro.ParametroPedido)
                {
                    filtro.NomeParametro = null;
                }
                else if (filtro.ParametroPedido)
                {
                    filtro.NomeParametro = TipoParametro.MARGEM_PEDIDO.ToString();
                }
                else if (filtro.ParametroItem)
                {
                    filtro.NomeParametro = TipoParametro.MARGEM_ITEM.ToString();
                }

                return ParametroPadraoDALC.Instance().SelectByFilterRelatorio(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gerar relatório parâmetro padrão.", ex);
                return null;
            }
        }

        public RelatorioParametroPadrao SelectByFilterMargemAtual(RelatorioParametroPadraoFiltro filtro)
        {
            try
            {
                return ParametroPadraoDALC.Instance().SelectByFilterMargemAtual(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gerar relatório parâmetro padrão atual.", ex);
                return null;
            }
        }

        public void Excluir(ParametroPadrao item)
        {
            try
            {
                ParametroPadraoDALC.Instance().Delete(item);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao excluir margens de item especial", ex);
            }
        }
    }
}
