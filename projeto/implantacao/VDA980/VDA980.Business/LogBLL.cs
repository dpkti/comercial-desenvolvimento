﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace VDA980.Business
{
    public sealed class LogBLL:BaseBLL<LogBLL>
    {
        #region Constantes

        /// <summary>
        /// constante que indica que será feito um log para os jobs
        /// </summary>
        public const string INFO_JOBS = "InfoJob";

        /// <summary>
        /// constante que indica que será feito um log para Debug
        /// </summary>
        public const string DEBUG = "Debug";

        /// <summary>
        /// constante que indica que será feito um log para Warning
        /// </summary>
        public const string WARNING = "Warning";

        /// <summary>
        /// constante que indica que será feito um log para Erro
        /// </summary>
        public const string ERROR = "Error";

        #endregion

        #region Construtor

        /// <summary>
        /// Construtor privado pois todos os metodos serão static
        /// </summary>
        private LogBLL() { }

        #endregion

        #region Propriedades

        /// <summary>
        /// constante que indica o source do event viewer
        /// </summary>
        public static string EventViewer { get { return RetornaEventViewerSource(); } }

        /// <summary>
        /// Verifica se o Log de InfoJob está abilitado
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsLogInfoJobEnabled()
        {
            bool isLogInfoJobEnabled =
                Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsLogInfoJobEnabled"));

            return isLogInfoJobEnabled;
        }

        /// <summary>
        /// Verifica se o Log de Info está abilitado
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsLogInfoEnabled()
        {
            bool isLogInfoEnabled =
                Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsLogInfoEnabled"));

            return isLogInfoEnabled;
        }

        /// <summary>
        /// Verifica se o Log de warning está abilitado
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsLogWarningEnabled()
        {
            bool isLogWarningEnabled =
                Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsLogWarningEnabled"));

            return isLogWarningEnabled;
        }

        /// <summary>
        /// Verifica se o Log de erro está abilitado
        /// </summary>
        /// <returns>bool</returns>
        public static bool IsLogErrorEnabled()
        {
            bool isLogErrorEnabled =
                Convert.ToBoolean(ConfigurationManager.AppSettings.Get("IsLogErrorEnabled"));
            return isLogErrorEnabled;
        }

        #endregion

        #region Métodos privados

        /// <summary>
        /// Retorna o nome do método que chamou o Log
        /// </summary>
        /// <returns></returns>
        public static string MetodoOrigem()
        {
            StackTrace oStack = new StackTrace(false);
            StackFrame frame;
            int i = 0;

            do
            {
                frame = oStack.GetFrame(i);
                i++;
            }
            while (frame.GetMethod().ReflectedType.FullName.Equals("VDA980.Business.LogBLL"));

            return frame.GetMethod().ReflectedType.FullName + "." + frame.GetMethod().Name;
        }

        private static string RetornaEventViewerSource()
        {
            return ConfigurationManager.AppSettings.Get("NomeAplicacaoEventViewer");
        }

        /// <summary>
        /// Loga as informações no Event Viewer quando tivermos erro no Log
        /// </summary>
        /// <param name="mensagem">Mensagem de erro</param>
        private static void InserirLogEventvwr(string mensagem)
        {
            EventLog eventLog = new EventLog();
            eventLog.Source = EventViewer;
            eventLog.WriteEntry(mensagem, System.Diagnostics.EventLogEntryType.Error);
        }

        #endregion

        #region Log de Informações

        /// <summary>
        /// Logar entrada de método.
        /// </summary>
        /// <returns>Data entrada do método.</returns>
        public static DateTime LogInicioMetodo()
        {
            return LogInfo(null, DateTime.MinValue);
        }

        public static void LogInfo(string message)
        {
            LogInfo(message, DateTime.MinValue);
        }

        private static DateTime LogInfo(string message, DateTime dataInicio)
        {
            DateTime dataAtual = DateTime.MinValue;

            if (LogBLL.IsLogInfoEnabled())
            {
                if (string.IsNullOrEmpty(message))
                {
                    dataAtual = DateTime.Now;

                    // Mensagem para logar entrada de método.
                    if (dataInicio.Equals(DateTime.MinValue))
                    {
                        message = "[INICIO]";
                    }
                    // Mensagem para logar saída de método.
                    else
                    {
                        message = "[FIM - " + dataAtual.Subtract(dataInicio).TotalMilliseconds + "ms]";
                    }
                }

                message = MetodoOrigem() + ": " + message;

                LogInfoEnterprise(message);
            }

            return dataAtual;
        }

        /// <summary>
        /// Loga saída de método.
        /// </summary>
        /// <param name="dataInicio">Data de entrada do método.</param>
        public static void LogFinalMetodo(DateTime dataInicio)
        {
            LogInfo(null, dataInicio);
        }

        #endregion

        #region Log de Erros

        /// <summary>
        /// Logar mensagem de erro.
        /// </summary>
        /// <param name="message">Mensagem.</param>
        public static void LogErro(string message)
        {
            LogErro(message, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">mensagem de erro</param>
        /// <param name="ex">Exception</param>
        public static void LogErro(string message, Exception ex)
        {
            if (LogBLL.IsLogErrorEnabled())
            {
                message = MetodoOrigem() + ": " + message;

                if (ex == null)
                    LogErrorEnterprise(message);
                else
                    LogErrorEnterprise(message + ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        /// <summary>
        /// Logar mensagem de erro.
        /// </summary>
        /// <param name="ex">Exceção.</param>
        public static void LogErro(Exception ex)
        {
            LogErro(null, ex);
        }

        #endregion

        #region Métodos Enterprise Library

        private static LogCategoryAttribute GetLogCategory()
        {
            StackTrace oStack = new StackTrace(false);

            IList<StackFrame> frames = oStack.GetFrames().ToList();

            foreach (var frame in frames)
            {
                MethodBase methodBase = frame.GetMethod();
                object atribute = null;

                if (methodBase.ReflectedType != null)
                    atribute = (frame.GetMethod()
                        .ReflectedType.GetCustomAttributes(typeof(LogCategoryAttribute), true).FirstOrDefault());

                if (atribute != null)
                    return ((LogCategoryAttribute)atribute);
            }

            return null;
        }

        /// <summary>
        /// Loga informações dos jobs
        /// </summary>
        /// <param name="message">Mensagem que será logada</param>
        public static void LogInfoJob(String message)
        {
            try
            {
                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(message, INFO_JOBS);
            }
            catch (Exception ex)
            {
                InserirLogEventvwr("Ocorreu o seguinte erro ao executar o método LogInfoJob(String message): "
                    + ex.Message);
            }
        }

        /// <summary>
        /// Loga informações para debug
        /// </summary>
        /// <param name="message">Mensagem que será logada</param>
        public static void LogInfoEnterprise(String message)
        {
            LogCategoryAttribute categoryAttribute = GetLogCategory();

            string logCategory;

            if (categoryAttribute == null)
            {
                logCategory = DEBUG;
            }
            else
            {
                if (!string.IsNullOrEmpty(categoryAttribute.DebugCategoryName))
                    logCategory = categoryAttribute.DebugCategoryName;
                else
                    logCategory = DEBUG;
            }

            try
            {
                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(message, logCategory);
            }
            catch (Exception ex)
            {
                InserirLogEventvwr("Ocorreu o seguinte erro ao executar o método LogInfo(String message): "
                    + ex.Message);
            }
        }

        /// <summary>
        /// Loga informações de alerta
        /// </summary>
        /// <param name="message">Mensagem que será logada</param>
        public static void LogWarning(String message)
        {
            LogCategoryAttribute categoryAttribute = GetLogCategory();

            string logCategory;

            if (categoryAttribute == null)
            {
                logCategory = WARNING;
            }
            else
            {
                if (!string.IsNullOrEmpty(categoryAttribute.WarningCategoryName))
                    logCategory = categoryAttribute.WarningCategoryName;
                else
                    logCategory = WARNING;
            }

            try
            {
                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(message, logCategory);
            }
            catch (Exception ex)
            {
                InserirLogEventvwr("Ocorreu o seguinte erro ao executar o método LogWarning(String message): "
                    + ex.Message);
            }
        }

        /// <summary>
        /// Loga informações de Erro
        /// </summary>
        /// <param name="message">Mensagem que será logada</param>
        public static void LogErrorEnterprise(String message)
        {
            try
            {
                LogCategoryAttribute categoryAttribute = GetLogCategory();

                string logCategory;

                if (categoryAttribute == null)
                {
                    logCategory = ERROR;
                }
                else
                {
                    if (!string.IsNullOrEmpty(categoryAttribute.ErrorCategoryName))
                        logCategory = categoryAttribute.ErrorCategoryName;
                    else
                        logCategory = ERROR;
                }

                Microsoft.Practices.EnterpriseLibrary.Logging.Logger.Write(message, logCategory);
            }
            catch (Exception ex)
            {
                InserirLogEventvwr("Ocorreu o seguinte erro ao executar o método LogError(String message): "
                    + ex.Message);
            }
        }

        #endregion
    }

    /// <summary>
    /// Indentifica que uma classe cria suas próprias categorias de Log.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class LogCategoryAttribute : Attribute
    {
        /// <summary>
        /// Define uma categoria para logs de erros criados pela classe.
        /// </summary>
        public string ErrorCategoryName { get; set; }

        /// <summary>
        /// Define uma categoria para logs de debug criados pela classe.
        /// </summary>
        public string DebugCategoryName { get; set; }

        /// <summary>
        /// Define uma categoria para logs de avisos criados pela classe.
        /// </summary>
        public string WarningCategoryName { get; set; }

        /// <summary>
        /// Cria uma nova instância de 'LogCategoryAttribute'.
        /// </summary>
        public LogCategoryAttribute() { }
    }
}