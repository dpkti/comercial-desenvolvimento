﻿using System;
using System.Collections.Generic;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class ClienteFornecedorBLL : BaseBLL<ClienteFornecedorBLL>
    {
        public List<ClienteFornecedor> ListarTodos(ClienteFornecedor filtro)
        {
            List<ClienteFornecedor> listaRetorno = null;

            try
            {
                listaRetorno = ClienteFornecedorDALC.Instance().SelectByFilter(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar margens de cliente fornecedor.", ex);
            }

            return listaRetorno;
        }

        public void Gravar(ClienteFornecedor item)
        {
            try
            {
                ClienteFornecedorDALC.Instance().Merge(item, Session.Usuario.Login);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gravar margem de cliente fornecedor.", ex);
            }
        }

        public void Excluir(ClienteFornecedor item)
        {
            try
            {
                ClienteFornecedorDALC.Instance().Delete(item);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao excluir margem de cliente fornecedor.", ex);
            }
        }

        public List<RelatorioClienteFornecedor> SelectByFilterRelatorio(RelatorioClienteFornecedorFiltro filtro)
        {
            try
            {
                return ClienteFornecedorDALC.Instance().SelectByFilterRelatorio(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gerar relatório de margem cliente fornecedor.", ex);
                return null;
            }
        }

        public List<Cliente> SelecionarEntidadeBusca(Cliente filtro)
        {
            try
            {
                return ClienteFornecedorDALC.Instance().SelectCliente(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao buscar cliente.", ex);
                return null;
            }
        }

        public String RetornarCodigoLabel()
        {
            return "CPF/CNPJ:";
        }
    }
}
