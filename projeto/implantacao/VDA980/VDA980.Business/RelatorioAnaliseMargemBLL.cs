﻿using System;
using System.Collections.Generic;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class RelatorioAnaliseMargemBLL:BaseBLL<RelatorioAnaliseMargemBLL>
    {
        public List<RelatorioAnaliseMargem> ListarTodos(RelatorioAnaliseMargemFiltro filtro)
        {
            List<RelatorioAnaliseMargem> listaRetorno = null;

            try
            {
                listaRetorno = RelatorioAnaliseMargemDALC.Instance().SelectByFilter(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gerar relatório de análise de margem.", ex);
            }

            return listaRetorno;
        }
    }
}
