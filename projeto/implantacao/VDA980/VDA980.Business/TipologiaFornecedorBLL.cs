﻿using System;
using System.Collections.Generic;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class TipologiaFornecedorBLL : BaseBLL<TipologiaFornecedorBLL>
    {
        public List<TipologiaFornecedor> ListarTodos(TipologiaFornecedor filtro)
        {
            List<TipologiaFornecedor> listaRetorno = null;

            try
            {
                listaRetorno = TipologiaFornecedorDALC.Instance().SelectByFilter(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar margens de tipologia fornecedor.", ex);
            }

            return listaRetorno;
        }

        public List<TipoCliente> ListarTipoCliente()
        {
            List<TipoCliente> listaRetorno = null;

            try
            {
                listaRetorno = TipologiaFornecedorDALC.Instance().SelectTipoCliente();
                TipoCliente semTipoCliente = new TipoCliente();
                semTipoCliente.CodTipoCliente = "0";
                semTipoCliente.Descricao = "Sem Tipo";
                listaRetorno.Insert(0, semTipoCliente);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar tipo cliente.", ex);
            }

            return listaRetorno;
        }

        public void Gravar(TipologiaFornecedor item)
        {
            try
            {
                TipologiaFornecedorDALC.Instance().Merge(item, Session.Usuario.Login);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gravar tipologia fornecedor.", ex);
            }
        }

        public void Excluir(TipologiaFornecedor item)
        {
            try
            {
                TipologiaFornecedorDALC.Instance().Delete(item);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao excluir margem de tipologia fornecedor.", ex);
            }
        }

        public List<RelatorioTipologiaFornecedor> SelectByFilterRelatorio(RelatorioTipologiaFornecedorFiltro filtro)
        {
            RelatorioTipologiaFornecedorFiltro filtroRelatorio = new RelatorioTipologiaFornecedorFiltro();
            try
            {
                if (filtro.CodTipoCliente.Equals("1"))
                {
                    filtroRelatorio = filtro.ClonarTipado();
                    filtroRelatorio.CodTipoCliente = null;
                }
                else
                {
                    filtroRelatorio = filtro.ClonarTipado();
                }
                return TipologiaFornecedorDALC.Instance().SelectByFilterRelatorio(filtroRelatorio);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gerar relatório de tipologia fornecedor.", ex);
                return null;
            }
        }

        public List<Fornecedor> SelecionarEntidadeBusca(Fornecedor filtro)
        {
            try
            {
                return TipologiaFornecedorDALC.Instance().SelectFornecedor(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao buscar fornecedor.", ex);
                return null;
            }
        }

        public String RetornarCodigoLabel()
        {
            return "CNPJ:";
        }
    }
}
