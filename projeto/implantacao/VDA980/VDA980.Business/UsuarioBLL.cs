﻿using System;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class UsuarioBLL : BaseBLL<UsuarioBLL>
    {
        public Usuario ValidarUsuario(Usuario usuario)
        {
            try
            {
                Usuario usuarioValidado = null;
                bool permissaoAplicacao = false;

                usuarioValidado = UsuarioDALC.Instance().SelecionarUsuario(usuario.Login, usuario.Senha);

                if (usuarioValidado != null)
                {
                    permissaoAplicacao = UsuarioDALC.Instance().ValidarAcessoAplicacao(usuarioValidado.CodUsuario, Session.Loja);
                    usuarioValidado.PermissaoAcesso = permissaoAplicacao;
                }
                else
                {
                    usuarioValidado = new Usuario();
                    usuarioValidado.Login = usuario.Login;
                    usuarioValidado.Senha = usuario.Senha;
                    usuarioValidado.PermissaoAcesso = permissaoAplicacao;
                }

                return usuarioValidado;
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao validar usuário.", ex);
                return null;
            }
        }
    }
}
