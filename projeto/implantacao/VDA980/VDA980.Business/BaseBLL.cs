﻿
using System;
using System.Reflection;
namespace VDA980.Business
{
    public class BaseBLL<T> where T : class
    {
        private static T instance;
        private static object initial = new object();

        public static T Instance()
        {
            // Se a instância for nula, cria uma nova instância
            if (instance == null)
            {
                criarInstancia();
            }

            return instance;
        }

        private static void criarInstancia()
        {
            lock (initial)
            {
                // Se ainda não há uma instância
                if (instance == null)
                {
                    //Obtém o tipo da classe
                    Type t = typeof(T);

                    // Verifica se a classe possui mais de um construtor
                    ConstructorInfo[] construtor = t.GetConstructors();
                    if (construtor.Length > 1)
                    {
                        throw new InvalidOperationException(String.Format("{0} tem mais de um construtor, o que impossibilita o uso do Singleton!", t.Name));
                    }

                    // Criando a instancia chamando o construtor padrão
                    instance = (T)Activator.CreateInstance(t, true);
                }
            }
        }
    }
}
