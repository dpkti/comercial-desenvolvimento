﻿using System;
using System.Collections.Generic;
using VDA980.DALC;
using VDA980.Entities;

namespace VDA980.Business
{
    public class ItemEspecialBLL : BaseBLL<ItemEspecialBLL>
    {
        public void Gravar(ItemEspecial item)
        {
            try
            {
                ItemEspecialDALC.Instance().Merge(item, Session.Usuario.Login);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao gravar margem de item especial.", ex);
            }
        }

        public List<ItemEspecial> ListarTodos(ItemEspecial filtro)
        {
            List<ItemEspecial> listaRetorno = null;

            try
            {
                listaRetorno = ItemEspecialDALC.Instance().SelectByFilter(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao listar margemns de item especial.", ex);
            }

            return listaRetorno;
        }

        public void Excluir(ItemEspecial item)
        {
            try
            {
                ItemEspecialDALC.Instance().Delete(item);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao excluir margens de item especial", ex);
            }
        }

        public List<RelatorioItemEspecial> SelectByFilterRelatorio(RelatorioItemEspecialFiltro filtro)
        {
            try
            {
                return ItemEspecialDALC.Instance().SelectByFilterRelatorio(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao excluir margens de item especial", ex);
                return null;
            }
        }

        public List<Material> SelecionarEntidadeBusca(Material filtro)
        {
            try
            {
                return ItemEspecialDALC.Instance().SelectMaterial(filtro);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro("Erro ao buscar material.", ex);
                return null;
            }
        }

        public String RetornarCodigoLabel()
        {
            return "Código:";
        }
    }
}
