﻿using System;

namespace VDA980.DALC
{
    public class Constantes
    {
        public const String ConexaoSdpk = "ORA_DPK_CONN";

        // Login - Usuário/Senha
        public const String ProcedureSelectUsuario = "PRODUCAO.PCK_VDA980.PR_SE_USUARIO";
        public const String ProcedureSelectPermissao = "PRODUCAO.PCK_VDA980.PR_SE_PERMISSAO_MARGEM";

        // CD's
        public const String ProcedureSelectCD = "PRODUCAO.PCK_VDA980.PR_SE_TODOS_CDS";

        // Análise
        public const String ProcedureSelectTipoAnalise = "PRODUCAO.PCK_VDA980.PR_SE_TIPO_ANALISE_MARGEM";

        // Cliente
        public const String ProcedureSelectCliente = "PRODUCAO.PCK_VDA980.PR_SE_CLIENTE";
        public const String ProcedureSelectTipoCliente = "PRODUCAO.PCK_VDA980.PR_SE_TIPO_CLIENTE";

        //Fornecedor
        public const String ProcedureSelectFornecedor = "PRODUCAO.PCK_VDA980.PR_SE_FORNECEDOR";

        // Material
        public const String ProcedureSelectMaterial = "PRODUCAO.PCK_VDA980.PR_SE_MATERIAL";        
        
        // Cliente/Fornecedor
        public const String ProcedureDeleteClienteFornecedor = "PRODUCAO.PCK_VDA980.PR_DE_CLIENTE_FORNECEDOR";
        public const String ProcedureMergeClienteFornecedor = "PRODUCAO.PCK_VDA980.PR_ME_CLIENTE_FORNECEDOR";
        public const String ProcedureSelectClienteFornecedor = "PRODUCAO.PCK_VDA980.PR_SE_CLIENTE_FORNECEDOR";

        // Item Especial
        public const String ProcedureDeleteItemEspecial = "PRODUCAO.PCK_VDA980.PR_DE_ITEM_ESPECIAL";
        public const String ProcedureMergeItemEspecial = "PRODUCAO.PCK_VDA980.PR_ME_ITEM_ESPECIAL";
        public const String ProcedureSelectItemEspecial = "PRODUCAO.PCK_VDA980.PR_SE_ITEM_ESPECIAL";
        
        // Parametro Default
        public const String ProcedureDeleteParametroPadrao = "PRODUCAO.PCK_VDA980.PR_DE_PARAMETRO_PADRAO";
        public const String ProcedureMergeParametroPadrao = "PRODUCAO.PCK_VDA980.PR_ME_PARAMETRO_PADRAO";
        public const String ProcedureSelectParametroPadrao = "PRODUCAO.PCK_VDA980.PR_SE_PARAMETRO_PADRAO";
        
        // Tipologia/Fornecedor
        public const String ProcedureDeleteTipologiaForncedor = "PRODUCAO.PCK_VDA980.PR_DE_TIPOLOGIA_FORNECEDOR";
        public const String ProcedureMergeTipologiaFornecedor = "PRODUCAO.PCK_VDA980.PR_ME_TIPOLOGIA_FORNECEDOR";
        public const String ProcedureSelectTipologiaFornecedor = "PRODUCAO.PCK_VDA980.PR_SE_TIPOLOGIA_FORNECEDOR";
        
        // Relatórios
        public const String ProcedureRelatorioAnaliseMargem = "PRODUCAO.PCK_VDA980.PR_REL_ANALISE_MARGEM";
        public const String ProcedureRelatorioClienteFornecedor = "PRODUCAO.PCK_VDA980.PR_REL_CLIENTE_FORNECEDOR";
        public const String ProcedureRelatorioItemEspecial = "PRODUCAO.PCK_VDA980.PR_REL_ITEM_ESPECIAL";
        public const String ProcedureRelatorioParametroPadrao = "PRODUCAO.PCK_VDA980.PR_REL_PARAMETRO_PADRAO";
        public const String ProcedureRelatorioParametroPadraoAtual = "PRODUCAO.PCK_VDA980.PR_REL_PARAMETRO_PADRAO_ATUAL";
        public const String ProcedureRelatorioTipologiaFornecedor = "PRODUCAO.PCK_VDA980.PR_REL_TIPOLOGIA_FORNECEDOR";
    }
}