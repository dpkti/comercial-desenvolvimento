﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class LojaDALC : BaseDALC<LojaDALC>
    {
        public List<Loja> SelecionarTodos()
        {
            IDataReader reader = null;
            Loja loja = null;
            List<Loja> listaRetorno = new List<Loja>();
            IRowMapper<Loja> mapper = MapBuilder<Loja>.MapAllProperties().DoNotMap(i=>i.TipoBanco).DoNotMap(i=>i.NomeExibicao).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectCD))
                {
                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        loja = mapper.MapRow(reader);
                        listaRetorno.Add(loja);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }
    }
}
