﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;
using System.Data.OracleClient;

namespace VDA980.DALC
{
    public class UsuarioDALC : BaseDALC<UsuarioDALC>
    {
        public Usuario SelecionarUsuario(string usuario, string senha)
        {
            IDataReader reader = null;
            Usuario usuarioLogado = null;
            IRowMapper<Usuario> mapper = MapBuilder<Usuario>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);
                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectUsuario))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, usuario);
                    db.AddInParameter(cmd, "P_SENHA", DbType.String, senha);

                    reader = db.ExecuteReader(cmd);
                    if (reader.Read())
                    {
                        usuarioLogado = mapper.MapRow(reader);
                    }
                }
            }
            catch (OracleException ex)
            {
                // Procedure de login do legado retorna NO_DATA_FOUND quando o usuário/senha não são encontrados.
                // Subir exceção caso ocorra um erro diferente deste.
                if (ex.Code != 1403)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return usuarioLogado;
        }

        public bool ValidarAcessoAplicacao(Int64 codUsuario, Loja loja)
        {
            bool possuiPermissao = false;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);
                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectPermissao))
                {

                    db.AddInParameter(cmd, "P_COD_USUARIO", DbType.String, codUsuario.ToString());
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, loja.CodigoLoja);
                    db.AddInParameter(cmd, "P_TIPO_BANCO", DbType.String, loja.TipoBanco);

                    // Caso o retorno seja maior que 0 é porque possui permissao
                    using (IDataReader reader = db.ExecuteReader(cmd))
                    {
                        if (reader.Read())
                        {
                            possuiPermissao = (int.Parse(reader[0].ToString()) > 0);
                        }
                    }
                }

                return possuiPermissao;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
