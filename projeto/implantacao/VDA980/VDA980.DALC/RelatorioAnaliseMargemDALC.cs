﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class RelatorioAnaliseMargemDALC : BaseDALC<RelatorioAnaliseMargemDALC>
    {
        public List<RelatorioAnaliseMargem> SelectByFilter(RelatorioAnaliseMargemFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioAnaliseMargem analise = null;
            List<RelatorioAnaliseMargem> listaRetorno = new List<RelatorioAnaliseMargem>();
            IRowMapper<RelatorioAnaliseMargem> mapper = MapBuilder<RelatorioAnaliseMargem>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioAnaliseMargem))
                {
                    //db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_LOJA_DESTINO", DbType.Int32, (filtro.CodigoLojaDestino != 0 ? (object)filtro.CodigoLojaDestino : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_PEDIDO_INICIO", DbType.Date, (filtro.DataPedidoInicio != null ? (object)filtro.DataPedidoInicio : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_PEDIDO_FIM", DbType.Date, (filtro.DataPedidoFim != null ? (object)filtro.DataPedidoFim : DBNull.Value));
                    db.AddInParameter(cmd, "P_NUM_PEDIDO", DbType.Int64, (filtro.NumeroPedido != 0 ? (object)filtro.NumeroPedido : DBNull.Value));
                    db.AddInParameter(cmd, "P_CD_TIPO_ANALISE_MARGEM", DbType.Int32, (filtro.CodigoTipoAnalise != 0 ? (object)filtro.CodigoTipoAnalise : DBNull.Value));
                    db.AddInParameter(cmd, "P_FL_ANALISE_BLOQUEIO", DbType.String, (!String.IsNullOrEmpty(filtro.FlagAnalise) ? (object)filtro.FlagAnalise : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        analise = mapper.MapRow(reader);
                        listaRetorno.Add(analise);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }
    }
}
