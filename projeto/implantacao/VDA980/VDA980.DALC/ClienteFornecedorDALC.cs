﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class ClienteFornecedorDALC : BaseDALC<ClienteFornecedorDALC>
    {
        public List<ClienteFornecedor> SelectByFilter(ClienteFornecedor filtro)
        {
            IDataReader reader = null;
            ClienteFornecedor item = null;
            List<ClienteFornecedor> listaRetorno = new List<ClienteFornecedor>();
            IRowMapper<ClienteFornecedor> mapper = MapBuilder<ClienteFornecedor>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectClienteFornecedor))
                {
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, (filtro.CGCFornecedor != 0 ? (object)filtro.CGCFornecedor : DBNull.Value));
                    db.AddInParameter(cmd, "P_CGC_CLIENTE", DbType.Int64, (filtro.CNPJCliente != 0 ? (object)filtro.CNPJCliente : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public void Merge(ClienteFornecedor item, string usuario)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureMergeClienteFornecedor))
                {
                    db.AddInParameter(cmd, "P_CGC_CLIENTE", DbType.Int64, item.CNPJCliente);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.String, item.CGCFornecedor);
                    db.AddInParameter(cmd, "P_PC_MARGEM", DbType.Decimal, item.Margem);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, usuario);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(ClienteFornecedor item)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureDeleteClienteFornecedor))
                {
                    db.AddInParameter(cmd, "P_CGC_CLIENTE", DbType.Int64, item.CNPJCliente);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, item.CGCFornecedor);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RelatorioClienteFornecedor> SelectByFilterRelatorio(RelatorioClienteFornecedorFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioClienteFornecedor item = null;
            List<RelatorioClienteFornecedor> listaRetorno = new List<RelatorioClienteFornecedor>();
            IRowMapper<RelatorioClienteFornecedor> mapper = MapBuilder<RelatorioClienteFornecedor>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioClienteFornecedor))
                {
                    db.AddInParameter(cmd, "P_CGC_CLIENTE", DbType.Int64, (filtro.CNPJCliente != 0 ? (object)filtro.CNPJCliente : DBNull.Value));
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, (filtro.CGCFornecedor != 0 ? (object)filtro.CGCFornecedor : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_INI", DbType.DateTime, (filtro.DataVigenciaIni != null ? (object)filtro.DataVigenciaIni : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_FIM", DbType.DateTime, (filtro.DataVigenciaFim != null ? (object)filtro.DataVigenciaFim : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public List<Cliente> SelectCliente(Cliente filtro)
        {
            IDataReader reader = null;
            Cliente item = null;
            List<Cliente> listaRetorno = new List<Cliente>();
            IRowMapper<Cliente> mapper = MapBuilder<Cliente>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectCliente))
                {
                    db.AddInParameter(cmd, "P_CGC_CLIENTE", DbType.Int64, (filtro.CNPJCliente != 0 ? (object)filtro.CNPJCliente : DBNull.Value));
                    db.AddInParameter(cmd, "P_NOME_CLIENTE", DbType.String, (!String.IsNullOrEmpty(filtro.NomeCliente) ? (object)filtro.NomeCliente : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }
    }
}
