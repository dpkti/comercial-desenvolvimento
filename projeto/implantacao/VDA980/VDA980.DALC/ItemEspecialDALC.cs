﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class ItemEspecialDALC : BaseDALC<ItemEspecialDALC>
    {
        public void Merge(ItemEspecial item, string usuario)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureMergeItemEspecial))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, item.CodigoDPK);
                    db.AddInParameter(cmd, "P_PC_MARGEM", DbType.Decimal, item.Margem);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, usuario);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //tratar
            }
        }

        public List<ItemEspecial> SelectByFilter(ItemEspecial filtro)
        {
            IDataReader reader = null;
            ItemEspecial item = null;
            List<ItemEspecial> listaRetorno = new List<ItemEspecial>();
            IRowMapper<ItemEspecial> mapper = MapBuilder<ItemEspecial>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectItemEspecial))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, (filtro.CodigoDPK != 0 ? (object)filtro.CodigoDPK : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public void Delete(ItemEspecial item)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureDeleteItemEspecial))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.String, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.String, item.CodigoDPK);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RelatorioItemEspecial> SelectByFilterRelatorio(RelatorioItemEspecialFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioItemEspecial item = null;
            List<RelatorioItemEspecial> listaRetorno = new List<RelatorioItemEspecial>();
            IRowMapper<RelatorioItemEspecial> mapper = MapBuilder<RelatorioItemEspecial>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioItemEspecial))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, (filtro.CodigoDPK != 0 ? (object)filtro.CodigoDPK : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_INI", DbType.DateTime, (filtro.DataVigenciaIni != null ? (object)filtro.DataVigenciaIni : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_FIM", DbType.DateTime, (filtro.DataVigenciaFim != null ? (object)filtro.DataVigenciaFim : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public List<Material> SelectMaterial(Material filtro)
        {
            IDataReader reader = null;
            Material item = null;
            List<Material> listaRetorno = new List<Material>();
            IRowMapper<Material> mapper = MapBuilder<Material>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectMaterial))
                {
                    db.AddInParameter(cmd, "P_COD_DPK", DbType.Int64, (filtro.CodigoDPK != 0 ? (object)filtro.CodigoDPK : DBNull.Value));
                    db.AddInParameter(cmd, "P_NOME_ITEM", DbType.String, (!String.IsNullOrEmpty(filtro.DescItem) ? (object)filtro.DescItem : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_FABRICA", DbType.String, (!String.IsNullOrEmpty(filtro.CodFabrica) ? (object)filtro.CodFabrica : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }
    }
}
