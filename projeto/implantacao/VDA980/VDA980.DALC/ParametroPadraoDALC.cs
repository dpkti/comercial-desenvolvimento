﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class ParametroPadraoDALC : BaseDALC<ParametroPadraoDALC>
    {
        public List<ParametroPadrao> SelectByFilter(ParametroPadrao filtro)
        {
            IDataReader reader = null;
            ParametroPadrao item = null;
            List<ParametroPadrao> listaRetorno = new List<ParametroPadrao>();
            IRowMapper<ParametroPadrao> mapper = MapBuilder<ParametroPadrao>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectParametroPadrao))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_NOME_PARAMETRO", DbType.String, (!String.IsNullOrEmpty(filtro.NomeParametro)  ? (object)filtro.NomeParametro : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public List<TipoAnalise> SelectTipoAnalise()
        {
            IDataReader reader = null;
            TipoAnalise item = null;
            List<TipoAnalise> listaRetorno = new List<TipoAnalise>();
            IRowMapper<TipoAnalise> mapper = MapBuilder<TipoAnalise>.MapAllProperties().DoNotMap(i => i.NomeExibicao).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectTipoAnalise))
                {
                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public void Merge(ParametroPadrao item, string usuario)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureMergeParametroPadrao))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_NOME_PARAMETRO", DbType.String, item.NomeParametro);
                    db.AddInParameter(cmd, "P_VALOR_PARAMETRO", DbType.String, item.ValorParametro);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, (item.DataVigencia != DateTime.MinValue ? (object)item.DataVigencia : DBNull.Value));
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, usuario);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RelatorioParametroPadrao> SelectByFilterRelatorio(RelatorioParametroPadraoFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioParametroPadrao item = null;
            List<RelatorioParametroPadrao> listaRetorno = new List<RelatorioParametroPadrao>();
            IRowMapper<RelatorioParametroPadrao> mapper = MapBuilder<RelatorioParametroPadrao>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).DoNotMap(i => i.MargemDecimal).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioParametroPadrao))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_NOME_PARAMETRO", DbType.String, (!String.IsNullOrEmpty(filtro.NomeParametro) ? (object)filtro.NomeParametro : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_INI", DbType.DateTime, (filtro.DataVigenciaIni != null ? (object)filtro.DataVigenciaIni : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_FIM", DbType.DateTime, (filtro.DataVigenciaFim != null ? (object)filtro.DataVigenciaFim : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public RelatorioParametroPadrao SelectByFilterMargemAtual(RelatorioParametroPadraoFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioParametroPadrao item = null;
            List<RelatorioParametroPadrao> listaRetorno = new List<RelatorioParametroPadrao>();
            IRowMapper<RelatorioParametroPadrao> mapper = MapBuilder<RelatorioParametroPadrao>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).DoNotMap(i => i.MargemDecimal).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioParametroPadraoAtual))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno.FirstOrDefault();
        }

        public void Delete(ParametroPadrao item)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureDeleteParametroPadrao))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.String, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_NOME_PARAMETRO", DbType.String, item.NomeParametro);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
