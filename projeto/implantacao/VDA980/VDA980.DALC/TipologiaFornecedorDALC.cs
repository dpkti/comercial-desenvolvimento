﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using VDA980.Entities;

namespace VDA980.DALC
{
    public class TipologiaFornecedorDALC : BaseDALC<TipologiaFornecedorDALC>
    {
        public List<TipologiaFornecedor> SelectByFilter(TipologiaFornecedor filtro)
        {
            IDataReader reader = null;
            TipologiaFornecedor item = null;
            List<TipologiaFornecedor> listaRetorno = new List<TipologiaFornecedor>();
            IRowMapper<TipologiaFornecedor> mapper = MapBuilder<TipologiaFornecedor>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectTipologiaFornecedor))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_TIPO_CLIENTE", DbType.String, !String.IsNullOrEmpty(filtro.CodTipoCliente) ? (object)filtro.CodTipoCliente : DBNull.Value);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, (filtro.CGCFornecedor != 0 ? (object)filtro.CGCFornecedor : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public List<TipoCliente> SelectTipoCliente()
        {
            IDataReader reader = null;
            TipoCliente item = null;
            List<TipoCliente> listaRetorno = new List<TipoCliente>();
            IRowMapper<TipoCliente> mapper = MapBuilder<TipoCliente>.MapAllProperties().DoNotMap(i => i.NomeExibicao).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectTipoCliente))
                {
                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public void Merge(TipologiaFornecedor item, string usuario)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureMergeTipologiaFornecedor))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_COD_TIPO_CLIENTE", DbType.String, item.CodTipoCliente);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, item.CGCFornecedor);
                    db.AddInParameter(cmd, "P_PC_MARGEM", DbType.Decimal, item.Margem);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, usuario);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(TipologiaFornecedor item)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureDeleteTipologiaForncedor))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, item.CodigoLoja);
                    db.AddInParameter(cmd, "P_COD_TIPO_CLIENTE", DbType.String, item.CodTipoCliente);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, item.CGCFornecedor);
                    db.AddInParameter(cmd, "P_DT_VIGENCIA", DbType.Date, item.DataVigencia);

                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RelatorioTipologiaFornecedor> SelectByFilterRelatorio(RelatorioTipologiaFornecedorFiltro filtro)
        {
            IDataReader reader = null;
            RelatorioTipologiaFornecedor item = null;
            List<RelatorioTipologiaFornecedor> listaRetorno = new List<RelatorioTipologiaFornecedor>();
            IRowMapper<RelatorioTipologiaFornecedor> mapper = MapBuilder<RelatorioTipologiaFornecedor>.MapAllProperties().DoNotMap(i => i.DataVigenciaStr).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureRelatorioTipologiaFornecedor))
                {
                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, (filtro.CodigoLoja != 0 ? (object)filtro.CodigoLoja : DBNull.Value));
                    db.AddInParameter(cmd, "P_COD_TIPO_CLIENTE", DbType.String, !String.IsNullOrEmpty(filtro.CodTipoCliente) ? (object)filtro.CodTipoCliente : DBNull.Value);
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, (filtro.CGCFornecedor != 0 ? (object)filtro.CGCFornecedor : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_INI", DbType.DateTime, (filtro.DataVigenciaIni != null ? (object)filtro.DataVigenciaIni : DBNull.Value));
                    db.AddInParameter(cmd, "P_DT_VIGENCIA_FIM", DbType.DateTime, (filtro.DataVigenciaFim != null ? (object)filtro.DataVigenciaFim : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }

        public List<Fornecedor> SelectFornecedor(Fornecedor filtro)
        {
            IDataReader reader = null;
            Fornecedor item = null;
            List<Fornecedor> listaRetorno = new List<Fornecedor>();
            IRowMapper<Fornecedor> mapper = MapBuilder<Fornecedor>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(Constantes.ConexaoSdpk);

                using (DbCommand cmd = db.GetStoredProcCommand(Constantes.ProcedureSelectFornecedor))
                {
                    db.AddInParameter(cmd, "P_CGC_FORNECEDOR", DbType.Int64, (filtro.CNPJFornecedor != 0 ? (object)filtro.CNPJFornecedor : DBNull.Value));
                    db.AddInParameter(cmd, "P_NOME_FORNECEDOR", DbType.String, (!String.IsNullOrEmpty(filtro.NomeFornecedor) ? (object)filtro.NomeFornecedor : DBNull.Value));

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        listaRetorno.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return listaRetorno;
        }
    }
}
