﻿namespace VDA980.Controle
{
    partial class ucTipologiaFornecedor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbInformacao = new System.Windows.Forms.GroupBox();
            this.btnPesquisaFornecedor = new System.Windows.Forms.Button();
            this.lblNomeFornecedor = new System.Windows.Forms.Label();
            this.btnExlcuir = new System.Windows.Forms.Button();
            this.cmbTipoCliente = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCgcFornecedor = new System.Windows.Forms.TextBox();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dgvTipologiaFornecedor = new System.Windows.Forms.DataGridView();
            this.txtDataVigencia = new System.Windows.Forms.MaskedTextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblDataVigencia = new System.Windows.Forms.Label();
            this.lblMargem = new System.Windows.Forms.Label();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbInformacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipologiaFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // gbInformacao
            // 
            this.gbInformacao.Controls.Add(this.btnPesquisaFornecedor);
            this.gbInformacao.Controls.Add(this.lblNomeFornecedor);
            this.gbInformacao.Controls.Add(this.btnExlcuir);
            this.gbInformacao.Controls.Add(this.cmbTipoCliente);
            this.gbInformacao.Controls.Add(this.label2);
            this.gbInformacao.Controls.Add(this.txtCgcFornecedor);
            this.gbInformacao.Controls.Add(this.cmbLoja);
            this.gbInformacao.Controls.Add(this.btnConsultar);
            this.gbInformacao.Controls.Add(this.dgvTipologiaFornecedor);
            this.gbInformacao.Controls.Add(this.txtDataVigencia);
            this.gbInformacao.Controls.Add(this.btnSalvar);
            this.gbInformacao.Controls.Add(this.lblDataVigencia);
            this.gbInformacao.Controls.Add(this.lblMargem);
            this.gbInformacao.Controls.Add(this.txtMargem);
            this.gbInformacao.Controls.Add(this.lblCodLoja);
            this.gbInformacao.Controls.Add(this.label1);
            this.gbInformacao.Location = new System.Drawing.Point(3, 3);
            this.gbInformacao.Name = "gbInformacao";
            this.gbInformacao.Size = new System.Drawing.Size(744, 379);
            this.gbInformacao.TabIndex = 0;
            this.gbInformacao.TabStop = false;
            this.gbInformacao.Text = "Cadastro de Margem - Tipologia/Fornecedor";
            // 
            // btnPesquisaFornecedor
            // 
            this.btnPesquisaFornecedor.BackgroundImage = global::VDA980.Properties.Resources.lupa;
            this.btnPesquisaFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisaFornecedor.FlatAppearance.BorderSize = 0;
            this.btnPesquisaFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisaFornecedor.Location = new System.Drawing.Point(281, 74);
            this.btnPesquisaFornecedor.Name = "btnPesquisaFornecedor";
            this.btnPesquisaFornecedor.Size = new System.Drawing.Size(20, 20);
            this.btnPesquisaFornecedor.TabIndex = 14;
            this.btnPesquisaFornecedor.UseVisualStyleBackColor = true;
            this.btnPesquisaFornecedor.Click += new System.EventHandler(this.btnPesquisaFornecedor_Click);
            // 
            // lblNomeFornecedor
            // 
            this.lblNomeFornecedor.AutoSize = true;
            this.lblNomeFornecedor.Location = new System.Drawing.Point(309, 78);
            this.lblNomeFornecedor.Name = "lblNomeFornecedor";
            this.lblNomeFornecedor.Size = new System.Drawing.Size(0, 13);
            this.lblNomeFornecedor.TabIndex = 26;
            // 
            // btnExlcuir
            // 
            this.btnExlcuir.Enabled = false;
            this.btnExlcuir.Location = new System.Drawing.Point(195, 159);
            this.btnExlcuir.Name = "btnExlcuir";
            this.btnExlcuir.Size = new System.Drawing.Size(75, 23);
            this.btnExlcuir.TabIndex = 8;
            this.btnExlcuir.Text = "&Excluir";
            this.btnExlcuir.UseVisualStyleBackColor = true;
            this.btnExlcuir.Click += new System.EventHandler(this.btnExlcuir_Click);
            // 
            // cmbTipoCliente
            // 
            this.cmbTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoCliente.FormattingEnabled = true;
            this.cmbTipoCliente.Location = new System.Drawing.Point(121, 49);
            this.cmbTipoCliente.Name = "cmbTipoCliente";
            this.cmbTipoCliente.Size = new System.Drawing.Size(150, 21);
            this.cmbTipoCliente.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "CNPJ Fornecedor";
            // 
            // txtCgcFornecedor
            // 
            this.txtCgcFornecedor.Location = new System.Drawing.Point(121, 75);
            this.txtCgcFornecedor.MaxLength = 14;
            this.txtCgcFornecedor.Name = "txtCgcFornecedor";
            this.txtCgcFornecedor.Size = new System.Drawing.Size(149, 20);
            this.txtCgcFornecedor.TabIndex = 3;
            this.txtCgcFornecedor.TextChanged += new System.EventHandler(this.txtCgcFornecedor_TextChanged);
            this.txtCgcFornecedor.Leave += new System.EventHandler(this.txtCgcFornecedor_Leave);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(121, 24);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(149, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(22, 159);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(85, 23);
            this.btnConsultar.TabIndex = 6;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dgvTipologiaFornecedor
            // 
            this.dgvTipologiaFornecedor.AllowUserToAddRows = false;
            this.dgvTipologiaFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTipologiaFornecedor.Location = new System.Drawing.Point(22, 195);
            this.dgvTipologiaFornecedor.MultiSelect = false;
            this.dgvTipologiaFornecedor.Name = "dgvTipologiaFornecedor";
            this.dgvTipologiaFornecedor.ReadOnly = true;
            this.dgvTipologiaFornecedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTipologiaFornecedor.Size = new System.Drawing.Size(704, 161);
            this.dgvTipologiaFornecedor.TabIndex = 21;
            this.dgvTipologiaFornecedor.SelectionChanged += new System.EventHandler(this.dgvTipologiaFornecedor_SelectionChanged);
            // 
            // txtDataVigencia
            // 
            this.txtDataVigencia.Location = new System.Drawing.Point(121, 126);
            this.txtDataVigencia.Mask = "00/00/0000";
            this.txtDataVigencia.Name = "txtDataVigencia";
            this.txtDataVigencia.Size = new System.Drawing.Size(149, 20);
            this.txtDataVigencia.TabIndex = 5;
            this.txtDataVigencia.ValidatingType = typeof(System.DateTime);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(113, 159);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 7;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblDataVigencia
            // 
            this.lblDataVigencia.AutoSize = true;
            this.lblDataVigencia.Location = new System.Drawing.Point(19, 133);
            this.lblDataVigencia.Name = "lblDataVigencia";
            this.lblDataVigencia.Size = new System.Drawing.Size(74, 13);
            this.lblDataVigencia.TabIndex = 18;
            this.lblDataVigencia.Text = "Data Vigência";
            // 
            // lblMargem
            // 
            this.lblMargem.AutoSize = true;
            this.lblMargem.Location = new System.Drawing.Point(19, 103);
            this.lblMargem.Name = "lblMargem";
            this.lblMargem.Size = new System.Drawing.Size(62, 13);
            this.lblMargem.TabIndex = 17;
            this.lblMargem.Text = "Margem (%)";
            // 
            // txtMargem
            // 
            this.txtMargem.Location = new System.Drawing.Point(121, 100);
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(149, 20);
            this.txtMargem.TabIndex = 4;
            this.txtMargem.TextChanged += new System.EventHandler(this.txtMargem_TextChanged);
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(19, 28);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 15;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tipo Cliente";
            // 
            // ucTipologiaFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbInformacao);
            this.Name = "ucTipologiaFornecedor";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbInformacao.ResumeLayout(false);
            this.gbInformacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipologiaFornecedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbInformacao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCgcFornecedor;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DataGridView dgvTipologiaFornecedor;
        private System.Windows.Forms.MaskedTextBox txtDataVigencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblDataVigencia;
        private System.Windows.Forms.Label lblMargem;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTipoCliente;
        private System.Windows.Forms.Button btnExlcuir;
        private System.Windows.Forms.Label lblNomeFornecedor;
        private System.Windows.Forms.Button btnPesquisaFornecedor;
    }
}
