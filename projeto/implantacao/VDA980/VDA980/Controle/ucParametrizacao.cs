﻿using System;

namespace VDA980.Controle
{
    public partial class ucParametrizacao : ucBase
    {
        public ucParametrizacao()
        {
            InitializeComponent();
        }

        private void ucParametrizacao_Load(object sender, EventArgs e)
        {
            new ucParametroPadrao().Parent = controleTab.TabPages[0];
            new ucItemEspecial().Parent = controleTab.TabPages[1];
            new ucTipologiaFornecedor().Parent = controleTab.TabPages[2];
            new ucClienteFornecedor().Parent = controleTab.TabPages[3];
        }
    }
}
