﻿using System.Windows.Forms;
using VDA980.Entities;

namespace VDA980.Controle
{
    public partial class ucBase : UserControl
    {
        public ucBase()
        {
            InitializeComponent();
        }

        protected bool MensagemConfirmacao(string mensagem, string titulo)
        {
            return (MessageBox.Show(mensagem, titulo, MessageBoxButtons.YesNo) == DialogResult.Yes);
        }

        protected bool MensagemConfirmacao(string mensagem)
        {
            return MensagemConfirmacao(mensagem, "Alerta!");
        }

        protected void Mensagem(string mensagem, string titulo)
        {
            MessageBox.Show(mensagem, titulo);
        }

        protected void MensagemErro(string erro)
        {
            MessageBox.Show(erro, "Ocorreu um erro!");
        }

        protected static void tratarCampoDecimal(object sender)
        {
            TextBox box = (TextBox)sender;
            decimal dec = 0;

            box.Text = box.Text.Replace(".", string.Empty);
            box.Select(box.Text.Length, 0);

            if (!decimal.TryParse(box.Text, out dec) & !box.Text.Equals("-") || (dec < -9999.99m || dec > 9999.99m))
            {
                if (box.Text.Length > 1)
                    box.Text = box.Text.Substring(0, box.Text.Length - 1);
                else
                    box.Text = "";

                box.Select(box.Text.Length, 0);
            }
        }

        protected static void tratarCampoLong(object sender)
        {
            TextBox box = (TextBox)sender;
            long lng;

            if (!long.TryParse(box.Text, out lng))
            {
                if (box.Text.Length > 1)
                    box.Text = box.Text.Substring(0, box.Text.Length - 1);
                else
                    box.Text = "";

                box.Select(box.Text.Length, 0);
            }
        }

        public virtual void AtualizarCodigoText(ModeloBusca modelo)
        {

        }

    }
}
