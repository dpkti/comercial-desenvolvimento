﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;

namespace VDA980.Controle
{
    public partial class ucParametroPadrao : ucBase
    {
        #region Propriedades

        private ParametroPadrao tipoAnalise = null;
        private ParametroPadrao itemManipulacao = new ParametroPadrao();

        #endregion

        #region Construtor

        public ucParametroPadrao()
        {
            InitializeComponent();

            carregarLoja();

            ajustarBind();

            habiltarControles(false);
        }

        #endregion

        #region Eventos

        private void txtMargemItem_TextChanged(object sender, EventArgs e)
        {
            tratarCampoDecimal(sender);
        }

        private void txtMargemPedido_TextChanged(object sender, EventArgs e)
        {
            tratarCampoDecimal(sender);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                efetuarConsulta();

                habiltarControles(true);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Consultar parâmetros padrão");
            }
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            limparTela();

            habiltarControles(false);
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                salvarParametros();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu ao realizar a gravação dos parâmetros: " + ex.Message, "Salvar item");
            }
        }

        private void dgvParametroPadrao_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                carregarCampoSelecao();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Selecionar item");
            }
        }

        private void rdbPedido_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbPedido.Checked)
            {
                alterarTipoMargem("P");
            }
        }

        private void rdbItem_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbItem.Checked)
            {
                alterarTipoMargem("I");
            }
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                validarAlteracaoTipoAnalise();

                if (validarPreenchimentoExclusao())
                {
                    if ((((List<ParametroPadrao>)dgvParametroPadrao.DataSource).Where(i => itemManipulacao.DataVigencia.Date <= DateTime.Now.Date
                                                                                   && i.CodigoLoja == itemManipulacao.CodigoLoja
                                                                                   && i.NomeParametro == itemManipulacao.NomeParametro
                                                                                   && i.DataVigencia == itemManipulacao.DataVigencia)).Count() == 1)
                    {
                        throw new Exception("Não é possível realizar a exclusão da margem vigente!");
                    }

                    exluirRegistroAtual();

                    efetuarConsulta();
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro ao realizar a exclusão: " + ex.Message, "Excluir item");
            }
        }

        #endregion

        #region Métodos

        private void validarAlteracaoTipoAnalise()
        {
            var tipoAnaliseOriginal =
                ParametroPadraoBLL.Instance().ObterParametroVigente((Loja)cmbLoja.SelectedItem, TipoParametro.TIPO_ANALISE_MARGEM);

            if (tipoAnalise.ValorParametro != tipoAnaliseOriginal.ValorParametro)
            {
                throw new Exception("O tipo de análise do CD (loja) foi alterado, é necessário salvar antes de realizar manipulação da(s) margen(s)");
            }
        }

        private void habiltarControles(bool ativo)
        {
            gpbTipoAnalise.Enabled = ativo;
            gpbPercentual.Enabled = ativo;
            btnSalvar.Enabled = ativo;
            btnExcluir.Enabled = ativo;
        }

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            txtMargem.DataBindings.Clear();
            var bindingMargem = new Binding("Text", itemManipulacao, "ValorParametro", true, DataSourceUpdateMode.OnPropertyChanged, null, "####.##", null);
            bindingMargem.Parse += OnNullableTextBindingParsed;
            txtMargem.DataBindings.Add(bindingMargem);

            txtDataVigencia.DataBindings.Clear();
            txtDataVigencia.DataBindings.Add("Text", itemManipulacao, "DataVigenciaStr");

        }

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private bool validarPreenchimentoExclusao()
        {
            bool validacao = true;

            if (itemManipulacao.CodigoLoja == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.ValorParametro == null)
            {
                validacao = false;
            }
            else
            {
                if (itemManipulacao.ValorParametro.Trim().Equals("0"))
                {
                    validacao = false;
                }
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
            }

            if (!validacao)
            {
                Mensagem("Selecione uma margem no grid antes de excluí-la!", "Validação de Informações");
            }
            return validacao;
        }

        private bool validarPreenchimento()
        {
            bool validacao = true;
            String mensagens = String.Empty;

            mensagens = "Ocorreram o(s) seguinte(s) erro(s):\n";

            if (itemManipulacao.CodigoLoja == 0)
            {
                validacao = false;
                mensagens += " - O Código Loja é obrigatório;\n";
            }

            if (itemManipulacao.ValorParametro == null)
            {
                validacao = false;
                mensagens += " - A Margem é obrigatória;\n";
            }
            else
            {
                if (itemManipulacao.ValorParametro.Trim().Equals("0"))
                {
                    validacao = false;
                    mensagens += " - A Margem não pode ser igual a 0;\n";
                }
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
                mensagens += " - A Data Vigência é obrigatória;\n";
            }

            var listaParam = (List<ParametroPadrao>)dgvParametroPadrao.DataSource;

            if (listaParam != null && listaParam.Count == 0 && itemManipulacao.DataVigencia.Date != DateTime.Now.Date)
            {
                validacao = false;
                mensagens += " - A primeira margem cadastrada na parametrização padrão deve ter vigência para data de hoje;\n";
                
            }

            if (!validacao)
            {
                Mensagem(mensagens, "Validação de Informações");
            }
            return validacao;
        }

        private void salvarParametros()
        {
            salvarTipoAnalise();

            salvarMargem();
        }

        private void salvarMargem()
        {
            string msg;

            if (itemManipulacao != null)
            {
                if (validarPreenchimento())
                {
                    msg = (!procurarMargem()) ? "Confirma inclusão de uma nova vigência?"
                        : "Deseja confirmar a atualização da vigência atual com a margem alterada?";

                    if (MensagemConfirmacao(msg))
                    {
                        ParametroPadraoBLL.Instance().Gravar(itemManipulacao);

                        Mensagem("Sucesso ao realizar a gravação do(s) parâmetro(s)!", "Salvar parametrização");

                        efetuarConsulta();
                    }
                }
            }
        }

        private void salvarTipoAnalise()
        {
            // inicia o parametro como o gravar = verdadeiro, pois se não ha nenhuma parametrização o tipoAtual será nulo
            bool gravar = true;

            var tipoAtual =
                ParametroPadraoBLL.Instance().ObterParametroVigente((Loja)cmbLoja.SelectedItem, TipoParametro.TIPO_ANALISE_MARGEM);

            if (tipoAtual != null && tipoAtual.ValorParametro != tipoAnalise.ValorParametro)
            {
                gravar = MensagemConfirmacao(String.Format("O CD (loja): {0} realiza análise por {1}, deseja alterar para análise por {2}?",
                    ((Loja)cmbLoja.SelectedItem).NomeExibicao, ParametroPadraoBLL.NomeTipoAnalise(tipoAtual), ParametroPadraoBLL.NomeTipoAnalise(tipoAnalise)));
            }

            if (gravar)
            {
                ParametroPadraoBLL.Instance().Gravar(tipoAnalise);
            }
        }

        private void efetuarConsulta()
        {
            var loja = (Loja)cmbLoja.SelectedItem;

            buscarParametros(loja);
        }

        private void buscarParametros(Loja loja)
        {
            if (loja != null)
            {
                tipoAnalise =
                    ParametroPadraoBLL.Instance().ObterParametroVigente(loja, TipoParametro.TIPO_ANALISE_MARGEM);

                buscarMargem(loja, String.Empty);
            }
        }

        private void buscarMargem(Loja loja, String selecaoTela)
        {
            TipoParametro tipo = TipoParametro.NENHUM;
            String valorParametro = string.Empty;

            if (String.IsNullOrEmpty(selecaoTela))
            {
                if (tipoAnalise != null)
                {
                    valorParametro = tipoAnalise.ValorParametro.ToUpper().Trim();
                }
                else
                {
                    tipoAnalise = new ParametroPadrao() { CodigoLoja = ((Loja)cmbLoja.SelectedItem).CodigoLoja, NomeParametro = TipoParametro.TIPO_ANALISE_MARGEM.ToString() };
                }
            }
            else
            {
                valorParametro = selecaoTela;
            }

            if (valorParametro == "P")
            {
                tipo = TipoParametro.MARGEM_PEDIDO;
                itemManipulacao.NomeParametro = TipoParametro.MARGEM_PEDIDO.ToString();
            }

            if (valorParametro == "I")
            {
                tipo = TipoParametro.MARGEM_ITEM;
                itemManipulacao.NomeParametro = TipoParametro.MARGEM_ITEM.ToString();
            }

            ajustarTipoMargem(tipo);

            dgvParametroPadrao.DataSource = ParametroPadraoBLL.Instance().ListarTodos(loja, tipo);

            ajustarTamanhoColuna();

            limparSelecao();
        }

        private void ajustarTipoMargem(TipoParametro tipo)
        {
            rdbPedido.Checked = (tipo == TipoParametro.MARGEM_PEDIDO);
            rdbItem.Checked = (tipo == TipoParametro.MARGEM_ITEM);
        }

        private void limparSelecao()
        {
            dgvParametroPadrao.ClearSelection();

            itemManipulacao = new ParametroPadrao() { CodigoLoja = ((Loja)cmbLoja.SelectedItem).CodigoLoja, NomeParametro = itemManipulacao.NomeParametro };

            ajustarBind();
        }

        private void ajustarTamanhoColuna()
        {
            if (dgvParametroPadrao.Columns.Count > 0)
            {
                dgvParametroPadrao.Columns[1].Width = 120;
                dgvParametroPadrao.Columns[3].Width = 120;
            }
        }

        private void exluirRegistroAtual()
        {
            if (MensagemConfirmacao("Confirma exclusão?"))
            {
                if (procurarMargem())
                {
                    ParametroPadraoBLL.Instance().Excluir(itemManipulacao);

                    Mensagem("Sucesso ao remover uma margem!", "Remoção de margens");
                }
                else
                {
                    Mensagem("Margem não encontrada para exclusão!", "Remoção de margens");
                }
            }
        }

        private void alterarTipoMargem(String tipo)
        {
            var loja = (Loja)cmbLoja.SelectedItem;

            if (tipoAnalise != null)
            {
                tipoAnalise.ValorParametro = tipo;
            }

            buscarMargem(loja, tipo);
        }

        private void limparTela()
        {
            rdbItem.Checked = false;
            rdbPedido.Checked = false;
            limparSelecao();
            dgvParametroPadrao.DataSource = null;
        }

        private void carregarCampoSelecao()
        {
            itemManipulacao = ((ParametroPadrao)this.dgvParametroPadrao.CurrentRow.DataBoundItem).ClonarTipado();
            ajustarBind();
        }

        #endregion

        private void txtMargem_Leave(object sender, EventArgs e)
        {
            double db = 0;
            TextBox box = (TextBox)sender;
            if (!String.IsNullOrEmpty(box.Text))
            {
                double.TryParse(box.Text, out db);
                if (db != 0)
                {
                    box.Text = Math.Round(db, 2).ToString();
                }
                else
                {
                    box.Text = "";
                }
            }
            
        }

        private bool procurarMargem()
        {
            var itemOriginal =
                        ParametroPadraoBLL.Instance().ListarTodos(itemManipulacao).FirstOrDefault(i => i.CodigoLoja == itemManipulacao.CodigoLoja
                                                                                                    && i.NomeParametro == itemManipulacao.NomeParametro
                                                                                                    && i.DataVigencia == itemManipulacao.DataVigencia);
            if (itemOriginal == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}


