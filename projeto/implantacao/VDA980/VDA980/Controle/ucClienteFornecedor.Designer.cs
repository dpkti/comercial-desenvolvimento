﻿namespace VDA980.Controle
{
    partial class ucClienteFornecedor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbInformacao = new System.Windows.Forms.GroupBox();
            this.btnPesquisaFornecedor = new System.Windows.Forms.Button();
            this.btnPesquisaCliente = new System.Windows.Forms.Button();
            this.lblNomeFornecedor = new System.Windows.Forms.Label();
            this.lblNomeCliente = new System.Windows.Forms.Label();
            this.txtCnpjFornecedor = new System.Windows.Forms.TextBox();
            this.txtCnpjCliente = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExlcuir = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dgvClienteFornecedor = new System.Windows.Forms.DataGridView();
            this.txtDataVigencia = new System.Windows.Forms.MaskedTextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblDataVigencia = new System.Windows.Forms.Label();
            this.lblMargem = new System.Windows.Forms.Label();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.gpbInformacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClienteFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbInformacao
            // 
            this.gpbInformacao.Controls.Add(this.btnPesquisaFornecedor);
            this.gpbInformacao.Controls.Add(this.btnPesquisaCliente);
            this.gpbInformacao.Controls.Add(this.lblNomeFornecedor);
            this.gpbInformacao.Controls.Add(this.lblNomeCliente);
            this.gpbInformacao.Controls.Add(this.txtCnpjFornecedor);
            this.gpbInformacao.Controls.Add(this.txtCnpjCliente);
            this.gpbInformacao.Controls.Add(this.label1);
            this.gpbInformacao.Controls.Add(this.label2);
            this.gpbInformacao.Controls.Add(this.btnExlcuir);
            this.gpbInformacao.Controls.Add(this.btnConsultar);
            this.gpbInformacao.Controls.Add(this.dgvClienteFornecedor);
            this.gpbInformacao.Controls.Add(this.txtDataVigencia);
            this.gpbInformacao.Controls.Add(this.btnSalvar);
            this.gpbInformacao.Controls.Add(this.lblDataVigencia);
            this.gpbInformacao.Controls.Add(this.lblMargem);
            this.gpbInformacao.Controls.Add(this.txtMargem);
            this.gpbInformacao.Location = new System.Drawing.Point(3, 3);
            this.gpbInformacao.Name = "gpbInformacao";
            this.gpbInformacao.Size = new System.Drawing.Size(744, 378);
            this.gpbInformacao.TabIndex = 1;
            this.gpbInformacao.TabStop = false;
            this.gpbInformacao.Text = "Cadastro de Margem - Cliente/Fornecedor";
            // 
            // btnPesquisaFornecedor
            // 
            this.btnPesquisaFornecedor.BackgroundImage = global::VDA980.Properties.Resources.lupa;
            this.btnPesquisaFornecedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisaFornecedor.FlatAppearance.BorderSize = 0;
            this.btnPesquisaFornecedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisaFornecedor.Location = new System.Drawing.Point(245, 55);
            this.btnPesquisaFornecedor.Name = "btnPesquisaFornecedor";
            this.btnPesquisaFornecedor.Size = new System.Drawing.Size(20, 20);
            this.btnPesquisaFornecedor.TabIndex = 32;
            this.btnPesquisaFornecedor.UseVisualStyleBackColor = true;
            this.btnPesquisaFornecedor.Click += new System.EventHandler(this.btnPesquisaFornecedor_Click);
            // 
            // btnPesquisaCliente
            // 
            this.btnPesquisaCliente.BackgroundImage = global::VDA980.Properties.Resources.lupa;
            this.btnPesquisaCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisaCliente.FlatAppearance.BorderSize = 0;
            this.btnPesquisaCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisaCliente.Location = new System.Drawing.Point(245, 24);
            this.btnPesquisaCliente.Name = "btnPesquisaCliente";
            this.btnPesquisaCliente.Size = new System.Drawing.Size(20, 20);
            this.btnPesquisaCliente.TabIndex = 13;
            this.btnPesquisaCliente.UseVisualStyleBackColor = true;
            this.btnPesquisaCliente.Click += new System.EventHandler(this.btnPesquisaCliente_Click);
            // 
            // lblNomeFornecedor
            // 
            this.lblNomeFornecedor.AutoSize = true;
            this.lblNomeFornecedor.Location = new System.Drawing.Point(270, 55);
            this.lblNomeFornecedor.Name = "lblNomeFornecedor";
            this.lblNomeFornecedor.Size = new System.Drawing.Size(0, 13);
            this.lblNomeFornecedor.TabIndex = 31;
            // 
            // lblNomeCliente
            // 
            this.lblNomeCliente.AutoSize = true;
            this.lblNomeCliente.Location = new System.Drawing.Point(270, 29);
            this.lblNomeCliente.Name = "lblNomeCliente";
            this.lblNomeCliente.Size = new System.Drawing.Size(0, 13);
            this.lblNomeCliente.TabIndex = 30;
            // 
            // txtCnpjFornecedor
            // 
            this.txtCnpjFornecedor.Location = new System.Drawing.Point(123, 51);
            this.txtCnpjFornecedor.MaxLength = 14;
            this.txtCnpjFornecedor.Name = "txtCnpjFornecedor";
            this.txtCnpjFornecedor.Size = new System.Drawing.Size(116, 20);
            this.txtCnpjFornecedor.TabIndex = 2;
            this.txtCnpjFornecedor.TextChanged += new System.EventHandler(this.txtCnpjFornecedor_TextChanged);
            this.txtCnpjFornecedor.Leave += new System.EventHandler(this.txtCnpjFornecedor_Leave);
            // 
            // txtCnpjCliente
            // 
            this.txtCnpjCliente.Location = new System.Drawing.Point(123, 24);
            this.txtCnpjCliente.MaxLength = 14;
            this.txtCnpjCliente.Name = "txtCnpjCliente";
            this.txtCnpjCliente.Size = new System.Drawing.Size(116, 20);
            this.txtCnpjCliente.TabIndex = 1;
            this.txtCnpjCliente.TextChanged += new System.EventHandler(this.txtCnpjCliente_TextChanged);
            this.txtCnpjCliente.Leave += new System.EventHandler(this.txtCnpjCliente_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "CPF/CNPJ Cliente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "CNPJ Fornecedor";
            // 
            // btnExlcuir
            // 
            this.btnExlcuir.Enabled = false;
            this.btnExlcuir.Location = new System.Drawing.Point(198, 133);
            this.btnExlcuir.Name = "btnExlcuir";
            this.btnExlcuir.Size = new System.Drawing.Size(75, 23);
            this.btnExlcuir.TabIndex = 7;
            this.btnExlcuir.Text = "&Excluir";
            this.btnExlcuir.UseVisualStyleBackColor = true;
            this.btnExlcuir.Click += new System.EventHandler(this.btnExlcuir_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(24, 133);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(85, 23);
            this.btnConsultar.TabIndex = 5;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dgvClienteFornecedor
            // 
            this.dgvClienteFornecedor.AllowUserToAddRows = false;
            this.dgvClienteFornecedor.AllowUserToDeleteRows = false;
            this.dgvClienteFornecedor.AllowUserToResizeColumns = false;
            this.dgvClienteFornecedor.AllowUserToResizeRows = false;
            this.dgvClienteFornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClienteFornecedor.Location = new System.Drawing.Point(21, 169);
            this.dgvClienteFornecedor.MultiSelect = false;
            this.dgvClienteFornecedor.Name = "dgvClienteFornecedor";
            this.dgvClienteFornecedor.ReadOnly = true;
            this.dgvClienteFornecedor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvClienteFornecedor.Size = new System.Drawing.Size(704, 199);
            this.dgvClienteFornecedor.TabIndex = 10;
            this.dgvClienteFornecedor.SelectionChanged += new System.EventHandler(this.dgvClienteFornecedor_SelectionChanged);
            // 
            // txtDataVigencia
            // 
            this.txtDataVigencia.Location = new System.Drawing.Point(123, 102);
            this.txtDataVigencia.Mask = "00/00/0000";
            this.txtDataVigencia.Name = "txtDataVigencia";
            this.txtDataVigencia.Size = new System.Drawing.Size(116, 20);
            this.txtDataVigencia.TabIndex = 4;
            this.txtDataVigencia.ValidatingType = typeof(System.DateTime);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(117, 133);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 6;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblDataVigencia
            // 
            this.lblDataVigencia.AutoSize = true;
            this.lblDataVigencia.Location = new System.Drawing.Point(21, 105);
            this.lblDataVigencia.Name = "lblDataVigencia";
            this.lblDataVigencia.Size = new System.Drawing.Size(74, 13);
            this.lblDataVigencia.TabIndex = 7;
            this.lblDataVigencia.Text = "Data Vigência";
            // 
            // lblMargem
            // 
            this.lblMargem.AutoSize = true;
            this.lblMargem.Location = new System.Drawing.Point(21, 80);
            this.lblMargem.Name = "lblMargem";
            this.lblMargem.Size = new System.Drawing.Size(62, 13);
            this.lblMargem.TabIndex = 5;
            this.lblMargem.Text = "Margem (%)";
            // 
            // txtMargem
            // 
            this.txtMargem.Location = new System.Drawing.Point(123, 77);
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(116, 20);
            this.txtMargem.TabIndex = 3;
            this.txtMargem.TextChanged += new System.EventHandler(this.txtMargem_TextChanged);
            // 
            // ucClienteFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbInformacao);
            this.Name = "ucClienteFornecedor";
            this.Size = new System.Drawing.Size(750, 382);
            this.gpbInformacao.ResumeLayout(false);
            this.gpbInformacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClienteFornecedor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbInformacao;
        private System.Windows.Forms.Button btnExlcuir;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DataGridView dgvClienteFornecedor;
        private System.Windows.Forms.MaskedTextBox txtDataVigencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblDataVigencia;
        private System.Windows.Forms.Label lblMargem;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCnpjFornecedor;
        private System.Windows.Forms.TextBox txtCnpjCliente;
        private System.Windows.Forms.Label lblNomeFornecedor;
        private System.Windows.Forms.Label lblNomeCliente;
        private System.Windows.Forms.Button btnPesquisaFornecedor;
        private System.Windows.Forms.Button btnPesquisaCliente;
    }
}
