﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;

namespace VDA980.Controle
{
    public partial class ucRelatorioAnaliseMargem : ucBase
    {
        #region Propriedades

        private RelatorioAnaliseMargemFiltro itemManipulacao = new RelatorioAnaliseMargemFiltro();

        #endregion

        #region Construtor

        public ucRelatorioAnaliseMargem()
        {
            try
            {
                InitializeComponent();

                carregarLoja();
                
                carregarTipoAnalise();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void txtNumeroPedido_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            List<RelatorioAnaliseMargem> listaRetorno = new List<RelatorioAnaliseMargem>();
            Relatorio relatorio = new Relatorio();

            try
            {
                if (validarTela())
                {
                    listaRetorno = RelatorioAnaliseMargemBLL.Instance().ListarTodos(itemManipulacao);

                    if (listaRetorno == null || listaRetorno.Count == 0)
                    {
                        Mensagem("Nenhum resultado foi encontrado.", "Relatório");
                    }
                    else
                    {
                        relatorio.Source = listaRetorno;
                        relatorio.NomeDataSet = "dsRelatorioAnaliseMargem";
                        relatorio.NomeRelatorio = "Relatório de Análise de Margem Pedido";
                        relatorio.ReportEmbeddedResource = "VDA980.Relatorios.RelatorioAnaliseMargem.rdlc";

                        frmRelatorio frm = new frmRelatorio();
                        frm.itemManipulacao = relatorio;
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Relatório");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
            else if (e.Value.ToString().Equals("  /  /")) e.Value = null;
        }

        #endregion

        #region Métodos

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private void carregarTipoAnalise()
        {
            var tipoAnalise = ParametroPadraoBLL.Instance().ListarTipoAnalise();

            cmbTipoAnalise.DataSource = tipoAnalise;
            cmbTipoAnalise.DisplayMember = "NomeExibicao";
            cmbTipoAnalise.ValueMember = "CodigoTipoAnalise";
        }

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLojaDestino");

            txtDataInicio.DataBindings.Clear();
            var bindingDataInicio = new Binding("Text", itemManipulacao, "DataPedidoInicio", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataInicio.Parse += OnNullableTextBindingParsed;
            txtDataInicio.DataBindings.Add(bindingDataInicio);

            txtDataFim.DataBindings.Clear();
            var bindingDataFim = new Binding("Text", itemManipulacao, "DataPedidoFim", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataFim.Parse += OnNullableTextBindingParsed;
            txtDataFim.DataBindings.Add(bindingDataFim);

            txtNumeroPedido.DataBindings.Clear();
            var bindingNumeroPedido = new Binding("Text", itemManipulacao, "NumeroPedido", true, DataSourceUpdateMode.OnPropertyChanged, null, "#######", null);
            bindingNumeroPedido.Parse += OnNullableTextBindingParsed;
            txtNumeroPedido.DataBindings.Add(bindingNumeroPedido);

            cmbTipoAnalise.DataBindings.Clear();
            cmbTipoAnalise.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoTipoAnalise");

            var bindingFlagBloqueado = new Binding("Checked", itemManipulacao, "Bloqueado", true, DataSourceUpdateMode.OnPropertyChanged, null);
            bindingFlagBloqueado.Parse += OnNullableTextBindingParsed;
            chkBloqueado.DataBindings.Add(bindingFlagBloqueado);

            var bindingFlagLiberado = new Binding("Checked", itemManipulacao, "Liberado", true, DataSourceUpdateMode.OnPropertyChanged, null);
            bindingFlagLiberado.Parse += OnNullableTextBindingParsed;
            chkLiberado.DataBindings.Add(bindingFlagLiberado);
        }

        private bool validarTela()
        {
            bool validarTela = true;

            if ((itemManipulacao.DataPedidoInicio != null && itemManipulacao.DataPedidoFim == null)
                    || (itemManipulacao.DataPedidoInicio == null && itemManipulacao.DataPedidoFim != null))
            {
                validarTela = false;
                Mensagem("É necessário preencher o Intervalo Data Vigência.", "Validação de Informações");
            }

            if (!itemManipulacao.Bloqueado && !itemManipulacao.Liberado)
            {
                validarTela = false;
                Mensagem("É necessário preencher o pelo menos um status.", "Validação de Informações");
            }

            if (itemManipulacao.DataPedidoInicio != null && itemManipulacao.DataPedidoFim != null)
            {
                if (itemManipulacao.DataPedidoInicio > itemManipulacao.DataPedidoFim)
                {
                    validarTela = false;
                    Mensagem("A Data Vigência Inicial deve ser menor que a Data Vigência Final.", "Validação de Informações");
                }
            }

            return validarTela;
        }

        #endregion

    }
}