﻿using System;
using System.Linq;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;


namespace VDA980.Controle
{
    public partial class ucItemEspecial : ucBase
    {

        #region Propriedades

        private ItemEspecial itemManipulacao = new ItemEspecial();
        private ItemEspecial ultimoFiltro = null;

        #endregion

        #region Construtor

        public ucItemEspecial()
        {
            InitializeComponent();
            try
            {
                carregarLoja();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void btnPesquisaMaterial_Click(object sender, EventArgs e)
        {
            var busca = new frmBuscaZoom(this, typeof(ItemEspecialBLL));

            busca.ShowDialog(this);
        }

        private void txtCodDpk_Leave(object sender, EventArgs e)
        {
            buscarItemDescricao();
        }

        private void txtMargem_TextChanged(object sender, EventArgs e)
        {
            tratarCampoDecimal(sender);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimento())
                {
                    salvarRegistroAtual();
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Salvar item");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                ultimoFiltro = itemManipulacao.ClonarTipado();

                carregarItens(itemManipulacao);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Consultar itens");
            }
        }

        private void dgvItemEspecial_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                carregarCampoSelecao();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Selecionar item");
            }

        }

        private void btnExlcuir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimentoExclusao())
                {
                    exluirRegistroAtual();

                    if (ultimoFiltro != null)
                    {
                        carregarItens(ultimoFiltro);
                    }
                    else
                    {
                        itemManipulacao = new ItemEspecial();
                        ajustarBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Excluir item");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
        }

        private void txtCodDpk_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        #endregion

        #region Métodos

        public override void AtualizarCodigoText(ModeloBusca modelo)
        {
            if (modelo.GetType() == typeof(Material))
            {
                this.txtCodDpk.Text = modelo.RetornarCodigoTexto();
                this.txtCodDpk.Select();
            }
        }

        private void buscarItemDescricao()
        {
            lblDescricaoItem.Text = String.Empty;

            if (itemManipulacao.CodigoDPK != 0)
            {
                var desc = ItemEspecialBLL.Instance().SelecionarEntidadeBusca(new Material() { CodigoDPK = itemManipulacao.CodigoDPK }).FirstOrDefault();

                if (desc != null)
                {
                    lblDescricaoItem.Text = desc.DescItem;
                }
            }
        }

        private void ajustarBind()
        {
            txtCodDpk.DataBindings.Clear();
            var bindingCodigoDPK = new Binding("Text", itemManipulacao, "CodigoDPK", true, DataSourceUpdateMode.OnPropertyChanged, null, "##################", null);
            bindingCodigoDPK.Parse += OnNullableTextBindingParsed;
            txtCodDpk.DataBindings.Add(bindingCodigoDPK);

            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            txtMargem.DataBindings.Clear();
            var bindingMargem = new Binding("Text", itemManipulacao, "Margem", true, DataSourceUpdateMode.OnPropertyChanged, null, "####.##", null);
            bindingMargem.Parse += OnNullableTextBindingParsed;
            txtMargem.DataBindings.Add(bindingMargem);

            txtDataVigencia.DataBindings.Clear();
            txtDataVigencia.DataBindings.Add("Text", itemManipulacao, "DataVigenciaStr");

        }

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private bool validarPreenchimento()
        {
            bool validacao = true;
            String mensagens = String.Empty;

            mensagens = "Ocorreram o(s) seguinte(s) erro(s):\n";

            if (itemManipulacao.CodigoLoja == 0)
            {
                validacao = false;
                mensagens += " - O Código Loja é obrigatório;\n";
            }

            if (itemManipulacao.CodigoDPK == 0)
            {
                validacao = false;
                mensagens += " - O Código DPK é obrigatório;\n";
            }
            else
            {
                if (ItemEspecialBLL.Instance().SelecionarEntidadeBusca(new Material() { CodigoDPK = itemManipulacao.CodigoDPK }).Count == 0)
                {
                    validacao = false;
                    mensagens += " - O material informado não está cadastrado, verifique o Código DPK;\n";
                }
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
                mensagens += " - A Margem é obrigatória;\n";
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
                mensagens += " - A Data Vigência é obrigatória;\n";
            }

            if (!validacao)
            {
                Mensagem(mensagens, "Validação de Informações");
            }

            return validacao;
        }

        public bool validarPreenchimentoExclusao()
        {
            bool validacao = true;

            if (itemManipulacao.CodigoLoja == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.CodigoDPK == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
            }

            if (!validacao)
            {
                Mensagem("Selecione uma margem no grid antes de excluí-la!", "Validação de Informações");
            }

            return validacao;
        }

        private void salvarRegistroAtual()
        {
            string msg;

            msg = (!procurarMargem()) ? "Confirma inclusão de uma nova vigência?"
                : "Deseja confirmar a atualização da vigência atual com a margem alterada?";

            if (MensagemConfirmacao(msg))
            {
                ItemEspecialBLL.Instance().Gravar(itemManipulacao);

                Mensagem("Sucesso ao realizar a gravação da margem!", "Salvar item especial");

                carregarItens(ultimoFiltro != null ? ultimoFiltro : itemManipulacao);

            }
        }

        private void carregarItens(ItemEspecial filtro)
        {
            var listaItens = ItemEspecialBLL.Instance().ListarTodos(filtro);

            if (listaItens.Count != 0)
            {
                btnExlcuir.Enabled = true;
            }
            else
            {
                btnExlcuir.Enabled = false;
                Mensagem("Esta pesquisa não retornou nenhum resultado!", "Consulta item especial");
            }

            dgvItemEspecial.DataSource = listaItens;

            ajustarTamanhoColuna();

            limparSelecao();
        }

        private void ajustarTamanhoColuna()
        {
            if (dgvItemEspecial.Columns.Count > 0)
            {
                dgvItemEspecial.Columns[0].Width = 78;
                dgvItemEspecial.Columns[1].Width = 90;
                dgvItemEspecial.Columns[3].Width = 180;
                dgvItemEspecial.Columns[4].Width = 70;
                dgvItemEspecial.Columns[5].Width = 100;
            }
        }

        private void limparSelecao()
        {
            dgvItemEspecial.ClearSelection();

            if (ultimoFiltro != null)
            {
                itemManipulacao = ultimoFiltro.ClonarTipado();
            }

            buscarItemDescricao();

            ajustarBind();
        }

        private void carregarCampoSelecao()
        {
             itemManipulacao = ((ItemEspecial)this.dgvItemEspecial.CurrentRow.DataBoundItem).ClonarTipado();
             buscarItemDescricao();
             ajustarBind();
        }

        private void exluirRegistroAtual()
        {
            if (MensagemConfirmacao("Confirma exclusão?"))
            {
                if (procurarMargem())
                {
                    ItemEspecialBLL.Instance().Excluir(itemManipulacao);

                    Mensagem("Sucesso ao remover uma margem!", "Remoção de margens");
                }
                else
                {
                    Mensagem("Margem não encontrada para exclusão!", "Remoção de margens");
                }
            }
        }

        private bool procurarMargem()
        {
            var itemOriginal =
                ItemEspecialBLL.Instance().ListarTodos(itemManipulacao).FirstOrDefault(i => i.CodigoDPK == itemManipulacao.CodigoDPK
                                                                                  && i.CodigoLoja == itemManipulacao.CodigoLoja
                                                                                  && i.DataVigencia == itemManipulacao.DataVigencia);
            if (itemOriginal == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion


    }
}

