﻿namespace VDA980.Controle
{
    partial class ucRelatorioTipologiaFornecedor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpRelatorio = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.cmbTipoCliente = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCgcFornecedor = new System.Windows.Forms.TextBox();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbpRelatorio.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbpRelatorio
            // 
            this.gbpRelatorio.Controls.Add(this.groupBox1);
            this.gbpRelatorio.Controls.Add(this.btnVisualizar);
            this.gbpRelatorio.Controls.Add(this.cmbTipoCliente);
            this.gbpRelatorio.Controls.Add(this.label2);
            this.gbpRelatorio.Controls.Add(this.txtCgcFornecedor);
            this.gbpRelatorio.Controls.Add(this.cmbLoja);
            this.gbpRelatorio.Controls.Add(this.lblCodLoja);
            this.gbpRelatorio.Controls.Add(this.label1);
            this.gbpRelatorio.Location = new System.Drawing.Point(4, 4);
            this.gbpRelatorio.Name = "gbpRelatorio";
            this.gbpRelatorio.Size = new System.Drawing.Size(744, 375);
            this.gbpRelatorio.TabIndex = 0;
            this.gbpRelatorio.TabStop = false;
            this.gbpRelatorio.Text = "Relatório - Tipologia/Fornecedor";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataInicio);
            this.groupBox1.Controls.Add(this.txtDataFim);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(21, 111);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 50);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Vigência";
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.Location = new System.Drawing.Point(21, 19);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(116, 20);
            this.txtDataInicio.TabIndex = 4;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            // 
            // txtDataFim
            // 
            this.txtDataFim.Location = new System.Drawing.Point(172, 19);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(116, 20);
            this.txtDataFim.TabIndex = 5;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(143, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Até";
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(21, 176);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(75, 23);
            this.btnVisualizar.TabIndex = 6;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // cmbTipoCliente
            // 
            this.cmbTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoCliente.FormattingEnabled = true;
            this.cmbTipoCliente.Location = new System.Drawing.Point(115, 56);
            this.cmbTipoCliente.Name = "cmbTipoCliente";
            this.cmbTipoCliente.Size = new System.Drawing.Size(150, 21);
            this.cmbTipoCliente.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "CNPJ Fornecedor";
            // 
            // txtCgcFornecedor
            // 
            this.txtCgcFornecedor.Location = new System.Drawing.Point(115, 85);
            this.txtCgcFornecedor.MaxLength = 14;
            this.txtCgcFornecedor.Name = "txtCgcFornecedor";
            this.txtCgcFornecedor.Size = new System.Drawing.Size(150, 20);
            this.txtCgcFornecedor.TabIndex = 3;
            this.txtCgcFornecedor.TextChanged += new System.EventHandler(this.txtCgcFornecedor_TextChanged);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(115, 27);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(150, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(18, 30);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 31;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Tipo Cliente";
            // 
            // ucRelatorioTipologiaFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbpRelatorio);
            this.Name = "ucRelatorioTipologiaFornecedor";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbpRelatorio.ResumeLayout(false);
            this.gbpRelatorio.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpRelatorio;
        private System.Windows.Forms.ComboBox cmbTipoCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCgcFornecedor;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
