﻿namespace VDA980.Controle
{
    partial class ucRelatorio
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controleTab = new System.Windows.Forms.TabControl();
            this.tabAnaliseMargem = new System.Windows.Forms.TabPage();
            this.tabParametroPadrao = new System.Windows.Forms.TabPage();
            this.tabItemEspecial = new System.Windows.Forms.TabPage();
            this.tabTipologiaFornecedor = new System.Windows.Forms.TabPage();
            this.tabClienteFornecedor = new System.Windows.Forms.TabPage();
            this.controleTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // controleTab
            // 
            this.controleTab.Controls.Add(this.tabAnaliseMargem);
            this.controleTab.Controls.Add(this.tabParametroPadrao);
            this.controleTab.Controls.Add(this.tabItemEspecial);
            this.controleTab.Controls.Add(this.tabTipologiaFornecedor);
            this.controleTab.Controls.Add(this.tabClienteFornecedor);
            this.controleTab.Location = new System.Drawing.Point(0, 0);
            this.controleTab.Name = "controleTab";
            this.controleTab.SelectedIndex = 0;
            this.controleTab.Size = new System.Drawing.Size(773, 424);
            this.controleTab.TabIndex = 0;
            // 
            // tabAnaliseMargem
            // 
            this.tabAnaliseMargem.Location = new System.Drawing.Point(4, 22);
            this.tabAnaliseMargem.Name = "tabAnaliseMargem";
            this.tabAnaliseMargem.Size = new System.Drawing.Size(765, 398);
            this.tabAnaliseMargem.TabIndex = 0;
            this.tabAnaliseMargem.Text = "Análise de Margem Pedido";
            this.tabAnaliseMargem.UseVisualStyleBackColor = true;
            // 
            // tabParametroPadrao
            // 
            this.tabParametroPadrao.Location = new System.Drawing.Point(4, 22);
            this.tabParametroPadrao.Name = "tabParametroPadrao";
            this.tabParametroPadrao.Size = new System.Drawing.Size(765, 398);
            this.tabParametroPadrao.TabIndex = 4;
            this.tabParametroPadrao.Text = "Parâmetros Padrão";
            this.tabParametroPadrao.UseVisualStyleBackColor = true;
            // 
            // tabItemEspecial
            // 
            this.tabItemEspecial.Location = new System.Drawing.Point(4, 22);
            this.tabItemEspecial.Name = "tabItemEspecial";
            this.tabItemEspecial.Size = new System.Drawing.Size(765, 398);
            this.tabItemEspecial.TabIndex = 1;
            this.tabItemEspecial.Text = "Item Especial";
            this.tabItemEspecial.UseVisualStyleBackColor = true;
            // 
            // tabTipologiaFornecedor
            // 
            this.tabTipologiaFornecedor.Location = new System.Drawing.Point(4, 22);
            this.tabTipologiaFornecedor.Name = "tabTipologiaFornecedor";
            this.tabTipologiaFornecedor.Size = new System.Drawing.Size(765, 398);
            this.tabTipologiaFornecedor.TabIndex = 2;
            this.tabTipologiaFornecedor.Text = "Tipologia/Fornecedor";
            this.tabTipologiaFornecedor.UseVisualStyleBackColor = true;
            // 
            // tabClienteFornecedor
            // 
            this.tabClienteFornecedor.Location = new System.Drawing.Point(4, 22);
            this.tabClienteFornecedor.Name = "tabClienteFornecedor";
            this.tabClienteFornecedor.Size = new System.Drawing.Size(765, 398);
            this.tabClienteFornecedor.TabIndex = 3;
            this.tabClienteFornecedor.Text = "Cliente/Fornecedor";
            this.tabClienteFornecedor.UseVisualStyleBackColor = true;
            // 
            // ucRelatorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.controleTab);
            this.Name = "ucRelatorio";
            this.Size = new System.Drawing.Size(779, 431);
            this.Load += new System.EventHandler(this.ucRelatorio_Load);
            this.controleTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl controleTab;
        private System.Windows.Forms.TabPage tabAnaliseMargem;
        private System.Windows.Forms.TabPage tabItemEspecial;
        private System.Windows.Forms.TabPage tabTipologiaFornecedor;
        private System.Windows.Forms.TabPage tabClienteFornecedor;
        private System.Windows.Forms.TabPage tabParametroPadrao;

    }
}
