﻿using System;
using System.Linq;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;

namespace VDA980.Controle
{
    public partial class ucTipologiaFornecedor : ucBase
    {
        #region Propriedades

        private TipologiaFornecedor itemManipulacao = new TipologiaFornecedor();
        private TipologiaFornecedor ultimoFiltro = null;

        #endregion

        #region Construtor

        public ucTipologiaFornecedor()
        {
            try
            {
                InitializeComponent();

                carregarLoja();

                carregarTipoCliente();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void txtCgcFornecedor_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void btnPesquisaFornecedor_Click(object sender, EventArgs e)
        {
            var busca = new frmBuscaZoom(this, typeof(TipologiaFornecedorBLL));

            busca.ShowDialog(this);
        }

        private void txtCgcFornecedor_Leave(object sender, EventArgs e)
        {
            buscarFornecedorNome();
        }

        private void txtMargem_TextChanged(object sender, EventArgs e)
        {
            tratarCampoDecimal(sender);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                ultimoFiltro = itemManipulacao.ClonarTipado();

                carregarItens(itemManipulacao);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Consultar itens");
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimento())
                {
                    salvarRegistroAtual();
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Salvar item");
            }
        }

        private void dgvTipologiaFornecedor_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                carregarCampoSelecao();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Selecionar item");
            }
        }

        private void btnExlcuir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimentoExclusao())
                {
                    exluirRegistroAtual();

                    if (ultimoFiltro != null)
                    {
                        carregarItens(ultimoFiltro);
                    }
                    else
                    {
                        itemManipulacao = new TipologiaFornecedor();
                        ajustarBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Excluir item");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
        }

        #endregion

        #region Métodos

        public override void AtualizarCodigoText(ModeloBusca modelo)
        {
            if (modelo.GetType() == typeof(Fornecedor))
            {
                this.txtCgcFornecedor.Text = modelo.RetornarCodigoTexto();
                this.txtCgcFornecedor.Select();
            }
        }

        private void buscarFornecedorNome()
        {
            lblNomeFornecedor.Text = String.Empty;

            if (itemManipulacao.CGCFornecedor != 0)
            {
                var desc = TipologiaFornecedorBLL.Instance().SelecionarEntidadeBusca(new Fornecedor() { CNPJFornecedor = itemManipulacao.CGCFornecedor }).FirstOrDefault();

                if (desc != null)
                {
                    lblNomeFornecedor.Text = desc.NomeFornecedor;
                }
            }
        }

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            cmbTipoCliente.DataBindings.Clear();
            cmbTipoCliente.DataBindings.Add("SelectedValue", itemManipulacao, "CodTipoCliente");

            txtCgcFornecedor.DataBindings.Clear();
            var bindingCGCFornecedor = new Binding("Text", itemManipulacao, "CGCFornecedor", true, DataSourceUpdateMode.OnPropertyChanged, null, "##############", null);
            bindingCGCFornecedor.Parse += OnNullableTextBindingParsed;
            txtCgcFornecedor.DataBindings.Add(bindingCGCFornecedor);

            txtMargem.DataBindings.Clear();
            var bindingMargem = new Binding("Text", itemManipulacao, "Margem", true, DataSourceUpdateMode.OnPropertyChanged, null, "####.##", null);
            bindingMargem.Parse += OnNullableTextBindingParsed;
            txtMargem.DataBindings.Add(bindingMargem);

            txtDataVigencia.DataBindings.Clear();
            txtDataVigencia.DataBindings.Add("Text", itemManipulacao, "DataVigenciaStr");
        }

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private void carregarTipoCliente()
        {
            var listaTipoCliente = TipologiaFornecedorBLL.Instance().ListarTipoCliente();
            cmbTipoCliente.DataSource = listaTipoCliente;
            cmbTipoCliente.DisplayMember = "NomeExibicao";
            cmbTipoCliente.ValueMember = "CodTipoCliente";
        }

        private void carregarItens(TipologiaFornecedor filtro)
        {
            var listaItem = TipologiaFornecedorBLL.Instance().ListarTodos(filtro);

            if (listaItem.Count != 0)
            {
                btnExlcuir.Enabled = true;
            }
            else
            {
                btnExlcuir.Enabled = false;
                Mensagem("Esta pesquisa não retornou nenhum resultado!", "Consulta Tipologia Fornecedor");
            }

            dgvTipologiaFornecedor.DataSource = listaItem;

            ajustarTamanhoColuna();

            limparSelecao();
        }

        private void limparSelecao()
        {
            dgvTipologiaFornecedor.ClearSelection();

            if (ultimoFiltro != null)
            {
                itemManipulacao = ultimoFiltro.ClonarTipado();
            }

            buscarFornecedorNome();

            ajustarBind();
        }

        private void ajustarTamanhoColuna()
        {
            if (dgvTipologiaFornecedor.Columns.Count > 0)
            {
                dgvTipologiaFornecedor.Columns[0].Width = 90;
                dgvTipologiaFornecedor.Columns[3].Width = 210;
            }
        }

        private void carregarCampoSelecao()
        {
            itemManipulacao = ((TipologiaFornecedor)this.dgvTipologiaFornecedor.CurrentRow.DataBoundItem).ClonarTipado();
            buscarFornecedorNome();
            ajustarBind();
        }

        private void salvarRegistroAtual()
        {
            string msg;

            msg = (!procurarMargem()) ? "Confirma inclusão de uma nova vigência?"
                : "Deseja confirmar a atualização da vigência atual com a margem alterada?";

            if (MensagemConfirmacao(msg))
            {
                TipologiaFornecedorBLL.Instance().Gravar(itemManipulacao);

                Mensagem("Sucesso ao realizar a gravação da margem!", "Salvar tipologia fornecedor");

                carregarItens(ultimoFiltro != null ? ultimoFiltro : itemManipulacao);

            }
        }

        private bool validarPreenchimento()
        {
            bool validacao = true;

            String mensagens = String.Empty;

            mensagens = "Ocorreram o(s) seguinte(s) erro(s):\n";

            if (itemManipulacao.CGCFornecedor == 0 && itemManipulacao.CodTipoCliente == "0")
            {
                validacao = false;
                mensagens += " - O Tipo Cliente ou o CNPJ Fornecedor é obrigatório;\n";
            }

            if (itemManipulacao.CGCFornecedor != 0)
            {
                if (TipologiaFornecedorBLL.Instance().SelecionarEntidadeBusca(new Fornecedor() { CNPJFornecedor = itemManipulacao.CGCFornecedor }).Count == 0)
                {
                    validacao = false;
                    mensagens += " - O fornecedor informado não está cadastrado, verifique o CPNJ/CPF;\n";
                }
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
                mensagens += " - A Margem é obrigatória;\n";
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
                mensagens += " - A Data Vigência é obrigatória;\n";
            }

            if (!validacao)
            {
                Mensagem(mensagens, "Validação de Informações");
            }

            return validacao;
        }

        public bool validarPreenchimentoExclusao()
        {
            bool validacao = true;

            if (itemManipulacao.CGCFornecedor == 0 && itemManipulacao.CodTipoCliente == "0")
            {
                validacao = false;
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
            }

            if (!validacao)
            {
                Mensagem("Selecione uma margem no grid antes de excluí-la!", "Validação de Informações");
            }

            return validacao;
        }

        private void exluirRegistroAtual()
        {
            if (MensagemConfirmacao("Confirma exclusão?"))
            {
                if (procurarMargem())
                {
                    TipologiaFornecedorBLL.Instance().Excluir(itemManipulacao);

                    Mensagem("Sucesso ao remover uma margem!", "Remoção de margens");
                }
                else
                {
                    Mensagem("Margem não encontrada para exclusão!", "Remoção de margens");
                }
            }
        }

        private bool procurarMargem()
        {
            var listaItem = TipologiaFornecedorBLL.Instance().ListarTodos(itemManipulacao);
            var itemOriginal = listaItem.FirstOrDefault(i => i.CGCFornecedor == itemManipulacao.CGCFornecedor
                                                                              && i.CodigoLoja == itemManipulacao.CodigoLoja
                                                                              && i.CodTipoCliente == itemManipulacao.CodTipoCliente
                                                                              && i.DataVigencia == itemManipulacao.DataVigencia);
            if (itemOriginal == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

    }
}
