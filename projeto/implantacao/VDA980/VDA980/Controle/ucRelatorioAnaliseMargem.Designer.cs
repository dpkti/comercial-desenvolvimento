﻿namespace VDA980.Controle
{
    partial class ucRelatorioAnaliseMargem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpRelatorio = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.lblAte = new System.Windows.Forms.Label();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.gbpStatus = new System.Windows.Forms.GroupBox();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.chkBloqueado = new System.Windows.Forms.CheckBox();
            this.lblTipoAnalise = new System.Windows.Forms.Label();
            this.cmbTipoAnalise = new System.Windows.Forms.ComboBox();
            this.lblNumeroPedido = new System.Windows.Forms.Label();
            this.txtNumeroPedido = new System.Windows.Forms.TextBox();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.gbpRelatorio.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbpStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbpRelatorio
            // 
            this.gbpRelatorio.Controls.Add(this.groupBox1);
            this.gbpRelatorio.Controls.Add(this.gbpStatus);
            this.gbpRelatorio.Controls.Add(this.lblTipoAnalise);
            this.gbpRelatorio.Controls.Add(this.cmbTipoAnalise);
            this.gbpRelatorio.Controls.Add(this.lblNumeroPedido);
            this.gbpRelatorio.Controls.Add(this.txtNumeroPedido);
            this.gbpRelatorio.Controls.Add(this.btnVisualizar);
            this.gbpRelatorio.Controls.Add(this.cmbLoja);
            this.gbpRelatorio.Controls.Add(this.lblCodLoja);
            this.gbpRelatorio.Location = new System.Drawing.Point(4, 4);
            this.gbpRelatorio.Name = "gbpRelatorio";
            this.gbpRelatorio.Size = new System.Drawing.Size(744, 375);
            this.gbpRelatorio.TabIndex = 15;
            this.gbpRelatorio.TabStop = false;
            this.gbpRelatorio.Text = "Relatório - Análise de Margem Pedido";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataInicio);
            this.groupBox1.Controls.Add(this.lblAte);
            this.groupBox1.Controls.Add(this.txtDataFim);
            this.groupBox1.Location = new System.Drawing.Point(17, 181);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 50);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Pedido";
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.Location = new System.Drawing.Point(24, 19);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(111, 20);
            this.txtDataInicio.TabIndex = 6;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            // 
            // lblAte
            // 
            this.lblAte.AutoSize = true;
            this.lblAte.Location = new System.Drawing.Point(142, 22);
            this.lblAte.Name = "lblAte";
            this.lblAte.Size = new System.Drawing.Size(23, 13);
            this.lblAte.TabIndex = 45;
            this.lblAte.Text = "Até";
            // 
            // txtDataFim
            // 
            this.txtDataFim.Location = new System.Drawing.Point(171, 19);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(111, 20);
            this.txtDataFim.TabIndex = 7;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            // 
            // gbpStatus
            // 
            this.gbpStatus.Controls.Add(this.chkLiberado);
            this.gbpStatus.Controls.Add(this.chkBloqueado);
            this.gbpStatus.Location = new System.Drawing.Point(17, 114);
            this.gbpStatus.Name = "gbpStatus";
            this.gbpStatus.Size = new System.Drawing.Size(241, 50);
            this.gbpStatus.TabIndex = 34;
            this.gbpStatus.TabStop = false;
            this.gbpStatus.Text = "Status Margem/Pedido";
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.Location = new System.Drawing.Point(148, 21);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(68, 17);
            this.chkLiberado.TabIndex = 5;
            this.chkLiberado.Text = "Atendida";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // chkBloqueado
            // 
            this.chkBloqueado.AutoSize = true;
            this.chkBloqueado.Location = new System.Drawing.Point(30, 21);
            this.chkBloqueado.Name = "chkBloqueado";
            this.chkBloqueado.Size = new System.Drawing.Size(91, 17);
            this.chkBloqueado.TabIndex = 4;
            this.chkBloqueado.Text = "Não Atendida";
            this.chkBloqueado.UseVisualStyleBackColor = true;
            // 
            // lblTipoAnalise
            // 
            this.lblTipoAnalise.AutoSize = true;
            this.lblTipoAnalise.Location = new System.Drawing.Point(14, 59);
            this.lblTipoAnalise.Name = "lblTipoAnalise";
            this.lblTipoAnalise.Size = new System.Drawing.Size(80, 13);
            this.lblTipoAnalise.TabIndex = 33;
            this.lblTipoAnalise.Text = "Tipo de Análise";
            // 
            // cmbTipoAnalise
            // 
            this.cmbTipoAnalise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoAnalise.FormattingEnabled = true;
            this.cmbTipoAnalise.Location = new System.Drawing.Point(114, 51);
            this.cmbTipoAnalise.Name = "cmbTipoAnalise";
            this.cmbTipoAnalise.Size = new System.Drawing.Size(144, 21);
            this.cmbTipoAnalise.TabIndex = 2;
            // 
            // lblNumeroPedido
            // 
            this.lblNumeroPedido.AutoSize = true;
            this.lblNumeroPedido.Location = new System.Drawing.Point(14, 86);
            this.lblNumeroPedido.Name = "lblNumeroPedido";
            this.lblNumeroPedido.Size = new System.Drawing.Size(95, 13);
            this.lblNumeroPedido.TabIndex = 31;
            this.lblNumeroPedido.Text = "Número do Pedido";
            // 
            // txtNumeroPedido
            // 
            this.txtNumeroPedido.Location = new System.Drawing.Point(114, 79);
            this.txtNumeroPedido.MaxLength = 7;
            this.txtNumeroPedido.Name = "txtNumeroPedido";
            this.txtNumeroPedido.Size = new System.Drawing.Size(144, 20);
            this.txtNumeroPedido.TabIndex = 3;
            this.txtNumeroPedido.TextChanged += new System.EventHandler(this.txtNumeroPedido_TextChanged);
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(17, 251);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(75, 23);
            this.btnVisualizar.TabIndex = 8;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(114, 23);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(144, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(14, 31);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 24;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // ucRelatorioAnaliseMargem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbpRelatorio);
            this.Name = "ucRelatorioAnaliseMargem";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbpRelatorio.ResumeLayout(false);
            this.gbpRelatorio.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbpStatus.ResumeLayout(false);
            this.gbpStatus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpRelatorio;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
        private System.Windows.Forms.Label lblNumeroPedido;
        private System.Windows.Forms.TextBox txtNumeroPedido;
        private System.Windows.Forms.GroupBox gbpStatus;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.CheckBox chkBloqueado;
        private System.Windows.Forms.Label lblTipoAnalise;
        private System.Windows.Forms.ComboBox cmbTipoAnalise;
        private System.Windows.Forms.Label lblAte;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
