﻿namespace VDA980.Controle
{
    partial class ucItemEspecial
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodDpk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.lblMargem = new System.Windows.Forms.Label();
            this.lblDataVigencia = new System.Windows.Forms.Label();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.txtDataVigencia = new System.Windows.Forms.MaskedTextBox();
            this.dgvItemEspecial = new System.Windows.Forms.DataGridView();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.btnExlcuir = new System.Windows.Forms.Button();
            this.btnPesquisaMaterial = new System.Windows.Forms.Button();
            this.lblDescricaoItem = new System.Windows.Forms.Label();
            this.gpbInformacao = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemEspecial)).BeginInit();
            this.gpbInformacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCodDpk
            // 
            this.txtCodDpk.Location = new System.Drawing.Point(98, 53);
            this.txtCodDpk.MaxLength = 18;
            this.txtCodDpk.Name = "txtCodDpk";
            this.txtCodDpk.Size = new System.Drawing.Size(126, 20);
            this.txtCodDpk.TabIndex = 2;
            this.txtCodDpk.TextChanged += new System.EventHandler(this.txtCodDpk_TextChanged);
            this.txtCodDpk.Leave += new System.EventHandler(this.txtCodDpk_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Código DPK";
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(18, 30);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 3;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // txtMargem
            // 
            this.txtMargem.Location = new System.Drawing.Point(98, 80);
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(126, 20);
            this.txtMargem.TabIndex = 3;
            this.txtMargem.TextChanged += new System.EventHandler(this.txtMargem_TextChanged);
            // 
            // lblMargem
            // 
            this.lblMargem.AutoSize = true;
            this.lblMargem.Location = new System.Drawing.Point(18, 83);
            this.lblMargem.Name = "lblMargem";
            this.lblMargem.Size = new System.Drawing.Size(62, 13);
            this.lblMargem.TabIndex = 5;
            this.lblMargem.Text = "Margem (%)";
            // 
            // lblDataVigencia
            // 
            this.lblDataVigencia.AutoSize = true;
            this.lblDataVigencia.Location = new System.Drawing.Point(18, 113);
            this.lblDataVigencia.Name = "lblDataVigencia";
            this.lblDataVigencia.Size = new System.Drawing.Size(74, 13);
            this.lblDataVigencia.TabIndex = 7;
            this.lblDataVigencia.Text = "Data Vigência";
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(112, 141);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 6;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // txtDataVigencia
            // 
            this.txtDataVigencia.Location = new System.Drawing.Point(98, 106);
            this.txtDataVigencia.Mask = "00/00/0000";
            this.txtDataVigencia.Name = "txtDataVigencia";
            this.txtDataVigencia.Size = new System.Drawing.Size(126, 20);
            this.txtDataVigencia.TabIndex = 4;
            this.txtDataVigencia.ValidatingType = typeof(System.DateTime);
            // 
            // dgvItemEspecial
            // 
            this.dgvItemEspecial.AllowUserToAddRows = false;
            this.dgvItemEspecial.AllowUserToDeleteRows = false;
            this.dgvItemEspecial.AllowUserToResizeColumns = false;
            this.dgvItemEspecial.AllowUserToResizeRows = false;
            this.dgvItemEspecial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemEspecial.Location = new System.Drawing.Point(21, 176);
            this.dgvItemEspecial.MultiSelect = false;
            this.dgvItemEspecial.Name = "dgvItemEspecial";
            this.dgvItemEspecial.ReadOnly = true;
            this.dgvItemEspecial.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItemEspecial.Size = new System.Drawing.Size(704, 184);
            this.dgvItemEspecial.TabIndex = 10;
            this.dgvItemEspecial.SelectionChanged += new System.EventHandler(this.dgvItemEspecial_SelectionChanged);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(21, 141);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(85, 23);
            this.btnConsultar.TabIndex = 5;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(98, 26);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(126, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // btnExlcuir
            // 
            this.btnExlcuir.Enabled = false;
            this.btnExlcuir.Location = new System.Drawing.Point(194, 141);
            this.btnExlcuir.Name = "btnExlcuir";
            this.btnExlcuir.Size = new System.Drawing.Size(75, 23);
            this.btnExlcuir.TabIndex = 11;
            this.btnExlcuir.Text = "&Excluir";
            this.btnExlcuir.UseVisualStyleBackColor = true;
            this.btnExlcuir.Click += new System.EventHandler(this.btnExlcuir_Click);
            // 
            // btnPesquisaMaterial
            // 
            this.btnPesquisaMaterial.BackgroundImage = global::VDA980.Properties.Resources.lupa;
            this.btnPesquisaMaterial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPesquisaMaterial.FlatAppearance.BorderSize = 0;
            this.btnPesquisaMaterial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisaMaterial.Location = new System.Drawing.Point(234, 53);
            this.btnPesquisaMaterial.Name = "btnPesquisaMaterial";
            this.btnPesquisaMaterial.Size = new System.Drawing.Size(20, 20);
            this.btnPesquisaMaterial.TabIndex = 12;
            this.btnPesquisaMaterial.UseVisualStyleBackColor = true;
            this.btnPesquisaMaterial.Click += new System.EventHandler(this.btnPesquisaMaterial_Click);
            // 
            // lblDescricaoItem
            // 
            this.lblDescricaoItem.AutoSize = true;
            this.lblDescricaoItem.Location = new System.Drawing.Point(260, 60);
            this.lblDescricaoItem.Name = "lblDescricaoItem";
            this.lblDescricaoItem.Size = new System.Drawing.Size(0, 13);
            this.lblDescricaoItem.TabIndex = 13;
            // 
            // gpbInformacao
            // 
            this.gpbInformacao.Controls.Add(this.lblDescricaoItem);
            this.gpbInformacao.Controls.Add(this.btnPesquisaMaterial);
            this.gpbInformacao.Controls.Add(this.btnExlcuir);
            this.gpbInformacao.Controls.Add(this.cmbLoja);
            this.gpbInformacao.Controls.Add(this.btnConsultar);
            this.gpbInformacao.Controls.Add(this.dgvItemEspecial);
            this.gpbInformacao.Controls.Add(this.txtDataVigencia);
            this.gpbInformacao.Controls.Add(this.btnSalvar);
            this.gpbInformacao.Controls.Add(this.lblDataVigencia);
            this.gpbInformacao.Controls.Add(this.lblMargem);
            this.gpbInformacao.Controls.Add(this.txtMargem);
            this.gpbInformacao.Controls.Add(this.lblCodLoja);
            this.gpbInformacao.Controls.Add(this.label1);
            this.gpbInformacao.Controls.Add(this.txtCodDpk);
            this.gpbInformacao.Location = new System.Drawing.Point(3, 3);
            this.gpbInformacao.Name = "gpbInformacao";
            this.gpbInformacao.Size = new System.Drawing.Size(744, 379);
            this.gpbInformacao.TabIndex = 0;
            this.gpbInformacao.TabStop = false;
            this.gpbInformacao.Text = "Cadastro de Margem - Item Especial";
            // 
            // ucItemEspecial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbInformacao);
            this.Name = "ucItemEspecial";
            this.Size = new System.Drawing.Size(750, 382);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemEspecial)).EndInit();
            this.gpbInformacao.ResumeLayout(false);
            this.gpbInformacao.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtCodDpk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.Label lblMargem;
        private System.Windows.Forms.Label lblDataVigencia;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.MaskedTextBox txtDataVigencia;
        private System.Windows.Forms.DataGridView dgvItemEspecial;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Button btnExlcuir;
        private System.Windows.Forms.Button btnPesquisaMaterial;
        private System.Windows.Forms.Label lblDescricaoItem;
        private System.Windows.Forms.GroupBox gpbInformacao;


    }
}
