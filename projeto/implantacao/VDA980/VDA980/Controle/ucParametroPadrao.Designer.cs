﻿namespace VDA980.Controle
{
    partial class ucParametroPadrao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbInformacao = new System.Windows.Forms.GroupBox();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dgvParametroPadrao = new System.Windows.Forms.DataGridView();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.gpbPercentual = new System.Windows.Forms.GroupBox();
            this.txtDataVigencia = new System.Windows.Forms.MaskedTextBox();
            this.lblDataVigencia = new System.Windows.Forms.Label();
            this.lblMargemPedido = new System.Windows.Forms.Label();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.gpbTipoAnalise = new System.Windows.Forms.GroupBox();
            this.rdbItem = new System.Windows.Forms.RadioButton();
            this.rdbPedido = new System.Windows.Forms.RadioButton();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.gpbInformacao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParametroPadrao)).BeginInit();
            this.gpbPercentual.SuspendLayout();
            this.gpbTipoAnalise.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpbInformacao
            // 
            this.gpbInformacao.Controls.Add(this.btnConsultar);
            this.gpbInformacao.Controls.Add(this.dgvParametroPadrao);
            this.gpbInformacao.Controls.Add(this.btnExcluir);
            this.gpbInformacao.Controls.Add(this.btnSalvar);
            this.gpbInformacao.Controls.Add(this.gpbPercentual);
            this.gpbInformacao.Controls.Add(this.gpbTipoAnalise);
            this.gpbInformacao.Controls.Add(this.cmbLoja);
            this.gpbInformacao.Controls.Add(this.lblCodLoja);
            this.gpbInformacao.Location = new System.Drawing.Point(3, 3);
            this.gpbInformacao.Name = "gpbInformacao";
            this.gpbInformacao.Size = new System.Drawing.Size(744, 379);
            this.gpbInformacao.TabIndex = 0;
            this.gpbInformacao.TabStop = false;
            this.gpbInformacao.Text = "Cadastro de Margem - Parâmetros Padrão";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(252, 23);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(85, 23);
            this.btnConsultar.TabIndex = 15;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dgvParametroPadrao
            // 
            this.dgvParametroPadrao.AllowUserToAddRows = false;
            this.dgvParametroPadrao.AllowUserToDeleteRows = false;
            this.dgvParametroPadrao.AllowUserToResizeColumns = false;
            this.dgvParametroPadrao.AllowUserToResizeRows = false;
            this.dgvParametroPadrao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvParametroPadrao.Location = new System.Drawing.Point(20, 163);
            this.dgvParametroPadrao.MultiSelect = false;
            this.dgvParametroPadrao.Name = "dgvParametroPadrao";
            this.dgvParametroPadrao.ReadOnly = true;
            this.dgvParametroPadrao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvParametroPadrao.Size = new System.Drawing.Size(658, 198);
            this.dgvParametroPadrao.TabIndex = 14;
            this.dgvParametroPadrao.SelectionChanged += new System.EventHandler(this.dgvParametroPadrao_SelectionChanged);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(436, 114);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(73, 23);
            this.btnExcluir.TabIndex = 13;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(436, 85);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(73, 23);
            this.btnSalvar.TabIndex = 12;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // gpbPercentual
            // 
            this.gpbPercentual.Controls.Add(this.txtDataVigencia);
            this.gpbPercentual.Controls.Add(this.lblDataVigencia);
            this.gpbPercentual.Controls.Add(this.lblMargemPedido);
            this.gpbPercentual.Controls.Add(this.txtMargem);
            this.gpbPercentual.Location = new System.Drawing.Point(203, 60);
            this.gpbPercentual.Name = "gpbPercentual";
            this.gpbPercentual.Size = new System.Drawing.Size(218, 81);
            this.gpbPercentual.TabIndex = 11;
            this.gpbPercentual.TabStop = false;
            this.gpbPercentual.Text = "Percentual de Análise";
            // 
            // txtDataVigencia
            // 
            this.txtDataVigencia.Location = new System.Drawing.Point(101, 47);
            this.txtDataVigencia.Mask = "00/00/0000";
            this.txtDataVigencia.Name = "txtDataVigencia";
            this.txtDataVigencia.Size = new System.Drawing.Size(100, 20);
            this.txtDataVigencia.TabIndex = 18;
            this.txtDataVigencia.ValidatingType = typeof(System.DateTime);
            // 
            // lblDataVigencia
            // 
            this.lblDataVigencia.AutoSize = true;
            this.lblDataVigencia.Location = new System.Drawing.Point(11, 50);
            this.lblDataVigencia.Name = "lblDataVigencia";
            this.lblDataVigencia.Size = new System.Drawing.Size(74, 13);
            this.lblDataVigencia.TabIndex = 19;
            this.lblDataVigencia.Text = "Data Vigência";
            // 
            // lblMargemPedido
            // 
            this.lblMargemPedido.AutoSize = true;
            this.lblMargemPedido.Location = new System.Drawing.Point(11, 21);
            this.lblMargemPedido.Name = "lblMargemPedido";
            this.lblMargemPedido.Size = new System.Drawing.Size(62, 13);
            this.lblMargemPedido.TabIndex = 11;
            this.lblMargemPedido.Text = "Margem (%)";
            // 
            // txtMargem
            // 
            this.txtMargem.Location = new System.Drawing.Point(101, 19);
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(100, 20);
            this.txtMargem.TabIndex = 14;
            this.txtMargem.TextChanged += new System.EventHandler(this.txtMargemPedido_TextChanged);
            this.txtMargem.Leave += new System.EventHandler(this.txtMargem_Leave);
            // 
            // gpbTipoAnalise
            // 
            this.gpbTipoAnalise.Controls.Add(this.rdbItem);
            this.gpbTipoAnalise.Controls.Add(this.rdbPedido);
            this.gpbTipoAnalise.Location = new System.Drawing.Point(20, 60);
            this.gpbTipoAnalise.Name = "gpbTipoAnalise";
            this.gpbTipoAnalise.Size = new System.Drawing.Size(164, 81);
            this.gpbTipoAnalise.TabIndex = 6;
            this.gpbTipoAnalise.TabStop = false;
            this.gpbTipoAnalise.Text = "Tipo de Análise";
            // 
            // rdbItem
            // 
            this.rdbItem.AutoSize = true;
            this.rdbItem.Location = new System.Drawing.Point(18, 48);
            this.rdbItem.Name = "rdbItem";
            this.rdbItem.Size = new System.Drawing.Size(104, 17);
            this.rdbItem.TabIndex = 1;
            this.rdbItem.TabStop = true;
            this.rdbItem.Text = "Margem por Item";
            this.rdbItem.UseVisualStyleBackColor = true;
            this.rdbItem.CheckedChanged += new System.EventHandler(this.rdbItem_CheckedChanged);
            // 
            // rdbPedido
            // 
            this.rdbPedido.AutoSize = true;
            this.rdbPedido.Location = new System.Drawing.Point(18, 25);
            this.rdbPedido.Name = "rdbPedido";
            this.rdbPedido.Size = new System.Drawing.Size(117, 17);
            this.rdbPedido.TabIndex = 0;
            this.rdbPedido.TabStop = true;
            this.rdbPedido.Text = "Margem por Pedido";
            this.rdbPedido.UseVisualStyleBackColor = true;
            this.rdbPedido.CheckedChanged += new System.EventHandler(this.rdbPedido_CheckedChanged);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(97, 24);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(149, 21);
            this.cmbLoja.TabIndex = 4;
            this.cmbLoja.SelectedIndexChanged += new System.EventHandler(this.cmbLoja_SelectedIndexChanged);
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(17, 28);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 5;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // ucParametroPadrao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbInformacao);
            this.Name = "ucParametroPadrao";
            this.Size = new System.Drawing.Size(750, 382);
            this.gpbInformacao.ResumeLayout(false);
            this.gpbInformacao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvParametroPadrao)).EndInit();
            this.gpbPercentual.ResumeLayout(false);
            this.gpbPercentual.PerformLayout();
            this.gpbTipoAnalise.ResumeLayout(false);
            this.gpbTipoAnalise.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbInformacao;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.GroupBox gpbTipoAnalise;
        private System.Windows.Forms.RadioButton rdbItem;
        private System.Windows.Forms.RadioButton rdbPedido;
        private System.Windows.Forms.GroupBox gpbPercentual;
        private System.Windows.Forms.Label lblMargemPedido;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.MaskedTextBox txtDataVigencia;
        private System.Windows.Forms.Label lblDataVigencia;
        private System.Windows.Forms.DataGridView dgvParametroPadrao;
        private System.Windows.Forms.Button btnConsultar;
    }
}
