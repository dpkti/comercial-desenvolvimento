﻿namespace VDA980.Controle
{
    partial class ucRelatorioParametroPadrao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpRelatorio = new System.Windows.Forms.GroupBox();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkItem = new System.Windows.Forms.CheckBox();
            this.chkPedido = new System.Windows.Forms.CheckBox();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.gbpRelatorio.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbpRelatorio
            // 
            this.gbpRelatorio.Controls.Add(this.btnVisualizar);
            this.gbpRelatorio.Controls.Add(this.groupBox3);
            this.gbpRelatorio.Controls.Add(this.groupBox2);
            this.gbpRelatorio.Controls.Add(this.cmbLoja);
            this.gbpRelatorio.Controls.Add(this.lblCodLoja);
            this.gbpRelatorio.Location = new System.Drawing.Point(4, 4);
            this.gbpRelatorio.Name = "gbpRelatorio";
            this.gbpRelatorio.Size = new System.Drawing.Size(744, 375);
            this.gbpRelatorio.TabIndex = 0;
            this.gbpRelatorio.TabStop = false;
            this.gbpRelatorio.Text = "Relatório - Parâmetros Padrão";
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(20, 169);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(75, 23);
            this.btnVisualizar.TabIndex = 6;
            this.btnVisualizar.Text = "Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtDataInicio);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtDataFim);
            this.groupBox3.Location = new System.Drawing.Point(20, 107);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(307, 50);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data Vigência";
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.Location = new System.Drawing.Point(21, 19);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(116, 20);
            this.txtDataInicio.TabIndex = 4;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(143, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 50;
            this.label3.Text = "Até";
            // 
            // txtDataFim
            // 
            this.txtDataFim.Location = new System.Drawing.Point(171, 19);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(116, 20);
            this.txtDataFim.TabIndex = 5;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkItem);
            this.groupBox2.Controls.Add(this.chkPedido);
            this.groupBox2.Location = new System.Drawing.Point(20, 51);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(307, 50);
            this.groupBox2.TabIndex = 53;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de Análise";
            // 
            // chkItem
            // 
            this.chkItem.AutoSize = true;
            this.chkItem.Location = new System.Drawing.Point(167, 22);
            this.chkItem.Name = "chkItem";
            this.chkItem.Size = new System.Drawing.Size(105, 17);
            this.chkItem.TabIndex = 3;
            this.chkItem.Text = "Margem por Item";
            this.chkItem.UseVisualStyleBackColor = true;
            // 
            // chkPedido
            // 
            this.chkPedido.AutoSize = true;
            this.chkPedido.Location = new System.Drawing.Point(24, 22);
            this.chkPedido.Name = "chkPedido";
            this.chkPedido.Size = new System.Drawing.Size(115, 17);
            this.chkPedido.TabIndex = 2;
            this.chkPedido.Text = "Magem por Pedido";
            this.chkPedido.UseVisualStyleBackColor = true;
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(101, 22);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(116, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(17, 25);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 47;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // ucRelatorioParametroPadrao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbpRelatorio);
            this.Name = "ucRelatorioParametroPadrao";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbpRelatorio.ResumeLayout(false);
            this.gbpRelatorio.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpRelatorio;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkItem;
        private System.Windows.Forms.CheckBox chkPedido;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnVisualizar;
    }
}
