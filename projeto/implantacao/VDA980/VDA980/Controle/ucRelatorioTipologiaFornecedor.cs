﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;

namespace VDA980.Controle
{
    public partial class ucRelatorioTipologiaFornecedor : ucBase
    {
        #region Propriedades

        private RelatorioTipologiaFornecedorFiltro itemManipulacao = new RelatorioTipologiaFornecedorFiltro();

        #endregion

        #region Construtor

        public ucRelatorioTipologiaFornecedor()
        {
            try
            {
                InitializeComponent();

                carregarLoja();

                carregarTipoCliente();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void txtCgcFornecedor_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            List<RelatorioTipologiaFornecedor> listaRetorno = new List<RelatorioTipologiaFornecedor>();
            Relatorio relatorio = new Relatorio();

            try
            {
                if (validarTela())
                {
                    listaRetorno = TipologiaFornecedorBLL.Instance().SelectByFilterRelatorio(itemManipulacao);

                    if (listaRetorno == null || listaRetorno.Count == 0)
                    {
                        Mensagem("Nenhum resultado foi encontrado.", "Relatório");
                    }
                    else
                    {
                        relatorio.Source = listaRetorno;
                        relatorio.NomeDataSet = "DataSetRelatorioTipologiaFornecedor";
                        relatorio.NomeRelatorio = "Relatório de Regras de Tipologia/Fornecedor";
                        relatorio.ReportEmbeddedResource = "VDA980.Relatorios.RelatorioTipologiaFornecedor.rdlc";

                        frmRelatorio frm = new frmRelatorio();
                        frm.itemManipulacao = relatorio;
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Relatório");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
            else if (e.Value.ToString().Equals("  /  /")) e.Value = null;
        }

        #endregion
        
        #region Métodos

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            cmbTipoCliente.DataBindings.Clear();
            cmbTipoCliente.DataBindings.Add("SelectedValue", itemManipulacao, "CodTipoCliente");

            txtCgcFornecedor.DataBindings.Clear();
            var bindingCgcFornecedor = new Binding("Text", itemManipulacao, "CGCFornecedor", true, DataSourceUpdateMode.OnPropertyChanged, null, "##############", null);
            bindingCgcFornecedor.Parse += OnNullableTextBindingParsed;
            txtCgcFornecedor.DataBindings.Add(bindingCgcFornecedor);

            txtDataInicio.DataBindings.Clear();
            var bindingDataInicio = new Binding("Text", itemManipulacao, "DataVigenciaIni", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataInicio.Parse += OnNullableTextBindingParsed;
            txtDataInicio.DataBindings.Add(bindingDataInicio);

            txtDataFim.DataBindings.Clear();
            var bindingDataFim = new Binding("Text", itemManipulacao, "DataVigenciaFim", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataFim.Parse += OnNullableTextBindingParsed;
            txtDataFim.DataBindings.Add(bindingDataFim);
        }

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private void carregarTipoCliente()
        {
            var listaTipoCliente = TipologiaFornecedorBLL.Instance().ListarTipoCliente();
            TipoCliente todosTipos = new TipoCliente();
            todosTipos.CodTipoCliente = "1";
            todosTipos.Descricao = "Todos";
            listaTipoCliente.Insert(1, todosTipos);
            cmbTipoCliente.DataSource = listaTipoCliente;
            cmbTipoCliente.DisplayMember = "NomeExibicao";
            cmbTipoCliente.ValueMember = "CodTipoCliente";
        }

        private bool validarTela()
        {
            bool validarTela = true;

            if ((itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim == null)
                    || (itemManipulacao.DataVigenciaIni == null && itemManipulacao.DataVigenciaFim != null))
            {
                validarTela = false;
                Mensagem("É necessário preencher o Intervalo Data Vigência.", "Validação de Informações");
            }
            if (itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim != null)
            {
                if (itemManipulacao.DataVigenciaIni > itemManipulacao.DataVigenciaFim)
                {
                    validarTela = false;
                    Mensagem("A Data Vigência Inicial deve ser menor que a Data Vigência Final.", "Validação de Informações");
                }
            }
            return validarTela;
        }

        #endregion


    }
}