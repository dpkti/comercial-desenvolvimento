﻿using System;

namespace VDA980.Controle
{
    public partial class ucRelatorio : ucBase
    {
        public ucRelatorio()
        {
            InitializeComponent();
        }

        private void ucRelatorio_Load(object sender, EventArgs e)
        {
            new ucRelatorioAnaliseMargem().Parent = controleTab.TabPages[0];
            new ucRelatorioParametroPadrao().Parent = controleTab.TabPages[1];
            new ucRelatorioItemEspecial().Parent = controleTab.TabPages[2];
            new ucRelatorioTipologiaFornecedor().Parent = controleTab.TabPages[3];
            new ucRelatorioClienteFornecedor().Parent = controleTab.TabPages[4];
            
        }
    }
}
