﻿namespace VDA980.Controle
{
    partial class ucRelatorioClienteFornecedor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpRelatorio = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.lblAte = new System.Windows.Forms.Label();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.txtCNPJCliente = new System.Windows.Forms.TextBox();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.lblFornecedor = new System.Windows.Forms.Label();
            this.txtCgcFornecedor = new System.Windows.Forms.TextBox();
            this.gbpRelatorio.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbpRelatorio
            // 
            this.gbpRelatorio.Controls.Add(this.groupBox1);
            this.gbpRelatorio.Controls.Add(this.lblCliente);
            this.gbpRelatorio.Controls.Add(this.txtCNPJCliente);
            this.gbpRelatorio.Controls.Add(this.btnVisualizar);
            this.gbpRelatorio.Controls.Add(this.lblFornecedor);
            this.gbpRelatorio.Controls.Add(this.txtCgcFornecedor);
            this.gbpRelatorio.Location = new System.Drawing.Point(4, 4);
            this.gbpRelatorio.Name = "gbpRelatorio";
            this.gbpRelatorio.Size = new System.Drawing.Size(744, 375);
            this.gbpRelatorio.TabIndex = 0;
            this.gbpRelatorio.TabStop = false;
            this.gbpRelatorio.Text = "Relatório - Cliente/Fornecedor";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataInicio);
            this.groupBox1.Controls.Add(this.lblAte);
            this.groupBox1.Controls.Add(this.txtDataFim);
            this.groupBox1.Location = new System.Drawing.Point(22, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 50);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Vigência";
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.Location = new System.Drawing.Point(19, 19);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(116, 20);
            this.txtDataInicio.TabIndex = 3;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            // 
            // lblAte
            // 
            this.lblAte.AutoSize = true;
            this.lblAte.Location = new System.Drawing.Point(141, 22);
            this.lblAte.Name = "lblAte";
            this.lblAte.Size = new System.Drawing.Size(23, 13);
            this.lblAte.TabIndex = 44;
            this.lblAte.Text = "Até";
            // 
            // txtDataFim
            // 
            this.txtDataFim.Location = new System.Drawing.Point(172, 19);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(116, 20);
            this.txtDataFim.TabIndex = 4;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Location = new System.Drawing.Point(19, 30);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(94, 13);
            this.lblCliente.TabIndex = 46;
            this.lblCliente.Text = "CPF/CNPJ Cliente";
            // 
            // txtCNPJCliente
            // 
            this.txtCNPJCliente.Location = new System.Drawing.Point(116, 27);
            this.txtCNPJCliente.MaxLength = 14;
            this.txtCNPJCliente.Name = "txtCNPJCliente";
            this.txtCNPJCliente.Size = new System.Drawing.Size(116, 20);
            this.txtCNPJCliente.TabIndex = 1;
            this.txtCNPJCliente.TextChanged += new System.EventHandler(this.txtCNPJCliente_TextChanged);
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(22, 145);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(75, 23);
            this.btnVisualizar.TabIndex = 5;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // lblFornecedor
            // 
            this.lblFornecedor.AutoSize = true;
            this.lblFornecedor.Location = new System.Drawing.Point(19, 56);
            this.lblFornecedor.Name = "lblFornecedor";
            this.lblFornecedor.Size = new System.Drawing.Size(91, 13);
            this.lblFornecedor.TabIndex = 42;
            this.lblFornecedor.Text = "CNPJ Fornecedor";
            // 
            // txtCgcFornecedor
            // 
            this.txtCgcFornecedor.Location = new System.Drawing.Point(116, 53);
            this.txtCgcFornecedor.MaxLength = 14;
            this.txtCgcFornecedor.Name = "txtCgcFornecedor";
            this.txtCgcFornecedor.Size = new System.Drawing.Size(116, 20);
            this.txtCgcFornecedor.TabIndex = 2;
            this.txtCgcFornecedor.TextChanged += new System.EventHandler(this.txtCgcFornecedor_TextChanged);
            // 
            // ucRelatorioClienteFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbpRelatorio);
            this.Name = "ucRelatorioClienteFornecedor";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbpRelatorio.ResumeLayout(false);
            this.gbpRelatorio.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpRelatorio;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.TextBox txtCNPJCliente;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.Label lblAte;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.Label lblFornecedor;
        private System.Windows.Forms.TextBox txtCgcFornecedor;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
