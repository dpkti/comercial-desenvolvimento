﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;

namespace VDA980.Controle
{
    public partial class ucRelatorioParametroPadrao : ucBase
    {
        #region Propriedades

        private RelatorioParametroPadraoFiltro itemManipulacao = new RelatorioParametroPadraoFiltro();

        #endregion

        #region Construtor

        public ucRelatorioParametroPadrao()
        {
            try
            {
                InitializeComponent();

                carregarLoja();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            List<RelatorioParametroPadrao> listaRetorno = new List<RelatorioParametroPadrao>();
            Relatorio relatorio = new Relatorio();

            try
            {
                if (validarTela())
                {
                    listaRetorno = ParametroPadraoBLL.Instance().SelectByFilterRelatorio(itemManipulacao);

                    if (listaRetorno == null || listaRetorno.Count == 0)
                    {
                        Mensagem("Nenhum resultado foi encontrado.", "Relatório");
                    }
                    else
                    {
                        relatorio.Source = listaRetorno;
                        relatorio.NomeDataSet = "DataSetRelatorioParametroPadrao";
                        relatorio.NomeRelatorio = "Relatório de Parâmetros Padrão";
                        relatorio.ReportEmbeddedResource = "VDA980.Relatorios.RelatorioParametroPadrao.rdlc";

                        RelatorioParametroPadrao margemAtual = new RelatorioParametroPadrao();
                        margemAtual = ParametroPadraoBLL.Instance().SelectByFilterMargemAtual(itemManipulacao);

                        KeyValuePair<string, object> parametroAnalise;
                        KeyValuePair<string, object> parametroMargem;
                        KeyValuePair<string, object> parametroData;
                        List<KeyValuePair<string, object>> listaParametros = new List<KeyValuePair<string, object>>();
                        if (margemAtual != null)
                        {
                            parametroAnalise = new KeyValuePair<string, object>("TipoAnalise", margemAtual.NomeParametro);
                            parametroMargem = new KeyValuePair<string, object>("Margem", margemAtual.MargemDecimal.ToString("0.00"));
                            parametroData = new KeyValuePair<string, object>("DataVigencia", margemAtual.DataVigencia.ToString("dd/MM/yyyy"));
                        }
                        else
                        {
                            parametroAnalise = new KeyValuePair<string, object>("TipoAnalise", "N/A");
                            parametroMargem = new KeyValuePair<string, object>("Margem", "N/A");
                            parametroData = new KeyValuePair<string, object>("DataVigencia", "N/A");
                        }
                        listaParametros.Add(parametroAnalise);
                        listaParametros.Add(parametroMargem);
                        listaParametros.Add(parametroData);

                        relatorio.Parametros = listaParametros;

                        frmRelatorio frm = new frmRelatorio();
                        frm.itemManipulacao = relatorio;
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Relatório");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
            else if (e.Value.ToString().Equals("  /  /")) e.Value = null;
        }

        #endregion

        #region Métodos

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            chkPedido.DataBindings.Clear();
            var bindingParametroPedido = new Binding("Checked", itemManipulacao, "ParametroPedido", true, DataSourceUpdateMode.OnPropertyChanged);
            chkPedido.DataBindings.Add(bindingParametroPedido);

            chkItem.DataBindings.Clear();
            var bindingParametroItem = new Binding("Checked", itemManipulacao, "ParametroItem", true, DataSourceUpdateMode.OnPropertyChanged);
            chkItem.DataBindings.Add(bindingParametroItem);

            txtDataInicio.DataBindings.Clear();
            var bindingDataInicio = new Binding("Text", itemManipulacao, "DataVigenciaIni", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataInicio.Parse += OnNullableTextBindingParsed;
            txtDataInicio.DataBindings.Add(bindingDataInicio);

            txtDataFim.DataBindings.Clear();
            var bindingDataFim = new Binding("Text", itemManipulacao, "DataVigenciaFim", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataFim.Parse += OnNullableTextBindingParsed;
            txtDataFim.DataBindings.Add(bindingDataFim);
        }

        private bool validarTela()
        {
            bool validarTela = true;

            if ((itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim == null)
                    || (itemManipulacao.DataVigenciaIni == null && itemManipulacao.DataVigenciaFim != null))
            {
                validarTela = false;
                Mensagem("É necessário preencher o Intervalo Data Vigência.", "Validação de Informações");
            }
            if (itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim != null)
            {
                if (itemManipulacao.DataVigenciaIni > itemManipulacao.DataVigenciaFim)
                {
                    validarTela = false;
                    Mensagem("A Data Vigência Inicial deve ser menor que a Data Vigência Final.", "Validação de Informações");
                }
            }
            if (!itemManipulacao.ParametroItem && !itemManipulacao.ParametroPedido)
            {
                validarTela = false;
                Mensagem("É necessário escolher pelo menos um tipo de análise.", "Validação de Informações");
            }
            return validarTela;
        }

        #endregion
    }
}
