﻿using System;
using System.Linq;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;

namespace VDA980.Controle
{
    public partial class ucClienteFornecedor : ucBase
    {
        #region Propriedades

        private ClienteFornecedor itemManipulacao = new ClienteFornecedor();
        private ClienteFornecedor ultimoFiltro = null;

        #endregion

        #region Construtor

        public ucClienteFornecedor()
        {
            try
            {
                InitializeComponent();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void btnPesquisaCliente_Click(object sender, EventArgs e)
        {
            var busca = new frmBuscaZoom(this, typeof(ClienteFornecedorBLL));

            busca.ShowDialog(this);
        }

        private void btnPesquisaFornecedor_Click(object sender, EventArgs e)
        {
            var busca = new frmBuscaZoom(this, typeof(TipologiaFornecedorBLL));

            busca.ShowDialog(this);
        }

        private void txtCnpjCliente_Leave(object sender, EventArgs e)
        {
            buscarClienteNome();
        }

        private void txtCnpjFornecedor_Leave(object sender, EventArgs e)
        {
            buscarFornecedorNome();
        }

        private void txtCnpjCliente_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void txtCnpjFornecedor_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void txtMargem_TextChanged(object sender, EventArgs e)
        {
            tratarCampoDecimal(sender);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                ultimoFiltro = itemManipulacao.ClonarTipado();

                carregarItens(itemManipulacao);
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Consultar itens");
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimento())
                {
                    salvarRegistroAtual();
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Salvar item");
            }
        }

        private void btnExlcuir_Click(object sender, EventArgs e)
        {
            try
            {
                if (validarPreenchimentoExclusao())
                {
                    exluirRegistroAtual();

                    if (ultimoFiltro != null)
                    {
                        carregarItens(ultimoFiltro);
                    }
                    else
                    {
                        itemManipulacao = new ClienteFornecedor();
                        ajustarBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Excluir item");
            }
        }

        private void dgvClienteFornecedor_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                carregarCampoSelecao();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Selecionar item");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
        }

        #endregion

        #region Métodos

        public override void AtualizarCodigoText(ModeloBusca modelo)
        {
            if (modelo.GetType() == typeof(Fornecedor))
            {
                this.txtCnpjFornecedor.Text = modelo.RetornarCodigoTexto();
                this.txtCnpjFornecedor.Select();
            }
            if (modelo.GetType() == typeof(Cliente))
            {
                this.txtCnpjCliente.Text = modelo.RetornarCodigoTexto();
                this.txtCnpjCliente.Select();
            } 
        }

        private void buscarFornecedorNome()
        {
            lblNomeFornecedor.Text = String.Empty;

            if (itemManipulacao.CGCFornecedor != 0)
            {
                var desc = TipologiaFornecedorBLL.Instance().SelecionarEntidadeBusca(new Fornecedor() { CNPJFornecedor = itemManipulacao.CGCFornecedor }).FirstOrDefault();

                if (desc != null)
                {
                    lblNomeFornecedor.Text = desc.NomeFornecedor;
                }
            }
        }

        private void buscarClienteNome()
        {
            lblNomeCliente.Text = String.Empty;

            if (itemManipulacao.CNPJCliente != 0)
            {
                var desc = ClienteFornecedorBLL.Instance().SelecionarEntidadeBusca(new Cliente() { CNPJCliente = itemManipulacao.CNPJCliente }).FirstOrDefault();

                if (desc != null)
                {
                    lblNomeCliente.Text = desc.NomeCliente;
                }
            }
        }

        private void ajustarBind()
        {
            txtCnpjFornecedor.DataBindings.Clear();
            var bindingCGCFornecedor = new Binding("Text", itemManipulacao, "CGCFornecedor", true, DataSourceUpdateMode.OnPropertyChanged, null, "##############", null);
            bindingCGCFornecedor.Parse += OnNullableTextBindingParsed;
            txtCnpjFornecedor.DataBindings.Add(bindingCGCFornecedor);

            txtMargem.DataBindings.Clear();
            var bindingMargem = new Binding("Text", itemManipulacao, "Margem", true, DataSourceUpdateMode.OnPropertyChanged, null, "####.##", null);
            bindingMargem.Parse += OnNullableTextBindingParsed;
            txtMargem.DataBindings.Add(bindingMargem);

            txtCnpjCliente.DataBindings.Clear();
            var bindingCNPJCliente = new Binding("Text", itemManipulacao, "CNPJCliente", true, DataSourceUpdateMode.OnPropertyChanged, null, "##############", null);
            bindingCNPJCliente.Parse += OnNullableTextBindingParsed;
            txtCnpjCliente.DataBindings.Add(bindingCNPJCliente);

            txtDataVigencia.DataBindings.Clear();
            txtDataVigencia.DataBindings.Add("Text", itemManipulacao, "DataVigenciaStr");
        }

        private void carregarItens(ClienteFornecedor filtro)
        {
            var listaItem = ClienteFornecedorBLL.Instance().ListarTodos(filtro);

            if (listaItem.Count != 0)
            {
                btnExlcuir.Enabled = true;
            }
            else
            {
                btnExlcuir.Enabled = false;
                Mensagem("Esta pesquisa não retornou nenhum resultado!", "Consulta cliente fornecedor");
            }

            dgvClienteFornecedor.DataSource = listaItem;

            ajustarTamanhoColuna();

            limparSelecao();
        }

        private void limparSelecao()
        {
            dgvClienteFornecedor.ClearSelection();

            if (ultimoFiltro != null)
            {
                itemManipulacao = ultimoFiltro.ClonarTipado();
            }

            buscarFornecedorNome();

            buscarClienteNome();

            ajustarBind();
        }

        private void ajustarTamanhoColuna()
        {
            if (dgvClienteFornecedor.Columns.Count > 0)
            {
                dgvClienteFornecedor.Columns[0].Width = 100;
                dgvClienteFornecedor.Columns[1].Width = 210;
                dgvClienteFornecedor.Columns[2].Width = 120;
                dgvClienteFornecedor.Columns[3].Width = 210;
            }
        }

        private void carregarCampoSelecao()
        {
            itemManipulacao = ((ClienteFornecedor)this.dgvClienteFornecedor.CurrentRow.DataBoundItem).ClonarTipado();
            buscarFornecedorNome();
            buscarClienteNome();
            ajustarBind();
        }

        private void salvarRegistroAtual()
        {
            string msg;

            msg = (!procurarMargem()) ? "Confirma inclusão de uma nova vigência?"
                : "Deseja confirmar a atualização da vigência atual com a margem alterada?";

            if (MensagemConfirmacao(msg))
            {
                ClienteFornecedorBLL.Instance().Gravar(itemManipulacao);

                Mensagem("Sucesso ao realizar a gravação da margem!", "Salvar cliente fornecedor");

                carregarItens(ultimoFiltro != null ? ultimoFiltro : itemManipulacao);

            }
        }

        private bool validarPreenchimento()
        {
            bool validacao = true;
            String mensagens = String.Empty;

            mensagens = "Ocorreram o(s) seguinte(s) erro(s):\n";

            if (itemManipulacao.CNPJCliente == 0)
            {
                validacao = false;
                mensagens += " - O CPF/CNPJ Cliente é obrigatório;\n";
            }
            else
            {
                if (ClienteFornecedorBLL.Instance().SelecionarEntidadeBusca(new Cliente() { CNPJCliente = itemManipulacao.CNPJCliente }).Count == 0)
                {
                    validacao = false;
                    mensagens += " - O cliente informado não está cadastrado, verifique o CPNJ/CPF;\n";
                }

            }

            if (itemManipulacao.CGCFornecedor != 0)
            {
                if (TipologiaFornecedorBLL.Instance().SelecionarEntidadeBusca(new Fornecedor() { CNPJFornecedor = itemManipulacao.CGCFornecedor }).Count == 0)
                {
                    validacao = false;
                    mensagens +=  " - O fornecedor informado não está cadastrado, verifique o CPNJ/CPF;\n";
                }
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
                mensagens += " - A Margem é obrigatória;\n";
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
                mensagens += " - A Data Vigência é obrigatória;\n";
            }

            if (!validacao)
            {
                Mensagem(mensagens, "Validação de Informações");
            }

            return validacao;
        }

        private bool validarPreenchimentoExclusao()
        {
            bool validacao = true;

            if (itemManipulacao.CNPJCliente == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.Margem == 0)
            {
                validacao = false;
            }

            if (itemManipulacao.DataVigencia == DateTime.MinValue)
            {
                validacao = false;
            }

            if (!validacao)
            {
                Mensagem("Selecione uma margem no grid antes de excluí-la!", "Validação de Informações");
            }

            return validacao;
        }

        private void exluirRegistroAtual()
        {
            if (MensagemConfirmacao("Confirma exclusão?"))
            {
                if (procurarMargem())
                {
                    ClienteFornecedorBLL.Instance().Excluir(itemManipulacao);
                    Mensagem("Sucesso ao remover uma margem!", "Remoção de margens");
                }
                else
                {
                    Mensagem("Margem não encontrada para exclusão!", "Remoção de margens");
                }
            }
        }

        private bool procurarMargem()
        {
            var listaItem = ClienteFornecedorBLL.Instance().ListarTodos(itemManipulacao);
            var itemOriginal = listaItem.FirstOrDefault(i => i.CGCFornecedor == itemManipulacao.CGCFornecedor
                                                                                  && i.CNPJCliente == itemManipulacao.CNPJCliente
                                                                                  && i.DataVigencia == itemManipulacao.DataVigencia);
            if (itemOriginal == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion
    }
}
