﻿namespace VDA980.Controle
{
    partial class ucRelatorioItemEspecial
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbpRelatorio = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.lblAte = new System.Windows.Forms.Label();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.txtCodDpk = new System.Windows.Forms.TextBox();
            this.lblCodDPK = new System.Windows.Forms.Label();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.lblCodLoja = new System.Windows.Forms.Label();
            this.gbpRelatorio.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbpRelatorio
            // 
            this.gbpRelatorio.Controls.Add(this.groupBox1);
            this.gbpRelatorio.Controls.Add(this.cmbLoja);
            this.gbpRelatorio.Controls.Add(this.txtCodDpk);
            this.gbpRelatorio.Controls.Add(this.lblCodDPK);
            this.gbpRelatorio.Controls.Add(this.btnVisualizar);
            this.gbpRelatorio.Controls.Add(this.lblCodLoja);
            this.gbpRelatorio.Location = new System.Drawing.Point(4, 4);
            this.gbpRelatorio.Name = "gbpRelatorio";
            this.gbpRelatorio.Size = new System.Drawing.Size(744, 375);
            this.gbpRelatorio.TabIndex = 0;
            this.gbpRelatorio.TabStop = false;
            this.gbpRelatorio.Text = "Relatório - Item Especial";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataInicio);
            this.groupBox1.Controls.Add(this.lblAte);
            this.groupBox1.Controls.Add(this.txtDataFim);
            this.groupBox1.Location = new System.Drawing.Point(22, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(307, 50);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Vigência";
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.Location = new System.Drawing.Point(22, 19);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(116, 20);
            this.txtDataInicio.TabIndex = 3;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            // 
            // lblAte
            // 
            this.lblAte.AutoSize = true;
            this.lblAte.Location = new System.Drawing.Point(144, 22);
            this.lblAte.Name = "lblAte";
            this.lblAte.Size = new System.Drawing.Size(23, 13);
            this.lblAte.TabIndex = 41;
            this.lblAte.Text = "Até";
            // 
            // txtDataFim
            // 
            this.txtDataFim.Location = new System.Drawing.Point(173, 19);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(116, 20);
            this.txtDataFim.TabIndex = 4;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(103, 27);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(116, 21);
            this.cmbLoja.TabIndex = 1;
            // 
            // txtCodDpk
            // 
            this.txtCodDpk.Location = new System.Drawing.Point(103, 55);
            this.txtCodDpk.MaxLength = 18;
            this.txtCodDpk.Name = "txtCodDpk";
            this.txtCodDpk.Size = new System.Drawing.Size(116, 20);
            this.txtCodDpk.TabIndex = 2;
            this.txtCodDpk.TextChanged += new System.EventHandler(this.txtCodDpk_TextChanged);
            // 
            // lblCodDPK
            // 
            this.lblCodDPK.AutoSize = true;
            this.lblCodDPK.Location = new System.Drawing.Point(19, 58);
            this.lblCodDPK.Name = "lblCodDPK";
            this.lblCodDPK.Size = new System.Drawing.Size(65, 13);
            this.lblCodDPK.TabIndex = 35;
            this.lblCodDPK.Text = "Código DPK";
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.Location = new System.Drawing.Point(22, 149);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(75, 23);
            this.btnVisualizar.TabIndex = 5;
            this.btnVisualizar.Text = "&Visualizar";
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            // 
            // lblCodLoja
            // 
            this.lblCodLoja.AutoSize = true;
            this.lblCodLoja.Location = new System.Drawing.Point(19, 30);
            this.lblCodLoja.Name = "lblCodLoja";
            this.lblCodLoja.Size = new System.Drawing.Size(63, 13);
            this.lblCodLoja.TabIndex = 27;
            this.lblCodLoja.Text = "Código Loja";
            // 
            // ucRelatorioItemEspecial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbpRelatorio);
            this.Name = "ucRelatorioItemEspecial";
            this.Size = new System.Drawing.Size(750, 382);
            this.gbpRelatorio.ResumeLayout(false);
            this.gbpRelatorio.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbpRelatorio;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.Label lblCodLoja;
        private System.Windows.Forms.TextBox txtCodDpk;
        private System.Windows.Forms.Label lblCodDPK;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.Label lblAte;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
