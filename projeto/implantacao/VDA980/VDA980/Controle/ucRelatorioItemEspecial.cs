﻿using System;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using VDA980.Formularios;
using System.Collections.Generic;

namespace VDA980.Controle
{
    public partial class ucRelatorioItemEspecial : ucBase
    {
        #region Propriedades

        private RelatorioItemEspecialFiltro itemManipulacao = new RelatorioItemEspecialFiltro();

        #endregion

        #region Construtor

        public ucRelatorioItemEspecial()
        {   
            try
            {
                InitializeComponent();

                carregarLoja();

                ajustarBind();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Montagem do controle");
            }
        }

        #endregion

        #region Eventos

        private void txtCodDpk_TextChanged(object sender, EventArgs e)
        {
            tratarCampoLong(sender);
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            List<RelatorioItemEspecial> listaRetorno = new List<RelatorioItemEspecial>();
            Relatorio relatorio = new Relatorio();

            try
            {
                if (validarTela())
                {
                    listaRetorno = ItemEspecialBLL.Instance().SelectByFilterRelatorio(itemManipulacao);

                    if (listaRetorno == null || listaRetorno.Count == 0)
                    {
                        Mensagem("Nenhum resultado foi encontrado.", "Relatório");
                    }
                    else
                    {
                        relatorio.Source = listaRetorno;
                        relatorio.NomeDataSet = "DataSetRelatorioItemEspecial";
                        relatorio.NomeRelatorio = "Relatório de Regras de Itens Especiais";
                        relatorio.ReportEmbeddedResource = "VDA980.Relatorios.RelatorioItemEspecial.rdlc";

                        frmRelatorio frm = new frmRelatorio();
                        frm.itemManipulacao = relatorio;
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                Mensagem("Ocorreu um erro: " + ex.Message, "Relatório");
            }
        }

        private void OnNullableTextBindingParsed(object sender, ConvertEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Value.ToString())) e.Value = null;
            else if (e.Value.ToString().Equals("  /  /")) e.Value = null;
        }

        #endregion

        #region Métodos

        private void carregarLoja()
        {
            var lojaAtiva = LojaBLL.Instance().ListarTodos();

            cmbLoja.DataSource = lojaAtiva;
            cmbLoja.DisplayMember = "NomeExibicao";
            cmbLoja.ValueMember = "CodigoLoja";
        }

        private void ajustarBind()
        {
            cmbLoja.DataBindings.Clear();
            cmbLoja.DataBindings.Add("SelectedValue", itemManipulacao, "CodigoLoja");

            txtCodDpk.DataBindings.Clear();
            var bindingCodigoDPK = new Binding("Text", itemManipulacao, "CodigoDPK", true, DataSourceUpdateMode.OnPropertyChanged, null, "#######", null);
            bindingCodigoDPK.Parse += OnNullableTextBindingParsed;
            txtCodDpk.DataBindings.Add(bindingCodigoDPK);

            txtDataInicio.DataBindings.Clear();
            var bindingDataInicio = new Binding("Text", itemManipulacao, "DataVigenciaIni", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataInicio.Parse += OnNullableTextBindingParsed;
            txtDataInicio.DataBindings.Add(bindingDataInicio);

            txtDataFim.DataBindings.Clear();
            var bindingDataFim = new Binding("Text", itemManipulacao, "DataVigenciaFim", true, DataSourceUpdateMode.OnPropertyChanged, null, "dd/MM/yyyy", null);
            bindingDataFim.Parse += OnNullableTextBindingParsed;
            txtDataFim.DataBindings.Add(bindingDataFim);
        }

        private bool validarTela()
        {
            bool validarTela = true;

            if ((itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim == null) 
                    || (itemManipulacao.DataVigenciaIni == null && itemManipulacao.DataVigenciaFim != null))
            {
                validarTela = false;
                Mensagem("É necessário preencher o Intervalo Data Vigência.", "Validação de Informações");
            }
            if (itemManipulacao.DataVigenciaIni != null && itemManipulacao.DataVigenciaFim != null)
            {
                if (itemManipulacao.DataVigenciaIni > itemManipulacao.DataVigenciaFim)
                {
                    validarTela = false;
                    Mensagem("A Data Vigência Inicial deve ser menor que a Data Vigência Final.", "Validação de Informações");
                }
            }
            return validarTela;
        }

        #endregion

    }
}
