﻿using System;
using System.Windows.Forms;
using VDA980.Entities;

namespace VDA980.Formularios
{
    public partial class frmPrincipal : Form
    {
        #region Propriedades

        private Usuario usuarioLogado;
        private Loja lojaUsuario;

        private bool efetuarLogoff = false;

        #endregion

        #region Métodos

        private void tratarAcaoMenu(string operacao)
        {
            switch (operacao)
            {
                case "PARAMETRIZAÇÃO":
                    exibirParametrizacao();
                    break;

                case "RELATÓRIOS":
                    exibirRelatorio();
                    break;

                case "SAIR":
                    encerrarAplicacao();
                    break;

                case "TROCARUSUÁRIO":
                    trocarUsuario();
                    break;
            }
        }

        private void trocarUsuario()
        {
            if (MessageBox.Show("Deseja trocar o usuário?", "Troca de Usuário", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                var login = new frmLogin();
                login.Show();
                this.efetuarLogoff = true;
                this.Close();
            }
        }

        private void encerrarAplicacao()
        {
            Application.Exit();
        }

        private void exibirRelatorio()
        {
            panExibicao.Controls.Clear();
            panExibicao.Controls.Add(new Controle.ucRelatorio());
        }

        private void exibirParametrizacao()
        {
            panExibicao.Controls.Clear();
            panExibicao.Controls.Add(new Controle.ucParametrizacao());
        }

        #endregion

        #region Eventos

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        public frmPrincipal()
        {
            InitializeComponent();
            usuarioLogado = Session.Usuario;
            lojaUsuario = Session.Loja;
            tslUsuario.Text = String.Format(tslUsuario.Text, usuarioLogado.Nome);
        }

        private void parametrizacaoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tratarAcaoMenu(sender.ToString().Replace(" ", "").Replace("&", "").ToUpper());
        }

        private void relatoriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tratarAcaoMenu(sender.ToString().Replace(" ", "").Replace("&", "").ToUpper());
        }

        private void sairToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tratarAcaoMenu(sender.ToString().Replace(" ", "").Replace("&", "").ToUpper());
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tratarAcaoMenu(sender.ToString().Replace(" ", "").Replace("&", "").ToUpper());
        }
        #endregion

        private void frmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Não finaliza a aplicação ser for uma soliticação de logoff.
            if (!efetuarLogoff)
            {
                encerrarAplicacao();
            }
        }
    }
}
