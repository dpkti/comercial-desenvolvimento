﻿using System;
using System.Drawing;
using System.Windows.Forms;
using VDA980.Business;
using VDA980.Entities;
using System.Globalization;
using System.Threading;

namespace VDA980.Formularios
{
    public partial class frmLogin : Form
    {
        private Usuario usuario = new Usuario();
        private Loja loja;

        public frmLogin()
        {
            InitializeComponent();
            
            this.CenterToScreen();

            setarCultura();
        }

        private static void setarCultura()
        {
            CultureInfo ci = new CultureInfo("PT-BR", false);
            ci.NumberFormat.NumberDecimalSeparator = ",";
            ci.NumberFormat.NumberGroupSeparator = ".";
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
        }

        private void ajustarBind()
        {
            txtUsuario.DataBindings.Clear();
            txtUsuario.DataBindings.Add("Text", usuario, "Login", true, DataSourceUpdateMode.OnPropertyChanged);

            txtSenha.DataBindings.Clear();
            txtSenha.DataBindings.Add("Text", usuario, "Senha", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            Usuario userLogado = new Usuario();

            controleAtivo(false);

            informarMensagem("Realizando autenticação, aguarde...", false);

            Application.DoEvents();

            loja = LojaBLL.Instance().ValidarLoja();

            Session.Loja = loja;

            if (loja == null)
            {
                informarMensagem("Erro ao ler arquivo CD.txt.", true);
            }
            else
            {
                userLogado = UsuarioBLL.Instance().ValidarUsuario(usuario);

                efetuarLogin(userLogado);

                controleAtivo(true);
            }
        }

        private void efetuarLogin(Usuario userLogado)
        {
            try
            {
                if (userLogado != null)
                {
                    if (userLogado.PermissaoAcesso)
                    {
                        // Seta a session com o usuário logado
                        userLogado.Login = userLogado.Login.ToUpper();
                        Session.Usuario = userLogado;

                        frmPrincipal frm = new frmPrincipal();
                        frm.Show();

                        this.Visible = false;
                    }
                    else
                    {
                        informarMensagem("Login não autorizado! Tente novamente.", true);
                    }
                }
                else
                {
                    informarMensagem("Usuário inválido! Tente novamente.", true);
                }
            }
            catch
            {
                //logar exceção
                informarMensagem("Ocorreu um erro, contate o suporte", true);
            }

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.txtUsuario.Select();
            ajustarBind();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSenha.Select();
            }
        }

        private void informarMensagem(string mensagem, bool erro)
        {
            tslMensagem.Text = mensagem;
            if (erro)
            {
                tslMensagem.ForeColor = Color.Red;
            }
            else
            {
                tslMensagem.ForeColor = Color.Black;
            }
        }

        private void controleAtivo(bool ativacao)
        {
            txtUsuario.Enabled = ativacao;
            txtSenha.Enabled = ativacao;
            btnEntrar.Enabled = ativacao;
            btnSair.Enabled = ativacao;
        }

        private void frmLogin_Move(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }


    }
}
