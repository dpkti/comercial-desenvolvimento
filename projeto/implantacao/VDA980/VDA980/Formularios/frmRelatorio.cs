﻿using System;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using VDA980.Business;
using VDA980.Entities;
using System.Collections.Generic;

namespace VDA980.Formularios
{
    public partial class frmRelatorio : Form
    {
        #region Propriedades

        public Relatorio itemManipulacao = new Relatorio();

        #endregion

        public frmRelatorio()
        {
            InitializeComponent();
        }

        private void frmRelatorio_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();

            // Resize window
            //this.WindowState = FormWindowState.Maximized;
            //this.rptRelatorio.Width = this.Width - 50;
            //this.rptRelatorio.Height = this.Height - 50;

            this.MaximizeBox = false;

            carregarRelatorio();
        }

        private void carregarRelatorio()
        {
            ReportDataSource reportDataSource;
            List<ReportParameter> parametros = new List<ReportParameter>();

            try
            {
                // Limpa o reportviewer
                this.rptRelatorio.Reset();
                this.rptRelatorio.LocalReport.Dispose();

                // Seta o datasource do Relatório
                reportDataSource = new ReportDataSource(
                    itemManipulacao.NomeDataSet, 
                    new BindingSource() { DataSource = itemManipulacao.Source }
                );

                this.rptRelatorio.LocalReport.DataSources.Clear();
                this.rptRelatorio.LocalReport.DataSources.Add(reportDataSource);

                // Seta o arquivo RDLC do Relatório
                this.rptRelatorio.LocalReport.ReportEmbeddedResource = itemManipulacao.ReportEmbeddedResource;

                // Seta display do form com o nome do Relatório
                this.Text = itemManipulacao.NomeRelatorio;

                // Seta os parâmetros do Relatório
                parametros.Add(new ReportParameter("NomeRelatorio", itemManipulacao.NomeRelatorio));                

                if (itemManipulacao.Parametros != null && itemManipulacao.Parametros.Count > 0)
                {
                    itemManipulacao.Parametros.ForEach(p =>
                    {
                        parametros.Add(new ReportParameter(p.Key, p.Value.ToString()));
                    });
                }

                this.rptRelatorio.LocalReport.SetParameters(parametros);

                // Atualiza reportviewer para exibir o Relatório.
                this.rptRelatorio.RefreshReport();
            }
            catch (Exception ex)
            {
                LogBLL.LogErro(ex);
                //Mensagem("Ocorreu um erro: " + ex.Message, "Relatório");
            }
        }
    }
}
