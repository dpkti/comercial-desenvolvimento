﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VDA980.Controle;
using VDA980.Entities;
using System.Reflection;
using VDA980.Business;

namespace VDA980.Formularios
{
    public partial class frmBuscaZoom : Form
    {
        #region Propriedades

        private ucBase controlePai;
        object intanciaBusca;
        Type tipagemBusca;

        #endregion

        #region Construtor

        public frmBuscaZoom(ucBase pai, Type tipo)
        {
            InitializeComponent();

            setarPropriedades(pai, tipo);

            this.intanciaBusca = criarInstancia(tipo);

            ajustarLabelCodigo();
        }

        #endregion

        #region Eventos

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            object listaResultado = invocarMetodoBusca();

            if (listaResultado != null)
            {
                dgvBusca.DataSource = listaResultado;

                ajustarTamanhoColuna();
            }

            dgvBusca.DataSource = listaResultado;
        }

        private void dgvBusca_DoubleClick(object sender, EventArgs e)
        {
            retornarBuscaControlePai();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvBusca_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        #endregion

        #region Métodos

        private void ajustarLabelCodigo()
        {
            List<Object> listaChave = new List<Object>();

            MethodInfo metodoCodigoLabel = tipagemBusca.GetMethod("RetornarCodigoLabel");

            object resultado = metodoCodigoLabel.Invoke(intanciaBusca, listaChave.ToArray());

            if (resultado != null)
            {
                lblCodigo.Text = resultado.ToString();
            }
        }

        private void ajustarTamanhoColuna()
        {
            if (dgvBusca.Columns.Count > 0)
            {
                dgvBusca.Columns[0].Width = 100;
                dgvBusca.Columns[1].Width = 100;
                dgvBusca.Columns[2].Width = 250;
            }
        }

        private object invocarMetodoBusca()
        {
            ModeloBusca chaveBusca;
            List<Object> listaChave = new List<Object>();

            MethodInfo metodoBusca = tipagemBusca.GetMethod("SelecionarEntidadeBusca");
            ParameterInfo[] tipagemParametro = metodoBusca.GetParameters();

            ConstructorInfo construtorTipoParametro = tipagemParametro[0].ParameterType.GetConstructor(Type.EmptyTypes);

            chaveBusca = (ModeloBusca)construtorTipoParametro.Invoke(new object[0]);

            chaveBusca.SetarParametrosBusca(txtCodigo.Text, txtDescricao.Text);
            listaChave.Add(chaveBusca);

            object listaResultado = metodoBusca.Invoke(intanciaBusca, listaChave.ToArray());
            return listaResultado;
        }

        private void retornarBuscaControlePai()
        {
            var itemSelecionado = (ModeloBusca)this.dgvBusca.CurrentRow.DataBoundItem;

            if (itemSelecionado != null)
            {
                controlePai.AtualizarCodigoText(itemSelecionado);
                this.Close();
            }
        }

        private void setarPropriedades(ucBase pai, Type tipo)
        {
            this.controlePai = pai;
            this.tipagemBusca = tipo;
        }

        private static object criarInstancia(Type tipo)
        {
            ConstructorInfo construtor = tipo.GetConstructor(Type.EmptyTypes);
            var obj = construtor.Invoke(new object[0]);
            return obj;
        }

        #endregion

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            if (intanciaBusca.GetType() != typeof(ItemEspecialBLL))
            {
                TextBox box = (TextBox)sender;
                long lng;

                if (!long.TryParse(box.Text, out lng))
                {
                    if (box.Text.Length > 1)
                        box.Text = box.Text.Substring(0, box.Text.Length - 1);
                    else
                        box.Text = "";

                    box.Select(box.Text.Length, 0);
                }
            }
        }
    }
}
