Attribute VB_Name = "Module2"
' Declara��o de vari�veis do banco

Public orasession As Object
Public oradatabase As Object
Public oradynaset As Object

Public sel1 As String
Public sel2 As String
Public situacao As Integer
Public via As String

Public Sub CONS_GRID(POS_TOP As Integer, _
                      POS_LEFT As Integer, _
                      POS_WIDTH As Integer, _
                      POS_HEIGHT As Integer, _
                      NUM_COL As Byte, _
                      FRM As Form, _
                      SEL As String)
                      
Dim CONT_L As Integer
Screen.MousePointer = 11

FRM.Grid1.Top = POS_TOP
FRM.Grid1.Left = POS_LEFT
FRM.Grid1.Width = POS_WIDTH
FRM.Grid1.Height = POS_HEIGHT

FRM.Grid1.Rows = oradynaset.RecordCount + 1
FRM.Grid1.Cols = NUM_COL
FRM.Grid1.FontBold = True

  FRM.Grid1.ColWidth(0) = 900
  FRM.Grid1.ColWidth(1) = 3600
  
  FRM.Grid1.Row = 0

  'CABE�ALHO
  FRM.Grid1.Col = 0
  FRM.Grid1.Text = " CODIGO"
  FRM.Grid1.Col = 1
  FRM.Grid1.Text = "       TRANSPORTADORA"
  
  For CONT_L = 1 To oradynaset.RecordCount
    FRM.Grid1.Row = CONT_L
    FRM.Grid1.Col = 0
    FRM.Grid1.Text = oradynaset.Fields("COD_TRANSP").Value
    FRM.Grid1.Col = 1
    FRM.Grid1.Text = oradynaset.Fields("NOME_TRANSP").Value
    oradynaset.MoveNext
  Next
   Screen.MousePointer = 0
  
End Sub

Sub dados_tela()



frmTransp.txtcodigo.Text = oradynaset.Fields("cod_transp").Value
frmTransp.txtnome.Text = oradynaset.Fields("nome_transp").Value

frmDados.lblendereco.Caption = oradynaset.Fields("endereco").Value
frmDados.lblbairro.Caption = oradynaset.Fields("bairro").Value
frmDados.lblcidade.Caption = oradynaset.Fields("nome_cidade").Value
frmDados.lbluf.Caption = oradynaset.Fields("cod_uf").Value
frmDados.lblcep.Caption = oradynaset.Fields("cep").Value
frmDados.lblddd.Caption = oradynaset.Fields("ddd").Value
frmDados.lblfone.Caption = oradynaset.Fields("fone").Value
frmDados.lblfax.Caption = oradynaset.Fields("fax").Value
frmDados.lblsigla.Caption = oradynaset.Fields("sigla").Value
situacao = oradynaset.Fields("situacao").Value

  If situacao = 9 Then
    frmDados.lblsituacao.ForeColor = &HFF&
    frmDados.lblsituacao.Caption = "DESATIVADA"
  ElseIf situacao = 0 Then
    frmDados.lblsituacao.ForeColor = &H800000
    frmDados.lblsituacao.Caption = "ATIVA"
  End If

via = oradynaset.Fields("via_transp").Value

  If via = "R" Then
    frmDados.lblviatransp.Caption = "RODOVI�RIA"
  ElseIf via = "A" Then
    frmDados.lblviatransp.Caption = "A�REA"
  ElseIf via = "F" Then
    frmDados.lblviatransp.Caption = "FERROVI�RIA"
  ElseIf via = "M" Then
    frmDados.lblviatransp.Caption = "MAR�TIMA"
  ElseIf via = "O" Then
    frmDados.lblviatransp.Caption = "�NIBUS"
  End If
  
  
End Sub


Sub informacoes()

On Error GoTo trata_erro

    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Remove "nome"
    
    If frmTransp.txtcodigo.Text = "" And frmTransp.txtnome.Text = "" Then
      MsgBox "Selecione uma Transportadora", 0, "ATEN��O"
      frmTransp.txtcodigo.SetFocus
    Else
      If frmTransp.txtcodigo.Text <> "" Then
         oradatabase.Parameters.Add "cod", frmTransp.txtcodigo.Text, 1
         Set oradynaset = oradatabase.dbcreatedynaset(sel1, 0&)
          If oradynaset.EOF Then
            MsgBox "C�digo " & frmTransp.txtcodigo.Text & " n�o cadastrado", 0, "ATEN��O"
            frmTransp.txtcodigo.Text = ""
            frmTransp.txtnome.Text = ""
            Exit Sub
          End If
      ElseIf frmTransp.txtnome.Text <> "" Then
         oradatabase.Parameters.Add "nome", frmTransp.txtnome.Text, 1
         Set oradynaset = oradatabase.dbcreatedynaset(sel2, 0&)
         MDIForm1.ssmsg.Caption = "Aguarde Consultando .."
        If oradynaset.EOF Then
           MDIForm1.ssmsg.Caption = ""
           MsgBox "Transportadora n�o CADASTRADA", 0, "ATEN��O"
           frmTransp.txtcodigo.Text = ""
           frmTransp.txtnome.Text = ""
           frmTransp.txtcodigo.SetFocus
           oradatabase.Parameters.Remove "nome"
           Exit Sub
        Else
           'CHAMA PROCEDURE QUE MONTA O GRID
           Call CONS_GRID(500, 2000, 4500, 1500, 2, frmTransp, sel2)
           MDIForm1.ssmsg.Caption = ""
           frmTransp.SSPanel1.Visible = False
           DoEvents
           frmTransp.Grid1.Visible = True
           Exit Sub
        End If
      End If
      Call dados_tela
      frmDados.Left = 75
      frmDados.Top = 1995
      frmDados.Height = 4500
      frmDados.Width = 9435
      frmDados.Show
    End If
    Exit Sub
    
trata_erro:
        frmTrataerro.Show
    
End Sub


Sub selects()
        
        sel1 = "Select a.cod_transp cod_transp,"
 sel1 = sel1 & " a.nome_transp nome_transp,"
 sel1 = sel1 & " a.endereco endereco,"
 sel1 = sel1 & " nvl(a.bairro,' ') bairro,"
 sel1 = sel1 & " b.nome_cidade nome_cidade,"
 sel1 = sel1 & " b.cod_uf cod_uf,"
 sel1 = sel1 & " substr(to_char(nvl(a.ddd,0),'0999'),2,4) ddd,"
 sel1 = sel1 & " substr(to_char(nvl(a.fone,0),'999999999'),2,9) fone,"
 sel1 = sel1 & " substr(to_char(nvl(a.cep,0),'09999999'),2,8) cep,"
 sel1 = sel1 & " substr(to_char(nvl(a.fax,0),'9999999999999'),2,13) fax,"
 sel1 = sel1 & " nvl(a.via_transp,' ') via_transp,"
 sel1 = sel1 & " a.situacao situacao,"
 sel1 = sel1 & " nvl(a.sigla,' ') sigla"
 sel1 = sel1 & " from transportadora a, cidade b"
 sel1 = sel1 & " where a.cod_cidade=b.cod_cidade and"
 sel1 = sel1 & " a.cod_transp = :cod"

        sel2 = "Select a.cod_transp cod_transp,"
 sel2 = sel2 & " a.nome_transp nome_transp"
 sel2 = sel2 & " from transportadora a, cidade b"
 sel2 = sel2 & " where a.cod_cidade=b.cod_cidade and"
 sel2 = sel2 & " a.nome_transp like :nome"


End Sub


