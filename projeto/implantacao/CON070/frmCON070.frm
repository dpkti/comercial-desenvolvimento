VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmCON070 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CON070 - Ativar/Desativar LINK"
   ClientHeight    =   4635
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5985
   ControlBox      =   0   'False
   Icon            =   "frmCON070.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4635
   ScaleWidth      =   5985
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "Banco IDPK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1605
      Left            =   60
      TabIndex        =   5
      Top             =   2970
      Width           =   2565
      Begin Bot�o.cmd cmdAtivarLinkIDPK 
         Height          =   615
         Left            =   60
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   300
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   "Ativar LINK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON070.frx":0CCA
         PICN            =   "frmCON070.frx":0CE6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesativarLinkIDPK 
         Height          =   615
         Left            =   60
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   930
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   "Desativar LINK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   255
         FCOLO           =   255
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON070.frx":19C0
         PICN            =   "frmCON070.frx":19DC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Banco PRODUCAO"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   60
      TabIndex        =   2
      Top             =   1200
      Width           =   2565
      Begin Bot�o.cmd cmdAtivarLinkProducao 
         Height          =   615
         Left            =   60
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   330
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   "Ativar LINK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   49152
         FCOLO           =   49152
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON070.frx":26B6
         PICN            =   "frmCON070.frx":26D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesativarLinkProducao 
         Height          =   615
         Left            =   60
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   960
         Width           =   2265
         _ExtentX        =   3995
         _ExtentY        =   1085
         BTYPE           =   3
         TX              =   "Desativar LINK"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   255
         FCOLO           =   255
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmCON070.frx":33AC
         PICN            =   "frmCON070.frx":33C8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   1125
      Width           =   5865
      _ExtentX        =   10345
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   4920
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON070.frx":40A2
      PICN            =   "frmCON070.frx":40BE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblStatusIDPK 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2670
      TabIndex        =   11
      Top             =   3240
      Width           =   60
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Status atual do LINK - IDPK:"
      Height          =   195
      Left            =   2670
      TabIndex        =   10
      Top             =   3000
      Width           =   2025
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Status atual do LINK - PRODUCAO:"
      Height          =   195
      Left            =   2700
      TabIndex        =   9
      Top             =   1260
      Width           =   2565
   End
   Begin VB.Label lblStatusProducao 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   210
      Left            =   2700
      TabIndex        =   8
      Top             =   1530
      Width           =   60
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmCON070.frx":4D98
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmCON070"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Private Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "Urvaz System": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Private Function AddToINI(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal KeyValue As String) As Integer
    WritePrivateProfileString Section, Key, KeyValue, FileName
    AddToINI = 1
End Function

Private Sub cmdAtivarLinkIDPK_Click()
    AddToINI "D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.ini", "LINKS", "LINK_IDPK", "S"
    Ler_Arquivo
End Sub

Private Sub cmdAtivarLinkProducao_Click()
    AddToINI "D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.ini", "LINKS", "LINK_PRODUCAO", "S"
    Ler_Arquivo
End Sub

Private Sub cmdDesativarLinkIDPK_Click()
    AddToINI "D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.ini", "LINKS", "LINK_IDPK", "N"
    Ler_Arquivo
End Sub

Private Sub cmdDesativarLinkProducao_Click()
    AddToINI "D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.ini", "LINKS", "LINK_PRODUCAO", "N"
    Ler_Arquivo
End Sub

Private Sub cmdSair_Click()
    End
End Sub
    
Private Sub Form_Load()
   Ler_Arquivo
End Sub

Sub Ler_Arquivo()
    
    If Dir("D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.INI") <> "" Then
       If GetKeyVal("D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.INI", "LINKS", "LINK_PRODUCAO") = "S" Then
          lblStatusProducao.Caption = "ATIVADO"
          lblStatusProducao.ForeColor = vbGreen
       Else
          lblStatusProducao.Caption = "DESATIVADO"
          lblStatusProducao.ForeColor = vbRed
       End If
       If GetKeyVal("D:\SISTEMAS\ORACLE\SISTEMAS\VB\32BITS\LINK.INI", "LINKS", "LINK_IDPK") = "S" Then
          lblStatusIDPK.Caption = "ATIVADO"
          lblStatusIDPK.ForeColor = vbGreen
       Else
          lblStatusIDPK.Caption = "DESATIVADO"
          lblStatusIDPK.ForeColor = vbRed
       End If
       
    Else
        lblStatusProducao.Caption = "Arquivo LINK.TXT n�o encontrado."
        lblStatusIDPK.Caption = "Arquivo LINK.TXT n�o encontrado."
    
    End If
    
End Sub

