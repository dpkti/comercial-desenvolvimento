Attribute VB_Name = "Module1"

Public intDuration As Integer
Public strDirName As String
Public counter As Integer
Public looped As Boolean

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Private Const BIF_RETURNONLYFSDIRS As Long = 1
Private Const BIF_DONTGOBELOWDOMAIN As Long = 2
Private Const MAX_PATH As Integer = 260

Private Declare Function SHBrowseForFolder Lib _
                "shell32" (lpbi As BrowseInfo) As Long

Private Declare Function SHGetPathFromIDList Lib _
                "shell32" (ByVal pidList As Long, ByVal lpBuffer _
                As String) As Long

Private Declare Function lstrcat Lib "kernel32" _
                Alias "lstrcatA" (ByVal lpString1 As String, ByVal _
                lpString2 As String) As Long

Private Type BrowseInfo
    hWndOwner As Long
    pIDLRoot As Long
    pszDisplayName As Long
    lpszTitle As Long
    ulFlags As Long
    lpfnCallback As Long
    lParam As Long
    iImage As Long
End Type

' Declarations and such needed for the example:
' (Copy them to the (declarations) section of a module.)
Public Declare Function GetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" _
              (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, _
              lpData As Any) As Long
Public Declare Function GetFileVersionInfoSize Lib "version.dll" Alias _
              "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Public Declare Function VerQueryValue Lib "version.dll" Alias "VerQueryValueA" (pBlock _
              As Any, ByVal lpSubBlock As String, lplpBuffer As Long, puLen As Long) As Long
'Public Declare Function lstrcpy Lib "kernel32.dll" Alias "lstrcpyA" (ByVal lpString1 _
              As Any, ByVal lpString2 As Any) As Long
Public Declare Sub CopyMemory Lib "kernel32.dll" Alias "RtlMoveMemory" (Destination As Any, _
              Source As Any, ByVal Length As Long)
Public Type VS_FIXEDFILEINFO
    dwSignature As Long
    dwStrucVersion As Long
    dwFileVersionMS As Long
    dwFileVersionLS As Long
    dwProductVersionMS As Long
    dwProductVersionLS As Long
    dwFileFlagsMask As Long
    dwFileFlags As Long
    dwFileOS As Long
    dwFileType As Long
    dwFileSubtype As Long
    dwFileDateMS As Long
    dwFileDateLS As Long
End Type


Public DbAccess2 As Object
Public SS As Object

Public Type STARTUPINFO
cb As Long
lpReserved As String
lpDesktop As String
lpTitle As String
dwX As Long
dwY As Long
dwXSize As Long
dwYSize As Long
dwXCountChars As Long
dwYCountChars As Long
dwFillAttribute As Long
dwFlags As Long
wShowWindow As Integer
cbReserved2 As Integer
lpReserved2 As Long
hStdInput As Long
hStdOutput As Long
hStdError As Long
End Type

Public Type PROCESS_INFORMATION
hProcess As Long
hThread As Long
dwProcessID As Long
dwThreadID As Long
End Type

Public Declare Function WaitForSingleObject Lib _
"kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds _
As Long) As Long

Declare Function CreateProcessA Lib "kernel32" _
(ByVal lpApplicationName As Long, ByVal lpCommandLine As _
String, ByVal lpProcessAttributes As Long, ByVal _
lpThreadAttributes As Long, ByVal bInheritHandles As Long, _
ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, _
ByVal lpCurrentDirectory As Long, lpStartupInfo As _
STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) _
As Long

Declare Function CloseHandle Lib "kernel32" (ByVal hObject _
As Long) As Long

Public Const NORMAL_PRIORITY_CLASS = &H20&
Public Const INFINITE = -1&

Public Sub ExecCmd(cmdline$)
    Dim proc As PROCESS_INFORMATION
    Dim start As STARTUPINFO
    
    'Inicia a strutura STARTUPINFO
    start.cb = Len(start)
    
    'Inicia a aplica��o escolhida para ser executada
    ret& = CreateProcessA(0&, cmdline$, 0&, 0&, 1&, _
    NORMAL_PRIORITY_CLASS, 0&, 0&, start, proc)
    
    'Aguarda a aplica��o iniciada terminar
    ret& = WaitForSingleObject(proc.hProcess, INFINITE)
    ret& = CloseHandle(proc.hProcess)
End Sub

Public Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "DPK": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Public Sub Browse_Folder()

  Dim lpIDList As Long 'Declare Varibles
  Dim sBuffer As String
  Dim szTitle As String
  Dim tBrowseInfo As BrowseInfo

    szTitle = "Select directory."

    'Text to appear in the the gray area under the title bar
    'telling you what to do

    With tBrowseInfo
        .hWndOwner = Form1.hWnd 'Owner Form
        .lpszTitle = lstrcat(szTitle, "")
        .ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN
    End With

    lpIDList = SHBrowseForFolder(tBrowseInfo)

    If (lpIDList) Then
        sBuffer = Space$(MAX_PATH)
        SHGetPathFromIDList lpIDList, sBuffer
        sBuffer = Left$(sBuffer, InStr(sBuffer, vbNullChar) - 1)
    End If

    strDirName = sBuffer


End Sub

Public Sub Get_File_Version(sDirName As String)

    Dim i As Integer
    Dim lngFileVerInfo As Long
    Dim sFileName As String
    Dim hWndOwner As Long
    Dim count As Integer
    'Dim lpData ':( As Variant ?
    Dim vffi As VS_FIXEDFILEINFO  ' version info structure
    Dim buffer() As Byte          ' buffer for version info resource
    Dim pData As Long             ' pointer to version info data
    Dim nDataLen As Long          ' length of info pointed at by pData
    Dim cpl(0 To 3) As Byte       ' buffer for code page & language
    Dim cplstr As String          ' 8-digit hex string of cpl
    Dim dispstr As String         ' string used to display version information
    Dim RetVal As Long            ' generic return value
    Dim sExtension As String
    Dim ItemX As ListItem
    Dim x As Integer
  
    Form1.lsvFileList.ListItems.Clear
    Form1.MousePointer = vbHourglass
    For i = 0 To Form1.File1.ListCount - 1
    DoEvents
    sExtension = Right$(Form1.File1.List(i), 3)
        If UCase$(sExtension) = "DLL" Or UCase$(sExtension) = "EXE" Or UCase$(sExtension) = "MDB" Then
            sFileName = sDirName + "\" + Form1.File1.List(i)
            If sFileName <> "" Then
                ' First, get the size of the version info resource.  If this function fails, then Text1
                ' identifies a file that isn't a 32-bit executable/DLL/etc.
                nDataLen = GetFileVersionInfoSize(sFileName, pData)
                If nDataLen <> 0 Then
                    ' Make the buffer large enough to hold the version info resource.
                    ReDim buffer(0 To nDataLen - 1) As Byte
                    ' Get the version information resource.
                    RetVal = GetFileVersionInfo(sFileName, 0, nDataLen, buffer(0))
                    ' Get a pointer to a structure that holds a bunch of data.
                    RetVal = VerQueryValue(buffer(0), "\", pData, nDataLen)
                    ' Copy that structure into the one we can access.
                    CopyMemory vffi, ByVal pData, nDataLen
                    ' Display the full version number of the file.
                    dispstr = Trim$(Str$(HIWORD(vffi.dwFileVersionMS))) & "." & _
                    Trim$(Str$(LOWORD(vffi.dwFileVersionMS))) & "." & _
                    Trim$(Str$(HIWORD(vffi.dwFileVersionLS))) & "." & _
                    Trim$(Str$(LOWORD(vffi.dwFileVersionLS)))
                    
                    Set ItemX = Form1.lsvFileList.ListItems.Add(, , Form1.File1.List(i))
                    ItemX.SubItems(1) = Left(Replace(dispstr, ".", ""), 1) & "." & Mid(Replace(dispstr, ".", ""), 2, 2) & "." & Format(Right(Replace(dispstr, ".", ""), 2), "000")
                    
                ElseIf UCase$(sExtension) = "MDB" Then 'N�o tem como pegar a vers�o dos arquivos MDB.
                    Set ItemX = Form1.lsvFileList.ListItems.Add(, , Form1.File1.List(i))
                    ItemX.SubItems(1) = ""  'Left(Replace(dispstr, ".", ""), 1) & "." & Mid(Replace(dispstr, ".", ""), 2, 2) & "." & Format(Right(Replace(dispstr, ".", ""), 2), "000")
                End If
            Else
                Exit Sub
            End If
        End If
    Next i
Form1.MousePointer = vbNormal
End Sub

' *** Place the following function definitions inside a module. ***

' HIWORD and LOWORD are API macros defined below.
Public Function HIWORD(ByVal dwValue As Long) As Long

  Dim hexstr As String

    hexstr = Right$("00000000" & Hex$(dwValue), 8)
    HIWORD = CLng("&H" & Left$(hexstr, 4))

End Function

Public Function LOWORD(ByVal dwValue As Long) As Long

  Dim hexstr As String

    hexstr = Right$("00000000" & Hex$(dwValue), 8)
    LOWORD = CLng("&H" & Right$(hexstr, 4))

End Function

' This nifty subroutine swaps two byte values without needing a buffer variable.
' This technique, which uses Xor, works as long as the two values to be swapped are
' numeric and of the same data type (here, both Byte).
Public Sub SwapByte(byte1 As Byte, byte2 As Byte)

    byte1 = byte1 Xor byte2
    byte2 = byte1 Xor byte2
    byte1 = byte1 Xor byte2

End Sub

' This function creates a hexadecimal string to represent a number, but it
' outputs a string of a fixed number of digits.  Extra zeros are added to make
' the string the proper length.  The "&H" prefix is not put into the string.
Public Function FixedHex(ByVal hexval As Long, ByVal nDigits As Long) As String

    FixedHex = Right$("00000000" & Hex$(hexval), nDigits)

End Function

Public Function Pegar_Drive() As String
    Dim fs, d, dc, n
    Dim vArq As Integer
    
    Pegar_Drive = Left(App.Path, 2)
    If Pegar_Drive = "\\" Then
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set dc = fs.Drives
        For Each d In dc
            If d.DriveType = 3 Then
                n = d.ShareName
            Else
                n = d.Path
            End If
            If InStr(1, App.Path, n) > 0 Then
                Pegar_Drive = d.DriveLetter & ":"
                Exit For
            End If
        Next
    End If

    If Pegar_Drive = "\\" Then
        vArq = FreeFile
        If Dir("C\DRIVE_PAAC.INI") <> "" Then
            Open "C\DRIVE_PAAC.INI" For Input As #vArq
            Input #vArq, Pegar_Drive
        Else
            Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
            Open "C\DRIVE_PAAC.INI" For Output As #vArq
            Print #vArq, Pegar_Drive
        End If
        Close #vArq
    End If
End Function

