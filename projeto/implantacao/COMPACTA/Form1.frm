VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Form1 
   Caption         =   "Gera��o dos Arquivos de Carga para os paac's"
   ClientHeight    =   8205
   ClientLeft      =   3555
   ClientTop       =   2760
   ClientWidth     =   12105
   ControlBox      =   0   'False
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8205
   ScaleWidth      =   12105
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkFIL240 
      Appearance      =   0  'Flat
      Caption         =   "Incluir arquivo FIL240.MDB - O Arquivo est� no Diret�rio do Execut�vel."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   120
      TabIndex        =   75
      Top             =   3300
      Value           =   1  'Checked
      Width           =   6375
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Gerar Arquivo PAAC.INI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   7500
      TabIndex        =   16
      Top             =   7290
      Width           =   1485
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   15
      Top             =   7800
      Width           =   12105
      _ExtentX        =   21352
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18283
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Repres"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   10770
      TabIndex        =   14
      ToolTipText     =   "Marcar/Desmarcar Repres"
      Top             =   3060
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "PAACs"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9390
      TabIndex        =   13
      ToolTipText     =   "Marcar/Desmarcar Paacs"
      Top             =   3060
      Width           =   1245
   End
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   8550
      Top             =   1020
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin VB.ListBox lstRepresentantes 
      Appearance      =   0  'Flat
      Height          =   2730
      ItemData        =   "Form1.frx":030A
      Left            =   10740
      List            =   "Form1.frx":0311
      Style           =   1  'Checkbox
      TabIndex        =   10
      Top             =   285
      Width           =   1245
   End
   Begin VB.ListBox lstPAACs 
      Appearance      =   0  'Flat
      Height          =   2730
      ItemData        =   "Form1.frx":0323
      Left            =   9390
      List            =   "Form1.frx":032A
      Style           =   1  'Checkbox
      TabIndex        =   9
      Top             =   270
      Width           =   1245
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      Height          =   225
      Left            =   3000
      TabIndex        =   8
      Top             =   720
      Visible         =   0   'False
      Width           =   675
   End
   Begin VB.CommandButton cmdCaminhoArquivosParaZipar 
      Caption         =   "..."
      Height          =   285
      Left            =   8880
      TabIndex        =   6
      Top             =   390
      Width           =   315
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   8040
      Top             =   1020
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton sscmdFim 
      Caption         =   "Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   10620
      TabIndex        =   5
      Top             =   7290
      Width           =   1485
   End
   Begin VB.CommandButton sscIniciar 
      Caption         =   "Iniciar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   9060
      TabIndex        =   4
      Top             =   7290
      Width           =   1485
   End
   Begin VB.TextBox txtArquivosparaZipar 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2790
      TabIndex        =   2
      Top             =   360
      Width           =   6075
   End
   Begin VB.TextBox txtDriveFTP 
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   2790
      MaxLength       =   2
      TabIndex        =   0
      Top             =   0
      Width           =   525
   End
   Begin MSComctlLib.ListView lsvFileList 
      Height          =   2115
      Left            =   60
      TabIndex        =   17
      Top             =   1020
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   3731
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Arquivo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Vers�o"
         Object.Width           =   2540
      EndProperty
   End
   Begin TabDlg.SSTab Guias 
      Height          =   3615
      Left            =   90
      TabIndex        =   18
      Top             =   3600
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   6376
      _Version        =   393216
      Tabs            =   5
      Tab             =   4
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Vers�es"
      TabPicture(0)   =   "Form1.frx":033C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Shape1"
      Tab(0).Control(1)=   "Label18"
      Tab(0).Control(2)=   "Label17"
      Tab(0).Control(3)=   "Label16"
      Tab(0).Control(4)=   "Label15"
      Tab(0).Control(5)=   "Label7"
      Tab(0).Control(6)=   "Label6"
      Tab(0).Control(7)=   "Label1"
      Tab(0).Control(8)=   "lsvVersoes"
      Tab(0).Control(9)=   "cmdExcluir"
      Tab(0).Control(10)=   "cmdAddVersao"
      Tab(0).Control(11)=   "txtVersao"
      Tab(0).Control(12)=   "txtPrograma"
      Tab(0).ControlCount=   13
      TabCaption(1)   =   "SQL"
      TabPicture(1)   =   "Form1.frx":0358
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label21"
      Tab(1).Control(1)=   "Label10"
      Tab(1).Control(2)=   "Label9"
      Tab(1).Control(3)=   "Label8"
      Tab(1).Control(4)=   "lsvSQL"
      Tab(1).Control(5)=   "cmdCriarTabela"
      Tab(1).Control(6)=   "cmdExcluirTabela"
      Tab(1).Control(7)=   "cmdExcluirCampo"
      Tab(1).Control(8)=   "cmdCriarCampo"
      Tab(1).Control(9)=   "txtOrdem"
      Tab(1).Control(10)=   "Command4"
      Tab(1).Control(11)=   "Command3"
      Tab(1).Control(12)=   "txtbanco"
      Tab(1).Control(13)=   "txtSQL"
      Tab(1).ControlCount=   14
      TabCaption(2)   =   ".BAT"
      TabPicture(2)   =   "Form1.frx":0374
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtBat"
      Tab(2).Control(1)=   "Command5"
      Tab(2).Control(2)=   "Command6"
      Tab(2).Control(3)=   "txtOrdemBat"
      Tab(2).Control(4)=   "lsvBAT"
      Tab(2).Control(5)=   "Label11"
      Tab(2).Control(6)=   "Label12"
      Tab(2).Control(7)=   "Label19"
      Tab(2).Control(8)=   "Label22"
      Tab(2).ControlCount=   9
      TabCaption(3)   =   ".EXE"
      TabPicture(3)   =   "Form1.frx":0390
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "txtEXE"
      Tab(3).Control(1)=   "Command7"
      Tab(3).Control(2)=   "Command8"
      Tab(3).Control(3)=   "txtOrdemEXE"
      Tab(3).Control(4)=   "lsvEXE"
      Tab(3).Control(5)=   "Label13"
      Tab(3).Control(6)=   "Label14"
      Tab(3).Control(7)=   "Label20"
      Tab(3).Control(8)=   "Label23"
      Tab(3).ControlCount=   9
      TabCaption(4)   =   "Enviar Arquivos"
      TabPicture(4)   =   "Form1.frx":03AC
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "Label30"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Label29"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "Label28"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "Label24"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "Label25"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "lsvMover"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).Control(6)=   "txtOrigem"
      Tab(4).Control(6).Enabled=   0   'False
      Tab(4).Control(7)=   "txtDestino"
      Tab(4).Control(7).Enabled=   0   'False
      Tab(4).Control(8)=   "Command11"
      Tab(4).Control(8).Enabled=   0   'False
      Tab(4).Control(9)=   "Command10"
      Tab(4).Control(9).Enabled=   0   'False
      Tab(4).Control(10)=   "txtOrdemMover"
      Tab(4).Control(10).Enabled=   0   'False
      Tab(4).Control(11)=   "chkApagarOrigem"
      Tab(4).Control(11).Enabled=   0   'False
      Tab(4).ControlCount=   12
      Begin VB.CheckBox chkApagarOrigem 
         Appearance      =   0  'Flat
         Caption         =   "Apagar Arquivo de Origem ap�s a c�pia"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1320
         TabIndex        =   74
         Top             =   450
         Width           =   3225
      End
      Begin VB.TextBox txtOrdemMover 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   630
         MaxLength       =   20
         TabIndex        =   72
         Top             =   420
         Width           =   555
      End
      Begin VB.TextBox txtPrograma 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -71370
         MaxLength       =   9
         TabIndex        =   44
         Top             =   690
         Width           =   1515
      End
      Begin VB.TextBox txtVersao 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -71370
         TabIndex        =   43
         Top             =   1320
         Width           =   1515
      End
      Begin VB.TextBox txtSQL 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1725
         Left            =   -68820
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   42
         Top             =   1200
         Width           =   5715
      End
      Begin VB.TextBox txtbanco 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -68190
         MaxLength       =   20
         TabIndex        =   41
         Top             =   630
         Width           =   2445
      End
      Begin VB.TextBox txtBat 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -68820
         MaxLength       =   20
         TabIndex        =   40
         Top             =   660
         Width           =   2265
      End
      Begin VB.TextBox txtEXE 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   -68850
         MaxLength       =   20
         TabIndex        =   39
         Top             =   660
         Width           =   2265
      End
      Begin VB.CommandButton cmdAddVersao 
         Caption         =   "Salvar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -69360
         TabIndex        =   38
         Top             =   660
         Width           =   1305
      End
      Begin VB.CommandButton cmdExcluir 
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -69360
         TabIndex        =   37
         Top             =   1170
         Width           =   1305
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -64050
         TabIndex        =   35
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Salvar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -65040
         TabIndex        =   34
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Salvar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -65040
         TabIndex        =   33
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -64050
         TabIndex        =   32
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Salvar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -65040
         TabIndex        =   31
         Top             =   600
         Width           =   945
      End
      Begin VB.CommandButton Command8 
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   -64050
         TabIndex        =   30
         Top             =   600
         Width           =   945
      End
      Begin VB.TextBox txtOrdem 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -68820
         MaxLength       =   20
         TabIndex        =   29
         Top             =   630
         Width           =   555
      End
      Begin VB.TextBox txtOrdemBat 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -69450
         MaxLength       =   20
         TabIndex        =   28
         Top             =   660
         Width           =   555
      End
      Begin VB.TextBox txtOrdemEXE 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   -69450
         MaxLength       =   20
         TabIndex        =   27
         Top             =   660
         Width           =   555
      End
      Begin VB.CommandButton cmdCriarCampo 
         Caption         =   "Adicionar Campo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -68850
         TabIndex        =   26
         Tag             =   "ALTER TABLE <NOME_TABELA> ADD COLUMN <NOME_CAMPO> <TIPO_CAMPO>"
         Top             =   2970
         Width           =   1005
      End
      Begin VB.CommandButton cmdExcluirCampo 
         Caption         =   "Excluir Campo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -67800
         TabIndex        =   25
         Tag             =   "ALTER TABLE <NOME_TABELA> DROP <NOME_CAMPO>"
         Top             =   2970
         Width           =   1005
      End
      Begin VB.CommandButton cmdExcluirTabela 
         Caption         =   "Excluir Tabela"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -66750
         TabIndex        =   24
         Tag             =   "DROP TABLE <NOME_TABELA>"
         Top             =   2970
         Width           =   975
      End
      Begin VB.CommandButton cmdCriarTabela 
         Caption         =   "Criar Tabela"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   -65730
         TabIndex        =   23
         Tag             =   $"Form1.frx":03C8
         Top             =   2970
         Width           =   975
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Excluir"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   10560
         TabIndex        =   22
         Top             =   1050
         Width           =   1305
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Salvar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Left            =   10560
         TabIndex        =   21
         Top             =   540
         Width           =   1305
      End
      Begin VB.TextBox txtDestino 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   90
         TabIndex        =   20
         Top             =   1650
         Width           =   6495
      End
      Begin VB.TextBox txtOrigem 
         Appearance      =   0  'Flat
         Height          =   345
         Left            =   90
         TabIndex        =   19
         Top             =   1020
         Width           =   6495
      End
      Begin MSComctlLib.ListView lsvVersoes 
         Height          =   2235
         Left            =   -74910
         TabIndex        =   36
         Top             =   690
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   3942
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Sistema"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Vers�o"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lsvSQL 
         Height          =   2295
         Left            =   -74910
         TabIndex        =   45
         Top             =   630
         Width           =   6045
         _ExtentX        =   10663
         _ExtentY        =   4048
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Ordem"
            Object.Width           =   882
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Banco"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Comando"
            Object.Width           =   7056
         EndProperty
      End
      Begin MSComctlLib.ListView lsvBAT 
         Height          =   2235
         Left            =   -74940
         TabIndex        =   46
         Top             =   660
         Width           =   5415
         _ExtentX        =   9551
         _ExtentY        =   3942
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Ordem"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Arquivo"
            Object.Width           =   5292
         EndProperty
      End
      Begin MSComctlLib.ListView lsvEXE 
         Height          =   2235
         Left            =   -74940
         TabIndex        =   47
         Top             =   660
         Width           =   5385
         _ExtentX        =   9499
         _ExtentY        =   3942
         View            =   3
         LabelEdit       =   1
         Sorted          =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Ordem"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Arquivo"
            Object.Width           =   5292
         EndProperty
      End
      Begin MSComctlLib.ListView lsvMover 
         Height          =   1275
         Left            =   120
         TabIndex        =   48
         Top             =   2220
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   2249
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   4
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Ordem"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Origem"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Destino"
            Object.Width           =   7937
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Apagar Origem"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Label Label25 
         AutoSize        =   -1  'True
         Caption         =   "Ordem"
         Height          =   195
         Left            =   90
         TabIndex        =   73
         Top             =   480
         Width           =   465
      End
      Begin VB.Label Label24 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   $"Form1.frx":0469
         ForeColor       =   &H80000008&
         Height          =   1065
         Left            =   6690
         TabIndex        =   71
         Top             =   1020
         Width           =   3645
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Softwares/Vers�es:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   70
         Top             =   480
         Width           =   1395
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Software: Ex: FIL030"
         Height          =   195
         Left            =   -71370
         TabIndex        =   69
         Top             =   480
         Width           =   1485
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o: Ex: 0.00.000"
         Height          =   195
         Left            =   -71370
         TabIndex        =   68
         Top             =   1110
         Width           =   1485
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Comandos SQL:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   67
         Top             =   420
         Width           =   1155
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Comando SQL:"
         Height          =   195
         Left            =   -68820
         TabIndex        =   66
         Top             =   990
         Width           =   1080
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Banco:"
         Height          =   195
         Left            =   -68190
         TabIndex        =   65
         Top             =   420
         Width           =   1200
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo .BAT"
         Height          =   195
         Left            =   -74940
         TabIndex        =   64
         Top             =   450
         Width           =   945
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo .BAT - Ex: TESTE.BAT"
         Height          =   225
         Left            =   -68820
         TabIndex        =   63
         Top             =   450
         Width           =   2235
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Arquivos .EXE"
         Height          =   195
         Left            =   -74940
         TabIndex        =   62
         Top             =   450
         Width           =   1020
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo .EXE - Ex: TESTE.EXE"
         Height          =   195
         Left            =   -68850
         TabIndex        =   61
         Top             =   450
         Width           =   2235
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Somente digitar o nome do Executavel sem a Extens�o"
         Height          =   195
         Left            =   -71310
         TabIndex        =   60
         Top             =   2070
         Width           =   3900
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Software:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -71310
         TabIndex        =   59
         Top             =   1830
         Width           =   975
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   -71310
         TabIndex        =   58
         Top             =   2400
         Width           =   825
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "Digitar no seguinte formato: Um Digito + ""."" + Dois Digitos + ""."" + Tr�s Digitos."
         Height          =   240
         Left            =   -71310
         TabIndex        =   57
         Top             =   2610
         Width           =   5610
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label19 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   "S� poder� exisitir um arquivo .BAT para ser executado no PAAC ou Representante."
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   -69450
         TabIndex        =   56
         Top             =   1680
         Width           =   6375
      End
      Begin VB.Label Label20 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BorderStyle     =   1  'Fixed Single
         Caption         =   "S� poder� exisitir um arquivo .EXE para ser executado no PAAC ou Representante."
         ForeColor       =   &H80000008&
         Height          =   1215
         Left            =   -69480
         TabIndex        =   55
         Top             =   1680
         Width           =   6405
      End
      Begin VB.Shape Shape1 
         Height          =   1125
         Left            =   -71370
         Top             =   1800
         Width           =   6375
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Ordem"
         Height          =   195
         Left            =   -68820
         TabIndex        =   54
         Top             =   420
         Width           =   465
      End
      Begin VB.Label Label22 
         AutoSize        =   -1  'True
         Caption         =   "Ordem"
         Height          =   195
         Left            =   -69450
         TabIndex        =   53
         Top             =   450
         Width           =   465
      End
      Begin VB.Label Label23 
         AutoSize        =   -1  'True
         Caption         =   "Ordem"
         Height          =   195
         Left            =   -69450
         TabIndex        =   52
         Top             =   450
         Width           =   465
      End
      Begin VB.Label Label28 
         AutoSize        =   -1  'True
         Caption         =   "Caminho completo do arquivo de Destino na Maquina do Representante/PAAC"
         Height          =   195
         Left            =   90
         TabIndex        =   51
         Top             =   1440
         Width           =   5610
      End
      Begin VB.Label Label29 
         AutoSize        =   -1  'True
         Caption         =   "Caminho completo do arquivo de Origem na Maquina do Representante/PAAC:"
         Height          =   195
         Left            =   90
         TabIndex        =   50
         Top             =   810
         Width           =   5610
      End
      Begin VB.Label Label30 
         AutoSize        =   -1  'True
         Caption         =   "Enviar Arquivos para o PAAC/Representante:"
         Height          =   195
         Left            =   120
         TabIndex        =   49
         Top             =   2010
         Width           =   3240
      End
   End
   Begin VB.Label Label26 
      AutoSize        =   -1  'True
      Caption         =   "O Arquivo PAAC.INI ser� criado na pasta dos Execut�veis."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2070
      TabIndex        =   76
      Top             =   7440
      Width           =   5025
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "PAAC�s"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   9390
      TabIndex        =   12
      Top             =   60
      Width           =   645
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Representantes"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   10740
      TabIndex        =   11
      Top             =   60
      Width           =   1350
   End
   Begin VB.Label File11 
      AutoSize        =   -1  'True
      Caption         =   "Arquivos que ser�o compactados:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   60
      TabIndex        =   7
      Top             =   810
      Visible         =   0   'False
      Width           =   2895
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Local arquivos a serem Zipados"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   30
      TabIndex        =   3
      Top             =   450
      Width           =   2715
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Drive Pasta de FTP ""OUT"""
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   450
      TabIndex        =   1
      Top             =   90
      Width           =   2310
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAddVersao_Click()
    Dim litem As ListItem
    Dim i As Integer
    
    If txtPrograma = "" Then
        MsgBox "Informe o Sistema.", vbInformation, "Aten��o"
        txtPrograma.SetFocus
        Exit Sub
    End If
    If txtVersao = "" Then
        MsgBox "Informe o Sistema.", vbInformation, "Aten��o"
        txtVersao.SetFocus
        Exit Sub
    End If
    
    For i = 1 To lsvVersoes.ListItems.count - 1
        If lsvVersoes.ListItems(i) = txtPrograma Then
            lsvVersoes.ListItems(i).SubItems(1) = Me.txtVersao
            GoTo FIM
        End If
    Next
    
    Set litem = lsvVersoes.ListItems.Add
    litem = txtPrograma
    litem.SubItems(1) = txtVersao
    
FIM:
    txtPrograma = ""
    txtVersao = ""
    
End Sub


Private Sub cmdCaminhoArquivosParaZipar_Click()
    Dim litem As ListItem
    
    CommonDialog1.ShowOpen
    txtArquivosparaZipar = Left(CommonDialog1.FileName, Len(CommonDialog1.FileName) - Len(CommonDialog1.FileTitle))
    
    Get_File_Version txtArquivosparaZipar
    
    File1.Path = txtArquivosparaZipar
    File1.Refresh
    File11.Visible = True
    
    Preencher_Guias
    lsvVersoes.ListItems.Clear
    For i = 1 To lsvFileList.ListItems.count
    
        Set litem = lsvVersoes.ListItems.Add
        litem = UCase(Left(lsvFileList.ListItems(i), InStr(1, lsvFileList.ListItems(i), ".") - 1))
        litem.SubItems(1) = lsvFileList.ListItems(i).SubItems(1)
    
    Next
    
End Sub


Private Sub cmdCriarCampo_Click()
    Me.txtSQL = cmdCriarCampo.Tag
End Sub

Private Sub cmdCriarTabela_Click()
    txtSQL = Me.cmdCriarTabela.Tag
End Sub

Private Sub cmdExcluir_Click()
    If txtPrograma <> "" And Me.txtVersao <> "" Then
       If MsgBox("Confirma a exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
          lsvVersoes.ListItems.Remove (Me.lsvVersoes.SelectedItem.Index)
          txtVersao = ""
          txtPrograma = ""
        End If
    End If
End Sub

Private Sub cmdExcluirCampo_Click()
    txtSQL = cmdExcluirCampo.Tag
End Sub

Private Sub Command1_Click()
    For i = 0 To lstPAACs.ListCount - 1
        lstPAACs.Selected(i) = Not (lstPAACs.Selected(i))
    Next
End Sub

Private Sub Command10_Click()
    lsvMover.ListItems.Remove (lsvMover.SelectedItem.Index)

    ReSequenciar lsvMover
    
    txtOrdemMover = lsvMover.ListItems.count + 1
End Sub

Private Sub Command11_Click()
    Dim litem As ListItem
    Dim i As Integer
    
    If txtOrigem = "" Then
        MsgBox "Informe o Arquivo de Origem.", vbInformation, "Aten��o"
        txtDestino.SetFocus
        Exit Sub
    End If
    If txtDestino = "" Then
        MsgBox "Informe o Arquivo de Destino.", vbInformation, "Aten��o"
        txtDestino.SetFocus
        Exit Sub
    End If
    
    Set litem = lsvMover.ListItems.Add
    
    litem = txtOrdemMover
    litem.SubItems(1) = txtOrigem
    litem.SubItems(2) = txtDestino
    litem.SubItems(3) = IIf(chkApagarOrigem.Value = 1, "SIM", "NAO")
    
    txtOrigem = ""
    txtDestino = ""
    chkApagarOrigem.Value = 0
    
    txtOrdemMover = lsvMover.ListItems.count + 1
    
End Sub

Private Sub Command2_Click()
    For i = 0 To lstRepresentantes.ListCount - 1
        lstRepresentantes.Selected(i) = Not (lstRepresentantes.Selected(i))
    Next
End Sub

Private Sub Command3_Click()
    If txtbanco <> "" And txtSQL <> "" Then
       If MsgBox("Confirma a exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
          lsvSQL.ListItems.Remove (lsvSQL.SelectedItem.Index)
          txtbanco = ""
          txtSQL = ""
          ReSequenciar lsvSQL
        End If
    End If
End Sub

Private Sub Command4_Click()
    Dim litem As ListItem
    Dim i As Integer
    
    If txtbanco = "" Then
        MsgBox "Informe o Banco de Dados onde ser� executado o Comando.", vbInformation, "Aten��o"
        txtbanco.SetFocus
        Exit Sub
    End If
    If txtSQL = "" Then
        MsgBox "Informe o Comando SQL a ser executado.", vbInformation, "Aten��o"
        txtSQL.SetFocus
        Exit Sub
    End If
    
    If Val(txtOrdem) = 0 Then
        MsgBox "Este campo n�o pode conter valor 0 (zero).", vbInformation, "Aten��o"
        txtOrdem.SetFocus
        Exit Sub
    End If
    
    Set litem = lsvSQL.ListItems.Add
    litem = txtOrdem
    litem.SubItems(1) = txtbanco
    litem.SubItems(2) = txtSQL
    
    txtbanco = ""
    txtSQL = ""
    
End Sub

Private Sub Command5_Click()
    Dim litem As ListItem
    Dim i As Integer
    
    If txtBat = "" Then
        MsgBox "Informe o nome do Arquivo .BAT", vbInformation, "Aten��o"
        txtBat.SetFocus
        Exit Sub
    End If
    
    If Val(txtOrdemBat) = 0 Then
        MsgBox "Este campo n�o pode conter valor 0 (zero).", vbInformation, "Aten��o"
        txtOrdemBat.SetFocus
        Exit Sub
    End If
    
    Set litem = lsvBAT.ListItems.Add
    litem = txtOrdemBat
    litem.SubItems(1) = txtBat
    
    txtOrdemBat = ""
    txtBat = ""
End Sub

Private Sub Command6_Click()
    If txtBat <> "" Then
       If MsgBox("Confirma a exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
          lsvBAT.ListItems.Remove (lsvBAT.SelectedItem.Index)
          txtBat = ""
          ReSequenciar lsvBAT
        End If
    End If
End Sub

Private Sub Command7_Click()
    Dim litem As ListItem
    Dim i As Integer
    
    If txtEXE = "" Then
        MsgBox "Informe o nome do Arquivo .EXE", vbInformation, "Aten��o"
        txtEXE.SetFocus
        Exit Sub
    End If
    
    If Val(txtOrdemEXE) = 0 Then
        MsgBox "Este campo n�o pode conter valor 0 (zero).", vbInformation, "Aten��o"
        txtOrdemEXE.SetFocus
        Exit Sub
    End If
    
    Set litem = lsvEXE.ListItems.Add
    litem = txtOrdemEXE
    litem.SubItems(1) = txtEXE
    
    txtOrdemEXE = ""
    txtEXE = ""
End Sub

Private Sub Command8_Click()
    If txtEXE <> "" Then
       If MsgBox("Confirma a exclus�o do item selecionado ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
          lsvEXE.ListItems.Remove (lsvEXE.SelectedItem.Index)
          txtOrdemEXE = ""
          txtEXE = ""
          ReSequenciar lsvEXE
        End If
    End If
End Sub

'**********************
'CRIAR ARQUIVO PAAC.INI
'**********************
Private Sub Command9_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim vArq As Integer
          Dim vBancos As String
          Dim vSqls As String
          Dim vBats As String
          Dim vEXEs As String
          Dim i As Integer
          Dim ii As Integer
          Dim vEncontrou As Boolean
          Dim vNaoEncontrou(20) As String
          Dim vVersaoIncorreta(20) As String
          Dim vListaArquivos As String
          
2         If Trim(txtDriveFTP) = "" Then
3            MsgBox "Informe o Drive.", vbInformation, "Aten��o"
4            txtDriveFTP.SetFocus
5            Exit Sub
6         End If
          
          'Verificar se o arquivo que est� no ZIP est� no PAAC.INI
'7         For i = 0 To File1.ListCount - 1
'8             If InStr(1, "MDB INI TXT HDR ZIP", UCase(Right(File1.List(i), 3))) = 0 Then
'9                vEncontrou = False
'10               For ii = 1 To lsvVersoes.ListItems.count
'11                   If UCase(Left(File1.List(i), InStr(1, File1.List(i), ".") - 1)) = UCase(lsvVersoes.ListItems(ii)) Then
'12                      vEncontrou = True
'13                      Exit For
'14                   End If
'15               Next
'16               If vEncontrou = False Then
'17                  vNaoEncontrou(i) = File1.List(i)
'18               End If
'19            End If
'20        Next
          
'21        For i = 0 To 19
'22          If vNaoEncontrou(i) <> "" Then
'23              If vListaArquivos = "" Then
'24                  vListaArquivos = vNaoEncontrou(i)
'25              Else
'26                  vListaArquivos = vListaArquivos & vbCrLf & vNaoEncontrou(i)
'27              End If
'28          End If
'29        Next
''30        If Trim(vListaArquivos) <> "" Then
''31           MsgBox "Faltou voc� adicionar os arquivos abaixo na guia Vers�es: " & vbCrLf & vListaArquivos, vbInformation, "Aten��o"
''32           Exit Sub
''33        End If
          
          'Verificar Versao dos arquivos com a versao cadastrada para o INI.
7         For i = 1 To lsvFileList.ListItems.count
8             vEncontrou = False
9             For ii = 1 To lsvVersoes.ListItems.count
10                If UCase(Left(lsvFileList.ListItems(i), InStr(1, lsvFileList.ListItems(i), ".") - 1)) = UCase(lsvVersoes.ListItems(ii)) Then
11                   If lsvFileList.ListItems(i).SubItems(1) = Me.lsvVersoes.ListItems(ii).SubItems(1) Then
12                      vEncontrou = True
13                      Exit For
14                   End If
15                End If
16            Next
17            If vEncontrou = False Then
18               vVersaoIncorreta(i) = lsvFileList.ListItems(i)
19            End If
20        Next
          
21        For i = 0 To 19
22          If vVersaoIncorreta(i) <> "" Then
23              If vListaArquivos = "" Then
24                  vListaArquivos = vVersaoIncorreta(i)
25              Else
26                  vListaArquivos = vListaArquivos & vbCrLf & vVersaoIncorreta(i)
27              End If
28          End If
29        Next
'57        If Trim(vListaArquivos) <> "" Then
'58           MsgBox "A vers�o do EXE est� diferente da Vers�o cadastrada dos seguintes arquivos : " & vbCrLf & vListaArquivos, vbInformation, "Aten��o"
'59           Exit Sub
'60        End If
                      
          
30        vArq = FreeFile
          
31        If Dir(txtDriveFTP & "\OUT\TMP\PAAC.INI", vbArchive) <> "" Then
32            Kill txtDriveFTP & "\OUT\TMP\PAAC.INI"
33        End If
          
34        If Dir(txtDriveFTP & "\OUT\TMP", vbDirectory) = "" Then
35           MkDir txtDriveFTP & "\OUT\TMP"
36        End If
          
37        If Dir(txtDriveFTP & "\OUT\TMP\*.*") <> "" Then Kill txtDriveFTP & "\OUT\TMP\*.*"
          
38        Open txtDriveFTP & "\OUT\TMP\PAAC.INI" For Output As #vArq
          
          '******
          'VERSAO
          '******
39        Print #vArq, "[VERSAO]"
40        For i = 1 To lsvVersoes.ListItems.count
41            Print #vArq, lsvVersoes.ListItems(i) & "=" & lsvVersoes.ListItems(i).SubItems(1)
42        Next

          '************
          'COMANDOS SQL
          '************
43        Print #vArq, ""
44        Print #vArq, "[SQL]"
45        For i = 1 To lsvSQL.ListItems.count
46            Print #vArq, "BANCO" & lsvSQL.ListItems(i) & "=" & lsvSQL.ListItems(i).SubItems(1)
47            Print #vArq, "SQL" & lsvSQL.ListItems(i) & "=" & lsvSQL.ListItems(i).SubItems(2)
48        Next
       
          '***
          'BAT
          '***
49        Print #vArq, ""
50        Print #vArq, "[BAT]"
          
51        For i = 1 To lsvBAT.ListItems.count
52            Print #vArq, "BAT" & lsvBAT.ListItems(i) & "=" & IIf(InStr(1, UCase(lsvBAT.ListItems(i).SubItems(1)), ".BAT") > 0, lsvBAT.ListItems(i).SubItems(1), lsvBAT.ListItems(i).SubItems(1) & ".BAT")
53        Next
          
          '***
          'EXE
          '***
54        Print #vArq, ""
55        Print #vArq, "[EXE]"
          
56        For i = 1 To lsvEXE.ListItems.count
57            Print #vArq, "EXE" & lsvEXE.ListItems(i) & "=" & IIf(InStr(1, lsvEXE.ListItems(i).SubItems(1), ".EXE") > 0, lsvEXE.ListItems(i).SubItems(1), lsvEXE.ListItems(i).SubItems(1) & ".EXE")
58        Next
          
          '*****
          'MOVER
          '*****
59        Print #vArq, ""
60        Print #vArq, "[MOVER]"
          
61        For i = 1 To lsvMover.ListItems.count
62            Print #vArq, "MOVER" & lsvMover.ListItems(i) & "=" & UCase(lsvMover.ListItems(i).SubItems(1)) & ";" & UCase(lsvMover.ListItems(i).SubItems(2)) & ";" & UCase(lsvMover.ListItems(i).SubItems(3))
63        Next
          
          
64        Close #vArq
          
'61        FileCopy txtArquivosparaZipar & "PAAC.INI", Path_drv & "\OUT\PAAC.INI"
65      FileCopy txtDriveFTP & "\OUT\TMP\PAAC.INI", Me.txtArquivosparaZipar & "PAAC.INI"

'Se a check do FIL240.mdb estiver marcada, ent�o copiar para o diretorio txtarquivosparazipar
'62        If chkFIL240.Value = 1 Then FileCopy App.Path & "\OUT\FIL240.MDB", txtArquivosparaZipar & "FIL240.MDB"

66        If Dir(Me.txtArquivosparaZipar & "\FIL240.MDB", vbArchive) = "" Then
67           MsgBox "Copie o arquivo FIL240.MDB para a pasta: " & UCase(txtArquivosparaZipar)
68           Exit Sub
69        Else
70           If chkFIL240.Value = 1 Then FileCopy "C:\OUT\FIL240.MDB", txtDriveFTP & "\OUT\TMP\FIL240.MDB"
71           If chkFIL240.Value = 1 Then FileCopy "C:\OUT\FIL240.MDB", txtArquivosparaZipar & "FIL240.MDB"
72        End If

73        sscIniciar.Enabled = True
          
74        MsgBox "Arquivo PAAC.INI gerado com Sucesso !" & vbCrLf & vbCrLf & "Verifique se o arquivo est� OK.", vbInformation, "Aten��o"
          
75        Shell "notepad.exe " & Me.txtArquivosparaZipar & "PAAC.INI", vbNormalFocus
          
Trata_Erro:
76        If Err.Number <> 0 Then
77            MsgBox "Sub Command9_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
78        End If

End Sub

Private Sub Form_Load()

    Me.Guias.Tab = 0


   '-- Licensa BWZip
    MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    
    StatusBar.Panels(2).Text = "Clique no bot�o iniciar para compactar!"
    Carregar_PAACREPRES
    
    Preencher_Guias
    
    Me.Guias.Tab = 0
    
    Me.Caption = Me.Caption & " - Vers�o:" & App.Major & "." & App.Minor & "." & App.Revision
    
End Sub

Private Sub Guias_Click(PreviousTab As Integer)
    If Guias.Tab = 4 Then
        txtOrdemMover = lsvMover.ListItems.count + 1
    End If
End Sub

Private Sub lsvBAT_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtBat = lsvBAT.SelectedItem
End Sub

Private Sub lsvEXE_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtEXE = lsvEXE.SelectedItem
End Sub

Private Sub lsvSQL_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtOrdem = lsvSQL.SelectedItem
    txtbanco = lsvSQL.SelectedItem.SubItems(1)
    txtSQL = lsvSQL.SelectedItem.SubItems(2)
End Sub

Private Sub lsvVersoes_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtPrograma = lsvVersoes.SelectedItem
    txtVersao = lsvVersoes.SelectedItem.SubItems(1)
End Sub

Private Sub sscIniciar_Click()
1         On Error GoTo Trata_Erro
          
          Dim Arquivo As String
          Dim num_arq As Integer
          Dim vCampos() As String

2         If MsgBox("Confirma a gera��o da Carga ?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
3             Exit Sub
4         End If

5         ChDir txtDriveFTP & "\OUT\TMP"

'6         If Dir(txtArquivosparaZipar & "TMP\*.*") <> "" Then Kill txtArquivosparaZipar & "TMP\*.*"
6         If Dir(txtDriveFTP & "\OUT\TMP\*.*") <> "" Then Kill txtDriveFTP & "\OUT\TMP\*.*"
          
7         If Dir(txtArquivosparaZipar & "FIL240.MDB") = "" Then
8           MsgBox "Arquivo FIL240.MDB n�o econtrado no caminho " & txtArquivosparaZipar, vbCritical, "Aten��o"
9           Exit Sub
10        End If
11        FileCopy txtArquivosparaZipar & "FIL240.MDB", txtDriveFTP & "\OUT\TMP\FIL240.MDB"
12        FileCopy txtArquivosparaZipar & "PAAC.INI", txtDriveFTP & "\OUT\TMP\PAAC.INI"

13        If Dir(txtArquivosparaZipar & "PAAC.INI") = "" Then
14            If MsgBox("Deseja Continuar sem o PAAC.INI ?", vbQuestion + vbYesNo) = True Then
                  'Verificar se o Arquivo PAAC.INI est� com a data de hoje
15                If Format(FileDateTime(txtCargaZip & "PAAC.INI"), "DD/MM/YY") <> Format(Now, "DD/MM/YY") Then
16                    MsgBox "Voc� tem que criar o arquivo PAAC.INI", vbInformation, "Aten��o"
17                    Exit Sub
18                End If
              
19                If Dir(txtArquivosparaZipar & "PAAC.INI") = "" Then
20                    MsgBox "Copie o Arquivo PAAC.INI para a pasta " & txtArquivosparaZipar
21                    Exit Sub
22                End If
                  
23                If Format(FileDateTime(txtArquivosparaZipar & "PAAC.INI"), "DD/MM/YY") <> Format(Now, "DD/MM/YY") Then
24                    MsgBox "Voc� tem que copiar o arquivo PAAC.INI correto para a pasta " & txtArquivosparaZipar, vbInformation, "Aten��o"
25                    Exit Sub
26                End If
27            End If
28        End If

29        StatusBar.Panels(2).Text = "Iniciando a compacta��o dos arquivos!"
         
         '**************************
         'busca informacoes sobre FTP origem/destino
         '**************************

      '*****
      'PAACs
      '*****
30        For i = 0 To lstPAACs.ListCount - 1
31            If lstPAACs.Selected(i) = False Then GoTo proximo
32            vCampos = Split(lstPAACs.List(i), ";")
33            Arquivo = "CAR" & Format(vCampos(0), "0000")
34            StatusBar.Panels(2).Text = "Filial: " & vCampos(0)
              
             'MONTA O ARQUIVO DE HEADER DA CARGA
35            num_arq = FreeFile
36            Open txtDriveFTP & "\OUT\" & Arquivo & ".HDR" For Output As #num_arq
              
37            Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
38            Print #num_arq, "TO        =" & vCampos(2)
39            Print #num_arq, "FROM      =DPKPEDID"
40            Print #num_arq, "DESCRICAO =" & "#CAR"; Format(Val(vCampos(0)), "0000")
41            Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
             'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
42            Print #num_arq, "NRQUEBRA  =0"
              
43            Close #num_arq
              
             'COMPACTAR ARQUIVO CAR*.ZIP NO DIRET�RIO P\OUT
      '        File2.Path = txtArquivosparaZipar
      '        File2.Refresh

      '        Do While File1.ListCount <> File2.ListCount
      '        Loop
              
              'Copiar somente os arquivos selecionados na list.
              
44            For arq = 1 To lsvFileList.ListItems.count
45                If lsvFileList.ListItems(arq).Checked = True Then
46                    FileCopy txtArquivosparaZipar & lsvFileList.ListItems(arq), Me.txtDriveFTP & "\OUT\TMP\" & lsvFileList.ListItems(arq)
47                End If
48            Next
49            MaqZip1.ZipCompress Me.txtDriveFTP & "\OUT\TMP", "*.*", txtDriveFTP & "\OUT", Arquivo & ".ZIP", "-6"

50            FileCopy txtDriveFTP & "\OUT\" & Arquivo & ".ZIP", Me.txtArquivosparaZipar & Arquivo & ".ZIP"

proximo:
51         Next
      '**************
      'Representantes
      '**************
52        For i = 0 To lstRepresentantes.ListCount - 1
              
53            If lstRepresentantes.Selected(i) = False Then GoTo proximo1
              
54            vCampos = Split(lstRepresentantes.List(i), ";")
55            Arquivo = "CAR" & Format(vCampos(0), "0000")
56            StatusBar.Panels(2).Text = "Representante: " & vCampos(0)
              
             'MONTA O ARQUIVO DE HEADER DA CARGA
57            num_arq = FreeFile
58            Open txtDriveFTP & "\OUT\" & Arquivo & ".HDR" For Output As #num_arq
              
59            Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
60            Print #num_arq, "TO        =" & vCampos(2)
61            Print #num_arq, "FROM      =DPKPEDID"
62            Print #num_arq, "DESCRICAO =" & "#CAR"; Format(vCampos(0), "0000")
63            Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
             'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
64            Print #num_arq, "NRQUEBRA  =0"
              
65            Close #num_arq

             'COMPACTAR ARQUIVO CAR*.ZIP NO DIRET�RIO P\OUT
66            File1.Path = Me.txtDriveFTP & "\OUT\TMP"
67            File1.Refresh
              
68            Do While File1.ListCount <> File1.ListCount
69               DoEvents
70            Loop
              
71            For arq = 1 To lsvFileList.ListItems.count
72                If lsvFileList.ListItems(arq).Checked = True Then
73                    FileCopy txtArquivosparaZipar & lsvFileList.ListItems(arq), Me.txtDriveFTP & "\OUT\TMP\" & lsvFileList.ListItems(arq)
74                End If
75            Next
              
76            MaqZip1.ZipCompress Me.txtDriveFTP & "\OUT\TMP", "*.*", txtDriveFTP & "\OUT", Arquivo & ".ZIP", "-6"
             
proximo1:
77         Next
          
78         StatusBar.Panels(2).Text = "Compacta��o conclu�da. Clique no bot�o finalizar!"

Trata_Erro:
79        If Err.Number = 76 Then
80            Resume Next
81        ElseIf Err.Number <> 0 Then
82            MsgBox "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
83        End If
End Sub

Private Sub sscmdFim_Click()
    
    End
    
End Sub


Sub Carregar_PAACREPRES()
    Dim VarSession As Object             'VARI�VEL DE SESS�O ORACLE
    Dim VarDB As Object                  'VARI�VEL DO BANCO DO ORACLE (DEFINE_BANCO)
    Dim reg As Integer
    Dim rst As Object

    Const ORATYPE_CURSOR = 102
    Const ORADYN_NO_BLANKSTRIP = &H2&
    Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
    Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
    Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
    Const ORATYPE_NUMBER = 2

    Set VarSession = CreateObject("oracleinproCServer.xorasession")
    Set VarDB = VarSession.OpenDatabase("PRODUCAO", "FIL430/PROD", 0&)
    
   '-- Declarando o Bind de Cursor
    VarDB.Parameters.Remove "vCursor"
    VarDB.Parameters.Add "vCursor", 0, ORAPARM_BOTH
    VarDB.Parameters("vCursor").serverType = ORATYPE_CURSOR
    VarDB.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    VarDB.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    lstPAACs.Clear
    lstRepresentantes.Clear
    
    Set rst = VarDB.CreateDynaset("select B.Cod_filial,0 Cic_Gerente,E_mail,FL_VDR FRom   paac.controle_paac A, " & _
                                  " paac.filial_base B, paac.deposito_filial c Where a.cod_filial = B.cod_filial " & _
                                  " and    b.cod_filial = c.cod_filial and    c.fl_base = 'S' ORDER BY B.COD_FILIAL", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstPAACs.AddItem rst!cod_filial & vbTab & vbTab & ";" & rst!Cic_Gerente & ";" & rst!E_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

    Set rst = VarDB.CreateDynaset("select B.Cod_Repres, 0 Cic_Gerente,E_mail,FL_VDR From   paac.controle_representante A," & _
                                  " paac.representante_base B, paac.deposito_repres c Where  a.cod_repres = b.cod_repres and " & _
                                  " B.Cod_Repres = c.cod_repres and c.fl_base = 'S' ORDER BY A.COD_REPRES", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstRepresentantes.AddItem rst!cod_repres & vbTab & vbTab & ";" & rst!Cic_Gerente & ";" & rst!E_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

End Sub

Sub Preencher_Guias()

1         On Error GoTo Trata_Erro

          Dim vArq As Integer
          Dim vConteudo As String
          Dim vLinha As String
          Dim litem As ListItem
          Dim vBanco As String
          Dim vSql As String
          Dim vBat As String
          Dim vExe As String
          Dim vMover As String
          
2         vArq = FreeFile()
          
3         If Dir(txtArquivosparaZipar & "PAAC.INI") = "" Then Exit Sub
4         Open txtArquivosparaZipar & "PAAC.INI" For Input As #vArq
          
5         lsvBAT.ListItems.Clear
6         lsvEXE.ListItems.Clear
7         lsvVersoes.ListItems.Clear
8         lsvSQL.ListItems.Clear
          
9         While Not EOF(vArq)
10            Input #vArq, vLinha
              '*******
              'VERSOES
              '*******
11            If vLinha = "[VERSAO]" Then
12                Input #vArq, vLinha
13                While vLinha <> ""
14                        Set litem = lsvVersoes.ListItems.Add
15                        litem = Left(vLinha, InStr(1, vLinha, "=") - 1)
16                        litem.SubItems(1) = Right(vLinha, 8)
17                        Input #vArq, vLinha
18                Wend
19            End If

20        Wend
21        Close vArq

          '****
          'SQL
          '****
22        For i = 1 To 1000
23            vBanco = GetKeyVal(txtArquivosparaZipar & "PAAC.INI", "SQL", "BANCO" & i)
24            vSql = GetKeyVal(txtArquivosparaZipar & "PAAC.INI", "SQL", "SQL" & i)
25            If vBanco = "" Then Exit For
26            Set litem = lsvSQL.ListItems.Add
27            litem = i
28            litem.SubItems(1) = vBanco
29            litem.SubItems(2) = vSql
30        Next

          '****
          'BAT
          '****
31        For i = 1 To 100
32            vBat = GetKeyVal(txtArquivosparaZipar & "PAAC.INI", "BAT", "BAT" & i)
33            If vBat = "" Then Exit For
34            Set litem = lsvBAT.ListItems.Add
35            litem = i
36            litem.SubItems(1) = vBat
37        Next
          
          '****
          'EXE
          '****
38        For i = 1 To 100
39            vExe = GetKeyVal(txtArquivosparaZipar & "PAAC.INI", "EXE", "EXE" & i)
40            If vExe = "" Then Exit For
41            Set litem = lsvEXE.ListItems.Add
42            litem = i
43            litem.SubItems(1) = vExe
44        Next

          '****
          'MOVER
          '****
45        For i = 1 To 100
46            vMover = GetKeyVal(txtArquivosparaZipar & "PAAC.INI", "MOVER", "MOVER" & i)
47            If vMover = "" Then Exit For
48            lsvMover.ListItems.Clear
49            Set litem = lsvMover.ListItems.Add
50            litem = i
51            litem.SubItems(1) = vMover
52        Next

Trata_Erro:
53        If Err.Number <> 0 And Err.Number <> 62 Then
54           MsgBox "Sub Preencher_Guias" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
55        End If
End Sub

Private Sub txtbanco_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtBat_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub



Private Sub txtEXE_KeyPress(KeyAscii As Integer)
   KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtPrograma_KeyPress(KeyAscii As Integer)
    If KeyAscii = 46 Then
        KeyAscii = 0
    Else
        KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End If
End Sub

Private Sub txtVersao_KeyPress(KeyAscii As Integer)
    If KeyAscii = 46 Or KeyAscii = 8 Then Exit Sub
    If InStr(1, "0123456789", Chr(KeyAscii)) = 0 Then
        KeyAscii = 0
    End If
End Sub

Function Pegar_Sequencia(Texto As String) As Integer
    Dim Numero As String
    For i = 1 To Len(Texto)
        If IsNumeric(Mid(Texto, i, 1)) Then
            Numero = Numero & Mid(Texto, i, 1)
        End If
    Next
    Pegar_Sequencia = Numero
End Function

Sub ReSequenciar(lsv As MSComctlLib.ListView)
    Dim litem As ListItem
    For i = 1 To lsv.ListItems.count
        If lsv.ListItems(i).SubItems(1) <> "" Then
            lsv.ListItems(i).Selected = True
            Set litem = lsv.SelectedItem
            litem = i
        End If
    Next
End Sub
