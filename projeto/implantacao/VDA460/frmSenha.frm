VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmSenha 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "AN�LISE DE OP"
   ClientHeight    =   2100
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   3210
   ControlBox      =   0   'False
   Icon            =   "frmSenha.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2100
   ScaleWidth      =   3210
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Informa��es do usu�rio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1140
      Left            =   30
      TabIndex        =   4
      Top             =   930
      Width           =   3120
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         TabIndex        =   0
         Top             =   270
         Width           =   2130
      End
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   855
         PasswordChar    =   "l"
         TabIndex        =   1
         Top             =   675
         Width           =   2130
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   5
         Top             =   720
         Width           =   690
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   7
      Top             =   795
      Width           =   3120
      _ExtentX        =   5503
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   840
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Sair"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSenha.frx":0442
      PICN            =   "frmSenha.frx":045E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd sscmdEntrar 
      Height          =   690
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Conectar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmSenha.frx":0D38
      PICN            =   "frmSenha.frx":0D54
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmSenha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdSair_Click()
    End
End Sub


Private Sub sscmdEntrar_Click()
    Dim V_SQL As String

    If txtUsuario = "" Or txtSenha = "" Then
        MsgBox "Entre com Login e senha da intranet para entrar no sistema"
        lngCod_Usuario = 0
        Exit Sub
    End If

    '**** Busca Usu�rio ****'
   ' usuario = Left(UCase(BuscaUSERNAME), 6)
     strUsuario = UCase(CStr(BuscaUSERNAME))
   
    lngCod_Usuario = 0
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "vErro"
    vBanco.Parameters.Add "vErro", 0, 2
    
    vBanco.Parameters.Remove "LOGIN"
    vBanco.Parameters.Add "LOGIN", txtUsuario, 1
    
    vBanco.Parameters.Remove "SENHA"
    vBanco.Parameters.Add "SENHA", txtSenha, 1
    
   
    
    vSql = "PRODUCAO.PCK_VDA070.pr_senha(:PM_CURSOR1,:login,:senha,:vErro)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF() Then
        MsgBox "Usu�rio/Senha n�o confere, favor verificar."
        Screen.MousePointer = 0
       End
    End If

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

     Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        lngCod_Usuario = 0
    Else
        lngCod_Usuario = vObjOracle(0)
    End If
    
    If lngCod_Usuario = 0 Then
        MsgBox "Login/Senha inv�lidos n�o � poss�vel abrir o programa", vbCritical, "Aten��o"
        Exit Sub
    End If
    
    

    Unload frmSenha

End Sub

Private Sub TXTSENHA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
       sscmdEntrar_Click
    End If
    KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtusuario_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub


