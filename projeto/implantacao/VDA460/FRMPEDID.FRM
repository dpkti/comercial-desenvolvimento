VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmPedidos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Notas Fiscais"
   ClientHeight    =   5385
   ClientLeft      =   75
   ClientTop       =   1215
   ClientWidth     =   9465
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5385
   ScaleWidth      =   9465
   Begin VB.PictureBox Picture2 
      BackColor       =   &H80000009&
      Height          =   255
      Left            =   600
      Picture         =   "FRMPEDID.frx":0000
      ScaleHeight     =   195
      ScaleWidth      =   315
      TabIndex        =   13
      Top             =   4680
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.ComboBox cmbSituacao 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   4920
      TabIndex        =   10
      Top             =   120
      Width           =   2205
   End
   Begin VB.ComboBox cmbLoja 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1800
      Sorted          =   -1  'True
      TabIndex        =   4
      Top             =   120
      Width           =   1965
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   600
      Picture         =   "FRMPEDID.frx":030A
      ScaleHeight     =   255
      ScaleWidth      =   375
      TabIndex        =   2
      Top             =   4200
      Width           =   375
   End
   Begin MSGrid.Grid grdPedido 
      Height          =   2535
      Left            =   600
      TabIndex        =   0
      Top             =   1200
      Visible         =   0   'False
      Width           =   8295
      _Version        =   65536
      _ExtentX        =   14631
      _ExtentY        =   4471
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      ScrollBars      =   2
      MouseIcon       =   "FRMPEDID.frx":0614
   End
   Begin Bot�o.cmd cmd1 
      Height          =   420
      Left            =   7680
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   120
      Width           =   1140
      _ExtentX        =   2011
      _ExtentY        =   741
      BTYPE           =   3
      TX              =   "Buscar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMPEDID.frx":0630
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   5040
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   4080
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMPEDID.frx":064C
      PICN            =   "FRMPEDID.frx":0668
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSMask.MaskEdBox txtData 
      Height          =   330
      Left            =   4920
      TabIndex        =   12
      Top             =   480
      Width           =   1230
      _ExtentX        =   2170
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      MaxLength       =   10
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Format          =   "dd/mm/yyyy"
      Mask            =   "99/99/9999"
      PromptChar      =   "�"
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      Caption         =   "A Partir de:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3960
      TabIndex        =   11
      Top             =   600
      Width           =   1035
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "Situa��o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3960
      TabIndex        =   9
      Top             =   240
      Width           =   915
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "Pedido analisado pelo Cr�dito"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1200
      TabIndex        =   7
      Top             =   4200
      Width           =   2595
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "Procurando Por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   720
      TabIndex        =   6
      Top             =   840
      Visible         =   0   'False
      Width           =   1395
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Dep�sitos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   720
      TabIndex        =   5
      Top             =   240
      Width           =   915
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2400
      TabIndex        =   1
      Top             =   960
      Visible         =   0   'False
      Width           =   4815
   End
End
Attribute VB_Name = "frmPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strPesquisa As String


Private Sub cmbLoja_GotFocus()
  grdPedido.Visible = False
End Sub



Private Sub cmbSituacao_LostFocus()
  If cmbSituacao = "" Then
    Exit Sub
  End If
  
  'If CInt(Mid(cmbSituacao, 1, 1)) = 9 Then
  '  Label4.Visible = True
  '  txtData.Visible = True
  'Else
  '  Label4.Visible = False
  '  txtData.Visible = False
  'End If
End Sub

Private Sub cmd1_Click()
  Dim ss As Object
  Dim ss1 As Object
  Dim SQL As String
  Dim lngCod_Cliente As Long
  Dim bTp_Transacao As Byte
  Dim i As Integer

 On Error GoTo TrataErro

  Screen.MousePointer = 11


   
  
  grdPedido.Visible = False
  DoEvents
  
  If cmbLoja = "" Then
    MsgBox "Selecione um CD para fazer a consulta", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    Exit Sub
  End If
  
  If cmbSituacao = "" Then
    MsgBox "Selecione uma situacao para fazer a consulta", vbInformation, "Aten��o"
    Screen.MousePointer = 0
    Exit Sub
  End If
  'If CDate(txtData) = True Then
  '  If txtData.Visible Then
  '    MsgBox "Selecione uma data para fazer a consulta", vbInformation, "Aten��o"
  '    Screen.MousePointer = 0
  '    Exit Sub
  '  End If
  'End If
  
  'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
  'Dep�sito 00-TODOS Somente para Situa��o 0-EM ANALISE...
  If Mid(cmbLoja, 1, 2) = "00" And Mid(cmbSituacao, 1, 1) <> "0" Then
    MsgBox "Op��o  00-TODOS  Somente para Situa��o  0-EM An�lise", vbExclamation
    Screen.MousePointer = 0
    Exit Sub
  End If
  
  'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdPedido
    
    grdPedido.Visible = False
  
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").serverType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    
    vBanco.Parameters.Remove "LOJA"
    vBanco.Parameters.Add "LOJA", Mid(cmbLoja, 1, 2), 1
    
    vBanco.Parameters.Remove "BANCO"
    vBanco.Parameters.Add "BANCO", "PRODUCAO.", 1
    
    vBanco.Parameters.Remove "CD"
    vBanco.Parameters.Add "CD", vCD, 1
    
    vBanco.Parameters.Remove "SIT"
    vBanco.Parameters.Add "SIT", CInt(Mid(cmbSituacao, 1, 1)), 1
 
    vBanco.Parameters.Remove "dt"
    
    If Val(Mid(txtData, 1, 2)) > 0 Then
      vBanco.Parameters.Add "dt", Null, 1
    Else
      If CDate(txtData) = True Then
        vBanco.Parameters.Add "dt", Null, 1
      Else
        vBanco.Parameters.Add "dt", CDate(txtData), 1
      End If
   End If
   
    'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
    'vSql = "PRODUCAO.PCK_VDA460.pr_busca_pedido(:PM_CURSOR1,:LOJA,:CD,:BANCO,:SIT,:dt,:PM_CODERRO)"
    If Mid(cmbLoja, 1, 2) = "00" Then
        vSql = "PRODUCAO.PCK_VDA460.pr_busca_todos_pedidos(:PM_CURSOR1,:LOJA,:CD,:BANCO,:SIT,:dt,:PM_CODERRO)"
    Else
        vSql = "PRODUCAO.PCK_VDA460.pr_busca_pedido(:PM_CURSOR1,:LOJA,:CD,:BANCO,:SIT,:dt,:PM_CODERRO)"
    End If
    'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
    
    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF() Then
        MsgBox "N�o existe pedido para esta consulta"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
    Dim vObjOracle3 As oradynaset
    'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
    
    With frmPedidos.grdPedido
        .Cols = 8 'TI-5912
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 1200
        .ColWidth(1) = 700
       ' .ColWidth(1) = 1600
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 3400
        .ColWidth(5) = 1600
        .ColWidth(6) = 500
       ' .ColWidth(6) = 1200
        
        .row = 0
        .col = 0
        .Text = "Nr.Pedido"
        .col = 1
        .Text = "Seq."
        '.Col = 1
        '.Text = "Dep�sito"
        .col = 2
        .Text = "Num. NF"
        .col = 3
        .Text = "Dt.Digita��o"
        .col = 4
        .Text = "Cliente"
        .col = 5
        .Text = "Cidade"
        .col = 6
        .Text = "UF"
      '  .Col = 6
      '  .Text = "DIVISAO"
            
        
        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
        '    lngCod_Cliente =!cod_cliente
        '    bTp_Transacao = ss!tp_dpkblau
        '    SQL = "Select c.nome_fantasia,"
        '    SQL = SQL & " a.nome_cliente,b.nome_cidade,b.cod_uf,"
        '    SQL = SQL & " d.descricao"
        '    SQL = SQL & " From cliente a,"
        '    SQL = SQL & " cidade b, loja c, divisao d"
        '    SQL = SQL & " Where d.tp_divisao = :tp_divisao and"
        '    SQL = SQL & "       c.cod_loja = :loja and "
        '    SQL = SQL & "       a.cod_cliente = :cod_cliente and"
        '    SQL = SQL & "       a.cod_cidade= b.cod_cidade"
  
  '          oradatabase.Parameters.Remove "cod_cliente"
  '          oradatabase.Parameters.Add "cod_cliente", lngCod_Cliente, 1
  '          oradatabase.Parameters.Remove "loja"
  '          oradatabase.Parameters.Add "loja", Trim(Mid(cboLoja, 2, 2)), 1
  '          oradatabase.Parameters.Remove "tp_divisao"
  '          oradatabase.Parameters.Add "tp_divisao", bTp_Transacao, 1

   '         Set ss1 = oradatabase.dbcreatedynaset(SQL, 0)
  
            .row = i
            
            .col = 0
            .ColAlignment(0) = 2
            If vObjOracle!bloq_protesto <> "N" Or _
               vObjOracle!bloq_juros <> "N" Or _
               vObjOracle!bloq_cheque <> "N" Or _
               vObjOracle!bloq_compra <> "N" Or _
               vObjOracle!bloq_duplicata <> "N" Or _
               vObjOracle!bloq_limite <> "N" Or _
               vObjOracle!bloq_saldo <> "N" Or _
               vObjOracle!bloq_clie_novo <> "N" Then
               .Picture = Picture1.Picture
               .Text = vObjOracle!num_pedido
             Else
               .Text = vObjOracle!num_pedido
             End If
           ' .Col = 1
           ' .ColAlignment(1) = 0
           ' .Text = vObjOracle!loja
            .col = 1
            .ColAlignment(1) = 1
            .Text = vObjOracle!seq_pedido
           
            .col = 2
            .ColAlignment(2) = 1
            
            'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
            vBanco.Parameters.Remove "P_CURSOR"
            vBanco.Parameters.Add "P_CURSOR", 0, 3
            vBanco.Parameters("P_CURSOR").serverType = 102
            vBanco.Parameters("P_CURSOR").DynasetOption = &H2&
            vBanco.Parameters("P_CURSOR").DynasetCacheParams 256, 16, 20, 2000, 0
          
            vBanco.Parameters.Remove "P_NUM_PEDIDO"
            vBanco.Parameters.Add "P_NUM_PEDIDO", vObjOracle!num_pedido, 1
            
            vBanco.Parameters.Remove "P_SEQ_PEDIDO"
            vBanco.Parameters.Add "P_SEQ_PEDIDO", vObjOracle!seq_pedido, 1
            
            vBanco.Parameters.Remove "P_CODLOJA"
            vBanco.Parameters.Add "P_CODLOJA", vObjOracle!Loja, 1 'TI-5912
        
            vBanco.Parameters.Remove "P_CODERRO"
            vBanco.Parameters.Add "P_CODERRO", 0, 2
            
            vBanco.Parameters.Remove "P_TXTERRO"
            vBanco.Parameters.Add "P_TXTERRO", "", 2
        
            vSql = "PRODUCAO.PCK_VDA030.PR_SEL_COMPROVANTEDEPOSITOINFO(:P_CURSOR,:P_NUM_PEDIDO,:P_SEQ_PEDIDO,:P_CODLOJA, :P_CODERRO,:P_TXTERRO)"
            
            vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
        
            vErro = IIf(IsNull(vBanco.Parameters("p_coderro")), 0, Val(vBanco.Parameters("p_coderro")))
        
            If vErro <> 0 Then
                MsgBox "Erro: " & vErro & " - Descri��o: " & vBanco.Parameters("p_txterro"), vbCritical
                Exit Sub
            End If
        
            Set vObjOracle3 = vBanco.Parameters("P_CURSOR").Value
        
            If Not vObjOracle3.EOF Then
                .ColWidth(2) = 1100
                .Picture = Picture2.Picture
            End If
            'Emerson - TI-5912 - 03/05/18 - Venda � VISTA DPK - Plano 412 Dep�sito Banc�rio, Novo Plano 413 Cart�o D�bito...
            
            .Text = vObjOracle!num_nota
            .col = 3
            .ColAlignment(3) = 1
            .Text = vObjOracle!dt_digitacao
            .col = 4
            .ColAlignment(4) = 0
            .Text = vObjOracle!cod_cliente & " - " & vObjOracle!nome_cliente
            .col = 5
            .ColAlignment(5) = 0
            .Text = vObjOracle!nome_cidade
            .col = 6
            .ColAlignment(6) = 0
            .Text = vObjOracle!cod_uf
         '   .Col = 6
         '   .ColAlignment(6) = 0
         '   .Text = vObjOracle!DESCRICAO
            'TI-5912
            .col = 7
            .ColAlignment(7) = 0
            .Text = vObjOracle!Loja
            'FIM TI-5912
            
            DoEvents
            vObjOracle.MoveNext
        Next
        .row = 1
    End With
       
    grdPedido.Visible = True
         
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
   If Err = 13 Then
     Screen.MousePointer = 0
     Resume Next
   End If


End Sub

Private Sub cmdSair_Click()
  grdPedido.Visible = False
  frmPedidos.Hide
End Sub

Private Sub Command1_Click()
  grdPedido.Visible = False
  frmPedidos.Hide
End Sub


Private Sub Command2_Click()
End Sub

Private Sub Form_Load()
  Screen.MousePointer = 0
  strFL_sair = "N"
  grdPedido.Visible = False
  
End Sub

Private Sub Form_Unload(Cancel As Integer)
 Set frmPedidos = Nothing
End Sub


Private Sub grdPedido_DblClick()
  Screen.MousePointer = 11
  grdPedido.col = 1
  lngSEQ_PEDIDO = grdPedido.Text
  grdPedido.col = 0
  lngNUM_PEDIDO = grdPedido.Text
  grdPedido.col = 7
  lngCod_Loja = grdPedido.Text 'TI-5912
  'Unload Me
  frmVisPedido.Show 1
  If strFL_sair = "N" Then
    cmd1_Click
  End If
End Sub

Private Sub grdPedido_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdPedido.col = 0
    If KeyAscii = 13 Then
       Screen.MousePointer = 11
       grdPedido.col = 0
       lngNUM_NOTA = grdPedido.Text
       grdPedido.col = 1
       lngCod_Loja = Mid(grdPedido.Text, 1, 3)
       Unload Me
       frmVisPedido.Show 1
  ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
               ' lblPesq.Visible = False
                grdPedido.row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdPedido
                    iLinha = .row
                    j = .row - 1
                    For i = j To 1 Step -1
                        .row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .row
                            Exit For
                        End If
                    Next
                    .row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
     '   lblPesq.Visible = False
        grdPedido.row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                 '   lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdPedido
                        iLinha = .row
                        If tam = 0 Then
                            .row = 0
                            For i = 1 To .Rows - 1
                                .row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .row
                            For i = j To .Rows - 1
                                .row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdPedido.row <> iLinha Then
                            .row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub

