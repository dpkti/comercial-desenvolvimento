Attribute VB_Name = "modGenericas"
Option Explicit

Public vData As Long
Public vNomeIni As String
Public vSessaoDia As String

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public vSessao As Object
Public vBanco As Object

Dim i As Long
Dim regs As Long
Public VarRet As Object
Public vArqsExcluir(80) As String
Public vDe As String
Public vPara As String

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Public Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    'Returns info from an INI file
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "Urvaz System": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Public Function AddToINI(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal KeyValue As String) As Integer
    'Add info to an INI file
    'Function returns 1 if successful and 0 if unsuccessful
    WritePrivateProfileString Section, Key, KeyValue, FileName
    AddToINI = 1
End Function

Public Sub ANTECIPACAO_TRIBUTARIA_dbf()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela = "ANTECIPACAO_TRIBUTARIA - Consultando"
3         frmGerar.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_ANTECIPACAO_TRIBUTARIA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
           
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "ANTECIPA"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela = "ANTECIPACAO_TRIBUTARIA - Inserindo Registros"
10        frmGerar.Refresh

11        Do While Not VarRet.EOF
12             frmGerar.DataInsert.Recordset.AddNew
13             frmGerar.DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
14             frmGerar.DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
15             frmGerar.DataInsert.Recordset("pc_margem") = VarRet!PC_MARGEM_LUCRO
16             frmGerar.DataInsert.UpdateRecord
17             VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop

Sair:
          AddToINI vNomeIni, vSessaoDia, "ANTECIPACAO_TRIBUTARIA", "S"
24        ArqExcluir "ANTECIPA"
25        frmGerar.lblRegistros.Caption = ""
26        frmGerar.lblTabela = ""
27        frmGerar.DataInsert.RecordSource = ""
28        frmGerar.DataInsert.Refresh
29        frmGerar.Refresh
          
30        Mover_Copiar 2, vDe, vPara, "ANTECIPA.DBF"

          Set VarRet = Nothing

Trata_Erro:
31    If Err.Number <> 0 Then
32      MsgBox "Sub Antecipacao_Tributaria" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33    End If
End Sub


Public Sub APLICACAO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "APLICACAO - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL "BEGIN PRODSIC.PCK_FIL400.PR_APLICACAO(:VCURSOR); END;"
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "APLICACA"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "APLICACAO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
14           frmGerar.DataInsert.Recordset("cod_catego") = VarRet!cod_categoria
15           frmGerar.DataInsert.Recordset("cod_montad") = VarRet!cod_montadora
16           frmGerar.DataInsert.Recordset("sequencia") = VarRet!sequencia
17           frmGerar.DataInsert.Recordset("cod_origin") = VarRet!cod_original
18           frmGerar.DataInsert.Recordset("desc_aplic") = VarRet!desc_aplicacao
19           frmGerar.DataInsert.UpdateRecord
20           VarRet.MoveNext
21           frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
22           If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
23              DoEvents
24              frmGerar.Refresh
25           End If
             frmGerar.lblRegistros.Refresh
26        Loop

Sair:
          AddToINI vNomeIni, vSessaoDia, "APLICACAO", "S"
27        ArqExcluir "APLICACA"
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.lblTabela = ""
30        frmGerar.DataInsert.RecordSource = ""
31        frmGerar.DataInsert.Refresh
32        frmGerar.Refresh
          
33        Mover_Copiar 2, vDe, vPara, "APLICACA.DBF"
          
          Set VarRet = Nothing
          
Trata_Erro:
34        If Err.Number <> 0 Then
35            MsgBox "Sub Aplicacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
36        End If

End Sub

Public Sub BANCO_DBF()
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "BANCO - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_BANCO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "BANCO"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "BANCO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_banco") = VarRet!cod_banco
14           frmGerar.DataInsert.Recordset("sigla") = VarRet!Sigla
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext

17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "BANCO", "S"
23        ArqExcluir "BANCO"
24        frmGerar.lblRegistros.Caption = ""
25        frmGerar.lblTabela = ""
26        frmGerar.DataInsert.RecordSource = ""
27        frmGerar.DataInsert.Refresh
28        frmGerar.Refresh
29        Mover_Copiar 2, vDe, vPara, "BANCO.DBF"

          Set VarRet = Nothing

Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub Banco" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub CANCEL_PEDNOTA_dbf()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela = "CANCEL PEDNOTA - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CANCEL_PEDNOTA(:VCURSOR); END;")

5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "CANCEL_P"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela = "CANCEL PEDNOTA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_cancel") = VarRet!cod_cancel
14           frmGerar.DataInsert.Recordset("desc_cance") = VarRet!desc_cancel
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext

17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "CANCEL_PEDNOTA", "S"
23        ArqExcluir "CANCEL_P"
24        frmGerar.lblRegistros.Caption = ""
25        frmGerar.lblTabela = ""
26        frmGerar.DataInsert.RecordSource = ""
27        frmGerar.DataInsert.Refresh
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "CANCEL_P.DBF"

          Set VarRet = Nothing

Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub CANCEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub


Public Sub CATEG_SINAL_dbf()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "CATEG_SINAL - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CATEG_SINAL(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "CATEG_S"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "CATEG_SINAL - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("categoria") = VarRet!categoria
14           frmGerar.DataInsert.Recordset("sinal") = VarRet!sinal
15           frmGerar.DataInsert.UpdateRecord

16           VarRet.MoveNext

17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "CATEG_SINAL", "S"
23        ArqExcluir "CATEG_S"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "CATEG_S.DBF"

          Set VarRet = Nothing

Trata_Erro:
30    If Err.Number <> 0 Then
31      MsgBox "Sub CATEG_SINAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32    End If

End Sub

Public Sub CIDADE_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = " CIDADE - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CIDADE(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "CIDADE"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = " CIDADE - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_cidade") = VarRet!cod_cidade
14           frmGerar.DataInsert.Recordset("nome_cidad") = VarRet!nome_cidade
15           frmGerar.DataInsert.Recordset("tp_cidade") = VarRet!tp_cidade
16           frmGerar.DataInsert.Recordset("cod_uf") = VarRet!cod_uf
17           frmGerar.DataInsert.Recordset("cod_regiao") = VarRet!cod_regiao
18           frmGerar.DataInsert.Recordset("cod_subreg") = VarRet!cod_subregiao
19           frmGerar.DataInsert.Recordset("caracteris") = VarRet!caracteristica
20           frmGerar.DataInsert.UpdateRecord
21           VarRet.MoveNext

22             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
23             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
24                DoEvents
25                frmGerar.Refresh
26             End If
               frmGerar.lblRegistros.Refresh
27        Loop

Sair:
          AddToINI vNomeIni, vSessaoDia, "CIDADE", "S"
28        ArqExcluir "CIDADE"
29        frmGerar.DataInsert.RecordSource = ""
30        frmGerar.DataInsert.Refresh
31        frmGerar.lblTabela = ""
32        frmGerar.lblRegistros.Caption = ""
33        frmGerar.Refresh

34        Mover_Copiar 2, vDe, vPara, "CIDADE.DBF"

          Set VarRet = Nothing

Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub Cidade" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If

End Sub

Public Sub CLASS_ANTEC_ENTRADA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "CLASS_ANTEC_ENTRADA - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLASS_ANTEC_ENTRADA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "CLASSANT"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "CLASS_ANTEC_ENTRADA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
14           frmGerar.DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
15           frmGerar.DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
16           frmGerar.DataInsert.Recordset("cod_trib") = VarRet!cod_trib
17           frmGerar.DataInsert.Recordset("PC_MARG_LU") = VarRet!PC_MARGEM_LUCRO
18           frmGerar.DataInsert.UpdateRecord
19           VarRet.MoveNext

20             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
21             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
22                DoEvents
23                frmGerar.Refresh
24             End If
               frmGerar.lblRegistros.Refresh
25        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "CLASS_ANTEC_ENTRADA", "S"
26        ArqExcluir "CLASSANT"
27        frmGerar.DataInsert.RecordSource = ""
28        frmGerar.DataInsert.Refresh
29        frmGerar.lblTabela = ""
30        frmGerar.lblRegistros.Caption = ""
31        frmGerar.Refresh
32        Mover_Copiar 2, vDe, vPara, "CLASSANT.DBF"
          Set VarRet = Nothing
Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub Class_Antec_Entrada" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If
End Sub

Public Sub CLASS_FISCAL_RED_BASE_DBF()

1         On Error GoTo Trata_Erro

2         frmGerar.lblTabela.Caption = "CLASS_FISCAL_RED_BASE - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLASS_FISCAL_RED_BASE(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "RED_BASE"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "CLASS_FISCAL_RED_BASE - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("COD_UF_ORI") = VarRet!COD_UF_ORIGEM
14           frmGerar.DataInsert.Recordset("COD_UF_DES") = VarRet!COD_UF_DESTINO
15           frmGerar.DataInsert.Recordset("CLASS_FISC") = VarRet!CLASS_FISCAL
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext

18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop

Sair:
          AddToINI vNomeIni, vSessaoDia, "CLASS_FISCAL_RED_BASE", "S"
24        ArqExcluir "RED_BASE"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh
          
30        Mover_Copiar 2, vDe, vPara, "RED_BASE.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub Class_Fiscal_Red_Base" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If
End Sub

Public Sub CLIE_MENSAGEM_DBF()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela = "CLIE_MENSAGEM - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLIE_MENSAGEM(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "CLIE_MEN"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela = "CLIE_MENSAGEM - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_mensag") = VarRet!cod_mensagem
14           frmGerar.DataInsert.Recordset("desc_mens") = VarRet!desc_mens
15           frmGerar.DataInsert.Recordset("fl_bloquei") = VarRet!fl_bloqueio
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext

18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "CLIE_MENSAGEM", "S"
24        ArqExcluir "CLIE_MEN"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "CLIE_MEN.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub Clie_mensagem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Public Sub DATAS_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "DATAS - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_DATAS(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "DATAS"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "DATAS - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("dt_real") = VarRet!dt_real
14           frmGerar.DataInsert.Recordset("dt_faturam") = VarRet!dt_faturamento
15           frmGerar.DataInsert.Recordset("dt_ini_fec") = VarRet!dt_ini_fech_mensal
16           frmGerar.DataInsert.Recordset("dt_fin_fec") = VarRet!dt_fin_fech_mensal
17           frmGerar.DataInsert.Recordset("dt_feri_1") = VarRet!dt_feriado_1
18           frmGerar.DataInsert.Recordset("dt_feri_2") = VarRet!dt_feriado_2
19           frmGerar.DataInsert.Recordset("fl_liber") = VarRet!fl_liber
20           frmGerar.DataInsert.Recordset("dt_fora_se") = VarRet!dt_fora_semana
21           frmGerar.DataInsert.Recordset("dt_ini_fd") = VarRet!dt_ini_fech_dia
22           frmGerar.DataInsert.UpdateRecord
23           VarRet.MoveNext

24             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
25             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
26                DoEvents
27                frmGerar.Refresh
28             End If
               frmGerar.lblRegistros.Refresh
29        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "DATAS", "S"
30        ArqExcluir "DATAS"
31        frmGerar.DataInsert.RecordSource = ""
32        frmGerar.DataInsert.Refresh
33        frmGerar.lblTabela = ""
34        frmGerar.lblRegistros.Caption = ""
35        frmGerar.Refresh

36        Mover_Copiar 2, vDe, vPara, "DATAS.DBF"
          Set VarRet = Nothing
Trata_Erro:
37        If Err.Number <> 0 Then
38            MsgBox "Sub Datas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If

End Sub

Public Sub DOLAR_DIARIO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "DOLAR DIARIO - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_DOLAR_DIARIO(:VCURSOR); END;")

5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "DOLAR_DI"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "DOLAR DIARIO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("data_uss") = VarRet!data_uss
14           frmGerar.DataInsert.Recordset("valor_uss") = VarRet!valor_uss
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "DOLAR_DIARIO", "S"
23        ArqExcluir "DOLAR_DI"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "DOLAR_DI.DBF"
          Set VarRet = Nothing
Trata_Erro:
30    If Err.Number <> 0 Then
31      MsgBox "Sub Dolar_Diario" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32    End If

End Sub

Public Sub EMBALADOR_DBF()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "EMBALADOR - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_EMBALADOR(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "EMBALAD"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "EMBALADOR - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_embal") = VarRet!cod_embal
14           frmGerar.DataInsert.Recordset("nome_embal") = VarRet!nome_embal
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "EMBALADOR", "S"
23        ArqExcluir "EMBALAD"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "EMBALAD.DBF"
          Set VarRet = Nothing
Trata_Erro:
30    If Err.Number <> 0 Then
31      MsgBox "Sub Embalador" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32    End If

End Sub

Public Sub Filial_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "FILIAL - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FILIAL(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "FILIAL"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "FILIAL - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_filial") = VarRet!cod_filial
14           frmGerar.DataInsert.Recordset("sigla") = VarRet!Sigla
15           frmGerar.DataInsert.Recordset("tp_filial") = VarRet!tp_filial
16           frmGerar.DataInsert.Recordset("cod_franqu") = VarRet!cod_franqueador
17           frmGerar.DataInsert.Recordset("cod_region") = VarRet!cod_regional
18           frmGerar.DataInsert.UpdateRecord
19           VarRet.MoveNext
20             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
21             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
22                DoEvents
23                frmGerar.Refresh
24             End If
               frmGerar.lblRegistros.Refresh
25        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "FILIAL", "S"
26        ArqExcluir "FILIAL"
27        frmGerar.DataInsert.RecordSource = ""
28        frmGerar.DataInsert.Refresh
29        frmGerar.lblTabela = ""
30        frmGerar.lblRegistros.Caption = ""
31        frmGerar.Refresh

32        Mover_Copiar 2, vDe, vPara, "FILIAL.DBF"
          Set VarRet = Nothing
Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub FILIAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If
End Sub

Public Sub FORNECEDOR_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "FORNECEDOR - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FORNECEDOR(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "FORNECED"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "FORNECEDOR - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
14           frmGerar.DataInsert.Recordset("sigla") = VarRet!Sigla
15           frmGerar.DataInsert.Recordset("divisao") = VarRet!divisao
16           frmGerar.DataInsert.Recordset("classifica") = VarRet!classificacao
17           frmGerar.DataInsert.Recordset("situacao") = VarRet!situacao
18           frmGerar.DataInsert.UpdateRecord
19           VarRet.MoveNext
20             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
21             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
22                DoEvents
23                frmGerar.Refresh
24             End If
               frmGerar.lblRegistros.Refresh
25        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "FORNECEDOR", "S"
26        ArqExcluir "FORNECED"
27        frmGerar.DataInsert.RecordSource = ""
28        frmGerar.DataInsert.Refresh
29        frmGerar.lblTabela = ""
30        frmGerar.lblRegistros.Caption = ""
31        frmGerar.Refresh

32        Mover_Copiar 2, vDe, vPara, "FORNECED.DBF"
          Set VarRet = Nothing
Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub Fornecedor" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub FORNECEDOR_ESPECIFICO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "FORNECEDOR ESPECIFICO - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FORNECEDOR_ESPECIFICO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "FORN_ESP"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "FORNECEDOR ESPECIFICO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
14           frmGerar.DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
15           frmGerar.DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
16           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
17           frmGerar.DataInsert.Recordset("fl_dif_icm") = VarRet!fl_dif_icms
18           frmGerar.DataInsert.Recordset("fl_adicion") = VarRet!fl_adicional
19           frmGerar.DataInsert.Recordset("tp_dif_icm") = VarRet!tp_dif_icms
20           frmGerar.DataInsert.UpdateRecord
21           VarRet.MoveNext

22             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
23             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
24                DoEvents
25                frmGerar.Refresh
26             End If
               frmGerar.lblRegistros.Refresh
27        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "FORNECEDOR_ESPECIFICO", "S"
28        ArqExcluir "FORN_ESP"
29        frmGerar.DataInsert.RecordSource = ""
30        frmGerar.DataInsert.Refresh
31        frmGerar.lblTabela = ""
32        frmGerar.lblRegistros.Caption = ""
33        frmGerar.Refresh

34        Mover_Copiar 2, vDe, vPara, "FORN_ESP.DBF"
          Set VarRet = Nothing
Trata_Erro:
35        If Err.Number <> 0 Then
36      MsgBox "Sub FORNECEDOR_ESPECIFICO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If

End Sub

Public Sub FRETE_UF_BLOQ_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "FRETE_UF_BLOQ - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FRETE_UF_BLOQ(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "FRETEUFB"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "FRETE_UF_BLOQ - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf") = VarRet!cod_uf
14           frmGerar.DataInsert.Recordset("cod_transp") = VarRet!cod_transp
15           frmGerar.DataInsert.Recordset("fl_bloquei") = VarRet!fl_bloqueio
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "FRETE_UF_BLOQ", "S"
24        ArqExcluir "FRETEUFB"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "FRETEUFB.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32            MsgBox "Sub Frete_UF_BLOQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Public Sub GRUPO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "GRUPO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_GRUPO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "GRUPO"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "GRUPO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
14           frmGerar.DataInsert.Recordset("desc_grupo") = VarRet!desc_grupo
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "GRUPO", "S"
23        ArqExcluir "GRUPO"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "GRUPO.DBF"

Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub Grupo" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub ITEM_CADASTRO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "ITEM_CADASTRO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_ITEM_CADASTRO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "ITEM_CAD"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "ITEM_CADASTRO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
14           frmGerar.DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
15           frmGerar.DataInsert.Recordset("cod_fabric") = VarRet!COD_FABRICA
16           frmGerar.DataInsert.Recordset("desc_item") = VarRet!desc_item
17           frmGerar.DataInsert.Recordset("cod_linha") = VarRet!cod_linha
18           frmGerar.DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
19           frmGerar.DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
20           frmGerar.DataInsert.Recordset("dt_cadastr") = Format(CDate(VarRet!dt_cadastramento), "DD/MM/YY")
21           frmGerar.DataInsert.Recordset("mascarado") = VarRet!mascarado
22           frmGerar.DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
23           frmGerar.DataInsert.Recordset("peso") = VarRet!peso
24           frmGerar.DataInsert.Recordset("volume") = VarRet!volume
25           frmGerar.DataInsert.Recordset("cod_unidad") = VarRet!cod_unidade
26           frmGerar.DataInsert.Recordset("cod_tribut") = VarRet!cod_tributacao
27           frmGerar.DataInsert.Recordset("cod_trib_i") = VarRet!cod_tributacao_ipi
28           frmGerar.DataInsert.Recordset("pc_ipi") = VarRet!pc_ipi
29           frmGerar.DataInsert.Recordset("cod_proced") = VarRet!cod_procedencia
30           frmGerar.DataInsert.Recordset("cod_dpk_an") = VarRet!cod_dpk_ant
31           frmGerar.DataInsert.Recordset("qtd_minfor") = VarRet!qtd_minforn
32           frmGerar.DataInsert.Recordset("qtd_minvda") = VarRet!qtd_minvda
33           frmGerar.DataInsert.UpdateRecord
34           VarRet.MoveNext
35             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
36             If Val(frmGerar.lblRegistros) Mod 10000 = 0 Then
37                DoEvents
38                frmGerar.Refresh
39             End If
               frmGerar.lblRegistros.Refresh
40        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "ITEM_CADASTRO", "S"
41        ArqExcluir "ITEM_CAD"
42        frmGerar.DataInsert.RecordSource = ""
43        frmGerar.DataInsert.Refresh
44        frmGerar.lblTabela = ""
45        frmGerar.lblRegistros.Caption = ""
46        frmGerar.Refresh
          
47        Mover_Copiar 2, vDe, vPara, "ITEM_CAD.DBF"
          Set VarRet = Nothing
Trata_Erro:
48    If Err.Number <> 0 Then
49      MsgBox "Sub Item_cadastro" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
50    End If

End Sub

Public Sub MONTADORA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "MONTADORA - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_MONTADORA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "MONTADOR"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "MONTADORA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_montad") = VarRet!cod_montadora
14           frmGerar.DataInsert.Recordset("desc_monta") = VarRet!desc_montadora
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "MONTADORA", "S"
23        ArqExcluir "MONTADOR"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "MONTADOR.DBF"
          Set VarRet = Nothing
Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub MONTADORA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub NATUREZA_OPERACAO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "NATUREZA OPERACAO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_NATUREZA_OPERACAO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "NATU_OPE"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "NATUREZA OPERACAO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_nature") = VarRet!cod_natureza
14           frmGerar.DataInsert.Recordset("desc_natur") = VarRet!desc_natureza
15           frmGerar.DataInsert.Recordset("desc_abrev") = VarRet!desc_abreviada
16           frmGerar.DataInsert.Recordset("cod_dest1") = VarRet!cod_cfo_dest1
17           frmGerar.DataInsert.Recordset("cod_dest2") = VarRet!cod_cfo_dest2
18           frmGerar.DataInsert.Recordset("cod_dest3") = VarRet!cod_cfo_dest3
19           frmGerar.DataInsert.Recordset("cod_cfo_op") = VarRet!cod_cfo_oper
20           frmGerar.DataInsert.Recordset("cod_transa") = VarRet!cod_transac
21           frmGerar.DataInsert.Recordset("cod_icms1") = VarRet!cod_trib_icms1
22           frmGerar.DataInsert.Recordset("cod_icms2") = VarRet!cod_trib_icms2
23           frmGerar.DataInsert.Recordset("cod_icms3") = VarRet!cod_trib_icms3
24           frmGerar.DataInsert.Recordset("cod_icms4") = VarRet!cod_trib_icms4
25           frmGerar.DataInsert.Recordset("cod_icms5") = VarRet!cod_trib_icms5
26           frmGerar.DataInsert.Recordset("cod_icms6") = VarRet!cod_trib_icms6
27           frmGerar.DataInsert.Recordset("cod_ipi1") = VarRet!cod_trib_ipi1
28           frmGerar.DataInsert.Recordset("cod_ipi2") = VarRet!cod_trib_ipi2
29           frmGerar.DataInsert.Recordset("cod_ipi3") = VarRet!cod_trib_ipi3
30           frmGerar.DataInsert.Recordset("cod_ipi4") = VarRet!cod_trib_ipi4
31           frmGerar.DataInsert.Recordset("cod_mens1") = VarRet!cod_mensagem1
32           frmGerar.DataInsert.Recordset("cod_mens2") = VarRet!cod_mensagem2
33           frmGerar.DataInsert.Recordset("fl_livro") = VarRet!fl_livro
34           frmGerar.DataInsert.Recordset("fl_cuema") = VarRet!fl_cuema
35           frmGerar.DataInsert.Recordset("fl_estoque") = VarRet!fl_estoque
36           frmGerar.DataInsert.Recordset("fl_estorno") = VarRet!fl_estorno
37           frmGerar.DataInsert.Recordset("fl_tributa") = VarRet!fl_tributacao
38           frmGerar.DataInsert.Recordset("fl_fornece") = VarRet!fl_fornecedor
39           frmGerar.DataInsert.Recordset("fl_vlconta") = VarRet!fl_vlcontabil
40           frmGerar.DataInsert.Recordset("fl_nfitem") = VarRet!fl_nfitem
41           frmGerar.DataInsert.Recordset("fl_quantid") = VarRet!fl_quantidade
42           frmGerar.DataInsert.Recordset("fl_cod_mer") = VarRet!fl_cod_merc
43           frmGerar.DataInsert.Recordset("fl_pcdesc") = VarRet!fl_pcdesc
44           frmGerar.DataInsert.Recordset("fl_contas_") = VarRet!fl_contas_pagar
45           frmGerar.DataInsert.Recordset("fl_tipo_no") = VarRet!fl_tipo_nota
46           frmGerar.DataInsert.Recordset("fl_centro_") = VarRet!fl_centro_custo
47           frmGerar.DataInsert.Recordset("fl_contabi") = VarRet!fl_contabil
48           frmGerar.DataInsert.Recordset("fl_base_re") = VarRet!fl_base_red_icms
49           frmGerar.DataInsert.Recordset("fl_consist") = VarRet!fl_consistencia
50           frmGerar.DataInsert.Recordset("fl_ipi_inc") = VarRet!fl_ipi_incid_icm
51           frmGerar.DataInsert.Recordset("fl_movimen") = VarRet!fl_movimentacao
52           frmGerar.DataInsert.UpdateRecord
53           VarRet.MoveNext
54             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
55             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
56                DoEvents
57                frmGerar.Refresh
58             End If
               frmGerar.lblRegistros.Refresh
59        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "NATUREZA_OPERACAO", "S"
60        ArqExcluir "NATU_OPE"
61        frmGerar.DataInsert.RecordSource = ""
62        frmGerar.DataInsert.Refresh
63        frmGerar.lblTabela = ""
64        frmGerar.lblRegistros.Caption = ""
65        frmGerar.Refresh

66        Mover_Copiar 2, vDe, vPara, "NATU_OPE.DBF"
          Set VarRet = Nothing
Trata_Erro:
67        If Err.Number <> 0 Then
68      MsgBox "Sub Natureza_Operacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
69        End If

End Sub

Public Sub PLANO_PGTO_DBF()

1         On Error GoTo Trata_Erro

2         frmGerar.lblTabela.Caption = "PLANO_PGTO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_PLANO_PGTO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "PLANO_PG"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "PLANO_PGTO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_plano") = VarRet!cod_plano
14           frmGerar.DataInsert.Recordset("desc_plano") = VarRet!desc_plano
15           frmGerar.DataInsert.Recordset("prazo_medi") = VarRet!prazo_medio
16           frmGerar.DataInsert.Recordset("pc_acres_f") = VarRet!pc_acres_fin_dpk
17           frmGerar.DataInsert.Recordset("pc_desc_fi") = VarRet!pc_desc_fin_dpk
18           frmGerar.DataInsert.Recordset("situacao") = VarRet!situacao
19           frmGerar.DataInsert.Recordset("fl_avista") = VarRet!fl_avista
20           frmGerar.DataInsert.Recordset("pc_d_vdr") = VarRet!pc_desc_fin_vdr
21           frmGerar.DataInsert.Recordset("pc_a_bla") = VarRet!pc_acres_fin_blau
22           frmGerar.DataInsert.UpdateRecord
23           VarRet.MoveNext
24             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
25             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
26                DoEvents
27                frmGerar.Refresh
28             End If
               frmGerar.lblRegistros.Refresh
29        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "PLANO_PGTO", "S"
30        ArqExcluir "PLANO_PG"
31        frmGerar.DataInsert.RecordSource = ""
32        frmGerar.DataInsert.Refresh
33        frmGerar.lblTabela = ""
34        frmGerar.lblRegistros.Caption = ""
35        frmGerar.Refresh
          
36        Mover_Copiar 2, vDe, vPara, "PLANO_PG.DBF"
          Set VarRet = Nothing
Trata_Erro:
37        If Err.Number <> 0 Then
38      MsgBox "Sub Plano_Pgto" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If

End Sub

Public Sub R_DPK_EQUIV_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = " R_DPK_EQUIV - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_DPK_EQUIV(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "R_DPK_EQ"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = " R_DPK_EQUIV - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
14           frmGerar.DataInsert.Recordset("cod_dpk_eq") = VarRet!cod_dpk_eq
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "R_DPK_EQUIV", "S"
23        ArqExcluir "R_DPK_EQ"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "R_DPK_EQ.DBF"
          Set VarRet = Nothing
Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub R_DPK_EQUIV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub R_FABRICA_DPK_DBF()

1         On Error GoTo Trata_Erro

2         frmGerar.lblTabela.Caption = " R_FABRICA_DPK - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_FABRICA_DPK(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "R_FABRIC"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = " R_FABRICA_DPK - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_fabric") = VarRet!COD_FABRICA
14           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
15           frmGerar.DataInsert.Recordset("fl_tipo") = VarRet!FL_TIPO
16           frmGerar.DataInsert.Recordset("dt_cadastr") = VarRet!DT_CADASTRO
17           frmGerar.DataInsert.Recordset("Sigla") = VarRet!Sigla
18           frmGerar.DataInsert.UpdateRecord
19           VarRet.MoveNext
20             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
21             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
22                DoEvents
23                frmGerar.Refresh
24             End If
               frmGerar.lblRegistros.Refresh
25        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "R_FABRICA_DPK", "S"
26        ArqExcluir "R_FABRIC"
27        frmGerar.DataInsert.RecordSource = ""
28        frmGerar.DataInsert.Refresh
29        frmGerar.lblTabela = ""
30        frmGerar.lblRegistros.Caption = ""
31        frmGerar.Refresh

32        Mover_Copiar 2, vDe, vPara, "R_FABRIC.DBF"
          Set VarRet = Nothing
Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub R_Fabrica_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If
End Sub

Public Sub R_FILDEP_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "R_FILDEP - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_FIL_DEP(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "R_FILDEP"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "R_FILDEP - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_loja") = VarRet!cod_loja
14           frmGerar.DataInsert.Recordset("cod_filial") = VarRet!cod_filial
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "R_FILDEP", "S"
23        ArqExcluir "R_FILDEP"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "R_FILDEP.DBF"
          Set VarRet = Nothing
Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub R_FILDEP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If
End Sub

Public Sub R_UF_DEPOSITO_DBF()

1         On Error GoTo Trata_Erro

2         frmGerar.lblTabela.Caption = "R_UF_DEPOSITO - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL "BEGIN PRODSIC.PCK_FIL400.PR_R_UF_DEPOSITO(:VCURSOR); END;"
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "R_UF_DEP"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "R_UF_DEPOSITO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf") = VarRet!cod_uf
14           frmGerar.DataInsert.Recordset("cod_loja") = VarRet!cod_loja
15           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "R_UF_DEPOSITO", "S"
24        ArqExcluir "R_UF_DEP"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh
30        Mover_Copiar 2, vDe, vPara, "R_UF_DEP.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub R_UF_DEPOSITO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If
End Sub


Public Sub RESULTADO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "RESULTADO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_RESULTADO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "RESULTAD"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "RESULTADO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_result") = VarRet!cod_resultado
14           frmGerar.DataInsert.Recordset("desc_resul") = VarRet!desc_resultado
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "RESULTADO", "S"
23        ArqExcluir "RESULTAD"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "RESULTAD.DBF"
          Set VarRet = Nothing
Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub Resultado" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub SUBGRUPO_DBF()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "SUBGRUPO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_SUBGRUPO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "SUBGRUPO"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "SUBGRUPO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
14           frmGerar.DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
15           frmGerar.DataInsert.Recordset("desc_subgr") = VarRet!desc_subgrupo
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "SUBGRUPO", "S"
24        ArqExcluir "SUBGRUPO"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "SUBGRUPO.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub SubGrupo" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Public Sub SUBST_TRIBUTARIA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "SUBST_TRIBUTARIA - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_SUBST_TRIBUTARIA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "SUBST_TR"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "SUBST_TRIBUTARIA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
14           frmGerar.DataInsert.Recordset("cod_origem") = VarRet!COD_UF_ORIGEM
15           frmGerar.DataInsert.Recordset("cod_destin") = VarRet!COD_UF_DESTINO
16           frmGerar.DataInsert.Recordset("cod_revend") = VarRet!cod_trib_revendedor
17           frmGerar.DataInsert.Recordset("cod_inscri") = VarRet!cod_trib_inscrito
18           frmGerar.DataInsert.Recordset("cod_isento") = VarRet!cod_trib_isento
19           frmGerar.DataInsert.Recordset("fator_reve") = VarRet!fator_revendedor
20           frmGerar.DataInsert.Recordset("fator_insc") = VarRet!fator_inscrito
21           frmGerar.DataInsert.Recordset("fator_isen") = VarRet!fator_isento
22           frmGerar.DataInsert.Recordset("fl_cred_su") = VarRet!fl_cred_suspenso
23           frmGerar.DataInsert.Recordset("pc_margem_") = VarRet!PC_MARGEM_LUCRO
24           frmGerar.DataInsert.UpdateRecord
25           VarRet.MoveNext
26             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
27             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
28                DoEvents
29                frmGerar.Refresh
30             End If
               frmGerar.lblRegistros.Refresh
31        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "SUBST_TRIBUTARIA", "S"
32        ArqExcluir "SUBST_TR"
33        frmGerar.DataInsert.RecordSource = ""
34        frmGerar.DataInsert.Refresh
35        frmGerar.lblTabela = ""
36        frmGerar.lblRegistros.Caption = ""
37        frmGerar.Refresh

38        Mover_Copiar 2, vDe, vPara, "SUBST_TR.DBF"
          Set VarRet = Nothing
Trata_Erro:
39        If Err.Number <> 0 Then
40      MsgBox "Sub Subst_Tributaria" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
41        End If

End Sub

Public Sub TABELA_DESCPER_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "TABELA_DESCPER - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TABELA_DESCPER(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "TABELAD"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "TABELA_DESCPER - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("sequencia") = VarRet!sequencia
14           frmGerar.DataInsert.Recordset("tp_tabela") = VarRet!tp_tabela
15           frmGerar.DataInsert.Recordset("ocorr_prec") = VarRet!ocorr_preco
16           frmGerar.DataInsert.Recordset("cod_filial") = VarRet!cod_filial
17           frmGerar.DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
18           frmGerar.DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
19           frmGerar.DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
20           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
21           frmGerar.DataInsert.Recordset("situacao") = VarRet!situacao
22           frmGerar.DataInsert.Recordset("pc_desc1") = VarRet!pc_desc_periodo1
23           frmGerar.DataInsert.Recordset("pc_desc2") = VarRet!pc_desc_periodo2
24           frmGerar.DataInsert.Recordset("pc_desc3") = VarRet!pc_desc_periodo3
25           frmGerar.DataInsert.UpdateRecord
26           VarRet.MoveNext
27             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
28             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
29                DoEvents
30                frmGerar.Refresh
31             End If
               frmGerar.lblRegistros.Refresh
32        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "TABELA_DESCPER", "S"
33        ArqExcluir "TABELAD"
34        frmGerar.DataInsert.RecordSource = ""
35        frmGerar.DataInsert.Refresh
36        frmGerar.lblTabela = ""
37        frmGerar.lblRegistros.Caption = ""
38        frmGerar.Refresh

39        Mover_Copiar 2, vDe, vPara, "TABELAD.DBF"
          Set VarRet = Nothing
Trata_Erro:
40        If Err.Number <> 0 Then
41      MsgBox "Sub Tabela_DescPer" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
42        End If

End Sub

Public Sub TABELA_VENDA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "TABELA_VENDA - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TABELA_VENDA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "TABELAV"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "TABELA_VENDA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("tabela_ven") = VarRet!TABELA_VENDA
14           frmGerar.DataInsert.Recordset("tp_tabela") = VarRet!tp_tabela
15           frmGerar.DataInsert.Recordset("ocorr_prec") = VarRet!ocorr_preco
16           frmGerar.DataInsert.Recordset("dt_vig1") = VarRet!dt_vigencia1
17           frmGerar.DataInsert.Recordset("dt_vig2") = VarRet!dt_vigencia2
18           frmGerar.DataInsert.Recordset("dt_vig3") = VarRet!dt_vigencia3
19           frmGerar.DataInsert.Recordset("dt_vig4") = VarRet!dt_vigencia4
20           frmGerar.DataInsert.Recordset("dt_vig5") = VarRet!dt_vigencia5
21           frmGerar.DataInsert.UpdateRecord
22           VarRet.MoveNext
23             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
24             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
25                DoEvents
26                frmGerar.Refresh
27             End If
               frmGerar.lblRegistros.Refresh
28        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "TABELA_VENDA", "S"
29        ArqExcluir "TABELAV"
30        frmGerar.DataInsert.RecordSource = ""
31        frmGerar.DataInsert.Refresh
32        frmGerar.lblTabela = ""
33        frmGerar.lblRegistros.Caption = ""
34        frmGerar.Refresh

35        Mover_Copiar 2, vDe, vPara, "TABELAV.DBF"
          Set VarRet = Nothing
Trata_Erro:
36        If Err.Number <> 0 Then
37      MsgBox "Sub Tabela_Venda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
38        End If

End Sub

Public Sub TIPO_CLIENTE_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "TIPO_CLIENTE - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TIPO_CLIENTE(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "TIPO_CLI"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "TIPO_CLIENTE - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_tipo_c") = VarRet!cod_tipo_cli
14           frmGerar.DataInsert.Recordset("desc_tipo") = VarRet!desc_tipo_cli
15           frmGerar.DataInsert.Recordset("cod_segmen") = VarRet!cod_segmento
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "TIPO_CLIENTE", "S"
24        ArqExcluir "TIPO_CLI"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "TIPO_CLI.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub Tipo_Cliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Public Sub TRANSPORTADORA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "TRANSPORTADORA - Consultando"
3         frmGerar.lblTabela.Refresh

4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TRANSPORTADORA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "TRANSPOR"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "TRANSPORTADORA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_transp") = VarRet!cod_transp
14           frmGerar.DataInsert.Recordset("nome_trans") = VarRet!nome_transp
15           frmGerar.DataInsert.Recordset("cod_cidade") = VarRet!cod_cidade
16           frmGerar.DataInsert.Recordset("situacao") = VarRet!situacao
17           frmGerar.DataInsert.UpdateRecord
18           VarRet.MoveNext
19             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
20             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
21                DoEvents
22                frmGerar.Refresh
23             End If
               frmGerar.lblRegistros.Refresh
24        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "TRANSPORTADORA", "S"
25        ArqExcluir "TRANSPOR"
26        frmGerar.DataInsert.RecordSource = ""
27        frmGerar.DataInsert.Refresh
28        frmGerar.lblTabela = ""
29        frmGerar.lblRegistros.Caption = ""
30        frmGerar.Refresh

31        Mover_Copiar 2, vDe, vPara, "TRANSPOR.DBF"
          Set VarRet = Nothing
Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub Transportadora" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub UF_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "UF - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "UF"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "UF - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf") = VarRet!cod_uf
14           frmGerar.DataInsert.Recordset("desc_uf") = VarRet!desc_uf
15           frmGerar.DataInsert.Recordset("dt_aliq_in") = VarRet!dt_aliq_interna
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "UF", "S"
24        ArqExcluir "UF"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "UF.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32      MsgBox "Sub UF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Public Sub UF_DPK_DBF()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "UF_DPK - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_DPK(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "UF_DPK"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "UF_DPK - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
14           frmGerar.DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
15           frmGerar.DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
16           frmGerar.DataInsert.Recordset("pc_desc") = VarRet!pc_desc
17           frmGerar.DataInsert.UpdateRecord
18           VarRet.MoveNext
19             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
20             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
21                DoEvents
22                frmGerar.Refresh
23             End If
               frmGerar.lblRegistros.Refresh
24        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "UF_DPK", "S"
25        ArqExcluir "UF_DPK"
26        frmGerar.DataInsert.RecordSource = ""
27        frmGerar.DataInsert.Refresh
28        frmGerar.lblTabela = ""
29        frmGerar.lblRegistros.Caption = ""
30        frmGerar.Refresh

31        Mover_Copiar 2, vDe, vPara, "UF_DPK.DBF"
          Set VarRet = Nothing
Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub UF_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub UF_ORIGEM_DESTINO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "UF_ORIGEM_DESTINO - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_ORIGEM_DESTINO(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         frmGerar.DataInsert.RecordSource = "UF_OR_DE"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "UF_ORIGEM_DESTINO - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
14           frmGerar.DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
15           frmGerar.DataInsert.Recordset("pc_icm") = VarRet!pc_icm
16           frmGerar.DataInsert.Recordset("pc_dificm") = VarRet!pc_dificm
17           frmGerar.DataInsert.UpdateRecord
18           VarRet.MoveNext
19             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
20             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
21                DoEvents
22                frmGerar.Refresh
23             End If
               frmGerar.lblRegistros.Refresh
24        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "UF_ORIGEM_DESTINO", "S"
25        ArqExcluir "UF_OR_DE"
26        frmGerar.DataInsert.RecordSource = ""
27        frmGerar.DataInsert.Refresh
28        frmGerar.lblTabela = ""
29        frmGerar.lblRegistros.Caption = ""
30        frmGerar.Refresh

31        Mover_Copiar 2, vDe, vPara, "UF_OR_DE.DBF"
          Set VarRet = Nothing
Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub UF_ORIGEM_DESTINO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub UF_TPCLIENTE_DBF()

1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "UF_TPCLIENTE - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_TPCLIENTE(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
7         frmGerar.DataInsert.RecordSource = "UF_TPCLI"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "UF_TPCLIENTE - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_origem") = VarRet!COD_UF_ORIGEM
14           frmGerar.DataInsert.Recordset("cod_destin") = VarRet!COD_UF_DESTINO
15           frmGerar.DataInsert.Recordset("tp_cliente") = VarRet!tp_cliente
16           frmGerar.DataInsert.Recordset("cod_tribut") = VarRet!cod_tributacao
17           frmGerar.DataInsert.Recordset("pc_desc") = VarRet!pc_desc
18           frmGerar.DataInsert.Recordset("cod_final") = VarRet!cod_tributacao_final
19           frmGerar.DataInsert.Recordset("pc_desc_di") = VarRet!pc_desc_diesel
20           frmGerar.DataInsert.UpdateRecord
21           VarRet.MoveNext
22             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
23             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
24                DoEvents
25                frmGerar.Refresh
26             End If
               frmGerar.lblRegistros.Refresh
27        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "UF_TPCLIENTE", "S"
28        ArqExcluir "UF_TPCLI"
29        frmGerar.DataInsert.RecordSource = ""
30        frmGerar.DataInsert.Refresh
31        frmGerar.lblTabela = ""
32        frmGerar.lblRegistros.Caption = ""
33        frmGerar.Refresh

34        Mover_Copiar 2, vDe, vPara, "UF_TPCLI.DBF"
          Set VarRet = Nothing
Trata_Erro:
35        If Err.Number <> 0 Then
36      MsgBox "Sub UF_TPCLIENTE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If

End Sub

Public Sub TIPO_CLIENTE_BLAU_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "TIPO_CLIENTE_BLAU - Consultando"
3         frmGerar.lblTabela.Refresh
          
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TIPO_CLIENTE_BLAU(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "TP_BLAU"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "TIPO_CLIENTE_BLAU - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_tipo_c") = VarRet!cod_tipo_cli
14           frmGerar.DataInsert.Recordset("desc_tipo_") = VarRet!desc_tipo_cli
15           frmGerar.DataInsert.UpdateRecord
16           VarRet.MoveNext
17             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
18             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
19                DoEvents
20                frmGerar.Refresh
21             End If
               frmGerar.lblRegistros.Refresh
22        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "TIPO_CLIENTE_BLAU", "S"
23        ArqExcluir "TP_BLAU"
24        frmGerar.DataInsert.RecordSource = ""
25        frmGerar.DataInsert.Refresh
26        frmGerar.lblTabela = ""
27        frmGerar.lblRegistros.Caption = ""
28        frmGerar.Refresh

29        Mover_Copiar 2, vDe, vPara, "TP_BLAU.DBF"
          Set VarRet = Nothing
Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub TIPO_CLIENTE_BLAU" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
32        End If

End Sub

Public Sub VENDA_LIMITADA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmGerar.lblTabela.Caption = "VENDA_LIMITADA - Consultando"
3         frmGerar.lblTabela.Refresh
         
4         vBanco.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_VENDA_LIMITADA(:VCURSOR); END;")
          
5         Set VarRet = vBanco.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         frmGerar.DataInsert.RecordSource = "VLIMITAD"
8         frmGerar.DataInsert.Refresh

9         frmGerar.lblTabela.Caption = "VENDA_LIMITADA - Inserindo"
10        frmGerar.lblTabela.Refresh

11        Do While Not VarRet.EOF
12           frmGerar.DataInsert.Recordset.AddNew
13           frmGerar.DataInsert.Recordset("cod_uf") = VarRet!cod_uf
14           frmGerar.DataInsert.Recordset("tp_cliente") = VarRet!tp_cliente
15           frmGerar.DataInsert.Recordset("cod_loja_o") = VarRet!cod_loja_obrigatoria
16           frmGerar.DataInsert.UpdateRecord
17           VarRet.MoveNext
18             frmGerar.lblRegistros = Val(frmGerar.lblRegistros) + 1
19             If Val(frmGerar.lblRegistros) Mod 1000 = 0 Then
20                DoEvents
21                frmGerar.Refresh
22             End If
               frmGerar.lblRegistros.Refresh
23        Loop
Sair:
          AddToINI vNomeIni, vSessaoDia, "VENDA_LIMITADA", "S"
24        ArqExcluir "VLIMITAD"
25        frmGerar.DataInsert.RecordSource = ""
26        frmGerar.DataInsert.Refresh
27        frmGerar.lblTabela = ""
28        frmGerar.lblRegistros.Caption = ""
29        frmGerar.Refresh

30        Mover_Copiar 2, vDe, vPara, "VLIMITAD.DBF"
          Set VarRet = Nothing
Trata_Erro:
31        If Err.Number <> 0 Then
32            MsgBox "Sub Venda_limitada" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
33        End If

End Sub

Sub ArqExcluir(pArq As String)
    For i = 0 To 79
        If vArqsExcluir(i) = "" Then
            vArqsExcluir(i) = pArq
            Exit For
        End If
    Next
End Sub
'**********************
'pTipo 1-Copiar 2-Mover
'**********************
Sub Mover_Copiar(pTipo As Byte, pDe As String, pPara As String, pArquivo As String)
    
    FileCopy pDe & pArquivo, pPara & pArquivo
    
    If pTipo = 2 Then Kill pDe & pArquivo
    
End Sub

Sub Deletar_Arquivos()
    On Error Resume Next
    Dim vArq As String
    Dim vExcluir As Boolean
    Dim ii As Integer
    
    frmGerar.File1.Path = "C:\OUT\COMPLETA\"
    
    For i = 0 To frmGerar.File1.ListCount - 1
        vExcluir = True
        For ii = 0 To 49
            If vArqsExcluir(ii) = "" Then Exit For
            If UCase(frmGerar.File1.List(i)) = UCase(vArqsExcluir(ii)) & ".DBF" = True Then
                vExcluir = False
                Exit For
            End If
        Next
        If vExcluir = True Then Kill "C:\OUT\COMPLETA\" & frmGerar.File1.List(i)
    Next

End Sub

