VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Begin VB.Form frmGerar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIL490 - Gerar Cargas Genericas Auto"
   ClientHeight    =   6825
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4605
   Icon            =   "frmGerar.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   6825
   ScaleWidth      =   4605
   StartUpPosition =   2  'CenterScreen
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   2490
      Pattern         =   "*.DBF"
      TabIndex        =   16
      Top             =   60
      Visible         =   0   'False
      Width           =   2085
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   15
      Top             =   6420
      Width           =   4605
      _ExtentX        =   8123
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8070
         EndProperty
      EndProperty
   End
   Begin VB.Data DataInsert 
      Caption         =   "Data1"
      Connect         =   "dBASE IV;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   3000
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   6000
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.Frame FraCargas 
      Caption         =   "Gerar Cargas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4245
      Left            =   2850
      TabIndex        =   9
      Top             =   1290
      Width           =   1695
      Begin VB.FileListBox FileGenerica 
         Height          =   870
         Left            =   360
         TabIndex        =   12
         Top             =   690
         Visible         =   0   'False
         Width           =   1005
      End
      Begin VB.ListBox lstRepresentantes 
         Appearance      =   0  'Flat
         Height          =   3735
         ItemData        =   "frmGerar.frx":23D2
         Left            =   870
         List            =   "frmGerar.frx":23D9
         TabIndex        =   11
         Top             =   450
         Width           =   765
      End
      Begin VB.ListBox lstPAACs 
         Appearance      =   0  'Flat
         Height          =   3735
         ItemData        =   "frmGerar.frx":23EB
         Left            =   90
         List            =   "frmGerar.frx":23F2
         TabIndex        =   10
         Top             =   450
         Width           =   675
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "PAAC�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   14
         Top             =   240
         Width           =   645
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Repres"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   870
         TabIndex        =   13
         Top             =   240
         Width           =   615
      End
   End
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   1740
      Top             =   360
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin VB.ListBox lstGenericas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4110
      ItemData        =   "frmGerar.frx":2404
      Left            =   60
      List            =   "frmGerar.frx":247A
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   1
      Top             =   1410
      Width           =   2715
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   1125
      Width           =   4425
      _ExtentX        =   7805
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Tabelas Gen�ricas"
      Height          =   195
      Left            =   60
      TabIndex        =   8
      Top             =   1200
      Width           =   1335
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   30
      Left            =   960
      TabIndex        =   7
      Top             =   1920
      Width           =   30
   End
   Begin VB.Label lblTabela 
      Caption         =   "Label2"
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   5640
      Width           =   4365
   End
   Begin VB.Label lblRegistros 
      Height          =   195
      Left            =   1470
      TabIndex        =   5
      Top             =   5880
      Width           =   1515
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Registros Inseridos:"
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   5880
      Width           =   1380
   End
   Begin VB.Label lblHoraInicio 
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   6120
      Width           =   1395
   End
   Begin VB.Label lblHoraFim 
      Height          =   195
      Left            =   1530
      TabIndex        =   2
      Top             =   6120
      Width           =   1395
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmGerar.frx":268A
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmGerar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()

    Dim vRodou As String
    Dim vObj As Object
    
    Me.Visible = True
    
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    Set vBanco = vSessao.OpenDatabase("BATCH", "PRODSIC/BATCH", 0&)
    
    Set vObj = vBanco.CreateDynaset("Select dt_faturamento from datas", 0&)
    
    vData = Format(vObj!dt_faturamento, "YYYYMMDD")
    
    'Verificar se existe o Ini do mes atual
    'Se nao tiver entao criar o arquivo com os dados abaixo
    vNomeIni = "E:\SISTEMAS\PAACS\CARGA_DADOS_AUTOMATICA\GENERICA_" & Left(CStr(vData), 6) & ".INI"
    
    If Dir(vNomeIni, vbArchive) = "" Then Criar_ini
    
    '--------------------------------------------
    '-- Variaveis de Lican�a para uso do BWZIP
    MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    '--------------------------------------------
    
    Carregar_PAACREPRES
    
    vSessaoDia = ""
        
    If GetKeyVal(vNomeIni, "DIA 1", "SUBST_TRIBUTARIA") = "N" Then
        Marcar_Para_Rodar "SUBST_TRIBUTARIA"
        vSessaoDia = "DIA 1"
        
    ElseIf GetKeyVal(vNomeIni, "DIA 2", "UF_DPK") = "N" Then
        Marcar_Para_Rodar "UF_DPK"
        vSessaoDia = "DIA 2"
        
    ElseIf GetKeyVal(vNomeIni, "DIA 3", "APLICACAO") = "N" Then
        Marcar_Para_Rodar "APLICACAO"
        vSessaoDia = "DIA 3"
        
    ElseIf GetKeyVal(vNomeIni, "DIA 4", "ITEM_CADASTRO") = "N" Then
        vSessaoDia = "DIA 4"
        Marcar_Para_Rodar "ITEM_CADASTRO"
        Marcar_Para_Rodar "CLASS_ANTEC_ENTRADA"
    
    ElseIf GetKeyVal(vNomeIni, "DIA 5", "ANTECIPACAO_TRIBUTARIA") = "N" Then
        vSessaoDia = "DIA 5"
        Marcar_Para_Rodar "ANTECIPACAO_TRIBUTARIA"
        Marcar_Para_Rodar "BANCO"
        Marcar_Para_Rodar "CATEG_SINAL"
        Marcar_Para_Rodar "CANCEL_PEDNOTA"
        Marcar_Para_Rodar "CIDADE"
        Marcar_Para_Rodar "CLASS_FISCAL_RED_BASE"
        Marcar_Para_Rodar "CLIE_MENSAGEM"
        Marcar_Para_Rodar "DATAS"
        Marcar_Para_Rodar "DOLAR_DIARIO"
        Marcar_Para_Rodar "EMBALADOR"
        Marcar_Para_Rodar "FILIAL"
        Marcar_Para_Rodar "FORNECEDOR"
        Marcar_Para_Rodar "FORNECEDOR_ESPECIFICO"
        Marcar_Para_Rodar "FRETE_UF_BLOQ"
        Marcar_Para_Rodar "GRUPO"
        Marcar_Para_Rodar "MONTADORA"
        Marcar_Para_Rodar "NATUREZA_OPERACAO"
        Marcar_Para_Rodar "PLANO_PGTO"
        Marcar_Para_Rodar "R_DPK_EQUIV"
        Marcar_Para_Rodar "R_FABRICA_DPK"
        Marcar_Para_Rodar "R_FILDEP"
        Marcar_Para_Rodar "R_UF_DEPOSITO"
        Marcar_Para_Rodar "RESULTADO"
        Marcar_Para_Rodar "SUBGRUPO"
        Marcar_Para_Rodar "TABELA_DESCPER"
        Marcar_Para_Rodar "TABELA_VENDA"
        Marcar_Para_Rodar "TIPO_CLIENTE"
        Marcar_Para_Rodar "TRANSPORTADORA"
        Marcar_Para_Rodar "UF"
        Marcar_Para_Rodar "UF_ORIGEM_DESTINO"
        Marcar_Para_Rodar "UF_TPCLIENTE"
        Marcar_Para_Rodar "TIPO_CLIENTE_BLAU"
        Marcar_Para_Rodar "VENDA_LIMITADA"
        
    End If
    
    If vSessaoDia <> "" Then
        cmdGerar_Click
        Gerar_Carga
        AddToINI vNomeIni, "DIA", "RODOU_EM", vData
    End If
    
    Set vBanco = Nothing
    Set vSessao = Nothing
    
    End
    
End Sub

Private Sub cmdGerar_Click()
    Dim vArquivo As String
    Dim vFL_VDR As String
    
    On Error GoTo Trata_Erro

    Erase vArqsExcluir
    
    lblHoraInicio = Now
    lblTabela.Caption = "Iniciando... Aguarde..."
    lblRegistros.Caption = ""
    
    'Deletar todos os arquivos DBFs existentes no C:\OUT\COMPLETA
    Do While Dir("C:\OUT\COMPLETA\*.dbf", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\COMPLETA\*.DBF")
        Kill "c:\OUT\COMPLETA\" & vArquivo
    Loop
    Do While Dir("C:\OUT\COMPLETA\*.MDB", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\COMPLETA\*.MDB")
        Kill "c:\OUT\COMPLETA\" & vArquivo
    Loop
    
    Do While Dir("C:\OUT\CARGA\*.ZIP", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\CARGA\*.ZIP")
        Kill "C:\OUT\CARGA\" & vArquivo
    Loop
    
    Do While Dir("C:\OUT\CARGA\*.HDR", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\CARGA\*.HDR")
        Kill "C:\OUT\CARGA\" & vArquivo
    Loop
    
    'Descompactar os arquivos DBFs
    MaqZip1.ZipUncompress "C:\OUT\COMPLETA\VAZIO.ZIP", "*", "C:\OUT\COMPLETA\", "-o"
    
    'Copiar o arquivo FIL240.MDB para Generica
    FileCopy "C:\OUT\COMPLETA\FIL240.MDB", "C:\OUT\ZIP\FIL240.MDB"
    Kill "C:\OUT\COMPLETA\FIL240.MDB"
    
    Criar_Cursor vBanco
    
    DataInsert.DatabaseName = "C:\OUT\COMPLETA\"

    vDe = "C:\OUT\COMPLETA\"
    vPara = "C:\OUT\GENERICA\"
    
    Do While Dir("C:\OUT\GENERICA\*.dbf", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\GENERICA\*.DBF")
        Kill "c:\OUT\GENERICA\" & vArquivo
    Loop
        
    If lstGenericas.SelCount > 0 Then
    
        If Dir("C:\OUT\GENERICA\*.*", vbArchive) <> "" Then
            Do While Dir("C:\OUT\GENERICA\*.dbf", vbArchive) <> ""
                vArquivo = Dir("C:\OUT\GENERICA\*.DBF")
                Kill "c:\OUT\GENERICA\" & vArquivo
            Loop
        End If
        
        If Gerar_Genericas_Dbf("ANTECIPACAO_TRIBUTARIA") = True Then Call ANTECIPACAO_TRIBUTARIA_dbf
        If Gerar_Genericas_Dbf("APLICACAO") = True Then Call APLICACAO_DBF
        If Gerar_Genericas_Dbf("banco") = True Then Call BANCO_DBF
        If Gerar_Genericas_Dbf("CATEG_SINAL") = True Then Call CATEG_SINAL_dbf
        If Gerar_Genericas_Dbf("CANCEL_PEDNOTA") = True Then Call CANCEL_PEDNOTA_dbf
        If Gerar_Genericas_Dbf("CIDADE") = True Then Call CIDADE_DBF
        If Gerar_Genericas_Dbf("CLASS_ANTEC_ENTRADA") = True Then Call CLASS_ANTEC_ENTRADA_DBF
        If Gerar_Genericas_Dbf("CLASS_FISCAL_RED_BASE") = True Then Call CLASS_FISCAL_RED_BASE_DBF
        If Gerar_Genericas_Dbf("CLIE_MENSAGEM") = True Then Call CLIE_MENSAGEM_DBF
        If Gerar_Genericas_Dbf("DATAS") = True Then Call DATAS_DBF
        If Gerar_Genericas_Dbf("DOLAR_DIARIO") = True Then Call DOLAR_DIARIO_DBF
        If Gerar_Genericas_Dbf("EMBALADOR") = True Then Call EMBALADOR_DBF
        If Gerar_Genericas_Dbf("Filial") = True Then Call Filial_DBF
        If Gerar_Genericas_Dbf("FORNECEDOR") = True Then Call FORNECEDOR_DBF
        If Gerar_Genericas_Dbf("FORNECEDOR_ESPECIFICO") = True Then Call FORNECEDOR_ESPECIFICO_DBF
        If Gerar_Genericas_Dbf("FRETE_UF_BLOQ") = True Then Call FRETE_UF_BLOQ_DBF
        If Gerar_Genericas_Dbf("GRUPO") = True Then Call GRUPO_DBF
        If Gerar_Genericas_Dbf("ITEM_CADASTRO") = True Then Call ITEM_CADASTRO_DBF
        If Gerar_Genericas_Dbf("MONTADORA") = True Then Call MONTADORA_DBF
        If Gerar_Genericas_Dbf("NATUREZA_OPERACAO") = True Then Call NATUREZA_OPERACAO_DBF
        If Gerar_Genericas_Dbf("PLANO_PGTO") = True Then Call PLANO_PGTO_DBF
        If Gerar_Genericas_Dbf("R_DPK_EQUIV") = True Then Call R_DPK_EQUIV_DBF
        If Gerar_Genericas_Dbf("R_FABRICA_DPK") = True Then Call R_FABRICA_DPK_DBF
        If Gerar_Genericas_Dbf("R_FILDEP") = True Then Call R_FILDEP_DBF
        If Gerar_Genericas_Dbf("R_UF_DEPOSITO") = True Then Call R_UF_DEPOSITO_DBF
        If Gerar_Genericas_Dbf("RESULTADO") = True Then Call RESULTADO_DBF
        If Gerar_Genericas_Dbf("SUBGRUPO") = True Then Call SUBGRUPO_DBF
        If Gerar_Genericas_Dbf("SUBST_TRIBUTARIA") = True Then Call SUBST_TRIBUTARIA_DBF
        If Gerar_Genericas_Dbf("TABELA_DESCPER") = True Then Call TABELA_DESCPER_DBF
        If Gerar_Genericas_Dbf("TABELA_VENDA") = True Then Call TABELA_VENDA_DBF
        If Gerar_Genericas_Dbf("TIPO_CLIENTE") = True Then Call TIPO_CLIENTE_DBF
        If Gerar_Genericas_Dbf("TRANSPORTADORA") = True Then Call TRANSPORTADORA_DBF
        If Gerar_Genericas_Dbf("UF") = True Then Call UF_DBF
        If Gerar_Genericas_Dbf("UF_DPK") = True Then Call UF_DPK_DBF
        If Gerar_Genericas_Dbf("UF_ORIGEM_DESTINO") = True Then Call UF_ORIGEM_DESTINO_DBF
        If Gerar_Genericas_Dbf("UF_TPCLIENTE") = True Then Call UF_TPCLIENTE_DBF
        If Gerar_Genericas_Dbf("TIPO_CLIENTE_BLAU") = True Then Call TIPO_CLIENTE_BLAU_DBF
        If Gerar_Genericas_Dbf("VENDA_LIMITADA") = True Then Call VENDA_LIMITADA_DBF

    End If

    lblTabela.Caption = "Gera��o das tabelas Gen�ricas est� Completa."
    
    lblHoraFim = Now
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
    End If
End Sub

Sub Marcar_Para_Rodar(pMarcar As String)
    
    For i = 0 To lstGenericas.ListCount - 1
        If UCase(lstGenericas.List(i)) = UCase(pMarcar) Then
            lstGenericas.Selected(i) = True
            Exit For
        End If
    Next
End Sub

Sub Criar_ini()
        AddToINI vNomeIni, "DIA", "RODOU_EM", ""
        AddToINI vNomeIni, "DIA 1", "SUBST_TRIBUTARIA", "N"
        AddToINI vNomeIni, "DIA 2", "UF_DPK", "N"
        AddToINI vNomeIni, "DIA 3", "APLICACAO", "N"
        AddToINI vNomeIni, "DIA 4", "ITEM_CADASTRO", "N"
        AddToINI vNomeIni, "DIA 4", "CLASS_ANTEC_ENTRADA", "N"
        AddToINI vNomeIni, "DIA 5", "ANTECIPACAO_TRIBUTARIA", "N"
        AddToINI vNomeIni, "DIA 5", "BANCO", "N"
        AddToINI vNomeIni, "DIA 5", "CATEG_SINAL", "N"
        AddToINI vNomeIni, "DIA 5", "CANCEL_PEDNOTA", "N"
        AddToINI vNomeIni, "DIA 5", "CIDADE", "N"
        AddToINI vNomeIni, "DIA 5", "CLASS_FISCAL_RED_BASE", "N"
        AddToINI vNomeIni, "DIA 5", "CLIE_MENSAGEM", "N"
        AddToINI vNomeIni, "DIA 5", "DATAS", "N"
        AddToINI vNomeIni, "DIA 5", "DOLAR_DIARIO", "N"
        AddToINI vNomeIni, "DIA 5", "EMBALADOR", "N"
        AddToINI vNomeIni, "DIA 5", "FILIAL", "N"
        AddToINI vNomeIni, "DIA 5", "FORNECEDOR", "N"
        AddToINI vNomeIni, "DIA 5", "FORNECEDOR_ESPECIFICO", "N"
        AddToINI vNomeIni, "DIA 5", "FRETE_UF_BLOQ", "N"
        AddToINI vNomeIni, "DIA 5", "GRUPO", "N"
        AddToINI vNomeIni, "DIA 5", "MONTADORA", "N"
        AddToINI vNomeIni, "DIA 5", "NATUREZA_OPERACAO", "N"
        AddToINI vNomeIni, "DIA 5", "PLANO_PGTO", "N"
        AddToINI vNomeIni, "DIA 5", "R_DPK_EQUIV", "N"
        AddToINI vNomeIni, "DIA 5", "R_FABRICA_DPK", "N"
        AddToINI vNomeIni, "DIA 5", "R_FILDEP", "N"
        AddToINI vNomeIni, "DIA 5", "R_UF_DEPOSITO", "N"
        AddToINI vNomeIni, "DIA 5", "RESULTADO", "N"
        AddToINI vNomeIni, "DIA 5", "SUBGRUPO", "N"
        AddToINI vNomeIni, "DIA 5", "TABELA_DESCPER", "N"
        AddToINI vNomeIni, "DIA 5", "TABELA_VENDA", "N"
        AddToINI vNomeIni, "DIA 5", "TIPO_CLIENTE", "N"
        AddToINI vNomeIni, "DIA 5", "TRANSPORTADORA", "N"
        AddToINI vNomeIni, "DIA 5", "UF", "N"
        AddToINI vNomeIni, "DIA 5", "UF_ORIGEM_DESTINO", "N"
        AddToINI vNomeIni, "DIA 5", "UF_TPCLIENTE", "N"
        AddToINI vNomeIni, "DIA 5", "TIPO_CLIENTE_BLAU", "N"
        AddToINI vNomeIni, "DIA 5", "VENDA_LIMITADA", "N"

End Sub
Sub Gerar_Carga()

    On Error GoTo Trata_Erro

    Dim Arquivo As String
    Dim num_arq As Integer
    Dim rst As Object
    Dim vCampos
    
    FileGenerica.Path = "C:\OUT\GENERICA"
    
    'Copiar arquivos da Pasta C:\OUT\GENERICA para a pasta C:\OUT\ZIP
    StatusBar1.Panels(1).Text = "Copiando arquivos Genericos..."
    For i = 0 To FileGenerica.ListCount - 1
        Mover_Copiar 1, "C:\OUT\GENERICA\", "C:\OUT\ZIP\", FileGenerica.List(i)
    Next

'*****
'PAACs
'*****
    For i = 0 To lstPAACs.ListCount - 1
        
        vCampos = Split(lstPAACs.List(i), ";")
        Arquivo = "CAR" & Format(vCampos(0), "0000")
        
        If Dir("C:\OUT\CARGA\" & Arquivo & ".HDR") <> "" Then
            Kill "C:\OUT\CARGA\" & Arquivo & ".HDR"
            Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        End If
        
        StatusBar1.Panels(1).Text = "Gerando carga para PAAC: " & vCampos(0)
       
       'MONTA O ARQUIVO DE HEADER DA CARGA
        num_arq = FreeFile
        Open "C:\OUT\CARGA\" & Arquivo & ".HDR" For Output As #num_arq
        
        Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
        Print #num_arq, "TO        =" & vCampos(2)
        Print #num_arq, "FROM      =DPKPEDID"
        Print #num_arq, "DESCRICAO =" & "#CAR"; Format(Val(vCampos(0)), "0000")
        Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
       'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
        Print #num_arq, "NRQUEBRA  =0"
        
        Close #num_arq
        
        MaqZip1.ZipCompress "C:\OUT\ZIP", "*.*", "C:\OUT\CARGA\", Arquivo & ".ZIP", "-6"

proximo:
     Next
'**************
'Representantes
'**************
    For i = 0 To lstRepresentantes.ListCount - 1
        
        vCampos = Split(lstRepresentantes.List(i), ";")
        Arquivo = "CAR" & Format(vCampos(0), "0000")
        
        If Dir("C:\OUT\ZIP\" & Arquivo & ".HDR") <> "" Then
            Kill "C:\OUT\CARGA\" & Arquivo & ".HDR"
            Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        End If
        
        StatusBar1.Panels(1).Text = "Gerando carga para Representante: " & vCampos(0)
        
       'MONTA O ARQUIVO DE HEADER DA CARGA
        num_arq = FreeFile
        Open "C:\OUT\CARGA\" & Arquivo & ".HDR" For Output As #num_arq
        
        Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
        Print #num_arq, "TO        =" & vCampos(2)
        Print #num_arq, "FROM      =DPKPEDID"
        Print #num_arq, "DESCRICAO =" & "#CAR"; Format(vCampos(0), "0000")
        Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
       'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
        Print #num_arq, "NRQUEBRA  =0"
        
        Close #num_arq
        
        MaqZip1.ZipCompress "C:\OUT\ZIP\", "*.*", "C:\OUT\CARGA\", Arquivo & ".ZIP", "-6"
       
proximo1:
     Next

    'Deletar todos os arquivos DBFs existentes no C:\OUT\ZIP
    StatusBar1.Panels(1).Text = "Excluindo arquivos da Pasta ZIP."
    Do While Dir("C:\OUT\ZIP\*.DBF", vbArchive) <> ""
       Arquivo = Dir("C:\OUT\ZIP\*.DBF")
       Kill "c:\OUT\ZIP\" & Arquivo
    Loop
    
    If Dir("C:\OUT\ZIP\FIL240.MDB") <> "" Then
        Kill "C:\OUT\ZIP\FIL240.MDB"
    End If

    Deletar_Arquivos

    Me.StatusBar1.Panels(1).Text = "Arquivos de Carga Gerados com Sucesso!"

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Gerar_carga" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description
    End If
End Sub

Sub Carregar_PAACREPRES()
    Dim reg As Integer
    Dim rst As Object

    Dim vSessao_prod As Object
    Dim vBanco_Prod As Object

    Set vSessao_prod = CreateObject("oracleinprocserver.xorasession")
    Set vBanco_Prod = vSessao_prod.OpenDatabase("PRODUCAO", "fil400/prod", 0&)

    lstPAACs.Clear
    lstRepresentantes.Clear
    
    Set rst = vBanco_Prod.CreateDynaset("select B.Cod_filial,0 Cic_Gerente,E_mail,FL_VDR FRom   paac.controle_paac A, " & _
                                  " paac.filial_base B, paac.deposito_filial c Where a.cod_filial = B.cod_filial " & _
                                  " and    b.cod_filial = c.cod_filial and    c.fl_base = 'S' ORDER BY B.COD_FILIAL", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstPAACs.AddItem rst!cod_filial & vbTab & vbTab & ";" & rst!cic_gerente & ";" & rst!e_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

    Set rst = vBanco_Prod.CreateDynaset("select B.Cod_Repres, 0 Cic_Gerente,E_mail,FL_VDR From   paac.controle_representante A," & _
                                  " paac.representante_base B, paac.deposito_repres c Where  a.cod_repres = b.cod_repres and " & _
                                  " B.Cod_Repres = c.cod_repres and c.fl_base = 'S' ORDER BY A.COD_REPRES", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstRepresentantes.AddItem rst!cod_repres & vbTab & vbTab & ";" & rst!cic_gerente & ";" & rst!e_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

End Sub

Public Sub Criar_Cursor(ByRef BANCO)
    BANCO.Parameters.Remove "vCursor"
    BANCO.Parameters.Add "vCursor", 0, 2
    BANCO.Parameters("vCursor").serverType = ORATYPE_CURSOR
    BANCO.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    BANCO.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub
Function Gerar_Genericas_Dbf(Tabela As String) As Boolean
    For i = 0 To lstGenericas.ListCount - 1
        If UCase(lstGenericas.List(i)) = UCase(Tabela) Then
            If lstGenericas.Selected(i) = True Then
                Gerar_Genericas_Dbf = True
                Exit For
            End If
        End If
    Next
End Function
