--- PACKAGE DO SISTEMA VDA400
--- DATA: 28/09/2005
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\VDA400\PADRAO\PCK_VDA400.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_VDA400 is

	Type tp_Cursor is Ref Cursor;
	
	PROCEDURE PR_CON_TRANSP(PM_CODTRANS  IN NUMBER,
							PM_NOMETRANS IN VARCHAR2,
							PM_CURSOR1   IN OUT tp_cursor,
							PM_ERRO      OUT NUMBER);			  

End PCK_VDA400;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_VDA400 is


	PROCEDURE PR_CON_TRANSP(PM_CODTRANS  IN NUMBER,
							PM_NOMETRANS IN VARCHAR2,
							PM_CURSOR1   IN OUT tp_cursor,
							PM_ERRO      OUT NUMBER) IS
	BEGIN

		PM_ERRO := 0;
		
		IF PM_CODTRANS <> 0 THEN
		
			--Abrindo Cursor
			OPEN PM_CURSOR1 FOR
			Select a.cod_transp cod_transp,
			a.nome_transp nome_transp,
			a.endereco endereco,
			nvl(a.bairro,' ') bairro,
			b.nome_cidade nome_cidade,
			b.cod_uf cod_uf,
			substr(to_char(nvl(a.ddd,0),'0999'),2,4) ddd,
			substr(to_char(nvl(a.fone,0),'999999999'),2,9) fone,
			substr(to_char(nvl(a.cep,0),'09999999'),2,8) cep,
			substr(to_char(nvl(a.fax,0),'9999999999999'),2,13) fax,
			nvl(a.via_transp,' ') via_transp,
			a.situacao situacao,
			nvl(a.sigla,' ') sigla
			from transportadora a, cidade b
			where a.cod_cidade=b.cod_cidade and
			a.cod_transp = PM_CODTRANS;
			
		ELSE		
		
			--Abrindo Cursor
			OPEN PM_CURSOR1 FOR
			Select a.cod_transp cod_transp,
			a.nome_transp nome_transp, b.nome_cidade
			from transportadora a, cidade b
			where a.cod_cidade=b.cod_cidade and
			a.nome_transp like PM_NOMETRANS;
			
		END IF;

		EXCEPTION
			WHEN OTHERS THEN
			PM_ERRO := SQLCODE;

	END PR_CON_TRANSP;

End PCK_VDA400;
---------------
/