VERSION 5.00
Begin VB.Form frmTransportadora 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Nome da tela"
   ClientHeight    =   5475
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10035
   Icon            =   "frmTransportadora.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   365
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   669
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   4065
      Left            =   90
      TabIndex        =   4
      Top             =   1260
      Width           =   9825
      Begin VB.TextBox lblSituacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1485
         Locked          =   -1  'True
         TabIndex        =   25
         Top             =   3465
         Width           =   2490
      End
      Begin VB.TextBox lblDDD 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         Locked          =   -1  'True
         TabIndex        =   23
         Top             =   2250
         Width           =   600
      End
      Begin VB.TextBox lblViaTransp 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1485
         Locked          =   -1  'True
         TabIndex        =   21
         Top             =   2880
         Width           =   2490
      End
      Begin VB.TextBox lblFax 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5715
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   2250
         Width           =   2490
      End
      Begin VB.TextBox lblFone 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1890
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   2250
         Width           =   2490
      End
      Begin VB.TextBox lblCEP 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7110
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   1665
         Width           =   2490
      End
      Begin VB.TextBox lblUF 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5715
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1665
         Width           =   555
      End
      Begin VB.TextBox lblCidade 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   1665
         Width           =   3930
      End
      Begin VB.TextBox lblBairro 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7110
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   1035
         Width           =   2490
      End
      Begin VB.TextBox lblEndereco 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   1035
         Width           =   5100
      End
      Begin VB.TextBox lblSigla 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         Locked          =   -1  'True
         TabIndex        =   5
         Top             =   495
         Width           =   2490
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   675
         TabIndex        =   24
         Top             =   3555
         Width           =   750
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Via Transporte:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   180
         TabIndex        =   22
         Top             =   2970
         Width           =   1260
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Fax:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5310
         TabIndex        =   20
         Top             =   2340
         Width           =   330
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Fone:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   630
         TabIndex        =   18
         Top             =   2340
         Width           =   465
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "CEP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6660
         TabIndex        =   16
         Top             =   1755
         Width           =   375
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5400
         TabIndex        =   14
         Top             =   1755
         Width           =   270
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Cidade:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   495
         TabIndex        =   12
         Top             =   1755
         Width           =   600
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Bairro:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   6525
         TabIndex        =   10
         Top             =   1125
         Width           =   510
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Endere�o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   270
         TabIndex        =   8
         Top             =   1125
         Width           =   840
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Sigla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   675
         TabIndex        =   6
         Top             =   585
         Width           =   420
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1005
      Left            =   90
      TabIndex        =   0
      Top             =   135
      Width           =   9825
      Begin VB.TextBox txtNome 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2565
         TabIndex        =   3
         Top             =   405
         Width           =   5775
      End
      Begin VB.TextBox txtCodigo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         MaxLength       =   6
         TabIndex        =   1
         Top             =   405
         Width           =   780
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Transportadora:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   270
         TabIndex        =   2
         Top             =   495
         Width           =   1305
      End
   End
End
Attribute VB_Name = "frmTransportadora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Public Sub ConsTransp()

    Screen.MousePointer = 11
    
    If txtCodigo.Text = "" And txtNome.Text = "" Then
        MsgBox "Entre com o c�digo ou o nome da Transportadora", , "Aten��o!"
        txtCodigo.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    COD = txtCodigo.Text
    NOME = txtNome.Text
    
    If txtCodigo.Text = "" Then
        COD = 0
    End If
    
    If txtNome.Text = "" Then
        NOME = Null
    End If
    
    LimpaForm

    vSql = "PRODUCAO.PCK_VDA400.PR_CON_TRANSP(:PM_CODTRANS,:PM_NOMETRANS,:PM_CURSOR1,:PM_CODERRO)"
     
    vBanco.Parameters.Remove "PM_CODTRANS"
    vBanco.Parameters.Add "PM_CODTRANS", COD, 1
    
    vBanco.Parameters.Remove "PM_NOMETRANS"
    vBanco.Parameters.Add "PM_NOMETRANS", "%" & NOME & "%", 1
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
          
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "Nenhuma Transportadora n�o encontrada", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If txtCodigo.Text = "" Then
    
        vVB_Generica_001.CarregaGridTabela frmTranspGrid.grd, vObjOracle, 4
        Screen.MousePointer = 0
        frmTranspGrid.Show 1
        Exit Sub
        
    End If
    
    txtNome = vObjOracle("nome_transp")
    lblSigla.Text = vObjOracle("sigla")
    lblEndereco.Text = vObjOracle("endereco")
    lblBairro.Text = vObjOracle("bairro")
    lblCidade.Text = vObjOracle("nome_cidade")
    lblUF.Text = vObjOracle("cod_uf")
    lblCEP.Text = vObjOracle("cep")
    lblDDD.Text = vObjOracle("ddd")
    lblFone.Text = Trim(vObjOracle("fone"))
    lblFax.Text = Trim(vObjOracle("fax"))
    
    situacao = vObjOracle("situacao")
    If situacao = 9 Then
        lblSituacao.ForeColor = &HFF&
        lblSituacao.Text = "DESATIVADA"
    ElseIf situacao = 0 Then
        lblSituacao.ForeColor = &H800000
        lblSituacao.Text = "ATIVA"
    End If
    
    via = vObjOracle("via_transp")
    
    If via = "R" Then
        lblViaTransp.Text = "RODOVI�RIA"
    ElseIf via = "A" Then
        lblViaTransp.Text = "A�REA"
    ElseIf via = "F" Then
        lblViaTransp.Text = "FERROVI�RIA"
    ElseIf via = "M" Then
        lblViaTransp.Text = "MAR�TIMA"
    ElseIf via = "O" Then
        lblViaTransp.Text = "�NIBUS"
    End If
    
    'lblViaTransp.Text = vObjOracle("via_transp")
    'lblSituacao.Text = vObjOracle("situacao")
    
    Screen.MousePointer = 0

End Sub

Public Sub LimpaForm()

    'txtCodigo.Text = ""
    txtNome.Text = ""
    
    lblSigla.Text = ""
    lblEndereco.Text = ""
    lblBairro.Text = ""
    lblCidade.Text = ""
    lblUF.Text = ""
    lblCEP.Text = ""
    lblDDD.Text = ""
    lblFone.Text = ""
    lblFax.Text = ""
    lblViaTransp.Text = ""
    lblSituacao.Text = ""

End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
    
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub
