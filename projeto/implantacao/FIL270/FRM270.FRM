VERSION 4.00
Begin VB.Form Form1 
   Caption         =   "GERA��O DE BASES PARA FILIAIS E REPRESENTANTES"
   ClientHeight    =   4140
   ClientLeft      =   960
   ClientTop       =   1485
   ClientWidth     =   7950
   Height          =   4545
   Icon            =   "fil270.frx":0000
   Left            =   900
   LinkTopic       =   "Form1"
   ScaleHeight     =   4140
   ScaleWidth      =   7950
   Top             =   1140
   Width           =   8070
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture6 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   360
      Left            =   6585
      Picture         =   "fil270.frx":0442
      ScaleHeight     =   360
      ScaleWidth      =   660
      TabIndex        =   19
      Top             =   2715
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.PictureBox Picture5 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   6570
      Picture         =   "fil270.frx":0884
      ScaleHeight     =   420
      ScaleWidth      =   660
      TabIndex        =   18
      Top             =   2340
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.PictureBox Picture4 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   6555
      Picture         =   "fil270.frx":0CC6
      ScaleHeight     =   420
      ScaleWidth      =   660
      TabIndex        =   17
      Top             =   1875
      Visible         =   0   'False
      Width           =   660
   End
   Begin VB.PictureBox Picture3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   7335
      Picture         =   "fil270.frx":1108
      ScaleHeight     =   465
      ScaleWidth      =   480
      TabIndex        =   16
      Top             =   2685
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   7335
      Picture         =   "fil270.frx":1412
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   15
      Top             =   2250
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   7320
      Picture         =   "fil270.frx":171C
      ScaleHeight     =   480
      ScaleWidth      =   465
      TabIndex        =   14
      Top             =   1755
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Finaliza"
      Height          =   495
      Left            =   6945
      TabIndex        =   7
      Top             =   840
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Gera Di�rio Gerencial"
      Height          =   495
      Left            =   6960
      TabIndex        =   6
      Top             =   240
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Height          =   1230
      Left            =   3120
      TabIndex        =   3
      Top             =   135
      Width           =   2775
      Begin VB.OptionButton Option2 
         Caption         =   "Mensal"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   810
         Width           =   975
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Di�rio"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1200
      Left            =   105
      TabIndex        =   0
      Top             =   135
      Width           =   2775
      Begin VB.CheckBox Check2 
         Caption         =   "Completa"
         Height          =   195
         Left            =   135
         TabIndex        =   2
         Top             =   795
         Width           =   975
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Alterados"
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Value           =   1  'Checked
         Width           =   975
      End
   End
   Begin VB.Label Label7 
      Caption         =   "Gerando Base Gen�rica: "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   450
      Left            =   1290
      TabIndex        =   20
      Top             =   1950
      Width           =   3015
   End
   Begin VB.Label Label6 
      Caption         =   "Representante"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4395
      TabIndex        =   13
      Top             =   2745
      Width           =   1665
   End
   Begin VB.Label Label3 
      Caption         =   "Franquia / Regional"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   270
      Left            =   4395
      TabIndex        =   12
      Top             =   2460
      Width           =   2160
   End
   Begin VB.Label Label2 
      Caption         =   "Gerente"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   270
      Left            =   4530
      TabIndex        =   11
      Top             =   3645
      Visible         =   0   'False
      Width           =   1065
   End
   Begin VB.Label Label1 
      Caption         =   "Gerando Base Espec�fica:"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   270
      Left            =   1290
      TabIndex        =   10
      Top             =   2475
      Width           =   2955
   End
   Begin VB.Label Label4 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   1695
      TabIndex        =   9
      Top             =   4920
      Width           =   6105
   End
   Begin VB.Label Label5 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   2835
      TabIndex        =   8
      Top             =   5580
      Width           =   3780
   End
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Command1_Click()
On Error GoTo TrataErro
    
  Screen.MousePointer = 11
   
  tempoini = Time
  
  'Verifica se j� executou o programa
  Call VERIF_EXECUCAO
  If w_termina Then
     Exit Sub
  End If
  
  'Verifica se algum form3.check foi escolhido
  Call VERIF_CHECK
  If w_termina Then
    Exit Sub
  End If
  
  'Abre o arquivo access de controle
  Call ABRE_CONTROLE
  
  'Verifica que tipos de bases ser�o geradas
  Call VERIFICA_TIPO
  
  Call DIRETORIOS
  
  'Verifica a exist�ncia do 0000.MDB, arquivo padr�o da base
  Call VERIF_0000MDB
  
  'Flega o in�cio da execu��o do programa
  w_execucao = True
  
    
  'Busca no CONTROLE a sequencia do arquivo diario da filial
  If TIPO_FILIAL = True Then
      Call BUSCA_NUMFILIAL
  End If
'  If TIPO_GERENTE = True Then
'      Call BUSCA_NUMGERE
'  End If
  If TIPO_REPRES = True Then
     Call BUSCA_NUMREPR
  End If
  
  'Define os arquivos de trabalho atrav�z do w_num_filial
  If TIPO_FILIAL = True Then
     Call DEFINE_NOME_FILIAL(1)
  Else
     Call DEFINE_NOME_REPRESENTANTE(1)
  End If
  'Abre o(s) arquivo(s) access para leitura da base
  Call ABRE_OUTROS
  
  'Cria o .MDB geral para todos os gerentes no diretorio de trabalho
  Form1.label5.Caption = "BASE GEN�RICA"
  Form1.Refresh
'  Call CRIA_TEMP
  
  Picture4.Visible = True
  
  Call APLICACAO
  Call BANCO
  Call CATEG_SINAL
  Call CIDADE
  Call CLIE_MENSAGEM_FILIAL
  Call DATAS_FILIAL
  Call DOLAR_DIARIO
  Call ESTATISTICA_ITEM
  Call filial
  Call FORNECEDOR
  Call GRUPO
  Call ITEM_ANALITICO
  Call ITEM_CADASTRO
'  Call ITEM_CUSTO
  Call ITEM_ESTOQUE
  Call ITEM_GLOBAL
  Call ITEM_PRECO
  Call MONTADORA
  Call PLANO_PGTO
  Call SUBGRUPO
  Call SUBST_TRIBUTARIA
  Call TAXA
  Call TABELA_DESCPER
  Call TABELA_VENDA
  Call TIPO_CLIENTE
  Call TRANSPORTADORA
  Call UF
  Call UF_DEPOSITO
  Call UF_ORIGEM_DESTINO
  Call UF_DPK
  Call UF_TPCLIENTE
       
  'NOVAS TABELAS PARA FILIAL, REGIONAL E REPRESENTANTE
'  Call DEPOSITO
      
  Call DEPOSITO_VISAO
  Call FILIAL_FRANQUIA
  Call FRETE_UF
  Call FRETE_UF_BLOQ
  Call LOJA
  Call NATUREZA_OPERACAO
  Call TIPO_CLIENTE_BLAU
  Call CANCEL_PEDNOTA
  Call R_REPVEN
  Call R_FILDEP
'  Call COTACAO
 ' Call ITEM_COTACAO
'  Call SALDO_PEDIDOS
  Call FRETE_ENTREGA
  Call EMBALADOR
  Call CLIENTE_CATEG_VDR
  
  'Tabelas com registros para dele��o
  Call DELETA_APLICACAO
  Call DELETA_TABELA_DESCPER
'  Call DELETA_CLIENTE_categ_vdr
    
  'Copia o arquivo gen�rico para todos os gerentes cadastrados no controle_bases.gerente
  If TIPO_FILIAL = True Then
      Call COPIA_ESPECIFICO_FILIAL
  Else
      Call COPIA_ESPECIFICO_FILIAL
  End If
  
  'Remove o(s) arquivo(s) gen�rico(s) diario e completo
  Call REMOVE_TEMP
    
  
Picture4.Visible = False
Picture1.Left = 6675
Picture1.Visible = True

  If TIPO_FILIAL = True Then
     Picture5.Visible = True
     If Form1.Check1.Value = 1 Then
        arqmdb = Dir(dir_diaria_filial & "????" & w_num_filial & "*.MDB")
     ElseIf Form1.check2.Value = 1 Then
        arqmdb = Dir(dir_completa_filial & "*.MDB")
     End If
     Do While arqmdb <> ""
        Form1.label5.Caption = "BASE FILIAL = " & Mid(arqmdb, 1, 4)
        Form1.Refresh
        Call DEFINE_NOME_FILIAL(2)
        Call BUSCA_FILIAL
        Call REPRESENTANTE_FILIAL
        Call REPR_END_CORRESP_FILIAL
        Call REPRESENTACAO_FILIAL
        Call R_REPCGC_FILIAL
        Call R_REPVEN_FILIAL
        Call CLIENTE_FILIAL
        Call CLIE_CREDITO_FILIAL
        Call CLIE_ENDERECO_FILIAL
        Call DUPLICATAS_FILIAL
        Call DELETA_REPRESENTANTE
        Call DELETA_CLIENTE
        Call SALDO_PEDIDOS_FILIAL
        Call PEDNOTA_VENDA("F")
        Call ITPEDNOTA_VENDA("F")
        Call R_PEDIDO_CONF("F")
        Call ROMANEIO("F")
        Call V_PEDLIQ_VENDA("F")
        Call DEPOSITO    ' GRAVA NA TABELA DEPOSITO A FILIAL DA BASE
        arqmdb = Dir
     Loop
 Picture5.Visible = False
 Picture2.Left = 6675
 Picture2.Visible = True
 End If
  
 If TIPO_REPRES = True Then
    Picture6.Visible = True
    If Form1.Check1.Value = 1 Then
       arqmdb = Dir(dir_diaria_repres & "????" & w_num_representante & "*.MDB")
    ElseIf Form1.check2.Value = 1 Then
       arqmdb = Dir(dir_completa_repres & "*.MDB")
    End If
    Do While arqmdb <> ""
       Form1.label5.Caption = "BASE REPRESENTANTE = " & Mid(arqmdb, 1, 4)
       Form1.Refresh
       Call DEFINE_NOME_REPRESENTANTE(2)
       Call BUSCA_CODREP
       Call REPRESENTANTE_REPR
       Call REPR_END_CORRESP_REPRESENTANTE
       Call REPRESENTACAO_REPRESENTANTE
       Call R_REPCGC_REPRESENTANTE
       Call R_REPVEN_REPRESENTANTE
       Call CLIENTE_REPRESENTANTE
       Call CLIE_CREDITO_REPRESENTANTE
       Call CLIE_ENDERECO_REPRESENTANTE
       Call DUPLICATAS_REPRESENTANTE
       Call SALDO_PEDIDOS_REPRESENTANTE
       Call PEDNOTA_VENDA("R")
       Call ITPEDNOTA_VENDA("R")
       Call R_PEDIDO_CONF("R")
       Call ROMANEIO("R")
       Call V_PEDLIQ_VENDA("R")
       Call DELETA_REPRESENTANTE
       Call DELETA_CLIENTE
       arqmdb = Dir
    Loop
 Picture6.Visible = False
 Picture3.Left = 6675
 Picture3.Visible = True
 
 
 End If
  
  'Compacta as bases espec�ficas di�rias dos gerentes
  Form1.Label4 = ""
  Form1.label5.Caption = "COMPACTA BASES DI�RIAS"
  Form1.Refresh
  Call COMPACTA_BANCO
        
  'Zipa as bases espec�ficas di�rias dos gerentes
   Form1.label5.Caption = "ZIPA BASES DI�RIAS"
  Form1.Refresh
  Call ZIPA_BASES_FILIAL
  
  'Gera arquivo para e_mail
   Form1.label5.Caption = "GERA E_MAIL"
  Form1.Refresh
  Call GERA_E_MAIL_FILIAL
  
  'Atualiza banco de controle CONTROLE.MDB
   Form1.label5.Caption = "BASE DE CONTROLE"
  Form1.Refresh
  Call NUM_FILIAL
  
  tempofim = Time
  Screen.MousePointer = 0
       
  MsgBox "Inicio: " & tempoini & "   Fim: " & tempofim, 0, "TEMPO EXECU��O"
   Form1.label5.Caption = "EXECU��O OK !!!, FINALIZAR"
   Form1.Label4.Caption = ""
            
  Exit Sub
   
TrataErro:
    If Err = 3022 Then
        Resume Next
    Else
        MsgBox Str(Err) + "-" + Error$
    End If
    
    Resume Next

End Sub

Private Sub Command2_Click()
    If w_termina Then
        dbextracao.Close
        dbcontrole.Close
    End If
    End
End Sub

Private Sub Form_Load()

   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   dir_e_mail = "C:\COM\SAIDA\MSG\"

   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False

If TIPO_FILIAL = True Then
   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base_filial = "C:\DADOS\FILIAL\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria_filial = "C:\DADOS\FILIAL\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa_filial = "C:\DADOS\FILIAL\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq_filial = "C:\COM\SAIDA\ARQUIVOS\FILIAL\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
   
'ElseIf TIPO_GERENTE = True Then

   'Define diretorio base do arquivo fixo 0000.MDB
   'dir_base = "C:\DADOS\GERENTE\0000.MDB"
 '  dir_base_gerente = "C:\DADOS\GERENTE\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   'dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
  ' dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   'dir_diaria = "C:\DADOS\GERENTE\DIARIA\"
   'dir_diaria_gerente = "C:\DADOS\GERENTE\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   'dir_completa = "C:\DADOS\GERENTE\COMPLETA\"
   'dir_completa_gerente = "C:\DADOS\GERENTE\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   'dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"
   'dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail = "C:\COM\SAIDA\MSG\"
   'dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\"
   'dir_e_mail_arq_gerente = "C:\COM\SAIDA\ARQUIVOS\GERENTE\"

   'Define diretorio do .BAT de pkzip dos arquivos
   'zipa = "C:\DADOS\PTOBAT\ZIPA"
   'zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
'   w_termina = False
 '  w_execucao = False
   
   
ElseIf TIPO_REPRES = True Then
    TIPO_BASE = "R"

   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base_repres = "C:\DADOS\REPRES\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria_repres = "C:\DADOS\REPRES\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa_repres = "C:\DADOS\REPRES\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq_repres = "C:\COM\SAIDA\ARQUIVOS\REPRES\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
End If




End Sub


