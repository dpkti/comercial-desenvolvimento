Attribute VB_Name = "ModBancoDados"
Option Explicit

'Private vConn       As ADODB.Connection
Private vSessao     As Object           'SESS�O DO ORACLE
'Private vNomeBanco  As String           'NOME DO BANCO DO ARQUIVO .INI
Private vDb         As Object           'Database

Sub ConectaDb()
    
    Dim vErro As String
    'Esta conex�o � apenas para pegar os dados do sistema, n�o a utilizo para mais nada
    'vErro = vVB_Generica_001.ConectaOracle("Producao", "CRG010", False, Me)
    'vErro = vVB_Generica_001.ConectaOracle("SDPK_TESTE", "FIS163", False, Me)
    'Set vBanco = vVB_Generica_001.vBanco
    
    'PRODU��O DPK
    'vErro = vVB_Generica_001.ConectaOracle("BATCH", "FIS163/DPK80021", False, Me)
    
    'DESENVOLVIMENTO - DPK - CIET
    'vErro = vVB_Generica_001.ConectaOracle("BATCH_H", "FIS163/DPK80021", False, Me)
    'Set db = vVB_Generica_001.vBanco

    'DESENVOLVIMENTO - DPK - CIET
    DBOpen "Provider=MSDAORA;" & _
           "Password=dpk43876;" & _
           "User ID=VDA960;" & _
           "Data Source=PRODUCAO;" & _
           "Persist Security Info=True"
    
End Sub

Private Function DBOpen(Optional ConnectionString As String = "") As Boolean
On Error GoTo err_DBopen
   
   Set vSessao = CreateObject("oracleinprocserver.xorasession")
   Set vDb = vSessao.OpenDatabase("PRODUCAO", "VDA960/dpk43876", 0&)
   
'   Set vConn = New ADODB.Connection
'
'   If vConn.State = adStateClosed Then  'Check for previous connection
'       vConn.CursorLocation = adUseClient
'       vConn.Open ConnectionString
'   End If
'
'   DBOpen = True
   
   Exit Function

err_DBopen:
   DBOpen = False
   Set vSessao = Nothing
   Set vDb = Nothing
   Err.Raise Err.Number, Err.Source & "[DBOpen]", Err.Description

End Function

Sub DisconectaDb()
    Set vSessao = Nothing
    Set vDb = Nothing
End Sub

Public Function BuscaClientePorCnpj(ByRef vCnpj As String) As Object
    Dim vSql As String
    Dim vRegistros As Object
    Dim vErro As Integer
    On Error GoTo TrataErro
    
    vErro = 0
    vSql = "Begin producao.pck_vda320.pr_cliente(:p_Cursor,:p_Cod_Cliente,:p_Cnpj,:p_Erro);END;"

    'oradatabase Parameters Add
    '  Nome do parametro
    '  Valor a passar ou receber do parametro
    '  Tipo do parametro 1 - Output, 2 - Input, 3 - Both
    vDb.Parameters.Add "p_Cursor", 0, 3
    vDb.Parameters("p_Cursor").ServerType = 102
    vDb.Parameters("p_Cursor").DynasetOption = &H2&
    vDb.Parameters("p_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vDb.Parameters.Add "p_Cod_Cliente", 0, 1
    vDb.Parameters.Add "p_Cnpj", vCnpj, 1
    vDb.Parameters.Add "p_Erro", 0, 2
    vDb.ExecuteSQL vSql
    Set vRegistros = vDb.Parameters("p_Cursor").Value
    vErro = vDb.Parameters("p_Erro").Value
    vDb.Parameters.Remove "p_Cursor"
    vDb.Parameters.Remove "p_Cod_Cliente"
    vDb.Parameters.Remove "p_Cnpj"
    vDb.Parameters.Remove "p_Erro"

    If vErro <> 0 Then
        GoTo TrataErro
    End If

    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If
    
    Set BuscaClientePorCnpj = vRegistros
    
    Exit Function

TrataErro:
    Set BuscaClientePorCnpj = Nothing
    If vErro = 0 Then
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Cod_Cliente"
        vDb.Parameters.Remove "p_Cnpj"
        vDb.Parameters.Remove "p_Erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & vErro
    End If

End Function
Public Function BuscaClientePorCodigo(ByRef vCodCliente As Long) As Object
    Dim vSql As String
    Dim vRegistros As Object
    Dim vErro As Integer
    On Error GoTo TrataErro
    
    vErro = 0
    vSql = "Begin producao.pck_vda320.pr_cliente(:p_Cursor,:p_Cod_Cliente,:p_Cnpj,:p_Erro);END;"

    'oradatabase Parameters Add
    '  Nome do parametro
    '  Valor a passar ou receber do parametro
    '  Tipo do parametro 1 - Output, 2 - Input, 3 - Both
    vDb.Parameters.Add "p_Cursor", 0, 3
    vDb.Parameters("p_Cursor").ServerType = 102
    vDb.Parameters("p_Cursor").DynasetOption = &H2&
    vDb.Parameters("p_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
    vDb.Parameters.Add "p_Cod_Cliente", vCodCliente, 1
    vDb.Parameters.Add "p_Cnpj", 0, 1
    vDb.Parameters.Add "p_Erro", 0, 2
    vDb.ExecuteSQL vSql
    Set vRegistros = vDb.Parameters("p_Cursor").Value
    vErro = vDb.Parameters("p_Erro").Value
    vDb.Parameters.Remove "p_Cursor"
    vDb.Parameters.Remove "p_Cod_Cliente"
    vDb.Parameters.Remove "p_Cnpj"
    vDb.Parameters.Remove "p_Erro"

    If vErro <> 0 Then
        GoTo TrataErro
    End If

    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If
    
    Set BuscaClientePorCodigo = vRegistros
    
    Exit Function

TrataErro:
    Set BuscaClientePorCodigo = Nothing
    If vErro = 0 Then
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Cod_Cliente"
        vDb.Parameters.Remove "p_Cnpj"
        vDb.Parameters.Remove "p_Erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & vErro
    End If

End Function

Public Function IncluirR_Inteligencia_Mercado(ByRef vMesAno As Date, _
    ByRef vCodCliente As Long, ByRef vVlMeta As Double) As Boolean
    
    On Error GoTo TrataErro
    Dim v_Cod_Erro As Integer
    Dim v_Txt_Erro As String
    
    If IsDate(vMesAno) And vCodCliente <> 0 And vVlMeta >= 0 Then
        Dim vSql As String
        
        vSql = "Begin PRODUCAO.PCK_VDA960.PR_INSERT_META_CLIENTE(:p_mes_ano,:p_cod_cliente," & _
           ":p_vl_meta, :p_cod_erro, :p_txt_erro);END;"
        
        vDb.Parameters.Add "p_mes_ano", vMesAno, 1
        vDb.Parameters.Add "p_cod_cliente", vCodCliente, 1
        vDb.Parameters.Add "p_vl_meta", vVlMeta, 1
        vDb.Parameters.Add "p_cod_erro", 0, 2
        vDb.Parameters.Add "p_txt_erro", "", 2
        vDb.ExecuteSQL vSql
        v_Cod_Erro = vDb.Parameters("p_cod_erro").Value
        If v_Cod_Erro <> 0 Then
            v_Txt_Erro = vDb.Parameters("p_txt_erro").Value
        End If
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_cliente"
        vDb.Parameters.Remove "p_vl_meta"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        
        If v_Cod_Erro <> 0 Then
            GoTo TrataErro
        End If
        
        IncluirR_Inteligencia_Mercado = True
        
    Else
        IncluirR_Inteligencia_Mercado = False
    End If
    
    Exit Function
        
TrataErro:
    IncluirR_Inteligencia_Mercado = False
    If v_Cod_Erro = 0 Then
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_cliente"
        vDb.Parameters.Remove "p_vl_meta"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & v_Cod_Erro & " - Descri��o " & v_Txt_Erro
    End If
    
End Function
Public Function AtualizarR_Inteligencia_Mercado(ByRef vMesAno As Date, _
    ByRef vCodCliente As Long, ByRef vVlMeta As Double) As Boolean
    
    On Error GoTo TrataErro
    Dim v_Cod_Erro As Integer
    Dim v_Txt_Erro As String
    
    If IsDate(vMesAno) And vCodCliente <> 0 And vVlMeta >= 0 Then
        Dim vSql As String
        
        vSql = "Begin PRODUCAO.PCK_VDA960.PR_UPDATE_META_CLIENTE(:p_mes_ano,:p_cod_cliente," & _
           ":p_vl_meta, :p_cod_erro, :p_txt_erro);END;"
        
        vDb.Parameters.Add "p_mes_ano", vMesAno, 1
        vDb.Parameters.Add "p_cod_cliente", vCodCliente, 1
        vDb.Parameters.Add "p_vl_meta", vVlMeta, 1
        vDb.Parameters.Add "p_cod_erro", 0, 2
        vDb.Parameters.Add "p_txt_erro", "", 2
        vDb.ExecuteSQL vSql
        v_Cod_Erro = vDb.Parameters("p_cod_erro").Value
        If v_Cod_Erro <> 0 Then
            v_Txt_Erro = vDb.Parameters("p_txt_erro").Value
        End If
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_cliente"
        vDb.Parameters.Remove "p_vl_meta"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        
        If v_Cod_Erro <> 0 Then
            GoTo TrataErro
        End If
        
        AtualizarR_Inteligencia_Mercado = True
        
    Else
        AtualizarR_Inteligencia_Mercado = False
    End If
    
    Exit Function
        
TrataErro:
    AtualizarR_Inteligencia_Mercado = False
    If v_Cod_Erro = 0 Then
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_cliente"
        vDb.Parameters.Remove "p_vl_meta"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & v_Cod_Erro & " - Descri��o " & v_Txt_Erro
    End If
    
End Function
Public Function BuscaMetasPorMes(ByRef vMes_Ano As Date, ByRef vCod_Cliente As Long) As Object
    Dim vSql As String
    Dim vRegistros As Object
    Dim vErro As Integer
    On Error GoTo TrataErro
    
    vErro = 0
    If IsDate(vMes_Ano) And Not IsNull(vMes_Ano) Then
        vSql = "Begin PRODUCAO.PCK_VDA960.PR_BUSCA_METAS_CLIENTE(:p_Cursor,:p_Mes_Ano,:p_Cod_Cliente,:p_Erro);END;"
    
        'oradatabase Parameters Add
        '  Nome do parametro
        '  Valor a passar ou receber do parametro
        '  Tipo do parametro 1 - Output, 2 - Input, 3 - Both
        vDb.Parameters.Add "p_Cursor", 0, 3
        vDb.Parameters("p_Cursor").ServerType = 102
        vDb.Parameters("p_Cursor").DynasetOption = &H2&
        vDb.Parameters("p_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
        vDb.Parameters.Add "p_Mes_Ano", vMes_Ano, 1
        vDb.Parameters.Add "p_Cod_Cliente", vCod_Cliente, 1
        vDb.Parameters.Add "p_Erro", 0, 2
        vDb.ExecuteSQL vSql
        Set vRegistros = vDb.Parameters("p_Cursor").Value
        vErro = vDb.Parameters("p_Erro").Value
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Mes_Ano"
        vDb.Parameters.Remove "p_Cod_Cliente"
        vDb.Parameters.Remove "p_Erro"
    
        If vErro <> 0 Then
            GoTo TrataErro
        End If
    
        If vRegistros.EOF Then
            Set vRegistros = Nothing
        End If
    Else
        Set vRegistros = Nothing
    End If
    
    Set BuscaMetasPorMes = vRegistros
    Exit Function
    
TrataErro:
    Set BuscaMetasPorMes = Nothing
    If vErro = 0 Then
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Mes_Ano"
        vDb.Parameters.Remove "p_Cod_Cliente"
        vDb.Parameters.Remove "p_Erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & vErro
    End If

End Function
Public Function LimpaMetasPorMes(ByRef vMesAno As Date) As Boolean
    On Error GoTo TrataErro
    Dim v_Cod_Erro As Integer
    Dim v_Txt_Erro As String
    
    If IsDate(vMesAno) Then
        Dim vSql As String
        
        vSql = "Begin PRODUCAO.PCK_VDA960.PR_LIMPA_METAS_MES(:p_mes_ano," & _
           ":p_cod_erro, :p_txt_erro);END;"
        
        vDb.Parameters.Add "p_mes_ano", vMesAno, 1
        vDb.Parameters.Add "p_cod_erro", 0, 2
        vDb.Parameters.Add "p_txt_erro", "", 2
        vDb.ExecuteSQL vSql
        v_Cod_Erro = vDb.Parameters("p_cod_erro").Value
        If v_Cod_Erro <> 0 Then
            v_Txt_Erro = vDb.Parameters("p_txt_erro").Value
        End If
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        
        If v_Cod_Erro <> 0 Then
            GoTo TrataErro
        End If
        
        LimpaMetasPorMes = True
        
    Else
        LimpaMetasPorMes = False
    End If
    
    Exit Function
        
TrataErro:
    LimpaMetasPorMes = False
    If v_Cod_Erro = 0 Then
        vDb.Parameters.Remove "p_mes_ano"
        vDb.Parameters.Remove "p_cod_erro"
        vDb.Parameters.Remove "p_txt_erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & v_Cod_Erro & " - Descri��o " & v_Txt_Erro
    End If
    
End Function

Public Function BuscaClientePorNome(ByRef vNomeCliente As String) As Object
    Dim vSql As String
    Dim vRegistros As Object
    Dim vErro As Integer
    On Error GoTo TrataErro
    
    vErro = 0
    If vNomeCliente <> "" Then
        vSql = "Begin producao.pck_vda320.pr_cliente1(:p_Cursor,:p_Nome,:p_Erro);END;"
    
        'oradatabase Parameters Add
        '  Nome do parametro
        '  Valor a passar ou receber do parametro
        '  Tipo do parametro 1 - Output, 2 - Input, 3 - Both
        vDb.Parameters.Add "p_Cursor", 0, 3
        vDb.Parameters("p_Cursor").ServerType = 102
        vDb.Parameters("p_Cursor").DynasetOption = &H2&
        vDb.Parameters("p_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0
    
        vDb.Parameters.Add "p_Nome", vNomeCliente, 1
        vDb.Parameters.Add "p_Erro", 0, 2
        vDb.ExecuteSQL vSql
        Set vRegistros = vDb.Parameters("p_Cursor").Value
        vErro = vDb.Parameters("p_Erro").Value
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Nome"
        vDb.Parameters.Remove "p_Erro"
    
        If vErro <> 0 Then
            GoTo TrataErro
        End If
    
        If vRegistros.EOF Then
            Set vRegistros = Nothing
        End If
    Else
        Set vRegistros = Nothing
    End If
    
    Set BuscaClientePorNome = vRegistros
    Exit Function
    
TrataErro:
    Set BuscaClientePorNome = Nothing
    If vErro = 0 Then
        vDb.Parameters.Remove "p_Cursor"
        vDb.Parameters.Remove "p_Nome"
        vDb.Parameters.Remove "p_Erro"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & vErro
    End If
End Function
