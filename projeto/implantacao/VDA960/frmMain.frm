VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "VDA960 - Importa��o / Exporta��o Metas por Cliente"
   ClientHeight    =   8250
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6510
   LinkTopic       =   "Form1"
   ScaleHeight     =   8250
   ScaleWidth      =   6510
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ProgressBar ProgressBar 
      Height          =   375
      Left            =   120
      TabIndex        =   21
      Top             =   7680
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin Threed.SSPanel pnCabecalho 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   6510
      _Version        =   65536
      _ExtentX        =   11483
      _ExtentY        =   2170
      _StockProps     =   15
      BackColor       =   13160660
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin MSComDlg.CommonDialog DialogSelecArquivo 
         Left            =   3135
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   105
         TabIndex        =   20
         Top             =   1155
         Width           =   6540
         _ExtentX        =   11536
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   1635
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":0000
         PICN            =   "frmMain.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "frmMain.frx":0CF6
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -60
         Width           =   1455
      End
   End
   Begin VB.Frame frmExportacao 
      Caption         =   " Exporta��o "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3975
      Left            =   120
      TabIndex        =   4
      Top             =   3240
      Width           =   6255
      Begin Bot�o.cmd cmdLimpaMes 
         Height          =   495
         Left            =   1920
         TabIndex        =   11
         Top             =   3240
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "&Limpa m�s"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":A70D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExportar 
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   3240
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "&Exportar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":A729
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdBuscarExp 
         Height          =   375
         Left            =   4665
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "S&alvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":A745
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Frame frmFiltroExp 
         Caption         =   " Filtro exporta��o "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2055
         Left            =   255
         TabIndex        =   15
         Top             =   1065
         Width           =   5775
         Begin VB.TextBox txtMesAno 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            IMEMode         =   3  'DISABLE
            Left            =   240
            MaxLength       =   7
            TabIndex        =   7
            Top             =   600
            Width           =   1335
         End
         Begin VB.TextBox txtRazaoExp 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   2160
            TabIndex        =   9
            Top             =   1320
            Width           =   3495
         End
         Begin VB.TextBox txtCnpjExp 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   240
            MaxLength       =   14
            TabIndex        =   8
            Top             =   1320
            Width           =   1815
         End
         Begin VB.Label lbFiltroMesAno 
            Caption         =   "M�s / Ano (MM/AAAA):"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   18
            Top             =   360
            Width           =   2145
         End
         Begin VB.Label lbRazaoExp 
            Caption         =   "Nome:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   2175
            TabIndex        =   17
            Top             =   1080
            Width           =   1455
         End
         Begin VB.Label lbCNPJExp 
            Caption         =   "CNPJ/CPF:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   225
            TabIndex        =   16
            Top             =   1080
            Width           =   1335
         End
      End
      Begin VB.TextBox txtArquivoExp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   600
         Width           =   4335
      End
      Begin VB.Label lblArquivoExp 
         Caption         =   "Arquivo � exportar:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame frmImportacao 
      Caption         =   " Importa��o "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1815
      Left            =   105
      TabIndex        =   0
      Top             =   1320
      Width           =   6255
      Begin Bot�o.cmd cmdImportar 
         Height          =   495
         Left            =   240
         TabIndex        =   3
         Top             =   1065
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "&Importar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":A761
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtArquivoImp 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   225
         TabIndex        =   1
         Top             =   585
         Width           =   4335
      End
      Begin Bot�o.cmd cmdBuscarImp 
         Height          =   375
         Left            =   4680
         TabIndex        =   2
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "&Buscar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmMain.frx":A77D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lbArquivoImp 
         Caption         =   "Arquivo � importar:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Label lbStatus 
      Height          =   255
      Left            =   120
      TabIndex        =   22
      Top             =   7440
      Width           =   6255
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdBuscarExp_Click()
    'dizendo que a caixa deva abrir com o nome em branco
    DialogSelecArquivo.FileName = ""
    'dando nome a janela
    DialogSelecArquivo.DialogTitle = "Selecione o arquivo que deseja exportar"
    'mostrando o filtro sempre excel
    DialogSelecArquivo.Filter = "Arquivo do Excel (*.xls)|*.xls"
    DialogSelecArquivo.Flags = cdlOFNLongNames + cdlOFNExplorer
    DialogSelecArquivo.ShowSave
    txtArquivoExp.Text = DialogSelecArquivo.FileName
End Sub

Private Sub cmdBuscarImp_Click()
    'dizendo que a caixa deva abrir com o nome em branco
    DialogSelecArquivo.FileName = ""
    'dando nome a janela
    DialogSelecArquivo.DialogTitle = "Selecione o arquivo que deseja importar"
    'mostrando o filtro sempre excel
    DialogSelecArquivo.Filter = "Arquivo do Excel (*.xls)|*.xls"
    DialogSelecArquivo.Flags = cdlOFNLongNames + cdlOFNExplorer
    DialogSelecArquivo.ShowOpen
    
    Dim vNomeArquivo As String
    vNomeArquivo = DialogSelecArquivo.FileName
    VerificaExistenciaArquivo (vNomeArquivo)
    txtArquivoImp.Text = vNomeArquivo
End Sub

Private Sub cmdExportar_Click()
    Dim vAppExcel As Object
    Dim vBookExcelExp As Object
    On Error GoTo TrataErro
    
    If txtArquivoExp.Text <> "" Then
        If txtMesAno.Text <> "" Then
            Set vAppExcel = CreateObject("excel.application")
            Set vBookExcelExp = vAppExcel.Workbooks.Add
                           
            With vBookExcelExp.Sheets(1)
                .Range("A1:D1").Font.Bold = True
                .Range("A1").Value = "M�s/Ano"
                .Range("B1").Value = "CNPJ/CPF"
                .Range("C1").Value = "Valor Meta"
                .Range("D1").Value = "Valor Totalizado"
            End With
                           
            ' Realiza a busca das metas gravadas
            Dim vTabMetas As Object
            Dim vMesAno As Date
            Dim vCod_Cliente As Long
            Dim vLinha As Long
            
            vMesAno = CDate("1/" & txtMesAno.Text)
            If txtCnpjExp.Tag <> "" Then
                vCod_Cliente = txtCnpjExp.Tag
            Else
                vCod_Cliente = 0
            End If
            Set vTabMetas = BuscaMetasPorMes(vMesAno, vCod_Cliente)
            
            If Not vTabMetas Is Nothing Then
                AtualizaTela True
                ProgressBar.Max = vTabMetas.RecordCount
                ProgressBar.Value = 1
                lbStatus.Caption = "Iniciando processo de exporta��o..."
                DoEvents
        
                vTabMetas.MoveFirst
                vLinha = 2
                With vBookExcelExp.Sheets(1)
                    While Not vTabMetas.EOF
                        lbStatus.Caption = "Gravando linha " & vLinha - 1 & "..."
                        ProgressBar.Value = vLinha - 1
                        DoEvents
                        .Range("A" & vLinha).Value = Format(vTabMetas!Mes_Ano, "mmyyyy")
                        .Range("B" & vLinha).Value = vTabMetas!Cgc
                        .Range("C" & vLinha).Value = vTabMetas!Vl_Meta
                        .Range("D" & vLinha).Value = vTabMetas!Vl_Totalizado
                        .Range("A" & vLinha & ":D" & vLinha).HorizontalAlignment = 4  'Alinha a direita
                        vLinha = vLinha + 1
                        vTabMetas.MoveNext
                    Wend
                End With
                lbStatus.Caption = "Exporta��o finalizada."
                ProgressBar.Value = 0
                DoEvents
            End If
            
            vAppExcel.Columns("A:D").EntireColumn.AutoFit  'Ajusta todas colunas para caber informacao
            
            vBookExcelExp.SaveAs txtArquivoExp.Text
            
            ' Limpa campos
            LimpaCamposExportacao
            
            vBookExcelExp.Close
            vAppExcel.Quit
        Else
            MsgBox "Informe o m�s e ano que deseja exportar."
            txtMesAno.SetFocus
        End If
    Else
        MsgBox "Informe onde deseja salvar o arquivo."
        txtArquivoExp.SetFocus
    End If
    
    Exit Sub
    
TrataErro:
    If Err.Number <> 0 Then
        If InStr(1, Err.Description, "SaveAs", vbTextCompare) > 0 Then
            MsgBox "Planilha n�o foi salva. Exporte os dados novamente."
        Else
            If Err.Number = 1004 Then 'Erro referente ao Excel
                If MsgBox(Err.Description & vbNewLine & "Deseja tentar novamente?", vbYesNo, "Confirma?") = vbYes Then
                    Resume
                Else
                    Resume Next
                End If
            Else
                MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
                    Err.Description & " - gerado por " & Err.Source & vbNewLine & "Planilha n�o foi salva."
            End If
        End If
    End If
    LimpaCamposExportacao
    If Not vBookExcelExp Is Nothing Then
        vBookExcelExp.Close False
        vAppExcel.Quit
        Set vBookExcelExp = Nothing
    End If
End Sub

Private Sub cmdImportar_Click()
    On Error GoTo TrataErro
    'para a carga do arquivo excel!
    Dim vAppExcel As Object
    Dim vBookExcelImp As Object
    Dim vBookExcelExp As Object
    Dim vNomeArquivo As String
    
    vNomeArquivo = txtArquivoImp.Text
    If Not VerificaExistenciaArquivo(vNomeArquivo) Then
        Exit Sub
    End If
    
    'aqui o arquivo ja existe e vai ser aberto
    Set vAppExcel = CreateObject("excel.application")
    Set vBookExcelImp = vAppExcel.Workbooks.Open(vNomeArquivo)
    'selecionando a planilha 1
    vBookExcelImp.Sheets(1).Select
    DoEvents
    'exibindo o progress bar e label de status
    AtualizaTela True
    
    'eoliveira - 09/04/2014 - #807
    'alterando a tipagem da vari�vel para conseguir realizar a leitura de arquivos grandes.
    Dim vContador As Long
    vContador = 0
    While Trim(vBookExcelImp.Sheets(1).Range("A" & vContador + 1)) <> "" And vContador < 3
        vContador = vContador + 1
    Wend
    If vContador <= 1 Then
        vBookExcelImp.Close
        vAppExcel.Quit
        MsgBox "O arquivo est� aberto ou n�o tem linhas v�lidas!", vbExclamation
        AtualizaTela False
        Exit Sub
    End If
    ProgressBar.Max = vContador - 1  'Tirar a linha dos titulos da coluna
    ProgressBar.Value = 1
    lbStatus.Caption = "Iniciando processo de importa��o..."
    DoEvents
    
    'eoliveira - 09/04/2014 - #807
    'alterando a tipagem da vari�vel para conseguir realizar a leitura de arquivos grandes.
    Dim vLinhaLeitura As Long
    
    Dim vMesAno As Date
    Dim vData As String
    Dim vCnpj As String
    Dim vValorMeta As Double
    ' Variaveis para poder exportar o status mesmo quando encontrar dados inv�lidos
    Dim vMesAnoStr As String
    Dim vValorMetaStr As String
    
    Dim vRetornoValidaCnpj As String
    Dim vTabCliente As Object
    Dim vTabMetas As Object
    Dim vCodCliente As Long
    Dim vCritica As String
    Dim vCriticaAux As String
     
    ' Cria planilha para exporta��o do status de importa��o
    Set vBookExcelExp = vAppExcel.Workbooks.Add
    With vBookExcelExp.Sheets(1)
        .Range("A1:D1").Font.Bold = True
        .Range("A1").Value = "M�s/Ano"
        .Range("B1").Value = "CNPJ/CPF"
        .Range("C1").Value = "Valor Meta"
        .Range("D1").Value = "Critica"
    End With
    
    'inicia na linha 2 por causa do titulo das colunas
    vLinhaLeitura = 2
    While Trim(vBookExcelImp.Sheets(1).Range("A" & vLinhaLeitura)) <> ""
        lbStatus.Caption = "Lendo linha " & vLinhaLeitura - 1 & "..."
        ProgressBar.Value = vLinhaLeitura - 1
        DoEvents
        vCritica = ""
        'lendo a linha do arquivo excel
        vMesAnoStr = vBookExcelImp.Sheets(1).Range("A" & vLinhaLeitura)
        vData = "1/" & Left$(Format(Trim(vBookExcelImp.Sheets(1).Range("A" & vLinhaLeitura)), "000000"), 2) & "/" & _
            Right$(Format(Trim(vBookExcelImp.Sheets(1).Range("A" & vLinhaLeitura)), "000000"), 4)
        If ValidaData(vData) Then
            vMesAno = CDate(vData)
        Else
            vCritica = "Data inv�lida; "
        End If
        
        vCnpj = Trim(vBookExcelImp.Sheets(1).Range("B" & vLinhaLeitura))
        Set vTabCliente = BuscaClientePorCnpj(vCnpj)
        If vTabCliente Is Nothing Then
            vCritica = vCritica & "Cliente n�o encontrado" & "; "
        Else
            vCodCliente = vTabCliente!Cod_Cliente
        End If
        
        vValorMetaStr = vBookExcelImp.Sheets(1).Range("C" & vLinhaLeitura)
        If IsNumeric(vValorMetaStr) Then
            vValorMeta = CDbl(Trim(Replace(vValorMetaStr, ",", ".")))
            vCriticaAux = ValidaValorMeta(vValorMeta)
        Else
            vCriticaAux = "Valor da meta inv�lido"
        End If
        If vCriticaAux <> "" Then
            vCritica = vCritica & vCriticaAux
        End If
        
        If vCritica = "" Then
            Set vTabMetas = BuscaMetasPorMes(vMesAno, vCodCliente)
            If vTabMetas Is Nothing Then
                If IncluirR_Inteligencia_Mercado(vMesAno, vCodCliente, vValorMeta) Then
                    vCritica = "Incluido;"
                End If
            Else
                If AtualizarR_Inteligencia_Mercado(vMesAno, vCodCliente, vValorMeta) Then
                    vCritica = "Atualizado;"
                End If
            End If
        End If
        'Exporta dados e status de grava��o
        ExportarStatus vMesAnoStr, vCnpj, vValorMetaStr, vCritica, vLinhaLeitura, vBookExcelExp
        vLinhaLeitura = vLinhaLeitura + 1
    Wend
    
    lbStatus.Caption = "Importa��o finalizada."
    ProgressBar.Value = 0
    DoEvents
    
    'Grava o arquivo de saida com os status
    vAppExcel.Columns("A:D").EntireColumn.AutoFit  'Ajusta todas colunas para caber informacao
    Dim vArqStatus As String
    vArqStatus = Trim(txtArquivoImp.Text)
    vBookExcelExp.SaveAs Left$(vArqStatus, Len(vArqStatus) - 4) & "_retorno.xls"

    txtArquivoImp.Text = ""
    
    ' Fecha o arquivo
    vBookExcelImp.Close False
    vBookExcelExp.Close False
    vAppExcel.Quit
    ' Desaloca memoria
    Set vBookExcelImp = Nothing
    Set vBookExcelExp = Nothing
    Exit Sub

TrataErro:
    If Err.Number <> 0 Then
        If InStr(1, Err.Description, "SaveAs", vbTextCompare) > 0 Then
            MsgBox "Planilha de retorno n�o foi salva. Importe os dados novamente."
        Else
            If Err.Number = 1004 Then 'Erro referente ao Excel
                If MsgBox(Err.Description & vbNewLine & "Deseja tentar novamente?", vbYesNo, "Confirma?") = vbYes Then
                    Resume
                Else
                    Resume Next
                End If
            Else
                vCritica = vCritica & "Erro n�mero #" & Str$(Err.Number) & " - " & _
                    Err.Description & " - gerado por " & Err.Source & "; "
                Resume Next
            End If
        End If
    End If
    ' Fecha o arquivo
    If Not vBookExcelExp Is Nothing Then
        vBookExcelImp.Close False
        vBookExcelExp.Close False
        vAppExcel.Quit
        ' Desaloca memoria
        Set vBookExcelImp = Nothing
        Set vBookExcelExp = Nothing
    End If
End Sub
Private Function ValidaData(vData As String) As Boolean
    On Error GoTo TrataErro
    Dim vDataStr As String
    vDataStr = vData
    If Len(vData) = 7 Then
        vDataStr = "1/" & vData
    End If
    If Not IsDate(vDataStr) Then
        ValidaData = False
        Exit Function
    End If
    
    'Valida o m�s
    Dim vMes As Integer
    
    vMes = Val(Mid$(vDataStr, InStr(1, vDataStr, "/", 1) + 1, InStr(InStr(1, vDataStr, "/", 1) + 1, vDataStr, "/", 1) - (InStr(1, vDataStr, "/", 1) + 1)))
    If vMes < 1 Or vMes > 12 Then
        ValidaData = False
        Exit Function
    End If
    
    'Valida o ano
    Dim vAno As Integer
    vAno = Val(Right$(vData, 4))
    If vAno < 1900 Or vAno > 9999 Then
        ValidaData = False
        Exit Function
    End If
    ValidaData = True
    Exit Function
TrataErro:
    ValidaData = False
End Function
Private Function ValidaValorMeta(vValorMeta As Double) As String
    Dim vMensagem As String
    vMensagem = ""
    If vValorMeta < 0 Or vValorMeta > 999999999999.99 Then
        vMensagem = "Valor da meta inv�lido"
    End If
        
    ValidaValorMeta = vMensagem
End Function
Private Function VerificaExistenciaArquivo(ByRef vNomeArquivo As String) As Boolean
    'este metodo vai verificar a existencia do arquivo selecionado
    VerificaExistenciaArquivo = True
    If Trim(vNomeArquivo) = "" Then
        MsgBox "O arquivo n�o foi selecionado!", vbCritical
        VerificaExistenciaArquivo = False
    End If
    If Dir(vNomeArquivo) = "" Then
        MsgBox "O arquivo selecionado n�o existe!", vbCritical
        VerificaExistenciaArquivo = False
    End If
    If Not VerificaExistenciaArquivo Then
        vNomeArquivo = ""
    End If
   
End Function

Private Sub cmdLimpaMes_Click()
    If txtMesAno.Text <> "" Then
        Dim vMesAno As Date
        vMesAno = CDate("1/" & txtMesAno.Text)
        If MsgBox("Esse procedimento exclui todas as metas do m�s informado." & _
                vbNewLine & "Tem certeza que deveja excluir as metas?", _
                vbYesNo + vbDefaultButton2, "Confirma?") = vbYes Then
            If LimpaMetasPorMes(vMesAno) Then
                MsgBox ("O m�s " & txtMesAno.Text & " foi exclu�do com sucesso.")
                txtMesAno.Text = ""
            End If
        End If
    Else
        MsgBox "Informe o m�s e ano que deseja exportar."
        txtMesAno.SetFocus
    End If
End Sub

Private Sub Form_Load()
    'Conecta com o banco de dados
    ConectaDb
    AtualizaTela False
End Sub

Private Sub txtCnpjExp_KeyPress(KeyAscii As Integer)
    KeyAscii = Numero(KeyAscii)
End Sub

Private Sub txtCnpjExp_LostFocus()
    CarregaCliente (False)
End Sub

Private Sub txtMesAno_Change()
    If Len(txtMesAno.Text) = 2 Then
        txtMesAno.Text = Left$(txtMesAno.Text, 2) + "/" + Right$(txtMesAno.Text, Len(txtMesAno.Text) - 2)
        txtMesAno.SelStart = 4
    End If
    If Len(txtMesAno.Text) = 7 Then
        If Not ValidaData(txtMesAno.Text) Then
            MsgBox "Data inv�lida."
            txtMesAno.SetFocus
        End If
    End If
End Sub

Private Sub cmdSair_Click()
    DisconectaDb
    End
End Sub

Sub AtualizaTela(ByRef vHabilita As Boolean)
    lbStatus.Visible = vHabilita
    ProgressBar.Visible = vHabilita
End Sub

Private Sub ExportarStatus(ByRef vMesAno As String, ByRef vCnpj As String, ByRef vValorMeta As String, _
    vCritica As String, ByVal vLinha As Long, ByRef vBookExcel As Excel.Workbook)
    With vBookExcel.Sheets(1)
        .Range("A" & vLinha).Value = vMesAno
        .Range("B" & vLinha).Value = vCnpj
        .Range("C" & vLinha).Value = vValorMeta
        .Range("D" & vLinha).Value = vCritica
        .Range("A" & vLinha & ":C" & vLinha).HorizontalAlignment = 4  'Alinha a direita
        .Range("D" & vLinha).HorizontalAlignment = 2                  'Alinha a esquerda
    End With
End Sub

Private Sub LimpaCamposExportacao()
    ' Limpa campos
    txtArquivoExp.Text = ""
    txtMesAno.Text = ""
    txtCnpjExp.Text = ""
    txtCnpjExp.Tag = "" 'Limpa codigo do cliente
    txtRazaoExp.Text = ""
End Sub

Private Sub txtMesAno_KeyPress(KeyAscii As Integer)
    KeyAscii = Numero(KeyAscii)
End Sub

Private Sub txtMesAno_LostFocus()
    If txtMesAno.Text <> "" Then
        If Not ValidaData(txtMesAno.Text) Then
            MsgBox "Data inv�lida."
            txtMesAno.SetFocus
        End If
    End If
End Sub

Private Sub txtRazaoExp_DblClick()
    frmClienteFimPedido.Form_Que_Chamou = "frmMain"
    frmClienteFimPedido.Show
    StayOnTop frmClienteFimPedido
End Sub

Public Sub CarregaCliente(ByRef vBuscaCodigo As Boolean)
    Dim vTabCliente As Object
    
    If txtCnpjExp.Text <> "" Then
        If vBuscaCodigo Then
            If Val(txtCnpjExp.Tag) <> 0 Then
                Set vTabCliente = BuscaClientePorCodigo(Val(txtCnpjExp.Tag))
            End If
        Else
            Set vTabCliente = BuscaClientePorCnpj(txtCnpjExp.Text)
        End If
        If Not vTabCliente Is Nothing Then
            txtCnpjExp.Tag = vTabCliente!Cod_Cliente
            txtCnpjExp.Text = vTabCliente!Cgc
            txtRazaoExp.Text = vTabCliente!Nome_Cliente
            DoEvents
            Set vTabCliente = Nothing
        Else
            MsgBox "Cliente n�o encontrado."
            txtCnpjExp.SetFocus
        End If
    Else
        txtCnpjExp.Tag = ""
        txtCnpjExp.Text = ""
        txtRazaoExp.Text = ""
    End If
End Sub

Private Sub txtRazaoExp_KeyPress(KeyAscii As Integer)
    KeyAscii = 0
End Sub
