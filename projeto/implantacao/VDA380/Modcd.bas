Attribute VB_Name = "ModCD"
Option Explicit

Public lngCD As Long
Public strTp_banco As String * 1
Public strPath As String
Public lngArq As Long
Public TextLine As String

Public vDepMulti As String  'Vai receber DEP99 ou branco


Public Sub Get_CD(strUnidade As String)
On Error GoTo erro

  strPath = strUnidade & ":\oracle\dados\32bits\cd.txt"
  'strPath = strUnidade & ":\Arquivos-Alethea\cd.txt"
  lngArq = FreeFile

  Open strPath For Input Shared As #lngArq
    Do While Not EOF(lngArq)
      Line Input #lngArq, TextLine
      lngCD = Mid(TextLine, 1, 2)
      strTp_banco = Mid(TextLine, 3, 1)
      'lngCD = 1
      'strTp_banco = "U"
    Loop
  Close #lngArq
  
Exit Sub

erro:

If Err = 53 Then
   MsgBox "O Arquivo de controle de tipo de Banco n�o foi encontrado !!!", 16, "Controle de Banco"
   End
Else
   MsgBox Err.Description, 16, "Controle de Banco"
End If

End Sub
