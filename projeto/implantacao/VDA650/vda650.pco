      *!---------------------------------------------------------------!*
      *! PROGRAMA : VDA650                                             !*
      *! DATA     : 21/10/98                                           !*
      *! DESCRICAO: RELATORIO DE INFORMACOES SOBRE BLOQUEIO DE PEDIDOS !*
      *! AUTOR    : RONALDO GABOS DE ARAUJO                            !*
      *! ALTERACAO: - ABRIL/2001      ELAINE                           !*
      *!              INCLUS�O DE QUEBRA DE N�VEL POR REPRESENTANTE    !*
      *! 	    - JANEIRO/2002    ELAINE                           !*
      *!              INCLUS�O DO C�LCULO DA MARGEM POR ITEM, REPRESEN-!*
      *!              TANTE, FILIAL E REGIONAL.                        !*
      *!            - ACERTO P/ SAIR MARGEM NEGATIVA - 30/12/04        !*
      *!---------------------------------------------------------------!*
       IDENTIFICATION DIVISION.
       PROGRAM-ID. VDA650.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
            SELECT RVDA650 ASSIGN TO PRINTER.

       DATA DIVISION.
       FILE SECTION.
       FD  RVDA650
           LABEL RECORD OMITTED
           VALUE OF FILE-ID IS WK-TITLE.
       01  LINHA        PIC X(132).

       WORKING-STORAGE SECTION.

      *------------------ DEFINICAO DE VARIAVEIS HOST ------------------*
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.

       01  USERNAME PIC  X(23) VALUE "BATCH_CON/PROD@PRODUCAO".
      
            
       01  W-DATA-INI           PIC  X(06).
       01  W-DATA-FIM           PIC  X(06).
       01  W-CDREG              PIC S9(04)      COMP.
       01  W-CDREGIONAL         PIC S9(04)      COMP.
       01  W-CDREGIONAL-AUX     PIC S9(04)      COMP.
       01  W-NMREGIONAL         PIC  X(25).
       01  W-CDFILIAL           PIC S9(04)      COMP.
       01  W-CDFILIAL-AUX       PIC S9(04)      COMP.
       01  W-NMFILIAL           PIC  X(25).
       01  W-CDREPRES           PIC S9(04)      COMP.
       01  W-NMREPRES           PIC  X(40).
       01  W-TPREPRES           PIC  X(01).
       01  W-NUMPED             PIC S9(07)      COMP.
       01  W-SEQPED             PIC S9(01)      COMP.
       01  W-TPPEDIDO           PIC S9(02)      COMP.
       01  W-CODDPK             PIC S9(05)      COMP.
       01  W-FORNEC             PIC  X(10).
       01  W-CDFABRICA          PIC  X(19).
       01  W-QTDATEND           PIC S9(06)      COMP.
       01  W-PCDESC3            COMP-2.
       01  W-VRLIQ              COMP-2.
       01  W-CONT1              PIC S9(03)      COMP.
       01  W-ITEM-PED           PIC S9(03)      COMP.
       01  W-CDLOJA             PIC S9(02)      COMP.
       01  W-LCR-BRUTO          PIC S9(10)V9999 COMP-3.
       01  W-LCR-BRUTO-REG      PIC S9(10)V9999 COMP-3.
       01  W-LCR-BRUTO-REP      PIC S9(10)V9999 COMP-3.
       01  W-LCR-BRUTO-FIL      PIC S9(10)V9999 COMP-3.
       01  W-VL-BVISTA          PIC S9(10)V9999 COMP-3.
       01  W-VL-BVISTA-REG      PIC S9(10)V9999 COMP-3.
       01  W-VL-BVISTA-REP      PIC S9(10)V9999 COMP-3.
       01  W-VL-BVISTA-FIL      PIC S9(10)V9999 COMP-3.
       01  W-MARGEM             PIC S9(04)V99   COMP-3.
       01  W-MARGEM-REG         PIC S9(04)V99   COMP-3.
       01  W-MARGEM-REP         PIC S9(04)V99   COMP-3.
       01  W-MARGEM-FIL         PIC S9(04)V99   COMP-3.
       01  DATSIS-DTREAL        PIC X(10).
       01  DATSIS-DTFATU        PIC X(06).
       01  DATSIS-DTREF         PIC X(06).
       01  DATSIS-HORA          PIC X(05).

           EXEC SQL END DECLARE SECTION END-EXEC.

      *---------------------- CHAMADA SQL - ORACLE ---------------------*
           EXEC SQL INCLUDE SQLCA END-EXEC.

           EXEC SQL INCLUDE ORACA END-EXEC.

      *-------------------- DEFINICAO DOS CURSORES --------------------*

           EXEC SQL DECLARE CURSOR_CONTADOR CURSOR FOR
              SELECT COD_FILIAL
              FROM FILIAL
              WHERE COD_REGIONAL = 0 AND
                    DIVISAO = 'D'
           END-EXEC.

           EXEC SQL DECLARE CURSOR_REGIONAL CURSOR FOR
              SELECT COD_FILIAL, NOME_FILIAL
              FROM FILIAL
              WHERE COD_REGIONAL = 0 AND
                    DIVISAO = 'D'
              ORDER BY COD_FILIAL
           END-EXEC.

           EXEC SQL DECLARE CURSOR_FILIAL CURSOR FOR
              SELECT COD_FILIAL, NOME_FILIAL
              FROM FILIAL
              WHERE (COD_FILIAL   = :W-CDREGIONAL) OR
                    (COD_REGIONAL = :W-CDREGIONAL  AND
                     DIVISAO = 'D')
              ORDER BY COD_FILIAL
           END-EXEC.

           EXEC SQL DECLARE CURSOR_REPRES CURSOR FOR
              SELECT COD_REPRES, NOME_REPRES, TIPO
              FROM REPRESENTANTE
              WHERE COD_FILIAL = :W-CDFILIAL    AND
                    TIPO IN ('A','V','M','R')
              ORDER BY COD_REPRES
           END-EXEC.

           EXEC SQL DECLARE CURSOR_PEDIDO CURSOR FOR
              SELECT /*+ index(a IX_dt_emiss_nota) */ 
                     NVL(B.NUM_PEDIDO, 0),
                     NVL(B.SEQ_PEDIDO, 0),
                     NVL(A.TP_PEDIDO, 0),
                     NVL(B.COD_DPK, 0),
                     NVL(D.SIGLA, ' '),
                     NVL(C.COD_FABRICA, ' '),
                     NVL(B.QTD_ATENDIDA, 0),
                     NVL(B.PC_DESC3, 0),
                     NVL(B.NUM_ITEM_PEDIDO, 0),
                     NVL(B.COD_LOJA, 0),
                     NVL(A.COD_FILIAL, 0)
	      FROM  ITPEDNOTA_VENDA B,
                    PEDNOTA_VENDA   A,
                    ITEM_ESTOQUE    E,
                    ITEM_CADASTRO   C,
                    FORNECEDOR      D
              WHERE DECODE(:W-TPREPRES,'R',A.COD_REPRES,A.COD_VEND) =
                           :W-CDREPRES                             AND
                  ((A.TP_PEDIDO = 10 AND :W-TPREPRES = 'R')
                   OR
                  (A.TP_PEDIDO = 21 AND :W-TPREPRES IN ('V','M'))
                   OR
                  (A.TP_PEDIDO = 22 AND :W-TPREPRES IN ('A','M'))
                  ) AND
                  A.DT_EMISSAO_NOTA >= TO_DATE(:W-DATA-INI,'DDMMRR') AND
                  A.DT_EMISSAO_NOTA <  TO_DATE(:W-DATA-FIM,'DDMMRR') + 1
                  AND A.TP_TRANSACAO+0  = 1                AND
                  A.TP_DPKBLAU+0    = 0                    AND
                  A.SITUACAO+0      = 0                    AND
                  A.FL_GER_NFIS||'' = 'S'                  AND
                  A.BLOQ_ITEM_DESC3||'' = 'L'              AND
                  B.COD_LOJA        = A.COD_LOJA           AND
                  B.NUM_PEDIDO      = A.NUM_PEDIDO         AND
                  B.SEQ_PEDIDO      = A.SEQ_PEDIDO         AND
                  B.PC_DESC3       <> 0                    AND
                  B.BLOQ_DESC3      = 'S'                  AND
                  C.COD_DPK         = B.COD_DPK            AND
                  D.COD_FORNECEDOR  = C.COD_FORNECEDOR     AND
                  E.COD_DPK         = C.COD_DPK            AND
                  B.COD_LOJA        = E.COD_LOJA           AND
                  E.SITUACAO        = 0
             ORDER BY A.COD_FILIAL,
                      D.SIGLA,
                      C.COD_FABRICA
           END-EXEC.

      *----------------------- NOME DOS ARQUIVOS -----------------------*
        01 WK-TITLE.
           02 WK-DIRETORIO       PIC X(14) VALUE
              "../relatorios/".
           02 WK-NOMEREL         PIC X(06)       VALUE "vda650".
           02 WK-OPREL           PIC X(01)       VALUE SPACES.
           02 WK-DIAFEC          PIC X(06)       VALUE SPACES.

      *  01 WK-TITLE.
      *	   02 WK-DIRETORIO       PIC X(45)       VALUE
      *	      "//nt2000_internet/informe/web/arquivos/vda650".
      *	   02 WK-NOMEREL         PIC X(06)       VALUE "vda650".
      *    02 WK-OPREL           PIC X(01)       VALUE SPACES.

           COPY "lib/lwcab.lib".

      *---------------------- LINHAS DO RELATORIO ----------------------*

      * 01  TRACO               PIC  X(132)     VALUE ALL "-".
       01  BRANCO               PIC  X(132)     VALUE SPACES.
       01  W-CAB01.
           2 F                  PIC  X(054)     VALUE
             "DPK  INFORMACOES SOBRE OS BLOQUEIOS DE PEDIDOS    REF:".
           2 W-C01-DTINI        PIC  X(010).
           2 F                  PIC  X(003)     VALUE " A ".
           2 W-C01-DTFIM        PIC  X(010).
           2 F                  PIC  X(021)     VALUE
	     "    DIARIO     EMIS: ".
           2 W-C01-DTEMIS       PIC  X(010).
           2 F                  PIC  X(004)     VALUE SPACES.
           2 W-C01-HORA.
             3 W-C01-HH         PIC  X(02)      VALUE SPACES.
             3 F                PIC  X(01)      VALUE ":".
             3 W-C01-MM         PIC  X(02)      VALUE SPACES.
           2 F                  PIC  X(10)      VALUE "    PAG.: ".
           2 W-C01-PAG          PIC  ZZZ9.

       01  W-CAB02.
           2 F                  PIC X(15)       VALUE "FILIAL       : ".
           2 W-C02-FILIAL       PIC 9(04).
           2 F                  PIC X(05)       VALUE "   - ".
           2 W-C02-NOMFIL       PIC X(25).

       01  W-CAB03.
           2 W-C03-TIPO         PIC X(15)       VALUE "REPRESENTANTE: ".
           2 W-C03-CODIGO       PIC 9(04).
           2 F                  PIC X(03)       VALUE " - ".
           2 W-C03-NOME         PIC X(40).

       01  W-CAB04.
           2 FILLER             PIC X(12)       VALUE " PEDIDO     ".
	   2 FILLER             PIC X(10)       VALUE "COD.DPK   ".
	   2 FILLER             PIC X(17)       VALUE
	     "FORNECEDOR       ".
	   2 FILLER             PIC X(18)       VALUE
	     "COD.FABRICA       ".
	   2 FILLER             PIC X(16)       VALUE
	     "QTDE.ATENDIDA   ".
	   2 FILLER             PIC X(12)       VALUE "DESCONTO    ".
	   2 FILLER             PIC X(13)       VALUE "VALOR LIQUIDO".
           2 FILLER             PIC X(14)       VALUE "       %MARGEM".

       01  W-CAB05.
           2 F                  PIC X(112) VALUE
             "---------   -------   ----------   -------------------   -
      -      "------------   --------    -------------       -------".

       01  W-CAB06.
           2 F                  PIC X(15)       VALUE "REGIONAL     : ".
           2 W-C06-FILIAL       PIC 9(04).
           2 F                  PIC X(05)       VALUE "   - ".
           2 W-C06-NOMFIL       PIC X(25).

       01  W-DET01.
           2 W-D01-NUMPED       PIC 9(07).
           2 FILLER             PIC X(01)       VALUE "-".
           2 W-D01-SEQPED       PIC 9(01).
           2 FILLER             PIC X(05)       VALUE SPACES.
           2 W-D01-CODDPK       PIC 9(05).
           2 FILLER             PIC X(03)       VALUE SPACES.
           2 W-D01-FORNEC       PIC X(10).
           2 FILLER             PIC X(03)       VALUE SPACES.
           2 W-D01-CODFABR      PIC X(19).
           2 FILLER             PIC X(09)       VALUE SPACES.
           2 W-D01-QTDATEND     PIC ZZZ.ZZ9-.
           2 FILLER             PIC X(05)       VALUE SPACES.
           2 W-D01-DESCTO       PIC Z9,99-.
           2 FILLER             PIC X(03)       VALUE SPACES.
           2 W-D01-VRLIQ        PIC ZZ.ZZZ.ZZ9,99.
	   2 FILLER             PIC X(07)       VALUE SPACES.
	   2 W-D01-PERCM        PIC ZZZ9,99-.

       01  W-DET02.
           2 FILLER             PIC X(09)       VALUE "TOTAL DO ".
           2 W-D02-TIPO         PIC X(13)       VALUE "REPRESENTANTE".
           2 FILLER             PIC X(01)       VALUE ":".
           2 FILLER             PIC X(38)       VALUE SPACES.
           2 W-D02-QTDTOTR      PIC Z.ZZZ.ZZ9-.
           2 FILLER             PIC X(14)       VALUE SPACES.
           2 W-D02-VRTOTR       PIC ZZ.ZZZ.ZZ9,99.
	   2 FILLER             PIC X(07)       VALUE SPACES.
	   2 W-D02-PERCM        PIC ZZZ9,99-.

       01  W-DET03.
           2 FILLER             PIC X(23)       VALUE
	     "TOTAL DA FILIAL       :".
           2 FILLER             PIC X(38)       VALUE SPACES.
           2 W-D03-QTDTOTF      PIC Z.ZZZ.ZZ9-.
           2 FILLER             PIC X(14)       VALUE SPACES.
           2 W-D03-VRTOTF       PIC ZZ.ZZZ.ZZ9,99.
	   2 FILLER             PIC X(07)       VALUE SPACES.
	   2 W-D03-PERCM        PIC ZZZ9,99-.

       01  W-DET04.
           2 F                  PIC X(23)       VALUE
	     "TOTAL DA REGIONAL     :".
           2 F                  PIC X(38)       VALUE SPACES.
           2 W-D04-QTDTOTR      PIC Z.ZZZ.ZZ9-.
           2 F                  PIC X(14)       VALUE SPACES.
           2 W-D04-VRTOTR       PIC ZZ.ZZZ.ZZ9,99.
	   2 FILLER             PIC X(07)       VALUE SPACES.
	   2 W-D04-PERCM        PIC ZZZ9,99-.
      *--------------------- VARIAVEIS DE TRABALHO ---------------------*

       01  WK-AUXDT             PIC 9(2)        VALUE 0.
       01  WK-DIAHOJE.
           2 WK-DIA             PIC 9(2).
           2 WK-MES             PIC 9(2).
           2 WK-ANO             PIC 9(2).
       01  WK-HORAR.
           2 WK-HH              PIC  9(02).
           2 WK-MM              PIC  9(02).
           2 WK-SS              PIC  9(02).
           2 WK-CC              PIC  9(02).
       01  WK-DIAHORA REDEFINES WK-HORAR PIC 9(08).
       01  WK-CPAG              PIC  9(02)      VALUE 0.
       01  WK-CONTROLE          PIC  X(25)      VALUE SPACES.
       01  W-TERMINOU           PIC  9(01).
       01  W-TERMINOU-REG       PIC  9(01).
       01  W-CONT-REG           PIC  9(03).
       01  W-CONT               PIC  9(03).
       01  W-AUX                PIC  9(01).
       01  W-FIM-REPRES         PIC  9(01).
       01  W-FIM-PEDIDO         PIC  9(01).
       01  WK-OPCAO             PIC  X(01).
       01  W-QTDTOT-FILIAL      PIC S9(07)      VALUE 0.
       01  W-VRTOT-FILIAL       PIC S9(10)V99   VALUE 0.
       01  W-QTDTOT-REGIONAL    PIC S9(07)      VALUE 0.
       01  W-VRTOT-REGIONAL     PIC S9(10)V99   VALUE 0.
       01  W-DTINICIAL          PIC  X(06).
       01  W-DTFINAL            PIC  X(06).
       01  WK-CLIN              PIC  9(02)      VALUE 0.
       01  W-CDREPRES-ANT       PIC  9(04)      VALUE 0.
       01  W-QTDATEND-TOT       PIC S9(07)      VALUE 0.
       01  W-VRLIQ-TOT          PIC S9(10)V99   VALUE 0.
       01  WK-CONF              PIC  X(01)      VALUE SPACES.
       01  W-ERRO               PIC  9(01)      VALUE 0.
       01  W-ERRO-OPCAO         PIC  9(01)      VALUE 0.
       01  W-JA-IMPRIMIU        PIC  9(01)      VALUE 0.

       01  W-DATA-AUX.
           2 W-DTAUX-DIA        PIC 9(02).
           2 W-DTAUX-MES        PIC 9(02).
           2 W-DTAUX-ANO.
             3 W-DTAUX-ANO1     PIC 9(02).
             3 W-DTAUX-ANO2     PIC 9(02).

       01  W-DATA-CAB.
           2 W-DTCAB-DIA        PIC 9(02).
           2 W-BARRA1           PIC X(01)       VALUE "/".
           2 W-DTCAB-MES        PIC 9(02).
           2 W-BARRA2           PIC X(01)       VALUE "/".
           2 W-DTCAB-ANO.
             3 W-DTCAB-ANO1     PIC 9(02).
             3 W-DTCAB-ANO2     PIC 9(02).

      *--------------------- PROCEDURE DIVISION ----------------------*
       PROCEDURE DIVISION.
       INICIAR SECTION.
       000-INICIO.

           EXEC ORACLE OPTION (ORACA=YES) END-EXEC.
           EXEC SQL CONNECT :USERNAME END-EXEC.

           EXEC SQL WHENEVER SQLWARNING CONTINUE END-EXEC.

           EXEC SQL WHENEVER SQLERROR GO TO 900-ERRO-ORACLE END-EXEC.

           MOVE 0 TO W-TERMINOU-REG, W-TERMINOU.
	   MOVE 1 TO W-AUX.
           PERFORM 020-LE-DATAS.
           PERFORM 010-INICIALIZA.
           MOVE 90 TO WK-CLIN.
           MOVE 1 TO W-ERRO-OPCAO.
           PERFORM 025-LE-PARAMETROS THRU 025-LE-PARAMETROS-SAI.
           MOVE 0 TO W-CONT-REG, W-CONT
           OPEN OUTPUT RVDA650.
           EXEC SQL OPEN CURSOR_CONTADOR END-EXEC.
           PERFORM 110-CONTADOR.
           EXEC SQL CLOSE CURSOR_CONTADOR END-EXEC.
           EXEC SQL OPEN CURSOR_REGIONAL END-EXEC.
           MOVE 0 TO W-TERMINOU, W-TERMINOU-REG.
           PERFORM 095-LE-REGIONAL.
      *	   IF W-AUX = 1
      *	      MOVE W-CDREGIONAL TO W-CDREGIONAL-AUX.
           PERFORM UNTIL W-CONT-REG = W-CONT
              MOVE 0 TO W-TERMINOU
              MOVE 0 TO  W-QTDTOT-REGIONAL, W-VRTOT-REGIONAL
              EXEC SQL OPEN CURSOR_FILIAL END-EXEC
              PERFORM 030-LE-FILIAL
	      IF W-AUX = 1
	         MOVE W-CDFILIAL TO W-CDFILIAL-AUX
	      END-IF
	      MOVE 0 TO W-AUX
              PERFORM UNTIL W-TERMINOU = 1
                 MOVE 0  TO W-JA-IMPRIMIU,
                 EXEC SQL OPEN CURSOR_REPRES END-EXEC
                 MOVE 0 TO W-FIM-REPRES
		 MOVE 0 TO W-QTDTOT-FILIAL
		 MOVE 0 TO W-VRTOT-FILIAL
                 PERFORM 040-LE-REPRESENTANTE
                 PERFORM UNTIL W-FIM-REPRES = 1
                    EXEC SQL OPEN CURSOR_PEDIDO END-EXEC
                    MOVE 0 TO W-FIM-PEDIDO
                    PERFORM 050-LE-PEDIDO UNTIL W-FIM-PEDIDO = 1
                    EXEC SQL CLOSE CURSOR_PEDIDO END-EXEC
                    PERFORM 040-LE-REPRESENTANTE
                 END-PERFORM
                 EXEC SQL CLOSE CURSOR_REPRES END-EXEC
                 IF W-JA-IMPRIMIU = 1
		    MOVE ZEROS TO W-MARGEM-FIL
		    COMPUTE W-MARGEM-FIL = (W-LCR-BRUTO-FIL /
		                           W-VL-BVISTA-FIL) * 100
		    MOVE ZEROS TO W-LCR-BRUTO-FIL
		    MOVE ZEROS TO W-VL-BVISTA-FIL
      *	    	    MOVE W-MARGEM-FIL    TO W-D03-PERCM
                    MOVE W-QTDTOT-FILIAL TO W-D03-QTDTOTF
                    MOVE W-VRTOT-FILIAL  TO W-D03-VRTOTF
                    WRITE LINHA FROM W-DET03 AFTER 2
                    ADD W-QTDTOT-FILIAL TO W-QTDTOT-REGIONAL
                    ADD W-VRTOT-FILIAL  TO W-VRTOT-REGIONAL
                 END-IF
                 MOVE 90 TO WK-CLIN
                 PERFORM 030-LE-FILIAL
              END-PERFORM
              EXEC SQL CLOSE CURSOR_FILIAL END-EXEC
	      MOVE ZEROS TO W-MARGEM-REG
	      COMPUTE W-MARGEM-REG = (W-LCR-BRUTO-REG /
	                              W-VL-BVISTA-REG) * 100
	      MOVE ZEROS TO W-LCR-BRUTO-REG
	      MOVE ZEROS TO W-VL-BVISTA-REG
      *       MOVE W-MARGEM-REG TO W-D04-PERCM
              MOVE W-QTDTOT-REGIONAL TO W-D04-QTDTOTR
              MOVE W-VRTOT-REGIONAL TO W-D04-VRTOTR
              IF W-VRTOT-REGIONAL <> 0
                 WRITE LINHA FROM W-DET04 AFTER 2
              END-IF
              MOVE 90 TO WK-CLIN
              MOVE 0 TO W-TERMINOU-REG
              PERFORM 095-LE-REGIONAL
           END-PERFORM.
           EXEC SQL CLOSE CURSOR_REGIONAL END-EXEC.
           PERFORM 910-FINALIZA.
           STOP RUN 0.

      *-----------------------------------------------------------------*
      *             ROTINA PARA INICIALIZACAO DE VARIAVEIS              *
      *-----------------------------------------------------------------*
       010-INICIALIZA.

           ACCEPT   WK-DIAHOJE  FROM    DATE.
           ACCEPT   WK-DIAHORA  FROM    TIME.
           MOVE     WK-DIA      TO      WK-AUXDT.
           MOVE     WK-ANO      TO      WK-DIA.
           MOVE     WK-AUXDT    TO      WK-ANO.
           MOVE     WK-HH       TO      W-C01-HH.
           MOVE     WK-MM       TO      W-C01-MM.
           IF WK-ANO > 90
              MOVE 19 TO W-DTCAB-ANO1
           ELSE
              MOVE 20 TO W-DTCAB-ANO2
           END-IF.
           MOVE WK-DIA TO W-DTCAB-DIA.
           MOVE WK-MES TO W-DTCAB-MES.
           MOVE WK-ANO TO W-DTCAB-ANO.
           MOVE W-DATA-CAB TO W-C01-DTEMIS.

           MOVE W-DTINICIAL TO W-DATA-AUX.
           IF W-DTAUX-ANO2 > 90
              MOVE 19 TO W-DTCAB-ANO1
           ELSE
              MOVE 20 TO W-DTCAB-ANO1
           END-IF.
           MOVE W-DTAUX-DIA TO W-DTCAB-DIA.
           MOVE W-DTAUX-MES TO W-DTCAB-MES.
           MOVE W-DTAUX-ANO TO W-DTCAB-ANO.
           MOVE W-DATA-CAB TO W-C01-DTINI.

           MOVE W-DTFINAL TO W-DATA-AUX.
           IF W-DTAUX-ANO2 > 90
              MOVE 19 TO W-DTCAB-ANO1
           ELSE
              MOVE 20 TO W-DTCAB-ANO1
           END-IF.
           MOVE W-DTAUX-DIA TO W-DTCAB-DIA.
           MOVE W-DTAUX-MES TO W-DTCAB-MES.
           MOVE W-DTAUX-ANO TO W-DTCAB-ANO.
           MOVE W-DATA-CAB   TO W-C01-DTFIM.

      *-----------------------------------------------------------------*
      *               ROTINA PARA LER AS DATAS DO SISTEMA               *
      *-----------------------------------------------------------------*
       020-LE-DATAS.

           MOVE 'LE DATA' TO WK-CONTROLE.
           EXEC SQL SELECT
                        NVL(TO_CHAR(SYSDATE        ,'YYMMDD'), ' ' ),
                        NVL(TO_CHAR(DT_INI_FECH_DIA,'YYMMDD'), ' ' ),
                        NVL(TO_CHAR(DT_INI_FECH_DIA,'DDMMYY'), ' ' ),
                        NVL(TO_CHAR(SYSDATE, 'HH24:MI')      , ' ' )
                    INTO DATSIS-DTREAL,
		         DATSIS-DTFATU,
                         DATSIS-DTREF,
			 DATSIS-HORA
                    FROM DATAS
           END-EXEC.

           MOVE DATSIS-HORA   TO CAB-HORA.
           DISPLAY "HORA: " DATSIS-HORA.
	   MOVE DATSIS-DTFATU TO WK-DIAFEC, WK-DTRED.
           MOVE DATSIS-DTREAL TO WK-DTREALRED.

      *-----------------------------------------------------------------*
      *             ROTINA PARA LER OS PARAMETROS DO SISTEMA            *
      *-----------------------------------------------------------------*
       025-LE-PARAMETROS.

           DISPLAY "OPCAO: <D> DIARIO , <E> ESPORADICO".
           ACCEPT WK-OPCAO.

           IF WK-OPCAO <> 'D' AND 'd' AND 'E' AND 'e'
              DISPLAY "OPCAO INVALIDA !!"
              GO TO 025-LE-PARAMETROS
           END-IF.

           IF WK-OPCAO = 'E' OR 'e'
              MOVE "ESPORADICO"  TO  CAB-FREQ
              MOVE "e"  TO  WK-OPREL
              MOVE 1 TO W-ERRO
              PERFORM 035-LE-DATAREF UNTIL W-ERRO = 0
           ELSE
              MOVE  "DIARIO"     TO  CAB-FREQ
              MOVE  "d"          TO  WK-OPREL
              MOVE DATSIS-DTREF  TO  W-DATA-INI, W-DATA-FIM
              DISPLAY "OPCAO  --->", WK-OPCAO
           END-IF.
           COPY "lib/lrcab.lib".

           MOVE  "VDA650"                            TO  CAB-PROG.
           MOVE  SPACES                              TO  CAB-REFE.
           MOVE  "INFORMACAO DE BLOQUEIO DE PEDIDO"  TO  CAB-TITU.
           MOVE  "/A"                                TO  CAB-RELA.

       025-LE-PARAMETROS-SAI.
           EXIT.

      *-----------------------------------------------------------------*
      *               ROTINA PARA LER O CURSOR DE FILIAIS               *
      *-----------------------------------------------------------------*
       030-LE-FILIAL.

           MOVE 'LE CURSOR_FILIAL' TO WK-CONTROLE.
           EXEC SQL FETCH CURSOR_FILIAL
                    INTO :W-CDFILIAL, :W-NMFILIAL
           END-EXEC.

           IF SQLCODE = 1403
              MOVE 1 TO W-TERMINOU
           END-IF.

      *-----------------------------------------------------------------*
      *                 ROTINA PARA ENTRAR COM AS DATAS                 *
      *-----------------------------------------------------------------*
       035-LE-DATAREF.

           DISPLAY "***  DIGITE DATA INICIAL DDMMAA: ".
           ACCEPT  W-DTINICIAL.

           DISPLAY "***  DIGITE DATA FINAL   DDMMAA: ".
           ACCEPT  W-DTFINAL.

           DISPLAY "***  VERIFIQUE  AS  DATAS !!".
           DISPLAY "DT.INICIAL :" W-DTINICIAL.
           DISPLAY "DT.FINAL :"   W-DTFINAL.
           DISPLAY "CONFIRME <S> OU <N>".
           ACCEPT WK-CONF.

           IF WK-CONF <> 'S' AND 's'
              MOVE 1 TO W-ERRO
           ELSE
              MOVE 0 TO W-ERRO
              MOVE W-DTINICIAL  TO W-DATA-INI
              MOVE W-DTFINAL    TO W-DATA-FIM.

      *-----------------------------------------------------------------*
      *           ROTINA PARA LER O CURSOR DE REPRESENTANTES            *
      *-----------------------------------------------------------------*
       040-LE-REPRESENTANTE.

           MOVE 'LE CURSOR_REPRES' TO WK-CONTROLE.

           EXEC SQL FETCH CURSOR_REPRES
                    INTO :W-CDREPRES, :W-NMREPRES, :W-TPREPRES
           END-EXEC.

           IF SQLCODE = 1403
              MOVE 1 TO W-FIM-REPRES
              PERFORM 090-TOTAL-REPRESENTANTE
           END-IF.

      *-----------------------------------------------------------------*
      *               ROTINA PARA LER O CURSOR DE PEDIDOS               *
      *-----------------------------------------------------------------*
       050-LE-PEDIDO.

           MOVE 'LE CURSOR_PEDIDO' TO WK-CONTROLE.
           MOVE ZEROS TO W-MARGEM.

           EXEC SQL FETCH CURSOR_PEDIDO
                    INTO :W-NUMPED,
                         :W-SEQPED,
                         :W-TPPEDIDO,
                         :W-CODDPK,
                         :W-FORNEC,
                         :W-CDFABRICA,
                         :W-QTDATEND,
                         :W-PCDESC3,
                         :W-ITEM-PED,
                         :W-CDLOJA
	   END-EXEC.

      *	   DISPLAY "W-NUMPED " W-NUMPED.
      *	   DISPLAY "W-SEQPED " W-SEQPED.
      *	   DISPLAY "W-CODDPK " W-CODDPK.
      *	   DISPLAY "W-CDLOJA " W-CDLOJA.
      *	   DISPLAY "W-TPREPRES " W-TPREPRES.
      *    DISPLAY "W-LCR-BRUTO " W-LCR-BRUTO.
      *	   DISPLAY "W-VL-BVISTA " W-VL-BVISTA.

           IF SQLCODE = 1403
              MOVE 1 TO W-FIM-PEDIDO
           ELSE
	      PERFORM 120-LE-VALOR-MARGEM
	      COMPUTE W-MARGEM = (W-LCR-BRUTO / W-VL-BVISTA) * 100
              PERFORM 060-PROCESSA-PEDIDO
           END-IF.

      *-----------------------------------------------------------------*
      *      ROTINA PARA PROCESSAR OS PEDIDOS E GERAR O RELATORIO       *
      *-----------------------------------------------------------------*
       060-PROCESSA-PEDIDO.

           IF WK-CLIN > 80
              PERFORM 070-IMPRIME-CABEC
           END-IF.

           IF W-CDREPRES NOT EQUAL W-CDREPRES-ANT
              PERFORM 090-TOTAL-REPRESENTANTE

              PERFORM 080-IMPRIME-REPRESENTANTE
           END-IF.
      *    IF (W-CDFILIAL-AUX <> W-CDFILIAL) AND
      *	      (W-CDREGIONAL-AUX = W-CDREGIONAL)
      *	      COMPUTE W-LCR-BRUTO-REG = W-LCR-BRUTO-REG + W-LCR-BRUTO
      *	      COMPUTE W-VL-BVISTA-REG = W-VL-BVISTA-REG + W-VL-BVISTA
      *	      MOVE W-CDFILIAL TO W-CDFILIAL-AUX
      *	   ELSE
      *	      IF W-CDREGIONAL-AUX <> W-CDREGIONAL
      *          MOVE W-CDREGIONAL TO W-CDREGIONAL-AUX
      *	      ELSE
      *	   COMPUTE W-LCR-BRUTO-REG = W-LCR-BRUTO-REG + W-LCR-BRUTO.
      *	   COMPUTE W-LCR-BRUTO-FIL = W-LCR-BRUTO-FIL + W-LCR-BRUTO.
      *	   COMPUTE W-LCR-BRUTO-REP = W-LCR-BRUTO-REP + W-LCR-BRUTO.
      *	   COMPUTE W-LCR-BRUTO-REG = W-LCR-BRUTO-REG + W-LCR-BRUTO.
      *	   COMPUTE W-VL-BVISTA-FIL = W-VL-BVISTA-FIL + W-VL-BVISTA.
      *	   COMPUTE W-VL-BVISTA-REP = W-VL-BVISTA-REP + W-VL-BVISTA.
      *	   COMPUTE W-VL-BVISTA-REG = W-VL-BVISTA-REG + W-VL-BVISTA.
           MOVE  W-NUMPED TO W-D01-NUMPED.
           MOVE  W-SEQPED TO W-D01-SEQPED.
           MOVE  W-CODDPK    TO W-D01-CODDPK.
           MOVE  W-FORNEC    TO W-D01-FORNEC.
           MOVE  W-CDFABRICA TO W-D01-CODFABR.
           MOVE  W-QTDATEND  TO W-D01-QTDATEND.
           MOVE  W-PCDESC3   TO W-D01-DESCTO.
	   MOVE  W-MARGEM    TO W-D01-PERCM.
           PERFORM 100-LE-VALOR-LIQUIDO.
      *    IF (W-CDFILIAL-AUX <> W-CDFILIAL) AND
      *	      (W-CDREGIONAL-AUX = W-CDREGIONAL)
      *	      COMPUTE W-LCR-BRUTO-REG = W-LCR-BRUTO-REG + W-LCR-BRUTO
      *	      COMPUTE W-VL-BVISTA-REG = W-VL-BVISTA-REG + W-VL-BVISTA
      *	      MOVE W-CDFILIAL TO W-CDFILIAL-AUX
      *	   ELSE
      *	      IF W-CDREGIONAL-AUX <> W-CDREGIONAL
      *          MOVE W-CDREGIONAL TO W-CDREGIONAL-AUX
      *	      ELSE
	   COMPUTE W-LCR-BRUTO-REG = W-LCR-BRUTO-REG + W-LCR-BRUTO.
	   COMPUTE W-VL-BVISTA-REG = W-VL-BVISTA-REG + W-VL-BVISTA.
      *    COMPUTE W-LCR-BRUTO-FIL = W-LCR-BRUTO-FIL + W-LCR-BRUTO.
      *	   COMPUTE W-VL-BVISTA-FIL = W-VL-BVISTA-FIL + W-VL-BVISTA.
	   COMPUTE W-LCR-BRUTO-REP = W-LCR-BRUTO-REP + W-LCR-BRUTO.
	   COMPUTE W-VL-BVISTA-REP = W-VL-BVISTA-REP + W-VL-BVISTA.
           MOVE  W-VRLIQ     TO W-D01-VRLIQ.
           WRITE LINHA FROM W-DET01.
           ADD   W-QTDATEND  TO W-QTDATEND-TOT.
           ADD   W-VRLIQ     TO W-VRLIQ-TOT.
           ADD   1 TO WK-CLIN.

      *-----------------------------------------------------------------*
      *         ROTINA PARA IMPRESSAO DO CABECALHO DO RELATORIO         *
      *-----------------------------------------------------------------*
       070-IMPRIME-CABEC.

           ADD 1 TO WK-PAG.
           MOVE WK-PAG TO CAB-PAGI.
           WRITE LINHA FROM CAB-1 AFTER PAGE.
           WRITE LINHA FROM  CAB-2  AFTER 1 LINE.
           MOVE W-CDREGIONAL TO W-C06-FILIAL.
	   MOVE W-NMREGIONAL TO W-C06-NOMFIL.
           MOVE W-CDFILIAL TO W-C02-FILIAL.
           MOVE W-NMFILIAL TO W-C02-NOMFIL.
           WRITE LINHA FROM W-CAB06 AFTER 2.
           WRITE LINHA FROM W-CAB02 AFTER 1.
           PERFORM 080-IMPRIME-REPRESENTANTE.
           WRITE LINHA FROM W-CAB04 AFTER 1.
           WRITE LINHA FROM W-CAB05 AFTER 1.
           MOVE 10 TO WK-CLIN.
           MOVE 1 TO W-JA-IMPRIMIU.

      *-----------------------------------------------------------------*
      *          ROTINA PARA IMPRIMIR A LINHA DE REPRESENTANTE          *
      *-----------------------------------------------------------------*
       080-IMPRIME-REPRESENTANTE.

           IF W-TPREPRES = 'R'
              MOVE "REPRESENTANTE: " TO W-C03-TIPO
           ELSE
              MOVE "VEND. INTERNO: " TO W-C03-TIPO
           END-IF.
           IF W-FIM-REPRES = 0
              MOVE W-CDREPRES TO W-C03-CODIGO
              MOVE W-NMREPRES TO W-C03-NOME
              WRITE LINHA FROM W-CAB03 AFTER 2
              WRITE LINHA FROM BRANCO  AFTER 1
              MOVE W-CDREPRES TO W-CDREPRES-ANT
              ADD 3 TO WK-CLIN
           END-IF.

      *-----------------------------------------------------------------*
      *           ROTINA PARA IMPRIMIR O TOTAL DO REPRESENTANTE         *
      *-----------------------------------------------------------------*
       090-TOTAL-REPRESENTANTE.


      *      MOVE ZEROS TO W-VL-BVISTA-REP.
      *       MOVE ZEROS TO W-LCR-BRUTO-REP.
           MOVE W-C03-TIPO TO W-D02-TIPO.
           IF W-JA-IMPRIMIU = 1
              MOVE W-QTDATEND-TOT TO W-D02-QTDTOTR
              MOVE W-VRLIQ-TOT  TO W-D02-VRTOTR
	      MOVE ZEROS TO W-MARGEM-REP
      *	      DISPLAY "W-LCR-BRUTO-REP " W-LCR-BRUTO-REP
      *	      DISPLAY "W-VL-BVISTA-REP " W-VL-BVISTA-REP
	      COMPUTE W-MARGEM-REP = (W-LCR-BRUTO-REP /
	                              W-VL-BVISTA-REP) * 100
      *	      DISPLAY "W-MARGEM-REP " W-MARGEM-REP
      *	      MOVE W-MARGEM-REP TO W-D02-PERCM
      *	      DISPLAY "W-D02-PERCM " W-D02-PERCM
	      ADD W-VRLIQ-TOT   TO W-VRTOT-FILIAL
              ADD W-QTDATEND-TOT  TO W-QTDTOT-FILIAL
	      COMPUTE W-LCR-BRUTO-FIL = W-LCR-BRUTO-FIL +
	                                W-LCR-BRUTO-REP
	      COMPUTE W-VL-BVISTA-FIL = W-VL-BVISTA-FIL +
	                                W-VL-BVISTA-REP
	      MOVE ZEROS TO W-LCR-BRUTO-REP
	      MOVE ZEROS TO W-VL-BVISTA-REP
              MOVE 0 TO W-QTDATEND-TOT, W-VRLIQ-TOT
              WRITE LINHA FROM W-DET02 AFTER 2
              ADD 2 TO WK-CLIN
           END-IF.

      *-----------------------------------------------------------------*
      *              ROTINA PARA LER O CURSOR DE REGIONAIS              *
      *-----------------------------------------------------------------*
       095-LE-REGIONAL.

           MOVE 'LE CURSOR REGIONAL' TO WK-CONTROLE.
           EXEC SQL FETCH CURSOR_REGIONAL
                    INTO :W-CDREGIONAL, :W-NMREGIONAL
           END-EXEC.
           ADD 1 TO W-CONT.
           IF SQLCODE = 1403
              MOVE 1 TO W-TERMINOU-REG
           END-IF.

      *-----------------------------------------------------------------*
      *             ROTINA PARA LER O VALOR LIQUIDO DO ITEM             *
      *-----------------------------------------------------------------*
       100-LE-VALOR-LIQUIDO.

           MOVE 'LE VALOR LIQUIDO' TO WK-CONTROLE.
           EXEC SQL SELECT NVL(VL_LIQUIDO,0)
                    INTO :W-VRLIQ
                    FROM V_PEDLIQ_VENDA
                    WHERE NUM_PEDIDO      = :W-NUMPED   AND
                          SEQ_PEDIDO      = :W-SEQPED   AND
                          COD_LOJA        = :W-CDLOJA   AND
                          NUM_ITEM_PEDIDO = :W-ITEM-PED AND
                          SITUACAO        = 0
           END-EXEC.

           IF SQLCODE = 1403
              MOVE 0 TO W-VRLIQ
           END-IF.

      *-----------------------------------------------------------------*
      *             ROTINA PARA CONTAR A QTDE DE REGIONAIL              *
      *-----------------------------------------------------------------*
       110-CONTADOR.

           MOVE 'LE CURSOR CONTADOR' TO WK-CONTROLE.
           PERFORM UNTIL W-TERMINOU = 1
              EXEC SQL FETCH CURSOR_CONTADOR
                   INTO :W-CDREG
              END-EXEC
              ADD 1 TO W-CONT-REG

           IF SQLCODE = 1403
              MOVE 1 TO W-TERMINOU
           END-IF
           END-PERFORM.

      *-----------------------------------------------------------------*
      *  ROTINA PARA LER O LUCRO E BASE A VISTA PARA CLADULO DA MARGEM  *
      *-----------------------------------------------------------------*
       120-LE-VALOR-MARGEM.

           EXEC SQL SELECT NVL(VL_LUCRO_BRUTO, 0),
                           NVL(VL_BVISTA, 0)
                    INTO :W-LCR-BRUTO,
			 :W-VL-BVISTA
                    FROM ITPEDNOTA_VENDA
                    WHERE NUM_PEDIDO      = :W-NUMPED   AND
                          SEQ_PEDIDO      = :W-SEQPED   AND
                          COD_LOJA        = :W-CDLOJA   AND
                          NUM_ITEM_PEDIDO = :W-ITEM-PED AND
                          SITUACAO        = 0
           END-EXEC.

           IF SQLCODE = 1403
              MOVE 0 TO W-LCR-BRUTO, W-VL-BVISTA
           END-IF.

      *-----------------------------------------------------------------*
      *                        TRATA ERRO ORACLE                        *
      *-----------------------------------------------------------------*
       900-ERRO-ORACLE.
           DISPLAY "*** ERRO NA EXECUCAO - PROGRAMA VDA650 ***".
           DISPLAY SQLERRMC.
           DISPLAY SQLERRML.
           DISPLAY ORASTXTC.
           DISPLAY WK-CONTROLE.
           EXEC SQL WHENEVER SQLERROR CONTINUE END-EXEC.
           EXEC SQL ROLLBACK WORK END-EXEC.
           STOP RUN 255.

      *-----------------------------------------------------------------*
      *                    ROTINA P/FINALIZAR BANCO                     *
      *-----------------------------------------------------------------*
       910-FINALIZA.
           EXEC SQL WHENEVER SQLERROR GO TO 900-ERRO-ORACLE END-EXEC.
           EXEC SQL COMMIT WORK RELEASE END-EXEC.
           DISPLAY "PGM. VDA650 - ENCERRADO OK !".
