VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmCON065 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CON065 - Listagem de E-mail"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10845
   Icon            =   "frmCON065.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8205
   ScaleWidth      =   10845
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Crit�rios para Filtro:"
      Height          =   1155
      Left            =   30
      TabIndex        =   7
      Top             =   1200
      Width           =   4215
      Begin VB.ComboBox cboSegmento 
         Height          =   315
         ItemData        =   "frmCON065.frx":23D2
         Left            =   1920
         List            =   "frmCON065.frx":23E5
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   510
         Width           =   2205
      End
      Begin VB.CheckBox CHKPRIMEIRO 
         Appearance      =   0  'Flat
         Caption         =   "Trazer somente primeiro e-mail"
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   90
         TabIndex        =   11
         ToolTipText     =   "Caso o cliente possuir mais de um email, trazer apenas 1"
         Top             =   870
         Width           =   2595
      End
      Begin VB.ComboBox cboDepto 
         Height          =   315
         ItemData        =   "frmCON065.frx":2416
         Left            =   1920
         List            =   "frmCON065.frx":2423
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   180
         Width           =   2205
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar Por Segmento:"
         Height          =   195
         Left            =   405
         TabIndex        =   13
         Top             =   570
         Width           =   1470
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Filtrar Por Departamento:"
         Height          =   195
         Left            =   120
         TabIndex        =   8
         Top             =   270
         Width           =   1755
      End
   End
   Begin MSComctlLib.ProgressBar PB1 
      Height          =   165
      Left            =   3660
      TabIndex        =   5
      Top             =   870
      Visible         =   0   'False
      Width           =   1515
      _ExtentX        =   2672
      _ExtentY        =   291
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin MSFlexGridLib.MSFlexGrid msfG1 
      Height          =   5445
      Left            =   30
      TabIndex        =   3
      Top             =   2400
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   9604
      _Version        =   393216
      Cols            =   6
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      Top             =   1125
      Width           =   10785
      _ExtentX        =   19024
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   10845
      _ExtentX        =   19129
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6429
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "09/18/2007"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:36"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   1005
      Left            =   9810
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON065.frx":244E
      PICN            =   "frmCON065.frx":246A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEmail 
      Height          =   1005
      Left            =   7710
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Consultar e-mail"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Consultar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON065.frx":3144
      PICN            =   "frmCON065.frx":3160
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcel 
      Height          =   1005
      Left            =   8760
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Exportar para Excel"
      Top             =   45
      Width           =   1005
      _ExtentX        =   1773
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Enviar para Excel"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON065.frx":3E3A
      PICN            =   "frmCON065.frx":3E56
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerarTabela 
      Height          =   1005
      Left            =   5520
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Exportar para tabela do Boletim"
      Top             =   45
      Width           =   2085
      _ExtentX        =   3678
      _ExtentY        =   1773
      BTYPE           =   3
      TX              =   "Gerar tabela boletim"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCON065.frx":4378
      PICN            =   "frmCON065.frx":4394
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "Aguarde..."
      Height          =   195
      Left            =   3660
      TabIndex        =   6
      Top             =   660
      Visible         =   0   'False
      Width           =   885
   End
   Begin VB.Image Image1 
      Height          =   1185
      Left            =   -90
      Picture         =   "frmCON065.frx":6A0E
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1455
   End
End
Attribute VB_Name = "frmCON065"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2

Private Sub cmdEmail_Click()
    Dim vObj As Object
    Dim vCampos As Byte
    Dim vRegs As Long
    Dim vSql As String
    Dim vCrit As String
    
    PB1.Visible = True
    Label1.Visible = True
    
    Me.Refresh
    
    PB1.Value = 0
    
    msfG1.TextMatrix(0, 1) = "C�d Cliente"
    msfG1.TextMatrix(0, 2) = "CGC"
    msfG1.TextMatrix(0, 3) = "E-mail"
    msfG1.TextMatrix(0, 4) = "Segmento"
    msfG1.TextMatrix(0, 5) = "Departamento"
    
    msfG1.ColWidth(0) = 250
    msfG1.ColWidth(1) = 1000
    msfG1.ColWidth(2) = 1500
    msfG1.ColWidth(3) = 3000
    msfG1.ColWidth(4) = 2000
    msfG1.ColWidth(5) = 1500
    
    If cboDepto <> "" Then
        vCrit = " WHERE cod_depto = " & Val(cboDepto)
    Else
        vCrit = ""
    End If
    
    Criar_Cursor
    
    vBanco.Parameters.Remove "DEPTO"
    vBanco.Parameters.Add "DEPTO", Val(cboDepto), 1
    
    vBanco.Parameters.Remove "PRIMEIRO"
    vBanco.Parameters.Add "PRIMEIRO", CHKPRIMEIRO.Value, 1
    
    vBanco.Parameters.Remove "SEGMENTO"
    vBanco.Parameters.Add "SEGMENTO", cboSegmento, 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CON065.PR_SELECT_SEGMENTO(:vCursor, :PRIMEIRO, :DEPTO, :SEGMENTO)")
    
    Set vObj = vBanco.Parameters("vCursor").Value
    
    vCampos = vObj.Fields.Count - 1
    
    vRegs = vObj.RecordCount
    
    stbBarra.Panels(1).Text = "Registros encontrados: " & vRegs
    
    PB1.Max = vRegs + 1
    
    msfG1.Rows = vRegs + 1
    
    For i = 1 To vRegs
        PB1.Value = PB1.Value + 1
        msfG1.TextMatrix(i, 1) = IIf(IsNull(vObj("Cod_Cliente")), "", vObj("Cod_Cliente"))
        msfG1.TextMatrix(i, 2) = vObj("cgc")
        msfG1.TextMatrix(i, 3) = IIf(IsNull(vObj("Email")), "", vObj("Email"))
        msfG1.TextMatrix(i, 4) = IIf(IsNull(vObj("Segmento")), "", vObj("Segmento"))
        msfG1.TextMatrix(i, 5) = IIf(IsNull(vObj("Depto")), "", vObj("Depto"))
        vObj.MoveNext
    Next
    
    Set vObj = Nothing
    
    PB1.Visible = False
    Label1.Visible = False
    
End Sub

Private Sub cmdExcel_Click()
    Dim vExcel As Object
    Dim vPasta As Object
    Dim vPlanilha As Object
    
    Set vExcel = CreateObject("Excel.Application")
    Set vPasta = vExcel.Workbooks.Add
    Set vPlanilha = vPasta.ActiveSheet
    
    Label1.Visible = True
    PB1.Value = 0
    PB1.Max = msfG1.Rows
    PB1.Visible = True
    
    For i = 0 To msfG1.Rows - 1
        PB1.Value = PB1.Value + 1
        vPlanilha.Cells(i + 1, 1).Value = msfG1.TextMatrix(i, 1)
        vPlanilha.Cells(i + 1, 2).Value = "'" & msfG1.TextMatrix(i, 2)
        vPlanilha.Cells(i + 1, 3).Value = msfG1.TextMatrix(i, 3)
        vPlanilha.Cells(i + 1, 4).Value = msfG1.TextMatrix(i, 4)
        vPlanilha.Cells(i + 1, 5).Value = msfG1.TextMatrix(i, 5)
    Next
    
    vPlanilha.Columns("A:I").EntireColumn.AutoFit
    vPlanilha.Range(vPlanilha.Cells(1, 1), vPlanilha.Cells(1, 9)).Font.Bold = True
    
    vExcel.Visible = True
    
    PB1.Value = 0
    PB1.Visible = False
    Label1.Visible = False
    
    Set vExcel = Nothing
    
End Sub

Private Sub cmdGerarTabela_Click()
    Me.MousePointer = vbhourglas
    Aguardar
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_CON065.PR_INSERT_BOLETIM")

    Unload frmAguardar
    Me.MousePointer = vbNormal
    MsgBox "Listagem Gerada!", vbInformation, "Informa��o"
End Sub

Private Sub cmdSair_Click()
    If vVB_Generica_001.Sair = 6 Then
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    End If
End Sub

Private Sub Form_Load()
    frmLogo.Show 1
        
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "CON065", True, Me)

    If vErro <> "" Then
        End
    End If

    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco
End Sub

Public Sub Criar_Cursor()
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 2
    vBanco.Parameters("vCursor").serverType = ORATYPE_CURSOR
    vBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub
