Imports Oracle.DataAccess.Client
Public Class Frete
    ' HERDA A CLASSE LOGISTICA
    Inherits Logistica
    Private _banco As MaxxDados
    Private _array As ArrayList

    Private _codLoja As Int32
    ''' <summary>
    ''' retorna codigo da loja
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodLoja() As Int32
        Get
            Return _codLoja
        End Get
        Set(ByVal value As Int32)
            _codLoja = value
        End Set
    End Property

    Private _codTransportadora As Int32
    ''' <summary>
    ''' retorna o c�digo da transportadora
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodTransportadora() As Int32
        Get
            Return _codTransportadora
        End Get
        Set(ByVal value As Int32)
            _codTransportadora = value
        End Set
    End Property

    Private _tipoTransacao As Int32
    ''' <summary>
    ''' retorna o tipo de transa��o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TipoTransacao() As Int32
        Get
            Return _tipoTransacao
        End Get
        Set(ByVal value As Int32)
            _tipoTransacao = value
        End Set
    End Property

    Private _dataVigencia As Date
    ''' <summary>
    ''' retorna a data de vigencia
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DataVigencia() As Date
        Get
            Return _dataVigencia
        End Get
        Set(ByVal value As Date)
            _dataVigencia = value
        End Set
    End Property

    Private _tipoTabela As Int32
    ''' <summary>
    ''' retorna o tipo de tabela
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TipoTabela() As Int32
        Get
            Return _tipoTabela
        End Get
        Set(ByVal value As Int32)
            _tipoTabela = value
        End Set
    End Property

    Private _kmInicio As Decimal
    ''' <summary>
    ''' retorna o km inicial
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property KmInicio() As Decimal
        Get
            Return _kmInicio
        End Get
        Set(ByVal value As Decimal)
            _kmInicio = value
        End Set
    End Property

    Private _kmFim As Decimal
    ''' <summary>
    ''' retorna o km final
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property KmFim() As Decimal
        Get
            Return _kmFim
        End Get
        Set(ByVal value As Decimal)
            _kmFim = value
        End Set
    End Property

    Private _pesoInicio As Decimal
    ''' <summary>
    ''' retorna o peso inicial
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PesoInicio() As Decimal
        Get
            Return _pesoInicio
        End Get
        Set(ByVal value As Decimal)
            _pesoInicio = value
        End Set
    End Property

    Private _pesoFim As Decimal
    ''' <summary>
    ''' retorna o peso inicial
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PesoFim() As Decimal
        Get
            Return _pesoFim
        End Get
        Set(ByVal value As Decimal)
            _pesoFim = value
        End Set
    End Property

    Private _valorFrete As Decimal
    ''' <summary>
    ''' retorna o valor do frete
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorFrete() As Decimal
        Get
            Return _valorFrete
        End Get
        Set(ByVal value As Decimal)
            _valorFrete = value
        End Set
    End Property

    Private _pcFrete As Decimal
    ''' <summary>
    ''' retorna a % do frete
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcFrete() As Decimal
        Get
            Return _pcFrete
        End Get
        Set(ByVal value As Decimal)
            _pcFrete = value
        End Set
    End Property

    Private _valorNotaInicio As Decimal
    ''' <summary>
    ''' retorna o valor inicial da nota
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorNotaInicio() As Decimal
        Get
            Return _valorNotaInicio
        End Get
        Set(ByVal value As Decimal)
            _valorNotaInicio = value
        End Set
    End Property

    Private _valorNotaFim As Decimal
    ''' <summary>
    ''' retorna o valor final da nota
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorNotaFim() As Decimal
        Get
            Return _valorNotaFim
        End Get
        Set(ByVal value As Decimal)
            _valorNotaFim = value
        End Set
    End Property

    Private _pcSeguro As Decimal
    ''' <summary>
    ''' retorna % do seguro
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcSeguro() As Decimal
        Get
            Return _pcSeguro
        End Get
        Set(ByVal value As Decimal)
            _pcSeguro = value
        End Set
    End Property

    Private _valorCAT As Decimal
    ''' <summary>
    ''' retorna o valor do CAT
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorCAT() As Decimal
        Get
            Return _valorCAT
        End Get
        Set(ByVal value As Decimal)
            _valorCAT = value
        End Set
    End Property

    Private _valorTAS As Decimal
    ''' <summary>
    ''' retorna o valor do TAS
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorTAS() As Decimal
        Get
            Return _valorTAS
        End Get
        Set(ByVal value As Decimal)
            _valorTAS = value
        End Set
    End Property

    Private _valorITR As Decimal
    ''' <summary>
    ''' retorna o valor do ITR
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorITR() As Decimal
        Get
            Return _valorITR
        End Get
        Set(ByVal value As Decimal)
            _valorITR = value
        End Set
    End Property

    Private _valorCTRC As Decimal
    ''' <summary>
    ''' retorna o valor do CTRC
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorCTRC() As Decimal
        Get
            Return _valorCTRC
        End Get
        Set(ByVal value As Decimal)
            _valorCTRC = value
        End Set
    End Property

    Private _pcGRIS As Decimal
    ''' <summary>
    ''' retorna a % do gris
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcGRIS() As Decimal
        Get
            Return _pcGRIS
        End Get
        Set(ByVal value As Decimal)
            _pcGRIS = value
        End Set
    End Property

    Private _valorMinimoGris As Decimal
    ''' <summary>
    ''' retorna o valor minimo Gris
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorMinimoGris() As Decimal
        Get
            Return _valorMinimoGris
        End Get
        Set(ByVal value As Decimal)
            _valorMinimoGris = value
        End Set
    End Property

    Private _valorPedagio As Decimal
    ''' <summary>
    ''' retorna o valor do ped�gio
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorPedagio() As Decimal
        Get
            Return _valorPedagio
        End Get
        Set(ByVal value As Decimal)
            _valorPedagio = value
        End Set
    End Property

    Private _qtdPedagio As Decimal
    ''' <summary>
    ''' retorna a qtde KG de ped�gio
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property QtdPedagio() As Decimal
        Get
            Return _qtdPedagio
        End Get
        Set(ByVal value As Decimal)
            _qtdPedagio = value
        End Set
    End Property

    Private _valorTxDesembaraco As Decimal
    ''' <summary>
    ''' retorna o valor da tx de desembara�o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorTxDesembaraco() As Decimal
        Get
            Return _valorTxDesembaraco
        End Get
        Set(ByVal value As Decimal)
            _valorTxDesembaraco = value
        End Set
    End Property

    Private _pcFluvial As Decimal
    ''' <summary>
    ''' retorna a % ...
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcFluvial() As Decimal
        Get
            Return _pcFluvial
        End Get
        Set(ByVal value As Decimal)
            _pcFluvial = value
        End Set
    End Property

    Private _pcReentrega As Decimal
    ''' <summary>
    ''' retorna a % de reentrega
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcReentrega() As Decimal
        Get
            Return _pcReentrega
        End Get
        Set(ByVal value As Decimal)
            _pcReentrega = value
        End Set
    End Property

    Private _pcTDE As Decimal
    ''' <summary>
    ''' retorna a % de TDE
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcTDE() As Decimal
        Get
            Return _pcTDE
        End Get
        Set(ByVal value As Decimal)
            _pcTDE = value
        End Set
    End Property

    Private _valorMinimoTDE As Decimal
    ''' <summary>
    ''' retorna o valor minimo do TDE
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorMinimoTDE() As Decimal
        Get
            Return _valorMinimoTDE
        End Get
        Set(ByVal value As Decimal)
            _valorMinimoTDE = value
        End Set
    End Property

    Private _valorMaximoTDE As Decimal
    ''' <summary>
    ''' retorna o valor maximo do TDE
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorMaximoTDE() As Decimal
        Get
            Return _valorMaximoTDE
        End Get
        Set(ByVal value As Decimal)
            _valorMaximoTDE = value
        End Set
    End Property

    Private _valorPaletizacao As Decimal
    ''' <summary>
    ''' retorna o valor da paletiza��o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorPaletizacao() As Decimal
        Get
            Return _valorPaletizacao
        End Get
        Set(ByVal value As Decimal)
            _valorPaletizacao = value
        End Set
    End Property

    Private _icmsIncluso As String
    ''' <summary>
    ''' retorna se o icms esta incluso ou n�o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ICMSIncluso() As String
        Get
            Return _icmsIncluso
        End Get
        Set(ByVal value As String)
            _icmsIncluso = value
        End Set
    End Property

    Private _prazoPgto As Int32
    ''' <summary>
    ''' retorna o prazo de pgto
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PrazoPagamento() As Int32
        Get
            Return _prazoPgto
        End Get
        Set(ByVal value As Int32)
            _prazoPgto = value
        End Set
    End Property

    Private _tipoFaturamento As String
    ''' <summary>
    ''' retorna o tipo de faturamento
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TipoFaturamento() As String
        Get
            Return _tipoFaturamento
        End Get
        Set(ByVal value As String)
            _tipoFaturamento = value
        End Set
    End Property

    Private _semanaFora As String
    ''' <summary>
    ''' retorna se � semana fora
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SemanaFora() As String
        Get
            Return _semanaFora
        End Get
        Set(ByVal value As String)
            _semanaFora = value
        End Set
    End Property

    Private _codRedespacho As Int32
    ''' <summary>
    ''' retorna o cod da transportadora de redespacho
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodRedespacho() As Int32
        Get
            Return _codRedespacho
        End Get
        Set(ByVal value As Int32)
            _codRedespacho = value
        End Set
    End Property

    Private _situacao As Int32
    ''' <summary>
    ''' retorna a situacao do redespacho
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Situacao() As Int32
        Get
            Return _situacao
        End Get
        Set(ByVal value As Int32)
            _situacao = value
        End Set
    End Property

    Private _caracteristica As Int32
    ''' <summary>
    ''' retorna caracteristica
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Caracteristica() As Int32
        Get
            Return _caracteristica
        End Get
        Set(ByVal value As Int32)
            _caracteristica = value
        End Set
    End Property

    Private _pcADvalorEM As Decimal
    ''' <summary>
    ''' retorna o PCAD
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property PcADvalorEM() As Decimal
        Get
            Return _pcADvalorEM
        End Get
        Set(ByVal value As Decimal)
            _pcADvalorEM = value
        End Set
    End Property

    Private _valorAdEm As Decimal
    ''' <summary>
    ''' retorna o valor ADEm m�nimo
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorAdEm() As Decimal
        Get
            Return _valorAdEm
        End Get
        Set(ByVal value As Decimal)
            _valorAdEm = value
        End Set
    End Property

    Private _valorKg As Decimal
    ''' <summary>
    ''' retorna o valor do Kg
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorKg() As Decimal
        Get
            Return _valorKg
        End Get
        Set(ByVal value As Decimal)
            _valorKg = value
        End Set
    End Property

    Private _valorTonelada As Decimal
    ''' <summary>
    ''' retorna o valor da Tonelada
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ValorTonelada() As Decimal
        Get
            Return _valorTonelada
        End Get
        Set(ByVal value As Decimal)
            _valorTonelada = value
        End Set
    End Property



    ''' <summary>
    ''' retorna os tipos de transacao
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaTransacao() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_TRANSACAO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna as lojas ativas
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaLoja() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_LOJA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Insere, atualiza e exclui Prazos de Pgto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaPrazoPagamento() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_FAT", ParameterDirection.Input, _tipoFaturamento, OracleDbType.Varchar2, 1))
        _array.Add(New MaxxParametro("PM_PRAZO", ParameterDirection.Input, _prazoPgto, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_SEMANA", ParameterDirection.Input, _semanaFora, OracleDbType.Varchar2, 1))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_PRAZO_PGTO", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna os prazos de pgto
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaPrazoPagamento() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_PRAZO_PGTO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna as transportadoras
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaTransportadora() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_TRANSPORTADORA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna Contratos por Rota
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaContratoRota() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_CONTRATO_ROTA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava o contrato
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaContratoRota() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_REGIAOFRETE", ParameterDirection.Input, MyBase.CodRegiaoFrete, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_DTVIGENCIA", ParameterDirection.Input, _dataVigencia, OracleDbType.Date))
        _array.Add(New MaxxParametro("PM_TABELA", ParameterDirection.Input, _tipoTabela, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_VLFRETE", ParameterDirection.Input, _valorFrete, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_VLPEDAGIO", ParameterDirection.Input, _valorPedagio, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_CONTRATO_ROTA", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna os Contratos por Nota Fiscal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaContratoNota() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_CONTRATO_NOTA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava Contrato por Nota Fiscal
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaContratoNota() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_DTVIGENCIA", ParameterDirection.Input, _dataVigencia, OracleDbType.Date))
        _array.Add(New MaxxParametro("PM_TABELA", ParameterDirection.Input, _tipoTabela, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_VLNOTAINICIO", ParameterDirection.Input, _valorNotaInicio, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_VLNOTAFIM", ParameterDirection.Input, _valorNotaFim, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_VLFRETE", ParameterDirection.Input, _valorFrete, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_PCFRETE", ParameterDirection.Input, _pcFrete, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_CONTRATO_NOTA", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna os tipos de tabela
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaTipoTabela() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_TIPO_TABELA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna os Redespachos
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaRedespacho()

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_REDESPACHO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava os dados do redespacho
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaRedespacho() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_REDESPACHO", ParameterDirection.Input, _codRedespacho, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_SITUACAO", ParameterDirection.Input, _situacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_REDESPACHO", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna o frete distancia
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaFreteDistancia() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_FRETE_DISTANCIA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava o frete distancia
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaFreteDistancia() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_DTVIGENCIA", ParameterDirection.Input, _dataVigencia, OracleDbType.Date))
        _array.Add(New MaxxParametro("PM_TABELA", ParameterDirection.Input, _tipoTabela, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_QTD_INICIO_KM", ParameterDirection.Input, _kmInicio, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_QTD_FIM_KM", ParameterDirection.Input, _kmFim, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_QTD_INICIO_PESO", ParameterDirection.Input, _pesoInicio, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_QTD_FIM_PESO", ParameterDirection.Input, _pesoFim, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_VLFRETE", ParameterDirection.Input, _valorFrete, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCFRETENF", ParameterDirection.Input, _pcFrete, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_PCSEGURO", ParameterDirection.Input, _pcSeguro, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLCAT", ParameterDirection.Input, _valorCAT, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLTAS", ParameterDirection.Input, _valorTAS, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLITR", ParameterDirection.Input, _valorITR, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLCTRC", ParameterDirection.Input, _valorCTRC, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCGRIS", ParameterDirection.Input, _pcGRIS, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_GRISNF", ParameterDirection.Input, _valorMinimoGris, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLPEDAGIO", ParameterDirection.Input, _valorPedagio, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_QTDPEDAGIO", ParameterDirection.Input, _qtdPedagio, OracleDbType.Decimal, 15))
        _array.Add(New MaxxParametro("PM_VLTX_DESEMBARACO", ParameterDirection.Input, _valorTxDesembaraco, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCFLUVIAL", ParameterDirection.Input, _pcFluvial, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_PCREENTREGA", ParameterDirection.Input, _pcReentrega, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLPALETIZACAO", ParameterDirection.Input, _valorPaletizacao, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCTDE", ParameterDirection.Input, _pcTDE, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_TDE", ParameterDirection.Input, _valorMinimoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_MAX_TDE", ParameterDirection.Input, _valorMaximoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_FL_ICMS_INCLUSO", ParameterDirection.Input, _icmsIncluso, OracleDbType.Varchar2, 1))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_FRETE_DISTANCIA", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna o frete minimo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaFreteMinimo()

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_FRETE_MINIMO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava frete minimo
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaFreteMinimo() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_DTVIGENCIA", ParameterDirection.Input, _dataVigencia, OracleDbType.Date))
        _array.Add(New MaxxParametro("PM_TABELA", ParameterDirection.Input, _tipoTabela, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_UF", ParameterDirection.Input, MyBase.CodUf, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CIDADE_DESTINO", ParameterDirection.Input, MyBase.codCidadeDestino, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CARACTERISTICA", ParameterDirection.Input, _caracteristica, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_REGIAO_FRETE", ParameterDirection.Input, MyBase.CodRegiaoFrete, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_VLFRETE_MINIMO", ParameterDirection.Input, _valorFrete, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCADVALOREM", ParameterDirection.Input, _pcADvalorEM, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLCAT", ParameterDirection.Input, _valorCAT, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLTAS", ParameterDirection.Input, _valorTAS, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLITR", ParameterDirection.Input, _valorITR, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLCTRC", ParameterDirection.Input, _valorCTRC, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCGRIS", ParameterDirection.Input, _pcGRIS, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_GRISNF", ParameterDirection.Input, _valorMinimoGris, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLPEDAGIO", ParameterDirection.Input, _valorPedagio, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_QTDPEDAGIO", ParameterDirection.Input, _qtdPedagio, OracleDbType.Decimal, 15))
        _array.Add(New MaxxParametro("PM_VLTX_DESEMBARACO", ParameterDirection.Input, _valorTxDesembaraco, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCFLUVIAL", ParameterDirection.Input, _pcFluvial, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_PCREENTREGA", ParameterDirection.Input, _pcReentrega, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLPALETIZACAO", ParameterDirection.Input, _valorPaletizacao, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCTDE", ParameterDirection.Input, _pcTDE, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_TDE", ParameterDirection.Input, _valorMinimoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_MAX_TDE", ParameterDirection.Input, _valorMaximoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_FL_ICMS_INCLUSO", ParameterDirection.Input, _icmsIncluso, OracleDbType.Varchar2, 1))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_FRETE_MINIMO", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna Frete por Peso
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaFretePeso() As DataTable

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return _banco.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_FRETE_PESO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava o Frete por Peso
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaFretePeso() As OracleParameterCollection

        _banco = New MaxxDados
        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODLOJA", ParameterDirection.Input, _codLoja, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODTRANSP", ParameterDirection.Input, _codTransportadora, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TRANSACAO", ParameterDirection.Input, _tipoTransacao, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_DTVIGENCIA", ParameterDirection.Input, _dataVigencia, OracleDbType.Date))
        _array.Add(New MaxxParametro("PM_TABELA", ParameterDirection.Input, _tipoTabela, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_UF", ParameterDirection.Input, MyBase.CodUf, OracleDbType.Varchar2, 2))
        _array.Add(New MaxxParametro("PM_CIDADE_DESTINO", ParameterDirection.Input, MyBase.codCidadeDestino, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CARACTERISTICA", ParameterDirection.Input, _caracteristica, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_REGIAO_FRETE", ParameterDirection.Input, MyBase.CodRegiaoFrete, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_QTDINICIO_PESO", ParameterDirection.Input, _pesoInicio, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_QTDFIM_PESO", ParameterDirection.Input, _pesoFim, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLFRETE", ParameterDirection.Input, _valorFrete, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_KG", ParameterDirection.Input, _valorKg, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_TONELADA", ParameterDirection.Input, _valorTonelada, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_ADEM", ParameterDirection.Input, _valorAdEm, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCADVALOREM", ParameterDirection.Input, _pcADvalorEM, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_PCSEGURO", ParameterDirection.Input, _pcSeguro, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLCAT", ParameterDirection.Input, _valorCAT, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLTAS", ParameterDirection.Input, _valorTAS, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLITR", ParameterDirection.Input, _valorITR, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLCTRC", ParameterDirection.Input, _valorCTRC, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCGRIS", ParameterDirection.Input, _pcGRIS, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_GRISNF", ParameterDirection.Input, _valorMinimoGris, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VLPEDAGIO", ParameterDirection.Input, _valorPedagio, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_QTDPEDAGIO", ParameterDirection.Input, _qtdPedagio, OracleDbType.Decimal, 15))
        _array.Add(New MaxxParametro("PM_VLTX_DESEMBARACO", ParameterDirection.Input, _valorTxDesembaraco, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCFLUVIAL", ParameterDirection.Input, _pcFluvial, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_PCREENTREGA", ParameterDirection.Input, _pcReentrega, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VLPALETIZACAO", ParameterDirection.Input, _valorPaletizacao, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_PCTDE", ParameterDirection.Input, _pcTDE, OracleDbType.Decimal, 5))
        _array.Add(New MaxxParametro("PM_VL_MIN_TDE", ParameterDirection.Input, _valorMinimoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_VL_MAX_TDE", ParameterDirection.Input, _valorMaximoTDE, OracleDbType.Decimal, 14))
        _array.Add(New MaxxParametro("PM_FL_ICMS_INCLUSO", ParameterDirection.Input, _icmsIncluso, OracleDbType.Varchar2, 1))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return _banco.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_FRETE_PESO", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

End Class
