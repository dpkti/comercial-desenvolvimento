Imports System.Configuration
Imports System.Data
Imports Oracle.DataAccess.Client
Public Class MaxxDados

    Private _action As Int32
    ''' <summary>
    ''' retorna a a��o do form para todas as Classes
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Action() As Int32
        Get
            Return _action
        End Get
        Set(ByVal value As Int32)
            _action = value
        End Set
    End Property

    ''' <summary>
    ''' abre a conex�o com o banco
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function conexao() As OracleConnection

        Dim strConn As String = Me.obterStringConexao()
        Dim conn As OracleConnection = New OracleConnection(strConn)
        Try
            conn.Open()
        Catch ex As Exception
            Throw ex
        End Try

        Return conn

    End Function

    ''' <summary>
    ''' Executa comando sql retornando um DataReader
    ''' </summary>
    ''' <param name="pComandoSql"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function RetornaDataReader(ByVal pComandoSql As String) As OracleDataReader

        Dim conn As OracleConnection = Me.conexao()

        Try
            Dim cmd As OracleCommand
            Dim dr As OracleDataReader
            ' cria o command para excutar a instru��o SQL
            cmd = New OracleCommand(pComandoSql, conn)
            ' excuta o comando 
            dr = cmd.ExecuteReader()

            Return dr

        Catch ex As Exception
            Throw ex
            conn.Close()
        End Try

    End Function

    ''' <summary>
    ''' Executa comando sql retornando DataTable
    ''' </summary>
    ''' <param name="pComandoSql"></param>
    ''' <param name="pComandType"></param>
    ''' <param name="pParametros"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function RetornaDataTable(ByVal pComandoSql As String, ByVal pComandType As CommandType, Optional ByRef pParametros As ArrayList = Nothing) As DataTable

        Dim conn As OracleConnection = Me.conexao()
        Dim da As OracleDataAdapter
        Dim dt As New DataTable

        ' cria o dataAdpter para a procedure
        da = New OracleDataAdapter(pComandoSql, conn)
        da.SelectCommand.CommandType = pComandType

        Try
            ' function que monta os paremtros e add no dataAdpter
            If Not pParametros Is Nothing Then
                Call MontaParametro(da, pParametros)
            End If

            ' preenche o DataTable
            da.Fill(dt)
            Return dt

            ' verifica se existe parametro p/ ent�o popular com os valores
            If Not pParametros Is Nothing Then
                If pParametros.Count > 0 Then
                    For i As Integer = 0 To pParametros.Count - 1
                        pParametros.Item(i) = da.SelectCommand.Parameters(i).Value
                    Next
                End If
            End If

        Catch ex As Exception
            Throw ex
        Finally
            da.Dispose()
            conn.Close()
            conn = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Executa comando sql retornando cole��o de par�metros
    ''' </summary>
    ''' <param name="pComandoSql"></param>
    ''' <param name="pParametros"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function ExecutaComando(ByVal pComandoSql As String, ByRef pParametros As ArrayList) As OracleParameterCollection

        Dim conn As OracleConnection = Me.conexao()
        Dim cmd As OracleCommand

        cmd = New OracleCommand(pComandoSql, conn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            Call MontaParametroCmd(cmd, pParametros)

            ' TRACE PARA DEBUG
            'Dim inty As Integer
            'Dim obj As Object
            'For inty = 0 To cmd.Parameters.Count - 1
            '    Trace.Write(cmd.Parameters.Item(inty).ParameterName() & " | ")
            '    Trace.Write(cmd.Parameters.Item(inty).DbType)
            '    Trace.Write(" | ")
            '    Trace.Write(cmd.Parameters.Item(inty).Value() & vbNewLine)
            'Next

            cmd.ExecuteNonQuery()
            Return cmd.Parameters

        Catch ex As Exception
            Throw ex
        Finally
            cmd.Dispose()
            conn.Close()
            conn = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Monta os parametros para a procedure e add no dataAdpter
    ''' </summary>
    ''' <param name="da"></param>
    ''' <param name="pParametros"></param>
    ''' <remarks></remarks>
    Private Sub MontaParametro(ByRef da As OracleDataAdapter, ByVal pParametros As ArrayList)

        Dim param As MaxxParametro

        For Each param In pParametros

            Dim vSqlParam As New OracleParameter

            vSqlParam.ParameterName = param.Nome
            vSqlParam.Value = param.Valor
            vSqlParam.OracleDbType = param.DbType
            vSqlParam.Direction = param.Direcao
            vSqlParam.Size = param.Size

            da.SelectCommand.Parameters.Add(vSqlParam)

        Next

    End Sub

    ''' <summary>
    ''' Monta os parametros para a procedure e add no Comamnd 
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="pParametros"></param>
    ''' <remarks></remarks>
    Private Sub MontaParametroCmd(ByRef cmd As OracleCommand, ByVal pParametros As ArrayList)

        Dim param As MaxxParametro

        For Each param In pParametros

            Dim vSqlParam As New OracleParameter

            vSqlParam.ParameterName = param.Nome
            vSqlParam.Value = param.Valor
            vSqlParam.OracleDbType = param.DbType
            vSqlParam.Direction = param.Direcao
            vSqlParam.Size = param.Size

            cmd.Parameters.Add(vSqlParam)

        Next

    End Sub

    ''' <summary>
    ''' retorna string de conex�o do app.comfig
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function obterStringConexao() As String
        Return ConfigurationManager.ConnectionStrings("DESENV").ConnectionString
    End Function
End Class
