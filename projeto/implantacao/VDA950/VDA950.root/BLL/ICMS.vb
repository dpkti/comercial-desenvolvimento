Imports Oracle.DataAccess.Client
Public Class ICMS
    ' herda da classe base MaxxDados
    Inherits MaxxDados
    Private _Array As ArrayList

    Private _codUfOrigem As String
    ''' <summary>
    ''' retorna o cod uf origem
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodUfOrigem() As String
        Get
            Return _codUfOrigem
        End Get
        Set(ByVal value As String)
            _codUfOrigem = value
        End Set
    End Property

    Private _codUfDestino As String
    ''' <summary>
    ''' retorna o cod uf destino
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodUfDestino() As String
        Get
            Return _codUfDestino
        End Get
        Set(ByVal value As String)
            _codUfDestino = value
        End Set
    End Property

    Private _ICMS As Decimal
    ''' <summary>
    ''' retorna ICMS
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ICMS() As Decimal
        Get
            Return _ICMS
        End Get
        Set(ByVal value As Decimal)
            _ICMS = value
        End Set
    End Property

    ''' <summary>
    ''' retorna o ICMS
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaICMS() As DataTable

        _Array = New ArrayList

        _Array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return MyBase.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_ICMS", CommandType.StoredProcedure, _Array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava o ICMS no banco
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaICMS() As OracleParameterCollection

        _Array = New ArrayList

        _Array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _Array.Add(New MaxxParametro("PM_UF_ORIGEM", ParameterDirection.Input, _codUfOrigem, OracleDbType.Varchar2, 2))
        _Array.Add(New MaxxParametro("PM_UF_DESTINO", ParameterDirection.Input, _codUfDestino, OracleDbType.Varchar2, 2))
        _Array.Add(New MaxxParametro("PM_ICMS", ParameterDirection.Input, _ICMS, OracleDbType.Decimal, 5))
        _Array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _Array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return MyBase.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_ICMS", _Array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function
End Class
