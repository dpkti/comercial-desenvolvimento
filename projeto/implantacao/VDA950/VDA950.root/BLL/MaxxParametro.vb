Imports Oracle.DataAccess.Client
Public Class MaxxParametro
    Public Nome As String
    Public Valor As Object
    Public Direcao As ParameterDirection
    Public Size As Int32
    Public DbType As OracleDbType

    ''' <summary>
    ''' Monta a estrutura dos Parametros
    ''' </summary>
    ''' <param name="pnomeParam"></param>
    ''' <param name="pDirecao"></param>
    ''' <param name="pValor"></param>
    ''' <param name="pDbType"></param>
    ''' <param name="pSize"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal pnomeParam As String, ByVal pDirecao As ParameterDirection, ByVal pValor As Object, Optional ByVal pDbType As OracleDbType = Nothing, Optional ByVal pSize As Int32 = 0)
        Nome = pnomeParam
        Direcao = pDirecao
        Valor = pValor
        DbType = pDbType
        Size = pSize
    End Sub

End Class
