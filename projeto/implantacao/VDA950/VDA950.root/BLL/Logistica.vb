Imports Oracle.DataAccess.Client
Public Class Logistica
    ' HERDA A CLASSE DE DADOS
    Inherits MaxxDados
    Private _array As ArrayList

    Private _codCidadeOrigem As Int32
    ''' <summary>
    ''' retorna codCidade de origem do frete
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property codCidadeOrigem() As Int32
        Get
            Return _codCidadeOrigem
        End Get
        Set(ByVal value As Int32)
            _codCidadeOrigem = value
        End Set
    End Property

    Private _codCidadeDestino As Int32
    ''' <summary>
    ''' retorna codCidade de destino do frete
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property codCidadeDestino() As Int32
        Get
            Return _codCidadeDestino
        End Get
        Set(ByVal value As Int32)
            _codCidadeDestino = value
        End Set
    End Property

    Private _codUf As String
    ''' <summary>
    ''' retorna o c�digo da UF
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodUf() As String
        Get
            Return _codUf
        End Get
        Set(ByVal value As String)
            _codUf = value
        End Set
    End Property

    Private _qtdKm As Decimal
    ''' <summary>
    ''' retorna km do frete
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property qtdKm() As Decimal
        Get
            Return _qtdKm
        End Get
        Set(ByVal value As Decimal)
            _qtdKm = value
        End Set
    End Property

    Private _codRegiaoFrete As Int32
    ''' <summary>
    ''' retorna o c�digo da regi�o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property CodRegiaoFrete() As Int32
        Get
            Return _codRegiaoFrete
        End Get
        Set(ByVal value As Int32)
            _codRegiaoFrete = value
        End Set
    End Property

    Private _descRegiao As String
    ''' <summary>
    ''' retorna o a descri��o da regi�o
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property DescRegiao() As String
        Get
            Return _descRegiao
        End Get
        Set(ByVal value As String)
            _descRegiao = value
        End Set
    End Property


    ''' <summary>
    ''' retorna as cidades
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaCidade() As DataTable

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))
        _array.Add(New MaxxParametro("PM_UF", ParameterDirection.Input, _codUf, OracleDbType.Varchar2, 2))

        Try
            Return MyBase.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_CIDADE", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna os estados
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaUF() As DataTable

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return MyBase.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_UF", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna a distancia do frete
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaDistancia() As DataTable

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return MyBase.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_DISTANCIA", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' Grava os dados da dist�ncia do frete
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaDistanciaFrete() As OracleParameterCollection

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CID_ORIGEM", ParameterDirection.Input, _codCidadeOrigem, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CID_DESTINO", ParameterDirection.Input, _codCidadeDestino, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_KM", ParameterDirection.Input, _qtdKm, OracleDbType.Decimal))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return MyBase.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_DISTANCIA", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' retorna as regi�es do frete
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelecionaRegiaoFrete() As DataTable

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_CURSOR", ParameterDirection.Output, DBNull.Value, OracleDbType.RefCursor))

        Try
            Return MyBase.RetornaDataTable("PRODUCAO.PCK_VDA950.PR_CON_REGIAO", CommandType.StoredProcedure, _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function

    ''' <summary>
    ''' grava regi�o do frete
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GravaRegiaoFrete() As OracleParameterCollection

        _array = New ArrayList

        _array.Add(New MaxxParametro("PM_ACTION", ParameterDirection.Input, MyBase.Action, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_CODREGIAO", ParameterDirection.Input, _codRegiaoFrete, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_REGIAO", ParameterDirection.Input, _descRegiao, OracleDbType.Varchar2, 25))
        _array.Add(New MaxxParametro("PM_CODERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Int32))
        _array.Add(New MaxxParametro("PM_TXTERRO", ParameterDirection.Output, DBNull.Value, OracleDbType.Varchar2))

        Try
            Return MyBase.ExecutaComando("PRODUCAO.PCK_VDA950.PR_ALT_INC_DEL_REGIAO", _array)
        Catch ex As Exception
            Throw New Exception("BBL: " & ex.Message)
        End Try

    End Function
End Class
