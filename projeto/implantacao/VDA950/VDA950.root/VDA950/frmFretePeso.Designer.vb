<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFretePeso
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFretePeso))
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtPcSeguro = New System.Windows.Forms.MaskedTextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtValorAd = New System.Windows.Forms.MaskedTextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtValorTonelada = New System.Windows.Forms.MaskedTextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtValorKg = New System.Windows.Forms.MaskedTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPesoFim = New System.Windows.Forms.MaskedTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPesoInicio = New System.Windows.Forms.MaskedTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboTipoTabela = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar
        Me.btnExcluir = New System.Windows.Forms.Button
        Me.btnSalvar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.GroupICMSincluso = New System.Windows.Forms.GroupBox
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.txtDtVigencia = New System.Windows.Forms.MaskedTextBox
        Me.txtVlMaxTDE = New System.Windows.Forms.MaskedTextBox
        Me.txtVlMinTDE = New System.Windows.Forms.MaskedTextBox
        Me.txtPcTDE = New System.Windows.Forms.MaskedTextBox
        Me.txtVlPaletizacao = New System.Windows.Forms.MaskedTextBox
        Me.txtPcReentrega = New System.Windows.Forms.MaskedTextBox
        Me.txtPcFluvial = New System.Windows.Forms.MaskedTextBox
        Me.txtVlTxDesembaraco = New System.Windows.Forms.MaskedTextBox
        Me.txtQtdPedagio = New System.Windows.Forms.MaskedTextBox
        Me.txtVlPedagio = New System.Windows.Forms.MaskedTextBox
        Me.txtVlGRIS = New System.Windows.Forms.MaskedTextBox
        Me.txtVlITR = New System.Windows.Forms.MaskedTextBox
        Me.txtVlCTRC = New System.Windows.Forms.MaskedTextBox
        Me.txtPcGRIS = New System.Windows.Forms.MaskedTextBox
        Me.txtVlTAS = New System.Windows.Forms.MaskedTextBox
        Me.txtVlCAT = New System.Windows.Forms.MaskedTextBox
        Me.txtPcAD = New System.Windows.Forms.MaskedTextBox
        Me.txtVlFrete = New System.Windows.Forms.MaskedTextBox
        Me.cboUF = New System.Windows.Forms.ComboBox
        Me.cboCaracteristica = New System.Windows.Forms.ComboBox
        Me.lblCaracteristica = New System.Windows.Forms.Label
        Me.cboTransacao = New System.Windows.Forms.ComboBox
        Me.lblCodRegiaoFrete = New System.Windows.Forms.Label
        Me.cboCidadeDestino = New System.Windows.Forms.ComboBox
        Me.cboRegiaoFrete = New System.Windows.Forms.ComboBox
        Me.cboTransportadora = New System.Windows.Forms.ComboBox
        Me.lblCodCidadeDestino = New System.Windows.Forms.Label
        Me.cboLoja = New System.Windows.Forms.ComboBox
        Me.lblCodLoja = New System.Windows.Forms.Label
        Me.lblCodUf = New System.Windows.Forms.Label
        Me.lblCodTransp = New System.Windows.Forms.Label
        Me.lblTpTransacao = New System.Windows.Forms.Label
        Me.lblDtVigencia = New System.Windows.Forms.Label
        Me.lblVlFreteMinimo = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.lblPcAdValorem = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.lblVlCat = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.lblVlTas = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblVlItr = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.lblVlCtrc = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblPcGris = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.dgvFretePeso = New System.Windows.Forms.DataGridView
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupICMSincluso.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvFretePeso, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(792, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 25)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(792, 526)
        Me.TabControl1.TabIndex = 16
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(784, 500)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cadastro"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPcSeguro)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtValorAd)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtValorTonelada)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtValorKg)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtPesoFim)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPesoInicio)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboTipoTabela)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.MonthCalendar1)
        Me.GroupBox1.Controls.Add(Me.btnExcluir)
        Me.GroupBox1.Controls.Add(Me.btnSalvar)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.GroupICMSincluso)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.txtDtVigencia)
        Me.GroupBox1.Controls.Add(Me.txtVlMaxTDE)
        Me.GroupBox1.Controls.Add(Me.txtVlMinTDE)
        Me.GroupBox1.Controls.Add(Me.txtPcTDE)
        Me.GroupBox1.Controls.Add(Me.txtVlPaletizacao)
        Me.GroupBox1.Controls.Add(Me.txtPcReentrega)
        Me.GroupBox1.Controls.Add(Me.txtPcFluvial)
        Me.GroupBox1.Controls.Add(Me.txtVlTxDesembaraco)
        Me.GroupBox1.Controls.Add(Me.txtQtdPedagio)
        Me.GroupBox1.Controls.Add(Me.txtVlPedagio)
        Me.GroupBox1.Controls.Add(Me.txtVlGRIS)
        Me.GroupBox1.Controls.Add(Me.txtVlITR)
        Me.GroupBox1.Controls.Add(Me.txtVlCTRC)
        Me.GroupBox1.Controls.Add(Me.txtPcGRIS)
        Me.GroupBox1.Controls.Add(Me.txtVlTAS)
        Me.GroupBox1.Controls.Add(Me.txtVlCAT)
        Me.GroupBox1.Controls.Add(Me.txtPcAD)
        Me.GroupBox1.Controls.Add(Me.txtVlFrete)
        Me.GroupBox1.Controls.Add(Me.cboUF)
        Me.GroupBox1.Controls.Add(Me.cboCaracteristica)
        Me.GroupBox1.Controls.Add(Me.lblCaracteristica)
        Me.GroupBox1.Controls.Add(Me.cboTransacao)
        Me.GroupBox1.Controls.Add(Me.lblCodRegiaoFrete)
        Me.GroupBox1.Controls.Add(Me.cboCidadeDestino)
        Me.GroupBox1.Controls.Add(Me.cboRegiaoFrete)
        Me.GroupBox1.Controls.Add(Me.cboTransportadora)
        Me.GroupBox1.Controls.Add(Me.lblCodCidadeDestino)
        Me.GroupBox1.Controls.Add(Me.cboLoja)
        Me.GroupBox1.Controls.Add(Me.lblCodLoja)
        Me.GroupBox1.Controls.Add(Me.lblCodUf)
        Me.GroupBox1.Controls.Add(Me.lblCodTransp)
        Me.GroupBox1.Controls.Add(Me.lblTpTransacao)
        Me.GroupBox1.Controls.Add(Me.lblDtVigencia)
        Me.GroupBox1.Controls.Add(Me.lblVlFreteMinimo)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.lblPcAdValorem)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.lblVlCat)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.lblVlTas)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.lblVlItr)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.lblVlCtrc)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.lblPcGris)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(770, 464)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Frete por Peso"
        '
        'txtPcSeguro
        '
        Me.txtPcSeguro.Location = New System.Drawing.Point(6, 388)
        Me.txtPcSeguro.Mask = "00000"
        Me.txtPcSeguro.Name = "txtPcSeguro"
        Me.txtPcSeguro.Size = New System.Drawing.Size(100, 20)
        Me.txtPcSeguro.TabIndex = 30
        Me.txtPcSeguro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPcSeguro.ValidatingType = GetType(Integer)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 368)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 13)
        Me.Label17.TabIndex = 55
        Me.Label17.Text = "Pc Seguro"
        '
        'txtValorAd
        '
        Me.txtValorAd.Location = New System.Drawing.Point(6, 229)
        Me.txtValorAd.Mask = "00000000000000"
        Me.txtValorAd.Name = "txtValorAd"
        Me.txtValorAd.Size = New System.Drawing.Size(100, 20)
        Me.txtValorAd.TabIndex = 15
        Me.txtValorAd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 210)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 13)
        Me.Label6.TabIndex = 53
        Me.Label6.Text = "Valor AD EM"
        '
        'txtValorTonelada
        '
        Me.txtValorTonelada.Location = New System.Drawing.Point(400, 171)
        Me.txtValorTonelada.Mask = "00000000000000"
        Me.txtValorTonelada.Name = "txtValorTonelada"
        Me.txtValorTonelada.Size = New System.Drawing.Size(100, 20)
        Me.txtValorTonelada.TabIndex = 13
        Me.txtValorTonelada.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(397, 155)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 13)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Valor Tonelada"
        '
        'txtValorKg
        '
        Me.txtValorKg.Location = New System.Drawing.Point(266, 172)
        Me.txtValorKg.Mask = "00000000000000"
        Me.txtValorKg.Name = "txtValorKg"
        Me.txtValorKg.Size = New System.Drawing.Size(100, 20)
        Me.txtValorKg.TabIndex = 12
        Me.txtValorKg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(266, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 49
        Me.Label4.Text = "Valor Kg"
        '
        'txtPesoFim
        '
        Me.txtPesoFim.Location = New System.Drawing.Point(133, 171)
        Me.txtPesoFim.Mask = "00000"
        Me.txtPesoFim.Name = "txtPesoFim"
        Me.txtPesoFim.Size = New System.Drawing.Size(100, 20)
        Me.txtPesoFim.TabIndex = 11
        Me.txtPesoFim.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoFim.ValidatingType = GetType(Integer)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(130, 155)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Qtd Peso Fim"
        '
        'txtPesoInicio
        '
        Me.txtPesoInicio.Location = New System.Drawing.Point(6, 171)
        Me.txtPesoInicio.Mask = "00000"
        Me.txtPesoInicio.Name = "txtPesoInicio"
        Me.txtPesoInicio.Size = New System.Drawing.Size(100, 20)
        Me.txtPesoInicio.TabIndex = 10
        Me.txtPesoInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPesoInicio.ValidatingType = GetType(Integer)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 155)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 45
        Me.Label2.Text = "Qtd Peso Inicio"
        '
        'cboTipoTabela
        '
        Me.cboTipoTabela.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoTabela.FormattingEnabled = True
        Me.cboTipoTabela.Location = New System.Drawing.Point(474, 85)
        Me.cboTipoTabela.Name = "cboTipoTabela"
        Me.cboTipoTabela.Size = New System.Drawing.Size(200, 21)
        Me.cboTipoTabela.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(474, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(92, 13)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Tipo de Cobran�a"
        '
        'MonthCalendar1
        '
        Me.MonthCalendar1.Location = New System.Drawing.Point(594, 132)
        Me.MonthCalendar1.Name = "MonthCalendar1"
        Me.MonthCalendar1.TabIndex = 43
        Me.MonthCalendar1.Visible = False
        '
        'btnExcluir
        '
        Me.btnExcluir.BackColor = System.Drawing.Color.Red
        Me.btnExcluir.ForeColor = System.Drawing.Color.White
        Me.btnExcluir.Location = New System.Drawing.Point(175, 432)
        Me.btnExcluir.Name = "btnExcluir"
        Me.btnExcluir.Size = New System.Drawing.Size(75, 23)
        Me.btnExcluir.TabIndex = 36
        Me.btnExcluir.Text = "Excluir"
        Me.btnExcluir.UseVisualStyleBackColor = False
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(94, 432)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 34
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(12, 432)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 35
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GroupICMSincluso
        '
        Me.GroupICMSincluso.Controls.Add(Me.RadioButton2)
        Me.GroupICMSincluso.Controls.Add(Me.RadioButton1)
        Me.GroupICMSincluso.Location = New System.Drawing.Point(393, 369)
        Me.GroupICMSincluso.Name = "GroupICMSincluso"
        Me.GroupICMSincluso.Size = New System.Drawing.Size(121, 49)
        Me.GroupICMSincluso.TabIndex = 50
        Me.GroupICMSincluso.TabStop = False
        Me.GroupICMSincluso.Text = "ICMS Incluso ?"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(57, 19)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(48, 17)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.Text = "NAO"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(7, 20)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(44, 17)
        Me.RadioButton1.TabIndex = 33
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "SIM"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(468, 132)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox1.TabIndex = 38
        Me.PictureBox1.TabStop = False
        '
        'txtDtVigencia
        '
        Me.txtDtVigencia.Location = New System.Drawing.Point(490, 128)
        Me.txtDtVigencia.Mask = "00/00/0000"
        Me.txtDtVigencia.Name = "txtDtVigencia"
        Me.txtDtVigencia.Size = New System.Drawing.Size(100, 20)
        Me.txtDtVigencia.TabIndex = 9
        Me.txtDtVigencia.ValidatingType = GetType(Date)
        '
        'txtVlMaxTDE
        '
        Me.txtVlMaxTDE.Location = New System.Drawing.Point(266, 388)
        Me.txtVlMaxTDE.Mask = "00000000000000"
        Me.txtVlMaxTDE.Name = "txtVlMaxTDE"
        Me.txtVlMaxTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtVlMaxTDE.TabIndex = 32
        Me.txtVlMaxTDE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlMinTDE
        '
        Me.txtVlMinTDE.Location = New System.Drawing.Point(133, 388)
        Me.txtVlMinTDE.Mask = "00000000000000"
        Me.txtVlMinTDE.Name = "txtVlMinTDE"
        Me.txtVlMinTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtVlMinTDE.TabIndex = 31
        Me.txtVlMinTDE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPcTDE
        '
        Me.txtPcTDE.Location = New System.Drawing.Point(527, 332)
        Me.txtPcTDE.Mask = "00000"
        Me.txtPcTDE.Name = "txtPcTDE"
        Me.txtPcTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtPcTDE.TabIndex = 29
        Me.txtPcTDE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPcTDE.ValidatingType = GetType(Integer)
        '
        'txtVlPaletizacao
        '
        Me.txtVlPaletizacao.Location = New System.Drawing.Point(400, 332)
        Me.txtVlPaletizacao.Mask = "00000000000000"
        Me.txtVlPaletizacao.Name = "txtVlPaletizacao"
        Me.txtVlPaletizacao.Size = New System.Drawing.Size(100, 20)
        Me.txtVlPaletizacao.TabIndex = 28
        Me.txtVlPaletizacao.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPcReentrega
        '
        Me.txtPcReentrega.Location = New System.Drawing.Point(266, 332)
        Me.txtPcReentrega.Mask = "00000"
        Me.txtPcReentrega.Name = "txtPcReentrega"
        Me.txtPcReentrega.Size = New System.Drawing.Size(100, 20)
        Me.txtPcReentrega.TabIndex = 27
        Me.txtPcReentrega.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPcReentrega.ValidatingType = GetType(Integer)
        '
        'txtPcFluvial
        '
        Me.txtPcFluvial.Location = New System.Drawing.Point(133, 332)
        Me.txtPcFluvial.Mask = "00000"
        Me.txtPcFluvial.Name = "txtPcFluvial"
        Me.txtPcFluvial.Size = New System.Drawing.Size(100, 20)
        Me.txtPcFluvial.TabIndex = 26
        Me.txtPcFluvial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlTxDesembaraco
        '
        Me.txtVlTxDesembaraco.Location = New System.Drawing.Point(6, 332)
        Me.txtVlTxDesembaraco.Mask = "00000000000000"
        Me.txtVlTxDesembaraco.Name = "txtVlTxDesembaraco"
        Me.txtVlTxDesembaraco.Size = New System.Drawing.Size(100, 20)
        Me.txtVlTxDesembaraco.TabIndex = 25
        Me.txtVlTxDesembaraco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtdPedagio
        '
        Me.txtQtdPedagio.Location = New System.Drawing.Point(527, 284)
        Me.txtQtdPedagio.Mask = "00000000000000"
        Me.txtQtdPedagio.Name = "txtQtdPedagio"
        Me.txtQtdPedagio.Size = New System.Drawing.Size(100, 20)
        Me.txtQtdPedagio.TabIndex = 24
        Me.txtQtdPedagio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlPedagio
        '
        Me.txtVlPedagio.Location = New System.Drawing.Point(400, 284)
        Me.txtVlPedagio.Mask = "00000000000000"
        Me.txtVlPedagio.Name = "txtVlPedagio"
        Me.txtVlPedagio.Size = New System.Drawing.Size(100, 20)
        Me.txtVlPedagio.TabIndex = 23
        Me.txtVlPedagio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlGRIS
        '
        Me.txtVlGRIS.Location = New System.Drawing.Point(266, 284)
        Me.txtVlGRIS.Mask = "00000000000000"
        Me.txtVlGRIS.Name = "txtVlGRIS"
        Me.txtVlGRIS.Size = New System.Drawing.Size(100, 20)
        Me.txtVlGRIS.TabIndex = 22
        Me.txtVlGRIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlITR
        '
        Me.txtVlITR.Location = New System.Drawing.Point(527, 229)
        Me.txtVlITR.Mask = "00000"
        Me.txtVlITR.Name = "txtVlITR"
        Me.txtVlITR.Size = New System.Drawing.Size(100, 20)
        Me.txtVlITR.TabIndex = 19
        Me.txtVlITR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtVlITR.ValidatingType = GetType(Integer)
        '
        'txtVlCTRC
        '
        Me.txtVlCTRC.Location = New System.Drawing.Point(6, 284)
        Me.txtVlCTRC.Mask = "00000000000000"
        Me.txtVlCTRC.Name = "txtVlCTRC"
        Me.txtVlCTRC.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtVlCTRC.Size = New System.Drawing.Size(100, 20)
        Me.txtVlCTRC.TabIndex = 20
        Me.txtVlCTRC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPcGRIS
        '
        Me.txtPcGRIS.Location = New System.Drawing.Point(133, 284)
        Me.txtPcGRIS.Mask = "00000000000000"
        Me.txtPcGRIS.Name = "txtPcGRIS"
        Me.txtPcGRIS.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtPcGRIS.Size = New System.Drawing.Size(100, 20)
        Me.txtPcGRIS.TabIndex = 21
        Me.txtPcGRIS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlTAS
        '
        Me.txtVlTAS.Location = New System.Drawing.Point(400, 229)
        Me.txtVlTAS.Mask = "00000000000000"
        Me.txtVlTAS.Name = "txtVlTAS"
        Me.txtVlTAS.Size = New System.Drawing.Size(100, 20)
        Me.txtVlTAS.TabIndex = 18
        Me.txtVlTAS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtVlCAT
        '
        Me.txtVlCAT.Location = New System.Drawing.Point(266, 229)
        Me.txtVlCAT.Mask = "00000000000000"
        Me.txtVlCAT.Name = "txtVlCAT"
        Me.txtVlCAT.Size = New System.Drawing.Size(100, 20)
        Me.txtVlCAT.TabIndex = 17
        Me.txtVlCAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPcAD
        '
        Me.txtPcAD.Location = New System.Drawing.Point(133, 229)
        Me.txtPcAD.Mask = "00000"
        Me.txtPcAD.Name = "txtPcAD"
        Me.txtPcAD.Size = New System.Drawing.Size(100, 20)
        Me.txtPcAD.TabIndex = 16
        Me.txtPcAD.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPcAD.ValidatingType = GetType(Integer)
        '
        'txtVlFrete
        '
        Me.txtVlFrete.Location = New System.Drawing.Point(527, 171)
        Me.txtVlFrete.Mask = "00000000000000"
        Me.txtVlFrete.Name = "txtVlFrete"
        Me.txtVlFrete.Size = New System.Drawing.Size(100, 20)
        Me.txtVlFrete.SkipLiterals = False
        Me.txtVlFrete.TabIndex = 14
        Me.txtVlFrete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboUF
        '
        Me.cboUF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUF.FormattingEnabled = True
        Me.cboUF.Location = New System.Drawing.Point(245, 85)
        Me.cboUF.Name = "cboUF"
        Me.cboUF.Size = New System.Drawing.Size(200, 21)
        Me.cboUF.TabIndex = 5
        '
        'cboCaracteristica
        '
        Me.cboCaracteristica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCaracteristica.FormattingEnabled = True
        Me.cboCaracteristica.Location = New System.Drawing.Point(245, 127)
        Me.cboCaracteristica.Name = "cboCaracteristica"
        Me.cboCaracteristica.Size = New System.Drawing.Size(200, 21)
        Me.cboCaracteristica.TabIndex = 8
        '
        'lblCaracteristica
        '
        Me.lblCaracteristica.AutoSize = True
        Me.lblCaracteristica.Location = New System.Drawing.Point(242, 111)
        Me.lblCaracteristica.Name = "lblCaracteristica"
        Me.lblCaracteristica.Size = New System.Drawing.Size(74, 13)
        Me.lblCaracteristica.TabIndex = 9
        Me.lblCaracteristica.Text = "Caracteristica "
        '
        'cboTransacao
        '
        Me.cboTransacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransacao.FormattingEnabled = True
        Me.cboTransacao.Location = New System.Drawing.Point(474, 39)
        Me.cboTransacao.Name = "cboTransacao"
        Me.cboTransacao.Size = New System.Drawing.Size(200, 21)
        Me.cboTransacao.TabIndex = 3
        '
        'lblCodRegiaoFrete
        '
        Me.lblCodRegiaoFrete.AutoSize = True
        Me.lblCodRegiaoFrete.Location = New System.Drawing.Point(6, 69)
        Me.lblCodRegiaoFrete.Name = "lblCodRegiaoFrete"
        Me.lblCodRegiaoFrete.Size = New System.Drawing.Size(68, 13)
        Me.lblCodRegiaoFrete.TabIndex = 10
        Me.lblCodRegiaoFrete.Text = "Regi�o Frete"
        '
        'cboCidadeDestino
        '
        Me.cboCidadeDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCidadeDestino.FormattingEnabled = True
        Me.cboCidadeDestino.Location = New System.Drawing.Point(6, 127)
        Me.cboCidadeDestino.Name = "cboCidadeDestino"
        Me.cboCidadeDestino.Size = New System.Drawing.Size(200, 21)
        Me.cboCidadeDestino.TabIndex = 7
        '
        'cboRegiaoFrete
        '
        Me.cboRegiaoFrete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRegiaoFrete.FormattingEnabled = True
        Me.cboRegiaoFrete.Location = New System.Drawing.Point(6, 85)
        Me.cboRegiaoFrete.Name = "cboRegiaoFrete"
        Me.cboRegiaoFrete.Size = New System.Drawing.Size(200, 21)
        Me.cboRegiaoFrete.TabIndex = 4
        '
        'cboTransportadora
        '
        Me.cboTransportadora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransportadora.FormattingEnabled = True
        Me.cboTransportadora.Location = New System.Drawing.Point(245, 39)
        Me.cboTransportadora.Name = "cboTransportadora"
        Me.cboTransportadora.Size = New System.Drawing.Size(200, 21)
        Me.cboTransportadora.TabIndex = 2
        '
        'lblCodCidadeDestino
        '
        Me.lblCodCidadeDestino.AutoSize = True
        Me.lblCodCidadeDestino.Location = New System.Drawing.Point(6, 111)
        Me.lblCodCidadeDestino.Name = "lblCodCidadeDestino"
        Me.lblCodCidadeDestino.Size = New System.Drawing.Size(79, 13)
        Me.lblCodCidadeDestino.TabIndex = 8
        Me.lblCodCidadeDestino.Text = "Cidade Destino"
        '
        'cboLoja
        '
        Me.cboLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoja.FormattingEnabled = True
        Me.cboLoja.Location = New System.Drawing.Point(6, 39)
        Me.cboLoja.Name = "cboLoja"
        Me.cboLoja.Size = New System.Drawing.Size(200, 21)
        Me.cboLoja.TabIndex = 1
        '
        'lblCodLoja
        '
        Me.lblCodLoja.AutoSize = True
        Me.lblCodLoja.Location = New System.Drawing.Point(6, 23)
        Me.lblCodLoja.Name = "lblCodLoja"
        Me.lblCodLoja.Size = New System.Drawing.Size(27, 13)
        Me.lblCodLoja.TabIndex = 0
        Me.lblCodLoja.Text = "Loja"
        '
        'lblCodUf
        '
        Me.lblCodUf.AutoSize = True
        Me.lblCodUf.Location = New System.Drawing.Point(242, 69)
        Me.lblCodUf.Name = "lblCodUf"
        Me.lblCodUf.Size = New System.Drawing.Size(21, 13)
        Me.lblCodUf.TabIndex = 7
        Me.lblCodUf.Text = "UF"
        '
        'lblCodTransp
        '
        Me.lblCodTransp.AutoSize = True
        Me.lblCodTransp.Location = New System.Drawing.Point(242, 23)
        Me.lblCodTransp.Name = "lblCodTransp"
        Me.lblCodTransp.Size = New System.Drawing.Size(82, 13)
        Me.lblCodTransp.TabIndex = 3
        Me.lblCodTransp.Text = "Transportadora "
        '
        'lblTpTransacao
        '
        Me.lblTpTransacao.AutoSize = True
        Me.lblTpTransacao.Location = New System.Drawing.Point(471, 23)
        Me.lblTpTransacao.Name = "lblTpTransacao"
        Me.lblTpTransacao.Size = New System.Drawing.Size(61, 13)
        Me.lblTpTransacao.TabIndex = 5
        Me.lblTpTransacao.Text = "Transa��o "
        '
        'lblDtVigencia
        '
        Me.lblDtVigencia.AutoSize = True
        Me.lblDtVigencia.Location = New System.Drawing.Point(471, 111)
        Me.lblDtVigencia.Name = "lblDtVigencia"
        Me.lblDtVigencia.Size = New System.Drawing.Size(74, 13)
        Me.lblDtVigencia.TabIndex = 4
        Me.lblDtVigencia.Text = "Data Vigencia"
        '
        'lblVlFreteMinimo
        '
        Me.lblVlFreteMinimo.AutoSize = True
        Me.lblVlFreteMinimo.Location = New System.Drawing.Point(524, 155)
        Me.lblVlFreteMinimo.Name = "lblVlFreteMinimo"
        Me.lblVlFreteMinimo.Size = New System.Drawing.Size(100, 13)
        Me.lblVlFreteMinimo.TabIndex = 0
        Me.lblVlFreteMinimo.Text = "Valor Frete Minimo :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(263, 369)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(95, 13)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "Valor Maximo TDE"
        '
        'lblPcAdValorem
        '
        Me.lblPcAdValorem.AutoSize = True
        Me.lblPcAdValorem.Location = New System.Drawing.Point(130, 211)
        Me.lblPcAdValorem.Name = "lblPcAdValorem"
        Me.lblPcAdValorem.Size = New System.Drawing.Size(81, 13)
        Me.lblPcAdValorem.TabIndex = 2
        Me.lblPcAdValorem.Text = "Pc AD ValorEM"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(130, 369)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(92, 13)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Valor Minimo TDE"
        '
        'lblVlCat
        '
        Me.lblVlCat.AutoSize = True
        Me.lblVlCat.Location = New System.Drawing.Point(263, 211)
        Me.lblVlCat.Name = "lblVlCat"
        Me.lblVlCat.Size = New System.Drawing.Size(55, 13)
        Me.lblVlCat.TabIndex = 3
        Me.lblVlCat.Text = "Valor CAT"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(524, 316)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Pc TDE"
        '
        'lblVlTas
        '
        Me.lblVlTas.AutoSize = True
        Me.lblVlTas.Location = New System.Drawing.Point(397, 211)
        Me.lblVlTas.Name = "lblVlTas"
        Me.lblVlTas.Size = New System.Drawing.Size(55, 13)
        Me.lblVlTas.TabIndex = 4
        Me.lblVlTas.Text = "Valor TAS"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(397, 316)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(89, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Valor Paletiza��o"
        '
        'lblVlItr
        '
        Me.lblVlItr.AutoSize = True
        Me.lblVlItr.Location = New System.Drawing.Point(524, 211)
        Me.lblVlItr.Name = "lblVlItr"
        Me.lblVlItr.Size = New System.Drawing.Size(55, 13)
        Me.lblVlItr.TabIndex = 5
        Me.lblVlItr.Text = "Valor ITR "
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(263, 316)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Pc Reentrega"
        '
        'lblVlCtrc
        '
        Me.lblVlCtrc.AutoSize = True
        Me.lblVlCtrc.Location = New System.Drawing.Point(6, 268)
        Me.lblVlCtrc.Name = "lblVlCtrc"
        Me.lblVlCtrc.Size = New System.Drawing.Size(63, 13)
        Me.lblVlCtrc.TabIndex = 6
        Me.lblVlCtrc.Text = "Valor CTRC"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(130, 316)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Pc Fluvial"
        '
        'lblPcGris
        '
        Me.lblPcGris.AutoSize = True
        Me.lblPcGris.Location = New System.Drawing.Point(136, 268)
        Me.lblPcGris.Name = "lblPcGris"
        Me.lblPcGris.Size = New System.Drawing.Size(49, 13)
        Me.lblPcGris.TabIndex = 7
        Me.lblPcGris.Text = "Pc GRIS"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 316)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "VL Tx Desembara�o"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(263, 268)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Valor M�nimo GRIS"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(524, 268)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "QTD Pedagio"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(397, 268)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Valor Pedagio"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvFretePeso)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(784, 500)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Registros"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvFretePeso
        '
        Me.dgvFretePeso.AllowUserToAddRows = False
        Me.dgvFretePeso.AllowUserToDeleteRows = False
        Me.dgvFretePeso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFretePeso.Location = New System.Drawing.Point(9, 7)
        Me.dgvFretePeso.Name = "dgvFretePeso"
        Me.dgvFretePeso.ReadOnly = True
        Me.dgvFretePeso.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFretePeso.Size = New System.Drawing.Size(767, 150)
        Me.dgvFretePeso.TabIndex = 0
        '
        'frmFretePeso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 551)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmFretePeso"
        Me.Text = "Frete Peso"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupICMSincluso.ResumeLayout(False)
        Me.GroupICMSincluso.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvFretePeso, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPcSeguro As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtValorAd As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtValorTonelada As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtValorKg As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPesoFim As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPesoInicio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboTipoTabela As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MonthCalendar1 As System.Windows.Forms.MonthCalendar
    Friend WithEvents btnExcluir As System.Windows.Forms.Button
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupICMSincluso As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtDtVigencia As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlMaxTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlMinTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlPaletizacao As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcReentrega As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcFluvial As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlTxDesembaraco As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtQtdPedagio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlPedagio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlGRIS As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlITR As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlCTRC As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcGRIS As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlTAS As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlCAT As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcAD As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlFrete As System.Windows.Forms.MaskedTextBox
    Friend WithEvents cboUF As System.Windows.Forms.ComboBox
    Friend WithEvents cboCaracteristica As System.Windows.Forms.ComboBox
    Friend WithEvents lblCaracteristica As System.Windows.Forms.Label
    Friend WithEvents cboTransacao As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodRegiaoFrete As System.Windows.Forms.Label
    Friend WithEvents cboCidadeDestino As System.Windows.Forms.ComboBox
    Friend WithEvents cboRegiaoFrete As System.Windows.Forms.ComboBox
    Friend WithEvents cboTransportadora As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodCidadeDestino As System.Windows.Forms.Label
    Friend WithEvents cboLoja As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodLoja As System.Windows.Forms.Label
    Friend WithEvents lblCodUf As System.Windows.Forms.Label
    Friend WithEvents lblCodTransp As System.Windows.Forms.Label
    Friend WithEvents lblTpTransacao As System.Windows.Forms.Label
    Friend WithEvents lblDtVigencia As System.Windows.Forms.Label
    Friend WithEvents lblVlFreteMinimo As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblPcAdValorem As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblVlCat As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblVlTas As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblVlItr As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblVlCtrc As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblPcGris As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgvFretePeso As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
End Class
