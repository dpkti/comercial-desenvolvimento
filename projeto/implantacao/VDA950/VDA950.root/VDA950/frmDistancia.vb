Imports BLL
Public Class frmDistancia

    Private _logistica As Logistica
    Private _validator As Boolean = True
    Private _comboLoaded As Boolean = False
    Private _actionForm As userInterface.Action
    Private _shiftObject As Boolean = True

    Private Sub frmDistancia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call MontaUF()
        Call MontaDistancia()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0
    End Sub

    Private Sub MontaDistancia()

        _logistica = New Logistica

        Me.dgvDistancia.DataSource = _logistica.SelecionaDistancia

        Me.dgvDistancia.Columns(0).Visible = False

        Me.dgvDistancia.Columns(1).HeaderText = "CIDADE ORIGEM"
        Me.dgvDistancia.Columns(1).ReadOnly = True

        Me.dgvDistancia.Columns(2).Visible = False
        Me.dgvDistancia.Columns(3).Visible = False

        Me.dgvDistancia.Columns(4).HeaderText = "CIDADE DESTINO"
        Me.dgvDistancia.Columns(4).ReadOnly = True

        Me.dgvDistancia.Columns(5).Visible = False

        Me.dgvDistancia.Columns(6).HeaderText = "DIST�NCIA (Km)"
        Me.dgvDistancia.Columns(6).ReadOnly = True

        Me.dgvDistancia.RowTemplate.Height = 16
        Me.dgvDistancia.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Private Sub MontaUF()

        _logistica = New Logistica

        Me.cboUForigem.DataSource = _logistica.SelecionaUF()
        Me.cboUForigem.DisplayMember = "DESC_UF"
        Me.cboUForigem.ValueMember = "COD_UF"
        Me.cboUForigem.SelectedIndex = -1

        Me.cboUFdestino.DataSource = _logistica.SelecionaUF()
        Me.cboUFdestino.DisplayMember = "DESC_UF"
        Me.cboUFdestino.ValueMember = "COD_UF"
        Me.cboUFdestino.SelectedIndex = -1

        _comboLoaded = True

    End Sub

    Private Sub MontaCidadeOrigem()

        If Me.cboUForigem.SelectedIndex <> -1 Then

            _logistica = New Logistica
            _logistica.CodUf = Me.cboUForigem.SelectedValue.ToString

            Me.cboCidadeOrigem.DataSource = _logistica.SelecionaCidade()
            Me.cboCidadeOrigem.DisplayMember = "NOME_CIDADE"
            Me.cboCidadeOrigem.ValueMember = "COD_CIDADE"

        End If

    End Sub

    Private Sub MontaCidadeDestino()

        If Me.cboUFdestino.SelectedIndex <> -1 Then

            _logistica = New Logistica
            _logistica.CodUf = Me.cboUFdestino.SelectedValue.ToString

            Me.cboCidadeDestino.DataSource = _logistica.SelecionaCidade()
            Me.cboCidadeDestino.DisplayMember = "NOME_CIDADE"
            Me.cboCidadeDestino.ValueMember = "COD_CIDADE"

        End If

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboCidadeOrigem.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboCidadeOrigem, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboCidadeDestino.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtQtdeKm.Text = "" Then
            Me.ErrorProvider1.SetError(Me.txtQtdeKm, "Campo Obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaDistancia()
            Call MontaDistancia()
        End If

    End Sub

    Private Sub GravaDistancia()

        _logistica = New Logistica

        _logistica.Action = _actionForm
        _logistica.codCidadeOrigem = Me.cboCidadeOrigem.SelectedValue.ToString
        _logistica.codCidadeDestino = Me.cboCidadeDestino.SelectedValue.ToString
        _logistica.qtdKm = IIf(Me.txtQtdeKm.Text.Trim = "", 0, Me.txtQtdeKm.Text.Trim)

        Try
            ' grava os dados no banco
            _logistica.GravaDistanciaFrete()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub cboUForigem_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUForigem.SelectedIndexChanged
        If _comboLoaded = True Then
            Call MontaCidadeOrigem()
            Me.ErrorProvider1.SetError(Me.cboCidadeOrigem, "")
        End If
    End Sub

    Private Sub cboUFdestino_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUFdestino.SelectedIndexChanged
        If _comboLoaded = True Then
            Call MontaCidadeDestino()
            Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub dgvDistancia_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDistancia.CellClick

        If e.RowIndex > -1 Then

            ' recupera os valores
            Dim intCidadeOrigem As Int32 = Me.dgvDistancia.Rows(e.RowIndex).Cells(0).Value
            Dim intUfOrigem As String = Me.dgvDistancia.Rows(e.RowIndex).Cells(2).Value
            Dim intCidadeDestino As Int32 = Me.dgvDistancia.Rows(e.RowIndex).Cells(3).Value
            Dim intUfDestino As String = Me.dgvDistancia.Rows(e.RowIndex).Cells(5).Value
            Dim intKm As Decimal = Me.dgvDistancia.Rows(e.RowIndex).Cells(6).Value

            ' preenche os objetos com os valores
            Me.cboUForigem.SelectedValue = intUfOrigem
            Me.cboCidadeOrigem.SelectedValue = intCidadeOrigem
            Me.cboUFdestino.SelectedValue = intUfDestino
            Me.cboCidadeDestino.SelectedValue = intCidadeDestino
            Me.txtQtdeKm.Text = intKm

            ' desabilita os objetos
            Me.cboUForigem.Enabled = False
            Me.cboCidadeOrigem.Enabled = False
            Me.cboUFdestino.Enabled = False
            Me.cboCidadeDestino.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboCidadeOrigem, "")
        Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")

    End Sub

    Private Sub txtQtdeKm_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtdeKm.GotFocus
        Me.ErrorProvider1.SetError(Me.txtQtdeKm, "")
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaDistancia()
                Call MontaDistancia()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub
End Class