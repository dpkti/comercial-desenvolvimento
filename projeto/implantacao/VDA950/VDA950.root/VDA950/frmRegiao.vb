Imports BLL
Public Class frmRegiao
    Private _logistica As Logistica
    Private _actionForm As userInterface.Action
    Private _shiftObject As Boolean = True
    Private _validator As Boolean = True

    Private Sub frmRegiao_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaRegiao()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaRegiao()

        _logistica = New Logistica

        Me.dgvRegiao.DataSource = _logistica.SelecionaRegiaoFrete()
        Me.dgvRegiao.Columns(0).Width = 160
        Me.dgvRegiao.Columns(0).HeaderText = "C�DIGO REGI�O"

        Me.dgvRegiao.Columns(1).Width = 420
        Me.dgvRegiao.Columns(1).HeaderText = "REGI�O"

        Me.dgvRegiao.RowTemplate.Height = 16
        Me.dgvRegiao.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Private Sub GravaRegiao()

        ' Function que cria o c�digo regi�o randomicamente
        ' Dim x As Object = DateTime.Now
        ' x = (Replace(x, ":", "").Replace("PM", "").Replace("AM", "")).Trim().Substring(9, 6)
        ' Dim intCodRegiao As Int32 = x

        _logistica = New Logistica

        _logistica.Action = _actionForm
        _logistica.CodRegiaoFrete = IIf(Me.txtCodRegiao.Text.Trim = "", 0, Me.txtCodRegiao.Text.Trim)
        _logistica.DescRegiao = Me.txtRegiaoFrete.Text.Trim

        Try
            _logistica.GravaRegiaoFrete()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaRegiao()
                Call MontaRegiao()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.txtCodRegiao.Text.Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtCodRegiao, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtRegiaoFrete.Text.Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtRegiaoFrete, "Campo Obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaRegiao()
            Call MontaRegiao()
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.txtCodRegiao, "")
        Me.ErrorProvider1.SetError(Me.txtRegiaoFrete, "")

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub dgvRegiao_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRegiao.CellClick

        If e.RowIndex > -1 Then

            ' recupera os valores
            Dim intCodRegiao As Int32 = Me.dgvRegiao.Rows(e.RowIndex).Cells(0).Value
            Dim strRegiao As String = Me.dgvRegiao.Rows(e.RowIndex).Cells(1).Value

            ' preenche os objetos com os valores
            Me.txtCodRegiao.Text = intCodRegiao
            Me.txtRegiaoFrete.Text = strRegiao

            ' desabilita os objetos
            Me.txtCodRegiao.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub txtCodRegiao_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodRegiao.GotFocus
        Me.ErrorProvider1.SetError(Me.txtCodRegiao, "")
    End Sub

    Private Sub txtRegiaoFrete_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRegiaoFrete.GotFocus
        Me.ErrorProvider1.SetError(Me.txtRegiaoFrete, "")
    End Sub
End Class