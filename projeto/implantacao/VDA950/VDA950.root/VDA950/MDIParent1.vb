Imports System.Windows.Forms

Public Class MDIParent1

    Private m_ChildFormNumber As Integer

    Private Sub Dist�nciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Dist�nciaToolStripMenuItem.Click
        Dim frm As New frmDistancia
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub ContratoPorNotaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoPorNotaToolStripMenuItem.Click
        Dim frm As New frmContratoNf
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub Regi�oToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Regi�oToolStripMenuItem.Click
        Dim frm As New frmRegiao
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub ContratoPorRotaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoPorRotaToolStripMenuItem.Click
        Dim frm As New frmContratoRota
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub FreteXDist�nciaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FreteXDist�nciaToolStripMenuItem.Click
        Dim frm As New frmFreteDistancia
        userInterface.FormOpen(True, frm, Me, FormWindowState.Maximized)
    End Sub

    Private Sub FreteM�nimoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FreteM�nimoToolStripMenuItem.Click
        Dim frm As New frmFreteMinimo
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub ICMSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ICMSToolStripMenuItem.Click
        Dim frm As New frmICMS
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub RedespachoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RedespachoToolStripMenuItem.Click
        Dim frm As New frmRedespacho
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub ContentsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContentsToolStripMenuItem.Click
        Dim frm As New Form1
        userInterface.FormOpen(True, frm, Me, FormWindowState.Maximized)
    End Sub

    Private Sub PrazoPagamentoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrazoPagamentoToolStripMenuItem.Click
        Dim frm As New frmPrazoPgto
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub

    Private Sub FreteXPesoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FreteXPesoToolStripMenuItem.Click
        Dim frm As New frmFretePeso
        userInterface.FormOpen(True, frm, Me, FormWindowState.Normal)
    End Sub
End Class
