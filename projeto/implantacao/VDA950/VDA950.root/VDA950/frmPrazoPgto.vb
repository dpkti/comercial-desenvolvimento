Imports BLL
Public Class frmPrazoPgto
    Private _frete As Frete
    Private _faturamento As String
    Private _semana As String
    Private _actionForm As userInterface.Action
    Private _validator As Boolean = True
    Private _shiftObject As Boolean = True

    Private Sub frmPrazoPgto_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaLoja()
        Call MontaTransacao()
        Call MontaTransportadora()
        Call MontaPrazoPagamento()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTransacao()

        _frete = New Frete

        Me.cboTransacao.DataSource = _frete.SelecionaTransacao
        Me.cboTransacao.DisplayMember = "DESC_TRANSACAO"
        Me.cboTransacao.ValueMember = "TP_TRANSACAO"
        Me.cboTransacao.SelectedIndex = -1

    End Sub

    Private Sub MontaPrazoPagamento()

        _frete = New Frete

        Me.dgvPrazoPgto.DataSource = _frete.SelecionaPrazoPagamento
        Me.dgvPrazoPgto.Columns(0).Visible = False
        Me.dgvPrazoPgto.Columns(1).HeaderText = "LOJA"
        Me.dgvPrazoPgto.Columns(2).Visible = False
        Me.dgvPrazoPgto.Columns(3).HeaderText = "TRANSPORTADORA"
        Me.dgvPrazoPgto.Columns(4).Visible = False
        Me.dgvPrazoPgto.Columns(5).HeaderText = "TRANSA��O"
        Me.dgvPrazoPgto.Columns(6).HeaderText = "FATURAMENTO"
        Me.dgvPrazoPgto.Columns(7).HeaderText = "PRAZO PAGTO"
        Me.dgvPrazoPgto.Columns(8).HeaderText = "SEMANA"
        Me.dgvPrazoPgto.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPrazoPgto.RowTemplate.Height = 16

    End Sub

    Private Sub GravaPrazoPgto()

        For Each ctl As Control In boxTpFaturamento.Controls
            If CType(ctl, RadioButton).Checked = True Then
                _faturamento = (ctl.Text.Substring(0, 1))
            End If
        Next

        For Each ctl As Control In boxForaSemana.Controls
            If CType(ctl, RadioButton).Checked = True Then
                _semana = (ctl.Text.Substring(0, 1))
            End If
        Next

        _frete = New Frete

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.TipoTransacao = Me.cboTransacao.SelectedValue
        _frete.PrazoPagamento = IIf(Me.txtPrazoPgto.Text.Trim = "", 0, Me.txtPrazoPgto.Text.Trim)
        _frete.TipoFaturamento = _faturamento
        _frete.SemanaFora = _semana

        Try
            ' grava os dados no banco
            _frete.GravaPrazoPagamento()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransacao.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransacao, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.txtPrazoPgto.Text = "" Then
            Me.ErrorProvider1.SetError(Me.txtPrazoPgto, "Campo obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaPrazoPgto()
            Call MontaPrazoPagamento()
        End If

    End Sub

    Private Sub ToolStripNovo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripNovo.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaPrazoPgto()
                Call MontaPrazoPagamento()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboTransacao, "")
        Me.ErrorProvider1.SetError(Me.txtPrazoPgto, "")

    End Sub

    Private Sub dgvPrazoPgto_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPrazoPgto.CellClick

        If e.RowIndex > -1 Then

            Dim intCodLoja As Int32 = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(0).Value
            Dim intCodTransp As Int32 = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(2).Value
            Dim intCodTransacao As Int32 = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(4).Value
            Dim strTpFaturamento As String = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(6).Value
            Dim intPrazoPgto As Int32 = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(7).Value
            Dim foraSemana As String = Me.dgvPrazoPgto.Rows(e.RowIndex).Cells(8).Value

            ' preenche os valores
            Me.cboLoja.SelectedValue = intCodLoja
            Me.cboTransportadora.SelectedValue = intCodTransp
            Me.cboTransacao.SelectedValue = intCodTransacao
            Me.txtPrazoPgto.Text = intPrazoPgto

            For Each ctl As Control In boxTpFaturamento.Controls
                If ctl.Text.ToString.ToUpper = strTpFaturamento.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            For Each ctl As Control In boxForaSemana.Controls
                If ctl.Text.ToString.ToUpper = foraSemana.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            ' desabilita os objetos
            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False
            Me.cboTransacao.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub txtPrazoPgto_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrazoPgto.GotFocus
        Me.ErrorProvider1.SetError(Me.txtPrazoPgto, "")
    End Sub

End Class