<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTabFretePeso
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.MaskedTextBox20 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox21 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox22 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox23 = New System.Windows.Forms.MaskedTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.MaskedTextBox24 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox25 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox26 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox27 = New System.Windows.Forms.MaskedTextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.MaskedTextBox28 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox29 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox30 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox31 = New System.Windows.Forms.MaskedTextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.MaskedTextBox32 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox33 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox34 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox35 = New System.Windows.Forms.MaskedTextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.MaskedTextBox36 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox37 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox38 = New System.Windows.Forms.MaskedTextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.ComboBox10 = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.ComboBox11 = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.ComboBox12 = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.ComboBox13 = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.ComboBox14 = New System.Windows.Forms.ComboBox
        Me.ComboBox15 = New System.Windows.Forms.ComboBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.ComboBox16 = New System.Windows.Forms.ComboBox
        Me.ComboBox17 = New System.Windows.Forms.ComboBox
        Me.ComboBox18 = New System.Windows.Forms.ComboBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.MaskedTextBox1 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox2 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox3 = New System.Windows.Forms.MaskedTextBox
        Me.MaskedTextBox4 = New System.Windows.Forms.MaskedTextBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(655, 25)
        Me.ToolStrip1.TabIndex = 4
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 425)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(655, 22)
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'MaskedTextBox20
        '
        Me.MaskedTextBox20.Location = New System.Drawing.Point(487, 312)
        Me.MaskedTextBox20.Mask = "00000"
        Me.MaskedTextBox20.Name = "MaskedTextBox20"
        Me.MaskedTextBox20.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox20.TabIndex = 111
        Me.MaskedTextBox20.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox20.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox21
        '
        Me.MaskedTextBox21.Location = New System.Drawing.Point(324, 312)
        Me.MaskedTextBox21.Mask = "00000"
        Me.MaskedTextBox21.Name = "MaskedTextBox21"
        Me.MaskedTextBox21.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox21.TabIndex = 110
        Me.MaskedTextBox21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox21.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox22
        '
        Me.MaskedTextBox22.Location = New System.Drawing.Point(173, 312)
        Me.MaskedTextBox22.Mask = "00000000000000"
        Me.MaskedTextBox22.Name = "MaskedTextBox22"
        Me.MaskedTextBox22.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox22.TabIndex = 109
        Me.MaskedTextBox22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox23
        '
        Me.MaskedTextBox23.Location = New System.Drawing.Point(18, 312)
        Me.MaskedTextBox23.Mask = "000000000000000"
        Me.MaskedTextBox23.Name = "MaskedTextBox23"
        Me.MaskedTextBox23.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox23.TabIndex = 108
        Me.MaskedTextBox23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(487, 294)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "PC Reentrega :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(321, 294)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 13)
        Me.Label2.TabIndex = 106
        Me.Label2.Text = "PC Fluvial Advalores :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(173, 294)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(124, 13)
        Me.Label3.TabIndex = 105
        Me.Label3.Text = "Valor Tx. Desembarque :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 294)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 13)
        Me.Label4.TabIndex = 104
        Me.Label4.Text = "Qtd KG Pedagio :"
        '
        'MaskedTextBox24
        '
        Me.MaskedTextBox24.Location = New System.Drawing.Point(487, 269)
        Me.MaskedTextBox24.Mask = "00000000000000"
        Me.MaskedTextBox24.Name = "MaskedTextBox24"
        Me.MaskedTextBox24.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox24.TabIndex = 103
        Me.MaskedTextBox24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox25
        '
        Me.MaskedTextBox25.Location = New System.Drawing.Point(324, 269)
        Me.MaskedTextBox25.Mask = "00000000000000"
        Me.MaskedTextBox25.Name = "MaskedTextBox25"
        Me.MaskedTextBox25.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox25.TabIndex = 102
        Me.MaskedTextBox25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox26
        '
        Me.MaskedTextBox26.Location = New System.Drawing.Point(173, 269)
        Me.MaskedTextBox26.Mask = "00000"
        Me.MaskedTextBox26.Name = "MaskedTextBox26"
        Me.MaskedTextBox26.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox26.TabIndex = 101
        Me.MaskedTextBox26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox26.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox27
        '
        Me.MaskedTextBox27.Location = New System.Drawing.Point(18, 269)
        Me.MaskedTextBox27.Mask = "00000000000000"
        Me.MaskedTextBox27.Name = "MaskedTextBox27"
        Me.MaskedTextBox27.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox27.TabIndex = 100
        Me.MaskedTextBox27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(487, 251)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 99
        Me.Label5.Text = "VL Pedagio :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(324, 251)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 98
        Me.Label6.Text = "VL Min Gris NF :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(173, 251)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 13)
        Me.Label7.TabIndex = 97
        Me.Label7.Text = "PC GRIS :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 251)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 96
        Me.Label8.Text = "VL CTRC :"
        '
        'MaskedTextBox28
        '
        Me.MaskedTextBox28.Location = New System.Drawing.Point(487, 226)
        Me.MaskedTextBox28.Mask = "00000000000000"
        Me.MaskedTextBox28.Name = "MaskedTextBox28"
        Me.MaskedTextBox28.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox28.TabIndex = 95
        Me.MaskedTextBox28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox29
        '
        Me.MaskedTextBox29.Location = New System.Drawing.Point(324, 226)
        Me.MaskedTextBox29.Mask = "00000000000000"
        Me.MaskedTextBox29.Name = "MaskedTextBox29"
        Me.MaskedTextBox29.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox29.TabIndex = 94
        Me.MaskedTextBox29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox30
        '
        Me.MaskedTextBox30.Location = New System.Drawing.Point(173, 226)
        Me.MaskedTextBox30.Mask = "00000000000000"
        Me.MaskedTextBox30.Name = "MaskedTextBox30"
        Me.MaskedTextBox30.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox30.TabIndex = 93
        Me.MaskedTextBox30.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox31
        '
        Me.MaskedTextBox31.Location = New System.Drawing.Point(18, 226)
        Me.MaskedTextBox31.Mask = "00000"
        Me.MaskedTextBox31.Name = "MaskedTextBox31"
        Me.MaskedTextBox31.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox31.TabIndex = 92
        Me.MaskedTextBox31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox31.ValidatingType = GetType(Integer)
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(487, 208)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(47, 13)
        Me.Label9.TabIndex = 91
        Me.Label9.Text = "VL ITR :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(324, 208)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(50, 13)
        Me.Label10.TabIndex = 90
        Me.Label10.Text = "VL TAS :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(173, 208)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(50, 13)
        Me.Label11.TabIndex = 89
        Me.Label11.Text = "VL CAT :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(18, 208)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 88
        Me.Label12.Text = "PC Seguro :"
        '
        'MaskedTextBox32
        '
        Me.MaskedTextBox32.Location = New System.Drawing.Point(487, 183)
        Me.MaskedTextBox32.Mask = "00000000000000"
        Me.MaskedTextBox32.Name = "MaskedTextBox32"
        Me.MaskedTextBox32.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox32.TabIndex = 87
        Me.MaskedTextBox32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox33
        '
        Me.MaskedTextBox33.Location = New System.Drawing.Point(324, 183)
        Me.MaskedTextBox33.Mask = "00000"
        Me.MaskedTextBox33.Name = "MaskedTextBox33"
        Me.MaskedTextBox33.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox33.TabIndex = 86
        Me.MaskedTextBox33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox33.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox34
        '
        Me.MaskedTextBox34.Location = New System.Drawing.Point(173, 183)
        Me.MaskedTextBox34.Mask = "00000000000000"
        Me.MaskedTextBox34.Name = "MaskedTextBox34"
        Me.MaskedTextBox34.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox34.TabIndex = 85
        Me.MaskedTextBox34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox35
        '
        Me.MaskedTextBox35.Location = New System.Drawing.Point(18, 183)
        Me.MaskedTextBox35.Mask = "00000000000000"
        Me.MaskedTextBox35.Name = "MaskedTextBox35"
        Me.MaskedTextBox35.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox35.TabIndex = 84
        Me.MaskedTextBox35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(487, 165)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(105, 13)
        Me.Label13.TabIndex = 83
        Me.Label13.Text = "VL AD Valorem Min :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(321, 165)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 13)
        Me.Label14.TabIndex = 82
        Me.Label14.Text = "PC AD Valorem :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(170, 165)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(85, 13)
        Me.Label15.TabIndex = 81
        Me.Label15.Text = "Valor Tonelada :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(15, 165)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(55, 13)
        Me.Label16.TabIndex = 80
        Me.Label16.Text = "Valor KG :"
        '
        'MaskedTextBox36
        '
        Me.MaskedTextBox36.Location = New System.Drawing.Point(487, 140)
        Me.MaskedTextBox36.Mask = "00000000000000"
        Me.MaskedTextBox36.Name = "MaskedTextBox36"
        Me.MaskedTextBox36.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox36.TabIndex = 79
        Me.MaskedTextBox36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox37
        '
        Me.MaskedTextBox37.Location = New System.Drawing.Point(321, 140)
        Me.MaskedTextBox37.Mask = "00000"
        Me.MaskedTextBox37.Name = "MaskedTextBox37"
        Me.MaskedTextBox37.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox37.TabIndex = 78
        Me.MaskedTextBox37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox37.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox38
        '
        Me.MaskedTextBox38.Location = New System.Drawing.Point(170, 140)
        Me.MaskedTextBox38.Mask = "00000"
        Me.MaskedTextBox38.Name = "MaskedTextBox38"
        Me.MaskedTextBox38.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox38.TabIndex = 77
        Me.MaskedTextBox38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox38.ValidatingType = GetType(Integer)
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(487, 122)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(64, 13)
        Me.Label17.TabIndex = 76
        Me.Label17.Text = "Valor Frete :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(167, 122)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(91, 13)
        Me.Label18.TabIndex = 75
        Me.Label18.Text = "QTD Inicio Peso :"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(318, 122)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(82, 13)
        Me.Label19.TabIndex = 74
        Me.Label19.Text = "QTD Fim Peso :"
        '
        'ComboBox10
        '
        Me.ComboBox10.FormattingEnabled = True
        Me.ComboBox10.Location = New System.Drawing.Point(15, 140)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox10.TabIndex = 73
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 122)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(99, 13)
        Me.Label20.TabIndex = 72
        Me.Label20.Text = "Cod. Regiao Frete :"
        '
        'ComboBox11
        '
        Me.ComboBox11.FormattingEnabled = True
        Me.ComboBox11.Location = New System.Drawing.Point(487, 96)
        Me.ComboBox11.Name = "ComboBox11"
        Me.ComboBox11.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox11.TabIndex = 71
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(484, 78)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(77, 13)
        Me.Label21.TabIndex = 70
        Me.Label21.Text = "Caracteristica :"
        '
        'ComboBox12
        '
        Me.ComboBox12.FormattingEnabled = True
        Me.ComboBox12.Location = New System.Drawing.Point(321, 96)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox12.TabIndex = 69
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(318, 78)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(110, 13)
        Me.Label22.TabIndex = 68
        Me.Label22.Text = "Cod. Cidade Destino :"
        '
        'ComboBox13
        '
        Me.ComboBox13.FormattingEnabled = True
        Me.ComboBox13.Location = New System.Drawing.Point(170, 96)
        Me.ComboBox13.Name = "ComboBox13"
        Me.ComboBox13.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox13.TabIndex = 67
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(167, 78)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(52, 13)
        Me.Label23.TabIndex = 66
        Me.Label23.Text = "Cod. UF :"
        '
        'ComboBox14
        '
        Me.ComboBox14.FormattingEnabled = True
        Me.ComboBox14.Location = New System.Drawing.Point(12, 96)
        Me.ComboBox14.Name = "ComboBox14"
        Me.ComboBox14.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox14.TabIndex = 65
        '
        'ComboBox15
        '
        Me.ComboBox15.FormattingEnabled = True
        Me.ComboBox15.Location = New System.Drawing.Point(484, 52)
        Me.ComboBox15.Name = "ComboBox15"
        Me.ComboBox15.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox15.TabIndex = 64
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(9, 78)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(70, 13)
        Me.Label24.TabIndex = 63
        Me.Label24.Text = "Tipo Tabela :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(481, 34)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(80, 13)
        Me.Label25.TabIndex = 62
        Me.Label25.Text = "Data Vigencia :"
        '
        'ComboBox16
        '
        Me.ComboBox16.FormattingEnabled = True
        Me.ComboBox16.Location = New System.Drawing.Point(318, 52)
        Me.ComboBox16.Name = "ComboBox16"
        Me.ComboBox16.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox16.TabIndex = 61
        '
        'ComboBox17
        '
        Me.ComboBox17.FormattingEnabled = True
        Me.ComboBox17.Location = New System.Drawing.Point(167, 52)
        Me.ComboBox17.Name = "ComboBox17"
        Me.ComboBox17.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox17.TabIndex = 60
        '
        'ComboBox18
        '
        Me.ComboBox18.FormattingEnabled = True
        Me.ComboBox18.Location = New System.Drawing.Point(12, 52)
        Me.ComboBox18.Name = "ComboBox18"
        Me.ComboBox18.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox18.TabIndex = 59
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(315, 34)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(91, 13)
        Me.Label26.TabIndex = 58
        Me.Label26.Text = "Tipo Transa��o : "
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(164, 34)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(110, 13)
        Me.Label27.TabIndex = 57
        Me.Label27.Text = "Cod. Transportadora :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(9, 34)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(58, 13)
        Me.Label28.TabIndex = 56
        Me.Label28.Text = "Cod. Loja :"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(18, 339)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(45, 13)
        Me.Label29.TabIndex = 112
        Me.Label29.Text = "Label29"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(176, 339)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(45, 13)
        Me.Label30.TabIndex = 113
        Me.Label30.Text = "Label30"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(324, 339)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(45, 13)
        Me.Label31.TabIndex = 114
        Me.Label31.Text = "Label31"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(490, 339)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(45, 13)
        Me.Label32.TabIndex = 115
        Me.Label32.Text = "Label32"
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.Location = New System.Drawing.Point(18, 355)
        Me.MaskedTextBox1.Mask = "00000000000000"
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox1.TabIndex = 116
        Me.MaskedTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox2
        '
        Me.MaskedTextBox2.Location = New System.Drawing.Point(173, 355)
        Me.MaskedTextBox2.Mask = "00000"
        Me.MaskedTextBox2.Name = "MaskedTextBox2"
        Me.MaskedTextBox2.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox2.TabIndex = 117
        Me.MaskedTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.MaskedTextBox2.ValidatingType = GetType(Integer)
        '
        'MaskedTextBox3
        '
        Me.MaskedTextBox3.Location = New System.Drawing.Point(324, 355)
        Me.MaskedTextBox3.Mask = "00000000000000"
        Me.MaskedTextBox3.Name = "MaskedTextBox3"
        Me.MaskedTextBox3.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox3.TabIndex = 118
        Me.MaskedTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MaskedTextBox4
        '
        Me.MaskedTextBox4.Location = New System.Drawing.Point(487, 355)
        Me.MaskedTextBox4.Mask = "00000000000000"
        Me.MaskedTextBox4.Name = "MaskedTextBox4"
        Me.MaskedTextBox4.Size = New System.Drawing.Size(121, 20)
        Me.MaskedTextBox4.TabIndex = 119
        Me.MaskedTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(21, 382)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(39, 13)
        Me.Label33.TabIndex = 120
        Me.Label33.Text = "Label3"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(18, 399)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(121, 20)
        Me.TextBox1.TabIndex = 121
        '
        'frmTabFretePeso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(655, 447)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label33)
        Me.Controls.Add(Me.MaskedTextBox4)
        Me.Controls.Add(Me.MaskedTextBox3)
        Me.Controls.Add(Me.MaskedTextBox2)
        Me.Controls.Add(Me.MaskedTextBox1)
        Me.Controls.Add(Me.Label32)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.Label30)
        Me.Controls.Add(Me.Label29)
        Me.Controls.Add(Me.MaskedTextBox20)
        Me.Controls.Add(Me.MaskedTextBox21)
        Me.Controls.Add(Me.MaskedTextBox22)
        Me.Controls.Add(Me.MaskedTextBox23)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.MaskedTextBox24)
        Me.Controls.Add(Me.MaskedTextBox25)
        Me.Controls.Add(Me.MaskedTextBox26)
        Me.Controls.Add(Me.MaskedTextBox27)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.MaskedTextBox28)
        Me.Controls.Add(Me.MaskedTextBox29)
        Me.Controls.Add(Me.MaskedTextBox30)
        Me.Controls.Add(Me.MaskedTextBox31)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.MaskedTextBox32)
        Me.Controls.Add(Me.MaskedTextBox33)
        Me.Controls.Add(Me.MaskedTextBox34)
        Me.Controls.Add(Me.MaskedTextBox35)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.MaskedTextBox36)
        Me.Controls.Add(Me.MaskedTextBox37)
        Me.Controls.Add(Me.MaskedTextBox38)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.ComboBox10)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.ComboBox11)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.ComboBox12)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.ComboBox13)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.ComboBox14)
        Me.Controls.Add(Me.ComboBox15)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.ComboBox16)
        Me.Controls.Add(Me.ComboBox17)
        Me.Controls.Add(Me.ComboBox18)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.Label28)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmTabFretePeso"
        Me.Text = "Tabela Frete Peso"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents MaskedTextBox20 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox21 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox22 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox23 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox24 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox25 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox26 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox27 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox28 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox29 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox30 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox31 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox32 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox33 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox34 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox35 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox36 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox37 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox38 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents ComboBox11 As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents ComboBox13 As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents ComboBox14 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox15 As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents ComboBox16 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox17 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox18 As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox1 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox2 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox3 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents MaskedTextBox4 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class
