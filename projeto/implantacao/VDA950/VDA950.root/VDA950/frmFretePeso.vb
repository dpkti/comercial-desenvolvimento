Imports BLL
Public Class frmFretePeso
    Private _frete As Frete
    Private _logistica As Logistica
    Private _shiftObject As Boolean = True
    Private _validator As Boolean = True
    Private _comboLoaded As Boolean = False
    Private _actionForm As userInterface.Action
    Private _icmsIncluso As String

    Private Sub frmFretePeso_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaLoja()
        Call MontaTransportadora()
        Call MontaTransacao()
        Call MontaRegiaoFrete()
        Call MontaUF()
        Call MontaTipoTabela()
        Call MontaFretePeso()

        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTransacao()

        _frete = New Frete

        Me.cboTransacao.DataSource = _frete.SelecionaTransacao
        Me.cboTransacao.DisplayMember = "DESC_TRANSACAO"
        Me.cboTransacao.ValueMember = "TP_TRANSACAO"
        Me.cboTransacao.SelectedIndex = -1

    End Sub

    Private Sub MontaRegiaoFrete()

        _logistica = New Logistica

        Me.cboRegiaoFrete.DataSource = _logistica.SelecionaRegiaoFrete()
        Me.cboRegiaoFrete.DisplayMember = "DESC_REGIAO"
        Me.cboRegiaoFrete.ValueMember = "COD_REGIAO_FRETE"
        Me.cboRegiaoFrete.SelectedIndex = -1

    End Sub

    Private Sub MontaTipoTabela()

        _frete = New Frete

        Me.cboTipoTabela.DataSource = _frete.SelecionaTipoTabela
        Me.cboTipoTabela.DisplayMember = "DESC_TABELA"
        Me.cboTipoTabela.ValueMember = "TP_TABELA"
        Me.cboTipoTabela.SelectedIndex = -1

    End Sub

    Private Sub MontaUF()

        _logistica = New Logistica

        Me.cboUF.DataSource = _logistica.SelecionaUF()
        Me.cboUF.DisplayMember = "DESC_UF"
        Me.cboUF.ValueMember = "COD_UF"
        Me.cboUF.SelectedIndex = -1

        _comboLoaded = True

    End Sub

    Private Sub MontaCidadeDestino()

        If Me.cboUF.SelectedIndex <> -1 Then

            _logistica = New Logistica
            _logistica.CodUf = Me.cboUF.SelectedValue.ToString

            Me.cboCidadeDestino.DataSource = _logistica.SelecionaCidade()
            Me.cboCidadeDestino.DisplayMember = "NOME_CIDADE"
            Me.cboCidadeDestino.ValueMember = "COD_CIDADE"

        End If

    End Sub
 
    Private Sub cboUF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUF.SelectedIndexChanged
        If _comboLoaded = True Then
            Call MontaCidadeDestino()
            Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")
        End If
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.MonthCalendar1.Visible = True
    End Sub

    Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateSelected
        Me.txtDtVigencia.Text = e.Start.ToShortDateString
        Me.MonthCalendar1.Visible = False
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
    End Sub

    Private Sub MontaFretePeso()

        _frete = New Frete

        With Me.dgvFretePeso
            .DataSource = _frete.SelecionaFretePeso
            .RowTemplate.Height = 15
        End With

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True


        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo obrigatório")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo obrigatório")
            _validator = False
        End If

        If Me.cboTransacao.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransacao, "Campo Obrigatório")
            _validator = False
        End If

        If Me.cboTipoTabela.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTipoTabela, "Campo Obrigatório")
            _validator = False
        End If

        'If Me.cboUF.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboUF, "Campo Obrigatório")
        '    _validator = False
        'End If

        'If Me.cboCidadeDestino.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "Campo Obrigatório")
        '    _validator = False
        'End If

        'If Me.cboCaracteristica.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboCaracteristica, "Campo Obrigatório")
        '    _validator = False
        'End If

        If Me.cboRegiaoFrete.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboRegiaoFrete, "Campo Obrigatório")
            _validator = False
        End If

        If Me.txtDtVigencia.Text.Replace("/", "").Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtDtVigencia, "Campo obrigatório")
            _validator = False
        End If

        If _validator = True Then
            Call GravaFrete()
            Me.TabControl1.SelectedTab = TabPage2
            Call MontaFretePeso()
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaFrete()
                Call MontaFretePeso()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub GravaFrete()

        For Each ctl As Control In Me.GroupICMSincluso.Controls
            If CType(ctl, RadioButton).Checked = True Then
                _icmsIncluso = (ctl.Text.Substring(0, 1))
            End If
        Next

        _frete = New Frete

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.TipoTransacao = Me.cboTransacao.SelectedValue
        _frete.DataVigencia = Me.txtDtVigencia.Text.Trim
        _frete.TipoTabela = Me.cboTipoTabela.SelectedValue
        _frete.CodUf = Me.cboUF.SelectedValue
        _frete.codCidadeDestino = Me.cboCidadeDestino.SelectedValue
        _frete.Caracteristica = 1 'Me.cboCaracteristica.SelectedValue
        _frete.CodRegiaoFrete = Me.cboRegiaoFrete.SelectedValue
        _frete.PesoInicio = IIf(Me.txtPesoInicio.Text.Trim = "", 0, Me.txtPesoInicio.Text.Trim)
        _frete.PesoFim = IIf(Me.txtPesoFim.Text.Trim = "", 0, Me.txtPesoFim.Text.Trim)
        _frete.ValorFrete = IIf(Me.txtVlFrete.Text.Trim = "", 0, Me.txtVlFrete.Text.Trim)
        _frete.ValorKg = IIf(Me.txtValorKg.Text.Trim = "", 0, Me.txtValorKg.Text.Trim)
        _frete.ValorTonelada = IIf(Me.txtValorTonelada.Text.Trim = "", 0, Me.txtValorTonelada.Text.Trim)
        _frete.PcADvalorEM = IIf(Me.txtPcAD.Text.Trim = "", 0, Me.txtPcAD.Text.Trim)
        _frete.ValorAdEm = IIf(Me.txtValorAd.Text.Trim = "", 0, Me.txtValorAd.Text.Trim)
        _frete.PcSeguro = IIf(Me.txtPcSeguro.Text.Trim = "", 0, Me.txtPcSeguro.Text.Trim)
        _frete.ValorCAT = IIf(Me.txtVlCAT.Text.Trim = "", 0, Me.txtVlCAT.Text.Trim)
        _frete.ValorTAS = IIf(Me.txtVlTAS.Text.Trim = "", 0, Me.txtVlTAS.Text.Trim)
        _frete.ValorITR = IIf(Me.txtVlITR.Text.Trim = "", 0, Me.txtVlITR.Text.Trim)
        _frete.ValorCTRC = IIf(Me.txtVlCTRC.Text.Trim = "", 0, Me.txtVlCTRC.Text.Trim)
        _frete.PcGRIS = IIf(Me.txtPcGRIS.Text.Trim = "", 0, Me.txtPcGRIS.Text.Trim)
        _frete.ValorMinimoGris = IIf(Me.txtVlGRIS.Text.Trim = "", 0, Me.txtVlGRIS.Text.Trim)
        _frete.ValorPedagio = IIf(Me.txtVlPedagio.Text.Trim = "", 0, Me.txtVlPedagio.Text.Trim)
        _frete.QtdPedagio = IIf(Me.txtQtdPedagio.Text.Trim = "", 0, Me.txtQtdPedagio.Text.Trim)
        _frete.ValorTxDesembaraco = IIf(Me.txtVlTxDesembaraco.Text.Trim = "", 0, Me.txtVlTxDesembaraco.Text.Trim)
        _frete.PcFluvial = IIf(Me.txtPcFluvial.Text.Trim = "", 0, Me.txtPcFluvial.Text.Trim)
        _frete.PcReentrega = IIf(Me.txtPcReentrega.Text.Trim = "", 0, Me.txtPcReentrega.Text.Trim)
        _frete.ValorPaletizacao = IIf(Me.txtVlPaletizacao.Text.Trim = "", 0, Me.txtVlPaletizacao.Text.Trim)
        _frete.PcTDE = IIf(Me.txtPcTDE.Text.Trim = "", 0, Me.txtPcTDE.Text.Trim)
        _frete.ValorMinimoTDE = IIf(Me.txtVlMinTDE.Text.Trim = "", 0, Me.txtVlMinTDE.Text.Trim)
        _frete.ValorMaximoTDE = IIf(Me.txtVlMaxTDE.Text.Trim = "", 0, Me.txtVlMaxTDE.Text.Trim)
        _frete.ICMSIncluso = _icmsIncluso

        Try
            ' grava os dados no banco
            _frete.GravaFretePeso()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a ação do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboTransacao, "")
        Me.ErrorProvider1.SetError(Me.cboTipoTabela, "")
        Me.ErrorProvider1.SetError(Me.cboCaracteristica, "")
        Me.ErrorProvider1.SetError(Me.cboUF, "")
        Me.ErrorProvider1.SetError(Me.cboRegiaoFrete, "")
        Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        If Me.TabControl1.SelectedIndex = 1 Then
            Me.TabControl1.SelectedTab = TabPage1
        End If

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub dgvFretePeso_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFretePeso.CellDoubleClick

        If e.RowIndex > -1 Then

            ' habilita a Tab de cadastro
            Me.TabControl1.SelectedTab = TabPage1

            ' preenche os objetos com os valores
            Me.cboLoja.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(0).Value
            Me.cboTransportadora.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(2).Value
            Me.cboTransacao.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(4).Value
            Me.cboTipoTabela.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(6).Value
            Me.cboUF.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(8).Value
            Me.cboCidadeDestino.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(9).Value
            Me.cboCaracteristica.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(10).Value
            Me.cboRegiaoFrete.SelectedValue = Me.dgvFretePeso.Rows(e.RowIndex).Cells(11).Value
            Me.txtDtVigencia.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(12).Value

            Me.txtPesoInicio.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(13).Value
            Me.txtPesoFim.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(14).Value
            Me.txtVlFrete.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(15).Value
            Me.txtValorKg.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(16).Value
            Me.txtValorTonelada.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(17).Value
            Me.txtValorAd.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(18).Value
            Me.txtPcAD.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(19).Value
            Me.txtPcSeguro.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(20).Value
            Me.txtVlCAT.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(21).Value
            Me.txtVlTAS.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(22).Value
            Me.txtVlITR.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(23).Value
            Me.txtVlCTRC.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(24).Value
            Me.txtPcGRIS.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(25).Value
            Me.txtVlGRIS.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(26).Value
            Me.txtVlPedagio.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(27).Value
            Me.txtQtdPedagio.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(28).Value
            Me.txtVlTxDesembaraco.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(29).Value
            Me.txtPcFluvial.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(30).Value
            Me.txtPcReentrega.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(31).Value
            Me.txtVlPaletizacao.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(32).Value
            Me.txtPcTDE.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(33).Value
            Me.txtVlMinTDE.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(34).Value
            Me.txtVlMaxTDE.Text = Me.dgvFretePeso.Rows(e.RowIndex).Cells(35).Value

            Dim strICMSincluso As String = Me.dgvFretePeso.Rows(e.RowIndex).Cells(36).Value
            For Each ctl As Control In GroupICMSincluso.Controls
                If ctl.Text.ToString.ToUpper = strICMSincluso.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            ' desabilita os objetos
            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False
            Me.cboTransacao.Enabled = False
            Me.cboTipoTabela.Enabled = False
            Me.cboUF.Enabled = False
            Me.cboCidadeDestino.Enabled = False
            Me.cboCaracteristica.Enabled = False
            Me.cboRegiaoFrete.Enabled = False
            Me.txtDtVigencia.Enabled = False
            Me.PictureBox1.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda ação do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub
End Class