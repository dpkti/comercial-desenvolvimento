<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFreteDistancia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFreteDistancia))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtPesoInicial = New System.Windows.Forms.TextBox
        Me.txtPesoFinal = New System.Windows.Forms.MaskedTextBox
        Me.btnExcluir = New System.Windows.Forms.Button
        Me.GroupICMSincluso = New System.Windows.Forms.GroupBox
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.btnSalvar = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.txtVlMaximoTDE = New System.Windows.Forms.MaskedTextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtVlMinimoTDE = New System.Windows.Forms.MaskedTextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtPcTDE = New System.Windows.Forms.MaskedTextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtVlPaletizacao = New System.Windows.Forms.MaskedTextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtPcReentrega = New System.Windows.Forms.MaskedTextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtPcFluvial = New System.Windows.Forms.MaskedTextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtVlTxDesembaraco = New System.Windows.Forms.MaskedTextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtQtdPedagio = New System.Windows.Forms.MaskedTextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtVlPedagio = New System.Windows.Forms.MaskedTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtVlGris = New System.Windows.Forms.MaskedTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPcGris = New System.Windows.Forms.MaskedTextBox
        Me.txtVlCTRC = New System.Windows.Forms.MaskedTextBox
        Me.txtVlITR = New System.Windows.Forms.MaskedTextBox
        Me.txtVlTAS = New System.Windows.Forms.MaskedTextBox
        Me.txtVlCAT = New System.Windows.Forms.MaskedTextBox
        Me.txtPcSeguro = New System.Windows.Forms.MaskedTextBox
        Me.txtPcFrete = New System.Windows.Forms.MaskedTextBox
        Me.txtVlFrete = New System.Windows.Forms.MaskedTextBox
        Me.txtKmFinal = New System.Windows.Forms.MaskedTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtKmInicial = New System.Windows.Forms.MaskedTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDtVigencia = New System.Windows.Forms.MaskedTextBox
        Me.cboTipoTabela = New System.Windows.Forms.ComboBox
        Me.cboTransacao = New System.Windows.Forms.ComboBox
        Me.cboTransportadora = New System.Windows.Forms.ComboBox
        Me.cboLoja = New System.Windows.Forms.ComboBox
        Me.lblCodLoja = New System.Windows.Forms.Label
        Me.lblCodUf = New System.Windows.Forms.Label
        Me.lblCodTransp = New System.Windows.Forms.Label
        Me.lblTpTransacao = New System.Windows.Forms.Label
        Me.lblDtVigencia = New System.Windows.Forms.Label
        Me.lblVlFreteMinimo = New System.Windows.Forms.Label
        Me.lblPcAdValorem = New System.Windows.Forms.Label
        Me.lblVlCat = New System.Windows.Forms.Label
        Me.lblVlTas = New System.Windows.Forms.Label
        Me.lblVlItr = New System.Windows.Forms.Label
        Me.lblVlCtrc = New System.Windows.Forms.Label
        Me.lblPcGris = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.dgvFreteDistancia = New System.Windows.Forms.DataGridView
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolStrip1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupICMSincluso.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvFreteDistancia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(792, 25)
        Me.ToolStrip1.TabIndex = 4
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "ToolStripButton1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "ToolStripButton2"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 25)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(792, 541)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(784, 515)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Cadastro"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPesoInicial)
        Me.GroupBox1.Controls.Add(Me.txtPesoFinal)
        Me.GroupBox1.Controls.Add(Me.btnExcluir)
        Me.GroupBox1.Controls.Add(Me.GroupICMSincluso)
        Me.GroupBox1.Controls.Add(Me.MonthCalendar1)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.btnSalvar)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.txtVlMaximoTDE)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.txtVlMinimoTDE)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtPcTDE)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtVlPaletizacao)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtPcReentrega)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtPcFluvial)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtVlTxDesembaraco)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtQtdPedagio)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtVlPedagio)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtVlGris)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPcGris)
        Me.GroupBox1.Controls.Add(Me.txtVlCTRC)
        Me.GroupBox1.Controls.Add(Me.txtVlITR)
        Me.GroupBox1.Controls.Add(Me.txtVlTAS)
        Me.GroupBox1.Controls.Add(Me.txtVlCAT)
        Me.GroupBox1.Controls.Add(Me.txtPcSeguro)
        Me.GroupBox1.Controls.Add(Me.txtPcFrete)
        Me.GroupBox1.Controls.Add(Me.txtVlFrete)
        Me.GroupBox1.Controls.Add(Me.txtKmFinal)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtKmInicial)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDtVigencia)
        Me.GroupBox1.Controls.Add(Me.cboTipoTabela)
        Me.GroupBox1.Controls.Add(Me.cboTransacao)
        Me.GroupBox1.Controls.Add(Me.cboTransportadora)
        Me.GroupBox1.Controls.Add(Me.cboLoja)
        Me.GroupBox1.Controls.Add(Me.lblCodLoja)
        Me.GroupBox1.Controls.Add(Me.lblCodUf)
        Me.GroupBox1.Controls.Add(Me.lblCodTransp)
        Me.GroupBox1.Controls.Add(Me.lblTpTransacao)
        Me.GroupBox1.Controls.Add(Me.lblDtVigencia)
        Me.GroupBox1.Controls.Add(Me.lblVlFreteMinimo)
        Me.GroupBox1.Controls.Add(Me.lblPcAdValorem)
        Me.GroupBox1.Controls.Add(Me.lblVlCat)
        Me.GroupBox1.Controls.Add(Me.lblVlTas)
        Me.GroupBox1.Controls.Add(Me.lblVlItr)
        Me.GroupBox1.Controls.Add(Me.lblVlCtrc)
        Me.GroupBox1.Controls.Add(Me.lblPcGris)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(768, 517)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cadastro de Frete por Dist�ncia"
        '
        'txtPesoInicial
        '
        Me.txtPesoInicial.Location = New System.Drawing.Point(306, 149)
        Me.txtPesoInicial.MaxLength = 5
        Me.txtPesoInicial.Name = "txtPesoInicial"
        Me.txtPesoInicial.Size = New System.Drawing.Size(100, 20)
        Me.txtPesoInicial.TabIndex = 132
        Me.txtPesoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPesoFinal
        '
        Me.txtPesoFinal.Location = New System.Drawing.Point(450, 149)
        Me.txtPesoFinal.Mask = "00.000"
        Me.txtPesoFinal.Name = "txtPesoFinal"
        Me.txtPesoFinal.Size = New System.Drawing.Size(100, 20)
        Me.txtPesoFinal.TabIndex = 131
        '
        'btnExcluir
        '
        Me.btnExcluir.BackColor = System.Drawing.Color.Red
        Me.btnExcluir.ForeColor = System.Drawing.Color.White
        Me.btnExcluir.Location = New System.Drawing.Point(168, 444)
        Me.btnExcluir.Name = "btnExcluir"
        Me.btnExcluir.Size = New System.Drawing.Size(75, 23)
        Me.btnExcluir.TabIndex = 31
        Me.btnExcluir.Text = "Excluir"
        Me.btnExcluir.UseVisualStyleBackColor = False
        '
        'GroupICMSincluso
        '
        Me.GroupICMSincluso.Controls.Add(Me.RadioButton1)
        Me.GroupICMSincluso.Controls.Add(Me.RadioButton2)
        Me.GroupICMSincluso.Location = New System.Drawing.Point(238, 77)
        Me.GroupICMSincluso.Name = "GroupICMSincluso"
        Me.GroupICMSincluso.Size = New System.Drawing.Size(120, 41)
        Me.GroupICMSincluso.TabIndex = 130
        Me.GroupICMSincluso.TabStop = False
        Me.GroupICMSincluso.Text = "ICMS Incluso ?"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(8, 19)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(44, 17)
        Me.RadioButton1.TabIndex = 5
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "SIM"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(56, 18)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(48, 17)
        Me.RadioButton2.TabIndex = 127
        Me.RadioButton2.Text = "NAO"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'MonthCalendar1
        '
        Me.MonthCalendar1.Location = New System.Drawing.Point(592, 77)
        Me.MonthCalendar1.Name = "MonthCalendar1"
        Me.MonthCalendar1.TabIndex = 129
        Me.MonthCalendar1.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(428, 101)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox1.TabIndex = 128
        Me.PictureBox1.TabStop = False
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(87, 444)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 29
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(6, 444)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 30
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtVlMaximoTDE
        '
        Me.txtVlMaximoTDE.Location = New System.Drawing.Point(147, 407)
        Me.txtVlMaximoTDE.Mask = "00000000000000"
        Me.txtVlMaximoTDE.Name = "txtVlMaximoTDE"
        Me.txtVlMaximoTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtVlMaximoTDE.TabIndex = 28
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(146, 391)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(95, 13)
        Me.Label15.TabIndex = 121
        Me.Label15.Text = "Valor M�ximo TDE"
        '
        'txtVlMinimoTDE
        '
        Me.txtVlMinimoTDE.Location = New System.Drawing.Point(6, 407)
        Me.txtVlMinimoTDE.Mask = "00000000000000"
        Me.txtVlMinimoTDE.Name = "txtVlMinimoTDE"
        Me.txtVlMinimoTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtVlMinimoTDE.TabIndex = 27
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 391)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(94, 13)
        Me.Label14.TabIndex = 119
        Me.Label14.Text = "Valor M�nimo TDE"
        '
        'txtPcTDE
        '
        Me.txtPcTDE.Location = New System.Drawing.Point(450, 354)
        Me.txtPcTDE.Mask = "00000"
        Me.txtPcTDE.Name = "txtPcTDE"
        Me.txtPcTDE.Size = New System.Drawing.Size(100, 20)
        Me.txtPcTDE.TabIndex = 26
        Me.txtPcTDE.ValidatingType = GetType(Integer)
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(451, 338)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(45, 13)
        Me.Label13.TabIndex = 117
        Me.Label13.Text = "Pc TDE"
        '
        'txtVlPaletizacao
        '
        Me.txtVlPaletizacao.Location = New System.Drawing.Point(147, 354)
        Me.txtVlPaletizacao.Mask = "00000000000000"
        Me.txtVlPaletizacao.Name = "txtVlPaletizacao"
        Me.txtVlPaletizacao.Size = New System.Drawing.Size(100, 20)
        Me.txtVlPaletizacao.TabIndex = 24
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(144, 338)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(89, 13)
        Me.Label12.TabIndex = 115
        Me.Label12.Text = "Valor Paletiza��o"
        '
        'txtPcReentrega
        '
        Me.txtPcReentrega.Location = New System.Drawing.Point(308, 354)
        Me.txtPcReentrega.Mask = "00000"
        Me.txtPcReentrega.Name = "txtPcReentrega"
        Me.txtPcReentrega.Size = New System.Drawing.Size(100, 20)
        Me.txtPcReentrega.TabIndex = 25
        Me.txtPcReentrega.ValidatingType = GetType(Integer)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(305, 338)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(73, 13)
        Me.Label11.TabIndex = 113
        Me.Label11.Text = "Pc Reentrega"
        '
        'txtPcFluvial
        '
        Me.txtPcFluvial.Location = New System.Drawing.Point(6, 354)
        Me.txtPcFluvial.Mask = "00000"
        Me.txtPcFluvial.Name = "txtPcFluvial"
        Me.txtPcFluvial.Size = New System.Drawing.Size(100, 20)
        Me.txtPcFluvial.TabIndex = 23
        Me.txtPcFluvial.ValidatingType = GetType(Integer)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(4, 338)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 13)
        Me.Label10.TabIndex = 111
        Me.Label10.Text = "Pc Fluvial"
        '
        'txtVlTxDesembaraco
        '
        Me.txtVlTxDesembaraco.Location = New System.Drawing.Point(450, 302)
        Me.txtVlTxDesembaraco.Mask = "00000000000000"
        Me.txtVlTxDesembaraco.Name = "txtVlTxDesembaraco"
        Me.txtVlTxDesembaraco.Size = New System.Drawing.Size(100, 20)
        Me.txtVlTxDesembaraco.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(447, 286)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(115, 13)
        Me.Label6.TabIndex = 109
        Me.Label6.Text = "Valor Tx Desembara�o"
        '
        'txtQtdPedagio
        '
        Me.txtQtdPedagio.Location = New System.Drawing.Point(147, 302)
        Me.txtQtdPedagio.Mask = "000000000000000"
        Me.txtQtdPedagio.Name = "txtQtdPedagio"
        Me.txtQtdPedagio.Size = New System.Drawing.Size(100, 20)
        Me.txtQtdPedagio.TabIndex = 20
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(144, 286)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 13)
        Me.Label5.TabIndex = 107
        Me.Label5.Text = "Qtd Kg Ped�gio"
        '
        'txtVlPedagio
        '
        Me.txtVlPedagio.Location = New System.Drawing.Point(308, 302)
        Me.txtVlPedagio.Mask = "00000000000000"
        Me.txtVlPedagio.Name = "txtVlPedagio"
        Me.txtVlPedagio.Size = New System.Drawing.Size(100, 20)
        Me.txtVlPedagio.TabIndex = 21
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(305, 286)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 105
        Me.Label4.Text = "Valor Ped�gio"
        '
        'txtVlGris
        '
        Me.txtVlGris.Location = New System.Drawing.Point(6, 302)
        Me.txtVlGris.Mask = "00000000000000"
        Me.txtVlGris.Name = "txtVlGris"
        Me.txtVlGris.Size = New System.Drawing.Size(100, 20)
        Me.txtVlGris.TabIndex = 19
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 286)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 13)
        Me.Label3.TabIndex = 103
        Me.Label3.Text = "Valor M�nimo GRIS"
        '
        'txtPcGris
        '
        Me.txtPcGris.Location = New System.Drawing.Point(450, 199)
        Me.txtPcGris.Mask = "00000"
        Me.txtPcGris.Name = "txtPcGris"
        Me.txtPcGris.Size = New System.Drawing.Size(100, 20)
        Me.txtPcGris.TabIndex = 14
        Me.txtPcGris.ValidatingType = GetType(Integer)
        '
        'txtVlCTRC
        '
        Me.txtVlCTRC.Location = New System.Drawing.Point(6, 251)
        Me.txtVlCTRC.Mask = "00000000000000"
        Me.txtVlCTRC.Name = "txtVlCTRC"
        Me.txtVlCTRC.Size = New System.Drawing.Size(100, 20)
        Me.txtVlCTRC.TabIndex = 15
        '
        'txtVlITR
        '
        Me.txtVlITR.Location = New System.Drawing.Point(450, 251)
        Me.txtVlITR.Mask = "00000000000000"
        Me.txtVlITR.Name = "txtVlITR"
        Me.txtVlITR.Size = New System.Drawing.Size(100, 20)
        Me.txtVlITR.TabIndex = 18
        '
        'txtVlTAS
        '
        Me.txtVlTAS.Location = New System.Drawing.Point(306, 251)
        Me.txtVlTAS.Mask = "00000000000000"
        Me.txtVlTAS.Name = "txtVlTAS"
        Me.txtVlTAS.Size = New System.Drawing.Size(100, 20)
        Me.txtVlTAS.TabIndex = 17
        '
        'txtVlCAT
        '
        Me.txtVlCAT.Location = New System.Drawing.Point(147, 251)
        Me.txtVlCAT.Mask = "000000000000000"
        Me.txtVlCAT.Name = "txtVlCAT"
        Me.txtVlCAT.Size = New System.Drawing.Size(100, 20)
        Me.txtVlCAT.TabIndex = 16
        '
        'txtPcSeguro
        '
        Me.txtPcSeguro.Location = New System.Drawing.Point(306, 199)
        Me.txtPcSeguro.Mask = "00000"
        Me.txtPcSeguro.Name = "txtPcSeguro"
        Me.txtPcSeguro.Size = New System.Drawing.Size(100, 20)
        Me.txtPcSeguro.TabIndex = 13
        Me.txtPcSeguro.ValidatingType = GetType(Integer)
        '
        'txtPcFrete
        '
        Me.txtPcFrete.Location = New System.Drawing.Point(147, 199)
        Me.txtPcFrete.Mask = "00000"
        Me.txtPcFrete.Name = "txtPcFrete"
        Me.txtPcFrete.Size = New System.Drawing.Size(100, 20)
        Me.txtPcFrete.TabIndex = 12
        Me.txtPcFrete.ValidatingType = GetType(Integer)
        '
        'txtVlFrete
        '
        Me.txtVlFrete.Location = New System.Drawing.Point(6, 199)
        Me.txtVlFrete.Mask = "00000000000000"
        Me.txtVlFrete.Name = "txtVlFrete"
        Me.txtVlFrete.Size = New System.Drawing.Size(100, 20)
        Me.txtVlFrete.TabIndex = 11
        '
        'txtKmFinal
        '
        Me.txtKmFinal.Location = New System.Drawing.Point(147, 149)
        Me.txtKmFinal.Mask = "00000000"
        Me.txtKmFinal.Name = "txtKmFinal"
        Me.txtKmFinal.Size = New System.Drawing.Size(100, 20)
        Me.txtKmFinal.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(144, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 91
        Me.Label2.Text = "km Final"
        '
        'txtKmInicial
        '
        Me.txtKmInicial.Location = New System.Drawing.Point(6, 149)
        Me.txtKmInicial.Mask = "00000000"
        Me.txtKmInicial.Name = "txtKmInicial"
        Me.txtKmInicial.Size = New System.Drawing.Size(100, 20)
        Me.txtKmInicial.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 133)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Km Inicial"
        '
        'txtDtVigencia
        '
        Me.txtDtVigencia.Location = New System.Drawing.Point(450, 97)
        Me.txtDtVigencia.Mask = "00/00/0000"
        Me.txtDtVigencia.Name = "txtDtVigencia"
        Me.txtDtVigencia.Size = New System.Drawing.Size(100, 20)
        Me.txtDtVigencia.TabIndex = 6
        Me.txtDtVigencia.ValidatingType = GetType(Date)
        '
        'cboTipoTabela
        '
        Me.cboTipoTabela.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoTabela.FormattingEnabled = True
        Me.cboTipoTabela.Location = New System.Drawing.Point(6, 97)
        Me.cboTipoTabela.Name = "cboTipoTabela"
        Me.cboTipoTabela.Size = New System.Drawing.Size(200, 21)
        Me.cboTipoTabela.TabIndex = 4
        '
        'cboTransacao
        '
        Me.cboTransacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransacao.FormattingEnabled = True
        Me.cboTransacao.Location = New System.Drawing.Point(472, 44)
        Me.cboTransacao.Name = "cboTransacao"
        Me.cboTransacao.Size = New System.Drawing.Size(200, 21)
        Me.cboTransacao.TabIndex = 3
        '
        'cboTransportadora
        '
        Me.cboTransportadora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTransportadora.FormattingEnabled = True
        Me.cboTransportadora.Location = New System.Drawing.Point(238, 44)
        Me.cboTransportadora.Name = "cboTransportadora"
        Me.cboTransportadora.Size = New System.Drawing.Size(200, 21)
        Me.cboTransportadora.TabIndex = 2
        '
        'cboLoja
        '
        Me.cboLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoja.FormattingEnabled = True
        Me.cboLoja.Location = New System.Drawing.Point(7, 44)
        Me.cboLoja.Name = "cboLoja"
        Me.cboLoja.Size = New System.Drawing.Size(200, 21)
        Me.cboLoja.TabIndex = 1
        '
        'lblCodLoja
        '
        Me.lblCodLoja.AutoSize = True
        Me.lblCodLoja.Location = New System.Drawing.Point(4, 28)
        Me.lblCodLoja.Name = "lblCodLoja"
        Me.lblCodLoja.Size = New System.Drawing.Size(27, 13)
        Me.lblCodLoja.TabIndex = 37
        Me.lblCodLoja.Text = "Loja"
        '
        'lblCodUf
        '
        Me.lblCodUf.AutoSize = True
        Me.lblCodUf.Location = New System.Drawing.Point(447, 81)
        Me.lblCodUf.Name = "lblCodUf"
        Me.lblCodUf.Size = New System.Drawing.Size(74, 13)
        Me.lblCodUf.TabIndex = 50
        Me.lblCodUf.Text = "Data Vig�ncia"
        '
        'lblCodTransp
        '
        Me.lblCodTransp.AutoSize = True
        Me.lblCodTransp.Location = New System.Drawing.Point(235, 28)
        Me.lblCodTransp.Name = "lblCodTransp"
        Me.lblCodTransp.Size = New System.Drawing.Size(79, 13)
        Me.lblCodTransp.TabIndex = 41
        Me.lblCodTransp.Text = "Transportadora"
        '
        'lblTpTransacao
        '
        Me.lblTpTransacao.AutoSize = True
        Me.lblTpTransacao.Location = New System.Drawing.Point(468, 28)
        Me.lblTpTransacao.Name = "lblTpTransacao"
        Me.lblTpTransacao.Size = New System.Drawing.Size(58, 13)
        Me.lblTpTransacao.TabIndex = 45
        Me.lblTpTransacao.Text = "Transa��o"
        '
        'lblDtVigencia
        '
        Me.lblDtVigencia.AutoSize = True
        Me.lblDtVigencia.Location = New System.Drawing.Point(4, 81)
        Me.lblDtVigencia.Name = "lblDtVigencia"
        Me.lblDtVigencia.Size = New System.Drawing.Size(64, 13)
        Me.lblDtVigencia.TabIndex = 43
        Me.lblDtVigencia.Text = "Tipo Tabela"
        '
        'lblVlFreteMinimo
        '
        Me.lblVlFreteMinimo.AutoSize = True
        Me.lblVlFreteMinimo.Location = New System.Drawing.Point(303, 133)
        Me.lblVlFreteMinimo.Name = "lblVlFreteMinimo"
        Me.lblVlFreteMinimo.Size = New System.Drawing.Size(61, 13)
        Me.lblVlFreteMinimo.TabIndex = 36
        Me.lblVlFreteMinimo.Text = "Peso Inicial"
        '
        'lblPcAdValorem
        '
        Me.lblPcAdValorem.AutoSize = True
        Me.lblPcAdValorem.Location = New System.Drawing.Point(447, 133)
        Me.lblPcAdValorem.Name = "lblPcAdValorem"
        Me.lblPcAdValorem.Size = New System.Drawing.Size(56, 13)
        Me.lblPcAdValorem.TabIndex = 39
        Me.lblPcAdValorem.Text = "Peso Final"
        '
        'lblVlCat
        '
        Me.lblVlCat.AutoSize = True
        Me.lblVlCat.Location = New System.Drawing.Point(3, 183)
        Me.lblVlCat.Name = "lblVlCat"
        Me.lblVlCat.Size = New System.Drawing.Size(58, 13)
        Me.lblVlCat.TabIndex = 40
        Me.lblVlCat.Text = "Valor Frete"
        '
        'lblVlTas
        '
        Me.lblVlTas.AutoSize = True
        Me.lblVlTas.Location = New System.Drawing.Point(144, 183)
        Me.lblVlTas.Name = "lblVlTas"
        Me.lblVlTas.Size = New System.Drawing.Size(47, 13)
        Me.lblVlTas.TabIndex = 44
        Me.lblVlTas.Text = "Pc Frete"
        '
        'lblVlItr
        '
        Me.lblVlItr.AutoSize = True
        Me.lblVlItr.Location = New System.Drawing.Point(303, 183)
        Me.lblVlItr.Name = "lblVlItr"
        Me.lblVlItr.Size = New System.Drawing.Size(57, 13)
        Me.lblVlItr.TabIndex = 46
        Me.lblVlItr.Text = "Pc Seguro"
        '
        'lblVlCtrc
        '
        Me.lblVlCtrc.AutoSize = True
        Me.lblVlCtrc.Location = New System.Drawing.Point(144, 235)
        Me.lblVlCtrc.Name = "lblVlCtrc"
        Me.lblVlCtrc.Size = New System.Drawing.Size(55, 13)
        Me.lblVlCtrc.TabIndex = 49
        Me.lblVlCtrc.Text = "Valor CAT"
        '
        'lblPcGris
        '
        Me.lblPcGris.AutoSize = True
        Me.lblPcGris.Location = New System.Drawing.Point(303, 235)
        Me.lblPcGris.Name = "lblPcGris"
        Me.lblPcGris.Size = New System.Drawing.Size(55, 13)
        Me.lblPcGris.TabIndex = 52
        Me.lblPcGris.Text = "Valor TAS"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(447, 235)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 54
        Me.Label7.Text = "Valor ITR"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(447, 183)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 60
        Me.Label9.Text = "Pc GRIS"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 235)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "Valor CTRC"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dgvFreteDistancia)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(784, 515)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Registros"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dgvFreteDistancia
        '
        Me.dgvFreteDistancia.AllowUserToAddRows = False
        Me.dgvFreteDistancia.AllowUserToDeleteRows = False
        Me.dgvFreteDistancia.AllowUserToOrderColumns = True
        Me.dgvFreteDistancia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFreteDistancia.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvFreteDistancia.Location = New System.Drawing.Point(3, 3)
        Me.dgvFreteDistancia.Name = "dgvFreteDistancia"
        Me.dgvFreteDistancia.ReadOnly = True
        Me.dgvFreteDistancia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFreteDistancia.Size = New System.Drawing.Size(778, 315)
        Me.dgvFreteDistancia.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'frmFreteDistancia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 566)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Name = "frmFreteDistancia"
        Me.Text = "Frete x Dist�ncia"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupICMSincluso.ResumeLayout(False)
        Me.GroupICMSincluso.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvFreteDistancia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtVlMaximoTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtVlMinimoTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPcTDE As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtVlPaletizacao As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtPcReentrega As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPcFluvial As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtVlTxDesembaraco As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtQtdPedagio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtVlPedagio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtVlGris As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPcGris As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlCTRC As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlITR As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlTAS As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlCAT As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcSeguro As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPcFrete As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtVlFrete As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtKmFinal As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtKmInicial As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDtVigencia As System.Windows.Forms.MaskedTextBox
    Friend WithEvents cboTipoTabela As System.Windows.Forms.ComboBox
    Friend WithEvents cboTransacao As System.Windows.Forms.ComboBox
    Friend WithEvents cboTransportadora As System.Windows.Forms.ComboBox
    Friend WithEvents cboLoja As System.Windows.Forms.ComboBox
    Friend WithEvents lblCodLoja As System.Windows.Forms.Label
    Friend WithEvents lblCodUf As System.Windows.Forms.Label
    Friend WithEvents lblCodTransp As System.Windows.Forms.Label
    Friend WithEvents lblTpTransacao As System.Windows.Forms.Label
    Friend WithEvents lblDtVigencia As System.Windows.Forms.Label
    Friend WithEvents lblVlFreteMinimo As System.Windows.Forms.Label
    Friend WithEvents lblPcAdValorem As System.Windows.Forms.Label
    Friend WithEvents lblVlCat As System.Windows.Forms.Label
    Friend WithEvents lblVlTas As System.Windows.Forms.Label
    Friend WithEvents lblVlItr As System.Windows.Forms.Label
    Friend WithEvents lblVlCtrc As System.Windows.Forms.Label
    Friend WithEvents lblPcGris As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgvFreteDistancia As System.Windows.Forms.DataGridView
    Friend WithEvents MonthCalendar1 As System.Windows.Forms.MonthCalendar
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupICMSincluso As System.Windows.Forms.GroupBox
    Friend WithEvents btnExcluir As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtPesoFinal As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtPesoInicial As System.Windows.Forms.TextBox
End Class
