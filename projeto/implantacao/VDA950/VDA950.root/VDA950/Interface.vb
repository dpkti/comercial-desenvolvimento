Public Class userInterface

    ''' <summary>
    ''' Verifica se o form esta aberto
    ''' </summary>
    ''' <param name="Unique"></param>
    ''' <param name="FormName"></param>
    ''' <param name="ParentForm"></param>
    ''' <param name="WindowState"></param>
    ''' <remarks></remarks>
    Public Shared Sub FormOpen(ByVal Unique As Boolean, ByVal FormName As Form, ByVal ParentForm As Form, ByVal WindowState As FormWindowState, Optional ByVal x As Int32 = 0, Optional ByVal y As Int32 = 0)

        Dim jaAberto As Boolean = False

        If Unique = True Then
            Dim tooltip As New ToolTip()
            For i As Integer = 0 To ParentForm.MdiChildren.Length - 1
                If ParentForm.MdiChildren(i).Name = FormName.Name Then
                    jaAberto = True
                End If
            Next
        End If

        If jaAberto = False Then
            FormName.MdiParent = ParentForm
            FormName.WindowState = WindowState
            ' vrf o window state
            'If WindowState <> FormWindowState.Maximized Then
            '    FormName.Width = x
            '    FormName.Height = y
            'End If
            FormName.Show()
        End If

    End Sub

    ''' <summary>
    ''' Limpa os controles
    ''' </summary>
    ''' <param name="ParentControl"></param>
    ''' <remarks></remarks>
    Public Shared Sub CleanFields(ByVal ParentControl As Control)

        For Each ctl As Control In ParentControl.Controls

            If TypeOf (ctl) Is TextBox Then 'TextBox
                DirectCast(ctl, TextBox).Text = String.Empty
            ElseIf TypeOf (ctl) Is CheckBox Then 'CheckBox
                DirectCast(ctl, CheckBox).Checked = False
            ElseIf TypeOf (ctl) Is ComboBox Then 'ComboBox
                DirectCast(ctl, ComboBox).SelectedIndex = -1
            ElseIf TypeOf (ctl) Is DataGrid Then 'Datagrid
                DirectCast(ctl, DataGrid).DataSource = Nothing
                'ElseIf TypeOf (ctl) Is RadioButton Then ' RadioButton
                '    DirectCast(ctl, RadioButton).Checked = True
            ElseIf TypeOf (ctl) Is MaskedTextBox Then ' maskara de texto
                DirectCast(ctl, MaskedTextBox).Text = String.Empty
            ElseIf ctl.Controls.Count > 0 Then
                CleanFields(ctl)
            End If

        Next

    End Sub

    ''' <summary>
    ''' habilita os objetos que estavam com enable=false
    ''' </summary>
    ''' <param name="ParentControm"></param>
    ''' <remarks></remarks>
    Public Shared Sub HabilitaObjeto(ByVal ParentControm As Control)

        For Each ctl As Control In ParentControm.Controls

            If ctl.Enabled = False Then
                ctl.Enabled = True
            ElseIf ctl.Controls.Count > 0 Then
                HabilitaObjeto(ctl)
            End If

        Next

    End Sub

    ''' <summary>
    ''' determina a a��o do formul�rio
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum Action
        Inserir = 1
        Editar = 2
        Deletar = 3
    End Enum

    ''' <summary>
    ''' grava o erro gerado
    ''' </summary>
    ''' <param name="pErro"></param>
    ''' <remarks></remarks>
    Public Shared Sub GravarLogErro(ByVal pErro As System.Exception)

    End Sub


End Class
