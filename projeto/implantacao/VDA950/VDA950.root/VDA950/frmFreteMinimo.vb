Imports BLL
Public Class frmFreteMinimo
    Private _frete As Frete
    Private _logistica As Logistica
    Private _icmsIncluso As String
    Private _actionForm As userInterface.Action
    Private _shiftObject As Boolean = True
    Private _validator As Boolean = True
    Private _comboLoaded As Boolean = False

    Private Sub frmFreteMinimo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaLoja()
        Call MontaTransportadora()
        Call MontaTransacao()
        Call MontaRegiaoFrete()
        Call MontaUF()
        Call MontaTipoTabela()
        Call MontaFreteMinimo()

        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTransacao()

        _frete = New Frete

        Me.cboTransacao.DataSource = _frete.SelecionaTransacao
        Me.cboTransacao.DisplayMember = "DESC_TRANSACAO"
        Me.cboTransacao.ValueMember = "TP_TRANSACAO"
        Me.cboTransacao.SelectedIndex = -1

    End Sub

    Private Sub MontaRegiaoFrete()

        _logistica = New Logistica

        Me.cboRegiaoFrete.DataSource = _logistica.SelecionaRegiaoFrete()
        Me.cboRegiaoFrete.DisplayMember = "DESC_REGIAO"
        Me.cboRegiaoFrete.ValueMember = "COD_REGIAO_FRETE"
        Me.cboRegiaoFrete.SelectedIndex = -1

    End Sub

    Private Sub MontaTipoTabela()

        _frete = New Frete

        Me.cboTipoTabela.DataSource = _frete.SelecionaTipoTabela
        Me.cboTipoTabela.DisplayMember = "DESC_TABELA"
        Me.cboTipoTabela.ValueMember = "TP_TABELA"
        Me.cboTipoTabela.SelectedIndex = -1

    End Sub

    Private Sub MontaUF()

        _logistica = New Logistica

        Me.cboUF.DataSource = _logistica.SelecionaUF()
        Me.cboUF.DisplayMember = "DESC_UF"
        Me.cboUF.ValueMember = "COD_UF"
        Me.cboUF.SelectedIndex = -1

        _comboLoaded = True

    End Sub

    Private Sub MontaCidadeDestino()

        If Me.cboUF.SelectedIndex <> -1 Then

            _logistica = New Logistica
            _logistica.CodUf = Me.cboUF.SelectedValue.ToString

            Me.cboCidadeDestino.DataSource = _logistica.SelecionaCidade()
            Me.cboCidadeDestino.DisplayMember = "NOME_CIDADE"
            Me.cboCidadeDestino.ValueMember = "COD_CIDADE"

        End If

    End Sub

    Private Sub cboUF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUF.SelectedIndexChanged

        If _comboLoaded = True Then
            Call MontaCidadeDestino()
            Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")
        End If

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.MonthCalendar1.Visible = True
    End Sub

    Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateSelected
        Me.txtDtVigencia.Text = e.Start.ToShortDateString
        Me.MonthCalendar1.Visible = False
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
    End Sub

    Private Sub MontaFreteMinimo()

        _frete = New Frete

        With Me.dgvFreteMinimo
            .DataSource = _frete.SelecionaFreteMinimo
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "LOJA"
            .Columns(2).Visible = False
            .Columns(3).HeaderText = "TRANSPORTADORA"
            .Columns(4).Visible = False
            .Columns(5).HeaderText = "TRANSA��O"
            .Columns(6).Visible = False
            .Columns(7).HeaderText = "COBRAN�A"
            .Columns(8).HeaderText = "UF"
            .Columns(9).HeaderText = "CIDADE DESTINO"
            .Columns(10).HeaderText = "CARACTERISTICA"
            .Columns(11).HeaderText = "REGI�O FRETE"
            .Columns(12).HeaderText = "VIG�NCIA"
            .Columns(13).HeaderText = "FRETE"
            .Columns(14).HeaderText = "% AD"
            .Columns(15).HeaderText = "CAT"
            .Columns(16).HeaderText = "TAS"
            .Columns(17).HeaderText = "ITR"
            .Columns(18).HeaderText = "CTRC"
            .Columns(19).HeaderText = "% GRIS"
            .Columns(20).HeaderText = "GRIS"
            .Columns(21).HeaderText = "PED�GIO"
            .Columns(22).HeaderText = "QTD PED�GIO"
            .Columns(23).HeaderText = "TX DESEMBARA�O"
            .Columns(24).HeaderText = "% FLUVIAL"
            .Columns(25).HeaderText = "% REENTREGA"
            .Columns(26).HeaderText = "PALETIZA��O"
            .Columns(27).HeaderText = "% TDE"
            .Columns(28).HeaderText = "VL MIN TDE"
            .Columns(29).HeaderText = "VL MAX TDE"
            .Columns(30).HeaderText = "ICMS INCLUSO"
            .RowTemplate.Height = 15
        End With

    End Sub

    Private Sub GravaFrete()

        For Each ctl As Control In Me.GroupICMSincluso.Controls
            If CType(ctl, RadioButton).Checked = True Then
                _icmsIncluso = (ctl.Text.Substring(0, 1))
            End If
        Next

        _frete = New Frete

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.TipoTransacao = Me.cboTransacao.SelectedValue
        _frete.DataVigencia = Me.txtDtVigencia.Text.Trim
        _frete.TipoTabela = Me.cboTipoTabela.SelectedValue
        _frete.CodUf = 1 'Me.cboUF.SelectedValue
        _frete.codCidadeDestino = Me.cboCidadeDestino.SelectedValue
        _frete.Caracteristica = 1 'Me.cboCaracteristica.SelectedValue
        _frete.CodRegiaoFrete = Me.cboRegiaoFrete.SelectedValue
        _frete.ValorFrete = IIf(Me.txtVlFrete.Text.Trim = "", 0, Me.txtVlFrete.Text.Trim)
        _frete.PcADvalorEM = IIf(Me.txtPcAD.Text.Trim = "", 0, Me.txtPcAD.Text.Trim)
        _frete.ValorCAT = IIf(Me.txtVlCAT.Text.Trim = "", 0, Me.txtVlCAT.Text.Trim)
        _frete.ValorTAS = IIf(Me.txtVlTAS.Text.Trim = "", 0, Me.txtVlTAS.Text.Trim)
        _frete.ValorITR = IIf(Me.txtVlITR.Text.Trim = "", 0, Me.txtVlITR.Text.Trim)
        _frete.ValorCTRC = IIf(Me.txtVlCTRC.Text.Trim = "", 0, Me.txtVlCTRC.Text.Trim)
        _frete.PcGRIS = IIf(Me.txtPcGRIS.Text.Trim = "", 0, Me.txtPcGRIS.Text.Trim)
        _frete.ValorMinimoGris = IIf(Me.txtVlGRIS.Text.Trim = "", 0, Me.txtVlGRIS.Text.Trim)
        _frete.ValorPedagio = IIf(Me.txtVlPedagio.Text.Trim = "", 0, Me.txtVlPedagio.Text.Trim)
        _frete.QtdPedagio = IIf(Me.txtQtdPedagio.Text.Trim = "", 0, Me.txtQtdPedagio.Text.Trim)
        _frete.ValorTxDesembaraco = IIf(Me.txtVlTxDesembaraco.Text.Trim = "", 0, Me.txtVlTxDesembaraco.Text.Trim)
        _frete.PcFluvial = IIf(Me.txtPcFluvial.Text.Trim = "", 0, Me.txtPcFluvial.Text.Trim)
        _frete.PcReentrega = IIf(Me.txtPcReentrega.Text.Trim = "", 0, Me.txtPcReentrega.Text.Trim)
        _frete.ValorPaletizacao = IIf(Me.txtVlPaletizacao.Text.Trim = "", 0, Me.txtVlPaletizacao.Text.Trim)
        _frete.PcTDE = IIf(Me.txtPcTDE.Text.Trim = "", 0, Me.txtPcTDE.Text.Trim)
        _frete.ValorMinimoTDE = IIf(Me.txtVlMinTDE.Text.Trim = "", 0, Me.txtVlMinTDE.Text.Trim)
        _frete.ValorMaximoTDE = IIf(Me.txtVlMaxTDE.Text.Trim = "", 0, Me.txtVlMaxTDE.Text.Trim)
        _frete.ICMSIncluso = _icmsIncluso

        Try
            ' grava os dados no banco
            _frete.GravaFreteMinimo()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaFrete()
                Call MontaFreteMinimo()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransacao.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransacao, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboTipoTabela.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTipoTabela, "Campo Obrigat�rio")
            _validator = False
        End If

        'If Me.cboUF.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboUF, "Campo Obrigat�rio")
        '    _validator = False
        'End If

        'If Me.cboCidadeDestino.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "Campo Obrigat�rio")
        '    _validator = False
        'End If

        'If Me.cboCaracteristica.SelectedIndex = -1 Then
        '    Me.ErrorProvider1.SetError(Me.cboCaracteristica, "Campo Obrigat�rio")
        '    _validator = False
        'End If

        If Me.cboRegiaoFrete.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboRegiaoFrete, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtDtVigencia.Text.Replace("/", "").Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtDtVigencia, "Campo obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaFrete()
            Me.TabControl1.SelectedTab = TabPage2
            Call MontaFreteMinimo()
        End If


    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboTransacao, "")
        Me.ErrorProvider1.SetError(Me.cboTipoTabela, "")
        Me.ErrorProvider1.SetError(Me.cboCaracteristica, "")
        Me.ErrorProvider1.SetError(Me.cboUF, "")
        Me.ErrorProvider1.SetError(Me.cboRegiaoFrete, "")
        Me.ErrorProvider1.SetError(Me.cboCidadeDestino, "")
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")

    End Sub

    Private Sub dgvFreteMinimo_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFreteMinimo.CellDoubleClick

        If e.RowIndex > -1 Then

            ' habilita a Tab de cadastro
            Me.TabControl1.SelectedTab = TabPage1

            Me.cboLoja.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(0).Value
            Me.cboTransportadora.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(2).Value
            Me.cboTransacao.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(4).Value
            Me.cboTipoTabela.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(6).Value
            Me.cboUF.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(8).Value
            Me.cboCidadeDestino.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(9).Value
            Me.cboCaracteristica.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(10).Value
            Me.cboRegiaoFrete.SelectedValue = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(11).Value

            Me.txtDtVigencia.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(12).Value
            Me.txtVlFrete.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(13).Value
            Me.txtPcAD.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(14).Value
            Me.txtVlCAT.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(15).Value
            Me.txtVlTAS.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(16).Value
            Me.txtVlITR.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(17).Value
            Me.txtVlCTRC.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(18).Value
            Me.txtPcGRIS.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(19).Value
            Me.txtVlGRIS.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(20).Value
            Me.txtVlPedagio.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(21).Value
            Me.txtQtdPedagio.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(22).Value
            Me.txtVlTxDesembaraco.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(23).Value
            Me.txtPcFluvial.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(24).Value
            Me.txtPcReentrega.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(25).Value
            Me.txtVlPaletizacao.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(26).Value
            Me.txtPcTDE.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(27).Value
            Me.txtVlMinTDE.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(28).Value
            Me.txtVlMaxTDE.Text = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(29).Value

            Dim strICMSincluso As String = Me.dgvFreteMinimo.Rows(e.RowIndex).Cells(30).Value
            For Each ctl As Control In GroupICMSincluso.Controls
                If ctl.Text.ToString.ToUpper = strICMSincluso.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            ' desabilita os objetos
            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False
            Me.cboTransacao.Enabled = False
            Me.cboTipoTabela.Enabled = False
            Me.cboUF.Enabled = False
            Me.cboCidadeDestino.Enabled = False
            Me.cboCaracteristica.Enabled = False
            Me.cboRegiaoFrete.Enabled = False
            Me.txtDtVigencia.Enabled = False
            Me.PictureBox1.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        If Me.TabControl1.SelectedIndex = 1 Then
            Me.TabControl1.SelectedTab = TabPage1
        End If

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub
End Class