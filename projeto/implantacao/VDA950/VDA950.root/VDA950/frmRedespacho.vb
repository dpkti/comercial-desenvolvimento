Imports BLL
Public Class frmRedespacho
    Private _frete As Frete
    Private _validator As Boolean = True
    Private _shiftObject As Boolean = True
    Private _actionForm As userInterface.Action
    Private _situacao As String

    Private Sub frmRedespacho_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call MontaLoja()
        Call MontaTransportadora()
        Call MontaTranspRedespacho()
        Call MontaRedespacho()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0
    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTranspRedespacho()

        Me.cboRedespacho.DataSource = _frete.SelecionaTransportadora
        Me.cboRedespacho.DisplayMember = "NOME_TRANSP"
        Me.cboRedespacho.ValueMember = "COD_TRANSP"
        Me.cboRedespacho.SelectedIndex = -1

    End Sub

    Private Sub MontaRedespacho()

        _frete = New Frete

        With Me.dgvRedespacho
            .DataSource = _frete.SelecionaRedespacho
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "LOJA"
            .Columns(2).Visible = False
            .Columns(3).HeaderText = "TRANSPORTADORA"
            .Columns(4).Visible = False
            .Columns(5).HeaderText = "REDESPACHO"
            .Columns(6).Visible = False
            .Columns(7).HeaderText = "SITUA��O"
            .RowTemplate.Height = 16
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            .ReadOnly = True
        End With

    End Sub

    Private Sub GravaRedespacho()

        _frete = New Frete

        For Each ctl As Control In Me.GroupBoxSituacao.Controls
            If CType(ctl, RadioButton).Checked = True Then
                If ctl.Text.ToString = "Ativa" Then
                    _situacao = 0
                Else
                    _situacao = 9
                End If
            End If
        Next

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.CodRedespacho = Me.cboRedespacho.SelectedValue
        _frete.Situacao = _situacao

        Try
            ' grava os dados no banco
            _frete.GravaRedespacho()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboRedespacho, "")

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaRedespacho()
                Call MontaRedespacho()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboRedespacho.Text = "" Then
            Me.ErrorProvider1.SetError(Me.cboRedespacho, "Campo Obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaRedespacho()
            Call MontaRedespacho()
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub dgvRedespacho_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRedespacho.CellClick

        If e.RowIndex > -1 Then

            ' recupera os valores
            Dim intCodLoja As Int32 = Me.dgvRedespacho.Rows(e.RowIndex).Cells(0).Value
            Dim intCodTransp As Int32 = Me.dgvRedespacho.Rows(e.RowIndex).Cells(2).Value
            Dim intCodRedespacho As Int32 = Me.dgvRedespacho.Rows(e.RowIndex).Cells(4).Value
            Dim intSituacao As Int32 = Me.dgvRedespacho.Rows(e.RowIndex).Cells(6).Value

            ' preenche os objetos com os valores
            Me.cboLoja.SelectedValue = intCodLoja
            Me.cboTransportadora.SelectedValue = intCodTransp
            Me.cboRedespacho.SelectedValue = intCodRedespacho

            Dim strSituacao As String

            If intSituacao = 0 Then
                strSituacao = "Ativa"
            Else
                strSituacao = "Cancelada"
            End If

            For Each ctl As Control In GroupBoxSituacao.Controls
                If ctl.Text.ToString.ToUpper = strSituacao.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            ' desabilita os objetos
            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub
End Class