Imports BLL
Public Class frmContratoNf
    Private _frete As Frete
    Private _actionForm As userInterface.Action
    Private _validator As Boolean = True
    Private _shiftObject As Boolean = True

    Private Sub frmContratoNf_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaLoja()
        Call MontaTransportadora()
        Call MontaTransacao()
        Call MontaTipoTabela()
        Call MontaContratoNota()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTransacao()

        _frete = New Frete

        Me.cboTransacao.DataSource = _frete.SelecionaTransacao
        Me.cboTransacao.DisplayMember = "DESC_TRANSACAO"
        Me.cboTransacao.ValueMember = "TP_TRANSACAO"
        Me.cboTransacao.SelectedIndex = -1

    End Sub

    Private Sub MontaTipoTabela()

        _frete = New Frete

        Me.cboTpTabela.DataSource = _frete.SelecionaTipoTabela
        Me.cboTpTabela.DisplayMember = "DESC_TABELA"
        Me.cboTpTabela.ValueMember = "TP_TABELA"
        Me.cboTpTabela.SelectedIndex = -1

    End Sub

    Private Sub MontaContratoNota()

        _frete = New Frete

        Me.dgvContrato.DataSource = _frete.SelecionaContratoNota
        Me.dgvContrato.Columns(0).Visible = False
        Me.dgvContrato.Columns(1).HeaderText = "LOJA"
        Me.dgvContrato.Columns(2).Visible = False
        Me.dgvContrato.Columns(3).HeaderText = "TRANSPORTADORA"
        Me.dgvContrato.Columns(4).Visible = False
        Me.dgvContrato.Columns(5).HeaderText = "TRANSA��O"
        Me.dgvContrato.Columns(6).Visible = False
        Me.dgvContrato.Columns(7).HeaderText = "COBRAN�A"
        Me.dgvContrato.Columns(8).HeaderText = "VIG�NCIA"

        Me.dgvContrato.Columns(9).HeaderText = "NF INICIO"
        Me.dgvContrato.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Me.dgvContrato.Columns(10).HeaderText = "NF FIM"
        Me.dgvContrato.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Me.dgvContrato.Columns(11).HeaderText = "FRETE"
        Me.dgvContrato.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Me.dgvContrato.Columns(12).HeaderText = "% FRETE"
        Me.dgvContrato.Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        Me.dgvContrato.RowTemplate.Height = 16
        Me.dgvContrato.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Private Sub GravaContrato()

        _frete = New Frete

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.TipoTransacao = Me.cboTransacao.SelectedValue
        _frete.TipoTabela = Me.cboTpTabela.SelectedValue
        _frete.DataVigencia = Me.txtDtVigencia.Text.Trim
        _frete.ValorNotaInicio = IIf(Me.txtVlNotaInicio.Text.Trim = "", 0, Me.txtVlNotaInicio.Text.Trim)
        _frete.ValorNotaFim = IIf(Me.txtVlNotaFim.Text.Trim = "", 0, Me.txtVlNotaFim.Text.Trim)
        _frete.ValorFrete = IIf(Me.txtVlFrete.Text = "", 0, Me.txtVlFrete.Text.Trim)
        _frete.PcFrete = IIf(Me.txtPcFrete.Text = "", 0, Me.txtPcFrete.Text.Trim)

        Try
            ' grava os dados no banco
            _frete.GravaContratoNota()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub PictureBox1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.MonthCalendar1.Visible = True
    End Sub

    Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateSelected
        Me.txtDtVigencia.Text = e.Start.ToShortDateString
        Me.MonthCalendar1.Visible = False
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransacao.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransacao, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboTpTabela.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTpTabela, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtDtVigencia.Text.Replace("/", "").Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtDtVigencia, "Campo obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaContrato()
            Call MontaContratoNota()
        End If

    End Sub

    Private Sub dgvContrato_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContrato.CellClick

        If e.RowIndex > -1 Then

            ' recupera os valores
            Dim intCodLoja As Int32 = Me.dgvContrato.Rows(e.RowIndex).Cells(0).Value
            Dim intCodTransp As Int32 = Me.dgvContrato.Rows(e.RowIndex).Cells(2).Value
            Dim intCodTransacao As Int32 = Me.dgvContrato.Rows(e.RowIndex).Cells(4).Value
            Dim intTpTabela As Int32 = Me.dgvContrato.Rows(e.RowIndex).Cells(6).Value
            Dim dtVigencia As Date = Me.dgvContrato.Rows(e.RowIndex).Cells(8).Value
            Dim vlNotaInicio As Decimal = Me.dgvContrato.Rows(e.RowIndex).Cells(9).Value
            Dim vlNotaFim As Decimal = Me.dgvContrato.Rows(e.RowIndex).Cells(10).Value
            Dim vlFrete As Decimal = Me.dgvContrato.Rows(e.RowIndex).Cells(11).Value
            Dim pcFrete As Decimal = Me.dgvContrato.Rows(e.RowIndex).Cells(12).Value

            ' preenche os objetos com os valores
            Me.cboLoja.SelectedValue = intCodLoja
            Me.cboTransportadora.SelectedValue = intCodTransp
            Me.cboTransacao.SelectedValue = intCodTransacao
            Me.cboTpTabela.SelectedValue = intTpTabela
            Me.txtDtVigencia.Text = dtVigencia
            Me.txtVlNotaInicio.Text = vlNotaInicio
            Me.txtVlNotaFim.Text = vlNotaFim
            Me.txtVlFrete.Text = vlFrete
            Me.txtPcFrete.Text = pcFrete

            ' desabilita os objetos
            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False
            Me.cboTransacao.Enabled = False
            Me.cboTpTabela.Enabled = False
            Me.txtDtVigencia.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboTransacao, "")
        Me.ErrorProvider1.SetError(Me.cboTpTabela, "")
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaContrato()
                Call MontaContratoNota()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub txtDtVigencia_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDtVigencia.GotFocus
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
    End Sub
End Class