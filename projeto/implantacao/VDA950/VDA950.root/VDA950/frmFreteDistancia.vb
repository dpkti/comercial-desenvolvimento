Imports BLL
Public Class frmFreteDistancia
    Private _frete As Frete
    Private _icmsIncluso As String
    Private _actionForm As userInterface.Action
    Private _shiftObject As Boolean = True
    Private _validator As Boolean = True

    Private Sub frmFreteDistancia_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call MontaLoja()
        Call MontaTransportadora()
        Call MontaTransacao()
        Call MontaTipoTabela()
        Call MontaFreteDistancia()

        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0

    End Sub

    Private Sub MontaLoja()

        _frete = New Frete

        Me.cboLoja.DataSource = _frete.SelecionaLoja()
        Me.cboLoja.DisplayMember = "NOME_FANTASIA"
        Me.cboLoja.ValueMember = "COD_LOJA"
        Me.cboLoja.SelectedIndex = -1

    End Sub

    Private Sub MontaTransportadora()

        _frete = New Frete

        Me.cboTransportadora.DataSource = _frete.SelecionaTransportadora
        Me.cboTransportadora.DisplayMember = "NOME_TRANSP"
        Me.cboTransportadora.ValueMember = "COD_TRANSP"
        Me.cboTransportadora.SelectedIndex = -1

    End Sub

    Private Sub MontaTransacao()

        _frete = New Frete

        Me.cboTransacao.DataSource = _frete.SelecionaTransacao
        Me.cboTransacao.DisplayMember = "DESC_TRANSACAO"
        Me.cboTransacao.ValueMember = "TP_TRANSACAO"
        Me.cboTransacao.SelectedIndex = -1

    End Sub

    Private Sub MontaTipoTabela()

        _frete = New Frete

        Me.cboTipoTabela.DataSource = _frete.SelecionaTipoTabela
        Me.cboTipoTabela.DisplayMember = "DESC_TABELA"
        Me.cboTipoTabela.ValueMember = "TP_TABELA"
        Me.cboTipoTabela.SelectedIndex = -1

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        Me.MonthCalendar1.Visible = True
    End Sub

    Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateSelected
        Me.txtDtVigencia.Text = e.Start.ToShortDateString
        Me.MonthCalendar1.Visible = False
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
    End Sub

    Private Sub GravaFrete()

        For Each ctl As Control In Me.GroupICMSincluso.Controls
            If CType(ctl, RadioButton).Checked = True Then
                _icmsIncluso = (ctl.Text.Substring(0, 1))
            End If
        Next

        _frete = New Frete

        _frete.Action = _actionForm
        _frete.CodLoja = Me.cboLoja.SelectedValue
        _frete.CodTransportadora = Me.cboTransportadora.SelectedValue
        _frete.TipoTransacao = Me.cboTransacao.SelectedValue
        _frete.DataVigencia = Me.txtDtVigencia.Text.Trim
        _frete.TipoTabela = Me.cboTipoTabela.SelectedValue
        _frete.KmInicio = IIf(Me.txtKmInicial.Text.Trim = ".", 0, Me.txtKmInicial.Text.Trim)
        _frete.KmFim = IIf(Me.txtKmFinal.Text.Trim = ".", 0, Me.txtKmFinal.Text.Trim)
        _frete.PesoInicio = IIf(Me.txtPesoInicial.Text.Trim = ".", 0, Me.txtPesoInicial.Text.Trim)
        _frete.PesoFim = IIf(Me.txtPesoFinal.Text.Trim = ".", 0, Me.txtPesoFinal.Text.Trim)
        _frete.ValorFrete = IIf(Me.txtVlFrete.Text.Trim = "", 0, Me.txtVlFrete.Text.Trim)
        _frete.PcFrete = IIf(Me.txtPcFrete.Text.Trim = "", 0, Me.txtPcFrete.Text.Trim)
        _frete.PcSeguro = IIf(Me.txtPcSeguro.Text.Trim = "", 0, Me.txtPcSeguro.Text.Trim)
        _frete.ValorCAT = IIf(Me.txtVlCAT.Text.Trim = "", 0, Me.txtVlCAT.Text.Trim)
        _frete.ValorTAS = IIf(Me.txtVlTAS.Text.Trim = "", 0, Me.txtVlTAS.Text.Trim)
        _frete.ValorITR = IIf(Me.txtVlITR.Text.Trim = "", 0, Me.txtVlITR.Text.Trim)
        _frete.ValorCTRC = IIf(Me.txtVlCTRC.Text.Trim = "", 0, Me.txtVlCTRC.Text.Trim)
        _frete.PcGRIS = IIf(Me.txtPcGris.Text.Trim = "", 0, Me.txtPcGris.Text.Trim)
        _frete.ValorMinimoGris = IIf(Me.txtVlGris.Text.Trim = "", 0, Me.txtVlGris.Text.Trim)
        _frete.ValorPedagio = IIf(Me.txtVlPedagio.Text.Trim = "", 0, Me.txtVlPedagio.Text.Trim)
        _frete.QtdPedagio = IIf(Me.txtQtdPedagio.Text.Trim = "", 0, Me.txtQtdPedagio.Text.Trim)
        _frete.ValorTxDesembaraco = IIf(Me.txtVlTxDesembaraco.Text.Trim = "", 0, Me.txtVlTxDesembaraco.Text.Trim)
        _frete.PcFluvial = IIf(Me.txtPcFluvial.Text.Trim = "", 0, Me.txtPcFluvial.Text.Trim)
        _frete.PcReentrega = IIf(Me.txtPcReentrega.Text.Trim = "", 0, Me.txtPcReentrega.Text.Trim)
        _frete.ValorPaletizacao = IIf(Me.txtVlPaletizacao.Text.Trim = "", 0, Me.txtVlPaletizacao.Text.Trim)
        _frete.PcTDE = IIf(Me.txtPcTDE.Text.Trim = "", 0, Me.txtPcTDE.Text.Trim)
        _frete.ValorMinimoTDE = IIf(Me.txtVlMinimoTDE.Text.Trim = "", 0, Me.txtVlMinimoTDE.Text.Trim)
        _frete.ValorMaximoTDE = IIf(Me.txtVlMaximoTDE.Text.Trim = "", 0, Me.txtVlMaximoTDE.Text.Trim)
        _frete.ICMSIncluso = _icmsIncluso

        Try
            ' grava os dados no banco
            _frete.GravaFreteDistancia()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os ojbetos q estavam enable false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboLoja.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboLoja, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransportadora.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransportadora, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.cboTransacao.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTransacao, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboTipoTabela.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboTipoTabela, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtDtVigencia.Text.Replace("/", "").Trim = String.Empty Then
            Me.ErrorProvider1.SetError(Me.txtDtVigencia, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.txtKmInicial.Text.Replace(".", "").Trim = "" Then
            Me.ErrorProvider1.SetError(Me.txtKmInicial, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.txtKmFinal.Text.Replace(".", "").Trim = "" Then
            Me.ErrorProvider1.SetError(Me.txtKmFinal, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.txtPesoInicial.Text.Replace(".", "").Trim = "" Then
            Me.ErrorProvider1.SetError(Me.txtPesoInicial, "Campo obrigat�rio")
            _validator = False
        End If

        If Me.txtPesoFinal.Text.Replace(".", "").Trim = "" Then
            Me.ErrorProvider1.SetError(Me.txtPesoFinal, "Campo obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaFrete()
            Me.TabControl1.SelectedTab = TabPage2
            Call MontaFreteDistancia()
        End If

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaFrete()
                Call MontaFreteDistancia()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboLoja, "")
        Me.ErrorProvider1.SetError(Me.cboTransportadora, "")
        Me.ErrorProvider1.SetError(Me.cboTransacao, "")
        Me.ErrorProvider1.SetError(Me.cboTipoTabela, "")
        Me.ErrorProvider1.SetError(Me.txtDtVigencia, "")
        Me.ErrorProvider1.SetError(Me.txtKmInicial, "")
        Me.ErrorProvider1.SetError(Me.txtKmFinal, "")
        Me.ErrorProvider1.SetError(Me.txtPesoInicial, "")
        Me.ErrorProvider1.SetError(Me.txtPesoFinal, "")

    End Sub

    Private Sub MontaFreteDistancia()

        _frete = New Frete

        With Me.dgvFreteDistancia
            .DataSource = _frete.SelecionaFreteDistancia
            .Columns(0).Visible = False
            .Columns(1).HeaderText = "LOJA"
            .Columns(2).Visible = False
            .Columns(3).HeaderText = "TRANSPORTADORA"
            .Columns(4).Visible = False
            .Columns(5).HeaderText = "TRANSA��O"
            .Columns(6).Visible = False
            .Columns(7).HeaderText = "COBRAN�A"
            .Columns(8).HeaderText = "VIG�NCIA"
            .Columns(9).HeaderText = "KM INICIO"
            .Columns(10).HeaderText = "KM FIM"
            .Columns(11).HeaderText = "PESO INICIO"
            .Columns(12).HeaderText = "PESO FIM"
            .Columns(13).HeaderText = "FRETE"
            .Columns(14).HeaderText = "% FRETE"
            .Columns(15).HeaderText = "% SEGURO"
            .Columns(16).HeaderText = "CAT"
            .Columns(17).HeaderText = "TAS"
            .Columns(18).HeaderText = "ITR"
            .Columns(19).HeaderText = "CTRC"
            .Columns(20).HeaderText = "% GRIS"
            .Columns(21).HeaderText = "VL GRIS"
            .Columns(22).HeaderText = "PED�GIO"
            .Columns(23).HeaderText = "QTD PED�GIO"
            .Columns(24).HeaderText = "DESEMBARA�O"
            .Columns(25).HeaderText = "% FLUVIAL"
            .Columns(26).HeaderText = "% REENTREGA"
            .Columns(27).HeaderText = "PALETIZA��O"
            .Columns(28).HeaderText = "% TDE"
            .Columns(29).HeaderText = "MIN TDE"
            .Columns(30).HeaderText = "MAX TDE"
            .Columns(31).HeaderText = "ICMS INCLUSO"
            .ScrollBars = ScrollBars.Both
            .RowTemplate.Height = 15
        End With

    End Sub

    Private Sub dgvFreteDistancia_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFreteDistancia.CellDoubleClick

        If e.RowIndex > -1 Then

            ' habilita a Tab de cadastro
            Me.TabControl1.SelectedTab = TabPage1

            ' preenche os objetos com os valores
            Me.cboLoja.SelectedValue = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(0).Value
            Me.cboTransportadora.SelectedValue = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(2).Value
            Me.cboTransacao.SelectedValue = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(4).Value
            Me.cboTipoTabela.SelectedValue = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(6).Value
            Me.txtDtVigencia.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(8).Value
            Me.txtKmInicial.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(9).Value
            Me.txtKmFinal.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(10).Value
            Me.txtPesoInicial.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(11).Value
            Me.txtPesoFinal.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(12).Value
            Me.txtVlFrete.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(13).Value
            Me.txtPcFrete.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(14).Value
            Me.txtPcSeguro.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(15).Value
            Me.txtVlCAT.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(16).Value
            Me.txtVlTAS.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(17).Value
            Me.txtVlITR.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(18).Value
            Me.txtVlCTRC.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(19).Value
            Me.txtPcGris.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(20).Value
            Me.txtVlGris.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(21).Value
            Me.txtVlPedagio.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(22).Value
            Me.txtQtdPedagio.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(23).Value
            Me.txtVlTxDesembaraco.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(24).Value
            Me.txtPcFluvial.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(25).Value
            Me.txtPcReentrega.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(26).Value
            Me.txtVlPaletizacao.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(27).Value
            Me.txtPcTDE.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(28).Value
            Me.txtVlMinimoTDE.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(29).Value
            Me.txtVlMaximoTDE.Text = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(30).Value

            Dim strICMSincluso As String = Me.dgvFreteDistancia.Rows(e.RowIndex).Cells(31).Value
            For Each ctl As Control In GroupICMSincluso.Controls
                If ctl.Text.ToString.ToUpper = strICMSincluso.ToUpper Then
                    CType(ctl, RadioButton).Checked = True
                End If
            Next

            ' desabilita os objetos

            Me.cboLoja.Enabled = False
            Me.cboTransportadora.Enabled = False
            Me.cboTransacao.Enabled = False
            Me.cboTipoTabela.Enabled = False
            Me.txtDtVigencia.Enabled = False
            Me.txtKmInicial.Enabled = False
            Me.txtKmFinal.Enabled = False
            Me.txtPesoInicial.Enabled = False
            Me.txtPesoFinal.Enabled = False
            Me.PictureBox1.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        If Me.TabControl1.SelectedIndex = 1 Then
            Me.TabControl1.SelectedTab = TabPage1
        End If

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub txtKmInicial_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKmInicial.GotFocus
        Me.ErrorProvider1.SetError(Me.txtKmInicial, "")
    End Sub

    Private Sub txtKmFinal_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtKmFinal.GotFocus
        Me.ErrorProvider1.SetError(Me.txtKmFinal, "")
    End Sub

    Private Sub txtPesoInicial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPesoInicial.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not e.KeyChar = vbBack And Not e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub
End Class