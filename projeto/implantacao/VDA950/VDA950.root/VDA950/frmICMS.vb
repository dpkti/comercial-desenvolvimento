Imports BLL
Public Class frmICMS
    Private _ICMS As ICMS
    Private _logistica As Logistica
    Private _actionForm As userInterface.Action
    Private _shiftObject As Boolean = True
    Private _validator As Boolean = True

    Private Sub frmICMS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call MontaUF()
        Call MontaICMS()
        _actionForm = userInterface.Action.Inserir
        Me.ErrorProvider1.BlinkRate = 0
    End Sub

    Private Sub MontaUF()

        _logistica = New Logistica

        Me.cboUForigem.DataSource = _logistica.SelecionaUF()
        Me.cboUForigem.DisplayMember = "DESC_UF"
        Me.cboUForigem.ValueMember = "COD_UF"
        Me.cboUForigem.SelectedIndex = -1

        Me.cboUFdestino.DataSource = _logistica.SelecionaUF()
        Me.cboUFdestino.DisplayMember = "DESC_UF"
        Me.cboUFdestino.ValueMember = "COD_UF"
        Me.cboUFdestino.SelectedIndex = -1

    End Sub

    Private Sub MontaICMS()

        _ICMS = New ICMS

        With Me.dgvICMS
            .DataSource = _ICMS.SelecionaICMS
            .Columns(0).HeaderText = "UF ORIGEM"
            .Columns(1).HeaderText = "UF DESTINO"
            .Columns(2).HeaderText = "ICMS"
            .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .RowTemplate.Height = 16
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        End With

    End Sub

    Private Sub GravaICMS()

        _ICMS = New ICMS

        _ICMS.Action = _actionForm
        _ICMS.CodUfOrigem = Me.cboUForigem.SelectedValue
        _ICMS.CodUfDestino = Me.cboUFdestino.SelectedValue
        _ICMS.ICMS = Me.txtICMS.Text.Trim

        Try
            ' grava os dados no banco
            _ICMS.GravaICMS()
            ' desabilita o validador
            Call DesabilitaErrorProvider()
            ' limpa os objetos
            userInterface.CleanFields(Me)
            ' habilita os objetos q estavam enable = false
            userInterface.HabilitaObjeto(Me)
            ' muda a a��o do form para Inserir
            _actionForm = userInterface.Action.Inserir
            ' habilita a flag Enable
            _shiftObject = True
        Catch ex As Exception
            MessageBox.Show("Erro ..." & vbCrLf & ex.Message)
            userInterface.GravarLogErro(ex)
        End Try

    End Sub

    Private Sub txtICMS_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtICMS.KeyPress
        If Not Char.IsNumber(e.KeyChar) And Not e.KeyChar = vbBack And Not e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click

        _validator = True

        If Me.cboUForigem.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboUForigem, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.cboUFdestino.SelectedIndex = -1 Then
            Me.ErrorProvider1.SetError(Me.cboUFdestino, "Campo Obrigat�rio")
            _validator = False
        End If

        If Me.txtICMS.Text = "" Then
            Me.ErrorProvider1.SetError(Me.txtICMS, "Campo Obrigat�rio")
            _validator = False
        End If

        If _validator = True Then
            Call GravaICMS()
            Call MontaICMS()
        End If

    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click

        If (MessageBox.Show("Deseja excluir esse Registro ?", "Nome da Janela", MessageBoxButtons.YesNo)) = DialogResult.Yes Then
            ' verifica se existe item selecionado
            If _shiftObject = False Then
                _actionForm = userInterface.Action.Deletar
                Call GravaICMS()
                Call MontaICMS()
            Else
                MessageBox.Show("Favor selecionar um registro para excluir !")
            End If
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub

    Private Sub DesabilitaErrorProvider()

        Me.ErrorProvider1.SetError(Me.cboUForigem, "")
        Me.ErrorProvider1.SetError(Me.cboUFdestino, "")
        Me.ErrorProvider1.SetError(Me.txtICMS, "")

    End Sub

    Private Sub dgvICMS_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvICMS.CellClick

        If e.RowIndex > -1 Then

            Dim strUFOrigem As String = Me.dgvICMS.Rows(e.RowIndex).Cells(0).Value
            Dim strUfDestino As String = Me.dgvICMS.Rows(e.RowIndex).Cells(1).Value
            Dim intICMS As Decimal = Me.dgvICMS.Rows(e.RowIndex).Cells(2).Value

            Me.cboUForigem.SelectedValue = strUFOrigem
            Me.cboUFdestino.SelectedValue = strUfDestino
            Me.txtICMS.Text = intICMS

            Me.cboUForigem.Enabled = False
            Me.cboUFdestino.Enabled = False

            ' desabilita a flag Enable
            _shiftObject = False

            ' muda a��o do form
            _actionForm = userInterface.Action.Editar

            ' desabilita o errorProvider
            Call DesabilitaErrorProvider()

        End If

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click

        Call DesabilitaErrorProvider()
        userInterface.HabilitaObjeto(Me)
        userInterface.CleanFields(Me)
        _actionForm = userInterface.Action.Inserir
        _shiftObject = True

    End Sub
End Class