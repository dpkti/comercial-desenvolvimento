<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDIParent1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIParent1))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip
        Me.HelpMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.ContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator
        Me.Dist�nciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Regi�oToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.RedespachoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ICMSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PrazoPagamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FreteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FreteM�nimoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FreteXDist�nciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FreteXPesoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ContratoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ContratoPorNotaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ContratoPorRotaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpMenu, Me.FreteToolStripMenuItem, Me.ContratoToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(632, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'HelpMenu
        '
        Me.HelpMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContentsToolStripMenuItem, Me.ToolStripSeparator8, Me.Dist�nciaToolStripMenuItem, Me.Regi�oToolStripMenuItem, Me.RedespachoToolStripMenuItem, Me.ICMSToolStripMenuItem, Me.PrazoPagamentoToolStripMenuItem})
        Me.HelpMenu.Name = "HelpMenu"
        Me.HelpMenu.Size = New System.Drawing.Size(63, 20)
        Me.HelpMenu.Text = "Cadastro"
        '
        'ContentsToolStripMenuItem
        '
        Me.ContentsToolStripMenuItem.Image = CType(resources.GetObject("ContentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ContentsToolStripMenuItem.Name = "ContentsToolStripMenuItem"
        Me.ContentsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.T), System.Windows.Forms.Keys)
        Me.ContentsToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ContentsToolStripMenuItem.Text = "Transportadora"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(204, 6)
        '
        'Dist�nciaToolStripMenuItem
        '
        Me.Dist�nciaToolStripMenuItem.Image = CType(resources.GetObject("Dist�nciaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.Dist�nciaToolStripMenuItem.Name = "Dist�nciaToolStripMenuItem"
        Me.Dist�nciaToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.D), System.Windows.Forms.Keys)
        Me.Dist�nciaToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.Dist�nciaToolStripMenuItem.Text = "Dist�ncia"
        '
        'Regi�oToolStripMenuItem
        '
        Me.Regi�oToolStripMenuItem.Image = CType(resources.GetObject("Regi�oToolStripMenuItem.Image"), System.Drawing.Image)
        Me.Regi�oToolStripMenuItem.Name = "Regi�oToolStripMenuItem"
        Me.Regi�oToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.R), System.Windows.Forms.Keys)
        Me.Regi�oToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.Regi�oToolStripMenuItem.Text = "Regi�o"
        '
        'RedespachoToolStripMenuItem
        '
        Me.RedespachoToolStripMenuItem.Image = CType(resources.GetObject("RedespachoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RedespachoToolStripMenuItem.Name = "RedespachoToolStripMenuItem"
        Me.RedespachoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.RedespachoToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.RedespachoToolStripMenuItem.Text = "Redespacho"
        '
        'ICMSToolStripMenuItem
        '
        Me.ICMSToolStripMenuItem.Image = CType(resources.GetObject("ICMSToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ICMSToolStripMenuItem.Name = "ICMSToolStripMenuItem"
        Me.ICMSToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.I), System.Windows.Forms.Keys)
        Me.ICMSToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.ICMSToolStripMenuItem.Text = "ICMS"
        '
        'PrazoPagamentoToolStripMenuItem
        '
        Me.PrazoPagamentoToolStripMenuItem.Image = CType(resources.GetObject("PrazoPagamentoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrazoPagamentoToolStripMenuItem.Name = "PrazoPagamentoToolStripMenuItem"
        Me.PrazoPagamentoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.PrazoPagamentoToolStripMenuItem.Size = New System.Drawing.Size(207, 22)
        Me.PrazoPagamentoToolStripMenuItem.Text = "Prazo Pagamento"
        '
        'FreteToolStripMenuItem
        '
        Me.FreteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FreteM�nimoToolStripMenuItem, Me.FreteXDist�nciaToolStripMenuItem, Me.FreteXPesoToolStripMenuItem})
        Me.FreteToolStripMenuItem.Name = "FreteToolStripMenuItem"
        Me.FreteToolStripMenuItem.Size = New System.Drawing.Size(45, 20)
        Me.FreteToolStripMenuItem.Text = "Frete"
        '
        'FreteM�nimoToolStripMenuItem
        '
        Me.FreteM�nimoToolStripMenuItem.Image = CType(resources.GetObject("FreteM�nimoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FreteM�nimoToolStripMenuItem.Name = "FreteM�nimoToolStripMenuItem"
        Me.FreteM�nimoToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.FreteM�nimoToolStripMenuItem.Text = "Frete M�nimo"
        '
        'FreteXDist�nciaToolStripMenuItem
        '
        Me.FreteXDist�nciaToolStripMenuItem.Image = CType(resources.GetObject("FreteXDist�nciaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FreteXDist�nciaToolStripMenuItem.Name = "FreteXDist�nciaToolStripMenuItem"
        Me.FreteXDist�nciaToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.FreteXDist�nciaToolStripMenuItem.Text = "Frete x Dist�ncia"
        '
        'FreteXPesoToolStripMenuItem
        '
        Me.FreteXPesoToolStripMenuItem.Image = CType(resources.GetObject("FreteXPesoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FreteXPesoToolStripMenuItem.Name = "FreteXPesoToolStripMenuItem"
        Me.FreteXPesoToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.FreteXPesoToolStripMenuItem.Text = "Frete x Peso"
        '
        'ContratoToolStripMenuItem
        '
        Me.ContratoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ContratoPorNotaToolStripMenuItem, Me.ContratoPorRotaToolStripMenuItem})
        Me.ContratoToolStripMenuItem.Name = "ContratoToolStripMenuItem"
        Me.ContratoToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.ContratoToolStripMenuItem.Text = "Contrato"
        '
        'ContratoPorNotaToolStripMenuItem
        '
        Me.ContratoPorNotaToolStripMenuItem.Image = CType(resources.GetObject("ContratoPorNotaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ContratoPorNotaToolStripMenuItem.Name = "ContratoPorNotaToolStripMenuItem"
        Me.ContratoPorNotaToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.ContratoPorNotaToolStripMenuItem.Text = "Contrato por Nota"
        '
        'ContratoPorRotaToolStripMenuItem
        '
        Me.ContratoPorRotaToolStripMenuItem.Image = CType(resources.GetObject("ContratoPorRotaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ContratoPorRotaToolStripMenuItem.Name = "ContratoPorRotaToolStripMenuItem"
        Me.ContratoPorRotaToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.ContratoPorRotaToolStripMenuItem.Text = "Contrato por Rota"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(632, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(38, 17)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'MDIParent1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 453)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "MDIParent1"
        Me.Text = "SFI - Sistema de Frete Ideal"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ContentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents FreteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Dist�nciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Regi�oToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedespachoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ICMSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FreteM�nimoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FreteXDist�nciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FreteXPesoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoPorNotaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratoPorRotaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrazoPagamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
