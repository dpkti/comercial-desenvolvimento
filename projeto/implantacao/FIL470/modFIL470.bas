Attribute VB_Name = "modFIL470"
Option Explicit

Public vInicio As String


Public vCaminho_Executaveis As String
Public vCaminho_Controles As String
Public vCaminho_Genericas As String
Public vCaminho_Gerar_Genericas As String

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)


'**************************************
'Windows API/Global Declarations for :Ge
'     t Version Number for EXE, DLL or OCX fil
'     es
'**************************************
Public Declare Function GetFileVersionInfo Lib "Version.dll" Alias "GetFileVersionInfoA" (ByVal lptstrFilename As String, ByVal dwhandle As Long, ByVal dwlen As Long, lpData As Any) As Long
Public Declare Function GetFileVersionInfoSize Lib "Version.dll" Alias "GetFileVersionInfoSizeA" (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Public Declare Function VerQueryValue Lib "Version.dll" Alias "VerQueryValueA" (pBlock As Any, ByVal lpSubBlock As String, lplpBuffer As Any, puLen As Long) As Long
Public Declare Sub MoveMemory Lib "kernel32" Alias "RtlMoveMemory" (dest As Any, ByVal Source As Long, ByVal Length As Long)
Public Declare Function lstrcpy Lib "kernel32" Alias "lstrcpyA" (ByVal lpString1 As String, ByVal lpString2 As Long) As Long

Public Type FILEINFO
    CompanyName As String
    FileDescription As String
    FileVersion As String
    InternalName As String
    LegalCopyright As String
    OriginalFileName As String
    ProductName As String
    ProductVersion As String
End Type

Public Enum VerisonReturnValue
    eOK = 1
    eNoVersion = 2
End Enum

'*********************************************


#If Win32 Then
    Public Const HWND_TOPMOST& = -1
#Else
    Public Const HWND_TOPMOST& = -1
#End If 'WIN32

#If Win32 Then
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#Else
    Const SWP_NOMOVE& = &H2
    Const SWP_NOSIZE& = &H1
#End If 'WIN32

#If Win32 Then
    Declare Function SetWindowPos& Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)
#Else
    Declare Sub SetWindowPos Lib "user" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer)
#End If 'WIN32


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Copyright �1996-2006 VBnet, Randy Birch, All Rights Reserved.
' Some pages may also contain other copyrights by the author.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Distribution: You can freely use this code in your own
'               applications, but you may not reproduce
'               or publish this code on any web site,
'               online service, or distribute as source
'               on any media without express permission.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Const LVM_FIRST As Long = &H1000
Public Const LVM_SETCOLUMNWIDTH As Long = (LVM_FIRST + 30)
Public Const LVSCW_AUTOSIZE As Long = -1
Public Const LVSCW_AUTOSIZE_USEHEADER As Long = -2
    
Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2
    

Public vBanco As Object
Public vSessao As Object

Public Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hwnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long


'---- LISTVIEW - INICIO
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Copyright �1996-2006 VBnet, Randy Birch, All Rights Reserved.
' Some pages may also contain other copyrights by the author.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Distribution: You can freely use this code in your own
'               applications, but you may not reproduce
'               or publish this code on any web site,
'               online service, or distribute as source
'               on any media without express permission.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'listview, header
Public Const ICC_LISTVIEW_CLASSES  As Long = &H1

Public Type tagINITCOMMONCONTROLSEX  'icc
   dwSize As Long                    'size of this structure
   dwICC As Long                     'which classes to be initialized
End Type

Public Declare Sub InitCommonControls Lib "comctl32.dll" ()

Public Declare Function InitCommonControlsEx Lib "comctl32.dll" _
   (lpInitCtrls As tagINITCOMMONCONTROLSEX) As Boolean


Public Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    'Returns info from an INI file
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "Urvaz System": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Public Function AddToINI(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal KeyValue As String) As Integer
    'Add info to an INI file
    'Function returns 1 if successful and 0 if unsuccessful
    WritePrivateProfileString Section, Key, KeyValue, FileName
    AddToINI = 1
End Function


'Returns True if the current working version of Comctl32.dll
'supports IE3 styles & msgs. Returns False if old version.
'Also ensures that the Comctl32.dll library is loaded for use.
Public Function IsNewComctl32(dwFlags As Long) As Boolean

   Dim icc As tagINITCOMMONCONTROLSEX
   
   On Error GoTo Err_InitOldVersion
   
   icc.dwSize = Len(icc)
   icc.dwICC = dwFlags
   
   'VB will generate error 453 "Specified DLL function not found"
   'here if the new version isn't installed.
   IsNewComctl32 = InitCommonControlsEx(icc)
   
   Exit Function
   
Err_InitOldVersion:
   
   InitCommonControls
   
End Function
'---- LISTVIEW


Public Sub Ajustar_largura_lsv(lsv As MSComctlLib.ListView)

  'Size each column based on the maximum of
  'EITHER the column header text width, or,
  'if the items below it are wider, the
  'widest list item in the column.
  '
  'The last column is always resized to occupy
  'the remaining width in the control.
  
   Dim col2adjust As Long

   For col2adjust = 0 To lsv.ColumnHeaders.Count - 1
   
      Call SendMessage(lsv.hwnd, _
                       LVM_SETCOLUMNWIDTH, _
                       col2adjust, _
                       ByVal LVSCW_AUTOSIZE_USEHEADER)
   
      'Call SendMessage(lsv.hwnd, _
                       LVM_SETCOLUMNWIDTH, _
                       col2adjust, _
                       ByVal LVSCW_AUTOSIZE_USEHEADER)
   Next
   
   
End Sub

Sub Data(ByRef KeyAscii, ByRef txtCampo As Control)
1         On Error GoTo TrataErro

          Dim bTam As Byte    'tamanho do campo JA digitado
          Dim strData As String
          Dim bKey As Byte
2         bTam = Len(txtCampo)

3         If KeyAscii = 8 And bTam > 0 Then 'backspace
4             If Mid$(txtCampo.Text, bTam) = "/" Then
5                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
6             Else
7                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
8             End If

9         ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
10            If bTam = 1 Then
11                strData = txtCampo.Text & Chr$(KeyAscii)
12                If CInt(strData) < 1 Or CInt(strData > 31) Then
13                    Beep
14                Else
15                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
16                End If

17            ElseIf bTam = 4 Then
18                strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
19                If CInt(strData) < 1 Or CInt(strData > 12) Then
20                    Beep
21                Else
22                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
23                End If

24            ElseIf bTam = 7 Then
25                strData = Mid$(txtCampo.Text, 1, 2)     'dia
26                If CInt(strData) < 1 Or CInt(strData > 31) Then
27                    Beep
28                Else
29                    strData = Mid$(txtCampo.Text, 4, 2)     'mes
30                    If CInt(strData) < 1 Or CInt(strData > 12) Then
31                        Beep
32                    Else
33                        strData = txtCampo.Text & Chr$(KeyAscii)
34                        If Not IsDate(CDate(strData)) Then
35                            Beep
36                        Else
37                            txtCampo.Text = strData
38                        End If
39                    End If
40                End If

41            ElseIf bTam < 8 Then
42                bKey = KeyAscii

43            Else
44                bKey = 0
45            End If
46        Else
47            Beep
48        End If

49        SendKeys "{END}"
50        KeyAscii = bKey
51        Exit Sub

TrataErro:

52        If Err.Number = 13 Then
53            MsgBox strData, vbInformation, "Data Inv�lida"
54            KeyAscii = 0
55            Beep
56            Err.Clear
57        ElseIf Err.Number <> 0 Then
58            MsgBox "Sub Data" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
59        End If

End Sub

Public Function GetFileVersionInformation(ByRef pstrFieName As String, ByRef tFileInfo As FILEINFO) As VerisonReturnValue
    Dim lBufferLen As Long, lDummy As Long
    Dim sBuffer() As Byte
    Dim lVerPointer As Long
    Dim lRet As Long
    Dim Lang_Charset_String As String
    Dim HexNumber As Long
    Dim i As Integer
    Dim strTemp As String
    'Clear the Buffer tFileInfo
    tFileInfo.CompanyName = ""
    tFileInfo.FileDescription = ""
    tFileInfo.FileVersion = ""
    tFileInfo.InternalName = ""
    tFileInfo.LegalCopyright = ""
    tFileInfo.OriginalFileName = ""
    tFileInfo.ProductName = ""
    tFileInfo.ProductVersion = ""
    lBufferLen = GetFileVersionInfoSize(pstrFieName, lDummy)


    If lBufferLen < 1 Then
        GetFileVersionInformation = eNoVersion
        Exit Function
    End If
    ReDim sBuffer(lBufferLen)
    lRet = GetFileVersionInfo(pstrFieName, 0&, lBufferLen, sBuffer(0))


    If lRet = 0 Then
        GetFileVersionInformation = eNoVersion
        Exit Function
    End If
    lRet = VerQueryValue(sBuffer(0), "\VarFileInfo\Translation", lVerPointer, lBufferLen)


    If lRet = 0 Then
        GetFileVersionInformation = eNoVersion
        Exit Function
    End If
    Dim bytebuffer(255) As Byte
    MoveMemory bytebuffer(0), lVerPointer, lBufferLen
    HexNumber = bytebuffer(2) + bytebuffer(3) * &H100 + bytebuffer(0) * &H10000 + bytebuffer(1) * &H1000000
    Lang_Charset_String = Hex(HexNumber)
    'Pull it all apart:
    '04------= SUBLANG_ENGLISH_USA
    '--09----= LANG_ENGLISH
    ' ----04E4 = 1252 = Codepage for Windows
    '     :Multilingual


    Do While Len(Lang_Charset_String) < 8
        Lang_Charset_String = "0" & Lang_Charset_String
    Loop
    Dim strVersionInfo(7) As String
    strVersionInfo(0) = "CompanyName"
    strVersionInfo(1) = "FileDescription"
    strVersionInfo(2) = "FileVersion"
    strVersionInfo(3) = "InternalName"
    strVersionInfo(4) = "LegalCopyright"
    strVersionInfo(5) = "OriginalFileName"
    strVersionInfo(6) = "ProductName"
    strVersionInfo(7) = "ProductVersion"
    Dim buffer As String


    For i = 0 To 7
        buffer = String(255, 0)
        strTemp = "\StringFileInfo\" & Lang_Charset_String _
        & "\" & strVersionInfo(i)
        lRet = VerQueryValue(sBuffer(0), strTemp, _
        lVerPointer, lBufferLen)


        If lRet = 0 Then
            GetFileVersionInformation = eNoVersion
            'Exit Function
        End If
        lstrcpy buffer, lVerPointer
        buffer = Mid$(buffer, 1, InStr(buffer, vbNullChar) - 1)


        Select Case i
            Case 0
            tFileInfo.CompanyName = buffer
            Case 1
            tFileInfo.FileDescription = buffer
            Case 2
            tFileInfo.FileVersion = buffer
            Case 3
            tFileInfo.InternalName = buffer
            Case 4
            tFileInfo.LegalCopyright = buffer
            Case 5
            tFileInfo.OriginalFileName = buffer
            Case 6
            tFileInfo.ProductName = buffer
            Case 7
            tFileInfo.ProductVersion = buffer
        End Select
Next i
GetFileVersionInformation = eOK
End Function


