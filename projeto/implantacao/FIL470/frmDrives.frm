VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmDrives 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Drives"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3015
   ControlBox      =   0   'False
   Icon            =   "frmDrives.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   165
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   201
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame4 
      Caption         =   "Drives:"
      Height          =   1455
      Left            =   60
      TabIndex        =   2
      Top             =   930
      Width           =   2865
      Begin VB.TextBox TxtDrivePastaSistemasPAAC 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   150
         MaxLength       =   1
         TabIndex        =   4
         Top             =   480
         Width           =   585
      End
      Begin VB.TextBox txtDrivePastaIN 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   150
         MaxLength       =   1
         TabIndex        =   3
         Top             =   1050
         Width           =   585
      End
      Begin VB.Label Label19 
         AutoSize        =   -1  'True
         Caption         =   "Drive da pasta SISTEMAS\PAACs:"
         Height          =   195
         Left            =   150
         TabIndex        =   6
         Top             =   270
         Width           =   2505
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "Drive da pasta IN\CONTROLE:"
         Height          =   195
         Left            =   150
         TabIndex        =   5
         Top             =   840
         Width           =   2250
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDrives.frx":23D2
      PICN            =   "frmDrives.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDrives"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()
    If Trim(txtDrivePastaIN) = "" Then
        MsgBox "Informe o Drive da Pasta IN.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If Trim(TxtDrivePastaSistemasPAAC) = "" Then
        MsgBox "Informe o Drive da Pasta Sistemas\PAAC.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    AddToINI "C:\FIL470.INI", "DRIVE", "SISTEMAS_PAACS_EXECUTAVEIS", TxtDrivePastaSistemasPAAC
    AddToINI "C:\FIL470.INI", "DRIVE", "IN_CONTROLE", txtDrivePastaIN
    
    vDriveIN_CONTROLE = GetKeyVal("C:\FIL470.INI", "DRIVE", "IN_CONTROLE")
    vDriveSISTEMAS_PAACS_EXECUTAVEIS = GetKeyVal("C:\FIL470.INI", "DRIVE", "SISTEMAS_PAACS_EXECUTAVEIS")
    
    Unload Me

End Sub
Private Sub Form_Load()
    
    txtDrivePastaIN = GetKeyVal("C:\FIL470.INI", "DRIVE", "IN_CONTROLE")
    TxtDrivePastaSistemasPAAC = GetKeyVal("C:\FIL470.INI", "DRIVE", "SISTEMAS_PAACS_EXECUTAVEIS")
    
End Sub
