VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Begin VB.Form frmFIL470 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIL470 - Monitoramento de PAACs e Representantes"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14655
   Icon            =   "frmFIL470.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8205
   ScaleWidth      =   14655
   WindowState     =   1  'Minimized
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   6840
      Top             =   3570
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin VB.Timer Timer_Envio 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   10890
      Top             =   60
   End
   Begin VB.Data DataInsert 
      Caption         =   "DataInsert"
      Connect         =   "dBASE IV;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   360
      Left            =   4440
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   240
      Visible         =   0   'False
      Width           =   1755
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   12360
      Top             =   90
   End
   Begin VB.TextBox txtData 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   9060
      TabIndex        =   14
      Top             =   390
      Width           =   1425
   End
   Begin VB.FileListBox File1 
      Height          =   480
      Left            =   6870
      Pattern         =   "*.TXT"
      TabIndex        =   5
      Top             =   210
      Visible         =   0   'False
      Width           =   1305
   End
   Begin TabDlg.SSTab TabControle 
      Height          =   7005
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   14565
      _ExtentX        =   25691
      _ExtentY        =   12356
      _Version        =   393216
      Tabs            =   13
      Tab             =   12
      TabsPerRow      =   9
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tab - PAACs"
      TabPicture(0)   =   "frmFIL470.frx":23D2
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "msfControlePaac"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tab - Repres"
      TabPicture(1)   =   "frmFIL470.frx":23EE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "msfControleRepres"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Vers�es - PAACs"
      TabPicture(2)   =   "frmFIL470.frx":240A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "msfControleVersoesPaac"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Vers�es - Repres"
      TabPicture(3)   =   "frmFIL470.frx":2426
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "msfControleVersoesRepres"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Erros de Atual."
      TabPicture(4)   =   "frmFIL470.frx":2442
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "lsvErrosPAAC"
      Tab(4).Control(1)=   "lsvErrosRepres"
      Tab(4).Control(2)=   "lbltotalRepresErro"
      Tab(4).Control(3)=   "lblRepresComProblemas"
      Tab(4).Control(4)=   "lbltotalPaacsErro"
      Tab(4).Control(5)=   "lblPAACComProblemas"
      Tab(4).Control(6)=   "Label3"
      Tab(4).Control(7)=   "Label2"
      Tab(4).ControlCount=   8
      TabCaption(5)   =   "Vers�o"
      TabPicture(5)   =   "frmFIL470.frx":245E
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "txtCaminhoExecutaveis"
      Tab(5).Control(1)=   "Frame2"
      Tab(5).Control(2)=   "Frame1"
      Tab(5).Control(3)=   "lsvVersao"
      Tab(5).Control(4)=   "Label12"
      Tab(5).Control(5)=   "Label7"
      Tab(5).ControlCount=   6
      TabCaption(6)   =   "Gerar Qtde"
      TabPicture(6)   =   "frmFIL470.frx":247A
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "txtCodPaacRepres"
      Tab(6).Control(1)=   "lstRepresentantes"
      Tab(6).Control(2)=   "lstPAACs"
      Tab(6).Control(3)=   "pb"
      Tab(6).Control(4)=   "lstGenericas"
      Tab(6).Control(5)=   "lstVDR"
      Tab(6).Control(6)=   "lstEspecificas"
      Tab(6).Control(7)=   "lsvTempos"
      Tab(6).Control(8)=   "cmdGenericas"
      Tab(6).Control(9)=   "cmdEspecificas"
      Tab(6).Control(10)=   "cmdVDR"
      Tab(6).Control(11)=   "cmdGerar"
      Tab(6).Control(12)=   "cmdMarcarPaac"
      Tab(6).Control(13)=   "cmdDesmarcarPAAC"
      Tab(6).Control(14)=   "cmdMarcarRepres"
      Tab(6).Control(15)=   "cmdDesmarcarREpres"
      Tab(6).Control(16)=   "Label10"
      Tab(6).Control(17)=   "Label9"
      Tab(6).ControlCount=   18
      TabCaption(7)   =   "Espec�ficas"
      TabPicture(7)   =   "frmFIL470.frx":2496
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "txtDtGeracao"
      Tab(7).Control(1)=   "txtDtAtualizacao"
      Tab(7).Control(2)=   "cmdGerarEspecifica"
      Tab(7).Control(3)=   "txtCodPaacRepresEspecifica"
      Tab(7).Control(4)=   "lsvEspecificas"
      Tab(7).Control(5)=   "Label15"
      Tab(7).Control(6)=   "Label14"
      Tab(7).Control(7)=   "Label13"
      Tab(7).ControlCount=   8
      TabCaption(8)   =   "Config"
      TabPicture(8)   =   "frmFIL470.frx":24B2
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "cmdGerarCarga"
      Tab(8).Control(1)=   "Frame3"
      Tab(8).Control(2)=   "txtHorario"
      Tab(8).Control(3)=   "cmdGravarEnvio"
      Tab(8).Control(4)=   "Label19"
      Tab(8).Control(5)=   "Label18"
      Tab(8).Control(6)=   "Label1"
      Tab(8).ControlCount=   7
      TabCaption(9)   =   "Indice - PAACs"
      TabPicture(9)   =   "frmFIL470.frx":24CE
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "msfIndicePaac"
      Tab(9).ControlCount=   1
      TabCaption(10)  =   "Indice - Repres"
      TabPicture(10)  =   "frmFIL470.frx":24EA
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "msfIndiceRepres"
      Tab(10).ControlCount=   1
      TabCaption(11)  =   "N� Tabelas"
      TabPicture(11)  =   "frmFIL470.frx":2506
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "Label20"
      Tab(11).Control(1)=   "lsvNumTabelas"
      Tab(11).ControlCount=   2
      TabCaption(12)  =   "Base Desat."
      TabPicture(12)  =   "frmFIL470.frx":2522
      Tab(12).ControlEnabled=   -1  'True
      Tab(12).Control(0)=   "msfBaseDesat"
      Tab(12).Control(0).Enabled=   0   'False
      Tab(12).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid msfBaseDesat 
         Height          =   3735
         Left            =   120
         TabIndex        =   90
         Top             =   840
         Width           =   10935
         _ExtentX        =   19288
         _ExtentY        =   6588
         _Version        =   393216
         Cols            =   4
      End
      Begin MSComctlLib.ListView lsvNumTabelas 
         Height          =   5955
         Left            =   -74880
         TabIndex        =   88
         Top             =   930
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   10504
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Codigo"
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "N� Tabelas"
            Object.Width           =   2540
         EndProperty
      End
      Begin Bot�o.cmd cmdGerarCarga 
         Height          =   495
         Left            =   -71730
         TabIndex        =   83
         ToolTipText     =   "Este bot�o ir� apagar as cargas e recri�-las"
         Top             =   3270
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "For�ar Gera��o de Cargas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":253E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Frame Frame3 
         Height          =   5385
         Left            =   -74880
         TabIndex        =   71
         Top             =   1440
         Width           =   3045
         Begin VB.ListBox lstPAACEnviar 
            Appearance      =   0  'Flat
            Height          =   4530
            ItemData        =   "frmFIL470.frx":255A
            Left            =   90
            List            =   "frmFIL470.frx":255C
            Style           =   1  'Checkbox
            TabIndex        =   73
            Top             =   345
            Width           =   1245
         End
         Begin VB.ListBox lstRepresEnviar 
            Appearance      =   0  'Flat
            Height          =   4530
            ItemData        =   "frmFIL470.frx":255E
            Left            =   1590
            List            =   "frmFIL470.frx":2560
            Style           =   1  'Checkbox
            TabIndex        =   72
            Top             =   360
            Width           =   1245
         End
         Begin Bot�o.cmd cmdMarcarTodosPaacsEnvio 
            Height          =   285
            Left            =   90
            TabIndex        =   77
            ToolTipText     =   "Gerar para o PAAC/Repres informado"
            Top             =   4950
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   503
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":2562
            PICN            =   "frmFIL470.frx":257E
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdDesmarcarTodosPaacsEnvio 
            Height          =   285
            Left            =   810
            TabIndex        =   78
            ToolTipText     =   "Desmarcar"
            Top             =   4950
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   503
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":2B18
            PICN            =   "frmFIL470.frx":2B34
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdMarcarTodosRepresEnvio 
            Height          =   285
            Left            =   1590
            TabIndex        =   79
            ToolTipText     =   "Gerar para o PAAC/Repres informado"
            Top             =   4950
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   503
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":30CE
            PICN            =   "frmFIL470.frx":30EA
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdDesmarcarTodosRepresEnvio 
            Height          =   285
            Left            =   2310
            TabIndex        =   80
            ToolTipText     =   "Desmarcar"
            Top             =   4950
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   503
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   -1  'True
            BCOL            =   13160660
            BCOLO           =   13160660
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":3684
            PICN            =   "frmFIL470.frx":36A0
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   0
            NGREY           =   0   'False
            FX              =   0
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label17 
            AutoSize        =   -1  'True
            Caption         =   "Representantes"
            Height          =   195
            Left            =   1590
            TabIndex        =   75
            Top             =   150
            Width           =   1125
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "PAAC�s"
            Height          =   195
            Left            =   90
            TabIndex        =   74
            Top             =   150
            Width           =   540
         End
      End
      Begin VB.TextBox txtHorario 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   -72720
         TabIndex        =   69
         Top             =   930
         Width           =   915
      End
      Begin MSFlexGridLib.MSFlexGrid msfControlePaac 
         Height          =   6255
         Left            =   -74880
         TabIndex        =   65
         Top             =   630
         Width           =   14235
         _ExtentX        =   25109
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
      End
      Begin VB.TextBox txtDtGeracao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -71970
         Locked          =   -1  'True
         TabIndex        =   63
         Top             =   1050
         Width           =   1275
      End
      Begin VB.TextBox txtDtAtualizacao 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   -70470
         Locked          =   -1  'True
         TabIndex        =   60
         Top             =   1050
         Width           =   1275
      End
      Begin VB.CommandButton cmdGerarEspecifica 
         Caption         =   "Gerar"
         Height          =   315
         Left            =   -73890
         TabIndex        =   59
         Top             =   930
         Width           =   615
      End
      Begin VB.TextBox txtCodPaacRepresEspecifica 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   -74910
         TabIndex        =   58
         Top             =   930
         Width           =   945
      End
      Begin MSComctlLib.ListView lsvEspecificas 
         Height          =   5415
         Left            =   -74910
         TabIndex        =   57
         Top             =   1380
         Width           =   5715
         _ExtentX        =   10081
         _ExtentY        =   9551
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   0   'False
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Tabela"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Qtd Banco"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Qtd Repres"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.TextBox txtCaminhoExecutaveis 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   -74880
         TabIndex        =   55
         Text            =   "H:\SISTEMAS\PAACS\EXECUTAVEIS\"
         Top             =   5220
         Width           =   4515
      End
      Begin VB.Frame Frame2 
         Caption         =   "Enviar Email para:"
         Height          =   6015
         Left            =   -65520
         TabIndex        =   49
         Top             =   780
         Width           =   4785
         Begin VB.TextBox txtEmail 
            Appearance      =   0  'Flat
            Height          =   285
            Left            =   90
            TabIndex        =   53
            Top             =   480
            Width           =   4605
         End
         Begin VB.ListBox lstEmail 
            Appearance      =   0  'Flat
            Height          =   4320
            Left            =   60
            TabIndex        =   52
            Top             =   1650
            Width           =   4665
         End
         Begin Bot�o.cmd cmdSalvar 
            Height          =   495
            Left            =   60
            TabIndex        =   50
            TabStop         =   0   'False
            ToolTipText     =   "Salvar"
            Top             =   1140
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":3C3A
            PICN            =   "frmFIL470.frx":3C56
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdExcluirEmail 
            Height          =   495
            Left            =   600
            TabIndex        =   51
            TabStop         =   0   'False
            ToolTipText     =   "Excluir"
            Top             =   1140
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":41F0
            PICN            =   "frmFIL470.frx":420C
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "E-mail:"
            Height          =   195
            Left            =   90
            TabIndex        =   54
            Top             =   270
            Width           =   465
         End
      End
      Begin VB.TextBox txtCodPaacRepres 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   -72000
         TabIndex        =   44
         Top             =   4020
         Visible         =   0   'False
         Width           =   945
      End
      Begin VB.ListBox lstRepresentantes 
         Appearance      =   0  'Flat
         Height          =   3180
         ItemData        =   "frmFIL470.frx":47A6
         Left            =   -73410
         List            =   "frmFIL470.frx":47AD
         Style           =   1  'Checkbox
         TabIndex        =   41
         Top             =   3300
         Width           =   1245
      End
      Begin VB.ListBox lstPAACs 
         Appearance      =   0  'Flat
         Height          =   3180
         ItemData        =   "frmFIL470.frx":47BF
         Left            =   -74910
         List            =   "frmFIL470.frx":47C6
         Style           =   1  'Checkbox
         TabIndex        =   40
         Top             =   3285
         Width           =   1245
      End
      Begin MSComctlLib.ProgressBar pb 
         Height          =   165
         Left            =   -67620
         TabIndex        =   35
         Top             =   6360
         Width           =   6765
         _ExtentX        =   11933
         _ExtentY        =   291
         _Version        =   393216
         BorderStyle     =   1
         Appearance      =   0
         Scrolling       =   1
      End
      Begin VB.ListBox lstGenericas 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5205
         ItemData        =   "frmFIL470.frx":47D8
         Left            =   -67860
         List            =   "frmFIL470.frx":485A
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   34
         Top             =   1110
         Width           =   2325
      End
      Begin VB.ListBox lstVDR 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5205
         ItemData        =   "frmFIL470.frx":4AA8
         Left            =   -63090
         List            =   "frmFIL470.frx":4ABE
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   33
         Top             =   1110
         Width           =   2205
      End
      Begin VB.ListBox lstEspecificas 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5205
         ItemData        =   "frmFIL470.frx":4B3B
         Left            =   -65460
         List            =   "frmFIL470.frx":4B9C
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   32
         Top             =   1110
         Width           =   2325
      End
      Begin MSComctlLib.ListView lsvTempos 
         Height          =   2355
         Left            =   -74910
         TabIndex        =   31
         Top             =   690
         Width           =   6525
         _ExtentX        =   11509
         _ExtentY        =   4154
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "PAAC/Repres"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tabela"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Inicio"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Fim"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Tempo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Registros"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.Frame Frame1 
         Caption         =   "Cadastro de Vers�es"
         Height          =   2085
         Left            =   -68970
         TabIndex        =   22
         Top             =   900
         Visible         =   0   'False
         Width           =   3405
         Begin VB.ComboBox cboSoftware 
            Enabled         =   0   'False
            Height          =   315
            Left            =   150
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   390
            Width           =   3075
         End
         Begin VB.TextBox txtVersao 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            Height          =   315
            Left            =   150
            MaxLength       =   10
            TabIndex        =   23
            Top             =   990
            Width           =   1665
         End
         Begin Bot�o.cmd cmdGravar 
            Height          =   495
            Left            =   1260
            TabIndex        =   26
            TabStop         =   0   'False
            ToolTipText     =   "Salvar"
            Top             =   1440
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   0   'False
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":4D4D
            PICN            =   "frmFIL470.frx":4D69
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdLimpar 
            Height          =   495
            Left            =   1830
            TabIndex        =   27
            TabStop         =   0   'False
            ToolTipText     =   "Desfazer"
            Top             =   1440
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":5303
            PICN            =   "frmFIL470.frx":531F
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdNovo 
            Height          =   495
            Left            =   150
            TabIndex        =   28
            TabStop         =   0   'False
            ToolTipText     =   "Novo"
            Top             =   1440
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":5639
            PICN            =   "frmFIL470.frx":5655
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdExcluir 
            Height          =   495
            Left            =   2400
            TabIndex        =   29
            TabStop         =   0   'False
            ToolTipText     =   "Excluir"
            Top             =   1440
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   -1  'True
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":5B3B
            PICN            =   "frmFIL470.frx":5B57
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin Bot�o.cmd cmdAlterar 
            Height          =   495
            Left            =   720
            TabIndex        =   30
            TabStop         =   0   'False
            ToolTipText     =   "Alterar"
            Top             =   1440
            Width           =   495
            _ExtentX        =   873
            _ExtentY        =   873
            BTYPE           =   3
            TX              =   ""
            ENAB            =   0   'False
            BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            COLTYPE         =   1
            FOCUSR          =   0   'False
            BCOL            =   16777215
            BCOLO           =   12640511
            FCOL            =   0
            FCOLO           =   0
            MCOL            =   12632256
            MPTR            =   1
            MICON           =   "frmFIL470.frx":60F1
            PICN            =   "frmFIL470.frx":610D
            UMCOL           =   -1  'True
            SOFT            =   0   'False
            PICPOS          =   2
            NGREY           =   0   'False
            FX              =   3
            HAND            =   0   'False
            CHECK           =   0   'False
            VALUE           =   0   'False
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Vers�o:"
            Height          =   195
            Left            =   150
            TabIndex        =   24
            Top             =   780
            Width           =   540
         End
      End
      Begin MSComctlLib.ListView lsvVersao 
         Height          =   4005
         Left            =   -74880
         TabIndex        =   20
         Top             =   900
         Width           =   5835
         _ExtentX        =   10292
         _ExtentY        =   7064
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome Software"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   1
            Text            =   "Versao"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lsvErrosPAAC 
         Height          =   5745
         Left            =   -74910
         TabIndex        =   6
         Top             =   870
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   10134
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Paac"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tabela"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "Qtde Env"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Qtde Rec"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Qtde Base_Atu"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lsvErrosRepres 
         Height          =   5745
         Left            =   -67140
         TabIndex        =   7
         Top             =   870
         Width           =   5325
         _ExtentX        =   9393
         _ExtentY        =   10134
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Repres"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tabela"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   2
            Text            =   "Qtde Env"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   3
            Text            =   "Qtde Rec"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            SubItemIndex    =   4
            Text            =   "Qtde Base_Atu"
            Object.Width           =   2540
         EndProperty
      End
      Begin Bot�o.cmd cmdGenericas 
         Height          =   390
         Left            =   -67860
         TabIndex        =   36
         TabStop         =   0   'False
         ToolTipText     =   "Gerar"
         Top             =   690
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   "Gen�ricas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":66A7
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEspecificas 
         Height          =   390
         Left            =   -65490
         TabIndex        =   37
         TabStop         =   0   'False
         ToolTipText     =   "Gerar"
         Top             =   690
         Width           =   2325
         _ExtentX        =   4101
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   "Espec�ficas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":66C3
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdVDR 
         Height          =   390
         Left            =   -63120
         TabIndex        =   38
         TabStop         =   0   'False
         ToolTipText     =   "Gerar"
         Top             =   690
         Width           =   2235
         _ExtentX        =   3942
         _ExtentY        =   688
         BTYPE           =   3
         TX              =   "VDR"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":66DF
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGerar 
         Height          =   585
         Left            =   -72000
         TabIndex        =   39
         ToolTipText     =   "Gerar para o PAAC/Repres informado"
         Top             =   3270
         Width           =   2565
         _ExtentX        =   4524
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   "Gerar Para Item Selecionado"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":66FB
         PICN            =   "frmFIL470.frx":6717
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdMarcarPaac 
         Height          =   285
         Left            =   -74910
         TabIndex        =   45
         ToolTipText     =   "Gerar para o PAAC/Repres informado"
         Top             =   6480
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":73F1
         PICN            =   "frmFIL470.frx":740D
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesmarcarPAAC 
         Height          =   285
         Left            =   -74190
         TabIndex        =   46
         ToolTipText     =   "Desmarcar"
         Top             =   6480
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":79A7
         PICN            =   "frmFIL470.frx":79C3
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdMarcarRepres 
         Height          =   285
         Left            =   -73410
         TabIndex        =   47
         ToolTipText     =   "Gerar para o PAAC/Repres informado"
         Top             =   6510
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":7F5D
         PICN            =   "frmFIL470.frx":7F79
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdDesmarcarREpres 
         Height          =   285
         Left            =   -72720
         TabIndex        =   48
         ToolTipText     =   "Desmarcar"
         Top             =   6510
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":8513
         PICN            =   "frmFIL470.frx":852F
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid msfControleRepres 
         Height          =   6255
         Left            =   -74940
         TabIndex        =   66
         Top             =   660
         Width           =   14235
         _ExtentX        =   25109
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
         SelectionMode   =   1
      End
      Begin MSFlexGridLib.MSFlexGrid msfControleVersoesPaac 
         Height          =   6255
         Left            =   -74940
         TabIndex        =   67
         Top             =   660
         Width           =   14235
         _ExtentX        =   25109
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
         SelectionMode   =   1
      End
      Begin MSFlexGridLib.MSFlexGrid msfControleVersoesRepres 
         Height          =   6255
         Left            =   -74940
         TabIndex        =   68
         Top             =   660
         Width           =   14205
         _ExtentX        =   25056
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
         SelectionMode   =   1
      End
      Begin Bot�o.cmd cmdGravarEnvio 
         Height          =   705
         Left            =   -71730
         TabIndex        =   81
         TabStop         =   0   'False
         ToolTipText     =   "Gravar"
         Top             =   750
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   1244
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmFIL470.frx":8AC9
         PICN            =   "frmFIL470.frx":8AE5
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSFlexGridLib.MSFlexGrid msfIndicePaac 
         Height          =   6255
         Left            =   -74910
         TabIndex        =   85
         Top             =   690
         Width           =   14235
         _ExtentX        =   25109
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
      End
      Begin MSFlexGridLib.MSFlexGrid msfIndiceRepres 
         Height          =   6255
         Left            =   -74910
         TabIndex        =   86
         Top             =   690
         Width           =   14235
         _ExtentX        =   25109
         _ExtentY        =   11033
         _Version        =   393216
         Rows            =   3
         Cols            =   120
         FixedRows       =   2
      End
      Begin VB.Label Label20 
         AutoSize        =   -1  'True
         Caption         =   "N� de Tabelas no BASE_DPK.MDB de cada PAAC/Representante"
         Height          =   195
         Left            =   -74880
         TabIndex        =   89
         Top             =   690
         Width           =   4755
      End
      Begin VB.Label Label19 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Clicando neste bot�o ele ir� refazer a carga para todos os PAACs e Repres selecionados."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   465
         Left            =   -71730
         TabIndex        =   84
         Top             =   3780
         Width           =   5235
      End
      Begin VB.Label Label18 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   1  'Fixed Single
         Caption         =   $"frmFIL470.frx":97BF
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1305
         Left            =   -71730
         TabIndex        =   76
         Top             =   1530
         Width           =   5235
      End
      Begin VB.Label Label1 
         Caption         =   "Hora para Envio das Cargas:"
         Height          =   285
         Left            =   -74820
         TabIndex        =   70
         Top             =   1020
         Width           =   2085
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Dt Gera��o Quant"
         Height          =   195
         Left            =   -71970
         TabIndex        =   64
         Top             =   840
         Width           =   1305
      End
      Begin VB.Label Label14 
         Caption         =   "C�d.Paac/Repres:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   62
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Data Atualizacao:"
         Height          =   195
         Left            =   -70470
         TabIndex        =   61
         Top             =   840
         Width           =   1260
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Caminho dos execut�veis"
         Height          =   195
         Left            =   -74880
         TabIndex        =   56
         Top             =   5010
         Width           =   1815
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "PAAC�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   -74880
         TabIndex        =   43
         Top             =   3090
         Width           =   645
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Representantes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   -73410
         TabIndex        =   42
         Top             =   3090
         Width           =   1350
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Vers�es:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   21
         Top             =   690
         Width           =   615
      End
      Begin VB.Label lbltotalRepresErro 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -64800
         TabIndex        =   13
         Top             =   6660
         Width           =   795
      End
      Begin VB.Label lblRepresComProblemas 
         AutoSize        =   -1  'True
         Caption         =   "Representantes com Problemas:"
         Height          =   195
         Left            =   -67140
         TabIndex        =   12
         Top             =   6660
         Width           =   2295
      End
      Begin VB.Label lbltotalPaacsErro 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   -73290
         TabIndex        =   11
         Top             =   6660
         Width           =   795
      End
      Begin VB.Label lblPAACComProblemas 
         AutoSize        =   -1  'True
         Caption         =   "PAAC com Problemas:"
         Height          =   195
         Left            =   -74910
         TabIndex        =   10
         Top             =   6660
         Width           =   1590
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Representantes:"
         Height          =   195
         Left            =   -67140
         TabIndex        =   9
         Top             =   660
         Width           =   1170
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "PAAC:"
         Height          =   195
         Left            =   -74940
         TabIndex        =   8
         Top             =   660
         Width           =   465
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   3
      Top             =   825
      Width           =   14565
      _ExtentX        =   25691
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   270
      Left            =   0
      TabIndex        =   0
      Top             =   7935
      Width           =   14655
      _ExtentX        =   25850
      _ExtentY        =   476
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13150
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "16/01/2012"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "17:08"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdSobre 
      Height          =   705
      Left            =   1350
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sobre"
      Top             =   45
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL470.frx":991D
      PICN            =   "frmFIL470.frx":9939
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   705
      Left            =   4380
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL470.frx":A613
      PICN            =   "frmFIL470.frx":A62F
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdRecarregar 
      Height          =   705
      Left            =   2070
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Atualizar"
      Top             =   60
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL470.frx":B309
      PICN            =   "frmFIL470.frx":B325
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDrives 
      Height          =   705
      Left            =   2820
      TabIndex        =   82
      TabStop         =   0   'False
      ToolTipText     =   "Drives"
      Top             =   60
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL470.frx":BFFF
      PICN            =   "frmFIL470.frx":C01B
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportarIndiceRepres 
      Height          =   705
      Left            =   3600
      TabIndex        =   87
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   60
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1244
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFIL470.frx":CCF5
      PICN            =   "frmFIL470.frx":CD11
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Data para Consulta:"
      Height          =   195
      Left            =   9060
      TabIndex        =   19
      Top             =   180
      Width           =   1410
   End
   Begin VB.Label lblTempo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "60"
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   12810
      TabIndex        =   18
      Top             =   540
      Width           =   315
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "minutos."
      Height          =   195
      Left            =   13140
      TabIndex        =   17
      Top             =   540
      Width           =   585
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Executar novamente em: "
      Height          =   195
      Left            =   10980
      TabIndex        =   16
      Top             =   540
      Width           =   1815
   End
   Begin VB.Image Image1 
      Height          =   885
      Left            =   -90
      Picture         =   "frmFIL470.frx":D9EB
      Stretch         =   -1  'True
      ToolTipText     =   "Acessar a Intranet"
      Top             =   -45
      Width           =   1155
   End
   Begin VB.Menu mnuVersoes 
      Caption         =   "Versoes"
      Visible         =   0   'False
      Begin VB.Menu mnuDiferencas 
         Caption         =   "Vers�es erradas"
      End
      Begin VB.Menu mnuTodas 
         Caption         =   "Todas"
      End
   End
End
Attribute VB_Name = "frmFIL470"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vTempo As Byte
Dim vNomeArquivo As String
'Dim vNomeControle As MSFlexGrid
Dim vCodFilial As Integer
Dim vBancoBatch As Object
Dim vSessaoBatch As Object

Dim vRstComp As Object 'Foi criado aqui porque nao estava conseguindo passar como parametro, dava erro
Dim vDriveIN_CONTROLE As String
Dim vDriveSISTEMAS_PAACS_EXECUTAVEIS As String

Sub atu_Indices(ByVal pCod_Paac_Repres As Integer, ByVal vTipo As String, ByVal vTabela As String, ByVal vStatus As String)
    On Error GoTo Trata_Erro

    vBanco.Parameters.Remove "PM_COD_PAAC_REPRES"
    vBanco.Parameters.Add "PM_COD_PAAC_REPRES", pCod_Paac_Repres, 1
    
    vBanco.Parameters.Remove "PM_TIPO"
    vBanco.Parameters.Add "PM_TIPO", vTipo, 1
    
    vBanco.Parameters.Remove "PM_TABLE"
    vBanco.Parameters.Add "PM_TABLE", vTabela, 1
    
    vBanco.Parameters.Remove "PM_STATUS_PK"
    vBanco.Parameters.Add "PM_STATUS_PK", vStatus, 1
    
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_ATUALIZA_STATUS(:PM_COD_PAAC_REPRES, :PM_TIPO, :PM_TABLE, :PM_STATUS_PK); END;"

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Atu_Indices - " & Err.Number & " - " & Err.Description
    End If
End Sub

Private Sub cmdDesmarcarPAAC_Click()
    Selecionar lstPAACs, False
End Sub

Private Sub cmdDesmarcarREpres_Click()
    Selecionar lstRepresentantes, False
End Sub

Private Sub cmdDesmarcarTodosPaacsEnvio_Click()
    Selecionar lstPAACEnviar, False
End Sub

Private Sub cmdDesmarcarTodosRepresEnvio_Click()
    Selecionar lstRepresEnviar, False
End Sub

Private Sub cmdDrives_Click()
    frmDrives.Show 1
End Sub

Private Sub cmdEspecificas_Click()
    For i = 0 To lstEspecificas.ListCount - 1
        lstEspecificas.Selected(i) = Not (lstEspecificas.Selected(i))
    Next
End Sub

Private Sub cmdExcluirEmail_Click()
    If Me.txtEmail = "" Then
        txtEmail.SetFocus
        MsgBox "Informe o E-mail a ser exclu�do.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    vBanco.ExecuteSQL "BEGIN DELETE FROM HELPDESK.PARAMETROS WHERE COD_SOFTWARE = 1270 AND NOME_PARAMETRO='EMAIL' AND VL_PARAMETRO='" & txtEmail & "'; COMMIT; END;"
    
    Preencher_Email
    
    txtEmail = ""

End Sub

Private Sub cmdExportarIndiceRepres_Click()

   Call GridExport(msfIndicePaac, "c:\IndicePAACs.txt")
   Call GridExport(msfIndiceRepres, "c:\IndiceRepres.txt")
   Call GridExport(msfControlePaac, "c:\TabPAACs.txt")
   Call GridExport(msfControleRepres, "c:\TabRepres.txt")
   Call GridExport(msfControleVersoesPaac, "c:\VersoesPAACs.txt")
   Call GridExport(msfControleVersoesRepres, "c:\VersoesRepres.txt")
   Call GridExportList
   
End Sub

Private Sub cmdGenericas_Click()
    For i = 0 To lstGenericas.ListCount - 1
        lstGenericas.Selected(i) = Not (lstGenericas.Selected(i))
    Next
End Sub

Private Sub cmdGerar_Click()
    Dim vPR As Integer
    
    If lstPAACs.SelCount = lstPAACs.ListCount And lstRepresentantes.SelCount = lstRepresentantes.ListCount Then
        If MsgBox("Confirma a gera��o para TODOS os PAACs e Representantes ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
            If MsgBox("Isso ir� demorar mais de 2 horas, Confirma ???", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
                If lstEspecificas.SelCount <> lstEspecificas.ListCount Then
                    MsgBox "Marque as Espec�ficas.", vbInformation, "Aten��o"
                    Exit Sub
                ElseIf Me.lstGenericas.SelCount <> lstGenericas.ListCount Then
                    MsgBox "Marque as Gen�ricas.", vbInformation, "Aten��o"
                    Exit Sub
                ElseIf Me.lstVDR.SelCount <> lstVDR.ListCount Then
                    MsgBox "Marque as VDRs.", vbInformation, "Aten��o"
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Else
            Exit Sub
        End If
    End If
    
    txtCodPaacRepres = ""
    
    For vPR = 0 To lstPAACs.ListCount - 1
        If lstPAACs.Selected(vPR) = True Then
            txtCodPaacRepres = lstPAACs.List(vPR)
            Gerar_Qtde
        End If
    Next
    
    For vPR = 0 To lstRepresentantes.ListCount - 1
        If lstRepresentantes.Selected(vPR) = True Then
            Me.txtCodPaacRepres = Me.lstPAACs.List(vPR)
            Gerar_Qtde
        End If
    Next

End Sub
'Comentado em 20/08/2007
'Habilitado em 08/02/2008 - Luciano
Private Sub cmdGerarCarga_Click()
    '****************************************************************************
    'Gerar Cargas para os PAACs e Repres que foram marcados ap�s a geracao normal
    'que � feita no load do sistema.
    '****************************************************************************
1     Me.Refresh

2     For i = 0 To lstPAACEnviar.ListCount - 1
3       If lstPAACEnviar.Selected(i) = True Then
4          Gerar_Cargas lstPAACEnviar.List(i), "P"
5       End If
6     Next
7     For i = 0 To lstRepresEnviar.ListCount - 1
8       If lstRepresEnviar.Selected(i) = True Then
9          Gerar_Cargas Me.lstRepresEnviar.List(i), "R"
10      End If
11    Next

End Sub

Private Sub cmdGerarEspecifica_Click()
    
    Dim vRst As Object
    Dim litem As ListItem
    
    If txtCodPaacRepresEspecifica = "" Then
        MsgBox "Informe o C�digo do Paac ou Representante", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Me.lsvEspecificas.ListItems.Clear

'QTDES DO BANCO
    Criar_Cursor "vCursor_Com"
    
    vBanco.Parameters.Remove "CodRepres"
    vBanco.Parameters.Add "CodRepres", txtCodPaacRepresEspecifica, 1
    
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_QTD_BANCO_ESPEC(:vCursor_Com, :CodRepres); END;"
    
    Set vRst = Nothing
    Set vRst = vBanco.Parameters("vCursor_Com").Value
    
    vBanco.Parameters.Remove "vCursor_Com"
    
    If vRst.RecordCount <= 0 Then
        MsgBox "Este Paac/Repres n�o possui dados, por favor gere os dados para ele.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Me.txtDtGeracao = vRst!dt_controle
    
    For i = 1 To vRst.RecordCount
        Set litem = lsvEspecificas.ListItems.Add
        litem = vRst!Tabela
        litem.SubItems(1) = vRst!REGISTROS
        vRst.MoveNext
    Next
    
    
'QTDES DO PAAC/REPRES
    Criar_Cursor "vCursor_Com"
    
    vBanco.Parameters.Remove "CodRepres"
    vBanco.Parameters.Add "CodRepres", txtCodPaacRepresEspecifica, 1
    
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_QTD_REPRES_ESPEC(:vCursor_Com, :CodRepres); END;"
    
    Set vRst = Nothing
    Set vRst = vBanco.Parameters("vCursor_Com").Value
    
    vBanco.Parameters.Remove "vCursor_Com"
    
    txtDtAtualizacao = vRst!dt_atualizacao
    
    For i = 1 To vRst.RecordCount
        For ii = 1 To lsvEspecificas.ListItems.Count
            If UCase(lsvEspecificas.ListItems(ii)) = UCase(vRst!Tabela) Then
                lsvEspecificas.ListItems(ii).SubItems(2) = vRst!REGISTROS
                If lsvEspecificas.ListItems(ii).SubItems(1) <> vRst!REGISTROS Then
                   lsvEspecificas.ListItems(ii).ForeColor = vbRed
                Else
                    lsvEspecificas.ListItems(ii).ForeColor = vbBlack
                End If
                Exit For
            End If
        Next
        vRst.MoveNext
    Next
End Sub

Private Sub cmdGravarEnvio_Click()
    PAAC_REPRES_ENVIAR
End Sub

Private Sub cmdMarcarPaac_Click()
    Selecionar lstPAACs, True
End Sub

Private Sub cmdMarcarRepres_Click()
    Selecionar lstRepresentantes, True
End Sub

Private Sub cmdMarcarTodosPaacsEnvio_Click()
    Selecionar Me.lstPAACEnviar, True
End Sub

Private Sub cmdMarcarTodosRepresEnvio_Click()
    Selecionar lstRepresEnviar, True
End Sub

Private Sub cmdRecarregar_Click()
    Me.Visible = False
    Form_Load
    Me.Visible = True
End Sub

Private Sub cmdSair_Click()
    End
End Sub

Sub Selecionar(pList As ListBox, pSel As Boolean)
    For i = 0 To pList.ListCount - 1
        pList.Selected(i) = pSel
    Next
End Sub

Private Sub cmdSalvar_Click()
    If txtEmail = "" Then
        MsgBox "Informe o E-mail.", vbInformation, "Aten��o"
        txtEmail.SetFocus
        Exit Sub
    End If
    vBanco.ExecuteSQL "BEGIN INSERT INTO HELPDESK.PARAMETROS VALUES (1270, 'EMAIL','" & txtEmail & "'); COMMIT; END;"
    
    Preencher_Email
    
    txtEmail = ""
    
End Sub

Private Sub cmdSobre_Click()
    frmSobre.Show 1
End Sub

Private Sub cmdVDR_Click()
    For i = 0 To lstVDR.ListCount - 1
        lstVDR.Selected(i) = Not (lstVDR.Selected(i))
    Next
End Sub

Private Sub Form_Activate()
    Me.Refresh
End Sub

Private Sub Form_Load()

1         On Error GoTo Trata_Erro

          vInicio = Now

          '--------------------------------------------
          '-- Variaveis de Lican�a para uso do BWZIP
2         MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
3         MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
4         MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
          '--------------------------------------------
              
          Dim DtGeracao As Date
          Dim DtGeracaoZip As Date
          Dim vArq As String
          
5         Me.Caption = "FIL470 - Monitoramento de PAACs e Representantes - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
          
6         Me.Tag = "LOAD"
              
7         Timer1.Enabled = False
              
8         vDriveIN_CONTROLE = GetKeyVal("C:\FIL470.INI", "DRIVE", "IN_CONTROLE")
9         vDriveSISTEMAS_PAACS_EXECUTAVEIS = GetKeyVal("C:\FIL470.INI", "DRIVE", "SISTEMAS_PAACS_EXECUTAVEIS")
          
10        If vDriveIN_CONTROLE = "" And vDriveSISTEMAS_PAACS_EXECUTAVEIS = "" Then frmDrives.Show 1
              
11        If Dir("C:\FIL470.INI") = "" Then
12            AddToINI "C:\FIL470.INI", "CAMINHO_EXECUTAVEIS", "CAMINHO", vDriveSISTEMAS_PAACS_EXECUTAVEIS & ":\SISTEMAS\PAACS\EXECUTAVEIS\"
13            AddToINI "C:\FIL470.INI", "CAMINHO_CONTROLE", "CONTROLE", vDriveIN_CONTROLE & ":\IN\CONTROLE\"
14            AddToINI "C:\FIL470.INI", "CAMINHO_TAB_GENERICAS", "CAMINHO", vDriveSISTEMAS_PAACS_EXECUTAVEIS & ":\SISTEMAS\PAACS\GENERICAS\"
15            AddToINI "C:\FIL470.INI", "CAMINHO_GERAR_GENERICAS", "CAMINHO", "C:\CARGAS_PAAC\GENERICAS\"
16        End If
              
17        If Dir("C:\CARGAS_PAAC", vbDirectory) = "" Then
18            MkDir "C:\CARGAS_PAAC"
19        End If
20        If Dir("C:\CARGAS_PAAC\GENERICAS", vbDirectory) = "" Then
21            MkDir "C:\CARGAS_PAAC\GENERICAS"
22        End If
23        If Dir("C:\CARGAS_PAAC\ZIP", vbDirectory) = "" Then
24            MkDir "C:\CARGAS_PAAC\ZIP"
25        End If
              
26        vCaminho_Executaveis = GetKeyVal("C:\FIL470.INI", "CAMINHO_EXECUTAVEIS", "CAMINHO")
27        If vCaminho_Executaveis = "" Then AddToINI "C:\FIL470.INI", "CAMINHO_EXECUTAVEIS", "CAMINHO", vDriveSISTEMAS_PAACS_EXECUTAVEIS & ":\SISTEMAS\PAACS\EXECUTAVEIS\"

28        vCaminho_Controles = GetKeyVal("C:\FIL470.INI", "CAMINHO_CONTROLE", "CONTROLE")
29        If vCaminho_Controles = "" Then AddToINI "C:\FIL470.INI", "CAMINHO_CONTROLE", "CONTROLE", vDriveIN_CONTROLE & ":\IN\CONTROLE\"

30        vCaminho_Genericas = GetKeyVal("C:\FIL470.INI", "CAMINHO_TAB_GENERICAS", "CAMINHO")
31        If vCaminho_Genericas = "" Then AddToINI "C:\FIL470.INI", "CAMINHO_TAB_GENERICAS", "CAMINHO", vDriveSISTEMAS_PAACS_EXECUTAVEIS & ":\SISTEMAS\PAACS\GENERICAS\"

32        vCaminho_Gerar_Genericas = GetKeyVal("C:\FIL470.INI", "CAMINHO_GERAR_GENERICAS", "CAMINHO")
33        If vCaminho_Gerar_Genericas = "" Then AddToINI "C:\FIL470.INI", "CAMINHO_GERAR_GENERICAS", "CAMINHO", "C:\CARGAS_PAAC\GENERICAS\"
          
34        frmLoad.Show
          
35        frmLoad.Refresh
          
36        Me.txtData = Date
                
37        DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "DATA_GENERICAS", "GERACAO") = "", Date - 1, GetKeyVal("C:\FIL470.INI", "DATA_GENERICAS", "GERACAO"))
38        If DtGeracao <> Date Then
39            AddToINI "C:\FIL470.INI", "DATA_GENERICAS", "GERACAO", Date
40            frmLoad.stb.Panels(1).Text = "Gerando Quantidades das Tabelas Genericas"
41            Gerar_Qtd_Genericas
42            frmLoad.stb.Panels(1).Text = "Quantidades Geradas com Sucesso !"
43        End If
          
          Preencher_Num_tabelas
          
44        Me.Refresh
45        Carregar_PAACREPRES
          
46        Me.Refresh
47        Ler_Arquivo
          
48        Me.Refresh
49        Preencher_Lsv_Versoes
          
50        Me.Refresh
51        Carregar_Dados
          
          '***********************
          'Gerar Tabelas Genericas
          '***********************
52        DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "DATA_TAB_GENERICAS", "GERACAO") = "", Date - 1, GetKeyVal("C:\FIL470.INI", "DATA_TAB_GENERICAS", "GERACAO"))
53        If DtGeracao <> Date Then
54            AddToINI "C:\FIL470.INI", "DATA_TAB_GENERICAS", "GERACAO", Date
55            frmLoad.stb.Panels(1).Text = "Gerando Tabelas Genericas"
56            Gerar_Tabelas_Genericas

              'Copiar tabelas Genericas geradas para a pasta da rede
57            frmLoad.stb.Panels(1).Text = "Copiando arquivos gerados para pasta da Rede"
                
58            File1.Pattern = "*.DBF"
59            File1.Path = vCaminho_Gerar_Genericas
60            File1.Refresh

61            For i = 0 To File1.ListCount - 1
62                FileCopy vCaminho_Gerar_Genericas & File1.List(i), vCaminho_Genericas & File1.List(i)
63            Next
64        End If
          
65        Me.Refresh
66        Ajustar_largura_lsv frmFIL470.lsvErrosPAAC
67        Ajustar_largura_lsv frmFIL470.lsvErrosRepres
          
68        Me.Refresh
69        Preencher_Email
          
70        Me.Refresh
71        Formatar_Flex_Grid
          
72        Me.Refresh
73        Pintar_Valores_Errados
          
      'Comentado em 20/08/2007
74        If GetKeyVal("C:\FIL470.INI", "ENVIAR", "PAACS") = "" And GetKeyVal("C:\FIL470.INI", "ENVIAR", "REPRES") = "" Then
75            MsgBox "Marque para quais PAACs ou Repres as cargas deverao ser enviadas.", vbInformation, "Aten��o"
76        Else
77            Enviar
78        End If
          
79        If GetKeyVal("C:\FIL470.INI", "ENVIAR", "HORARIO") = "" Then
80            AddToINI "C:\FIL470.INI", "ENVIAR", "HORARIO", "19:00"
81            txtHorario = "19:00"
82        Else
83            txtHorario = GetKeyVal("C:\FIL470.INI", "ENVIAR", "HORARIO")
84        End If
          
          '*****************************************
          'Gerar Cargas para todos os PAACs e Repres
          '*****************************************
85        Me.Refresh
          'Limpando arquivos da pasta C:\CARGA_PAACS\ZIP
86        DtGeracaoZip = IIf(GetKeyVal("C:\FIL470.INI", "DATA_GERACAO_ZIP", "GERACAO") = "", Date - 1, GetKeyVal("C:\FIL470.INI", "DATA_GERACAO_ZIP", "GERACAO"))
87        If DtGeracaoZip <> Date And Day(Date) < 21 Then
88            AddToINI "C:\FIL470.INI", "DATA_GERACAO_ZIP", "GERACAO", Date
89            frmLoad.stb.Panels(1).Text = "Limpando a pasta ZIP."

90            Do While Dir("C:\CARGAS_PAAC\ZIP\", vbArchive) <> ""
91                vArq = Dir("C:\CARGAS_PAAC\ZIP\*.*")
92                If vArq = "" Then Exit Do
93                Kill "C:\CARGAS_PAAC\ZIP\" & vArq
94            Loop

95            For i = 0 To lstPAACEnviar.ListCount - 1
96                If lstPAACEnviar.Selected(i) = True Then
97                   Gerar_Cargas Me.lstPAACEnviar.List(i), "P"
98                End If
99            Next
100           For i = 0 To Me.lstRepresEnviar.ListCount - 1
101               If lstRepresEnviar.Selected(i) = True Then
102                  Gerar_Cargas Me.lstRepresEnviar.List(i), "R"
103               End If
104           Next
105        End If
           
           
          'Preenche a lista para cargas diarias.
          Dim vListaRepres As String
          Dim vListaPaac As String
          Dim vMarcadoPaac() As String
          Dim vMarcadoRepres() As String
          Dim ii As Integer
          
106       vListaRepres = GetKeyVal("C:\FIL470.INI", "ENVIAR", "REPRES")
107       vListaPaac = GetKeyVal("C:\FIL470.INI", "ENVIAR", "PAACS")
          
108       If vListaRepres <> "" Then
109           vMarcadoRepres = Split(vListaRepres, ",")
110           For ii = 0 To UBound(vMarcadoRepres)
111               For i = 0 To lstRepresEnviar.ListCount - 1
112                   If vMarcadoRepres(ii) = lstRepresEnviar.List(i) Then
113                       lstRepresEnviar.Selected(i) = True
114                       Exit For
115                   End If
                      
116               Next
117           Next
118       End If

          
119       If vListaPaac <> "" Then
120           vMarcadoPaac = Split(vListaPaac, ",")
121           For ii = 0 To UBound(vMarcadoPaac)
122               For i = 0 To lstPAACEnviar.ListCount - 1
123                   If vMarcadoPaac(ii) = lstPAACEnviar.List(i) Then
124                       lstPAACEnviar.Selected(i) = True
125                       Exit For
126                   End If
127               Next
128           Next
129       End If
          
          Call preencheTitulosGridBaseDesativada
          
130       Unload frmLoad
        
131       Me.Tag = ""
          
132       Me.TabControle.Tab = 1
          
133       Timer_Envio.Enabled = True
134       Timer1.Enabled = True
          
Trata_Erro:
135       If Err.Number = 440 Then
136           Resume Next
137       ElseIf Err.Number <> 0 Then
138           MsgBox "Sub frmFIL470_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
139       End If
End Sub

Sub Carregar_PAACREPRES()
    Dim reg As Integer
    Dim pRst As Object
    Dim rRst As Object
    Dim litem As ListItem
    
    frmLoad.pb1.Value = 0
    
    Set vSessao = CreateObject("oracleinproCServer.xorasession")
    Set vBanco = vSessao.OpenDatabase("SDPK_META", "FIL430/PROD", 0&)
    
    Set pRst = vBanco.CreateDynaset("Select * from paac.deposito_filial where fl_base = 'S' ORDER BY COD_FILIAL", 0&)
    Set rRst = vBanco.CreateDynaset("select A.* From paac.controle_representante A,paac.deposito_repres b, representante C Where  B.Fl_Base = 'S' and a.cod_repres = b.cod_repres and a.cod_repres = c.cod_repres and c.situacao <> 9 ORDER BY A.COD_REPRES", 0&)
    
    msfControlePaac.Clear
    
    msfControlePaac.Rows = pRst.RecordCount + 2 '* 2 + 2
    msfControleVersoesPaac.Rows = pRst.RecordCount + 2
    msfIndicePaac.Rows = pRst.RecordCount + 2
    
    
        
    msfControleRepres.Rows = rRst.RecordCount + 3 '* 2 + 2
    msfControleVersoesRepres.Rows = rRst.RecordCount + 2
    msfIndiceRepres.Rows = rRst.RecordCount + 2
    
    frmLoad.pb1.Max = msfControlePaac.Rows + msfControleVersoesPaac.Rows + msfControleRepres.Rows + msfControleVersoesRepres.Rows + msfIndicePaac.Rows + msfIndiceRepres.Rows
    
    lstPAACEnviar.Clear
    lstPAACs.Clear
    
    For reg = 1 To pRst.RecordCount '* 2
        frmLoad.pb1.Value = frmLoad.pb1.Value + 1
        msfControlePaac.TextMatrix(reg + 1, 0) = pRst!cod_filial
        msfControleVersoesPaac.TextMatrix(reg + 1, 0) = pRst!cod_filial
        msfIndicePaac.TextMatrix(reg + 1, 0) = pRst!cod_filial
        
        lstPAACs.AddItem pRst!cod_filial
        lstPAACEnviar.AddItem pRst!cod_filial
        
        pRst.MoveNext
    Next

'Repres
    msfControleRepres.Clear
    For reg = 1 To rRst.RecordCount '* 2
        msfControleRepres.TextMatrix(reg + 1, 0) = rRst!cod_repres
        msfControleVersoesRepres.TextMatrix(reg + 1, 0) = rRst!cod_repres
        msfIndiceRepres.TextMatrix(reg + 1, 0) = rRst!cod_repres
        
        lstRepresentantes.AddItem rRst!cod_repres
        lstRepresEnviar.AddItem rRst!cod_repres
        
        rRst.MoveNext
    Next
    
    frmLoad.pb1.Value = frmLoad.pb1.Max

End Sub

Sub Ler_Arquivo()

1         On Error GoTo Trata_Erro
          
          Dim vArq As Integer
          Dim vArquivo As String
          Dim vLinha As String
          Dim i As Integer
          Dim ii As Integer
          Dim iii As Integer
          Dim litem As ListItem
          Dim vTipo As String
          Dim vErrosSeparados() As String
          Dim vData As String
          Dim vCod As Long
          Dim vRst As Object
          Dim vNomeTable As String
          Dim vStatus As String
                
          '------------------------------------------------------------------------------------------------'
          ' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
          ' Inclus�o da Aba "Base Desat."
          ' inserido verifica��o e rotina para busca das bases de representantes
          '------------------------------------------------------------------------------------------------'
          Dim contVet As Integer
          Dim strDataAux As String
          Dim intIndice As Integer
          
          intIndice = -1 ' Variavel para auxiliar na verifica��o e valida��o no preenchimento do vetor
          '------------------------------------------------------------------------------------------------'
2         frmLoad.pb2.Value = 0
       
3         lbltotalPaacsErro.Caption = 0
4         lbltotalRepresErro.Caption = 0
5         lsvErrosPAAC.ListItems.Clear
6         lsvErrosRepres.ListItems.Clear
       
7         vArq = FreeFile
       
8         If Dir(vCaminho_Controles) <> "" Then
9             vTipo = Dir(vCaminho_Controles, vbArchive)
10            If UCase(Left(vTipo, 8)) <> "PEDCONTR" Then
11                MsgBox "Caminho Invalido." & vbCrLf & "Por favor confirme o caminho dos arquivos de controle.", vbInformation + vbOKOnly, "Aten��o"
12                Exit Sub
13            End If
14        End If
                
15        File1.Path = vCaminho_Controles
16        File1.Pattern = "PED*.TXT"
17        File1.Refresh
18        frmLoad.pb2.Value = frmLoad.pb2.Value + File1.ListCount
                
          '------------------------------------------------------------------------------------------------'
          ' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
          ' Inclus�o da Aba "Base Desat."
          ' inserido verifica��o e rotina para busca das bases de representantes
          '------------------------------------------------------------------------------------------------'
                
          contVet = File1.ListCount
          
          ReDim vetIdRepresentante(contVet) As Integer
          ReDim vetDataAtualizacao(contVet) As String
                
          '------------------------------------------------------------------------------------------------'
                
19        For i = 0 To File1.ListCount - 1
20            Erase vErrosSeparados
21            vArquivo = File1.List(i)
22            vCod = Val(Mid(vArquivo, 10, 4))
23            vTipo = Mid(vArquivo, 9, 1)
          
24            If vCod > 0 Then
              
25            Open vCaminho_Controles & vArquivo For Input As #vArq
26            Do While Not EOF(vArq)
27                Input #vArq, vLinha
28                If vLinha = "[VERSOES]" Then
29                    For ii = 0 To 100
30                        Input #vArq, vLinha
31                        If vLinha = "" Or vLinha = "[TABELAS]" Or InStr(1, vLinha, ".") = 0 Then Exit For
32                        If Left(vLinha, 1) <> "[" And InStr(1, vLinha, "=") > 0 Then
33                            If (InStr(1, UCase(vLinha), "FIL") > 0 Or InStr(1, UCase(vLinha), "GER") > 0) And InStr(1, UCase(vLinha), "OLD") = 0 Then
34                                Inserir_Oracle vData, vCod, vTipo, "", 0, Trim(Left(vLinha, InStr(1, vLinha, "=") - 1)), Trim(Mid(vLinha, InStr(1, vLinha, "=") + 1)), 0, 0, 0, "VERSAO"
35                            End If
36                        End If
37                        frmLoad.stb.Panels(1).Text = "Vers�es: " & vLinha & " Arquivo: " & vArquivo
38                        frmLoad.stb.Refresh: DoEvents
39                    Next
40                ElseIf vLinha = "[TABELAS]" Then
41                    For ii = 0 To 500
42                        Input #vArq, vLinha
43                        If vLinha = "" Or InStr(1, vLinha, ";") > 0 Or InStr(1, vLinha, ".") > 0 Then Exit For
44                        If Left(UCase(vLinha), 4) <> "MSYS" And InStr(1, vLinha, "=") > 0 Then
45                            Inserir_Oracle vData, vCod, vTipo, Trim(Left(vLinha, InStr(1, vLinha, "=") - 1)), Trim(Mid(vLinha, InStr(1, vLinha, "=") + 1)), "", "", 0, 0, 0, "TABELA"
46                        End If
47                        frmLoad.stb.Panels(1).Text = "Tabelas: " & vLinha & " Arquivo: " & vArquivo
48                        frmLoad.stb.Refresh: DoEvents
49                    Next
50                ElseIf vLinha = "[ATUALIZACAO]" Then
51                    Input #vArq, vLinha
52                    vData = vLinha
                      '------------------------------------------------------------------------------------------------'
                      ' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
                      ' Inclus�o da Aba "Base Desat."
                      ' inserido verifica��o e rotina para busca das bases de representantes
                      '------------------------------------------------------------------------------------------------'
                      strDataAux = vData
                      '------------------------------------------------------------------------------------------------'
53                ElseIf vLinha = "[DIFERENCA]" Then
54                    For ii = 0 To 100
55                        Input #vArq, vLinha
56                        If vLinha = "" Then Exit For
57                        vErrosSeparados = Split(Trim(Mid(vLinha, InStr(1, vLinha, "=") + 1)), ";")
58                        Inserir_Oracle vData, vCod, vTipo, Trim(Mid(vLinha, 1, InStr(1, vLinha, "=") - 1)), 0, "", "", vErrosSeparados(0), vErrosSeparados(1), vErrosSeparados(2), "ERRO"
59                        frmLoad.stb.Panels(1).Text = "Diferencas: " & vLinha & " Arquivo: " & vArquivo
60                        frmLoad.stb.Refresh: DoEvents
61                    Next
62                Else 'Verifica os arquivos de indices - @Author:Luciano Carlos | @Date:20/03/2008
                      'Dim v As String
                      'Debug.Print Dir(vCaminho_Controles, vbArchive)
                      
63                    If Mid(vArquivo, 1, 8) = "PEDINDIC" Then
64                    For ii = 0 To 200
65                        Input #vArq, vLinha
66                        If vLinha = "" Then Exit For
67                        vNomeTable = RTrim(Left(vLinha, InStr(1, vLinha, "-") - 1))
68                        vStatus = LTrim(Right(vLinha, InStr(1, vLinha, " ") - 1))
69                        vStatus = Trim(Mid(vStatus, InStr(1, vStatus, "-") + 1))
70                        atu_Indices vCod, vTipo, vNomeTable, vStatus
71                        frmLoad.stb.Refresh: DoEvents
72                    Next
73                    End If
74                End If
                  
                  '------------------------------------------------------------------------------------------------'
                  ' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
                  ' Inclus�o da Aba "Base Desat."
                  ' inserido verifica��o e rotina para busca das bases de representantes
                  '------------------------------------------------------------------------------------------------'
                  ' Se o vetor n�o foi preenchido e existe data
                  If (vetDataAtualizacao(i) = "" And strDataAux <> "" And _
                      intIndice <> i And vetIdRepresentante(i) <> vCod) Then
                    vetIdRepresentante(i) = vCod
                    vetDataAtualizacao(i) = strDataAux
                    strDataAux = ""
                    intIndice = i
                  End If
                  '------------------------------------------------------------------------------------------------'
Proximo:
75            Loop
76            Close vArq
77            If InStr(1, UCase(vCaminho_Controles), "\CONTROLE\") > 0 Then Kill vCaminho_Controles & vArquivo
78            If frmLoad.pb2.Value < frmLoad.pb2.Max Then frmLoad.pb2.Value = frmLoad.pb2.Value + 1
79          Else
80            Close vArq
81            If InStr(1, UCase(vCaminho_Controles), "\CONTROLE\") > 0 Then Kill vCaminho_Controles & vArquivo
82            If frmLoad.pb2.Value < frmLoad.pb2.Max Then frmLoad.pb2.Value = frmLoad.pb2.Value + 1
83          End If
84        Next
85        frmLoad.pb2.Value = frmLoad.pb2.Max
          
          
          '------------------------------------------------------------------------------------------------'
          ' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
          ' Inclus�o da Aba "Base Desat."
          ' inserido verifica��o e rotina para busca das bases de representantes
          '------------------------------------------------------------------------------------------------'
          Call preencheGridBaseDesativada(vetIdRepresentante, vetDataAtualizacao)
          '------------------------------------------------------------------------------------------------'

Trata_Erro:
86        If Err.Number = 53 Then
87            GoTo Proximo
88        ElseIf Err.Number = 62 Then
89            vLinha = ""
90            Resume Next
91        ElseIf Err.Number <> 0 Then
92            MsgBox "Sub Ler_Arquivo" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
93        End If

End Sub

Sub Inserir_Oracle(pData As String, pCod As Long, pTipo As String, pTabela As String, pRegs As Long, pSistema As String, pVersao As String, pReg_Env As String, pReg_Rec As String, pReg_Base As String, pControle As String)
              
1         On Error GoTo Trata_Erro
              
              Dim vDia As String
              Dim vMes As String
              Dim vAno As String
              
2             vDia = Day(CDate(pData))
3             vMes = Choose(Month(CDate(pData)), "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
4             vAno = Year(CDate(pData))
              
5             vBanco.Parameters.Remove "DATA"
6             vBanco.Parameters.Add "DATA", vDia & "-" & vMes & "-" & vAno, 1
                  
7             vBanco.Parameters.Remove "Cod"
8             vBanco.Parameters.Add "cod", pCod, 1
                  
9             vBanco.Parameters.Remove "TIPO"
10            vBanco.Parameters.Add "TIPO", pTipo, 1
                  
11            vBanco.Parameters.Remove "TABELA"
12            vBanco.Parameters.Add "TABELA", pTabela, 1

13            vBanco.Parameters.Remove "REGS"
14            vBanco.Parameters.Add "REGS", pRegs, 1

15            vBanco.Parameters.Remove "SISTEMA"
16            vBanco.Parameters.Add "SISTEMA", pSistema, 1
                  
17            vBanco.Parameters.Remove "VERSAO"
18            vBanco.Parameters.Add "VERSAO", pVersao, 1
                  
19            vBanco.Parameters.Remove "REG_ENV"
20            vBanco.Parameters.Add "REG_ENV", pReg_Env, 1
                  
21            vBanco.Parameters.Remove "REG_REC"
22            vBanco.Parameters.Add "REG_REC", pReg_Rec, 1
                  
23            vBanco.Parameters.Remove "REG_BASE"
24            vBanco.Parameters.Add "REG_BASE", pReg_Base, 1

25            vBanco.Parameters.Remove "CONTROLE"
26            vBanco.Parameters.Add "CONTROLE", pControle, 1

27            vBanco.Parameters.Remove "COD_ERRO"
28            vBanco.Parameters.Add "COD_ERRO", 0, 2
                  
29            vBanco.Parameters.Remove "TXT_ERRO"
30            vBanco.Parameters.Add "TXT_ERRO", "", 2
                  
31            vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_INSERT_ATUALIZA(:DATA, :COD, :TIPO, :TABELA, :REGS, :SISTEMA, :VERSAO, :REG_ENV, :REG_REC, :REG_BASE, :CONTROLE, :COD_ERRO, :TXT_ERRO); END;"
                    
32            If vBanco.Parameters("COD_ERRO").Value <> 0 Then
33               MsgBox "Erro no Oracle: " & vBanco.Parameters("COD_ERRO").Value & vbCrLf & "Descri��o:" & vBanco.Parameters("TXT_ERRO").Value

34               End
35            End If
36            DoEvents
Trata_Erro:
37        If Err.Number <> 0 Then
38            MsgBox "Sub Inserir_Oracle" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If
End Sub

Public Sub Criar_Cursor(pCursor As String)
    vBanco.Parameters.Remove pCursor
    vBanco.Parameters.Add pCursor, 0, 2
    vBanco.Parameters(pCursor).serverType = ORATYPE_CURSOR
    vBanco.Parameters(pCursor).DynasetOption = ORADYN_NO_BLANKSTRIP
    vBanco.Parameters(pCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Public Sub Criar_Cursor_Batch(pCursor As String)
    vBancoBatch.Parameters.Remove pCursor
    vBancoBatch.Parameters.Add pCursor, 0, 2
    vBancoBatch.Parameters(pCursor).serverType = ORATYPE_CURSOR
    vBancoBatch.Parameters(pCursor).DynasetOption = ORADYN_NO_BLANKSTRIP
    vBancoBatch.Parameters(pCursor).DynasetCacheParams 256, 16, 20, 2000, 0
End Sub


Public Sub Carregar_Dados()
          
1         On Error GoTo Trata_Erro
          Dim iiiii As Integer
          Dim iiii As Integer
          Dim iii As Integer
          Dim vDia As String
          Dim vMes As String
          Dim vAno As String
          Dim vCodPaacRepr As Long
          Dim vRst As Object
          Dim litem As ListItem
          
2         frmLoad.PB3.Value = 0
          
3         If Me.Tag = "LOAD" Then
                        
                'Nomes das colunas faz somente uma vez - Tabelas
4               Nomes_Tabelas msfControlePaac
5               Nomes_Tabelas msfControleRepres
6               Nomes_Tabelas msfIndicePaac
7               Nomes_Tabelas msfIndiceRepres
                
8               frmLoad.PB3.Max = frmLoad.PB3.Value + lsvVersao.ListItems.Count
                
9               For ii = 1 To lsvVersao.ListItems.Count
10                  If ii = 1 Then
11                      msfControleVersoesPaac.TextMatrix(1, 0) = "Paac"
12                      msfControleVersoesRepres.TextMatrix(1, 0) = "Repres"
13                   End If
14                   msfControleVersoesPaac.TextMatrix(0, ii) = Mid(UCase(Left(lsvVersao.ListItems(ii), 1)) & Mid(LCase(lsvVersao.ListItems(ii)), 2), 1, InStr(lsvVersao.ListItems(ii), ".") - 1)
15                   msfControleVersoesRepres.TextMatrix(0, ii) = Mid(UCase(Left(lsvVersao.ListItems(ii), 1)) & Mid(LCase(lsvVersao.ListItems(ii)), 2), 1, InStr(lsvVersao.ListItems(ii), ".") - 1)
16              Next
17               For ii = 1 To lsvVersao.ListItems.Count
18                  frmLoad.PB3.Value = frmLoad.PB3.Value + 1
19                  For iii = 1 To msfControleVersoesPaac.Cols - 1
20                      If UCase(msfControleVersoesPaac.TextMatrix(0, iii)) = Mid(UCase(lsvVersao.ListItems(ii)), 1, InStr(lsvVersao.ListItems(ii), ".") - 1) Then
21                          msfControleVersoesPaac.TextMatrix(1, iii) = lsvVersao.ListItems(ii).SubItems(1)
22                          Exit For
23                      End If
24                  Next
25                  For iii = 1 To msfControleVersoesRepres.Cols - 1
26                      If UCase(msfControleVersoesRepres.TextMatrix(0, iii)) = Mid(UCase(lsvVersao.ListItems(ii)), 1, InStr(lsvVersao.ListItems(ii), ".") - 1) Then
27                          msfControleVersoesRepres.TextMatrix(1, iii) = lsvVersao.ListItems(ii).SubItems(1)
28                          Exit For
29                      End If
30                  Next
31              Next
32        End If
                    
33        vDia = Day(CDate(txtData))
34        vMes = Choose(Month(CDate(txtData)), "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
35        vAno = Year(CDate(txtData))
                    
36        vBanco.Parameters.Remove "DT_MOVIMENTO"
37        vBanco.Parameters.Add "DT_MOVIMENTO", vDia & "-" & vMes & "-" & vAno, 1
                    
          'Selecionar os registros do dia
          'Localizar o Paac ou o Representante correspondente
38        Criar_Cursor "vCursor_Tab"
39        Criar_Cursor "vCursor_Ver"
40        Criar_Cursor "vCursor_Err"
41        Criar_Cursor "vCursor_Com"
42        Criar_Cursor "vCursor_Ind"

            
43        vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_TODAS(:VCURSOR_TAB, :vCursor_VER, :vCursor_Ind, :vCursor_Err, :DT_MOVIMENTO); END;"
'43        Set vRst = Nothing
44        Set vRst = vBanco.Parameters("vCursor_Ind").Value
45        vBanco.Parameters.Remove "vCursor_Ind"
                      
          'Registros Gerados com Qtdes para Comparacao
46        Preencher_Registros_Comparacao_PAAC
47        Preencher_Registros_Comparacao_REPRES

48        frmLoad.PB6.Value = 0
49        frmLoad.PB6.Max = IIf(vRst.RecordCount = 0, 1, vRst.RecordCount)
50        For ii = 1 To vRst.RecordCount
51            frmLoad.PB6.Value = frmLoad.PB6.Value + 1
52            If vRst!tipo = "P" Then
                  'Registros Recebidos dos PAACs e Repres
                  frmLoad.stb.Panels(1).Text = "Tipo P: " & vRst!cod_paac_repres
53                For iii = 2 To msfIndicePaac.Rows - 1
55                       If msfIndicePaac.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
56                          'msfIndicePaac.TextMatrix(iii, 1) = vRst!dt_atualizacao
57                          For iiii = 0 To msfIndicePaac.Cols - 1
                                frmLoad.stb.Panels(1).Text = "Tipo R Segundo Loop: " & vRst!cod_paac_repres & " - " & iiii
58                              If msfIndicePaac.TextMatrix(0, iiii) = "" And iiii > 0 Then Exit For
59                              If UCase(msfIndicePaac.TextMatrix(0, iiii)) = UCase(vRst!Tabela) Then
60                                  msfIndicePaac.TextMatrix(iii, iiii) = IIf(IsNull(vRst!STATUS_PK), "", vRst!STATUS_PK)
61                                  Exit For
62                              End If
63                          Next
64                          Exit For
65                       End If
66                Next
67            ElseIf vRst!tipo = "R" Then
68                For iii = 2 To msfIndiceRepres.Rows - 1
                      frmLoad.stb.Panels(1).Text = "Tipo R: " & vRst!cod_paac_repres & " - " & iii
70                    If msfIndiceRepres.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
'66                       msfIndiceRepres.TextMatrix(iii, 1) = vRst!dt_atualizacao
71                       For iiii = 0 To msfIndiceRepres.Cols - 1
                             frmLoad.stb.Panels(1).Text = "Tipo R Segundo Loop: " & vRst!cod_paac_repres & " - " & iiii
72                           If msfIndiceRepres.TextMatrix(0, iiii) = "" And iiii > 0 Then Exit For
73                           If UCase(msfIndiceRepres.TextMatrix(0, iiii)) = UCase(vRst!Tabela) Then
74                              msfIndiceRepres.TextMatrix(iii, iiii) = IIf(IsNull(vRst!STATUS_PK), "", vRst!STATUS_PK)
75                              Exit For
76                           End If
77                       Next
78                       Exit For
79                    End If
80                Next
81            End If
82            vRst.MoveNext
              DoEvents
              frmLoad.stb.Refresh
83        Next
84        Set vRst = Nothing
                      
'42        vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_TODAS(:VCURSOR_TAB, :vCursor_VER, :vCursor_Err, :DT_MOVIMENTO); END;"
'43        Set vRst = Nothing
85        Set vRst = vBanco.Parameters("VCURSOR_Tab").Value
86        vBanco.Parameters.Remove "vCursor_Tab"
                      
          'Registros Gerados com Qtdes para Comparacao
87        Preencher_Registros_Comparacao_PAAC
88        Preencher_Registros_Comparacao_REPRES

89        frmLoad.PB6.Value = 0
90        frmLoad.PB6.Max = IIf(vRst.RecordCount = 0, 1, vRst.RecordCount)
91        For ii = 1 To vRst.RecordCount
92            frmLoad.PB6.Value = frmLoad.PB6.Value + 1
93            If vRst!tipo = "P" Then
                  'Registros Recebidos dos PAACs e Repres
94                For iii = 2 To msfControlePaac.Rows - 1
96                       If msfControlePaac.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
97                          msfControlePaac.TextMatrix(iii, 1) = vRst!dt_atualizacao
98                          For iiii = 0 To msfControlePaac.Cols - 1
99                              If msfControlePaac.TextMatrix(0, iiii) = "" And iiii > 0 Then Exit For
100                             If UCase(msfControlePaac.TextMatrix(0, iiii)) = UCase(vRst!Tabela) Then
101                                 msfControlePaac.TextMatrix(iii, iiii) = vRst!REGISTROS
102                                 Exit For
103                             End If
104                         Next
105                         Exit For
106                      End If
107               Next
108           ElseIf vRst!tipo = "R" Then
109               For iii = 2 To msfControleRepres.Rows - 1
111                   If msfControleRepres.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
112                      msfControleRepres.TextMatrix(iii, 1) = vRst!dt_atualizacao
113                      For iiii = 0 To msfControleRepres.Cols - 1
114                          If msfControleRepres.TextMatrix(0, iiii) = "" And iiii > 0 Then Exit For
115                          If UCase(msfControleRepres.TextMatrix(0, iiii)) = UCase(vRst!Tabela) Then
116                             msfControleRepres.TextMatrix(iii, iiii) = vRst!REGISTROS
117                             Exit For
118                          End If
119                      Next
120                      Exit For
121                   End If
122               Next
123           End If
              DoEvents
              frmLoad.stb.Panels(1).Text = "Registros Recebidos Tipo: " & vRst!tipo & " - C�digo: " & vRst!cod_paac_repres
124           vRst.MoveNext
125       Next
126       Set vRst = Nothing
                      
          'Versoes
127       Set vRst = vBanco.Parameters("vCursor_VER").Value
128       frmLoad.PB7.Value = 0
129       frmLoad.PB7.Max = IIf(vRst.RecordCount = 0, 1, vRst.RecordCount)
130       vBanco.Parameters.Remove "vCursor_ver"
131       For ii = 1 To vRst.RecordCount
132           frmLoad.PB7.Value = frmLoad.PB7.Value + 1
133           If vRst!tipo = "P" Then
134               For iii = 2 To msfControleVersoesPaac.Rows - 1
135                   If msfControleVersoesPaac.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
136                       DoEvents
137                       For iiii = 0 To msfControleVersoesPaac.Cols - 1
138                           If UCase(msfControleVersoesPaac.TextMatrix(0, iiii)) = UCase(vRst!Sistema) Then
139                               msfControleVersoesPaac.TextMatrix(iii, iiii) = vRst!Versao
140                               If msfControleVersoesPaac.TextMatrix(1, iiii) <> vRst!Versao Then
141                                  Pintar_Errados msfControleVersoesPaac, iiii, iii
142                               End If
143                               GoTo Proximo
144                           End If
145                       Next
146                   End If
147               Next
148           Else
149               For iii = 2 To msfControleVersoesRepres.Rows - 1
150                   If msfControleVersoesRepres.TextMatrix(iii, 0) = vRst!cod_paac_repres Then
151                         DoEvents
152                         For iiii = 0 To msfControleVersoesRepres.Cols - 1
153                           If UCase(msfControleVersoesRepres.TextMatrix(0, iiii)) = UCase(vRst!Sistema) Then
154                               msfControleVersoesRepres.TextMatrix(iii, iiii) = vRst!Versao
155                               If msfControleVersoesRepres.TextMatrix(1, iiii) <> vRst!Versao Then
156                                  Pintar_Errados msfControleVersoesRepres, iiii, iii
157                               End If
158                               GoTo Proximo
159                           End If
160                       Next
161                   End If
162               Next
163           End If
Proximo:
164           DoEvents
165           vRst.MoveNext
166       Next
167       frmLoad.PB7.Value = frmLoad.PB7.Max
          
          
          'Erros
168       Set vRst = vBanco.Parameters("vCursor_Err").Value
169       vBanco.Parameters.Remove "vCursor_Err"
170       frmLoad.PB8.Value = 0
171       frmLoad.PB8.Max = IIf(vRst.RecordCount = 0, 1, vRst.RecordCount)
172       For ii = 1 To vRst.RecordCount
173           frmLoad.PB8.Value = frmLoad.PB8.Value + 1
174           If vRst!tipo = "P" Then
175               If vCodPaacRepr <> vRst!cod_paac_repres Then
176                   vCodPaacRepr = vRst!cod_paac_repres
177                   Set litem = lsvErrosPAAC.ListItems.Add
178                   litem = vRst!cod_paac_repres
179                   lbltotalPaacsErro.Caption = Val(lbltotalPaacsErro.Caption) + 1
180               End If

181               Set litem = lsvErrosPAAC.ListItems.Add
182               litem.SubItems(1) = vRst!Tabela
183               litem.SubItems(2) = vRst!Reg_Env
184               litem.SubItems(3) = vRst!reg_rec
185               litem.SubItems(4) = vRst!reg_base
186           Else
187               If vCodPaacRepr <> vRst!cod_paac_repres Then
188                   vCodPaacRepr = vRst!cod_paac_repres
189                   Set litem = lsvErrosRepres.ListItems.Add
190                   litem = vRst!cod_paac_repres
191                   lbltotalRepresErro.Caption = Val(lbltotalRepresErro.Caption) + 1
192               End If
193               Set litem = lsvErrosRepres.ListItems.Add
194               litem.SubItems(1) = vRst!Tabela
195               litem.SubItems(2) = vRst!Reg_Env
196               litem.SubItems(3) = vRst!reg_rec
197               litem.SubItems(4) = vRst!reg_base
198           End If
199           vRst.MoveNext
200       Next
          
201       frmLoad.PB8.Value = frmLoad.PB8.Max
          
202       Set vRst = Nothing
          
Trata_Erro:
203       If Err.Number <> 0 Then
204           MsgBox "Sub Carregar_Dados" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
205       End If
End Sub

Private Sub lstEmail_Click()
    Me.txtEmail = Me.lstEmail
End Sub

Sub PAAC_REPRES_ENVIAR()
    Dim vMarcados As String
    
    For i = 0 To lstPAACEnviar.ListCount - 1
        If lstPAACEnviar.Selected(i) = True Then
           If vMarcados = "" Then
              vMarcados = lstPAACEnviar.List(i)
           Else
              vMarcados = vMarcados & "," & lstPAACEnviar.List(i)
           End If
        End If
    Next
    
    AddToINI "C:\FIL470.INI", "ENVIAR", "PAACS", vMarcados
    
    vMarcados = ""
    
    For i = 0 To lstRepresEnviar.ListCount - 1
        If lstRepresEnviar.Selected(i) = True Then
           If vMarcados = "" Then
              vMarcados = lstRepresEnviar.List(i)
           Else
              vMarcados = vMarcados & "," & lstRepresEnviar.List(i)
           End If
        End If
    Next
    
    AddToINI "C:\FIL470.INI", "ENVIAR", "REPRES", vMarcados


    If CDate(Date & " " & txtHorario) < Now Then
        MsgBox "Hor�rio menor que o atual.", vbInformation, "Aten��o"
        txtHorario.SetFocus
        Exit Sub
    End If
    If txtHorario = "" Then
        MsgBox "Informe o Hor�rio para efetuar carga.", vbInformation, "Aten��o"
        txtHorario.SetFocus
        Exit Sub
    End If
    
    AddToINI "C:\FIL470.INI", "ENVIAR", "HORARIO", txtHorario

End Sub

Private Sub mnuDiferencas_Click()
'    Exibir_linhas "PROBLEMAS", vNomeControle
End Sub

Private Sub mnuTodas_Click()
'    Exibir_linhas "TODAS", vNomeControle
End Sub

Private Sub msfControleVersoesPaac_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Set vNomeControle = msfControleVersoesPaac
        PopupMenu mnuVersoes
    End If
End Sub

Private Sub msfControleVersoesRepres_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        Set vNomeControle = msfControleVersoesRepres
        PopupMenu mnuVersoes
    End If
End Sub



Private Sub Timer_Envio_Timer()
    On Error GoTo Trata_Erro

    If Now >= CDate(Date & " " & txtHorario) And Day(Date) < 21 Then

        For i = 0 To lstPAACEnviar.ListCount - 1
            If lstPAACEnviar.Selected(i) = True Then
               If Dir("C:\CARGAS_PAAC\ZIP\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".ZIP", vbArchive) <> "" Then
                  FileCopy "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".ZIP", vDriveIN_CONTROLE & ":\OUT\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".ZIP"
                  FileCopy "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".HDR", vDriveIN_CONTROLE & ":\OUT\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".HDR"
                  Kill "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".ZIP"
                  Kill "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstPAACEnviar.List(i), "0000") & ".HDR"
               End If
            End If
        Next
        For i = 0 To lstRepresEnviar.ListCount - 1
            If lstRepresEnviar.Selected(i) = True Then
               If Dir("C:\CARGAS_PAAC\ZIP\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".ZIP", vbArchive) <> "" Then
                  FileCopy "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".ZIP", vDriveIN_CONTROLE & ":\OUT\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".ZIP"
                  FileCopy "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".HDR", vDriveIN_CONTROLE & ":\OUT\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".HDR"
                  Kill "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".ZIP"
                  Kill "C:\CARGAS_PAAC\ZIP\CAR" & Format(lstRepresEnviar.List(i), "0000") & ".HDR"
               End If
            End If
        Next

    End If

Trata_Erro:
    If Err.Number = 53 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub Timer_Envio_timer" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub Timer1_Timer()
    
    If vTempo >= Val(lblTempo) Then
        vTempo = 0
        Me.lblTempo = 120
        Me.Visible = False
        Form_Load
        Me.Visible = True
    Else
        Me.lblTempo = 120 - vTempo
        vTempo = vTempo + 1
    End If
End Sub

Function StayOnTop(Form As Form)
    Dim lFlags As Long
    Dim lStay As Long

    lFlags = SWP_NOSIZE Or SWP_NOMOVE
    lStay = SetWindowPos(Form.hwnd, HWND_TOPMOST, 0, 0, 0, 0, lFlags)
End Function

Private Sub txtData_KeyPress(KeyAscii As Integer)
    Data KeyAscii, txtData
End Sub

Sub Preencher_Lsv_Versoes_old()
    Dim litem As ListItem
    
    Criar_Cursor "vCursor"
                  
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_VERSAO(:vCURSOR); END;"
    
    Set vRst = vBanco.Parameters("vCursor").Value
    
    frmLoad.PB9.Max = frmLoad.PB9.Max + vRst.RecordCount
    
    lsvVersao.ListItems.Clear
    
    For i = 1 To vRst.RecordCount
        
        frmLoad.PB9.Value = frmLoad.PB9.Value + 1
                    
        Set litem = Me.lsvVersao.ListItems.Add
        litem = vRst!Cod_Software
        litem.SubItems(1) = vRst!NOME_SOFTWARE
        litem.SubItems(2) = vRst!Versao
        vRst.MoveNext
    Next
    
    frmLoad.PB9.Value = frmLoad.PB9.Max
        
    vBanco.Parameters.Remove "vCursor"
End Sub

Sub Preencher_Lsv_Versoes()
          
1         On Error GoTo Trata_Erro
          
          Dim litem As ListItem
          Dim fso As New FileSystemObject
          Dim Pasta As Folder
          Dim Arquivo As File
          Dim udtFileInfo As FILEINFO
          Dim Versao()
          Dim vVersao() As String
          
2         If Dir(vCaminho_Executaveis) = "" Then
3           MsgBox "No arquivo FIL470.INI est� faltando parametro.", vbInformation, "Atencao"
4           Exit Sub
5         End If
          
6         Set Pasta = fso.GetFolder(vCaminho_Executaveis)
          
7         lsvVersao.ListItems.Clear
          
8         Me.Refresh
          
9         For Each Arquivo In Pasta.Files
10            If UCase(Arquivo.Type) = "APLICATIVO" Or UCase(Arquivo.Type) = "APPLICATION" Then
11                If GetFileVersionInformation(Arquivo.Path, udtFileInfo) = eNoVersion Then
12                     MsgBox "No version available For this file", vbInformation
13                     Exit Sub
14                End If
15                vVersao = Split(udtFileInfo.ProductVersion, ".")
16                Set litem = lsvVersao.ListItems.Add
17                litem = UCase(Arquivo.Name)
18                If UBound(vVersao) = 1 Then
19                    litem.SubItems(1) = vVersao(0) & "." & vVersao(1) & ".000"
20                ElseIf UBound(vVersao) = 2 Then
21                    litem.SubItems(1) = vVersao(0) & "." & vVersao(1) & "." & Format(Val(vVersao(2)), "000")
22                ElseIf UBound(vVersao) = 3 Then
23                    litem.SubItems(1) = vVersao(0) & "." & vVersao(1) & "." & Format(Val(vVersao(3)), "000")
24                End If
25            End If
26        Next

Trata_Erro:
27        If Err.Number <> 0 Then
28      MsgBox "Sub Preencher_lsv_Versoes" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
29        End If
End Sub


Sub Preencher_Cbo_Versao()
    
    frmLoad.PB9.Value = 0
    
    Criar_Cursor "vCursor"

    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_SOFTWARE(:vCURSOR); END;"
    
    Set vRst = vBanco.Parameters("vCursor").Value
    
    frmLoad.PB9.Max = vRst.RecordCount
    
    cboSoftware.Clear
    
    For i = 1 To vRst.RecordCount
        
        frmLoad.PB9.Value = frmLoad.PB9.Value + 1
        
        cboSoftware.AddItem vRst!NOME_SOFTWARE
        cboSoftware.ItemData(cboSoftware.NewIndex) = vRst!Cod_Software
        vRst.MoveNext
        
    Next
    
    frmLoad.PB9.Value = frmLoad.PB9.Max
    
    vBanco.Parameters.Remove "vCursor"
End Sub


Sub Formatar_Flex_Grid()
    
    Dim Largura As Double
    
    msfControlePaac.ColWidth(0) = 500
    msfControleRepres.ColWidth(0) = 500
    msfControleVersoesPaac.ColWidth(0) = 500
    msfControleVersoesRepres.ColWidth(0) = 500
    
    For i = 1 To msfControleVersoesPaac.Cols - 1
        msfControleVersoesPaac.ColWidth(i) = 850
        msfControleVersoesRepres.ColWidth(i) = 850
    Next i
    
    'Zerar a largura das colunas em branco
    For i = 2 To msfControlePaac.Cols - 1
        If msfControlePaac.TextMatrix(0, i) = "" Then msfControlePaac.ColWidth(i) = 0
    Next
    For i = 2 To msfControlePaac.Cols - 1
        If msfControleRepres.TextMatrix(0, i) = "" Then msfControleRepres.ColWidth(i) = 0
    Next

End Sub


Private Sub txtVersao_KeyPress(KeyAscii As Integer)
    cmdLimpar.Enabled = True
End Sub

Sub Exibir_linhas(pTipo As String, pControle As MSHFlexGrid)
    
    Dim vAltura As Double
    vAltura = pControle.RowHeight(0)
    pControle.Col = 0
    
    For i = 2 To pControle.Rows - 1
        If pTipo = "TODAS" Then
            pControle.RowHeight(i) = vAltura
        Else
            pControle.Row = i
            If pControle.CellBackColor <> 8421631 Then
                pControle.RowHeight(i) = 0
            Else
                pControle.RowHeight(i) = vAltura
            End If
        End If
    Next
    
End Sub

Sub Pintar_Errados(pControle As MSFlexGrid, pColuna As Integer, pLinha As Integer)
    
    pControle.Row = 1
    pControle.Col = pColuna
    pControle.CellBackColor = &H8080FF
    
    pControle.Row = pLinha
    pControle.Col = 0
    pControle.CellBackColor = &H8080FF
    
    pControle.Col = pColuna
    pControle.CellBackColor = &H8080FF

End Sub


Sub Gerar_Qtde()
    
    Dim vQtde(55, 1) As String
    Dim vPR As Integer 'Paac Repres
    
    If lstGenericas.SelCount = 0 And Me.lstEspecificas.SelCount = 0 And Me.lstVDR.SelCount = 0 Then
        MsgBox "Informe qual tabela dever� contada.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Set vSessaoBatch = CreateObject("oracleinproCServer.xorasession")
    Set vBancoBatch = vSessaoBatch.OpenDatabase("BATCH", "PRODSIC/BATCH", 0&)

    lsvTempos.ListItems.Clear

    pb.Value = 0
        
    pb.Max = lstGenericas.SelCount + lstEspecificas.SelCount * IIf(Trim(txtCodPaacRepres) <> "", 1, lstEspecificas.SelCount) + lstVDR.SelCount

'****************
'Inicio GENERCIAS
    vBancoBatch.Parameters.Remove "vQtde"
    vBancoBatch.Parameters.Add "vQtde", 0, 2
        
    If lstGenericas.SelCount > 0 Then
        If FGerar(lstGenericas, "APLICACAO") = True Then fGerarGenericas "APLICACAO", 9999, "PR_APLICACAO"
        If FGerar(lstGenericas, "BANCO") = True Then fGerarGenericas "BANCO", 9999, "PR_BANCO"
        If FGerar(lstGenericas, "CATEG_SINAL") = True Then fGerarGenericas "CATEG_SINAL", 9999, "PR_CATEG_SINAL"
        If FGerar(lstGenericas, "CIDADE") = True Then fGerarGenericas "CIDADE", 9999, "PR_CIDADE"
        If FGerar(lstGenericas, "CLIE_MENSAGEM") = True Then fGerarGenericas "CLIE_MENSAGEM", 9999, "PR_CLIE_MENSAGEM"
        If FGerar(lstGenericas, "DATAS") = True Then fGerarGenericas "DATAS", 9999, "PR_DATAS"
        If FGerar(lstGenericas, "DOLAR_DIARIO") = True Then fGerarGenericas "DOLAR_DIARIO", 9999, "PR_DOLAR_DIARIO"
        If FGerar(lstGenericas, "FILIAL") = True Then fGerarGenericas "FILIAL", 9999, "PR_FILIAL"
        If FGerar(lstGenericas, "FORNECEDOR") = True Then fGerarGenericas "FORNECEDOR", 9999, "PR_FORNECEDOR"
        If FGerar(lstGenericas, "GRUPO") = True Then fGerarGenericas "GRUPO", 9999, "PR_GRUPO"
        If FGerar(lstGenericas, "ITEM_CADASTRO") = True Then fGerarGenericas "ITEM_CADASTRO", 9999, "PR_ITEM_CADASTRO"
        If FGerar(lstGenericas, "MONTADORA") = True Then fGerarGenericas "MONTADORA", 9999, "PR_MONTADORA"
        If FGerar(lstGenericas, "PLANO_PGTO") = True Then fGerarGenericas "PLANO_PGTO", 9999, "PR_PLANO_PGTO"
        If FGerar(lstGenericas, "SUBGRUPO") = True Then fGerarGenericas "SUBGRUPO", 9999, "PR_SUBGRUPO"
        If FGerar(lstGenericas, "SUBST_TRIBUTARIA") = True Then fGerarGenericas "SUBST_TRIBUTARIA", 9999, "PR_SUBST_TRIBUTARIA"
        If FGerar(lstGenericas, "TABELA_DESCPER") = True Then fGerarGenericas "TABELA_DESCPER", 9999, "PR_TABELA_DESCPER"
        If FGerar(lstGenericas, "TABELA_VENDA") = True Then fGerarGenericas "TABELA_VENDA", 9999, "PR_TABELA_VENDA"
        If FGerar(lstGenericas, "TIPO_CLIENTE") = True Then fGerarGenericas "TIPO_CLIENTE", 9999, "PR_TIPO_CLIENTE"
        If FGerar(lstGenericas, "TRANSPORTADORA") = True Then fGerarGenericas "TRANSPORTADORA", 9999, "PR_TRANSPORTADORA"
        If FGerar(lstGenericas, "UF") = True Then fGerarGenericas "UF", 9999, "PR_UF"
        If FGerar(lstGenericas, "UF_ORIGEM_DESTINO") = True Then fGerarGenericas "UF_ORIGEM_DESTINO", 9999, "PR_UF_ORIGEM_DESTINO"
        If FGerar(lstGenericas, "UF_DPK") = True Then fGerarGenericas "UF_DPK", 9999, "PR_UF_DPK"
        If FGerar(lstGenericas, "UF_TPCLIENTE") = True Then fGerarGenericas "UF_TPCLIENTE", 9999, "PR_UF_TPCLIENTE"
        If FGerar(lstGenericas, "FRETE_UF_BLOQ") = True Then fGerarGenericas "FRETE_UF_BLOQ", 9999, "PR_FRETE_UF_BLOQ"
        If FGerar(lstGenericas, "NATUREZA_OPERACAO") = True Then fGerarGenericas "NATUREZA_OPERACAO", 9999, "PR_NATUREZA_OPERACAO"
        If FGerar(lstGenericas, "TIPO_CLIENTE_BLAU") = True Then fGerarGenericas "TIPO_CLIENTE_BLAU", 9999, "PR_TIPO_CLIENTE_BLAU"
        If FGerar(lstGenericas, "CANCEL_PEDNOTA") = True Then fGerarGenericas "CANCEL_PEDNOTA", 9999, "PR_CANCEL_PEDNOTA"
        If FGerar(lstGenericas, "R_FILDEP") = True Then fGerarGenericas "R_FILDEP", 9999, "PR_R_FIL_DEP"
        If FGerar(lstGenericas, "EMBALADOR") = True Then fGerarGenericas "EMBALADOR", 9999, "PR_EMBALADOR"
        If FGerar(lstGenericas, "FORNECEDOR_ESPECIFICO") = True Then fGerarGenericas "FORNECEDOR_ESPECIFICO", 9999, "PR_FORNECEDOR_ESPECIFICO"
        If FGerar(lstGenericas, "R_DPK_EQUIV") = True Then fGerarGenericas "R_DPK_EQUIV", 9999, "PR_R_DPK_EQUIV"
        If FGerar(lstGenericas, "R_UF_DEPOSITO") = True Then fGerarGenericas "R_UF_DEPOSITO", 9999, "PR_R_UF_DEPOSITO"
        If FGerar(lstGenericas, "RESULTADO") = True Then fGerarGenericas "RESULTADO", 9999, "PR_RESULTADO"
        If FGerar(lstGenericas, "ANTECIPACAO_TRIBUTARIA") = True Then fGerarGenericas "ANTECIPACAO_TRIBUTARIA", 9999, "PR_ANTEC_TRIBUTARIA"
        If FGerar(lstGenericas, "CLASS_ANTEC_ENTRADA") = True Then fGerarGenericas "CLASS_ANTEC_ENTRADA", 9999, "PR_CLASS_ANTEC_ENTRADA"
        If FGerar(lstGenericas, "VENDA_LIMITADA") = True Then fGerarGenericas "VENDA_LIMITADA", 9999, "PR_VENDA_LIMITADA"
        If FGerar(lstGenericas, "R_FABRICA_DPK") = True Then fGerarGenericas "R_FABRICA_DPK", 9999, "PR_R_FABRICA_DPK"
        If FGerar(lstGenericas, "CLASS_FISCAL_RED_BASE") = True Then fGerarGenericas "CLASS_FISCAL_RED_BASE", 9999, "PR_CLASS_FISCAL_RED_BASE"
        If FGerar(lstGenericas, "ITEM_GLOBAL") = True Then fGerarGenericas "ITEM_GLOBAL", 9999, "PR_ITEM_GLOBAL"
        If FGerar(lstGenericas, "UF_CATEG") = True Then fGerarGenericas "UF_CATEG", 9999, "PR_UF_CATEG"
        If FGerar(lstGenericas, "SALDO_PEDIDOS") = True Then fGerarGenericas "SALDO_PEDIDOS", 9999, "PR_SALDO_PEDIDOS"
        If FGerar(lstGenericas, "CLIENTE_CARACTERISTICA") = True Then fGerarGenericas "CLIENTE_CARACTERISTICA", 9999, "PR_CLIENTE_CARACT"
    End If
    
    If lstVDR.SelCount > 0 Then
    
        If FGerar(lstVDR, "VDR_CLIENTE_CATEG_VDR") = True Then fGerarGenericas "VDR_CLIENTE_CATEG_VDR", 9999, "PR_CLIENTE_CATEG_VDR"
        If FGerar(lstVDR, "VDR_CONTROLE_VDR") = True Then fGerarGenericas "VDR_CONTROLE_VDR", 9999, "PR_CONTROLE_VDR"
        If FGerar(lstVDR, "VDR_DESCONTO_CATEG") = True Then fGerarGenericas "VDR_DESCONTO_CATEG", 9999, "PR_DESCONTO_CATEG"
        If FGerar(lstVDR, "VDR_ITEM_PRECO") = True Then fGerarGenericas "VDR_ITEM_PRECO", 9999, "PR_VDR_ITEM_PRECO"
        If FGerar(lstVDR, "VDR_R_CLIE_LINHA_PRODUTO") = True Then fGerarGenericas "VDR_R_CLIE_LINHA_PRODUTO", 9999, "PR_R_CLIE_LINHA_PRODUTO"
        If FGerar(lstVDR, "VDR_TABELA_VENDA") = True Then fGerarGenericas "VDR_TABELA_VENDA", 9999, "PR_VDR_TABELA_VENDA"

    
    End If
    
    If lstPAACs.SelCount > 0 And Val(txtCodPaacRepres) <> 0 Then
        For i = 1 To msfControlePaac.Rows - 1
                    
            If Trim(txtCodPaacRepres) <> "" Then
                If msfControlePaac.TextMatrix(i, 0) <> txtCodPaacRepres Then
                   GoTo ProximoP
                End If
            End If
            
            '*****************************************
            'TABELAS ESPECIFICAS POR DEPOSITO x FILIAL
            '*****************************************
            vBanco.Parameters.Remove "vTipoBase"
            vBanco.Parameters.Add "vTipoBase", "P", 1
            vBanco.Parameters.Remove "TIPO_BASE"
            vBanco.Parameters.Add "TIPO_BASE", "C", 1
            vBanco.Parameters.Remove "PM_CODIGO"
            vBanco.Parameters.Add "PM_CODIGO", msfControlePaac.TextMatrix(i, 0), 1
            Criar_Cursor "vCursor"
    
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL400CAD.PR_VDR(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
    
            Set vRst = vBanco.Parameters("vCursor").Value
        
            vBanco.Parameters.Remove "CodFilial"
            vBanco.Parameters.Add "CodFilial", vRst!cod_filial, 1
            
            vCodFilial = vRst!cod_filial
            
            vFL_VDR = vRst!FL_VDR
    
            Criar_Cursor "vCursor"
    
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL400CAD.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
          
            Set vRst = vBanco.Parameters("vCursor").Value
          
            If FGerar(lstEspecificas, "ALIQUOTA_ME") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ALIQUOTA_ME", "ALIQUOTA_ME", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "CESTA_ITEM") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CESTA_ITEM", "CESTA_ITEM", "1", vCodFilial 'ok
            If FGerar(lstEspecificas, "CESTA_VENDA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CESTA_VENDA", "CESTA_VENDA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIE_CREDITO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CLIE_CREDITO", "CLIE_CREDITO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIE_ENDERECO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CLIE_ENDERECO", "CLIE_ENDERECO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIENTE_INTERNET") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CLIE_INTERNET", "CLIENTE_INTERNET", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIENTE") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CLIENTE", "CLIENTE", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIENTE_ACUMULADO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_CLIENTE_ACUMULADO", "CLIENTE_ACUMULADO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "DEPOSITO_VISAO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_DEPOSITO_VISAO", "DEPOSITO_VISAO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "DUPLICATAS") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_DUPLICATAS", "DUPLICATAS", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_ENTREGA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_FRETE_ENTREGA", "FRETE_ENTREGA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_UF") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_FRETE_UF", "FRETE_UF", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_UF_TRANSP") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_FRETE_UF_TRANSP", "FRETE_UF_TRANSP", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_ANALITICO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ITEM_ANALITICO", "ITEM_ANALITICO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_ESTOQUE") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ITEM_ESTOQUE", "ITEM_ESTOQUE", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_PRECO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ITEM_PRECO", "ITEM_PRECO", "1", vCodFilial  'OK
            If FGerar(lstEspecificas, "ITPEDNOTA_VENDA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ITPEDNOTA_VENDA", "ITPEDNOTA_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "LOJA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_LOJA", "LOJA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "PEDNOTA_VENDA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_PEDNOTA_VENDA", "PEDNOTA_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_CLIE_REPRES") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_R_CLIE_REPRES", "R_CLIE_REPRES", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_PEDIDO_CONF") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_R_PEDIDO_CONF", "R_PEDIDO_CONF", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_REPCGC") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_R_REPCGC", "R_REPCGC", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_REPVEN") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_R_REPVEN", "R_REPVEN", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPR_END_CORRESP") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_REPR_END_CORRESP", "REPR_END_CORRESP", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPRESENTACAO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_REPRESENTACAO", "REPRESENTACAO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPRESENTANTE") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_REPRESENTANTE", "REPRESENTANTE", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRANQUEADOR") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_FRANQUEADOR", "FRANQUEADOR", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "ROMANEIO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_ROMANEIO", "ROMANEIO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "UF_DEPOSITO") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_UF_DEPOSITO", "UF_DEPOSITO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "V_PEDLIQ_VENDA") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_V_PEDLIQ_VENDA", "V_PEDLIQ_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "V_CLIENTE_FIEL") = True Then fEspecifica "P", vRst!Tipo_Loja, msfControlePaac.TextMatrix(i, 0), "PR_V_CLIENTE_FIEL", "V_CLIENTE_FIEL", "2", vCodFilial 'OK
ProximoP:
        
        Next
    ElseIf Me.lstRepresentantes.SelCount > 0 And Val(txtCodPaacRepres) <> 0 Then
        For i = 1 To msfControleRepres.Rows - 1
            
            If Trim(txtCodPaacRepres) <> "" Then
                If msfControleRepres.TextMatrix(i, 0) <> txtCodPaacRepres Then
                   GoTo ProximoR
                End If
            End If
            
            '*****************************************
            'TABELAS ESPECIFICAS POR DEPOSITO x FILIAL
            '*****************************************
            vBanco.Parameters.Remove "vTipoBase"
            vBanco.Parameters.Add "vTipoBase", "R", 1
            vBanco.Parameters.Remove "TIPO_BASE"
            vBanco.Parameters.Add "TIPO_BASE", "C", 1
            vBanco.Parameters.Remove "PM_CODIGO"
            vBanco.Parameters.Add "PM_CODIGO", Val(msfControleRepres.TextMatrix(i, 0)), 1
            Criar_Cursor "vCursor"
    
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL400CAD.PR_VDR(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
    
            Set vRst = vBanco.Parameters("vCursor").Value
        
            vBanco.Parameters.Remove "CodFilial"
            vBanco.Parameters.Add "CodFilial", vRst!cod_repres, 1
            
            vCodFilial = vRst!cod_repres
            
            vFL_VDR = vRst!FL_VDR
    
            Criar_Cursor "vCursor"
    
            vBanco.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL400CAD.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
          
            Set vRst = vBanco.Parameters("vCursor").Value
          
            If FGerar(lstEspecificas, "ALIQUOTA_ME") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ALIQUOTA_ME", "ALIQUOTA_ME", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "CESTA_ITEM") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CESTA_ITEM", "CESTA_ITEM", "1", vCodFilial 'ok
            If FGerar(lstEspecificas, "CESTA_VENDA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CESTA_VENDA", "CESTA_VENDA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIE_CREDITO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CLIE_CREDITO", "CLIE_CREDITO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIE_ENDERECO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CLIE_ENDERECO", "CLIE_ENDERECO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIE_INTERNET") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CLIE_INTERNET", "CLIE_INTERNET", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIENTE") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CLIENTE", "CLIENTE", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "CLIENTE_ACUMULADO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_CLIENTE_ACUMULADO", "CLIENTE_ACUMULADO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "DEPOSITO_VISAO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_DEPOSITO_VISAO", "DEPOSITO_VISAO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "DUPLICATAS") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_DUPLICATAS", "DUPLICATAS", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_ENTREGA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_FRETE_ENTREGA", "FRETE_ENTREGA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_UF") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_FRETE_UF", "FRETE_UF", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRETE_UF_TRANSP") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_FRETE_UF_TRANSP", "FRETE_UF_TRANSP", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_ANALITICO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ITEM_ANALITICO", "ITEM_ANALITICO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_ESTOQUE") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ITEM_ESTOQUE", "ITEM_ESTOQUE", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "ITEM_PRECO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ITEM_PRECO", "ITEM_PRECO", "1", vCodFilial  'OK
            If FGerar(lstEspecificas, "ITPEDNOTA_VENDA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ITPEDNOTA_VENDA", "ITPEDNOTA_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "LOJA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_LOJA", "LOJA", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "PEDNOTA_VENDA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_PEDNOTA_VENDA", "PEDNOTA_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_CLIE_REPRES") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_R_CLIE_REPRES", "R_CLIE_REPRES", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_PEDIDO_CONF") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_R_PEDIDO_CONF", "R_PEDIDO_CONF", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_REPCGC") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_R_REPCGC", "R_REPCGC", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "R_REPVEN") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_R_REPVEN", "R_REPVEN", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPR_END_CORRESP") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_REPR_END_CORRESP", "REPR_END_CORRESP", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPRESENTACAO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_REPRESENTACAO", "REPRESENTACAO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "REPRESENTANTE") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_REPRESENTANTE", "REPRESENTANTE", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "FRANQUEADOR") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_FRANQUEADOR", "FRANQUEADOR", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "ROMANEIO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_ROMANEIO", "ROMANEIO", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "UF_DEPOSITO") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_UF_DEPOSITO", "UF_DEPOSITO", "1", vCodFilial 'OK
            If FGerar(lstEspecificas, "V_PEDLIQ_VENDA") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_V_PEDLIQ_VENDA", "V_PEDLIQ_VENDA", "2", vCodFilial 'OK
            If FGerar(lstEspecificas, "V_CLIENTE_FIEL") = True Then fEspecifica "R", vRst!Tipo_Loja, msfControleRepres.TextMatrix(i, 0), "PR_V_CLIENTE_FIEL", "V_CLIENTE_FIEL", "2", vCodFilial 'OK
ProximoR:
        Next
    End If
    
Set vBancoBatch = Nothing
Set vSessaoBatch = Nothing
    
End Sub
Sub Inserir_Qtde(pCodRepres As Long, pTipo As String, pTabela As String, pQtde As Long)
    If pb.Max <> pb.Value Then pb.Value = pb.Value + 1
    
    vBanco.Parameters.Remove "CodRepres"
    vBanco.Parameters.Add "CodRepres", pCodRepres, 1
        
    vBanco.Parameters.Remove "Tipo"
    vBanco.Parameters.Add "Tipo", pTipo, 1
        
    vBanco.Parameters.Remove "Tabela"
    vBanco.Parameters.Add "TABELA", pTabela, 1
        
    vBanco.Parameters.Remove "Qtde"
    vBanco.Parameters.Add "Qtde", pQtde, 1
        
    vBanco.Parameters.Remove "CodErro"
    vBanco.Parameters.Add "CodErro", 0, 2
        
    vBanco.Parameters.Remove "TxtErro"
    vBanco.Parameters.Add "TxtErro", "", 2
        
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_INSERT_QTDE(:CodRepres, :Tipo, :Tabela, :Qtde, :CodErro, :TxtErro); END;"

    If vBanco.Parameters("CodErro").Value <> 0 Then
        MsgBox "Sub Inerir_Qtde" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description
    End If
End Sub


Function fEspecifica(pTipoBase As String, pTipoLoja As Long, pCodRepres As Long, pProcedure As String, pTabela As String, pCdNovo As String, pFilial As Integer)
    
    
    vBancoBatch.Parameters.Remove "TipoBase"
    vBancoBatch.Parameters.Add "TipoBase", pTipoBase, 1
    
    vBancoBatch.Parameters.Remove "Cod_Filial"
    vBancoBatch.Parameters.Add "Cod_Filial", pFilial, 1
    
    vBancoBatch.Parameters.Remove "TIPO_LOJA"
    vBancoBatch.Parameters.Add "TIPO_LOJA", pTipoLoja, 1
    
    vBancoBatch.Parameters.Remove "CODPAACREPRES"
    vBancoBatch.Parameters.Add "CODPAACREPRES", pCodRepres, 1
    
    vBancoBatch.Parameters.Remove "vQTDE"
    vBancoBatch.Parameters.Add "vQTDE", 0, 2
    
    Preencher_Lsv_Tempos pCodRepres, pTabela, "I", 0
    
    If pCdNovo = "1" Then
        vBancoBatch.ExecuteSQL "BEGIN PRODSIC.PCK_CONTROLE_QTDE." & pProcedure & "(:Tipo_Loja,:vQTDE); END;"
    ElseIf pCdNovo = "2" Then
        vBancoBatch.ExecuteSQL "BEGIN PRODSIC.PCK_CONTROLE_QTDE." & pProcedure & "(:TipoBase,:Cod_Filial, :vQTDE); END;"
    End If
    Preencher_Lsv_Tempos pCodRepres, "", "F", vBancoBatch.Parameters("vQtde").Value
    
    Inserir_Qtde pCodRepres, pTipoBase, pTabela, vBancoBatch("vqtde").Value
End Function

Sub Preencher_Lsv_Tempos(pCod As Long, pTabela As String, pInicio_Fim As String, pQtd As Long)
    Dim litem As ListItem
    
    If pInicio_Fim = "I" Then
        Set litem = lsvTempos.ListItems.Add
        litem = pCod
        litem.SubItems(1) = pTabela
        litem.SubItems(2) = Now
        litem.Selected = True
    Else
        Set litem = lsvTempos.SelectedItem
        litem.SubItems(3) = Now
        litem.SubItems(4) = DateDiff("s", litem.SubItems(2), litem.SubItems(3))
        litem.SubItems(5) = pQtd
    End If
End Sub

Function fGerarGenericas(pTabela As String, pCodPaacRepres As Long, pProcedure As String)
    
    Preencher_Lsv_Tempos "9999", UCase(pTabela), "I", 0
    vBancoBatch.ExecuteSQL "BEGIN PRODSIC.PCK_CONTROLE_QTDE." & pProcedure & "(:vQtde); END;"
    Preencher_Lsv_Tempos pCodPaacRepres, "", "F", vBancoBatch.Parameters("vQtde").Value
    Inserir_Qtde pCodPaacRepres, "P", pTabela, vBancoBatch.Parameters("vQtde").Value
    
End Function

Function FGerar(LST As ListBox, Tabela As String) As Boolean
    Dim a As Integer
    For a = 0 To LST.ListCount - 1
        If UCase(LST.List(a)) = UCase(Tabela) Then
            If LST.Selected(a) = True Then
                FGerar = True
                Exit For
            End If
        End If
    Next
End Function

Sub Marcar_lst(pLst As ListBox)
    If pLst.SelCount > 0 Then
        For i = 0 To pLst.ListCount - 1
            pLst.Selected(i) = True
        Next
    Else
        For i = 0 To pLst.ListCount - 1
            pLst.Selected(i) = False
        Next
    End If
End Sub

Sub Preencher_Registros_Comparacao_PAAC()
    
    On Error GoTo Trata_Erro
    
    Dim C As Byte 'Coluna
    Dim L As Integer 'L
    Dim vRst As Object
    Dim LinGrid As Integer
    
    frmLoad.PB4.Value = 0
    
    Criar_Cursor "vCursor_Com"
    
    vBanco.Parameters.Remove "vTipo"
    vBanco.Parameters.Add "vTipo", "P", 1
    
    vBanco.Parameters.Remove "vGE"
    vBanco.Parameters.Add "vGE", "G", 1
    
    vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_QTD_COMPLETA(:vCursor_Com, :vTIPO, :vGE); END;"
    
    Set vRst = Nothing
    Set vRst = vBanco.Parameters("vCursor_Com").Value
    
    frmLoad.PB4.Max = vRst.RecordCount
            
    vBanco.Parameters.Remove "vCursor_Com"
        
    frmLoad.PB4.Value = frmLoad.PB4.Value + 1
        
    msfControlePaac.TextMatrix(1, 1) = txtData
    For C = 2 To msfControlePaac.Cols - 1
        If msfControlePaac.TextMatrix(0, C) = "" Then Exit For
        For i = 1 To vRst.RecordCount
            If UCase(msfControlePaac.TextMatrix(0, C)) = UCase(vRst!Tabela) Then
                msfControlePaac.TextMatrix(1, C) = vRst!REGISTROS
                msfControlePaac.Row = 1
                msfControlePaac.Col = C
                msfControlePaac.CellForeColor = vbBlue
                msfControlePaac.CellFontBold = True
                vRst.MoveFirst
                Exit For
            Else
                vRst.MoveNext
            End If
        Next
    Next
    
    frmLoad.PB4.Value = frmLoad.PB4.Max
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Preencher_Registros_Comparacao_PAAC" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Sub Preencher_Registros_Comparacao_REPRES()
          
1         On Error GoTo Trata_Erro
          
          Dim C As Byte 'Coluna
          Dim L As Integer 'L
          Dim vRst As Object
          Dim LinGrid As Integer
          
2         frmLoad.PB5.Value = 0
          
3         Criar_Cursor "vCursor_Com"
          
4         vBanco.Parameters.Remove "vTipo"
5         vBanco.Parameters.Add "vTipo", "P", 1 'Aqui � P porque todas as tabelas genericas estao cadastradas como P
          
6         vBanco.Parameters.Remove "vGE"
7         vBanco.Parameters.Add "vGE", "G", 1
          
8         vBanco.ExecuteSQL "BEGIN PAAC.PCK_CONTROLE_PAAC_REPRES.PR_SELECT_QTD_COMPLETA(:vCursor_Com, :vTIPO, :vGE); END;"
          
9         Set vRst = Nothing
10        Set vRst = vBanco.Parameters("vCursor_Com").Value
11        vBanco.Parameters.Remove "vCursor_com"
          
12        frmLoad.PB5.Max = msfControleRepres.Rows
          
13        For L = 1 To vRst.RecordCount
14            msfControleRepres.TextMatrix(1, 1) = txtData
15            msfControleRepres.Row = 1
16            msfControleRepres.Col = 1
17            msfControleRepres.CellFontBold = True
18            msfControleRepres.CellForeColor = vbBlue
                      
19            For C = 0 To msfControleRepres.Cols - 1
20                If UCase(msfControleRepres.TextMatrix(0, C)) = UCase(vRst!Tabela) Then
21                   msfControleRepres.TextMatrix(1, C) = vRst!REGISTROS
22                   msfControleRepres.Row = 1
23                   msfControleRepres.Col = C
24                   msfControleRepres.CellFontBold = True
25                   msfControleRepres.CellForeColor = vbBlue
26                   Exit For
27                End If
28            Next
29            vRst.MoveNext
30        Next
31        frmLoad.PB5.Value = frmLoad.PB5.Max
          
Trata_Erro:
32        If Err.Number <> 0 Then
33            MsgBox "Sub Preencher_Registros_Comparacao_Repres" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
34        End If
End Sub

Sub Nomes_Tabelas(pGrid As MSFlexGrid)
    pGrid.TextMatrix(0, 1) = "Atualizacao"
    pGrid.TextMatrix(0, 2) = "Antecipacao_Tributaria"
    pGrid.TextMatrix(0, 3) = "Aplicacao"
    pGrid.TextMatrix(0, 4) = "Banco"
    pGrid.TextMatrix(0, 5) = "Cancel_Pednota"
    pGrid.TextMatrix(0, 6) = "Categ_Sinal"
    pGrid.TextMatrix(0, 7) = "Cidade"
    pGrid.TextMatrix(0, 8) = "Class_Antec_Entrada"
    pGrid.TextMatrix(0, 9) = "Class_Fiscal_Red_Base"
    pGrid.TextMatrix(0, 10) = "Clie_Mensagem"
    pGrid.TextMatrix(0, 11) = "Datas"
    pGrid.TextMatrix(0, 12) = "Dolar_Diario"
    pGrid.TextMatrix(0, 13) = "Embalador"
    pGrid.TextMatrix(0, 14) = "Filial"
    pGrid.TextMatrix(0, 15) = "Fornecedor"
    pGrid.TextMatrix(0, 16) = "Fornecedor_Especifico"
    pGrid.TextMatrix(0, 17) = "Frete_UF_Bloq"
    pGrid.TextMatrix(0, 18) = "GRUPO"
    pGrid.TextMatrix(0, 19) = "Item_Cadastro"
    pGrid.TextMatrix(0, 20) = "Montadora"
    pGrid.TextMatrix(0, 21) = "Natureza_Operacao"
    pGrid.TextMatrix(0, 22) = "Plano_Pgto"
    pGrid.TextMatrix(0, 23) = "R_DPK_Equiv"
    pGrid.TextMatrix(0, 24) = "R_Fabrica_DPK"
    pGrid.TextMatrix(0, 25) = "R_FilDep"
    pGrid.TextMatrix(0, 26) = "R_UF_Deposito"
    pGrid.TextMatrix(0, 27) = "Resultado"
    pGrid.TextMatrix(0, 28) = "SubGrupo"
    pGrid.TextMatrix(0, 29) = "Subst_Tributaria"
    pGrid.TextMatrix(0, 30) = "Tabela_DescPer"
    pGrid.TextMatrix(0, 31) = "Tabela_Venda"
    pGrid.TextMatrix(0, 32) = "Tipo_Cliente"
    pGrid.TextMatrix(0, 33) = "Tipo_Cliente_Blau"
    pGrid.TextMatrix(0, 34) = "Transportadora"
    pGrid.TextMatrix(0, 35) = "Transferencia"
    pGrid.TextMatrix(0, 36) = "UF"
    pGrid.TextMatrix(0, 37) = "UF_DPK"
    pGrid.TextMatrix(0, 38) = "UF_Origem_Destino"
    pGrid.TextMatrix(0, 39) = "UF_TPCliente"
    pGrid.TextMatrix(0, 40) = "Venda_Limitada"
End Sub

Sub Pintar_Valores_Errados()
          
1         On Error GoTo Trata_Erro
          
          Dim vPaacSemAtu As String
          Dim vReprSemAtu As String
          Dim vPaacComDif As String
          Dim vReprComDif As String
          
          'Pintar_valores
          'Pintar Linhas impares, pois elas contem os valores para comparacao
          'Colocar 0 (zero) em todos os campos em branco
           
2         frmLoad.PB10.Value = 0
3         frmLoad.PB10.Max = msfControlePaac.Rows
           'PAAC
4          For i = 1 To msfControlePaac.Rows - 1
5              frmLoad.PB10.Value = frmLoad.PB10.Value + 1
6              msfControlePaac.Row = i
               'Verificar se tem data de atualizacao do PAAC, se nao tiver preencher a variavel para email
7              If msfControlePaac.TextMatrix(i, 1) = "" Then
8                 If vPaacSemAtu = "" Then vPaacSemAtu = "ATUALIZACAO - PAACs que nao fizeram atualizacao: " & vbCrLf
9                    vPaacSemAtu = vPaacSemAtu & msfControlePaac.TextMatrix(i, 0) & ", "
10                   For ii = 2 To msfControlePaac.Cols - 1
11                       msfControlePaac.Col = ii
12                       msfControlePaac.TextMatrix(i, ii) = 0
13                   Next
14                Else
15                   For ii = 1 To msfControlePaac.Cols - 1
16                       If msfControlePaac.TextMatrix(0, ii) <> "" Then
17                            If msfControlePaac.TextMatrix(i, ii) <> msfControlePaac.TextMatrix(1, ii) Then
18                               Pintar_Qtdes_Com_Dif msfControlePaac, CInt(i), CInt(ii)
19                               If msfControlePaac.TextMatrix(0, ii) <> "" Then
20                                  If vPaacComDif = "" Then
21                                     vPaacComDif = "PAACs com Diferenca na quantidade de registros: "
22                                  End If
23                                  If InStr(1, vPaacComDif, msfControlePaac.TextMatrix(i, 0)) = 0 Then vPaacComDif = vPaacComDif & "," & msfControlePaac.TextMatrix(i, 0)
24                               End If
25                            End If
26                        End If
27                    Next
28                End If
29        Next
30        frmLoad.PB10.Value = frmLoad.PB10.Max
          
          '******
          'REPRES
          '******
31        frmLoad.PB11.Value = 0
32        frmLoad.PB11.Max = msfControleRepres.Rows
33        For i = 2 To msfControleRepres.Rows - 1
34            frmLoad.PB11.Value = frmLoad.PB11.Value + 1
35            msfControleRepres.Row = i
                  'Verificar se tem data de atualizacao do PAAC, se nao tiver preencher a variavel para email
36                If msfControleRepres.TextMatrix(i, 1) = "" Then
37                   If vReprSemAtu = "" Then vReprSemAtu = "ATUALIZACAO - Representantes que nao fizeram atualizacao: "
38                   vReprSemAtu = vReprSemAtu & msfControleRepres.TextMatrix(i, 0) & ", "
39                   For ii = 1 To msfControleRepres.Cols - 1
40                       msfControleRepres.Col = ii
41                       msfControleRepres = 0
42                   Next
43                Else
44                   For ii = 1 To msfControleRepres.Cols - 1
45                          If msfControleRepres.TextMatrix(i, ii) <> msfControleRepres.TextMatrix(1, ii) Then
46                             Pintar_Qtdes_Com_Dif msfControleRepres, CInt(i), CInt(ii)
47                             If msfControleRepres.TextMatrix(0, ii) <> "" Then
48                                If vReprComDif = "" Then
49                                    vReprComDif = "Representantes com Diferenca na quantidade de registros: "
50                                End If
51                                If InStr(1, vReprComDif, msfControleRepres.TextMatrix(i, 0)) = 0 Then
52                                    vReprComDif = vReprComDif & "," & msfControleRepres.TextMatrix(i, 0) '& Space(10 - Len(msfControleRepres.TextMatrix(i, 0))) & msfControleRepres.TextMatrix(0, ii) & Space(30 - Len(msfControleRepres.TextMatrix(0, ii))) & msfControleRepres.TextMatrix(i, ii) & Space(10 - Len(msfControleRepres.TextMatrix(i, ii))) & msfControleRepres.TextMatrix(i - 1, ii)
53                                End If
54                             End If
55                          End If
56                   Next
57                End If
58        Next
59        If Len(vReprSemAtu) > 0 Or Len(vReprComDif) > 0 Or Len(vPaacSemAtu) > 0 Or Len(vPaacComDif) > 0 Then
60            Gerar_Email vPaacSemAtu, vReprSemAtu, "ATULIZACAO - PAAC sem atualizacao", "ATUALIZACAO - REPRESENTANTES sem atualizacao"
              'Gerar_Email vPaacComDif, vReprComDif, "DIFERENCA - PAAC com diferenca", "DIFERENCA - REPRESENTANTES com diferenca"
61        End If
          
62        frmLoad.PB11.Value = frmLoad.PB11.Max
          
63        Gerar_Email_Versao
          'Gerar_Email_Atualizacao lsvErrosPAAC

Trata_Erro:
64        If Err.Number <> 0 Then
65            MsgBox "Sub Pintar_Valores_Errados" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
66        End If

End Sub

Sub Gerar_Email(pPAAC As String, pREPRES As String, pTituloPAAC As String, pTituloRepres As String)
        
    Dim vSessaoIDPK As Object
    Dim vBancoIDPK As Object
    Dim i As Integer
          
    Set vSessaoIDPK = CreateObject("oracleinprocserver.xorasession")
    Set vBancoIDPK = vSessaoIDPK.OpenDatabase("IDPK", "INTRANET/PROD", 0&)
        
    For i = 0 To lstEmail.ListCount - 1

        vBancoIDPK.Parameters.Remove "Para"
        vBancoIDPK.Parameters.Add "Para", lstEmail.List(i), 1

        vBancoIDPK.Parameters.Remove "Titulo"
        vBancoIDPK.Parameters.Add "Titulo", pTituloPAAC, 1

        vBancoIDPK.Parameters.Remove "Conteudo"
        vBancoIDPK.Parameters.Add "Conteudo", pPAAC, 1

        vBancoIDPK.ExecuteSQL "BEGIN INTRANET.ENVIA_EMAIL (:para, :titulo, :conteudo); END;"

        vBancoIDPK.Parameters.Remove "Titulo"
        vBancoIDPK.Parameters.Add "Titulo", pTituloRepres, 1

        vBancoIDPK.Parameters.Remove "Conteudo"
        vBancoIDPK.Parameters.Add "Conteudo", pREPRES, 1

        vBancoIDPK.ExecuteSQL "BEGIN INTRANET.ENVIA_EMAIL (:para, :titulo, :conteudo); END;"

    Next
    
    Set vSessaoIDPK = Nothing
    Set vBancoIDPK = Nothing
    
End Sub


Sub Pintar_Qtdes_Com_Dif(pGrid As MSFlexGrid, pLinha As Integer, pColuna As Integer)
    
    If pGrid.TextMatrix(pLinha, pColuna) <> pGrid.TextMatrix(1, pColuna) Then
        
        pGrid.Row = pLinha
        pGrid.Col = pColuna
        pGrid.CellForeColor = &H8080FF
        pGrid.CellFontBold = True
        
        pGrid.Col = 0
        pGrid.CellBackColor = &H8080FF
        
        pGrid.Row = 0
        pGrid.Col = pColuna
        pGrid.CellBackColor = &H8080FF
                            
    End If

End Sub

Sub Pintar_Grid(pGrid As MSHFlexGrid)
    Dim vLinha_inicial As Integer
    Dim vLinha_Antiga As Integer
    Dim vColunas As Integer
    
    Dim a As Byte
    
    vColunas = pGrid.Cols - 1
    pGrid.Row = Val(pGrid.Tag) - 1
        
Pintar:
    For a = 1 To vColunas
        pGrid.Row = Val(pGrid.Tag) 'vLinha_inicial
        pGrid.Col = a
        pGrid.CellBackColor = vbYellow
    Next
    pGrid.Tag = vLinha_inicial
    pGrid.Col = 1
    pGrid.Row = volinha_inicial
    
    
End Sub

Sub Gerar_Email_Versao()
    Dim a As Integer
    
    Dim vVersoesErradasPAAC As String
    Dim vVersoesErradasREPR As String
    Dim vSistemasErrados As String
    
    Dim vPaacSistem(100, 1) As String
    Dim vReprSistem(300, 1) As String
    
    For a = 2 To msfControleVersoesPaac.Rows - 1
        For aa = 1 To msfControleVersoesPaac.Cols - 1
            If msfControleVersoesPaac.TextMatrix(0, aa) <> "" Then
               If msfControleVersoesPaac.TextMatrix(a, aa) <> msfControleVersoesPaac.TextMatrix(1, aa) Then
                  vPaacSistem(a, 0) = msfControleVersoesPaac.TextMatrix(a, 0)
                  vPaacSistem(a, 1) = vPaacSistem(a, 1) & msfControleVersoesPaac.TextMatrix(0, aa) & ", "
               End If
            End If
        Next
    Next

    For a = 2 To msfControleVersoesRepres.Rows - 1
        For aa = 1 To msfControleVersoesRepres.Cols - 1
            If msfControleVersoesRepres.TextMatrix(0, aa) <> "" Then
               If msfControleVersoesRepres.TextMatrix(a, aa) <> msfControleVersoesRepres.TextMatrix(1, aa) Then
                  vReprSistem(a, 0) = msfControleVersoesRepres.TextMatrix(a, 0)
                  vReprSistem(a, 1) = vReprSistem(a, 1) & msfControleVersoesRepres.TextMatrix(0, aa) & ", "
               End If
            End If
        Next
    Next
    
    vVersoesErradasPAAC = "PAACs com Versao Incorreta: "
    For i = 2 To 100
        If vPaacSistem(i, 0) <> "" Then
           vVersoesErradasPAAC = vVersoesErradasPAAC & ", " & vPaacSistem(i, 0)
        End If
    Next
    
    vVersoesErradasREPR = "Representantes com Versao Incorreta: "
    For i = 2 To 300
        If vReprSistem(i, 0) <> "" Then
           vVersoesErradasREPR = vVersoesErradasREPR & ", " & vReprSistem(i, 0)
        End If
    Next
    
    Gerar_Email vVersoesErradasPAAC, vVersoesErradasREPR, "VERSAO - PAAC com versao Incorreta de sistema", "VERSAO - REPRESENTANTE com versao incorreta de sistema"

End Sub

Sub Gerar_Email_Atualizacao(pLsv As ListView)
    Dim vErrosPAAC As String
    Dim vErrosREPR As String
    
    For i = 1 To lsvErrosPAAC.ListItems.Count
        If lsvErrosPAAC.ListItems(i) <> "" Then
           vErrosPAAC = vErrosPAAC & "," & lsvErrosPAAC.ListItems(i)
        End If
    Next
    
    For i = 1 To lsvErrosRepres.ListItems.Count
        If lsvErrosRepres.ListItems(i) <> "" Then
           vErrosREPR = vErrosREPR & "," & lsvErrosRepres.ListItems(i)
        End If
    Next
    
    'Gerar_Email vErrosPAAC, vErrosREPR, "ATUALIZACAO - DIFERENCA NOS PAACS", "ATUALIZACAO - DIFERENCA NOS REPRESENTANTES"

End Sub

'Sub Carregar_Cbo_PAACREPRES()
'    Dim Rst As Object
'    Dim reg As Integer
'
'    lstPAACs.Clear
'    lstRepresentantes.Clear
'
'    Set Rst = vBanco.CreateDynaset("Select * from paac.controle_paac  ORDER BY COD_FILIAL", 0&)
'
'    frmLoad.PB9.Max = frmLoad.PB9.Value + Rst.RecordCount
'
'    For reg = 1 To Rst.RecordCount
'        frmLoad.PB9.Value = frmLoad.PB9 + 1
'        lstPAACs.AddItem Rst!cod_filial
'        lstPAACEnviar.AddItem Rst!cod_filial
'        Rst.MoveNext
'    Next
'
'    Set Rst = vBanco.CreateDynaset("select A.* From paac.controle_representante A,paac.deposito_repres b, representante C Where  B.Fl_Base = 'S' and a.cod_repres = b.cod_repres and a.cod_repres = c.cod_repres and c.situacao <> 9 ORDER BY A.COD_REPRES", 0&)
'    frmLoad.PB9.Max = frmLoad.PB9.Value + Rst.RecordCount
'    For reg = 1 To Rst.RecordCount
'        frmLoad.PB9.Value = frmLoad.PB9.Value + 1
'        lstRepresentantes.AddItem Rst!cod_repres
'        lstRepresEnviar.AddItem Rst!cod_repres
'        Rst.MoveNext
'    Next
'    frmLoad.PB9.Value = frmLoad.PB9.Max
'    Set Rst = Nothing
'End Sub


Sub Preencher_Email()

    Dim vRstEmail As Object
    
    Set vRstEmail = vBanco.CreateDynaset("Select vl_parametro from Helpdesk.parametros where cod_software=1270", 0&)

    lstEmail.Clear

    For i = 1 To vRstEmail.RecordCount
        lstEmail.AddItem vRstEmail!vl_parametro
        vRstEmail.MoveNext
    Next

    vRstEmail.Close
    Set vRstEmail = Nothing

End Sub
Sub Gerar_Qtd_Genericas()

    Set vSessao = CreateObject("oracleinproCServer.xorasession")
    Set vBanco = vSessao.OpenDatabase("SDPK_META", "FIL430/PROD", 0&)

    Set vSessaoBatch = CreateObject("oracleinproCServer.xorasession")
    Set vBancoBatch = vSessaoBatch.OpenDatabase("BATCH", "FIL430/BATCH", 0&)

    vBancoBatch.Parameters.Remove "vQtde"
    vBancoBatch.Parameters.Add "vQtde", 0, 2
        
    fGerarGenericas "APLICACAO", 9999, "PR_APLICACAO"
    fGerarGenericas "BANCO", 9999, "PR_BANCO"
    fGerarGenericas "CATEG_SINAL", 9999, "PR_CATEG_SINAL"
    fGerarGenericas "CIDADE", 9999, "PR_CIDADE"
    fGerarGenericas "CLIE_MENSAGEM", 9999, "PR_CLIE_MENSAGEM"
    fGerarGenericas "DATAS", 9999, "PR_DATAS"
    fGerarGenericas "DOLAR_DIARIO", 9999, "PR_DOLAR_DIARIO"
    fGerarGenericas "FILIAL", 9999, "PR_FILIAL"
    fGerarGenericas "FORNECEDOR", 9999, "PR_FORNECEDOR"
    fGerarGenericas "GRUPO", 9999, "PR_GRUPO"
    fGerarGenericas "ITEM_CADASTRO", 9999, "PR_ITEM_CADASTRO"
    fGerarGenericas "MONTADORA", 9999, "PR_MONTADORA"
    fGerarGenericas "PLANO_PGTO", 9999, "PR_PLANO_PGTO"
    fGerarGenericas "SUBGRUPO", 9999, "PR_SUBGRUPO"
    fGerarGenericas "SUBST_TRIBUTARIA", 9999, "PR_SUBST_TRIBUTARIA"
    fGerarGenericas "TABELA_DESCPER", 9999, "PR_TABELA_DESCPER"
    fGerarGenericas "TABELA_VENDA", 9999, "PR_TABELA_VENDA"
    fGerarGenericas "TIPO_CLIENTE", 9999, "PR_TIPO_CLIENTE"
    fGerarGenericas "TRANSPORTADORA", 9999, "PR_TRANSPORTADORA"
    fGerarGenericas "UF", 9999, "PR_UF"
    fGerarGenericas "UF_ORIGEM_DESTINO", 9999, "PR_UF_ORIGEM_DESTINO"
    fGerarGenericas "UF_DPK", 9999, "PR_UF_DPK"
    fGerarGenericas "UF_TPCLIENTE", 9999, "PR_UF_TPCLIENTE"
    fGerarGenericas "FRETE_UF_BLOQ", 9999, "PR_FRETE_UF_BLOQ"
    fGerarGenericas "NATUREZA_OPERACAO", 9999, "PR_NATUREZA_OPERACAO"
    fGerarGenericas "TIPO_CLIENTE_BLAU", 9999, "PR_TIPO_CLIENTE_BLAU"
    fGerarGenericas "CANCEL_PEDNOTA", 9999, "PR_CANCEL_PEDNOTA"
    fGerarGenericas "R_FILDEP", 9999, "PR_R_FIL_DEP"
    fGerarGenericas "EMBALADOR", 9999, "PR_EMBALADOR"
    fGerarGenericas "FORNECEDOR_ESPECIFICO", 9999, "PR_FORNECEDOR_ESPECIFICO"
    fGerarGenericas "R_DPK_EQUIV", 9999, "PR_R_DPK_EQUIV"
    fGerarGenericas "R_UF_DEPOSITO", 9999, "PR_R_UF_DEPOSITO"
    fGerarGenericas "RESULTADO", 9999, "PR_RESULTADO"
    fGerarGenericas "ANTECIPACAO_TRIBUTARIA", 9999, "PR_ANTEC_TRIBUTARIA"
    fGerarGenericas "CLASS_ANTEC_ENTRADA", 9999, "PR_CLASS_ANTEC_ENTRADA"
    fGerarGenericas "VENDA_LIMITADA", 9999, "PR_VENDA_LIMITADA"
    fGerarGenericas "R_FABRICA_DPK", 9999, "PR_R_FABRICA_DPK"
    fGerarGenericas "CLASS_FISCAL_RED_BASE", 9999, "PR_CLASS_FISCAL_RED_BASE"
    fGerarGenericas "ITEM_GLOBAL", 9999, "PR_ITEM_GLOBAL"
    fGerarGenericas "UF_CATEG", 9999, "PR_UF_CATEG"
    fGerarGenericas "SALDO_PEDIDOS", 9999, "PR_SALDO_PEDIDOS"
    fGerarGenericas "CLIENTE_CARACTERISTICA", 9999, "PR_CLIENTE_CARACT"
    fGerarGenericas "TRANSFERENCIA", 9999, "PR_TRANSFERENCIA"

    Set vSessao = Nothing
    Set vBanco = Nothing
    
    Set vSessaoBatch = Nothing
    Set vBancoBatch = Nothing
    
End Sub

Sub Gerar_Tabelas_Genericas()
1         On Error GoTo TrataErro

2         Set vSessaoBatch = CreateObject("oracleinproCServer.xorasession")
3         Set vBancoBatch = vSessaoBatch.OpenDatabase("BATCH", "analista/dpk83614", 0&)
          
4         Criar_Cursor_Batch "vCursor"

5         frmLoad.stb.Panels(1).Text = "Excluindo arquivos da pasta " & vCaminho_Genericas & "*.DBF"
          
          'Apagando DBFs da rede
6         Do While Dir(vCaminho_Genericas & "*.DBF", vbArchive) <> ""
7             vArquivo = Dir(vCaminho_Genericas & "*.DBF")
8             Kill vCaminho_Genericas & vArquivo
9         Loop
          'Apagando DBFs Locais
10        Do While Dir(vCaminho_Gerar_Genericas & "*.DBF", vbArchive) <> ""
11            vArquivo = Dir(vCaminho_Gerar_Genericas & "*.DBF")
12            Kill vCaminho_Gerar_Genericas & vArquivo
13        Loop
          
14        frmLoad.stb.Panels(1).Text = "Copiando arquivos vazios para pasta Local"
          
15        File1.Pattern = "*.DBF"
16        File1.Path = vDriveSISTEMAS_PAACS_EXECUTAVEIS & ":\SISTEMAS\PAACS\Genericas\Arquivos_Vazios"
17        File1.Refresh
          
18        For i = 0 To File1.ListCount
19            If File1.List(i) = "" Then Exit For
20            FileCopy vCaminho_Genericas & "Arquivos_Vazios\" & File1.List(i), vCaminho_Gerar_Genericas & File1.List(i)
21        Next
          
22        DataInsert.DatabaseName = vCaminho_Gerar_Genericas
          
23        ANTECIPACAO_TRIBUTARIA_dbf
24        APLICACAO_DBF
25        BANCO_DBF
26        CATEG_SINAL_dbf
27        CANCEL_PEDNOTA_dbf
28        CIDADE_DBF
29        CLASS_ANTEC_ENTRADA_DBF
30        CLASS_FISCAL_RED_BASE_DBF
31        CLIE_MENSAGEM_DBF
32        DATAS_DBF
33        DOLAR_DIARIO_DBF
34        EMBALADOR_DBF
35        Filial_DBF
36        FORNECEDOR_DBF
37        FORNECEDOR_ESPECIFICO_DBF
38        FRETE_UF_BLOQ_DBF
39        GRUPO_DBF
40        ITEM_CADASTRO_DBF
41        MONTADORA_DBF
42        NATUREZA_OPERACAO_DBF
43        PLANO_PGTO_DBF
44        R_DPK_EQUIV_DBF
45        R_FABRICA_DPK_DBF
46        R_FILDEP_DBF
47        R_UF_DEPOSITO_DBF
48        RESULTADO_DBF
49        SUBGRUPO_DBF
50        SUBST_TRIBUTARIA_DBF
51        TABELA_DESCPER_DBF
52        TABELA_VENDA_DBF
53        TIPO_CLIENTE_DBF
54        TRANSPORTADORA_DBF
55        UF_DBF
56        UF_DPK_DBF
57        UF_ORIGEM_DESTINO_DBF
58        UF_TPCLIENTE_DBF
59        TIPO_CLIENTE_BLAU_DBF
60        VENDA_LIMITADA_DBF
'61        TRANSFERENCIA_DBF

62        Set vSessaoBatch = Nothing
63        Set vBancoBatch = Nothing

64        File1.Pattern = "*.TXT"

TrataErro:
65        If Err.Number <> 0 Then
66            MsgBox "Sub Gerar_Tabelas_Genericas" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
67        End If
End Sub

Public Sub ANTECIPACAO_TRIBUTARIA_dbf()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "ANTECIPACAO_TRIBUTARIA - Consultando"
3         Me.Refresh
         
4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_ANTECIPACAO_TRIBUTARIA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
           
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "ANTECIPA"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "ANTECIPACAO_TRIBUTARIA - Inserindo Registros"
11        Me.Refresh

12        Do While Not VarRet.EOF
13             vContador = vContador + 1
14             DataInsert.Recordset.AddNew
15             DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
16             DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
17             DataInsert.Recordset("pc_margem") = VarRet!PC_MARGEM_LUCRO
18             DataInsert.UpdateRecord
19             VarRet.MoveNext
20             If vContador Mod 100 = 0 Then
21                frmLoad.stb.Panels(1).Text = "ANTECIPACAO_TRIBUTARIA: " & vContador
22                frmLoad.stb.Refresh: DoEvents
23             End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'29        If fContador("ANTECIPACAO_TRIBUTARIA") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela ANTECIPACAO_TRIBUTARIA", vbInformation, "Aten��o"
'31           End
'32        End If


Trata_Erro:
33    If Err.Number <> 0 Then
34      MsgBox "Sub Antecipacao_Tributaria" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35    End If

End Sub

Public Sub APLICACAO_DBF()
    Dim vContador As Long
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "APLICACAO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL "BEGIN PRODSIC.PCK_FIL400.PR_APLICACAO(:VCURSOR); END;"
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         Set DataInsert.Recordset = Nothing
          
7         DataInsert.RecordSource = "APLICACA"
8         DataInsert.Refresh

9         frmLoad.stb.Panels(1).Text = "APLICACAO - Inserindo"
10        Me.Refresh

11        Do While Not VarRet.EOF
12           vContador = vContador + 1
13           DataInsert.Recordset.AddNew
14           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
15           DataInsert.Recordset("cod_catego") = VarRet!cod_categoria
16           DataInsert.Recordset("cod_montad") = VarRet!cod_montadora
17           DataInsert.Recordset("sequencia") = VarRet!sequencia
18           DataInsert.Recordset("cod_origin") = VarRet!cod_original
19           DataInsert.Recordset("desc_aplic") = VarRet!desc_aplicacao
20           DataInsert.UpdateRecord
21           VarRet.MoveNext
22           If vContador Mod 1000 = 0 Then
23              frmLoad.stb.Panels(1).Text = "Aplicacao: " & vContador
24              frmLoad.stb.Refresh: DoEvents
25           End If
26        Loop

Sair:
27        frmLoad.stb.Panels(1).Text = ""
28        DataInsert.RecordSource = ""
29        DataInsert.Refresh
30        Set VarRet = Nothing
          
'31        If fContador("APLICACAO") <> vContador Then
'32           MsgBox "Problemas na gera��o da tabela APLICACAO", vbInformation, "Aten��o"
'33           End
'34        End If
          
Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub Aplicacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If
End Sub

Public Sub BANCO_DBF()
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "BANCO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_BANCO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "BANCO"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "BANCO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_banco") = VarRet!cod_banco
16           DataInsert.Recordset("sigla") = VarRet!Sigla
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "BANCO: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("BANCO") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela BANCO", vbInformation, "Aten��o"
'30           End
'31        End If


Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub Banco" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub CATEG_SINAL_dbf()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "CATEG_SINAL - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CATEG_SINAL(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "CATEG_S"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "CATEG_SINAL - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("categoria") = VarRet!categoria
16           DataInsert.Recordset("sinal") = VarRet!sinal
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "CATEG_SINAL: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("CATEG_SINAL") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela CATEG_SINAL", vbInformation, "Aten��o"
'30           End
'31        End If


Trata_Erro:
32    If Err.Number <> 0 Then
33      MsgBox "Sub CATEG_SINAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34    End If

End Sub

Public Sub CANCEL_PEDNOTA_dbf()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "CANCEL PEDNOTA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CANCEL_PEDNOTA(:VCURSOR); END;")

5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "CANCEL_P"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "CANCEL PEDNOTA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_cancel") = VarRet!cod_cancel
16           DataInsert.Recordset("desc_cance") = VarRet!desc_cancel
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "CANCEL_PEDNOTA: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("CANCEL_PEDNOTA") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela CANCEL_PEDNOTA", vbInformation, "Aten��o"
'30           End
'31        End If


Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub CANCEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub


Public Sub CIDADE_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = " CIDADE - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CIDADE(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "CIDADE"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = " CIDADE - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_cidade") = VarRet!cod_cidade
16           DataInsert.Recordset("nome_cidad") = VarRet!nome_cidade
17           DataInsert.Recordset("tp_cidade") = VarRet!tp_cidade
18           DataInsert.Recordset("cod_uf") = VarRet!cod_uf
19           DataInsert.Recordset("cod_regiao") = VarRet!cod_regiao
20           DataInsert.Recordset("cod_subreg") = VarRet!cod_subregiao
21           DataInsert.Recordset("caracteris") = VarRet!caracteristica
22           DataInsert.UpdateRecord
23           VarRet.MoveNext
24           If vContador Mod 100 = 0 Then
25              frmLoad.stb.Panels(1).Text = "CIDADE: " & vContador
26              frmLoad.stb.Refresh: DoEvents
27           End If

28        Loop

Sair:
29        frmLoad.stb.Panels(1).Text = ""
30        DataInsert.RecordSource = ""
31        DataInsert.Refresh
32        Set VarRet = Nothing

'33        If fContador("CIDADE") <> vContador Then
'34           MsgBox "Problemas na gera��o da tabela CIDADE", vbInformation, "Aten��o"
'35           End
'36        End If


Trata_Erro:
37        If Err.Number <> 0 Then
38            MsgBox "Sub Cidade" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If

End Sub

Public Sub CLASS_ANTEC_ENTRADA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "CLASS_ANTEC_ENTRADA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLASS_ANTEC_ENTRADA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "CLASSANT"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "CLASS_ANTEC_ENTRADA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
16           DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
17           DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
18           DataInsert.Recordset("cod_trib") = VarRet!cod_trib
19           DataInsert.Recordset("PC_MARG_LU") = VarRet!PC_MARGEM_LUCRO
20           DataInsert.UpdateRecord
21           VarRet.MoveNext
22           If vContador Mod 100 = 0 Then
23              frmLoad.stb.Panels(1).Text = "CLASS_ANTEC_ENTRADA: " & vContador
24              frmLoad.stb.Refresh: DoEvents
25           End If
26        Loop
Sair:
27        frmLoad.stb.Panels(1).Text = ""
28        DataInsert.RecordSource = ""
29        DataInsert.Refresh
30        Set VarRet = Nothing
          
'31        If fContador("CLASS_ANTEC_ENTRADA") <> vContador Then
'32           MsgBox "Problemas na gera��o da tabela CLASS_ANTEC_ENTRADA", vbInformation, "Aten��o"
'33           End
'34        End If
          
Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub Class_Antec_Entrada" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If
End Sub

Public Sub CLASS_FISCAL_RED_BASE_DBF()

1         On Error GoTo Trata_Erro

2         frmLoad.stb.Panels(1).Text = "CLASS_FISCAL_RED_BASE - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLASS_FISCAL_RED_BASE(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "RED_BASE"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "CLASS_FISCAL_RED_BASE - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("COD_UF_ORI") = VarRet!COD_UF_ORIGEM
16           DataInsert.Recordset("COD_UF_DES") = VarRet!COD_UF_DESTINO
17           DataInsert.Recordset("CLASS_FISC") = VarRet!CLASS_FISCAL
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "CLASS_FISCAL_RED_BASE: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop

Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'29        If fContador("CLASS_FISCAL_RED_BASE") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela CLASS_FISCAL_RED_BASE", vbInformation, "Aten��o"
'31           End
'32        End If

Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub Class_Fiscal_Red_Base" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub CLIE_MENSAGEM_DBF()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "CLIE_MENSAGEM - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_CLIE_MENSAGEM(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "CLIE_MEN"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "CLIE_MENSAGEM - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_mensag") = VarRet!cod_mensagem
16           DataInsert.Recordset("desc_mens") = VarRet!desc_mens
17           DataInsert.Recordset("fl_bloquei") = VarRet!fl_bloqueio
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "CLIE_MENSAGEM: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'29        If fContador("CLIE_MENSAGEM") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela CLIE_MENSAGEM", vbInformation, "Aten��o"
'31           End
'32        End If

Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub Clie_mensagem" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub DATAS_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "DATAS - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_DATAS(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "DATAS"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "DATAS - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("dt_real") = VarRet!dt_real
16           DataInsert.Recordset("dt_faturam") = VarRet!dt_faturamento
17           DataInsert.Recordset("dt_ini_fec") = VarRet!dt_ini_fech_mensal
18           DataInsert.Recordset("dt_fin_fec") = VarRet!dt_fin_fech_mensal
19           DataInsert.Recordset("dt_feri_1") = VarRet!dt_feriado_1
20           DataInsert.Recordset("dt_feri_2") = VarRet!dt_feriado_2
21           DataInsert.Recordset("fl_liber") = VarRet!fl_liber
22           DataInsert.Recordset("dt_fora_se") = VarRet!dt_fora_semana
23           DataInsert.Recordset("dt_ini_fd") = VarRet!dt_ini_fech_dia
24           DataInsert.UpdateRecord
25           VarRet.MoveNext
26           frmLoad.stb.Panels(1).Text = "DATAS: " & vContador
27           frmLoad.stb.Refresh: DoEvents
28        Loop
Sair:
29        frmLoad.stb.Panels(1).Text = ""
30        DataInsert.RecordSource = ""
31        DataInsert.Refresh
32        Set VarRet = Nothing

'33        If fContador("DATAS") <> vContador Then
'34           MsgBox "Problemas na gera��o da tabela DATAS", vbInformation, "Aten��o"
'35           End
'36        End If

Trata_Erro:
37        If Err.Number <> 0 Then
38            MsgBox "Sub Datas" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If
End Sub


Public Sub DOLAR_DIARIO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "DOLAR DIARIO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_DOLAR_DIARIO(:VCURSOR); END;")

5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "DOLAR_DI"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "DOLAR DIARIO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("data_uss") = VarRet!data_uss
16           DataInsert.Recordset("valor_uss") = VarRet!valor_uss
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "DOLAR_DIARIO: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("DOLAR_DIARIO") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela DOLAR_DIARIO", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32    If Err.Number <> 0 Then
33      MsgBox "Sub Dolar_Diario" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34    End If

End Sub

Public Sub EMBALADOR_DBF()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "EMBALADOR - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_EMBALADOR(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "EMBALAD"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "EMBALADOR - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_embal") = VarRet!cod_embal
16           DataInsert.Recordset("nome_embal") = VarRet!nome_embal
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "EMBALADOR: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("EMBALADOR") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela EMBALADOR", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32    If Err.Number <> 0 Then
33      MsgBox "Sub Embalador" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34    End If

End Sub


Public Sub Filial_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "FILIAL - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FILIAL(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "FILIAL"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "FILIAL - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_filial") = VarRet!cod_filial
16           DataInsert.Recordset("sigla") = VarRet!Sigla
17           DataInsert.Recordset("tp_filial") = VarRet!tp_filial
18           DataInsert.Recordset("cod_franqu") = VarRet!cod_franqueador
19           DataInsert.Recordset("cod_region") = VarRet!cod_regional
20           DataInsert.UpdateRecord
21           VarRet.MoveNext
22           If vContador Mod 100 = 0 Then
23              frmLoad.stb.Panels(1).Text = "FILIAL: " & vContador
24              frmLoad.stb.Refresh: DoEvents
25           End If
26        Loop
Sair:
27        frmLoad.stb.Panels(1).Text = ""
28        DataInsert.RecordSource = ""
29        DataInsert.Refresh
30        Set VarRet = Nothing

'31        If fContador("FILIAL") <> vContador Then
'32           MsgBox "Problemas na gera��o da tabela FILIAL", vbInformation, "Aten��o"
'33           End
'34        End If

Trata_Erro:
35        If Err.Number <> 0 Then
36      MsgBox "Sub FILIAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If
End Sub

Public Sub FORNECEDOR_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "FORNECEDOR - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FORNECEDOR(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "FORNECED"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "FORNECEDOR - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
16           DataInsert.Recordset("sigla") = VarRet!Sigla
17           DataInsert.Recordset("divisao") = VarRet!divisao
18           DataInsert.Recordset("classifica") = VarRet!classificacao
19           DataInsert.Recordset("situacao") = VarRet!situacao
20           DataInsert.UpdateRecord
21           VarRet.MoveNext
22           If vContador Mod 100 = 0 Then
23              frmLoad.stb.Panels(1).Text = "FORNECEDOR: " & vContador
24              frmLoad.stb.Refresh: DoEvents
25           End If
26        Loop
Sair:
27        frmLoad.stb.Panels(1).Text = ""
28        DataInsert.RecordSource = ""
29        DataInsert.Refresh
30        Set VarRet = Nothing

'31        If fContador("FORNECEDOR") <> vContador Then
'32           MsgBox "Problemas na gera��o da tabela FORNECEDOR", vbInformation, "Aten��o"
'33           End
'34        End If

Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub Fornecedor" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If

End Sub

Public Sub FORNECEDOR_ESPECIFICO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "FORNECEDOR ESPECIFICO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FORNECEDOR_ESPECIFICO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "FORN_ESP"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "FORNECEDOR ESPECIFICO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
16           DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
17           DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
18           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
19           DataInsert.Recordset("fl_dif_icm") = VarRet!fl_dif_icms
20           DataInsert.Recordset("fl_adicion") = VarRet!fl_adicional
21           DataInsert.Recordset("tp_dif_icm") = VarRet!tp_dif_icms
22           DataInsert.UpdateRecord
23           VarRet.MoveNext
24           If vContador Mod 100 = 0 Then
25              frmLoad.stb.Panels(1).Text = "FORNECEDOR_ESPECIFICO: " & vContador
26              frmLoad.stb.Refresh: DoEvents
27           End If
28        Loop
Sair:
29        frmLoad.stb.Panels(1).Text = ""
30        DataInsert.RecordSource = ""
31        DataInsert.Refresh
32        Set VarRet = Nothing

'33        If fContador("FORNECEDOR_ESPECIFICO") <> vContador Then
'34           MsgBox "Problemas na gera��o da tabela FORNECEDOR_ESPECIFICO", vbInformation, "Aten��o"
'35           End
'36        End If

Trata_Erro:
37        If Err.Number <> 0 Then
38      MsgBox "Sub FORNECEDOR_ESPECIFICO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If

End Sub

Public Sub FRETE_UF_BLOQ_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "FRETE_UF_BLOQ - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_FRETE_UF_BLOQ(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "FRETEUFB"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "FRETE_UF_BLOQ - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf") = VarRet!cod_uf
16           DataInsert.Recordset("cod_transp") = VarRet!cod_transp
17           DataInsert.Recordset("fl_bloquei") = VarRet!fl_bloqueio
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "FRETE_UF_BLOQ: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'''29        If fContador("FRETE_UF_BLOQ") <> vContador Then
'''30           MsgBox "Problemas na gera��o da tabela FRETE_UF_BLOQ", vbInformation, "Aten��o"
'''31           End
'''32        End If


Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub Frete_UF_BLOQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub GRUPO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "GRUPO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_GRUPO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "GRUPO"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "GRUPO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
16           DataInsert.Recordset("desc_grupo") = VarRet!desc_grupo
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "GRUPO: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing
          
'28        If fContador("GRUPO") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela GRUPO", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub Grupo" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub ITEM_CADASTRO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "ITEM_CADASTRO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_ITEM_CADASTRO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "ITEM_CAD"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "ITEM_CADASTRO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
16           DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
17           DataInsert.Recordset("cod_fabric") = VarRet!COD_FABRICA
18           DataInsert.Recordset("desc_item") = VarRet!desc_item
19           DataInsert.Recordset("cod_linha") = VarRet!cod_linha
20           DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
21           DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
22           DataInsert.Recordset("dt_cadastr") = Format(CDate(VarRet!dt_cadastramento), "DD/MM/YY")
23           DataInsert.Recordset("mascarado") = VarRet!mascarado
24           DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
25           DataInsert.Recordset("peso") = VarRet!peso
26           DataInsert.Recordset("volume") = VarRet!Volume
27           DataInsert.Recordset("cod_unidad") = VarRet!cod_unidade
28           DataInsert.Recordset("cod_tribut") = VarRet!cod_tributacao
29           DataInsert.Recordset("cod_trib_i") = VarRet!cod_tributacao_ipi
30           DataInsert.Recordset("pc_ipi") = VarRet!pc_ipi
31           DataInsert.Recordset("cod_proced") = VarRet!cod_procedencia
32           DataInsert.Recordset("cod_dpk_an") = VarRet!cod_dpk_ant
33           DataInsert.Recordset("qtd_minfor") = VarRet!qtd_minforn
34           DataInsert.Recordset("qtd_minvda") = VarRet!qtd_minvda
35           DataInsert.UpdateRecord
36           VarRet.MoveNext
37           If vContador Mod 100 = 0 Then
38              DoEvents
39              frmLoad.stb.Panels(1).Text = "ITEM_CADASTRO: " & vContador
40              frmLoad.stb.Refresh: DoEvents
41           End If
42        Loop
Sair:
43        frmLoad.stb.Panels(1).Text = ""
44        DataInsert.RecordSource = ""
45        DataInsert.Refresh
46        Set VarRet = Nothing
          
'47        If fContador("ITEM_CADASTRO") <> vContador Then
'48           MsgBox "Problemas na gera��o da tabela ITEM_CADASTRO", vbInformation, "Aten��o"
'49           End
'50        End If
          
          
Trata_Erro:
51    If Err.Number <> 0 Then
52      MsgBox "Sub Item_cadastro" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
53    End If

End Sub

Public Sub MONTADORA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "MONTADORA - Consultando"
3         Me.Refresh
4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_MONTADORA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "MONTADOR"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "MONTADORA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_montad") = VarRet!cod_montadora
16           DataInsert.Recordset("desc_monta") = VarRet!desc_montadora
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "MONTADORA: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("MONTADORA") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela MONTADORA", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub MONTADORA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub NATUREZA_OPERACAO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "NATUREZA OPERACAO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_NATUREZA_OPERACAO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "NATU_OPE"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "NATUREZA OPERACAO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_nature") = VarRet!cod_natureza
16           DataInsert.Recordset("desc_natur") = VarRet!desc_natureza
17           DataInsert.Recordset("desc_abrev") = VarRet!desc_abreviada
18           DataInsert.Recordset("cod_dest1") = VarRet!cod_cfo_dest1
19           DataInsert.Recordset("cod_dest2") = VarRet!cod_cfo_dest2
20           DataInsert.Recordset("cod_dest3") = VarRet!cod_cfo_dest3
21           DataInsert.Recordset("cod_cfo_op") = VarRet!cod_cfo_oper
22           DataInsert.Recordset("cod_transa") = VarRet!cod_transac
23           DataInsert.Recordset("cod_icms1") = VarRet!cod_trib_icms1
24           DataInsert.Recordset("cod_icms2") = VarRet!cod_trib_icms2
25           DataInsert.Recordset("cod_icms3") = VarRet!cod_trib_icms3
26           DataInsert.Recordset("cod_icms4") = VarRet!cod_trib_icms4
27           DataInsert.Recordset("cod_icms5") = VarRet!cod_trib_icms5
28           DataInsert.Recordset("cod_icms6") = VarRet!cod_trib_icms6
29           DataInsert.Recordset("cod_ipi1") = VarRet!cod_trib_ipi1
30           DataInsert.Recordset("cod_ipi2") = VarRet!cod_trib_ipi2
31           DataInsert.Recordset("cod_ipi3") = VarRet!cod_trib_ipi3
32           DataInsert.Recordset("cod_ipi4") = VarRet!cod_trib_ipi4
33           DataInsert.Recordset("cod_mens1") = VarRet!cod_mensagem1
34           DataInsert.Recordset("cod_mens2") = VarRet!cod_mensagem2
35           DataInsert.Recordset("fl_livro") = VarRet!fl_livro
36           DataInsert.Recordset("fl_cuema") = VarRet!fl_cuema
37           DataInsert.Recordset("fl_estoque") = VarRet!fl_estoque
38           DataInsert.Recordset("fl_estorno") = VarRet!fl_estorno
39           DataInsert.Recordset("fl_tributa") = VarRet!fl_tributacao
40           DataInsert.Recordset("fl_fornece") = VarRet!fl_fornecedor
41           DataInsert.Recordset("fl_vlconta") = VarRet!fl_vlcontabil
42           DataInsert.Recordset("fl_nfitem") = VarRet!fl_nfitem
43           DataInsert.Recordset("fl_quantid") = VarRet!fl_quantidade
44           DataInsert.Recordset("fl_cod_mer") = VarRet!fl_cod_merc
45           DataInsert.Recordset("fl_pcdesc") = VarRet!fl_pcdesc
46           DataInsert.Recordset("fl_contas_") = VarRet!fl_contas_pagar
47           DataInsert.Recordset("fl_tipo_no") = VarRet!fl_tipo_nota
48           DataInsert.Recordset("fl_centro_") = VarRet!fl_centro_custo
49           DataInsert.Recordset("fl_contabi") = VarRet!fl_contabil
50           DataInsert.Recordset("fl_base_re") = VarRet!fl_base_red_icms
51           DataInsert.Recordset("fl_consist") = VarRet!fl_consistencia
52           DataInsert.Recordset("fl_ipi_inc") = VarRet!fl_ipi_incid_icm
53           DataInsert.Recordset("fl_movimen") = VarRet!fl_movimentacao
54           DataInsert.UpdateRecord
55           VarRet.MoveNext
56           If vContador Mod 100 = 0 Then
57              frmLoad.stb.Panels(1).Text = "NATUREZA_OPERACAO: " & vContador
58              frmLoad.stb.Refresh: DoEvents
59           End If
60        Loop
Sair:
61        frmLoad.stb.Panels(1).Text = ""
62        DataInsert.RecordSource = ""
63        DataInsert.Refresh
64        Set VarRet = Nothing
          
'65        If fContador("NATUREZA_OPERACAO") <> vContador Then
'66           MsgBox "Problemas na gera��o da tabela NATUREZA_OPERACAO", vbInformation, "Aten��o"
'67           End
'68        End If
          
          
Trata_Erro:
69        If Err.Number <> 0 Then
70      MsgBox "Sub Natureza_Operacao" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
71        End If

End Sub

Public Sub PLANO_PGTO_DBF()

1         On Error GoTo Trata_Erro

2         frmLoad.stb.Panels(1).Text = "PLANO_PGTO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_PLANO_PGTO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "PLANO_PG"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "PLANO_PGTO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_plano") = VarRet!cod_plano
16           DataInsert.Recordset("desc_plano") = VarRet!desc_plano
17           DataInsert.Recordset("prazo_medi") = VarRet!prazo_medio
18           DataInsert.Recordset("pc_acres_f") = VarRet!pc_acres_fin_dpk
19           DataInsert.Recordset("pc_desc_fi") = VarRet!pc_desc_fin_dpk
20           DataInsert.Recordset("situacao") = VarRet!situacao
21           DataInsert.Recordset("fl_avista") = VarRet!fl_avista
22           DataInsert.Recordset("pc_d_vdr") = VarRet!pc_desc_fin_vdr
23           DataInsert.Recordset("pc_a_bla") = VarRet!pc_acres_fin_blau
24           DataInsert.UpdateRecord
25           VarRet.MoveNext
26           If vContador Mod 100 = 0 Then
27              frmLoad.stb.Panels(1).Text = "PLANO_PGTO: " & vContador
28              frmLoad.stb.Refresh: DoEvents
29           End If
30        Loop
Sair:
31        frmLoad.stb.Panels(1).Text = ""
32        DataInsert.RecordSource = ""
33        DataInsert.Refresh
34        Set VarRet = Nothing
          
'35        If fContador("PLANO_PGTO") <> vContador Then
'36           MsgBox "Problemas na gera��o da tabela PLANO_PG", vbInformation, "Aten��o"
'37           End
'38        End If
          
Trata_Erro:
39        If Err.Number <> 0 Then
40      MsgBox "Sub Plano_Pgto" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
41        End If
End Sub

Public Sub R_DPK_EQUIV_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = " R_DPK_EQUIV - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_DPK_EQUIV(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "R_DPK_EQ"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = " R_DPK_EQUIV - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
16           DataInsert.Recordset("cod_dpk_eq") = VarRet!cod_dpk_eq
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "R_DPK_EQUIV: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("R_DPK_EQUIV") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela R_DPK_EQUIV", vbInformation, "Aten��o"
'30           End
'31        End If


Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub R_DPK_EQUIV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub R_FABRICA_DPK_DBF()

1         On Error GoTo Trata_Erro

2         frmLoad.stb.Panels(1).Text = " R_FABRICA_DPK - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_FABRICA_DPK(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "R_FABRIC"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = " R_FABRICA_DPK - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_fabric") = VarRet!COD_FABRICA
16           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
17           DataInsert.Recordset("fl_tipo") = VarRet!FL_TIPO
18           DataInsert.Recordset("dt_cadastr") = VarRet!DT_CADASTRO
19           DataInsert.Recordset("Sigla") = VarRet!Sigla
20           DataInsert.UpdateRecord
21           VarRet.MoveNext
22           If vContador Mod 100 = 0 Then
23              frmLoad.stb.Panels(1).Text = "R_FABRICA_DPK: " & vContador
24              frmLoad.stb.Refresh: DoEvents
25           End If
26        Loop
Sair:
27        frmLoad.stb.Panels(1).Text = ""
28        DataInsert.RecordSource = ""
29        DataInsert.Refresh
30        Set VarRet = Nothing

'31        If fContador("R_FABRICA_DPK") <> vContador Then
'32           MsgBox "Problemas na gera��o da tabela R_FABRICA_DPK", vbInformation, "Aten��o"
'33           End
'34        End If

Trata_Erro:
35        If Err.Number <> 0 Then
36            MsgBox "Sub R_Fabrica_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
37        End If
End Sub

Public Sub R_FILDEP_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "R_FILDEP - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_R_FIL_DEP(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "R_FILDEP"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "R_FILDEP - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_loja") = VarRet!cod_loja
16           DataInsert.Recordset("cod_filial") = VarRet!cod_filial
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "R_FILDEP: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing
          
'28        If fContador("R_FILDEP") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela R_FILDEP", vbInformation, "Aten��o"
'30           End
'31        End If
          
Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub R_FILDEP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If
End Sub


Public Sub R_UF_DEPOSITO_DBF()

1         On Error GoTo Trata_Erro

2         frmLoad.stb.Panels(1).Text = "R_UF_DEPOSITO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL "BEGIN PRODSIC.PCK_FIL400.PR_R_UF_DEPOSITO(:VCURSOR); END;"
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "R_UF_DEP"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "R_UF_DEPOSITO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf") = VarRet!cod_uf
16           DataInsert.Recordset("cod_loja") = VarRet!cod_loja
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "R_UF_DEPOSITO: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("R_UF_DEPOSITO") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela R_UF_DEPOSITO", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub R_UF_DEPOSITO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If
End Sub


Public Sub RESULTADO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "RESULTADO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_RESULTADO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "RESULTAD"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "RESULTADO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_result") = VarRet!cod_resultado
16           DataInsert.Recordset("desc_resul") = VarRet!desc_resultado
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "RESULTADO: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("RESULTADO") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela RESULTADO", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub Resultado" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub SUBGRUPO_DBF()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "SUBGRUPO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_SUBGRUPO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "SUBGRUPO"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "SUBGRUPO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
16           DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
17           DataInsert.Recordset("desc_subgr") = VarRet!desc_subgrupo
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "SUBGRUPO: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'29        If fContador("SUBGRUPO") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela SUBGRUPO", vbInformation, "Aten��o"
'31           End
'32        End If

Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub SubGrupo" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub SUBST_TRIBUTARIA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "SUBST_TRIBUTARIA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_SUBST_TRIBUTARIA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "SUBST_TR"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "SUBST_TRIBUTARIA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("class_fisc") = VarRet!CLASS_FISCAL
16           DataInsert.Recordset("cod_origem") = VarRet!COD_UF_ORIGEM
17           DataInsert.Recordset("cod_destin") = VarRet!COD_UF_DESTINO
18           DataInsert.Recordset("cod_revend") = VarRet!cod_trib_revendedor
19           DataInsert.Recordset("cod_inscri") = VarRet!cod_trib_inscrito
20           DataInsert.Recordset("cod_isento") = VarRet!cod_trib_isento
21           DataInsert.Recordset("fator_reve") = VarRet!fator_revendedor
22           DataInsert.Recordset("fator_insc") = VarRet!fator_inscrito
23           DataInsert.Recordset("fator_isen") = VarRet!fator_isento
24           DataInsert.Recordset("fl_cred_su") = VarRet!fl_cred_suspenso
25           DataInsert.Recordset("pc_margem_") = VarRet!PC_MARGEM_LUCRO
26           DataInsert.UpdateRecord
27           VarRet.MoveNext
28           If vContador Mod 100 = 0 Then
29              DoEvents
30              frmLoad.stb.Panels(1).Text = "SUBST_TRIBUTARIA: " & vContador
31              frmLoad.stb.Refresh: DoEvents
32           End If
33        Loop
Sair:
34        frmLoad.stb.Panels(1).Text = ""
35        DataInsert.RecordSource = ""
36        DataInsert.Refresh
37        Set VarRet = Nothing

'38        If fContador("SUBST_TRIBUTARIA") <> vContador Then
'39           MsgBox "Problemas na gera��o da tabela SUBST_TRIBUTARIA", vbInformation, "Aten��o"
'40           End
'41        End If

Trata_Erro:
42      If Err.Number = 3043 Then
43          Resume
44      ElseIf Err.Number = 444 Then
45          Resume Next
46      ElseIf Err.Number <> 0 Then
47          MsgBox "Sub Subst_Tributaria" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
48      End If

End Sub

Public Sub TABELA_DESCPER_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TABELA_DESCPER - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TABELA_DESCPER(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair

7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "TABELAD"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TABELA_DESCPER - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("sequencia") = VarRet!sequencia
16           DataInsert.Recordset("tp_tabela") = VarRet!tp_tabela
17           DataInsert.Recordset("ocorr_prec") = VarRet!ocorr_preco
18           DataInsert.Recordset("cod_filial") = VarRet!cod_filial
19           DataInsert.Recordset("cod_fornec") = VarRet!cod_fornecedor
20           DataInsert.Recordset("cod_grupo") = VarRet!cod_grupo
21           DataInsert.Recordset("cod_subgru") = VarRet!cod_subgrupo
22           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
23           DataInsert.Recordset("situacao") = VarRet!situacao
24           DataInsert.Recordset("pc_desc1") = VarRet!pc_desc_periodo1
25           DataInsert.Recordset("pc_desc2") = VarRet!pc_desc_periodo2
26           DataInsert.Recordset("pc_desc3") = VarRet!pc_desc_periodo3
27           DataInsert.UpdateRecord
28           VarRet.MoveNext
29           If vContador Mod 100 = 0 Then
30              frmLoad.stb.Panels(1).Text = "TABELA_DESCPER: " & vContador
31              frmLoad.stb.Refresh: DoEvents
32           End If
33        Loop
Sair:
34        frmLoad.stb.Panels(1).Text = ""
35        DataInsert.RecordSource = ""
36        DataInsert.Refresh
37        Set VarRet = Nothing
          
'38        If fContador("TABELA_DESCPER") <> vContador Then
'39           MsgBox "Problemas na gera��o da tabela TABELA_DESCPER", vbInformation, "Aten��o"
'40           End
'41        End If
          
Trata_Erro:
42        If Err.Number <> 0 Then
43      MsgBox "Sub Tabela_DescPer" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
44        End If
End Sub

Public Sub TABELA_VENDA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TABELA_VENDA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TABELA_VENDA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "TABELAV"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TABELA_VENDA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("tabela_ven") = VarRet!TABELA_VENDA
16           DataInsert.Recordset("tp_tabela") = VarRet!tp_tabela
17           DataInsert.Recordset("ocorr_prec") = VarRet!ocorr_preco
18           DataInsert.Recordset("dt_vig1") = VarRet!dt_vigencia1
19           DataInsert.Recordset("dt_vig2") = VarRet!dt_vigencia2
20           DataInsert.Recordset("dt_vig3") = VarRet!dt_vigencia3
21           DataInsert.Recordset("dt_vig4") = VarRet!dt_vigencia4
22           DataInsert.Recordset("dt_vig5") = VarRet!dt_vigencia5
23           DataInsert.UpdateRecord
24           VarRet.MoveNext
25           If vContador Mod 100 = 0 Then
26              frmLoad.stb.Panels(1).Text = "TABELA_VENDA: " & vContador
27              frmLoad.stb.Refresh: DoEvents
28           End If
29        Loop
Sair:
30        frmLoad.stb.Panels(1).Text = ""
31        DataInsert.RecordSource = ""
32        DataInsert.Refresh
33        Set VarRet = Nothing
          
'34        If fContador("TABELA_VENDA") <> vContador Then
'35           MsgBox "Problemas na gera��o da tabela TABELA_VENDA", vbInformation, "Aten��o"
'36           End
'37        End If
          
Trata_Erro:
38        If Err.Number <> 0 Then
39      MsgBox "Sub Tabela_Venda" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
40        End If

End Sub

Public Sub TIPO_CLIENTE_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TIPO_CLIENTE(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "TIPO_CLI"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_tipo_c") = VarRet!cod_tipo_cli
16           DataInsert.Recordset("desc_tipo") = VarRet!desc_tipo_cli
17           DataInsert.Recordset("cod_segmen") = VarRet!cod_segmento
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing
          
'29        If fContador("TIPO_CLIENTE") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela TIPO_CLIENTE", vbInformation, "Aten��o"
'31           End
'32        End If
          
Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub Tipo_Cliente" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub TRANSPORTADORA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TRANSPORTADORA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TRANSPORTADORA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value
          
6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "TRANSPOR"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TRANSPORTADORA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_transp") = VarRet!cod_transp
16           DataInsert.Recordset("nome_trans") = VarRet!nome_transp
17           DataInsert.Recordset("cod_cidade") = VarRet!cod_cidade
18           DataInsert.Recordset("situacao") = VarRet!situacao
19           DataInsert.UpdateRecord
20           VarRet.MoveNext
21           If vContador Mod 100 = 0 Then
22              frmLoad.stb.Panels(1).Text = "TRANSPORTADORA: " & vContador
23              frmLoad.stb.Refresh: DoEvents
24           End If
25        Loop
Sair:
26        frmLoad.stb.Panels(1).Text = ""
27        DataInsert.RecordSource = ""
28        DataInsert.Refresh
29        Set VarRet = Nothing

'30        If fContador("TRANSPORTADORA") <> vContador Then
'31           MsgBox "Problemas na gera��o da tabela TRANSPORTADORA", vbInformation, "Aten��o"
'32           End
'33        End If

Trata_Erro:
34        If Err.Number <> 0 Then
35      MsgBox "Sub Transportadora" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
36        End If

End Sub

Public Sub UF_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "UF - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "UF"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "UF - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf") = VarRet!cod_uf
16           DataInsert.Recordset("desc_uf") = VarRet!desc_uf
17           DataInsert.Recordset("dt_aliq_in") = VarRet!dt_aliq_interna
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "UF: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing

'29        If fContador("UF") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela UF", vbInformation, "Aten��o"
'31           End
'32        End If

Trata_Erro:
33        If Err.Number <> 0 Then
34      MsgBox "Sub UF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub UF_DPK_DBF()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "UF_DPK - Consultando"
3         Me.Refresh
4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_DPK(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "UF_DPK"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "UF_DPK - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
16           DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
17           DataInsert.Recordset("cod_dpk") = VarRet!cod_dpk
18           DataInsert.Recordset("pc_desc") = VarRet!pc_desc
19           DataInsert.UpdateRecord
20           VarRet.MoveNext
21           If vContador Mod 100 = 0 Then
22              frmLoad.stb.Panels(1).Text = "UF_DPK: " & vContador
23              frmLoad.stb.Refresh: DoEvents
24           End If
25        Loop
Sair:
26        frmLoad.stb.Panels(1).Text = ""
27        DataInsert.RecordSource = ""
28        DataInsert.Refresh
29        Set VarRet = Nothing
          
'30        If fContador("UF_DPK") <> vContador Then
'31           MsgBox "Problemas na gera��o da tabela UF_DPK", vbInformation, "Aten��o"
'32           End
'33        End If
          
Trata_Erro:
34        If Err.Number <> 0 Then
35      MsgBox "Sub UF_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
36        End If

End Sub

Public Sub UF_ORIGEM_DESTINO_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "UF_ORIGEM_DESTINO - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_ORIGEM_DESTINO(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "UF_OR_DE"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "UF_ORIGEM_DESTINO - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf_ori") = VarRet!COD_UF_ORIGEM
16           DataInsert.Recordset("cod_uf_des") = VarRet!COD_UF_DESTINO
17           DataInsert.Recordset("pc_icm") = VarRet!pc_icm
18           DataInsert.Recordset("pc_dificm") = VarRet!pc_dificm
19           DataInsert.UpdateRecord
20           VarRet.MoveNext
21           If vContador Mod 100 = 0 Then
22              frmLoad.stb.Panels(1).Text = "UF_ORIGEM_DESTINO: " & vContador
23              frmLoad.stb.Refresh: DoEvents
24           End If
25        Loop
Sair:
26        frmLoad.stb.Panels(1).Text = ""
27        DataInsert.RecordSource = ""
28        DataInsert.Refresh
29        Set VarRet = Nothing

'30        If fContador("UF_ORIGEM_DESTINO") <> vContador Then
'31           MsgBox "Problemas na gera��o da tabela UF_OR_DE", vbInformation, "Aten��o"
'32           End
'33        End If

Trata_Erro:
34        If Err.Number <> 0 Then
35      MsgBox "Sub UF_ORIGEM_DESTINO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
36        End If
End Sub

Public Sub UF_TPCLIENTE_DBF()

1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "UF_TPCLIENTE - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_UF_TPCLIENTE(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing

8         DataInsert.RecordSource = "UF_TPCLI"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "UF_TPCLIENTE - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_origem") = VarRet!COD_UF_ORIGEM
16           DataInsert.Recordset("cod_destin") = VarRet!COD_UF_DESTINO
17           DataInsert.Recordset("tp_cliente") = VarRet!tp_cliente
18           DataInsert.Recordset("cod_tribut") = VarRet!cod_tributacao
19           DataInsert.Recordset("pc_desc") = VarRet!pc_desc
20           DataInsert.Recordset("cod_final") = VarRet!cod_tributacao_final
21           DataInsert.Recordset("pc_desc_di") = VarRet!pc_desc_diesel
22           DataInsert.UpdateRecord
23           VarRet.MoveNext
24           If vContador Mod 100 = 0 Then
25              frmLoad.stb.Panels(1).Text = "UF_TPCLIENTE: " & vContador
26              frmLoad.stb.Refresh: DoEvents
27           End If
28        Loop
Sair:
29        frmLoad.stb.Panels(1).Text = ""
30        DataInsert.RecordSource = ""
31        DataInsert.Refresh
32        Set VarRet = Nothing
          
'33        If fContador("UF_TPCLIENTE") <> vContador Then
'34           MsgBox "Problemas na gera��o da tabela UF_TPCLIENTE", vbInformation, "Aten��o"
'35           End
'36        End If

Trata_Erro:
37        If Err.Number <> 0 Then
38      MsgBox "Sub UF_TPCLIENTE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
39        End If

End Sub


Public Sub TIPO_CLIENTE_BLAU_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE_BLAU - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TIPO_CLIENTE_BLAU(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "TP_BLAU"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE_BLAU - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_tipo_c") = VarRet!cod_tipo_cli
16           DataInsert.Recordset("desc_tipo_") = VarRet!desc_tipo_cli
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           If vContador Mod 100 = 0 Then
20              frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE_BLAU: " & vContador
21              frmLoad.stb.Refresh: DoEvents
22           End If
23        Loop
Sair:
24        frmLoad.stb.Panels(1).Text = ""
25        DataInsert.RecordSource = ""
26        DataInsert.Refresh
27        Set VarRet = Nothing

'28        If fContador("TIPO_CLIENTE_BLAU") <> vContador Then
'29           MsgBox "Problemas na gera��o da tabela TIPO_CLIENTE_BLAU", vbInformation, "Aten��o"
'30           End
'31        End If

Trata_Erro:
32        If Err.Number <> 0 Then
33      MsgBox "Sub TIPO_CLIENTE_BLAU" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
34        End If

End Sub

Public Sub VENDA_LIMITADA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "VENDA_LIMITADA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_VENDA_LIMITADA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
          
8         DataInsert.RecordSource = "VLIMITAD"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "VENDA_LIMITADA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("cod_uf") = VarRet!cod_uf
16           DataInsert.Recordset("tp_cliente") = VarRet!tp_cliente
17           DataInsert.Recordset("cod_loja_o") = VarRet!cod_loja_obrigatoria
18           DataInsert.UpdateRecord
19           VarRet.MoveNext
20           If vContador Mod 100 = 0 Then
21              frmLoad.stb.Panels(1).Text = "VENDA_LIMITADA: " & vContador
22              frmLoad.stb.Refresh: DoEvents
23           End If
24        Loop
Sair:
25        frmLoad.stb.Panels(1).Text = ""
26        DataInsert.RecordSource = ""
27        DataInsert.Refresh
28        Set VarRet = Nothing
          
'29        If fContador("VENDA_LIMITADA") <> vContador Then
'30           MsgBox "Problemas na gera��o da tabela VENDA_LIMITADA", vbInformation, "Aten��o"
'31           End
'32        End If

Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub Venda_limitada" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
35        End If

End Sub

Public Sub TRANSFERENCIA_DBF()
          
1         On Error GoTo Trata_Erro
          
2         frmLoad.stb.Panels(1).Text = "TRANSFERENCIA - Consultando"
3         Me.Refresh

4         vBancoBatch.ExecuteSQL ("BEGIN PRODSIC.PCK_FIL400.PR_TRANSFERENCIA(:VCURSOR); END;")
          
5         Set VarRet = vBancoBatch.Parameters("vCursor").Value

6         If VarRet Is Nothing Or IsNull(VarRet.Fields(0)) Then GoTo Sair
          
7         Set DataInsert.Recordset = Nothing
          
8         DataInsert.RecordSource = "TRANSFER"
9         DataInsert.Refresh

10        frmLoad.stb.Panels(1).Text = "TRANSFERENCIA - Inserindo"
11        Me.Refresh

12        Do While Not VarRet.EOF
13           vContador = vContador + 1
14           DataInsert.Recordset.AddNew
15           DataInsert.Recordset("DEP_ORIG") = VarRet!COD_DEP_ORIGEM
16           DataInsert.Recordset("DEP_DEST") = VarRet!COD_DEP_DESTINO
17           DataInsert.UpdateRecord
18           VarRet.MoveNext
19           frmLoad.stb.Panels(1).Text = "TIPO_CLIENTE_BLAU: " & vContador
20           frmLoad.stb.Refresh: DoEvents
21        Loop
Sair:
22        frmLoad.stb.Panels(1).Text = ""
23        DataInsert.RecordSource = ""
24        DataInsert.Refresh
25        Set VarRet = Nothing

'26        If fContador("TRANSFERENCIA") <> vContador Then
'27           MsgBox "Problemas na gera��o da tabela TRANSFERENCIA", vbInformation, "Aten��o"
'28           End
'29        End If

Trata_Erro:
30        If Err.Number <> 0 Then
31      MsgBox "Sub TRANSFERENCIA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
        Exit Sub
32        End If
End Sub

Function Gerar_Genericas_Dbf(Tabela As String) As Boolean
1         For i = 0 To lstGenericas.ListCount - 1
2             If UCase(lstGenericas.List(i)) = UCase(Tabela) Then
3                 If frmGerarDBF.lstGenericas.Selected(i) = True Then
4                     Gerar_Genericas_Dbf = True
5                     Exit For
6                 End If
7             End If
8         Next
End Function

Function fContador(pTabela As String) As Long
    For i = 0 To msfControlePaac.Cols
        If UCase(msfControlePaac.TextMatrix(0, i)) = UCase(pTabela) Then
            fContador = msfControlePaac.TextMatrix(1, i)
            Exit For
        End If
    Next
End Function

'Comentado em 20/08/2007
'Habilitado em 08/02/2008 - Luciano
Public Sub Gerar_Cargas(pCod As Long, pTipo As String)

1         On Error GoTo Trata_Erro

              Dim vRstEmail As Object
              Dim num_Arq As Integer
              Dim vArq As String
              Dim vGerar As Boolean

2             frmLoad.stb.Panels(1).Text = "Gerando carga para PAAC/Repres: " & pCod

3             If pTipo = "P" Then
                  '*************
                  'Copiar Tabela
                  '*************
                  'Verificar para cada tabela do Grid de Tabelas de PAAC se ela est� com Diferenca
                      'Se Sim
                          'Se foi copiada para aquele PAAC em menos de 5 Dias.
                              'Se Sim
                                  'Nao faz nada
                              'Se Nao
                                  'Copiar o DBF para a pasta C:\CARGAS_PAAC\

4                 For i = 2 To msfControlePaac.Rows - 1
5                     If Val(msfControlePaac.TextMatrix(i, 0)) = pCod Then
6                         DoEvents
7                         If Len(Trim(msfControlePaac.TextMatrix(i, 1))) > 1 Then
8                             For ii = 2 To msfControlePaac.Cols - 1
9                                 If msfControlePaac.TextMatrix(i, ii) <> msfControlePaac.TextMatrix(1, ii) Then
10                                    DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "P_" & Format(pCod, "0000"), UCase(msfControlePaac.TextMatrix(0, ii))) = "", Date - 6, GetKeyVal("C:\FIL470.INI", "P_" & Format(pCod, "0000"), UCase(msfControlePaac.TextMatrix(0, ii))))
11                                    If DateDiff("d", DtGeracao, Date) > 5 Then
12                                        COPIAR_TABELA UCase(msfControlePaac.TextMatrix(0, ii))
13                                        AddToINI "C:\FIL470.INI", UCase(pTipo) & "_" & Format(pCod, "0000"), UCase(msfControlePaac.TextMatrix(0, ii)), Date
14                                        vGerar = True
15                                        GoTo PAAC_EXECUTAVEL
16                                    End If
17                                End If
18                            Next
19                        End If
20                    End If
21                Next

PAAC_EXECUTAVEL:
22                For i = 2 To msfControleVersoesPaac.Rows - 1
23                    If Val(msfControleVersoesPaac.TextMatrix(i, 0)) = pCod Then
24                        DoEvents
25                        If msfControleVersoesPaac.TextMatrix(i, 1) <> "" Then
26                            For ii = 1 To msfControleVersoesPaac.Cols - 1
27                                If msfControleVersoesPaac.TextMatrix(i, ii) <> msfControleVersoesPaac.TextMatrix(1, ii) Then
28                                    DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "P_" & Format(pCod, "0000"), msfControleVersoesPaac.TextMatrix(0, ii)) = "", Date - 6, GetKeyVal("C:\FIL470.INI", "P_" & Format(pCod, "0000"), msfControleVersoesPaac.TextMatrix(0, ii)))
29                                    If DateDiff("d", DtGeracao, Date) > 5 Then
30                                        COPIAR_EXECUTAVEL UCase(msfControleVersoesPaac.TextMatrix(0, ii))
31                                        AddToINI "C:\FIL470.INI", UCase(pTipo) & "_" & Format(pCod, "0000"), UCase(msfControleVersoesPaac.TextMatrix(0, ii)), Date
32                                        vGerar = True
33                                    End If
34                                End If
35                            Next
36                        End If
37                    End If
38                Next

39        ElseIf pTipo = "R" Then

40            For i = 1 To msfControleRepres.Rows - 1
41                If Val(msfControleRepres.TextMatrix(i, 0)) = pCod Then
42                    DoEvents
43                    For ii = 2 To msfControleRepres.Cols - 1
44                        If Val(msfControleRepres.TextMatrix(i, ii)) <> Val(msfControleRepres.TextMatrix(1, ii)) Then
45                            DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "R_" & Format(pCod, "0000"), UCase(msfControleRepres.TextMatrix(0, ii))) = "", Date - 6, GetKeyVal("C:\FIL470.INI", "R_" & Format(pCod, "0000"), UCase(msfControleRepres.TextMatrix(0, ii))))
46                            If DateDiff("d", DtGeracao, Date) >= 5 Then
47                                COPIAR_TABELA UCase(msfControleRepres.TextMatrix(0, ii))
48                                AddToINI "C:\FIL470.INI", UCase(pTipo) & "_" & Format(pCod, "0000"), UCase(msfControleRepres.TextMatrix(0, ii)), Date
49                                vGerar = True
50                                GoTo Repres_Executavel
51                            End If
52                        End If
53                    Next
54                End If
55            Next


              '*****************
              'Copiar Executavel
              '*****************
              'Verificar para cada Executavel do Grid de Executaveis do PAAC se ele est� com Diferenca
                  'Se Sim
                      'Se foi copiado para o PAAC em menos de 5 Dias
                          'Se Sim
                              'Entao nao faz nada
                          'Senao
                              'Copiar o arquivo .EXE para a pasta C:\CARGAS_PAAC\

Repres_Executavel:
56            For i = 1 To msfControleVersoesRepres.Rows - 1
57                If Val(msfControleVersoesRepres.TextMatrix(i, 0)) = pCod Then
58                    If msfControleVersoesRepres.TextMatrix(i, 1) <> "" Then
59                        DoEvents
60                        For ii = 1 To msfControleVersoesRepres.Cols - 1
61                            If msfControleVersoesRepres.TextMatrix(i, ii) <> msfControleVersoesRepres.TextMatrix(1, ii) Then
62                                DtGeracao = IIf(GetKeyVal("C:\FIL470.INI", "R_" & pCod, UCase(msfControleVersoesRepres.TextMatrix(0, ii))) = "", Date - 6, GetKeyVal("C:\FIL470.INI", "R_" & pCod, UCase(msfControleVersoesRepres.TextMatrix(0, ii))))
63                                If DateDiff("d", DtGeracao, Date) > 5 Then
64                                    COPIAR_EXECUTAVEL UCase(msfControleVersoesRepres.TextMatrix(0, ii))
65                                    AddToINI "C:\FIL470.INI", UCase(pTipo) & "_" & Format(pCod, "0000"), UCase(msfControleVersoesRepres.TextMatrix(0, ii)), Date
66                                    vGerar = True
67                                    GoTo Fim
68                                End If
69                            End If
70                        Next
71                    End If
72                End If
73            Next
74        End If

Fim:
75        If vGerar = False Then Exit Sub

76        Montar_HDR pCod, pTipo

77        MaqZip1.ZipCompress "C:\CARGAS_PAAC\", "*.DBF *.EXE *.MDB", "C:\CARGAS_PAAC\ZIP\", vNomeArquivo & ".ZIP", "-6"

78        Do While Dir("C:\CARGAS_PAAC\ZIP\" & vNomeArquivo & ".ZIP") = ""
79          DoEvents
80        Loop

81        Do While Dir("C:\CARGAS_PAAC\*.*", vbArchive) <> ""
82           vArq = Dir("C:\CARGAS_PAAC\*.*", vbArchive)
83           If vArq = "" Then Exit Do
84           Kill "C:\CARGAS_PAAC\" & vArq
85           DoEvents
86        Loop

87        vNomeArquivo = ""

Trata_Erro:
88        If Err.Number <> 0 Then
89      MsgBox "Sub Gerar_Cargas" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
90        End If

End Sub

'Comentado em 20/08/2007
Sub COPIAR_TABELA(pNome_Tabela As String)

    On Error GoTo Trata_Erro

          Dim vNomeTabela As String

1         If UCase(pNome_Tabela) = "ANTECIPACAO_TRIBUTARIA" Then
2             vNomeTabela = "ANTECIPA.DBF"
3         ElseIf UCase(pNome_Tabela) = "APLICACAO" Then
4             vNomeTabela = "APLICACA.DBF"
5         ElseIf UCase(pNome_Tabela) = "BANCO" Then
6             vNomeTabela = "BANCO.DBF"
7         ElseIf UCase(pNome_Tabela) = "CANCEL_PEDNOTA" Then
8             vNomeTabela = "CANCEL_P.DBF"
9         ElseIf UCase(pNome_Tabela) = "CATEG_SINAL" Then
10            vNomeTabela = "CATEG_S.DBF"
11        ElseIf UCase(pNome_Tabela) = "CIDADE" Then
12            vNomeTabela = "CIDADE.DBF"
13        ElseIf UCase(pNome_Tabela) = "CLASS_ANTEC_ENTRADA" Then
14            vNomeTabela = "CLASSANT.DBF"
15        ElseIf UCase(pNome_Tabela) = "CLASS_FISCAL_RED_BASE" Then
16            vNomeTabela = "RED_BASE.DBF"
17        ElseIf UCase(pNome_Tabela) = "CLIE_MENSAGEM" Then
18            vNomeTabela = "CLIE_MEN.DBF"
19        ElseIf UCase(pNome_Tabela) = "DATAS" Then
20            vNomeTabela = "DATAS.DBF"
21        ElseIf UCase(pNome_Tabela) = "DOLAR_DIARIO" Then
22            vNomeTabela = "DOLAR_DI.DBF"
23        ElseIf UCase(pNome_Tabela) = "EMBALADOR" Then
24            vNomeTabela = "EMBALAD.DBF"
25        ElseIf UCase(pNome_Tabela) = "FILIAL" Then
26            vNomeTabela = "FILIAL.DBF"
27        ElseIf UCase(pNome_Tabela) = "FORNECEDOR" Then
28            vNomeTabela = "FORNECED.DBF"
29        ElseIf UCase(pNome_Tabela) = "FORNECEDOR_ESPECIFICO" Then
30            vNomeTabela = "FORN_ESP.DBF"
31        ElseIf UCase(pNome_Tabela) = "FRETE_UF_BLOQ" Then
32            vNomeTabela = "FRETEUFB.DBF"
33        ElseIf UCase(pNome_Tabela) = "GRUPO" Then
34            vNomeTabela = "GRUPO.DBF"
35        ElseIf UCase(pNome_Tabela) = "ITEM_CADASTRO" Then
36            vNomeTabela = "ITEM_CAD.DBF"
37        ElseIf UCase(pNome_Tabela) = "MONTADORA" Then
38            vNomeTabela = "MONTADOR.DBF"
39        ElseIf UCase(pNome_Tabela) = "NATUREZA_OPERACAO" Then
40            vNomeTabela = "NATU_OPE.DBF"
41        ElseIf UCase(pNome_Tabela) = "PLANO_PGTO" Then
42            vNomeTabela = "PLANO_PG.DBF"
43        ElseIf UCase(pNome_Tabela) = "R_DPK_EQUIV" Then
44            vNomeTabela = "R_DPK_EQ.DBF"
45        ElseIf UCase(pNome_Tabela) = "R_FABRICA_DPK" Then
46            vNomeTabela = "R_FABRIC.DBF"
47        ElseIf UCase(pNome_Tabela) = "R_FILDEP" Then
48            vNomeTabela = "R_FILDEP.DBF"
49        ElseIf UCase(pNome_Tabela) = "R_UF_DEPOSITO" Then
50            vNomeTabela = "R_UF_DEP.DBF"
51        ElseIf UCase(pNome_Tabela) = "RESULTADO" Then
52            vNomeTabela = "RESULTAD.DBF"
53        ElseIf UCase(pNome_Tabela) = "SUBGRUPO" Then
54            vNomeTabela = "SUBGRUPO.DBF"
55        ElseIf UCase(pNome_Tabela) = "SUBST_TRIBUTARIA" Then
56            vNomeTabela = "SUBST_TR.DBF"
57        ElseIf UCase(pNome_Tabela) = "TABELA_DESCPER" Then
58            vNomeTabela = "TABELAD.DBF"
59        ElseIf UCase(pNome_Tabela) = "TABELA_VENDA" Then
60            vNomeTabela = "TABELAV.DBF"
61        ElseIf UCase(pNome_Tabela) = "TIPO_CLIENTE" Then
62            vNomeTabela = "TIPO_CLI.DBF"
63        ElseIf UCase(pNome_Tabela) = "TRANSPORTADORA" Then
64            vNomeTabela = "TRANSPOR.DBF"
65        ElseIf UCase(pNome_Tabela) = "UF" Then
66            vNomeTabela = "UF.DBF"
67        ElseIf UCase(pNome_Tabela) = "UF_DPK" Then
68            vNomeTabela = "UF_DPK.DBF"
69        ElseIf UCase(pNome_Tabela) = "UF_ORIGEM_DESTINO" Then
70            vNomeTabela = "UF_OR_DE.DBF"
71        ElseIf UCase(pNome_Tabela) = "UF_TPCLIENTE" Then
72            vNomeTabela = "UF_TPCLI.DBF"
73        ElseIf UCase(pNome_Tabela) = "VENDA_LIMITADA" Then
74            vNomeTabela = "VLIMITAD.DBF"
75        ElseIf UCase(pNome_Tabela) = "TRANSFERENCIA" Then
76            vNomeTabela = "TRANSFER.DBF"
77        End If

78        FileCopy vCaminho_Gerar_Genericas & vNomeTabela, "C:\CARGAS_PAAC\" & vNomeTabela

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Copiar_tabela" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Sub COPIAR_EXECUTAVEL(pNomeExecutavel As String)

    On Error GoTo Trata_Erro

1    FileCopy vCaminho_Executaveis & pNomeExecutavel & ".EXE", "C:\CARGAS_PAAC\" & pNomeExecutavel & ".EXE"
2    FileCopy vCaminho_Executaveis & "FIL240.EXE", "C:\CARGAS_PAAC\FIL240.EXE"
3    FileCopy vCaminho_Executaveis & "FIL240.MDB", "C:\CARGAS_PAAC\FIL240.MDB"

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Copiar_Executavel" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Sub Montar_HDR(pCod As Long, pTipo As String)

    On Error GoTo Trata_Erro

          Dim vRstEmail As Object

           '*********
           'Gerar HDR
           '*********
1          If pTipo = "P" Then 'PAAC
2              Set vRstEmail = vBanco.CreateDynaset("Select E_Mail from PAAC.FILIAL_BASE WHERE COD_FILIAL = " & pCod, 0&)
3          ElseIf pTipo = "R" Then 'REPRES
4              Set vRstEmail = vBanco.CreateDynaset("Select E_Mail from PAAC.REPRESENTANTE_BASE WHERE COD_REPRES =" & pCod, 0&)
5          End If

6          vNomeArquivo = "CAR" & Format(pCod, "0000")

          'MONTA O ARQUIVO DE HEADER DA CARGA
7          num_Arq = FreeFile
8          Open "C:\CARGAS_PAAC\ZIP\" & vNomeArquivo & ".HDR" For Output As #num_Arq

9          Print #num_Arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
10         Print #num_Arq, "TO        =" & vRstEmail!E_mail
11         Print #num_Arq, "FROM      =DPKPEDID"
12         Print #num_Arq, "DESCRICAO =" & "#CAR"; Format(Val(pCod), "0000")
13         Print #num_Arq, "FILE      =" & vNomeArquivo & ".ZIP"
          'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
14         Print #num_Arq, "NRQUEBRA  =0"

15         Close #num_Arq

16        Set vRstEmail = Nothing
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Copiar_Executavel" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Sub Enviar()

'End Sub

1         On Error GoTo Trata_Erro

          Dim i As Integer
          Dim ii As Integer

          Dim vPAACs
          Dim vRepres

2         If GetKeyVal("C:\FIL470.INI", "ENVIAR", "PAACS") <> "" Then
3             vPAACs = Split(GetKeyVal("C:\FIL470.INI", "ENVIAR", "PAACS"), ",")

4             Selecionar lstPAACEnviar, False

5             For i = 0 To UBound(vPAACs)
6                 For ii = 0 To Me.lstPAACEnviar.ListCount - 1
7                     If vPAACs(i) = lstPAACEnviar.List(ii) Then
8                         lstPAACEnviar.Selected(ii) = True
9                         Exit For
10                    End If
11                Next
12            Next

13        End If

14        If GetKeyVal("C:\FIL470.INI", "ENVIAR", "REPRES") <> "" Then
15            vRepres = Split(GetKeyVal("C:\FIL470.INI", "ENVIAR", "REPRES"), ",")

16            Selecionar lstRepresEnviar, False

17            For i = 0 To UBound(vRepres)
18                For ii = 0 To lstRepresEnviar.ListCount - 1
19                    If vRepres(i) = lstRepresEnviar.List(ii) Then
20                        lstRepresEnviar.Selected(ii) = True
21                        Exit For
22                    End If
23                Next
24            Next

25        End If

Trata_Erro:
26        If Err.Number <> 0 Then
27      MsgBox "Sub Enviar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
28        End If
End Sub

Sub Preencher_Num_tabelas()
    Dim litem As ListItem
    Dim vRst As Object
    
    If vBanco Is Nothing Then
        Set vSessao = CreateObject("oracleinproCServer.xorasession")
        Set vBanco = vSessao.OpenDatabase("SDPK_META", "FIL430/PROD", 0&)
    End If
    
    Set vRst = vBanco.CreateDynaset("SELECT cod_paac_repres, a.tipo, COUNT(tabela) qtd FROM paac.controle_tabelas a, representante b Where a.cod_paac_repres = b.cod_repres And b.situacao = 0 GROUP BY cod_paac_repres, a.tipo ORDER BY cod_paac_repres, a.tipo ", 0&)
    
    lsvNumTabelas.ListItems.Clear
    For i = 1 To vRst.RecordCount
        Set litem = Me.lsvNumTabelas.ListItems.Add
        litem = vRst!cod_paac_repres
        litem.SubItems(1) = vRst!tipo
        litem.SubItems(2) = vRst!qtd
        vRst.MoveNext
    Next
    Set vRst = Nothing
End Sub

Public Sub GridExport(GridAExportar As MSFlexGrid, Arquivo As String)

1     On Error GoTo Trata_Erro

      Dim iNumRows As Integer
      Dim iNumCols As Integer
      Dim iArqNumero As Integer
      Dim vLinha As String

2     iArqNumero = FreeFile
3     Open Arquivo For Output As #iArqNumero

4     For iNumRows = 0 To GridAExportar.Rows - 1
5         GridAExportar.Row = iNumRows
          
6         vLinha = ""
          
7         For iNumCols = 0 To GridAExportar.Cols - 1
8             GridAExportar.Col = iNumCols

9             If iNumCols = 0 Then
10                vLinha = GridAExportar.Text
11            Else
12                vLinha = vLinha & ";" & GridAExportar.Text
13            End If

14        Next iNumCols

15        Print #iArqNumero, vLinha

16    Next iNumRows

17    Close #iArqNumero

Trata_Erro:
18        If Err.Number <> 0 Then
19            MsgBox "Sub GridExport" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
20        End If
End Sub
Public Sub GridExportList()

           Dim iNumRows As Integer
           Dim iNumCols As Integer
           Dim iArqNumero As Integer
           Dim vLinha As String
           
1          On Error GoTo Trata_Erro
           
2          iArqNumero = FreeFile
3          Open "c:\Tabelas.txt" For Output As #iArqNumero

4          vLinha = "Codigo;Tipo;N� Tabelas"
           
5          Print #iArqNumero, vLinha

6          For iNumRows = 1 To Me.lsvNumTabelas.ListItems.Count
                
7              vLinha = Me.lsvNumTabelas.ListItems(iNumRows).Text & ";" & lsvNumTabelas.ListItems(iNumRows).SubItems(1) & ";" & lsvNumTabelas.ListItems(iNumRows).SubItems(2)

8              Print #iArqNumero, vLinha

9          Next iNumRows
           
10         Close #iArqNumero

Trata_Erro:
11            If Err.Number <> 0 Then
12                MsgBox "Sub GridExportList" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
13            End If
End Sub

'------------------------------------------------------------------------------------------------'
' Jo�o Marcos Sakalauska - SDS 2405 - 16/01/2012
' Inclus�o da Aba "Base Desat."
' Sub's: preencheTitulosGridBaseDesativada e preencheGridBaseDesativada
'------------------------------------------------------------------------------------------------'

Public Sub preencheTitulosGridBaseDesativada()
    msfBaseDesat.ColWidth(0) = 1500
    msfBaseDesat.ColWidth(1) = 5000
    msfBaseDesat.TextMatrix(0, 1) = "Representante"
    msfBaseDesat.ColWidth(2) = 1000
    msfBaseDesat.TextMatrix(0, 2) = "Data"
    msfBaseDesat.ColWidth(3) = 1500
    msfBaseDesat.TextMatrix(0, 3) = "Atualiza��o"
End Sub

Public Sub preencheGridBaseDesativada(vetID() As Integer, vetData() As String)

On Error GoTo Trata_Erro
    Dim reg As Integer
    Dim rstRep As Object
    Dim strSql As String
    Dim strSqlParametros As String
    Dim i As Integer
    Dim x As Integer
    Dim y As Integer
    Dim intIDAux As Integer
    Dim strDataAux As String
    
    ' Ordena itens para n�o gerar problema com dados encontrados na Base de Dados
    For x = 0 To UBound(vetID) - 1 Step 1
        For y = x + 1 To UBound(vetID) - 1 Step 1
            If vetID(x) > vetID(y) Then
                intIDAux = vetID(y)
                strDataAux = vetData(y)

                vetID(y) = vetID(x)
                vetData(y) = vetData(x)

                vetID(x) = intIDAux
                vetData(x) = strDataAux
            End If
        Next y
    Next x
    
    'Monta a string SQL
    For i = 0 To UBound(vetID) - 1
        If (strSqlParametros = "") Then
            strSqlParametros = vetID(i)
        Else
            strSqlParametros = strSqlParametros & "," & vetID(i)
        End If
    Next
    
    strSql = "SELECT REP.COD_REPRES, REP.NOME_REPRES FROM REPRESENTANTE REP WHERE REP.COD_REPRES IN "
    strSql = strSql & "(" & strSqlParametros & ") "
    strSql = strSql & "AND REP.SITUACAO <> 9 ORDER BY REP.COD_REPRES"
    
    Set vSessao = CreateObject("oracleinproCServer.xorasession")
    Set vBanco = vSessao.OpenDatabase("SDPK_META", "FIL430/PROD", 0&)

    Set rstRep = vBanco.CreateDynaset(strSql, 0&)
    
    msfBaseDesat.Clear
    
    msfBaseDesat.Rows = rstRep.RecordCount + 1
    
    ' Monta os dados na grid
    For reg = 0 To rstRep.RecordCount - 1
        
        msfBaseDesat.TextMatrix(reg + 1, 0) = vetID(reg)
        msfBaseDesat.TextMatrix(reg + 1, 1) = rstRep!NOME_REPRES
        msfBaseDesat.TextMatrix(reg + 1, 2) = vetData(reg)
        
        'Verificar se o sistema est� atualizado ou se � s�bado e domingo.
        If DateDiff("d", vetData(reg), Format(Date, "DD/MM/YY")) > 0 And Weekday(Date) <> 1 And Weekday(Date) <> 7 Then
            msfBaseDesat.TextMatrix(reg + 1, 3) = "Desatualizado"
        Else
            msfBaseDesat.TextMatrix(reg + 1, 3) = "Atualizado"
        End If
        
        rstRep.MoveNext
    Next
Trata_Erro:
          If Err.Number = 62 Then
              vLinha = ""
              Resume Next
          ElseIf Err.Number <> 0 Then
              MsgBox "Sub Ler_Arquivo" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
          End If
End Sub
'------------------------------------------------------------------------------------------------'
