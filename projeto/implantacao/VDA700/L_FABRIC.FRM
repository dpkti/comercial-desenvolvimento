VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmFabrica 
   Caption         =   "Lista de Codigo de F�brica do Fornecedor "
   ClientHeight    =   4485
   ClientLeft      =   150
   ClientTop       =   1890
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4485
   ScaleWidth      =   9315
   Begin VB.ComboBox cboMontadora 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   1200
      TabIndex        =   4
      Top             =   600
      Width           =   1695
   End
   Begin VB.ComboBox cboGrupo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   3600
      TabIndex        =   3
      Top             =   600
      Width           =   1815
   End
   Begin VB.ComboBox cboSubgrupo 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   6480
      TabIndex        =   2
      Top             =   600
      Width           =   2775
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   8160
      TabIndex        =   0
      Top             =   3600
      Width           =   1095
   End
   Begin Threed.SSFrame ssfraPesq 
      Height          =   735
      Left            =   120
      TabIndex        =   9
      Top             =   3600
      Visible         =   0   'False
      Width           =   7455
      _Version        =   65536
      _ExtentX        =   13150
      _ExtentY        =   1296
      _StockProps     =   14
      Caption         =   "Pesquisa"
      ForeColor       =   255
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox Text1 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   3255
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   375
         Left            =   4080
         TabIndex        =   12
         Top             =   240
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "F�brica"
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   375
         Left            =   5880
         TabIndex        =   11
         Top             =   240
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Aplica��o"
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin Threed.SSCommand SSCommand3 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "L_FABRIC.frx":0000
   End
   Begin MSGrid.Grid grdFabrica 
      Height          =   2415
      Left            =   15
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   9300
      _Version        =   65536
      _ExtentX        =   16404
      _ExtentY        =   4260
      _StockProps     =   77
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "L_FABRIC.frx":031A
   End
   Begin VB.Label lblMontadora 
      Caption         =   "Montadora"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   600
      Width           =   975
   End
   Begin VB.Label lblGrupo 
      Caption         =   "Grupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3000
      TabIndex        =   6
      Top             =   600
      Width           =   615
   End
   Begin VB.Label lblSubgrupo 
      Caption         =   "Subgrupo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5520
      TabIndex        =   5
      Top             =   600
      Width           =   855
   End
End
Attribute VB_Name = "frmFabrica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String

Private Sub cboGrupo_Click()

 'Seleciona um grupo, guarda o c�digo para posterior consulta
  grupo = arrgrupo(cboGrupo.ListIndex + 1)
  
  'Limpa o array que guarda os subgrupos
  Erase arrsubgrupo
  
  'Limpa o combo e vari�vel do subgrupo
  frmFabrica.cboSubgrupo.Clear
  frmFabrica.cboSubgrupo.Text = ""
  subgrupo = 0

End Sub


Private Sub cboGrupo_DropDown()

Dim ss As Object
Dim cont As Long

' Verifica se foi escolhido um fornecedor, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor executa o select que busca os grupos
' N�o permite executar o select se o combo estiver cheio

If frmVenda.txtCOD_FORNECEDOR.Text = "" Then
  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
  Exit Sub
Else
  If cboGrupo.ListCount = 0 Then
    Screen.MousePointer = 11
    'Select utilizado para preencher o combo de grupo
          SQL = "select distinct b.desc_grupo desc_grupo,"
    SQL = SQL & " b.cod_grupo cod_grupo"
    SQL = SQL & " from grupo b, item_cadastro a"
    SQL = SQL & " where b.cod_grupo=a.cod_grupo and"
    SQL = SQL & " a.cod_fornecedor = :cod"
    SQL = SQL & " order by b.desc_grupo"
  
    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Add "cod", frmVenda.txtCOD_FORNECEDOR, 1
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
      If ss.EOF Then
         MsgBox "N�o exite Grupo cadastrado para o fornecedor " & frmVenda.txtCOD_FORNECEDOR, 0, "ATEN��O"
         Screen.MousePointer = 0
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.recordcount
         'Redefine array para guardar os c�digos de grupos
          ReDim Preserve arrgrupo(1 To ss.recordcount)
          cboGrupo.AddItem ss.Fields("desc_grupo").Value
          arrgrupo(cont) = ss.Fields("cod_grupo").Value
          ss.movenext
        Next
        Screen.MousePointer = 0
      End If
  End If
End If



End Sub


Private Sub cboGrupo_KeyPress(KeyAscii As Integer)

  'n�o permite alterar a informa��o do combo
  
  KeyAscii = 0
  Beep



End Sub

Private Sub cboMontadora_Change()
  
  If cboMontadora.ListCount = 0 Then
     Call cboMontadora_DropDown
 End If
  
End Sub

Private Sub cboMontadora_Click()

 
   montadora = arrmontadora(cboMontadora.ListIndex + 1)
   If subgrupo <> 0 Then
     Call cboSubgrupo_Click
   End If

End Sub

Private Sub cboMontadora_DropDown()

Dim ss As Object


' Verifica se foi escolhido um fornecedor, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor executa o select que busca as montadoras
' N�o permite executar o select se o combo estiver cheio



If frmVenda.txtCOD_FORNECEDOR.Text = "" Then
  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
  Exit Sub
Else
  If cboMontadora.ListCount = 0 Then
     'Select utilizado para preencher o combo de montadora
          SQL = "select a.desc_montadora desc_montadora,"
    SQL = SQL & " a.cod_montadora cod_montadora"
    SQL = SQL & " from montadora a"
    SQL = SQL & " order by desc_montadora"

      Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
      Screen.MousePointer = 11
      If ss.EOF Then
         MsgBox "N�o exite montadora cadastrada", 0, "ATEN��O"
         Exit Sub
      Else
        'Enche o combo
        For cont = 1 To ss.recordcount
          'Redefine array para guardar os c�digos de montadoras
          ReDim Preserve arrmontadora(1 To ss.recordcount)
          cboMontadora.AddItem ss.Fields("desc_montadora").Value
          arrmontadora(cont) = ss.Fields("cod_montadora").Value
          ss.movenext
        Next
        Screen.MousePointer = 0
      End If
   End If
 End If

End Sub


Private Sub cboMontadora_KeyPress(KeyAscii As Integer)

 KeyAscii = Maiusculo(KeyAscii)

End Sub


Private Sub cboMontadora_LostFocus()
  
 Dim INICIO As Integer
 Dim CONTINUA As Integer
 Dim POSICAO As Integer
 
 CONTINUA = 1
 
 
 If cboMontadora.DataChanged Then
   For INICIO = 1 To cboMontadora.ListCount
      If InStr(cboMontadora.Text, cboMontadora.List(CONTINUA)) <> 0 Then
        If CONTINUA >= cboMontadora.ListCount Then
          MsgBox "Montadora n�o cadastrada", vbExclamation, "Aten��o"
          cboMontadora.SetFocus
          Exit Sub
        End If
        montadora = arrmontadora(CONTINUA + 1)
        If subgrupo <> 0 Then
          Call cboSubgrupo_Click
          Exit Sub
        End If
        Exit Sub
      End If
      CONTINUA = CONTINUA + 1
      If CONTINUA > cboMontadora.ListCount Then
        MsgBox "Montadora n�o cadastrada", vbExclamation, "Aten��o"
        cboMontadora.SetFocus
        Exit Sub
      End If
   Next
 End If
  
End Sub

Private Sub cboSubgrupo_Click()
   
    
    Dim ss As Object
    Dim strFabrica As String
    Dim bAltura As Byte
    Dim ss2 As Object
    Dim I As Integer
    
   'Seleciona um grupo, guarda o c�digo para posterior consulta
  subgrupo = arrsubgrupo(cboSubgrupo.ListIndex + 1)
  
      
 If grupo = 0 Or subgrupo = 0 Then
    MsgBox "Selecione um grupo e um subgrupo", 0, "ATEN��O"
    frmFabrica.cboGrupo.SetFocus
    Exit Sub
  End If
    
'Monta select para buscar todos os itens que possuam pre�o de venda

' oradatabase.ExecuteSQL ("alter session set sql_trace=true")
 Screen.MousePointer = 11
 'Select utilizado para mostrar os itens atrav�s da sele��o feita
    SQL = "select a.COD_FABRICA,a.DESC_ITEM,"
    SQL = SQL & " c.DESC_APLICACAO"
    SQL = SQL & " from item_cadastro a, item_estoque b, aplicacao c "
    SQL = SQL & " where c.COD_DPK(+) = a.cod_dpk and"
    SQL = SQL & " b.cod_loja = :loja and "
    SQL = SQL & " b.cod_dpk = a.cod_dpk and "
    SQL = SQL & " b.SITUACAO = 0 and "
    SQL = SQL & " a.COD_FORNECEDOR = :forn"
    

 If montadora <> 0 Then
      SQL = SQL & " and c.cod_montadora = :montadora"
 End If
 
 If grupo <> 0 Then
      SQL = SQL & " and a.cod_grupo = :grupo"
 End If
 
 If subgrupo <> 0 Then
      SQL = SQL & " and a.cod_subgrupo = :subgrupo"
 End If
 
 SQL = SQL & " order by a.COD_FABRICA"
 
 oradatabase.Parameters.Remove "forn"
 oradatabase.Parameters.Add "forn", frmVenda.txtCOD_FORNECEDOR.Text, 1
 oradatabase.Parameters.Remove "montadora"
 oradatabase.Parameters.Add "montadora", montadora, 1
 oradatabase.Parameters.Remove "grupo"
 oradatabase.Parameters.Add "grupo", grupo, 1
 oradatabase.Parameters.Remove "subgrupo"
 oradatabase.Parameters.Add "subgrupo", subgrupo, 1
 oradatabase.Parameters.Remove "loja"
 oradatabase.Parameters.Add "loja", Mid(frmVenda.cboDeposito.Text, 1, 3), 1

 tot_grdfabr = 0
 Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
 
 Screen.MousePointer = 11
 
       If ss.EOF Then
           Screen.MousePointer = 0
           MsgBox "N�o h� itens para a sele��o escolhida", 0, "ATEN��O"
           Screen.MousePointer = 0
           frmFabrica.cboMontadora.Clear
           frmFabrica.cboGrupo.Clear
           frmFabrica.cboSubgrupo.Clear
           grdFabrica.Visible = False
           ssfraPesq.Visible = False
           montadora = 0
           grupo = 0
           subgrupo = 0
           Erase arrmontadora
           Erase arrgrupo
           Erase arrsubgrupo
           Exit Sub
        Else
           'carrega dados
          tot_grdfabr = ss.recordcount
         
          With frmFabrica.grdFabrica
             .Cols = 3
             .Rows = 1
             .ColWidth(0) = 1950
             .ColWidth(1) = 3165
             .ColWidth(2) = 3870
        
             .Row = 0
             .Col = 0
             .Text = "Codigo de F�brica"
             .Col = 1
             .Text = "Descri��o do Item"
             .Col = 2
             .Text = "Aplica��o"
        
             strFabrica = ""

             For I = 1 To ss.recordcount
                If strFabrica <> ss("COD_FABRICA") Then
                  'cria linha
                  .Rows = .Rows + 1
                  .Row = .Rows - 1
                  'carrega dados
                  .Col = 0
                  .Text = ss("COD_FABRICA")
                  .Col = 1
                  .Text = ss("DESC_ITEM")
                  .Col = 2
                  If IsNull(ss("DESC_APLICACAO")) Then
                      .Text = ""
                  Else
                      .Text = Trim$(ss("DESC_APLICACAO"))
                  End If
                  'carrega codigo de fabrica
                  strFabrica = ss("COD_FABRICA")
                 Else
                  'carrega aplicacao
                 If Not IsNull(ss("DESC_APLICACAO")) Then
                     .Col = 2
                     .Text = .Text & " " & Trim$(ss("DESC_APLICACAO"))
                     bAltura = Len(Trim$(.Text)) \ 30
                     If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
                         bAltura = bAltura + 1
                     End If
                     If bAltura > 1 Then
                         .RowHeight(.Row) = bAltura * 225
                     End If
                  End If
               End If
               
               If I = 20 Then
                  grdFabrica.Visible = True
                  ssfraPesq.Visible = True
                  DoEvents
               End If
                        
            ss.movenext
        Next
        .Row = 1
        .FixedRows = 1
    End With
    grdFabrica.Visible = True
    ssfraPesq.Visible = True
            
    'mouse
    Screen.MousePointer = vbDefault
        
    
   End If
    Exit Sub
  'oradatabase.ExecuteSQL ("alter session set sql_trace=false")

End Sub


Private Sub cboSubgrupo_DropDown()

Dim ss As Object
Dim cont As Long

 
' Verifica se foi escolhido um fornecedor e um grupo, caso contr�rio n�o permite sele��o
' Se escolheu fornecedor e grupo executa o select que busca os subgrupos
' N�o permite executar o select se o combo estiver cheio

If frmVenda.txtCOD_FORNECEDOR.Text = "" Then
  MsgBox "Escolha um fornecedor", 0, "ATEN��O"
  Exit Sub
Else
  If frmFabrica.cboGrupo.Text = "" Then
    MsgBox "Escolha um grupo", 0, "ATEN��O"
    frmFabrica.cboGrupo.SetFocus
    Exit Sub
  Else
    If cboSubgrupo.ListCount = 0 Then
      Screen.MousePointer = 11
      'Select utilizado para preencher o combo de subgrupo
         SQL = "select distinct b.desc_subgrupo desc_subgrupo,"
   SQL = SQL & " b.cod_subgrupo cod_subgrupo"
   SQL = SQL & " from subgrupo b, item_cadastro a"
   SQL = SQL & " where b.cod_subgrupo=a.cod_subgrupo and"
   SQL = SQL & " b.cod_grupo = a.cod_grupo and"
   SQL = SQL & " a.cod_grupo = :grupo and"
   SQL = SQL & " a.cod_fornecedor = :cod"
   SQL = SQL & " order by b.desc_subgrupo"

      oradatabase.Parameters.Remove "cod"
      oradatabase.Parameters.Remove "grupo"
      oradatabase.Parameters.Add "cod", frmVenda.txtCOD_FORNECEDOR, 1
      oradatabase.Parameters.Add "grupo", grupo, 1
      Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
        
        If ss.EOF Then
           MsgBox "N�o exite Subgrupo cadastrado para o fornecedor " & frmVenda.txtCOD_FORNECEDOR, 0, "ATEN��O"
           Exit Sub
        Else
          'Enche o combo
          For cont = 1 To ss.recordcount
            'Redefine array para guardar os c�digos de subgrupos
            ReDim Preserve arrsubgrupo(1 To ss.recordcount)
            cboSubgrupo.AddItem ss.Fields("desc_subgrupo").Value
            arrsubgrupo(cont) = ss.Fields("cod_subgrupo").Value
            ss.movenext
          Next
          Screen.MousePointer = 0
        End If
    End If
  End If
End If



End Sub


Private Sub cboSubgrupo_KeyPress(KeyAscii As Integer)

   'n�o permite alterar a informa��o do combo
  
  KeyAscii = 0
  Beep

End Sub

Private Sub cmdSair_Click()
    
    txtResposta = ""
    frmFabrica.Hide
    
    
End Sub


Private Sub Form_Activate()

Dim ss As Object

Dim strFabrica As String
Dim bAltura As Byte
Dim ss2 As Object
Dim I As Integer


If InStr(1, frmVenda.txtCOD_FABRICA.Text, pesq) > 0 And _
 InStr(1, frmVenda.txtDescricao.Text, pesq) = 0 Then
 
          SQL = "select a.COD_FABRICA,a.DESC_ITEM,"
    SQL = SQL & " c.DESC_APLICACAO"
    SQL = SQL & " from item_cadastro a, item_estoque b, aplicacao c "
    SQL = SQL & " where c.COD_DPK(+) = a.cod_dpk and"
    SQL = SQL & " b.cod_loja = :loja and "
    SQL = SQL & " b.cod_dpk = a.cod_dpk and "
    SQL = SQL & " b.SITUACAO <> 9 and "
    SQL = SQL & " a.COD_FORNECEDOR = :forn and"
    SQL = SQL & " a.COD_FABRICA like :fabrica"
    SQL = SQL & " order by cod_fabrica"
   
 oradatabase.Parameters.Remove "forn"
 oradatabase.Parameters.Add "forn", frmVenda.txtCOD_FORNECEDOR, 1
 oradatabase.Parameters.Remove "fabrica"
 oradatabase.Parameters.Add "fabrica", frmVenda.txtCOD_FABRICA.Text, 1
 oradatabase.Parameters.Remove "loja"
 oradatabase.Parameters.Add "loja", Mid(frmVenda.cboDeposito.Text, 1, 3), 1
 tot_grdfabr = 0
 
 'oradatabase.ExecuteSQL ("alter session set sql_trace=true")
 Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
 
 'oradatabase.ExecuteSQL ("alter session set sql_trace=FALSE")
 
 Screen.MousePointer = 11
 
       If ss.EOF Then
           Screen.MousePointer = 0
           MsgBox "N�o h� itens para a sele��o escolhida", 0, "ATEN��O"
           Unload frmFabrica
           Exit Sub
        Else
   
          'carrega dados
          tot_grdfabr = ss.recordcount
          With frmFabrica.grdFabrica
             .Cols = 3
             .Rows = 1
             .ColWidth(0) = 1950
             .ColWidth(1) = 3165
             .ColWidth(2) = 3870
        
             .Row = 0
             .Col = 0
             .Text = "Codigo de F�brica"
             .Col = 1
             .Text = "Descri��o do Item"
             .Col = 2
             .Text = "Aplica��o"
        
             strFabrica = ""

             For I = 1 To ss.recordcount
                If strFabrica <> ss!COD_FABRICA Then
                  'cria linha
                  .Rows = .Rows + 1
                  .Row = .Rows - 1
                  'carrega dados
                  .Col = 0
                  .Text = ss!COD_FABRICA
                  .Col = 1
                  .Text = ss!DESC_ITEM
                  .Col = 2
                  If IsNull(ss!DESC_APLICACAO) Then
                      .Text = ""
                  Else
                      .Text = Trim$(ss!DESC_APLICACAO)
                  End If
                  'carrega codigo de fabrica
                  strFabrica = ss!COD_FABRICA
                 Else
                  'carrega aplicacao
                 If Not IsNull(ss!DESC_APLICACAO) Then
                     .Col = 2
                     .Text = .Text & " " & Trim$(ss!DESC_APLICACAO)
                     bAltura = Len(Trim$(.Text)) \ 30
                     If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
                         bAltura = bAltura + 1
                     End If
                     If bAltura > 1 Then
                         .RowHeight(.Row) = bAltura * 225
                     End If
                  End If
               End If
                        
               If I = 20 Then
                  grdFabrica.Visible = True
                  ssfraPesq.Visible = True
                  DoEvents
               End If
                        
            ss.movenext
        Next
         .Row = 1
         .FixedRows = 1
    End With
  End If
    cboMontadora.Text = ""
    cboGrupo.Text = ""
    cboSubgrupo.Text = ""
    grdFabrica.Visible = True
    ssfraPesq.Visible = True
            
    'mouse
    Screen.MousePointer = vbDefault
 
 
ElseIf InStr(1, frmVenda.txtDescricao, pesq) > 0 And _
   InStr(1, frmVenda.txtCOD_FABRICA, pesq) = 0 Then
   
          SQL = "select a.COD_FABRICA,a.DESC_ITEM,"
    SQL = SQL & " c.DESC_APLICACAO"
    SQL = SQL & " from item_cadastro a, item_estoque b, aplicacao c "
    SQL = SQL & " where c.COD_DPK(+) = a.cod_dpk and"
    SQL = SQL & " b.cod_loja = :loja and "
    SQL = SQL & " b.cod_dpk = a.cod_dpk and "
    SQL = SQL & " b.SITUACAO = 0 and "
    SQL = SQL & " a.COD_FORNECEDOR = :forn and"
    SQL = SQL & " a.desc_item like :desc_item"
    SQL = SQL & " order by cod_fabrica"
   
 oradatabase.Parameters.Remove "forn"
 oradatabase.Parameters.Add "forn", frmVenda.txtCOD_FORNECEDOR, 1
 oradatabase.Parameters.Remove "desc_item"
 oradatabase.Parameters.Add "desc_item", frmVenda.txtDescricao.Text, 1
 oradatabase.Parameters.Remove "loja"
 oradatabase.Parameters.Add "loja", Mid(frmVenda.cboDeposito.Text, 1, 3), 1
 tot_grdfabr = 0
 
'oradatabase.ExecuteSQL ("alter session set sql_trace=true")
 Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
 
 'oradatabase.ExecuteSQL ("alter session set sql_trace=FALSE")
 
 Screen.MousePointer = 11
 
       If ss.EOF Then
           Screen.MousePointer = 0
           MsgBox "N�o h� itens para a sele��o escolhida", 0, "ATEN��O"
           Unload frmFabrica
           Exit Sub
        Else
   
          'carrega dados
          tot_grdfabr = ss.recordcount
          With frmFabrica.grdFabrica
             .Cols = 3
             .Rows = 1
             .ColWidth(0) = 1950
             .ColWidth(1) = 3165
             .ColWidth(2) = 3870
        
             .Row = 0
             .Col = 0
             .Text = "Codigo de F�brica"
             .Col = 1
             .Text = "Descri��o do Item"
             .Col = 2
             .Text = "Aplica��o"
        
             strFabrica = ""

             For I = 1 To ss.recordcount
                If strFabrica <> ss!COD_FABRICA Then
                  'cria linha
                  .Rows = .Rows + 1
                  .Row = .Rows - 1
                  'carrega dados
                  .Col = 0
                  .Text = ss!COD_FABRICA
                  .Col = 1
                  .Text = ss!DESC_ITEM
                  .Col = 2
                  If IsNull(ss!DESC_APLICACAO) Then
                      .Text = ""
                  Else
                      .Text = Trim$(ss!DESC_APLICACAO)
                  End If
                  'carrega codigo de fabrica
                  strFabrica = ss!COD_FABRICA
                 Else
                  'carrega aplicacao
                 If Not IsNull(ss!DESC_APLICACAO) Then
                     .Col = 2
                     .Text = .Text & " " & Trim$(ss!DESC_APLICACAO)
                     bAltura = Len(Trim$(.Text)) \ 30
                     If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
                         bAltura = bAltura + 1
                     End If
                     If bAltura > 1 Then
                         .RowHeight(.Row) = bAltura * 225
                     End If
                  End If
               End If
                        
               If I = 20 Then
                  grdFabrica.Visible = True
                  ssfraPesq.Visible = True
                  DoEvents
               End If
                        
            ss.movenext
        Next
         .Row = 1
         .FixedRows = 1
    End With
  End If
    cboMontadora.Text = ""
    cboGrupo.Text = ""
    cboSubgrupo.Text = ""
    grdFabrica.Visible = True
    ssfraPesq.Visible = True
            
    'mouse
    Screen.MousePointer = vbDefault
End If

  
End Sub

Private Sub grdFabrica_Click()

    grdFabrica.Col = 0
    frmVenda.txtCOD_FABRICA.Text = grdFabrica.Text
    txtResposta = grdFabrica.Text
    frmFabrica.Hide

End Sub

Private Sub lblPesquisa_Click()

End Sub


Private Sub SSCommand1_Click()
  On Error GoTo TopRowError
  'N�mero da coluna onde a pesquisa deve agir
  grdFabrica.Col = 0
  
  'texto que dever� ser encontrado
  TEXTOPARAPROCURA = Trim(Text1.Text)
  
  'desabilita o highlight
  grdFabrica.HighLight = False
  
  'posiciona no in�cio do grid
  grdFabrica.Row = 1
  grdFabrica.TopRow = 1
  
  For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grdfabr
     grdFabrica.Row = INICIOPESQUISA
     
     'verifica se a string digitada est� contida no texto da aplica�ao
     If InStr(grdFabrica.Text, TEXTOPARAPROCURA) <> 0 Then
       'define nova ocorr�ncia
       OCORRENCIA = OCORRENCIA + 1
     End If
     
     If InStr(grdFabrica.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
       'vari�vel que controla quantas vezes determinado string j� foi localizado
       CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1
       
       'liga o highlight
       grdFabrica.HighLight = True
       grdFabrica.SelStartRow = INICIOPESQUISA
       grdFabrica.SelEndRow = INICIOPESQUISA
       grdFabrica.SelStartCol = 0
       grdFabrica.SelEndCol = 2
       
       'se encontrou o texto posiciona a linha do grid
       If grdFabrica.CellSelected Then
         If INICIOPESQUISA > 1 Then
           For INILINHA = 1 To INICIOPESQUISA - 1
           'o m�ximo valor que toprow pode ter deve ser igual ao numero de
           'linhas total do grid menos o numero de linhas visiveis do grid
             If grdFabrica.TopRow = grdFabrica.Row - tot_grdfabr Then
               grdFabrica.TopRow = grdFabrica.Rows - tot_grdfabr
             Else
               grdFabrica.TopRow = grdFabrica.TopRow + 1
             End If
           Next
         End If
       End If
       
       'a proxima pesquisa continuara a partir desta variavel
       CONTINUAPESQUISA = INICIOPESQUISA
       SSCommand1.Caption = "Pr�xima"
       Exit Sub
    End If
   Next
     
     
     MsgBox "Fim de Pesquisa", vbExclamation, "ATEN��O"
     SSCommand1.Caption = "F�brica"
     Exit Sub
           
TopRowError:
    grdFabrica.TopRow = 1
    Resume Next


End Sub

Private Sub SSCommand1_GotFocus()
  SSCommand1.Refresh
End Sub


Private Sub SSCommand2_Click()
   On Error GoTo TopRowError
  'N�mero da coluna onde a pesquisa deve agir
  grdFabrica.Col = 2
  
  'texto que dever� ser encontrado
  TEXTOPARAPROCURA = Trim(Text1.Text)
  
  'desabilita o highlight
  grdFabrica.HighLight = False
  
  'posiciona no in�cio do grid
  grdFabrica.Row = 1
  grdFabrica.TopRow = 1
  
  For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grdfabr
     grdFabrica.Row = INICIOPESQUISA
     
     'verifica se a string digitada est� contida no texto da aplica�ao
     If InStr(grdFabrica.Text, TEXTOPARAPROCURA) <> 0 Then
       'define nova ocorr�ncia
       OCORRENCIA = OCORRENCIA + 1
     End If
     
     If InStr(grdFabrica.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
       'vari�vel que controla quantas vezes determinado string j� foi localizado
       CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1
       
       'liga o highlight
       grdFabrica.HighLight = True
       grdFabrica.SelStartRow = INICIOPESQUISA
       grdFabrica.SelEndRow = INICIOPESQUISA
       grdFabrica.SelStartCol = 0
       grdFabrica.SelEndCol = 2
       
       'se encontrou o texto posiciona a linha do grid
       If grdFabrica.CellSelected Then
         If INICIOPESQUISA > 1 Then
           For INILINHA = 1 To INICIOPESQUISA - 1
           'o m�ximo valor que toprow pode ter deve ser igual ao numero de
           'linhas total do grid menos o numero de linhas visiveis do grid
             If grdFabrica.TopRow = grdFabrica.Row - tot_grdfabr Then
               grdFabrica.TopRow = grdFabrica.Rows - tot_grdfabr
             Else
               grdFabrica.TopRow = grdFabrica.TopRow + 1
             End If
           Next
         End If
       End If
       
       'a proxima pesquisa continuara a partir desta variavel
       CONTINUAPESQUISA = INICIOPESQUISA
       SSCommand2.Caption = "Pr�xima"
       Exit Sub
    End If
   Next
     
     
     MsgBox "Fim de Pesquisa", vbExclamation, "ATEN��O"
     SSCommand2.Caption = "Aplica��o"
     Exit Sub
           
TopRowError:
    grdFabrica.TopRow = 1
    Resume Next


End Sub

Private Sub SSCommand2_GotFocus()
  SSCommand2.Refresh
End Sub


Private Sub SSCommand3_Click()

  grdFabrica.Visible = False
  ssfraPesq.Visible = False
  Erase arrmontadora
  Erase arrgrupo
  Erase arrsubgrupo

  montadora = 0
  grupo = 0
  subgrupo = 0
  
  cboMontadora.Clear
  cboGrupo.Clear
  cboSubgrupo.Clear

End Sub


Private Sub Text1_GotFocus()
    
    SSCommand1.Caption = "F�brica"
    SSCommand2.Caption = "Aplica��o"
  
  'reset nas posi�oes do grid
    frmFabrica.grdFabrica.HighLight = False
    grdFabrica.TopRow = 1
    grdFabrica.Row = 1
    grdFabrica.Col = 2


  'Variaveis que definem inicio ou continuacao da pesquisa
  
    CONTINUAPESQUISA = 0
    OCORRENCIA = 0
    CONTADOROCORRENCIA = 0




End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


