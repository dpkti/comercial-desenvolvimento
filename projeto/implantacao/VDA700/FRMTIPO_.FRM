VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmTipo_Vdr 
   Caption         =   "Tipo de VDR"
   ClientHeight    =   4140
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4140
   ScaleWidth      =   6690
   Begin MSGrid.Grid grdVdr 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "FRMTIPO_.frx":0000
   End
End
Attribute VB_Name = "frmTipo_Vdr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
 
  Unload Me
End Sub


Private Sub Form_Load()

    On Error GoTo TrataErro
    
    Dim SQL As String
    Dim ss As Object
    Dim I As Integer
    
    'mouse
    Screen.MousePointer = vbHourglass
    
    'montar SQL
    SQL = " Select a.cod_fornecedor, b.sigla,"
    SQL = SQL & " a.cod_grupo,a.cod_subgrupo,a.cod_tipo_cliente,"
    SQL = SQL & " a.cod_vdr"
    SQL = SQL & " from vdr.controle_vdr a, "
    SQL = SQL & " fornecedor b"
    SQL = SQL & " where  a.cod_fornecedor=b.cod_fornecedor"
    SQL = SQL & " Group by a.cod_fornecedor,b.sigla,"
    SQL = SQL & " a.cod_grupo,a.cod_subgrupo,a.cod_tipo_cliente,"
    SQL = SQL & " a.cod_vdr"
    SQL = SQL & " Order by a.cod_fornecedor,b.sigla,"
    SQL = SQL & " a.cod_grupo,a.cod_subgrupo,a.cod_tipo_cliente,"
    SQL = SQL & " a.cod_vdr"
    
    'criar consulta
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.bof Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o h� Controle para VDR cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    
    'carrega dados
    If ss.recordcount > 1 Then
      With frmTipo_Vdr.grdVdr
        .Cols = 6
        .Rows = ss.recordcount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 800
        .ColWidth(2) = 800
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
        .ColWidth(5) = 800
        
        
        .Row = 0
        .Col = 0
        .Text = "Forn"
        .Col = 1
        .Text = "Sigla"
        .Col = 2
        .Text = "Grupo"
        .Col = 3
        .Text = "SubGrupo"
        .Col = 4
        .Text = "Tp.Cliente"
        .Col = 5
        .Text = "Cod."

        
        
        
        ss.movefirst
        For I = 1 To .Rows - 1
            .Row = I
            
            .Col = 0
            .Text = ss!cod_fornecedor
            .Col = 1
            .Text = ss!sigla
            .Col = 2
            .Text = ss!cod_grupo
            .Col = 3
            .Text = ss!cod_subgrupo
            .Col = 4
            .Text = ss!cod_tipo_cliente
            .Col = 5
            .Text = ss!cod_vdr
            
            ss.movenext
        Next
        .Row = 1
    End With
  Else
    lngCod_vdr = ss!cod_vdr
    Unload Me
  End If
            
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If

End Sub


Private Sub grdVdr_DblClick()
  grdVdr.Col = 5
  lngCod_vdr = grdVdr.Text
  Unload Me
End Sub

