VERSION 4.00
Begin VB.Form frmTranspCif 
   Caption         =   "Lista de Transportadoras - CIF"
   ClientHeight    =   3075
   ClientLeft      =   1170
   ClientTop       =   1995
   ClientWidth     =   7020
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   Height          =   3480
   Left            =   1110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   7020
   Top             =   1650
   Width           =   7140
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   4935
   End
   Begin MSGrid.Grid grdTransportadora 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   6855
      _version        =   65536
      _extentx        =   12091
      _extenty        =   3836
      _stockprops     =   77
      backcolor       =   16777215
      fixedcols       =   0
      mouseicon       =   "L_TRACIF.frx":0000
   End
End
Attribute VB_Name = "frmTranspCif"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String
Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmTranspCif = Nothing
End Sub


Private Sub Form_Load()

    On Error GoTo TrataErro
    
    
    Dim ss As Object
    Dim i As Integer
    
    'mouse
    Screen.MousePointer = vbHourglass
    
    'nome do form
    frmTranspCif.Caption = frmTranspCif.Caption & " - Uf: " & frmFimPedido.lblCOD_UF.Caption
    
    'montar SQL
    SQL = "select transp.COD_TRANSP,"
    SQL = SQL & "transp.NOME_TRANSP,"
    SQL = SQL & "transp.VIA_TRANSP,"
    SQL = SQL & "decode(frete.FL_BLOQUEIO,'S',' SIM ','     ') FL_LIBERADO "
    SQL = SQL & "from TRANSPORTADORA transp,FRETE_UF_BLOQ frete "
    SQL = SQL & "where transp.SITUACAO = 0 and "
    SQL = SQL & "frete.COD_TRANSP = transp.COD_TRANSP and "
    SQL = SQL & "frete.COD_UF = :uf "
    SQL = SQL & "order by transp.NOME_TRANSP"
    
    'criar consulta
    
    oradatabase.Parameters.Remove "uf"
    oradatabase.Parameters.Add "uf", frmFimPedido.lblCOD_UF.Caption, 1
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
       
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha transportadora cadastrada", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    With grdTransportadora
        .Cols = 4
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 5085
        .ColWidth(2) = 300
        .ColWidth(3) = 700
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Nome da Transportadora"
        .Col = 2
        .Text = "Via"
        .Col = 3
        .Text = "Liberado"
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss("COD_TRANSP")
            .Col = 1
            .Text = ss("NOME_TRANSP")
            .Col = 2
            .Text = ss("VIA_TRANSP")
            .Col = 3
            .Text = ss("FL_LIBERADO")
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
        
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Then
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmTransp = Nothing
End Sub


Private Sub grdTransportadora_DblClick()
    grdTransportadora.Col = 0
    txtResposta = grdTransportadora.Text
    Unload Me
End Sub

Private Sub grdTransportadora_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdTransportadora.Col = 1
    If KeyAscii = 13 Then
        grdTransportadora.Col = 0
        txtResposta = grdTransportadora.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdTransportadora.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdTransportadora
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdTransportadora.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdTransportadora
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdTransportadora.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub

