VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmfil046 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   2250
   ClientLeft      =   5400
   ClientTop       =   4740
   ClientWidth     =   4635
   Icon            =   "frmfil046.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2250
   ScaleWidth      =   4635
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   60
      Top             =   1560
   End
   Begin VB.TextBox txtCaminho 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   750
      TabIndex        =   4
      Top             =   30
      Width           =   3855
   End
   Begin MSComctlLib.StatusBar stStatus 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   2
      Top             =   1965
      Width           =   4635
      _ExtentX        =   8176
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1835
            MinWidth        =   1835
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3263
            MinWidth        =   3263
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   3263
            MinWidth        =   3263
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Bot�o.cmd cmdIniciar 
      Height          =   375
      Left            =   3480
      TabIndex        =   1
      Top             =   1590
      Width           =   1125
      _ExtentX        =   1984
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "Iniciar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmfil046.frx":57E2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.ListBox lstInfo 
      Appearance      =   0  'Flat
      Height          =   1200
      Left            =   60
      TabIndex        =   0
      Top             =   360
      Width           =   4545
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Caminho:"
      Height          =   195
      Left            =   90
      TabIndex        =   3
      Top             =   90
      Width           =   660
   End
End
Attribute VB_Name = "frmfil046"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Path_drv As String

Private Sub cmdIniciar_Click()
      Dim vResp As Integer
1     On Error GoTo Tratamento
2         Timer1.Enabled = False
3         stStatus.Panels(1).Text = "Tempo:"
4         stStatus.Panels(2).Text = DateTime.Now
5         If cmdIniciar.Caption = "Sair" Then
6             End
7         Else
8             vResp = MsgBox("O processo ser� iniciado." & vbCrLf & "Todos os sistemas est�o fechados?", vbYesNo, "Aten��o")
9             If vResp = 6 Then
                  'Verifica se existe usuario conectado no banco
10                If Dir$(Path_drv & "BASE_DPK.ldb") <> "" Then
11                    MsgBox "N�o ser� possivel compactar o banco, por favor feche todos os sistemas.", vbCritical, "Aten��o"
12                    Exit Sub
13                Else
14                    cmdIniciar.Enabled = False
15                    lstInfo.AddItem "Iniciando processo...AGUARDE"
16                    Me.Refresh
17                    Call ADO_CompactarDB
18                    stStatus.Panels(3).Text = DateTime.Now
19                    End
20                End If
21            Else
22                stStatus.Panels(3).Text = DateTime.Now
23                End
24            End If
25        End If
Tratamento:
26    If Err.Number <> 0 Then
27      MsgBox "Sub cmdIniciar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & vbCrLf & "Entre em contato com suporte e notifique esta mensagem."
28    End If
End Sub

Private Sub Form_Load()
1         If App.PrevInstance = True Then
2             MsgBox "O aplicativo se encontra aberto", vbInformation
3             End
4         End If
5         Me.Caption = "FIL046: Otimiza��o do banco - vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
          'Path_drv = PegaIni("Diretorio", "Caminho", "Caminho.ini")
6         txtCaminho.Text = Pegar_Drive & "\Dados\"
7         Path_drv = txtCaminho.Text
8         If Path_drv = "" Then
9             MsgBox "Sistema n�o localizou o diret�rio" & vbCrLf & "para otimiza��o do banco de dados." & vbCrLf & "Por favor entre em contato com suporte DPK.", vbCritical, "Notifica��o"
10            cmdIniciar.Enabled = False
11        End If
End Sub

Private Sub ADO_CompactarDB()
          Dim vNomeOri As String, vNomeBak As String
          Dim JR As New jro.JetEngine
          Dim S_DbNome As String, S_DbTemp As String

1         On Error GoTo Tratamento

          'Descobrimos o caminho do Arquivo Original e do Arquivo Temp
2         S_DbNome = Path_drv & "BASE_DPK.mdb"
3         S_DbTemp = Path_drv & "BASE_DPK.new"
4         If Right$(Path_drv, 1) = "\" Then
5             S_DbNome = Path_drv & "BASE_DPK.mdb"
6             S_DbTemp = Path_drv & "BASE_DPK.new"
7         End If

          'Descobrimos se o Arquivo Temp existe...
8         If Dir$(S_DbTemp) <> "" Then
              'Se existe, deletamos (vide *Kill l� em baixo)
9             Call Kill(S_DbTemp)
10        End If

          'Verifica se existe usuario conectado no banco
11        If Dir$(Path_drv & "BASE_DPK.ldb") <> "" Then
12            MsgBox "N�o ser� possivel compactar o banco, por favor feche todos os sistemas.", vbCritical, "Aten��o"
13            Exit Sub
14        Else
              'Faz uma copia de seguran�a
15            lstInfo.AddItem "Iniciando copia de seguran�a...AGUARDE"
16            Me.Refresh
17            If Dir$(Path_drv & "BASE_DPK.old") <> "" Then
18                Kill Path_drv & "BASE_DPK.old"
19            End If
20            vNomeOri = Path_drv & "BASE_DPK.mdb"
21            vNomeBak = Path_drv & "BASE_DPK.old"
22            FileCopy vNomeOri, vNomeBak
23            lstInfo.AddItem "Copia de seguran�a realizada...AGUARDE"
24            Me.Refresh
25        End If

          'Compactamos o banco de dados com o
          'nome de Arquivo Temp
26        lstInfo.AddItem "Iniciando otimiza��o do banco...AGUARDE"
27        Me.Refresh
28        JR.CompactDatabase "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & S_DbNome & ";Jet OLEDB:Database Password=", _
          "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & S_DbTemp & ";Jet OLEDB:Engine Type=4;Jet OLEDB:Database Password="
          'Este "Engine Type" no final indica a vers�o do Access que est�
          'sendo usada. Veja os valores e as vers�es correspondentes:
          '5 (Defaut) para Access 2000
          '4 para Access 97
          '3 para Access 95/6
          '2 para Access 2
          '1 para Access 1

          'Se Arquivo Original existir...
29        If Dir$(S_DbNome) <> "" Then
30            Kill S_DbNome
31            Name S_DbTemp As S_DbNome
32        End If
33        Me.Refresh

34        Set JR = Nothing
35        lstInfo.AddItem "Otimiza��o realizada com sucesso..."
36        lstInfo.AddItem "Processo Finalizado."
37        cmdIniciar.Caption = "Sair"
38        cmdIniciar.Enabled = True
39    Exit Sub

Tratamento:
40        If Err.Number <> 0 Then
41            MsgBox "Sub cmdIniciar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & vbCrLf & "Entre em contato com suporte e notifique esta mensagem."
42        End If
End Sub

Private Sub Timer1_Timer()
    Call cmdIniciar_Click
End Sub
