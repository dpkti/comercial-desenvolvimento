Attribute VB_Name = "modFil046"
Option Explicit
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function GetProfileString Lib "kernel32" Alias "GetProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer) As Integer
Public Const SW_SHOWNORMAL = 1
Function PegaIni(sessao As String, chave As String, Optional ARQUIVO As String)
    Dim Val As String
    Dim Valor As Integer
    If Len(Trim(ARQUIVO)) = 0 Then
        ARQUIVO = App.Path & "\" & Replace(LCase(App.EXEName), ".exe", "") & ".ini"
    Else
        ARQUIVO = App.Path & "\" & ARQUIVO
    End If
    Val = String$(255, 0)
    Valor = GetPrivateProfileString(sessao, chave, "", Val, Len(Val), ARQUIVO)
    If Valor = 0 Then
        PegaIni = ""
    Else
        PegaIni = Left(Val, Valor)
    End If
End Function

'Sub GravaIni(sessao As String, chave As String, Valor As String, Optional ARQUIVO As String)

'    Dim result As Integer
'    If Len(Trim(ARQUIVO)) = 0 Then
'        ARQUIVO = App.Path & "\" & Replace(LCase(App.EXEName), ".exe", "") & ".ini"
'    End If
'    result = WritePrivateProfileString(sessao, chave, Valor, ARQUIVO)
'End Sub

Public Function FileExist(FileName As String) As Boolean
    Dim TempAttr As Integer
    On Error GoTo ErrorFileExist
    TempAttr = GetAttr(FileName)
    FileExist = True
    GoTo ExitFileExist
ErrorFileExist:
    FileExist = False
    Resume ExitFileExist
ExitFileExist:
    On Error GoTo 0
End Function

Public Function Pegar_Drive() As String
    Dim fs, d, dc, n
    Dim vArq As Integer
   
    Pegar_Drive = Left(App.Path, 2)
    If Pegar_Drive = "\\" Then
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set dc = fs.Drives
        For Each d In dc
            If d.DriveType = 3 Then
                n = d.ShareName
            Else
                n = d.Path
            End If
            If InStr(1, App.Path, n) > 0 Then
                Pegar_Drive = d.DriveLetter & ":"
                Exit For
            End If
        Next
    End If
    If Pegar_Drive = "\\" Then
        vArq = FreeFile
        If Dir("C:\DRIVE_PAAC.INI") <> "" Then
            Open "C:\DRIVE_PAAC.INI" For Input As #vArq
            Input #vArq, Pegar_Drive
        Else
            Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
            Open "C:\DRIVE_PAAC.INI" For Output As #vArq
            Print #vArq, Pegar_Drive
        End If
        Close #vArq
    End If

End Function

