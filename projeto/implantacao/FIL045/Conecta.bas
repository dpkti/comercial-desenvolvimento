Attribute VB_Name = "mFIL040"
Declare Function GetSystemMenu Lib "user32" (ByVal hwnd As Long, ByVal bRevert As Long) As Long
Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Long) As Long
Declare Function RemoveMenu Lib "user32" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long

Private Declare Sub WaitForSingleObject Lib "kernel32.dll" (ByVal hHandle As Long, ByVal dwMilliseconds As Long)
Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDA As Long, ByVal bIH As Integer, ByVal dwPID As Long) As Long
Private Declare Sub CloseHandle Lib "kernel32.dll" (ByVal hObject As Long)

Public Const MF_BYPOSITION = &H400&
Public Const MF_REMOVE = &H1000&

Public connBaseDpk As ADODB.Connection
Public connDBF As ADODB.Connection
Public Path_drv As String
Public vNomeArqDBF As String
Public vRst As ADODB.Recordset
Public i As Long
Public vArq As String
Public vIntArq As Integer
Public POS_SEP As Integer
Public CONTEUDO
Public strSQL As String
Dim vRegs As Long
Dim vRegsUp As Long
Dim vRegsIn As Long
Dim vRegsRec As Long
Dim vRegsDel As Long
Dim vRegsRst As Long
Dim vSQL As String

'************** PEDNOTA_VENDA ************
 Public Ped_Loja As Integer
 Public Ped_Num As Double
 Public Ped_Seq As Integer
 Public Ped_Loja_Nota
 Public Ped_Num_Nota
 Public Ped_Pen
 Public Ped_Fil
 Public Ped_Tp_Ped
 Public Ped_Tp_DB
 Public Ped_Tp_Tran
 Public Ped_Dt_Dig As String
 Public Ped_Dt_Ped As String
 Public Ped_Dt_Emiss As String
 Public Ped_Dt_Ssm As String
 Public Ped_Dt_Pol As String
 Public Ped_Dt_Cre As String
 Public Ped_Dt_Fre As String
 Public Ped_Nope
 Public Ped_Cli
 Public Ped_For
 Public Ped_Ent
 Public Ped_Cob
 Public Ped_Tran
 Public Ped_Rep
 Public Ped_Ven
 Public Ped_Pla
 Public Ped_Ban
 Public Ped_Dest
 Public Ped_Oper
 Public Ped_Fre
 Public Ped_Pes
 Public Ped_Qtd_Ssm
 Public Ped_Qtd_Ped
 Public Ped_Qtd_Not
 Public Ped_Vl
 Public Ped_Ipi
 Public Ped_Icm1
 Public Ped_Icm2
 Public Ped_Bas1
 Public Ped_Bas2
 Public Ped_Bas3
 Public Ped_Base
 Public Ped_Bas5
 Public Ped_Bas6
 Public Ped_Out
 Public Ped_Maj
 Public Ped_Ret
 Public Ped_Vl_Ipi
 Public Ped_Is_Ipi
 Public Ped_Ou_Ipi
 Public Ped_Vl_Fre
 Public Ped_Ace
 Public Ped_Des
 Public Ped_Suf
 Public Ped_Acr
 Public Ped_Seg
 Public Ped_Pc_Icm1
 Public Ped_Pc_Icm2
 Public Ped_Ali
 Public Ped_Can
 Public Ped_Fl_Ssm
 Public Ped_Fl_Nfis
 Public Ped_Fl_Pen
 Public Ped_Fl_Ace
 Public Ped_Fl_Icm
 Public Ped_Sit
 Public Ped_Pro
 Public Ped_Jur
 Public Ped_Che
 Public Ped_Com
 Public Ped_Dup
 Public Ped_Lim
 Public Ped_Sal
 Public Ped_Cli_Nov
 Public Ped_Desc
 Public Ped_Acre
 Public Ped_VlFat
 Public Ped_Desc1
 Public Ped_Desc2
 Public Ped_Desc3
 Public Ped_Fret
 Public Ped_MensP
 Public Ped_MensN
 Public Ped_CODVDR

'************** ITPEDNOTA_VENDA ***************
 Public Ite_Num_It
 Public Ite_Loja_Nota
 Public Ite_Num_Nota
 Public Ite_It_Nota
 Public Ite_Dpk
 Public Ite_Sol
 Public Ite_Ate
 Public Ite_Pre
 Public Ite_Tab
 Public Ite_Desc1
 Public Ite_Desc2
 Public Ite_Desc3
 Public Ite_Icm
 Public Ite_Ipi
 Public Ite_Com
 Public Ite_Tlm
 Public Ite_Trib
 Public Ite_Trip
 Public Ite_Sit

'**************** V_PEDLIQ_VENDA *****************
 Public V_P_Ite
 Public V_P_Sit
 Public V_P_Pr
 Public V_P_Vl
 Public V_P_Prs
 Public V_P_Vls

'***************** ROMANEIO ********************
 Public Rom_DtC
 Public Rom_DtD
 Public Rom_Num
 Public Rom_Con
 Public Rom_Car
 Public Rom_Qtd

'*************** R_PEDIDO_CONF
 Public R_P_Num
 Public R_P_Con
 Public R_P_Emb
 Public R_P_Exp
 Public R_P_Fl
 Public R_P_Qtd

Public Sub conectaBaseDPK(pDrive As String)
    Set connBaseDpk = CreateObject("ADODB.Connection")
    connBaseDpk.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & pDrive & "\Dados\Base_Dpk.mdb;User Id=admin;Password=;"
End Sub

Public Sub conectaBaseDBF(pDrive As String)
    Set connDBF = New ADODB.Connection
    connDBF.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & pDrive & "\Com\entrada\arquivos;Extended Properties=""DBASE IV"";"
End Sub
    
Sub zeraVariavel()
    vRegs = 0
    vRegsRst = 0
    vRegsIn = 0
    vRegsUp = 0
    vRegsDel = 0
    vRegsRst = 0
End Sub

Public Function Pegar_Drive() As String
    Dim fs, d, dc, n
    Dim vArq As Integer
    
    Pegar_Drive = Left(App.Path, 2)
    If Pegar_Drive = "\\" Then
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set dc = fs.Drives
        For Each d In dc
            If d.DriveType = 3 Then
                n = d.ShareName
            Else
                n = d.Path
            End If
            If InStr(1, App.Path, n) > 0 Then
                Pegar_Drive = d.DriveLetter & ":"
                Exit For
            End If
        Next
    End If

    If Pegar_Drive = "\\" Then
        vArq = FreeFile
        If Dir("C:\DRIVE_PAAC.INI") <> "" Then
            Open "C:\DRIVE_PAAC.INI" For Input As #vArq
            Input #vArq, Pegar_Drive
        Else
            Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
            Open "C:\DRIVE_PAAC.INI" For Output As #vArq
            Print #vArq, Pegar_Drive
        End If
        Close #vArq
    End If
End Function

Sub Atualizar_lsv(pNomeTabela As String, pAcao As String, pRegs As Long)
    For i = 1 To frmFil040.lsvDados.ListItems.Count
        If UCase(pNomeTabela) = UCase(frmFil040.lsvDados.ListItems(i)) Then
            If pAcao = "RECEBIDO" Then
                frmFil040.lsvDados.ListItems(i).SubItems(1) = pRegs
                frmFil040.lsvDados.ListItems(i).Selected = True
                frmFil040.lsvDados.SelectedItem.EnsureVisible
                Exit For
            ElseIf pAcao = "UPDATE" Then
                frmFil040.lsvDados.ListItems(i).SubItems(2) = pRegs
                frmFil040.lsvDados.ListItems(i).Selected = True
                frmFil040.lsvDados.SelectedItem.EnsureVisible
                Exit For
            ElseIf pAcao = "INSERT" Then
                frmFil040.lsvDados.ListItems(i).SubItems(3) = pRegs
                frmFil040.lsvDados.ListItems(i).Selected = True
                frmFil040.lsvDados.SelectedItem.EnsureVisible
                Exit For
            ElseIf pAcao = "DELETE" Then
                frmFil040.lsvDados.ListItems(i).SubItems(4) = pRegs
                frmFil040.lsvDados.ListItems(i).Selected = True
                frmFil040.lsvDados.SelectedItem.EnsureVisible
                Exit For
            End If
        End If
    Next
    frmFil040.Refresh
    
End Sub
Sub ProgressBar(pTamanho As Long, pValue As Integer)
    
'pTamanho: Significa o tamanho total do recordset, ou seja , o tamanha maximo para a barra de progresso.
'pValue: Caso o valor do paramentro vier zero(0) significa que a barra de progresso ser� inicializada / zerada.
'        Caso o valor do paramentro vier um(1) significa que a barra ser� incrementada com mais um registro.
'Parametro -1: � apenas um valor opcional para n�o executar determinado paramentro.
    
    If pTamanho <> -1 Then
        If pTamanho = 0 Then
            frmFil040.PB.Max = 100
            frmFil040.PB.Value = 100
        Else
            frmFil040.PB.Max = pTamanho
        End If
    Else
        If pValue <> -1 Then
            If pValue = 0 Then
                frmFil040.PB.Value = pValue
            Else
                frmFil040.PB.Value = frmFil040.PB.Value + pValue
            End If
        End If
    End If
End Sub


Public Function ATU_ANTECIPACAO_TRIBUTARIA() As Boolean

    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ANTECIPA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ANTECIPACAO_TRIBUTARIA", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "delete * from ANTECIPACAO_TRIBUTARIA", vRegsDel
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "INSERT INTO ANTECIPACAO_TRIBUTARIA (" & _
                                "COD_UF_ORIGEM, " & _
                                "COD_UF_DESTINO, " & _
                                "PC_MARGEM_LUCRO) VALUES (" & _
                                IIf(IsNull(vRst!cod_uf_ori), "Null", "'" & vRst!cod_uf_ori & "'") & _
                                "," & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                                "," & IIf(IsNull(vRst!PC_MARGEM), 0, vRst!PC_MARGEM) & ")"
            vRst.MoveNext
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        Next
    End If
        
    Atualizar_lsv "ANTECIPACAO_TRIBUTARIA", "INSERT", vRegsIn
    Atualizar_lsv "ANTECIPACAO_TRIBUTARIA", "DELETE", vRegsDel
    Atualizar_lsv "ANTECIPACAO_TRIBUTARIA", "UPDATE", vRegsUp
    
    Set vRst = Nothing
          
    ATU_ANTECIPACAO_TRIBUTARIA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ANTECIPACAO_TRIBUTARIA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ANTECIPACAO_TRIBUTARIA - IQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ALIQUOTA_ME() As Boolean
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
               
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ALIQUOTA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ALIQUOTA_ME", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "delete * from ALIQUOTA_ME ", vRegsDel
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "Insert into aliquota_me(cod_loja, cgc, pc_icm)values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & ", " & IIf(IsNull(vRst!cgc), 0, vRst!cgc) & ", " & IIf(IsNull(vRst!pc_icm), 0, vRst!pc_icm) & ")"
            vRegsIn = vRegsIn + 1
            vRst.MoveNext
            ProgressBar -1, 1
        Next
    End If
    
    Atualizar_lsv "ALIQUOTA_ME", "INSERT", vRegsIn
    Atualizar_lsv "ALIQUOTA_ME", "DELETE", vRegsDel
    Atualizar_lsv "ALIQUOTA_ME", "UPDATE", vRegsUp
    Set vRst = Nothing
    
    ATU_ALIQUOTA_ME = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ALIQUOTA_ME = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ALIQUOTA_ME" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_APLICACAO() As Boolean
    On Error GoTo VerErro
    Dim cod_original As String

    Call zeraVariavel
    ProgressBar -1, 0
  
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from APLICACA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "APLICACAO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        If vRst!cod_origin <> Null Then
            cod_original = Replace(vRst!cod_origin, "'", "")
        End If
        connBaseDpk.Execute "UPDATE APLICACAO " & _
                          " SET COD_ORIGINAL = " & IIf(IsNull(cod_original), "Null", "'" & cod_original & "'") & "," & _
                          " DESC_APLICACAO = " & IIf(IsNull(Replace("" & vRst!DESC_APLIC, "'", "")), "Null", "'" & Replace("" & vRst!DESC_APLIC, "'", "") & "'") & _
                          " WHERE COD_DPK = " & vRst!COD_DPK & _
                          " AND COD_CATEGORIA = '" & vRst!cod_catego & _
                          "' AND COD_MONTADORA = " & vRst!cod_montad & _
                          " AND SEQUENCIA = " & vRst!sequencia, vRegs
                          
        If vRegs = 0 Then
            connBaseDpk.Execute "INSERT INTO APLICACAO VALUES (" & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & "," & IIf(IsNull(vRst!cod_catego), "null", "'" & vRst!cod_catego & "'") & "," & IIf(IsNull(vRst!cod_montad), 0, vRst!cod_montad) & "," & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & "," & IIf(IsNull(vRst!cod_origin), "null", "'" & vRst!cod_origin & "'") & "," & IIf(IsNull(Replace("" & vRst!DESC_APLIC, "'", "")), "null", "'" & Replace("" & vRst!DESC_APLIC, "'", "") & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
       
        ProgressBar -1, 1
        
        vRst.MoveNext
    Next
    
    Atualizar_lsv "APLICACAO", "UPDATE", vRegsUp
    Atualizar_lsv "APLICACAO", "INSERT", vRegsIn
    Atualizar_lsv "APLICACAO", "DELETE", vRegsDel

    Set vRst = Nothing
    
    ATU_APLICACAO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_APLICACAO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_APLICACAO - AW" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_BANCO() As Boolean

On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from BANCO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "BANCO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "UPDATE BANCO SET SIGLA = " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & " WHERE COD_BANCO = " & vRst!Cod_Banco, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "INSERT INTO BANCO VALUES(" & IIf(IsNull(vRst!Cod_Banco), 0, vRst!Cod_Banco) & "," & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "BANCO", "UPDATE", vRegsUp
    Atualizar_lsv "BANCO", "DELETE", vRegsDel
    Atualizar_lsv "BANCO", "INSERT", vRegsIn
    Set vRst = Nothing
       
    ATU_BANCO = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_BANCO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_BANCO - AA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CANCEL_PEDNOTA() As Boolean
          
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
                   
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CANCEL_P#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CANCEL_PEDNOTA", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from cancel_pednota", vRegsDel
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "insert into cancel_pednota values(" & IIf(IsNull(vRst!Cod_Cancel), "null", vRst!Cod_Cancel) & ", " & IIf(IsNull(vRst!Desc_cance), "null", "'" & vRst!Desc_cance & "'") & ")"
            vRegsIn = vRegsIn + 1
            vRst.MoveNext
            ProgressBar -1, 1
        Next
    End If
       
    Atualizar_lsv "CANCEL_PEDNOTA", "INSERT", vRegsIn
    Atualizar_lsv "CANCEL_PEDNOTA", "UPDATE", vRegsUp
    Atualizar_lsv "CANCEL_PEDNOTA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CANCEL_PEDNOTA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_CANCEL_PEDNOTA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CANCEL_PEDNOTA - AB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CATEGSINAL() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
          
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CATEG_S#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CATEG_SINAL", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update categ_sinal set sinal = " & IIf(IsNull(vRst!sinal), "null", "'" & vRst!sinal & "'") & " where categoria = " & vRst!categoria, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into categ_sinal values(" & IIf(IsNull(vRst!categoria), 0, vRst!categoria) & ", " & IIf(IsNull(vRst!sinal), "null", "'" & vRst!sinal & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CATEG_SINAL", "INSERT", vRegsIn
    Atualizar_lsv "CATEG_SINAL", "UPDATE", vRegsUp
    Atualizar_lsv "CATEG_SINAL", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CATEGSINAL = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_CATEGSINAL = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CATEGSINAL - BB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CESTAITEM() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
       
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CESTAI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CESTA_ITEM", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from Cesta_Item", vRegsDel
        For i = 1 To vRegsRst
            DoEvents
            connBaseDpk.Execute "Insert into Cesta_Item values(" & IIf(IsNull(vRst!Cod_loja), "null", vRst!Cod_loja & ", " & IIf(IsNull(vRst!COD_DPK), "null", vRst!COD_DPK) & ", " & IIf(IsNull(vRst!Tp_Cesta), "null", vRst!Tp_Cesta)) & ")"
            vRegsIn = vRegsIn + 1
            vRst.MoveNext
            ProgressBar -1, 1
        Next
    End If
    
    Atualizar_lsv "CESTA_ITEM", "INSERT", vRegsIn
    Atualizar_lsv "CESTA_ITEM", "DELETE", vRegsDel
    Atualizar_lsv "CESTA_ITEM", "UPDATE", vRegsUp
    Set vRst = Nothing
    ATU_CESTAITEM = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_CESTAITEM = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CESTAITEM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CESTAVENDA() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
       
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CESTAV#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CESTA_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from Cesta_Venda", vRegsDel
        For i = 1 To vRegsRst
            DoEvents
            connBaseDpk.Execute "Insert into Cesta_Venda values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & ", " & IIf(IsNull(vRst!Tp_Cesta), "Null", vRst!Tp_Cesta) & ", " & IIf(IsNull(vRst!NomeCest), "Null", "'" & vRst!NomeCest & "'") & ", " & IIf(IsNull(vRst!DtVigenc), "Null", vRst!DtVigenc) & ")"
            vRegsIn = vRegsIn + 1
            vRst.MoveNext
            ProgressBar -1, 1
        Next
    End If
       
    Atualizar_lsv "CESTA_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "CESTA_VENDA", "DELETE", vRegsDel
    Atualizar_lsv "CESTA_VENDA", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    
    ATU_CESTAVENDA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_CESTAVENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CESTAVENDA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CIDADE() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CIDADE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CIDADE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update cidade set Nome_Cidade = " & IIf(IsNull(Replace(vRst!Nome_Cidad, "'", "")), "Null", "'" & Replace(vRst!Nome_Cidad, "'", "") & "'") & _
                            ", Tp_Cidade = " & IIf(IsNull(vRst!Tp_Cidade), "Null", "'" & vRst!Tp_Cidade & "'") & ", Cod_uf = " & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                            ", Cod_Regiao = " & IIf(IsNull(vRst!Cod_Regiao), 0, vRst!Cod_Regiao) & _
                            ", Cod_Subregiao = " & IIf(IsNull(vRst!Cod_SubReg), 0, vRst!Cod_SubReg) & _
                            ", Caracteristica = " & IIf(IsNull(vRst!caracteris), 0, vRst!caracteris) & " where Cod_Cidade = " & vRst!cod_cidade, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into cidade values(" & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                                                           ", " & IIf(IsNull(Replace(vRst!Nome_Cidad, "'", "")), "Null", "'" & Replace(vRst!Nome_Cidad, "'", "") & "'") & _
                                                           ", " & IIf(IsNull(vRst!Tp_Cidade), "null", "'" & vRst!Tp_Cidade & "'") & _
                                                           ", " & IIf(IsNull(vRst!Cod_uf), "null", "'" & vRst!Cod_uf & "'") & _
                                                           ", " & IIf(IsNull(vRst!Cod_Regiao), 0, vRst!Cod_Regiao) & _
                                                           ", " & IIf(IsNull(vRst!Cod_SubReg), 0, vRst!Cod_SubReg) & _
                                                           ", " & IIf(IsNull(vRst!caracteris), 0, vRst!caracteris) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CIDADE", "INSERT", vRegsIn
    Atualizar_lsv "CIDADE", "UPDATE", vRegsUp
    Atualizar_lsv "CIDADE", "DELETE", vRegsDel
    Set vRst = Nothing
    ATU_CIDADE = True
VerErro:
    If Err.Number <> 0 Then
        ATU_CIDADE = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CIDADE - AC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLASS_ANTEC_ENTRADA() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLASSANT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLASS_ANTEC_ENTRADA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Class_Antec_Entrada set Cod_Trib = " & IIf(IsNull(vRst!Cod_Trib), 0, vRst!Cod_Trib) & _
                            ", Pc_Margem_Lucro = " & IIf(IsNull(vRst!Pc_Marg_lu), 0, vRst!Pc_Marg_lu) & " where Class_Fiscal = " & vRst!Class_Fisc & _
                            " and Cod_Uf_Origem = '" & vRst!cod_uf_ori & _
                            "' and Cod_Uf_Destino = '" & vRst!cod_uf_des & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Class_Antec_Entrada values(" & IIf(IsNull(vRst!Class_Fisc), 0, vRst!Class_Fisc) & _
                                                                        ", " & IIf(IsNull(vRst!cod_uf_ori), "Null", "'" & vRst!cod_uf_ori & "'") & _
                                                                        ", " & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                                                                        ", " & IIf(IsNull(vRst!Cod_Trib), 0, vRst!Cod_Trib) & _
                                                                        ", " & IIf(IsNull(vRst!Pc_Marg_lu), 0, vRst!Pc_Marg_lu) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLASS_ANTEC_ENTRADA", "UPDATE", vRegsUp
    Atualizar_lsv "CLASS_ANTEC_ENTRADA", "INSERT", vRegsIn
    Atualizar_lsv "CLASS_ANTEC_ENTRADA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CLASS_ANTEC_ENTRADA = True
            
VerErro:
    If Err.Number <> 0 Then
        ATU_CLASS_ANTEC_ENTRADA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLASS_ANTEC_ENTRADA - KL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIECRED() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIE_CRE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIE_CREDITO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Clie_Credito set Dt_Ult_Compra = " & IIf(IsNull(vRst!dt_ult_com), "Null", "'" & vRst!dt_ult_com & "'") & _
                            ", Nota_Credito = " & IIf(IsNull(vRst!Nota_Credi), 0, vRst!Nota_Credi) & ", Limite_Credito = " & IIf(IsNull(vRst!Limite_Cre), 0, vRst!Limite_Cre) & _
                            " Where Cod_Cliente = " & vRst!cod_client, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Clie_Credito values(" & IIf(IsNull(vRst!cod_client), 0, vRst!cod_client) & _
                                                                ", " & IIf(IsNull(vRst!dt_ult_com), "Null", "'" & vRst!dt_ult_com & "'") & _
                                                                ", " & IIf(IsNull(vRst!Nota_Credi), 0, vRst!Nota_Credi) & _
                                                                ", " & IIf(IsNull(vRst!Limite_Cre), 0, vRst!Limite_Cre) & ")"

            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIE_CREDITO", "INSERT", vRegsIn
    Atualizar_lsv "CLIE_CREDITO", "UPDATE", vRegsUp
    Atualizar_lsv "CLIE_CREDITO", "DELETE", vRegsDel
    Set vRst = Nothing
    ATU_CLIECRED = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_CLIECRED = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIECRED - BC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIEEND() As Boolean
          
    On Error GoTo VerErro
          

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIE_END#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIE_ENDERECO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Clie_Endereco " & _
                            " set Tp_Endereco = " & IIf(IsNull(vRst!Tp_Enderec), "Null", "'" & vRst!Tp_Enderec & "'") & _
                            ", Sequencia = " & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & _
                            ", Endereco = " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                            ", Cod_Cidade = " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                            ", Bairro = " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                            ", Cep = " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                            ", DDD = " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                            ", Fone = " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                            ", Fax = " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                            ", CxPostal = " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & _
                            ", Tp_Docto = " & IIf(IsNull(vRst!Tp_Docto), 0, vRst!Tp_Docto) & _
                            ", Cgc = " & IIf(IsNull(vRst!cgc), 0, vRst!cgc) & _
                            ", Inscr_Estadual = " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & ", Nome_Cliente = " & IIf(IsNull(vRst!Nome_Clien), "Null", "'" & vRst!Nome_Clien & "'") & _
                            " Where Cod_Cliente = " & vRst!cod_client, vRegs
                            
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Clie_Endereco values(" & IIf(IsNull(vRst!cod_client), 0, vRst!cod_client) & _
                                ", " & IIf(IsNull(vRst!Tp_Enderec), "Null", "'" & vRst!Tp_Enderec & "'") & _
                                ", " & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & _
                                ", " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                                ", " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                                ", " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                                ", " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                                ", " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                                ", " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                                ", " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & _
                                ", " & IIf(IsNull(vRst!Tp_Docto), 0, vRst!Tp_Docto) & _
                                ", " & IIf(IsNull(vRst!cgc), 0, vRst!cgc) & _
                                ", " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & _
                                ", " & IIf(IsNull(vRst!Nome_Clien), "Null", "'" & vRst!Nome_Clien & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIE_ENDERECO", "UPDATE", vRegsUp
    Atualizar_lsv "CLIE_ENDERECO", "INSERT", vRegsIn
    Atualizar_lsv "CLIE_ENDERECO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CLIEEND = True
VerErro:
    If Err.Number <> 0 Then
        ATU_CLIEEND = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIEEND - AE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIEINT() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIE_INT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIENTE_INTERNET", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Cliente_Internet set E_mail = " & IIf(IsNull(vRst!E_mail), "Null", "'" & vRst!E_mail & "'") & _
                            ", HomePage = " & IIf(IsNull(vRst!HomePage), "Null", "'" & vRst!HomePage & "'") & " Where Cod_Cliente = " & vRst!cod_client, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Cliente_Internet values(" & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client) & ", " & IIf(IsNull(vRst!E_mail), "Null", "'" & vRst!E_mail & "'") & ", " & IIf(IsNull(vRst!HomePage), "Null", "'" & vRst!HomePage & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
    
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIENTE_INTERNET", "UPDATE", vRegsUp
    Atualizar_lsv "CLIENTE_INTERNET", "DELETE", vRegsDel
    Atualizar_lsv "CLIENTE_INTERNET", "INSERT", vRegsIn
    Set vRst = Nothing
    ATU_CLIEINT = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_CLIEINT = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIEINT - MF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIEMSG() As Boolean
    
    On Error GoTo VerErro
         
    Call zeraVariavel
    ProgressBar -1, 0

    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIE_MEN#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIE_MENSAGEM", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Clie_Mensagem set Desc_Mens = " & IIf(IsNull(vRst!Desc_Mens), "Null", "'" & vRst!Desc_Mens & "'") & ", Fl_Bloqueio = " & IIf(IsNull(vRst!Fl_Bloquei), "Null", "'" & vRst!Fl_Bloquei & "'") & " where Cod_Mensagem = " & vRst!Cod_Mensag, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Clie_Mensagem values(" & IIf(IsNull(vRst!Cod_Mensag), "Null", vRst!Cod_Mensag & ", " & IIf(IsNull(vRst!Desc_Mens), "Null", "'" & vRst!Desc_Mens & "'") & ", " & IIf(IsNull(vRst!Fl_Bloquei), "Null", "'" & vRst!Fl_Bloquei & "'")) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIE_MENSAGEM", "INSERT", vRegsIn
    Atualizar_lsv "CLIE_MENSAGEM", "UPDATE", vRegsUp
    Atualizar_lsv "CLIE_MENSAGEM", "DELETE", vRegsDel
    Set vRst = Nothing
    ATU_CLIEMSG = True
VerErro:
    If Err.Number <> 0 Then
        ATU_CLIEMSG = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIEMSG - BS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIENTE() As Boolean
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIENTE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIENTE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Cliente set Cgc = " & IIf(IsNull(vRst!cgc), "Null", vRst!cgc) & _
                            ", Nome_Cliente = " & IIf(IsNull(vRst!Nome_Clien), "Null", "'" & vRst!Nome_Clien & "'") & _
                            ", Nome_Contato = " & IIf(IsNull(vRst!Nome_Conta), "Null", "'" & vRst!Nome_Conta & "'") & _
                            ", Classificacao = " & IIf(IsNull(vRst!Classifica), "Null", "'" & vRst!Classifica & "'") & _
                            ", Endereco = " & IIf(IsNull(Replace("" & vRst!endereco, "'", "")), "Null", "'" & Replace("" & vRst!endereco, "'", "") & "'") & _
                            ", Cod_Cidade = " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                            ", Bairro = " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                            ", DDD1 = " & IIf(IsNull(vRst!DDD1), "Null", vRst!DDD1) & _
                            ", Fone1 = " & IIf(IsNull(vRst!Fone1), "Null", vRst!Fone1) & _
                            ", DDD2 = " & IIf(IsNull(vRst!DDD2), "Null", vRst!DDD2) & _
                            ", Fone2 = " & IIf(IsNull(vRst!fone2), "Null", vRst!fone2) & _
                            ", Cep = " & IIf(IsNull(vRst!cep), "Null", vRst!cep) & _
                            ", CxPostal = " & IIf(IsNull(vRst!cxpostal), "Null", vRst!cxpostal) & _
                            ", Telex = " & IIf(IsNull(vRst!Telex), "Null", vRst!Telex) & _
                            ", Fax = " & IIf(IsNull(vRst!fax), "Null", vRst!fax) & _
                            ", Inscr_Estadual = " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & _
                            ", Inscr_Suframa = " & IIf(IsNull(vRst!Inscr_Sufr), "Null", "'" & vRst!Inscr_Sufr & "'") & _
                            ", Cod_Tipo_Cliente = " & IIf(IsNull(vRst!cod_tipo_c), "Null", "'" & vRst!cod_tipo_c & "'") & _
                            ", Dt_Cadastr = " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                            ", Cod_Transp = " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                            ", Cod_Banco = " & IIf(IsNull(vRst!Cod_Banco), "Null", vRst!Cod_Banco) & _
                            ", Cod_Mensagem = " & IIf(IsNull(vRst!Cod_Mensag), "Null", vRst!Cod_Mensag) & _
                            ", Fl_Cons_Final = " & IIf(IsNull(vRst!Fl_Cons_Fi), "Null", "'" & vRst!Fl_Cons_Fi & "'") & _
                            ", Caracteristica = " & IIf(IsNull(vRst!caracteris), "Null", "'" & vRst!caracteris & "'") & _
                            ", Cod_Mensagem_Fiscal = " & IIf(IsNull(vRst!Cod_Mens_F), "Null", vRst!Cod_Mens_F) & ", Tp_Empresa = " & IIf(IsNull(vRst!Tp_Empresa), "Null", "'" & vRst!Tp_Empresa & "'") & " where Cod_Cliente = " & vRst!cod_client, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Cliente values(" & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client) & _
                                ", " & IIf(IsNull(vRst!cgc), "Null", vRst!cgc) & _
                                ", " & IIf(IsNull(vRst!Nome_Clien), "Null", "'" & vRst!Nome_Clien & "'") & _
                                ", " & IIf(IsNull(vRst!Nome_Conta), "Null", "'" & vRst!Nome_Conta & "'") & _
                                ", " & IIf(IsNull(vRst!Classifica), "Null", "'" & vRst!Classifica & "'") & _
                                ", " & IIf(IsNull(Replace("" & vRst!endereco, "'", "")), "Null", "'" & Replace("" & vRst!endereco, "'", "") & "'") & _
                                ", " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                                ", " & IIf(IsNull(vRst!DDD1), "Null", vRst!DDD1) & _
                                ", " & IIf(IsNull(vRst!Fone1), "Null", vRst!Fone1) & _
                                ", " & IIf(IsNull(vRst!DDD2), "Null", vRst!DDD2) & _
                                ", " & IIf(IsNull(vRst!fone2), "Null", vRst!fone2) & _
                                ", " & IIf(IsNull(vRst!cep), "Null", vRst!cep) & _
                                ", " & IIf(IsNull(vRst!cxpostal), "Null", vRst!cxpostal) & _
                                ", " & IIf(IsNull(vRst!Telex), "Null", vRst!Telex) & _
                                ", " & IIf(IsNull(vRst!fax), "Null", vRst!fax) & _
                                ", " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & _
                                ", " & IIf(IsNull(vRst!Inscr_Sufr), "Null", "'" & vRst!Inscr_Sufr & "'") & _
                                ", " & IIf(IsNull(vRst!cod_tipo_c), "Null", "'" & vRst!cod_tipo_c & "'") & _
                                ", " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                                ", " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                                ", " & IIf(IsNull(vRst!Cod_Repr_V), "Null", vRst!Cod_Repr_V) & _
                                ", " & IIf(IsNull(vRst!Cod_Banco), "Null", vRst!Cod_Banco) & _
                                ", " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & _
                                ", " & IIf(IsNull(vRst!Cod_Mensag), "Null", vRst!Cod_Mensag) & ", " & IIf(IsNull(vRst!Fl_Cons_Fi), "Null", "'" & vRst!Fl_Cons_Fi & "'") & ", " & IIf(IsNull(vRst!caracteris), "Null", "'" & vRst!caracteris & "'") & ", " & IIf(IsNull(vRst!Cod_Mens_F), "Null", vRst!Cod_Mens_F) & ", " & IIf(IsNull(vRst!Tp_Empresa), "Null", "'" & vRst!Tp_Empresa & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIENTE", "INSERT", vRegsIn
    Atualizar_lsv "CLIENTE", "UPDATE", vRegsUp
    Atualizar_lsv "CLIENTE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CLIENTE = True
VerErro:
If Err.Number <> 0 Then
    ATU_CLIENTE = False
    Screen.MousePointer = 1
    MsgBox "Sub ATU_CLIENTE - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
End If
End Function

Public Function ATU_CLIENTE_ACUMULADO() As Boolean
     Dim DTFATU As String
     Dim DTINIDIA As String
     Dim CLIENTE As Long
     Dim MES1 As String
     Dim VL_1 As Double
     Dim MES2 As String
     Dim VL_2 As Double
     Dim MES3 As String
     Dim VL_3 As Double
     Dim MES4 As String
     Dim VL_4 As Double
    
    On Error GoTo VerErro

'    '-------------------------------------------------------------
'     'Alterado por Eduardo Relvas
'     'Alterado em 21/01/05
'     'Objetivo:
'     '    Atualizar o campo Valor1=0 quando o campo Valor1 for nulo
'     '    Atualizar o campo Valor2=0 quando o campo Valor2 for nulo
'     '    Atualizar o campo Valor3=0 quando o campo Valor3 for nulo
'     '    Atualizar o campo Valor4=0 quando o campo Valor4 for nulo
'     '-------------------------------------------------------------
          
'    strSQL = "UPDATE CLIENTE_ACUMULADO SET VALOR1 = 0 WHERE VALOR1 IS NULL"
'    dbLocal.Execute strSQL
'
'    strSQL = "UPDATE CLIENTE_ACUMULADO SET VALOR2 = 0 WHERE VALOR2 IS NULL"
'    dbLocal.Execute strSQL
'
'    strSQL = "UPDATE CLIENTE_ACUMULADO SET VALOR3 = 0 WHERE VALOR3 IS NULL"
'    dbLocal.Execute strSQL
'
'    strSQL = "UPDATE CLIENTE_ACUMULADO SET VALOR4 = 0 WHERE VALOR4 IS NULL"
'    dbLocal.Execute strSQL
'
'    strSQL = "SELECT dt_faturamento, dt_ini_fech_dia FROM DATAS"
'    Set ss = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
'
'    DTFATU = Format(ss!DT_FATURAMENTO, "dd/mm/yy")
'    DTINIDIA = Format(ss!DT_INI_FECH_DIA, "dd/mm/yy")
'
'    If Mid(DTFATU, 4, 2) <> Mid(DTINIDIA, 4, 2) Then
'       strSQL = "SELECT * FROM cliente_acumulado"
'       Set ss = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
'       Do While Not ss.EOF
'           CLIENTE = ss!cod_cliente
'           MES1 = Mid(DTFATU, 4, 2) & "-" & Mid(Format(DTFATU, "DD/MM/YYYY"), 7, 4)
'           VL_1 = 0
'           MES2 = ss!ANO_MES1
'           VL_2 = IIf(IsNull(ss!valor1), 0, ss!valor1)
'           MES3 = ss!ANO_MES2
'           VL_3 = IIf(IsNull(ss!valor2), 0, ss!valor2)
'           MES4 = ss!ANO_MES3
'           VL_4 = IIf(IsNull(ss!valor3), 0, ss!valor3)
'
'           strSQL = "UPDATE cliente_acumulado " & _
'                      "SET ANO_MES1 = '" & MES1 & "', VALOR1 = " & VL_1 & ", " & _
'                      "ANO_MES2 = '" & MES2 & "', VALOR2 = " & VL_2 & ", " & _
'                      "ANO_MES3 = '" & MES3 & "', VALOR3 = " & VL_3 & ", " & _
'                      "ANO_MES4 = '" & MES4 & "', VALOR4 = " & VL_4 & " " & _
'                      "WHERE COD_CLIENTE = " & CLIENTE
'
'           dbLocal.Execute strSQL
'           ss.MoveNext
'       Loop
'    End If

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIEACUM#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIENTE_ACUMULADO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Cliente_Acumulado set Ano_Mes1 = " & IIf(IsNull(vRst!Ano_Mes1), "Null", vRst!Ano_Mes1) & _
                            ", Valor1 = " & IIf(IsNull(vRst!Valor1), 0, vRst!Valor1) & _
                            ", Ano_Mes2 = " & IIf(IsNull(vRst!Ano_Mes2), "Null", "'" & vRst!Ano_Mes2 & "'") & _
                            ", Valor2 = " & IIf(IsNull(vRst!Valor2), 0, vRst!Valor2) & _
                            ", Ano_Mes3 = " & IIf(IsNull(vRst!Ano_Mes3), "Null", "'" & vRst!Ano_Mes3 & "'") & _
                            ", Valor3 = " & IIf(IsNull(vRst!Valor3), 0, vRst!Valor3) & _
                            ", Ano_Mes4 = " & IIf(IsNull(vRst!Ano_Mes4), "Null", "'" & vRst!Ano_Mes4 & "'") & _
                            ", valor4 = " & IIf(IsNull(vRst!Valor4), 0, vRst!Valor4) & " Where Cod_Cliente = " & vRst!cod_client, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Cliente_Acumulado values(" & IIf(IsNull(vRst!cod_client), 0, vRst!cod_client) & _
                                ", " & IIf(IsNull(vRst!Ano_Mes1), "null", vRst!Ano_Mes1) & _
                                ", " & IIf(IsNull(vRst!Valor1), 0, vRst!Valor1) & _
                                ", " & IIf(IsNull(vRst!Ano_Mes2), "Null", "'" & vRst!Ano_Mes2 & "'") & _
                                ", " & IIf(IsNull(vRst!Valor2), 0, vRst!Valor2) & _
                                ", " & IIf(IsNull(vRst!Ano_Mes3), "Null", "'" & vRst!Ano_Mes3 & "'") & _
                                ", " & IIf(IsNull(vRst!Valor3), 0, vRst!Valor3) & _
                                ", " & IIf(IsNull(vRst!Ano_Mes4), "null", "'" & vRst!Ano_Mes4 & "'") & _
                                ", " & IIf(IsNull(vRst!Valor4), 0, vRst!Valor4) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "CLIENTE_ACUMULADO", "INSERT", vRegsIn
    Atualizar_lsv "CLIENTE_ACUMULADO", "UPDATE", vRegsUp
    Atualizar_lsv "CLIENTE_ACUMULADO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CLIENTE_ACUMULADO = True
VerErro:
    If Err.Number <> 0 Then
        ATU_CLIENTE_ACUMULADO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIENTE_ACUMULADO - BC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_CLIENTE_CARACTERISTICA() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CLIE_CAR#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIENTE_CARACTERISTICA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Cliente_Caracteristica set Desc_Caracteristica = " & IIf(IsNull(vRst!Desc_Carac), "Null", "'" & vRst!Desc_Carac & "'") & _
                            ", Fl_Desc_Automatico = " & IIf(IsNull(vRst!Fl_Desc_Au), "Null", "'" & vRst!Fl_Desc_Au & "'") & _
                            ", Fl_Red_Comis = " & IIf(IsNull(vRst!Fl_Red_Com), "Null", "'" & vRst!Fl_Red_Com & "'") & " where Caracteristica = '" & vRst!caracteris & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Cliente_Caracteristica values(" & IIf(IsNull(vRst!caracteris), "Null", "'" & vRst!caracteris & "'") & _
                                ", " & IIf(IsNull(vRst!Desc_Carac), "Null", "'" & vRst!Desc_Carac & "'") & _
                                ", " & IIf(IsNull(vRst!Fl_Desc_Au), "Null", "'" & vRst!Fl_Desc_Au & "'") & _
                                ", " & IIf(IsNull(vRst!Fl_Red_Com), "Null", "'" & vRst!Fl_Red_Com & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CLIENTE_CARACTERISTICA", "INSERT", vRegsIn
    Atualizar_lsv "CLIENTE_CARACTERISTICA", "UPDATE", vRegsUp
    Atualizar_lsv "CLIENTE_CARACTERISTICA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CLIENTE_CARACTERISTICA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_CLIENTE_CARACTERISTICA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_CLIENTE_CARACTERISTICA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_DEPOSITO_VISAO() As Boolean
        
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEP_VISA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEPOSITO_VISAO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from Deposito_Visao", vRegsDel
        For i = 1 To vRegsRst
            DoEvents
            connBaseDpk.Execute "Insert into Deposito_Visao values(" & IIf(IsNull(vRst!Nome_Progr), "Null", "'" & vRst!Nome_Progr & "'") & ", " & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & ")"
            vRst.MoveNext
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        Next
    End If
    Atualizar_lsv "DEPOSITO_VISAO", "INSERT", vRegsIn
    Atualizar_lsv "DEPOSITO_VISAO", "UPDATE", vRegsUp
    Atualizar_lsv "DEPOSITO_VISAO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_DEPOSITO_VISAO = True

VerErro:
If Err.Number <> 0 Then
    ATU_DEPOSITO_VISAO = False
    Screen.MousePointer = 1
    MsgBox "Sub ATU_DEPOSITO_VISAO " & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
End If
End Function

Public Function ATU_DEPOSITO() As Boolean
        
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEPOSITO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEPOSITO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from Deposito", vRegsDel
        For i = 1 To vRegsRst
            DoEvents
            connBaseDpk.Execute "Insert into Deposito values(" & _
                                IIf(IsNull(vRst!Cod_loja), "Null", "'" & vRst!Cod_loja & "'") & _
                                ", " & IIf(IsNull(vRst!Cod_filial), "Null", vRst!Cod_filial) & _
                                ", " & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!Cod_Repres), "Null", "'" & vRst!Cod_Repres & "'") & _
                                ", " & IIf(IsNull(vRst!Fl_vdr), "Null", "'" & vRst!Fl_vdr & "'") & _
                                ")"
            vRst.MoveNext
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        Next
    End If
    Atualizar_lsv "DEPOSITO", "INSERT", vRegsIn
    Atualizar_lsv "DEPOSITO", "UPDATE", vRegsUp
    Atualizar_lsv "DEPOSITO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_DEPOSITO = True

VerErro:
If Err.Number <> 0 Then
    ATU_DEPOSITO = False
    Screen.MousePointer = 1
    MsgBox "Sub ATU_DEPOSITO " & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
End If
End Function

Public Function ATU_DOLARDIARIO() As Boolean
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DOLAR_DI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DOLAR_DIARIO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Dolar_Diario set Valor_Uss = " & IIf(IsNull(vRst!Valor_Uss), 0, vRst!Valor_Uss) & " where Data_Uss = #" & Format(CDate(vRst!Data_Uss), "MM/DD/YY") & "#", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Dolar_Diario values(" & IIf(IsNull(vRst!Data_Uss), "Null", "'" & vRst!Data_Uss & "'") & ", " & IIf(IsNull(vRst!Valor_Uss), 0, vRst!Valor_Uss) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DOLAR_DIARIO", "INSERT", vRegsIn
    Atualizar_lsv "DOLAR_DIARIO", "DELETE", vRegsDel
    Atualizar_lsv "DOLAR_DIARIO", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_DOLARDIARIO = True
VerErro:
If Err.Number <> 0 Then
    ATU_DOLARDIARIO = False
    Screen.MousePointer = 1
    MsgBox "Sub ATU_DOLARDIARIO - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
End If
End Function

Public Function ATU_DUPLICATA() As Boolean

    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DUPLICAT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DUPLICATAS", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from Duplicatas", vRegsDel
        For i = 1 To vRegsRst
            DoEvents
            connBaseDpk.Execute "Insert into Duplicatas values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!Num_Fatura), "Null", "'" & vRst!Num_Fatura & "'") & _
                                ", " & IIf(IsNull(vRst!Num_Ordem), "Null", "'" & vRst!Num_Ordem & "'") & _
                                ", " & IIf(IsNull(vRst!Situacao_P), 0, vRst!Situacao_P) & _
                                ", " & IIf(IsNull(vRst!Tp_Duplica), 0, vRst!Tp_Duplica) & _
                                ", " & IIf(IsNull(vRst!cod_client), 0, vRst!cod_client) & _
                                ", " & IIf(IsNull(vRst!Cod_Banco), 0, vRst!Cod_Banco) & _
                                ", " & IIf(IsNull(vRst!Tp_Pagamen), "Null", "'" & vRst!Tp_Pagamen & "'") & _
                                ", " & IIf(IsNull(vRst!Dt_Vencime), "Null", "'" & vRst!Dt_Vencime & "'") & _
                                ", " & IIf(IsNull(vRst!Vl_Aberto), 0, vRst!Vl_Aberto) & _
                                ", " & IIf(IsNull(vRst!Num_Dupl_B), 0, vRst!Num_Dupl_B) & ")"
        
            vRst.MoveNext
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        Next
    End If
    
    Atualizar_lsv "DUPLICATAS", "INSERT", vRegsIn
    Atualizar_lsv "DUPLICATAS", "DELETE", vRegsDel
    Atualizar_lsv "DUPLICATAS", "UPDATE", vRegsUp

    Set vRst = Nothing
    ATU_DUPLICATA = True

VerErro:
    If Err.Number = -2147217913 Then
        Resume Next
    ElseIf Err.Number = -2147217900 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        ATU_DUPLICATA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_DUPLICATA - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_EMBALADOR() As Boolean
     On Error GoTo VerErro

     Call zeraVariavel
     ProgressBar -1, 0
     
     Set vRst = New ADODB.Recordset
     vRst.Open "Select * from EMBALAD#DBF", connDBF, adOpenKeyset, adLockOptimistic
     vRegsRst = vRst.RecordCount
     Atualizar_lsv "EMBALADOR", "RECEBIDO", vRegsRst
     
     ProgressBar vRegsRst, -1
     
     For i = 1 To vRegsRst
     DoEvents
        connBaseDpk.Execute "Update Embalador set Nome_Embal = " & IIf(IsNull(vRst!Nome_Embal), "Null", "'" & vRst!Nome_Embal & "'") & " where Cod_Embal = " & vRst!Cod_Embal, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Embalador values(" & IIf(IsNull(vRst!Cod_Embal), "Null", vRst!Cod_Embal) & ", " & IIf(IsNull(vRst!Nome_Embal), "Null", "'" & vRst!Nome_Embal & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
     Next
    Atualizar_lsv "EMBALADOR", "UPDATE", vRegsUp
    Atualizar_lsv "EMBALADOR", "INSERT", vRegsIn
    Atualizar_lsv "EMBALADOR", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_EMBALADOR = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_EMBALADOR = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_EMBALADOR - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FILIAL() As Boolean
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FILIAL#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FILIAL", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Filial Set Sigla = " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                            ", Tp_Filial = " & IIf(IsNull(vRst!Tp_Filial), "Null", "'" & vRst!Tp_Filial & "'") & _
                            ", Cod_Franqueador = " & IIf(IsNull(vRst!Cod_Franqu), 0, vRst!Cod_Franqu) & _
                            ", Cod_Regional = " & IIf(IsNull(vRst!Cod_Region), 0, vRst!Cod_Region) & " where Cod_Filial = " & vRst!Cod_filial, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Filial values(" & IIf(IsNull(vRst!Cod_filial), 0, vRst!Cod_filial) & _
                                ", " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                                ", " & IIf(IsNull(vRst!Tp_Filial), "Null", "'" & vRst!Tp_Filial & "'") & _
                                ", " & IIf(IsNull(vRst!Cod_Franqu), 0, vRst!Cod_Franqu) & _
                                ", " & IIf(IsNull(vRst!Cod_Region), 0, vRst!Cod_Region) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FILIAL", "UPDATE", vRegsUp
    Atualizar_lsv "FILIAL", "INSERT", vRegsIn
    Atualizar_lsv "FILIAL", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FILIAL = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FILIAL = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FILIAL - FI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FILIAL_FRANQUIA() As Boolean
     On Error GoTo VerErro

     Call zeraVariavel
     ProgressBar -1, 0
     
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FIL_FRA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FILIAL_FRANQUIA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
     
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Filial_Franquia Set Seq_Batch = " & IIf(IsNull(vRst!Seq_Batch), "Null", vRst!Seq_Batch) & _
                            ", E_Mail = " & IIf(IsNull(vRst!E_mail), "Null", "'" & vRst!E_mail & "'") & " where Cod_Filial = " & vRst!Cod_filial, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Filial_Franquia values(" & IIf(IsNull(vRst!Cod_filial), "Null", vRst!Cod_filial) & _
                                ", " & IIf(IsNull(vRst!Seq_Batch), "Null", vRst!Seq_Batch) & _
                                ", " & IIf(IsNull(vRst!E_mail), "Null", "'" & vRst!E_mail & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FILIAL_FRANQUIA", "UPDATE", vRegsUp
    Atualizar_lsv "FILIAL_FRANQUIA", "INSERT", vRegsIn
    Atualizar_lsv "FILIAL_FRANQUIA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FILIAL_FRANQUIA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FILIAL_FRANQUIA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FILIAL_FRANQUIA - FI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FORNECEDOR() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FORNECED#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FORNECEDOR", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    If vRegsRst <> 0 Then
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "Update Fornecedor Set Sigla = " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                                ", Divisao = " & IIf(IsNull(vRst!divisao), "Null", "'" & vRst!divisao & "'") & _
                                ", Classificacao = " & IIf(IsNull(vRst!Classifica), "Null", "'" & vRst!Classifica & "'") & _
                                ", Situacao = " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & " where Cod_Fornecedor = " & vRst!cod_fornec, vRegs
            If vRegs = 0 Then
                connBaseDpk.Execute "Insert Into Fornecedor values(" & IIf(IsNull(vRst!cod_fornec), 0, vRst!cod_fornec) & _
                                    ", " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                                    ", " & IIf(IsNull(vRst!divisao), "Null", "'" & vRst!divisao & "'") & _
                                    ", " & IIf(IsNull(vRst!Classifica), "Null", "'" & vRst!Classifica & "'") & _
                                    ", " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & ")"
                vRegsIn = vRegsIn + 1
            Else
                vRegsUp = vRegsUp + 1
            End If
            vRst.MoveNext
            ProgressBar -1, 1
        Next
        Set vRst = Nothing
        
        Screen.MousePointer = 11
        
        'Deleta registro da tabela Item_Estoque
        frmFil040.SBPanel.Panels(1) = "Obtendo dados para atualizar registros de fornecedor...Este processo pode demorar alguns minutos..."
        Set vRst = New ADODB.Recordset
        vRst.Open "Select * from Fornecedor where divisao <> 'D' or Situacao <> 0 or Classificacao <> 'A'", connBaseDpk
        vRegsRst = vRst.RecordCount
        
        For i = 1 To vRegsRst
        DoEvents
            'frmFil040.SBPanel.Panels(1) = "Atualizando os registro " & i & "..."
            connBaseDpk.Execute "Delete * From Item_Estoque where cod_Fornecedor = " & vRst!Cod_Fornecedor
            vRst.MoveNext
        Next
        Set vRst = Nothing
        
        
        frmFil040.SBPanel.Panels(1) = "Obtendo dados para atualizar registros de fornecedor..."
        Set vRst = New ADODB.Recordset
        vRst.Open "Select Cod_dpk From Item_Cadastro, Fornecedor " & _
                    " Where Item_Cadastro.Cod_Fornecedor = Fornecedor.Cod_Fornecedor " & _
                    " and Divisao <> 'D' and Situacao <> 0 and Classificacao <> 'A'", connBaseDpk, adOpenKeyset, adLockOptimistic
        vRegsRst = vRst.RecordCount
        For i = 1 To vRegsRst
        DoEvents
            frmFil040.SBPanel.Panels(1) = "Atualizando os registro de " & i & " at� " & vRegsRst & "..."
            'Deleta registro da tabela Item_Analitico
            connBaseDpk.Execute "Delete * From Item_Analitico where Cod_Dpk = " & vRst!COD_DPK
            
            'Deleta registro da tabela Item_Custo
            connBaseDpk.Execute "Delete * From Item_Custo where Cod_Dpk = " & vRst!COD_DPK
            
            'Deleta registro da tabela Item_Global
            connBaseDpk.Execute "Delete * From Item_Global where Cod_Dpk = " & vRst!COD_DPK
            
            'Deleta registro da tabela Item_Preco
            connBaseDpk.Execute "Delete * From Item_Preco where Cod_Dpk = " & vRst!COD_DPK
            
            'Deleta registro da tabela Aplicacao
            connBaseDpk.Execute "Delete * From Aplicacao where Cod_Dpk = " & vRst!COD_DPK
            
            'Deleta registro da tabela Item_Cadastro
            connBaseDpk.Execute "Delete * From Item_Cadastro where Cod_Dpk = " & vRst!COD_DPK
            
            vRst.MoveNext
        Next
    End If
    Atualizar_lsv "FORNECEDOR", "UPDATE", vRegsUp
    Atualizar_lsv "FORNECEDOR", "DELETE", vRegsDel
    Atualizar_lsv "FORNECEDOR", "INSERT", vRegsIn
    
    Set vRst = Nothing
    ATU_FORNECEDOR = True
    Screen.MousePointer = 1

VerErro:
    If Err.Number <> 0 Then
        ATU_FORNECEDOR = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FORNECEDOR - AK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FORNECEDOR_ESPECIFICO() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
            
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FORN_ESP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FORNECEDOR_ESPECIFICO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Fornecedor_Especifico set Fl_Dif_Icms = " & IIf(IsNull(vRst!Fl_Dif_Icm), "Null", "'" & vRst!Fl_Dif_Icm & "'") & _
                            ", Fl_Adicional = " & IIf(IsNull(vRst!Fl_Adicion), "Null", "'" & vRst!Fl_Adicion & "'") & _
                            ", Tp_Dif_Icms = " & IIf(IsNull(vRst!Tp_Dif_Icm), "null", vRst!Tp_Dif_Icm) & " Where Cod_Fornecedor = " & vRst!cod_fornec & _
                            " and Cod_Grupo = " & vRst!cod_grupo & _
                            " and Cod_SubGrupo = " & vRst!cod_subgru & _
                            " and Cod_Dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Fornecedor_Especifico(" & _
                                "Cod_Fornecedor, " & _
                                "Fl_Dif_Icms, " & _
                                "Fl_Adicional, " & _
                                "Cod_Grupo, " & _
                                "Cod_SubGrupo, " & _
                                "Cod_Dpk, " & _
                                "Tp_Dif_Icms " & _
                                ")values(" & _
                                IIf(IsNull(vRst!cod_fornec), "Null", vRst!cod_fornec) & ", " & _
                                IIf(IsNull(vRst!Fl_Dif_Icm), "Null", "'" & vRst!Fl_Dif_Icm & "'") & ", " & _
                                IIf(IsNull(vRst!Fl_Adicion), "Null", "'" & vRst!Fl_Adicion & "'") & ", " & _
                                IIf(IsNull(vRst!cod_grupo), "Null", vRst!cod_grupo) & ", " & _
                                IIf(IsNull(vRst!cod_subgru), "Null", vRst!cod_subgru) & ", " & _
                                IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & ", " & IIf(IsNull(vRst!Tp_Dif_Icm), "Null", vRst!Tp_Dif_Icm) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FORNECEDOR_ESPECIFICO", "UPDATE", vRegsUp
    Atualizar_lsv "FORNECEDOR_ESPECIFICO", "INSERT", vRegsIn
    Atualizar_lsv "FORNECEDOR_ESPECIFICO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FORNECEDOR_ESPECIFICO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FORNECEDOR_ESPECIFICO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FORNECEDOR_ESPECIFICO - HD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FRETE_ENTREGA() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FRETE_EN#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FRETE_ENTREGA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Frete_Entrega set Vl_Frete_Entrega = " & IIf(IsNull(vRst!Vl_Frete_E), "Null", vRst!Vl_Frete_E) & _
                            ", Situacao = " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & _
                            " Where Cod_Loja = " & vRst!Cod_loja & _
                            " and Cod_Transp = " & vRst!cod_transp & _
                            " and Cod_Uf = '" & vRst!Cod_uf & _
                            "' and Cod_Regiao = " & vRst!Cod_Regiao & _
                            " and Cod_SubRegiao = " & vRst!Cod_SubReg & _
                            " and Cod_Cidade = " & vRst!cod_cidade, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Frete_Entrega values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                                ", " & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!Cod_Regiao), "Null", vRst!Cod_Regiao) & _
                                ", " & IIf(IsNull(vRst!Cod_SubReg), "Null", vRst!Cod_SubReg) & _
                                ", " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!Tp_Frete), "Null", "'" & vRst!Tp_Frete & "'") & _
                                ", " & IIf(IsNull(vRst!Vl_Frete_E), "Null", vRst!Vl_Frete_E) & _
                                ", " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FRETE_ENTREGA", "UPDATE", vRegsUp
    Atualizar_lsv "FRETE_ENTREGA", "INSERT", vRegsIn
    Atualizar_lsv "FRETE_ENTREGA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FRETE_ENTREGA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FRETE_ENTREGA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FRETE_ENTREGA - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FRETE_UF() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FRETE_UF#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FRETE_UF", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Frete_Uf set Vl_Frete = " & IIf(IsNull(vRst!Vl_Frete), "Null", vRst!Vl_Frete) & _
                            " Where Cod_Loja = " & vRst!Cod_loja & _
                            " and Cod_Uf = '" & vRst!Cod_uf & _
                            "' and Cod_Regiao = " & vRst!Cod_Regiao & _
                            " and Cod_SubRegiao = " & vRst!Cod_SubReg & _
                            " and Cod_Cidade = " & vRst!cod_cidade, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Frete_Uf values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!Cod_Regiao), "Null", vRst!Cod_Regiao) & _
                                ", " & IIf(IsNull(vRst!Cod_SubReg), "Null", vRst!Cod_SubReg) & _
                                ", " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!Vl_Frete), "Null", vRst!Vl_Frete) & ")"
            
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FRETE_UF", "UPDATE", vRegsUp
    Atualizar_lsv "FRETE_UF", "INSERT", vRegsIn
    Atualizar_lsv "FRETE_UF", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FRETE_UF = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FRETE_UF = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FRETE_UF - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FRETE_UF_BLOQ() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FRETEUFB#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FRETE_UF_BLOQ", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Frete_Uf_Bloq set Fl_Bloqueio = " & IIf(IsNull(vRst!Fl_Bloquei), "Null", "'" & vRst!Fl_Bloquei & "'") & " Where Cod_Uf = '" & vRst!Cod_uf & _
                            "' and Cod_Transp = " & vRst!cod_transp, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Frete_Uf_Bloq values(" & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                                ", " & IIf(IsNull(vRst!Fl_Bloquei), "Null", "'" & vRst!Fl_Bloquei & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FRETE_UF_BLOQ", "UPDATE", vRegsUp
    Atualizar_lsv "FRETE_UF_BLOQ", "INSERT", vRegsIn
    Atualizar_lsv "FRETE_UF_BLOQ", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_FRETE_UF_BLOQ = True

VerErro:
    If Err.Number <> 0 Then
        ATU_FRETE_UF_BLOQ = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FRETE_UF_BLOQ - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_FRETE_UF_TRANSP() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from FRETEUFT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FRETE_UF_TRANSP", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Frete_Uf_Transp set Vl_Frete = " & IIf(IsNull(vRst!Vl_Frete), "Null", vRst!Vl_Frete) & _
                            " Where Cod_Loja = " & vRst!Cod_loja & _
                            " and Cod_Transp = " & vRst!cod_transp & _
                            " and Cod_Uf = '" & vRst!Cod_uf & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Frete_Uf_Transp values(" & IIf(IsNull(vRst!Cod_loja), "Null", "'" & vRst!Cod_loja & "'") & _
                                ", " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                                ", " & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!Vl_Frete), "Null", vRst!Vl_Frete) & ")"
                                
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "FRETE_UF_TRANSP", "UPDATE", vRegsUp
    Atualizar_lsv "FRETE_UF_TRANSP", "DELETE", vRegsDel
    Atualizar_lsv "FRETE_UF_TRANSP", "INSERT", vRegsIn
    
    Set vRst = Nothing
    ATU_FRETE_UF_TRANSP = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_FRETE_UF_TRANSP = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_FRETE_UF_TRANSP - ME" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_GRUPO() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from GRUPO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "GRUPO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Grupo set Desc_Grupo = " & IIf(IsNull(vRst!Desc_Grupo), "Null", "'" & vRst!Desc_Grupo & "'") & " Where Cod_Grupo = " & vRst!cod_grupo, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Grupo values(" & IIf(IsNull(vRst!cod_grupo), 0, vRst!cod_grupo) & ", " & IIf(IsNull(vRst!Desc_Grupo), "Null", "'" & vRst!Desc_Grupo & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "GRUPO", "UPDATE", vRegsUp
    Atualizar_lsv "GRUPO", "INSERT", vRegsIn
    Atualizar_lsv "GRUPO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_GRUPO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_GRUPO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_GRUPO - AL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITPEDNOTA_VENDA() As Boolean
    
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITPEDNOT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITPEDNOTA_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        If vRst!num_pedido <> "" Then
            connBaseDpk.Execute "Update ItPedNota_Venda set Num_Nota = " & IIf(IsNull(vRst!Num_Nota), "Null", vRst!Num_Nota) & _
                                ", Cod_Loja_Nota = " & IIf(IsNull(vRst!Cod_Loja_N), "Null", vRst!Cod_Loja_N) & _
                                ", Cod_Dpk = " & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & _
                                ", Qtd_Solicitada = " & IIf(IsNull(vRst!Qtd_Solici), "Null", vRst!Qtd_Solici) & _
                                ", Qtd_Atendida = " & IIf(IsNull(vRst!Qtd_Atendi), "Null", vRst!Qtd_Atendi) & _
                                ", Preco_Unitario = " & IIf(IsNull(vRst!Preco_Unit), "Null", vRst!Preco_Unit) & _
                                ", Tabela_Venda = " & IIf(IsNull(vRst!tabela_ven), "Null", "'" & vRst!tabela_ven & "'") & _
                                ", Pc_Desc1 = " & IIf(IsNull(vRst!pc_desc1), "Null", vRst!pc_desc1) & _
                                ", Pc_Desc2 = " & IIf(IsNull(vRst!pc_desc2), "Null", vRst!pc_desc2) & _
                                ", Pc_Desc3 = " & IIf(IsNull(vRst!pc_desc3), "Null", vRst!pc_desc3) & _
                                ", Pc_Dificm = " & IIf(IsNull(vRst!pc_dificm), "Null", vRst!pc_dificm) & _
                                ", Pc_Ipi = " & IIf(IsNull(vRst!Pc_Ipi), "Null", vRst!Pc_Ipi) & _
                                ", Pc_Comiss = " & IIf(IsNull(vRst!Pc_Comiss), "Null", vRst!Pc_Comiss) & _
                                ", Pc_ComissTlmk = " & IIf(IsNull(vRst!Pc_Comisst), "Null", vRst!Pc_Comisst) & _
                                ", Cod_Trib = " & IIf(IsNull(vRst!Cod_Trib), "Null", vRst!Cod_Trib) & _
                                ", Cod_TriBipi = " & IIf(IsNull(vRst!Cod_Tribip), "Null", vRst!Cod_Tribip) & _
                                ", Situacao = " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & " where Cod_Loja = " & vRst!Cod_loja & _
                                " and Num_Pedido = " & vRst!num_pedido & _
                                " and Seq_Pedido = " & vRst!seq_pedido & _
                                " and Num_Item_Pedido = " & vRst!num_item_p, vRegs
            If vRegs = 0 Then
                connBaseDpk.Execute "Insert into ItPedNota_Venda values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                    ", " & IIf(IsNull(vRst!num_pedido), "Null", vRst!num_pedido) & _
                                    ", " & IIf(IsNull(vRst!seq_pedido), "Null", vRst!seq_pedido) & _
                                    ", " & IIf(IsNull(vRst!num_item_p), "Null", vRst!num_item_p) & _
                                    ", " & IIf(IsNull(vRst!Cod_Loja_N), "Null", vRst!Cod_Loja_N) & _
                                    ", " & IIf(IsNull(vRst!Num_Nota), "Null", vRst!Num_Nota) & _
                                    ", " & IIf(IsNull(vRst!Num_Item_N), "Null", vRst!Num_Item_N) & _
                                    ", " & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & _
                                    ", " & IIf(IsNull(vRst!Qtd_Solici), "Null", vRst!Qtd_Solici) & _
                                    ", " & IIf(IsNull(vRst!Qtd_Atendi), "Null", vRst!Qtd_Atendi) & _
                                    ", " & IIf(IsNull(vRst!Preco_Unit), "Null", vRst!Preco_Unit) & _
                                    ", " & IIf(IsNull(vRst!tabela_ven), "Null", "'" & vRst!tabela_ven & "'") & _
                                    ", " & IIf(IsNull(vRst!pc_desc1), "Null", vRst!pc_desc1) & _
                                    ", " & IIf(IsNull(vRst!pc_desc2), "Null", vRst!pc_desc2) & _
                                    ", " & IIf(IsNull(vRst!pc_desc3), "Null", vRst!pc_desc3) & _
                                    ", " & IIf(IsNull(vRst!pc_dificm), "Null", vRst!pc_dificm) & _
                                    ", " & IIf(IsNull(vRst!Pc_Ipi), "Null", vRst!Pc_Ipi) & _
                                    ", " & IIf(IsNull(vRst!Pc_Comiss), "Null", vRst!Pc_Comiss) & _
                                    ", " & IIf(IsNull(vRst!Pc_Comisst), "Null", vRst!Pc_Comisst) & _
                                    ", " & IIf(IsNull(vRst!Cod_Trib), "Null", vRst!Cod_Trib) & _
                                    ", " & IIf(IsNull(vRst!Cod_Tribip), "Null", vRst!Cod_Tribip) & _
                                    ", " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & ")"
                vRegsIn = vRegsIn + 1
            Else
                vRegsUp = vRegsUp + 1
            End If
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITPEDNOTA_VENDA", "UPDATE", vRegsUp
    Atualizar_lsv "ITPEDNOTA_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "ITPEDNOTA_VENDA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITPEDNOTA_VENDA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITPEDNOTA_VENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITPEDNOTA_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMANAL() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_ANA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_ANALITICO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    'vRegsRst = 0
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Item_Analitico Set Categoria = " & IIf(IsNull(vRst!categoria), "Null", vRst!categoria) & _
                            ", Class_Abc = " & IIf(IsNull(vRst!Class_Abc), "Null", "'" & vRst!Class_Abc & "'") & _
                            ", Class_Abcf = " & IIf(IsNull(vRst!Class_Abcf), "Null", "'" & vRst!Class_Abcf & "'") & _
                            ", Class_Venda = " & IIf(IsNull(vRst!Class_Vend), "Null", vRst!Class_Vend) & _
                            ", Qtd_Mes_Ant = " & IIf(IsNull(vRst!Qtd_Mes_An), "Null", vRst!Qtd_Mes_An) & _
                            ", Custo_Medio = " & IIf(IsNull(vRst!Custo_Medi), "Null", vRst!Custo_Medi) & _
                            ", Custo_Medio_Ant = " & IIf(IsNull(vRst!Custo_Med_), "Null", vRst!Custo_Med_) & " Where Cod_Loja = " & vRst!Cod_loja & " and Cod_Dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Item_Analitico values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!categoria), "Null", vRst!categoria) & _
                                ", " & IIf(IsNull(vRst!Class_Abc), "Null", "'" & vRst!Class_Abc & "'") & _
                                ", " & IIf(IsNull(vRst!Class_Abcf), "Null", "'" & vRst!Class_Abcf & "'") & _
                                ", " & IIf(IsNull(vRst!Class_Vend), "Null", vRst!Class_Vend) & _
                                ", " & IIf(IsNull(vRst!Qtd_Mes_An), "Null", vRst!Qtd_Mes_An) & _
                                ", " & IIf(IsNull(vRst!Custo_Medi), "Null", vRst!Custo_Medi) & _
                                ", " & vRst!Custo_Med_ & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_ANALITICO", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_ANALITICO", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_ANALITICO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMANAL = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMANAL = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMANAL - DK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMCAD() As Boolean
Dim vRegsCount As Long
Dim vrstCount As ADODB.Recordset
On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_CAD#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_CADASTRO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Item_Cadastro Set Cod_Fornecedor = " & IIf(IsNull(vRst!cod_fornec), 0, vRst!cod_fornec) & _
                            ", Cod_Fabrica = " & IIf(IsNull(vRst!Cod_fabric), "Null", "'" & vRst!Cod_fabric & "'") & _
                            ", Desc_Item = " & IIf(IsNull(Replace("" & vRst!Desc_Item, "'", "")), "Null", "'" & Replace("" & vRst!Desc_Item, "'", "") & "'") & _
                            ", Cod_Linha = " & IIf(IsNull(vRst!Cod_Linha), 0, vRst!Cod_Linha) & _
                            ", Cod_Grupo = " & IIf(IsNull(vRst!cod_grupo), 0, vRst!cod_grupo) & _
                            ", Cod_SubGrupo = " & IIf(IsNull(vRst!cod_subgru), 0, vRst!cod_subgru) & _
                            ", Dt_Cadastramento = " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                            ", Mascarado = " & IIf(IsNull(vRst!Mascarado), "Null", "'" & vRst!Mascarado & "'") & _
                            ", Class_Fiscal = " & IIf(IsNull(vRst!Class_Fisc), 0, vRst!Class_Fisc) & _
                            ", Peso = " & IIf(IsNull(vRst!Peso), 0, vRst!Peso) & _
                            ", Volume = " & IIf(IsNull(vRst!Volume), 0, vRst!Volume) & _
                            ", Cod_Unidade = " & IIf(IsNull(vRst!Cod_Unidad), "Null", "'" & vRst!Cod_Unidad & "'") & _
                            ", Cod_Tributacao = " & IIf(IsNull(vRst!cod_tribut), 0, vRst!cod_tribut) & _
                            ", Cod_Tributacao_Ipi = " & IIf(IsNull(vRst!Cod_Trib_i), 0, vRst!Cod_Trib_i) & _
                            ", Pc_Ipi = " & IIf(IsNull(vRst!Pc_Ipi), 0, vRst!Pc_Ipi) & _
                            ", Cod_Procedencia = " & IIf(IsNull(vRst!Cod_Proced), 0, vRst!Cod_Proced) & _
                            ", Cod_Dpk_Ant = " & IIf(IsNull(vRst!Cod_Dpk_An), 0, vRst!Cod_Dpk_An) & _
                            ", Qtd_Minforn = " & IIf(IsNull(vRst!Qtd_MinFor), 0, vRst!Qtd_MinFor) & _
                            ", Qtd_Minvda = " & IIf(IsNull(vRst!Qtd_Minvda), 0, vRst!Qtd_Minvda) & " Where Cod_Dpk = " & vRst!COD_DPK, vRegs
        
        If vRegs = 0 Then
            Set vrstCount = New ADODB.Recordset
            vrstCount.Open "SELECT count(*) as Qtd FROM ITEM_CADASTRO where Cod_Fornecedor = " & vRst!cod_fornec & " And Cod_Fabrica = '" & vRst!Cod_fabric & "'", connBaseDpk
            vRegsCount = vrstCount!qtd
            If vRegsCount = 0 Then
                connBaseDpk.Execute "Insert Into Item_Cadastro values(" & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & _
                                    ", " & IIf(IsNull(vRst!cod_fornec), 0, vRst!cod_fornec) & _
                                    ", " & IIf(IsNull(vRst!Cod_fabric), "Null", "'" & vRst!Cod_fabric & "'") & _
                                    ", " & IIf(IsNull(Replace("" & vRst!Desc_Item, "'", "")), "Null", "'" & Replace("" & vRst!Desc_Item, "'", "") & "'") & _
                                    ", " & IIf(IsNull(vRst!Cod_Linha), 0, vRst!Cod_Linha) & _
                                    ", " & IIf(IsNull(vRst!cod_grupo), 0, vRst!cod_grupo) & _
                                    ", " & IIf(IsNull(vRst!cod_subgru), 0, vRst!cod_subgru) & _
                                    ", " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                                    ", " & IIf(IsNull(vRst!Mascarado), "Null", "'" & vRst!Mascarado & "'") & _
                                    ", " & IIf(IsNull(vRst!Class_Fisc), 0, vRst!Class_Fisc) & _
                                    ", " & IIf(IsNull(vRst!Peso), 0, vRst!Peso) & _
                                    ", " & IIf(IsNull(vRst!Volume), 0, vRst!Volume) & _
                                    ", " & IIf(IsNull(vRst!Cod_Unidad), "Null", "'" & vRst!Cod_Unidad & "'") & _
                                    ", " & IIf(IsNull(vRst!cod_tribut), 0, vRst!cod_tribut) & _
                                    ", " & IIf(IsNull(vRst!Cod_Trib_i), 0, vRst!Cod_Trib_i) & _
                                    ", " & IIf(IsNull(vRst!Pc_Ipi), 0, vRst!Pc_Ipi) & _
                                    ", " & IIf(IsNull(vRst!Cod_Proced), 0, vRst!Cod_Proced) & _
                                    ", " & IIf(IsNull(vRst!Cod_Dpk_An), 0, vRst!Cod_Dpk_An) & _
                                    ", " & IIf(IsNull(vRst!Qtd_MinFor), 0, vRst!Qtd_MinFor) & _
                                    ", " & IIf(IsNull(vRst!Qtd_Minvda), 0, vRst!Qtd_Minvda) & ")"
            Else
                GoTo Erro_chave
            End If
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        
Erro_chave:
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_CADASTRO", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_CADASTRO", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_CADASTRO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMCAD = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMCAD = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMCAD - AM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMCUSTO() As Boolean
    On Error GoTo VerErro
     
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_CUS#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_CUSTO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Item_Custo set Custo_Reposicao = " & IIf(IsNull(vRst!Custo_Repo), 0, vRst!Custo_Repo) & " Where Cod_Loja = " & vRst!Cod_loja & " and Cod_Dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Item_Custo values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & ", " & IIf(IsNull(vRst!Custo_Repo), 0, vRst!Custo_Repo) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_CUSTO", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_CUSTO", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_CUSTO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMCUSTO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMCUSTO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMCUSTO - AY" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMESTOQUE() As Boolean
     On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_EST#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_ESTOQUE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
        DoEvents
        vSQL = "Update Item_Estoque set Qtd_Atual = " & IIf(IsNull(vRst!Qtd_Atual), 0, vRst!Qtd_Atual)
        vSQL = vSQL & ", Qtd_Reserv = " & IIf(IsNull(vRst!Qtd_Reserv), 0, vRst!Qtd_Reserv)
        vSQL = vSQL & ", Qtd_Pendente = " & IIf(IsNull(vRst!Qtd_Penden), 0, vRst!Qtd_Penden)
        vSQL = vSQL & ", Qtd_Maxvda = " & IIf(IsNull(vRst!Qtd_Maxvda), 0, vRst!Qtd_Maxvda)
        vSQL = vSQL & ", Cuema = " & IIf(IsNull(vRst!cuema), 0, vRst!cuema)
        vSQL = vSQL & ", Dt_Cuema = " & IIf(IsNull(vRst!dt_cuema), "Null", "'" & vRst!dt_cuema & "'")
        vSQL = vSQL & ", Vl_Ult_Compra = " & IIf(IsNull(vRst!Vl_Ult_Com), 0, vRst!Vl_Ult_Com)
        vSQL = vSQL & ", Dt_Ult_Compra = " & IIf(IsNull(vRst!dt_ult_com), "Null", "'" & vRst!dt_ult_com & "'")
        vSQL = vSQL & ", Situacao = " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & " where Cod_Loja = " & vRst!Cod_loja & " and Cod_Dpk = " & vRst!COD_DPK
        connBaseDpk.Execute vSQL, vRegs

        If vRegs = 0 Then
            vSQL = " Insert Into Item_Estoque( cod_loja, cod_dpk, qtd_atual, qtd_reserv, qtd_pendente,"
            vSQL = vSQL & " qtd_maxvda, cuema, "
            vSQL = vSQL & "dt_cuema, "
            vSQL = vSQL & " vl_ult_compra, "
            vSQL = vSQL & " dt_ult_compra, "
            vSQL = vSQL & " situacao)values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Atual), 0, vRst!Qtd_Atual)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Reserv), 0, vRst!Qtd_Reserv)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Penden), 0, vRst!Qtd_Penden)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Maxvda), 0, vRst!Qtd_Maxvda)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!cuema), 0, vRst!cuema)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!dt_cuema), "Null", "'" & vRst!dt_cuema & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Ult_Com), 0, vRst!Vl_Ult_Com)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!dt_ult_com), "Null", "'" & vRst!dt_ult_com & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & ")"
            connBaseDpk.Execute vSQL
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_ESTOQUE", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_ESTOQUE", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_ESTOQUE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMESTOQUE = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMESTOQUE = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMESTOQUE - AZ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMGLOBAL() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_GLO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_GLOBAL", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Item_Global set Sequencia_Tab = " & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & " where Cod_Dpk = " & vRst!COD_DPK, vRegs
        
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Item_Global values(" & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & ", " & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_GLOBAL", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_GLOBAL", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_GLOBAL", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMGLOBAL = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMGLOBAL = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMGLOBAL - DL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ITEMPRECO() As Boolean

    On Error GoTo VerErro
     
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ITEM_PRE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ITEM_PRECO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Item_preco set Preco_venda = " & IIf(IsNull(vRst!Preco_Vend), 0, vRst!Preco_Vend) & _
                            ", Preco_venda_ant = " & IIf(IsNull(vRst!Preco_v_an), 0, vRst!Preco_v_an) & _
                            ", Preco_Of = " & IIf(IsNull(vRst!Preco_of), 0, vRst!Preco_of) & _
                            ", Preco_of_Ant = " & IIf(IsNull(vRst!Preco_o_an), 0, vRst!Preco_o_an) & _
                            ", Preco_Sp = " & IIf(IsNull(vRst!Preco_sp), 0, vRst!Preco_sp) & _
                            ", Preco_Sp_ant = " & IIf(IsNull(vRst!Preco_S_An), 0, vRst!Preco_S_An) & " where Cod_loja = " & vRst!Cod_loja & " and Cod_Dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Item_preco values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!Preco_Vend), 0, vRst!Preco_Vend) & _
                                ", " & IIf(IsNull(vRst!Preco_v_an), 0, vRst!Preco_v_an) & _
                                ", " & IIf(IsNull(vRst!Preco_of), 0, vRst!Preco_of) & _
                                ", " & IIf(IsNull(vRst!Preco_o_an), 0, vRst!Preco_o_an) & _
                                ", " & IIf(IsNull(vRst!Preco_sp), 0, vRst!Preco_sp) & _
                                ", " & IIf(IsNull(vRst!Preco_S_An), 0, vRst!Preco_S_An) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ITEM_PRECO", "UPDATE", vRegsUp
    Atualizar_lsv "ITEM_PRECO", "INSERT", vRegsIn
    Atualizar_lsv "ITEM_PRECO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ITEMPRECO = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_ITEMPRECO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ITEMPRECO - BA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_LOJA() As Boolean
Dim vRstCont As ADODB.Recordset
Dim vRstCont1 As String
Dim Y As Integer
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from LOJA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "LOJA", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from loja", vRegsDel
        For Y = 1 To vRegsRst
            DoEvents
            Set vRstCont = New ADODB.Recordset
            vRstCont.Open "Select Count(*) as qtd From Loja where cod_loja = " & vRst!Cod_loja, connBaseDpk
            vRegsCont1 = vRstCont!qtd
            If vRegsCont1 = 0 Then
                connBaseDpk.Execute "Insert into Loja values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                    ", " & IIf(IsNull(vRst!cgc), "Null", vRst!cgc) & _
                                    ", " & IIf(IsNull(vRst!Razao_Soci), "Null", "'" & vRst!Razao_Soci & "'") & _
                                    ", " & IIf(IsNull(vRst!Nome_Fanta), "Null", "'" & vRst!Nome_Fanta & "'") & _
                                    ", " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                                    ", " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                                    ", " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                                    ", " & IIf(IsNull(vRst!cep), "Null", vRst!cep) & _
                                    ", " & IIf(IsNull(vRst!ddd), "Null", vRst!ddd) & _
                                    ", " & IIf(IsNull(vRst!fone), "Null", vRst!fone) & _
                                    ", " & IIf(IsNull(vRst!fax), "Null", vRst!fax) & _
                                    ", " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                                    ", " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & _
                                    ", " & IIf(IsNull(vRst!Repart_Fis), "Null", "'" & vRst!Repart_Fis & "'") & ")"
                vRegsIn = vRegsIn + 1
                ProgressBar -1, 1
            End If
            vRst.MoveNext
        Next
    End If

    Atualizar_lsv "LOJA", "INSERT", vRegsIn
    Atualizar_lsv "LOJA", "DELETE", vRegsDel
    Atualizar_lsv "LOJA", "UPDATE", vRegsUp
    Set vRst = Nothing
    ATU_LOJA = True

VerErro:

    If Err.Number <> 0 Then
        ATU_LOJA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_LOJA - CB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_MONTADORA() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from MONTADOR#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "MONTADORA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Montadora set Desc_Montadora = " & IIf(IsNull(vRst!Desc_Monta), "Null", "'" & vRst!Desc_Monta & "'") & " Where Cod_Montadora = " & vRst!cod_montad, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Montadora values(" & IIf(IsNull(vRst!cod_montad), 0, vRst!cod_montad) & ", " & IIf(IsNull(vRst!Desc_Monta), "Null", "'" & vRst!Desc_Monta & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "MONTADORA", "UPDATE", vRegsUp
    Atualizar_lsv "MONTADORA", "INSERT", vRegsIn
    Atualizar_lsv "MONTADORA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_MONTADORA = True

VerErro:

    If Err.Number <> 0 Then
        ATU_MONTADORA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_MONTADORA - CC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_NAT_OPERACAO() As Boolean
     On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from NATU_OPE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "NATUREZA_OPERACAO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    vSQL = ""

    For i = 1 To vRegsRst
    DoEvents
        vSQL = "Update Natureza_Operacao set Desc_Natureza = " & IIf(IsNull(vRst!Desc_Natur), "Null", "'" & vRst!Desc_Natur & "'")
        vSQL = vSQL & ", Desc_Abreviada = " & IIf(IsNull(vRst!Desc_Abrev), "Null", "'" & vRst!Desc_Abrev & "'")
        vSQL = vSQL & ", Cod_Cfo_Dest1 = " & IIf(IsNull(vRst!Cod_dest1), "Null", vRst!Cod_dest1)
        vSQL = vSQL & ", Cod_Cfo_Dest2 = " & IIf(IsNull(vRst!Cod_dest2), "Null", vRst!Cod_dest2)
        vSQL = vSQL & ", Cod_Cfo_Dest3 = " & IIf(IsNull(vRst!Cod_dest3), "Null", vRst!Cod_dest3)
        vSQL = vSQL & ", Cod_Cfo_Oper = " & IIf(IsNull(vRst!Cod_Cfo_Op), "Null", vRst!Cod_Cfo_Op)
        vSQL = vSQL & ", Cod_Transac = " & IIf(IsNull(vRst!Cod_Transa), "Null", vRst!Cod_Transa)
        vSQL = vSQL & ", Cod_Trib_Icms1 = " & IIf(IsNull(vRst!Cod_Icms1), "null", vRst!Cod_Icms1)
        vSQL = vSQL & ", Cod_Trib_Icms2 = " & IIf(IsNull(vRst!Cod_Icms2), "null", vRst!Cod_Icms2)
        vSQL = vSQL & ", Cod_Trib_Icms3 = " & IIf(IsNull(vRst!Cod_Icms3), "null", vRst!Cod_Icms3)
        vSQL = vSQL & ", Cod_Trib_Icms4 = " & IIf(IsNull(vRst!Cod_Icms4), "null", vRst!Cod_Icms4)
        vSQL = vSQL & ", Cod_Trib_Icms5 = " & IIf(IsNull(vRst!Cod_Icms5), "null", vRst!Cod_Icms5)
        vSQL = vSQL & ", Cod_Trib_Icms6 = " & IIf(IsNull(vRst!Cod_Icms6), "null", vRst!Cod_Icms6)
        vSQL = vSQL & ", Cod_Trib_Ipi1 = " & IIf(IsNull(vRst!Cod_Ipi1), "null", vRst!Cod_Ipi1)
        vSQL = vSQL & ", Cod_Trib_Ipi2 = " & IIf(IsNull(vRst!Cod_Ipi2), "null", vRst!Cod_Ipi2)
        vSQL = vSQL & ", Cod_Trib_Ipi3 = " & IIf(IsNull(vRst!Cod_Ipi3), "null", vRst!Cod_Ipi3)
        vSQL = vSQL & ", Cod_Trib_Ipi4 = " & IIf(IsNull(vRst!Cod_Ipi4), "null", vRst!Cod_Ipi4)
        vSQL = vSQL & ", Cod_Mensagem1 = " & IIf(IsNull(vRst!Cod_Mens1), "null", vRst!Cod_Mens1)
        vSQL = vSQL & ", Cod_Mensagem2 = " & IIf(IsNull(vRst!Cod_Mens2), "null", vRst!Cod_Mens2)
        vSQL = vSQL & ", Fl_Livro = " & IIf(IsNull(vRst!Fl_Livro), "Null", "'" & vRst!Fl_Livro & "'")
        vSQL = vSQL & ", Fl_Cuema = " & IIf(IsNull(vRst!Fl_Cuema), "Null", "'" & vRst!Fl_Cuema & "'")
        vSQL = vSQL & ", Fl_Estoque = " & IIf(IsNull(vRst!Fl_Estoque), "Null", "'" & vRst!Fl_Estoque & "'")
        vSQL = vSQL & ", Fl_Estorno = " & IIf(IsNull(vRst!Fl_Estorno), "Null", "'" & vRst!Fl_Estorno & "'")
        vSQL = vSQL & ", Fl_Tributacao = " & IIf(IsNull(vRst!Fl_Tributa), "Null", "'" & vRst!Fl_Tributa & "'")
        vSQL = vSQL & ", Fl_Fornecedor = " & IIf(IsNull(vRst!Fl_Fornece), "Null", "'" & vRst!Fl_Fornece & "'")
        vSQL = vSQL & ", Fl_VlContabil = " & IIf(IsNull(vRst!Fl_VlConta), "Null", "'" & vRst!Fl_VlConta & "'")
        vSQL = vSQL & ", Fl_NfItem = " & IIf(IsNull(vRst!Fl_NfItem), "Null", "'" & vRst!Fl_NfItem & "'")
        vSQL = vSQL & ", Fl_Quantidade = " & IIf(IsNull(vRst!Fl_Quantid), "Null", "'" & vRst!Fl_Quantid & "'")
        vSQL = vSQL & ", Fl_Cod_Merc = " & IIf(IsNull(vRst!Fl_Cod_Mer), "Null", "'" & vRst!Fl_Cod_Mer & "'")
        vSQL = vSQL & ", Fl_PcDesc = " & IIf(IsNull(vRst!Fl_PcDesc), "Null", "'" & vRst!Fl_PcDesc & "'")
        vSQL = vSQL & ", Fl_Contas_Pagar = " & IIf(IsNull(vRst!Fl_Contas_), "Null", "'" & vRst!Fl_Contas_ & "'")
        vSQL = vSQL & ", Fl_Tipo_Nota = " & IIf(IsNull(vRst!Fl_Tipo_No), "Null", vRst!Fl_Tipo_No)
        vSQL = vSQL & ", Fl_Centro_Custo = " & IIf(IsNull(vRst!Fl_Centro_), "Null", "'" & vRst!Fl_Centro_ & "'")
        vSQL = vSQL & ", Fl_Contabil = " & IIf(IsNull(vRst!Fl_Contabi), "Null", "'" & vRst!Fl_Contabi & "'")
        vSQL = vSQL & ", Fl_Base_Red_Icms = " & IIf(IsNull(vRst!Fl_Base_Re), "Null", "'" & vRst!Fl_Base_Re & "'")
        vSQL = vSQL & ", Fl_Consistencia = " & IIf(IsNull(vRst!Fl_Consist), "Null", "'" & vRst!Fl_Consist & "'")
        vSQL = vSQL & ", Fl_Ipi_Incid_Icm = " & IIf(IsNull(vRst!Fl_Ipi_Inc), "Null", "'" & vRst!Fl_Ipi_Inc & "'")
        vSQL = vSQL & ", Fl_Movimentacao = " & IIf(IsNull(vRst!Fl_Movimen), "Null", "'" & vRst!Fl_Movimen & "'")
        vSQL = vSQL & " Where Cod_Natureza = '" & vRst!Cod_Nature & "'"
        connBaseDpk.Execute vSQL, vRegs
        
        
        
        If vRegs = 0 Then
            vSQL = "Insert Into Natureza_Operacao values(" & IIf(IsNull(vRst!Cod_Nature), "Null", "'" & vRst!Cod_Nature & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Desc_Natur), "Null", "'" & vRst!Desc_Natur & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Desc_Abrev), "Null", "'" & vRst!Desc_Abrev & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_dest1), "Null", vRst!Cod_dest1)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_dest2), "Null", vRst!Cod_dest2)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_dest3), "Null", vRst!Cod_dest3)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Cfo_Op), "Null", vRst!Cod_Cfo_Op)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Transa), "Null", vRst!Cod_Transa)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms1), "Null", vRst!Cod_Icms1)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms2), "Null", vRst!Cod_Icms2)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms3), "Null", vRst!Cod_Icms3)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms4), "Null", vRst!Cod_Icms4)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms5), "Null", vRst!Cod_Icms5)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Icms6), "Null", vRst!Cod_Icms6)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Ipi1), "Null", vRst!Cod_Ipi1)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Ipi2), "Null", vRst!Cod_Ipi2)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Ipi3), "Null", vRst!Cod_Ipi3)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Ipi4), "Null", vRst!Cod_Ipi4)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Mens1), "Null", vRst!Cod_Mens1)
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!Cod_Mens2), "Null", vRst!Cod_Mens2)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Livro), "Null", "'" & vRst!Fl_Livro & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Cuema), "Null", "'" & vRst!Fl_Cuema & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Estoque), "Null", "'" & vRst!Fl_Estoque & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Estorno), "Null", "'" & vRst!Fl_Estorno & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Tributa), "Null", "'" & vRst!Fl_Tributa & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Fornece), "Null", "'" & vRst!Fl_Fornece & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_VlConta), "Null", "'" & vRst!Fl_VlConta & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_NfItem), "Null", "'" & vRst!Fl_NfItem & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Quantid), "Null", "'" & vRst!Fl_Quantid & "'")
            vSQL = vSQL & ", " & IIf(IsNull(Fl_Cod_Mer), "Null", "'" & Fl_Cod_Mer & "'")
            vSQL = vSQL & ", " & IIf(IsNull(Fl_PcDesc), "Null", "'" & Fl_PcDesc & "'")
            vSQL = vSQL & ", " & IIf(IsNull(Fl_Contas_), "Null", "'" & Fl_Contas_ & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Tipo_No), "Null", vRst!Fl_Tipo_No)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Centro_), "Null", "'" & vRst!Fl_Centro_ & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Contabi), "Null", "'" & vRst!Fl_Contabi & "'")
            vSQL = vSQL & ", " & IIf(IsNull(Fl_Base_Re), "Null", "'" & Fl_Base_Re & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Consist), "Null", "'" & vRst!Fl_Consist & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Ipi_Inc), "Null", "'" & vRst!Fl_Ipi_Inc & "'")
            vSQL = vSQL & ", " & IIf(IsNull(Fl_Movimen), "Null", "'" & Fl_Movimen & "'") & ")"
            connBaseDpk.Execute vSQL
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "NATUREZA_OPERACAO", "UPDATE", vRegsUp
    Atualizar_lsv "NATUREZA_OPERACAO", "INSERT", vRegsIn
    Atualizar_lsv "NATUREZA_OPERACAO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_NAT_OPERACAO = True

VerErro:

    If Err.Number <> 0 Then
        ATU_NAT_OPERACAO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_NAT_OPERACAO - CC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_PEDNOTA_VENDA() As Boolean
    On Error GoTo VerErro
    

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from PEDNOTA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "PEDNOTA_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        vSQL = " Update PedNota_venda set Cod_loja_nota = " & IIf(IsNull(vRst!Cod_Loja_N), "Null", vRst!Cod_Loja_N)
        vSQL = vSQL & ", Num_Nota = " & IIf(IsNull(vRst!Num_Nota), "Null", vRst!Num_Nota)
        vSQL = vSQL & ", Num_Pendente = " & IIf(IsNull(vRst!Num_Penden), "Null", vRst!Num_Penden)
        vSQL = vSQL & ", Cod_Filial = " & IIf(IsNull(vRst!Cod_filial), "Null", vRst!Cod_filial)
        vSQL = vSQL & ", Tp_Pedido = " & IIf(IsNull(vRst!Tp_Pedido), "Null", vRst!Tp_Pedido)
        vSQL = vSQL & ", Tp_DpkBlau = " & IIf(IsNull(vRst!Tp_DpkBlau), "Null", vRst!Tp_DpkBlau)
        vSQL = vSQL & ", Tp_Transacao = " & IIf(IsNull(vRst!Tp_Transac), "Null", vRst!Tp_Transac)
        vSQL = vSQL & ", Dt_Digitacao = " & IIf(IsNull(vRst!dt_Digitac), "Null", "'" & vRst!dt_Digitac & "'")
        vSQL = vSQL & ", Dt_Pedido = " & IIf(IsNull(vRst!Dt_Pedido), "Null", "'" & vRst!Dt_Pedido & "'")
        vSQL = vSQL & ", Dt_Emissao_Nota = " & IIf(IsNull(vRst!Dt_Emissao), "Null", "'" & vRst!Dt_Emissao & "'")
        vSQL = vSQL & ", Dt_Ssm = " & IIf(IsNull(vRst!Dt_Ssm), "Null", "'" & vRst!Dt_Ssm & "'")
        vSQL = vSQL & ", Dt_Bloq_Politica = " & IIf(IsNull(vRst!Dt_Politic), "Null", "'" & vRst!Dt_Politic & "'")
        vSQL = vSQL & ", Dt_Bloq_Credito = " & IIf(IsNull(vRst!Dt_Credito), "Null", "'" & vRst!Dt_Credito & "'")
        vSQL = vSQL & ", Dt_Bloq_Frete = " & IIf(IsNull(vRst!Dt_Frete), "Null", "'" & vRst!Dt_Frete & "'")
        vSQL = vSQL & ", Cod_Nope = " & IIf(IsNull(vRst!Cod_Nope), "Null", "'" & vRst!Cod_Nope & "'")
        vSQL = vSQL & ", Cod_Cliente = " & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client)
        vSQL = vSQL & ", Cod_Fornecedor = " & IIf(IsNull(vRst!cod_fornec), "Null", vRst!cod_fornec)
        vSQL = vSQL & ", Cod_End_Entrega = " & IIf(IsNull(vRst!Cod_Entreg), "Null", vRst!Cod_Entreg)
        vSQL = vSQL & ", Cod_End_Cobranca = " & IIf(IsNull(vRst!Cod_Cobran), "Null", vRst!Cod_Cobran)
        vSQL = vSQL & ", Cod_Transp = " & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp)
        vSQL = vSQL & ", Cod_Repres = " & IIf(IsNull(vRst!Cod_Repres), "Null", vRst!Cod_Repres)
        vSQL = vSQL & ", Cod_Vend = " & IIf(IsNull(vRst!Cod_vend), "Null", vRst!Cod_vend)
        vSQL = vSQL & ", Cod_Plano = " & IIf(IsNull(vRst!Cod_Plano), "Null", vRst!Cod_Plano)
        vSQL = vSQL & ", Cod_Banco = " & IIf(IsNull(vRst!Cod_Banco), "Null", vRst!Cod_Banco)
        vSQL = vSQL & ", Cod_Cfo_Dest = " & IIf(IsNull(vRst!Cod_Dest), "Null", vRst!Cod_Dest)
        vSQL = vSQL & ", Cod_Cfo_Oper = " & IIf(IsNull(vRst!Cod_Oper), "Null", vRst!Cod_Oper)
        vSQL = vSQL & ", Frete_Pago = " & IIf(IsNull(vRst!Frete_Pago), "Null", "'" & vRst!Frete_Pago & "'")
        vSQL = vSQL & ", Peso_Bruto = " & IIf(IsNull(vRst!Peso_Bruto), "Null", vRst!Peso_Bruto)
        vSQL = vSQL & ", Qtd_Ssm = " & IIf(IsNull(vRst!Qtd_Ssm), "Null", vRst!Qtd_Ssm)
        vSQL = vSQL & ", Qtd_Item_Pedido = " & IIf(IsNull(vRst!Qtd_Pedido), "Null", vRst!Qtd_Pedido)
        vSQL = vSQL & ", Qtd_item_nota = " & IIf(IsNull(vRst!Qtd_Nota), "Null", vRst!Qtd_Nota)
        vSQL = vSQL & ", Vl_Contabil = " & IIf(IsNull(vRst!Vl_Contabi), "Null", vRst!Vl_Contabi)
        vSQL = vSQL & ", Vl_Ipi = " & IIf(IsNull(vRst!Vl_Ipi), "Null", vRst!Vl_Ipi)
        vSQL = vSQL & ", Vl_BaseIcm1 = " & IIf(IsNull(vRst!Vl_Icm1), "Null", vRst!Vl_Icm1)
        vSQL = vSQL & ", Vl_BaseIcm2 = " & IIf(IsNull(vRst!Vl_Icm2), "Null", vRst!Vl_Icm2)
        vSQL = vSQL & ", Vl_Base_1 = " & IIf(IsNull(vRst!Vl_Base_1), "Null", vRst!Vl_Base_1)
        vSQL = vSQL & ", Vl_Base_2 = " & IIf(IsNull(vRst!Vl_Base_2), "Null", vRst!Vl_Base_2)
        vSQL = vSQL & ", Vl_Base_3 = " & IIf(IsNull(vRst!Vl_Base_3), "null", vRst!Vl_Base_3)
        vSQL = vSQL & ", Vl_BaseIsen = " & IIf(IsNull(vRst!Vl_BaseIse), "null", vRst!Vl_BaseIse)
        vSQL = vSQL & ", Vl_Base_5 = " & IIf(IsNull(vRst!Vl_Base_5), "null", vRst!Vl_Base_5)
        vSQL = vSQL & ", Vl_Base_6 = " & IIf(IsNull(vRst!Vl_Base_6), "null", vRst!Vl_Base_6)
        vSQL = vSQL & ", Vl_BaseOutr = " & IIf(IsNull(vRst!Vl_BaseOut), "null", vRst!Vl_BaseOut)
        vSQL = vSQL & ", Vl_BaseMaj = " & IIf(IsNull(vRst!Vl_BaseMaj), "null", vRst!Vl_BaseMaj)
        vSQL = vSQL & ", Vl_IcmRetido = " & IIf(IsNull(vRst!Vl_IcmReti), "null", vRst!Vl_IcmReti)
        vSQL = vSQL & ", Vl_BaseIpi = " & IIf(IsNull(vRst!Vl_BaseIpi), "null", vRst!Vl_BaseIpi)
        vSQL = vSQL & ", Vl_BIsenIpi = " & IIf(IsNull(vRst!Vl_BIsenIp), "null", vRst!Vl_BIsenIp)
        vSQL = vSQL & ", Vl_BOutrIpi = " & IIf(IsNull(vRst!Vl_BOutrIp), "null", vRst!Vl_BOutrIp)
        vSQL = vSQL & ", Vl_Frete = " & IIf(IsNull(vRst!Vl_Frete), "null", vRst!Vl_Frete)
        vSQL = vSQL & ", Vl_Desp_Acess = " & IIf(IsNull(vRst!Vl_Desp_ac), "null", vRst!Vl_Desp_ac)
        vSQL = vSQL & ", Pc_Desconto = " & IIf(IsNull(vRst!Pc_Descont), "null", vRst!Pc_Descont)
        vSQL = vSQL & ", Pc_Desc_Suframa = " & IIf(IsNull(vRst!Pc_Desc_Su), "null", vRst!Pc_Desc_Su)
        vSQL = vSQL & ", Pc_Acrescimo = " & IIf(IsNull(vRst!Pc_Acresci), "null", vRst!Pc_Acresci)
        vSQL = vSQL & ", Pc_Seguro = " & IIf(IsNull(vRst!Pc_Seguro), "null", vRst!Pc_Seguro)
        vSQL = vSQL & ", Pc_Icm1 = " & IIf(IsNull(vRst!Pc_Icm1), "null", vRst!Pc_Icm1)
        vSQL = vSQL & ", Pc_Icm2 = " & IIf(IsNull(vRst!Pc_Icm2), "null", vRst!Pc_Icm2)
        vSQL = vSQL & ", Pc_Aliq_Interna = " & IIf(IsNull(vRst!Pc_Aliq_In), "null", vRst!Pc_Aliq_In)
        vSQL = vSQL & ", Cod_Cancel = " & IIf(IsNull(vRst!Cod_Cancel), "Null", vRst!Cod_Cancel)
        vSQL = vSQL & ", Fl_Ger_Ssm = " & IIf(IsNull(vRst!Fl_Ssm), "null", "'" & vRst!Fl_Ssm & "'")
        vSQL = vSQL & ", Fl_Ger_Nfis = " & IIf(IsNull(vRst!Fl_Nfis), "null", "'" & vRst!Fl_Nfis & "'")
        vSQL = vSQL & ", Fl_Pendencia = " & IIf(IsNull(vRst!Fl_Pendenc), "null", "'" & vRst!Fl_Pendenc & "'")
        vSQL = vSQL & ", Fl_Desp_Acess = " & IIf(IsNull(vRst!Fl_Desp_Ac), "null", "'" & vRst!Fl_Desp_Ac & "'")
        vSQL = vSQL & ", Fl_Dif_Icm = " & IIf(IsNull(vRst!Fl_Dif_Icm), "null", "'" & vRst!Fl_Dif_Icm & "'")
        vSQL = vSQL & ", Situacao = " & IIf(IsNull(vRst!situacao), "null", vRst!situacao)
        vSQL = vSQL & ", Bloq_Protesto = " & IIf(IsNull(vRst!Bloq_Prote), "null", "'" & vRst!Bloq_Prote & "'")
        vSQL = vSQL & ", Bloq_Juros = " & IIf(IsNull(vRst!Bloq_Juros), "null", "'" & vRst!Bloq_Juros & "'")
        vSQL = vSQL & ", Bloq_Cheque = " & IIf(IsNull(vRst!Bloq_Chequ), "null", "'" & vRst!Bloq_Chequ & "'")
        vSQL = vSQL & ", Bloq_Compra = " & IIf(IsNull(vRst!Bloq_Compr), "null", "'" & vRst!Bloq_Compr & "'")
        vSQL = vSQL & ", Bloq_Duplicata = " & IIf(IsNull(vRst!Bloq_Dupli), "null", "'" & vRst!Bloq_Dupli & "'")
        vSQL = vSQL & ", Bloq_Limite = " & IIf(IsNull(vRst!Bloq_Limit), "null", "'" & vRst!Bloq_Limit & "'")
        vSQL = vSQL & ", Bloq_Saldo = " & IIf(IsNull(vRst!Bloq_Saldo), "null", "'" & vRst!Bloq_Saldo & "'")
        vSQL = vSQL & ", Bloq_Clie_Novo = " & IIf(IsNull(vRst!Bloq_Clie_), "null", "'" & vRst!Bloq_Clie_ & "'")
        vSQL = vSQL & ", Bloq_Desconto = " & IIf(IsNull(vRst!Bloq_Desco), "null", "'" & vRst!Bloq_Desco & "'")
        vSQL = vSQL & ", Bloq_Acrescimo = " & IIf(IsNull(vRst!Bloq_Acres), "null", "'" & vRst!Bloq_Acres & "'")
        vSQL = vSQL & ", Bloq_VlFatMin = " & IIf(IsNull(vRst!Bloq_VlFat), "null", "'" & vRst!Bloq_VlFat & "'")
        vSQL = vSQL & ", Bloq_Item_Desc1 = " & IIf(IsNull(vRst!Bloq_Desc1), "null", "'" & vRst!Bloq_Desc1 & "'")
        vSQL = vSQL & ", Bloq_Item_Desc2 = " & IIf(IsNull(vRst!Bloq_Desc2), "null", "'" & vRst!Bloq_Desc2 & "'")
        vSQL = vSQL & ", Bloq_Item_Desc3 = " & IIf(IsNull(vRst!Bloq_Desc3), "null", "'" & vRst!Bloq_Desc3 & "'")
        vSQL = vSQL & ", Bloq_Frete = " & IIf(IsNull(vRst!Bloq_Frete), "null", "'" & vRst!Bloq_Frete & "'")
        vSQL = vSQL & ", Mens_Pedido = " & IIf(IsNull(Mens_Pedid), "null", "'" & vRst!Mens_Pedid & "'")
        vSQL = vSQL & ", Mens_Nota = " & IIf(IsNull(vRst!Mens_Nota), "null", "'" & vRst!Mens_Nota & "'")
        vSQL = vSQL & ", Cod_Vdr = " & IIf(IsNull(vRst!cod_vdr), "null", vRst!cod_vdr)
        vSQL = vSQL & " Where Cod_loja = " & vRst!Cod_loja & " and Num_Pedido = " & vRst!num_pedido & " and Seq_Pedido = " & vRst!seq_pedido
        connBaseDpk.Execute vSQL, vRegs
        If vRegs = 0 Then
            vSQL = "Insert into PedNota_venda values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!num_pedido), "null", vRst!num_pedido)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!seq_pedido), "null", vRst!seq_pedido)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Loja_N), "null", vRst!Cod_Loja_N)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Num_Nota), "null", vRst!Num_Nota)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Num_Penden), "Null", vRst!Num_Penden)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_filial), "null", vRst!Cod_filial)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Tp_Pedido), "null", vRst!Tp_Pedido)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Tp_DpkBlau), "null", vRst!Tp_DpkBlau)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Tp_Transac), "null", vRst!Tp_Transac)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!dt_Digitac), "Null", "'" & vRst!dt_Digitac & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Pedido), "Null", "'" & vRst!Dt_Pedido & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Emissao), "Null", "'" & vRst!Dt_Emissao & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Ssm), "Null", "'" & vRst!Dt_Ssm & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Politic), "Null", "'" & vRst!Dt_Politic & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Credito), "Null", "'" & vRst!Dt_Credito & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Dt_Frete), "Null", "'" & vRst!Dt_Frete & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Nope), "null", "'" & vRst!Cod_Nope & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!cod_client), "null", vRst!cod_client)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!cod_fornec), "Null", vRst!cod_fornec)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Entreg), "null", vRst!Cod_Entreg)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Cobran), "Null", vRst!Cod_Cobran)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!cod_transp), "null", vRst!cod_transp)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Repres), "Null", vRst!Cod_Repres)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_vend), "null", vRst!Cod_vend)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Plano), "null", vRst!Cod_Plano)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Banco), "null", vRst!Cod_Banco)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Dest), "null", vRst!Cod_Dest)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Oper), "null", vRst!Cod_Oper)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Frete_Pago), "null", "'" & vRst!Frete_Pago & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Peso_Bruto), "null", vRst!Peso_Bruto)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Ssm), "null", vRst!Qtd_Ssm)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Pedido), "null", vRst!Qtd_Pedido)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Qtd_Nota), "null", vRst!Qtd_Nota)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Contabi), "null", vRst!Vl_Contabi)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Ipi), "null", vRst!Vl_Ipi)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Icm1), "null", vRst!Vl_Icm1)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Icm2), "null", vRst!Vl_Icm2)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Base_1), "null", vRst!Vl_Base_1)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Base_2), "null", vRst!Vl_Base_2)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Base_3), "null", vRst!Vl_Base_3)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BaseIse), "null", vRst!Vl_BaseIse)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Base_5), "null", vRst!Vl_Base_5)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Base_6), "null", vRst!Vl_Base_6)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BaseOut), "null", vRst!Vl_BaseOut)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BaseMaj), "null", vRst!Vl_BaseMaj)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_IcmReti), "null", vRst!Vl_IcmReti)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BaseIpi), "null", vRst!Vl_BaseIpi)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BIsenIp), "null", vRst!Vl_BIsenIp)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_BOutrIp), "null", vRst!Vl_BOutrIp)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Frete), "null", vRst!Vl_Frete)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Vl_Desp_ac), "null", vRst!Vl_Desp_ac)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Descont), "null", vRst!Pc_Descont)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Desc_Su), "null", vRst!Pc_Desc_Su)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Acresci), "null", vRst!Pc_Acresci)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Seguro), "null", vRst!Pc_Seguro)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Icm1), "null", vRst!Pc_Icm1)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Icm2), "null", vRst!Pc_Icm2)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Pc_Aliq_In), "null", vRst!Pc_Aliq_In)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Cod_Cancel), "Null", vRst!Cod_Cancel)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Ssm), "null", "'" & vRst!Fl_Ssm & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Nfis), "null", "'" & vRst!Fl_Nfis & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Pendenc), "null", "'" & vRst!Fl_Pendenc & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Desp_Ac), "null", "'" & vRst!Fl_Desp_Ac & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Fl_Dif_Icm), "null", "'" & vRst!Fl_Dif_Icm & "'")
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!situacao), "null", vRst!situacao)
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Prote), "null", "'" & vRst!Bloq_Prote & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Juros), "null", "'" & vRst!Bloq_Juros & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Chequ), "null", "'" & vRst!Bloq_Chequ & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Compr), "null", "'" & vRst!Bloq_Compr & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Dupli), "null", "'" & vRst!Bloq_Dupli & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Limit), "null", "'" & vRst!Bloq_Limit & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Saldo), "null", "'" & vRst!Bloq_Saldo & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Clie_), "null", "'" & vRst!Bloq_Clie_ & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Desco), "null", "'" & vRst!Bloq_Desco & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Acres), "null", "'" & vRst!Bloq_Acres & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_VlFat), "null", "'" & vRst!Bloq_VlFat & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Desc1), "null", "'" & vRst!Bloq_Desc1 & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Desc2), "null", "'" & vRst!Bloq_Desc2 & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Desc3), "null", "'" & vRst!Bloq_Desc3 & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Bloq_Frete), "null", "'" & vRst!Bloq_Frete & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Mens_Pedid), "null", "'" & vRst!Mens_Pedid & "'")
            vSQL = vSQL & ", " & IIf(IsNull(vRst!Mens_Nota), "null", "'" & vRst!Mens_Nota & "'")
            vSQL = vSQL & ",  " & IIf(IsNull(vRst!cod_vdr), "null", vRst!cod_vdr) & ")"
            connBaseDpk.Execute vSQL
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "PEDNOTA_VENDA", "UPDATE", vRegsUp
    Atualizar_lsv "PEDNOTA_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "PEDNOTA_VENDA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_PEDNOTA_VENDA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_PEDNOTA_VENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_PEDNOTA_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_PLANO() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from PLANO_PG#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "PLANO_PGTO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Plano_Pgto set Desc_Plano = " & IIf(IsNull(vRst!Desc_Plano), "Null", "'" & vRst!Desc_Plano & "'") & _
                            ", Prazo_Medio = " & IIf(IsNull(vRst!Prazo_Medi), "Null", vRst!Prazo_Medi) & _
                            ", Pc_Acres_Fin_Blau = " & IIf(IsNull(vRst!Pc_A_Bla), "Null", vRst!Pc_A_Bla) & _
                            ", Pc_Acres_Fin_Dpk = " & IIf(IsNull(vRst!Pc_Acres_F), "Null", vRst!Pc_Acres_F) & _
                            ", Pc_Desc_Fin_Dpk = " & IIf(IsNull(vRst!Pc_Desc_Fi), "Null", vRst!Pc_Desc_Fi) & _
                            ", Situacao = " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & _
                            ", Pc_Desc_Fin_Vdr = " & IIf(IsNull(vRst!Pc_D_Vdr), "Null", vRst!Pc_D_Vdr) & _
                            ", Fl_AVista = " & IIf(IsNull(vRst!Fl_AVista), "Null", "'" & vRst!Fl_AVista & "'") & " where Cod_Plano = " & vRst!Cod_Plano, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into Plano_Pgto(Cod_Plano, " & _
                                "Desc_Plano, " & _
                                "Prazo_Medio,  " & _
                                "Pc_Acres_Fin_Blau,  " & _
                                "Pc_Acres_Fin_Dpk,  " & _
                                "Pc_Desc_Fin_Dpk,  " & _
                                "Situacao, " & _
                                "Pc_Desc_Fin_Vdr,  " & _
                                "Fl_AVista ) values (" & IIf(IsNull(vRst!Cod_Plano), "Null", vRst!Cod_Plano) & _
                                "," & IIf(IsNull(vRst!Desc_Plano), "Null", "'" & vRst!Desc_Plano & "'") & _
                                ", " & IIf(IsNull(vRst!Prazo_Medi), "Null", vRst!Prazo_Medi) & _
                                ",  " & IIf(IsNull(vRst!Pc_A_Bla), "Null", vRst!Pc_A_Bla) & _
                                ", " & IIf(IsNull(vRst!Pc_Acres_F), "Null", vRst!Pc_Acres_F) & _
                                ", " & IIf(IsNull(vRst!Pc_Desc_Fi), "Null", vRst!Pc_Desc_Fi) & _
                                ", " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & _
                                ", " & IIf(IsNull(vRst!Pc_D_Vdr), "Null", vRst!Pc_D_Vdr) & _
                                ", " & IIf(IsNull(vRst!Fl_AVista), "Null", "'" & vRst!Fl_AVista & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "PLANO_PGTO", "UPDATE", vRegsUp
    Atualizar_lsv "PLANO_PGTO", "INSERT", vRegsIn
    Atualizar_lsv "PLANO_PGTO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_PLANO = True
VerErro:
    If Err.Number <> 0 Then
        ATU_PLANO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_PLANO - BM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_CLIE_REPRES() As Boolean
Dim vrstCount As ADODB.Recordset
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_CLIE_R#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_CLIE_REPRES", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
    Set vrstCount = New ADODB.Recordset
    vrstCount.Open "Select Count(*) as Qtd from R_Clie_Repres where cod_cliente = " & vRst!cod_client & " and cod_repres = " & vRst!Cod_Repres, connBaseDpk
    If vrstCount!qtd = 0 Then
        connBaseDpk.Execute "Insert Into R_Clie_Repres values(" & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client) & ", " & IIf(IsNull(vRst!Cod_Repres), "Null", vRst!Cod_Repres) & ")"
        vRegsIn = vRegsIn + 1
        ProgressBar -1, 1
    End If
    vRst.MoveNext
    Next
    Atualizar_lsv "R_CLIE_REPRES", "INSERT", vRegsIn
    Atualizar_lsv "R_CLIE_REPRES", "DELETE", vRegsDel
    Atualizar_lsv "R_CLIE_REPRES", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_R_CLIE_REPRES = True

VerErro:
    If Err.Number <> 0 Then
        ATU_R_CLIE_REPRES = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_R_CLIE_REPRES - JM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_DPK_EQUIV() As Boolean
Dim vRstCont As ADODB.Recordset
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_DPK_EQ#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_DPK_EQUIV", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        'Set vRstCont = New ADODB.Recordset
        'vRstCont.Open "Select Count(*) from R_Dpk_Equiv where cod_dpk = " & XXX & " and cod_dpk_eq = " & XXX, connBaseDpk
        
        connBaseDpk.Execute "Update R_DPK_EQUIV Set Cod_Dpk_Eq = " & IIf(IsNull(vRst!cod_dpk_eq), "Null", vRst!cod_dpk_eq) & " where Cod_Dpk = " & vRst!COD_DPK & "and cod_dpk_eq = " & vRst!cod_dpk_eq, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert Into R_DPK_EQUIV values(" & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & ", " & IIf(IsNull(vRst!cod_dpk_eq), "Null", vRst!cod_dpk_eq) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "R_DPK_EQUIV", "INSERT", vRegsIn
    Atualizar_lsv "R_DPK_EQUIV", "UPDATE", vRegsUp
    Atualizar_lsv "R_DPK_EQUIV", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_R_DPK_EQUIV = True

VerErro:
    If Err.Number <> 0 Then
        ATU_R_DPK_EQUIV = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_R_DPK_EQUIV - FK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_FILDEP() As Boolean
Dim vrstCount As ADODB.Recordset
     On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_FILDEP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_FILDEP", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        Set vrstCount = New ADODB.Recordset
        vrstCount.Open "Select Count(*) as Qtd From R_FilDep where cod_loja = " & vRst!Cod_loja & " and cod_filial = " & vRst!Cod_filial, connBaseDpk
        
        If vrstCount!qtd = 0 Then
            connBaseDpk.Execute "Insert Into R_FilDep values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & ", " & IIf(IsNull(vRst!Cod_filial), "Null", vRst!Cod_filial) & ")"
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        End If
        vRst.MoveNext
    Next
    Atualizar_lsv "R_FILDEP", "INSERT", vRegsIn
    Atualizar_lsv "R_FILDEP", "DELETE", vRegsDel
    Atualizar_lsv "R_FILDEP", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_R_FILDEP = True
    
VerErro:
    If Err.Number <> 0 Then
        If Err.Number = -2147467259 Then
            Resume Next
        End If
        ATU_R_FILDEP = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_R_FILDEP - ES" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_PEDIDO_CONF() As Boolean
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_PED_CO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_PEDIDO_CONF", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update R_Pedido_Conf set Cod_conferente = " & IIf(IsNull(vRst!cod_confer), "Null", vRst!cod_confer) & _
                            ", Cod_embalador = " & IIf(IsNull(vRst!Cod_embala), "Null", vRst!Cod_embala) & _
                            ", Cod_expedidor = " & IIf(IsNull(vRst!Cod_Expedi), "Null", vRst!Cod_Expedi) & _
                            ", Fl_pend_etiq = " & IIf(IsNull(vRst!fl_pend_et), "Null", "'" & vRst!fl_pend_et & "'") & _
                            ", Qtd_volume_parc = " & IIf(IsNull(vRst!qtd_Volume), "Null", vRst!qtd_Volume) & _
                            " where cod_loja = " & vRst!Cod_loja & _
                            " and num_pedido = " & vRst!num_pedido & _
                            " and seq_pedido = " & vRst!seq_pedido & _
                            " and num_caixa = " & vRst!num_caixa, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into R_Pedido_Conf VALUES(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!num_pedido), "Null", vRst!num_pedido) & _
                                ", " & IIf(IsNull(vRst!seq_pedido), "Null", vRst!seq_pedido) & _
                                ", " & IIf(IsNull(vRst!num_caixa), "Null", vRst!num_caixa) & _
                                ", " & IIf(IsNull(vRst!cod_confer), "Null", vRst!cod_confer) & _
                                ", " & IIf(IsNull(vRst!Cod_embala), "Null", vRst!Cod_embala) & _
                                ", " & IIf(IsNull(vRst!Cod_Expedi), "Null", vRst!Cod_Expedi) & _
                                ", " & IIf(IsNull(vRst!fl_pend_et), "Null", "'" & vRst!fl_pend_et & "'") & _
                                ", " & IIf(IsNull(vRst!qtd_Volume), "Null", vRst!qtd_Volume) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "R_PEDIDO_CONF", "INSERT", vRegsIn
    Atualizar_lsv "R_PEDIDO_CONF", "UPDATE", vRegsUp
    Atualizar_lsv "R_PEDIDO_CONF", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_R_PEDIDO_CONF = True

VerErro:

    If Err.Number <> 0 Then
        ATU_R_PEDIDO_CONF = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_R_PEDIDO_CONF - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_REPCGC() As Boolean
Dim vrstCount As ADODB.Recordset

     On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_REPCGC#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_REPCGC", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        Set vrstCount = New ADODB.Recordset
        vrstCount.Open "Select Count(*) as Qtd from R_repcgc where cod_repres = " & vRst!Cod_Repres & " and cgc = " & vRst!cgc, connBaseDpk
        If vrstCount!qtd = 0 Then
            connBaseDpk.Execute "Insert into R_repcgc values(" & IIf(IsNull(vRst!Cod_Repres), 0, vRst!Cod_Repres) & "," & IIf(IsNull(vRst!cgc), 0, vRst!cgc) & ")"
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        End If
        vRst.MoveNext
    Next
    Atualizar_lsv "R_REPCGC", "INSERT", vRegsIn
    Atualizar_lsv "R_REPCGC", "DELETE", vRegsDel
    Atualizar_lsv "R_REPCGC", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_REPCGC = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_REPCGC = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_REPCGC - RC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_REPVEN() As Boolean
Dim vrstCount As ADODB.Recordset
    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_REPVEN#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_REPVEN", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        Set vrstCount = New ADODB.Recordset
        vrstCount.Open "Select Count(*) as Qtd from R_Repven where cod_repres = " & vRst!Cod_Repres & " and cod_vend = " & vRst!Cod_vend, connBaseDpk
        If vrstCount!qtd = 0 Then
            connBaseDpk.Execute "Insert into R_Repven values(" & IIf(IsNull(vRst!Cod_Repres), 0, vRst!Cod_Repres) & ", " & IIf(IsNull(vRst!Cod_vend), 0, vRst!Cod_vend) & ")"
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
        End If
        vRst.MoveNext
    Next
    Atualizar_lsv "R_REPVEN", "INSERT", vRegsIn
    Atualizar_lsv "R_REPVEN", "DELETE", vRegsDel
    Atualizar_lsv "R_REPVEN", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_REPVEN = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_REPVEN = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_REPVEN - RV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_UF_DEPOSITO() As Boolean
Dim vrstCount As ADODB.Recordset
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_UF_DEP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_UF_DEPOSITO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        Set vrstCount = New ADODB.Recordset
        vrstCount.Open "Select Count(*) as Qtd from R_uf_deposito where cod_uf = '" & vRst!Cod_uf & "' and cod_loja = " & vRst!Cod_loja, connBaseDpk
        If vrstCount!qtd = 0 Then
            connBaseDpk.Execute "Insert into R_UF_DEPOSITO values(" & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & ", " & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & ")"
            vRegsIn = vRegsIn + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "R_UF_DEPOSITO", "INSERT", vRegsIn
    Atualizar_lsv "R_UF_DEPOSITO", "DELETE", vRegsDel
    Atualizar_lsv "R_UF_DEPOSITO", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_R_UF_DEPOSITO = True
          
VerErro:
    If Err.Number <> 0 Then
        ATU_R_UF_DEPOSITO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_R_UF_DEPOSITO - KJ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_REPEND() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from REP_ENDC#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "REPR_END_CORRESP", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update REPR_END_CORRESP set endereco = " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                            ", bairro = " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                            ", cod_cidade = " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                            ", cep = " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                            ", ddd = " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                            ", fone = " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                            ", fax = " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                            ", celular = " & IIf(IsNull(vRst!celular), 0, vRst!celular) & _
                            ", cxpostal = " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & " where cod_repres = " & vRst!Cod_Repres, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into REPR_END_CORRESP values(" & IIf(IsNull(vRst!Cod_Repres), 0, vRst!Cod_Repres) & _
                                ", " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                                ", " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                                ", " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                                ", " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                                ", " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                                ", " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                                ", " & IIf(IsNull(vRst!celular), 0, vRst!celular) & _
                                ", " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "REPR_END_CORRESP", "INSERT", vRegsIn
    Atualizar_lsv "REPR_END_CORRESP", "UPDATE", vRegsUp
    Atualizar_lsv "REPR_END_CORRESP", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_REPEND = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_REPEND = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_REPEND - RE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_REPRESENTACAO() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from REPRESEN#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "REPRESENTACAO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update REPRESENTACAO set Razao_social = " & IIf(IsNull(vRst!Razao_Soci), "null", "'" & vRst!Razao_Soci & "'") & _
                            ", Num_cori = " & IIf(IsNull(vRst!Num_cori), 0, vRst!Num_cori) & _
                            ", Endereco = " & IIf(IsNull(vRst!endereco), "null", "'" & vRst!endereco & "'") & _
                            ", Bairro = " & IIf(IsNull(vRst!bairro), "null", "'" & vRst!bairro & "'") & _
                            ", Cod_cidade = " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                            ", Cep = " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                            ", Ddd = " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                            ", Fone = " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                            ", Fax = " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                            ", Celular = " & IIf(IsNull(vRst!celular), 0, vRst!celular) & _
                            ", Cxpostal = " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & " where Cgc =" & vRst!cgc, vRegs
            If vRegs = 0 Then
                connBaseDpk.Execute "Insert into REPRESENTACAO values(" & IIf(IsNull(vRst!cgc), 0, vRst!cgc) & _
                                    ", " & IIf(IsNull(vRst!Razao_Soci), "null", "'" & vRst!Razao_Soci & "'") & _
                                    ", " & IIf(IsNull(vRst!Num_cori), 0, vRst!Num_cori) & _
                                    ", " & IIf(IsNull(vRst!endereco), "null", "'" & vRst!endereco & "'") & _
                                    ", " & IIf(IsNull(vRst!bairro), "null", "'" & vRst!bairro & "'") & _
                                    ", " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                                    ", " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                                    ", " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                                    ", " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                                    ", " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                                    ", " & IIf(IsNull(vRst!celular), 0, vRst!celular) & _
                                    ", " & IIf(IsNull(vRst!cxpostal), 0, vRst!cxpostal) & " )"
                vRegsIn = vRegsIn + 1
            Else
                vRegsUp = vRegsUp + 1
            End If
            vRst.MoveNext
            ProgressBar -1, 1
    Next
    Atualizar_lsv "REPRESENTACAO", "INSERT", vRegsIn
    Atualizar_lsv "REPRESENTACAO", "UPDATE", vRegsUp
    Atualizar_lsv "REPRESENTACAO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_REPRESENTACAO = True

VerErro:

    If Err.Number <> 0 Then
        ATU_REPRESENTACAO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_REPRESENTACAO - RP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_REPRESENTANTE() As Boolean

    On Error GoTo VerErro
     
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from REPRES#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "REPRESENTANTE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Representante set Pseudonimo = " & IIf(IsNull(vRst!pseudonimo), "Null", "'" & vRst!pseudonimo & "'") & _
                            ", Endereco = " & IIf(IsNull(vRst!endereco), "Null", "'" & vRst!endereco & "'") & _
                            ", bairro = " & IIf(IsNull(vRst!bairro), "Null", "'" & vRst!bairro & "'") & _
                            ", cod_cidade = " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                            ", cep = " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                            ", ddd = " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                            ", fone = " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                            ", fax = " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                            ", ramal = " & IIf(IsNull(vRst!ramal), 0, vRst!ramal) & _
                            ", cod_filial = " & IIf(IsNull(vRst!Cod_filial), 0, vRst!Cod_filial) & _
                            ", cic_gerente = " & IIf(IsNull(vRst!cic_gerent), 0, vRst!cic_gerent) & _
                            ", situacao = " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & _
                            ", tipo = " & IIf(IsNull(vRst!tipo), "Null", "'" & vRst!tipo & "'") & _
                            ", seq_franquia = " & IIf(IsNull(vRst!seq_franqu), 0, vRst!seq_franqu) & _
                            ", dt_cadastr = " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                            ", dt_desligamento = " & IIf(IsNull(vRst!dt_desliga), "Null", "'" & vRst!dt_desliga & "'") & " where cic = " & vRst!cic & " and cod_repres = " & vRst!Cod_Repres & " and nome_repres = '" & vRst!nome_repre & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Representante values(" & IIf(IsNull(vRst!cic), 0, vRst!cic) & _
                                ", " & IIf(IsNull(vRst!Cod_Repres), 0, vRst!Cod_Repres) & _
                                ", " & IIf(IsNull(vRst!nome_repre), "null", "'" & vRst!nome_repre & "'") & _
                                ", " & IIf(IsNull(vRst!pseudonimo), "null", "'" & vRst!pseudonimo & "'") & _
                                ", " & IIf(IsNull(vRst!endereco), "null", "'" & vRst!endereco & "'") & _
                                ", " & IIf(IsNull(vRst!bairro), "null", "'" & vRst!bairro & "'") & _
                                ", " & IIf(IsNull(vRst!cod_cidade), 0, vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!cep), 0, vRst!cep) & _
                                ", " & IIf(IsNull(vRst!ddd), 0, vRst!ddd) & _
                                ", " & IIf(IsNull(vRst!fone), 0, vRst!fone) & _
                                ", " & IIf(IsNull(vRst!fax), 0, vRst!fax) & _
                                ", " & IIf(IsNull(vRst!ramal), 0, vRst!ramal) & _
                                ", " & IIf(IsNull(vRst!Cod_filial), 0, vRst!Cod_filial) & _
                                ", " & IIf(IsNull(vRst!cic_gerent), 0, vRst!cic_gerent) & _
                                ", " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & _
                                ", " & IIf(IsNull(vRst!tipo), "null", "'" & vRst!tipo & "'") & _
                                ", " & IIf(IsNull(vRst!seq_franqu), 0, vRst!seq_franqu) & _
                                ", " & IIf(IsNull(vRst!dt_cadastr), "null", "'" & vRst!dt_cadastr & "'") & _
                                ", " & IIf(IsNull(vRst!dt_desliga), "null", "'" & vRst!dt_desliga & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    'Deleta representante com situa��o = 9 das tabelas relacionadas.
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from representante where situacao = 9", connBaseDpk
    vRegsRst = vRst.RecordCount
    For i = 1 To vRegsRst
    DoEvents
        'Tabela Representa��o
        connBaseDpk.Execute "Delete from Representacao where cgc = " & vRst!cgc
        
        'Tabela R_RepCgc
        connBaseDpk.Execute "Delete from R_RepCgc where cod_repres = " & vRst!Cod_Repres
        
        'Tabela Repr_End_Corresp
        connBaseDpk.Execute "Delete from Repr_End_Corresp where cod_repres = " & vRst!Cod_Repres
        
        'Tabela R_RepVen
        connBaseDpk.Execute "Delete from R_RepVen where cod_repres = " & vRst!Cod_Repres & " or Cod_vend = " & vRst!Cod_Repres
                
        'Tabela Clie_Credito
        connBaseDpk.Execute "Delete from Clie_Credito where cod_repr_vend = " & vRst!Cod_Repres
                
        'Tabela Clie_endereco
        connBaseDpk.Execute "Delete from Clie_endereco where cod_repr_vend = " & vRst!Cod_Repres
                
        'Tabela duplicatas
        connBaseDpk.Execute "Delete from duplicatas where cod_repr_vend = " & vRst!Cod_Repres
                
        'Tabela cliente
        connBaseDpk.Execute "Delete from cliente where cod_repr_vend = " & vRst!Cod_Repres
                
        'Tabela representante
        connBaseDpk.Execute "Delete from representante where cod_repres = " & vRst!Cod_Repres
        
        vRst.MoveNext
    Next
    Atualizar_lsv "REPRESENTANTE", "INSERT", vRegsIn
    Atualizar_lsv "REPRESENTANTE", "UPDATE", vRegsUp
    Atualizar_lsv "REPRESENTANTE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_REPRESENTANTE = True

VerErro:
    If Err.Number <> 0 Then
        ATU_REPRESENTANTE = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_REPRESENTANTE - AO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_RESULTADO() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from RESULTAD#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "RESULTADO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Resultado set desc_resultado = " & IIf(IsNull(vRst!desc_resul), "Null", "'" & vRst!desc_resul & "'") & " where cod_resultado = " & vRst!cod_result, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Resultado values (" & IIf(IsNull(vRst!cod_result), 0, vRst!cod_result) & ", " & IIf(IsNull(vRst!desc_resul), "Null", "'" & vRst!desc_resul & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "RESULTADO", "INSERT", vRegsIn
    Atualizar_lsv "RESULTADO", "DELETE", vRegsDel
    Atualizar_lsv "RESULTADO", "UPDATE", vRegsUp
    Set vRst = Nothing
    ATU_RESULTADO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_RESULTADO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_RESULTADO - FN" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ROMANEIO() As Boolean
     On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ROMANEIO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ROMANEIO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Romaneio set dt_coleta = " & IIf(IsNull(vRst!dt_coleta), "Null", "'" & vRst!dt_coleta & "'") & _
                            ", dt_despacho = " & IIf(IsNull(vRst!dt_despach), "Null", "'" & vRst!dt_despach & "'") & _
                            ", num_romaneio = " & IIf(IsNull(vRst!num_romane), "Null", vRst!num_romane) & _
                            ", num_conhecimento = " & IIf(IsNull(vRst!num_conhec), "Null", "'" & vRst!num_conhec & "'") & _
                            ", num_carro = " & IIf(IsNull(vRst!num_carro), "Null", "'" & vRst!num_carro & "'") & _
                            ", qtd_volume = " & IIf(IsNull(vRst!qtd_Volume), "Null", vRst!qtd_Volume) & " where cod_loja = " & vRst!Cod_loja & " and num_pedido = " & vRst!num_pedido & " and seq_pedido = " & vRst!seq_pedido, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Romaneio values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!num_pedido), "Null", vRst!num_pedido) & _
                                ", " & IIf(IsNull(vRst!seq_pedido), "Null", vRst!seq_pedido) & _
                                ", " & IIf(IsNull(vRst!dt_coleta), "Null", "'" & vRst!dt_coleta & "'") & _
                                ", " & IIf(IsNull(vRst!dt_despach), "Null", "'" & vRst!dt_despach & "'") & _
                                ", " & IIf(IsNull(vRst!num_romane), "Null", vRst!num_romane) & _
                                ", " & IIf(IsNull(vRst!num_conhec), "Null", "'" & vRst!num_conhec & "'") & _
                                ", " & IIf(IsNull(vRst!num_carro), "Null", "'" & vRst!num_carro & "'") & _
                                ", " & IIf(IsNull(vRst!qtd_Volume), "Null", vRst!qtd_Volume) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "ROMANEIO", "INSERT", vRegsIn
    Atualizar_lsv "ROMANEIO", "UPDATE", vRegsUp
    Atualizar_lsv "ROMANEIO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_ROMANEIO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ROMANEIO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ROMANEIO - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_SALDO_PEDIDO() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
       
   
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from SALDO_PE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "SALDO_PEDIDOS", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from Saldo_Pedidos", vRegsDel
    
        connBaseDpk.Execute "Insert into Saldo_pedidos values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                            ", " & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client) & _
                            ", " & IIf(IsNull(vRst!saldo_pedi), "Null", vRst!saldo_pedi) & ")"
        vRegsIn = vRegsIn + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "SALDO_PEDIDOS", "INSERT", vRegsIn
    Atualizar_lsv "SALDO_PEDIDOS", "DELETE", vRegsDel
    Atualizar_lsv "SALDO_PEDIDOS", "UPDATE", vRegsUp
    Set vRst = Nothing
    ATU_SALDO_PEDIDO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_SALDO_PEDIDO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_SALDO_PEDIDO - AF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_SUBGRUPO() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from SUBGRUPO#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "SUBGRUPO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update SubGrupo set Desc_SubGrupo = " & IIf(IsNull(vRst!desc_subgr), "Null", "'" & vRst!desc_subgr & "'") & " where cod_grupo = " & vRst!cod_grupo & "  and cod_subgrupo = " & vRst!cod_subgru, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into SubGrupo values(" & IIf(IsNull(vRst!cod_grupo), 0, vRst!cod_grupo) & ", " & IIf(IsNull(vRst!cod_subgru), 0, vRst!cod_subgru) & ", " & IIf(IsNull(vRst!desc_subgr), "Null", "'" & vRst!desc_subgr & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "SUBGRUPO", "INSERT", vRegsIn
    Atualizar_lsv "SUBGRUPO", "UPDATE", vRegsUp
    Atualizar_lsv "SUBGRUPO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_SUBGRUPO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_SUBGRUPO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_SUBGRUPO - AP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_SUBTRIB() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from SUBST_TR#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "SUBST_TRIBUTARIA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Subst_Tributaria set Cod_trib_revendedor = " & IIf(IsNull(vRst!cod_revend), 0, vRst!cod_revend) & _
                            ", cod_trib_inscrito = " & IIf(IsNull(vRst!cod_inscri), 0, vRst!cod_inscri) & _
                            ", cod_trib_isento = " & IIf(IsNull(vRst!cod_isento), 0, vRst!cod_isento) & _
                            ", fator_revendedor = " & IIf(IsNull(vRst!fator_reve), 0, vRst!fator_reve) & _
                            ", fator_inscrito = " & IIf(IsNull(vRst!fator_insc), 0, vRst!fator_insc) & _
                            ", fator_isento = " & IIf(IsNull(vRst!fator_isen), 0, vRst!fator_isen) & _
                            ", fl_cred_suspenso = " & IIf(IsNull(vRst!fl_cred_su), "Null", "'" & vRst!fl_cred_su & "'") & _
                            ", pc_margem_lucro = " & IIf(IsNull(vRst!pc_margem_), 0, vRst!pc_margem_) & _
                            " where class_fiscal = " & vRst!Class_Fisc & _
                            " and cod_uf_origem = '" & vRst!cod_origem & _
                            "' and cod_uf_destino = '" & vRst!cod_destin & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Subst_Tributaria values(" & IIf(IsNull(vRst!Class_Fisc), 0, vRst!Class_Fisc) & _
                                ", " & IIf(IsNull(vRst!cod_origem), "Null", "'" & vRst!cod_origem & "'") & _
                                ", " & IIf(IsNull(vRst!cod_destin), "Null", "'" & vRst!cod_destin & "'") & _
                                ", " & IIf(IsNull(vRst!cod_revend), 0, vRst!cod_revend) & _
                                ", " & IIf(IsNull(vRst!cod_inscri), 0, vRst!cod_inscri) & _
                                ", " & IIf(IsNull(vRst!cod_isento), 0, vRst!cod_isento) & _
                                ", " & IIf(IsNull(vRst!fator_reve), 0, vRst!fator_reve) & _
                                ", " & IIf(IsNull(vRst!fator_insc), 0, vRst!fator_insc) & _
                                ", " & IIf(IsNull(vRst!fator_isen), 0, vRst!fator_isen) & _
                                ", " & IIf(IsNull(vRst!fl_cred_su), "Null", "'" & vRst!fl_cred_su & "'") & _
                                ", " & IIf(IsNull(vRst!pc_margem_), 0, vRst!pc_margem_) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "SUBST_TRIBUTARIA", "INSERT", vRegsIn
    Atualizar_lsv "SUBST_TRIBUTARIA", "UPDATE", vRegsUp
    Atualizar_lsv "SUBST_TRIBUTARIA", "DELETE", vRegsDel
    Set vRst = Nothing
    ATU_SUBTRIB = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_SUBTRIB = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_SUBTRIB - CP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TABDESCPER() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TABELAD#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TABELA_DESCPER", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from Tabela_descper", vRegsDel
        
        connBaseDpk.Execute "Insert into Tabela_descper values(" & IIf(IsNull(vRst!sequencia), 0, vRst!sequencia) & _
                            ", " & IIf(IsNull(vRst!Tp_Tabela), 0, vRst!Tp_Tabela) & _
                            ", " & IIf(IsNull(vRst!ocorr_prec), 0, vRst!ocorr_prec) & _
                            ", " & IIf(IsNull(vRst!Cod_filial), 0, vRst!Cod_filial) & _
                            ", " & IIf(IsNull(vRst!cod_fornec), 0, vRst!cod_fornec) & _
                            ", " & IIf(IsNull(vRst!cod_grupo), 0, vRst!cod_grupo) & _
                            ", " & IIf(IsNull(vRst!cod_subgru), 0, vRst!cod_subgru) & _
                            ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & _
                            ", " & IIf(IsNull(vRst!situacao), 0, vRst!situacao) & _
                            ", " & IIf(IsNull(vRst!pc_desc1), 0, vRst!pc_desc1) & _
                            ", " & IIf(IsNull(vRst!pc_desc2), 0, vRst!pc_desc2) & _
                            ", " & IIf(IsNull(vRst!pc_desc3), 0, vRst!pc_desc3) & ")"
        vRegsIn = vRegsIn + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "TABELA_DESCPER", "INSERT", vRegsIn
    Atualizar_lsv "TABELA_DESCPER", "DELETE", vRegsDel
    Atualizar_lsv "TABELA_DESCPER", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_TABDESCPER = True
    
VerErro:
    If Err.Number <> 0 Then
        ATU_TABDESCPER = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TABDESCPER - AQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TABVENDA() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TABELAV#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TABELA_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Tabela_Venda set tabela_venda = " & IIf(IsNull(vRst!tabela_ven), "Null", "'" & vRst!tabela_ven & "'") & _
                            ", dt_vigencia1 = " & IIf(IsNull(vRst!dt_vig1), 0, vRst!dt_vig1) & _
                            ", dt_vigencia2 = " & IIf(IsNull(vRst!dt_vig2), 0, vRst!dt_vig2) & _
                            ", dt_vigencia3 = " & IIf(IsNull(vRst!dt_vig3), 0, vRst!dt_vig3) & _
                            ", dt_vigencia4 = " & IIf(IsNull(vRst!dt_vig4), 0, vRst!dt_vig4) & _
                            ", dt_vigencia5 = " & IIf(IsNull(vRst!dt_vig5), 0, vRst!dt_vig5) & " where tp_tabela = " & vRst!Tp_Tabela & " and ocorr_preco = " & vRst!ocorr_prec, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Tabela_Venda values(" & IIf(IsNull(vRst!tabela_ven), "Null", "'" & vRst!tabela_ven & "'") & _
                                ", " & IIf(IsNull(vRst!Tp_Tabela), 0, vRst!Tp_Tabela) & _
                                ", " & IIf(IsNull(vRst!ocorr_prec), 0, vRst!ocorr_prec) & _
                                ", " & IIf(IsNull(vRst!dt_vig1), 0, vRst!dt_vig1) & _
                                ", " & IIf(IsNull(vRst!dt_vig2), 0, vRst!dt_vig2) & _
                                ", " & IIf(IsNull(vRst!dt_vig3), 0, vRst!dt_vig3) & _
                                ", " & IIf(IsNull(vRst!dt_vig4), 0, vRst!dt_vig4) & _
                                ", " & IIf(IsNull(vRst!dt_vig5), 0, vRst!dt_vig5) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "TABELA_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "TABELA_VENDA", "UPDATE", vRegsUp
    Atualizar_lsv "TABELA_VENDA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_TABVENDA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_TABVENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TABVENDA - AR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TPCLIE() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TIPO_CLI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TIPO_CLIENTE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Tipo_Cliente set Desc_tipo_cli = " & IIf(IsNull(vRst!desc_tipo), "Null", "'" & vRst!desc_tipo & "'") & _
                            ", cod_segmento = " & IIf(IsNull(vRst!cod_segmen), "Null", "'" & vRst!cod_segmen & "'") & " where cod_tipo_cli = '" & vRst!cod_tipo_c & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Tipo_cliente values(" & IIf(IsNull(vRst!cod_tipo_c), "Null", "'" & vRst!cod_tipo_c & "'") & ", " & IIf(IsNull(vRst!desc_tipo), "Null", "'" & vRst!desc_tipo & "'") & ", " & IIf(IsNull(vRst!cod_segmen), "Null", "'" & vRst!cod_segmen & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "TIPO_CLIENTE", "INSERT", vRegsIn
    Atualizar_lsv "TIPO_CLIENTE", "UPDATE", vRegsUp
    Atualizar_lsv "TIPO_CLIENTE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_TPCLIE = True

VerErro:
    If Err.Number <> 0 Then
        ATU_TPCLIE = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TPCLIE - AT" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TIPO_CLIENTE_BLAU() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TP_BLAU#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TIPO_CLIENTE_BLAU", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update TIPO_CLIENTE_BLAU set desc_tipo_cli = " & IIf(IsNull(vRst!desc_tipo_), "Null", "'" & vRst!desc_tipo_ & "'") & " where cod_tipo_cli = '" & vRst!cod_tipo_c & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into TIPO_CLIENTE_BLAU values(" & IIf(IsNull(vRst!cod_tipo_c), "Null", "'" & vRst!cod_tipo_c & "'") & "," & IIf(IsNull(vRst!desc_tipo_), "Null", "'" & vRst!desc_tipo_ & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "TIPO_CLIENTE_BLAU", "INSERT", vRegsIn
    Atualizar_lsv "TIPO_CLIENTE_BLAU", "UPDATE", vRegsUp
    Atualizar_lsv "TIPO_CLIENTE_BLAU", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_TIPO_CLIENTE_BLAU = True
          
VerErro:
    If Err.Number <> 0 Then
        If Err.Number = -2147467259 Then
            Resume Next
        End If
        ATU_TIPO_CLIENTE_BLAU = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TIPO_CLIENTE_BLAU - CH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TRANSPORTADORA() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TRANSPOR#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TRANSPORTADORA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Transportadora set Nome_transp = " & IIf(IsNull(vRst!nome_trans), "Null", "'" & vRst!nome_trans & "'") & _
                            ", cod_cidade = " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                            ", situacao = " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & " where cod_transp = " & vRst!cod_transp, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into transportadora values(" & IIf(IsNull(vRst!cod_transp), "Null", vRst!cod_transp) & _
                                ", " & IIf(IsNull(vRst!nome_trans), "Null", "'" & vRst!nome_trans & "'") & _
                                ", " & IIf(IsNull(vRst!cod_cidade), "Null", vRst!cod_cidade) & _
                                ", " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "TRANSPORTADORA", "INSERT", vRegsIn
    Atualizar_lsv "TRANSPORTADORA", "UPDATE", vRegsUp
    Atualizar_lsv "TRANSPORTADORA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_TRANSPORTADORA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_TRANSPORTADORA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TRANSPORTADORA - AU" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UF() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update uf set desc_uf = " & IIf(IsNull(vRst!desc_uf), "Null", "'" & vRst!desc_uf & "'") & _
                            ", dt_aliq_interna = " & IIf(IsNull(vRst!dt_aliq_in), "Null", "'" & vRst!dt_aliq_in & "'") & " where cod_uf = '" & vRst!Cod_uf & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into uf values(" & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!desc_uf), "Null", "'" & vRst!desc_uf & "'") & _
                                ", " & IIf(IsNull(vRst!dt_aliq_in), "Null", "'" & vRst!dt_aliq_in & "'") & "')"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "UF", "INSERT", vRegsIn
    Atualizar_lsv "UF", "UPDATE", vRegsUp
    Atualizar_lsv "UF", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_UF = True

VerErro:

    If Err.Number <> 0 Then
        ATU_UF = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UF - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UF_CATEG() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF_CATEG#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF_CATEG", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update UF_CATEG set pc_desc_adic = " & IIf(IsNull(vRst!pc_desc_ad), "Null", vRst!pc_desc_ad) & _
                            ", pc_adic_maximo = " & IIf(IsNull(vRst!pc_adic_ma), "Null", vRst!pc_adic_ma) & _
                            ", pc_desc_comis = " & IIf(IsNull(vRst!pc_desc_co), "Null", vRst!pc_desc_co) & _
                            ", desc_comis_maximo = " & IIf(IsNull(vRst!desc_comis), "Null", vRst!desc_comis) & " where cod_uf = '" & vRst!Cod_uf & "' and categoria = " & vRst!categoria & " and caracteristica = '" & vRst!caracteris & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into UF_CATEG values(" & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & _
                                ", " & IIf(IsNull(vRst!categoria), "Null", vRst!categoria) & _
                                ", " & IIf(IsNull(vRst!pc_desc_ad), "Null", vRst!pc_desc_ad) & _
                                ", " & IIf(IsNull(vRst!pc_adic_ma), "Null", vRst!pc_adic_ma) & _
                                ", " & IIf(IsNull(vRst!pc_desc_co), "Null", vRst!pc_desc_co) & _
                                ", " & IIf(IsNull(vRst!desc_comis), "Null", vRst!desc_comis) & _
                                ", '" & IIf(IsNull(vRst!caracteris), "Null", vRst!caracteris) & "')"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "UF_CATEG", "INSERT", vRegsIn
    Atualizar_lsv "UF_CATEG", "UPDATE", vRegsUp
    Atualizar_lsv "UF_CATEG", "DELETE", vRegsDel
    Set vRst = Nothing
    ATU_UF_CATEG = True

VerErro:
    If Err.Number <> 0 Then
        ATU_UF_CATEG = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UF_CATEG - 03" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UF_DEPOSITO() As Boolean
Dim vrstCount As ADODB.Recordset
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF_DEP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF_DEPOSITO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update UF_DEPOSITO set pc_frete = " & IIf(IsNull(vRst!pc_frete), 0, vRst!pc_frete) & _
                            " where cod_loja = " & vRst!Cod_loja & _
                            " and cod_uf_destino = '" & vRst!cod_uf_des & "' and Inscr_estadual_st = '" & vRst!inscr_esta & "'", vRegs
        If vRegs = 0 Then
            Set vrstCount = New ADODB.Recordset
            vrstCount.Open "Select count(*) as Qtd from Uf_Deposito where cod_loja = " & vRst!Cod_loja & "  and cod_uf_destino = '" & vRst!cod_uf_des & "'", connBaseDpk
            If vrstCount!qtd = 0 Then
                connBaseDpk.Execute "Insert into UF_DEPOSITO values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & _
                                    ", " & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                                    ", " & IIf(IsNull(vRst!pc_frete), 0, vRst!pc_frete) & _
                                    ", " & IIf(IsNull(vRst!inscr_esta), "Null", "'" & vRst!inscr_esta & "'") & ")"
            End If

            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "UF_DEPOSITO", "INSERT", vRegsIn
    Atualizar_lsv "UF_DEPOSITO", "UPDATE", vRegsUp
    Atualizar_lsv "UF_DEPOSITO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_UF_DEPOSITO = True

VerErro:
    If Err.Number <> 0 Then
        ATU_UF_DEPOSITO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UF_DEPOSITO - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UFDPK() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF_DPK#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF_DPK", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Uf_dpk set pc_desc = " & IIf(IsNull(vRst!pc_desc), 0, vRst!pc_desc) & _
                            " where cod_uf_origem = '" & vRst!cod_uf_ori & _
                            "' and cod_uf_destino = '" & vRst!cod_uf_des & _
                            "' and cod_dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute " Insert into uf_dpk values(" & IIf(IsNull(vRst!cod_uf_ori), "Null", "'" & vRst!cod_uf_ori & "'") & _
                                ", " & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                                ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!pc_desc), 0, vRst!pc_desc) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "UF_DPK", "INSERT", vRegsIn
    Atualizar_lsv "UF_DPK", "UPDATE", vRegsUp
    Atualizar_lsv "UF_DPK", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_UFDPK = True

VerErro:
    If Err.Number <> 0 Then
        ATU_UFDPK = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UFDPK - 03" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UF_ORIGDEST() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF_OR_DE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF_ORIGEM_DESTINO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update UF_ORIGEM_DESTINO set pc_icm = " & IIf(IsNull(vRst!pc_icm), 0, vRst!pc_icm) & _
                            ", pc_dificm = " & IIf(IsNull(vRst!pc_dificm), 0, vRst!pc_dificm) & " where cod_uf_origem = '" & vRst!cod_uf_ori & "' and cod_uf_destino = '" & vRst!cod_uf_des & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into UF_ORIGEM_DESTINO values(" & IIf(IsNull(vRst!cod_uf_ori), "Null", "'" & vRst!cod_uf_ori & "'") & _
                                ", " & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                                "," & IIf(IsNull(vRst!pc_icm), 0, vRst!pc_icm) & _
                                ", " & IIf(IsNull(vRst!pc_dificm), 0, vRst!pc_dificm) & " )"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "UF_ORIGEM_DESTINO", "INSERT", vRegsIn
    Atualizar_lsv "UF_ORIGEM_DESTINO", "UPDATE", vRegsUp
    Atualizar_lsv "UF_ORIGEM_DESTINO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_UF_ORIGDEST = True

VerErro:
    If Err.Number <> 0 Then
        ATU_UF_ORIGDEST = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UF_ORIGDEST - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_UF_TPCLIENTE() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from UF_TPCLI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "UF_TPCLIENTE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Uf_tpCliente set pc_desc = " & IIf(IsNull(vRst!pc_desc), "Null", vRst!pc_desc) & _
                            ", cod_tributacao_final = " & IIf(IsNull(vRst!cod_final), "Null", vRst!cod_final) & _
                            ", pc_desc_diesel = " & IIf(IsNull(vRst!pc_desc_di), "Null", vRst!pc_desc_di) & " where cod_uf_origem = '" & vRst!cod_origem & _
                            "' and cod_uf_destino = '" & vRst!cod_destin & "' and tp_cliente = '" & vRst!tp_cliente & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Uf_tpCliente values(" & IIf(IsNull(vRst!cod_origem), "Null", "'" & vRst!cod_origem & "'") & _
                                ", " & IIf(IsNull(vRst!cod_destin), "Null", "'" & vRst!cod_destin & "'") & _
                                ", " & IIf(IsNull(vRst!tp_cliente), "Null", "'" & vRst!tp_cliente & "'") & _
                                ", " & IIf(IsNull(vRst!cod_tribut), "Null", vRst!cod_tribut) & _
                                ", " & IIf(IsNull(vRst!pc_desc), "Null", vRst!pc_desc) & _
                                ", " & IIf(IsNull(vRst!cod_final), "Null", vRst!cod_final) & _
                                ", " & IIf(IsNull(vRst!pc_desc_di), "Null", vRst!pc_desc_di) & " )"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "UF_TPCLIENTE", "INSERT", vRegsIn
    Atualizar_lsv "UF_TPCLIENTE", "UPDATE", vRegsUp
    Atualizar_lsv "UF_TPCLIENTE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_UF_TPCLIENTE = True

VerErro:
    If Err.Number <> 0 Then
        ATU_UF_TPCLIENTE = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_UF_TPCLIENTE - FP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_V_CLIENTE_FIEL() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VCLIFIEL#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "V_CLIENTE_FIEL", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from v_cliente_fiel", vRegsDel
        
        connBaseDpk.Execute "Insert into v_cliente_fiel values(" & IIf(IsNull(vRst!cod_client), 0, vRst!cod_client) & _
                            ", " & IIf(IsNull(vRst!tp_fiel), 0, vRst!tp_fiel) & _
                            ", " & IIf(IsNull(vRst!desc_fiel), "Null", "'" & vRst!desc_fiel & "'") & ")"
        vRegsIn = vRegsIn + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "V_CLIENTE_FIEL", "INSERT", vRegsIn
    Atualizar_lsv "V_CLIENTE_FIEL", "DELETE", vRegsDel
    Atualizar_lsv "V_CLIENTE_FIEL", "UPDATE", vRegsUp

    Set vRst = Nothing
    ATU_V_CLIENTE_FIEL = True

VerErro:

    If Err.Number <> 0 Then
        ATU_V_CLIENTE_FIEL = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_V_CLIENTE_FIEL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_V_PEDLIQ_VENDA() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from V_PEDLIQ#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "V_PEDLIQ_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update v_pedliq_venda set situacao = " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & _
                            ", pr_liquido = " & IIf(IsNull(vRst!pr_liquido), "Null", vRst!pr_liquido) & _
                            ", vl_liquido = " & IIf(IsNull(vRst!vl_liquido), "Null", vRst!vl_liquido) & _
                            ", pr_liquido_suframa = " & IIf(IsNull(vRst!pr_suframa), "Null", vRst!pr_suframa) & _
                            ", vl_liquido_suframa = " & IIf(IsNull(vRst!vl_suframa), "Null", vRst!vl_suframa) & " where cod_loja = " & vRst!Cod_loja & _
                            " and num_pedido = " & vRst!num_pedido & " and seq_pedido = " & vRst!seq_pedido & " and num_item_pedido = " & vRst!num_item_p, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into v_pedliq_venda values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!num_pedido), "Null", vRst!num_pedido) & _
                                ", " & IIf(IsNull(vRst!seq_pedido), "Null", vRst!seq_pedido) & _
                                ", " & IIf(IsNull(vRst!num_item_p), "Null", vRst!num_item_p) & _
                                ", " & IIf(IsNull(vRst!situacao), "Null", vRst!situacao) & _
                                ", " & IIf(IsNull(vRst!pr_liquido), "Null", vRst!pr_liquido) & _
                                ", " & IIf(IsNull(vRst!vl_liquido), "Null", vRst!vl_liquido) & _
                                ", " & IIf(IsNull(vRst!pr_suframa), "Null", vRst!pr_suframa) & _
                                ", " & IIf(IsNull(vRst!vl_suframa), "Null", vRst!vl_suframa) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "V_PEDLIQ_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "V_PEDLIQ_VENDA", "UPDATE", vRegsUp
    Atualizar_lsv "V_PEDLIQ_VENDA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_V_PEDLIQ_VENDA = True

VerErro:

    If Err.Number <> 0 Then
        ATU_V_PEDLIQ_VENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_V_PEDLIQ_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VENDA_LIMITADA() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
   
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VLIMITAD#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VENDA_LIMITADA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from Venda_limitada", vRegsDel
        
        connBaseDpk.Execute "Insert into venda_limitada values(" & IIf(IsNull(vRst!Cod_uf), "Null", "'" & vRst!Cod_uf & "'") & ", " & IIf(IsNull(vRst!tp_cliente), "Null", "'" & vRst!tp_cliente & "'") & ", " & IIf(IsNull(vRst!cod_loja_o), 0, vRst!cod_loja_o) & ")"
        vRegsIn = vRegsIn + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VENDA_LIMITADA", "INSERT", vRegsRst
    Atualizar_lsv "VENDA_LIMITADA", "DELETE", vRegsDel
    Atualizar_lsv "VENDA_LIMITADA", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_VENDA_LIMITADA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_VENDA_LIMITADA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VENDA_LIMITADA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VDR_CONTROLE_VDR() As Boolean
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_CONT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_CONTROLE_VDR", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Vdr_controle_vdr set cod_vdr = " & IIf(IsNull(vRst!cod_vdr), "Null", vRst!cod_vdr) & _
                            ", linha_produto = " & IIf(IsNull(vRst!linha_pr), "Null", "'" & vRst!linha_pr & "'") & _
                            " where cod_fornecedor = " & vRst!cod_forn & _
                            " and cod_grupo = " & vRst!cod_grup & _
                            " and cod_subgrupo = " & vRst!cod_subg & _
                            " and cod_tipo_cliente = '" & vRst!cod_tipo & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Vdr_controle_vdr values(" & IIf(IsNull(vRst!cod_forn), "Null", vRst!cod_forn) & _
                                ", " & IIf(IsNull(vRst!cod_grup), "Null", vRst!cod_grup) & _
                                ", " & IIf(IsNull(vRst!cod_subg), "Null", vRst!cod_subg) & _
                                ", " & IIf(IsNull(vRst!cod_tipo), "Null", "'" & vRst!cod_tipo & "'") & _
                                ", " & IIf(IsNull(vRst!cod_vdr), "Null", vRst!cod_vdr) & _
                                ", " & IIf(IsNull(vRst!linha_pr), "Null", "'" & vRst!linha_pr & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VDR_CONTROLE_VDR", "INSERT", vRegsIn
    Atualizar_lsv "VDR_CONTROLE_VDR", "UPDATE", vRegsUp
    Atualizar_lsv "VDR_CONTROLE_VDR", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_VDR_CONTROLE_VDR = True

VerErro:
    
    If Err.Number <> 0 Then
        ATU_VDR_CONTROLE_VDR = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_CONTROLE_VDR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VDR_R_CLIE_LINHA_PRODUTO() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_RCLI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_R_CLIE_LINHA_PRODUTO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from Vdr_r_clie_linha_produto", vRegsDel
        
        connBaseDpk.Execute "Insert into Vdr_r_clie_linha_produto values (" & IIf(IsNull(vRst!cod_clie), "Null", vRst!cod_clie) & ", " & IIf(IsNull(vRst!linha_pr), "Null", "'" & vRst!linha_pr & "'") & ")"
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VDR_R_CLIE_LINHA_PRODUTO", "INSERT", vRegsRst
    Atualizar_lsv "VDR_R_CLIE_LINHA_PRODUTO", "DELETE", vRegsDel
    Atualizar_lsv "VDR_R_CLIE_LINHA_PRODUTO", "UPDATE", vRegsUp
    Set vRst = Nothing
    ATU_VDR_R_CLIE_LINHA_PRODUTO = True

VerErro:

    If Err.Number <> 0 Then
        ATU_VDR_R_CLIE_LINHA_PRODUTO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_CLIE_LINHA_PRODUTO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VDR_CLIENTE_CATEG_VDR() As Boolean
     On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_CLIE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_CLIENTE_CATEG_VDR", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Vdr_cliente_categ_vdr set categoria = " & IIf(IsNull(vRst!categoria), "Null", "'" & vRst!categoria & "'") & " where cod_cliente = " & vRst!cod_client, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Vdr_cliente_categ_vdr values(" & IIf(IsNull(vRst!cod_client), "Null", vRst!cod_client) & ", " & IIf(IsNull(vRst!categoria), "Null", "'" & vRst!categoria & "'") & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VDR_CLIENTE_CATEG_VDR", "INSERT", vRegsIn
    Atualizar_lsv "VDR_CLIENTE_CATEG_VDR", "UPDATE", vRegsUp
    Atualizar_lsv "VDR_CLIENTE_CATEG_VDR", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_VDR_CLIENTE_CATEG_VDR = True

VerErro:

    If Err.Number <> 0 Then
        ATU_VDR_CLIENTE_CATEG_VDR = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_CLIENTE_CATEG_VDR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VDR_DESCONTO_CATEG() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_DESC#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_DESCONTO_CATEG", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Vdr_desconto_categ set pc_desc = " & IIf(IsNull(vRst!pc_desc), "Null", vRst!pc_desc) & " where categoria = '" & vRst!categori & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Vdr_desconto_categ values(" & IIf(IsNull(vRst!categori), "Null", "'" & vRst!categori & "'") & ", " & IIf(IsNull(vRst!pc_desc), "Null", vRst!pc_desc) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VDR_DESCONTO_CATEG", "INSERT", vRegsIn
    Atualizar_lsv "VDR_DESCONTO_CATEG", "UPDATE", vRegsUp
    Atualizar_lsv "VDR_DESCONTO_CATEG", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_VDR_DESCONTO_CATEG = True

VerErro:

    If Err.Number <> 0 Then
        ATU_VDR_DESCONTO_CATEG = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_DESCONTO_CATEG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_VDR_TABELA_VENDA() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_TABE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_TABELA_VENDA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Vdr_tabela_venda set tabela_venda = " & IIf(IsNull(vRst!tabela_v), "Null", "'" & vRst!tabela_v & "'") & _
                            ", dt_vigencia1 = " & IIf(IsNull(vRst!dt_vige1), "Null", vRst!dt_vige1) & _
                            ", dt_vigencia2 = " & IIf(IsNull(vRst!dt_vige2), "Null", vRst!dt_vige2) & _
                            ", dt_vigencia3 = " & IIf(IsNull(vRst!dt_vige3), "Null", vRst!dt_vige3) & _
                            ", dt_vigencia4 = " & IIf(IsNull(vRst!dt_vige4), "Null", vRst!dt_vige4) & _
                            ", dt_vigencia5 = " & IIf(IsNull(vRst!dt_vige5), "Null", vRst!dt_vige5) & " where divisao = '" & vRst!divisao & _
                            "' and Tp_tabela = " & vRst!tp_tabel & " and ocorr_preco = " & vRst!ocorr_pr, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Vdr_tabela_venda values(" & IIf(IsNull(vRst!divisao), "Null", "'" & vRst!divisao & "'") & _
                                ", " & IIf(IsNull(vRst!tp_tabel), "Null", vRst!tp_tabel) & _
                                ", " & IIf(IsNull(vRst!ocorr_pr), "Null", vRst!ocorr_pr) & _
                                ", " & IIf(IsNull(vRst!tabela_v), "Null", "'" & vRst!tabela_v & "'") & _
                                ", " & IIf(IsNull(vRst!dt_vige1), "Null", vRst!dt_vige1) & _
                                ", " & IIf(IsNull(vRst!dt_vige2), "Null", vRst!dt_vige2) & _
                                ", " & IIf(IsNull(vRst!dt_vige3), "Null", vRst!dt_vige3) & _
                                ", " & IIf(IsNull(vRst!dt_vige4), "Null", vRst!dt_vige4) & _
                                ", " & IIf(IsNull(vRst!dt_vige5), "Null", vRst!dt_vige5) & " )"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "VDR_TABELA_VENDA", "UPDATE", vRegsUp
    Atualizar_lsv "VDR_TABELA_VENDA", "INSERT", vRegsIn
    Atualizar_lsv "VDR_TABELA_VENDA", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_VDR_TABELA_VENDA = True

VerErro:
    
    If Err.Number <> 0 Then
        ATU_VDR_TABELA_VENDA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_TABELA_VENDA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function R_FABRICA_DPK() As Boolean
    
    On Error GoTo Trata_Erro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from R_FABRIC#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_FABRICA_DPK", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update R_fabrica_dpk set dt_cadastro = " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                            ", sigla = " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                            ", Cod_fornecedor = " & IIf(IsNull(vRst!cod_fornec), "Null", vRst!cod_fornec) & _
                            " where cod_fabrica = '" & vRst!Cod_fabric & _
                            "' and cod_dpk = " & vRst!COD_DPK & _
                            " and fl_tipo = '" & vRst!fl_tipo & "'", vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into R_fabrica_dpk values(" & IIf(IsNull(vRst!Cod_fabric), "Null", "'" & vRst!Cod_fabric & "'") & _
                                ", " & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!fl_tipo), "Null", "'" & vRst!fl_tipo & "'") & _
                                ", " & IIf(IsNull(vRst!dt_cadastr), "Null", "'" & vRst!dt_cadastr & "'") & _
                                ", " & IIf(IsNull(vRst!sigla), "Null", "'" & vRst!sigla & "'") & _
                                ", " & IIf(IsNull(vRst!cod_fornec), "Null", vRst!cod_fornec) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "R_FABRICA_DPK", "INSERT", vRegsIn
    Atualizar_lsv "R_FABRICA_DPK", "UPDATE", vRegsUp
    Atualizar_lsv "R_FABRICA_DPK", "DELETE", vRegsDel
    
    Set vRst = Nothing
    R_FABRICA_DPK = True

Trata_Erro:

    If Err.Number = -2147217900 Or Err.Number = -2147467259 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
      Screen.MousePointer = 1
      R_FABRICA_DPK = False
      MsgBox "Sub R_FABRICA_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_DATAS() As Boolean
  
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
        
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DATAS#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DATAS", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from datas", vRegsDel
        
        connBaseDpk.Execute "Insert into Datas values(" & IIf(IsNull(vRst!dt_real), "Null", "'" & vRst!dt_real & "'") & _
                            ", " & IIf(IsNull(vRst!dt_faturam), "Null", "'" & vRst!dt_faturam & "'") & _
                            ", " & IIf(IsNull(vRst!dt_ini_fec), "Null", "'" & vRst!dt_ini_fec & "'") & _
                            ", " & IIf(IsNull(vRst!dt_fin_fec), "Null", "'" & vRst!dt_fin_fec & "'") & _
                            ", " & IIf(IsNull(vRst!dt_feri_1), "Null", "'" & vRst!dt_feri_1 & "'") & _
                            ", " & IIf(IsNull(vRst!dt_feri_2), "Null", "'" & vRst!dt_feri_2 & "'") & _
                            ", " & IIf(IsNull(vRst!fl_liber), "Null", "'" & vRst!fl_liber & "'") & _
                            ", " & IIf(IsNull(vRst!dt_fora_se), "Null", "'" & vRst!dt_fora_se & "'") & _
                            ", " & IIf(IsNull(vRst!dt_ini_fd), "Null", "'" & vRst!dt_ini_fd & "'") & ")"
        vRst.MoveNext
        vRegsIn = vRegsIn + 1
        ProgressBar -1, 1
    Next
    connBaseDpk.Execute "DELETE DOLAR_DIARIO.* FROM DOLAR_DIARIO INNER JOIN DATAS " & _
               " ON DOLAR_DIARIO.DATA_USS <= DATEADD('M',-3,DATAS.DT_FATURAMENTO)"
    
    Atualizar_lsv "DATAS", "INSERT", vRegsIn
    Atualizar_lsv "DATAS", "DELETE", vRegsDel
    Atualizar_lsv "DATAS", "UPDATE", vRegsDel
    
    Set vRst = Nothing
    ATU_DATAS = True

VerErro:

    If Err.Number <> 0 Then
        ATU_DATAS = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_DATAS - AF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

'************************************************************************************
'A tabela CONTROLE vai ser deletada pelo FIL050IN antes de rodar o FIL260 ou o FIL040
'************************************************************************************
Function ATU_CONTROLE() As Boolean
          
    On Error GoTo VerErro
       
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from CONTROLE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CONTROLE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete from Controle", vRegsDel
        
        connBaseDpk.Execute "Insert into Controle (Nome_tab)values('" & vRst!Nome_tab & "')"
        vRst.MoveNext
        vRegsIn = vRegsIn + 1
        ProgressBar -1, 1
    Next
    Atualizar_lsv "CONTROLE", "INSERT", vRegsIn
    Atualizar_lsv "CONTROLE", "UPDATE", vRegsUp
    Atualizar_lsv "CONTROLE", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_CONTROLE = True

VerErro:
    If Err.Number <> 0 Then
       ATU_CONTROLE = False
       Screen.MousePointer = 1
       MsgBox "Sub ATU_CONTROLE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & "Linha:" & Erl
    End If
End Function

Public Function ATU_VDR_ITEM_PRECO() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from VDR_ITEM#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "VDR_ITEM_PRECO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Update Vdr_item_preco set Preco_venda = " & IIf(IsNull(vRst!preco_ve), "Null", vRst!preco_ve) & _
                            ", Preco_venda_ant = " & IIf(IsNull(vRst!preco_an), "Null", vRst!preco_an) & " where cod_loja = " & vRst!Cod_loja & _
                            " and cod_dpk = " & vRst!COD_DPK, vRegs
        If vRegs = 0 Then
            connBaseDpk.Execute "Insert into Vdr_item_preco values(" & IIf(IsNull(vRst!Cod_loja), "Null", vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!COD_DPK), "Null", vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!preco_ve), "Null", vRst!preco_ve) & _
                                ", " & IIf(IsNull(vRst!preco_an), "Null", vRst!preco_an) & ")"
            vRegsIn = vRegsIn + 1
        Else
            vRegsUp = vRegsUp + 1
        End If
        ProgressBar -1, 1
        vRst.MoveNext
    Next
    Atualizar_lsv "VDR_ITEM_PRECO", "UPDATE", vRegsUp
    Atualizar_lsv "VDR_ITEM_PRECO", "INSERT", vRegsIn
    Atualizar_lsv "VDR_ITEM_PRECO", "DELETE", vRegsDel
    
    Set vRst = Nothing
    ATU_VDR_ITEM_PRECO = True
VerErro:

    If Err.Number <> 0 Then
        ATU_VDR_ITEM_PRECO = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_VDR_ITEM_PRECO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_TAXA() As Boolean
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
    
    connBaseDpk.Execute "Delete from taxa", vRegsDel
    Atualizar_lsv "TAXA", "DELETE", vRegsDel
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from TAXA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "TAXA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Insert into Taxa values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & _
                            ", " & IIf(IsNull(vRst!valor_2), 0, vRst!valor_2) & _
                            ", " & IIf(IsNull(vRst!valor_6), 0, vRst!valor_6) & _
                            ", " & IIf(IsNull(vRst!valor_7), 0, vRst!valor_7) & _
                            ", " & IIf(IsNull(vRst!data_1), "Null", "'" & vRst!data_1 & "'") & _
                            ", " & IIf(IsNull(vRst!data_2), "Null", "'" & vRst!data_2 & "'") & _
                            ", " & IIf(IsNull(vRst!data_3), "Null", "'" & vRst!data_3 & "'") & _
                            ", " & IIf(IsNull(vRst!data_4), "Null", "'" & vRst!data_4 & "'") & _
                            ", " & IIf(IsNull(vRst!data_5), "Null", "'" & vRst!data_5 & "'") & _
                            ", " & IIf(IsNull(vRst!valor_13), 0, vRst!valor_13) & ")"
        vRegsIn = vRegsIn + 1
        ProgressBar -1, 1
        vRst.MoveNext
    Next
    Atualizar_lsv "TAXA", "INSERT", vRegsIn
    Atualizar_lsv "TAXA", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_TAXA = True

VerErro:
    If Err.Number <> 0 Then
        ATU_TAXA = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_TAXA - AS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function CLASS_FISCAL_RED_BASE() As Boolean
          
    On Error GoTo Trata_Erro
            
    Call zeraVariavel
    ProgressBar -1, 0
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from RED_BASE#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FISCAL_RED_BASE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Insert into Class_fiscal_red_base values(" & IIf(IsNull(vRst!cod_uf_ori), "Null", "'" & vRst!cod_uf_ori & "'") & _
                            ", " & IIf(IsNull(vRst!cod_uf_des), "Null", "'" & vRst!cod_uf_des & "'") & _
                            ", " & IIf(IsNull(vRst!Class_Fisc), 0, vRst!Class_Fisc) & ")"
        vRegsIn = vRegsIn + 1
        ProgressBar -1, 1
        vRst.MoveNext
    Next
    Atualizar_lsv "FISCAL_RED_BASE", "INSERT", vRegsIn
    Atualizar_lsv "FISCAL_RED_BASE", "DELETE", vRegsDel
    Atualizar_lsv "FISCAL_RED_BASE", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    CLASS_FISCAL_RED_BASE = True

Trata_Erro:
    If Err.Number <> 0 Then
        Screen.MousePointer = 1
        CLASS_FISCAL_RED_BASE = False
        MsgBox "Sub CLASS_FISCAL_RED_BASE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function


Public Function DEL_CLASS_FISCAL_RED_BASE() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_RBAS#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLASSE_FISCAL", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "Delete * from Class_Fiscal_Red_Base where cod_uf_origem = '" & vRst!cod_uf_ori & _
                                "' and cod_uf_destino = '" & vRst!cod_uf_desc & _
                                "' and class_fiscal = " & vRst!Class_Fisc
            vRst.MoveNext
            vRegsDel = vRegsDel + 1
            ProgressBar -1, 1
        Next
    End If

    Atualizar_lsv "CLASSE_FISCAL", "DELETE", vRegsDel
    Atualizar_lsv "CLASSE_FISCAL", "INSERT", vRegsIn
    Atualizar_lsv "CLASSE_FISCAL", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_CLASS_FISCAL_RED_BASE = True

VerErro:
    If Err.Number <> 0 Then
        DEL_CLASS_FISCAL_RED_BASE = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_CLASS_FISCAL_RED_BASE - LD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_APLICACAO() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_APL#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEL_APLICACAO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from aplicacao where cod_dpk = " & vRst!COD_DPK & _
                            " and cod_categoria = '" & vRst!cod_catego & _
                            "' and cod_montadora = " & vRst!cod_montad
        vRst.MoveNext
        vRegsDel = vRegsDel + 1
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DEL_APLICACAO", "DELETE", vRegsDel
    Atualizar_lsv "DEL_APLICACAO", "INSERT", vRegsIn
    Atualizar_lsv "DEL_APLICACAO", "UPDATE", vRegsUp
    Set vRst = Nothing
    DEL_APLICACAO = True

VerErro:
    If Err.Number <> 0 Then
        DEL_APLICACAO = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_APLICACAO - EE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_CLIENTE() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_CLI#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DELETE_CLI", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from clie_credito where cod_cliente = " & vRst!cod_client
        
        connBaseDpk.Execute "Delete * from clie_endereco where cod_cliente = " & vRst!cod_client
        
        connBaseDpk.Execute "Delete * from duplicata where cod_cliente = " & vRst!cod_client
        
        connBaseDpk.Execute "Delete * from cliente where cod_cliente = " & vRst!cod_client
        vRst.MoveNext
        vRegsDel = vRegsDel + 1
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DELETE_CLI", "DELETE", vRegsDel
    Atualizar_lsv "DELETE_CLI", "INSERT", vRegsIn
    Atualizar_lsv "DELETE_CLI", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_CLIENTE = True

VerErro:
    If Err.Number <> 0 Then
        DEL_CLIENTE = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_CLIENTE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_FORNECEDOR_ESPECIFICO() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_FOES#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DELETE_FORNECEDOR", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from fornecedor_especifico where cod_fornecedor = " & vRst!cod_fornec
    
        vRst.MoveNext
        vRegsDel = vRegsDel + 1
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "DELETE_FORNECEDOR", "DELETE", vRegsDel
    Atualizar_lsv "DELETE_FORNECEDOR", "INSERT", vRegsIn
    Atualizar_lsv "DELETE_FORNECEDOR", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_FORNECEDOR_ESPECIFICO = True

VerErro:
    If Err.Number <> 0 Then
        DEL_FORNECEDOR_ESPECIFICO = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_FORNECEDOR_ESPECIFICO - HE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_PEDNOTA() As Boolean

    'DELETA PEDIDOS (PEDNOTA_VENDA) E ITENS DO PEDIDO (ITPEDNOTA_VENDA)
    'COM DATA DE PEDIDO MAIOR QUE 30 DIAS.
    'MANTEM APENAS OS PEDIDOS COM NF EMITIDAS POR UM PERIODO DE 1 MES.

    On Error GoTo VerErro
    Dim vRsData As Recordset
    Dim data As Date
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_PED#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "PEDIDO_NOTA", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    'PEDIDOS QUE DEVEM SER DELETADOS
    Set vRsData = New ADODB.Recordset
    vRsData.Open "Select dt_faturamento from datas", connBaseDpk, adOpenKeyset, adLockReadOnly
    data = vRsData!dt_faturamento - 30
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from Pednota_venda where cod_loja = " & vRst!Cod_loja & _
                            " and num_pedido = " & vRst!num_pedido & _
                            " and seq_pedido = " & vRst!seq_pedido & _
                            " and dt_pedido < cdate('" & CDate(data) & "')", vRegs
        If vRegs > 0 Then
            connBaseDpk.Execute "Delete * from Pednota_venda where cod_loja = " & vRst!Cod_loja & _
                                " and num_pedido = " & vRst!num_pedido & _
                                " and seq_pedido = " & vRst!seq_pedido
            vRegsDel = vRegsDel + 1
        End If
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "PEDIDO_NOTA", "DELETE", vRegsDel
    Atualizar_lsv "PEDIDO_NOTA", "INSERT", vRegsIn
    Atualizar_lsv "PEDIDO_NOTA", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_PEDNOTA = True

VerErro:
    If Err.Number <> 0 Then
        DEL_PEDNOTA = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_R_CLIE_REPRES() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_R_CL#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "CLIENTE_REPRESENTANTE", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from R_Clie_Repres where cod_cliente = " & vRst!cod_client & " and cod_repres = " & vRst!Cod_Repres
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "CLIENTE_REPRESENTANTE", "DELETE", vRegsDel
    Atualizar_lsv "CLIENTE_REPRESENTANTE", "UPDATE", vRegsUp
    Atualizar_lsv "CLIENTE_REPRESENTANTE", "INSERT", vRegsIn
    
    Set vRst = Nothing
    DEL_R_CLIE_REPRES = True

VerErro:
    If Err.Number <> 0 Then
        DEL_R_CLIE_REPRES = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_R_CLIE_REPRES - JN" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_R_FABRICA_DPK() As Boolean

On Error GoTo VerErro
         
    Call zeraVariavel
    ProgressBar -1, 0
    
    
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_R_FA#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "FABRICA_DPK", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1
    
    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from R_Fabrica_Dpk where cod_fabrica = " & vRst!Cod_fabric & _
                            " and cod_dpk = " & vRst!COD_DPK & _
                            " and fl_tipo = " & vRst!fl_tipo
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    
    Atualizar_lsv "FABRICA_DPK", "DELETE", vRegsDel
    Atualizar_lsv "FABRICA_DPK", "INSERT", vRegsIn
    Atualizar_lsv "FABRICA_DPK", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_R_FABRICA_DPK = True

VerErro:
If Err.Number <> 0 Then
    DEL_R_FABRICA_DPK = False
    Screen.MousePointer = 1
    MsgBox "Sub DEL_R_FABRICA_DPK - LD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
End If
End Function

'Public Function DEL_R_CLIE_REPRES() As Boolean
'    On Error GoTo VerErro
'
'    vRegs = 0
'    vRegsUp = 0
'    ProgressBar -1, 0
'
'
'    Set vRst = New ADODB.Recordset
'    vRst.Open "Select * from DEL_RCLI#DBF", connDBF, adOpenKeyset, adLockOptimistic
'    vRegsRst = vRst.RecordCount
'    ProgressBar vRegsRst, -1
'
'    For i = 1 To vRegsRst
'        connBaseDpk.Execute "Delete * from R_Clie_Repres where cod_cliente = " & vRst!cod_clie
'        vRegsUp = vRegsUp + 1
'        vRst.MoveNext
'        ProgressBar -1, 1
'    Next
'
'    Atualizar_lsv "CLIENTE_REPRES", "DELETE", vRegsUp
'    Set vRst = Nothing
'    DEL_R_CLIE_REPRES = True
'
'
'VerErro:
'    If Err.Number <> 0 Then
'        DEL_R_CLIE_REPRES = False
'        Screen.MousePointer = 1
'        MsgBox "Sub DEL_R_CLIE_REPRES - JN" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
'    End If
'End Function

Public Function DEL_R_DPK_EQUIV() As Boolean
    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0


    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_RDEQ#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "R_DPK_EQUIV", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from R_Dpk_Equiv where cod_dpk = " & vRst!COD_DPK & " and cod_dpk_eq = " & vRst!cod_dpk_eq
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next

    Atualizar_lsv "R_DPK_EQUIV", "DELETE", vRegsDel
    Atualizar_lsv "R_DPK_EQUIV", "INSERT", vRegsIn
    Atualizar_lsv "R_DPK_EQUIV", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_R_DPK_EQUIV = True
          
VerErro:
    If Err.Number <> 0 Then
        DEL_R_DPK_EQUIV = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_R_DPK_EQUIV - HF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_REPR() As Boolean

    On Error GoTo VerErro
          
    Call zeraVariavel
    ProgressBar -1, 0


    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_REP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEL_REPR", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from Representacao where cod_repres = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from R_RepCgc where cod_repres = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from Repr_End_Corresp where cod_repres = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from R_Repven where cod_repres = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from Clie_Credito where cod_repr_vend = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from Clie_endereco where cod_repr_vend = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from Duplicata where cod_repr_vend = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from cliente where cod_repr_vend = " & vRst!Cod_Repres
        connBaseDpk.Execute "Delete * from Representante where cod_repres = " & vRst!Cod_Repres
        
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DEL_REPR", "DELETE", vRegsDel
    Atualizar_lsv "DEL_REPR", "INSERT", vRegsIn
    Atualizar_lsv "DEL_REPR", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_REPR = True

VerErro:
    If Err.Number <> 0 Then
        DEL_REPR = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_REPR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function DEL_TABDESC() As Boolean
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0


    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_TABD#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEL_TABD", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from Tabela_DescPer where Seguencia = " & vRst!seguencia & _
                            " and Tp_tabela = " & vRst!Tp_Tabela & _
                            " and Ocorr_Preco = " & vRst!ocorr_prec & _
                            " and cod_fornecedor = " & vRst!cod_fornec & _
                            " and cod_dpk = " & vRst!COD_DPK & _
                            " and cod_grupo = " & vRst!grupo & _
                            " and cod_subgrupo = " & vRst!cod_subgru
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DEL_TABD", "DELETE", vRegsDel
    Atualizar_lsv "DEL_TABD", "INSERT", vRegsIn
    Atualizar_lsv "DEL_TABD", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_TABDESC = True

VerErro:
    If Err.Number <> 0 Then
        DEL_TABDESC = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_TABDESC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function DEL_R_UF_DEPOSITO() As Boolean
    On Error GoTo VerErro
    
    Call zeraVariavel
    ProgressBar -1, 0
          
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_UFDP#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEL_UF_DEPOSITO", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from R_uf_deposito where cod_uf = " & vRst!Cod_uf & " and cod_loja = " & vRst!Cod_loja
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DEL_UF_DEPOSITO", "DELETE", vRegsDel
    Atualizar_lsv "DEL_UF_DEPOSITO", "INSERT", vRegsIn
    Atualizar_lsv "DEL_UF_DEPOSITO", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_R_UF_DEPOSITO = True

VerErro:
    If Err.Number <> 0 Then
        DEL_R_UF_DEPOSITO = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_R_UF_DEPOSITO - KK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function DEL_VDR_CLIENTE_CATEG_VDR() As Boolean
    On Error GoTo VerErro
    Call zeraVariavel
    ProgressBar -1, 0

    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from DEL_VDR#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "DEL_CLIENTE_CATEG", "RECEBIDO", vRegsRst
    
    ProgressBar vRegsRst, -1

    For i = 1 To vRegsRst
    DoEvents
        connBaseDpk.Execute "Delete * from VDR_CLIENTE_CATEG_VDR where cod_cliente = " & vRst!cod_client
        vRegsDel = vRegsDel + 1
        vRst.MoveNext
        ProgressBar -1, 1
    Next
    Atualizar_lsv "DEL_CLIENTE_CATEG", "DELETE", vRegsDel
    Atualizar_lsv "DEL_CLIENTE_CATEG", "INSERT", vRegsIn
    Atualizar_lsv "DEL_CLIENTE_CATEG", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    DEL_VDR_CLIENTE_CATEG_VDR = True

VerErro:
    If Err.Number <> 0 Then
        DEL_VDR_CLIENTE_CATEG_VDR = False
        Screen.MousePointer = 1
        MsgBox "Sub DEL_VDR_CLIENTE_CATEG_VDR - GX" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ESTAITEM() As Boolean

    On Error GoTo VerErro

    Call zeraVariavel
    ProgressBar -1, 0
       
    Set vRst = New ADODB.Recordset
    vRst.Open "Select * from ESTAT_IT#DBF", connDBF, adOpenKeyset, adLockOptimistic
    vRegsRst = vRst.RecordCount
    Atualizar_lsv "ESTATISTICA_ITEM", "RECEBIDO", vRegsRst
    ProgressBar vRegsRst, -1
    
    If vRst.EOF = False Then
        DoEvents
        connBaseDpk.Execute "Delete * from estatistica_item", vRegsDel
        For i = 1 To vRegsRst
        DoEvents
            connBaseDpk.Execute "Insert into estatistica_item values(" & IIf(IsNull(vRst!Cod_loja), 0, vRst!Cod_loja) & _
                                ", " & IIf(IsNull(vRst!COD_DPK), 0, vRst!COD_DPK) & _
                                ", " & IIf(IsNull(vRst!Qtd_Venda), 0, vRst!Qtd_Venda) & ")"
    
            vRegsIn = vRegsIn + 1
            ProgressBar -1, 1
            vRst.MoveNext
        Next
    End If
    
    Atualizar_lsv "ESTATISTICA_ITEM", "INSERT", vRegsIn
    Atualizar_lsv "ESTATISTICA_ITEM", "DELETE", vRegsDel
    Atualizar_lsv "ESTATISTICA_ITEM", "UPDATE", vRegsUp
    
    Set vRst = Nothing
    ATU_ESTAITEM = True

VerErro:
    If Err.Number <> 0 Then
        ATU_ESTAITEM = False
        Screen.MousePointer = 1
        MsgBox "Sub ATU_ESTAITEM - AI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_PED()

    On Error GoTo Trata_Erro

    '*************************************************************
    'Separa valores dos demais campos da tabela PEDNOTA_VENDA fora
    ' a chave da tabela

    Call SEPARA
    Ped_Loja_Nota = CONTEUDO
    Call SEPARA
    Ped_Num_Nota = CONTEUDO
    Call SEPARA
    Ped_Pen = CONTEUDO
    Call SEPARA
    Ped_Fil = CONTEUDO
    Call SEPARA
    Ped_Tp_Ped = CONTEUDO
    Call SEPARA
    Ped_Tp_DB = CONTEUDO
    Call SEPARA
    Ped_Tp_Tran = CONTEUDO
    Call SEPARA
    Ped_Dt_Dig = CONTEUDO
    Call SEPARA
    Ped_Dt_Ped = CONTEUDO
    Call SEPARA
    Ped_Dt_Emiss = CONTEUDO
    Call SEPARA
    Ped_Dt_Ssm = CONTEUDO
    Call SEPARA
    Ped_Dt_Pol = CONTEUDO
    Call SEPARA
    Ped_Dt_Cre = CONTEUDO
    Call SEPARA
    Ped_Dt_Fre = CONTEUDO
    Call SEPARA
    Ped_Nope = Trim(CONTEUDO)
    Call SEPARA
    Ped_Cli = CONTEUDO
    Call SEPARA
    Ped_For = CONTEUDO
    Call SEPARA
    Ped_Ent = CONTEUDO
    Call SEPARA
    Ped_Cob = CONTEUDO
    Call SEPARA
    Ped_Tran = CONTEUDO
    Call SEPARA
    Ped_Rep = CONTEUDO
    Call SEPARA
    Ped_Ven = CONTEUDO
    Call SEPARA
    Ped_Pla = CONTEUDO
    Call SEPARA
    Ped_Ban = CONTEUDO
    Call SEPARA
    Ped_Dest = CONTEUDO
    Call SEPARA
    Ped_Oper = CONTEUDO
    Call SEPARA
    Ped_Fre = CONTEUDO
    Call SEPARA
    Ped_Pes = CONTEUDO
    Call SEPARA
    Ped_Qtd_Ssm = CONTEUDO
    Call SEPARA
    Ped_Qtd_Ped = CONTEUDO
    Call SEPARA
    Ped_Qtd_Not = CONTEUDO
    Call SEPARA
    Ped_Vl = CONTEUDO
    Call SEPARA
    Ped_Ipi = CONTEUDO
    Call SEPARA
    Ped_Icm1 = CONTEUDO
    Call SEPARA
    Ped_Icm2 = CONTEUDO
    Call SEPARA
    Ped_Bas1 = CONTEUDO
    Call SEPARA
    Ped_Bas2 = CONTEUDO
    Call SEPARA
    Ped_Bas3 = CONTEUDO
    Call SEPARA
    Ped_Base = CONTEUDO
    Call SEPARA
    Ped_Bas5 = CONTEUDO
    Call SEPARA
    Ped_Bas6 = CONTEUDO
    Call SEPARA
    Ped_Out = CONTEUDO
    Call SEPARA
    Ped_Maj = CONTEUDO
    Call SEPARA
    Ped_Ret = CONTEUDO
    Call SEPARA
    Ped_Vl_Ipi = CONTEUDO
    Call SEPARA
    Ped_Is_Ipi = CONTEUDO
    Call SEPARA
    Ped_Ou_Ipi = CONTEUDO
    Call SEPARA
    Ped_Vl_Fre = CONTEUDO
    Call SEPARA
    Ped_Ace = CONTEUDO
    Call SEPARA
    Ped_Des = CONTEUDO
    Call SEPARA
    Ped_Suf = CONTEUDO
    Call SEPARA
    Ped_Acr = CONTEUDO
    Call SEPARA
    Ped_Seg = CONTEUDO
    Call SEPARA
    Ped_Pc_Icm1 = CONTEUDO
    Call SEPARA
    Ped_Pc_Icm2 = CONTEUDO
    Call SEPARA
    Ped_Ali = CONTEUDO
    Call SEPARA
    Ped_Can = CONTEUDO
    Call SEPARA
    Ped_Fl_Ssm = CONTEUDO
    Call SEPARA
    Ped_Fl_Nfis = CONTEUDO
    Call SEPARA
    Ped_Fl_Pen = CONTEUDO
    Call SEPARA
    Ped_Fl_Ace = CONTEUDO
    Call SEPARA
    Ped_Fl_Icm = CONTEUDO
    Call SEPARA
    Ped_Sit = CONTEUDO
    Call SEPARA
    Ped_Pro = CONTEUDO
    Call SEPARA
    Ped_Jur = CONTEUDO
    Call SEPARA
    Ped_Che = CONTEUDO
    Call SEPARA
    Ped_Com = CONTEUDO
    Call SEPARA
    Ped_Dup = CONTEUDO
    Call SEPARA
    Ped_Lim = CONTEUDO
    Call SEPARA
    Ped_Sal = CONTEUDO
    Call SEPARA
    Ped_Cli_Nov = CONTEUDO
    Call SEPARA
    Ped_Desc = CONTEUDO
    Call SEPARA
    Ped_Acre = CONTEUDO
    Call SEPARA
    Ped_VlFat = CONTEUDO
    Call SEPARA
    Ped_Desc1 = CONTEUDO
    Call SEPARA
    Ped_Desc2 = CONTEUDO
    Call SEPARA
    Ped_Desc3 = CONTEUDO
    Call SEPARA
    Ped_Fret = CONTEUDO
    Call SEPARA
    Ped_MensP = CONTEUDO
    Call SEPARA
    Ped_MensN = CONTEUDO
    Call SEPARA
    Ped_CODVDR = CONTEUDO
    '*******************************************
    
    '*******************************************
    'Grava valores lidos na tabela PEDNOTA_VENDA
    'Ped_Num = 517212
    
    Set vRst = New ADODB.Recordset
    strSQL = ""
    strSQL = "SELECT * from PEDNOTA_VENDA "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    strSQL = ""
          
    'Formata��o das datas
    If Ped_Dt_Emiss <> "0" Then
        Ped_Dt_Emiss = Format$(Ped_Dt_Emiss, "dd/mm/yy hh:nn:ss")
    End If
    If Ped_Dt_Ssm <> "0" Then
        Ped_Dt_Ssm = Format$(Ped_Dt_Ssm, "dd/mm/yy hh:nn:ss")
    End If
    If Ped_Dt_Pol <> "0" Then
        Ped_Dt_Pol = Format$(Ped_Dt_Pol, "dd/mm/yy hh:nn:ss")
    End If
    If Ped_Dt_Cre <> "0" Then
        Ped_Dt_Cre = Format$(Ped_Dt_Cre, "dd/mm/yy hh:nn:ss")
    End If
    If Ped_Dt_Fre <> "0" Then
        Ped_Dt_Fre = Format$(Ped_Dt_Fre, "dd/mm/yy hh:nn:ss")
    End If
    
    If vRst.EOF = True Then
        frmFil040.lstCONTXT.AddItem "Inserindo pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        'Se n�o existe o pedido ent�o insere...
        strSQL = "INSERT "
        strSQL = strSQL & " into PEDNOTA_VENDA values "
        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
        strSQL = strSQL & " " & Ped_Loja_Nota & " , " & Ped_Num_Nota & ", "
        strSQL = strSQL & " " & Val(Mid(Ped_Pen, 3, 11)) & ", " & Ped_Fil & " , " & Ped_Tp_Ped & " ,"
        strSQL = strSQL & " " & Ped_Tp_DB & " , " & Ped_Tp_Tran & ", " & IIf(Ped_Dt_Dig = "0", "Null", "'" & Ped_Dt_Dig & "'") & ", "
        strSQL = strSQL & " " & IIf(Ped_Dt_Ped = "0", "Null", "'" & Ped_Dt_Ped & "'") & " , " & IIf(Ped_Dt_Emiss = "0", "Null", "'" & Ped_Dt_Emiss & "'") & " , " & IIf(Ped_Dt_Ssm = "0", "Null", "'" & Ped_Dt_Ssm & "'") & " , "
        strSQL = strSQL & " " & IIf(Ped_Dt_Pol = "0", "Null", "'" & Ped_Dt_Pol & "'") & " , " & IIf(Ped_Dt_Cre = "0", "Null", "'" & Ped_Dt_Cre & "'") & "," & IIf(Ped_Dt_Fre = "0", "Null", "'" & Ped_Dt_Fre & "'") & ","
        strSQL = strSQL & " '" & Ped_Nope & "' , " & Ped_Cli & ", " & Ped_For & " ,"
        strSQL = strSQL & " " & Ped_Ent & " , " & Ped_Cob & ", " & Ped_Tran & ","
        strSQL = strSQL & " " & Ped_Rep & ", " & Ped_Ven & " , " & Ped_Pla & ", "
        strSQL = strSQL & " " & Ped_Ban & ", " & Ped_Dest & ", " & Ped_Oper & " ,"
        strSQL = strSQL & " '" & Ped_Fre & "', " & Ped_Pes & ", " & Ped_Qtd_Ssm & " , "
        strSQL = strSQL & " " & Ped_Qtd_Ped & ", " & Ped_Qtd_Not & ", " & Ped_Vl & " ,"
        strSQL = strSQL & " " & Ped_Ipi & ", " & Ped_Icm1 & ", " & Ped_Icm2 & ", "
        strSQL = strSQL & " " & Ped_Bas1 & ", " & Ped_Bas2 & " , "
        strSQL = strSQL & " " & Ped_Bas3 & " , " & Ped_Base & " , " & Ped_Bas5 & " ,"
        strSQL = strSQL & " " & Ped_Bas6 & " , " & Ped_Out & " , " & Ped_Maj & ","
        strSQL = strSQL & " " & Ped_Ret & ", " & Ped_Vl_Ipi & ", " & Ped_Is_Ipi & " ,"
        strSQL = strSQL & " " & Ped_Ou_Ipi & ", " & Ped_Vl_Fre & " , " & Ped_Ace & " ,"
        strSQL = strSQL & " " & Ped_Des & " , " & Ped_Suf & ", " & Ped_Acr & " ,"
        strSQL = strSQL & " " & Ped_Seg & ", " & Ped_Pc_Icm1 & " , " & Ped_Pc_Icm2 & " , "
        strSQL = strSQL & " " & Ped_Ali & " , " & Ped_Can & " , '" & Ped_Fl_Ssm & "' , "
        strSQL = strSQL & " '" & Ped_Fl_Nfis & "' , '" & Ped_Fl_Pen & "' , '" & Ped_Fl_Ace & "' ,"
        strSQL = strSQL & " '" & Ped_Fl_Icm & "' , " & Ped_Sit & " , '" & Ped_Pro & "' ,"
        strSQL = strSQL & " '" & Ped_Jur & "', '" & Ped_Che & "',  '" & Ped_Com & "', '" & Ped_Dup & "',"
        strSQL = strSQL & " '" & Ped_Lim & "' , '" & Ped_Sal & "' , '" & Ped_Cli_Nov & "', "
        strSQL = strSQL & " '" & Ped_Desc & "', '" & Ped_Acre & "' , '" & Ped_VlFat & "' ,"
        strSQL = strSQL & " '" & Ped_Desc1 & "' , '" & Ped_Desc2 & "' , '" & Ped_Desc3 & "' ,"
        strSQL = strSQL & " '" & Ped_Fret & "' , '" & Ped_MensP & "' , '" & Ped_MensN & "', " & Ped_CODVDR & ")"
        connBaseDpk.Execute strSQL
    Else
        'Se existir o pedido ent�o altera...
        frmFil040.lstCONTXT.AddItem "Alterando pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "UPDATE "
        strSQL = strSQL & " PEDNOTA_VENDA "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " COD_LOJA_NOTA =  " & Ped_Loja_Nota & ", "
        strSQL = strSQL & " NUM_NOTA = " & Ped_Num_Nota & ", "
        strSQL = strSQL & " NUM_PENDENTE = " & Val(Mid(Ped_Pen, 3, 11)) & ", "
        strSQL = strSQL & " COD_FILIAL = " & Ped_Fil & ", "
        strSQL = strSQL & " TP_PEDIDO = " & Ped_Tp_Ped & ","
        strSQL = strSQL & " TP_DPKBLAU = " & Ped_Tp_DB & ","
        strSQL = strSQL & " TP_TRANSACAO = " & Ped_Tp_Tran & ", "
        strSQL = strSQL & " DT_DIGITACAO = " & IIf(Ped_Dt_Dig = "0", "Null", "'" & Ped_Dt_Dig & "'") & ","
        strSQL = strSQL & " DT_PEDIDO = " & IIf(Ped_Dt_Ped = "0", "Null", "'" & Format$(Ped_Dt_Ped, "DD/MM/YY") & "'") & ", "
        strSQL = strSQL & " DT_EMISSAO_NOTA = " & IIf(Ped_Dt_Emiss = "0", "Null", "'" & Ped_Dt_Emiss & "'") & " ,"
        strSQL = strSQL & " DT_SSM = " & IIf(Ped_Dt_Ssm = "0", "Null", "'" & Ped_Dt_Ssm & "'") & ","
        strSQL = strSQL & " DT_BLOQ_POLITICA = " & IIf(Ped_Dt_Pol = "0", "Null", "'" & Ped_Dt_Pol & "'") & ","
        strSQL = strSQL & " DT_BLOQ_CREDITO = " & IIf(Ped_Dt_Cre = "0", "Null", "'" & Ped_Dt_Cre & "'") & ","
        strSQL = strSQL & " DT_BLOQ_FRETE = " & IIf(Ped_Dt_Fre = "0", "Null", "'" & Ped_Dt_Fre & "'") & ","
        strSQL = strSQL & " COD_NOPE= '" & Trim(Ped_Nope) & "',"
        strSQL = strSQL & " COD_CLIENTE = " & Ped_Cli & ","
        strSQL = strSQL & " COD_FORNECEDOR = " & Ped_For & ","
        strSQL = strSQL & " COD_END_ENTREGA = " & Ped_Ent & ","
        strSQL = strSQL & " COD_END_COBRANCA = " & Ped_Cob & ","
        strSQL = strSQL & " COD_TRANSP = " & Ped_Tran & ", "
        strSQL = strSQL & " COD_REPRES = " & Ped_Rep & " ,"
        strSQL = strSQL & " COD_BANCO = " & Ped_Ban & " ,"
        strSQL = strSQL & " COD_VEND = " & Ped_Ven & ", "
        strSQL = strSQL & " COD_PLANO = " & Ped_Pla & ", "
        strSQL = strSQL & " COD_CFO_DEST = " & Ped_Dest & " ,"
        strSQL = strSQL & " COD_CFO_OPER = " & Ped_Oper & ", "
        strSQL = strSQL & " FRETE_PAGO = '" & Ped_Fre & "', "
        strSQL = strSQL & " PESO_BRUTO = " & Ped_Pes & ", "
        strSQL = strSQL & " QTD_SSM = " & Ped_Qtd_Ssm & ","
        strSQL = strSQL & " QTD_ITEM_PEDIDO = " & Ped_Qtd_Ped & ","
        strSQL = strSQL & " QTD_ITEM_NOTA = " & Ped_Qtd_Not & ","
        strSQL = strSQL & " VL_CONTABIL = " & Ped_Vl & ","
        strSQL = strSQL & " VL_IPI = " & Ped_Ipi & ","
        strSQL = strSQL & " VL_BASEICM1 = " & Ped_Icm1 & ","
        strSQL = strSQL & " VL_BASEICM2 = " & Ped_Icm2 & ","
        strSQL = strSQL & " VL_BASE_1 = " & Ped_Bas1 & ", "
        strSQL = strSQL & " VL_BASE_2 = " & Ped_Bas2 & ","
        strSQL = strSQL & " VL_BASE_3 = " & Ped_Bas3 & ","
        strSQL = strSQL & " VL_BASEISEN = " & Ped_Base & ","
        strSQL = strSQL & " VL_BASE_5 = " & Ped_Bas5 & " ,"
        strSQL = strSQL & " VL_BASE_6 = " & Ped_Bas6 & ","
        strSQL = strSQL & " VL_BASEOUTR = " & Ped_Out & " ,"
        strSQL = strSQL & " VL_BASEMAJ = " & Ped_Maj & ","
        strSQL = strSQL & " VL_ICMRETIDO= " & Ped_Ret & " ,"
        strSQL = strSQL & " VL_BASEIPI = " & Ped_Vl_Ipi & " ,"
        strSQL = strSQL & " VL_BISENIPI = " & Ped_Is_Ipi & " ,"
        strSQL = strSQL & " VL_BOUTRIPI = " & Ped_Ou_Ipi & ","
        strSQL = strSQL & " VL_FRETE = " & Ped_Vl_Fre & ","
        strSQL = strSQL & " VL_DESP_ACESS = " & Ped_Ace & " ,"
        strSQL = strSQL & " PC_DESCONTO = " & Ped_Des & " ,"
        strSQL = strSQL & " PC_DESC_SUFRAMA = " & Ped_Suf & " ,"
        strSQL = strSQL & " PC_ACRESCIMO = " & Ped_Acr & " , "
        strSQL = strSQL & " PC_SEGURO = " & Ped_Seg & " ,"
        strSQL = strSQL & " PC_ICM1 = " & Ped_Pc_Icm1 & " ,"
        strSQL = strSQL & " PC_ICM2 = " & Ped_Pc_Icm2 & ","
        strSQL = strSQL & " PC_ALIQ_INTERNA = " & Ped_Ali & " ,"
        strSQL = strSQL & " COD_CANCEL = " & Ped_Can & ","
        strSQL = strSQL & " FL_GER_SSM = '" & Ped_Fl_Ssm & "' ,"
        strSQL = strSQL & " FL_GER_NFIS = '" & Ped_Fl_Nfis & "',"
        strSQL = strSQL & " FL_PENDENCIA = '" & Ped_Fl_Pen & "' ,"
        strSQL = strSQL & " FL_DESP_ACESS = '" & Ped_Fl_Ace & "',"
        strSQL = strSQL & " FL_DIF_ICM = '" & Ped_Fl_Icm & "',"
        strSQL = strSQL & " SITUACAO = " & Ped_Sit & " ,"
        strSQL = strSQL & " BLOQ_PROTESTO = '" & Ped_Pro & "' ,"
        strSQL = strSQL & " BLOQ_JUROS = '" & Ped_Jur & "',"
        strSQL = strSQL & " BLOQ_CHEQUE = '" & Ped_Che & "',"
        strSQL = strSQL & " BLOQ_COMPRA = '" & Ped_Com & "' ,"
        strSQL = strSQL & " BLOQ_DUPLICATA = '" & Ped_Dup & "',"
        strSQL = strSQL & " BLOQ_LIMITE ='" & Ped_Lim & "' ,"
        strSQL = strSQL & " BLOQ_SALDO = '" & Ped_Sal & "',"
        strSQL = strSQL & " BLOQ_CLIE_NOVO = '" & Ped_Cli_Nov & "', "
        strSQL = strSQL & " BLOQ_DESCONTO = '" & Ped_Desc & "', "
        strSQL = strSQL & " BLOQ_ACRESCIMO = '" & Ped_Acre & "', "
        strSQL = strSQL & " BLOQ_VLFATMIN = '" & Ped_VlFat & "', "
        strSQL = strSQL & " BLOQ_ITEM_DESC1 = '" & Ped_Desc1 & "' ,"
        strSQL = strSQL & " BLOQ_ITEM_DESC2 = '" & Ped_Desc2 & "' ,"
        strSQL = strSQL & " BLOQ_ITEM_DESC3 = '" & Ped_Desc3 & "',"
        strSQL = strSQL & " BLOQ_FRETE = '" & Ped_Fret & "',"
        strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' ,"
        strSQL = strSQL & " MENS_NOTA = '" & Ped_MensN & "' ,"
        strSQL = strSQL & " COD_VDR = " & Ped_CODVDR & " "
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
        connBaseDpk.Execute strSQL
    End If
    
    Set vRst = Nothing
    
    'SE HOUVER PEDIDO DE TRANSFER�NCIA
    Set vRst = New ADODB.Recordset
    strSQL = "Select * from R_COTACAO_TRANSF where COD_LOJA_COTACAO = " & Val(Mid(Ped_Pen, 1, 2)) & " AND " & _
    "NUM_COTACAO_TEMP = " & Val(Mid(Ped_Pen, 3, 11))
          
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    Dim ped_cotacao
          
    If Not vRst.EOF Then
        If Ped_Nope = "A30" Then
            ped_cotacao = Mid(Ped_MensN, InStr(Ped_MensN, ":") + 2)
            
            'n�mero do pedido de transfer�ncia
            strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_TRANSF =" & Ped_Loja & ", " & _
                "NUM_PEDIDO =" & Ped_Num & ", SEQ_PEDIDO =" & Ped_Seq & ", " & _
                "NUM_COTACAO = " & ped_cotacao & " where " & _
                "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                connBaseDpk.Execute strSQL
        Else
            'n�mero do pedido de venda
            strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_VENDA =" & Ped_Loja & ", " & _
                "NUM_PEDIDO_VENDA =" & Ped_Num & ", SEQ_PEDIDO_VENDA =" & Ped_Seq & " where " & _
                "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                connBaseDpk.Execute strSQL
        End If
    End If
    Set vRst = Nothing
 
    '*********************************************
Trata_Erro:
    If Err.Number <> 0 Then
        If Err.Number = -2147467259 Then
            Resume Next
        End If
        MsgBox "Sub ATU_PED" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl & "StrSql:" & strSQL
    End If
End Function

Public Function ATU_FLAG()
    'Atualiza Flag do Pedido de Digitacao (DIGITA) ou Telemarketing (VENDAS) quando
    'o e-mail de retorno voltou indicando que ocorreu erro na finaliza��o do pedido.
    'a Flag (FL_PEDIDO_FINALIZADO) voltar� para "N" para que o pedido possa ser
    'enviado novamente para Campinas
    Dim Filial
    On Error GoTo TRATA_ERRO_LOCAL

    strSQL = ""
    

    'DEFINE A FILIAL DO PEDIDO
    Set vRst = New ADODB.Recordset
    vRst.Open "SELECT COD_FILIAL from DEPOSITO", connBaseDpk, adOpenKeyset, adLockOptimistic
    If vRst.EOF = True Then Filial = recTab!Cod_filial Else Filial = 0
    Set vRst = Nothing
    
    Set vRst = New ADODB.Recordset
    strSQL = ""
    strSQL = "SELECT * from PEDNOTA_VENDA "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    
    If vRst.EOF = True Then ' J� existe o Pedido - Atualizar
        strSQL = ""
        strSQL = "UPDATE "
        strSQL = strSQL & " PEDNOTA_VENDA "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " SITUACAO = 0 ,"
        strSQL = strSQL & " FL_GER_NFIS = 'N' ,"
        strSQL = strSQL & " COD_FILIAL = " & Filial & " ,"
        strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' "
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
        connBaseDpk.Execute strSQL
    Else ' N�o Existe o Pedido - Incluir
        strSQL = ""
        strSQL = "INSERT "
        strSQL = strSQL & " into PEDNOTA_VENDA "
        strSQL = strSQL & " (cod_loja, num_pedido, seq_pedido,Mens_pedido,fl_ger_nfis,SITUACAO,COD_FILIAL)"
        strSQL = strSQL & " Values "
        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
        strSQL = strSQL & " ' " & Ped_MensP & " ', 'N',0," & Filial & ")"
        connBaseDpk.Execute strSQL
    End If
    
    frmFil040.lstCONTXT.AddItem "Pedido " & Ped_Num & " Atualizado."
    frmFil040.lstCONTXT.Refresh

    Exit Function

TRATA_ERRO_LOCAL:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_FLAG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
    Resume
End Function

Public Function ATU_ITE()

    '*************************************************************
    'Separa valores dos demais campos da tabela ITPEDNOTA_VENDA fora
    ' a chave da tabela

    On Error GoTo Trata_Erro

    Call SEPARA
    Ite_Num_It = CONTEUDO
    Call SEPARA
    Ite_Loja_Nota = CONTEUDO
    Call SEPARA
    Ite_Num_Nota = CONTEUDO
    Call SEPARA
    Ite_It_Nota = CONTEUDO
    Call SEPARA
    Ite_Dpk = CONTEUDO
    Call SEPARA
    Ite_Sol = CONTEUDO
    Call SEPARA
    Ite_Ate = CONTEUDO
    Call SEPARA
    Ite_Pre = CONTEUDO
    Call SEPARA
    Ite_Tab = CONTEUDO
    Call SEPARA
    Ite_Desc1 = CONTEUDO
    Call SEPARA
    Ite_Desc2 = CONTEUDO
    Call SEPARA
    Ite_Desc3 = CONTEUDO
    Call SEPARA
    Ite_Icm = CONTEUDO
    Call SEPARA
    Ite_Ipi = CONTEUDO
    Call SEPARA
    Ite_Com = CONTEUDO
    Call SEPARA
    Ite_Tlm = CONTEUDO
    Call SEPARA
    Ite_Trib = CONTEUDO
    Call SEPARA
    Ite_Trip = CONTEUDO
    Call SEPARA
    Ite_Sit = CONTEUDO
    
    '*******************************************
       
    '*******************************************
    'Grava valores lidos na tabela PEDNOTA_VENDA
    
    Set vRst = New ADODB.Recordset
    strSQL = "SELECT * from ITPEDNOTA_VENDA "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
    strSQL = strSQL & " num_item_pedido = " & Ite_Num_It
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    
    strSQL = ""
    If vRst.EOF = True Then
        frmFil040.lstCONTXT.AddItem "Inserindo os item(s) do pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "INSERT "
        strSQL = strSQL & " into ITPEDNOTA_VENDA values "
        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
        strSQL = strSQL & " " & Ite_Num_It & "," & Ite_Loja_Nota & " , " & Ite_Num_Nota & ", "
        strSQL = strSQL & " " & Ite_It_Nota & ", " & Ite_Dpk & " , " & Ite_Sol & " ,"
        strSQL = strSQL & " " & Ite_Ate & " , " & Ite_Pre & ", '" & Ite_Tab & "', "
        strSQL = strSQL & " " & Ite_Desc1 & " , " & Ite_Desc2 & " , " & Ite_Desc3 & " , "
        strSQL = strSQL & " " & Ite_Icm & " , " & Ite_Ipi & "," & Ite_Com & ","
        strSQL = strSQL & " " & Ite_Tlm & " , " & Ite_Trib & ", " & Ite_Trip & " ," & Ite_Sit & ")"
        connBaseDpk.Execute strSQL
    Else
        frmFil040.lstCONTXT.AddItem "Alterando pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "UPDATE "
        strSQL = strSQL & " ITPEDNOTA_VENDA "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " COD_LOJA_NOTA =  " & Ite_Loja_Nota & ", "
        strSQL = strSQL & " NUM_NOTA = " & Ite_Num_Nota & ", "
        strSQL = strSQL & " NUM_ITEM_NOTA = " & Ite_It_Nota & ", "
        strSQL = strSQL & " COD_DPK = " & Ite_Dpk & ", "
        strSQL = strSQL & " QTD_SOLICITADA = " & Ite_Sol & ","
        strSQL = strSQL & " QTD_ATENDIDA = " & Ite_Ate & ","
        strSQL = strSQL & " PRECO_UNITARIO = " & Ite_Pre & ", "
        strSQL = strSQL & " TABELA_VENDA = ' " & Trim(Right(Ite_Tab, 4)) & "',"
        strSQL = strSQL & " PC_DESC1 =  " & Ite_Desc1 & " , "
        strSQL = strSQL & " PC_DESC2 =  " & Ite_Desc2 & " ,"
        strSQL = strSQL & " PC_DESC3 =  " & Ite_Desc3 & ","
        strSQL = strSQL & " PC_DIFICM =  " & Ite_Icm & ","
        strSQL = strSQL & " PC_IPI =  " & Ite_Ipi & ","
        strSQL = strSQL & " PC_COMISS =  " & Ite_Com & ","
        strSQL = strSQL & " PC_COMISSTLMK=  " & Ite_Tlm & ","
        strSQL = strSQL & " COD_TRIB = " & Ite_Trib & ","
        strSQL = strSQL & " COD_TRIBIPI = " & Ite_Trip & ","
        strSQL = strSQL & " SITUACAO = " & Ite_Sit
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " and "
        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & Ite_Num_It
        connBaseDpk.Execute strSQL
    End If
    
    Set vRst = Nothing
    
Trata_Erro:
        If Err.Number <> 0 Then
            MsgBox "Sub ATU_ITE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
        End If
End Function

Public Function ATU_V_P()
    On Error GoTo Trata_Erro
    
    '*************************************************************
    'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
    'a chave da tabela
    
    Call SEPARA
    V_P_Ite = CONTEUDO
    Call SEPARA
    V_P_Sit = CONTEUDO
    Call SEPARA
    V_P_Pr = CONTEUDO
    Call SEPARA
    V_P_Vl = CONTEUDO
    Call SEPARA
    V_P_Prs = CONTEUDO
    Call SEPARA
    V_P_Vls = CONTEUDO
    
    '*******************************************
          
    '*******************************************
    'Grava valores lidos na tabela PEDNOTA_VENDA
    Set vRst = New ADODB.Recordset
    strSQL = "SELECT * from V_PEDLIQ_VENDA "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & " AND "
    strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    
    strSQL = ""
    If vRst.EOF = True Then
        frmFil040.lstCONTXT.AddItem "Inserindo valores da venda. Pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "INSERT "
        strSQL = strSQL & " into V_PEDLIQ_VENDA values "
        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
        strSQL = strSQL & " " & V_P_Ite & ", " & V_P_Sit & ", " & V_P_Pr & " , "
        strSQL = strSQL & " " & V_P_Vl & " , " & V_P_Prs & ", " & V_P_Vls & ")"
        connBaseDpk.Execute strSQL
    Else
        frmFil040.lstCONTXT.AddItem "Alterando valores da venda. Pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "UPDATE "
        strSQL = strSQL & " V_PEDLIQ_VENDA "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " SITUACAO = " & V_P_Sit & ", "
        strSQL = strSQL & " PR_LIQUIDO = " & V_P_Pr & ", "
        strSQL = strSQL & " VL_LIQUIDO = " & V_P_Vl & ", "
        strSQL = strSQL & " PR_LIQUIDO_SUFRAMA = " & V_P_Prs & ","
        strSQL = strSQL & " VL_LIQUIDO_SUFRAMA = " & V_P_Vls & ""
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND"
        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
        connBaseDpk.Execute strSQL
    End If
    
    Set vRst = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_V_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_ROM()

    On Error GoTo Trata_Erro
    
    '*************************************************************
    'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
    ' a chave da tabela

    Call SEPARA
    Rom_DtC = CONTEUDO
    Call SEPARA
    Rom_DtD = CONTEUDO
    Call SEPARA
    Rom_Num = CONTEUDO
    Call SEPARA
    Rom_Con = CONTEUDO
    Call SEPARA
    Rom_Car = CONTEUDO
    Call SEPARA
    Rom_Qtd = CONTEUDO
    '*******************************************
          
    '*******************************************
    'Grava valores lidos na tabela PEDNOTA_VENDA
    'Set dbLocal = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)
    Set vRst = New ADODB.Recordset
    strSQL = "SELECT * from ROMANEIO "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    strSQL = ""
    
    If vRst.EOF = True Then
        frmFil040.lstCONTXT.AddItem "Inserindo romaneio do pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "INSERT "
        strSQL = strSQL & " into ROMANEIO values "
        strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
        strSQL = strSQL & " " & IIf(Rom_DtC = "0", "Null", "'" & Rom_DtC & "'") & ", " & IIf(Rom_DtD = "0", "Null", "'" & Rom_DtD & "'") & ", " & Rom_Num & " , "
        strSQL = strSQL & " '" & Rom_Con & "' , '" & Rom_Car & "', " & Rom_Qtd & ")"
        connBaseDpk.Execute strSQL
    Else
        frmFil040.lstCONTXT.AddItem "Atualizando romaneio do pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "UPDATE "
        strSQL = strSQL & " ROMANEIO "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " DT_COLETA =  " & IIf(Rom_DtC = "0", "Null", "'" & Rom_DtC & "'") & ", "
        strSQL = strSQL & " DT_DESPACHO = " & IIf(Rom_DtD = "0", "Null", "'" & Rom_DtD & "'") & ", "
        strSQL = strSQL & " NUM_ROMANEIO = " & Rom_Num & ", "
        strSQL = strSQL & " NUM_CONHECIMENTO = '" & Rom_Con & "', "
        strSQL = strSQL & " NUM_CARRO = '" & Rom_Car & "',"
        strSQL = strSQL & " QTD_VOLUME = " & Rom_Qtd & " "
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
        connBaseDpk.Execute strSQL
    End If
    
    Set vRst = Nothing

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_ROM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function

Public Function ATU_R_P()
    On Error GoTo Trata_Erro
    
    '*************************************************************
    'Separa valores dos demais campos da tabela R_PEDIDO_CONF fora
    ' a chave da tabela
    'mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando R_PEDIDO_CONF do Pedido n�: " & Ped_Num & " - " & Ped_Seq
    
    Call SEPARA
    R_P_Num = CONTEUDO
    Call SEPARA
    R_P_Con = CONTEUDO
    Call SEPARA
    R_P_Emb = CONTEUDO
    Call SEPARA
    R_P_Exp = CONTEUDO
    Call SEPARA
    R_P_Fl = CONTEUDO
    Call SEPARA
    R_P_Qtd = CONTEUDO
    '*******************************************
    
    '*******************************************
    'Grava valores lidos na tabela PEDNOTA_VENDA

    Set vRst = New ADODB.Recordset
    strSQL = "SELECT * from R_PEDIDO_CONF "
    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
    strSQL = strSQL & " NUM_CAIXA =  " & R_P_Num & ""
    vRst.Open strSQL, connBaseDpk, adOpenKeyset, adLockOptimistic
    
    strSQL = ""
    
    If vRst.EOF = True Then
        frmFil040.lstCONTXT.AddItem "Inserindo os item(s) de romaneio do pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
       strSQL = "INSERT "
       strSQL = strSQL & " into R_PEDIDO_CONF values "
       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
       strSQL = strSQL & " " & R_P_Num & ", " & R_P_Con & ", " & R_P_Emb & " , "
       strSQL = strSQL & " " & R_P_Exp & " , '" & R_P_Fl & "', " & R_P_Qtd & ")"
       connBaseDpk.Execute strSQL
    Else
        frmFil040.lstCONTXT.AddItem "Atualizando os item(s) de romaneio do pedido: " & Ped_Num & " /CD: " & Ped_Loja
        frmFil040.lstCONTXT.Refresh
        strSQL = "UPDATE "
        strSQL = strSQL & " R_PEDIDO_CONF "
        strSQL = strSQL & " SET "
        strSQL = strSQL & " COD_CONFERENTE = " & R_P_Con & ", "
        strSQL = strSQL & " COD_EMBALADOR = " & R_P_Emb & ", "
        strSQL = strSQL & " COD_EXPEDIDOR = " & R_P_Exp & ", "
        strSQL = strSQL & " FL_PEND_ETIQ = '" & R_P_Fl & "',"
        strSQL = strSQL & " QTD_VOLUME_PARC = " & R_P_Qtd & ""
        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND "
        strSQL = strSQL & " NUM_CAIXA= " & R_P_Num
        connBaseDpk.Execute strSQL
    End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_R_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
    
End Function

Sub Tamanho_Zero(pAcao As Byte)
          Dim vCatalogo As New ADOX.Catalog
          Dim vTable As New ADOX.Table
          Dim vArq As Integer
          Dim vlinha As String
          
1         On Error GoTo trataerro
          
2         vCatalogo.ActiveConnection = connBaseDpk
          
          'Permitir comprimento Zero
3         If pAcao = 1 Then
4             vArq = FreeFile
5             Open "C:\TAMANHO_ZERO.TXT" For Output As #vArq
              
6             For Each Table In vCatalogo.Tables
7                 Set vTable = vCatalogo.Tables(Table.Name)
8                 For i = 0 To vTable.Columns.Count - 1
9                     If vTable.Columns(i).Type = adChar Or vTable.Columns(i).Type = adVarWChar Then
10                        If vTable.Columns(i).Properties("JET OLEDB:ALLOW ZERO LENGTH").Value = False Then
11                            Print #vArq, vTable.Name & ";" & vTable.Columns(i).Name
12                            vTable.Columns(i).Properties("JET OLEDB:ALLOW ZERO LENGTH").Value = True
13                        End If
14                    End If
15                Next
16            Next
17            Close #vArq
18        Else
              'Nao Permitir comprimento Zero
19            vArq = FreeFile
20            Open "C:\TAMANHO_ZERO.TXT" For Input As #vArq
21            Do While EOF(vArq) = False
22                Input #vArq, vlinha
23                For Each Table In vCatalogo.Tables
24                    Set vTable = vCatalogo.Tables(Table.Name)
25                    If vTable.Name = Mid(vlinha, 1, InStr(1, vlinha, ";") - 1) Then
26                       vTable.Columns(Mid(vlinha, InStr(1, vlinha, ";") + 1)).Properties("JET OLEDB:ALLOW ZERO LENGTH").Value = False
27                       Exit For
28                    End If
29                Next
30            Loop
31            Close #vArq
32            Kill "C:\TAMANHO_ZERO.TXT"
33        End If
          
trataerro:
34        If Err.Number <> 0 Then
35            MsgBox "Sub Tamanho_Zero " & vbCrLf & "Codigo: " & Err.Number & vbCrLf & " Descri��o: " & Err.Description & vbCrLf & "Linha: " & Er1
36        End If
End Sub

Public Function SEPARA()
1         On Error GoTo Trata_Erro

2         POS_SEP = InStr(vArq, "|")

3         CONTEUDO = Left(vArq, POS_SEP - 1)
4         If UCase(CONTEUDO) = "PED" Then
5             vArq = Mid(vArq, POS_SEP + 1)
6             POS_SEP = InStr(vArq, "|")
7             CONTEUDO = Left(vArq, POS_SEP - 1)
8         End If

9         If UCase(CONTEUDO) = "ITE" Then
10            vArq = Mid(vArq, POS_SEP + 1)
11            POS_SEP = InStr(vArq, "|")
12            CONTEUDO = Left(vArq, POS_SEP - 1)
13        End If

14        If UCase(CONTEUDO) = "ROM" Then
15            vArq = Mid(vArq, POS_SEP + 1)
16            POS_SEP = InStr(vArq, "|")
17            CONTEUDO = Left(vArq, POS_SEP - 1)
18        End If

19        If UCase(CONTEUDO) = "V_P" Then
20            vArq = Mid(vArq, POS_SEP + 1)
21            POS_SEP = InStr(vArq, "|")
22            CONTEUDO = Left(vArq, POS_SEP - 1)
23        End If

24        If UCase(CONTEUDO) = "R_P" Then
25            vArq = Mid(vArq, POS_SEP + 1)
26            POS_SEP = InStr(vArq, "|")
27            CONTEUDO = Left(vArq, POS_SEP - 1)
28        End If

29        If CONTEUDO = "" Then
30            CONTEUDO = 0
31        End If

32        vArq = Mid(vArq, POS_SEP + 1)

Trata_Erro:
33        If Err.Number <> 0 Then
34            MsgBox "Sub SEPARA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
35        End If
End Function

Public Function CHAVE_PED()
   'Primeira linha do arquivo guarda a chave da tabela
   'PEDNOTA_VENDA: COD_LOJA + NUM_PEDIDO + SEQ_PEDIDO
    
    Call SEPARA
    Ped_Loja = CONTEUDO
    Call SEPARA
    Ped_Num = CONTEUDO
    Call SEPARA
    Ped_Seq = CONTEUDO

   'MsgBox " pedido: " & Ped_Loja & "  " & Ped_Num & "-" & Ped_Seq
End Function

Public Sub RunCmd(CmdPath As String, Optional WindowStyle As VbAppWinStyle = vbNormalFocus)

   Dim hProcess As Long

   On Error GoTo Err_RunCmd

   hProcess = OpenProcess(SYNCHRONIZE, 0, Shell(CmdPath, WindowStyle))

   If hProcess Then
       WaitForSingleObject hProcess, INFINITE
       CloseHandle hProcess
   End If

   Exit Sub

Err_RunCmd:

   Err.Clear

End Sub


