VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmFil040 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   11910
   Icon            =   "frmFil040.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8625
   ScaleWidth      =   11910
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H8000000A&
      Caption         =   "Verifica Arquivos Txt/Con: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   2355
      Left            =   30
      TabIndex        =   4
      Top             =   150
      Width           =   11805
      Begin VB.ListBox lstCONTXT 
         Appearance      =   0  'Flat
         Height          =   1785
         ItemData        =   "frmFil040.frx":0CCA
         Left            =   90
         List            =   "frmFil040.frx":0CD1
         TabIndex        =   5
         Top             =   390
         Width           =   11595
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      Caption         =   "Atualiza��o dos Dados:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000D&
      Height          =   5115
      Left            =   30
      TabIndex        =   2
      Top             =   2640
      Width           =   11805
      Begin MSComctlLib.ListView lsvDados 
         Height          =   4605
         Left            =   150
         TabIndex        =   3
         Top             =   390
         Width           =   11565
         _ExtentX        =   20399
         _ExtentY        =   8123
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome da Tabela"
            Object.Width           =   4074
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Dados Recebidos"
            Object.Width           =   2573
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Dados Atualizados"
            Object.Width           =   2749
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Dados Inseridos"
            Object.Width           =   2749
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Dados Deletados"
            Object.Width           =   2749
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Tipo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "DBF"
            Object.Width           =   3226
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar SBPanel 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   8250
      Width           =   11910
      _ExtentX        =   21008
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   20946
            MinWidth        =   2716
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar PB 
      Height          =   375
      Left            =   30
      TabIndex        =   1
      Top             =   7800
      Width           =   11835
      _ExtentX        =   20876
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Scrolling       =   1
   End
End
Attribute VB_Name = "frmFil040"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim numArq As Integer
Dim rs As ADODB.Recordset
Dim vfso As New FileSystemObject

Sub geraIndices()
    Dim adoCat As ADOX.Catalog
    Dim adoTbl As ADOX.Table
    Dim adoIdx As ADOX.Index

    Dim i As Integer
    Dim ii As Integer
    On Error GoTo Trata_Erro
    
    
    Set adoCat = New ADOX.Catalog
    adoCat.ActiveConnection = connBaseDpk

    For Each adoTbl In adoCat.Tables
        For Each adoIdx In adoTbl.Indexes
            If UCase(Left(adoTbl.Name, 4)) <> "MSYS" Then
                DoEvents
                'Debug.Print "Nome da Tabela : " & adoTbl.Name & " - Indice : " & adoIdx.Name
                SBPanel.Panels(1).Text = "Deletando indices da tabela: " & adoTbl.Name
                If adoIdx.PrimaryKey = True Then
                    adoTbl.Keys.Delete 0
                Else
                    adoTbl.Indexes.Delete adoIdx.Name
                End If
            End If
'            Debug.Print "Nome da Tabela : " & adoTbl.Name & " - Chave primaria " & adoIdx.Name
        Next
    Next
    
    
    Set vRst = New ADODB.Recordset
    Me.SBPanel.Panels(1) = "Criando Indices e Chaves Primarias..."
    vRst.Open "SELECT * From INDICES", connBaseDpk, adOpenKeyset, adLockReadOnly
    For i = 1 To vRst.RecordCount
        SBPanel.Panels(1).Text = "Criando indices da tabela: " & vRst!nome_tabela
        If vRst!nome_indice <> "" Then
            'connBaseDpk.Execute "DROP INDEX " & vRst!nome_indice & " on " & vRst!nome_tabela
            connBaseDpk.Execute "CREATE INDEX " & vRst!nome_indice & " ON " & vRst!nome_tabela & " (" & vRst!CAMPOS_INDICE & ")"
        End If
        DoEvents
        If vRst!NOME_PK <> "" Then
            'connBaseDpk.Execute "ALTER TABLE " & vRst!nome_tabela & " DROP CONSTRAINT " & vRst!NOME_PK
            connBaseDpk.Execute "CREATE INDEX " & vRst!NOME_PK & " ON " & vRst!nome_tabela & " (" & vRst!campos_pk & ") WITH PRIMARY"
        End If
        vRst.MoveNext
    Next
    
Trata_Erro:
    If Err.Number = -2147467259 Or -2147217887 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub Form_Load()
          
1         On Error GoTo Trata_Erro
          
          Dim X As Integer
          Dim xx As Integer
          Dim vNomeView As String
          Dim vNome As String
          Dim hSysMenu As Long
          Dim nCnt As Long
          Dim vResp As Integer
          Dim ii As Integer
          Dim vData As String
          Dim vRst1 As Recordset
          
2         vData = Now
          
3         If App.PrevInstance = True Then
4            MsgBox "J� existe uma inst�ncia deste programa aberta."
5            End
6         End If
          
7         Me.Visible = True
8         Me.Caption = " FIL045 - PAAC/Representante - Atualiza��o de Dados - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision


          'Desabilita bot�o fechar do formulario
9         hSysMenu = GetSystemMenu(Me.hwnd, False)
10        If hSysMenu Then
11            nCnt = GetMenuItemCount(hSysMenu)
12            If nCnt Then
13                RemoveMenu hSysMenu, nCnt - 1, MF_BYPOSITION Or MF_REMOVE
14                RemoveMenu hSysMenu, nCnt - 2, MF_BYPOSITION Or MF_REMOVE
15                DrawMenuBar Me.hwnd
16            End If
17        End If
                  
                  
'18        Path_drv = Pegar_Drive
18        Path_drv = "C:"
19        Call conectaBaseDPK(Path_drv)
20        Call conectaBaseDBF(Path_drv)
         
          'Cria os indices diariamente.
21        Set vRst = New ADODB.Recordset
22        vRst.Open "Select * From PARAMETROS WHERE NOME_SOFTWARE  = 'FIL040'", connBaseDpk, adOpenKeyset, adLockReadOnly
23        If vRst.EOF = False Then
24            If vRst!VL_PARAMETRO <> Format(vData, "DD/MM/YYYY") Then
25                connBaseDpk.Execute ("UPDATE PARAMETROS SET VL_PARAMETRO = " & Format(vData, "DD/MM/YYYY") & " WHERE NOME_SOFTWARE = 'FIL040' AND NOME_PARAMETRO = 'INDICES'")
26                'Call geraIndices
27            End If
28        Else
29            connBaseDpk.Execute ("INSERT INTO PARAMETROS (NOME_SOFTWARE, NOME_PARAMETRO, VL_PARAMETRO) VALUES('FIL040', 'INDICES', " & vData & ")")
30            'Call geraIndices
31        End If
          Set vRst = Nothing
          
32        Preencher_Lsv
33        lsvDados.SortOrder = lvwAscending
34        lsvDados.Sorted = True
          
35        Me.Refresh
36        Call ArqTXTCON
          
37        Me.Refresh
38        Call Atualizar

39        Me.SBPanel.Panels(1) = "Atualizando tabela controle..."
40        Set vRst = New ADODB.Recordset
41        vRst.Open "SELECT NOME_TAB From CONTROLE ORDER BY CONTROLE.NOME_TAB;", connBaseDpk, adOpenKeyset, adLockReadOnly

      '    For x = 1 To vRst.RecordCount
      '        vNome = vRst!Nome_tab
      '        For xx = 1 To lsvDados.ListItems.Count - 1
      '            vNomeView = Mid(lsvDados.ListItems(xx).SubItems(6), 1, InStrRev(lsvDados.ListItems(xx).SubItems(6), ".") - 1)
      '            If vNomeView = vNome Then
      '                Me.SBPanel.Panels(1) = "Atualizando " & vNomeView & " na tabela controle."
      '                connBaseDpk.Execute "Update Controle set Reg_Rec = " & Val(lsvDados.ListItems(xx).SubItems(1)) & _
      '                                    ", Reg_Atu = " & Val(lsvDados.ListItems(xx).SubItems(2)) & _
      '                                    ", Reg_Ins = " & Val(lsvDados.ListItems(xx).SubItems(3)) & _
      '                                    ", Reg_Del = " & Val(lsvDados.ListItems(xx).SubItems(4)) & _
      '                                    ", Acao = '" & lsvDados.ListItems(xx).SubItems(5) & _
      '                                    "' where Nome_tab = '" & vNome & "'"
      '                Exit For
      '            End If
      '        Next
      '        vRst.MoveNext
      '    Next
42        Me.SBPanel.Panels(1) = "Registros atualizados com sucesso..."
43        vResp = MsgBox("Deseja fazer a otimiza��o do banco de dados agora?", vbYesNo)
44        If vResp = 6 Then
45            RunCmd Path_drv & "\PGMS\FIL046.exe", 1
46        End If
47        Unload Me
          
Trata_Erro:
48        If Err.Number = -2147467259 Or -2147217887 Then
49            Resume Next
50        ElseIf Err.Number <> 0 Then
51            MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
52        End If
          
End Sub


Sub ArqTXTCON()
    Dim vNomeArq As String
    Dim vlinhaTemp As String
    lstCONTXT.Clear
    lstCONTXT.AddItem "Aguarde verificando consultas de pedidos..."
    lstCONTXT.Refresh
    lstCONTXT.AddItem ""
    
    While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> ""
        vNomeArq = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON")
        lstCONTXT.AddItem "Consultando pedido:" & vNomeArq
        vNomeArq = Path_drv & "\COM\ENTRADA\PEDIDOS\" & vNomeArq
        Open vNomeArq For Input As #1
        Do While Not EOF(1)
            Line Input #1, vArq
            vIntArq = InStr(vArq, "|")
            If vIntArq <> 0 Then
                Select Case UCase(Mid(vArq, 1, vIntArq - 1))
                    Case "PED":
                        Call ATU_PED
                    Case "ITE":
                        Call ATU_ITE
                    Case "V_P":
                        Call ATU_V_P
                    Case "ROM":
                        Call ATU_ROM
                    Case "R_P":
                        Call ATU_R_P
                    Case Else:
                        Call CHAVE_PED
                End Select
            Else
                vArq = vArq & vbCrLf
            End If
        Loop
        Close #1
        Kill vNomeArq
    Wend
    
    vNomeArq = ""
    
    lstCONTXT.AddItem ""
    lstCONTXT.Refresh
    While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
        vNomeArq = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT")
        lstCONTXT.AddItem "Consultando pedido:" & vNomeArq
        lstCONTXT.Refresh
        vNomeArq = Path_drv & "\COM\ENTRADA\PEDIDOS\" & vNomeArq
        Open vNomeArq For Input As 1#
        Do While Not EOF(1)
            Line Input #1, vlinhaTemp
            POS_SEP = InStr(vlinhaTemp, "|")
            Ped_Loja = Left(vlinhaTemp, POS_SEP - 1)
            vlinhaTemp = Mid(vlinhaTemp, POS_SEP + 1)
            POS_SEP = InStr(vlinhaTemp, "|")
            Ped_Num = Left(vlinhaTemp, POS_SEP - 1)
            vlinhaTemp = Mid(vlinhaTemp, POS_SEP + 1)
            POS_SEP = InStr(vlinhaTemp, "|")
            Ped_Seq = Left(vlinhaTemp, POS_SEP - 1)
            vlinhaTemp = Mid(vlinhaTemp, POS_SEP + 1)
            Ped_MensP = vlinhaTemp
            Call ATU_FLAG
        Loop
        Close 1#
        Kill vNomeArq
    Wend
    
    While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.HDR") <> ""
        Kill Path_drv & "\COM\ENTRADA\PEDIDOS\*.HDR"
    Wend
    
    lstCONTXT.AddItem ""
    lstCONTXT.AddItem "Consulta de pedidos finalizada."
    lstCONTXT.Selected(lstCONTXT.ListCount - 1) = True
    lstCONTXT.Refresh
End Sub

Sub Atualizar()
    Dim vPasta As Scripting.Folder
    Dim vArq
    Dim vNome As String
    
    Call Tamanho_Zero(1)
    Set vPasta = vfso.GetFolder(Path_drv & "\COM\Entrada\Arquivos\")
    If vPasta.Files.Count > 0 Then
        For Each vArq In vPasta.Files
            If UCase(Right(vArq.Name, 4)) = ".DBF" Then
                vNomeArqDBF = UCase(vArq.Name)
                Select Case UCase(vArq.Name)
                    Case "ALIQUOTA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ALIQUOTA.DBF por favor aguarde..."
                        If ATU_ALIQUOTA_ME = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ALIQUOTA.DBF atualizada com sucesso."
                    Case "ANTECIPA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ANTECIPA.DBF por favor aguarde..."
                        If ATU_ANTECIPACAO_TRIBUTARIA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ANTECIPA.DBF atualizada com sucesso."
                    Case "APLICACA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela APLICACA.DBF por favor aguarde..."
                        If ATU_APLICACAO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro APLICACA.DBF atualizada com sucesso."
                    Case "BANCO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela BANCO.DBF por favor aguarde..."
                        If ATU_BANCO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro BANCO.DBF atualizada com sucesso."
                    Case "CANCEL_P.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CANCEL_P.DBF por favor aguarde..."
                        If ATU_CANCEL_PEDNOTA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CANCEL_P.DBF atualizada com sucesso."
                    Case "CATEG_S.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CATEG_S.DBF por favor aguarde..."
                        If ATU_CATEGSINAL = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CATEG_S.DBF atualizada com sucesso."
                    Case "CESTAI.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CESTAI.DBF por favor aguarde..."
                        If ATU_CESTAITEM = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CESTAI.DBF atualizada com sucesso."
                    Case "ITEM_CUS.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_CUS.DBF por favor aguarde..."
                        If ATU_ITEMCUSTO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_CUS.DBF atualizada com sucesso."
                    Case "CESTAV.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CESTAV.DBF por favor aguarde..."
                        If ATU_CESTAVENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CESTAV.DBF atualizada com sucesso."
                    Case "CIDADE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CIDADE.DBF por favor aguarde..."
                        If ATU_CIDADE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CIDADE.DBF atualizada com sucesso."
                    Case "CLASSANT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLASSANT.DBF por favor aguarde..."
                        If ATU_CLASS_ANTEC_ENTRADA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLASSANT.DBF atualizada com sucesso."
                    Case "CLIE_CRE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIE_CRE.DBF por favor aguarde..."
                        If ATU_CLIECRED = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIE_CRE.DBF atualizada com sucesso."
                    Case "CLIE_END.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIE_END.DBF por favor aguarde..."
                        If ATU_CLIEEND = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIE_END.DBF atualizada com sucesso."
                    Case "CLIE_MEN.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIE_MEN.DBF por favor aguarde..."
                        If ATU_CLIEMSG = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIE_MEN.DBF atualizada com sucesso."
                    Case "CLIENTE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIENTE.DBF por favor aguarde..."
                        If ATU_CLIENTE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIENTE.DBF atualizada com sucesso."
                    Case "CLIEACUM.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIEACUM.DBF por favor aguarde..."
                        If ATU_CLIENTE_ACUMULADO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIEACUM.DBF atualizada com sucesso."
                    Case "CLIE_CAR.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIE_CAR.DBF por favor aguarde..."
                        If ATU_CLIENTE_CARACTERISTICA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIE_CAR.DBF atualizada com sucesso."
                    Case "CLIE_INT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CLIE_INT.DBF por favor aguarde..."
                        If ATU_CLIEINT = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CLIE_INT.DBF atualizada com sucesso."
                    Case "DATAS.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DATAS.DBF por favor aguarde..."
                        If ATU_DATAS = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DATAS.DBF atualizada com sucesso."
                    Case "DEPOSITO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEPOSITO.DBF por favor aguarde..."
                        If ATU_DEPOSITO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEPOSITO.DBF atualizada com sucesso."
                    Case "DEP_VISA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEP_VISA.DBF por favor aguarde..."
                        If ATU_DEPOSITO_VISAO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEP_VISA.DBF atualizada com sucesso."
                    Case "DOLAR_DI.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DOLAR_DI.DBF por favor aguarde..."
                        If ATU_DOLARDIARIO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DOLAR_DI.DBF atualizada com sucesso."
                    Case "DUPLICAT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DUPLICAT.DBF por favor aguarde..."
                        If ATU_DUPLICATA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DUPLICAT.DBF atualizada com sucesso."
                    Case "EMBALAD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela EMBALAD.DBF por favor aguarde..."
                        If ATU_EMBALADOR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro EMBALAD.DBF atualizada com sucesso."
                    Case "FILIAL.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FILIAL.DBF por favor aguarde..."
                        If ATU_FILIAL = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FILIAL.DBF atualizada com sucesso."
                    Case "FIL_FRA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FIL_FRA.DBF por favor aguarde..."
                        If ATU_FILIAL_FRANQUIA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FIL_FRA.DBF atualizada com sucesso."
                    Case "FORNECED.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FORNECED.DBF por favor aguarde..."
                        If ATU_FORNECEDOR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FORNECED.DBF atualizada com sucesso."
                    Case "FORN_ESP.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FORN_ESP.DBF por favor aguarde..."
                        If ATU_FORNECEDOR_ESPECIFICO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FORN_ESP.DBF atualizada com sucesso."
                    Case "FRETE_EN.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FRETE_EN.DBF por favor aguarde..."
                        If ATU_FRETE_ENTREGA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FRETE_EN.DBF atualizada com sucesso."
                    Case "FRETE_UF.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FRETE_UF.DBF por favor aguarde..."
                        If ATU_FRETE_UF = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FRETE_UF.DBF atualizada com sucesso."
                    Case "FRETEUFB.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FRETEUFB.DBF por favor aguarde..."
                        If ATU_FRETE_UF_BLOQ = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FRETEUFB.DBF atualizada com sucesso."
                    Case "FRETEUFT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela FRETEUFT.DBF por favor aguarde..."
                        If ATU_FRETE_UF_TRANSP = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro FRETEUFT.DBF atualizada com sucesso."
                    Case "GRUPO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela GRUPO.DBF por favor aguarde..."
                        If ATU_GRUPO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro GRUPO.DBF atualizada com sucesso."
                    Case "ITEM_ANA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_ANA.DBF por favor aguarde..."
                        If ATU_ITEMANAL = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_ANA.DBF atualizada com sucesso."
                    Case "ITEM_CAD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_CAD.DBF por favor aguarde..."
                        If ATU_ITEMCAD = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_CAD.DBF atualizada com sucesso."
                    Case "ITEM_EST.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_EST.DBF por favor aguarde..."
                        If ATU_ITEMESTOQUE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_EST.DBF atualizada com sucesso."
                    Case "ITEM_GLO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_GLO.DBF por favor aguarde..."
                        If ATU_ITEMGLOBAL = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_GLO.DBF atualizada com sucesso."
                    Case "ITEM_PRE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITEM_PRE.DBF por favor aguarde..."
                        If ATU_ITEMPRECO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITEM_PRE.DBF atualizada com sucesso."
                    Case "ITPEDNOT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ITPEDNOT.DBF por favor aguarde..."
                        If ATU_ITPEDNOTA_VENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ITPEDNOT.DBF atualizada com sucesso."
                    Case "LOJA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela LOJA.DBF por favor aguarde..."
                        If ATU_LOJA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro LOJA.DBF atualizada com sucesso."
                    Case "MONTADOR.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela MONTADOR.DBF por favor aguarde..."
                        If ATU_MONTADORA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro MONTADOR.DBF atualizada com sucesso."
                    Case "NATU_OPE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela NATU_OPE.DBF por favor aguarde..."
                        If ATU_NAT_OPERACAO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro NATU_OPE.DBF atualizada com sucesso."
                    Case "PEDNOTA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela PEDNOTA.DBF por favor aguarde..."
                        If ATU_PEDNOTA_VENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro PEDNOTA.DBF atualizada com sucesso."
                    Case "PLANO_PG.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela PLANO_PG.DBF por favor aguarde..."
                        If ATU_PLANO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro PLANO_PG.DBF atualizada com sucesso."
                    Case "R_CLIE_R.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_CLIE_R.DBF por favor aguarde..."
                        If ATU_R_CLIE_REPRES = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_CLIE_R.DBF atualizada com sucesso."
                    Case "R_DPK_EQ.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_DPK_EQ.DBF por favor aguarde..."
                        If ATU_R_DPK_EQUIV = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_DPK_EQ.DBF atualizada com sucesso."
                    Case "R_FABRIC.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_FABRIC.DBF por favor aguarde..."
                        If R_FABRICA_DPK = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_FABRIC.DBF atualizada com sucesso."
                    Case "R_FILDEP.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_FILDEP.DBF por favor aguarde..."
                        If ATU_R_FILDEP = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_FILDEP.DBF atualizada com sucesso."
                    Case "R_PED_CO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_PED_CO.DBF por favor aguarde..."
                        If ATU_R_PEDIDO_CONF = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_PED_CO.DBF atualizada com sucesso."
                    Case "R_REPCGC.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_REPCGC.DBF por favor aguarde..."
                        If ATU_REPCGC = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_REPCGC.DBF atualizada com sucesso."
                    Case "R_REPVEN.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_REPVEN.DBF por favor aguarde..."
                        If ATU_REPVEN = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_REPVEN.DBF atualizada com sucesso."
                    Case "R_UF_DEP.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela R_UF_DEP.DBF por favor aguarde..."
                        If ATU_R_UF_DEPOSITO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro R_UF_DEP.DBF atualizada com sucesso."
                    Case "REP_ENDC.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela REP_ENDC.DBF por favor aguarde..."
                        If ATU_REPEND = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro REP_ENDC.DBF atualizada com sucesso."
                    Case "REPRES.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela REPRES.DBF por favor aguarde..."
                        If ATU_REPRESENTANTE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro REPRES.DBF atualizada com sucesso."
                    Case "REPRESEN.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela REPRESEN.DBF por favor aguarde..."
                        If ATU_REPRESENTACAO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro REPRESEN.DBF atualizada com sucesso."
                    Case "RESULTAD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela RESULTAD.DBF por favor aguarde..."
                        If ATU_RESULTADO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro RESULTAD.DBF atualizada com sucesso."
                    Case "ROMANEIO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ROMANEIO.DBF por favor aguarde..."
                        If ATU_ROMANEIO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ROMANEIO.DBF atualizada com sucesso."
                    Case "SALDO_PE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela SALDO_PE.DBF por favor aguarde..."
                        If ATU_SALDO_PEDIDO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro SALDO_PE.DBF atualizada com sucesso."
                    Case "SUBGRUPO.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela SUBGRUPO.DBF por favor aguarde..."
                        If ATU_SUBGRUPO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro SUBGRUPO.DBF atualizada com sucesso."
                    Case "SUBST_TR.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela SUBST_TR.DBF por favor aguarde..."
                        If ATU_SUBTRIB = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro SUBST_TR.DBF atualizada com sucesso."
                    Case "TABELAD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela TABELAD.DBF por favor aguarde..."
                        If ATU_TABDESCPER = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TABELAD.DBF atualizada com sucesso."
                    Case "TABELAV.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela TABELAV.DBF por favor aguarde..."
                        If ATU_TABVENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TABELAV.DBF atualizada com sucesso."
                    Case "TAXA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela TAXA.DBF por favor aguarde..."
                         If ATU_TAXA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TAXA.DBF atualizada com sucesso."
                    Case "TIPO_CLI.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela TIPO_CLI.DBF por favor aguarde..."
                        If ATU_TPCLIE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TIPO_CLI.DBF atualizada com sucesso."
                    Case "CONTROLE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela CONTROLE.DBF por favor aguarde..."
                        If ATU_CONTROLE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro CONTROLE.DBF atualizada com sucesso."
                    Case "TP_BLAU.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela TP_BLAU.DBF por favor aguarde..."
                        If ATU_TIPO_CLIENTE_BLAU = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TP_BLAU.DBF atualizada com sucesso."
                    Case "TRANSPOR.DBF"
                    Me.SBPanel.Panels(1) = "Atualizando registro da tabela TRANSPOR.DBF por favor aguarde..."
                        If ATU_TRANSPORTADORA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro TRANSPOR.DBF atualizada com sucesso."
                    Case "UF.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF.DBF por favor aguarde..."
                        If ATU_UF = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF.DBF atualizada com sucesso."
                    Case "UF_CATEG.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF_CATEG.DBF por favor aguarde..."
                        If ATU_UF_CATEG = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF_CATEG.DBF atualizada com sucesso."
                    Case "UF_DEP.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF_DEP.DBF por favor aguarde..."
                        If ATU_UF_DEPOSITO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF_DEP.DBF atualizada com sucesso."
                    Case "UF_DPK.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF_DPK.DBF por favor aguarde..."
                        If ATU_UFDPK = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF_DPK.DBF atualizada com sucesso."
                    Case "UF_OR_DE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF_OR_DE.DBF por favor aguarde..."
                        If ATU_UF_ORIGDEST = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF_OR_DE.DBF atualizada com sucesso."
                    Case "UF_TPCLI.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela UF_TPCLI.DBF por favor aguarde..."
                        If ATU_UF_TPCLIENTE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro UF_TPCLI.DBF atualizada com sucesso."
                    Case "VCLIFIEL.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VCLIFIEL.DBF por favor aguarde..."
                        If ATU_V_CLIENTE_FIEL = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VCLIFIEL.DBF atualizada com sucesso."
                    Case "VDR_CLIE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_CLIE.DBF por favor aguarde..."
                        If ATU_VDR_CLIENTE_CATEG_VDR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VDR_CLIE.DBF atualizada com sucesso."
                    Case "VDR_CONT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_CONT.DBF por favor aguarde..."
                        If ATU_VDR_CONTROLE_VDR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VDR_CONT.DBF atualizada com sucesso."
                    Case "VDR_DESC.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_DESC.DBF por favor aguarde..."
                        If ATU_VDR_DESCONTO_CATEG = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VDR_DESC.DBF atualizada com sucesso."
                    Case "VDR_ITEM.DBF"
                    Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_ITEM.DBF por favor aguarde..."
                        If ATU_VDR_ITEM_PRECO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                    Case "V_PEDLIQ.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela V_PEDLIQ.DBF por favor aguarde..."
                        If ATU_V_PEDLIQ_VENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro V_PEDLIQ.DBF atualizada com sucesso."
                    Case "VDR_RCLI.DBF"
                    Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_RCLI.DBF por favor aguarde..."
                        If ATU_VDR_R_CLIE_LINHA_PRODUTO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                    Case "VDR_TABE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VDR_TABE.DBF por favor aguarde..."
                        If ATU_VDR_TABELA_VENDA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VDR_TABE.DBF atualizada com sucesso."
                    Case "VLIMITAD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela VLIMITAD.DBF por favor aguarde..."
                        If ATU_VENDA_LIMITADA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro VLIMITAD.DBF atualizada com sucesso."
                    Case "ESTAT_IT.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela ESTAT_IT.DBF por favor aguarde..."
                        If ATU_ESTAITEM = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro ESTAT_IT.DBF atualizada com sucesso."
                    Case "RED_BASE.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela RED_BASE.DBF por favor aguarde..."
                        If CLASS_FISCAL_RED_BASE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro RED_BASE.DBF atualizada com sucesso."
                        
                    'DBF's de Dele��o
                    Case "DEL_RBAS.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_RBAS.DBF por favor aguarde..."
                        If DEL_CLASS_FISCAL_RED_BASE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_RBAS.DBF atualizada com sucesso."
                    Case "DEL_APL.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_APL.DBF por favor aguarde..."
                        If DEL_APLICACAO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_APL.DBF atualizada com sucesso."
                    Case "DEL_CLI.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_CLI.DBF por favor aguarde..."
                        If DEL_CLIENTE = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_CLI.DBF atualizada com sucesso."
                    Case "DEL_FOES.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_FOES.DBF por favor aguarde..."
                        If DEL_FORNECEDOR_ESPECIFICO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_FOES.DBF atualizada com sucesso."
                    Case "DEL_PED.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_PED.DBF por favor aguarde..."
                        If DEL_PEDNOTA = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_PED.DBF atualizada com sucesso."
                    Case "DEL_R_CL.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_R_CL.DBF por favor aguarde..."
                        If DEL_R_CLIE_REPRES = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_R_CL.DBF atualizada com sucesso."
                    Case "DEL_R_FA.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_R_FA.DBF por favor aguarde..."
                        If DEL_R_FABRICA_DPK = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_R_FA.DBF atualizada com sucesso."
                    Case "DEL_RCLI.DBF"
                        'If DEL_R_CLIE_REPRES = True Then
                        '    'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                        '    Kill vPasta & "\" & vNomeArqDBF
                        'End If
                    Case "DEL_RDEQ.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_RDEQ.DBF por favor aguarde..."
                        If DEL_R_DPK_EQUIV = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_RDEQ.DBF atualizada com sucesso."
                    Case "DEL_REP.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_REP.DBF por favor aguarde..."
                        If DEL_REPR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_REP.DBF atualizada com sucesso."
                    Case "DEL_TABD.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_TABD.DBF por favor aguarde..."
                        If DEL_TABDESC = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_TABD.DBF atualizada com sucesso."
                    Case "DEL_UFDP.DBF"
                    Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_UFDP.DBF por favor aguarde..."
                        If DEL_R_UF_DEPOSITO = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_UFDP.DBF atualizada com sucesso."
                    Case "DEL_VDR.DBF"
                        Me.SBPanel.Panels(1) = "Atualizando registro da tabela DEL_VDR.DBF por favor aguarde..."
                        If DEL_VDR_CLIENTE_CATEG_VDR = True Then
                            'FileCopy vPasta & "\" & vNomeArqDBF, vPasta & "\ArqOk\" & vNomeArqDBF
                            Kill vPasta & "\" & vNomeArqDBF
                        End If
                        Me.SBPanel.Panels(1) = "Registro DEL_VDR.DBF atualizada com sucesso."
                End Select
            End If
        Next
    End If
    Call Tamanho_Zero(2)
End Sub

Sub Preencher_Lsv()
    Dim lItem As ListItem
1         On Error GoTo Trata_Erro
          
2         Set lItem = lsvDados.ListItems.Add
3         lItem = "Aliquota_me"
4         lItem.SubItems(5) = "D/I"
5         lItem.SubItems(6) = "ALIQUOTA.DBF"
          
6         Set lItem = lsvDados.ListItems.Add
7         lItem = "Antecipacao_Tributaria"
8         lItem.SubItems(5) = "D/I"
9         lItem.SubItems(6) = "ANTECIPA.DBF"

10        Set lItem = lsvDados.ListItems.Add
11        lItem = "Aplicacao"
12        lItem.SubItems(5) = "I/U"
13        lItem.SubItems(6) = "APLICACA.DBF"

14        Set lItem = lsvDados.ListItems.Add
15        lItem = "Banco"
16        lItem.SubItems(5) = "I/U"
17        lItem.SubItems(6) = "BANCO.DBF"
          
18        Set lItem = lsvDados.ListItems.Add
19        lItem = "Cancel_Pednota"
20        lItem.SubItems(5) = "D/I"
21        lItem.SubItems(6) = "CANCEL_P.DBF"
          
22        Set lItem = lsvDados.ListItems.Add
23        lItem = "Categ_Sinal"
24        lItem.SubItems(5) = "I/U"
25        lItem.SubItems(6) = "CATEG_S.DBF"

26        Set lItem = lsvDados.ListItems.Add
27        lItem = "Cesta_item"
28        lItem.SubItems(5) = "D/I"
29        lItem.SubItems(6) = "CESTAI.DBF"

30        Set lItem = lsvDados.ListItems.Add
31        lItem = "Cesta_venda"
32        lItem.SubItems(5) = "D/I"
33        lItem.SubItems(6) = "CESTAV.DBF"

34        Set lItem = lsvDados.ListItems.Add
35        lItem = "Cidade"
36        lItem.SubItems(5) = "I/U"
37        lItem.SubItems(6) = "CIDADE.DBF"

38        Set lItem = lsvDados.ListItems.Add
39        lItem = "Class_Antec_Entrada"
40        lItem.SubItems(5) = "I/U"
41        lItem.SubItems(6) = "CLASSANT.DBF"

42        Set lItem = lsvDados.ListItems.Add
43        lItem = "Clie_Credito"
44        lItem.SubItems(5) = "I/U"
45        lItem.SubItems(6) = "CLIE_CRE.DBF"

46        Set lItem = lsvDados.ListItems.Add
47        lItem = "Clie_Endereco"
48        lItem.SubItems(5) = "I/U"
49        lItem.SubItems(6) = "CLIE_END.DBF"

50        Set lItem = lsvDados.ListItems.Add
51        lItem = "Clie_Mensagem"
52        lItem.SubItems(5) = "I/U"
53        lItem.SubItems(6) = "CLIE_MEN.DBF"

54        Set lItem = lsvDados.ListItems.Add
55        lItem = "Cliente"
56        lItem.SubItems(5) = "I/U"
57        lItem.SubItems(6) = "CLIENTE.DBF"

58        Set lItem = lsvDados.ListItems.Add
59        lItem = "Cliente_Acumulado"
60        lItem.SubItems(5) = "I/U"
61        lItem.SubItems(6) = "CLIEACUM.DBF"

62        Set lItem = lsvDados.ListItems.Add
63        lItem = "Cliente_caracteristica"
64        lItem.SubItems(5) = "I/U"
65        lItem.SubItems(6) = "CLIE_CAR.DBF"

66        Set lItem = lsvDados.ListItems.Add
67        lItem = "Cliente_Internet"
68        lItem.SubItems(5) = "I/U"
69        lItem.SubItems(6) = "CLIE_INT.DBF"

70        Set lItem = lsvDados.ListItems.Add
71        lItem = "V_PedLiq_venda"
72        lItem.SubItems(5) = "I/U"
73        lItem.SubItems(6) = "V_PEDLIQ.DBF"

74        Set lItem = lsvDados.ListItems.Add
75        lItem = "Datas"
76        lItem.SubItems(5) = "D/I"
77        lItem.SubItems(6) = "DATAS.DBF"

78        Set lItem = lsvDados.ListItems.Add
79        lItem = "Deposito_visao"
80        lItem.SubItems(5) = "D/I"
81        lItem.SubItems(6) = "DEP_VISA.DBF"

82        Set lItem = lsvDados.ListItems.Add
83        lItem = "Deposito"
84        lItem.SubItems(5) = "D/I"
85        lItem.SubItems(6) = "DEPOSITO.DBF"

86        Set lItem = lsvDados.ListItems.Add
87        lItem = "Dolar_Diario"
88        lItem.SubItems(5) = "I/U"
89        lItem.SubItems(6) = "DOLAR_DI.DBF"

90        Set lItem = lsvDados.ListItems.Add
91        lItem = "Duplicatas"
92        lItem.SubItems(5) = "D/I"
93        lItem.SubItems(6) = "DUPLICAT.DBF"

94        Set lItem = lsvDados.ListItems.Add
95        lItem = "Embalador"
96        lItem.SubItems(5) = "I/U"
97        lItem.SubItems(6) = "EMBALAD.DBF"

98        Set lItem = lsvDados.ListItems.Add
99        lItem = "Filial"
100       lItem.SubItems(5) = "I/U"
101       lItem.SubItems(6) = "FILIAL.DBF"

102       Set lItem = lsvDados.ListItems.Add
103       lItem = "Filial_Franquia"
104       lItem.SubItems(5) = "I/U"
105       lItem.SubItems(6) = "FIL_FRA.DBF"
          
106       Set lItem = lsvDados.ListItems.Add
107       lItem = "Fornecedor"
108       lItem.SubItems(5) = "I/U"
109       lItem.SubItems(6) = "FORNECED.DBF"

110       Set lItem = lsvDados.ListItems.Add
111       lItem = "Fornecedor_Especifico"
112       lItem.SubItems(5) = "I/U"
113       lItem.SubItems(6) = "FORN_ESP.DBF"

114       Set lItem = lsvDados.ListItems.Add
115       lItem = "Frete_entrega"
116       lItem.SubItems(5) = "I/U"
117       lItem.SubItems(6) = "FRETE_EN.DBF"

118       Set lItem = lsvDados.ListItems.Add
119       lItem = "Frete_uf"
120       lItem.SubItems(5) = "I/U"
121       lItem.SubItems(6) = "FRETE_UF.DBF"

122       Set lItem = lsvDados.ListItems.Add
123       lItem = "Frete_UF_Bloq"
124       lItem.SubItems(5) = "I/U"
125       lItem.SubItems(6) = "FRETEUFB.DBF"

126       Set lItem = lsvDados.ListItems.Add
127       lItem = "Frete_uf_transp"
128       lItem.SubItems(5) = "I/U"
129       lItem.SubItems(6) = "FRETEUFT.DBF"

130       Set lItem = lsvDados.ListItems.Add
131       lItem = "Grupo"
132       lItem.SubItems(5) = "I/U"
133       lItem.SubItems(6) = "GRUPO.DBF"

134       Set lItem = lsvDados.ListItems.Add
135       lItem = "Item_analitico"
136       lItem.SubItems(5) = "I/U"
137       lItem.SubItems(6) = "ITEM_ANA.DBF"

138       Set lItem = lsvDados.ListItems.Add
139       lItem = "Item_Cadastro"
140       lItem.SubItems(5) = "I/U"
141       lItem.SubItems(6) = "ITEM_CAD.DBF"

142       Set lItem = lsvDados.ListItems.Add
143       lItem = "Item_Custo"
144       lItem.SubItems(5) = "I/U"
145       lItem.SubItems(6) = "ITEM_CUS.DBF"
          
146       Set lItem = lsvDados.ListItems.Add
147       lItem = "Item_estoque"
148       lItem.SubItems(5) = "I/U"
149       lItem.SubItems(6) = "ITEM_EST.DBF"

150       Set lItem = lsvDados.ListItems.Add
151       lItem = "Item_global"
152       lItem.SubItems(5) = "I/U"
153       lItem.SubItems(6) = "ITEM_GLO.DBF"

154       Set lItem = lsvDados.ListItems.Add
155       lItem = "Item_preco"
156       lItem.SubItems(5) = "I/U"
157       lItem.SubItems(6) = "ITEM_PRE.DBF"

158       Set lItem = lsvDados.ListItems.Add
159       lItem = "ItPedNota_Venda"
160       lItem.SubItems(5) = "I/U"
161       lItem.SubItems(6) = "ITPEDNOT.DBF"

162       Set lItem = lsvDados.ListItems.Add
163       lItem = "Loja"
164       lItem.SubItems(5) = "D/I"
165       lItem.SubItems(6) = "LOJA.DBF"

166       Set lItem = lsvDados.ListItems.Add
167       lItem = "Montadora"
168       lItem.SubItems(5) = "I/U"
169       lItem.SubItems(6) = "MONTADOR.DBF"

170       Set lItem = lsvDados.ListItems.Add
171       lItem = "Natureza_Operacao"
172       lItem.SubItems(5) = "I/U"
173       lItem.SubItems(6) = "NATU_OPE.DBF"

174       Set lItem = lsvDados.ListItems.Add
175       lItem = "PedNota_Venda"
176       lItem.SubItems(5) = "I/U"
177       lItem.SubItems(6) = "PEDNOTA.DBF"
          
178       Set lItem = lsvDados.ListItems.Add
179       lItem = "Plano_Pgto"
180       lItem.SubItems(5) = "I/U"
181       lItem.SubItems(6) = "PLANO_PG.DBF"

182       Set lItem = lsvDados.ListItems.Add
183       lItem = "R_Clie_Repres"
184       lItem.SubItems(5) = "I"
185       lItem.SubItems(6) = "R_CLIE_R.DBF"

186       Set lItem = lsvDados.ListItems.Add
187       lItem = "R_DPK_Equiv"
188       lItem.SubItems(5) = "I/U"
189       lItem.SubItems(6) = "R_DPK_EQ.DBF"

190       Set lItem = lsvDados.ListItems.Add
191       lItem = "R_Fabrica_DPK"
192       lItem.SubItems(5) = "I/U"
193       lItem.SubItems(6) = "R_FABRIC.DBF"

194       Set lItem = lsvDados.ListItems.Add
195       lItem = "R_FilDep"
196       lItem.SubItems(5) = "I"
197       lItem.SubItems(6) = "R_FILDEP.DBF"

198       Set lItem = lsvDados.ListItems.Add
199       lItem = "R_Pedido_Conf"
200       lItem.SubItems(5) = "I/U"
201       lItem.SubItems(6) = "R_PED_CO.DBF"

202       Set lItem = lsvDados.ListItems.Add
203       lItem = "R_RepCGC"
204       lItem.SubItems(5) = "I"
205       lItem.SubItems(6) = "R_REPCGC.DBF"

206       Set lItem = lsvDados.ListItems.Add
207       lItem = "R_RepVen"
208       lItem.SubItems(5) = "I"
209       lItem.SubItems(6) = "R_REPVEN.DBF"

210       Set lItem = lsvDados.ListItems.Add
211       lItem = "R_UF_Deposito"
212       lItem.SubItems(5) = "I"
213       lItem.SubItems(6) = "R_UF_DEP.DBF"

214       Set lItem = lsvDados.ListItems.Add
215       lItem = "Repr_End_Corresp"
216       lItem.SubItems(5) = "I/U"
217       lItem.SubItems(6) = "REP_ENDC.DBF"

218       Set lItem = lsvDados.ListItems.Add
219       lItem = "Representacao"
220       lItem.SubItems(5) = "I/U"
221       lItem.SubItems(6) = "REPRES.DBF"

222       Set lItem = lsvDados.ListItems.Add
223       lItem = "Representante"
224       lItem.SubItems(5) = "I/U"
225       lItem.SubItems(6) = "REPRESEN.DBF"

226       Set lItem = lsvDados.ListItems.Add
227       lItem = "Resultado"
228       lItem.SubItems(5) = "I/U"
229       lItem.SubItems(6) = "RESULTAD.DBF"

230       Set lItem = lsvDados.ListItems.Add
231       lItem = "Romaneio"
232       lItem.SubItems(5) = "I/U"
233       lItem.SubItems(6) = "ROMANEIO.DBF"

234       Set lItem = lsvDados.ListItems.Add
235       lItem = "Saldo_Pedidos"
236       lItem.SubItems(5) = "D/I"
237       lItem.SubItems(6) = "SALDO_PE.DBF"

238       Set lItem = lsvDados.ListItems.Add
239       lItem = "SubGrupo"
240       lItem.SubItems(5) = "I/U"
241       lItem.SubItems(6) = "SUBGRUPO.DBF"

242       Set lItem = lsvDados.ListItems.Add
243       lItem = "Subst_Tributaria"
244       lItem.SubItems(5) = "I/U"
245       lItem.SubItems(6) = "SUBST_TR.DBF"

246       Set lItem = lsvDados.ListItems.Add
247       lItem = "Tabela_DescPer"
248       lItem.SubItems(5) = "D/I"
249       lItem.SubItems(6) = "TABELAD.DBF"

250       Set lItem = lsvDados.ListItems.Add
251       lItem = "Tabela_Venda"
252       lItem.SubItems(5) = "I/U"
253       lItem.SubItems(6) = "TABELAV.DBF"

254       Set lItem = lsvDados.ListItems.Add
255       lItem = "Taxa"
256       lItem.SubItems(5) = "D/I"
257       lItem.SubItems(6) = "TAXA.DBF"

258       Set lItem = lsvDados.ListItems.Add
259       lItem = "Tipo_Cliente"
260       lItem.SubItems(5) = "I/U"
261       lItem.SubItems(6) = "TIPO_CLI.DBF"

262       Set lItem = lsvDados.ListItems.Add
263       lItem = "Tipo_Cliente_Blau"
264       lItem.SubItems(5) = "I/U"
265       lItem.SubItems(6) = "TP_BLAU.DBF"

266       Set lItem = lsvDados.ListItems.Add
267       lItem = "Transportadora"
268       lItem.SubItems(5) = "I/U"
269       lItem.SubItems(6) = "TRANSPOR.DBF"

270       Set lItem = lsvDados.ListItems.Add
271       lItem = "UF"
272       lItem.SubItems(5) = "I/U"
273       lItem.SubItems(6) = "UF.DBF"

274       Set lItem = lsvDados.ListItems.Add
275       lItem = "UF_categ"
276       lItem.SubItems(5) = "I/U"
277       lItem.SubItems(6) = "UF_CATEG.DBF"

278       Set lItem = lsvDados.ListItems.Add
279       lItem = "UF_deposito"
280       lItem.SubItems(5) = "I"
281       lItem.SubItems(6) = "UF_DEP.DBF"

282       Set lItem = lsvDados.ListItems.Add
283       lItem = "UF_DPK"
284       lItem.SubItems(5) = "I/U"
285       lItem.SubItems(6) = "UF_DPK.DBF"

286       Set lItem = lsvDados.ListItems.Add
287       lItem = "UF_Origem_Destino"
288       lItem.SubItems(5) = "I/U"
289       lItem.SubItems(6) = "UF_OR_DE.DBF"

290       Set lItem = lsvDados.ListItems.Add
291       lItem = "UF_TpCliente"
292       lItem.SubItems(5) = "I/U"
293       lItem.SubItems(6) = "UF_TPCLI.DBF"

294       Set lItem = lsvDados.ListItems.Add
295       lItem = "V_Cliente_Fiel"
296       lItem.SubItems(5) = "D/I"
297       lItem.SubItems(6) = "VCLIFIEL.DBF"

298       Set lItem = lsvDados.ListItems.Add
299       lItem = "Vdr_Cliente_Categ_VDR"
300       lItem.SubItems(5) = "I/U"
301       lItem.SubItems(6) = "VDR_CLIE.DBF"

302       Set lItem = lsvDados.ListItems.Add
303       lItem = "Vdr_Controle_Vdr"
304       lItem.SubItems(5) = "I/U"
305       lItem.SubItems(6) = "VDR_CONT.DBF"
          
306       Set lItem = lsvDados.ListItems.Add
307       lItem = "Controle"
308       lItem.SubItems(5) = "D/I"
309       lItem.SubItems(6) = "CONTROLE.DBF"

310       Set lItem = lsvDados.ListItems.Add
311       lItem = "Vdr_Desconto_Categ"
312       lItem.SubItems(5) = "I/U"
313       lItem.SubItems(6) = "VDR_DESC.DBF"

314       Set lItem = lsvDados.ListItems.Add
315       lItem = "Vdr_Item_Preco"
316       lItem.SubItems(5) = "I/U"
317       lItem.SubItems(6) = "VDR_ITEM.DBF"

318       Set lItem = lsvDados.ListItems.Add
319       lItem = "Vdr_R_Clie_Linha_Produto"
320       lItem.SubItems(5) = "D/I"
321       lItem.SubItems(6) = "VDR_RCLI.DBF"

322       Set lItem = lsvDados.ListItems.Add
323       lItem = "Vdr_Tabela_Venda"
324       lItem.SubItems(5) = "I/U"
325       lItem.SubItems(6) = "VDR_TABE.DBF"

326       Set lItem = lsvDados.ListItems.Add
327       lItem = "Venda_Limitada"
328       lItem.SubItems(5) = "D/I"
329       lItem.SubItems(6) = "VLIMITAD.DBF"

330       Set lItem = lsvDados.ListItems.Add
331       lItem = "Estatistica_Item"
332       lItem.SubItems(5) = "D/I"
333       lItem.SubItems(6) = "ESTAT_IT.DBF"
          
334       Set lItem = lsvDados.ListItems.Add
335       lItem = "Classe_Fiscal"
336       lItem.SubItems(5) = "D"
337       lItem.SubItems(6) = "DEL_RBAS.DBF"
          
338       Set lItem = lsvDados.ListItems.Add
339       lItem = "Del_Aplicacao"
340       lItem.SubItems(5) = "D"
341       lItem.SubItems(6) = "DEL_APL.DBF"

342       Set lItem = lsvDados.ListItems.Add
343       lItem = "Delete_Cli"
344       lItem.SubItems(5) = "D"
345       lItem.SubItems(6) = "DEL_CLI.DBF"
          
346       Set lItem = lsvDados.ListItems.Add
347       lItem = "Delete_Fornecedor"
348       lItem.SubItems(5) = "D"
349       lItem.SubItems(6) = "DEL_FOES.DBF"

350       Set lItem = lsvDados.ListItems.Add
351       lItem = "Pedido_Nota"
352       lItem.SubItems(5) = "D"
353       lItem.SubItems(6) = "DEL_FOES.DBF"
          
354       Set lItem = lsvDados.ListItems.Add
355       lItem = "Cliente_Representante"
356       lItem.SubItems(5) = "D"
357       lItem.SubItems(6) = "DEL_R_CL.DBF"
          
358       Set lItem = lsvDados.ListItems.Add
359       lItem = "Fabrica_Dpk"
360       lItem.SubItems(5) = "D"
361       lItem.SubItems(6) = "DEL_R_FA.DBF"

362       Set lItem = lsvDados.ListItems.Add
363       lItem = "Cliente_Repres"
364       lItem.SubItems(5) = "D"
365       lItem.SubItems(6) = "DEL_R_FA.DBF"

366       Set lItem = lsvDados.ListItems.Add
367       lItem = "R_Dpk_Equiv"
368       lItem.SubItems(5) = "D"
369       lItem.SubItems(6) = "DEL_RDEQ.DBF"
          
370       Set lItem = lsvDados.ListItems.Add
371       lItem = "Del_Repr"
372       lItem.SubItems(5) = "D"
373       lItem.SubItems(6) = "DEL_REP.DBF"
          
374       Set lItem = lsvDados.ListItems.Add
375       lItem = "Del_TabD"
376       lItem.SubItems(5) = "D"
377       lItem.SubItems(6) = "DEL_TABD.DBF"
          
378       Set lItem = lsvDados.ListItems.Add
379       lItem = "Del_UF_Deposito"
380       lItem.SubItems(5) = "D"
381       lItem.SubItems(6) = "DEL_UFDP.DBF"
          
382       Set lItem = lsvDados.ListItems.Add
383       lItem = "Del_cliente_categ"
384       lItem.SubItems(5) = "D"
385       lItem.SubItems(6) = "DEL_VDR.DBF"
          
386       Set lItem = lsvDados.ListItems.Add
387       lItem = "Fiscal_Red_Base"
388       lItem.SubItems(5) = "I"
389       lItem.SubItems(6) = "RED_BASE.DBF"
          
Trata_Erro:
390       If Err.Number <> 0 Then
391           MsgBox "Sub Preencher_Lsv" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
392       End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
          Dim vResp As Integer
          
          'vResp = MsgBox("Deseja fazer a otimiza��o do banco de dados neste momento?", vbYesNo, "Aten��o")
          'If vResp = 6 Then
          '    RunCmd Path_drv & "\pgms\Repara_Banco.exe", 1 'FIL046
          'End If
          
1         connBaseDpk.Close
2         Set connBaseDpk = Nothing
End Sub

