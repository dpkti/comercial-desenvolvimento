VERSION 4.00
Begin VB.Form frmRepres 
   BorderStyle     =   0  'None
   ClientHeight    =   5415
   ClientLeft      =   90
   ClientTop       =   1440
   ClientWidth     =   9330
   BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Height          =   5820
   Left            =   30
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   9330
   ShowInTaskbar   =   0   'False
   Top             =   1095
   Width           =   9450
   Begin VB.TextBox txtPseudonimo 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2160
      MaxLength       =   15
      TabIndex        =   2
      Top             =   360
      Width           =   1935
   End
   Begin VB.TextBox txtCodigo 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1395
      MaxLength       =   4
      TabIndex        =   1
      Top             =   360
      Width           =   615
   End
   Begin MSGrid.Grid Grid2 
      Height          =   615
      Left            =   2280
      TabIndex        =   5
      Top             =   360
      Visible         =   0   'False
      Width           =   3975
      _Version        =   65536
      _ExtentX        =   7011
      _ExtentY        =   1085
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      ScrollBars      =   2
      MouseIcon       =   "FRMREPRE.frx":0000
   End
   Begin MSGrid.Grid Grid1 
      Height          =   615
      Left            =   2280
      TabIndex        =   4
      Top             =   360
      Visible         =   0   'False
      Width           =   3975
      _Version        =   65536
      _ExtentX        =   7011
      _ExtentY        =   1085
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      ScrollBars      =   2
      MouseIcon       =   "FRMREPRE.frx":001C
   End
   Begin VB.Label lblNome 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   4230
      TabIndex        =   3
      Top             =   360
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.Label Label1 
      Caption         =   "Repres."
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   540
      TabIndex        =   0
      Top             =   360
      Width           =   735
   End
End
Attribute VB_Name = "frmRepres"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Private Sub Text1_KeyPress(KeyAscii As Integer)
 
    KeyAscii = Numerico(KeyAscii)
 
End Sub




Private Sub Text2_KeyPress(KeyAscii As Integer)

    KeyAscii = Maiusculo(KeyAscii)
  
End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

    MDIForm1.ssmsg.Caption = ""

End Sub


Private Sub Grid1_DblClick()

    Dim COD As String
    
    frmRepres.Grid1.Col = 0
    COD = Grid1.Text
    frmRepres.Grid1.Visible = False
    frmRepres.Label1.Visible = True
    frmRepres.txtcodigo.Visible = True
    frmRepres.txtpseudonimo.Visible = True
    frmRepres.lblnome.Visible = True
    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Add "cod", COD, 1
    Set oradynaset = oradatabase.dbcreatedynaset(sel1, 0&)
    
    If oradynaset.EOF Then
        MsgBox "Repres.:  " & COD & " n�o cadastrado", 0, "ATEN��O"
        Exit Sub
    End If
    
    oradatabase.Parameters.Remove "cod"
    Call DADOS_TELA
    Exit Sub

End Sub


Private Sub Grid2_DblClick()

    Dim COD As Double
    
    frmRepres.Grid2.Col = 0
    COD = Grid2.Text
    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Add "cod", COD, 1
    Set oradynaset = oradatabase.dbcreatedynaset(sel9, 0&)
    MDIForm1.ssmsg.Caption = "Aguarde Consultando .."
       
        If oradynaset.EOF Then
            MDIForm1.ssmsg.Caption = ""
            MsgBox "N�o h� representante/vendedor para esta filial", 0, "ATEN��O"
            Grid2.Visible = True
            oradatabase.Parameters.Remove "cod"
            Exit Sub
        Else
            'CHAMA PROCEDURE QUE MONTA O GRID
            botao = 6
            Call CONS_GRID(360, 1800, 5400, 2500, 3, frmRepres, sel9)
            MDIForm1.ssmsg.Caption = ""
            frmRepres.Grid2.Visible = False
            frmRepres.Grid1.Visible = True
            Exit Sub
        End If
 
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)

    KeyAscii = Numerico(KeyAscii)

End Sub

Private Sub txtPseudonimo_Change()

    If frmRepres.txtpseudonimo <> "" Then
        frmRepres.txtpseudonimo.SetFocus
    End If

End Sub


Private Sub txtPseudonimo_KeyPress(KeyAscii As Integer)

    KeyAscii = Maiusculo(KeyAscii)

End Sub


