VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmFornecedor 
   Caption         =   "Lista de Fornecedores"
   ClientHeight    =   3075
   ClientLeft      =   1260
   ClientTop       =   1965
   ClientWidth     =   6705
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3075
   ScaleWidth      =   6705
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5400
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
   Begin MSGrid.Grid grdFornecedor 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "L_FORNEC.frx":0000
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   4815
   End
End
Attribute VB_Name = "frmFornecedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String
Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmFornecedor = Nothing
End Sub


Private Sub Form_Load()

    On Error GoTo TrataErro
    
   
    Dim ss As Object
    Dim i As Integer
    
    'mouse
    Screen.MousePointer = vbHourglass
    
    'montar SQL
    sql = "select COD_FORNECEDOR,SIGLA "
    sql = sql & "from FORNECEDOR "
    sql = sql & "where SITUACAO = 0 and "
    sql = sql & "DIVISAO = 'D' and CLASSIFICACAO = 'A' "
    sql = sql & "order by SIGLA"
    
    'criar consulta
    Set ss = oradatabase.dbcreatedynaset(sql, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha fornecedor cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    'carrega dados
    With frmFornecedor.grdFornecedor
        .Cols = 2
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 5325
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Sigla do Fornecedor"
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss("COD_FORNECEDOR")
            .Col = 1
            .Text = ss("SIGLA")
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
        
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     Resume
   Else
     Call Process_Line_Errors(sql)
   End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmFornecedor = Nothing
End Sub


Private Sub grdFornecedor_DblClick()
    grdFornecedor.Col = 0
    txtResposta = grdFornecedor.Text
    Unload Me
End Sub

Private Sub grdFornecedor_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdFornecedor.Col = 1
    If KeyAscii = 13 Then
        grdFornecedor.Col = 0
        txtResposta = grdFornecedor.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdFornecedor.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdFornecedor
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdFornecedor.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdFornecedor
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdFornecedor.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub

