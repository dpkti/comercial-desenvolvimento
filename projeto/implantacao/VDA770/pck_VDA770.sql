--- PACKAGE DO SISTEMA VDA770
--- DATA: 23/07/2004
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\VDA770\PADRAO\PCK_VDA770.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_VDA770 is

	Type tp_Cursor is Ref Cursor;
			  				
	PROCEDURE pr_CON_DADOSPEDIDO( PM_NUMPED  IN NUMBER,
								  PM_SEQPED  IN NUMBER,
								  PM_CODLOJA IN NUMBER,
								  PM_CURSOR1 IN OUT tp_Cursor,
			  					  PM_CODERRO OUT NUMBER,
			  					  PM_TXTERRO OUT VARCHAR2);
			  					  
			  					  
	PROCEDURE pr_CON_PEDIDOGER( PM_CODGER  IN NUMBER,
								PM_INICIO  IN VARCHAR2,
								PM_FIM     IN VARCHAR2,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2);
			  					
			  					
	PROCEDURE pr_CON_LISTAGERENTES( PM_CURSOR1 IN OUT tp_Cursor,
			  						PM_CODERRO OUT NUMBER,
			  						PM_TXTERRO OUT VARCHAR2);
			  						
			  						
	PROCEDURE pr_CON_PEDIDOSGERDATA( PM_CODGER   IN NUMBER,
									 PM_DTINICIO IN VARCHAR2,
									 PM_DTFIM    IN VARCHAR2,
									 PM_CURSOR1  IN OUT tp_Cursor,
			  						 PM_CODERRO  OUT NUMBER,
			  						 PM_TXTERRO  OUT VARCHAR2);
			  						 
	PROCEDURE pr_CON_PEDIDOGERDET( PM_CODCLI  IN NUMBER,
								   PM_CODGER  IN NUMBER,
								   PM_INICIO  IN VARCHAR2,
								   PM_FIM     IN VARCHAR2,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2);
			  						 
			  						 
	PROCEDURE pr_CON_PEDIDOSGERDATAdet( PM_CODGER   IN NUMBER,
										PM_CODCLI   IN NUMBER,
										PM_DTINICIO IN VARCHAR2,
										PM_DTFIM    IN VARCHAR2,
										PM_CURSOR1  IN OUT tp_Cursor,
			  							PM_CODERRO  OUT NUMBER,
			  							PM_TXTERRO  OUT VARCHAR2);
			  							
			  							
	PROCEDURE pr_CON_ANALISTAS( PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2);
			  					
			  					
	PROCEDURE pr_CON_PEDIDOANALISTA( PM_CODANA  IN NUMBER,
									 PM_INICIO  IN VARCHAR2,
									 PM_FIM     IN VARCHAR2,
									 PM_CURSOR1 IN OUT tp_Cursor,
			  						 PM_CODERRO OUT NUMBER,
			  						 PM_TXTERRO OUT VARCHAR2);
			  						 
			  						 
	PROCEDURE pr_CON_PEDIDOANADET( PM_CODCLI  IN NUMBER,
								   PM_CODANA  IN NUMBER,
								   PM_INICIO  IN VARCHAR2,
								   PM_FIM     IN VARCHAR2,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2);
			  					   
			  					   
	PROCEDURE pr_CON_DESEMPINT( PM_CODANA   IN NUMBER,
								PM_SITUACAO IN VARCHAR2,
								PM_CURSOR1  IN OUT tp_Cursor,
			  					PM_CODERRO  OUT NUMBER,
			  					PM_TXTERRO  OUT VARCHAR2);
			  					
			  					
	PROCEDURE pr_CON_BLOQUEIOS( PM_SITUACAO IN VARCHAR2,
								PM_INICIO   IN VARCHAR2,
								PM_FIM      IN VARCHAR2,
								PM_CURSOR1  IN OUT tp_Cursor,
				  				PM_CODERRO  OUT NUMBER,
				  				PM_TXTERRO  OUT VARCHAR2) ;
				  					   
				  					   
	PROCEDURE pr_CON_PEDMEDIA( PM_SITUACAO IN VARCHAR2,
							   PM_CURSOR1  IN OUT tp_Cursor,
				  			   PM_CODERRO  OUT NUMBER,
				  			   PM_TXTERRO  OUT VARCHAR2);
				  			   
				  			   
	PROCEDURE pr_CON_TOTPED( PM_SITUACAO IN VARCHAR2,
							 PM_INICIO   IN VARCHAR2,
							 PM_FIM      IN VARCHAR2,
							 PM_CURSOR1  IN OUT tp_Cursor,
				  			 PM_CODERRO  OUT NUMBER,
				  			 PM_TXTERRO  OUT VARCHAR2);

End PCK_VDA770;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_VDA770 is


	PROCEDURE pr_CON_DADOSPEDIDO( PM_NUMPED  IN NUMBER,
								  PM_SEQPED  IN NUMBER,
								  PM_CODLOJA IN NUMBER,
								  PM_CURSOR1 IN OUT tp_Cursor,
			  					  PM_CODERRO OUT NUMBER,
			  					  PM_TXTERRO OUT VARCHAR2) AS
		
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		SELECT
		A.COD_USUARIO_CREDITO,B.NOME_USUARIO USUARIO,
		TRUNC(A.DT_LIBERACAO)  DATA_LIBERACAO, TO_CHAR(A.DT_LIBERACAO,'HH:MI:SS') HS_LIBERACAO,
		A.FL_LIBERACAO lib_can, A.COD_USUARIO_RESPONSAVEL COD_RESPONSAVEL, C.NOME_USUARIO NOME_GERENTE
		from 
		PRODUCAO.R_PEDIDO_CREDITO A, 
		HELPDESK.USUARIO B,
		HELPDESK.USUARIO C
		where 
		B.cod_usuario = A.cod_usuario_credito AND
		C.COD_USUARIO(+) = A.COD_USUARIO_RESPONSAVEL AND
		num_pedido = PM_NUMPED and 
		seq_pedido = PM_SEQPED and A.cod_loja = PM_CODLOJA;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_DADOSPEDIDO;


	PROCEDURE pr_CON_PEDIDOGER( PM_CODGER  IN NUMBER,
								PM_INICIO  IN VARCHAR2,
								PM_FIM     IN VARCHAR2,
								PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2) AS
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;


		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' CL.cod_cliente "COD", ';
		STRSQL := STRSQL || ' CL.NOME_CLIENTE "CLIENTE",';
		STRSQL := STRSQL || ' COUNT(DISTINCT(d.NUM_PEDIDO)) "QTD. PEDIDOS", ';
		STRSQL := STRSQL || ' SUM(VL_DUPLICATA) "VL. CONT�BIL"';
		STRSQL := STRSQL || ' ,SUM((SELECT NVL(sum((vl_duplicata - vl_desconto - vl_abatimento - vl_devolucao - vl_pago)),0) FROM ';
		STRSQL := STRSQL || '		COBRANCA.DUPLICATAS DD, DATAS DT';
		STRSQL := STRSQL || '		WHERE';
		STRSQL := STRSQL || '		DD.SITUACAO_PAGTO = 0';
		STRSQL := STRSQL || '		AND DD.dt_vencimento < DT.DT_FATURAMENTO';
		STRSQL := STRSQL || '		AND DD.cod_loja = b.cod_loja';
		STRSQL := STRSQL || '		and DD.num_fatura = b.num_fatura';
		STRSQL := STRSQL || '		AND DD.NUM_ORDEM = B.NUM_ORDEM)) "VL. VENCIDO" ';
		STRSQL := STRSQL || ' from ';
		STRSQL := STRSQL || ' CLIENTE CL, ';
		STRSQL := STRSQL || ' datas,';
		STRSQL := STRSQL || ' cobranca.duplicatas b,';
		STRSQL := STRSQL || ' PRODUCAO.pednota_venda D,';
		STRSQL := STRSQL || ' PRODUCAO.R_PEDIDO_CREDITO A';
		STRSQL := STRSQL || ' where';
		
		STRSQL := STRSQL || ' d.cod_loja_nota = b.cod_loja';
		STRSQL := STRSQL || ' and rpad(d.num_nota,6) = b.num_fatura';
		STRSQL := STRSQL || ' and d.COD_LOJA = a.COD_LOJA';
		STRSQL := STRSQL || ' and d.NUM_PEDIDO = a.NUM_PEDIDO';
		STRSQL := STRSQL || ' and d.SEQ_PEDIDO = a.SEQ_PEDIDO ';
		STRSQL := STRSQL || ' AND D.COD_CLIENTE = CL.COD_CLIENTE';
		
		IF PM_INICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' AND TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
		END IF ;

		IF PM_FIM IS NOT NULL THEN
			STRSQL := STRSQL || ' AND TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
		END IF ;
		
		IF PM_CODGER <> 0 THEN
			STRSQL := STRSQL || ' AND A.COD_USUARIO_RESPONSAVEL = ' || PM_CODGER ;
		END IF ;
		
		STRSQL := STRSQL || ' group by CL.cod_cliente, CL.NOME_CLIENTE';
		STRSQL := STRSQL || ' ORDER BY cl.NOME_CLIENTE ';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOGER;
	
	
	PROCEDURE pr_CON_LISTAGERENTES( PM_CURSOR1 IN OUT tp_Cursor,
			  						PM_CODERRO OUT NUMBER,
			  						PM_TXTERRO OUT VARCHAR2) AS
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		Select distinct
		A.COD_USUARIO_RESPONSAVEL CODIGO, C.NOME_USUARIO NOME
		from 
		PRODUCAO.R_PEDIDO_CREDITO A, 
		HELPDESK.USUARIO C
		where 
		A.COD_USUARIO_RESPONSAVEL <> 0 AND
		C.COD_USUARIO = A.COD_USUARIO_RESPONSAVEL
		order by C.NOME_USUARIO;
		
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_LISTAGERENTES;
	
	
	PROCEDURE pr_CON_PEDIDOSGERDATA( PM_CODGER   IN NUMBER,
									 PM_DTINICIO IN VARCHAR2,
									 PM_DTFIM    IN VARCHAR2,
									 PM_CURSOR1  IN OUT tp_Cursor,
			  						 PM_CODERRO  OUT NUMBER,
			  						 PM_TXTERRO  OUT VARCHAR2) AS
			  						 
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' b.cod_cliente "COD", CL.NOME_CLIENTE "CLIENTE",';
		STRSQL := STRSQL || ' sum((vl_duplicata - vl_desconto - vl_abatimento - vl_devolucao - vl_pago)) "VL. ABERTO",';
		STRSQL := STRSQL || ' max(trunc(dt_faturamento)-to_date(b.dt_vencimento,''dd/mm/rrrr'')) "MAX. ATRASO"';
		STRSQL := STRSQL || ' from ';
		STRSQL := STRSQL || ' datas,cobranca.duplicatas b,PRODUCAO.pednota_venda D,PRODUCAO.R_PEDIDO_CREDITO A';
		STRSQL := STRSQL || ' , CLIENTE CL where';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL = '|| PM_CODGER;
		STRSQL := STRSQL || ' and b.situacao_pagto = 0 ';
		STRSQL := STRSQL || ' and b.dt_vencimento < (dt_faturamento - 5)';
		STRSQL := STRSQL || ' and d.cod_loja_nota = b.cod_loja';
		STRSQL := STRSQL || ' and rpad(d.num_nota,6) = b.num_fatura ';
		STRSQL := STRSQL || ' and d.COD_LOJA = a.COD_LOJA';
		STRSQL := STRSQL || ' and d.NUM_PEDIDO = a.NUM_PEDIDO';
		STRSQL := STRSQL || ' and d.SEQ_PEDIDO = a.SEQ_PEDIDO ';
		STRSQL := STRSQL || ' AND B.COD_CLIENTE = CL.COD_CLIENTE';
		
		IF PM_DTINICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' and a.dt_liberacao >= TO_DATE(''' || PM_DTINICIO || ''',''DD/MM/RR'') ';
		END IF;
		
		IF PM_DTFIM IS NOT NULL THEN
			STRSQL := STRSQL || ' AND TRUNC(a.dt_liberacao) <= TO_DATE(''' || PM_DTFIM || ''',''DD/MM/RR'') ';
		END IF;
		
		STRSQL := STRSQL || ' group by b.cod_cliente, CL.NOME_CLIENTE';
		STRSQL := STRSQL || ' order by b.cod_cliente';
		
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOSGERDATA;
	
	
	PROCEDURE pr_CON_PEDIDOGERDET( PM_CODCLI  IN NUMBER,
								   PM_CODGER  IN NUMBER,
								   PM_INICIO  IN VARCHAR2,
								   PM_FIM     IN VARCHAR2,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2) AS
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' a.cod_loja "LOJA",';
		STRSQL := STRSQL || ' a.num_pedido "NUM. PEDIDO",';
		STRSQL := STRSQL || ' A.SEQ_PEDIDO "SEQU�NCIA",';
		STRSQL := STRSQL || ' P.VL_CONTABIL "VALOR",';
		--STRSQL := STRSQL || ' A.COD_USUARIO_CREDITO "COD. USU�RIO",';
		--STRSQL := STRSQL || ' B.NOME_USUARIO "USU�RIO", ';
		STRSQL := STRSQL || ' A.DT_LIBERACAO "DATA LIBERA��O", ';
		STRSQL := STRSQL || ' DECODE(A.FL_LIBERACAO,''L'',''LIBERADO'',''CANCELADO'') "SITUA��O",';
		STRSQL := STRSQL || ' b.NOME_USUARIO "OPERADOR"';
		STRSQL := STRSQL || ' from  ';
		STRSQL := STRSQL || ' PRODUCAO.R_PEDIDO_CREDITO A,  ';
		STRSQL := STRSQL || ' PRODUCAO.PEDNOTA_VENDA P,';
		STRSQL := STRSQL || ' HELPDESK.USUARIO B';
		STRSQL := STRSQL || ' where  ';
		STRSQL := STRSQL || ' P.COD_LOJA = A.COD_LOJA AND';
		STRSQL := STRSQL || ' P.NUM_PEDIDO = A.NUM_PEDIDO AND';
		STRSQL := STRSQL || ' P.SEQ_PEDIDO = A.SEQ_PEDIDO AND';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL <> 0 AND ';
		STRSQL := STRSQL || ' B.cod_usuario = A.cod_usuario_credito AND ';
		STRSQL := STRSQL || ' P.COD_CLIENTE = ' ||PM_CODCLI  || ' AND';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL = ' || PM_CODGER;		
		
		IF PM_INICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
		END IF ;

		IF PM_FIM IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
		END IF ;
		
		STRSQL := STRSQL || ' order by A.DT_LIBERACAO, a.cod_loja, a.num_pedido,a.seq_pedido';
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOGERDET;
	
	
	PROCEDURE pr_CON_PEDIDOSGERDATAdet( PM_CODGER   IN NUMBER,
										PM_CODCLI   IN NUMBER,
										PM_DTINICIO IN VARCHAR2,
										PM_DTFIM    IN VARCHAR2,
										PM_CURSOR1  IN OUT tp_Cursor,
			  							PM_CODERRO  OUT NUMBER,
			  							PM_TXTERRO  OUT VARCHAR2) AS
			  						 
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := STRSQL || ' SELECT';
		STRSQL := STRSQL || ' A.COD_LOJA "LOJA", A.NUM_PEDIDO "PEDIDO", A.SEQ_PEDIDO "SEQU�NCIA",';
		STRSQL := STRSQL || ' TO_CHAR(A.DT_LIBERACAO,''DD/MM/YYYY'') "DT. LIBERA��O",';
		STRSQL := STRSQL || ' B.DT_EMISSAO_DUPL "DT. EMISS�O", B.DT_VENCIMENTO "DT. VENCIMENTO",';
		STRSQL := STRSQL || ' trunc(dt_faturamento)-to_date(b.dt_vencimento,''dd/mm/rrrr'') "ATRASO",';
		STRSQL := STRSQL || ' (vl_duplicata - vl_desconto - vl_abatimento - vl_devolucao - vl_pago) "VL. ABERTO",     ';
		STRSQL := STRSQL || ' U.NOME_USUARIO "OPERADOR"';
		STRSQL := STRSQL || ' from ';
		STRSQL := STRSQL || ' datas,cobranca.duplicatas b,PRODUCAO.pednota_venda D,PRODUCAO.R_PEDIDO_CREDITO A';
		STRSQL := STRSQL || ' , CLIENTE CL, HELPDESK.USUARIO U where';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL = ' || PM_CODGER;
		STRSQL := STRSQL || ' AND B.COD_CLIENTE = ' || PM_CODCLI;
		STRSQL := STRSQL || ' and b.situacao_pagto = 0 ';
		STRSQL := STRSQL || ' and b.dt_vencimento < (dt_faturamento - 5)';
		STRSQL := STRSQL || ' and d.cod_loja_nota = b.cod_loja';
		STRSQL := STRSQL || ' and rpad(d.num_nota,6) = b.num_fatura ';
		STRSQL := STRSQL || ' and d.COD_LOJA = a.COD_LOJA';
		STRSQL := STRSQL || ' and d.NUM_PEDIDO = a.NUM_PEDIDO';
		STRSQL := STRSQL || ' and d.SEQ_PEDIDO = a.SEQ_PEDIDO ';
		STRSQL := STRSQL || ' AND B.COD_CLIENTE = CL.COD_CLIENTE';
		STRSQL := STRSQL || ' AND A.COD_USUARIO_CREDITO = U.COD_USUARIO';
		
		IF PM_DTINICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' and a.dt_liberacao >= TO_DATE(''' || PM_DTINICIO || ''',''DD/MM/RR'') ';
		END IF;
		
		IF PM_DTFIM IS NOT NULL THEN
			STRSQL := STRSQL || ' AND TRUNC(a.dt_liberacao) <= TO_DATE(''' || PM_DTFIM || ''',''DD/MM/RR'') ';
		END IF;
		
		STRSQL := STRSQL || ' order by A.DT_LIBERACAO, a.cod_loja, a.num_pedido,a.seq_pedido';
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOSGERDATAdet;
	
	
	PROCEDURE pr_CON_ANALISTAS( PM_CURSOR1 IN OUT tp_Cursor,
			  					PM_CODERRO OUT NUMBER,
			  					PM_TXTERRO OUT VARCHAR2) AS
		BEGIN
		
		PM_CODERRO := 0;
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		select
		LTRIM(TO_CHAR(b.cod_usuario,'00000'),5) COD,
		b.nome_usuario "NOME_USUARIO"
		from 
		helpdesk.usuario b,
		producao.r_pedido_credito a
		where 
		a.cod_usuario_credito = b.cod_usuario and
		a.Cod_Usuario_Responsavel = 0
		GROUP BY b.cod_usuario,b.nome_usuario
		ORDER BY nome_usuario;
		
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_ANALISTAS;
	
	
	PROCEDURE pr_CON_PEDIDOANALISTA( PM_CODANA  IN NUMBER,
									 PM_INICIO  IN VARCHAR2,
									 PM_FIM     IN VARCHAR2,
									 PM_CURSOR1 IN OUT tp_Cursor,
			  						 PM_CODERRO OUT NUMBER,
			  						 PM_TXTERRO OUT VARCHAR2) AS
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;


		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' CL.cod_cliente "COD", ';
		STRSQL := STRSQL || ' CL.NOME_CLIENTE "CLIENTE",';
		STRSQL := STRSQL || ' COUNT(DISTINCT(d.NUM_PEDIDO)) "QTD. PEDIDOS", ';
		STRSQL := STRSQL || ' SUM(VL_DUPLICATA) "VL. CONT�BIL"';
		STRSQL := STRSQL || ' ,SUM((SELECT NVL(sum((vl_duplicata - vl_desconto - vl_abatimento - vl_devolucao - vl_pago)),0) FROM ';
		STRSQL := STRSQL || '		COBRANCA.DUPLICATAS DD, DATAS DT';
		STRSQL := STRSQL || '		WHERE';
		STRSQL := STRSQL || '		DD.SITUACAO_PAGTO = 0';
		STRSQL := STRSQL || '		AND DD.dt_vencimento < DT.DT_FATURAMENTO';
		STRSQL := STRSQL || '		AND DD.cod_loja = b.cod_loja';
		STRSQL := STRSQL || '		and DD.num_fatura = b.num_fatura';
		STRSQL := STRSQL || '		AND DD.NUM_ORDEM = B.NUM_ORDEM)) "VL. VENCIDO" ';
		STRSQL := STRSQL || ' from ';
		STRSQL := STRSQL || ' datas,';
		STRSQL := STRSQL || ' CLIENTE CL, ';
		STRSQL := STRSQL || ' cobranca.duplicatas b,';
		STRSQL := STRSQL || ' PRODUCAO.pednota_venda D,';
		STRSQL := STRSQL || ' PRODUCAO.R_PEDIDO_CREDITO A';
		STRSQL := STRSQL || ' where';
		
		STRSQL := STRSQL || ' d.cod_loja_nota = b.cod_loja';
		STRSQL := STRSQL || ' and rpad(d.num_nota,6) = b.num_fatura';
		STRSQL := STRSQL || ' and d.COD_LOJA = a.COD_LOJA';
		STRSQL := STRSQL || ' and d.NUM_PEDIDO = a.NUM_PEDIDO';
		STRSQL := STRSQL || ' and d.SEQ_PEDIDO = a.SEQ_PEDIDO ';
		STRSQL := STRSQL || ' AND D.COD_CLIENTE = CL.COD_CLIENTE';
		STRSQL := STRSQL || ' AND A.COD_USUARIO_RESPONSAVEL = 0';
		
		IF PM_INICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
		END IF ;

		IF PM_FIM IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
		END IF ;
		
		IF PM_CODANA <> 0 THEN
			STRSQL := STRSQL || ' and A.COD_USUARIO_credito = ' || PM_CODANA;
		END IF ;
		
		STRSQL := STRSQL || ' group by CL.cod_cliente, CL.NOME_CLIENTE';
		STRSQL := STRSQL || ' ORDER BY cl.NOME_CLIENTE ';
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOANALISTA;
	
	
	PROCEDURE pr_CON_PEDIDOANADET( PM_CODCLI  IN NUMBER,
								   PM_CODANA  IN NUMBER,
								   PM_INICIO  IN VARCHAR2,
								   PM_FIM     IN VARCHAR2,
								   PM_CURSOR1 IN OUT tp_Cursor,
			  					   PM_CODERRO OUT NUMBER,
			  					   PM_TXTERRO OUT VARCHAR2) AS
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' a.cod_loja "LOJA",';
		STRSQL := STRSQL || ' a.num_pedido "NUM. PEDIDO",';
		STRSQL := STRSQL || ' A.SEQ_PEDIDO "SEQU�NCIA",';
		STRSQL := STRSQL || ' P.VL_CONTABIL "VALOR",';
		--STRSQL := STRSQL || ' A.COD_USUARIO_CREDITO "COD. USU�RIO",';
		--STRSQL := STRSQL || ' B.NOME_USUARIO "USU�RIO", ';
		STRSQL := STRSQL || ' A.DT_LIBERACAO "DATA LIBERA��O", ';
		STRSQL := STRSQL || ' DECODE(A.FL_LIBERACAO,''L'',''LIBERADO'',''CANCELADO'') "SITUA��O",';
		STRSQL := STRSQL || ' b.NOME_USUARIO "OPERADOR"';
		STRSQL := STRSQL || ' from  ';
		STRSQL := STRSQL || ' PRODUCAO.R_PEDIDO_CREDITO A,  ';
		STRSQL := STRSQL || ' PRODUCAO.PEDNOTA_VENDA P,';
		STRSQL := STRSQL || ' HELPDESK.USUARIO B';
		STRSQL := STRSQL || ' where  ';
		STRSQL := STRSQL || ' P.COD_LOJA = A.COD_LOJA AND';
		STRSQL := STRSQL || ' P.NUM_PEDIDO = A.NUM_PEDIDO AND';
		STRSQL := STRSQL || ' P.SEQ_PEDIDO = A.SEQ_PEDIDO AND';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL = 0 AND ';
		STRSQL := STRSQL || ' B.cod_usuario = A.cod_usuario_credito AND ';
		STRSQL := STRSQL || ' P.COD_CLIENTE = ' ||PM_CODCLI  || ' AND';
		STRSQL := STRSQL || ' A.COD_USUARIO_CREDITO = ' || PM_CODANA;
		
		IF PM_INICIO IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
		END IF ;

		IF PM_FIM IS NOT NULL THEN
			STRSQL := STRSQL || ' and TO_DATE(A.DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
		END IF ;
		
		STRSQL := STRSQL || ' order by A.DT_LIBERACAO, a.cod_loja, a.num_pedido,a.seq_pedido';
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_PEDIDOANADET;
	
	
	PROCEDURE pr_CON_DESEMPINT( PM_CODANA   IN NUMBER,
								PM_SITUACAO IN VARCHAR2,
								PM_CURSOR1  IN OUT tp_Cursor,
			  					PM_CODERRO  OUT NUMBER,
			  					PM_TXTERRO  OUT VARCHAR2) AS
		STRSQL VARCHAR2(2000);
		
		BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' b.NOME_USUARIO "OPERADOR", ';
		STRSQL := STRSQL || ' A.ANO_MES "DATA LIBERA��O", ';
		STRSQL := STRSQL || ' sum(A.TOTAL_PEDIDOS) "PEDIDOS", ';
		STRSQL := STRSQL || ' C.MEDIA ';
		STRSQL := STRSQL || ' FROM producao.desempenho_credito A, HELPDESK.USUARIO B, ';
		STRSQL := STRSQL || ' (SELECT AA.ANO_MES,sum(AA.TOTAL_PEDIDOS)/4 MEDIA ';
		STRSQL := STRSQL || ' FROM producao.desempenho_credito AA ';
		STRSQL := STRSQL || ' WHERE AA.ANO_MES >= TO_CHAR(ADD_MONTHS(SYSDATE,-6),''RRRRMM'') ';
			IF PM_SITUACAO IS NOT NULL THEN
				STRSQL := STRSQL || ' AND AA.FL_LIBERACAO = ''' || PM_SITUACAO || ''' ';
			END IF;
		STRSQL := STRSQL || ' GROUP BY AA.ANO_MES) c ';
		STRSQL := STRSQL || ' WHERE ';
		STRSQL := STRSQL || ' A.ANO_MES = C.ANO_MES AND ';
		STRSQL := STRSQL || ' A.COD_USUARIO_CREDITO = B.COD_USUARIO AND';
		STRSQL := STRSQL || ' A.ANO_MES >= TO_CHAR(ADD_MONTHS(SYSDATE,-6),''RRRRMM'') ';
		
		IF PM_SITUACAO IS NOT NULL THEN
			STRSQL := STRSQL || ' AND A.FL_LIBERACAO = ''' || PM_SITUACAO || ''' ';
		END IF;
		
		IF PM_CODANA <> 0 THEN
			STRSQL := STRSQL || ' AND A.COD_USUARIO_CREDITO = ' || PM_CODANA ;
		END IF;
		
		STRSQL := STRSQL || ' group by b.NOME_USUARIO, A.ANO_MES,C.MEDIA ';
		STRSQL := STRSQL || ' order by b.NOME_USUARIO, a.ano_mes ';
/*
		STRSQL := STRSQL || ' SELECT ';
		STRSQL := STRSQL || ' b.NOME_USUARIO "OPERADOR", ';
		STRSQL := STRSQL || ' TO_CHAR(A.DT_LIBERACAO,''RRRRMM'') "DATA LIBERA��O",';
		STRSQL := STRSQL || ' COUNT(A.NUM_PEDIDO) "PEDIDOS"	,';
		STRSQL := STRSQL || ' C.MEDIAMESES "M�DIA"';
		STRSQL := STRSQL || ' from ';
		STRSQL := STRSQL || ' PRODUCAO.PEDNOTA_VENDA P,';
		STRSQL := STRSQL || ' HELPDESK.USUARIO B';
		STRSQL := STRSQL || ' ,(SELECT COUNT(NUM_PEDIDO)/4 MEDIAMESES , TO_NUMBER(TO_CHAR(DT_LIBERACAO,''RRRRMM'')) MESES';
		STRSQL := STRSQL || ' FROM PRODUCAO.R_PEDIDO_CREDITO   ';
		STRSQL := STRSQL || ' WHERE DT_LIBERACAO >= TRUNC(ADD_MONTHS(SYSDATE,-5),''MONTH'')';
		STRSQL := STRSQL || ' AND COD_USUARIO_RESPONSAVEL+0 = 0 ';
		STRSQL := STRSQL || ' AND COD_USUARIO_CREDITO+0 <> 0 ';
		STRSQL := STRSQL || ' GROUP BY TO_NUMBER(TO_CHAR(DT_LIBERACAO,''RRRRMM''))) C,';
		STRSQL := STRSQL || ' PRODUCAO.R_PEDIDO_CREDITO A ';
		STRSQL := STRSQL || ' where  ';
		STRSQL := STRSQL || ' C.MESES        = TO_NUMBER(TO_CHAR(A.DT_LIBERACAO,''RRRRMM'')) AND';
		STRSQL := STRSQL || ' P.COD_LOJA     = A.COD_LOJA AND';
		STRSQL := STRSQL || ' P.NUM_PEDIDO   = A.NUM_PEDIDO AND';
		STRSQL := STRSQL || ' P.SEQ_PEDIDO   = A.SEQ_PEDIDO AND';
		STRSQL := STRSQL || ' B.cod_usuario  = A.cod_usuario_credito AND ';
		STRSQL := STRSQL || ' A.DT_LIBERACAO >= TRUNC(ADD_MONTHS(SYSDATE,-5),''MONTH'') AND';
		STRSQL := STRSQL || ' A.COD_USUARIO_RESPONSAVEL+0 = 0	';
		STRSQL := STRSQL || ' GROUP BY ';
		STRSQL := STRSQL || ' b.NOME_USUARIO, TO_CHAR(A.DT_LIBERACAO,''RRRRMM''),C.MEDIAMESES';
		STRSQL := STRSQL || ' ORDER BY NOME_USUARIO,TO_CHAR(A.DT_LIBERACAO,''RRRRMM''),3';*/
		
		
		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR 
		STRSQL;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	end pr_CON_DESEMPINT;
	
	
	PROCEDURE pr_CON_BLOQUEIOS( PM_SITUACAO IN VARCHAR2,
								PM_INICIO   IN VARCHAR2,
								PM_FIM      IN VARCHAR2,
								PM_CURSOR1  IN OUT tp_Cursor,
				  				PM_CODERRO  OUT NUMBER,
				  				PM_TXTERRO  OUT VARCHAR2) AS
				  				
			STRSQL VARCHAR2(2000);
			
			BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := STRSQL || ' select ';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_PROTESTO,''S''),''N'',0,1)),0) PROTESTO,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_JUROS,''S''),''N'',0,1)),0) JUROS,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_CHEQUE,''S''),''N'',0,1)),0) CHEQUE,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_COMPRA,''S''),''N'',0,1)),0) COMPRA,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_DUPLICATA,''S''),''N'',0,1)),0) DUPLICATA,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_LIMITE,''S''),''N'',0,1)),0) LIMITE,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_SALDO,''S''),''N'',0,1)),0) SALDO,';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_CLIE_NOVO,''S''),''N'',0,1)),0) "CLIENTE NOVO",';
			STRSQL := STRSQL || ' nvl(sum(decode(NVL(a.BLOQ_PROTESTO,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_JUROS,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_CHEQUE,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_COMPRA,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_DUPLICATA,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_LIMITE,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_SALDO,''S''),''N'',0,1))+';
			STRSQL := STRSQL || ' sum(decode(NVL(a.BLOQ_CLIE_NOVO,''S''),''N'',0,1)),0) TOTAL';
			STRSQL := STRSQL || ' from PRODUCAO.pednota_venda a, PRODUCAO.R_PEDIDO_CREDITO B';
			STRSQL := STRSQL || ' where';
			STRSQL := STRSQL || ' A.NUM_PEDIDO = B.NUM_PEDIDO AND';
			STRSQL := STRSQL || ' A.COD_LOJA = B.COD_LOJA AND';
			STRSQL := STRSQL || ' A.SEQ_PEDIDO = B.SEQ_PEDIDO AND';
			STRSQL := STRSQL || ' (a.BLOQ_PROTESTO <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_JUROS <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_CHEQUE <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_COMPRA <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_DUPLICATA <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_LIMITE <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_SALDO <> ''N'' or';
			STRSQL := STRSQL || ' a.BLOQ_CLIE_NOVO <> ''N'')';
			
			IF PM_INICIO IS NOT NULL THEN
				STRSQL := STRSQL || ' and TO_DATE(DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
			END IF ;
	
			IF PM_FIM IS NOT NULL THEN
				STRSQL := STRSQL || ' and TO_DATE(DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
			END IF ;
			
			IF PM_SITUACAO IS NOT NULL THEN
				STRSQL := STRSQL || ' AND FL_LIBERACAO = ''' || PM_SITUACAO || ''' ';
			END IF;
			
			-- Abrindo Cursor
			OPEN PM_CURSOR1 FOR 
			STRSQL;
			
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	end pr_CON_BLOQUEIOS;
	
	
	PROCEDURE pr_CON_PEDMEDIA( PM_SITUACAO IN VARCHAR2,
							   PM_CURSOR1  IN OUT tp_Cursor,
				  			   PM_CODERRO  OUT NUMBER,
				  			   PM_TXTERRO  OUT VARCHAR2) AS
				  			   
			STRSQL VARCHAR2(2000);
			
			BEGIN
			
			PM_CODERRO := 0;
			
			STRSQL := STRSQL || 'SELECT sum(AA.TOTAL_PEDIDOS)/4 MEDIAMESES, AA.ANO_MES MESES ';
			STRSQL := STRSQL || ' FROM producao.desempenho_credito AA ';
			STRSQL := STRSQL || ' WHERE AA.ANO_MES >= TO_CHAR(ADD_MONTHS(SYSDATE,-6),''RRRRMM'') ';
			IF PM_SITUACAO IS NOT NULL THEN
				STRSQL := STRSQL || ' AND AA.FL_LIBERACAO = ''' || PM_SITUACAO || ''' ';
			END IF;
			STRSQL := STRSQL || ' GROUP BY AA.ANO_MES';
			
			-- Abrindo Cursor
			OPEN PM_CURSOR1 FOR 
			STRSQL;
			
			/*SELECT COUNT(NUM_PEDIDO)/4 MEDIAMESES , TO_NUMBER(TO_CHAR(DT_LIBERACAO,'RRRRMM')) MESES
			FROM PRODUCAO.R_PEDIDO_CREDITO   
			WHERE DT_LIBERACAO >= TRUNC(ADD_MONTHS(SYSDATE,-5),'MONTH')
			AND COD_USUARIO_RESPONSAVEL+0 = 0 
			AND COD_USUARIO_CREDITO+0 <> 0 
			GROUP BY TO_NUMBER(TO_CHAR(DT_LIBERACAO,'RRRRMM'));*/
			
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	end pr_CON_PEDMEDIA;
	
	
	PROCEDURE pr_CON_TOTPED( PM_SITUACAO IN VARCHAR2,
							 PM_INICIO   IN VARCHAR2,
							 PM_FIM      IN VARCHAR2,
							 PM_CURSOR1  IN OUT tp_Cursor,
				  			 PM_CODERRO  OUT NUMBER,
				  			 PM_TXTERRO  OUT VARCHAR2) AS
				  				
			STRSQL VARCHAR2(2000);
			
			BEGIN
			
			PM_CODERRO := 0;
			
			
			STRSQL := STRSQL || ' select ';
			STRSQL := STRSQL || ' COUNT(A.NUM_PEDIDO) TOTAL';
			STRSQL := STRSQL || ' from PRODUCAO.pednota_venda a, PRODUCAO.R_PEDIDO_CREDITO B';
			STRSQL := STRSQL || ' where';
			STRSQL := STRSQL || ' A.NUM_PEDIDO = B.NUM_PEDIDO AND';
			STRSQL := STRSQL || ' A.COD_LOJA = B.COD_LOJA AND';
			STRSQL := STRSQL || ' A.SEQ_PEDIDO = B.SEQ_PEDIDO';
			
			IF PM_INICIO IS NOT NULL THEN
				STRSQL := STRSQL || ' and TO_DATE(DT_LIBERACAO,''DD/MM/RRRR'') >= TO_DATE(''' || PM_INICIO || ''',''DD/MM/RRRR'')';
			END IF ;
	
			IF PM_FIM IS NOT NULL THEN
				STRSQL := STRSQL || ' and TO_DATE(DT_LIBERACAO,''DD/MM/RRRR'') <= TO_DATE(''' || PM_FIM || ''',''DD/MM/RRRR'')';
			END IF ;
			
			IF PM_SITUACAO IS NOT NULL THEN
				STRSQL := STRSQL || ' AND FL_LIBERACAO = ''' || PM_SITUACAO || ''' ';
			END IF;
			
			-- Abrindo Cursor
			OPEN PM_CURSOR1 FOR 
			STRSQL;
			
		EXCEPTION
			WHEN OTHERS THEN
			ROLLBACK;
			PM_CODERRO := SQLCODE;
			PM_TXTERRO := SQLERRM;
			
	end pr_CON_TOTPED;


End PCK_VDA770;
---------------
/