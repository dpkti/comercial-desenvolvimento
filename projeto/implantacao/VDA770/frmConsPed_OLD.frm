VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmConsPed 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta por Pedido e Loja"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6510
   Icon            =   "frmConsPed.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   372
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   434
   Begin VB.Frame Frame1 
      Caption         =   "Resultado da Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2760
      Left            =   45
      TabIndex        =   7
      Top             =   2430
      Width           =   6405
      Begin VB.TextBox txtGerente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3915
         TabIndex        =   20
         Top             =   2205
         Width           =   2310
      End
      Begin VB.TextBox txtAutorizado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   19
         Top             =   2205
         Width           =   1680
      End
      Begin VB.TextBox txtSituacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   18
         Top             =   1755
         Width           =   1680
      End
      Begin VB.TextBox txtHorario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   17
         Top             =   1260
         Width           =   1680
      End
      Begin VB.TextBox txtData 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   16
         Top             =   810
         Width           =   1680
      End
      Begin VB.TextBox txtLiberado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   15
         Top             =   360
         Width           =   4290
      End
      Begin VB.Line Line2 
         X1              =   3690
         X2              =   3825
         Y1              =   2385
         Y2              =   2385
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Autorizado por:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   585
         TabIndex        =   12
         Top             =   2295
         Width           =   1275
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o do Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   11
         Top             =   1845
         Width           =   1635
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Hor�rio da Libera��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   10
         Top             =   1350
         Width           =   1770
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Data da Libera��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   9
         Top             =   855
         Width           =   1545
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Liberado por:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   8
         Top             =   405
         Width           =   1140
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1455
      Left            =   45
      TabIndex        =   3
      Top             =   900
      Width           =   6405
      Begin VB.TextBox txtSeqPedido 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3015
         TabIndex        =   14
         Text            =   "0"
         Top             =   900
         Width           =   330
      End
      Begin VB.TextBox txtNumPedido 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   13
         Top             =   900
         Width           =   1545
      End
      Begin VB.ComboBox cboLoja 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         TabIndex        =   6
         Text            =   "Combo1"
         Top             =   360
         Width           =   2625
      End
      Begin VB.Line Line1 
         X1              =   2790
         X2              =   2925
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "No. Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   5
         Top             =   945
         Width           =   1005
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Loja:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   675
         TabIndex        =   4
         Top             =   405
         Width           =   465
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5250
      Width           =   6510
      _ExtentX        =   11483
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10980
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   6945
      _ExtentX        =   12250
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsPed.frx":23D2
      PICN            =   "frmConsPed.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboLoja, "DEP�SITO")
    
    txtLiberado.Locked = True
    txtData.Locked = True
    txtHorario.Locked = True
    txtSituacao.Locked = True
    txtAutorizado.Locked = True
    txtGerente.Locked = True
    Line2.Visible = False
    txtGerente.Visible = False
    
End Sub

Private Sub txt1_Change()

End Sub

Private Sub txtNumPedido_GotFocus()
    txtNumPedido.Text = ""
    txtLiberado.Text = ""
    txtData.Text = ""
    txtHorario.Text = ""
    txtSituacao.Text = ""
    txtAutorizado.Text = ""
    txtGerente.Text = ""
    Line2.Visible = False
    txtGerente.Visible = False
End Sub

Private Sub txtSeqPedido_GotFocus()
    txtSeqPedido.SelStart = 0
    txtSeqPedido.SelLength = Len(txtSeqPedido)
End Sub

Private Sub txtSeqPedido_LostFocus()
    Dim CodLoja As Integer
    Dim VarLoja As String
    Dim VarDigito As String
    Dim CodResp As String

    If cboLoja = "" Then
        MsgBox "Escolha a loja para a consulta do pedido", , "Aten��o!"
        cboLoja.SetFocus
        Exit Sub
    Else
        VarLoja = cboLoja.Text
        VarDigito = InStr(VarLoja, "-") - 1
        CodLoja = Mid(VarLoja, 1, VarDigito)
    End If
    
    If txtNumPedido = "" Then
        MsgBox "Digite o n�mero do pedido a ser consultado", , "Aten��o!"
        txtNumPedido.SetFocus
        Exit Sub
    End If
    
   'Consulta os dados do pedido
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", txtNumPedido.Text, 1
    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", txtSeqPedido.Text, 1
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", CodLoja, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_DADOSPEDIDO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If Not vObjOracle.EOF Then
        CodResp = vObjOracle!COD_RESPONSAVEL
        txtLiberado.Text = vObjOracle!USUARIO
        txtData.Text = vObjOracle!DATA_LIBERACAO
        txtHorario.Text = vObjOracle!HS_LIBERACAO
        If vObjOracle!lib_can = "L" Then
            txtSituacao.Text = "Liberado"
        ElseIf vObjOracle!lib_can = "C" Then
            txtSituacao.Text = "Cancelado"
        End If
        If vObjOracle!COD_RESPONSAVEL = "0" Then
            txtAutorizado.Text = "Depto.Cr�dito"
        Else
            txtAutorizado.Text = "Ger�ncia"
            Line2.Visible = True
            txtGerente.Visible = True
           'Busca o c�digo do gerente que fez a libera��o
           txtGerente.Text = vObjOracle!NOME_GERENTE
        End If
    Else
        MsgBox "Este pedido n�o foi liberado pelo Depto. de Cr�dito de Campinas!", , "Aten��o!"
        MsgBox "Verifique a Loja escolhida e o N�mero de Pedido digitado!", , "Verifique com Aten��o!"
        cboLoja.SetFocus
    End If
    
End Sub
