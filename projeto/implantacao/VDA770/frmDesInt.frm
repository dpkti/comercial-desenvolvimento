VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmDesInt 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CONTROLE DE DESEMPENHO INTERNO"
   ClientHeight    =   8250
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8235
   Icon            =   "frmDesInt.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   550
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   549
   Begin VB.Frame fra 
      Caption         =   "Op��es para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1365
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   8115
      Begin VB.ComboBox cboSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   810
         Width           =   3120
      End
      Begin VB.ComboBox cboGerentes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   5010
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Analistas:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   6
         Top             =   405
         Width           =   825
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   5
         Top             =   855
         Width           =   825
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7920
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14023
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDesInt.frx":23D2
      PICN            =   "frmDesInt.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1035
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDesInt.frx":30C8
      PICN            =   "frmDesInt.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stb 
      Height          =   5460
      Left            =   45
      TabIndex        =   9
      Top             =   2385
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   9631
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tabela"
      TabPicture(0)   =   "frmDesInt.frx":3DBE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Grid1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Gr�fico"
      TabPicture(1)   =   "frmDesInt.frx":3DDA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Graph1"
      Tab(1).ControlCount=   1
      Begin GraphLib.Graph Graph1 
         Height          =   4740
         Left            =   -74865
         TabIndex        =   12
         Top             =   585
         Width           =   7845
         _Version        =   65536
         _ExtentX        =   13838
         _ExtentY        =   8361
         _StockProps     =   96
         BorderStyle     =   1
         GraphType       =   6
         LegendStyle     =   1
         NumPoints       =   2
         PrintStyle      =   3
         RandomData      =   0
         ColorData       =   0
         ExtraData       =   0
         ExtraData[]     =   0
         FontFamily      =   4
         FontSize        =   4
         FontSize[0]     =   200
         FontSize[1]     =   150
         FontSize[2]     =   100
         FontSize[3]     =   100
         FontStyle       =   4
         GraphData       =   1
         GraphData[]     =   2
         GraphData[0,0]  =   0
         GraphData[0,1]  =   0
         LabelText       =   0
         LegendText      =   0
         PatternData     =   0
         SymbolData      =   0
         XPosData        =   0
         XPosData[]      =   0
      End
      Begin MSGrid.Grid Grid1 
         Height          =   4740
         Left            =   135
         TabIndex        =   10
         Top             =   585
         Width           =   7845
         _Version        =   65536
         _ExtentX        =   13838
         _ExtentY        =   8361
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1755
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDesInt.frx":3DF6
      PICN            =   "frmDesInt.frx":3E12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDesInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vOBJMedia As Object

Private Sub cmd1_Click()
    Dim varCombo
    Dim varNumGer
    Dim varSituacao
    
    Screen.MousePointer = 11
    stbBarra.Panels(1).Text = "Aguarde.."
    DoEvents
    
   'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo Grid1
    
    stb.Enabled = False
    cmd2.Enabled = False
    
    varCombo = cboGerentes.Text
    If varCombo = "" Or varCombo = "TODOS" Then
        ResultNumGer = 0
    Else
        varNumGer = InStr(varCombo, "-") - 1
        ResultNumGer = Trim(Mid(varCombo, 1, varNumGer))
    End If
    
    varCombo = cboSituacao.Text
    If varCombo = "" Then
        varSituacao = Null
    Else
        varSituacao = Left(varCombo, 1)
        If varSituacao = "T" Then
            varSituacao = Null
        End If
    End If
    
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", varSituacao, 1
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDMEDIA(:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vOBJMedia = vBanco.Parameters("PM_CURSOR1").Value
    
   
    vBanco.Parameters.Remove "PM_CODANA"
    vBanco.Parameters.Add "PM_CODANA", ResultNumGer, 1
    
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", varSituacao, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_DESEMPINT(:PM_CODANA,:PM_SITUACAO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF() Then
        MsgBox "N�o h� registros para esta consulta."
    Else
        
        stb.Enabled = True
        cmd2.Enabled = True
        
        vVB_Generica_001.CarregaGridTabela Grid1, vObjOracle, 5
        
        Graph1.AutoInc = 0
        Graph1.ThisPoint = 1
        Graph1.IndexStyle = gphEnhanced
        Graph1.ThisPoint = 1
        Graph1.DataReset = gphAllData
        Graph1.DataReset = gphLegendText
        Graph1.DataReset = gphLabelText

        Graph1.FontUse = gphLabels
        Graph1.FontFamily = gphSwiss
        Graph1.FontSize = 90

        Graph1.FontUse = gphLegend
        Graph1.FontFamily = gphSwiss
        Graph1.FontSize = 80
        
        If ResultNumGer = 0 Then
        
            Graph1.NumSets = cboGerentes.ListCount + 1
            Graph1.NumPoints = cboGerentes.ListCount + 1

            vObjOracle.MoveFirst
            AUX = vObjOracle("OPERADOR")
            i = 1
            j = 1
            FL = 0
            While Not vObjOracle.EOF
                If AUX = vObjOracle("OPERADOR") Then

                    If FL = 0 Then
                        Graph1.ThisSet = i
                        Graph1.ThisPoint = i
                        Graph1.LegendText = Mid(vObjOracle("OPERADOR"), 1, InStr(1, vObjOracle("OPERADOR"), " ") - 1)
                        FL = 1
                    End If
                    
                    Graph1.ThisPoint = j
                    Graph1.GraphData = vObjOracle("PEDIDOS")

                    vObjOracle.MoveNext
                    j = j + 1
                Else
                    AUX = vObjOracle("OPERADOR")
                    i = i + 1
                    j = 1
                    FL = 0
                    Graph1.DrawMode = gphDraw
                End If
            Wend

            Graph1.ThisSet = i + 1
            Graph1.ThisPoint = i + 1
            Graph1.LegendText = "M�DIA"
            vOBJMedia.MoveFirst
            i = 1
            While Not vOBJMedia.EOF
                mes = Right(vOBJMedia("MESES"), 2)
                ano = Mid(vOBJMedia("MESES"), 3, 2)
                Graph1.ThisPoint = i
                Graph1.LabelText = UCase(MonthName(mes, True)) & "/" & ano
                Graph1.GraphData = vOBJMedia("MEDIAMESES")
                vOBJMedia.MoveNext
                i = i + 1
            Wend

            Graph1.NumPoints = 6
            Graph1.DrawMode = gphDraw

        Else
            Graph1.NumSets = 2
            Graph1.NumPoints = 6
            
            nome = Mid(cboGerentes.Text, 9, Len(cboGerentes.Text))
            
            Graph1.ThisSet = 1
            Graph1.LegendText = Mid(nome, 1, InStr(1, nome, " ") - 1)
            
            vObjOracle.MoveFirst
            
            For i = 1 To 6
                
                Graph1.ThisPoint = i
                If Not vObjOracle.EOF Then
                    Graph1.GraphData = vObjOracle("PEDIDOS")
                    vObjOracle.MoveNext
                Else
                    Graph1.GraphData = 0
                End If
                    
            Next
            
            Graph1.ThisSet = 2
            Graph1.LegendText = "MEDIA"
            vOBJMedia.MoveFirst
            i = 1
            While Not vOBJMedia.EOF
                mes = Right(vOBJMedia("MESES"), 2)
                ano = Mid(vOBJMedia("MESES"), 3, 2)
                Graph1.ThisPoint = i
                Graph1.LabelText = UCase(MonthName(mes, True)) & "/" & ano
                Graph1.GraphData = vOBJMedia("MEDIAMESES")
                vOBJMedia.MoveNext
                i = i + 1
            Wend
            Graph1.DrawMode = gphDraw

        End If

    End If
    
    Screen.MousePointer = 0
    stbBarra.Panels(1).Text = ""
    DoEvents

End Sub

Private Sub cmd2_Click()
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        tipo = stb.Tab
        If tipo = 0 Then
            PRINTLISTA
        Else
            PRINTGRAFICO
        End If

    End If
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000


    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_ANALISTAS(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboGerentes, "COD", "NOME_USUARIO")
    
    cboGerentes.AddItem "TODOS"
    
    cboSituacao.AddItem "TODOS"
    cboSituacao.AddItem "LIBERADOS"
    cboSituacao.AddItem "CANCELADOS"
    
    
End Sub

Sub PRINTLISTA()

    With Grid1
        
        'GRID VAZIO
        If .Rows = 1 Then
            .Col = 0
            If .Text = "" Then
                Exit Sub
            End If
        End If
        
        'Printer.Orientation = vbPRORLandscape
        Printer.Print ""
        Printer.FontName = "Courier New"
        Printer.FontSize = 9
        
        .Row = 0
        liPagina = 0
        
        Call pImprimirCabecalholista
        
        Do
            .Row = .Row + 1
           'IMPRIMIR LINHA DE DETALHE
           
           'lstrlinha = " C�DIGO  |                    NOME                   | PEDIDOS | VL. CONT�BIL "
           
            .Col = 1:  lstrlinha = .Text & Space(35 - Len(.Text)) & ""
            .Col = 2:  lstrlinha = lstrlinha & " | " & Space(14 - Len(.Text)) & .Text & ""
            .Col = 3:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            .Col = 4:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            
            Printer.Print lstrlinha
            
            liLinhaImpressao = liLinhaImpressao + 1
            
           'VERIFICA QUEBRA DE PAGINA
            If liLinhaImpressao > 60 And .Row <> .Rows - 1 Then
                Printer.Print String(79, "-")
                Printer.EndDoc
                Printer.Print ""
                Printer.FontName = "Courier New"
                Printer.FontSize = 9
                Call pImprimirCabecalholista
                liLinhaImpressao = 0
                liPagina = liPagina + 1
            End If
        
        Loop Until .Row = .Rows - 1
        
        Printer.Print String(79, "-")
        Printer.EndDoc
    
        MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
    End With

End Sub

Sub PRINTGRAFICO()

    Graph1.DrawMode = gphPrint

End Sub


Private Sub pImprimirCabecalholista()
    
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "                              CONTROLE DE DESEMPENHO INTERNO                                   "
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(79, "-")
    Printer.Print
    
    
    If cboGerentes.Text = "" Then
        lstrlinha = " ANALISTA: TODOS"
    Else
        lstrlinha = " ANALISTA: " & cboGerentes.Text & ""
    End If
    
    Printer.Print lstrlinha
    
    If cboSituacao.Text = "" Then
        lstrlinha = " SITUA��O: TODOS"
    Else
        lstrlinha = " SITUA��O: " & cboSituacao.Text & ""
    End If
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(79, "-")
    
    lstrlinha = " OPERADOR                           | DATA LIBERA��O |  PEDIDOS   |   M�DIA  "
    
    Printer.Print lstrlinha
    Printer.Print String(79, "-")

End Sub

