VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmConsDupl 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta com Duplicatas"
   ClientHeight    =   8160
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8460
   Icon            =   "frmConsDupl.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   544
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   564
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   6900
      Left            =   45
      TabIndex        =   0
      Top             =   855
      Width           =   8340
      Begin VB.ComboBox cboGerentes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1200
         TabIndex        =   17
         Text            =   "cboGerentes"
         Top             =   1440
         Width           =   5010
      End
      Begin MSGrid.Grid Grid1 
         Height          =   4845
         Left            =   120
         TabIndex        =   14
         Top             =   1920
         Visible         =   0   'False
         Width           =   8070
         _Version        =   65536
         _ExtentX        =   14235
         _ExtentY        =   8546
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
      Begin VB.TextBox txtFimDupl 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2925
         TabIndex        =   2
         Top             =   405
         Width           =   1455
      End
      Begin VB.TextBox txtInicioDupl 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   1
         Top             =   405
         Width           =   1455
      End
      Begin VB.ComboBox cboGerentesDupl 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         TabIndex        =   3
         Text            =   "cboGerentesDupl"
         Top             =   900
         Width           =   5010
      End
      Begin MSGrid.Grid grdDuplicatas 
         Height          =   4845
         Left            =   135
         TabIndex        =   4
         Top             =   1920
         Width           =   8070
         _Version        =   65536
         _ExtentX        =   14235
         _ExtentY        =   8546
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Analistas:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   240
         TabIndex        =   16
         Top             =   1440
         Width           =   780
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "a"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2745
         TabIndex        =   13
         Top             =   450
         Width           =   150
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Periodo de:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   12
         Top             =   450
         Width           =   960
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Gerentes:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   5
         Top             =   945
         Width           =   915
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   6
      Top             =   7830
      Width           =   8460
      _ExtentX        =   14923
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14420
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   -3420
      TabIndex        =   7
      Top             =   765
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Fechar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsDupl.frx":23D2
      PICN            =   "frmConsDupl.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1215
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsDupl.frx":30C8
      PICN            =   "frmConsDupl.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1980
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsDupl.frx":3DBE
      PICN            =   "frmConsDupl.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2745
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsDupl.frx":46B4
      PICN            =   "frmConsDupl.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   690
      Left            =   3510
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsDupl.frx":53AA
      PICN            =   "frmConsDupl.frx":53C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsDupl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vObjRS As Object
Dim DataInicioDupl As String
Dim DataFimDupl As String
Dim ResultNumGerDupl
Dim FL As Integer

Private Sub cmd1_Click()
    Dim varComboDupl
    Dim varNumGerDupl
    
    Dim varComboDupl_ana
    Dim varNumGerDupl_ana
    
    Dim vLoja As Double
    Dim vNumero As String
    Dim vSequencia As String
    Dim count
    
    vVB_Generica_001.LimpaGridComTitulo Grid1
    Grid1.Visible = False
    cmd4.Visible = False
    
   'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdDuplicatas
    cmd3.Enabled = False
   
    If txtInicioDupl.Text = "" Or txtFimDupl.Text = "" Then
        MsgBox "Digite o per�odo a ser consultado."
        txtInicioDupl.SetFocus
        Exit Sub
    End If
    
    If cboGerentesDupl.Text = "" And cboGerentes = "" Then
        MsgBox "Escolha o supervisor/gerente ou uma analista para realizar a consulta"
        cboGerentesDupl.SetFocus
        Exit Sub
    End If
    
    If cboGerentesDupl.Text <> "" And cboGerentes <> "" Then
        MsgBox "Escolha um supervisor/gerente ou uma analista para realizar a consulta, n�o � permitido selecionar os dois"
        cboGerentesDupl.Text = ""
        cboGerentes.Text = ""
        cboGerentesDupl.SetFocus
        Exit Sub
    End If
    
    
    Screen.MousePointer = 11

    DataInicioDupl = txtInicioDupl.Text
    DataFimDupl = txtFimDupl.Text
  
    If cboGerentesDupl <> "" Then
      varComboDupl = cboGerentesDupl.Text
      varNumGerDupl = InStr(varComboDupl, "-") - 1
      ResultNumGerDupl = Trim(Mid(varComboDupl, 1, varNumGerDupl))
    End If
    
    If cboGerentes <> "" Then
      varComboDupl_ana = cboGerentes.Text
      varNumGerDupl_ana = InStr(varComboDupl_ana, "-") - 1
      ResultNumGerDupl_ana = Trim(Mid(varComboDupl_ana, 1, varNumGerDupl_ana))
    End If

    vBanco.Parameters.Remove "PM_CODGER"
    vBanco.Parameters.Add "PM_CODGER", IIf(ResultNumGerDupl = Empty, 0, ResultNumGerDupl), 1
    
    vBanco.Parameters.Remove "PM_CODANA"
    vBanco.Parameters.Add "PM_CODANA", IIf(ResultNumGerDupl_ana = Empty, 0, ResultNumGerDupl_ana), 1
    
    vBanco.Parameters.Remove "PM_DTINICIO"
    vBanco.Parameters.Add "PM_DTINICIO", DataInicioDupl, 1
    
    vBanco.Parameters.Remove "PM_DTFIM"
    vBanco.Parameters.Add "PM_DTFIM", DataFimDupl, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDIDOSGERDATA(:PM_CODGER,:PM_CODANA, :PM_DTINICIO,:PM_DTFIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        FL = 1
    Else
        FL = 0
        cmd3.Enabled = True
    End If
    
    vVB_Generica_001.CarregaGridTabela grdDuplicatas, vObjOracle, 5
    
    Screen.MousePointer = 0
End Sub

Private Sub cmd2_Click()

    'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdDuplicatas
    cmd3.Enabled = False

End Sub

Private Sub cmd3_Click()

    
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
        If Grid1.Visible Then
            PRINTDETALHE
        Else
            PRINTLISTA
        End If
        
    End If

End Sub

Private Sub cmd4_Click()

    vVB_Generica_001.LimpaGridComTitulo Grid1
    Grid1.Visible = False
    cmd4.Visible = False

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_ANALISTAS(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboGerentes, "COD", "NOME_USUARIO")
    
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    'vBanco.Parameters.Remove "PM_TXTERRO"
    'vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.PR_RESPONSAVEL(:PM_CURSOR1,:PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboGerentesDupl, "COD", "NOME_USUARIO")
    
End Sub


Private Sub grdDuplicatas_DblClick()
    
    If FL = 0 Then
    
         Screen.MousePointer = 11

         grdDuplicatas.Col = 1
         cli = grdDuplicatas.Text
         
         vBanco.Parameters.Remove "PM_CODGER"
         vBanco.Parameters.Add "PM_CODGER", CInt(ResultNumGerDupl), 1
         
         vBanco.Parameters.Remove "PM_CODANA"
         vBanco.Parameters.Add "PM_CODANA", CInt(ResultNumGerDupl_ana), 1
         
         vBanco.Parameters.Remove "PM_CODCLI"
         vBanco.Parameters.Add "PM_CODCLI", cli, 1
         
         vBanco.Parameters.Remove "PM_DTINICIO"
         vBanco.Parameters.Add "PM_DTINICIO", DataInicioDupl, 1
         
         vBanco.Parameters.Remove "PM_DTFIM"
         vBanco.Parameters.Add "PM_DTFIM", DataFimDupl, 1
        
         vBanco.Parameters.Remove "PM_CURSOR1"
         vBanco.Parameters.Add "PM_CURSOR1", 0, 3
         vBanco.Parameters("PM_CURSOR1").ServerType = 102
         vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
         vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
         vBanco.Parameters.Remove "PM_CODERRO"
         vBanco.Parameters.Add "PM_CODERRO", 0, 2
         vBanco.Parameters.Remove "PM_TXTERRO"
         vBanco.Parameters.Add "PM_TXTERRO", "", 2
         
         vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDIDOSGERDATADET(:PM_CODGER,:PM_CODANA,:PM_CODCLI,:PM_DTINICIO,:PM_DTFIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
         
         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
         Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
         
         vVB_Generica_001.CarregaGridTabela Grid1, vObjOracle, 10
         
         If Not vObjOracle.EOF Then
             Grid1.Visible = True
             cmd4.Visible = True
         End If
         
         Screen.MousePointer = 0
         
    End If
End Sub

Private Sub txtFimDupl_KeyPress(Keyascii As Integer)

    DATA Keyascii, txtFimDupl

End Sub

Private Sub txtInicioDupl_KeyPress(Keyascii As Integer)

    DATA Keyascii, txtInicioDupl

End Sub

Private Sub pImprimirCabecalhoDupl()

    'Printer.Orientation = vbPRORLandscape
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "                     DUPLICATAS VENCIDAS DE CLIENTES COM PEDIDOS LIBERADOS                      "
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(116, "-")
    Printer.Print
    
    lstrlinha = "PER�ODO DE: " & Format(txtInicioDupl.Text, "dd/mm/yy") & ""
    lstrlinha = lstrlinha & " � " & Format(txtFimDupl.Text, "dd/mm/yy") & ""
    
    Printer.Print lstrlinha
    
    lstrlinha = "GERENTE/SUPERVISOR: " & Mid(cboGerentesDupl.Text, 6)
    
    Printer.Print lstrlinha
    
    lstrlinha = "ANALISTA: " & Mid(cboGerentes.Text, 6)
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(116, "-")
    
    lstrlinha = ""
    lstrlinha = "LOJA | NUM.PEDIDO | SEQU�NCIA |  LIBERA��O  |   EMISS�O   | VENCIMENTO | ATRASO | VL. ABERTO | OPERADOR"
    Printer.Print lstrlinha
    Printer.Print String(116, "-")
    
End Sub

Sub PRINTDETALHE()


With Grid1
            
             'GRID VAZIO
            If .Rows = 1 Then
                .Col = 1
                If .Text = "" Then
                    Exit Sub
                End If
            End If
            
            Printer.Orientation = vbPRORLandscape
            Printer.Print ""
            Printer.FontName = "Courier New"
            Printer.FontSize = 9
            
            .Row = 0
            liPagina = 0
            
            Call pImprimirCabecalhoDupl
            
            Do
                .Row = .Row + 1
                'lstrlinha = "LOJA | NUM.PEDIDO | SEQU�NCIA |  LIBERA��O  |   EMISS�O   | VENCIMENTO | ATRASO | VL. ABERTO | OPERADOR"
                'IMPRIMIR LINHA DE DETALHE
                .Col = 1:  lstrlinha = Space(4 - Len(Format(.Text, "00"))) & Format(.Text, "00")
                .Col = 2:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
                .Col = 3:  lstrlinha = lstrlinha & " | " & Space(9 - Len(.Text)) & .Text & ""
                .Col = 4:  lstrlinha = lstrlinha & " | " & Space(11 - Len(.Text)) & .Text & ""
                .Col = 5:  lstrlinha = lstrlinha & " | " & Space(11 - Len(.Text)) & .Text & ""
                .Col = 6:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
                .Col = 7:  lstrlinha = lstrlinha & " | " & Space(6 - Len(.Text)) & .Text & ""
                .Col = 8:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
                .Col = 9:  lstrlinha = lstrlinha & " | " & .Text & ""
                
                Printer.Print lstrlinha
                
                liLinhaImpressao = liLinhaImpressao + 1
                
               'VERIFICA QUEBRA DE PAGINA
                If liLinhaImpressao > 56 And .Row <> .Rows - 1 Then
                    Printer.Print String(116, "-")
                    Printer.EndDoc
                    Printer.Print ""
                    Printer.FontName = "Courier New"
                    Printer.FontSize = 9
                    Call pImprimirCabecalhoDupl
                    liLinhaImpressao = 0
                    liPagina = liPagina + 1
                End If
            
            Loop Until .Row = .Rows - 1
            
            Printer.Print String(116, "-")
            Printer.EndDoc

            MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
        End With


End Sub

Sub PRINTLISTA()


    With grdDuplicatas
        
        'GRID VAZIO
        If .Rows = 1 Then
            .Col = 0
            If .Text = "" Then
                Exit Sub
            End If
        End If
        
        'Printer.Orientation = vbPRORLandscape
        Printer.Print ""
        Printer.FontName = "Courier New"
        Printer.FontSize = 9
        
        .Row = 0
        liPagina = 0
        
        Call pImprimirCabecalholista
        
        Do
            .Row = .Row + 1
           'IMPRIMIR LINHA DE DETALHE
           
           'lstrlinha = " C�DIGO  |                    NOME                   | VL. ABERTO | ATRASO "
           
            .Col = 1:  lstrlinha = Space(8 - Len(.Text)) & .Text & ""
            .Col = 2:  lstrlinha = lstrlinha & " | " & .Text & Space(41 - Len(.Text)) & ""
            .Col = 3:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            .Col = 4:  lstrlinha = lstrlinha & " | " & Space(5 - Len(.Text)) & .Text & ""
            
            Printer.Print lstrlinha
            
            liLinhaImpressao = liLinhaImpressao + 1
            
           'VERIFICA QUEBRA DE PAGINA
            If liLinhaImpressao > 60 And .Row <> .Rows - 1 Then
                Printer.Print String(75, "-")
                Printer.EndDoc
                Printer.Print ""
                Printer.FontName = "Courier New"
                Printer.FontSize = 9
                Call pImprimirCabecalholista
                liLinhaImpressao = 0
                liPagina = liPagina + 1
            End If
        
        Loop Until .Row = .Rows - 1
        
        Printer.Print String(75, "-")
        Printer.EndDoc
    
        MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
    End With


End Sub



Private Sub pImprimirCabecalholista()
    
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "         DUPLICATAS VENCIDAS DE CLIENTES COM PEDIDOS LIBERADOS"
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(75, "-")
    Printer.Print
    
    lstrlinha = "PER�ODO DE: " & Format(txtInicioDupl.Text, "dd/mm/yy") & ""
    lstrlinha = lstrlinha & " � " & Format(txtFimDupl.Text, "dd/mm/yy") & ""
    
    Printer.Print lstrlinha
    
    lstrlinha = "GERENTE/SUPERVISOR: " & Mid(cboGerentesDupl.Text, 6)
    
    Printer.Print lstrlinha
    
    lstrlinha = "ANALISTA: " & Mid(cboGerentes.Text, 6)
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(75, "-")
    
    lstrlinha = " C�DIGO  |                    NOME                   | VL. ABERTO | ATRASO "
    
    Printer.Print lstrlinha
    Printer.Print String(75, "-")
    
End Sub
