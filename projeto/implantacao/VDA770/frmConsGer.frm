VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmConsGer 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta por Per�odo e Gerente"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8445
   Icon            =   "frmConsGer.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   547
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   563
   Begin MSComDlg.CommonDialog cd 
      Left            =   7200
      Top             =   180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   6900
      Left            =   45
      TabIndex        =   3
      Top             =   900
      Width           =   8340
      Begin VB.TextBox txtFim 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3285
         TabIndex        =   13
         Top             =   810
         Width           =   1545
      End
      Begin VB.TextBox txtInicio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1170
         TabIndex        =   11
         Top             =   810
         Width           =   1545
      End
      Begin VB.ComboBox cboGerentes 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1170
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   5010
      End
      Begin MSGrid.Grid grdGerente 
         Height          =   5460
         Left            =   135
         TabIndex        =   6
         Top             =   1305
         Width           =   8070
         _Version        =   65536
         _ExtentX        =   14235
         _ExtentY        =   9631
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
      Begin MSGrid.Grid Grid1 
         Height          =   5460
         Left            =   135
         TabIndex        =   15
         Top             =   1305
         Visible         =   0   'False
         Width           =   8070
         _Version        =   65536
         _ExtentX        =   14235
         _ExtentY        =   9631
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "at�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2925
         TabIndex        =   14
         Top             =   855
         Width           =   1005
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Periodo de:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   12
         Top             =   855
         Width           =   1005
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Gerentes:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   5
         Top             =   405
         Width           =   915
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14393
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   90
      TabIndex        =   1
      Top             =   810
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Fechar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":23D2
      PICN            =   "frmConsGer.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1215
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":30C8
      PICN            =   "frmConsGer.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1980
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":3DBE
      PICN            =   "frmConsGer.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2745
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":46B4
      PICN            =   "frmConsGer.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd4 
      Height          =   690
      Left            =   3510
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":53AA
      PICN            =   "frmConsGer.frx":53C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd5 
      Height          =   690
      Left            =   4275
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsGer.frx":60A0
      PICN            =   "frmConsGer.frx":60BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsGer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ResultNumGer
Dim vInicio As String
Dim vFim As String
Dim grdAux As Object
Dim FL As Integer


Private Sub cmd1_Click()
Dim varCombo
    Dim varNumGer
    
    Dim DataInicio As String
    Dim DataFim As String
    
    Screen.MousePointer = 11
    cmd4.Enabled = False
    cmd3.Enabled = False
    
   'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdGerente
    
    vVB_Generica_001.LimpaGridComTitulo Grid1
    Grid1.Visible = False
    cmd5.Visible = False
    
    'grdGerente.Visible = False
    
    varCombo = cboGerentes.Text
    If varCombo = "" Then
        ResultNumGer = 0
    Else
        varNumGer = InStr(varCombo, "-") - 1
        ResultNumGer = Trim(Mid(varCombo, 1, varNumGer))
    End If
    
    If txtInicio.Text = "" Then
        vInicio = ""
    Else
        vInicio = txtInicio.Text
    End If
    If txtFim.Text = "" Then
        vFim = ""
    Else
        vFim = txtFim.Text
    End If
   
    vBanco.Parameters.Remove "PM_CODGER"
    vBanco.Parameters.Add "PM_CODGER", ResultNumGer, 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Add "PM_INICIO", vInicio, 1
    
    vBanco.Parameters.Remove "PM_FIM"
    vBanco.Parameters.Add "PM_FIM", vFim, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDIDOGER(:PM_CODGER,:PM_INICIO,:PM_FIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF() Then
        MsgBox "N�o h� registros para esta consulta."
        FL = 1
    Else
        FL = 0
        vVB_Generica_001.CarregaGridTabela grdGerente, vObjOracle, 6
        
        LINHAS = grdGerente.Rows
        grdGerente.Rows = LINHAS + 3
        
        grdGerente.Col = 3
        
        grdGerente.Row = LINHAS + 1
        grdGerente.Text = "% INAD."
        
        vObjOracle.MoveFirst
        
        TOTINA = 0
        TOTPED = 0
        While Not vObjOracle.EOF
            TOTPED = CDbl(TOTPED) + CDbl(vObjOracle("VL. CONT�BIL"))
            TOTINA = CDbl(TOTINA) + CDbl(Val(vObjOracle("VL. VENCIDO")))
            vObjOracle.MoveNext
        Wend
        
        PERCENTINA = (TOTINA * 100) / TOTPED
        
        grdGerente.Col = 4
        grdGerente.Text = Replace(FormatNumber(PERCENTINA, 2), ",", ".")
        
        cmd4.Enabled = True
        cmd3.Enabled = True
    End If
    
    If vObjOracle.EOF() And grdGerente.Rows >= 3 Then
        grdGerente.RemoveItem 1
    End If
    
    Screen.MousePointer = 0

End Sub

Private Sub cmd2_Click()
    
    vVB_Generica_001.LimpaGridComTitulo grdGerente
    vVB_Generica_001.LimpaGridComTitulo Grid1
    Grid1.Visible = False
    
    txtFim.Text = ""
    txtInicio.Text = ""
    
    cmd4.Enabled = False
    cmd3.Enabled = False
    
    cboGerentes.SetFocus
    
End Sub

Private Sub cmd3_Click()
    
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
        If Grid1.Visible Then
            PRINTDETALHE
        Else
            PRINTLISTA
        End If
        
    End If
End Sub

Private Sub cmd4_Click()

    Dim j As Long           'Ponteiro de caracter no campo observa��o
    Dim sGER As String   'Guarda o c�digo de DPK para impress�o
    Dim liLinhaGrid, liColunaGrid As Integer
    Dim vPath As String
    
    liLinhaImpressao = 0
    
    vPath = vbGetBrowseDirectory
    
    If vPath <> "" Then
    
        If Grid1.Visible Then
            Set grdAux = Grid1
        Else
            Set grdAux = grdGerente
        End If
    
        Screen.MousePointer = 11
      
       'Gera arquivo com o nome CONSDDMMAAHHMMSS.TXT
        vdt_cad = Date
        vhs_cad = Time
        Open vPath & Mid(vdt_cad, 1, 2) & Mid(vdt_cad, 4, 2) & Mid(vdt_cad, 7, 2) & _
                                  Mid(vhs_cad, 1, 2) & Mid(vhs_cad, 4, 2) & Mid(vhs_cad, 7, 2) & ".txt" For Output As #1
        
        With grdAux
        
        Print #1, "RELAT�RIO DE CONSULTA - LOG DE CR�DITO "
        Print #1, ""
        
        lstrlinha = ""
        .Row = 0
        For i = 1 To .Cols - 1
            .Col = i
            lstrlinha = lstrlinha & .Text & ";"
        Next
        lstrlinha = Left(lstrlinha, Len(lstrlinha) - 1)
         
        Print #1, lstrlinha
        Print #1, ""
        lstrlinha = ""
            Do
                .Row = .Row + 1
                
                lstrlinha = ""
                For i = 1 To .Cols - 1
                    .Col = i
                    lstrlinha = lstrlinha & .Text & ";"
                Next
                lstrlinha = Left(lstrlinha, Len(lstrlinha) - 1)
                
               'GRAVAR LINHA DE DETALHE
               ' .Col = 0:  lstrlinha = Format(.Text, "00") & ";"
               ' .Col = 1:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 2:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 3:  lstrlinha = lstrlinha & Format(.Text, "dd/mm/yy") & ";"
               ' .Col = 4:  lstrlinha = lstrlinha & Format(.Text, "hh:mm:ss") & ";"
               ' .Col = 5:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 6:  lstrlinha = lstrlinha & .Text
                
                Print #1, lstrlinha
                
            Loop Until .Row = .Rows - 1
            
        End With
        
        Close #1
    
        Screen.MousePointer = 0
        MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
    
    End If
            

End Sub

Private Sub cmd5_Click()

    vVB_Generica_001.LimpaGridComTitulo Grid1
    Grid1.Visible = False
    cmd5.Visible = False

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    'vBanco.Parameters.Remove "PM_TXTERRO"
    'vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.PR_RESPONSAVEL(:PM_CURSOR1,:PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboGerentes, "COD", "NOME_USUARIO")
    
End Sub



Private Sub pImprimirCabecalho()
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "                                CONSULTA  GERENTE POR PER�ODO                                   "
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(140, "-")
    Printer.Print
    
    lstrlinha = " GERENTE: " & cboGerentes.Text & ""
    
    If vInicio <> "" Then
        lstrlinha = lstrlinha & "  -  PER�ODO DE: " & vInicio & ""
    End If
    
    If vFim <> "" Then
        lstrlinha = lstrlinha & " � " & vFim & ""
    End If
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(140, "-")
    
    lstrlinha = "LOJA | NUM.PEDIDO | SEQU�NCIA |   VALOR   | DATA DE LIBERA��O |  SITUA��O  | OPERADOR"
    
    Printer.Print lstrlinha
    Printer.Print String(140, "-")
    
End Sub

Private Sub grdGerente_DblClick()

    Dim cli As Double
    
    If FL = 0 Then
        Screen.MousePointer = 11
    
        grdGerente.Col = 1
        cli = grdGerente.Text
        
        vBanco.Parameters.Remove "PM_CODCLI"
        vBanco.Parameters.Add "PM_CODCLI", cli, 1
        
        vBanco.Parameters.Remove "PM_CODGER"
        vBanco.Parameters.Add "PM_CODGER", ResultNumGer, 1
        
        vBanco.Parameters.Remove "PM_INICIO"
        vBanco.Parameters.Add "PM_INICIO", vInicio, 1
        
        vBanco.Parameters.Remove "PM_FIM"
        vBanco.Parameters.Add "PM_FIM", vFim, 1
        
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
       
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
        
        vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDIDOGERDET(:PM_CODCLI,:PM_CODGER,:PM_INICIO,:PM_FIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
        
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        If Not vObjOracle.EOF Then
            Grid1.Visible = True
            cmd5.Visible = True
        End If
        
        vVB_Generica_001.CarregaGridTabela Grid1, vObjOracle, 8
    End If
    
    Screen.MousePointer = 0

End Sub


Private Sub txtFim_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtFim
End Sub

Private Sub txtInicio_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtInicio
End Sub

Sub PRINTDETALHE()

    With Grid1
        
        'GRID VAZIO
        If .Rows = 1 Then
            .Col = 0
            If .Text = "" Then
                Exit Sub
            End If
        End If
        
        Printer.Orientation = vbPRORLandscape
        Printer.Print ""
        Printer.FontName = "Courier New"
        Printer.FontSize = 9
        
        .Row = 0
        liPagina = 0
        
        Call pImprimirCabecalho
        
        Do
            .Row = .Row + 1
           'IMPRIMIR LINHA DE DETALHE
           
           'lstrlinha = "LOJA | NUM.PEDIDO | SEQU�NCIA |  VALOR  | DATA DE LIBERA��O |  SITUA��O  | OPERADOR"
           
            .Col = 1:  lstrlinha = Space(4 - Len(Format(.Text, "00"))) & Format(.Text, "00")
            .Col = 2:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            .Col = 3:  lstrlinha = lstrlinha & " | " & Space(9 - Len(.Text)) & .Text & ""
            .Col = 4:  lstrlinha = lstrlinha & " | " & Space(9 - Len(.Text)) & .Text & ""
            .Col = 5:  lstrlinha = lstrlinha & " | " & Space(17 - Len(.Text)) & .Text & ""
            .Col = 6:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            .Col = 7:  lstrlinha = lstrlinha & " | " & .Text & ""
            
            Printer.Print lstrlinha
            
            liLinhaImpressao = liLinhaImpressao + 1
            
           'VERIFICA QUEBRA DE PAGINA
            If liLinhaImpressao > 45 And .Row <> .Rows - 1 Then
                Printer.Print String(140, "-")
                Printer.EndDoc
                Printer.Print ""
                Printer.FontName = "Courier New"
                Printer.FontSize = 9
                Call pImprimirCabecalho
                liLinhaImpressao = 0
                liPagina = liPagina + 1
            End If
        
        Loop Until .Row = .Rows - 1
        
        Printer.Print String(140, "-")
        Printer.EndDoc
    
        MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
    End With

End Sub


Sub PRINTLISTA()

    With grdGerente
        
        'GRID VAZIO
        If .Rows = 1 Then
            .Col = 0
            If .Text = "" Then
                Exit Sub
            End If
        End If
        
        'Printer.Orientation = vbPRORLandscape
        Printer.Print ""
        Printer.FontName = "Courier New"
        Printer.FontSize = 9
        
        .Row = 0
        liPagina = 0
        
        Call pImprimirCabecalholista
        
        Do
            .Row = .Row + 1
           'IMPRIMIR LINHA DE DETALHE
           
           'lstrlinha = " C�DIGO  |                    NOME                   | PEDIDOS | VL. CONT�BIL "
           
            .Col = 1:  lstrlinha = Space(8 - Len(.Text)) & .Text & ""
            .Col = 2:  lstrlinha = lstrlinha & " | " & .Text & Space(41 - Len(.Text)) & ""
            .Col = 3:  lstrlinha = lstrlinha & " | " & Space(7 - Len(.Text)) & .Text & ""
            .Col = 4:  lstrlinha = lstrlinha & " | " & Space(13 - Len(.Text)) & .Text & ""
            
            Printer.Print lstrlinha
            
            liLinhaImpressao = liLinhaImpressao + 1
            
           'VERIFICA QUEBRA DE PAGINA
            If liLinhaImpressao > 60 And .Row <> .Rows - 1 Then
                Printer.Print String(79, "-")
                Printer.EndDoc
                Printer.Print ""
                Printer.FontName = "Courier New"
                Printer.FontSize = 9
                Call pImprimirCabecalholista
                liLinhaImpressao = 0
                liPagina = liPagina + 1
            End If
        
        Loop Until .Row = .Rows - 1
        
        Printer.Print String(79, "-")
        Printer.EndDoc
    
        MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
    End With

End Sub


Private Sub pImprimirCabecalholista()
    
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "                              CONSULTA GERENTE POR PER�ODO                                   "
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(79, "-")
    Printer.Print
    
    lstrlinha = " GERENTE: " & cboGerentes.Text & ""
    
    If vInicio <> "" Then
        lstrlinha = lstrlinha & "  -  PER�ODO DE: " & vInicio & ""
    End If
    
    If vFim <> "" Then
        lstrlinha = lstrlinha & " � " & vFim & ""
    End If
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(79, "-")
    
    lstrlinha = " C�DIGO  |                    NOME                   | PEDIDOS | VL. CONT�BIL "
    
    Printer.Print lstrlinha
    Printer.Print String(79, "-")
    
End Sub
