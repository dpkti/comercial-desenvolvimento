VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmCtrBloq 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CONTROLE DE BLOQUEIOS"
   ClientHeight    =   8250
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8220
   Icon            =   "frmCtrBloq.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   550
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   548
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1365
      Left            =   45
      TabIndex        =   3
      Top             =   945
      Width           =   8115
      Begin VB.TextBox txtInicio 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         TabIndex        =   10
         Top             =   360
         Width           =   1545
      End
      Begin VB.TextBox txtFim 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3375
         TabIndex        =   9
         Top             =   360
         Width           =   1545
      End
      Begin VB.ComboBox cboSituacao 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   810
         Width           =   3120
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Periodo de:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   12
         Top             =   405
         Width           =   1005
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "at�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2925
         TabIndex        =   11
         Top             =   405
         Width           =   375
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   405
         TabIndex        =   4
         Top             =   855
         Width           =   825
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7920
      Width           =   8220
      _ExtentX        =   14499
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13996
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCtrBloq.frx":23D2
      PICN            =   "frmCtrBloq.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1035
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCtrBloq.frx":30C8
      PICN            =   "frmCtrBloq.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin TabDlg.SSTab stb 
      Height          =   5460
      Left            =   45
      TabIndex        =   7
      Top             =   2385
      Width           =   8115
      _ExtentX        =   14314
      _ExtentY        =   9631
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   794
      ShowFocusRect   =   0   'False
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Tabela"
      TabPicture(0)   =   "frmCtrBloq.frx":3DBE
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Grid1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Gr�fico"
      TabPicture(1)   =   "frmCtrBloq.frx":3DDA
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Graph1"
      Tab(1).ControlCount=   1
      Begin GraphLib.Graph Graph1 
         Height          =   4740
         Left            =   -74865
         TabIndex        =   14
         Top             =   585
         Width           =   7845
         _Version        =   65536
         _ExtentX        =   13838
         _ExtentY        =   8361
         _StockProps     =   96
         BorderStyle     =   1
         GraphType       =   1
         NumPoints       =   8
         PrintStyle      =   3
         RandomData      =   1
         ColorData       =   0
         ExtraData       =   0
         ExtraData[]     =   0
         FontFamily      =   4
         FontSize        =   4
         FontSize[0]     =   200
         FontSize[1]     =   150
         FontSize[2]     =   100
         FontSize[3]     =   100
         FontStyle       =   4
         GraphData       =   0
         GraphData[]     =   0
         LabelText       =   0
         LegendText      =   0
         PatternData     =   0
         SymbolData      =   0
         XPosData        =   0
         XPosData[]      =   0
      End
      Begin MSGrid.Grid Grid1 
         Height          =   4740
         Left            =   135
         TabIndex        =   8
         Top             =   585
         Width           =   7845
         _Version        =   65536
         _ExtentX        =   13838
         _ExtentY        =   8361
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   10
         Cols            =   3
         HighLight       =   0   'False
      End
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1755
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCtrBloq.frx":3DF6
      PICN            =   "frmCtrBloq.frx":3E12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCtrBloq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmd1_Click()
    Dim varCombo
    Dim varNumGer
    Dim varSituacao
    
    Screen.MousePointer = 11
    stbBarra.Panels(1).Text = "Aguarde.."
    DoEvents
    
    stb.Enabled = False
    cmd2.Enabled = False
    
   'Limpar o grid
    'vVB_Generica_001.LimpaGridComTitulo Grid1
    
    
    If txtInicio.Text = "" Then
        vInicio = ""
    Else
        vInicio = txtInicio.Text
    End If
    If txtFim.Text = "" Then
        vFim = ""
    Else
        vFim = txtFim.Text
    End If
    
    varCombo = cboSituacao.Text
    If varCombo = "" Then
        varSituacao = Null
    Else
        varSituacao = Left(varCombo, 1)
        If varSituacao = "T" Then
            varSituacao = Null
        End If
    End If
   
    vBanco.Parameters.Remove "PM_SITUACAO"
    vBanco.Parameters.Add "PM_SITUACAO", varSituacao, 1
    
    vBanco.Parameters.Remove "PM_INICIO"
    vBanco.Parameters.Add "PM_INICIO", vInicio, 1
    
    vBanco.Parameters.Remove "PM_FIM"
    vBanco.Parameters.Add "PM_FIM", vFim, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_BLOQUEIOS(:PM_SITUACAO,:PM_INICIO,:PM_FIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF() Then
        MsgBox "N�o h� registros para esta consulta."
    Else
        
        stb.Enabled = True
        cmd2.Enabled = True
        ''vVB_Generica_001.CarregaGridTabela Grid1, vObjOracle, 5
        
        vObjOracle.MoveFirst
            
        With Grid1
        
            .Rows = 11
            .Cols = 3
        
            .Row = 0
            .Col = 0
            .ColWidth(0) = 300
            
            .Row = 0
            .Col = 1
            .Text = "TIPO BLOQUEIO"
            .ColWidth(1) = 1700
            
            .Row = 0
            .Col = 2
            .Text = "QTD"
            .ColWidth(2) = 1000
        
            .Row = 1
            .Col = 1
            .Text = "PROTESTO"
            .Row = 2
            .Col = 1
            .Text = "JUROS"
            .Row = 3
            .Col = 1
            .Text = "CHEQUE"
            .Row = 4
            .Col = 1
            .Text = "COMPRA"
            .Row = 5
            .Col = 1
            .Text = "DUPLICATA"
            .Row = 6
            .Col = 1
            .Text = "LIMITE"
            .Row = 7
            .Col = 1
            .Text = "SALDO"
            .Row = 8
            .Col = 1
            .Text = "CLIENTE NOVO"
            .Row = 9
            .Col = 1
            .Text = "TOTAL BLOQUEIOS"
            .Row = 10
            .Col = 1
            .Text = "TOTAL PEDIDOS"
            
            .Row = 1
            .Col = 2
            .Text = vObjOracle(0)
            .Row = 2
            .Col = 2
            .Text = vObjOracle(1)
            .Row = 3
            .Col = 2
            .Text = vObjOracle(2)
            .Row = 4
            .Col = 2
            .Text = vObjOracle(3)
            .Row = 5
            .Col = 2
            .Text = vObjOracle(4)
            .Row = 6
            .Col = 2
            .Text = vObjOracle(5)
            .Row = 7
            .Col = 2
            .Text = vObjOracle(6)
            .Row = 8
            .Col = 2
            .Text = vObjOracle(7)
            .Row = 9
            .Col = 2
            .Text = vObjOracle(8)
        End With
            
        Graph1.NumPoints = 8
    
        Graph1.AutoInc = 0
        Graph1.ThisPoint = 1
        Graph1.IndexStyle = gphStandard
        Graph1.ThisPoint = 1
        Graph1.DataReset = gphAllData
        Graph1.DataReset = gphLabelText
        
        Graph1.FontUse = gphLabels
        Graph1.FontFamily = gphSwiss
        Graph1.FontSize = 120
    
        Graph1.FontUse = gphLegend
        Graph1.FontFamily = gphSwiss
        Graph1.FontSize = 90
            
        Graph1.ThisPoint = 1
        Graph1.GraphData = vObjOracle(0)
        Graph1.LabelText = vObjOracle(0)
        Graph1.LegendText = "PROTESTO"
        Graph1.ThisPoint = 2
        Graph1.GraphData = vObjOracle(1)
        Graph1.LabelText = vObjOracle(1)
        Graph1.LegendText = "JUROS"
        Graph1.ThisPoint = 3
        Graph1.GraphData = vObjOracle(2)
        Graph1.LabelText = vObjOracle(2)
        Graph1.LegendText = "CHEQUE"
        Graph1.ThisPoint = 4
        Graph1.GraphData = vObjOracle(3)
        Graph1.LabelText = vObjOracle(3)
        Graph1.LegendText = "COMPRA"
        Graph1.ThisPoint = 5
        Graph1.GraphData = vObjOracle(4)
        Graph1.LabelText = vObjOracle(4)
        Graph1.LegendText = "DUPLICATA"
        Graph1.ThisPoint = 6
        Graph1.GraphData = vObjOracle(5)
        Graph1.LabelText = vObjOracle(5)
        Graph1.LegendText = "LIMITE"
        Graph1.ThisPoint = 7
        Graph1.GraphData = vObjOracle(6)
        Graph1.LabelText = vObjOracle(6)
        Graph1.LegendText = "SALDO"
        Graph1.ThisPoint = 8
        Graph1.GraphData = vObjOracle(7)
        Graph1.LabelText = vObjOracle(7)
        Graph1.LegendText = "CLIENTE NOVO"
        
        Graph1.DrawMode = gphDraw
        
        vSql = "PRODUCAO.PCK_VDA770.pr_CON_TOTPED(:PM_SITUACAO,:PM_INICIO,:PM_FIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        Grid1.Row = 10
        Grid1.Col = 2
        Grid1.Text = vObjOracle(0)
            
    End If
    
    Screen.MousePointer = 0
    stbBarra.Panels(1).Text = ""

End Sub

Private Sub cmd2_Click()
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        tipo = stb.Tab
        If tipo = 0 Then
            PRINTLISTA
        Else
            PRINTGRAFICO
        End If
    End If
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub txtInicio_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtInicio
End Sub
Private Sub txtFim_KeyPress(Keyascii As Integer)
    DATA Keyascii, txtFim
End Sub

Private Sub Form_Load()
    
    
    Me.Top = 2000
    Me.Left = 2000
    
    
    cboSituacao.AddItem "TODOS"
    cboSituacao.AddItem "LIBERADOS"
    cboSituacao.AddItem "CANCELADOS"

    
End Sub


Sub PRINTLISTA()

    With Grid1
        
        'GRID VAZIO
        If .Rows = 1 Then
            .Col = 0
            If .Text = "" Then
                Exit Sub
            End If
        End If
        
        'Printer.Orientation = vbPRORLandscape
        Printer.Print ""
        Printer.FontName = "Courier New"
        Printer.FontSize = 9
        
        .Row = 0
        liPagina = 0
        
        Call pImprimirCabecalholista
        
        Do
            .Row = .Row + 1
           'IMPRIMIR LINHA DE DETALHE
           
           'lstrlinha = " C�DIGO  |                    NOME                   | PEDIDOS | VL. CONT�BIL "
           
            .Col = 1:  lstrlinha = .Text & Space(17 - Len(.Text)) & ""
            .Col = 2:  lstrlinha = lstrlinha & " | " & Space(10 - Len(.Text)) & .Text & ""
            
            Printer.Print lstrlinha
            
            liLinhaImpressao = liLinhaImpressao + 1
            
           'VERIFICA QUEBRA DE PAGINA
            If liLinhaImpressao > 60 And .Row <> .Rows - 1 Then
                Printer.Print String(79, "-")
                Printer.EndDoc
                Printer.Print ""
                Printer.FontName = "Courier New"
                Printer.FontSize = 9
                Call pImprimirCabecalholista
                liLinhaImpressao = 0
                liPagina = liPagina + 1
            End If
        
        Loop Until .Row = .Rows - 1
        
        Printer.Print String(79, "-")
        Printer.EndDoc
    
        MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
    End With

End Sub

Sub PRINTGRAFICO()
    
    Graph1.DrawMode = gphPrint
    
End Sub


Private Sub pImprimirCabecalholista()
    
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 9
        
    Printer.Print
    lstrlinha = "                              CONTROLE DE BLOQUEIOS                                   "
    Printer.Print lstrlinha
    Printer.Print
    
    Printer.Print String(79, "-")
    Printer.Print
    
    If cboSituacao.Text = "" Then
        lstrlinha = " SITUA��O: TODOS"
    Else
        lstrlinha = " SITUA��O: " & cboSituacao.Text & ""
    End If
    
    If vInicio <> "" Then
        lstrlinha = lstrlinha & "  -  PER�ODO DE: " & vInicio & ""
    End If
    
    If vFim <> "" Then
        lstrlinha = lstrlinha & " � " & vFim & ""
    End If
    
    Printer.Print lstrlinha
    
    Printer.Print
    Printer.Print String(79, "-")
    
    lstrlinha = " TIPO BLOQUEIO   |   QTD"
    
    Printer.Print lstrlinha
    Printer.Print String(79, "-")

End Sub
