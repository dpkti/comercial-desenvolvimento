VERSION 5.00
Begin VB.Form FrmDir 
   Caption         =   "Imprimir Relat�rio"
   ClientHeight    =   4635
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   4545
   LinkTopic       =   "Form1"
   ScaleHeight     =   4635
   ScaleWidth      =   4545
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   3045
      Left            =   600
      TabIndex        =   2
      Top             =   600
      Width           =   3225
      Begin VB.DriveListBox drvDrive 
         Height          =   315
         Left            =   150
         TabIndex        =   4
         Top             =   240
         Width           =   2925
      End
      Begin VB.DirListBox dirDiretorio 
         Height          =   2340
         Left            =   150
         TabIndex        =   3
         Top             =   600
         Width           =   2925
      End
   End
   Begin VB.CommandButton cmdSair 
      Cancel          =   -1  'True
      Height          =   615
      Left            =   2520
      Picture         =   "FrmDir.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Sair"
      Top             =   3780
      Width           =   885
   End
   Begin VB.CommandButton cmdGrava 
      Default         =   -1  'True
      Height          =   615
      Left            =   1065
      Picture         =   "FrmDir.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Imprimir Relat�rio"
      Top             =   3780
      Width           =   885
   End
   Begin VB.Label lblDir 
      Caption         =   "Diret�rio de Exporta��o do Relat�rio:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   720
      TabIndex        =   5
      Top             =   360
      Width           =   3255
   End
End
Attribute VB_Name = "FrmDir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdGrava_Click()
    frmConsPed.txtDirArq.Text = Trim(dirDiretorio.Path) & "\"
    Unload Me
End Sub

Private Sub cmdSair_Click()
    frmConsPed.txtDirArq.Text = ""
    Unload Me
End Sub

Private Sub drvDrive_Change()
    dirDiretorio.Path = drvDrive.Drive
End Sub

Private Sub Form_Load()
    dirDiretorio.Path = drvDrive.Drive
End Sub
