VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFRAME.OCX"
Begin VB.Form frmLibCredito 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acompanhamento de Libera��o de Cr�dito"
   ClientHeight    =   8475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6195
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   565
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   413
   Begin VB.CheckBox chkJuros 
      Caption         =   "Juros"
      Height          =   255
      Left            =   4680
      TabIndex        =   13
      Top             =   5160
      Width           =   1095
   End
   Begin CoolXPFrame.xpFrame xpFrameParametros 
      Height          =   5370
      Left            =   120
      Top             =   1200
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   9472
      Caption         =   "Par�metros para gera��o de arquivo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.CheckBox chkCliente 
         Caption         =   "Cliente Novo"
         Height          =   255
         Left            =   2640
         TabIndex        =   18
         Top             =   5040
         Width           =   1335
      End
      Begin VB.CheckBox chkSaldo 
         Caption         =   "Saldo"
         Height          =   255
         Left            =   4560
         TabIndex        =   19
         Top             =   5040
         Width           =   1095
      End
      Begin VB.CheckBox chkDuplicata 
         Caption         =   "Duplicata"
         Height          =   255
         Left            =   4560
         TabIndex        =   17
         Top             =   4680
         Width           =   1095
      End
      Begin VB.CheckBox chkLimite 
         Caption         =   "Limite"
         Height          =   255
         Left            =   2640
         TabIndex        =   16
         Top             =   4680
         Width           =   1095
      End
      Begin VB.CheckBox chkCompra 
         Caption         =   "Compra"
         Height          =   255
         Left            =   4560
         TabIndex        =   15
         Top             =   4320
         Width           =   1095
      End
      Begin VB.CheckBox chkCheque 
         Caption         =   "Cheque"
         Height          =   255
         Left            =   2640
         TabIndex        =   14
         Top             =   4320
         Width           =   1095
      End
      Begin VB.CheckBox chkProtesto 
         Caption         =   "Protesto"
         Height          =   255
         Left            =   2640
         TabIndex        =   12
         Top             =   3960
         Width           =   1095
      End
      Begin VB.ComboBox cboSituacao 
         Height          =   330
         Left            =   2640
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   2040
         Width           =   3015
      End
      Begin MSComDlg.CommonDialog CommonDialog 
         Left            =   240
         Top             =   120
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.TextBox txtNomeFilial 
         Height          =   315
         Left            =   2640
         MaxLength       =   25
         TabIndex        =   6
         Top             =   1680
         Width           =   3015
      End
      Begin VB.TextBox txtSitPedido 
         Height          =   285
         Left            =   5040
         TabIndex        =   22
         Top             =   5880
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox txtCodPlanoPagto 
         Height          =   315
         Left            =   2640
         MaxLength       =   3
         TabIndex        =   8
         Top             =   2400
         Width           =   3015
      End
      Begin VB.TextBox txtNomeOpCredito 
         Height          =   315
         Left            =   2640
         MaxLength       =   50
         TabIndex        =   9
         Top             =   2760
         Width           =   3015
      End
      Begin VB.TextBox txtCodCliente 
         Height          =   315
         Left            =   2640
         MaxLength       =   6
         TabIndex        =   10
         Top             =   3120
         Width           =   3015
      End
      Begin VB.TextBox txtBaseCGC 
         Height          =   315
         Left            =   2640
         MaxLength       =   14
         TabIndex        =   11
         Top             =   3480
         Width           =   3015
      End
      Begin VB.TextBox txtTipoBloqueio 
         Height          =   285
         Left            =   5040
         MaxLength       =   1
         TabIndex        =   24
         Top             =   6240
         Visible         =   0   'False
         Width           =   2295
      End
      Begin VB.TextBox txtDataInicioUltimaCompra 
         Height          =   315
         Left            =   2640
         MaxLength       =   10
         TabIndex        =   2
         Top             =   960
         Width           =   1455
      End
      Begin VB.TextBox txtDataInicioEmissaoNF 
         Height          =   315
         Left            =   2640
         MaxLength       =   10
         TabIndex        =   0
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox txtDataInicioDigitacao 
         Height          =   315
         Left            =   2640
         MaxLength       =   10
         TabIndex        =   4
         Top             =   1320
         Width           =   1455
      End
      Begin VB.TextBox txtDataFimEmissaoNF 
         Height          =   315
         Left            =   4200
         MaxLength       =   10
         TabIndex        =   1
         Top             =   600
         Width           =   1455
      End
      Begin VB.TextBox txtDataFimUltimaCompra 
         Height          =   315
         Left            =   4200
         MaxLength       =   10
         TabIndex        =   3
         Top             =   960
         Width           =   1455
      End
      Begin VB.TextBox txtDataFimDigitacao 
         Height          =   315
         Left            =   4200
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblDtEmissaoNF 
         Caption         =   "Data emiss�o NF"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label lblInicio 
         Alignment       =   2  'Center
         Caption         =   "In�cio"
         Height          =   255
         Left            =   2640
         TabIndex        =   35
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label lblFim 
         Alignment       =   2  'Center
         Caption         =   "Fim"
         Height          =   255
         Left            =   4320
         TabIndex        =   34
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblDtCompra 
         Caption         =   "Data �ltima compra"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label lblDigitacao 
         Caption         =   "Data digita��o do pedido"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label lblNomeFilial 
         Caption         =   "Nome da Filial"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label lblSitPedido 
         Caption         =   "Situa��o do pedido"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   2040
         Width           =   1575
      End
      Begin VB.Label lblCodPlanoPagto 
         Caption         =   "C�digo plano pagamento"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   2400
         Width           =   1935
      End
      Begin VB.Label lblNomeOpCredito 
         Caption         =   "Nome operador cr�dito"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   2760
         Width           =   2295
      End
      Begin VB.Label lblCodCliente 
         Caption         =   "C�digo do cliente"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   3120
         Width           =   1575
      End
      Begin VB.Label lblBaseCGC 
         Caption         =   "Base do CGC do cliente"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   3480
         Width           =   1935
      End
      Begin VB.Label lblTipoBloqueio 
         Caption         =   "Tipo do bloqueio do pedido"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   3960
         Width           =   2415
      End
   End
   Begin CoolXPFrame.xpFrame xpFrameInformacoes 
      Height          =   1605
      Left            =   120
      Top             =   6720
      Width           =   5805
      _ExtentX        =   10239
      _ExtentY        =   2831
      Caption         =   "Informa��es"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.TextBox textBoxErro 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   1125
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   360
         Width           =   5520
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   37
      Top             =   0
      Width           =   14010
      _ExtentX        =   24712
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin pkGradientControl.pkGradient pkGradient2 
      Height          =   30
      Left            =   120
      TabIndex        =   38
      Top             =   960
      Width           =   5970
      _ExtentX        =   10530
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   690
      Left            =   120
      TabIndex        =   21
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLibCred.frx":0000
      PICN            =   "frmLibCred.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExecutar 
      Height          =   690
      Left            =   960
      TabIndex        =   20
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   120
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmLibCred.frx":0CF6
      PICN            =   "frmLibCred.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmLibCredito"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Dim OraSession As Object
Dim db As Object

Dim y As Integer
Dim x As Integer
Dim obrigatorio As Boolean
Dim ValidaDatas As Boolean


Public Sub HabilitaComandos(Flag As Boolean)
  cmdExecutar.Enabled = Flag
  cmdSair.Enabled = Flag
    
  txtDataInicioEmissaoNF.Enabled = Flag
  txtDataFimEmissaoNF.Enabled = Flag
  txtDataInicioUltimaCompra.Enabled = Flag
  txtDataFimUltimaCompra.Enabled = Flag
  txtDataInicioDigitacao.Enabled = Flag
  txtDataFimDigitacao.Enabled = Flag
  txtNomeFilial.Enabled = Flag
  txtSitPedido.Enabled = Flag
  txtCodPlanoPagto.Enabled = Flag
  txtNomeOpCredito.Enabled = Flag
  txtCodCliente.Enabled = Flag
  txtBaseCGC.Enabled = Flag
  txtTipoBloqueio.Enabled = Flag
  cboSituacao.Enabled = Flag
End Sub

Private Sub cmdExecutar_Click()
    
  Dim strAno As String
  Dim strMes As String
  Dim strPeriodo As String
  Dim strLinha As String
  Dim strTeste As String
  Dim intTamanhoNomeArquivo As Integer
  Dim vCarregando As Boolean
  Dim strNomeArquivo As String
  Dim strPastaArquivo As String
    
  'Limpa a Text Box Informa��es
  textBoxErro.Text = ""
    
  strNomeArquivo = ""
  strPastaArquivo = ""
  obrigatorio = True
    
  Call CampoObrigatorio
  If obrigatorio = True Then
    If txtDataInicioEmissaoNF <> "" And txtDataFimEmissaoNF <> "" Then
      Call ValidaData(txtDataInicioEmissaoNF, txtDataFimEmissaoNF)
      If ValidaDatas = False Then Exit Sub
    End If
    If txtDataInicioUltimaCompra <> "" And txtDataFimUltimaCompra <> "" Then
      Call ValidaData(txtDataInicioUltimaCompra, txtDataFimUltimaCompra)
      If ValidaDatas = False Then Exit Sub
    End If
    If txtDataInicioDigitacao <> "" And txtDataFimDigitacao <> "" Then
      Call ValidaData(txtDataInicioDigitacao, txtDataFimDigitacao)
      If ValidaDatas = False Then Exit Sub
    End If
  Else
    Exit Sub
  End If
  
  If cboSituacao.Text <> Empty And cboSituacao.ListIndex < 0 Then
    MsgBox "Situa��o do pedido inv�lida", vbOKOnly + vbInformation, "Aten��o"
    cboSituacao.SetFocus
    Exit Sub
  End If
  
    
  'Desabilita os comandos enquanto o sistema est� em execu��o
  Call HabilitaComandos(False)
    
  'Carrega rotina para salvar o arquivo .xls
  CommonDialog.Filter = "Planilhas Excel (*.xls) | *.xls"
  CommonDialog.InitDir = "C:\Documents and Settings\" & mdiVDA770.stbBarra.Panels(2).Text & "\Meus documentos"
  CommonDialog.ShowSave
    
    
  If CommonDialog.FileName <> "" Then
    intTamanhoNomeArquivo = Len(CommonDialog.FileTitle)
    strNomeArquivo = CommonDialog.FileTitle
    strPastaArquivo = Left(CommonDialog.FileName, Len(CommonDialog.FileName) - intTamanhoNomeArquivo)
    RegistraExecucao ("Efetuando a gera��o do relat�rio")
    Call GerarPlanilhaExcel(strPastaArquivo, strNomeArquivo)
    'Habilita os comandos enquanto o sistema est� em execu��o
    Call HabilitaComandos(True)
  Else
    'Habilita os comandos enquanto o sistema est� em execu��o
    Call HabilitaComandos(True)
  End If
    
End Sub

Private Sub cmdSair_Click()
  Unload Me
End Sub

Private Sub Command1_Click()
  Dim intTamanhoNomeArquivo As Integer
    
  CommonDialog.Filter = "Planilhas Excel (*.xls) | *.xls"
  CommonDialog.InitDir = "C:\"
  CommonDialog.ShowSave
    
  If CommonDialog.FileName <> "" Then
    MsgBox (CommonDialog.FileTitle)
    If CommonDialog.FileTitle <> "" Then
      intTamanhoNomeArquivo = Len(CommonDialog.FileTitle)
      MsgBox ("Relat�rio '" & CommonDialog.FileTitle & "' gerado com sucesso no diret�rio: '" & Left(CommonDialog.FileName, Len(CommonDialog.FileName) - intTamanhoNomeArquivo) & "'")
    End If
  End If

End Sub

Private Sub cmdVoltar_Click()
  Unload Me
End Sub

Private Sub Form_Load()

  Me.Top = 2000
  Me.Left = 2000
    
  Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
  'Call vVB_Generica_001.PreencheComboList(vObjOracle, cboLoja, "DEP�SITO")
    
  'Limpa a Text Box Informa��es
  textBoxErro.Text = ""
  
  'Popula o combo com as situa��es de pedido
  'SDS2376 - Ricardo Gomes
  'Removido  o item "PROBLEMA COM FINALIZA��O"
  cboSituacao.AddItem Empty
  cboSituacao.AddItem "LIBERADO"
  cboSituacao.AddItem "EM MANUTEN��O"
  cboSituacao.AddItem "CANCELADO"
    
        
End Sub

Public Sub CampoObrigatorio()


  If txtDataInicioEmissaoNF = "" And txtDataInicioUltimaCompra = "" And txtDataInicioDigitacao = "" Then
    MsgBox "Informe um per�odo de data", , "Aten��o!"
    txtDataInicioEmissaoNF.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioEmissaoNF <> "" And txtDataFimEmissaoNF = "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataFimEmissaoNF.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioUltimaCompra <> "" And txtDataFimUltimaCompra = "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataFimUltimaCompra.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioDigitacao <> "" And txtDataFimDigitacao = "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataFimDigitacao.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioEmissaoNF = "" And txtDataFimEmissaoNF <> "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataInicioEmissaoNF.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioUltimaCompra = "" And txtDataFimUltimaCompra <> "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataInicioUltimaCompra.SetFocus
    obrigatorio = False
  ElseIf (txtDataInicioDigitacao = "" And txtDataFimDigitacao <> "") Then
    MsgBox "Informe a data em branco", , "Aten��o!"
    txtDataInicioDigitacao.SetFocus
    obrigatorio = False
  End If
   
End Sub




Public Function ValidaData(txtDe As TextBox, txtAte As TextBox) As Boolean

  Dim dias As Integer
  Dim strAno As String
  Dim strMes As String
  Dim strDia, Dia As String
  Dim strData, strDataAte As String

  ValidaDatas = True
    
  
  If Not IsDate(txtDe.Text) Or Len(txtDe.Text) <> 10 Then
    MsgBox "Data incorreta ou formato incorreto" & vbCrLf & "Digite uma data no formato DD/MM/AAAA - somente n�meros", _
    vbOKOnly + vbExclamation, "Aten��o!"
    txtDe.SetFocus
    ValidaDatas = False
    Exit Function
  End If
  
  If Not IsDate(txtAte.Text) Or Len(txtAte.Text) <> 10 Then
    MsgBox "Data incorreta ou formato incorreto" & vbCrLf & "Digite uma data no formato DD/MM/AAAA - somente n�meros", _
    vbOKOnly + vbExclamation, "Aten��o!"
    ValidaDatas = False
    txtAte.SetFocus
    Exit Function
  End If
          
'  strData = txtDe.Text
'  strAno = Mid$(strData, 7, 4)
'  strMes = Mid$(strData, 4, 2)
'  Dia = Mid$(strData, 1, 2)
'
'  ' Pega a data e o mes atual
'  '
'  Select Case strMes
'    Case "04", "06", "09", "11"
'      strDia = "30"
'
'    Case "02"
'      If Bissexto(Val(strAno)) Then
'        strDia = "29"
'      Else
'        strDia = "28"
'      End If
'
'    Case Else
'      strDia = "31"
'  End Select
'
'  If Dia <= strDia Then
'    strData = txtAte
'    strAno = Mid$(strData, 7, 4)
'    strMes = Mid$(strData, 4, 2)
'    Dia = Mid$(strData, 1, 2)
'
'    ' Pega a data e o mes atual
'    '
'    Select Case strMes
'      Case "04", "06", "09", "11"
'        strDia = "30"
'
'      Case "02"
'        If Bissexto(Val(strAno)) Then
'          strDia = "29"
'        Else
'          strDia = "28"
'        End If
'
'      Case Else
'        strDia = "31"
'    End Select
'
'    If Dia > strDia Then
'      MsgBox "Data incorreta", , "Aten��o!"
'      ValidaDatas = False
'      Exit Function
'    End If
'  Else
'    MsgBox "Data incorreta", , "Aten��o!"
'    ValidaDatas = False
'    Exit Function
'  End If
              
  '****
  If CDate(txtDe.Text) <= CDate(txtAte.Text) Then
    dias = DateDiff("d", txtDe.Text, txtAte.Text)
    If dias > 30 Then
      MsgBox "Per�odo entre data inicial e final n�o pode ser superior a 30(trinta) dias", , "Aten��o!"
      ValidaDatas = False
      txtDe.SetFocus
      Exit Function
    End If
  Else
    MsgBox "Data final n�o pode ser menor que data inicial", , "Aren��o!"
    ValidaDatas = False
    txtDe.SetFocus
    Exit Function
  End If
  
    
        
End Function


Sub GerarPlanilhaExcel(strPastaArquivo As String, strNomeArquivo As String)
    
  Dim strSQL As String
  Dim objExcel As Object
  Dim objPlanilha As Object
  Dim strLinha As String
  'Dim RstExcel As Object
  'Dim vNumNota As Long
  Dim lngLinha As Long
  Dim strbloq As String
  Dim strBloqueio As String
  Dim strCNPJ As String
  
  '-------------------------
  Dim prmDtInicioEmissao    As ADODB.Parameter
  Dim prmDtFimEmissao       As ADODB.Parameter
  Dim prmDtInicioUltCompra  As ADODB.Parameter
  Dim prmDtFimUltCompra     As ADODB.Parameter
  Dim prmDtInicioDigitacao  As ADODB.Parameter
  Dim prmDtFimDigitacao     As ADODB.Parameter
  Dim prmNomeFilial         As ADODB.Parameter
  Dim prmSitPedido          As ADODB.Parameter
  Dim prmCodPlanoPgto       As ADODB.Parameter
  Dim prmNomeOperador       As ADODB.Parameter
  Dim prmCodCliente         As ADODB.Parameter
  Dim prmBaseCGC            As ADODB.Parameter
  Dim prmTipoBloqueio       As ADODB.Parameter
    
  '-------------------------
  Dim prmMsgerro     As ADODB.Parameter
  Dim prmErro        As ADODB.Parameter
  '-------------------------
    
  On Error GoTo TrataErro
    
  strLinha = 1
  
  Set objExcel = CreateObject("Excel.Application")
  objExcel.Workbooks.Add
  Set objPlanilha = objExcel.ActiveWorkbook.Sheets(1)
    
  With objPlanilha.PageSetup
    .LeftMargin = objPlanilha.Application.InchesToPoints(0.196850393700787)
    .RightMargin = objPlanilha.Application.InchesToPoints(0.196850393700787)
    .TopMargin = objPlanilha.Application.InchesToPoints(0.196850393700787)
    .BottomMargin = objPlanilha.Application.InchesToPoints(0.196850393700787)
    .HeaderMargin = objPlanilha.Application.InchesToPoints(0.511811023622047)
    .FooterMargin = objPlanilha.Application.InchesToPoints(0.511811023622047)
    .PrintGridlines = False
    .Orientation = 2
    .FirstPageNumber = 1
    .Order = 1
    .BlackAndWhite = False
    .Zoom = 100
  End With
    
  'Ajusta o tamanho das c�lulas
  objPlanilha.Columns(1).ColumnWidth = 41
  objPlanilha.Columns(2).ColumnWidth = 19
  objPlanilha.Columns(3).ColumnWidth = 10
  objPlanilha.Columns(4).ColumnWidth = 12
  objPlanilha.Columns(5).ColumnWidth = 19
  objPlanilha.Columns(6).ColumnWidth = 19
  objPlanilha.Columns(7).ColumnWidth = 26
  objPlanilha.Columns(8).ColumnWidth = 19
  objPlanilha.Columns(9).ColumnWidth = 16
  objPlanilha.Columns(10).ColumnWidth = 11
  objPlanilha.Columns(11).ColumnWidth = 40
  objPlanilha.Columns(12).ColumnWidth = 40
  objPlanilha.Columns(13).ColumnWidth = 32
  objPlanilha.Columns(14).ColumnWidth = 22
    
  objPlanilha.Columns(15).ColumnWidth = 15
  objPlanilha.Columns(16).ColumnWidth = 11
  objPlanilha.Columns(17).ColumnWidth = 12
    
  objPlanilha.Columns(18).ColumnWidth = 16
  objPlanilha.Columns(19).ColumnWidth = 26
  objPlanilha.Columns(20).ColumnWidth = 15
  objPlanilha.Columns(21).ColumnWidth = 10
  objPlanilha.Columns(22).ColumnWidth = 18
  objPlanilha.Columns(23).ColumnWidth = 18
  objPlanilha.Columns(24).ColumnWidth = 32
  objPlanilha.Columns(25).ColumnWidth = 14
  objPlanilha.Columns(26).ColumnWidth = 15
  objPlanilha.Columns(27).ColumnWidth = 19
  objPlanilha.Columns(28).ColumnWidth = 20
  objPlanilha.Columns(29).ColumnWidth = 23
  objPlanilha.Columns(30).ColumnWidth = 23
  objPlanilha.Columns(31).ColumnWidth = 23
  objPlanilha.Columns(32).ColumnWidth = 23
  objPlanilha.Columns(33).ColumnWidth = 27
  objPlanilha.Columns(34).ColumnWidth = 12
  objPlanilha.Columns(35).ColumnWidth = 16
  objPlanilha.Columns(36).ColumnWidth = 16
  objPlanilha.Columns(37).ColumnWidth = 10
  objPlanilha.Columns(38).ColumnWidth = 15
  objPlanilha.Columns(39).ColumnWidth = 15
  objPlanilha.Columns(40).ColumnWidth = 15
  objPlanilha.Columns(41).ColumnWidth = 17
  objPlanilha.Columns(42).ColumnWidth = 40
       
  'Monta os t�tulos das colunas
  objPlanilha.Cells(strLinha, 1).Value = "Nome da Filial"
  objPlanilha.Cells(strLinha, 2).Value = "CD do Pedido (Centro de Distribui��o)"
  objPlanilha.Cells(strLinha, 3).Value = "N�mero do Pedido"
  objPlanilha.Cells(strLinha, 4).Value = "Valor do Pedido"
  objPlanilha.Cells(strLinha, 5).Value = "Data/Hora da Digita��o do Pedido"
  objPlanilha.Cells(strLinha, 6).Value = "Data/Hora do Pedido Pol�tica"
  objPlanilha.Cells(strLinha, 7).Value = "Data/Hora da Primeira An�lise do Pedido de Cr�dito"
  objPlanilha.Cells(strLinha, 8).Value = "Data/Hora do Pedido de Cr�dito"
  objPlanilha.Cells(strLinha, 9).Value = "Tempo de An�lise do Pedido"
  objPlanilha.Cells(strLinha, 10).Value = "Tempo para Libera��o"
  objPlanilha.Cells(strLinha, 11).Value = "Situa��o do Pedido"
  objPlanilha.Cells(strLinha, 12).Value = "Motivo do Cancelamento"
  objPlanilha.Cells(strLinha, 13).Value = "Nome do Operador de Cr�dito"
  objPlanilha.Cells(strLinha, 14).Value = "CD do Operador (Centro de Distribui��o)"
    
  objPlanilha.Cells(strLinha, 15).Value = "Data de Emiss�o da Nota Fiscal"
  objPlanilha.Cells(strLinha, 16).Value = "Numero da Nota Fiscal"
  objPlanilha.Cells(strLinha, 17).Value = "Valor Total da Nota Fiscal"
    
    
  objPlanilha.Cells(strLinha, 18).Value = "C�digo do Plano de Pagamento"
  objPlanilha.Cells(strLinha, 19).Value = "Descri��o do Plano de Pagamento"
  objPlanilha.Cells(strLinha, 20).Value = "Flag do Gerente"
  objPlanilha.Cells(strLinha, 21).Value = "C�digo do Cliente"
  objPlanilha.Cells(strLinha, 22).Value = "Base do CGC do Cliente"
  objPlanilha.Cells(strLinha, 23).Value = "CPF/CNPJ do Cliente"
  objPlanilha.Cells(strLinha, 24).Value = "Nome do Cliente"
  objPlanilha.Cells(strLinha, 25).Value = "Data da �ltima Compra"
  objPlanilha.Cells(strLinha, 26).Value = "D�vida Vencida"
  objPlanilha.Cells(strLinha, 27).Value = "Porcentagem (%) de Pontualidade"
  objPlanilha.Cells(strLinha, 28).Value = "Quantidade de T�tulos Pagos Pontuais"
  objPlanilha.Cells(strLinha, 29).Value = "Quantidade de T�tulos com Atrasos at� 15 dias"
  objPlanilha.Cells(strLinha, 30).Value = "Quantidade de T�tulos com Atrasos at� 30 dias"
  objPlanilha.Cells(strLinha, 31).Value = "Quantidade de T�tulos com Atrasos at� 60 dias"
  objPlanilha.Cells(strLinha, 32).Value = "Quantidade de T�tulos com Atrasos at� 90 dias"
  objPlanilha.Cells(strLinha, 33).Value = "Quantidade de T�tulos com Atrasos com mais de 90 dias"
  objPlanilha.Cells(strLinha, 34).Value = "Data de Funda��o"
  objPlanilha.Cells(strLinha, 35).Value = "Valor do Limite em Vig�ncia"
  objPlanilha.Cells(strLinha, 36).Value = "Valor da Carteira do Cliente"
  objPlanilha.Cells(strLinha, 37).Value = "Valor a Vencer"
  objPlanilha.Cells(strLinha, 38).Value = "Valor Vencido"
  objPlanilha.Cells(strLinha, 39).Value = "Nota de Cr�dito"
  objPlanilha.Cells(strLinha, 40).Value = "Instru��o para Cart�rio"
  objPlanilha.Cells(strLinha, 41).Value = "Tipo de Bloqueio do Pedido"
  objPlanilha.Cells(strLinha, 42).Value = "Descri��o Mensagem Cr�dito"
     
  objPlanilha.Range(objPlanilha.Cells(strLinha, 1), objPlanilha.Cells(strLinha, 45)).Font.Bold = True
  
  'Monta a condi��o para tipo de bloqueio
  strBloqueio = Empty
  
  If chkProtesto.Value = 1 Then
    strBloqueio = strBloqueio & " AND (P.BLOQ_PROTESTO = 'L'"
  End If
  
  If chkJuros.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_JUROS = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_JUROS = 'L'"
    End If
  End If
  
  If chkCheque.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_CHEQUE = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_CHEQUE = 'L'"
    End If
  End If
  
  If chkCompra.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_COMPRA = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_COMPRA = 'L'"
    End If
  End If
  
  If chkLimite.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_LIMITE = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_LIMITE = 'L'"
    End If
  End If
  
  If chkDuplicata.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_DUPLICATA = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_DUPLICATA = 'L'"
    End If
  End If
  
  If chkSaldo.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_SALDO = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_SALDO = 'L'"
    End If
  End If
  
  If chkCliente.Value = 1 Then
    If strBloqueio = Empty Then
      strBloqueio = strBloqueio & " AND (P.BLOQ_CLIE_NOVO = 'L'"
    Else
      strBloqueio = strBloqueio & " OR P.BLOQ_CLIE_NOVO = 'L'"
    End If
  End If
  
  If strBloqueio <> Empty Then
    strBloqueio = strBloqueio & ")"
  End If
  
  'Busca as informa��es na base para alimentar planilha a ser gerada
  vBanco.Parameters.Remove "PM_CURSOR"
  vBanco.Parameters.Add "PM_CURSOR", 0, 2
  vBanco.Parameters("PM_CURSOR").ServerType = 102
  vBanco.Parameters("PM_CURSOR").DynasetOption = &H2&
  vBanco.Parameters("PM_CURSOR").DynasetCacheParams 256, 16, 20, 2000, 0
         
  vBanco.Parameters.Remove "PM_CODERRO"
  vBanco.Parameters.Add "PM_CODERRO", 0, 2
  vBanco.Parameters.Remove "PM_TXTERRO"
  vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
  vBanco.Parameters.Remove "PM_DATAINICIOEMISSAONF"
  vBanco.Parameters.Add "PM_DATAINICIOEMISSAONF", txtDataInicioEmissaoNF.Text, 2
  vBanco.Parameters.Remove "PM_DATAFIMEMISSAONF"
  vBanco.Parameters.Add "PM_DATAFIMEMISSAONF", txtDataFimEmissaoNF.Text, 2
    
  vBanco.Parameters.Remove "PM_DATAINICIOULTIMACOMPRA"
  vBanco.Parameters.Add "PM_DATAINICIOULTIMACOMPRA", txtDataInicioUltimaCompra.Text, 2
  vBanco.Parameters.Remove "PM_DATAFIMULTIMACOMPRA"
  vBanco.Parameters.Add "PM_DATAFIMULTIMACOMPRA", txtDataFimUltimaCompra.Text, 2
    
  vBanco.Parameters.Remove "PM_DATAINICIODIGITACAO"
  vBanco.Parameters.Add "PM_DATAINICIODIGITACAO", txtDataInicioDigitacao.Text, 2
  vBanco.Parameters.Remove "PM_DATAFIMDIGITACAO"
  vBanco.Parameters.Add "PM_DATAFIMDIGITACAO", txtDataFimDigitacao.Text, 2
    
  vBanco.Parameters.Remove "PM_NOMEFILIAL"
  vBanco.Parameters.Add "PM_NOMEFILIAL", UCase(txtNomeFilial.Text), 2
        
  vBanco.Parameters.Remove "PM_SITUACAOPEDIDO"
  'vBanco.Parameters.Add "PM_SITUACAOPEDIDO", txtSitPedido.Text, 2
  vBanco.Parameters.Add "PM_SITUACAOPEDIDO", cboSituacao.Text, 2
  
  vBanco.Parameters.Remove "PM_CODPLANOPGTO"
  vBanco.Parameters.Add "PM_CODPLANOPGTO", txtCodPlanoPagto.Text, 2
    
  vBanco.Parameters.Remove "PM_NOMEOPERACOR"
  vBanco.Parameters.Add "PM_NOMEOPERACOR", txtNomeOpCredito.Text, 2
    
  vBanco.Parameters.Remove "PM_CODCLIENTE"
  vBanco.Parameters.Add "PM_CODCLIENTE", txtCodCliente.Text, 2
    
  vBanco.Parameters.Remove "PM_BASECGC"
  vBanco.Parameters.Add "PM_BASECGC", txtBaseCGC.Text, 2
    
  vBanco.Parameters.Remove "PM_TIPOBLOQUEIO"
  'vBanco.Parameters.Add "PM_TIPOBLOQUEIO", txtTipoBloqueio.Text, 2
  vBanco.Parameters.Add "PM_TIPOBLOQUEIO", strBloqueio, 2
  
  '----------------------------------------------------------------------
  strSQL = "PRODUCAO.PCK_VDA770.PR_ACOMPANHAMENTO_ANALISE_CRED(:PM_DATAINICIOEMISSAONF,:PM_DATAFIMEMISSAONF,:PM_DATAINICIOULTIMACOMPRA, :PM_DATAFIMULTIMACOMPRA,:PM_DATAINICIODIGITACAO,:PM_DATAFIMDIGITACAO,:PM_NOMEFILIAL,:PM_SITUACAOPEDIDO,:PM_CODPLANOPGTO,:PM_NOMEOPERACOR,:PM_CODCLIENTE,:PM_BASECGC,:PM_TIPOBLOQUEIO,:PM_CODERRO,:PM_TXTERRO,:PM_CURSOR)"

  vErro = vVB_Generica_001.ExecutaPl(vBanco, strSQL)
             
  Set vObjOracle = vBanco.Parameters("PM_CURSOR").Value
    
  Do While vObjOracle.EOF = False
       
    strLinha = strLinha + 1
       
    'If strLinha Mod 45 = 0 Then
    '     objPlanilha.Cells(strLinha, 1).Select
    '     objPlanilha.Cells(strLinha, 1).EntireRow.PageBreak = xlPageBreakManual
    'End If
       
    objPlanilha.Cells(strLinha, 1).Value = vObjOracle!NOME_FILIAL
    objPlanilha.Cells(strLinha, 2).Value = vObjOracle!CD_PEDIDO
    objPlanilha.Cells(strLinha, 3).Value = vObjOracle!num_pedido
    objPlanilha.Cells(strLinha, 3).HorizontalAlignment = -4152
    If Not IsNull(vObjOracle!VALOR_PEDIDO) Then
      objPlanilha.Cells(strLinha, 4).Value = CDbl(vObjOracle!VALOR_PEDIDO)
      objPlanilha.Cells(strLinha, 4).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 4).HorizontalAlignment = -4152
    End If
    objPlanilha.Cells(strLinha, 5).Value = vObjOracle!DT_DIGITACAO
    objPlanilha.Cells(strLinha, 5).numberformat = "DD/MM/YYYY HH:mm:SS" ':NN:SS
    objPlanilha.Cells(strLinha, 6).Value = vObjOracle!DT_PEDIDO_POLITICA
    objPlanilha.Cells(strLinha, 6).numberformat = "DD/MM/YYYY HH:mm:SS" ':NN:SS
    objPlanilha.Cells(strLinha, 7).Value = vObjOracle!DT_ANALISE_CREDITO
    objPlanilha.Cells(strLinha, 7).numberformat = "DD/MM/YYYY HH:mm:SS" ':NN:SS
    objPlanilha.Cells(strLinha, 8).Value = vObjOracle!DT_PEDIDO
    objPlanilha.Cells(strLinha, 8).numberformat = "DD/MM/YYYY HH:mm:SS" ':NN:SS
    objPlanilha.Cells(strLinha, 9).Value = vObjOracle!TEMPO_ANALISE_PEDIDO
    objPlanilha.Cells(strLinha, 9).HorizontalAlignment = -4152
    If vObjOracle!TEMPO_LIBERACAO <> "::" Then
      objPlanilha.Cells(strLinha, 10).Value = vObjOracle!TEMPO_LIBERACAO
      objPlanilha.Cells(strLinha, 10).HorizontalAlignment = -4152
    End If
    'SDS2376_RM_1 - Ricardo Gomes
    'Tratamento da situa��o do pedido "PROBLEMA COM FINALIZA��O"
    If IsNull(vObjOracle!SITUACAO_PEDIDO) Then
      objPlanilha.Cells(strLinha, 11).Value = Empty
    Else
      objPlanilha.Cells(strLinha, 11).Value = vObjOracle!SITUACAO_PEDIDO
    End If
    
    objPlanilha.Cells(strLinha, 12).Value = vObjOracle!MOTIVO_CANCEL
    objPlanilha.Cells(strLinha, 13).Value = vObjOracle!NOME_OPERADOR
    objPlanilha.Cells(strLinha, 14).Value = vObjOracle!CD_OPERADOR
    objPlanilha.Cells(strLinha, 15).Value = vObjOracle!EMISSAO_NF
    objPlanilha.Cells(strLinha, 15).numberformat = "DD/MM/YYYY"
    If Not IsNull(vObjOracle!NUM_NOTA) Then
      objPlanilha.Cells(strLinha, 16).Value = CDbl(vObjOracle!NUM_NOTA)
      objPlanilha.Cells(strLinha, 16).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!VALOR_NOTA) Then
      objPlanilha.Cells(strLinha, 17).Value = CDbl(vObjOracle!VALOR_NOTA)
      objPlanilha.Cells(strLinha, 17).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 17).HorizontalAlignment = -4152
    End If
    objPlanilha.Cells(strLinha, 18).Value = vObjOracle!COD_PLANO_PGTO
    objPlanilha.Cells(strLinha, 18).HorizontalAlignment = -4152
    objPlanilha.Cells(strLinha, 19).Value = vObjOracle!DESC_PLANO
    objPlanilha.Cells(strLinha, 20).Value = vObjOracle!FLAG_GERENTE
    objPlanilha.Cells(strLinha, 21).Value = vObjOracle!cod_cliente
    objPlanilha.Cells(strLinha, 21).HorizontalAlignment = -4152
    strCNPJ = Empty
    strCNPJ = vObjOracle!CGC_CPF_CLIENTE
    objPlanilha.Cells(strLinha, 22).numberformat = "@"
    objPlanilha.Cells(strLinha, 22).HorizontalAlignment = -4152
    'SDS2376_RM_1 - Ricardo Gomes
    'Base do CGC do Cliente - 8 caracteres
    objPlanilha.Cells(strLinha, 22).Value = Mid(strCNPJ, 1, 8)
    
    'SDS2376_RM_1 - Ricardo Gomes
    'CGC do Cliente - sem formata��o
'    If Len(strCNPJ) = 10 Or Len(strCNPJ) = 11 Then
'      strCNPJ = Mid(strCNPJ, 1, 3) & "." & Mid(strCNPJ, 4, 3) & "." _
'      & Mid(strCNPJ, 7, 3) & "-" & Mid(strCNPJ, 10)
'    ElseIf Len(strCNPJ) = 12 Then
'      strCNPJ = Mid(strCNPJ, 1, 3) & "." & Mid(strCNPJ, 4, 3) & "/" _
'      & Mid(strCNPJ, 7, 4) & "-" & Mid(strCNPJ, 11)
'    ElseIf Len(strCNPJ) = 13 Then
'      strCNPJ = Mid(strCNPJ, 1, 1) & "." & Mid(strCNPJ, 2, 3) & "." & Mid(strCNPJ, 5, 3) & "/" _
'      & Mid(strCNPJ, 8, 4) & "-" & Mid(strCNPJ, 12)
'    ElseIf Len(strCNPJ) = 14 Then
'      strCNPJ = Mid(strCNPJ, 1, 2) & "." & Mid(strCNPJ, 3, 3) & "." & Mid(strCNPJ, 6, 3) & "/" _
'      & Mid(strCNPJ, 9, 4) & "-" & Mid(strCNPJ, 13)
'    End If
    objPlanilha.Cells(strLinha, 23).numberformat = "@"
    objPlanilha.Cells(strLinha, 23).Value = strCNPJ
    objPlanilha.Cells(strLinha, 23).HorizontalAlignment = -4152
    objPlanilha.Cells(strLinha, 24).Value = vObjOracle!nome_cliente
    objPlanilha.Cells(strLinha, 25).Value = vObjOracle!DT_ULT_COMPRA
    objPlanilha.Cells(strLinha, 25).numberformat = "DD/MM/YYYY" ':NN:SS
    If Not IsNull(vObjOracle!DIVIDA_VENCIDA) Then
      objPlanilha.Cells(strLinha, 26).Value = CDbl(vObjOracle!DIVIDA_VENCIDA)
'      objPlanilha.Cells(strLinha, 26).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 26).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!PERC_PONTUAL) Then
      objPlanilha.Cells(strLinha, 27).Value = CDbl(vObjOracle!PERC_PONTUAL)
      objPlanilha.Cells(strLinha, 27).numberformat = "##0,00"
      objPlanilha.Cells(strLinha, 27).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_PONTUAL) Then
      objPlanilha.Cells(strLinha, 28).Value = CDbl(vObjOracle!QTD_PONTUAL)
      objPlanilha.Cells(strLinha, 28).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_15DD) Then
      objPlanilha.Cells(strLinha, 29).Value = CDbl(vObjOracle!QTD_15DD)
      objPlanilha.Cells(strLinha, 29).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_30DD) Then
      objPlanilha.Cells(strLinha, 30).Value = CDbl(vObjOracle!QTD_30DD)
      objPlanilha.Cells(strLinha, 30).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_60DD) Then
    objPlanilha.Cells(strLinha, 31).Value = CDbl(vObjOracle!QTD_60DD)
    objPlanilha.Cells(strLinha, 31).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_90DD) Then
      objPlanilha.Cells(strLinha, 32).Value = vObjOracle!QTD_90DD
      objPlanilha.Cells(strLinha, 32).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!QTD_MAIS90DD) Then
      objPlanilha.Cells(strLinha, 33).Value = CDbl(vObjOracle!QTD_MAIS90DD)
      objPlanilha.Cells(strLinha, 33).HorizontalAlignment = -4152
    End If
    objPlanilha.Cells(strLinha, 34).Value = vObjOracle!DT_FUNDACAO
    objPlanilha.Cells(strLinha, 34).numberformat = "DD/MM/YYYY" ':NN:SS
    If Not IsNull(vObjOracle!VL_LIMITE_VIG) Then
      objPlanilha.Cells(strLinha, 35).Value = CDbl(vObjOracle!VL_LIMITE_VIG)
      objPlanilha.Cells(strLinha, 35).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 35).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!VL_CARTEIRA) Then
      objPlanilha.Cells(strLinha, 36).Value = CDbl(vObjOracle!VL_CARTEIRA)
     objPlanilha.Cells(strLinha, 36).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 36).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!VL_VENCER) Then
      objPlanilha.Cells(strLinha, 37).Value = CDbl(vObjOracle!VL_VENCER)
      objPlanilha.Cells(strLinha, 37).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 37).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!VL_VENCIDO) Then
      objPlanilha.Cells(strLinha, 38).Value = CDbl(vObjOracle!VL_VENCIDO)
      objPlanilha.Cells(strLinha, 38).numberformat = "#,###,##0.00"
      objPlanilha.Cells(strLinha, 38).HorizontalAlignment = -4152
    End If
    If Not IsNull(vObjOracle!NOTA_CREDITO) Then
      objPlanilha.Cells(strLinha, 39).Value = CDbl(vObjOracle!NOTA_CREDITO)
      objPlanilha.Cells(strLinha, 39).numberformat = "##0,00"
      objPlanilha.Cells(strLinha, 39).HorizontalAlignment = -4152
    End If
    objPlanilha.Cells(strLinha, 40).Value = vObjOracle!INSTRUCAO_CARTORIO
       
       

    ''''   MONTA STRING PARA BLOQUEIO
    strbloq = ""
    If vObjOracle!bloq_protesto = "L" Then
      strbloq = "Protesto"
    End If
    If vObjOracle!bloq_juros = "L" Then
      If strbloq = Empty Then
        strbloq = "Juros"
      Else
        strbloq = strbloq & Chr(10) & "Juros"
      End If
    End If
    If vObjOracle!bloq_cheque = "L" Then
      If strbloq = Empty Then
        strbloq = "Cheque"
      Else
        strbloq = strbloq & Chr(10) & "Cheque"
      End If
    End If
    If vObjOracle!bloq_compra = "L" Then
      If strbloq = Empty Then
        strbloq = "Compra"
      Else
        strbloq = strbloq & Chr(10) & "Compra"
      End If
    End If
    If vObjOracle!bloq_limite = "L" Then
      If strbloq = Empty Then
        strbloq = "Limite"
      Else
        strbloq = strbloq & Chr(10) & "Limite"
      End If
    End If
    If vObjOracle!bloq_duplicata = "L" Then
      If strbloq = Empty Then
        strbloq = "Duplicata"
      Else
        strbloq = strbloq & Chr(10) & "Duplicata"
      End If
    End If
    If vObjOracle!bloq_saldo = "L" Then
      If strbloq = Empty Then
        strbloq = "Saldo"
      Else
        strbloq = strbloq & Chr(10) & "Saldo"
      End If
    End If
    If vObjOracle!bloq_clie_novo = "L" Then
      If strbloq = Empty Then
        strbloq = "Cliente novo"
      Else
        strbloq = strbloq & Chr(10) & "Cliente novo"
      End If
    End If
    objPlanilha.Cells(strLinha, 41).Value = strbloq

    objPlanilha.Cells(strLinha, 42).Value = vObjOracle!DESC_MENS
              
    vObjOracle.MoveNext
  Loop
    
  'strLinha = strLinha + 2
    
  objPlanilha.Range("A1:AP1").WrapText = True
  objPlanilha.Range("A1:AP1").HorizontalAlignment = -4108
  objPlanilha.Range("A1:AP1").RowHeight = 31
  objPlanilha.Cells.VerticalAlignment = -4108
    
  objPlanilha.SaveAs (strPastaArquivo & strNomeArquivo)
  objExcel.Visible = True
    
  Set objPlanilha = Nothing
  Set objExcel = Nothing
    
  RegistraExecucao ("Relat�rio '" & strNomeArquivo & "' gerado com sucesso no diret�rio: '" & strPastaArquivo & "'")
  Exit Sub
    
TrataErro:
  RegistraExecucao ("ERRO: " & CStr(Err.Number))
  RegistraExecucao ("ERRO: " & Err.Description)
End Sub

Private Sub txtBaseCGC_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtBaseCGC.Text
End Sub

Private Sub txtCodCliente_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtCodCliente.Text
End Sub

Private Sub txtCodPlanoPagto_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtCodPlanoPagto.Text
End Sub

Private Sub txtDataFimDigitacao_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataFimDigitacao.Text
End Sub

Private Sub txtDataFimEmissaoNF_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataFimEmissaoNF.Text
End Sub

Private Sub txtDataFimUltimaCompra_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataFimUltimaCompra.Text
End Sub

Private Sub txtDataInicioDigitacao_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataInicioDigitacao.Text
End Sub

Public Sub txtDataInicioEmissaoNF_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataInicioEmissaoNF.Text
End Sub

Private Sub txtDataInicioUltimaCompra_KeyPress(Keyascii As Integer)
  Numero Keyascii, txtDataInicioUltimaCompra.Text
End Sub

Private Sub txtNomeFilial_LostFocus()
  txtNomeFilial.Text = UCase(txtNomeFilial.Text)
End Sub

Private Sub txtNomeOpCredito_LostFocus()
  txtNomeOpCredito.Text = UCase(txtNomeOpCredito.Text)
End Sub

Private Sub txtSitPedido_LostFocus()
  txtSitPedido.Text = UCase(txtSitPedido.Text)
End Sub

Private Sub txtTipoBloqueio_LostFocus()
  txtTipoBloqueio.Text = UCase(txtTipoBloqueio.Text)
End Sub

Private Sub cboSituacao_GotFocus()
   On Error Resume Next
   cboSituacao.SelStart = 0
   cboSituacao.SelLength = Len(cboSituacao.Text)
End Sub

