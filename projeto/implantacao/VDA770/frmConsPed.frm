VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmConsPed 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta por Pedido e Loja"
   ClientHeight    =   7695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   14190
   Icon            =   "frmConsPed.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   513
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   946
   Begin VB.Frame Frame1 
      Caption         =   "Resultado da Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2760
      Left            =   9240
      TabIndex        =   19
      Top             =   1080
      Visible         =   0   'False
      Width           =   4605
      Begin VB.TextBox txtLiberado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   25
         Top             =   360
         Width           =   4290
      End
      Begin VB.TextBox txtData 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   24
         Top             =   810
         Width           =   1680
      End
      Begin VB.TextBox txtHorario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   23
         Top             =   1260
         Width           =   1680
      End
      Begin VB.TextBox txtSituacao 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   22
         Top             =   1755
         Width           =   1680
      End
      Begin VB.TextBox txtAutorizado 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         TabIndex        =   21
         Top             =   2205
         Width           =   1680
      End
      Begin VB.TextBox txtGerente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3915
         TabIndex        =   20
         Top             =   2205
         Width           =   2310
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Liberado por:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   30
         Top             =   405
         Width           =   1140
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Data da Libera��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   315
         TabIndex        =   29
         Top             =   855
         Width           =   1545
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Hor�rio da Libera��o:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   28
         Top             =   1350
         Width           =   1770
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Situa��o do Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   27
         Top             =   1845
         Width           =   1635
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "Autorizado por:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   585
         TabIndex        =   26
         Top             =   2295
         Width           =   1275
      End
      Begin VB.Line Line2 
         X1              =   3690
         X2              =   3825
         Y1              =   2385
         Y2              =   2385
      End
   End
   Begin VB.TextBox txtDirArq 
      Height          =   375
      Left            =   13080
      TabIndex        =   18
      Text            =   "DirArq"
      Top             =   360
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2895
      Left            =   45
      TabIndex        =   9
      Top             =   960
      Width           =   4005
      Begin VB.TextBox txtAte 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   3
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   10
         TabIndex        =   6
         Top             =   2280
         Width           =   1545
      End
      Begin VB.TextBox txtDe 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "dd/MM/yyyy"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   3
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1800
         Width           =   1545
      End
      Begin VB.TextBox txtCliente 
         Appearance      =   0  'Flat
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1046
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   6
         TabIndex        =   4
         Top             =   1360
         Width           =   1545
      End
      Begin VB.ComboBox cboLoja 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1320
         TabIndex        =   1
         Text            =   "Combo1"
         Top             =   360
         Width           =   2205
      End
      Begin VB.TextBox txtNumPedido 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         MaxLength       =   7
         TabIndex        =   2
         Top             =   900
         Width           =   1545
      End
      Begin VB.TextBox txtSeqPedido 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3135
         MaxLength       =   1
         TabIndex        =   3
         Text            =   "0"
         Top             =   900
         Width           =   330
      End
      Begin VB.Label lblAte 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "At�:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   14
         Top             =   2280
         Width           =   465
      End
      Begin VB.Label lblDe 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "De:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   13
         Top             =   1835
         Width           =   465
      End
      Begin VB.Label lblCliente 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   360
         TabIndex        =   12
         Top             =   1390
         Width           =   825
      End
      Begin VB.Line Line1 
         X1              =   2910
         X2              =   3045
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "N� Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   120
         TabIndex        =   11
         Top             =   945
         Width           =   1065
      End
      Begin VB.Label lbl 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         Caption         =   "Loja:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   720
         TabIndex        =   10
         Top             =   480
         Width           =   465
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7365
      Width           =   14190
      _ExtentX        =   25030
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   24977
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   7
      Top             =   810
      Width           =   14010
      _ExtentX        =   24712
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsPed.frx":23D2
      PICN            =   "frmConsPed.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdBuscar 
      Height          =   690
      Left            =   840
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsPed.frx":30C8
      PICN            =   "frmConsPed.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExportar 
      Height          =   690
      Left            =   1650
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Imprimir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsPed.frx":3DBE
      PICN            =   "frmConsPed.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grdPedido 
      Height          =   3300
      Left            =   45
      TabIndex        =   17
      Top             =   3960
      Width           =   14070
      _Version        =   65536
      _ExtentX        =   24818
      _ExtentY        =   5821
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Cols            =   12
      HighLight       =   0   'False
   End
   Begin VB.Label Label11 
      Caption         =   "* O Per�odo deve ter intervalo m�ximo de 30 dias."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   35
      Top             =   3000
      Width           =   4335
   End
   Begin VB.Label Label10 
      Caption         =   "Cliente + Per�odo ( De - At� ) *"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   34
      Top             =   2400
      Width           =   2535
   End
   Begin VB.Label Label9 
      Caption         =   "Loja + Per�odo ( De - At� ) *"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   33
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Label Label8 
      Caption         =   "Loja + N� Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4560
      TabIndex        =   32
      Top             =   1680
      Width           =   1455
   End
   Begin VB.Label Label7 
      Caption         =   "Combina��es de Pesquisa:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   375
      Left            =   4320
      TabIndex        =   31
      Top             =   1320
      Width           =   4215
   End
End
Attribute VB_Name = "frmConsPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Flag As Boolean

Private Sub cmdBuscar_Click()

Dim CodLoja As Integer
Dim VarLoja As String
Dim VarDigito As String
Dim Linha As Integer
Dim vInicio, vFim, vCliente As String
    
'Limpar o grid
vVB_Generica_001.LimpaGridComTitulo grdPedido

cmdExportar.Enabled = False
    
Call Consistencia
If Flag = True Then
    
    If cboLoja <> "" Then
        VarLoja = cboLoja.Text
        VarDigito = InStr(VarLoja, "-") - 1
        CodLoja = Mid(VarLoja, 1, VarDigito)
    End If
    If txtDe.Text = "" Then
        vInicio = ""
    Else
        vInicio = txtDe.Text
    End If
    If txtAte.Text = "" Then
        vFim = ""
    Else
        vFim = txtAte.Text
    End If
    
    '****
    If (cboLoja <> "" And txtNumPedido <> "") Or (cboLoja <> "" And txtDe <> "" And txtAte <> "") Or (txtCliente <> "" And txtDe <> "" And txtAte <> "") Then
                
        'Consulta os dados do pedido
         vBanco.Parameters.Remove "PM_NUMPED"
         vBanco.Parameters.Add "PM_NUMPED", txtNumPedido.Text, 1
         vBanco.Parameters.Remove "PM_SEQPED"
         vBanco.Parameters.Add "PM_SEQPED", txtSeqPedido.Text, 1
         vBanco.Parameters.Remove "PM_CODLOJA"
         vBanco.Parameters.Add "PM_CODLOJA", CodLoja, 1
        
         vBanco.Parameters.Remove "PM_CURSOR1"
         vBanco.Parameters.Add "PM_CURSOR1", 0, 3
         vBanco.Parameters("PM_CURSOR1").serverType = 102
         vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
         vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
         vBanco.Parameters.Remove "PM_CODERRO"
         vBanco.Parameters.Add "PM_CODERRO", 0, 2
         vBanco.Parameters.Remove "PM_TXTERRO"
         vBanco.Parameters.Add "PM_TXTERRO", "", 2
         
         vBanco.Parameters.Remove "PM_CODCLIENTE"
         vBanco.Parameters.Add "PM_CODCLIENTE", txtCliente.Text, 1
         vBanco.Parameters.Remove "PM_DT_INICIO"
         vBanco.Parameters.Add "PM_DT_INICIO", vInicio, 2 '1
         vBanco.Parameters.Remove "PM_DT_FIM"
         vBanco.Parameters.Add "PM_DT_FIM", vFim, 2 '1
         
         vSql = "PRODUCAO.PCK_VDA770.pr_CON_DADOSPEDIDO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA, :PM_CODCLIENTE, :PM_DT_INICIO, :PM_DT_FIM, :PM_CURSOR1, :PM_CODERRO,:PM_TXTERRO)"
         
         vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
         Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
                      
        '' *** preenche grid ****''
        If vObjOracle.RecordCount > 0 Then
            frmConsPed.grdPedido.Cols = 12
            frmConsPed.grdPedido.Visible = True
                    
            frmConsPed.grdPedido.Rows = vObjOracle.RecordCount + 1
            frmConsPed.grdPedido.Row = 0
            frmConsPed.grdPedido.Col = 0
        
            frmConsPed.grdPedido.ColWidth(0) = 1400
            frmConsPed.grdPedido.FixedAlignment(0) = 2
            frmConsPed.grdPedido.Text = "Loja"
            
            frmConsPed.grdPedido.Col = 1
            frmConsPed.grdPedido.ColWidth(1) = 1000
            frmConsPed.grdPedido.FixedAlignment(1) = 2
            frmConsPed.grdPedido.Text = "Pedido"
            
            frmConsPed.grdPedido.Col = 2
            frmConsPed.grdPedido.ColWidth(2) = 1000
            frmConsPed.grdPedido.FixedAlignment(2) = 2
            frmConsPed.grdPedido.Text = "Cod. Cliente"
            
            frmConsPed.grdPedido.Col = 3
            frmConsPed.grdPedido.ColWidth(3) = 3300
            frmConsPed.grdPedido.FixedAlignment(3) = 2
            frmConsPed.grdPedido.Text = "Nome Cliente"
            
            frmConsPed.grdPedido.Col = 4
            frmConsPed.grdPedido.ColWidth(4) = 1200
            frmConsPed.grdPedido.FixedAlignment(4) = 2
            frmConsPed.grdPedido.Text = "Vl.Cont�bil"
            
            frmConsPed.grdPedido.Col = 5
            frmConsPed.grdPedido.ColWidth(5) = 1200
            frmConsPed.grdPedido.FixedAlignment(5) = 2
            frmConsPed.grdPedido.Text = "Vl.Vencido"
            
            frmConsPed.grdPedido.Col = 6
            frmConsPed.grdPedido.ColWidth(6) = 2100
            frmConsPed.grdPedido.FixedAlignment(6) = 2
            frmConsPed.grdPedido.Text = "Data/Hora Entrada"
            
            frmConsPed.grdPedido.Col = 7
            frmConsPed.grdPedido.ColWidth(7) = 2100
            frmConsPed.grdPedido.FixedAlignment(7) = 2
            frmConsPed.grdPedido.Text = "Data/Hora Analise"
            
            frmConsPed.grdPedido.Col = 8
            frmConsPed.grdPedido.ColWidth(8) = 2100
            frmConsPed.grdPedido.FixedAlignment(8) = 2
            frmConsPed.grdPedido.Text = "Data/Hora Libera��o"
            
            frmConsPed.grdPedido.Col = 9
            frmConsPed.grdPedido.ColWidth(9) = 1800
            frmConsPed.grdPedido.FixedAlignment(9) = 2
            frmConsPed.grdPedido.Text = "Autorizado Por"
    
            frmConsPed.grdPedido.Col = 10
            frmConsPed.grdPedido.ColWidth(10) = 3300
            frmConsPed.grdPedido.FixedAlignment(10) = 2
            frmConsPed.grdPedido.Text = "Liberado Por"
            
            frmConsPed.grdPedido.Col = 11
            frmConsPed.grdPedido.ColWidth(11) = 1300
            frmConsPed.grdPedido.FixedAlignment(11) = 2
            frmConsPed.grdPedido.Text = "Sit. Pedido"
            
            Linha = 1
            Do While Not vObjOracle.EOF()
                frmConsPed.grdPedido.Row = Linha
                
                frmConsPed.grdPedido.Col = 0
                frmConsPed.grdPedido.ColAlignment(1) = 0
                frmConsPed.grdPedido.Text = vObjOracle.Fields("cod_loja") & "-" & vObjOracle.Fields("nome_loja")
                            
                frmConsPed.grdPedido.Col = 1
                frmConsPed.grdPedido.ColAlignment(1) = 1
                frmConsPed.grdPedido.Text = vObjOracle.Fields("num_pedido") & "-" & vObjOracle.Fields("seq_pedido")
                
                frmConsPed.grdPedido.Col = 2
                frmConsPed.grdPedido.ColAlignment(2) = 2
                frmConsPed.grdPedido.Text = vObjOracle.Fields("cod_cliente")
                
                frmConsPed.grdPedido.Col = 3
                frmConsPed.grdPedido.ColAlignment(3) = 0
                frmConsPed.grdPedido.Text = vObjOracle.Fields("nome_cliente")
                
                frmConsPed.grdPedido.Col = 4
                frmConsPed.grdPedido.ColAlignment(4) = 1
                frmConsPed.grdPedido.Text = vObjOracle.Fields("vl_contabil")
                            
                frmConsPed.grdPedido.Col = 5
                frmConsPed.grdPedido.ColAlignment(5) = 1
                If vObjOracle.Fields("valor_vencido") <> "" Then
                   frmConsPed.grdPedido.Text = vObjOracle.Fields("valor_vencido")
                Else: frmConsPed.grdPedido.Text = 0
                End If
                
                frmConsPed.grdPedido.Col = 6
                frmConsPed.grdPedido.ColAlignment(6) = 1
                frmConsPed.grdPedido.Text = vObjOracle.Fields("dt_entrada")
                             
                frmConsPed.grdPedido.Col = 7
                frmConsPed.grdPedido.ColAlignment(7) = 1
                If vObjOracle.Fields("dt_analise") <> "" Then
                   frmConsPed.grdPedido.Text = vObjOracle.Fields("dt_analise")
                Else: frmConsPed.grdPedido.Text = ""
                End If
                                
                frmConsPed.grdPedido.Col = 8
                frmConsPed.grdPedido.ColAlignment(8) = 1
                frmConsPed.grdPedido.Text = vObjOracle.Fields("data_liberacao") & " " & vObjOracle.Fields("hs_liberacao")
                
                frmConsPed.grdPedido.Col = 9
                frmConsPed.grdPedido.ColAlignment(9) = 0
                If vObjOracle.Fields("COD_RESPONSAVEL") = "0" Then
                    frmConsPed.grdPedido.Text = "Depto.Cr�dito"
                Else
                    frmConsPed.grdPedido.Text = "Ger�ncia" '& vObjOracle.Fields("nome_gerente")
                End If
                                        
                frmConsPed.grdPedido.Col = 10
                frmConsPed.grdPedido.ColAlignment(10) = 0
                frmConsPed.grdPedido.Text = vObjOracle.Fields("usuario")
                
                frmConsPed.grdPedido.Col = 11
                frmConsPed.grdPedido.ColAlignment(11) = 2
                'frmConsPed.grdPedido.Text = vObjOracle.Fields("lib_can")
                If vObjOracle!lib_can = "L" Then
                   frmConsPed.grdPedido.Text = "Liberado"
                ElseIf vObjOracle!lib_can = "C" Then
                   frmConsPed.grdPedido.Text = "Cancelado"
                ElseIf vObjOracle!lib_can = "M" Then
                   frmConsPed.grdPedido.Text = "Manuten��o"
                ElseIf vObjOracle!lib_can = "" Then
                   frmConsPed.grdPedido.Text = ""
                End If
                
                vObjOracle.MoveNext
                Linha = Linha + 1
            Loop
            cmdExportar.Enabled = True
        Else
                                   
            MsgBox "N�o existe registro a ser exibido!", , "Aten��o!"
            cboLoja.SetFocus
                        
            Exit Sub
        End If
    'algum campo em branco para consulta
    Else
        MsgBox "Informe os campos para consulta", , "Aten��o!"
        cboLoja.SetFocus
        Exit Sub
    End If
End If
End Sub
Public Function Bissexto(intAno As String) As Boolean
'
' verifica se um ano � bissexto
'
Bissexto = False

  If intAno Mod 4 = 0 Then
     If intAno Mod 100 = 0 Then
        If intAno Mod 400 = 0 Then
            Bissexto = True
        End If
     Else
           Bissexto = True
     End If
  End If

End Function


Sub Consistencia()
Dim dias As Integer
Dim strAno As String
Dim strMes As String
Dim strDia, Dia As String
Dim strData, strDataAte As String
Dim Dia1 As Integer
Dim valida As Boolean

    Flag = True
'    If txtNumPedido.Text <> "" And Len(txtNumPedido.Text) > 8 Then
'        msgbox("N�Pedido
'
'    End If
    
    If txtCliente.Text <> "" Then
       If Not IsNumeric(txtCliente.Text) Then
            MsgBox ("Digite o C�digo do Cliente - somente N�meros")
            txtCliente.SetFocus
            Flag = False
            Exit Sub
       End If
    End If
    If txtDe <> "" And txtAte <> "" Then
        If Not IsDate(txtDe) Or Not IsDate(txtAte) Then
            MsgBox "Data incorreta", , "Aten��o!"
            txtDe.SetFocus
            Flag = False
            Exit Sub
        End If
        If Len(txtDe) <> 10 Or Len(txtAte) <> 10 Then
            MsgBox ("Digite uma Data no formato DD/MM/AAAA - somente N�meros")
            txtDe.SetFocus
            Flag = False
            Exit Sub
        End If
        
       '*** Left(DateAdd("d", -1, DateAdd("m", 1, DateSerial(Year("14/10/2011"), Month("14/10/2011"), 1))), 2)
       'Dia1 = Left(DateAdd("d", -1, DateAdd("m", 1, DateSerial(Year(txtAte), Month(txtAte), 1))), 2)
        strData = txtDe
        strAno = Mid$(strData, 7, 4)
        strMes = Mid$(strData, 4, 2)
        Dia = Mid$(strData, 1, 2)
        
        ' Pega a data e o mes atual
        '
        Select Case strMes
          Case "04", "06", "09", "11"
          strDia = "30"
        
          Case "02"
          If Bissexto(Val(strAno)) Then
             strDia = "29"
          Else
             strDia = "28"
          End If
        
          Case Else
           strDia = "31"
        End Select
        If Dia <= strDia Then
            strData = txtAte
            strAno = Mid$(strData, 7, 4)
            strMes = Mid$(strData, 4, 2)
            Dia = Mid$(strData, 1, 2)
            
            ' Pega a data e o mes atual
            '
            Select Case strMes
              Case "04", "06", "09", "11"
              strDia = "30"
            
              Case "02"
              If Bissexto(Val(strAno)) Then
                 strDia = "29"
              Else
                 strDia = "28"
              End If
            
              Case Else
               strDia = "31"
            End Select
            If Dia > strDia Then
                MsgBox "Data incorreta", , "Aten��o!"
                txtDe.SetFocus
                Flag = False
                Exit Sub
            End If
        Else
            MsgBox "Data incorreta", , "Aten��o!"
            txtDe.SetFocus
            Flag = False
            Exit Sub
        End If
       'End If
       
       '****
       If CDate(txtDe) <= CDate(txtAte) Then
            dias = DateDiff("d", txtDe.Text, txtAte.Text)
            If dias > 30 Then
                MsgBox "Per�odo maior que 30 dias", , "Aten��o!"
                txtDe.SetFocus
                Flag = False
                Exit Sub
            End If
        Else
            MsgBox "Data de Inicio maior que Data Fim", , "Aren��o!"
            txtDe.SetFocus
            Flag = False
            Exit Sub
        End If
    End If
    If (cboLoja <> "" And txtNumPedido = "" And txtCliente = "" And txtDe = "" And txtAte = "") Then
        MsgBox "Preencha os campos essenciais para consulta", , "Aten��o!"
        cboLoja.SetFocus
        Flag = False
        Exit Sub
    ElseIf (cboLoja = "" And txtNumPedido = "" And txtCliente <> "" And txtDe = "" And txtAte = "") Then
        MsgBox "Preencha os campos essenciais para consulta", , "Aten��o!"
        cboLoja.SetFocus
        Flag = False
        Exit Sub
    ElseIf (txtDe <> "" And txtAte = "") Or (txtDe = "" And txtAte <> "") Then
        MsgBox "Preencher todas as datas", , "Aten��o!"
        txtDe.SetFocus
        Flag = False
        Exit Sub
'    ElseIf (cboLoja <> "" And txtNumPedido = "" And txtCliente = "" And txtDe <> "" And txtAte <> "") Then
'        Flag = True
'        Exit Sub
'    ElseIf (cboLoja <> "" And txtNumPedido <> "" And txtCliente = "" And txtDe = "" And txtAte = "") Then
'        Flag = True
'        Exit Sub
'    ElseIf (cboLoja = "" And txtNumPedido = "" And txtCliente <> "" And txtDe <> "" And txtAte <> "") Then
'        Flag = True
'        Exit Sub
'    Else
'        MsgBox "Preencha somente os campos essenciais para consulta", , "Aten��o!"
'        txtDe.SetFocus
'        Flag = False
'        Exit Sub
    End If
        
End Sub


Private Sub cmdExportar_Click()
Dim DirArq As String
Dim ArqTxt As Long
Dim Linha As String
Dim CodLoja As Integer
Dim VarLoja As String
Dim VarDigito As String
    
    FrmDir.Show 1
    
    If cboLoja <> "" Then
        VarLoja = cboLoja.Text
        VarDigito = InStr(VarLoja, "-") - 1
        CodLoja = Mid(VarLoja, 1, VarDigito)
    End If
    
    If txtDirArq.Text <> "" Then
    
        If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
             vBanco.Parameters.Remove "PM_NUMPED"
             vBanco.Parameters.Add "PM_NUMPED", txtNumPedido.Text, 1
             vBanco.Parameters.Remove "PM_SEQPED"
             vBanco.Parameters.Add "PM_SEQPED", txtSeqPedido.Text, 1
             vBanco.Parameters.Remove "PM_CODLOJA"
             vBanco.Parameters.Add "PM_CODLOJA", CodLoja, 1
            
             vBanco.Parameters.Remove "PM_CURSOR1"
             vBanco.Parameters.Add "PM_CURSOR1", 0, 3
             vBanco.Parameters("PM_CURSOR1").serverType = 102
             vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
             vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
            
             vBanco.Parameters.Remove "PM_CODERRO"
             vBanco.Parameters.Add "PM_CODERRO", 0, 2
             vBanco.Parameters.Remove "PM_TXTERRO"
             vBanco.Parameters.Add "PM_TXTERRO", "", 2
             
             vBanco.Parameters.Remove "PM_CODCLIENTE"
             vBanco.Parameters.Add "PM_CODCLIENTE", txtCliente.Text, 1
             vBanco.Parameters.Remove "PM_DT_INICIO"
             vBanco.Parameters.Add "PM_DT_INICIO", txtDe.Text, 1
             vBanco.Parameters.Remove "PM_DT_FIM"
             vBanco.Parameters.Add "PM_DT_FIM", txtAte, 1
             
             vSql = "PRODUCAO.PCK_VDA770.pr_CON_DADOSPEDIDO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA, :PM_CODCLIENTE, :PM_DT_INICIO, :PM_DT_FIM, :PM_CURSOR1, :PM_CODERRO,:PM_TXTERRO)"
                      
             vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
             
             Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
             
             ArqTxt = FreeFile
            
             DirArq = "ConsultaLogPedido" & Day(Now) & Month(Now) & Year(Now) & Hour(Now) & Minute(Now) & Second(Now) & ".csv"
            
             ' Abre o arquivo
             'Open App.Path & "\" & DirArq For Output As ArqTxt '#1
             Open txtDirArq & DirArq For Output As ArqTxt
             
             Linha = "Loja; Pedido; Cod.Cliente; Nome Cliente; Valor Cont�bil; Valor Vencido; Data Entrada; Hora Entrada; Data An�lise; Hora An�lise; Data Libera��o; Hora Libera��o; Autorizado Por; Liberador Por; Situa��o do Pedido; "
             Print #ArqTxt, Linha
                     
             Do While Not vObjOracle.EOF()
                 Linha = ""
                 Linha = Linha & vObjOracle!cod_loja & "-" & vObjOracle!nome_loja & ";"
                 Linha = Linha & vObjOracle!num_pedido & "-" & vObjOracle!seq_pedido & ";"
                 Linha = Linha & vObjOracle!cod_cliente & ";"
                 Linha = Linha & vObjOracle!nome_cliente & ";"
                 Linha = Linha & vObjOracle!vl_contabil & ";"
                 Linha = Linha & vObjOracle!valor_vencido & ";"
                 Linha = Linha & FormatDateTime(vObjOracle!dt_entrada, vbShortDate) & ";"
                 Linha = Linha & FormatDateTime(vObjOracle!dt_entrada, vbLongTime) & ";"
                 Linha = Linha & FormatDateTime(vObjOracle!dt_analise, vbShortDate) & ";"
                 Linha = Linha & FormatDateTime(vObjOracle!dt_analise, vbLongTime) & ";"
                 Linha = Linha & vObjOracle!data_liberacao & ";"
                 Linha = Linha & vObjOracle!hs_liberacao & ";"
                 If vObjOracle.Fields("COD_RESPONSAVEL") = "0" Then
                    Linha = Linha & "Depto.Cr�dito" & ";"
                 Else
                    Linha = Linha & "Ger�ncia" & ";"  '& vObjOracle.Fields("nome_gerente")
                 End If
                 Linha = Linha & vObjOracle!Usuario & ";"
                 If vObjOracle!lib_can = "L" Then
                    Linha = Linha & "Liberado" & ";"
                 ElseIf vObjOracle!lib_can = "C" Then
                    Linha = Linha & "Cancelado" & ";"
                 ElseIf vObjOracle!lib_can = "M" Then
                    Linha = Linha & "Manuten��o" & ";"
                 ElseIf vObjOracle!lib_can = "" Then
                    Linha = Linha & ";"
                 End If
                 
                 Print #ArqTxt, Linha
                 vObjOracle.MoveNext
             Loop
             
             Close ArqTxt
            
             Screen.MousePointer = 0
             MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
             
         Else
         
         Exit Sub
      End If
    End If
End Sub

'Private Sub txtAte_KeyPress(Keyascii As Integer)
'    DATA Keyascii, txtAte
'End Sub
'
'
'Private Sub txtDe_KeyPress(Keyascii As Integer)
'    DATA Keyascii, txtDe
'End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 2000
    
    Set vObjOracle = vVB_Generica_001.TabelaLoja(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboLoja, "DEP�SITO")
    
    txtNumPedido.Text = ""
    txtCliente.Text = ""
    txtDe.Text = ""
    txtAte.Text = ""
    
    cmdBuscar.Enabled = True
    cmdExportar.Enabled = False
    
End Sub

Private Sub txtNumPedido_KeyPress(Keyascii As Integer)
 
 Numero Keyascii, txtNumPedido.Text

End Sub
Private Sub txtSeqPedido_KeyPress(Keyascii As Integer)
 Numero Keyascii, txtSeqPedido.Text

End Sub

Private Sub txtSeqPedido_GotFocus()
    txtSeqPedido.SelStart = 0
    txtSeqPedido.SelLength = Len(txtSeqPedido)
End Sub

Function Numero(ByRef Keyascii, ByRef TxtCampo)
    If Keyascii = 8 Then    'BACKSPACE
        Numero = Keyascii
        Exit Function
    End If
    If Chr(Keyascii) < "0" Or Chr(Keyascii) > "9" Then
        Keyascii = 0
        Beep
    End If
    Numero = Keyascii
End Function
''
''Private Sub txtSeqPedido_LostFocus()
''    Dim CodLoja As Integer
''    Dim VarLoja As String
''    Dim VarDigito As String
''    Dim CodResp As String
''
''    If cboLoja = "" Then
''        MsgBox "Escolha a loja para a consulta do pedido", , "Aten��o!"
''        cboLoja.SetFocus
''        Exit Sub
''    Else
''        VarLoja = cboLoja.Text
''        VarDigito = InStr(VarLoja, "-") - 1
''        CodLoja = Mid(VarLoja, 1, VarDigito)
''    End If
''
''    If txtNumPedido = "" Then
''        MsgBox "Digite o n�mero do pedido a ser consultado", , "Aten��o!"
''        txtNumPedido.SetFocus
''        Exit Sub
''    End If
''
''   'Consulta os dados do pedido
''    vBanco.Parameters.Remove "PM_NUMPED"
''    vBanco.Parameters.Add "PM_NUMPED", txtNumPedido.Text, 1
''    vBanco.Parameters.Remove "PM_SEQPED"
''    vBanco.Parameters.Add "PM_SEQPED", txtSeqPedido.Text, 1
''    vBanco.Parameters.Remove "PM_CODLOJA"
''    vBanco.Parameters.Add "PM_CODLOJA", CodLoja, 1
''
''    vBanco.Parameters.Remove "PM_CURSOR1"
''    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
''    vBanco.Parameters("PM_CURSOR1").serverType = 102
''    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
''    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
''
''    vBanco.Parameters.Remove "PM_CODERRO"
''    vBanco.Parameters.Add "PM_CODERRO", 0, 2
''    vBanco.Parameters.Remove "PM_TXTERRO"
''    vBanco.Parameters.Add "PM_TXTERRO", "", 2
''
''      vBanco.Parameters.Remove "PM_CODCLIENTE"
''         vBanco.Parameters.Add "PM_CODCLIENTE", txtCliente.Text, 1
''         vBanco.Parameters.Remove "PM_DT_INICIO"
''         vBanco.Parameters.Add "PM_DT_INICIO", txtDe.Text, 1
''         vBanco.Parameters.Remove "PM_DT_FIM"
''         vBanco.Parameters.Add "PM_DT_FIM", txtAte, 1
''
''         'vSql = "PRODUCAO.PCK_VDA770.pr_CON_DADOSPEDIDO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
''         vSql = "PRODUCAO.PCK_VDA770.pr_CON_DADOSPEDIDO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO, :PM_CODCLIENTE, :PM_DT_INICIO, :PM_DT_FIM)"
''
''    vErro = vVB_Generica_001.ExecutaPL(vBanco, vSql)
''
''    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
''
''
''    If Not vObjOracle.EOF Then
''        CodResp = vObjOracle!COD_RESPONSAVEL
''        txtLiberado.Text = vObjOracle!Usuario
''        txtData.Text = vObjOracle!data_liberacao
''        txtHorario.Text = vObjOracle!hs_liberacao
''        If vObjOracle!lib_can = "L" Then
''            txtSituacao.Text = "Liberado"
''        ElseIf vObjOracle!lib_can = "C" Then
''            txtSituacao.Text = "Cancelado"
''        End If
''        If vObjOracle!COD_RESPONSAVEL = "0" Then
''            txtAutorizado.Text = "Depto.Cr�dito"
''        Else
''            txtAutorizado.Text = "Ger�ncia"
''            Line2.Visible = True
''            txtGerente.Visible = True
''           'Busca o c�digo do gerente que fez a libera��o
''           txtGerente.Text = vObjOracle!nome_gerente
''        End If
''    Else
''        MsgBox "Este pedido n�o foi liberado pelo Depto. de Cr�dito de Campinas!", , "Aten��o!"
''        MsgBox "Verifique a Loja escolhida e o N�mero de Pedido digitado!", , "Verifique com Aten��o!"
''        cboLoja.SetFocus
''   End If
''
''End Sub




