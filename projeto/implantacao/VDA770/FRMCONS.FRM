VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmConsPed 
   Caption         =   "Consulta por Pedido e Loja"
   ClientHeight    =   4455
   ClientLeft      =   2220
   ClientTop       =   2775
   ClientWidth     =   7095
   Icon            =   "frmCons.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   7095
   Begin VB.Frame Frame2 
      Caption         =   "Resultado da Consulta"
      ForeColor       =   &H00000080&
      Height          =   2445
      Left            =   90
      TabIndex        =   21
      Top             =   1305
      Width           =   6900
      Begin VB.TextBox txtGerente 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3915
         TabIndex        =   19
         Top             =   1935
         Width           =   1770
      End
      Begin VB.TextBox txtAutorizado 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1845
         TabIndex        =   17
         Top             =   1935
         Width           =   1770
      End
      Begin VB.TextBox txtSituacao 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1845
         TabIndex        =   15
         Top             =   1530
         Width           =   1770
      End
      Begin VB.TextBox txtHorario 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1845
         TabIndex        =   13
         Top             =   1125
         Width           =   1770
      End
      Begin VB.TextBox txtData 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1845
         TabIndex        =   11
         Top             =   720
         Width           =   1770
      End
      Begin VB.TextBox txtLiberado 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1845
         TabIndex        =   9
         Top             =   315
         Width           =   1770
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   3735
         TabIndex        =   18
         Top             =   1935
         Width           =   75
      End
      Begin VB.Label lblAutorizado 
         AutoSize        =   -1  'True
         Caption         =   "Autorizado por............:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   16
         Top             =   2025
         Width           =   1605
      End
      Begin VB.Label lblSituacao 
         AutoSize        =   -1  'True
         Caption         =   "Situa��o do Pedido...:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   14
         Top             =   1620
         Width           =   1575
      End
      Begin VB.Label lblHorario 
         AutoSize        =   -1  'True
         Caption         =   "Hor�rio da Libera��o.:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   12
         Top             =   1215
         Width           =   1575
      End
      Begin VB.Label lblLiberado 
         AutoSize        =   -1  'True
         Caption         =   "Liberado por..............:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   8
         Top             =   405
         Width           =   1560
      End
      Begin VB.Label lblData 
         AutoSize        =   -1  'True
         Caption         =   "Data da Libera��o....:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   10
         Top             =   810
         Width           =   1545
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Dados necess�rios para Consulta"
      ForeColor       =   &H00000080&
      Height          =   1095
      Left            =   90
      TabIndex        =   20
      Top             =   90
      Width           =   6900
      Begin VB.TextBox txtSeqPedido 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2520
         TabIndex        =   6
         Text            =   "0"
         Top             =   720
         Width           =   240
      End
      Begin VB.TextBox txtNumPedido 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1305
         TabIndex        =   4
         Top             =   720
         Width           =   1005
      End
      Begin VB.ComboBox cboLoja 
         Height          =   315
         Left            =   1305
         TabIndex        =   1
         Top             =   270
         Width           =   2310
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Digite o n�mero do pedido a ser consultado."
         ForeColor       =   &H00000080&
         Height          =   195
         Left            =   2925
         TabIndex        =   7
         Top             =   810
         Width           =   3120
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Escolha a loja do pedido a ser consultado."
         ForeColor       =   &H00000080&
         Height          =   195
         Left            =   3735
         TabIndex        =   2
         Top             =   405
         Width           =   3000
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   2385
         TabIndex        =   5
         Top             =   720
         Width           =   75
      End
      Begin VB.Label lblNumPedido 
         AutoSize        =   -1  'True
         Caption         =   "N� do Pedido.:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   810
         Width           =   1035
      End
      Begin VB.Label lblLoja 
         AutoSize        =   -1  'True
         Caption         =   "Loja...............:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   180
         TabIndex        =   0
         Top             =   405
         Width           =   1020
      End
   End
   Begin Threed.SSCommand sscmdVoltar 
      Height          =   555
      Left            =   6435
      TabIndex        =   22
      Top             =   3825
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      ForeColor       =   0
      Picture         =   "frmCons.frx":030A
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmConsPed"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
    
    Dim CodNome As String
    
    oracleSQL = "Select a.cod_loja, b.nome_fantasia " & _
                "from deposito_visao a, loja b " & _
                "where a.cod_loja = b.cod_loja and " & _
                "a.nome_programa = 'VDA070'"
    
    Set oradynaset = oradatabase.dbcreatedynaset(oracleSQL, 0&)

    Do While Not oradynaset.EOF
        CodNome = Format(oradynaset!cod_loja, "00") & " - " & oradynaset!NOME_FANTASIA
        cboLoja.AddItem CodNome
        oradynaset.MoveNext
    Loop

    
    txtLiberado.Enabled = False
    txtData.Enabled = False
    txtHorario.Enabled = False
    txtSituacao.Enabled = False
    txtAutorizado.Enabled = False
    txtGerente.Enabled = False
    Label4.Visible = False
    txtGerente.Visible = False

End Sub

Private Sub mnuVoltar_Click()
    Unload Me
End Sub

Private Sub sscmdVoltar_Click()
    Unload Me
End Sub

Private Sub txtNumPedido_GotFocus()
    txtNumPedido.Text = ""
    txtLiberado.Text = ""
    txtData.Text = ""
    txtHorario.Text = ""
    txtSituacao.Text = ""
    txtAutorizado.Text = ""
    txtGerente.Text = ""
    Label4.Visible = False
    txtGerente.Visible = False
End Sub

Private Sub txtSeqPedido_GotFocus()
    txtSeqPedido.SelStart = 0
    txtSeqPedido.SelLength = Len(txtSeqPedido)
End Sub

Private Sub txtSeqPedido_LostFocus()
    Dim CodLoja As Integer
    Dim VarLoja As String
    Dim VarDigito As String
    Dim CodResp As String

    If cboLoja = "" Then
        MsgBox "Escolha a loja para a consulta do pedido", , "Aten��o!"
        cboLoja.SetFocus
        Exit Sub
    Else
        VarLoja = cboLoja.Text
        VarDigito = InStr(VarLoja, "-") - 1
        CodLoja = Mid(VarLoja, 1, VarDigito)
    End If
    
    If txtNumPedido = "" Then
        MsgBox "Digite o n�mero do pedido a ser consultado", , "Aten��o!"
        txtNumPedido.SetFocus
        Exit Sub
    End If
    
   'Consulta no Access dos dados do pedido
    
    accessSQL = "Select usuario,data_liberacao, hs_liberacao, " & _
                "lib_can,responsavel,cod_responsavel " & _
                "from controle " & _
                "where num_pedido=" & txtNumPedido.Text & " and " & _
                "seq_pedido=" & txtSeqPedido.Text & " and cod_loja =" & CodLoja & ""
    
    Set ss = dbAccess.CreateSnapshot(accessSQL)
    FreeLocks

    If Not ss.EOF Then
        CodResp = ss!COD_RESPONSAVEL
        txtLiberado.Text = ss!USUARIO
        txtData.Text = ss!DATA_LIBERACAO
        txtHorario.Text = ss!HS_LIBERACAO
        If ss!lib_can = "L" Then
            txtSituacao.Text = "Liberado"
        ElseIf ss!lib_can = "C" Then
            txtSituacao.Text = "Cancelado"
        End If
        If ss!responsavel = "C" Then
            txtAutorizado.Text = "Depto.Cr�dito"
        ElseIf ss!responsavel = "G" Then
            txtAutorizado.Text = "Ger�ncia"
            Label4.Visible = True
            txtGerente.Visible = True
           'Busca o c�digo do gerente que fez a libera��o
            accessSQL = "Select a.nome_gerente " & _
                        "from gerentes a, controle b " & _
                        "where a.cod_gerente=b.cod_responsavel and " & _
                        "a.cod_gerente=" & CodResp & ""
                        
            Set ss1 = dbAccess.CreateSnapshot(accessSQL)
            FreeLocks
            
            txtGerente.Text = ss1!NOME_GERENTE
        End If
    Else
        MsgBox "Este pedido n�o foi liberado pelo Depto. de Cr�dito de Campinas!", , "Aten��o!"
        MsgBox "Verifique a Loja escolhida e o N�mero de Pedido digitado!", , "Verifique com Aten��o!"
        cboLoja.SetFocus
    End If
    
End Sub
