Attribute VB_Name = "Module1"
'Janela de Pastas - Inicio
Public ResultNumGerDupl_ana
    
    Public fName As String
    Public fPath As String
    Public bi As BROWSEINFO
    Public tmpPath As String
    Public Type BROWSEINFO
      hOwner          As Long
      pidlRoot        As Long
      pszDisplayName  As String
      lpszTitle       As String
      ulFlags         As Long
      lpfn            As Long
      lParam          As Long
      iImage          As Long
    End Type
        
    Public Declare Function SHGetPathFromIDList Lib _
       "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, _
        ByVal pszPath As String) As Long
    
    Public Declare Function SHBrowseForFolder Lib _
       "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long
'Janela de pastas - Fim



Sub DATA(ByRef KeyAscii, ByRef TxtCampo)
    'on error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(TxtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(TxtCampo.Text, bTam) = "/" Then
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 2)
        Else
            TxtCampo.Text = Mid$(TxtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = TxtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(TxtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                TxtCampo.Text = TxtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(TxtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(TxtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = TxtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        TxtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub

Function Valida_Dt_LostFocus(Campo As TextBox)
    If Trim(Campo.Text) = "" Then
        Exit Function
    End If
    
    If IsDate(Campo.Text) = False Then
        MsgBox "Esta n�o e uma Data V�lida, Favor Verificar !", vbCritical, "Aten��o"
        Campo.Text = ""
        Campo.SetFocus
    End If
End Function

'Abrir tela de Pastas
Public Function vbGetBrowseDirectory() As String

    pidl = SHBrowseForFolder(bi)
    
    tmpPath = Space$(512)
    R = SHGetPathFromIDList(ByVal pidl, ByVal tmpPath)
      
    If R Then
          pos = InStr(tmpPath, Chr$(0))
          tmpPath = Left(tmpPath, pos - 1)
          vbGetBrowseDirectory = ValidateDir(tmpPath)
    Else: vbGetBrowseDirectory = ""
    End If

End Function

Public Function ValidateDir(tmpPath As String) As String

    If Right$(tmpPath, 1) = "\" Then
          ValidateDir = tmpPath
    Else: ValidateDir = tmpPath & "\"
    End If

End Function

Public Function Numero(ByRef KeyAscii, ByRef TxtCampo)
    If KeyAscii = 8 Or KeyAscii = 47 Then    'BACKSPACE , "/"
        Numero = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    
    Numero = KeyAscii
End Function

Public Function Bissexto(intAno As String) As Boolean
'
' verifica se um ano � bissexto
'
Bissexto = False

  If intAno Mod 4 = 0 Then
     If intAno Mod 100 = 0 Then
        If intAno Mod 400 = 0 Then
            Bissexto = True
        End If
     Else
           Bissexto = True
     End If
  End If
  
End Function
  
Public Sub RegistraExecucao(Texto As String)
    frmLibCredito.textBoxErro.Text = frmLibCredito.textBoxErro.Text & Texto & vbCrLf
End Sub

