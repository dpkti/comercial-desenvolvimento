VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmConsGer 
   Caption         =   "Consulta por Per�odo e Gerente"
   ClientHeight    =   6165
   ClientLeft      =   300
   ClientTop       =   2010
   ClientWidth     =   11295
   ForeColor       =   &H00000000&
   Icon            =   "frmGer.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6165
   ScaleWidth      =   11295
   Begin MSGrid.Grid grdGerente 
      Height          =   5235
      Left            =   225
      TabIndex        =   4
      Top             =   855
      Width           =   10995
      _Version        =   65536
      _ExtentX        =   19394
      _ExtentY        =   9234
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Cols            =   7
   End
   Begin Threed.SSCommand sscmdPesquisa 
      Height          =   555
      Left            =   3735
      TabIndex        =   2
      Top             =   150
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Picture         =   "frmGer.frx":030A
   End
   Begin VB.ComboBox cboGerentes 
      Height          =   315
      Left            =   900
      TabIndex        =   1
      Top             =   135
      Width           =   2625
   End
   Begin Threed.SSCommand sscmdLimpa 
      Height          =   555
      Left            =   4410
      TabIndex        =   3
      Top             =   150
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Picture         =   "frmGer.frx":0624
   End
   Begin Threed.SSCommand sscmdVoltar 
      Height          =   555
      Left            =   6450
      TabIndex        =   5
      Top             =   150
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Picture         =   "frmGer.frx":0A76
   End
   Begin Threed.SSCommand sscmdImprimir 
      Height          =   555
      Left            =   5085
      TabIndex        =   6
      Top             =   150
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Picture         =   "frmGer.frx":0EC8
   End
   Begin Threed.SSCommand sscmdArqTxt 
      Height          =   555
      Left            =   5760
      TabIndex        =   7
      Top             =   135
      Visible         =   0   'False
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   979
      _StockProps     =   78
      Caption         =   ".TXT"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmGer.frx":11E2
   End
   Begin VB.Label lblGerente 
      Caption         =   "Gerente:"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   135
      TabIndex        =   0
      Top             =   225
      Width           =   690
   End
   Begin VB.Menu mnuVoltar 
      Caption         =   "&Voltar"
   End
End
Attribute VB_Name = "frmConsGer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub pImprimirCabecalho()
    Printer.Print ""
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
        
    Printer.Print
    lstrLinha = "                                   CONSULTA  LOG  DE  CREDITO                                   "
    Printer.Print lstrLinha
    Printer.Print
    
    Printer.Print String(96, "-")
    Printer.Print
    
    lstrLinha = "PER�ODO DE: " & Format(txtInicio.Text, "dd/mm/yy") & ""
    lstrLinha = lstrLinha & " � " & Format(txtFim.Text, "dd/mm/yy") & ""
    
    Printer.Print lstrLinha
    
    Printer.Print
    Printer.Print String(96, "-")
    
    lstrLinha = ""
    lstrLinha = "LOJA| NUM.PEDIDO | USU�RIO | DATA DE |HOR�RIO DE| SITUA��O |RESPONS�VEL"
    Printer.Print lstrLinha
    lstrLinha = "    |            |         |LIBERA��O|LIBERA��O |DO PEDIDO |           "
    Printer.Print lstrLinha
    Printer.Print String(96, "-")
    
End Sub


Private Sub Limpa_Grid()
    If grdGerente <> "" Then
        With grdGerente
            .Rows = 2
            .Row = 1
            
            For I = 0 To .Cols - 1
                .Col = I: .Text = ""
            Next I
        End With
    
    Call Inicia_Grid
    End If
End Sub

Private Sub Inicia_Grid()

    grdGerente.Row = 0
    grdGerente.Col = 0
    grdGerente.ColWidth(0) = 500
    grdGerente.FixedAlignment(0) = 2
    grdGerente.Text = "LOJA"
    grdGerente.Col = 1
    grdGerente.ColWidth(1) = 1500
    grdGerente.FixedAlignment(1) = 2
    grdGerente.Text = "NUM.PEDIDO"
    grdGerente.Col = 2
    grdGerente.ColWidth(2) = 1500
    grdGerente.FixedAlignment(2) = 2
    grdGerente.Text = "USU�RIO"
    grdGerente.Col = 3
    grdGerente.ColWidth(3) = 1500
    grdGerente.FixedAlignment(3) = 2
    grdGerente.Text = "DT LIBERA��O"
    grdGerente.Col = 4
    grdGerente.ColWidth(4) = 1500
    grdGerente.FixedAlignment(4) = 2
    grdGerente.Text = "HS LIBERA��O"
    grdGerente.Col = 5
    grdGerente.ColWidth(5) = 2000
    grdGerente.FixedAlignment(5) = 2
    grdGerente.Text = "SITUA��O DO PEDIDO"
    grdGerente.Col = 6
    grdGerente.ColWidth(6) = 2000
    grdGerente.FixedAlignment(6) = 2
    grdGerente.Text = "RESPONS�VEL"
    grdGerente.FixedRows = 1

End Sub


Private Sub Form_Load()
   'Consulta Access para inclus�o no combo de gerentes
    accessSQL = "Select cod_Gerente,Nome_Gerente from gerentes " & _
                "order by Nome_gerente desc"
    
    Set ss = dbAccess.CreateSnapshot(accessSQL)
    FreeLocks

    cboGerentes.AddItem ""
    Do While Not ss.EOF
        cboGerentes.AddItem Format(ss!cod_gerente, "00") & " - " & ss!NOME_GERENTE
        ss.MoveNext
    Loop
    
End Sub

Private Sub mnuVoltar_Click()
    Unload Me
End Sub


Private Sub sscmdArqTxt_Click()
    Dim j As Long           'Ponteiro de caracter no campo observa��o
    Dim sGER As String   'Guarda o c�digo de DPK para impress�o
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma gera��o do arquivo?", vbYesNo, "VDA770") = vbYes Then
    
        Screen.MousePointer = 11
      
       'Gera arquivo com o nome CONSDDMMAAHHMMSS.TXT
        vdt_cad = Date
        vhs_cad = Time
        Open "C:\CONSULTA\CONS" & Mid(vdt_cad, 1, 2) & Mid(vdt_cad, 4, 2) & Mid(vdt_cad, 7, 2) & _
                                  Mid(vhs_cad, 1, 2) & Mid(vhs_cad, 4, 2) & Mid(vhs_cad, 7, 2) & ".txt" For Output As #1
        
        Print #1, "RELAT�RIO DE CONSULTA - LOG DE CR�DITO "
        Print #1, ""
        Print #1, "Loja;Num.Pedido;Usuario;Dt.Libera��o;Hs.Libera��o;Situa��o do Pedido;Respons�vel"
        Print #1, ""
               
        With grdGerente
        
            Do
                .Row = .Row + 1
               'GRAVAR LINHA DE DETALHE
                .Col = 0:  lstrLinha = Format(.Text, "00") & ";"
                .Col = 1:  lstrLinha = lstrLinha & .Text & ";"
                .Col = 2:  lstrLinha = lstrLinha & .Text & ";"
                .Col = 3:  lstrLinha = lstrLinha & Format(.Text, "dd/mm/yy") & ";"
                .Col = 4:  lstrLinha = lstrLinha & Format(.Text, "hh:mm:ss") & ";"
                .Col = 5:  lstrLinha = lstrLinha & .Text & ";"
                .Col = 6:  lstrLinha = lstrLinha & .Text
                
                Print #1, lstrLinha
                
            Loop Until .Row = .Rows - 1
            
        End With
    End If
               
    Close #1
    
    Screen.MousePointer = 0
    MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"

End Sub




Private Sub sscmdImprimir_Click()
    
    Dim liLinhaGrid, liColunaGrid As Integer
    
    liLinhaImpressao = 0
    
    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
        With grdGerente
        
            Printer.Print ""
            Printer.FontName = "Courier New"
            Printer.FontSize = 10
            
           'GRID VAZIO
            If .Rows = 2 Then
                .Col = 0
                If .Text = "" Then
                    Exit Sub
                End If
            End If
            
            .Row = 0
            liPagina = 0
            
            Call pImprimirCabecalho
            
            Do
                .Row = .Row + 1
               'IMPRIMIR LINHA DE DETALHE
                .Col = 0:  lstrLinha = " " & Format(.Text, "00")
                .Col = 1:  lstrLinha = lstrLinha & " | " & Space(10 - Len(.Text)) & .Text & ""
                .Col = 2:  lstrLinha = lstrLinha & " | " & Space(7 - Len(.Text)) & .Text & ""
                .Col = 3:  lstrLinha = lstrLinha & " |" & Format(.Text, "dd/mm/yy") & ""
                .Col = 4:  lstrLinha = lstrLinha & " | " & Format(.Text, "hh:mm:ss") & ""
                .Col = 5:  lstrLinha = lstrLinha & " | " & Space(8 - Len(.Text)) & .Text & ""
                .Col = 6:  lstrLinha = lstrLinha & " | " & .Text & ""
                
                Printer.Print lstrLinha
                
                liLinhaImpressao = liLinhaImpressao + 1
                
               'VERIFICA QUEBRA DE PAGINA
                If liLinhaImpressao > 56 And .Row <> .Rows - 1 Then
                    Printer.Print String(96, "-")
                    Printer.EndDoc
                    Printer.Print ""
                    Printer.FontName = "Courier New"
                    Printer.FontSize = 10
                    Call pImprimirCabecalho
                    liLinhaImpressao = 0
                    liPagina = liPagina + 1
                End If
            
            Loop Until .Row = .Rows - 1
            
            Printer.Print String(96, "-")
            Printer.EndDoc

            MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
        End With
    End If
End Sub

Private Sub sscmdLimpa_Click()
    Call Limpa_Grid
    
    grdGerente.Visible = False
    cboGerentes.Text = ""
    cboGerentes.SetFocus
End Sub

Private Sub sscmdPesquisa_Click()
    Dim varCombo
    Dim varNumGer
    Dim ResultNumGer
    Dim DataInicio As String
    Dim DataFim As String
    
   'Limpar o grid
    Call Limpa_Grid
    
    grdGerente.Visible = False
    
    Screen.MousePointer = 11
   
    If cboGerentes.Text <> "" Then
        varCombo = cboGerentes.Text
        varNumGer = InStr(varCombo, "-") - 1
        ResultNumGer = Trim(Mid(varCombo, 1, varNumGer))

        accessSQL = "Select a.cod_loja,a.num_pedido,a.usuario,a.data_liberacao, " & _
                    "a.hs_liberacao,a.lib_can,a.responsavel,b.nome_gerente " & _
                    "from controle a, gerentes b " & _
                    "where a.cod_responsavel=b.cod_gerente and " & _
                    "a.responsavel= 'G' and " & _
                    "b.cod_gerente= " & ResultNumGer & " " & _
                    "order by b.nome_gerente,a.data_liberacao"
    Else
        accessSQL = "Select a.cod_loja,a.num_pedido,a.usuario,a.data_liberacao, " & _
                    "a.hs_liberacao,a.lib_can,a.responsavel,b.nome_gerente " & _
                    "from controle a, gerentes b " & _
                    "where a.responsavel= 'G' and " & _
                    "a.cod_responsavel=b.cod_gerente " & _
                    "order by b.nome_gerente,a.data_liberacao"
                    
    End If
        
    Set ss = dbAccess.CreateSnapshot(accessSQL)
    FreeLocks
    
    If ss.EOF() Then
        MsgBox "N�o h� registros para esta consulta."
        Call Inicia_Grid
    Else
       'Monta Grid
        Call Inicia_Grid
        
        Do While Not ss.EOF()
            DoEvents
            grdGerente.AddItem ss.Fields("COD_LOJA") & Chr(9) & ss.Fields("NUM_PEDIDO") & _
            Chr(9) & ss.Fields("USUARIO") & Chr(9) & ss.Fields("DATA_LIBERACAO") & _
            Chr(9) & ss.Fields("HS_LIBERACAO") & _
            Chr(9) & IIf(ss.Fields("LIB_CAN") = "L", "LIBERADO", "CANCELADO") & _
            Chr(9) & ss.Fields("NOME_GERENTE")
            ss.MoveNext
            grdGerente.Visible = True
        Loop
        sscmdArqTxt.Visible = True
    End If
    
    If ss.EOF() And grdGerente.Rows >= 3 Then
        grdGerente.RemoveItem 1
    End If
    
    Screen.MousePointer = 0

End Sub

Private Sub sscmdVoltar_Click()
    Unload Me
End Sub

Private Sub txtFim_GotFocus()
    txtFim.SelStart = 0
    txtFim.SelLength = Len(txtFim)
End Sub

Private Sub txtFim_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtFim)
End Sub

Private Sub txtInicio_GotFocus()
    txtInicio.SelStart = 0
    txtInicio.SelLength = Len(txtInicio)
End Sub

Private Sub txtInicio_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtInicio)
End Sub
