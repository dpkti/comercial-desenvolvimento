VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmArquivo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gera Arquivo Texto"
   ClientHeight    =   8190
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8445
   Icon            =   "frmArquivo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   546
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   563
   Begin VB.Frame fra 
      Caption         =   "Dados necess�rios para Consulta"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   6900
      Left            =   45
      TabIndex        =   0
      Top             =   900
      Width           =   8340
      Begin VB.ComboBox cboSupervisor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1440
         TabIndex        =   1
         Text            =   "Combo1"
         Top             =   360
         Width           =   5010
      End
      Begin MSGrid.Grid grdSupervisor 
         Height          =   5865
         Left            =   135
         TabIndex        =   2
         Top             =   900
         Width           =   8070
         _Version        =   65536
         _ExtentX        =   14235
         _ExtentY        =   10345
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   7
         HighLight       =   0   'False
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Supervisores:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   270
         TabIndex        =   3
         Top             =   405
         Width           =   1095
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   4
      Top             =   7860
      Width           =   8445
      _ExtentX        =   14896
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   14843
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   5
      Top             =   810
      Width           =   8340
      _ExtentX        =   14711
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivo.frx":23D2
      PICN            =   "frmArquivo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1215
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Pesquisar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivo.frx":30C8
      PICN            =   "frmArquivo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1980
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivo.frx":3DBE
      PICN            =   "frmArquivo.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   2745
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Importar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivo.frx":46B4
      PICN            =   "frmArquivo.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComDlg.CommonDialog cd 
      Left            =   7200
      Top             =   180
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmArquivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmd1_Click()
    Dim varComboDupl
    Dim varNumGerDupl
    Dim ResultNumGerDupl
    Dim DataInicioDupl As String
    Dim DataFimDupl As String
    Dim vLoja As Double
    Dim vNumero As String
    Dim vSequencia As String
    Dim count
    
   'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdSupervisor
    cmd3.Enabled = False
    
    Screen.MousePointer = 11
    
    If cboSupervisor.Text = "" Then
        MsgBox "Escolha o supervisor/gerente a ser consultado."
        cboSupervisor.SetFocus
        Exit Sub
    End If
    
    ''Call Inicia_GridSupervisor
  
    varComboDupl = cboSupervisor.Text
    varNumGerDupl = InStr(varComboDupl, "-") - 1
    ResultNumGerDupl = Trim(Mid(varComboDupl, 1, varNumGerDupl))

    vBanco.Parameters.Remove "PM_CODGER"
    vBanco.Parameters.Add "PM_CODGER", ResultNumGerDupl, 1
    
    vBanco.Parameters.Remove "PM_CODANA"
    vBanco.Parameters.Add "PM_CODANA", 0, 1
    
    vBanco.Parameters.Remove "PM_DTINICIO"
    vBanco.Parameters.Add "PM_DTINICIO", Null, 1
    
    vBanco.Parameters.Remove "PM_DTFIM"
    vBanco.Parameters.Add "PM_DTFIM", Null, 1
   
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.pr_CON_PEDIDOSGERDATA(:PM_CODGER,:PM_CODANA,:PM_DTINICIO,:PM_DTFIM,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    vVB_Generica_001.CarregaGridTabela grdSupervisor, vObjOracle, 5
    
    cmd3.Enabled = True
    
    Screen.MousePointer = 0
End Sub

Private Sub cmd2_Click()
    'Limpar o grid
    vVB_Generica_001.LimpaGridComTitulo grdSupervisor
    cmd3.Enabled = False
End Sub

Private Sub cmd3_Click()
    Dim j As Long           'Ponteiro de caracter no campo observa��o
    Dim sGER As String   'Guarda o c�digo de DPK para impress�o
    Dim liLinhaGrid, liColunaGrid As Integer
    Dim vPath As String
    
    liLinhaImpressao = 0
    
    vPath = vbGetBrowseDirectory
    
    If vPath <> "" Then
    
        Screen.MousePointer = 11
      
       'Gera arquivo com o nome CONSDDMMAAHHMMSS.TXT
        vdt_cad = Date
        vhs_cad = Time
        Open vPath & Mid(vdt_cad, 1, 2) & Mid(vdt_cad, 4, 2) & Mid(vdt_cad, 7, 2) & _
                                  Mid(vhs_cad, 1, 2) & Mid(vhs_cad, 4, 2) & Mid(vhs_cad, 7, 2) & ".txt" For Output As #1
        
        Print #1, "RELAT�RIO DE CONSULTA - DUPLICATAS POR GERENTE"
        Print #1, ""
        
        With grdSupervisor
        
        lstrlinha = ""
        .Row = 0
        For i = 1 To .Cols - 1
            .Col = i
            lstrlinha = lstrlinha & .Text & ";"
        Next
        lstrlinha = Left(lstrlinha, Len(lstrlinha) - 1)
         
        Print #1, lstrlinha
        Print #1, ""
        lstrlinha = ""
            Do
                .Row = .Row + 1
                
                lstrlinha = ""
                For i = 1 To .Cols - 1
                    .Col = i
                    lstrlinha = lstrlinha & .Text & ";"
                Next
                lstrlinha = Left(lstrlinha, Len(lstrlinha) - 1)
                
               'GRAVAR LINHA DE DETALHE
               ' .Col = 0:  lstrlinha = Format(.Text, "00") & ";"
               ' .Col = 1:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 2:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 3:  lstrlinha = lstrlinha & Format(.Text, "dd/mm/yy") & ";"
               ' .Col = 4:  lstrlinha = lstrlinha & Format(.Text, "hh:mm:ss") & ";"
               ' .Col = 5:  lstrlinha = lstrlinha & .Text & ";"
               ' .Col = 6:  lstrlinha = lstrlinha & .Text
                
                Print #1, lstrlinha
                
            Loop Until .Row = .Rows - 1
            
        End With
        
        Close #1
    
        Screen.MousePointer = 0
        MsgBox "Arquivo gerado com sucesso!", vbInformation, "Aten��o!"
        
    End If
               
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
       Me.Top = 2000
    Me.Left = 2000
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
   
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    'vBanco.Parameters.Remove "PM_TXTERRO"
    'vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
    vSql = "PRODUCAO.PCK_VDA770.PR_RESPONSAVEL(:PM_CURSOR1,:PM_CODERRO)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cboSupervisor, "COD", "NOME_USUARIO")
    
End Sub

Private Sub Inicia_GridSupervisor()

    With grdSupervisor
        .Row = 0
        .Cols = 3
        .RowHeight(0) = 500
        
        .Col = 0: .ColWidth(0) = 1000: .FixedAlignment(0) = 2
        .Text = "COD. CLIENTE"
        
        .Col = 1: .ColWidth(1) = 3000: .FixedAlignment(1) = 2
        .Text = "NOME CLIENTE"
        
        .Col = 2: .ColWidth(2) = 1200: .FixedAlignment(2) = 2
        .Text = "TOTAL DUPLICATAS"
        
        .FixedRows = 1
    End With
    
End Sub
