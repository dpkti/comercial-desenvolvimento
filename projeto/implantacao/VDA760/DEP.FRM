VERSION 4.00
Begin VB.Form Form1 
   BackColor       =   &H00C0C0C0&
   ClientHeight    =   3000
   ClientLeft      =   1890
   ClientTop       =   1395
   ClientWidth     =   6690
   Height          =   3405
   Left            =   1830
   LinkTopic       =   "Form1"
   ScaleHeight     =   3000
   ScaleWidth      =   6690
   Top             =   1050
   Width           =   6810
   Begin VB.Timer Timer2 
      Interval        =   15000
      Left            =   5160
      Top             =   2520
   End
   Begin VB.CommandButton Sair 
      Caption         =   "Sair"
      Height          =   405
      Left            =   1680
      TabIndex        =   3
      Top             =   2520
      Width           =   1395
   End
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   4440
      Top             =   2520
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Iniciar"
      Height          =   420
      Left            =   135
      TabIndex        =   2
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Label Label2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Iniciando Processo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   465
      Left            =   2400
      TabIndex        =   1
      Top             =   1680
      Width           =   2445
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      Index           =   3
      X1              =   105
      X2              =   6495
      Y1              =   75
      Y2              =   75
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      Index           =   2
      X1              =   105
      X2              =   6495
      Y1              =   195
      Y2              =   195
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      Index           =   1
      X1              =   105
      X2              =   6495
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      Index           =   0
      X1              =   120
      X2              =   6510
      Y1              =   1320
      Y2              =   1320
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "PROCESSA RETORNO DE NF DPASCHOAL"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   480
      TabIndex        =   0
      Top             =   480
      Width           =   5895
   End
End
Attribute VB_Name = "Form1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Sub DEP_DPA()

On Error GoTo trata_erros

Label2.Caption = "DPA..."
Form1.Refresh

Dim CABEC As String
CABEC = ""
'COPIA CORPO DO E-MAIL (PEDIDO)
'DO DIRET�RIO DE ORIGEM H:\COM\ENTRADA\PEDIDOS PARA O DIRETORIO DE DESTINO
'CONFORME O DEP�SITO H:\COM\ENTRADA\PEDIDOS\DPA
CABEC = Mid(strArquivo, 24)

FileCopy strArquivo, DIR_NFS_DPA & CABEC
Kill strArquivo


Exit Sub

trata_erros:

If Err = 70 Then
   Resume Next
Else
   Resume Next
End If



End Sub

Sub DEP_GO()

On Error GoTo trata_erros

Label2.Caption = "Goiania..."
Form1.Refresh

Dim CABEC As String

'COPIA CORPO DO E-MAIL (PEDIDO)
'DO DIRET�RIO DE ORIGEM H:\COM\ENTRADA\PEDIDOS PARA O DIRETORIO DE DESTINO
'CONFORME O DEP�SITO H:\COM\ENTRADA\PEDIDOS\CPS
CABEC = Mid(STRPEDIDO, 31)

FileCopy STRPEDIDO, DIR_PEDIDOS_GO & CABEC
Kill STRPEDIDO


Exit Sub

trata_erros:

If Err = 70 Then
   Resume Next
Else
   Resume Next
End If

End Sub

Private Sub Command1_Click()

On Error GoTo trata_erros

Label2.Caption = "Buscando Pedidos..."
Form1.Refresh

Dim STRCHAR As String
Dim STRLINHA As String
Dim TMP_LINHA As String

If Dir(DIR_NFS & "*.TXT") = "" Then ' N�O EXISTE NF
   Exit Sub
Else                                    ' EXISTE NF
    Do While Dir(DIR_NFS & "*.TXT") <> ""

        strArquivo = DIR_NFS & Dir(DIR_NFS & "*.TXT")
        
        strNome_Controle = Mid(Mid(strArquivo, 1, InStr(strArquivo, ".") - 1), 24, 8)
       
        Open strArquivo For Input As #1
        Line Input #1, TMP_LINHA
        strTipo_arquivo = Mid(TMP_LINHA, 1, 3)
    
        Close #1
    
        If strTipo_arquivo = "ENF" Then
           Call DEP_DPA
        End If
    Loop

End If

Exit Sub

trata_erros:
If Err = 70 Then
   Resume Next
Else
   Resume Next
End If

End Sub

Private Sub Form_Load()
'IDENTIFICA DIRET�RIO
'path_drv = Left(App.Path, 2)
path_drv = "h:"
'path_drv = "f:"

'DIRET�RIO DE DESTINO DPA
DIR_NFS_DPA = path_drv & "\COM\ENTRADA\PEDIDOS\DPA\"

'DIRET�RIO DE ORIGEM DOS nfs
DIR_NFS = path_drv & "\COM\ENTRADA\PEDIDOS\"

'Conexao oracle
Set orasession = CreateObject("oracleinprocserver.xorasession")
'TESTE
'Set oradatabase = orasession.OpenDatabase("desenv", "producao/des", 0&)
'producao
Set oradatabase = orasession.OpenDatabase("producao", "vda760/PROD", 0&)
    

End Sub


Private Sub Sair_Click()
'Close
End
End Sub

Private Sub Timer1_Timer()
  Call Command1_Click
End Sub


Private Sub Timer2_Timer()
 Call Atualiza_NFs
End Sub


