CREATE OR REPLACE Package PRODUCAO.PCK_VDA160 is

	--Declara cursor
	Type tp_Cursor is Ref Cursor;
									
	--Declara procedure pr_busca_clienteliebte_fim_pedido							
	PROCEDURE pr_busca_cliente(pm_cursor In Out tp_Cursor,
							 										          pm_nome In Varchar2);
                                 
 --Declara procedure pr_busca_fornecedor							
	PROCEDURE pr_busca_fornecedor(pm_cursor In Out tp_Cursor,
							 										             pm_nome In Varchar2);
                               
 --Declara procedure pr_busca_transportadora							
	PROCEDURE pr_busca_transportadora(pm_cursor In Out tp_Cursor,
                                   pm_transp In varchar2);

 --Declara procedure pr_carrega_cliente							
	PROCEDURE pr_carrega_cliente(pm_cursor In Out tp_Cursor,
                              pm_codigo_cliente In Number);
                              
 --Declara procedure pr_consulta_endereco_entrega							
	PROCEDURE pr_consulta_endereco_entrega(pm_cursor In Out tp_Cursor,
                                        pm_codigo_cliente In Number);
                                        
 --Declara procedure pr_verifica_cliente							
	PROCEDURE pr_verifica_cliente(pm_cursor In Out tp_Cursor);
                                        
 --Declara procedure pr_verifica_uf							
	PROCEDURE pr_verifica_uf(pm_cursor In Out tp_Cursor,
                          pm_codigo_cliente In Number);
                          
 --Declara procedure pr_busca_deposito							
	PROCEDURE pr_busca_deposito(pm_cursor In Out tp_Cursor,
                             pm_codigo_loja In Number);
                               
 --Declara procedure pr_carrega_nat_operacao							
	PROCEDURE pr_carrega_nat_operacao(pm_cursor In Out tp_Cursor,
                                   pm_nope In varchar2);
                                   
 --Declara procedure pr_verifica_nat_operacao							
	PROCEDURE pr_verifica_nat_operacao(pm_cursor In Out tp_Cursor,
                                    pm_codigo_loja In Number);
                                    
 --Declara procedure pr_busca_data							
	PROCEDURE pr_busca_data(pm_cursor In Out tp_Cursor);
 
 --Declara procedure pr_busca_data							
	PROCEDURE pr_carrega_deposito(pm_cursor In Out tp_Cursor);
 
 --Declara procedure pr_carrega_divisao							
	PROCEDURE pr_carrega_divisao(pm_cursor In Out tp_Cursor);
 
 --Declara procedure pr_carrega_natureza							
	PROCEDURE pr_carrega_natureza(pm_cursor In Out tp_Cursor);
 
 --Declara procedure pr_carrega_icm							
	PROCEDURE pr_carrega_icm(pm_cursor In Out tp_Cursor,
                          pm_destino In varchar2,
                          pm_origem In varchar2);
                          
 --Declara procedure pr_carrega_trasnportadora							
	PROCEDURE pr_carrega_trasnportadora(pm_cursor In Out tp_Cursor,
                                     pm_transp In Number);
                                      
 --Declara procedure pr_carrega_pedido							
	PROCEDURE pr_carrega_pedido(pm_cursor In Out tp_Cursor,
                             pm_ped In Number,
                             pm_seq In Number,
                             pm_loja In Number);
 
 --Declara procedure pr_carrega_fornecedor							
	PROCEDURE pr_carrega_fornecedor(pm_cursor In Out tp_Cursor,
                                 pm_codigo In Number);
                                 
 --Declara procedure pr_carrega_grupo							
	PROCEDURE pr_carrega_grupo(pm_cursor In Out tp_Cursor,
                            pm_codigo In Number);
                            
 --Declara procedure pr_carrega_montadora							
	PROCEDURE pr_carrega_montadora(pm_cursor In Out tp_Cursor);
 
 --Declara procedure pr_carrega_itens							
	PROCEDURE pr_carrega_itens(pm_cursor In Out tp_Cursor,
                            pm_forn In Number,
                            pm_montadora In Number,
                            pm_grupo In Number,
                            pm_subgrupo In Number);
                              
 --Declara procedure pr_carrega_subgrupo							
	PROCEDURE pr_carrega_subgrupo(pm_cursor In Out tp_Cursor,
                               pm_codigo In Number,
                               pm_grupo In Number);    
                              
 --Declara procedure pr_carrega_fabrica							
	PROCEDURE pr_carrega_fabrica(pm_cursor In Out tp_Cursor,
                              pm_forn In Number,
                              pm_fabrica In varchar2);
                              
 --Declara procedure pr_carrega_descricao_item							
	PROCEDURE pr_carrega_descricao_item(pm_cursor In Out tp_Cursor,
                              pm_forn In Number,
                              pm_descricao_item In varchar2);  
                              
 --Declara procedure pr_mostra_fornecedor							
	PROCEDURE pr_mostra_fornecedor(pm_cursor In Out tp_Cursor);
      
 --Declara procedure pr_carrega_tributacao_final							
	PROCEDURE pr_carrega_tributacao_final(pm_cursor In Out tp_Cursor,
                                       pm_origem In varchar2,
                                       pm_destino In varchar2,
                                       pm_class In Number);
  
 --Declara procedure pr_consulta_itens							
	PROCEDURE pr_consulta_itens(pm_cursor In Out tp_Cursor,
                             pm_loja In Number,
                             pm_forn In varchar2,
                             pm_dpk In Number,
                             pm_fabrica In varchar2);
  
 --Declara procedure pr_get_fornecedor							
	PROCEDURE pr_get_fornecedor(pm_cursor In Out tp_Cursor,
                             pm_forn In Number);
                                                   
 --Declara procedure pr_mostra_fornecedor							
	PROCEDURE pr_carrega_unidade(pm_cursor In Out tp_Cursor);                                                                                                							 				  					  

  --Declara procedure pr_carrega_tributacao_final							
	PROCEDURE pr_carrega_codigo_dpk(pm_cursor In Out tp_Cursor,
                                 pm_dpk In Number,
                                 pm_loja In Number,
                                 pm_flg In varchar2);
                                 
 --Declara procedure pr_busca_transportadora_cif							
	PROCEDURE pr_busca_transportadora_cif(pm_cursor In Out tp_Cursor,
                                       pm_uf In varchar2);
 --Declara procedure pr_busca_divisao                                      
	PROCEDURE pr_busca_divisao(pm_cursor In Out tp_Cursor,
                            pm_divisao In varchar2);
                            
 --Declara procedure pr_carrega_cabecalho							
	PROCEDURE pr_carrega_cabecalho(pm_cursor In Out tp_Cursor,
                                pm_codigo_cliente In Number,
                                pm_sequencia In Number,
                                pm_flg In varchar2);
                                
 --Declara procedure pr_carrega_nome_fantasia							
	PROCEDURE pr_carrega_nome_fantasia(pm_cursor In Out tp_Cursor,
                                    pm_loja In Number);
                                    
 --Declara procedure pr_busca_nat_operacao							
	PROCEDURE pr_busca_nat_operacao(pm_cursor In Out tp_Cursor,
                                 pm_nope In varchar2);
                                 
 --Declara procedure pr_carrega_faturamento							
	PROCEDURE pr_carrega_faturamento(pm_cursor In Out tp_Cursor);
                                   
End PCK_VDA160;
/
CREATE OR REPLACE PACKAGE BODY PRODUCAO.PCK_VDA160 IS
	
	--Cria procedure pr_busca_cliente
	PROCEDURE pr_busca_cliente(pm_cursor In Out tp_Cursor,
							 										          pm_nome In Varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
  					  cli.COD_CLIENTE COD_CLIENTE,
         cli.NOME_CLIENTE ,
        	cid.nome_cidade, 
         cid.cod_uf
       from 
         CLIENTE cli,
         CIDADE cid
       where 
         cli.COD_CIDADE = cid.COD_CIDADE 
       And
         cli.SITUACAO = 0
       and 
         NOME_CLIENTE 
       like 
         pm_nome
       order by 
         cli.NOME_CLIENTE;
         
   Exception
   
     When Others Then
       
       Rollback;
   
   End;

	--Cria procedure  pr_busca_fornecedor
	PROCEDURE pr_busca_fornecedor(pm_cursor In Out tp_Cursor,
							 										             pm_nome In Varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         forn.COD_FORNECEDOR,
         forn.NOME_FORNEC,
         cid.nome_cidade,
         cid.cod_uf
       from 
         FORNECEDOR forn,
         CIDADE cid
       where 
         forn.COD_CIDADE = cid.COD_CIDADE 
       And
         forn.SITUACAO = 0
       and 
         NOME_FORNEC 
       like 
         pm_nome
       order by 
         forn.NOME_FORNEC;      
  
   Exception
   
     When Others Then
       
       Rollback;
       
   End;   

 --Cria procedure  pr_busca_transportadora
	PROCEDURE pr_busca_transportadora(pm_cursor In Out tp_Cursor, 
                                   pm_transp In varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         COD_TRANSP,
         NOME_TRANSP,
         VIA_TRANSP
       from 
         TRANSPORTADORA
       where 
         SITUACAO = 0 
       and 
         nome_transp 
       like 
         pm_transp
       order by 
         NOME_TRANSP;      

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_cliente
	PROCEDURE pr_carrega_cliente(pm_cursor In Out tp_Cursor, 
                              pm_codigo_cliente In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         nvl(cli.NOME_CLIENTE,' ') nome_cliente,
         cli.cgc CGC,
         cid.NOME_CIDADE,
         cid.COD_UF,
         nvl(tran.COD_TRANSP,0) cod_transp,
         nvl(tran.NOME_TRANSP,' ') nome_transp,
         nvl(cli.INSCR_ESTADUAL,' ') inscr_estadual,
         nvl(cli.INSCR_SUFRAMA,' ') inscr_suframa,
         nvl(cli.fl_cons_final,' ') fl_cons_final,
         cli.situacao
       from 
         TRANSPORTADORA tran,
         CIDADE cid,
         CLIENTE cli
       where 
         tran.SITUACAO(+) = 0 
       And
         cid.COD_CIDADE = cli.COD_CIDADE 
       And
         tran.COD_TRANSP(+) = cli.COD_TRANSP 
       And
         cli.COD_CLIENTE = pm_codigo_cliente;       

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_consulta_endereco_entrega
	PROCEDURE pr_consulta_endereco_entrega(pm_cursor In Out tp_Cursor, 
                                        pm_codigo_cliente In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         SEQUENCIA,
         NOME_CLIENTE||' - '|| endereco endereco
       from 
         CLIE_ENDERECO 
       where 
         COD_CLIENTE = pm_codigo_cliente
       and 
         TP_ENDERECO = 1;       

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_verifica_cliente
	PROCEDURE pr_verifica_cliente(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         cod_cliente
       from 
         transferencia.loja_cliente;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
   
 --Cria procedure  pr_verifica_uf
	PROCEDURE pr_verifica_uf(pm_cursor In Out tp_Cursor, 
                          pm_codigo_cliente In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         B.COD_UF
       from 
         cliente a,
         cidade b
       Where
         A.COD_CLIENTE = pm_codigo_cliente 
       And
         A.CGC NOT IN (SELECT CGC FROM LOJA) 
       And
         A.SITUACAO=0 
       And
         A.CGC Like '45987005%' 
       And
         b.cod_uf in ('RJ','MG') 
       And
         a.cod_cidade = b.cod_cidade;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_busca_deposito
	PROCEDURE pr_busca_deposito(pm_cursor In Out tp_Cursor, 
                             pm_codigo_loja In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         b.cod_uf  cod_uf
       From 
         loja a,
         cidade b
       Where 
         a.cod_cidade = b.cod_cidade 
       And
         a.cod_loja = pm_codigo_loja;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
   
 --Cria procedure  pr_carrega_nat_operacao
	PROCEDURE pr_carrega_nat_operacao(pm_cursor In Out tp_Cursor, 
                                   pm_nope In varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select *
       From 
         natureza_operacao
       Where 
         cod_natureza = pm_nope 
       and 
         cod_natureza not in ('B06','A20');    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;   

 --Cria procedure  pr_verifica_nat_operacao
	PROCEDURE pr_verifica_nat_operacao(pm_cursor In Out tp_Cursor, 
                                    pm_codigo_loja In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         cod_cliente
       from 
         loja a,
         cliente b
       where  
         b.situacao = 0 
       And
         cod_loja = pm_codigo_loja 
       and 
         a.cgc = b.cgc;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_busca_data
	PROCEDURE pr_busca_data(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         to_char(dt_faturamento,'dd/mm/rr')||' '||to_char(sysdate,'hh24:mi:ss') dt_dig 
       from 
         datas;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
   
 --Cria procedure  pr_carrega_deposito
	PROCEDURE pr_carrega_deposito(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         to_char(a.cod_loja,'09') || '-' || b.nome_fantasia deposito
       from 
         deposito_visao a,
         loja b
       where 
         a.cod_loja = b.cod_loja 
       and 
         a.nome_programa = 'VDA160';    
   
   Exception
   
     When Others Then
       
       Rollback;
         
   End;
   
 --Cria procedure  pr_carrega_divisao
	PROCEDURE pr_carrega_divisao(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         divisao,
         descricao
       from 
         divisao
       Order by 
         tp_divisao; 

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
 
 --Cria procedure  pr_carrega_natureza
	PROCEDURE pr_carrega_natureza(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         rpad(cod_natureza||' - '||desc_natureza,36,' ') natureza,
         fl_estoque
       from 
         natureza_operacao
       Where 
         fl_livro = 'S' 
       and 
         fl_tipo_nota = 2 
       AND 
         COD_NATUREZA NOT IN ('B06','A20')
       order by 
         cod_natureza;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
 
 --Cria procedure  pr_carrega_icm
	PROCEDURE pr_carrega_icm(pm_cursor In Out tp_Cursor, 
                          pm_destino In varchar2,
                          pm_origem In varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         pc_icm
       From 
         uf_origem_destino
       Where 
         COD_UF_ORIGEM = pm_origem
       AND 
         COD_UF_DESTINO = pm_destino;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
   
 --Cria procedure  pr_carrega_trasnportadora
	PROCEDURE pr_carrega_trasnportadora(pm_cursor In Out tp_Cursor, 
                                     pm_transp In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         NOME_TRANSP,
         SITUACAO,
         cod_transp 
       from 
         TRANSPORTADORA
       where 
         COD_TRANSP = pm_transp;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;   

 --Cria procedure  pr_carrega_pedido
	PROCEDURE pr_carrega_pedido(pm_cursor In Out tp_Cursor, 
                             pm_ped In Number,
                             pm_seq In Number,
                             pm_loja In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         num_pedido,
         seq_pedido
       from 
         pednota_venda
       Where 
         num_pedido = pm_ped 
       and 
         seq_pedido = pm_seq 
       AND 
         COD_LOJA = pm_loja;    

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
 
 --Cria procedure  pr_carrega_fornecedor
	PROCEDURE pr_carrega_fornecedor(pm_cursor In Out tp_Cursor, 
                                 pm_codigo In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         nvl(forn.NOME_FORNEC,' ') nome_fornecedor,
         cid.NOME_CIDADE,
         cid.COD_UF,
         nvl(forn.INSCR_ESTADUAL,' ') inscr_estadual,
         forn.situacao,
         div.divisao
       from 
         CIDADE cid,
         FORNECEDOR forn,
         divisao div
       where 
         forn.divisao = div.divisao 
       And
         cid.COD_CIDADE = forn.COD_CIDADE 
       And
         forn.COD_FORNECEDOR = pm_codigo;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
 
  --Cria procedure  pr_carrega_grupo
	PROCEDURE pr_carrega_grupo(pm_cursor In Out tp_Cursor, 
                            pm_codigo In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select distinct 
         b.desc_grupo desc_grupo,
         b.cod_grupo cod_grupo
       from 
         grupo b,
         item_cadastro a
       where 
         b.cod_grupo=a.cod_grupo 
       And
         a.cod_fornecedor = pm_codigo
       order by 
         b.desc_grupo;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_montadora
	PROCEDURE pr_carrega_montadora(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         a.desc_montadora desc_montadora,
         a.cod_montadora cod_montadora
       from 
         montadora a
       order by 
         desc_montadora;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_itens
	PROCEDURE pr_carrega_itens(pm_cursor In Out tp_Cursor, 
                            pm_forn In Number,
                            pm_montadora In Number,
                            pm_grupo In Number,
                            pm_subgrupo In Number) is
   
   --Declarando vari�veis
   v_sql varchar2(4000) := Null;

   Begin
     
     --Select que retornara os dados para o cursor que retorna para a aplica��o
     --Atribuindo sql a variaval
     v_sql := 'select a.COD_FABRICA,a.DESC_ITEM,';
     v_sql := v_sql || ' c.DESC_APLICACAO';
     v_sql := v_sql || ' from item_cadastro a, item_estoque b, aplicacao c ';
     v_sql := v_sql || ' where c.COD_DPK(+) = a.cod_dpk And ';
     v_sql := v_sql || ' b.cod_loja+0 = 01 and ';
     v_sql := v_sql || ' b.cod_dpk = a.cod_dpk and ';
     v_sql := v_sql || ' b.SITUACAO = 0 and ';
     v_sql := v_sql || ' a.COD_FORNECEDOR = ' || pm_forn;
     If pm_montadora <> 0 Then
       v_sql := v_sql || ' and c.cod_montadora = ' || pm_montadora;
     End If;
     If pm_grupo <> 0 Then
       v_sql := v_sql || ' and a.cod_grupo = ' || pm_grupo;
     End If;
     If pm_subgrupo <> 0 Then
       v_sql := v_sql || ' and a.cod_subgrupo = ' || pm_subgrupo;
     End If;
     v_sql := v_sql || ' order by a.COD_FABRICA';
 
     --Abre cursor
     OPEN pm_cursor For v_sql;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_subgrupo
	PROCEDURE pr_carrega_subgrupo(pm_cursor In Out tp_Cursor, 
                               pm_codigo In Number,
                               pm_grupo In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select distinct 
         b.desc_subgrupo desc_subgrupo,
         b.cod_subgrupo cod_subgrupo
       from 
         subgrupo b,
         item_cadastro a
       where 
         b.cod_subgrupo = a.cod_subgrupo 
       And
         b.cod_grupo = a.cod_grupo 
       And
         a.cod_grupo = pm_grupo 
       And
         a.cod_fornecedor = pm_codigo
       order by 
         b.desc_subgrupo;       

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_fabrica
	PROCEDURE pr_carrega_fabrica(pm_cursor In Out tp_Cursor, 
                              pm_forn In Number,
                              pm_fabrica In Varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         a.COD_FABRICA,
         a.DESC_ITEM,
         c.DESC_APLICACAO
       from 
         item_cadastro a,
         item_estoque b,
         aplicacao c 
       where 
         c.COD_DPK(+) = a.cod_dpk 
       And
         b.cod_loja+0 = 01 
       and 
         b.cod_dpk = a.cod_dpk 
       and 
         b.SITUACAO <> 9 
       and 
         a.COD_FORNECEDOR = pm_forn 
       And
         a.COD_FABRICA like pm_fabrica
       order by 
         cod_fabrica;       

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_descricao_itema_fabrica
	PROCEDURE pr_carrega_descricao_item(pm_cursor In Out tp_Cursor, 
                                     pm_forn In Number,
                                     pm_descricao_item In Varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         a.COD_FABRICA,
         a.DESC_ITEM,
         c.DESC_APLICACAO
       from 
         item_cadastro a,
         item_estoque b,
         aplicacao c 
       where 
         c.COD_DPK(+) = a.cod_dpk 
       And
         b.cod_loja+0 = 01
       And
         b.cod_dpk = a.cod_dpk 
       And
         b.SITUACAO = 0 
       And
         a.COD_FORNECEDOR = pm_forn 
       And
         a.desc_item Like pm_descricao_item
       order by 
         cod_fabrica;      

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_mostra_fornecedor
	PROCEDURE pr_mostra_fornecedor(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         COD_FORNECEDOR,
         SIGLA 
       from 
         FORNECEDOR 
       where 
         SITUACAO = 0 
       And
         DIVISAO = 'D' 
       and 
         CLASSIFICACAO = 'A'
       order by 
         SIGLA;     

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_tributacao_final
	PROCEDURE pr_carrega_tributacao_final(pm_cursor In Out tp_Cursor, 
                                       pm_origem In varchar2,
                                       pm_destino In varchar2,
                                       pm_class In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       Select 
         cod_trib_revendedor, 
         cod_trib_inscrito,
         cod_trib_isento
       From 
         compras.subst_tributaria
       Where 
         class_fiscal = pm_class 
       And
         cod_uf_origem = pm_origem 
       And
         cod_uf_destino = pm_destino;   

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_consulta_itens
	PROCEDURE pr_consulta_itens(pm_cursor In Out tp_Cursor, 
                             pm_loja In Number,
                             pm_forn In varchar2,
                             pm_dpk In Number,
                             pm_fabrica In varchar2) is
   
   --Declarando vari�veis
   v_sql varchar2(4000) := Null;

   Begin
     
     --Select que retornara os dados para o cursor que retorna para a aplica��o
     --Atribuindo sql a variaval
     v_sql := 'select e.COD_DPK,e.COD_FORNECEDOR,nvl(f.SIGLA,'' '') sigla,';
     v_sql := v_sql || 'nvl(e.COD_FABRICA,0) cod_fabrica,nvl(e.DESC_ITEM,'' '') desc_item,';
     v_sql := v_sql || 'nvl(e.cod_grupo,0) cod_grupo,nvl(e.cod_subgrupo,0) cod_subgrupo,';
     v_sql := v_sql || 'nvl(a.SINAL,'' '') sinal,nvl(e.class_fiscal,0) class_fiscal,';
     v_sql := v_sql || 'nvl(e.QTD_MINVDA,1) qtd_minvda,nvl(b.QTD_MAXVDA,1) qtd_maxvda,';
     v_sql := v_sql || 'e.COD_TRIBUTACAO cod_tributacao,e.COD_TRIBUTACAO_IPI,';
     v_sql := v_sql || 'e.PC_IPI,b.SITUACAO,e.cod_unidade,';
     v_sql := v_sql || 'nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,f.DIVISAO ';
     v_sql := v_sql || ' from categ_sinal a,item_estoque b,item_analitico c,';
     v_sql := v_sql || 'item_preco d,item_cadastro e,fornecedor f ';
     v_sql := v_sql || ' where c.categoria = a.categoria and ';
     v_sql := v_sql || ' b.cod_loja = c.cod_loja and b.cod_dpk = c.cod_dpk and ';
     v_sql := v_sql || ' c.cod_loja = d.cod_loja and c.cod_dpk = d.cod_dpk and ';
     v_sql := v_sql || ' d.cod_loja = ' || pm_loja || ' and d.cod_dpk = e.cod_dpk and ';
     v_sql := v_sql || ' e.cod_fornecedor = f.cod_fornecedor ';
     If pm_dpk > 0 Then
       v_sql := v_sql || ' and e.COD_DPK = ' || pm_dpk;
     Elsif pm_forn <> '-1' And pm_fabrica <> '-1' Then
       v_sql := v_sql || ' and e.COD_FORNECEDOR = ' || to_number(pm_forn);
       v_sql := v_sql || ' and e.COD_FABRICA = ' || pm_fabrica;
     End if;

     --Abre cursor
     OPEN pm_cursor For v_sql;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
     
 --Cria procedure pr_get_fornecedor
	PROCEDURE pr_get_fornecedor(pm_cursor In Out tp_Cursor,
							 										           pm_forn In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         nvl(SIGLA,' ') sigla,
         SITUACAO 
       from 
         FORNECEDOR 
       where 
         SITUACAO = 0 
       and 
         COD_FORNECEDOR = pm_forn;
         
   Exception
   
     When Others Then
       
       Rollback;
   
   End;
  
 --Cria procedure  pr_carrega_unidade
	PROCEDURE pr_carrega_unidade(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         cod_unidade
       from 
         unidade
       order by 
         cod_unidade;     

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_codigo_dpk
	PROCEDURE pr_carrega_codigo_dpk(pm_cursor In Out tp_Cursor, 
                                 pm_dpk In Number,
                                 pm_loja In Number,
                                 pm_flg In varchar2) is
   --Declarando vari�veis
   v_sql varchar2(4000) := Null;
   
   Begin

     --Select que retornara os dados para o cursor que retorna para a aplica��o
     If pm_flg = '1' Then          
     
       v_sql := 'Select custo_medio From item_analitico ';
       v_sql := v_sql || ' Where cod_dpk = ' || pm_dpk; 
       v_sql := v_sql || ' And  cod_loja = ' || pm_loja;  
     
     Elsif pm_flg = '2' Then  
     
       v_sql := 'Select CASE WHEN B.COD_TRIBUTACAO_IPI = 1 Then a.vl_ult_compra Else';
       v_sql := v_sql || ' round(a.vl_ult_compra +(a.vl_ult_compra * (b.pc_ipi/100)),2) '; 
       v_sql := v_sql || ' END  preco_transf From item_estoque a,item_cadastro b';
       v_sql := v_sql || ' Where a.cod_dpk = ' || pm_dpk; 
       v_sql := v_sql || ' And a.cod_loja = ' || pm_loja;
       v_sql := v_sql || ' And a.cod_dpk = b.cod_dpk'; 
             
     End If; 
          
     --Abre cursor
     OPEN pm_cursor For v_sql;
    
   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_busca_transportadora_cif
	PROCEDURE pr_busca_transportadora_cif(pm_cursor In Out tp_Cursor, 
                                       pm_uf In varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         transp.COD_TRANSP,
         transp.NOME_TRANSP,
         transp.VIA_TRANSP,
         decode(frete.FL_BLOQUEIO,'S',' SIM ','     ') FL_LIBERADO
       from 
         TRANSPORTADORA transp,
         FRETE_UF_BLOQ frete
       where transp.SITUACAO = 0 And
         frete.COD_TRANSP = transp.COD_TRANSP 
       and 
         frete.COD_UF = pm_uf
       order by 
         transp.NOME_TRANSP;

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure pr_busca_divisao
	PROCEDURE pr_busca_divisao(pm_cursor In Out tp_Cursor,
							 										          pm_divisao In Varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         descricao 
       from 
         divisao 
       where 
         divisao = pm_divisao;
         
   Exception
   
     When Others Then
       
       Rollback;
   
   End;

 --Cria procedure  pr_carrega_cabecalho
	PROCEDURE pr_carrega_cabecalho(pm_cursor In Out tp_Cursor, 
                                pm_codigo_cliente In Number,
                                pm_sequencia In Number,
                                pm_flg In varchar2) is
   --Declarando vari�veis
   v_sql varchar2(4000) := Null;
   
   Begin

     --Select que retornara os dados para o cursor que retorna para a aplica��o
     If pm_flg = '1' Then          
     
       v_sql := 'Select a.nome_fornec nome,b.NOME_CIDADE From ';
       v_sql := v_sql || ' fornecedor a,CIDADE b where '; 
       v_sql := v_sql || ' a.cod_fornecedor = ' || pm_codigo_cliente; 
       v_sql := v_sql || ' and a.COD_CIDADE = b.COD_CIDADE';
         
     
     Elsif pm_flg = '2' Then  
     
       v_sql := 'Select cli.NOME_CLIENTE nome,cid.NOME_CIDADE,';
       v_sql := v_sql || ' cli.FL_CONS_FINAL,nvl(cli.INSCR_SUFRAMA,'' '') inscr_suframa,'; 
       v_sql := v_sql || ' nvl(cli.INSCR_ESTADUAL,'' '') inscr_estadual,';
       v_sql := v_sql || ' nvl(ende.nome_cliente,' || chr(39) || ' ' || chr(39) || ') || ' || chr(39) || '-' || chr(39) || ' || nvl(ende.ENDERECO,'' '') endereco '; 
       v_sql := v_sql || ' from CLIENTE cli,CIDADE cid,clie_endereco ende ';
       v_sql := v_sql || ' where cli.COD_CLIENTE = ' || pm_codigo_cliente;
       v_sql := v_sql || ' and ende.TP_ENDERECO(+) = 1 ';
       v_sql := v_sql || ' and ende.sequencia(+) = ' || pm_sequencia;
       v_sql := v_sql || ' and cli.cod_cliente = ende.cod_cliente(+)'; 
       v_sql := v_sql || ' and cid.COD_CIDADE = cli.COD_CIDADE'; 
             
     End If; 

     --Abre cursor
     OPEN pm_cursor For v_sql;
    
   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_nome_fantasia
	PROCEDURE pr_carrega_nome_fantasia(pm_cursor In Out tp_Cursor, 
                                    pm_loja In Number) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         nome_fantasia,
         cod_uf
       from 
         loja a,
         cidade b
       where 
         a.cod_cidade = b.cod_cidade
       And 
         cod_loja = pm_loja;      

   Exception
   
     When Others Then
       
       Rollback;
         
   End;   

 --Cria procedure  pr_busca_nat_operacao
	PROCEDURE pr_busca_nat_operacao(pm_cursor In Out tp_Cursor, 
                                 pm_nope In varchar2) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select *
       from 
         natureza_operacao
       where 
         cod_natureza = pm_nope;      

   Exception
   
     When Others Then
       
       Rollback;
         
   End;

 --Cria procedure  pr_carrega_faturamento
	PROCEDURE pr_carrega_faturamento(pm_cursor In Out tp_Cursor) is
   
   Begin
   
     --Abre cursor
     OPEN pm_cursor For
    
       --Select que retornara os dados para o cursor que retorna para a aplica��o
       select 
         a.dt_faturamento,
         a.dt_real,
         to_char(b.cod_filial,'0009') || '-' || c.sigla filial,
         d.cod_uf cod_uf,
         to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default
       from 
         datas a,
         producao.deposito b,
         filial c,
         cidade d,
         loja e
       Where
         b.cod_filial = c.cod_filial
       And 
         b.cod_loja = e.cod_loja
       And 
         e.cod_cidade = d.cod_cidade;     

   Exception
   
     When Others Then
       
       Rollback;
         
   End;
                                                          
End PCK_VDA160;
/
