Attribute VB_Name = "ModCD"
Option Explicit

Public lngCD As Integer
Public strTp_banco As String * 1
Public strPath As String
Public lngArq As Long
Public TextLine As String

Public lngCOD_PLANO

Public Sub Get_CD(strUnidade As String)
    On Error GoTo ERRO

    strPath = "h:\oracle\dados\32bits\cd.txt"

    lngArq = FreeFile

    Open strPath For Input Shared As #lngArq
    Do While Not EOF(lngArq)
        Line Input #lngArq, TextLine
        lngCD = Mid(TextLine, 1, 2)
        strTp_banco = Mid(TextLine, 3, 1)
    Loop
    Close #lngArq

    Exit Sub

ERRO:

    If Err = 53 Then
        MsgBox "O Arquivo de controle de tipo de Banco n�o foi encontrado !!!", 16, "Controle de Banco"
        End
    Else
        MsgBox Err.Description, 16, "Controle de Banco"
    End If

End Sub
