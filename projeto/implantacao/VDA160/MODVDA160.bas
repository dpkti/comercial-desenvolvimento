Attribute VB_Name = "MODVDA160"
Option Explicit

Public lngCD As Integer
Public strTp_banco As String * 1
Public strPath As String
Public lngArq As Long
Public TextLine As String

Public lngCOD_PLANO

Public vCgc As String
Public vVB_Generica_001 As New clsVB_Generica_001
Public vErro As String

Public vConsultado As Boolean
'CBacci - 19/10/2012 - SDS2671 - Parametro que identifica se usar Armaz�m Geral
Public vUsaAG As String
'CBacci - 19/10/2012 - SDS2671 - Natureza que deve ser gravado na mensagem o numero da nota
' de entrada e a data de emiss�o
Private vNatsRemessaAg As String
'CBacci - 21/10/2012 - SDS2671 - Mostra se a entrada foi carrega
Public fl_nf_entrada_carregada As Boolean
Public ssAG As Object

Public Sub Criar_Cursor()

    oradatabase.Parameters.Remove "PM_Cursor"
    oradatabase.Parameters.Add "PM_Cursor", 0, 2
    oradatabase.Parameters("PM_Cursor").ServerType = 102
    oradatabase.Parameters("PM_Cursor").DynasetOption = &H2&
    oradatabase.Parameters("PM_Cursor").DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

Public Function CarregaCCG(ByVal pCodigo As Long, ByVal pTipo As String) As String
      Dim vCGCFun As String
      Dim ss As Object
      Dim vSql As String
      Dim vTipo As Integer

1     On Error GoTo trata_erro

2         If pTipo = "Cliente" Then
3             vTipo = 0
4         ElseIf pTipo = "Fornecedor" Then
5             vTipo = 1
6         End If
          
          'passa parametros para a procedure
7             oradatabase.Parameters.Remove "pm_cod"
8             oradatabase.Parameters.Add "pm_cod", pCodigo, 1
9             oradatabase.Parameters.Remove "pm_tipo"
10            oradatabase.Parameters.Add "pm_tipo", vTipo, 1

          'padr�o pra criar cursor
11            Criar_Cursor

12            vSql = strTabela_Banco & "PCK_VDA160.pr_cliente_fornecedor(:Pm_Cursor, :pm_cod, :pm_tipo)"

13            vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)

14            Set ss = oradatabase.Parameters("Pm_Cursor").Value

15            If Not ss.BOF And Not ss.EOF Then
16                vCGCFun = ss!CGC
17            End If
              
18            CarregaCCG = vCGCFun
              
trata_erro:
19        If Err.Number <> 0 Then
20            MsgBox "Sub: CarregaCCG" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
21        End If
End Function

Public Function RetornaSelect(ByVal pOwner As String, ByVal pLink As String, ByVal pCOD_DPK As Long, ByVal pCGC As String, ByVal pCDLoja As Integer, ByRef pCount As Integer, ByVal pNumNota As Long) As String
          Dim vSql As String
          Dim ss As Object
          Dim itmx As Object, i As Integer
          Dim ii As Integer

1         On Error GoTo trata_erro

2         If pLink = "" Then
3             FRMITENS.lblmsg = "Aguarde... Consultando banco de dados Local"
4             FRMITENS.lblmsg.Visible = True
5         ElseIf pLink = "@lnk_batch" Then
6             FRMITENS.lblmsg = "Aguarde... Consultando banco de dados Historico"
7             FRMITENS.lblmsg.Visible = True
8         End If

          'passa parametros para a procedure
9         oradatabase.Parameters.Remove "pm_codloja"
10        oradatabase.Parameters.Add "pm_codloja", pCDLoja, 1
11        oradatabase.Parameters.Remove "pm_cgc"
12        oradatabase.Parameters.Add "pm_cgc", pCGC, 1
13        oradatabase.Parameters.Remove "pm_coddpk"
14        oradatabase.Parameters.Add "pm_coddpk", pCOD_DPK, 1
15        oradatabase.Parameters.Remove "pm_owner"
16        oradatabase.Parameters.Add "pm_owner", pOwner, 1
17        oradatabase.Parameters.Remove "pm_link"
18        oradatabase.Parameters.Add "pm_link", pLink, 1
19        oradatabase.Parameters.Remove "pm_num_nf"
20        oradatabase.Parameters.Add "pm_num_nf", pNumNota, 1
21        oradatabase.Parameters.Remove "pm_tipo"
22        oradatabase.Parameters.Add "pm_tipo", vTipoCliForn, 1
          'padr�o pra criar cursor
23        Criar_Cursor

24        vSql = strTabela_Banco & "PCK_VDA160.pr_nota_compra(:Pm_Cursor, :pm_codloja, :pm_cgc, :pm_coddpk, :pm_owner, :pm_link, :pm_num_nf, :pm_tipo)"

25        vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)

26        Set ss = oradatabase.Parameters("Pm_Cursor").Value

27        i = 0
          'Carregando listview

28        If ss.RecordCount = 1 Then
29            i = 1
30            FRMITENS.vNumNf = ss!num_nf
31            FRMITENS.vCodTribICMS = ss!cod_trib
32            FRMITENS.vCodTribIPI = ss!COD_TRIBipi
33            FRMITENS.vQtde = ss!Qtd_Atendida
34            FRMITENS.vPreco = ss!PRECO
35            FRMITENS.vPercIPI = ss!ipi

36            FRMITENS.lblCGC = ss!CGC
37            FRMITENS.lblCodTrib = ss!cod_trib
38            FRMITENS.lblCodTribIPI = ss!COD_TRIBipi
39            FRMITENS.lblDifer = ss!DIFER
40            FRMITENS.lblPcAliqInterna = ss!PC_ALIQ_INTERNA
41            FRMITENS.lblPCICM = ss!PC_ICM
42            FRMITENS.lblPcMargemLucro = ss!PC_MARGEM_LUCRO
43            FRMITENS.lblSerie = ss!SERIE
44            FRMITENS.lblSituacao = ss!Situacao
45            FRMITENS.lblVlIcmRetido = ss!VL_ICM_RETIDO
46            FRMITENS.lblVlLiquido = ss!PRECO
47            FRMITENS.lblPCIPI = ss!pc_ipi


48        ElseIf ss.RecordCount > 0 Then
49            FRMITENS.lsvNotas.ListItems.Clear
50            For i = 1 To ss.RecordCount
51                Set itmx = FRMITENS.lsvNotas.ListItems.Add()

52                itmx.Text = ss!cod_loja
53                itmx.SubItems(1) = ss!num_nf
54                itmx.SubItems(2) = ss!CGC
55                itmx.SubItems(3) = ss!NOME_FORNEC
56                itmx.SubItems(4) = ss!SERIE
57                itmx.SubItems(5) = ss!DIFER
58                itmx.SubItems(6) = ss!Situacao
59                itmx.SubItems(7) = ss!cod_trib
60                itmx.SubItems(8) = ss!COD_TRIBipi
61                itmx.SubItems(9) = ss!pc_ipi
62                itmx.SubItems(10) = ss!PRECO
63                itmx.SubItems(11) = ss!PC_ICM
64                itmx.SubItems(12) = ss!VL_ICM_RETIDO
65                itmx.SubItems(13) = ss!Qtd_Atendida
66                itmx.SubItems(14) = ss!PC_ALIQ_INTERNA
67                itmx.SubItems(15) = ss!PC_MARGEM_LUCRO
68                itmx.SubItems(16) = ss!cod_fornecedor

69                ss.MoveNext
70            Next

71            For ii = 1 To FRMITENS.lsvNotas.ListItems.count

72                If FRMITENS.lsvNotas.ListItems(ii).SubItems(16) = FRMITENS.txtCOD_FORNECEDOR Then
73                    FRMITENS.vNumNf = FRMITENS.lsvNotas.ListItems(ii).SubItems(1)
74                    FRMITENS.vCodTribIPI = FRMITENS.lsvNotas.ListItems(ii).SubItems(8)
75                    FRMITENS.vQtde = FRMITENS.lsvNotas.ListItems(ii).SubItems(13)
76                    FRMITENS.vPreco = FRMITENS.lsvNotas.ListItems(ii).SubItems(10)
77                    FRMITENS.vPercIPI = FRMITENS.lsvNotas.ListItems(ii).SubItems(9)
78                    FRMITENS.vCodTribICMS = FRMITENS.lsvNotas.ListItems(ii).SubItems(7)


79                    FRMITENS.lblCGC = FRMITENS.lsvNotas.ListItems(i).SubItems(2)
80                    FRMITENS.lblCodTrib = FRMITENS.lsvNotas.ListItems(i).SubItems(7)
81                    FRMITENS.lblCodTribIPI = FRMITENS.lsvNotas.ListItems(i).SubItems(8)
82                    FRMITENS.lblDifer = FRMITENS.lsvNotas.ListItems(i).SubItems(5)
83                    FRMITENS.lblPcAliqInterna = FRMITENS.lsvNotas.ListItems(i).SubItems(13)
84                    FRMITENS.lblPCICM = FRMITENS.lsvNotas.ListItems(i).SubItems(11)
85                    FRMITENS.lblPcMargemLucro = FRMITENS.lsvNotas.ListItems(i).SubItems(14)
86                    FRMITENS.lblSerie = FRMITENS.lsvNotas.ListItems(i).SubItems(4)
87                    FRMITENS.lblSituacao = FRMITENS.lsvNotas.ListItems(i).SubItems(6)
88                    FRMITENS.lblVlIcmRetido = FRMITENS.lsvNotas.ListItems(i).SubItems(12)
89                    FRMITENS.lblVlLiquido = FRMITENS.lsvNotas.ListItems(i).SubItems(10)
90                    FRMITENS.lblPCIPI = FRMITENS.lsvNotas.ListItems(i).SubItems(9)

91                    Exit For
92                End If

93            Next

94        End If

95        pCount = i
96        ss.MoveFirst
97        If ss.EOF Then
98            RetornaSelect = "0"
99        Else
100           RetornaSelect = "1"
101       End If

102       FRMITENS.lblmsg = ""
103       FRMITENS.lblmsg.Visible = False

trata_erro:
104       If Err.Number <> 0 Then
105           MsgBox "Sub: RetornaSelect" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
106       End If
End Function

Function Parametros(ByVal pValor As String) As Boolean
      Dim vSql As String
      Dim ss As Object
      
1         Parametros = False
          'CBacci - 18/10/2012 - SDS2671 - N�o passava o nome do parametro, gerando problema
          ' para novos parametros criados que tem uma natureza de opera��o em seu valor
'2         vSql = "Select * FROM " & IIf(strTp_banco = "U", "HELPDESK.", strTabela_Banco) & "PARAMETROS WHERE COD_SOFTWARE = 1064 AND VL_PARAMETRO = '" & pValor & "'"
2          vSql = "Select * FROM " & IIf(strTp_banco = "U", "HELPDESK.", strTabela_Banco) & _
               "PARAMETROS WHERE COD_SOFTWARE = 1064 AND NOME_PARAMETRO = 'NATUREZA_OPERACAO' " & _
               "AND VL_PARAMETRO = '" & pValor & "'"

3         Set ss = oradatabase.DbCreateDynaset(vSql, 0&)
             
4         If Not ss.BOF And Not ss.EOF Then
5             Parametros = True
6         End If
          
End Function
Public Sub Get_CD(strUnidade As String)
    On Error GoTo Erro

    strPath = "h:\oracle\dados\32bits\cd.txt"

    lngArq = FreeFile

    Open strPath For Input Shared As #lngArq
    Do While Not EOF(lngArq)
        Line Input #lngArq, TextLine
        lngCD = Mid(TextLine, 1, 2)
        strTp_banco = Mid(TextLine, 3, 1)
    Loop
    Close #lngArq

    Exit Sub

Erro:

    If Err = 53 Then
        MsgBox "O Arquivo de controle de tipo de Banco n�o foi encontrado !!!", 16, "Controle de Banco"
        End
    Else
        MsgBox Err.Description, 16, "Controle de Banco"
    End If

End Sub

' Ci&T - Fabio Marques - 31/03/2012
' Criada fun��o para retornar se item � importado ou n�o
Function ItemImportado(ByVal pCOD_DPK As Long) As Boolean

    Dim vSql As String
    Dim ss As Object
      
    vSql = "SELECT cod_dpk FROM producao.item_cadastro ic " & _
           "WHERE ic.cod_procedencia = 2 AND cod_dpk = " & pCOD_DPK
    
    Set ss = oradatabase.DbCreateDynaset(vSql, 0&)
        
    If Not ss.BOF And Not ss.EOF Then
        ItemImportado = True
    End If
    
End Function

'fcassiani - 15/10/12 - SDS2671 - Retorna notas de compra
Public Function RetornaNfEntrada(ByVal pCDLoja As Integer, ByVal pNumNota As Long, ByVal pCNPJ As String) As Object
    Dim vSql As String
    Dim vRegistros As Object
    On Error GoTo trata_erro
         
    'passa parametros para a procedure
    oradatabase.Parameters.Remove "pCOD_LOJA"
    oradatabase.Parameters.Add "pCOD_LOJA", pCDLoja, 1
    oradatabase.Parameters.Remove "pNF_ENTRADA"
    oradatabase.Parameters.Add "pNF_ENTRADA", CLng(pNumNota), 1
    oradatabase.Parameters.Remove "pCNPJ"
    oradatabase.Parameters.Add "pCNPJ", pCNPJ, 1
       
    'padr�o pra criar cursor
    Criar_Cursor
        
    vSql = strTabela_Banco & "PCK_VDA160.PR_BUSCA_NF_ENTRADA(:PM_Cursor, :pNF_ENTRADA, :pCOD_LOJA, :pCNPJ)"
    vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)

    Set vRegistros = oradatabase.Parameters("Pm_Cursor").Value
    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If

    Set RetornaNfEntrada = vRegistros
        
    Exit Function
trata_erro:
    Set vRegistros = Nothing
    If Err.Number <> 0 Then
        MsgBox "Sub: RetornaNfEntrada" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Function

'fcassiani - 16/10/12 - SDS2671 - Grava pedido de saida de remessa para AG
Public Function GravaArmazemGeral(ByVal pm_cod_loja As String, _
                             ByVal pm_num_pedido As String, _
                             ByVal pm_seq_pedido As String, _
                             ByVal pm_num_item As String, _
                             ByVal pm_num_nota As String, _
                             ByVal PM_CGC As String, _
                             ByVal PM_SERIE As String, _
                             ByVal PM_DIFER As String, _
                             ByVal PM_SITUACAO As String, _
                             ByVal PM_CODTRIB As String, _
                             ByVal PM_CODTRIBIPI As String, _
                             ByVal PM_VL_LIQUIDO As String, _
                             ByVal PM_VL_ICMRETIDO As String, _
                             ByVal PM_PC_ICM As String, _
                             ByVal PM_PC_ALIQ_INTERNA As String, _
                             ByVal PM_PC_MARGEM_LUCRO As String, _
                             ByVal PM_PC_IPI As String) As Boolean
                             
    On Error GoTo TrataErro
    Dim vSql As String, vMSG As String, vErro As String
    
    oradatabase.Parameters.Remove "pm_acao"
    oradatabase.Parameters.Add "pm_acao", 1, 1
    oradatabase.Parameters.Remove "pm_cod_loja"
    oradatabase.Parameters.Add "pm_cod_loja", pm_cod_loja, 1
    oradatabase.Parameters.Remove "pm_num_pedido"
    oradatabase.Parameters.Add "pm_num_pedido", pm_num_pedido, 1
    oradatabase.Parameters.Remove "pm_seq_pedido"
    oradatabase.Parameters.Add "pm_seq_pedido", pm_seq_pedido, 1
    oradatabase.Parameters.Remove "pm_num_item"
    oradatabase.Parameters.Add "pm_num_item", Val(pm_num_item), 1
    oradatabase.Parameters.Remove "pm_num_nota"
    oradatabase.Parameters.Add "pm_num_nota", Val(pm_num_nota), 1
    oradatabase.Parameters.Remove "PM_CGC"
    oradatabase.Parameters.Add "PM_CGC", Val(PM_CGC), 1
    oradatabase.Parameters.Remove "PM_SERIE"
    oradatabase.Parameters.Add "PM_SERIE", PM_SERIE, 1
    oradatabase.Parameters.Remove "PM_DIFER"
    oradatabase.Parameters.Add "PM_DIFER", Val(PM_DIFER), 1
    oradatabase.Parameters.Remove "PM_SITUACAO"
    oradatabase.Parameters.Add "PM_SITUACAO", Val(PM_SITUACAO), 1
    oradatabase.Parameters.Remove "PM_CODTRIB"
    oradatabase.Parameters.Add "PM_CODTRIB", PM_CODTRIB, 1
    oradatabase.Parameters.Remove "PM_CODTRIBIPI"
    oradatabase.Parameters.Add "PM_CODTRIBIPI", PM_CODTRIBIPI, 1
    oradatabase.Parameters.Remove "PM_VL_LIQUIDO"
    oradatabase.Parameters.Add "PM_VL_LIQUIDO", PM_VL_LIQUIDO, 1
    oradatabase.Parameters.Remove "PM_VL_ICMRETIDO"
    oradatabase.Parameters.Add "PM_VL_ICMRETIDO", PM_VL_ICMRETIDO, 1
    oradatabase.Parameters.Remove "PM_PC_ICM"
    oradatabase.Parameters.Add "PM_PC_ICM", PM_PC_ICM, 1
    oradatabase.Parameters.Remove "PM_PC_ALIQ_INTERNA"
    oradatabase.Parameters.Add "PM_PC_ALIQ_INTERNA", PM_PC_ALIQ_INTERNA, 1
    oradatabase.Parameters.Remove "PM_PC_MARGEM_LUCRO"
    oradatabase.Parameters.Add "PM_PC_MARGEM_LUCRO", PM_PC_MARGEM_LUCRO, 1
    oradatabase.Parameters.Remove "PM_PC_IPI"
    oradatabase.Parameters.Add "PM_PC_IPI", PM_PC_IPI, 1
    oradatabase.Parameters.Remove "pm_msg"
    oradatabase.Parameters.Add "pm_msg", "", 2

    vSql = strTabela_Banco & "PCK_VDA160.pr_pedido_referencia(:PM_ACAO, " & _
               ":pm_cod_loja, " & _
               ":pm_num_pedido, " & _
               ":pm_seq_pedido, " & _
               ":pm_num_item, " & _
               ":pm_num_nota, " & _
               ":pm_cgc, " & _
               ":pm_serie, " & _
               ":pm_difer, " & _
               ":pm_situacao, " & _
               ":pm_codtrib, " & _
               ":pm_codtribipi, " & _
               ":pm_vl_liquido, " & _
               ":pm_vl_icmretido, " & _
               ":pm_pc_icm, " & _
               ":pm_pc_aliq_interna, " & _
               ":pm_pc_margem_lucro, " & _
               ":pm_pc_IPI, " & _
               ":pm_msg)"

    vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)
    vMSG = oradatabase.Parameters("pm_msg").Value
    GravaArmazemGeral = True
    
    Exit Function
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: GravaArmazemGeral" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
    GravaArmazemGeral = False
End Function
'fcassiani - 16/10/12 - SDS2671 - Carrega nota fiscal de compra
Public Function PreencheSsAg(ByVal pCOD_LOJA As Integer, ByVal pNF_ENTRADA As Long, _
                      ByVal pCGC As String, ByVal pSERIE As String, _
                      ByVal pDIFER As Integer) As Object
        
    Dim vSql As String
    Dim vRegistros As Object
    On Error GoTo trata_erro
         
    'passa parametros para a procedure
    oradatabase.Parameters.Remove "pCOD_LOJA"
    oradatabase.Parameters.Add "pCOD_LOJA", pCOD_LOJA, 1
    oradatabase.Parameters.Remove "pNF_ENTRADA"
    oradatabase.Parameters.Add "pNF_ENTRADA", pNF_ENTRADA, 1
    oradatabase.Parameters.Remove "pCGC"
    oradatabase.Parameters.Add "pCGC", pCGC, 1
    oradatabase.Parameters.Remove "pSERIE"
    oradatabase.Parameters.Add "pSERIE", pSERIE, 1
    oradatabase.Parameters.Remove "pDIFER"
    oradatabase.Parameters.Add "pDIFER", pDIFER, 1
          
    'padr�o pra criar cursor
    Criar_Cursor

    vSql = strTabela_Banco & "PCK_VDA160.PR_BUSCA_GRAVA_AG(:PM_CURSOR,:pCOD_LOJA,:pNF_ENTRADA,:pCGC,:pSERIE,:pDIFER)"

    vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)

    Set vRegistros = oradatabase.Parameters("Pm_Cursor").Value
        
    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If

    Set PreencheSsAg = vRegistros
    
    Exit Function
trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub: PreencheSsAg" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Function

'fcassiani - 16/10/12 - SDS2671 - Retorno o valor do parametro PAR_USA_AG
Public Function usaAg() As String
    
    Dim vSql As String
    Dim vRegistros As Object

    On Error GoTo trata_erro
     
    'passa parametros para a procedure
    oradatabase.Parameters.Remove "PM_COD_SOFTWARE"
    oradatabase.Parameters.Add "PM_COD_SOFTWARE", "1064", 1
    oradatabase.Parameters.Remove "PM_NOME_PARAMETRO"
    oradatabase.Parameters.Add "PM_NOME_PARAMETRO", "PAR_USA_AG", 1
    
    'padr�o pra criar cursor
    Criar_Cursor

    vSql = strTabela_Banco & "PCK_VDA160.PR_VERIFICA_USA_AG(:PM_CURSOR,:PM_COD_SOFTWARE,:PM_NOME_PARAMETRO)"

    vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSql)

    Set vRegistros = oradatabase.Parameters("Pm_Cursor").Value
    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If
    
    usaAg = "N"
    If Not vRegistros Is Nothing Then
        usaAg = vRegistros!VL_PARAMETRO
    End If

    Exit Function
trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub: RetornaNfEntrada" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Function
'CBacci - 18/10/2012 - SDS2671 - Fun��o que busca o valor do parametro, vCodSoftware
' quando n�o for passado busca o valor do parametro do pr�prio sistema
Public Function BuscaParametros(ByRef vCodSoftware As Integer, ByRef vNomeParametro As String) As Object
    On Error GoTo TrataErro
    Dim vRegistros As Object
    Dim vSqlInt As String
          
    If vNomeParametro <> "" Then
        If vCodSoftware = 0 Then
            vCodSoftware = 1064
        End If
            
        If strTp_banco = "U" Then
           vSqlInt = "SELECT a.vl_parametro " & _
                     "FROM helpdesk.parametros a " & _
                     "where a.cod_software = " & Str(vCodSoftware) & _
                     " and a.nome_parametro= '" & vNomeParametro & "'"
        Else
           vSqlInt = "SELECT a.vl_parametro " & _
                     "FROM DEP" & Format(lngCD, "00") & ".parametros a " & _
                     "where a.cod_software = " & Str(vCodSoftware) & _
                     " and a.nome_parametro= '" & vNomeParametro & "'"
        End If
        
        Set vRegistros = oradatabase.DbCreateDynaset(vSqlInt, 0&)
             
        If vRegistros.EOF Then
            Set vRegistros = Nothing
        End If
    Else
        Set vRegistros = Nothing
    End If
    
    Set BuscaParametros = vRegistros
    
    Exit Function
        
TrataErro:
    Set BuscaParametros = Nothing
    MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
        Err.Description & " - gerado por " & Err.Source
End Function
'CBacci - 16/10/2012 - SDS2671 - Fun��o que busca as Naturezas que s�o de remessa para Armaz�m Geral
Public Sub BuscaParametroNatsRemessa()
    Dim vTabParametro As Object
    
    vNatsRemessaAg = ""
    Set vTabParametro = BuscaParametros(1064, "NAT_OP_NF_SAIDA")
    If Not vTabParametro Is Nothing Then
        While Not vTabParametro.EOF
            vNatsRemessaAg = vNatsRemessaAg & vTabParametro!VL_PARAMETRO & ";"
            vTabParametro.MoveNext
        Wend
        Set vTabParametro = Nothing
    End If
    
End Sub
'CBacci - 19/10/2012 - SDS2671 - Fun��o que retorna se a natureza � uma remessa de Armaz�m
Public Function ValidaNatRemessaAg(ByRef vNatureza As String) As Boolean
    ValidaNatRemessaAg = (InStr(1, vNatsRemessaAg, vNatureza, vbTextCompare) > 0)
End Function

'fcassiani 17/10/12 Fun��o da VB Gen�rica que n�o funciona na DLL do ambiente DPK
Function ValidaCGC(pCGC) As Boolean

    Dim vFator As Integer
    Dim vSoma As Double
    Dim vResto As Double
    Dim vDigito As Integer
    Dim vVerif As Integer
    Dim i As Integer
    
    vFator = 2
    vSoma = 0
    pCGC = Format(pCGC, "00000000000000")

    For i = 12 To 1 Step -1
        vSoma = vSoma + (Val(Mid(pCGC, i, 1)) * vFator)
        vFator = vFator + 1
        If vFator = 10 Then
            vFator = 2
        End If
    Next
    vResto = Int(vSoma / 11) * 11
    vResto = vSoma - vResto
    If vResto = 0 Or _
            vResto = 1 Then
        vDigito = 0
    Else
        vDigito = 11 - vResto
    End If
    vVerif = vDigito * 10
    vSoma = 0
    vFator = 2
    For i = 13 To 1 Step -1
        vSoma = vSoma + (Val(Mid(pCGC, i, 1)) * vFator)
        vFator = vFator + 1
        If vFator = 10 Then
            vFator = 2
        End If
    Next
    vResto = Int(vSoma / 11) * 11
    vResto = vSoma - vResto
    If vResto = 0 Or _
            vResto = 1 Then
        vDigito = 0
    Else
        vDigito = 11 - vResto
    End If
    vVerif = vVerif + vDigito
    vVerif = Format(vVerif, "00")
    If vVerif = Mid(pCGC, 13, 2) Then
        ValidaCGC = True
    Else
        ValidaCGC = False
    End If

End Function

'fcassiani - 17/10/12 - SDS2671 - Fun��o da VB Gen�rica que n�o funciona na DLL do ambiente DPK
Function Numero(ByVal KeyAscii As Integer) As Integer

    If KeyAscii = 8 Then
        Numero = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or _
            Chr(KeyAscii) > "9" Then
        KeyAscii = 0
    End If
    Numero = KeyAscii
    
End Function
'CBacci - 20/10/2012 - SDS2671 - Fun��o que retorno os itens da nota de compra
Public Function BuscaItensNotaCompra(ByRef vCodLoja As Integer, ByRef vNumNf As Long, _
        ByRef vCnpj As String, ByRef vSerie As String, ByRef vDifer As Integer, _
        ByRef vSituacao As Integer) As Object
    On Error GoTo TrataErro
    Dim vRegistros As Object
    Dim vSqlInt As String
    Dim v_Cod_Erro As Integer
    Dim v_Txt_Erro As String
    
    Criar_Cursor
    
    oradatabase.Parameters.Remove "PM_COD_LOJA"
    oradatabase.Parameters.Remove "PM_NUM_NF"
    oradatabase.Parameters.Remove "PM_CGC"
    oradatabase.Parameters.Remove "PM_SERIE"
    oradatabase.Parameters.Remove "PM_DIFER"
    oradatabase.Parameters.Remove "PM_SITUACAO"
    oradatabase.Parameters.Add "PM_COD_LOJA", vCodLoja, 1
    oradatabase.Parameters.Add "PM_NUM_NF", vNumNf, 1
    oradatabase.Parameters.Add "PM_CGC", vCnpj, 1
    oradatabase.Parameters.Add "PM_SERIE", vSerie, 1
    oradatabase.Parameters.Add "PM_DIFER", vDifer, 1
    oradatabase.Parameters.Add "PM_SITUACAO", vSituacao, 1
    oradatabase.Parameters.Add "PM_COD_ERRO", 0, 2
    oradatabase.Parameters.Add "PM_TXT_ERRO", "", 2

    vSqlInt = strTabela_Banco & "PCK_VDA160.PR_BUSCA_ITENS_NOTA_COMPRA(:PM_Cursor,:PM_COD_LOJA," & _
       ":PM_NUM_NF,:PM_CGC, :PM_SERIE, :PM_DIFER, :PM_SITUACAO, :PM_COD_ERRO, :PM_TXT_ERRO)"
     
    vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSqlInt)
     
    Set vRegistros = oradatabase.Parameters("PM_Cursor").Value
     
    v_Cod_Erro = oradatabase.Parameters("PM_COD_ERRO").Value
    If v_Cod_Erro <> 0 Then
        v_Txt_Erro = oradatabase.Parameters("PM_TXT_ERRO").Value
    End If
         
    oradatabase.Parameters.Remove "PM_COD_ERRO"
    oradatabase.Parameters.Remove "PM_TXT_ERRO"
     
    If vRegistros.EOF Then
        Set vRegistros = Nothing
    End If
    
    Set BuscaItensNotaCompra = vRegistros
    
    Exit Function
        
TrataErro:
    Set BuscaItensNotaCompra = Nothing
    If v_Cod_Erro = 0 Then
        oradatabase.Parameters.Remove "PM_COD_ERRO"
        oradatabase.Parameters.Remove "PM_TXT_ERRO"
        MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
            Err.Description & " - gerado por " & Err.Source
    Else
        MsgBox "Erro no banco de dados n�mero " & v_Cod_Erro & " - Descri��o " & v_Txt_Erro
    End If
End Function
'CBacci - 20/10/2012 - SDS2671 - Busca os itens da nota de compra e grava
Public Sub GravarPedidoItens(ByRef vNumPedido As Long, ByRef vSeqPedido As Integer, _
        ByRef vCodLoja As Integer, ByRef vNumNf As Long, ByRef vCnpj As String, _
        ByRef vSerie As String, ByRef vDifer As Integer, ByRef vSituacao As Integer)
    Dim vTabItensCompra As Object
    Dim vTabItemCadastro As Object
    Dim vNum_Item As Integer
        
    vNum_Item = 0
    Set vTabItensCompra = BuscaItensNotaCompra(vCodLoja, vNumNf, vCnpj, vSerie, vDifer, vSituacao)
    
    If Not vTabItensCompra Is Nothing Then
        While Not vTabItensCompra.EOF
            Set vTabItemCadastro = BuscaItemPorCodDpk(vCodLoja, vTabItensCompra!Cod_Dpk)
            If Not vTabItemCadastro Is Nothing Then
                vNum_Item = vNum_Item + 1
                Call GravaPedidoItens(vNumPedido, vSeqPedido, vCodLoja, vNum_Item, _
                        vTabItensCompra!Cod_Dpk, vTabItemCadastro!cod_fornecedor, _
                        vTabItemCadastro!cod_fabrica, vTabItemCadastro!cod_unidade, _
                        vTabItemCadastro!desc_item, 4, 0, vTabItensCompra!Qtd_Atendida, _
                        vTabItensCompra!Preco_Unitario, vTabItensCompra!Preco_Unitario, _
                        0, 0, vSituacao)
                Call GravaArmazemGeral(vCodLoja, vNumPedido, vSeqPedido, vNum_Item, vNumNf, vCnpj, _
                        vSerie, vDifer, vSituacao, 4, 0, vTabItensCompra!Preco_Unitario, 0, 0, 0, _
                        0, 0)
            End If
            vTabItensCompra.MoveNext
        Wend
    End If
            
End Sub
'CBacci - 20/10/2012 - SDS2671 - Busca os dados do item pelo cod_dpk
Public Function BuscaItemPorCodDpk(ByRef vCodLoja As Integer, ByRef vCodDpk As Long) As Object
    On Error GoTo TrataErro
    Dim vRegistros As Object
    Dim vSqlInt As String
    
    If vCodDpk <> 0 Then
        'passa parametros para a procedure
        oradatabase.Parameters.Remove "pm_loja"
        oradatabase.Parameters.Add "pm_loja", vCodLoja, 1
        oradatabase.Parameters.Remove "pm_forn"
        oradatabase.Parameters.Add "pm_forn", "-1", 1
        oradatabase.Parameters.Remove "pm_dpk"
        oradatabase.Parameters.Add "pm_dpk", vCodDpk, 1
        oradatabase.Parameters.Remove "pm_fabrica"
        oradatabase.Parameters.Add "pm_fabrica", "-1", 1
    
        'padr�o pra criar cursor
        Criar_Cursor
    
        vSqlInt = strTabela_Banco & "PCK_VDA160.pr_consulta_itens(:Pm_Cursor, :pm_loja,:pm_forn,:pm_dpk,:pm_fabrica)"
    
        vErro = vVB_Generica_001.ExecutaPl(oradatabase, vSqlInt)
    
        Set vRegistros = oradatabase.Parameters("Pm_Cursor").Value
        
        If vRegistros.EOF Then
            Set vRegistros = Nothing
        End If
    Else
        Set vRegistros = Nothing
    End If
    
    Set BuscaItemPorCodDpk = vRegistros
    
    Exit Function
TrataErro:
    Set BuscaItemPorCodDpk = Nothing
    MsgBox "Erro n�mero #" & Str$(Err.Number) & " - " & _
        Err.Description & " - gerado por " & Err.Source
End Function

'TI-5024
Function fFormata_Data(pData As String) As String

    If Trim(pData) = "" Then Exit Function

    Dim vDia As Byte
    Dim vMes As Byte
    Dim vAno As Integer
    Dim vMesExtenso As String
    Dim vHora As String
    
    vDia = Day(CDate(pData))
    vMes = Month(CDate(pData))
    vAno = Year(CDate(pData))
    
    If Len(pData) > 10 Then
        vHora = Trim(Right(pData, 8))
    End If
    
    vMesExtenso = Choose(vMes, "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
    
    fFormata_Data = Trim(Format(vDia, "00") & "-" & vMesExtenso & "-" & Format(vAno, "0000") & " " & vHora)

End Function



