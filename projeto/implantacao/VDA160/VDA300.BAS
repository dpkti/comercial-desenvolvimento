Attribute VB_Name = "Module2"
'Declarar variaveis globais
Public strCod_nope As String
Global lngNUM_PEDIDO As Long    'NUMERO DE PEDIDO
Global Const lngSEQ_PEDIDO = 0  'TI-5024
Global sCOD_VEND As String       'CODIGO DE VENDEDOR
'Global strPath As String        'PATH DOS DADOS NA REDE
Global strPathUsuario As String 'PATH DO USUARIO
'Global dbAccess As Database     'BANCO DE DADOS ACCESS    'TI-5024
Global lngcod_cliente As Long   'CODIGO DO CLIENTE - DPK 24 HORAS
Public sngDesc_uf As Single
Public qtd As Byte
Public tp_item As Byte           ' indica se o item ser� digitado preco liquido
Public ssItem As Object
Public ssTrib As Object
Public bTributacao_Final As Byte
Public strNatureza As String
Public strIEfim As String
Public strISFim As String
Public lngCod_Loja As Long
Public lngFilial As Long
Public tp_cliente As String
Public ssDescontos As Object
Public Tribut As Integer
Public Preco_Bruto As Double
Public class_fiscal As String
Public sPC_DESCONTO_PLANO As Single
Public sPC_ACRESCIMO_PLANO As Single
Public lngQtd_inf As Long
Public lngQtd_dig As Long
Public item As Boolean
Public tela As String
Public strDt_Tabela As String
Public ckicm As Boolean



Public tp_tabela As Integer
Public ocorr_preco As Integer
Public SQL As String
Public deposito_default As String
Public uf_destino As String
Public uf_origem As String
Public FILIAL_PED As String
Public tipo_cliente As String     'categoria do cliente

Public qtd_max As Long
Public qtd_min As Long


Public forn As Integer
Public pesq As String
Public data_real As Date
Public dt_pedido As Date
Public dt_carencia As Date
Public dt_vig2 As Boolean     'Se dt_vig2=true existe data de vigencia2
'Se dt_vig2=false n�o existe data de vigencia2,
'portanto a tabela entrou em vigor na semana atual
Public contador As Long




Public Fl_Quantidade As String
Public Fl_Cod_Merc As String
Public Fl_Tributacao As String
Public Fl_Base_Red_ICMS As String

Public Cod_Trib_Icms1 As String
Public Cod_Trib_Icms2 As String
Public Cod_Trib_Icms3 As String
Public Cod_Trib_Icms4 As String
Public Cod_Trib_Icms5 As String
Public Cod_Trib_Icms6 As String

Public Cod_Trib_Ipi1 As String
Public Cod_Trib_Ipi2 As String
Public Cod_Trib_Ipi3 As String
Public Cod_Trib_Ipi4 As String

Public Fl_NFItem As String


Public grupo As Integer
Public subgrupo As Integer

Global Const Branco = &HFFFFFF
Global Const Vermelho = &H8080FF
Global Const Cinza = &H8000000F
Public strTabela_Banco As String


Sub ReSequenciar()

    On Error GoTo trata_erro

    Dim SQL As String
    Dim ss As Object   'TI-5024
    Dim ss2 As Object  'TI-5024
    Dim PLSQL As String
    Dim i As Integer
    Dim ITEM_COT As Integer
    Dim qtd As Long

    'carregar dpk e sequencias
1             SQL = "select COD_DPK,NUM_ITEM_PEDIDO from VENDAS.ITPEDIDO_OUTSAI where "
2             SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
3             SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
4             SQL = SQL & " and COD_LOJA = " & lngCod_Loja
5             SQL = SQL & " order by NUM_ITEM_PEDIDO"
'6             Set ss = dbAccess.CreateSnapshot(SQL)   'TI-5024
6             Set ss = oradatabase.DbCreateDynaset(SQL, 0&)
7             FreeLocks
8             If ss.EOF Then
9                 Exit Sub
10            End If
11            ss.MoveFirst
12            ss.MoveLast
13            If (ss.EOF And ss.BOF) Or ss.RecordCount = 0 Then
14                Exit Sub
15            Else

16            End If
17            ss.MoveFirst
18            i = 1
19            Do
20                If i <> ss("NUM_ITEM_PEDIDO") Then
                      SQL = "Begin "
21                    SQL = SQL & "update VENDAS.ITPEDIDO_OUTSAI "
22                    SQL = SQL & "set NUM_ITEM_PEDIDO = " & i
23                    SQL = SQL & " where SEQ_PEDIDO = " & lngSEQ_PEDIDO
24                    SQL = SQL & " and NUM_PENDENTE = " & lngNUM_PEDIDO
25                    SQL = SQL & " and COD_LOJA = " & lngCod_Loja
26                    SQL = SQL & " and NUM_ITEM_PEDIDO = " & ss("NUM_ITEM_PEDIDO") & ";"
                      SQL = SQL & " COMMIT;"
                      SQL = SQL & "EXCEPTION"
                      SQL = SQL & " WHEN OTHERS THEN"
                      SQL = SQL & " ROLLBACK;"
                      SQL = SQL & " :cod_errora := SQLCODE;"
                      SQL = SQL & " :txt_errora := SQLERRM;"
                      SQL = SQL & "END;"
                      oradatabase.Parameters.Remove "cod_errora"
                      oradatabase.Parameters.Add "cod_errora", 0, 2
                      oradatabase.Parameters.Remove "txt_errora"
                      oradatabase.Parameters.Add "txt_errora", "", 2
'27                    dbAccess.Execute SQL, dbFailOnError 'TI-5024
27                    oradatabase.ExecuteSQL SQL
28                    FreeLocks
29                End If
30                i = i + 1
31                ss.MoveNext
32            Loop Until ss.EOF
33            ss.Close

    'SQL = "select max(num_item_pedido) as qtd"
    'SQL = SQL & " from VENDAS.ITPEDIDO_OUTSAI"
    'SQL = SQL & " where "
    'SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
    'SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
    'SQL = SQL & " and COD_LOJA = " & lngCod_Loja

    'Set ss2 = dbAccess.CreateSnapshot(SQL)
    'FreeLocks

    'frmVisPedido.txtQtdeItens.Text = ss2!qtd
    'qtd = ss2!qtd

    'SQL = "update pedido set qtd_itens = " & qtd
    'SQL = SQL & " where "
    'SQL = SQL & " NUM_PENDENTE = " & lngNUM_PEDIDO
    'SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
    'SQL = SQL & " and COD_LOJA = " & lngCod_Loja

    'atualizar
    'dbAccess.Execute SQL, dbFailOnError
    'FreeLocks

    'ss2.Close

trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ReSequenciar" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub
'TI-5024
Public Function GravaPedidoItens(ByRef vNumPedido As Long, ByRef vSeqPedido As Integer, _
        ByRef vCodLoja As Integer, ByRef vNumItem As Integer, ByRef vCodDpk As Long, _
        ByRef vCodFornecedor As Integer, ByRef vCodFabrica As String, ByRef vUnidade As String, _
        ByRef vDescItem As String, ByRef vCodTrib As Integer, ByRef vCodTribIPI As Integer, _
        ByRef vQtd As Long, ByRef vPrecoUnit As Double, ByRef vPrecoLiq As Double, _
        ByRef vBasered As Double, ByRef vIpi As Double, ByRef vSituacao As Integer) As Boolean
    On Error GoTo TrataErro

    SQL = "Begin"
    SQL = SQL & " Insert into vendas.ITPEDIDO_OUTSAI ( NUM_PENDENTE,"
    SQL = SQL & " SEQ_PEDIDO,"
    SQL = SQL & " COD_LOJA,"
    SQL = SQL & " NUM_ITEM_PEDIDO,"
    SQL = SQL & " COD_DPK,"
    SQL = SQL & " COD_FORNECEDOR,"
    SQL = SQL & " COD_FABRICA,"
    SQL = SQL & " UNIDADE,"
    SQL = SQL & " DESC_ITEM,"
    SQL = SQL & " COD_TRIBUTACAO,"
    SQL = SQL & " COD_TRIBUTACAO_IPI,"
    SQL = SQL & " QTD_SOLICITADA,"
    SQL = SQL & " PRECO_UNITARIO,"
    SQL = SQL & " PRECO_LIQUIDO,"
    SQL = SQL & " PC_BASERED,"
    SQL = SQL & " PC_IPI, SITUACAO) "
    SQL = SQL & " values(" & vNumPedido & ","
    SQL = SQL & vSeqPedido & ","
    SQL = SQL & vCodLoja & ","
    SQL = SQL & vNumItem & ","
    SQL = SQL & vCodDpk & ","
    SQL = SQL & vCodFornecedor & ","
    SQL = SQL & "'" & vCodFabrica & "',"
    SQL = SQL & "'" & vUnidade & "',"
    SQL = SQL & "'" & vDescItem & "',"
    SQL = SQL & vCodTrib & ","
    SQL = SQL & vCodTribIPI & ","
    SQL = SQL & vQtd & ","
    SQL = SQL & FmtBR(vPrecoUnit) & ","
    SQL = SQL & FmtBR(vPrecoLiq) & ","
    SQL = SQL & FmtBR(vBasered) & ","
    SQL = SQL & FmtBR(vIpi) & ","
    SQL = SQL & vSituacao & ");"
    SQL = SQL & " COMMIT;"
    SQL = SQL & "EXCEPTION"
    SQL = SQL & " WHEN OTHERS THEN"
    SQL = SQL & " ROLLBACK;"
    SQL = SQL & " :cod_errora := SQLCODE;"
    SQL = SQL & " :txt_errora := SQLERRM;"
    SQL = SQL & "END;"
    oradatabase.Parameters.Remove "cod_errora"
    oradatabase.Parameters.Add "cod_errora", 0, 2
    oradatabase.Parameters.Remove "txt_errora"
    oradatabase.Parameters.Add "txt_errora", "", 2
    'criar item de pedido
    oradatabase.ExecuteSQL SQL
    FreeLocks

    'atualizar pedido de venda
    SQL = "Begin "
    SQL = SQL & "update vendas.PEDIDO_OUTSAI set "
    SQL = SQL & "VL_CONTABIL = VL_CONTABIL + " & FmtBR(FmtBR(vQtd) * FmtBR(vPrecoLiq))
    SQL = SQL & " where NUM_PENDENTE = " & vNumPedido
    SQL = SQL & " and SEQ_PEDIDO = " & vSeqPedido
    SQL = SQL & " and cod_loja = " & vCodLoja & ";"
    SQL = SQL & " COMMIT;"
    SQL = SQL & "EXCEPTION"
    SQL = SQL & " WHEN OTHERS THEN"
    SQL = SQL & " ROLLBACK;"
    SQL = SQL & " :cod_errora := SQLCODE;"
    SQL = SQL & " :txt_errora := SQLERRM;"
    SQL = SQL & "END;"
    oradatabase.Parameters.Remove "cod_errora"
    oradatabase.Parameters.Add "cod_errora", 0, 2
    oradatabase.Parameters.Remove "txt_errora"
    oradatabase.Parameters.Add "txt_errora", "", 2
     'executa atualizacao
    oradatabase.ExecuteSQL SQL
    FreeLocks
    
    GravaPedidoItens = True
    
    Exit Function

TrataErro:
    GravaPedidoItens = False
    If Err.Number <> 0 Then
       MsgBox "GravaPedidoItens" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Function

