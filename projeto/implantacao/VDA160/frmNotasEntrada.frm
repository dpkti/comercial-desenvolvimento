VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmNotasEntrada 
   Caption         =   "Notas Fiscais de Entrada"
   ClientHeight    =   3210
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   ScaleHeight     =   3210
   ScaleWidth      =   11880
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnFechar 
      Caption         =   "Fechar"
      Height          =   390
      Left            =   10575
      TabIndex        =   1
      Top             =   2745
      Width           =   1215
   End
   Begin MSGrid.Grid grdNfEntrada 
      Height          =   2535
      Left            =   120
      TabIndex        =   0
      Top             =   135
      Width           =   11655
      _Version        =   65536
      _ExtentX        =   20558
      _ExtentY        =   4471
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Cols            =   3
      FixedCols       =   0
   End
   Begin VB.Label lblDuploClick 
      Caption         =   "Duplo clique para selecionar Nota de Entrada"
      ForeColor       =   &H80000002&
      Height          =   270
      Left            =   90
      TabIndex        =   2
      Top             =   2865
      Width           =   3405
   End
End
Attribute VB_Name = "frmNotasEntrada"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnFechar_Click()
    'frmFimPedido.txtCOD_CLIENTE.SetFocus
    'frmFimPedido.txtCNPJ.SetFocus
    If frmFimPedido.txtCOD_CLIENTE.Text = "" Then
        frmFimPedido.txtCOD_CLIENTE.SetFocus
        frmCli_Forn.Show
        StayOnTop frmCli_Forn
    End If
    Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set frmNotasEntrada = Nothing
End Sub

Public Sub grdNfEntrada_DblClick()
    Dim pNF_ENTRADA As Long
    Dim pCOD_LOJA As Integer
    Dim pCGC As String
    Dim pSERIE As String
    Dim pDIFER As Integer
    Dim pQTD_ITEM As Double
    
    pCOD_LOJA = Mid(frmFimPedido.cboDeposito.Text, 1, 3)
    
    grdNfEntrada.Col = 0
    pCGC = grdNfEntrada.Text
    grdNfEntrada.Col = 1
    pNF_ENTRADA = grdNfEntrada.Text
    grdNfEntrada.Col = 3
    pSERIE = grdNfEntrada.Text
    grdNfEntrada.Col = 4
    pDIFER = grdNfEntrada.Text
    grdNfEntrada.Col = 9
    frmFimPedido.txtNrItens = grdNfEntrada.Text
    
    Set ssAG = PreencheSsAg(pCOD_LOJA, pNF_ENTRADA, pCGC, pSERIE, pDIFER)
    
    fl_nf_entrada_carregada = True
    MsgBox "Os dados da nota informada foram carregados com sucesso!", vbInformation, "Nota Carregada"
    Call btnFechar_Click
End Sub

