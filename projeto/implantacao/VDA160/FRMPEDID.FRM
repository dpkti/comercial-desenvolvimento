VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmPedidos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listagem de Pedidos"
   ClientHeight    =   3285
   ClientLeft      =   480
   ClientTop       =   1350
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3285
   ScaleWidth      =   6690
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   330
      Picture         =   "FRMPEDID.frx":0000
      ScaleHeight     =   255
      ScaleWidth      =   375
      TabIndex        =   4
      Top             =   3000
      Width           =   375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Sair"
      Height          =   285
      Left            =   5940
      TabIndex        =   1
      Top             =   2940
      Width           =   645
   End
   Begin MSGrid.Grid grdPedido 
      Height          =   2535
      Left            =   90
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   6495
      _Version        =   65536
      _ExtentX        =   11456
      _ExtentY        =   4471
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "FRMPEDID.frx":030A
   End
   Begin VB.Label Label1 
      Caption         =   "Pedido em Manuten��o"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   810
      TabIndex        =   5
      Top             =   3000
      Width           =   1935
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1650
      TabIndex        =   3
      Top             =   90
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   210
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
End
Attribute VB_Name = "frmPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strPesquisa As String

Private Sub Command1_Click()
    Unload Me
End Sub


Private Sub Form_Load()
          
1         On Error GoTo trata_erro
           
          Dim ss As Object  'TI-5024
          Dim ss2 As Object 'TI-5024
          Dim i As Integer

2         Screen.MousePointer = 11

3         SQL = "Select a.num_pendente, a.seq_pedido,"
4         SQL = SQL & " a.cod_loja, "
5         SQL = SQL & " a.dt_digitacao, "
6         SQL = SQL & " a.qtd_itens,"
7         SQL = SQL & " a.num_pedido,"
8         SQL = SQL & " count(b.num_pendente) as qt "
9         SQL = SQL & " from VENDAS.PEDIDO_OUTSAI a LEFT JOIN VENDAS.ITPEDIDO_OUTSAI b "
10        SQL = SQL & " ON (a.num_pendente = b.num_pendente "
11        SQL = SQL & " and a.seq_pedido = b.seq_pedido and"
12        SQL = SQL & " a.cod_loja = b.cod_loja) "
13        SQL = SQL & " where a.fl_pedido_finalizado = 'N' "
          SQL = SQL & " And a.cod_software in (1064, 1097)"
14        SQL = SQL & " group by a.num_pendente,a.seq_pedido,a.cod_loja,"
15        SQL = SQL & " a.dt_digitacao, a.qtd_itens, a.num_pedido"
16        SQL = SQL & " order by a.num_pendente,a.dt_digitacao"
'17       Set ss = dbAccess.CreateSnapshot(SQL) 'TI-5024
17        Set ss = oradatabase.DbCreateDynaset(SQL, 0&)
18        FreeLocks

19        If ss.EOF Then
20            Screen.MousePointer = vbDefault
21            MsgBox "N�o existe pedido digitado", vbExclamation, "Aten��o"
22            Exit Sub
23        End If

24        grdPedido.Visible = True
25        ss.MoveLast
26        With frmPedidos.grdPedido
27            .Cols = 6
28            .Rows = ss.RecordCount + 1
29            .ColWidth(0) = 1400
30            .ColWidth(1) = 500
31            .ColWidth(2) = 1000
32            .ColWidth(3) = 1500
33            .ColWidth(4) = 800
34            .ColWidth(5) = 800

35            .Row = 0
36            .Col = 0
37            .Text = "  Nr.Pedido"
38            .Col = 1
39            .Text = "Sq."
40            .Col = 2
41            .Text = "  Dep�sito"
42            .Col = 3
43            .Text = "  Dt.Digita��o"
44            .Col = 4
45            .Text = " Qtd.Inf."
46            .Col = 5
47            .Text = "Qtd.Dig."


48            ss.MoveFirst
49            For i = 1 To .Rows - 1
50                .Row = i

51                .Col = 0
52                .ColAlignment(0) = 1
53                If ss!num_pedido <> "" And ss!num_pedido <> 0 Then
54                    .Text = ss!num_pendente
55                    .Picture = Picture1.Picture
56                Else
57                    .Text = ss!num_pendente
58                End If
59                .Col = 1
60                .ColAlignment(1) = 2
61                .Text = ss!seq_pedido
62                .Col = 2
63                .ColAlignment(2) = 2
64                .Text = ss!cod_loja
65                .Col = 3
66                .ColAlignment(3) = 2
67                .Text = Format$(ss!dt_digitacao, "dd/mm/yy hh:nn")
68                .Col = 4
69                .ColAlignment(4) = 2
70                .Text = ss!qtd_itens
71                .Col = 5
72                .ColAlignment(5) = 2
73                .Text = ss!qt

74                ss.MoveNext
75            Next
76            .Row = 1
77        End With
78        ss.Close

          'mouse
79        Screen.MousePointer = vbDefault

80        Exit Sub

trata_erro:
81        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
82            Resume
83        Else
84            MsgBox "frmPedidos_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl

'84            Call Process_Line_Errors(SQL)
85        End If

End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmPedidos = Nothing
End Sub


Private Sub grdPedido_DblClick()

    'Screen.MousePointer = 11
    grdPedido.Col = 0
    lngNUM_PEDIDO = grdPedido.Text
    txtResposta = grdPedido.Text
    grdPedido.Col = 1
    'lngSEQ_PEDIDO = grdPedido.Text   'TI-5024
    grdPedido.Col = 2
    lngCod_Loja = grdPedido.Text
    grdPedido.Col = 4
    lngQtd_inf = grdPedido.Text
    grdPedido.Col = 5
    lngQtd_dig = grdPedido.Text
    Unload Me
    frmVisPedido.Show 1
    'StayOnTop frmVisPedido
    Exit Sub



End Sub

Private Sub grdPedido_KeyPress(KeyAscii As Integer)
          
          Dim iAscii As Integer
          Dim tam As Byte
          Dim i As Integer
          Dim j As Integer
          Dim iLinha As Integer

          On Error GoTo trata_erro

          'carrega variavel de pesquisa
1         tam = Len(strPesquisa)
2         grdPedido.Col = 0
3         If KeyAscii = 13 Then
4             'Screen.MousePointer = 11
5             grdPedido.Col = 0
6             lngNUM_PEDIDO = grdPedido.Text
7             txtResposta = grdPedido.Text
8             grdPedido.Col = 1
'9             lngSEQ_PEDIDO = grdPedido.Text  'TI-5024
10            grdPedido.Col = 2
11            lngCod_Loja = grdPedido.Text
12            grdPedido.Col = 4
13            lngQtd_inf = grdPedido.Text
14            grdPedido.Col = 5
15            lngQtd_dig = grdPedido.Text
16            Unload Me
17            frmVisPedido.Show 1
              ' StayOnTop frmVisPedido
18        ElseIf KeyAscii = 8 Then 'backspace
              'mouse
19            Screen.MousePointer = vbHourglass

20            If tam > 0 Then
21                strPesquisa = Mid$(strPesquisa, 1, tam - 1)
22                If strPesquisa = "" Then
23                    lblPesquisa.Caption = strPesquisa
24                    lblPesquisa.Visible = False
25                    lblPesq.Visible = False
26                    grdPedido.Row = 1
27                    SendKeys "{LEFT}+{END}"
28                Else
29                    lblPesquisa.Caption = strPesquisa
30                    DoEvents
31                    With grdPedido
32                        iLinha = .Row
33                        j = .Row - 1
34                        For i = j To 1 Step -1
35                            .Row = i
36                            If (.Text Like strPesquisa & "*") Then
37                                iLinha = .Row
38                                Exit For
39                            End If
40                        Next
41                        .Row = iLinha
42                        SendKeys "{LEFT}+{END}"
43                    End With
44                End If
45            End If

46        ElseIf KeyAscii = 27 Then
47            strPesquisa = ""
48            lblPesquisa.Caption = strPesquisa
49            lblPesquisa.Visible = False
50            lblPesq.Visible = False
51            grdPedido.Row = 1
52            SendKeys "{LEFT}{RIGHT}"

53        Else
              'mouse
54            Screen.MousePointer = vbHourglass

55            If tam < 33 Then
56                iAscii = Texto(KeyAscii)
57                If iAscii > 0 Then
                      'pesquisa
58                    strPesquisa = strPesquisa & Chr$(iAscii)
59                    If tam >= 0 Then
60                        lblPesquisa.Visible = True
61                        lblPesq.Visible = True
62                        lblPesquisa.Caption = strPesquisa
63                        DoEvents
64                        With grdPedido
65                            iLinha = .Row
66                            If tam = 0 Then
67                                .Row = 0
68                                For i = 1 To .Rows - 1
69                                    .Row = i
70                                    If (.Text Like strPesquisa & "*") Then
71                                        iLinha = .Row
72                                        Exit For
73                                    End If
74                                Next i
75                            Else
76                                j = .Row
77                                For i = j To .Rows - 1
78                                    .Row = i
79                                    If (.Text Like strPesquisa & "*") Then
80                                        iLinha = .Row
81                                        Exit For
82                                    End If
83                                Next
84                            End If
85                            If grdPedido.Row <> iLinha Then
86                                .Row = iLinha
87                                strPesquisa = Mid$(strPesquisa, 1, tam)
88                                lblPesquisa.Caption = strPesquisa
89                                Beep
90                            End If
91                            SendKeys "{LEFT}+{END}"
92                        End With

93                    End If
94                End If
95            Else
96                Beep
97            End If
98        End If

          'mouse
99        Screen.MousePointer = vbDefault
trata_erro:
          MsgBox "grdPedido_KeyPress" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl

End Sub

