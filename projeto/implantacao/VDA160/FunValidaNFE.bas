Attribute VB_Name = "FunValidaNFE"
Option Explicit
Global vCharEspeciais As String

Function ValidaLetra(ByVal KeyAscii As Integer) As Integer

    On Error GoTo trata_erro
    
    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaLetra = KeyAscii
        Exit Function

    End If

    If KeyAscii < 65 Or KeyAscii > 90 Then

        KeyAscii = 0
        
    End If

    ValidaLetra = KeyAscii
    
    Exit Function

trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaLetra" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If

End Function

Function ValidaNumero(ByVal KeyAscii As Integer) As Integer

On Error GoTo trata_erro

    If KeyAscii = 8 Or KeyAscii = 13 Then

        ValidaNumero = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaNumero = KeyAscii

Exit Function

trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaNumero" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Function ValidaAlfaNumerico(ByVal KeyAscii As Integer) As Integer

On Error GoTo trata_erro

    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaAlfaNumerico = KeyAscii
        Exit Function

    End If
    
    If InStr(1, vCharEspeciais, Chr(KeyAscii)) > 0 Then
        
        KeyAscii = 0

    End If

    ValidaAlfaNumerico = KeyAscii

Exit Function

trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaAlfaNumerico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Public Function ValidaMaiuscula(pKeyascii As Integer) As Integer

    ValidaMaiuscula = Asc(UCase(Chr(pKeyascii)))

    Exit Function

End Function

Function ValidaBranco(pTXT As TextBox) As Boolean
        
    Dim vbranco As Byte
    Dim i  As Long
    
    'Atribui true na fun��o
    ValidaBranco = True
    
    'Valida se existe mais de um espa�o em branco nos campos
    For i = 1 To Len(pTXT)
        If Asc(Mid(pTXT, i, 1)) = 32 Then
            vbranco = vbranco + 1
        Else
            vbranco = 0
        End If
        If vbranco >= 2 Then
            ValidaBranco = False
            Exit Function
        End If
    Next
 
End Function

Function ValidaTamanho(pTXT As TextBox, pTamanho As Integer) As Boolean
    
    'Atribui true na fun��o
    ValidaTamanho = True
    
    'Valida o tamnho do campo
    If Len(pTXT) < pTamanho Then
        ValidaTamanho = False
    End If
 
End Function

Public Function fCharEspeciais()

1         On Error GoTo trata_erro
2         oradatabase.Parameters.Remove "PM_CD"
4         oradatabase.Parameters.Remove "PM_CARACTERES"
          
5         oradatabase.Parameters.Add "PM_CD", lngCD, 1
7         oradatabase.Parameters.Add "PM_CARACTERES", "", 2
          
8         oradatabase.ExecuteSQL "BEGIN PRODUCAO.PR_SELECT_CARACT_ESP_NFE(:PM_CD, :PM_CARACTERES);END;"
          
9         fCharEspeciais = oradatabase.Parameters("PM_CARACTERES").Value
       
trata_erro:
10        If Err.Number <> 0 Then
11            MsgBox "Function: fCharEspeciais" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, vbCritical, "CTB050 - GNRE SA�DA"
12        End If
End Function
