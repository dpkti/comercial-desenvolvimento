VERSION 5.00
Begin VB.Form frmIEST 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "VDA540 - Digitar Inscri��o Estadual ST"
   ClientHeight    =   1005
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6345
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1005
   ScaleWidth      =   6345
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtDeposito 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   30
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   240
      Width           =   2055
   End
   Begin VB.CommandButton cmdFechar 
      Caption         =   "Fechar"
      Height          =   375
      Left            =   4680
      TabIndex        =   11
      Top             =   570
      Width           =   735
   End
   Begin VB.CommandButton cmdGravar 
      Caption         =   "Gravar "
      Height          =   375
      Left            =   5520
      TabIndex        =   10
      Top             =   570
      Width           =   735
   End
   Begin VB.TextBox txtIEST 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   4650
      MaxLength       =   12
      TabIndex        =   4
      Top             =   240
      Width           =   1605
   End
   Begin VB.TextBox txtDtPedido 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3600
      Locked          =   -1  'True
      MaxLength       =   8
      TabIndex        =   3
      Top             =   240
      Width           =   975
   End
   Begin VB.TextBox txtSequencia 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3150
      Locked          =   -1  'True
      MaxLength       =   1
      TabIndex        =   2
      Top             =   240
      Width           =   285
   End
   Begin VB.TextBox txtNrpedido 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2130
      Locked          =   -1  'True
      MaxLength       =   7
      TabIndex        =   1
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "Inscri��o Estadual ST"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   4650
      TabIndex        =   9
      Top             =   60
      Width           =   1560
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Data Pedido"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3600
      TabIndex        =   8
      Top             =   60
      Width           =   885
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Seq"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   3150
      TabIndex        =   7
      Top             =   60
      Width           =   285
   End
   Begin VB.Label lblDeposito 
      AutoSize        =   -1  'True
      Caption         =   "Dep�sito"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   30
      TabIndex        =   6
      Top             =   30
      Width           =   630
   End
   Begin VB.Label lblPedido 
      AutoSize        =   -1  'True
      Caption         =   "Nr.Pedido"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2130
      TabIndex        =   5
      Top             =   60
      Width           =   705
   End
End
Attribute VB_Name = "frmIEST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdFechar_Click()
    Unload Me
End Sub

Private Sub cmdGravar_Click()

1     On Error GoTo Trata_Erro

2         If Trim(txtIEST) = "" Then
3             MsgBox "Informe a Inscri��o Estadual ST.", vbCritical, "VDA540 - IE ST"
4             txtIEST.SetFocus
5             Exit Sub
6         End If

7          If Len(Trim(txtIEST)) < 7 Then
8              MsgBox "Informe a Inscri��o Estadual ST.", vbCritical, "VDA540 - IE ST"
9              txtIEST.SetFocus
10             Exit Sub
11         End If

12         If Not ValidaInscricaoEstadual(uf_origem, "", Trim(txtIEST)) Then
13             MsgBox "Inscri��o Estadual ST inv�lida.", vbCritical, "VDA540 - IE ST"
14             txtIEST.SetFocus
15             Exit Sub
16         End If

17        Criar_Cursor
              
18        oradatabase.Parameters.Remove "PM_COD_LOJA"
19        oradatabase.Parameters.Add "PM_COD_LOJA", Val(txtDeposito), 1
          
20        oradatabase.Parameters.Remove "PM_NUM_PEDIDO"
21        oradatabase.Parameters.Add "PM_NUM_PEDIDO", Val(txtNrpedido), 1
          
22        oradatabase.Parameters.Remove "PM_SEQ_PEDIDO"
23        oradatabase.Parameters.Add "PM_SEQ_PEDIDO", Val(txtSequencia), 1
          
24        oradatabase.Parameters.Remove "PM_DT_PEDIDO"
25        oradatabase.Parameters.Add "PM_DT_PEDIDO", Formata_Data(txtDtPedido), 1
          
26        oradatabase.Parameters.Remove "PM_IE_ST"
27        oradatabase.Parameters.Add "PM_IE_ST", txtIEST, 1
              
28        oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA540.PR_IE_ST(:PM_CURSOR, :PM_COD_LOJA, :PM_NUM_PEDIDO, :PM_SEQ_PEDIDO, :PM_DT_PEDIDO, :PM_IE_ST); END;"

29         oradatabase.Parameters.Remove "PM_COD_LOJA"
30         oradatabase.Parameters.Remove "PM_NUM_PEDIDO"
31         oradatabase.Parameters.Remove "PM_SEQ_PEDIDO"
32         oradatabase.Parameters.Remove "PM_DT_PEDIDO"
33         oradatabase.Parameters.Remove "PM_IE_ST"
34         oradatabase.Parameters.Remove "PM_CURSOR"

35        frmVisPedido.vIEST = True

36        Unload Me

Trata_Erro:
37        If Err.Number <> 0 Then
38            MsgBox "Sub cmdGravar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
39            frmVisPedido.vIEST = False
40        End If

End Sub

Private Sub Form_Load()

    txtDeposito = frmVisPedido.lblDeposito(17)
    txtNrpedido = frmVisPedido.lblNr_Pedido
    txtSequencia = frmVisPedido.lblSeq_Pedido
    txtDtPedido = frmVisPedido.lblDt_Pedido
    
    If Val(txtDeposito) > 0 And Val(txtNrpedido) > 0 And Trim(txtSequencia) <> "" And Trim(txtDtPedido) <> "" Then
        
        Pesquisar_IE_ST
    
    End If
    
    If Trim(txtIEST) <> "" Then
        frmVisPedido.vIEST = True
    End If
    
End Sub

Private Sub txtDtPedido_KeyPress(Keyascii As Integer)
    Data Keyascii, txtDtPedido
End Sub

Function Formata_Data(pData As String) As String
    Dim vDia As String
    Dim vMes As String
    Dim vAno As String
    
    vDia = Format(Day(CDate(pData)), "00")
    vMes = Choose(Month(CDate(pData)), "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
    vAno = Format(Year(CDate(pData)), "0000")
    
    Formata_Data = vDia & "-" & vMes & "-" & vAno

End Function

Sub Pesquisar_IE_ST()
    
    Dim vObj As Object
    
    Criar_Cursor
        
    oradatabase.Parameters.Remove "PM_COD_LOJA"
    oradatabase.Parameters.Add "PM_COD_LOJA", Val(txtDeposito), 1
    
    oradatabase.Parameters.Remove "PM_NUM_PEDIDO"
    oradatabase.Parameters.Add "PM_NUM_PEDIDO", Val(txtNrpedido), 1
    
    oradatabase.Parameters.Remove "PM_SEQ_PEDIDO"
    oradatabase.Parameters.Add "PM_SEQ_PEDIDO", Val(txtSequencia), 1
    
    oradatabase.Parameters.Remove "PM_DT_PEDIDO"
    oradatabase.Parameters.Add "PM_DT_PEDIDO", Formata_Data(txtDtPedido), 1
    
    oradatabase.Parameters.Remove "PM_IE_ST"
    oradatabase.Parameters.Add "PM_IE_ST", 0, 1
        
    oradatabase.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA540.PR_IE_ST(:PM_CURSOR, :PM_COD_LOJA, :PM_NUM_PEDIDO, :PM_SEQ_PEDIDO, :PM_DT_PEDIDO, :PM_IE_ST); END;"
    
    Set vObj = oradatabase.Parameters("PM_CURSOR").Value
    
    Me.txtIEST = "" & vObj!INSCR_ESTADUAL_ST
    
    Set vObj = Nothing
    
End Sub

