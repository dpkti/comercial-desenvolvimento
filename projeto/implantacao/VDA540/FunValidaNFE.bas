Attribute VB_Name = "FunValidaNFE"
Option Explicit
Global vCharEspeciais As String

Function ValidaLetra(ByVal KeyAscii As Integer) As Integer

    On Error GoTo Trata_Erro
    
    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaLetra = KeyAscii
        Exit Function

    End If

    If KeyAscii < 65 Or KeyAscii > 90 Then

        KeyAscii = 0
        
    End If

    ValidaLetra = KeyAscii
    
    Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaLetra" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If

End Function

Function ValidaNumero(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 13 Then

        ValidaNumero = KeyAscii
        Exit Function

    End If

    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then

        KeyAscii = 0

    End If

    ValidaNumero = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaNumero" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Function ValidaAlfaNumerico(ByVal KeyAscii As Integer) As Integer

On Error GoTo Trata_Erro

    If KeyAscii = 8 Or KeyAscii = 32 Or KeyAscii = 13 Then

        ValidaAlfaNumerico = KeyAscii
        Exit Function

    End If
    
    If InStr(1, vCharEspeciais, Chr(KeyAscii)) > 0 Then
        
        KeyAscii = 0

    End If
    
    ValidaAlfaNumerico = KeyAscii

Exit Function

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Function ValidaAlfaNumerico" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description, "Aten��o", &H40000
    End If
End Function

Public Function ValidaMaiuscula(pKeyascii As Integer) As Integer

    ValidaMaiuscula = Asc(UCase(Chr(pKeyascii)))

    Exit Function

End Function

Function ValidaBranco(pTXT As TextBox) As Boolean
        
    Dim vbranco As Byte
    Dim i  As Long
    
    'Atribui true na fun��o
    ValidaBranco = True
    
    'Valida se existe mais de um espa�o em branco nos campos
    For i = 1 To Len(pTXT)
        If Asc(Mid(pTXT, i, 1)) = 32 Then
            vbranco = vbranco + 1
        Else
            vbranco = 0
        End If
        If vbranco >= 2 Then
            ValidaBranco = False
            Exit Function
        End If
    Next
 
End Function

Function ValidaTamanho(pTXT As TextBox, pTamanho As Integer) As Boolean
    
    'Atribui true na fun��o
    ValidaTamanho = True
    
    'Valida o tamnho do campo
    If Len(pTXT) < pTamanho Then
        ValidaTamanho = False
    End If
 
End Function

Function ValidaInscricaoEstadual(V_UF As String, V_DT_CAD As String, V_IE As String) As Boolean
 
1         On Error GoTo Trata_Erro
 
          Dim VALOR_IE
          Dim S_SQL, vRet As Byte
 
2         ValidaInscricaoEstadual = True
3         If V_UF = "AC" Then
4             If Len(V_IE) = 9 Then
5                 V_DT_CAD = "101999"
6             ElseIf Len(V_IE) = 13 Then
7                 V_DT_CAD = "121999"
8             Else
9                 ValidaInscricaoEstadual = False
10                V_DT_CAD = ""
11            End If
12        ElseIf V_UF = "RO" Then
13            If Len(V_IE) = 9 Then
14                V_DT_CAD = "062000"
15            ElseIf Len(V_IE) = 14 Then
16                V_DT_CAD = "092000"
17            Else
18                V_DT_CAD = ""
19                ValidaInscricaoEstadual = False
20            End If
21        Else
22            V_DT_CAD = "      "
23            ValidaInscricaoEstadual = True
24        End If
          'Se a IE n�o for Isenta, ent�o chama a fun��o para validar a IE
25        If (V_IE <> "ISENTA" Or V_IE <> "ISENTO" Or V_IE <> "") And ValidaInscricaoEstadual <> False Then
26            S_SQL = "BEGIN PRODUCAO.PCK_IE.PR_IE(:UF,:DT_IE,:IE,:IE_VALIDA); END;"
27            oradatabase.Parameters.Remove "UF"
28            oradatabase.Parameters.Add "UF", V_UF, 1
29            oradatabase.Parameters.Remove "DT_IE"
30            oradatabase.Parameters.Add "DT_IE", V_DT_CAD, 1
31            oradatabase.Parameters.Remove "IE"
32            oradatabase.Parameters.Add "IE", V_IE, 1
33            oradatabase.Parameters.Remove "IE_VALIDA"
34            oradatabase.Parameters.Add "IE_VALIDA", 0, 3
35            oradatabase.ExecuteSQL S_SQL
36            vRet = oradatabase.Parameters("IE_VALIDA")
              'Se fun��o retornar:
              '0 = IE ok
              '1 = IE n�o ok
37            If vRet = 0 Then
38              ValidaInscricaoEstadual = True
39            ElseIf vRet = 1 Then
40              ValidaInscricaoEstadual = False
41            End If
42        End If
 
Trata_Erro:
43        If Err.Number <> 0 Then
44            MsgBox "Sub: VALIDA_IE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, vbCritical, "CTB050 - GNRE SA�DA"
45            End
46        End If
 
End Function

Public Function fCharEspeciais()

1         On Error GoTo Trata_Erro
2         oradatabase.Parameters.Remove "PM_CD"
4         oradatabase.Parameters.Remove "PM_CARACTERES"
          
5         oradatabase.Parameters.Add "PM_CD", lngCD, 1
7         oradatabase.Parameters.Add "PM_CARACTERES", "", 2
          
8         oradatabase.ExecuteSQL "BEGIN PRODUCAO.PR_SELECT_CARACT_ESP_NFE(:PM_CD, :PM_CARACTERES);END;"
          
9         fCharEspeciais = oradatabase.Parameters("PM_CARACTERES").Value
       
Trata_Erro:
10        If Err.Number <> 0 Then
11            MsgBox "Function: fCharEspeciais" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl, vbCritical, "CTB050 - GNRE SA�DA"
12        End If
End Function
