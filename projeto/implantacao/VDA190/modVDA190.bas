Attribute VB_Name = "modVDA190"
Option Explicit

Public Sub Criar_Cursor(Banco As Object, pNomeCursor As String)

    Banco.Remove pNomeCursor
    Banco.Add pNomeCursor, 0, 3
    Banco(pNomeCursor).serverType = 102
    Banco(pNomeCursor).DynasetOption = &H2&
    Banco(pNomeCursor).DynasetCacheParams 256, 16, 20, 2000, 0

End Sub
