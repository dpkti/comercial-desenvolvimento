VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{039BFB0A-D5B6-442B-81E5-700DDB9AA78E}#1.3#0"; "OPTION.OCX"
Begin VB.Form frmManutencao 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: MANUTEN��O :::"
   ClientHeight    =   5235
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7575
   ControlBox      =   0   'False
   Icon            =   "frmManutencao.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   349
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   505
   StartUpPosition =   1  'CenterOwner
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   7485
      _ExtentX        =   13203
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   6840
      TabIndex        =   0
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManutencao.frx":23D2
      PICN            =   "frmManutencao.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDeletar 
      Height          =   690
      Left            =   765
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Excluir Transportadora Selecionada"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManutencao.frx":30C8
      PICN            =   "frmManutencao.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdNovaTransp 
      Height          =   690
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Incluir Nova Transportadora"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmManutencao.frx":39BE
      PICN            =   "frmManutencao.frx":39DA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid mfgManutencao 
      Height          =   2850
      Left            =   45
      TabIndex        =   1
      Top             =   2340
      Visible         =   0   'False
      Width           =   7485
      _ExtentX        =   13203
      _ExtentY        =   5027
      _Version        =   393216
      BackColorBkg    =   -2147483633
      HighLight       =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin CoolXPFrame.xpFrame fra 
      Height          =   1455
      Left            =   45
      Top             =   810
      Width           =   7485
      _ExtentX        =   13203
      _ExtentY        =   2566
      Caption         =   "Consulta"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin Bot�o.cmd cmdPesquisar 
         Height          =   690
         Left            =   6705
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Pesquisar"
         Top             =   675
         Width           =   690
         _ExtentX        =   1217
         _ExtentY        =   1217
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmManutencao.frx":46B4
         PICN            =   "frmManutencao.frx":46D0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin CoolXPOption.xpOption optBloqueadas 
         Height          =   240
         Left            =   45
         TabIndex        =   9
         Top             =   1125
         Width           =   2490
         _ExtentX        =   4392
         _ExtentY        =   423
         Caption         =   "Transportadora bloqueadas"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Value           =   -1  'True
         ShowFocusRect   =   -1  'True
      End
      Begin CoolXPOption.xpOption optTransp 
         Height          =   240
         Left            =   45
         TabIndex        =   8
         Top             =   720
         Width           =   1545
         _ExtentX        =   2725
         _ExtentY        =   423
         Caption         =   "Transportadora"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowFocusRect   =   -1  'True
      End
      Begin VB.ComboBox cmbUF 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   585
         TabIndex        =   6
         Top             =   270
         Width           =   2895
      End
      Begin CoolXPOption.xpOption optUF 
         Height          =   240
         Left            =   45
         TabIndex        =   7
         Top             =   315
         Width           =   510
         _ExtentX        =   900
         _ExtentY        =   423
         Caption         =   "UF"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ShowFocusRect   =   -1  'True
      End
      Begin VB.ComboBox cmbTransp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1620
         TabIndex        =   5
         Top             =   675
         Width           =   4605
      End
   End
   Begin VB.Image imgBloqueado 
      Height          =   150
      Left            =   1530
      Picture         =   "frmManutencao.frx":53AA
      Top             =   45
      Visible         =   0   'False
      Width           =   165
   End
End
Attribute VB_Name = "frmManutencao"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vObjTransp As Object
Dim fl_bloq As String

Private Sub cmbTransp_KeyPress(KeyAscii As Integer)

    On Error GoTo TrataErro

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbTransp_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbTransp_KeyUp(KeyCode As Integer, Shift As Integer)

    On Error GoTo TrataErro

    If KeyCode <> 8 Then

        Dim vValorDigitado As String

        vValorDigitado = cmbTransp

        For i = 0 To cmbTransp.ListCount - 1

            If Mid(cmbTransp.List(i), 1, Len(vValorDigitado)) = cmbTransp Then

                cmbTransp.Text = cmbTransp.List(i)
                cmbTransp.SelStart = Len(vValorDigitado)
                cmbTransp.SelLength = Len(cmbTransp)
                Exit Sub

            End If
        Next
    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbTransp_KeyUp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbUF_KeyPress(KeyAscii As Integer)

    On Error GoTo TrataErro

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbUF_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbUF_KeyUp(KeyCode As Integer, Shift As Integer)

    On Error GoTo TrataErro

    If KeyCode <> 8 Then

        Dim vValorDigitado As String

        vValorDigitado = cmbUF

        For i = 0 To cmbUF.ListCount - 1

            If Mid(cmbUF.List(i), 1, Len(vValorDigitado)) = cmbUF Then

                cmbUF.Text = cmbUF.List(i)
                cmbUF.SelStart = Len(vValorDigitado)
                cmbUF.SelLength = Len(cmbUF)
                Exit Sub

            End If

        Next

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbUF_KeyUp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmdDeletar_Click()

    On Error GoTo TrataErro

    If mfgManutencao.Visible = False Then
        Exit Sub
    Else

        If vVB_Generica_001.Perguntar("Deseja excluir essa transportadora?") = 6 Then

            vBanco.Parameters.Remove "COD_UF"
            vBanco.Parameters.Add "COD_UF", mfgManutencao.TextMatrix(mfgManutencao.Row, 1), 1

            vBanco.Parameters.Remove "COD_TRANSP"
            vBanco.Parameters.Add "COD_TRANSP", mfgManutencao.TextMatrix(mfgManutencao.Row, 2), 1

            vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_DELETE_FRETE_UF_BLOQ(:COD_UF,:COD_TRANSP)")

            If vErro <> "" Then
                vVB_Generica_001.ProcessaErro vErro
            Else
                vVB_Generica_001.Informar ("Exclu�do com sucesso!")
                cmdPesquisar_Click
            End If
        Else
            Exit Sub
        End If
    End If
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdDeletar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmdNovaTransp_Click()

    On Error GoTo TrataErro

    frmInclus�o.Show , Me

TrataErro:
    
    If Err.Number <> 0 Then
        
        MsgBox "Sub: cmdNovaTransp_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    
    End If

End Sub
Private Sub cmdPesquisar_Click()

    On Error GoTo TrataErro

    mfgManutencao.Visible = False

    If optTransp.Value = False And optUF.Value = False And optBloqueadas.Value = False Then
        vVB_Generica_001.Informar ("Escolha um tipo de pesquisa!")
        Exit Sub
    End If

    Criar_Cursor vBanco.Parameters, "vCursor"

    If optUF.Value Then

        vBanco.Parameters.Remove "COD_UF"
        vBanco.Parameters.Add "COD_UF", Mid(Trim(cmbUF), 1, 2), 1

        vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_SELECT_POR_UF(:VCURSOR,:COD_UF)")

        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro vErro
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If

    End If

    If optTransp.Value Then

        vBanco.Parameters.Remove "COD_TRANSP"
        vBanco.Parameters.Add "COD_TRANSP", IIf(Trim(Mid(cmbTransp, 36, 9)) = "", 0, Trim(Mid(cmbTransp, 36, 9))), 1

        vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_SELECT_POR_TRANSP(:VCURSOR,:COD_TRANSP)")

        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro vErro
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If

    End If

    If optBloqueadas.Value Then

        vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_SELECT_BLOQUEADAS(:VCURSOR)")

        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro vErro
        Else
            Set vObjOracle = vBanco.Parameters("VCURSOR").Value
        End If

    End If

    If vObjOracle.EOF Then
        vVB_Generica_001.Informar ("N�o encontrei dados para essa pesquisa!")
        Exit Sub
    Else

        Call vVB_Generica_001.CarregaGridTabela(mfgManutencao, vObjOracle, vObjOracle.Fields.Count + 1)

        For i = 1 To vObjOracle.RecordCount
            If mfgManutencao.TextMatrix(i, 4) = "N" Then
                mfgManutencao.Row = i
                mfgManutencao.Col = 4
                Set mfgManutencao.CellPicture = LoadPicture()
                mfgManutencao.TextMatrix(i, 4) = " "
            Else
                mfgManutencao.TextMatrix(i, 4) = ""
                mfgManutencao.Row = i
                mfgManutencao.Col = 4
                Set mfgManutencao.CellPicture = imgBloqueado.Picture
                mfgManutencao.CellPictureAlignment = 4
            End If
        Next

        mfgManutencao.ColAlignment(1) = 1
        mfgManutencao.ColAlignment(2) = 7
        mfgManutencao.ColAlignment(3) = 1
        mfgManutencao.ColAlignment(4) = 1

        mfgManutencao.FixedAlignment(1) = 4
        mfgManutencao.FixedAlignment(2) = 4
        mfgManutencao.FixedAlignment(3) = 4
        mfgManutencao.FixedAlignment(4) = 4

        mfgManutencao.Visible = True
        cmdDeletar.Enabled = True

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdPesquisar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmdVoltar_Click()

    On Error GoTo TrataErro

    Unload Me

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdVoltar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub Form_Load()

    On Error GoTo TrataErro

    Me.Top = 0
    Me.Left = 0

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Form_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub mfgManutencao_DblClick()

    On Error GoTo TrataErro

    vBanco.Parameters.Remove "COD_UF"
    vBanco.Parameters.Add "COD_UF", mfgManutencao.TextMatrix(mfgManutencao.Row, 1), 1

    vBanco.Parameters.Remove "COD_TRANSP"
    vBanco.Parameters.Add "COD_TRANSP", mfgManutencao.TextMatrix(mfgManutencao.Row, 2), 1

    If mfgManutencao.TextMatrix(mfgManutencao.Row, 4) = " " Then
        vBanco.Parameters.Remove "FL_BLOQ"
        vBanco.Parameters.Add "FL_BLOQ", "S", 1

        If vVB_Generica_001.Perguntar("Confirma bloqueio?") = 6 Then
            UPDATE_FLAG
        Else
            Exit Sub
        End If

    Else
        vBanco.Parameters.Remove "FL_BLOQ"
        vBanco.Parameters.Add "FL_BLOQ", "N", 1

        If vVB_Generica_001.Perguntar("Confirma desbloqueio?") = 6 Then
            UPDATE_FLAG
        Else
            Exit Sub
        End If

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: mfgManutencao_DblClick" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Sub UPDATE_FLAG()

    On Error GoTo TrataErro

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_UPDATE_FRETE_UF_BLOQ(:COD_UF,:COD_TRANSP,:FL_BLOQ)")

    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro vErro
    Else
        If mfgManutencao.TextMatrix(mfgManutencao.Row, 4) = " " Then
            mfgManutencao.Row = mfgManutencao.Row
            mfgManutencao.Col = 4
            Set mfgManutencao.CellPicture = imgBloqueado.Picture
            mfgManutencao.CellPictureAlignment = 4
            mfgManutencao.TextMatrix(mfgManutencao.Row, 4) = ""
        Else
            mfgManutencao.Row = mfgManutencao.Row
            mfgManutencao.Col = 4
            Set mfgManutencao.CellPicture = LoadPicture()
            mfgManutencao.TextMatrix(mfgManutencao.Row, 4) = " "
        End If
    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: UPDATE_FLAG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub


Private Sub optBloqueadas_Click()

    On Error GoTo TrataErro

    cmbTransp.Enabled = False
    cmbUF.Enabled = False

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: optBloqueadas_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub optTransp_Click()

    On Error GoTo TrataErro

    cmbTransp.Enabled = True
    cmbUF.Enabled = False

    Criar_Cursor vBanco.Parameters, "vCursor"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_SELECT_TRANSPORTADORA(:VCURSOR)")

    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro vErro
    Else
        Set vObjTransp = vBanco.Parameters("VCURSOR").Value

        Call vVB_Generica_001.PreencheComboList(vObjTransp, cmbTransp, "TRANSPORTADORA")

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: optTransp_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub optUF_Click()

    On Error GoTo TrataErro

    cmbTransp.Enabled = False
    cmbUF.Enabled = True

    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF, vObjOracle.Fields(0).Name, vObjOracle.Fields(1).Name)

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: optUF_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Function VERIFICA_CAMPOS_CONSULTA() As Boolean

    On Error GoTo TrataErro

    If cmbTransp = "" Then
        vVB_Generica_001.Informar ("Escolha uma transportadora!")
        cmbTransp.SetFocus
        VERIFICA_CAMPOS_CONSULTA = False
        Exit Function
    End If

    If cmbUF = "" Then
        vVB_Generica_001.Informar ("Escolha uma UF!")
        cmbUF.SetFocus
        VERIFICA_CAMPOS_CONSULTA = False
        Exit Function
    End If

    If optTransp.Value = False And optUF.Value = False And optBloqueadas.Value = False Then
        vVB_Generica_001.Informar ("Voc� precisa informar a flag de bloqueio!")
        VERIFICA_CAMPOS_CONSULTA = False
        Exit Function
    End If

    VERIFICA_CAMPOS_CONSULTA = True

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: VERIFICA_CAMPOS_CONSULTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Function
