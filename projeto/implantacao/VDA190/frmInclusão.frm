VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFrame.ocx"
Object = "{039BFB0A-D5B6-442B-81E5-700DDB9AA78E}#1.3#0"; "OPTION.OCX"
Begin VB.Form frmInclus�o 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "::: INCLUS�O DE RELACIONAMENTOS :::"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6135
   ControlBox      =   0   'False
   Icon            =   "frmInclus�o.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   163
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   409
   StartUpPosition =   1  'CenterOwner
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   810
      Width           =   6045
      _ExtentX        =   10663
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   5400
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmInclus�o.frx":23D2
      PICN            =   "frmInclus�o.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmInclus�o.frx":30C8
      PICN            =   "frmInclus�o.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fra 
      Height          =   1635
      Left            =   45
      Top             =   765
      Width           =   6045
      _ExtentX        =   10663
      _ExtentY        =   2884
      Caption         =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BorderColor     =   8421504
      ColorStyle      =   99
      UseCoolToolTip  =   0   'False
      Begin VB.ComboBox cmbTransp 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1305
         TabIndex        =   4
         Top             =   630
         Width           =   4650
      End
      Begin VB.ComboBox cmbUF 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   360
         TabIndex        =   3
         Top             =   225
         Width           =   2985
      End
      Begin CoolXPFrame.xpFrame xpFrame1 
         Height          =   600
         Left            =   90
         Top             =   945
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   1058
         Caption         =   "Flag de bloqueio"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   13977088
         BorderColor     =   8421504
         ColorStyle      =   99
         UseCoolToolTip  =   0   'False
         Begin CoolXPOption.xpOption optNao 
            Height          =   240
            Left            =   1080
            TabIndex        =   8
            Top             =   270
            Width           =   600
            _ExtentX        =   1058
            _ExtentY        =   423
            Caption         =   "N�o"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ShowFocusRect   =   -1  'True
         End
         Begin CoolXPOption.xpOption optSim 
            Height          =   240
            Left            =   45
            TabIndex        =   7
            Top             =   270
            Width           =   555
            _ExtentX        =   979
            _ExtentY        =   423
            Caption         =   "Sim"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Value           =   -1  'True
            ShowFocusRect   =   -1  'True
         End
      End
      Begin VB.Label lblTransp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Tranportadora"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   6
         Top             =   675
         Width           =   1170
      End
      Begin VB.Label lblUF 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "UF"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   5
         Top             =   270
         Width           =   210
      End
   End
End
Attribute VB_Name = "frmInclus�o"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vObjTransp As Object

Private Sub cmbTransp_KeyPress(KeyAscii As Integer)

    On Error GoTo TrataErro

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbTransp_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbTransp_KeyUp(KeyCode As Integer, Shift As Integer)

    On Error GoTo TrataErro

    If KeyCode <> 8 Then

        Dim vValorDigitado As String

        vValorDigitado = cmbTransp

        For i = 0 To cmbTransp.ListCount - 1

            If Mid(cmbTransp.List(i), 1, Len(vValorDigitado)) = cmbTransp Then

                cmbTransp.Text = cmbTransp.List(i)
                cmbTransp.SelStart = Len(vValorDigitado)
                cmbTransp.SelLength = Len(cmbTransp)
                Exit Sub

            End If

        Next

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbTransp_KeyUp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbUF_KeyPress(KeyAscii As Integer)

    On Error GoTo TrataErro

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbUF_KeyPress" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmbUF_KeyUp(KeyCode As Integer, Shift As Integer)

    On Error GoTo TrataErro

    If KeyCode <> 8 Then

        Dim vValorDigitado As String

        vValorDigitado = cmbUF

        For i = 0 To cmbUF.ListCount - 1

            If Mid(cmbUF.List(i), 1, Len(vValorDigitado)) = cmbUF Then

                cmbUF.Text = cmbUF.List(i)
                cmbUF.SelStart = Len(vValorDigitado)
                cmbUF.SelLength = Len(cmbUF)
                Exit Sub

            End If

        Next

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmbUF_KeyUp" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmdGravar_Click()

    On Error GoTo TrataErro

    If VERIFICA_CAMPOS = True Then

        vBanco.Parameters.Remove "COD_UF"
        vBanco.Parameters.Add "COD_UF", Mid(Trim(cmbUF), 1, 2), 1

        vBanco.Parameters.Remove "COD_TRANSP"
        vBanco.Parameters.Add "COD_TRANSP", IIf(Trim(Mid(cmbTransp, 36, 9)) = "", 0, Trim(Mid(cmbTransp, 36, 9))), 1

        If optNao.Value Then

            vBanco.Parameters.Remove "FL_BLOQ"
            vBanco.Parameters.Add "FL_BLOQ", "N", 1

        ElseIf optSim.Value Then

            vBanco.Parameters.Remove "FL_BLOQ"
            vBanco.Parameters.Add "FL_BLOQ", "S", 1

        End If

        vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_INSERT_FRETE_UF_BLOQ(:COD_UF,:COD_TRANSP,:FL_BLOQ)")

        If vErro <> "" Then
            vVB_Generica_001.ProcessaErro vErro
        Else
            vVB_Generica_001.Informar ("Inclu�do com sucesso!")
            Unload Me
        End If
    Else
        Exit Sub
    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdGravar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Private Sub cmdVoltar_Click()

    On Error GoTo TrataErro

    Unload Me


TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: cmdVoltar_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub
Private Sub Form_Load()

    On Error GoTo TrataErro

    Me.Top = 0
    Me.Left = 0

    'COMBO UF
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF, vObjOracle.Fields(0).Name, vObjOracle.Fields(1).Name)

    'COMBO TRANSPORTADORA
    Criar_Cursor vBanco.Parameters, "vCursor"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, "FRETE.PCK_VDA190.PR_SELECT_TRANSPORTADORA(:VCURSOR)")

    If vErro <> "" Then
        vVB_Generica_001.ProcessaErro vErro
    Else
        Set vObjTransp = vBanco.Parameters("VCURSOR").Value

        Call vVB_Generica_001.PreencheComboList(vObjTransp, cmbTransp, "TRANSPORTADORA")

    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: Form_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Sub

Function VERIFICA_CAMPOS() As Boolean

    On Error GoTo TrataErro

    If cmbTransp = "" Then
        vVB_Generica_001.Informar ("Escolha uma transportadora!")
        cmbTransp.SetFocus
        VERIFICA_CAMPOS = False
        Exit Function
    End If

    If cmbUF = "" Then
        vVB_Generica_001.Informar ("Escolha uma UF!")
        cmbUF.SetFocus
        VERIFICA_CAMPOS = False
        Exit Function
    End If

    If optSim.Value = False And optNao.Value = False Then
        vVB_Generica_001.Informar ("Voc� precisa informar a flag de bloqueio!")
        VERIFICA_CAMPOS = False
        Exit Function
    End If

    VERIFICA_CAMPOS = True

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub: VERIFICA_CAMPOS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If

End Function
