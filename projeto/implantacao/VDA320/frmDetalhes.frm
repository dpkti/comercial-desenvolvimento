VERSION 5.00
Begin VB.Form frmDetalhes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Dados do cliente"
   ClientHeight    =   1905
   ClientLeft      =   2190
   ClientTop       =   2550
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1905
   ScaleWidth      =   6015
   Begin VB.Label lblMsg 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   960
      TabIndex        =   8
      Top             =   1440
      Width           =   4815
   End
   Begin VB.Label Label3 
      Caption         =   "MSG"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   7
      Top             =   1440
      Width           =   495
   End
   Begin VB.Label lblContato 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   6
      Top             =   960
      Width           =   2895
   End
   Begin VB.Label Label2 
      Caption         =   "CONTATO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   960
      Width           =   975
   End
   Begin VB.Label lblFone2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2280
      TabIndex        =   4
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label lblDDD2 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   3
      Top             =   600
      Width           =   735
   End
   Begin VB.Label lblFone1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2280
      TabIndex        =   2
      Top             =   360
      Width           =   1215
   End
   Begin VB.Label lblDDD1 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "FONE"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   735
   End
End
Attribute VB_Name = "frmDetalhes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Unload(Cancel As Integer)

    lblDDD1 = ""
    lblFone1 = ""
    lblDDD2 = ""
    lblFone2 = ""
    lblContato = ""
    lblMsg = ""

End Sub


