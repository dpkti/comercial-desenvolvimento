VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmConsulta_Recados 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recados"
   ClientHeight    =   4710
   ClientLeft      =   1140
   ClientTop       =   1545
   ClientWidth     =   11130
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   11130
   Begin MSGrid.Grid grdRecados 
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   10935
      _Version        =   65536
      _ExtentX        =   19288
      _ExtentY        =   7223
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "frmConsulta_Recados.frx":0000
   End
   Begin VB.Label lblMensagem 
      AutoSize        =   -1  'True
      Caption         =   "Para excluir um recado: ""duplo click"""
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   480
      TabIndex        =   1
      Top             =   120
      Width           =   3180
   End
End
Attribute VB_Name = "frmConsulta_Recados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub grdrep_RowColChange()

End Sub

Private Sub grdRepresentante_RowColChange()

End Sub

Private Sub grdRecados_DblClick()
    sql = "Begin producao.pck_vda230.pr_deleta_recado(:vend,:dt_recado,:vErro);END;"

    OraParameters.Remove "vend"
    OraParameters.Add "vend", sCOD_VEND, 1
    grdRecados.Col = 0
    OraParameters.Remove "dt_recado"
    OraParameters.Add "dt_recado", CDate(grdRecados.Text), 1

    oradatabase.ExecuteSQL sql

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Resume Next
    End If
    Unload Me
End Sub

