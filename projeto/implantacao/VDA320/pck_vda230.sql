CREATE OR REPLACE PACKAGE PCK_VDA230
--------------------------------------------
--- SISTEMA	: TELEMARKETING
--- ANALISTA    : MARICI 	
--- DATA	: 19/11/2002

IS
 -- variavel tipo cursor
 TYPE tp_cursor IS REF CURSOR;

 PROCEDURE PR_GERA_PED(p_cursor      IN OUT tp_cursor,
 		       p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
 		       p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                       p_erro        OUT    NUMBER);
                       
 PROCEDURE PR_GERA_ITPED(p_cursor      IN OUT tp_cursor,
  		         p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		         p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                         p_erro        OUT    NUMBER);
                         
 PROCEDURE PR_COT_CAD(p_cursor      IN OUT tp_cursor,
 		      p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
 		      p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                      p_erro        OUT    NUMBER);

 PROCEDURE PR_COTACAO_VEND(p_cursor      IN OUT tp_cursor,
  		           p_cod_vend    IN     representante.cod_repres%TYPE,
                           p_erro        OUT    NUMBER);
                           
 PROCEDURE PR_COTACAO_PED(p_cursor      IN OUT tp_cursor,
 		          p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
 		          p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                          p_erro        OUT    NUMBER);
 

 PROCEDURE PR_COTACAO_ITPED(p_cursor      IN OUT tp_cursor,
  		            p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		            p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                            p_erro        OUT    NUMBER);
                            
 PROCEDURE PR_ITEM_CADASTRO(p_cursor      IN OUT tp_cursor,
   		            p_cod_dpk     IN     item_cadastro.cod_dpk%TYPE,
   		            p_erro        OUT    NUMBER);

 PROCEDURE PR_CESTA_ITEM(p_cursor      IN OUT tp_cursor,
   		         p_cod_loja    IN     loja.cod_loja%TYPE,
   		         p_tp_cesta    IN     cesta_item.tp_cesta%TYPE,
   		         p_erro        OUT    NUMBER);

 PROCEDURE PR_EQUIV(p_cursor      IN OUT tp_cursor,
    		    p_cod_dpk     IN     item_cadastro.cod_dpk%TYPE,
    		    p_erro        OUT    NUMBER);

 PROCEDURE PR_SEQ_FRANQUIA(p_cursor      IN OUT tp_cursor,
   		           p_cod_repres  IN     representante.cod_repres%TYPE,
                           p_erro        OUT    NUMBER);
 
 PROCEDURE PR_REPRES(p_cursor      IN OUT tp_cursor,
    		     p_erro        OUT    NUMBER);
 
 PROCEDURE PR_TRANSP_CIF(p_cursor      IN OUT tp_cursor,
 			 p_cod_uf      IN     uf.cod_uf%TYPE,
    		         p_erro        OUT    NUMBER);

 PROCEDURE PR_VALIDA_TP_FRETE(p_cursor      IN OUT tp_cursor,
  			      p_cod_transp  IN     transportadora.cod_transp%TYPE,
  			      p_cod_loja    IN     loja.cod_loja%TYPE,
  			      p_cod_cliente IN     cliente.cod_cliente%TYPE,
  			      p_erro        OUT    NUMBER);

 PROCEDURE PR_ICMS_RETIDO(p_cod_loja       IN     loja.cod_loja%TYPE,
                          p_cod_cliente    IN     cliente.cod_cliente%TYPE,
                          p_cod_dpk        IN     item_cadastro.cod_dpk%TYPE,
                          p_cod_trib       IN     item_cadastro.cod_tributacao%TYPE,
                          p_preco_liquido  IN     itpednota_venda.preco_unitario%TYPE,
                          p_vl_icmretido   OUT    itpednota_venda.vl_icmretido%TYPE,
     		          p_erro           OUT    NUMBER);

 PROCEDURE PR_DETALHES_CLIENTE(p_cursor      IN OUT tp_cursor,
   		               p_cod_cliente IN     cliente.cod_cliente%TYPE,
                               p_erro        OUT    NUMBER);
 
 PROCEDURE PR_COTACAO_CABECALHO(p_cursor      IN OUT tp_cursor,
 		      		p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
 		      		p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                      		p_erro        OUT    NUMBER);

 PROCEDURE PR_COTACAO_ITEM(p_cursor      IN OUT tp_cursor,
  		      	   p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		      	   p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                      	   p_erro        OUT    NUMBER);

 PROCEDURE PR_DADOS_LOJA(p_cursor      IN OUT tp_cursor,
  		    	 p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		         p_erro        OUT    NUMBER);

PROCEDURE PR_GETITEM(p_cursor       IN OUT tp_cursor,
   		     p_cod_loja     IN     loja.cod_loja%TYPE,
   		     p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
   		     p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
   		     p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
   		     p_erro         OUT    NUMBER);
   		     
PROCEDURE PR_ITENS_NOVOS(p_cursor      IN OUT tp_cursor,
  		    	 p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		         p_erro        OUT    NUMBER);

PROCEDURE PR_KIT_VEICULO(p_cursor      IN OUT tp_cursor,
  		    	 p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		    	 p_veiculo     IN     VARCHAR,
  		         p_erro        OUT    NUMBER);


END PCK_VDA230;
/

CREATE OR REPLACE PACKAGE BODY PCK_VDA230 IS
  
  
 PROCEDURE PR_GERA_PED(p_cursor      IN OUT tp_cursor,
  		       p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		       p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                       p_erro        OUT    NUMBER)
  IS
    
  BEGIN
  
    p_erro := 0;
    
    --Abrindo Cursor
    OPEN p_cursor FOR select cot.DT_COTACAO,cot.DT_VALIDADE,cot.COD_CLIENTE,cli.NOME_CLIENTE,
			nvl(cot.COD_VEND,0) cod_vend,nvl(vend.PSEUDONIMO,' ') pseudonimo,
			cot.COD_PLANO,pla.DESC_PLANO,
			to_char(cot.cod_loja,'09') || '-' || loja.nome_fantasia deposito,
			nvl(cot.pc_desc_suframa,0) pc_desc_suframa,
  		        cot.COD_REPRES,cot.PC_ACRESCIMO,cot.PC_DESCONTO,cot.FL_DIF_ICM,
  		        cot.OBSERV
			from vendas.COTACAO cot, CLIENTE cli, PLANO_PGTO pla,
			REPRESENTANTE vend, loja  
			where cot.COD_CLIENTE = cli.COD_CLIENTE and
			 cot.COD_PLANO = pla.COD_PLANO and 
			 cot.COD_VEND = vend.COD_REPRES(+) and
			 cot.cod_loja=loja.cod_loja and
			 cot.NUM_COTACAO = p_num_cotacao and
			 cot.COD_LOJA = p_cod_loja;

    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;

  
  END;
  
  PROCEDURE PR_GERA_ITPED(p_cursor      IN OUT tp_cursor,
    		         p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
    		         p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                         p_erro        OUT    NUMBER)
 
   IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select itcot.NUM_ITEM_COTACAO,it.COD_FORNECEDOR,it.COD_FABRICA,
			itcot.QTD_SOLICITADA,itcot.TABELA_VENDA,itcot.PRECO_UNITARIO,
			itcot.PC_DESC1,itcot.PC_DESC2,itcot.PC_DESC3,itcot.PC_DIFICM
			 from vendas.ITEM_COTACAO itcot,ITEM_CADASTRO it
			 where itcot.COD_DPK = it.COD_DPK and
			 itcot.NUM_COTACAO = p_num_cotacao and
			  itcot.COD_LOJA = p_cod_loja
			order by num_item_cotacao;

      
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;
  
   PROCEDURE PR_COT_CAD(p_cursor      IN OUT tp_cursor,
   		      p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
   		      p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                      p_erro        OUT    NUMBER)
 
   
      IS
         
       BEGIN
       
         p_erro := 0;
         
         --Abrindo Cursor
         OPEN p_cursor FOR select it.COD_TRIBUTACAO,it.COD_TRIBUTACAO_IPI,
			It.COD_DPK,it.COD_FORNECEDOR,it.COD_FABRICA,
			itcot.PRECO_UNITARIO, it.peso 
			from ITEM_CADASTRO it, vendas.ITEM_COTACAO itcot 
			where itcot.COD_DPK = it.COD_DPK and 
			itcot.NUM_COTACAO = p_num_cotacao and 
			itcot.COD_LOJA = p_cod_loja 
			order by NUM_ITEM_COTACAO;
         
         
         EXCEPTION
         WHEN OTHERS THEN
         p_erro := SQLCODE;
     
       
  END;
  
  PROCEDURE PR_COTACAO_VEND(p_cursor      IN OUT tp_cursor,
    		            p_cod_vend    IN     representante.cod_repres%TYPE,
                            p_erro        OUT    NUMBER)
 
  
  IS
           
         BEGIN
         
           p_erro := 0;
           
           --Abrindo Cursor
           OPEN p_cursor FOR select NUM_COTACAO,
			COD_LOJA, COD_CLIENTE, COD_REPRES
			from VENDAS.COTACAO 
			where SITUACAO=8 and cod_vend = p_cod_vend
			ORDER BY NUM_COTACAO;
           
           
           
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
       
         
  END;
 
 PROCEDURE PR_COTACAO_PED(p_cursor      IN OUT tp_cursor,
  		          p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		          p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                          p_erro        OUT    NUMBER)
IS
           
         BEGIN
         
           p_erro := 0;
           
           --Abrindo Cursor
           OPEN p_cursor FOR select cot.DT_COTACAO,cot.DT_VALIDADE,cot.COD_CLIENTE,
		cli.NOME_CLIENTE, to_char(cot.cod_loja,'09') || '-' || loja.nome_fantasia deposito, 
		nvl(cot.COD_VEND,0) cod_vend, cot.COD_REPRES, 
		nvl(vend.PSEUDONIMO,' ') pseudonimo,cot.COD_PLANO,
		pla.DESC_PLANO, cot.PC_ACRESCIMO,cot.PC_DESCONTO,
		cot.FL_DIF_ICM,cot.OBSERV, nvl(cot.pc_desc_suframa,0) pc_desc_suframa, 
		cid.COD_UF,cid.NOME_CIDADE,cli.INSCR_ESTADUAL,
		  nvl(cli.INSCR_SUFRAMA,' ') INSCR_SUFRAMA, 
		FL_CONS_FINAL, nvl(cli.COD_TARE,0) COD_TARE
		 from vendas.COTACAO cot, CLIENTE cli, PLANO_PGTO pla,
		REPRESENTANTE vend, CIDADE cid, loja 
		 where cot.COD_CLIENTE = cli.COD_CLIENTE and
		  cot.COD_PLANO = pla.COD_PLANO and 
		 cot.COD_VEND = vend.COD_REPRES(+) and
		  cot.cod_loja=loja.cod_loja and 
		cli.COD_CIDADE = cid.COD_CIDADE and 
		 cot.NUM_COTACAO = p_num_cotacao and 
		cot.cod_loja=p_cod_loja;

           
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
       
         
  END;
 
 PROCEDURE PR_COTACAO_ITPED(p_cursor      IN OUT tp_cursor,
   		            p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
   		            p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                            p_erro        OUT    NUMBER)
                            
 IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR select itcot.NUM_ITEM_COTACAO,itcot.PRECO_UNITARIO, 
		nvl(itcot.PC_DESC1,0) pc_desc1, nvl(itcot.PC_DESC2,0) pc_desc2,
		 nvl(itcot.PC_DESC3,0) pc_desc3 , nvl(itcot.PC_DIFICM,0) pc_dificm,
		 itcot.QTD_SOLICITADA,it.COD_TRIBUTACAO,itcot.COD_DPK 
		from vendas.ITEM_COTACAO itcot, ITEM_CADASTRO it 
		where itcot.cod_dpk = it.cod_dpk and 
		itcot.NUM_COTACAO = p_num_cotacao and 
		itcot.cod_loja = p_cod_loja;

 
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;


 PROCEDURE PR_ITEM_CADASTRO(p_cursor      IN OUT tp_cursor,
   		            p_cod_dpk     IN     item_cadastro.cod_dpk%TYPE,
   		            p_erro        OUT    NUMBER)
   		            
  IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR Select cod_fornecedor, cod_grupo,
            	cod_subgrupo,class_fiscal
            	From item_cadastro 
            	Where cod_dpk = p_cod_dpk;

 
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;
 
 PROCEDURE PR_CESTA_ITEM(p_cursor      IN OUT tp_cursor,
    		         p_cod_loja    IN     loja.cod_loja%TYPE,
    		         p_tp_cesta    IN     cesta_item.tp_cesta%TYPE,
   		         p_erro        OUT    NUMBER)
  IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR Select b.cod_fornecedor,c.sigla, b.cod_fabrica,
            	b.desc_item,a.cod_dpk 
            	From cesta_item a, item_cadastro b, 
            	fornecedor c 
            	Where a.tp_cesta = p_tp_cesta and
            	a.cod_loja = p_cod_loja and 
            	b.cod_fornecedor = c.cod_fornecedor and
            	a.cod_dpk = b.cod_dpk  
            	Order by b.cod_fornecedor,b.cod_fabrica;

            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;  		         
   		         
 PROCEDURE PR_EQUIV(p_cursor      IN OUT tp_cursor,
     		    p_cod_dpk     IN     item_cadastro.cod_dpk%TYPE,
    		    p_erro        OUT    NUMBER)
 IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR select b.COD_FORNECEDOR, b.COD_FABRICA,
            	a.COD_DPK_EQ dpk 
            	from r_dpk_equiv a, item_cadastro b 
            	where    a.cod_dpk = p_cod_dpk and
            	a.cod_dpk_eq=b.cod_dpk 
            	Group by b.cod_fornecedor,b.cod_fabrica,a.cod_dpk_eq
            	Order by b.cod_fornecedor,b.cod_fabrica,a.cod_dpk_eq;

            
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;  		            		    
    		    
 PROCEDURE PR_SEQ_FRANQUIA(p_cursor      IN OUT tp_cursor,
    		           p_cod_repres  IN     representante.cod_repres%TYPE,
                           p_erro        OUT    NUMBER)
 IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR select prep.SEQ_FRANQUIA 
			from REPRESENTANTE prep,
			FILIAL fil 
			where fil.COD_FILIAL = prep.COD_FILIAL and 
			fil.COD_FRANQUEADOR = p_cod_repres and 
			prep.DIVISAO = 'D' and prep.SITUACAO = 0 and prep.SEQ_FRANQUIA > 0 
			order by prep.COD_REPRES;

            
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;  		            		                              
                           
  PROCEDURE PR_REPRES(p_cursor      IN OUT tp_cursor,
    		      p_erro        OUT    NUMBER)
 
   IS
            
          BEGIN
          
            p_erro := 0;
            
            --Abrindo Cursor
            OPEN p_cursor FOR select COD_REPRES,PSEUDONIMO,TIPO 
			from REPRESENTANTE 
			Where SITUACAO = 0 and DIVISAO = 'D' and TIPO in ('R','P') 
			and SEQ_FRANQUIA = 0
			order by PSEUDONIMO;
            
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
        
          
 END;  		 
 
 PROCEDURE PR_TRANSP_CIF(p_cursor      IN OUT tp_cursor,
  			 p_cod_uf      IN     uf.cod_uf%TYPE,
    		         p_erro        OUT    NUMBER)
    		         
   IS
             
           BEGIN
           
             p_erro := 0;
             
             --Abrindo Cursor
             OPEN p_cursor FOR select transp.COD_TRANSP,
			transp.NOME_TRANSP,
			transp.VIA_TRANSP,
			decode(frete.FL_BLOQUEIO,'S',' SIM ','     ') FL_LIBERADO 
			from TRANSPORTADORA transp,FRETE_UF_BLOQ frete 
			where transp.SITUACAO = 0 and 
			frete.COD_TRANSP = transp.COD_TRANSP and 
			frete.COD_UF = p_cod_uf
			order by transp.NOME_TRANSP;
             
             
             EXCEPTION
             WHEN OTHERS THEN
             p_erro := SQLCODE;
         
           
 END;  		 
 
 PROCEDURE PR_VALIDA_TP_FRETE(p_cursor      IN OUT tp_cursor,
   			      p_cod_transp  IN     transportadora.cod_transp%TYPE,
   			      p_cod_loja    IN     loja.cod_loja%TYPE,
   			      p_cod_cliente IN     cliente.cod_cliente%TYPE,
  			      p_erro        OUT    NUMBER)
   IS
              
            BEGIN
            
              p_erro := 0;
              
              --Abrindo Cursor
              OPEN p_cursor FOR Select tp_frete 
			From frete_entrega a,
			cidade c,
			cliente b
			Where a.situacao=0 and
			a.cod_cidade in (c.cod_cidade,0) and
			a.cod_subregiao in (c.cod_subregiao,0) and
			a.cod_regiao in (c.cod_regiao,0) and
			a.cod_uf = c.cod_uf and
			a.cod_transp = p_cod_transp and
			a.cod_loja = p_cod_loja and
			c.cod_cidade = b.cod_cidade and
			b.cod_cliente = p_cod_cliente
			Order by a.cod_cidade desc,
			     a.cod_subregiao desc,
			     a.cod_regiao desc;
    
              
              
              EXCEPTION
              WHEN OTHERS THEN
              p_erro := SQLCODE;
          
            
  END;  		 
 
  PROCEDURE PR_ICMS_RETIDO(p_cod_loja        IN     loja.cod_loja%TYPE,
                            p_cod_cliente    IN     cliente.cod_cliente%TYPE,
                            p_cod_dpk        IN     item_cadastro.cod_dpk%TYPE,
                            p_cod_trib       IN     item_cadastro.cod_tributacao%TYPE,
                            p_preco_liquido  IN     itpednota_venda.preco_unitario%TYPE,
                            p_vl_icmretido   OUT    itpednota_venda.vl_icmretido%TYPE,
     		            p_erro           OUT    NUMBER)
     		            
  IS
    w_cod_procedencia     item_cadastro.cod_procedencia%TYPE;
    w_cod_uf_origem       uf.cod_uf%TYPE;
    w_cod_uf_destino      uf.cod_uf%TYPE;
    w_inscr_estadual      cliente.inscr_estadual%TYPE;
    w_fl_cons_final       cliente.fl_cons_final%TYPE;
    w_tp_cliente          VARCHAR2(1) := 'R';
    w_vl_baseicm1         pednota_venda.vl_baseicm1%TYPE :=0;
    w_vl_baseicm2         pednota_venda.vl_baseicm2%TYPE :=0;
    w_vl_base_1           pednota_venda.vl_base_1%TYPE :=0;
    w_vl_basemaj_1        pednota_venda.vl_basemaj%TYPE :=0;
    w_vl_basemaj_3        pednota_venda.vl_basemaj%TYPE :=0;
    w_cliente_antec       VARCHAR2(1) :='N';
    w_cod_mensagem        cliente.cod_mensagem%TYPE;
    w_cod_mensagem_fiscal cliente.cod_mensagem_fiscal%TYPE;
    w_pc_mg_lucro_antec   compras.subst_tributaria.pc_margem_lucro%TYPE :=0;
    w_pc_margem_lucro     compras.subst_tributaria.pc_margem_lucro%TYPE :=0;
    w_class_antec         NUMBER(3) := 0;
    w_class_antec_RJ      NUMBER(3) := 0;
    w_class_fiscal        item_cadastro.class_fiscal%type;
    w_inscr_suframa       cliente.inscr_suframa%type :='0';
    w_vl_baseicm1_ret     NUMBER (17,5) := 0;
    w_vl_baseicm2_ret     NUMBER (17,5) := 0;
    w_vl_base_1_aux       NUMBER (17,5) := 0;
    w_vl_base_2           NUMBER (17,5) := 0;
    w_vl_base_2_aux       NUMBER (17,5) := 0; 
    w_vl_base_3           NUMBER (17,5) := 0;
    w_vl_baseisen         NUMBER (17,5) := 0;
    w_vl_baseisen_aux     NUMBER (17,5) := 0;
    w_vl_baseisen_aux1    NUMBER (17,5) := 0;
    w_vl_base_5           NUMBER (17,5) := 0;
    w_vl_base_6           NUMBER (17,5) := 0;
    w_vl_baseoutr         NUMBER (17,5) := 0;
    w_pc_aliq_interna     uf_origem_destino.pc_icm%TYPE;
    w_pc_aliq_interna_me  uf_origem_destino.pc_icm%TYPE;
    w_pc_icm              uf_origem_destino.pc_icm%TYPE;
    w_vl_icmretido        pednota_venda.vl_icmretido%TYPE := 0;
    
   CURSOR retido0 IS
     Select cod_procedencia, class_fiscal
       from item_cadastro
       where cod_dpk = p_cod_dpk;
   
   CURSOR retido1 IS
     select a.cod_uf
       from cidade a, loja b
       where a.cod_cidade = b.cod_cidade and
             b.cod_loja = p_cod_loja;
   
   CURSOR retido2 IS
        select a.cod_uf,b.inscr_estadual,b.fl_cons_final,
          b.cod_mensagem, nvl(b.cod_mensagem_fiscal,0) cod_mensagem_fiscal,
          nvl(b.inscr_suframa,'0') 
          from cidade a, cliente b
          where a.cod_cidade = b.cod_cidade and
             b.cod_cliente = p_cod_cliente;
       
   CURSOR retido3 IS    
       select pc_margem_lucro
       from antecipacao_tributaria a, cliente b, cidade c,
 	 loja d, cidade e
       where b.cod_cliente = p_cod_cliente and
	     d.cod_loja = p_cod_loja and
	     a.cod_uf_origem = e.cod_uf and
	     d.cod_cidade = e.cod_cidade and
	     a.cod_uf_destino = c.cod_uf and
	     b.cod_cidade = c.cod_cidade;    

   CURSOR retido4 IS
     select count(*)
       from compras.class_antec_entrada
       Where class_fiscal = w_class_fiscal and
             cod_uf_origem = w_cod_uf_origem and
	     cod_uf_destino = w_cod_uf_destino;
   
   CURSOR retido5 IS	
     select pc_margem_lucro
       from compras.subst_tributaria
       where cod_uf_origem = w_cod_uf_origem   and
             cod_uf_destino = decode(w_inscr_suframa,'0',w_cod_uf_destino,
                                    'SU') and
              class_fiscal = w_class_fiscal;
   
   CURSOR retido6 IS
     select pc_icm
       from uf_origem_destino
       where cod_uf_origem  = w_cod_uf_destino and
             cod_uf_destino = w_cod_uf_destino;
   
   CURSOR retido7 IS
     select a.pc_icm
       from aliquota_me a, cliente b
       where b.cod_cliente = p_cod_cliente and 
             a.cod_loja = p_cod_loja and
	     a.cgc = b.cgc;

   CURSOR retido8 IS
        select pc_icm
          from uf_origem_destino
          where cod_uf_origem  = w_cod_uf_origem and
             cod_uf_destino =decode(w_inscr_suframa,'0',w_cod_uf_destino,'SU');
   
   
  BEGIN
            
    p_erro := 0;
              
    OPEN retido0;
      FETCH retido0 INTO w_cod_procedencia,w_class_fiscal;
      IF retido0%NOTFOUND   THEN
         w_cod_procedencia :=0;
         w_class_fiscal := 0;
      END IF;
    CLOSE retido0;
  
    OPEN retido1;
       FETCH retido1 INTO w_cod_uf_origem;
    CLOSE retido1;
  
    OPEN retido2;
       FETCH retido2 INTO w_cod_uf_destino,w_inscr_estadual,w_fl_cons_final,
         w_cod_mensagem,w_cod_mensagem_fiscal,w_inscr_suframa;
    CLOSE retido2;
  
    IF w_inscr_estadual = 'ISENTO' THEN
      w_tp_cliente := 'I'; 
    ELSIF w_fl_cons_final = 'S' THEN
      w_tp_cliente := 'C';
    ELSE
      w_tp_cliente := 'R';
    END IF;
    
    IF w_tp_cliente = 'R' and w_cod_mensagem_fiscal <> 53 THEN
        OPEN retido3;
        FETCH retido3 INTO w_pc_mg_lucro_antec;
        IF retido3%NOTFOUND THEN
            w_pc_mg_lucro_antec := 0;
            w_cliente_antec := 'N';
        ELSE
            w_cliente_antec := 'S';
        END IF;
        CLOSE retido3;
     END IF;
     
     OPEN retido4;
  	FETCH retido4 INTO w_class_antec;
    	 IF retido4%NOTFOUND  THEN 
     	     w_class_antec := 0;
     	 END IF;
     CLOSE retido4;
     
     IF p_cod_trib in (1,3) THEN
       OPEN retido5;
         FETCH retido5 INTO w_pc_margem_lucro;
         IF retido5%NOTFOUND THEN
            w_pc_margem_lucro := 0;
         END IF;
       CLOSE retido5;
       IF w_fl_cons_final = 'S'  THEN
          w_pc_margem_lucro := 0;
       END IF;
     ELSIF (p_cod_trib = 0 or p_cod_trib = 8) and w_class_antec <> 0 and w_cliente_antec = 'S' THEN
        w_pc_margem_lucro := w_pc_mg_lucro_antec;
     END IF;
     
   
     OPEN retido6;
       FETCH retido6 INTO w_pc_aliq_interna;
     CLOSE retido6;
     
     
     IF w_cod_uf_origem = 'GO' and w_cod_uf_destino = 'GO' THEN
       OPEN retido7; 
         FETCH retido7 INTO w_pc_aliq_interna_me;
         IF retido7%NOTFOUND THEN
    	   w_pc_aliq_interna_me := 0;
         END IF;
       CLOSE retido7;	 
       IF w_pc_aliq_interna_me <> 0 THEN
          w_pc_aliq_interna := w_pc_aliq_interna_me;
       END IF;
      END IF;
     
     OPEN retido8; 
        FETCH retido8 INTO w_pc_icm;
        IF retido8%NOTFOUND THEN
      	   w_pc_icm := 0;
        END IF;
     CLOSE retido8;
  
    IF (p_cod_trib = 0  and w_cod_procedencia = 0) OR
       (p_cod_trib = 0 and ((w_cod_procedencia  = 1 OR
        w_cod_procedencia = 2) AND p_cod_loja <> 3))   THEN 
        
         w_vl_baseicm1 := p_preco_liquido;
              
       IF w_cliente_antec = 'S' and w_class_antec <> 0 THEN
          w_vl_basemaj_1 := p_preco_liquido * (1 + w_pc_margem_lucro / 100);
                 
          w_vl_baseicm1_ret := p_preco_liquido;                  
       END IF;
    ELSIF p_cod_trib = 1  THEN
        ---tratamento especial para goiania base reduzida 24/03/00
        IF (w_tp_cliente = 'R' and w_cod_uf_origem = 'GO' and
            w_cod_uf_destino = 'GO') THEN
            w_vl_base_1_aux := p_preco_liquido;
        ----reducao
            w_vl_baseisen_aux1 :=  w_vl_base_1_aux;
            w_vl_base_1 :=  (w_vl_base_1_aux * 10) / 17;
            w_vl_baseisen :=  (w_vl_base_1_aux - (w_vl_base_1_aux * 10) / 17);
        ELSE 
    	    w_vl_base_1 := p_preco_liquido;
        END IF;   
                 
        IF w_pc_margem_lucro = 0 and w_cod_uf_origem='RJ' and
           w_cod_uf_destino = 'RJ'  THEN
           w_pc_margem_lucro := 20;
        END IF;  
              
         w_vl_basemaj_1 := p_preco_liquido * (1 + w_pc_margem_lucro / 100);
     ELSIF p_cod_trib = 2  THEN
         w_vl_base_2_aux :=  p_preco_liquido;
         
         ---- reducao base colocado para tratar GOIANIA
         w_vl_baseisen_aux := w_vl_base_2_aux; 
         w_vl_base_2 :=  (w_vl_base_2_aux * 10) / 17;
         w_vl_baseisen := (w_vl_base_2_aux - (w_vl_base_2_aux * 10) /17); 
    
     ELSIF p_cod_trib = 3  THEN
         w_vl_base_3 := p_preco_liquido;
         w_vl_basemaj_3 := p_preco_liquido * (1 + w_pc_margem_lucro / 100);
     
     ELSIF p_cod_trib = 4  THEN
         w_vl_baseisen := p_preco_liquido;
         
     ELSIF p_cod_trib = 5  THEN
        w_vl_base_5 := p_preco_liquido;
     
     ELSIF p_cod_trib = 6  THEN
        w_vl_base_6 := p_preco_liquido;
     
     ELSIF (p_cod_trib = 8 OR ((w_cod_procedencia = 1 OR
            w_cod_procedencia = 2) AND p_cod_loja = 3)) THEN
        w_vl_baseicm2 := p_preco_liquido;
    	
    	IF w_cliente_antec = 'S' and w_class_antec <> 0 THEN
    	   w_vl_basemaj_1 := p_preco_liquido * (1 + w_pc_margem_lucro / 100);
           w_vl_baseicm2_ret := p_preco_liquido;             
       	END IF;			 
     ELSIF p_cod_trib = 9  THEN
        w_vl_baseoutr := p_preco_liquido;
     END IF;
  
     IF w_cliente_antec = 'N'  THEN   
          w_vl_icmretido := ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) -
                            (w_vl_base_1 * w_pc_icm /100))            +
                            (w_vl_basemaj_3 * w_pc_aliq_interna / 100);
     					  
     ELSE
          w_vl_icmretido := ((w_vl_basemaj_1 * w_pc_aliq_interna / 100) -
    	                    ((w_vl_baseicm1_ret * w_pc_icm / 100) +
     		            (w_vl_baseicm2_ret * w_pc_icm / 100) +
     	                    (w_vl_base_1 * w_pc_icm /100)))   +
                            (w_vl_basemaj_3 * w_pc_aliq_interna / 100);
    END IF;

    p_vl_icmretido := w_vl_icmretido;
  
  
   EXCEPTION
   WHEN OTHERS THEN
   p_erro := SQLCODE;
          
            
  END;
  
  PROCEDURE PR_DETALHES_CLIENTE(p_cursor      IN OUT tp_cursor,
       		                p_cod_cliente IN     cliente.cod_cliente%TYPE,
                                p_erro        OUT    NUMBER)
  IS
              
          BEGIN
          
          p_erro := 0;
              
          --Abrindo Cursor
           OPEN p_cursor FOR Select nvl(ddd1,0) ddd1,nvl(fone1,0) fone1,
                               nvl(ddd2,0) ddd2,nvl(fone2,0) fone2,
                               nome_contato,
                               desc_mens
                             From Cliente a,
                                  clie_mensagem b
                             Where cod_cliente = p_cod_cliente and
                                  a.cod_mensagem = b.cod_mensagem (+);
              
              
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
          
            
 END;
 
 PROCEDURE PR_COTACAO_CABECALHO(p_cursor      IN OUT tp_cursor,
  		      		p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		      		p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                       		p_erro        OUT    NUMBER)

  IS
               
           BEGIN
           
           p_erro := 0;
               
           --Abrindo Cursor
            OPEN p_cursor FOR select to_char(cot.DT_VALIDADE,'dd/mm/rr') dt_validade,
            		nvl(cot.COD_CLIENTE,0) cod_cliente, 
			nvl(cot.COD_VEND,0) cod_vend,
			 translate(nvl(cot.OBSERV,' '),'''',' ') observ, 
			NVL(COT.PC_DESC_SUFRAMA,0) PC_DESC_SUFRAMA, 
			nvl(cot.PC_DESCONTO,0) pc_desconto, 
			nvl(cot.PC_ACRESCIMO,0) pc_acrescimo,
			 translate(nvl(cli.NOME_CLIENTE,' '),'''',' ') nome_cliente,
			 nvl(cid.NOME_CIDADE,' ') nome_cidade, 
			nvl(cid.COD_UF,' ') cod_uf, 
			translate(nvl(vend.PSEUDONIMO,' '),'''',' ') pseudonimo, 
			translate(nvl(pla.DESC_PLANO,' '),'''',' ') desc_plano
			from vendas.COTACAO cot,
			CLIENTE cli,
			REPRESENTANTE vend,
			PLANO_PGTO pla,
			CIDADE cid 
			where cot.COD_PLANO = pla.COD_PLANO and
			 cot.COD_VEND = vend.COD_REPRES(+) and
			 cli.COD_CIDADE = cid.COD_CIDADE and 
			cot.COD_CLIENTE = cli.COD_CLIENTE and
			 cot.NUM_COTACAO = p_num_cotacao and
			 cot.COD_LOJA=p_cod_loja;

               
               
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
           
             
  END;
  
  PROCEDURE PR_COTACAO_ITEM(p_cursor      IN OUT tp_cursor,
    		      	   p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
    		      	   p_num_cotacao IN     vendas.cotacao.num_cotacao%TYPE,
                      	   p_erro        OUT    NUMBER)

    IS
                 
             BEGIN
             
             p_erro := 0;
                 
             --Abrindo Cursor
              OPEN p_cursor FOR select itcot.COD_DPK,
 		translate(nvl(itcot.COMPLEMENTO,' '),'''',' ') complemento, 
		nvl(itcot.QTD_SOLICITADA,0) qtd_solicitada,
		 nvl((itcot.PRECO_UNITARIO      * 
		(1 - itcot.PC_DESC1/100)  * 
		(1 - itcot.PC_DESC2/100)  *
		 (1 - itcot.PC_DESC3/100)  *
		 (1 - itcot.PC_DIFICM/100)  *
		 (1 - cot.pc_desconto/100) *
		 (1 + cot.pc_acrescimo/100) *
		 (1 - cot.pc_desc_suframa)),0) PRECO, 
		translate(nvl(it.DESC_ITEM,' '),'''',' ') desc_item, 
		translate(nvl(it.MASCARADO,' '),'''',' ') mascarado, 
		translate(nvl(fr.SIGLA,' '),'''',' ') sigla,
		 nvl(it.COD_UNIDADE,' ') cod_unidade 
		from ITEM_CADASTRO it,
		 vendas.ITEM_COTACAO itcot, 
		fornecedor fr,
		vendas.cotacao cot
		 where itcot.COD_DPK = it.COD_DPK and 
		it.cod_fornecedor = fr.cod_fornecedor and 
		itcot.NUM_COTACAO = p_num_cotacao and 
		itcot.COD_LOJA=p_cod_loja and
		cot.num_cotacao = itcot.num_cotacao and
		cot.cod_loja = itcot.cod_loja
		order by NUM_ITEM_COTACAO;
  
                 
                 
              EXCEPTION
              WHEN OTHERS THEN
              p_erro := SQLCODE;
             
               
  END;
  
  PROCEDURE PR_DADOS_LOJA(p_cursor      IN OUT tp_cursor,
    		    	  p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		          p_erro        OUT    NUMBER)

  IS
                
            BEGIN
            
            p_erro := 0;
                
            --Abrindo Cursor
             OPEN p_cursor FOR select a.ddd, a.fone, a.fax,
			a.endereco,
			a.bairro,
			a.cep,
			b.nome_cidade,
			b.cod_uf
			from loja a, cidade b
			where
			a.cod_loja=p_cod_loja and
			a.cod_cidade=b.cod_cidade;
                
                
             EXCEPTION
             WHEN OTHERS THEN
             p_erro := SQLCODE;
            
              
 END;
 
 PROCEDURE PR_GETITEM(p_cursor       IN OUT tp_cursor,
   		      p_cod_loja     IN     loja.cod_loja%TYPE,
   		      p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
   		      p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
   		      p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
   		      p_erro         OUT    NUMBER)

    IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      IF p_cod_dpk <> -1 THEN
       OPEN p_cursor FOR select e.COD_DPK, e.COD_FORNECEDOR, nvl(f.SIGLA,' ') sigla,
 		nvl(e.COD_FABRICA,0) cod_fabrica, nvl(e.DESC_ITEM,' ') desc_item, 
		nvl(b.QTD_ATUAL,0) qtd_atual, nvl(b.QTD_RESERV,0) qtd_reserv,
		 nvl(b.QTD_PENDENTE,0) qtd_pendente, 
		nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE, 
		nvl(a.SINAL,' ') sinal, nvl(e.QTD_MINVDA,1) qtd_minvda,
		 nvl(b.QTD_MAXVDA,1) qtd_maxvda, nvl(e.PESO,0) peso,
		 nvl(e.cod_grupo,0) cod_grupo, nvl(e.cod_subgrupo,0) cod_subgrupo, 
		nvl(e.COD_UNIDADE,' ') cod_unidade, 
		nvl(b.QTD_PENDENTE,0) qtd_pendente,
		 e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI,
		 e.PC_IPI, nvl(d.PRECO_VENDA,0) PRECO_VENDA,
		 nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT,
		 nvl(d.PRECO_OF,0) PRECO_OF, 
		nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT,
		 nvl(d.PRECO_SP,0) PRECO_SP,
		 nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
		 b.SITUACAO, nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
		 nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
		 f.DIVISAO, c.categoria 
		from categ_sinal a, item_estoque b,
		 item_analitico c, item_preco d, 
		item_cadastro e, fornecedor f 
		where c.categoria = a.categoria and
		 b.cod_loja = c.cod_loja and
		 b.cod_dpk = c.cod_dpk and 
		c.cod_loja = d.cod_loja and
		 c.cod_dpk = d.cod_dpk and
		 d.cod_loja = p_cod_loja and
		 d.cod_dpk = e.cod_dpk and 
		e.cod_fornecedor = f.cod_fornecedor and 
		e.COD_DPK = p_cod_dpk;

      
      
      ELSIF p_cod_forn <> -1 and p_cod_fabrica <> '-1' THEN
      
       OPEN p_cursor FOR select e.COD_DPK, e.COD_FORNECEDOR, nvl(f.SIGLA,' ') sigla,
       nvl(e.COD_FABRICA,0) cod_fabrica, nvl(e.DESC_ITEM,' ') desc_item,
       nvl(b.QTD_ATUAL,0) qtd_atual, nvl(b.QTD_RESERV,0) qtd_reserv, 
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE,
       nvl(a.SINAL,' ') sinal, nvl(e.QTD_MINVDA,1) qtd_minvda, 
       nvl(b.QTD_MAXVDA,1) qtd_maxvda, 
       nvl(e.PESO,0) peso, nvl(e.cod_grupo,0) cod_grupo, 
       nvl(e.cod_subgrupo,0) cod_subgrupo, nvl(e.COD_UNIDADE,' ') cod_unidade,
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI, 
       e.PC_IPI, nvl(d.PRECO_VENDA,0) PRECO_VENDA, 
       nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT, 
       nvl(d.PRECO_OF,0) PRECO_OF, 
       nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT, 
       nvl(d.PRECO_SP,0) PRECO_SP,
       nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
       b.SITUACAO, nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
       nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
       f.DIVISAO, c.categoria 
       from categ_sinal a,
        item_estoque b, 
        item_analitico c,
        item_preco d,
        item_cadastro e, 
       fornecedor f 
      where c.categoria = a.categoria and 
       b.cod_loja = c.cod_loja and 
       b.cod_dpk = c.cod_dpk and 
       c.cod_loja = d.cod_loja and 
       c.cod_dpk = d.cod_dpk and
       d.cod_loja = p_cod_loja and
       d.cod_dpk = e.cod_dpk and 
       e.cod_fornecedor = f.cod_fornecedor and 
       e.COD_FORNECEDOR = p_cod_forn  AND 
       e.COD_FABRICA = p_cod_fabrica;
    ELSIF p_cod_forn = -1 and p_cod_fabrica <> '-1' THEN
       OPEN p_cursor FOR select e.COD_DPK, e.COD_FORNECEDOR, nvl(f.SIGLA,' ') sigla,
       nvl(e.COD_FABRICA,0) cod_fabrica, nvl(e.DESC_ITEM,' ') desc_item,
       nvl(b.QTD_ATUAL,0) qtd_atual, nvl(b.QTD_RESERV,0) qtd_reserv, 
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE,
       nvl(a.SINAL,' ') sinal, nvl(e.QTD_MINVDA,1) qtd_minvda, 
       nvl(b.QTD_MAXVDA,1) qtd_maxvda, 
       nvl(e.PESO,0) peso, nvl(e.cod_grupo,0) cod_grupo, 
       nvl(e.cod_subgrupo,0) cod_subgrupo, nvl(e.COD_UNIDADE,' ') cod_unidade,
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI, 
       e.PC_IPI, nvl(d.PRECO_VENDA,0) PRECO_VENDA, 
       nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT, 
       nvl(d.PRECO_OF,0) PRECO_OF, 
       nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT, 
       nvl(d.PRECO_SP,0) PRECO_SP,
       nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
       b.SITUACAO, nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
       nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
       f.DIVISAO, c.categoria 
       from categ_sinal a,
        item_estoque b, 
        item_analitico c,
        item_preco d,
        item_cadastro e, 
       fornecedor f 
      where c.categoria = a.categoria and 
       b.cod_loja = c.cod_loja and 
       b.cod_dpk = c.cod_dpk and 
       c.cod_loja = d.cod_loja and 
       c.cod_dpk = d.cod_dpk and
       d.cod_loja = p_cod_loja and
       d.cod_dpk = e.cod_dpk and 
       e.cod_fornecedor = f.cod_fornecedor and 
       e.COD_FABRICA = p_cod_fabrica
       UNION
       select e.COD_DPK, e.COD_FORNECEDOR, nvl(f.SIGLA,' ') sigla,
       nvl(e.COD_FABRICA,0) cod_fabrica, nvl(e.DESC_ITEM,' ') desc_item,
       nvl(b.QTD_ATUAL,0) qtd_atual, nvl(b.QTD_RESERV,0) qtd_reserv, 
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE,
       nvl(a.SINAL,' ') sinal, nvl(e.QTD_MINVDA,1) qtd_minvda, 
       nvl(b.QTD_MAXVDA,1) qtd_maxvda, 
       nvl(e.PESO,0) peso, nvl(e.cod_grupo,0) cod_grupo, 
       nvl(e.cod_subgrupo,0) cod_subgrupo, nvl(e.COD_UNIDADE,' ') cod_unidade,
       nvl(b.QTD_PENDENTE,0) qtd_pendente, 
       e.COD_TRIBUTACAO, e.COD_TRIBUTACAO_IPI, 
       e.PC_IPI, nvl(d.PRECO_VENDA,0) PRECO_VENDA, 
       nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT, 
       nvl(d.PRECO_OF,0) PRECO_OF, 
       nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT, 
       nvl(d.PRECO_SP,0) PRECO_SP,
       nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
       b.SITUACAO, nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
       nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
       f.DIVISAO, c.categoria 
       from categ_sinal a,
        item_estoque b, 
        item_analitico c,
        item_preco d,
        item_cadastro e, 
       fornecedor f ,
       R_FABRICA_DPK G
      where c.categoria = a.categoria and 
       b.cod_loja = c.cod_loja and 
       b.cod_dpk = c.cod_dpk and 
       c.cod_loja = d.cod_loja and 
       c.cod_dpk = d.cod_dpk and
       d.cod_loja = p_cod_loja and
       d.cod_dpk = e.cod_dpk and 
       E.COD_DPK = G.COD_DPK AND
       e.cod_fornecedor = f.cod_fornecedor and 
       G.COD_FABRICA = p_cod_fabrica;
       
     
    END IF;   

    
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;                      
  
  PROCEDURE PR_ITENS_NOVOS(p_cursor      IN OUT tp_cursor,
  		    	   p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
  		           p_erro        OUT    NUMBER)
    IS
                  
              BEGIN
              
              p_erro := 0;
                  
              --Abrindo Cursor
               OPEN p_cursor FOR Select b.cod_fornecedor,c.sigla, b.cod_fabrica,
		b.desc_item,b.cod_dpk 
		from  item_estoque a, item_cadastro b,
		fornecedor c, datas d 
		where  
		(a.qtd_atual - a.qtd_pendente - a.qtd_reserv) > 0 and
		b.dt_cadastramento >= add_months(d.dt_faturamento,-1) and
		b.dt_cadastramento < d.dt_faturamento - 1 and
		b.cod_fornecedor = c.cod_fornecedor and
		a.cod_loja = p_cod_loja and	
		a.cod_dpk = b.cod_dpk  
		Order by b.cod_fornecedor,b.cod_fabrica;

               
               
               EXCEPTION
               WHEN OTHERS THEN
               p_erro := SQLCODE;
              
                
   END;
   
   PROCEDURE PR_KIT_VEICULO(p_cursor      IN OUT tp_cursor,
     		    	    p_cod_loja    IN     vendas.cotacao.cod_loja%TYPE,
     		    	    p_veiculo     IN     VARCHAR,
  		            p_erro        OUT    NUMBER)
 
    IS
                     
                 BEGIN
                 
                 p_erro := 0;
                     
                 --Abrindo Cursor
                  OPEN p_cursor FOR Select b.cod_fornecedor,c.sigla, b.cod_fabrica,
   		b.desc_item,b.cod_dpk,E.DESC_APLICACAO 
   		from  item_estoque a, item_cadastro b,
   		fornecedor c, datas d ,
          	 APLICACAO E
   		where  
   		a.situacao in (0,8) and
   		b.cod_fornecedor = c.cod_fornecedor and
   		a.cod_loja+0 = p_cod_loja and
            	e.desc_aplicacao like p_veiculo AND
		b.cod_dpk = e.cod_dpk and	
   		a.cod_dpk = b.cod_dpk  
   		Order by B.COD_DPK,B.DESC_ITEM,B.COD_FORNECEDOR;
   
                  
                  
                  EXCEPTION
                  WHEN OTHERS THEN
                  p_erro := SQLCODE;
                 
                   
   END;

 
 
END PCK_VDA230;  

