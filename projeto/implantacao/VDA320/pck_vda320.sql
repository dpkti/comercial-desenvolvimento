CREATE OR REPLACE PACKAGE PCK_VDA320
--------------------------------------------
--- SISTEMA	: CONSULTA ITEM
--- ANALISTA    : MARICI 	
--- DATA	: 07/11/2002

IS
 -- variavel tipo cursor
 TYPE tp_cursor IS REF CURSOR;

 PROCEDURE PR_TABELA(p_cursor   IN OUT tp_cursor,
                     p_erro     OUT    NUMBER);
 
 PROCEDURE PR_BANCO(p_cursor   IN OUT tp_cursor,
                    p_erro     OUT    NUMBER);
 
 PROCEDURE PR_APLICA1(p_cursor       IN OUT tp_cursor,
 		      p_cod_loja     IN     loja.cod_loja%TYPE,
 		      p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
 		      p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
 		      p_desc_item    IN     item_cadastro.desc_item%TYPE,
                      p_erro         OUT    NUMBER);
 
 PROCEDURE PR_MONTADORA(p_cursor   IN OUT tp_cursor,
                        p_erro     OUT    NUMBER);  
 
 PROCEDURE PR_GRUPO(p_cursor   IN OUT tp_cursor,
 		    p_cod_forn IN     item_cadastro.cod_fornecedor%TYPE,
                    p_erro     OUT    NUMBER);  
 
 PROCEDURE PR_SUBGRUPO(p_cursor   IN OUT tp_cursor,
  		       p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
  		       p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
                       p_erro         OUT    NUMBER);  
                    
 PROCEDURE PR_APLICA2(p_cursor       IN OUT tp_cursor,
  		      p_cod_dpk      IN     loja.cod_loja%TYPE,
  		      p_erro         OUT    NUMBER);

 PROCEDURE PR_PLANO(p_cursor   IN OUT tp_cursor,
                    p_erro     OUT    NUMBER);
   		      
 PROCEDURE PR_FORNECEDOR(p_cursor   IN OUT tp_cursor,
                         p_erro     OUT    NUMBER);
 
 PROCEDURE PR_UF(p_cursor   IN OUT tp_cursor,
                 p_erro     OUT    NUMBER);  

 
PROCEDURE PR_FILIAL(p_cursor   IN OUT tp_cursor,
                    p_erro     OUT    NUMBER); 
                    
PROCEDURE PR_FORNEC(p_cursor   IN OUT tp_cursor,
                    p_cod_forn IN     item_cadastro.cod_fornecedor%TYPE,
                    p_erro     OUT    NUMBER);
 
PROCEDURE PR_PLANO2(p_cursor    IN OUT tp_cursor,
		    p_cod_plano IN     plano_pgto.cod_plano%TYPE,
                    p_erro      OUT    NUMBER);

PROCEDURE PR_UF_DPK(p_cursor     IN OUT tp_cursor,
		    p_cod_dpk    IN     item_cadastro.cod_dpk%TYPE,
		    p_uf_origem  IN     uf.cod_uf%TYPE,
		    p_uf_destino IN     uf.cod_uf%TYPE,
                    p_erro       OUT    NUMBER);

PROCEDURE PR_CLIENTE(p_cursor      IN OUT tp_cursor,
		     p_cod_CLIENTE IN     cliente.cod_cliente%TYPE,
		     p_cgc         IN     cliente.cgc%TYPE,
                     p_erro        OUT    NUMBER);
                    
PROCEDURE PR_CLIE_REPRES(p_cursor      IN OUT tp_cursor,
                         p_cod_CLIENTE IN     cliente.cod_cliente%TYPE,
                         p_erro        OUT    NUMBER);

PROCEDURE PR_CLIE_VDR(p_cursor      IN OUT tp_cursor,
                      p_cod_CLIENTE IN     cliente.cod_cliente%TYPE,
                      p_erro        OUT    NUMBER);                         


PROCEDURE PR_CLIE_CARAC(p_cursor      IN OUT tp_cursor,
                        p_carac       IN     cliente.caracteristica%TYPE,
                        p_erro        OUT    NUMBER);                         

PROCEDURE PR_ITEM(p_cursor       IN OUT tp_cursor,
 	          p_cod_loja     IN     loja.cod_loja%TYPE,
 		  p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
 		  p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
 		  p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
 		  p_erro         OUT    NUMBER);
 
PROCEDURE PR_FORN_ESPECIFICO(p_cursor       IN OUT tp_cursor,
 	    		     p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
 		             p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
 		             p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
 		             p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,  
    		             p_erro         OUT    NUMBER);

PROCEDURE PR_FORNECEDOR_ESPECIFICO(p_cursor       IN OUT tp_cursor,
 	    		     p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
 		             p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
 		             p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
 		             p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,  
    		             p_erro         OUT    NUMBER);  

PROCEDURE PR_UF_CATEG(p_cursor      IN OUT tp_cursor,
                      p_cod_uf      IN     uf.cod_uf%TYPE,
                      p_categ       IN     item_analitico.categoria%TYPE,
                      p_carac       IN     cliente.caracteristica%TYPE,
                      p_erro        OUT    NUMBER);  

PROCEDURE PR_DESC_PERIODO(p_cursor       IN OUT tp_cursor,
                          p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
                          p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
                          p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
                          p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
                          p_cod_filial   IN     filial.cod_filial%TYPE,
                          p_ocorr0       IN     tabela_descper.ocorr_preco%TYPE,
                          p_ocorr1       IN     tabela_descper.ocorr_preco%TYPE,
                          p_ocorr2       IN     tabela_descper.ocorr_preco%TYPE,
                          p_seq0         IN     tabela_descper.sequencia%TYPE,
                          p_seq1         IN     tabela_descper.sequencia%TYPE,
                          p_seq2         IN     tabela_descper.sequencia%TYPE,
                          p_erro         OUT    NUMBER);  
                      
 PROCEDURE PR_UF_ORIGEM_DESTINO(p_cursor         IN OUT tp_cursor,
                      		p_cod_uf_origem  IN     uf.cod_uf%TYPE,
                     		p_cod_uf_destino IN     uf.cod_uf%TYPE,
                           	p_erro        OUT    NUMBER);  
                           	
 PROCEDURE PR_SUBST_TRIB(p_cursor         IN OUT tp_cursor,
 			 p_class          IN     item_cadastro.class_fiscal%TYPE,
                         p_cod_uf_origem  IN     uf.cod_uf%TYPE,
                   	 p_cod_uf_destino IN     uf.cod_uf%TYPE,
                         p_erro           OUT    NUMBER);
 
 PROCEDURE PR_LOJA(p_cursor   IN OUT tp_cursor,
 		   p_cod_loja IN     loja.cod_loja%TYPE,
                   p_erro     OUT    NUMBER);
 
 PROCEDURE PR_CLIENTE1(p_cursor       IN OUT tp_cursor,
  		       p_nome_cliente IN     cliente.nome_cliente%TYPE,
                       p_erro         OUT    NUMBER);


END PCK_VDA320;
/

CREATE OR REPLACE PACKAGE BODY PCK_vda320 IS
  
  
  PROCEDURE PR_TABELA(p_cursor   IN OUT tp_cursor,
                      p_erro     OUT    NUMBER)
  IS
    
  BEGIN
  
    p_erro := 0;
    
    --Abrindo Cursor
    OPEN p_cursor FOR select tabela_venda, tp_tabela, ocorr_preco,
 	decode(nvl(dt_vigencia1,0),'0','0',
 	to_char(to_date(dt_vigencia1,'YYYYMMDD'),'DD/MM/YYYY')) dt_vigencia1,
 	decode(nvl(dt_vigencia2,0),'0','0',
 	to_char(to_date(dt_vigencia2,'YYYYMMDD'),'DD/MM/YYYY')) dt_vigencia2,
 	decode(nvl(dt_vigencia3,0),'0','0',
 	to_char(to_date(dt_vigencia3,'YYYYMMDD'),'DD/MM/YYYY')) dt_vigencia3,
 	decode(nvl(dt_vigencia4,0),'0','0',
 	to_char(to_date(dt_vigencia4,'YYYYMMDD'),'DD/MM/YYYY')) dt_vigencia4,
 	decode(nvl(dt_vigencia5,0),'0','0',
 	to_char(to_date(dt_vigencia5,'YYYYMMDD'),'DD/MM/YYYY')) dt_vigencia5
	from tabela_venda	
	where divisao='D'
	order by ocorr_preco;

    
    
    
    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;

  
  END;
  
   PROCEDURE PR_BANCO(p_cursor   IN OUT tp_cursor,
                      p_erro     OUT    NUMBER)
    IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      OPEN p_cursor FOR select COD_BANCO,SIGLA 
			from BANCO 
			where DIVISAO IN ('D','T') and FL_VENDA = 'V' 
			order by SIGLA;
    
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;
  
  PROCEDURE PR_APLICA1(p_cursor       IN OUT tp_cursor,
   		      p_cod_loja     IN     loja.cod_loja%TYPE,
   		      p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
   		      p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
   		      p_desc_item    IN     item_cadastro.desc_item%TYPE,
                      p_erro         OUT    NUMBER)
                      
    IS
      
    BEGIN
    
      p_erro := 0;
      
      --Abrindo Cursor
      IF p_cod_forn = 0 THEN
              OPEN p_cursor FOR select a.COD_FABRICA,a.DESC_ITEM,
      			 c.DESC_APLICACAO, a.cod_fornecedor,
      			 d.sigla
      			 from item_cadastro a, item_estoque b, aplicacao c,
      			 fornecedor d
      			 where 
      			 a.cod_fornecedor = d.cod_fornecedor and
      			 c.COD_DPK(+) = a.cod_dpk and
      			 b.cod_loja+0 = p_cod_loja and 
      			 b.cod_dpk = a.cod_dpk and 
      			 b.SITUACAO <> 9 and 
      			 a.COD_FABRICA like p_cod_fabrica
      			 UNION
      			 select a.COD_FABRICA,a.DESC_ITEM,
			   c.DESC_APLICACAO, a.cod_fornecedor,
			   d.sigla
			 from item_cadastro a, item_estoque b, aplicacao c,
			 fornecedor d, R_FABRICA_DPK E
			 where 
			 a.cod_fornecedor = d.cod_fornecedor and
			 c.COD_DPK(+) = a.cod_dpk and
			 b.cod_loja+0 = p_cod_loja and 
			 b.cod_dpk = a.cod_dpk and 
			 b.SITUACAO <> 9 and 
			 E.COD_FABRICA like p_cod_fabrica AND
			 E.COD_DPK = A.COD_DPK 
      			 order by 1,4;
      
      ELSIF p_desc_item = '0' THEN
        OPEN p_cursor FOR select a.COD_FABRICA,a.DESC_ITEM,
			 c.DESC_APLICACAO
			 from item_cadastro a, item_estoque b, aplicacao c 
			 where c.COD_DPK(+) = a.cod_dpk and
			 b.cod_loja+0 = p_cod_loja and 
			 b.cod_dpk = a.cod_dpk and 
			 b.SITUACAO <> 9 and 
			 a.COD_FORNECEDOR = p_cod_forn and
			 a.COD_FABRICA like p_cod_fabrica
			 order by cod_fabrica;
   
      
      ELSE
        OPEN p_cursor FOR select a.COD_FABRICA,a.DESC_ITEM,
			c.DESC_APLICACAO
			from item_cadastro a, item_estoque b, aplicacao c 
			where c.COD_DPK(+) = a.cod_dpk and
			b.cod_loja+0 = p_cod_loja and 
			b.cod_dpk = a.cod_dpk and 
			b.SITUACAO = 0 and 
			a.COD_FORNECEDOR = p_cod_forn and
			a.desc_item like p_desc_item
			order by cod_fabrica;
   
      
      END IF;
    
      
      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;
  
    
  END;
  
  PROCEDURE PR_MONTADORA(p_cursor   IN OUT tp_cursor,
                         p_erro     OUT    NUMBER)
  
  IS
        
      BEGIN
      
        p_erro := 0;
        
        --Abrindo Cursor
        OPEN p_cursor FOR select a.desc_montadora desc_montadora,
		 a.cod_montadora cod_montadora
 		from montadora a
		  order by desc_montadora;
        
        
        
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
    
      
  END;
  
   PROCEDURE PR_GRUPO(p_cursor   IN OUT tp_cursor,
   	    	      p_cod_forn IN     item_cadastro.cod_fornecedor%TYPE,
                      p_erro     OUT    NUMBER)
                    
     IS
        
      BEGIN
      
        p_erro := 0;
        
        --Abrindo Cursor
        IF p_cod_forn <> 0 THEN
        
          OPEN p_cursor FOR select distinct b.desc_grupo desc_grupo,
			b.cod_grupo cod_grupo
			 from grupo b, item_cadastro a
			 where b.cod_grupo=a.cod_grupo and
			 a.cod_fornecedor = p_cod_forn
			 order by b.desc_grupo;
        ELSE
          OPEN p_cursor FOR select distinct b.desc_grupo desc_grupo,
	  			b.cod_grupo cod_grupo
	  			 from grupo b, item_cadastro a
	  			 where b.cod_grupo=a.cod_grupo 
			 order by b.desc_grupo;
        
        END IF;
        
        
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
    
      
  END;

   PROCEDURE PR_SUBGRUPO(p_cursor   IN OUT tp_cursor,
    		         p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
    		         p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
                         p_erro         OUT    NUMBER)  

    IS
           
         BEGIN
         
           p_erro := 0;
           
           --Abrindo Cursor
           IF p_cod_forn <> 0 THEN
             OPEN p_cursor FOR select distinct b.desc_subgrupo desc_subgrupo,
			b.cod_subgrupo cod_subgrupo
			 from subgrupo b, item_cadastro a
			 where b.cod_subgrupo=a.cod_subgrupo and
			 b.cod_grupo = a.cod_grupo and
			 a.cod_grupo = p_cod_grupo and
			 a.cod_fornecedor = p_cod_forn
			 order by b.desc_subgrupo;
           ELSE
             OPEN p_cursor FOR select distinct b.desc_subgrupo desc_subgrupo,
	     			b.cod_subgrupo cod_subgrupo
	     			 from subgrupo b, item_cadastro a
	     			 where b.cod_subgrupo=a.cod_subgrupo and
	     			 b.cod_grupo = a.cod_grupo and
	     			 a.cod_grupo = p_cod_grupo 
			 order by b.desc_subgrupo;
           END IF;
           
           
           
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
       
         
  END;

  PROCEDURE PR_APLICA2(p_cursor       IN OUT tp_cursor,
    		       p_cod_dpk      IN     loja.cod_loja%TYPE,
  		       p_erro         OUT    NUMBER)
  		       
  		       
  IS
           
         BEGIN
         
           p_erro := 0;
           
           --Abrindo Cursor
           OPEN p_cursor FOR select a.desc_montadora,
			b.desc_aplicacao
			from montadora a, aplicacao b
			 where a.cod_montadora = b.cod_montadora and
			 b.cod_dpk = p_cod_dpk
			order by b.cod_montadora, b.cod_categoria, b.sequencia;
    
           
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;
       
         
  END;                   
  
  PROCEDURE PR_PLANO(p_cursor   IN OUT tp_cursor,
                     p_erro     OUT    NUMBER)
      IS
        
      BEGIN
      
        p_erro := 0;
        
        --Abrindo Cursor
        OPEN p_cursor FOR select COD_PlANO,DESC_PLANO,PRAZO_MEDIO,
			PC_DESC_FIN_DPK,PC_ACRES_FIN_DPK
			 from PLANO_PGTO
			 where SITUACAO = 0
			 order by DESC_PLANO;
    
        
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
    
      
  END;
  
  PROCEDURE PR_FORNECEDOR(p_cursor   IN OUT tp_cursor,
                          p_erro     OUT    NUMBER)
        IS
          
        BEGIN
        
          p_erro := 0;
          
          --Abrindo Cursor
          OPEN p_cursor FOR select COD_FORNECEDOR,SIGLA 
			from FORNECEDOR 
			where SITUACAO = 0 and 
			DIVISAO = 'D' and CLASSIFICACAO = 'A' 
			order by SIGLA;
          
          
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
      
        
  END;
  
  PROCEDURE PR_UF(p_cursor   IN OUT tp_cursor,
                  p_erro     OUT    NUMBER)
  
    IS
            
       BEGIN
          
         p_erro := 0;
            
          --Abrindo Cursor
          OPEN p_cursor FOR select COD_UF from UF order by COD_UF;
            
            
            
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
        
          
  END;
  
  PROCEDURE PR_FILIAL(p_cursor   IN OUT tp_cursor,
                      p_erro     OUT    NUMBER)
                    
   IS
            
       BEGIN
          
         p_erro := 0;
            
          --Abrindo Cursor
          OPEN p_cursor FOR select to_char(cod_filial,'0009') || '-' || sigla filial
		from filial 
		 Where divisao='D' 
		 order by cod_filial;
            
            
            
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
        
          
  END;
  
  PROCEDURE PR_FORNEC(p_cursor   IN OUT tp_cursor,
                      p_cod_forn IN     item_cadastro.cod_fornecedor%TYPE,
                      p_erro     OUT    NUMBER)
                    
   IS
              
         BEGIN
            
           p_erro := 0;
              
            --Abrindo Cursor
            OPEN p_cursor FOR select nvl(SIGLA,' ') sigla, SITUACAO 
			from FORNECEDOR 
			where SITUACAO = 0 and 
			COD_FORNECEDOR = p_cod_forn;
        
              
              
              
            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;
          
            
  END;
  
  PROCEDURE PR_PLANO2(p_cursor    IN OUT tp_cursor,
  		      p_cod_plano IN     plano_pgto.cod_plano%TYPE,
                      p_erro      OUT    NUMBER)
  
    IS
                
           BEGIN
              
             p_erro := 0;
                
              --Abrindo Cursor
              OPEN p_cursor FOR select DESC_PLANO,
			PC_ACRES_FIN_DPK,
			PC_DESC_FIN_DPK,
			SITUACAO,
			fl_avista
			from PLANO_PGTO 
			where COD_PLANO = p_cod_plano;
                
                
                
              EXCEPTION
              WHEN OTHERS THEN
              p_erro := SQLCODE;
            
              
  END;
  
  PROCEDURE PR_UF_DPK(p_cursor     IN OUT tp_cursor,
  		    p_cod_dpk    IN     item_cadastro.cod_dpk%TYPE,
  		    p_uf_origem  IN     uf.cod_uf%TYPE,
  		    p_uf_destino IN     uf.cod_uf%TYPE,
                    p_erro       OUT    NUMBER)
  
   IS
                  
      BEGIN
                
         p_erro := 0;
                  
         --Abrindo Cursor
         OPEN p_cursor FOR select PC_DESC from VENDAS.UF_DPK where 
			 COD_DPK = p_cod_dpk and 
			 COD_UF_ORIGEM =  p_uf_origem and 
			 COD_UF_DESTINO = p_uf_destino; 
    
                  
           
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
              
                
  END;
  
  PROCEDURE PR_CLIENTE(p_cursor      IN OUT tp_cursor,
  		       p_cod_CLIENTE IN     cliente.cod_cliente%TYPE,
  		       p_cgc         IN     cliente.cgc%TYPE,
                       p_erro        OUT    NUMBER)
  
  IS
                    
        BEGIN
                  
           p_erro := 0;
                    
           --Abrindo Cursor
           IF p_cod_cliente <> '0' THEN
             OPEN p_cursor FOR select cli.COD_CLIENTE,
			cli.CGC,
 			nvl(cli.NOME_CLIENTE,' ') nome_cliente,
 			nvl(cli.INSCR_ESTADUAL,' ') inscr_estadual,
 			nvl(cli.INSCR_SUFRAMA,' ') inscr_suframa,
 			nvl(cli.COD_TARE,0) cod_tare,
 			nvl(cli.FL_CONS_FINAL,' ') fl_cons_final,
 			cli.SITUACAO situacao,
 			NVL(cli.CLASSIFICACAO,' ') CLASSIFICACAO,
 			nvl(cid.NOME_CIDADE,' ') nome_cidade,
 			nvl(cli.cod_tipo_cliente,' ') cod_tipo_cliente,
 			cid.COD_UF, cli.caracteristica caracteristica,
 			cli.cod_mensagem,c.desc_mens mensagem
 			from CLIENTE cli, CIDADE cid, clie_mensagem c
 			where cli.cod_mensagem = c.cod_mensagem(+) and
 			cli.COD_CIDADE = cid.COD_CIDADE and 
 			cli.COD_CLIENTE = p_cod_cliente;
 
           ELSE
             OPEN p_cursor FOR select cli.COD_CLIENTE,
			cli.CGC,
			 nvl(cli.NOME_CLIENTE,' ') nome_cliente,
			 nvl(cli.INSCR_ESTADUAL,' ') inscr_estadual,
			 nvl(cli.INSCR_SUFRAMA,' ') inscr_suframa,
			 nvl(cli.COD_TARE,0) cod_tare,
			 nvl(cli.FL_CONS_FINAL,' ') fl_cons_final,
			 cli.SITUACAO situacao,
			 NVL(cli.CLASSIFICACAO,' ') CLASSIFICACAO,
			 nvl(cid.NOME_CIDADE,' ') nome_cidade,
			 nvl(cli.cod_tipo_cliente,' ') cod_tipo_cliente,
			 cid.COD_UF, cli.caracteristica caracteristica,
			 cli.cod_mensagem,c.desc_mens mensagem
			 from CLIENTE cli, CIDADE cid, clie_mensagem c 
			 where cli.cod_mensagem = c.cod_mensagem(+) and
			 cli.COD_CIDADE = cid.COD_CIDADE and 
			  cli.CGC = p_cgc;
           END IF;
           
             
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
                
                  
  END;
  
  PROCEDURE PR_CLIE_REPRES(p_cursor      IN OUT tp_cursor,
                           p_cod_cliente IN     cliente.cod_cliente%TYPE,
                           p_erro        OUT    NUMBER)
   IS
                    
        BEGIN
                  
           p_erro := 0;
                    
           --Abrindo Cursor
           OPEN p_cursor FOR Select a.cod_repres,b.pseudonimo,
 			to_char(B.cod_filial,'0009') || '-' || c.sigla filial
 			From r_clie_repres a, representante b,
 			filial c
 			Where cod_cliente = p_cod_cliente and
 			b.cod_filial = c.cod_filial and
			a.cod_repres=b.cod_repres 
			order by b.cod_filial, a.cod_repres;
      
                    
             
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
                
                  
  END;
  
  PROCEDURE PR_CLIE_VDR(p_cursor      IN OUT tp_cursor,
                        p_cod_CLIENTE IN     cliente.cod_cliente%TYPE,
                        p_erro        OUT    NUMBER)                         
   IS
                    
        BEGIN
                  
           p_erro := 0;
                    
           --Abrindo Cursor
           OPEN p_cursor FOR Select cod_cliente
			From vdr.cliente_categ_vdr
			Where cod_cliente = p_cod_cliente;
                    
             
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
                
                  
  END;

  PROCEDURE PR_CLIE_CARAC(p_cursor      IN OUT tp_cursor,
                          p_carac       IN     cliente.caracteristica%TYPE,
                          p_erro        OUT    NUMBER)
 
    IS
                      
       BEGIN
                    
       p_erro := 0;
                      
        --Abrindo Cursor
        OPEN p_cursor FOR select desc_caracteristica,
 		fl_desc_automatico 
 		From producao.cliente_caracteristica
 		 Where caracteristica = p_carac;
                      
               
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
                  
                    
  END;

  PROCEDURE PR_ITEM(p_cursor       IN OUT tp_cursor,
   	          p_cod_loja     IN     loja.cod_loja%TYPE,
   		  p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
   		  p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
   		  p_cod_fabrica  IN     item_cadastro.cod_fabrica%TYPE,
   		  p_erro         OUT    NUMBER)
  IS
                        
         BEGIN
                      
         p_erro := 0;
                        
          --Abrindo Cursor
          IF p_cod_dpk <> 0 THEN
            OPEN p_cursor FOR select e.COD_DPK,
			e.COD_FORNECEDOR,
			nvl(f.SIGLA,' ') sigla,
			nvl(e.COD_FABRICA,0) cod_fabrica,
			nvl(e.DESC_ITEM,' ') desc_item,
			nvl(b.QTD_ATUAL,0) qtd_atual,
			nvl(b.QTD_RESERV,0) qtd_reserv,
			nvl(b.QTD_PENDENTE,0) qtd_pendente,
			nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE,
			nvl(a.SINAL,' ') sinal,
			nvl(e.QTD_MINVDA,1) qtd_minvda,
			nvl(b.QTD_MAXVDA,1) qtd_maxvda,
			nvl(e.PESO,0) peso,
			nvl(e.cod_grupo,0) cod_grupo,
			nvl(e.cod_subgrupo,0) cod_subgrupo,
			nvl(e.COD_UNIDADE,' ') cod_unidade,
			nvl(b.QTD_PENDENTE,0) qtd_pendente,
			e.COD_TRIBUTACAO,
			e.COD_TRIBUTACAO_IPI,
			e.PC_IPI,
			nvl(d.PRECO_VENDA,0) PRECO_VENDA,
			nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT,
			nvl(d.PRECO_OF,0) PRECO_OF,
			nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT,
			nvl(d.PRECO_SP,0) PRECO_SP,
			nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
			b.SITUACAO,
			nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
			nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
			f.DIVISAO, c.categoria
			from categ_sinal a,
			item_estoque b,
			item_analitico c,
			item_preco d,
			item_cadastro e,
			fornecedor f
			where c.categoria = a.categoria and
			b.cod_loja = c.cod_loja and
			b.cod_dpk = c.cod_dpk and
			c.cod_loja = d.cod_loja and
			c.cod_dpk = d.cod_dpk and
			d.cod_loja = p_cod_loja and
			d.cod_dpk = e.cod_dpk and
			e.cod_fornecedor = f.cod_fornecedor
			and e.COD_DPK = p_cod_dpk;
          ELSE
            OPEN p_cursor FOR   select e.COD_DPK,
			e.COD_FORNECEDOR,
			nvl(f.SIGLA,' ') sigla,
			nvl(e.COD_FABRICA,0) cod_fabrica,
			nvl(e.DESC_ITEM,' ') desc_item,
			nvl(b.QTD_ATUAL,0) qtd_atual,
			nvl(b.QTD_RESERV,0) qtd_reserv,
			nvl(b.QTD_PENDENTE,0) qtd_pendente,
			nvl((b.QTD_ATUAL - b.QTD_RESERV - b.QTD_PENDENTE),0) ESTOQUE,
			nvl(a.SINAL,' ') sinal,
			nvl(e.QTD_MINVDA,1) qtd_minvda,
			nvl(b.QTD_MAXVDA,1) qtd_maxvda,
			nvl(e.PESO,0) peso,
			nvl(e.cod_grupo,0) cod_grupo,
			nvl(e.cod_subgrupo,0) cod_subgrupo,
			nvl(e.COD_UNIDADE,' ') cod_unidade,
			nvl(b.QTD_PENDENTE,0) qtd_pendente,
			e.COD_TRIBUTACAO,
			e.COD_TRIBUTACAO_IPI,
			e.PC_IPI,
			nvl(d.PRECO_VENDA,0) PRECO_VENDA,
			nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT,
			nvl(d.PRECO_OF,0) PRECO_OF,
			nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT,
			nvl(d.PRECO_SP,0) PRECO_SP,
			nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,
			b.SITUACAO,
			nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,
			nvl(e.CLASS_FISCAL,0) CLASS_FISCAL,
			f.DIVISAO, c.categoria
			from categ_sinal a,
			item_estoque b,
			item_analitico c,
			item_preco d,
			item_cadastro e,
			fornecedor f
			where c.categoria = a.categoria and
			b.cod_loja = c.cod_loja and
			b.cod_dpk = c.cod_dpk and
			c.cod_loja = d.cod_loja and
			c.cod_dpk = d.cod_dpk and
			d.cod_loja = p_cod_loja and
			d.cod_dpk = e.cod_dpk and
			e.cod_fornecedor = f.cod_fornecedor
			and e.COD_FORNECEDOR = p_cod_forn and
                        e.COD_FABRICA = p_cod_fabrica;
          END IF;
          
                 
          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;
                    
                      
  END;
 
  PROCEDURE PR_FORN_ESPECIFICO(p_cursor       IN OUT tp_cursor,
   	    		       p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
   		               p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
   		               p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
   		               p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,  
    		               p_erro         OUT    NUMBER)
 
 IS
                        
      BEGIN
                      
      p_erro := 0;
                        
       --Abrindo Cursor
       OPEN p_cursor FOR Select fl_dif_icms,
			fl_adicional,TP_DIF_ICMS
			 From fornecedor_especifico
			 where 
			 cod_dpk in (0,p_cod_dpk) and
			 cod_subgrupo in (0,p_cod_subgrupo) and
			 cod_grupo in (0,p_cod_grupo) and
			 cod_fornecedor = p_cod_forn
			 order by cod_dpk desc,cod_subgrupo desc,
			 cod_grupo desc, cod_fornecedor;
                        
                 
       EXCEPTION
       WHEN OTHERS THEN
       p_erro := SQLCODE;
                    
                      
 END;

PROCEDURE PR_FORNECEDOR_ESPECIFICO(p_cursor       IN OUT tp_cursor,
   	    		       p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
   		               p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
   		               p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
   		               p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,  
    		               p_erro         OUT    NUMBER)
 
 IS
                        
      BEGIN
                      
      p_erro := 0;
                        
       --Abrindo Cursor
       OPEN p_cursor FOR Select fl_dif_icms,
			fl_adicional,TP_DIF_ICMS
			 From fornecedor_especifico
			 where 
			 cod_dpk in (0,p_cod_dpk) and
			 cod_subgrupo in (0,p_cod_subgrupo) and
			 cod_grupo in (0,p_cod_grupo) and
			 cod_fornecedor = p_cod_forn
			 order by cod_dpk desc,cod_subgrupo desc,
			 cod_grupo desc, cod_fornecedor;
                        
                 
       EXCEPTION
       WHEN OTHERS THEN
       p_erro := SQLCODE;
                    
                      
 END;

 PROCEDURE PR_UF_CATEG(p_cursor      IN OUT tp_cursor,
                       p_cod_uf      IN     uf.cod_uf%TYPE,
                       p_categ       IN     item_analitico.categoria%TYPE,
                       p_carac       IN     cliente.caracteristica%TYPE,
                       p_erro        OUT    NUMBER)
                      
  IS
                        
      BEGIN
                      
      p_erro := 0;
                        
       --Abrindo Cursor
       OPEN p_cursor FOR Select pc_desc_adic
			from vendas.uf_categ
			 Where cod_uf in ('UF',p_cod_uf)
			  and categoria = p_categ
			  and caracteristica = p_carac
			 Order by cod_uf asc;
                        
                 
       EXCEPTION
       WHEN OTHERS THEN
       p_erro := SQLCODE;
                    
                      
 END;
 
 PROCEDURE PR_DESC_PERIODO(p_cursor       IN OUT tp_cursor,
                           p_cod_dpk      IN     item_cadastro.cod_dpk%TYPE,
                           p_cod_subgrupo IN     item_cadastro.cod_subgrupo%TYPE,
                           p_cod_grupo    IN     item_cadastro.cod_grupo%TYPE,
                           p_cod_forn     IN     item_cadastro.cod_fornecedor%TYPE,
                           p_cod_filial   IN     filial.cod_filial%TYPE,
                           p_ocorr0       IN     tabela_descper.ocorr_preco%TYPE,
                           p_ocorr1       IN     tabela_descper.ocorr_preco%TYPE,
                           p_ocorr2       IN     tabela_descper.ocorr_preco%TYPE,
                           p_seq0         IN     tabela_descper.sequencia%TYPE,
                           p_seq1         IN     tabela_descper.sequencia%TYPE,
                           p_seq2         IN     tabela_descper.sequencia%TYPE,
                           p_erro         OUT    NUMBER)
 IS
                        
      BEGIN
                      
      p_erro := 0;
                        
       --Abrindo Cursor
       OPEN p_cursor FOR Select SEQUENCIA,
			TP_TABELA,
			 COD_FILIAL,
			 COD_FORNECEDOR,
			 COD_DPK,
			 COD_GRUPO,
			 COD_SUBGRUPO,
			 PC_DESC_PERIODO1,
			 PC_DESC_PERIODO2,
			 PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and  
			sequencia = p_seq0 and
			tp_tabela=0 and
			ocorr_preco = p_ocorr0 and
			cod_fornecedor = p_cod_forn and
			cod_filial = 0 and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			Union 
			select SEQUENCIA,
			TP_TABELA,
			COD_FILIAL,
			COD_FORNECEDOR,
			COD_DPK,
			COD_GRUPO,
			COD_SUBGRUPO,
			PC_DESC_PERIODO1,
			PC_DESC_PERIODO2,
			PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and
			sequencia = p_seq0 and
			tp_tabela=0 and
			ocorr_preco = p_ocorr0 and
			cod_fornecedor = p_cod_forn and
			cod_filial = p_cod_filial and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			Union 
			select SEQUENCIA,
			TP_TABELA,
			COD_FILIAL,
			COD_FORNECEDOR,
			COD_DPK,
			COD_GRUPO,
			COD_SUBGRUPO,
			PC_DESC_PERIODO1,
			PC_DESC_PERIODO2,
			PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and  
			sequencia = p_seq1 and
			tp_tabela=1 and
			ocorr_preco = p_ocorr1 and
			cod_fornecedor = p_cod_forn and
			cod_filial = 0 and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			Union 
			select SEQUENCIA,
			TP_TABELA,
			COD_FILIAL,
			COD_FORNECEDOR,
			COD_DPK,
			COD_GRUPO,
			COD_SUBGRUPO,
			PC_DESC_PERIODO1,
			PC_DESC_PERIODO2,
			PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and  
			sequencia = p_seq1 and
			tp_tabela=1 and
			ocorr_preco = p_ocorr1 and
			cod_fornecedor = p_cod_forn and
			cod_filial = p_cod_filial and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			Union
			select SEQUENCIA,
			TP_TABELA,
			COD_FILIAL,
			COD_FORNECEDOR,
			COD_DPK,
			COD_GRUPO,
			COD_SUBGRUPO,
			PC_DESC_PERIODO1,
			PC_DESC_PERIODO2,
			PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and  
			sequencia = p_seq2 and
			tp_tabela=2 and
			ocorr_preco = p_ocorr2 and
			cod_fornecedor = p_cod_forn and
			cod_filial = 0 and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			Union 
			select SEQUENCIA,
			TP_TABELA,
			COD_FILIAL,
			COD_FORNECEDOR,
			COD_DPK,
			COD_GRUPO,
			COD_SUBGRUPO,
			PC_DESC_PERIODO1,
			PC_DESC_PERIODO2,
			PC_DESC_PERIODO3 
			from TABELA_DESCPER 
			where SITUACAO = 0 and
			sequencia = p_seq2 and
			tp_tabela=2 and
			ocorr_preco = p_ocorr2 and
			cod_fornecedor = p_cod_forn and
			cod_filial = p_cod_filial and
			cod_grupo in (0,p_cod_grupo) and
			cod_subgrupo in (0,p_cod_subgrupo) and
			cod_dpk in (0,p_cod_dpk) 
			order by 2 desc,3 desc, 5 desc,7 desc, 6 desc;

                 
       EXCEPTION
       WHEN OTHERS THEN
       p_erro := SQLCODE;
                    
                      
 END;                          
 
 PROCEDURE PR_UF_ORIGEM_DESTINO(p_cursor         IN OUT tp_cursor,
                       		p_cod_uf_origem  IN     uf.cod_uf%TYPE,
                      		p_cod_uf_destino IN     uf.cod_uf%TYPE,
                           	p_erro        OUT    NUMBER)
 IS
                         
       BEGIN
                       
       p_erro := 0;
                         
        --Abrindo Cursor
        OPEN p_cursor FOR select PC_ICM,PC_DIFICM 
 			from UF_ORIGEM_DESTINO 
 			where COD_UF_ORIGEM = p_cod_uf_origem and
 			      COD_UF_DESTINO = p_cod_uf_destino;
                         
                  
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
                     
                       
  END;
  
  
 PROCEDURE PR_SUBST_TRIB(p_cursor         IN OUT tp_cursor,
 			 p_class          IN     item_cadastro.class_fiscal%TYPE,
                         p_cod_uf_origem  IN     uf.cod_uf%TYPE,
                   	 p_cod_uf_destino IN     uf.cod_uf%TYPE,
                         p_erro           OUT    NUMBER)

 IS
                         
       BEGIN
                       
       p_erro := 0;
                         
        --Abrindo Cursor
        OPEN p_cursor FOR Select cod_trib_revendedor,
			cod_trib_inscrito,
			cod_trib_isento,
			cod_trib_Tare
			From compras.subst_tributaria
			 Where class_fiscal = p_class and 
			 cod_uf_origem = p_cod_uf_origem and
			 cod_uf_destino = p_cod_uf_destino;
                         
                  
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
                     
                       
  END;
  
PROCEDURE PR_LOJA(p_cursor   IN OUT tp_cursor,
                  p_cod_loja IN     loja.cod_loja%TYPE,
                  p_erro     OUT    NUMBER)
 
IS
                         
       BEGIN
                       
       p_erro := 0;
                         
        --Abrindo Cursor
        OPEN p_cursor FOR Select b.cod_uf  cod_uf
			From loja a, cidade b
			Where a.cod_cidade = b.cod_cidade and
			a.cod_loja= p_cod_loja;
                         
                  
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
                     
                       
  END; 
 
 PROCEDURE PR_CLIENTE1(p_cursor       IN OUT tp_cursor,
   		       p_nome_cliente IN     cliente.nome_cliente%TYPE,
                       p_erro         OUT    NUMBER)
                       
 IS
                         
       BEGIN
                       
       p_erro := 0;
                         
        --Abrindo Cursor
        OPEN p_cursor FOR select cli.COD_CLIENTE,cli.NOME_CLIENTE,
			cid.nome_cidade, cid.cod_uf
			from CLIENTE cli,CIDADE cid
			where cli.COD_CIDADE = cid.COD_CIDADE and
			 cli.SITUACAO = 0
			and NOME_CLIENTE like p_nome_cliente
			order by cli.NOME_CLIENTE;

                         
                  
        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;
                     
                       
  END;
  
 
 
END PCK_VDA320;  