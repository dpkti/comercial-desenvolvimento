VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmRecado 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recado para Outro Operador"
   ClientHeight    =   3945
   ClientLeft      =   1140
   ClientTop       =   1545
   ClientWidth     =   6585
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3945
   ScaleWidth      =   6585
   Begin Threed.SSFrame ssfraAgenda 
      Height          =   3615
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   6255
      _Version        =   65536
      _ExtentX        =   11033
      _ExtentY        =   6376
      _StockProps     =   14
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox txtVendedor 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   960
         MaxLength       =   4
         TabIndex        =   0
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox txtObs 
         ForeColor       =   &H00800000&
         Height          =   1695
         Left            =   960
         MaxLength       =   100
         MultiLine       =   -1  'True
         TabIndex        =   2
         Top             =   1080
         Width           =   4815
      End
      Begin Threed.SSCommand sscmdGrava_Recado 
         Height          =   495
         Left            =   4440
         TabIndex        =   3
         Top             =   2880
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmRecado.frx":0000
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   5280
         TabIndex        =   4
         Top             =   2880
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "frmRecado.frx":031A
      End
      Begin VB.Label lblPseudonimo 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1800
         TabIndex        =   8
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Label3 
         Caption         =   "Vendedor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Height          =   255
         Left            =   720
         TabIndex        =   6
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label8 
         Caption         =   "Recado"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1560
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmRecado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Text1_Change()

End Sub

Private Sub sscmdGrava_Recado_Click()
    If txtVendedor = "" Then
        MsgBox "Favor escolher o vendedor que dever� receber o recado", vbInformation, "Aten��o"
        Exit Sub
    End If

    If txtObs = "" Then
        MsgBox "Favor incluir o recado", vbInformation, "Aten��o"
        Exit Sub
    End If

    sql = "Begin producao.pck_vda230.pr_insere_recado(:vend,:cliente,:recado,:vErro);END;"

    OraParameters.Remove "vend"
    OraParameters.Add "vend", txtVendedor, 1
    OraParameters.Remove "cliente"
    OraParameters.Add "cliente", frmVenda.txtCod_cliente, 1
    OraParameters.Remove "recado"
    OraParameters.Add "recado", "VEND.:" & sCOD_VEND & ": " & UCase(txtObs), 1

    oradatabase.ExecuteSQL sql

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Resume Next
    End If
    Unload Me

End Sub

Private Sub SSCommand3_Click()
    txtVendedor = ""
    lblPseudonimo = ""
    txtObs = ""
End Sub

Private Sub SSCommand5_Click()

End Sub

Private Sub txtObs_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)

End Sub

Private Sub txtVendedor_DblClick()
    txtVendedor.DataChanged = False
    frmVendedor.Show vbModal
    txtVendedor.Text = txtResposta
    Call txtVendedor_LostFocus


End Sub

Private Sub txtVendedor_KeyPress(KeyAscii As Integer)
    KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtVendedor_LostFocus()
    Dim ss As Object


    If txtVendedor <> "" Then
        sql = "Select pseudonimo, situacao, tipo"
        sql = sql & " From representante"
        sql = sql & " Where cod_repres = :repres AND SITUACAO=0 AND TIPO IN ('A','M','V')"

        oradatabase.Parameters.Remove "repres"
        oradatabase.Parameters.Add "repres", txtVendedor, 1

        Set ss = oradatabase.dbcreatedynaset(sql, 8&)
        If ss.EOF And ss.BOF Then
            MsgBox "Vendedor n�o localizado, verifique", vbInformation, "Aten��o"
            Exit Sub
        Else
            lblPseudonimo = ss!pseudonimo
        End If
    End If

End Sub
