VERSION 5.00
Begin VB.Form frmLogin 
   Caption         =   "Login"
   ClientHeight    =   1860
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   3180
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1860
   ScaleWidth      =   3180
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1643
      TabIndex        =   4
      Top             =   1335
      Width           =   975
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   563
      TabIndex        =   3
      Top             =   1335
      Width           =   975
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Informa��es do usu�rio"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1140
      Left            =   10
      TabIndex        =   0
      Top             =   60
      Width           =   3120
      Begin VB.TextBox txtUsuario 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   855
         MaxLength       =   20
         TabIndex        =   1
         Top             =   270
         Width           =   2130
      End
      Begin VB.TextBox txtSenha 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   330
         IMEMode         =   3  'DISABLE
         Left            =   855
         MaxLength       =   20
         PasswordChar    =   "l"
         TabIndex        =   2
         Top             =   675
         Width           =   2130
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         BackStyle       =   0  'Transparent
         Caption         =   "Usu�rio:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   690
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackStyle       =   0  'Transparent
         Caption         =   "Senha:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   5
         Top             =   720
         Width           =   690
      End
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmLogin
' Author    : c.samuel.oliveira
' Date      : 29/12/15
' Purpose   : TI-3818
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmLogin
' Author    : c.samuel.oliveira
' Date      : 28/09/15
' Purpose   : TI-2807
'---------------------------------------------------------------------------------------
Option Explicit
Private Sub cmdCancelar_Click()
    
    End
     
End Sub

Private Sub cmdOk_Click()
    On Error GoTo TrataErro

    Dim rs As Object
    Dim vSQL As String

    'TI-3818
    If Len(Trim(txtUsuario.Text)) = 0 Then
        MsgBox "Preencha o campo Usu�rio!", vbInformation, "VDA320 - Login"
        Exit Sub
    
    End If
    
    If Len(Trim(txtSenha.Text)) = 0 Then
        MsgBox "Preencha o campo Senha!", vbInformation, "VDA320 - Login"
        Exit Sub
    End If
    'FIM TI-3818
    
    'criar consulta
    Criar_Cursor
    OraParameters.Remove "login"
    OraParameters.Add "login", txtUsuario.Text, 1
    OraParameters.Remove "senha"
    OraParameters.Add "senha", txtSenha.Text, 1

    vSQL = "Begin producao.pck_vda320.pr_sel_login(:vCursor,:login,:senha,:vErro);END;"

    oradatabase.ExecuteSQL vSQL

    Set rs = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        GoTo TrataErro
    End If
                
    If rs.EOF And rs.BOF Then
        MsgBox "Usu�rio n�o encontrado!", vbInformation, "VDA320 - Login"
        Exit Sub
    
    Else
    
        If rs!TP_USUARIO = "0" Then
            MsgBox "Usu�rio sem permiss�o!", vbInformation, "VDA320 - Login"
            Exit Sub
        End If
    
    End If
    
    Unload Me
    
    Exit Sub
    
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 5 Or Err = 30009 Then
        Resume Next
    Else
        Call Process_Line_Errors(sql)
    End If
    
End Sub

Private Sub txtSenha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then cmdOk_Click
End Sub
