VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{00025600-0000-0000-C000-000000000046}#4.6#0"; "crystl32.ocx"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "CONSULTA ITEM"
   ClientHeight    =   4245
   ClientLeft      =   1620
   ClientTop       =   1845
   ClientWidth     =   6675
   Icon            =   "VDA230.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   Begin Threed.SSPanel SSpMsg 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   3945
      Width           =   6675
      _Version        =   65536
      _ExtentX        =   11774
      _ExtentY        =   529
      _StockProps     =   15
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   1
   End
   Begin Threed.SSPanel sspMenu 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6675
      _Version        =   65536
      _ExtentX        =   11774
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand5 
         Height          =   495
         Left            =   1320
         TabIndex        =   6
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "VDA230.frx":0CCA
      End
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   2520
         TabIndex        =   5
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "VDA230.frx":0FE4
      End
      Begin Threed.SSCommand SSCommand3 
         Height          =   495
         Left            =   1920
         TabIndex        =   4
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "VDA230.frx":12FE
      End
      Begin Threed.SSCommand SSCommand2 
         Height          =   495
         Left            =   720
         TabIndex        =   3
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "VDA230.frx":1618
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         Picture         =   "VDA230.frx":1932
      End
   End
   Begin Crystal.CrystalReport rptGeral 
      Left            =   120
      Top             =   5760
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   262150
      WindowBorderStyle=   1
      WindowControlBox=   -1  'True
      WindowMaxButton =   0   'False
      WindowMinButton =   0   'False
   End
   Begin VB.Menu mnuVendas 
      Caption         =   "&Vendas"
   End
   Begin VB.Menu mnuCotacao 
      Caption         =   "Co&ta��o"
      Begin VB.Menu mnuCotacaoManutencao 
         Caption         =   "&Manuten��o"
      End
      Begin VB.Menu mnuCotacaoPed 
         Caption         =   "&Gerar Pedido"
      End
   End
   Begin VB.Menu mnuAgenda 
      Caption         =   "&Agenda"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : MDIForm1
' Author    : c.samuel.oliveira
' Date      : 29/03/2019
' Purpose   : DPK-74
'---------------------------------------------------------------------------------------
'---------------------------------------------------------------------------------------
' Module    : MDIForm1
' Author    : c.samuel.oliveira
' Date      : 29/12/15
' Purpose   : TI-3818
'---------------------------------------------------------------------------------------
'---------------------------------------------------------------------------------------
' Module    : MDIForm1
' Author    : c.samuel.oliveira
' Date      : 28/09/15
' Purpose   : TI-2807
'---------------------------------------------------------------------------------------
Option Explicit

Private Sub MDIForm_Load()
    On Error GoTo TrataErro

    Dim strLogin As String
    Dim ss As Object
    Dim strMsg As String


    If App.PrevInstance Then
        MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
        End
    End If


    'TI-5495
    'Call Get_CD(Mid(App.Path, 1, 1))
    'Verifica se foi informado par�metro no atalho do sistema.  (Exemplos: CD01U, CD04M, CD10M)
    If Mid(Command$, InStr(1, Command$, "CD") + 2, 2) = "" Or Mid(Command$, InStr(1, Command$, "CD") + 4) = "" Then
        MsgBox "Par�metro de inicializa��o do CD n�o informado corretamente.  Informe o depto. Sistemas", vbCritical, "Aten��o"
        End
    Else
        lngCD = Mid(Command$, InStr(1, Command$, "CD") + 2, 2)
        strTp_banco = Mid(Command$, InStr(1, Command$, "CD") + 4)
    End If
    'TI-5495(FIM)
    

    If strTp_banco = "U" Then
        strTabela_Banco = "PRODUCAO."
    Else
        strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
    End If


    'fl_banco = 0      'TI-5495
    'posicionar tela
    MDIForm1.Top = 0
    MDIForm1.Left = 0
    MDIForm1.Width = Screen.Width
    MDIForm1.Height = Screen.Height


    ' If Command$ = "V" Then
    'carregar nome do vendedor
    sCOD_VEND = 599
    'Else
    'sCOD_VEND = CInt(Environ("USER"))
    ' sCOD_VEND = CInt(BuscaUSERNAME)
    'End If


    'path na rede
    'strPath = "H:\ORACLE\DADOS\32BITS\"    'TI-5495
    strPathUsuario = "G:\USR\" & sCOD_VEND & "\"
    strPath_Foto = "H:\oracle\dados\fotos\"
    'strPath = "F:\ORACLE\DADOS\32BITS\"
    'strPathUsuario = "F:\USR\" & sCOD_VEND & "\"
    'strPath_Foto = "F:\oracle\dados\fotos\"
    Operacao = "C:\TESSW\OPERACAO.TXT"

    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "vda320/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("desenv", "producao/des", 0&)
    'Set oradatabase = orasession.OpenDatabase("sdpk_teste", "vda320/prod", 0&)


    Set OraParameters = oradatabase.Parameters
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2
    OraParameters("vErro").serverType = ORATYPE_NUMBER


    'Conexao Access
    ''TI-5495
    'Set dbAccess = OpenDatabase(strPath & "VENDAS.MDB")
    'fl_banco = 1

    Criar_Cursor

    'carregar data de faturamento,uf origem, filial origem
    sql = "select a.dt_faturamento, a.dt_real, "
    sql = sql & " to_char(b.cod_filial,'0009') || '-' || c.sigla filial, "
    sql = sql & " d.cod_uf cod_uf,"
    sql = sql & " to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default"
    sql = sql & " from datas a, " & strTabela_Banco & "deposito b, filial c,  cidade d, loja e "
    sql = sql & " where  b.cod_filial=c.cod_filial and "
    sql = sql & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "

    Set ss = oradatabase.dbcreatedynaset(sql, 0&)

    'DPK-74
    If ss.EOF And ss.BOF Then
        Call Process_Line_Errors(sql)
    Else
'        Data_Faturamento = CDate(ss!dt_faturamento)
'        data_real = CDate(ss!Dt_Real)
'        FILIAL_PED = ss!filial
'        uf_origem = ss!cod_uf
'        deposito_default = ss!deposito_default
        For i = 1 To ss.RecordCount
            If Val(lngCD) = Val(ss!deposito_default) Then
                Data_Faturamento = CDate(ss!dt_faturamento)
                data_real = CDate(ss!Dt_Real)
                FILIAL_PED = ss!filial
                uf_origem = ss!cod_uf
                deposito_default = ss!deposito_default
            End If
            ss.MoveNext
        Next
        'FIM DPK-74
    End If

    '   OraParameters.Remove "cod"
    '   OraParameters.Add "cod", sCOD_VEND, 1
    '   OraParameters.Remove "cota"
    '  OraParameters.Add "cota", 0, 2

    ' V_SQL = "Begin producao.pck_vda230.pr_cota_dia(:cod,:cota,:vErro);END;"


    'oradatabase.ExecuteSQL V_SQL
    'If Val(oradatabase.Parameters("vErro")) <> 0 Then
    '  dblVL_Cota_Dia = 0
    'Else
    ' dblVL_Cota_Dia = Val(oradatabase.Parameters("cota"))
    'End If
    '      frmBF.Show 1
    Call SSCommand1_Click

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    ElseIf Err = 13 Then
        strMsg = "PARA EXECUTAR ESTE PROGRAMA � PRECISO ESTAR LOGADO"
        strMsg = strMsg & " NA REDE COMO UM TLMKT (C�DIGO DE VENDA)."
        'strMsg = strMsg & " VOC� EST� LOGADO COMO: '" & Environ("USER") & "'"
        MsgBox strMsg, vbExclamation, "ATEN��O"
        End
    ElseIf Err = 440 Then
        If Err.Description Like "*ORA-02391*" Then
            strMsg = "ESTE USU�RIO J� EST� LOGADO NO BANCO DE DADOS, "
            strMsg = strMsg & " LIGUE NO CPD E SOLICITE PARA MATAR O USU�RIO: "
            strMsg = strMsg & "TLM_" & sCOD_VEND
            MsgBox strMsg, vbExclamation, "ATEN��O"
            End
        Else
            Call Process_Line_Errors(sql)
        End If
    Else
        Call Process_Line_Errors(sql)
    End If
End Sub


Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = ""

End Sub


Private Sub MDIForm_Unload(Cancel As Integer)
    'dbAccess.Close      'TI-5495
    End
End Sub




Private Sub mnuAgenda_Click()

    frmAgenda.Show 1

End Sub

Private Sub mnuCotacaoManutencao_Click()
    On Error GoTo TrataErro


    Dim ss2 As Object
    Dim PLSQL As String


    'verificar cota��o pendente
    sql = "select NUM_COTACAO, COD_REPRES "
    sql = sql & "from VENDAS.COTACAO "
    sql = sql & "where COD_VEND IN (:vend)"
    sql = sql & " and SITUACAO = 8"

    oradatabase.Parameters.Remove "vend"
    oradatabase.Parameters.Add "vend", CStr(sCOD_VEND), 1

    Set ss2 = oradatabase.dbcreatedynaset(sql, 8&)

    If ss2.EOF() And ss2.BOF Then
        'n�o ha cotacao pendente
        lngNUM_COTACAO = 0
    Else
        lngNUM_COTACAO = ss2!NUM_COTACAO
    End If



    If lngNUM_COTACAO = 0 Then
        SSpMsg.Visible = False
        txtResposta = "0"
        frmListaCotacao.Show vbModal
        If txtResposta <> "0" And txtResposta <> "" Then
            'coloca cotacao em manuten��o
            PLSQL = "BEGIN"
            PLSQL = PLSQL & " Update vendas.cotacao set situacao=8"
            '  If CLng(sCOD_VEND) <> 599 Then
            '     PLSQL = PLSQL & " ,cod_vend = :vend,"
            '    PLSQL = PLSQL & " COD_REPRES = :repres"
            '  End If
            PLSQL = PLSQL & " where num_cotacao = :cot and cod_loja = :loja;"
            PLSQL = PLSQL & " COMMIT;"
            PLSQL = PLSQL & "EXCEPTION"
            PLSQL = PLSQL & " WHEN OTHERS THEN"
            PLSQL = PLSQL & " ROLLBACK;"
            PLSQL = PLSQL & " :cod_errora := SQLCODE;"
            PLSQL = PLSQL & " :txt_errora := SQLERRM;"
            PLSQL = PLSQL & "END;"

            oradatabase.Parameters.Remove "cot"
            oradatabase.Parameters.Add "cot", txtResposta, 1
            oradatabase.Parameters.Remove "loja"
            oradatabase.Parameters.Add "loja", lngCod_loja, 1
            oradatabase.Parameters.Remove "vend"
            oradatabase.Parameters.Add "vend", CStr(sCOD_VEND), 1
            oradatabase.Parameters.Remove "repres"
            oradatabase.Parameters.Add "repres", lngCod_repres, 1
            oradatabase.Parameters.Remove "cod_errora"
            oradatabase.Parameters.Add "cod_errora", 0, 2
            oradatabase.Parameters.Remove "txt_errora"
            oradatabase.Parameters.Add "txt_errora", "", 2

            oradatabase.ExecuteSQL PLSQL

            vErro = IIf(oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
            If vErro <> 0 Then
                MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
                Exit Sub
            End If

            Unload frmVenda
            'Set frmVenda = Nothing
            'ativa a venda
            'frmVenda.Top = 20
            'frmVenda.Left = 20
            'frmVenda.Width = 9400
            'frmVenda.Height = 6700
            frmVenda.Top = 15
            frmVenda.Left = -25
            frmVenda.Width = 11705
            frmVenda.Height = 8310

            frmVenda.Show

        End If
        Screen.MousePointer = vbDefault
    Else
        MsgBox "Cota��o " & lngNUM_COTACAO & " est� pendente. Finalize-a.", vbExclamation, "Aten��o"
    End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 440 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        Call Process_Line_Errors(sql)
    End If

End Sub

Private Sub mnuCotacaoPed_Click()
    On Error GoTo TrataErro


    Dim ss As Snapshot
    Dim ss1 As Object

    'verificar pedido pendente
    sql = "select NUM_PENDENTE "
    sql = sql & "from PEDIDO "
    sql = sql & "where FL_PEDIDO_FINALIZADO = 'N' and COD_VEND = " & CStr(sCOD_VEND)
    'Set ss = dbAccess.CreateSnapshot(sql)  'TI-5495
    FreeLocks
    If ss.EOF() And ss.BOF Then
        'n�o ha pedido pendente
        lngNUM_PEDIDO = 0
    Else
        lngNUM_PEDIDO = ss!num_pendente
    End If
    ss.Close

    If lngNUM_PEDIDO = 0 Then
        SSpMsg.Visible = False
        txtResposta = "0"
        Unload frmVenda
        frmListaCotacao.Show vbModal
        If txtResposta <> "0" And txtResposta <> "" Then

            'VERIFICA SE COTA��O � VALIDA PARA GERAR PEDIDO
            DoEvents
            OraParameters.Remove "loja"
            OraParameters.Add "loja", lngCod_loja, 1
            OraParameters.Remove "cotacao"
            OraParameters.Add "cotacao", txtResposta, 1

            Criar_Cursor
            V_SQL = "Begin producao.pck_vda230.pr_cot_inv(:vCursor,:loja,:cotacao,:vErro);END;"

            oradatabase.ExecuteSQL V_SQL
            DoEvents
            Set ss1 = oradatabase.Parameters("vCursor").Value
            If ss1!total <> 0 Then
                MsgBox "Cota��o est� com a validade vencida, n�o � poss�vel transformar em pedido", vbInformation, "Aten��o"
                Exit Sub
            Else
                DoEvents
                V_SQL = "Begin producao.pck_vda230.pr_itemcot_inv(:vCursor,:loja,:cotacao,:vErro);END;"
                Criar_Cursor
                oradatabase.ExecuteSQL V_SQL
                DoEvents
                Set ss1 = oradatabase.Parameters("vCursor").Value
                If ss1!total <> 0 Then
                    MsgBox "Cota��o possui itens com condi��o da semana anterior, n�o � poss�vel transformar em pedido", vbInformation, "Aten��o"
                    Exit Sub
                End If

                'gera pedido atrav�s de cotacao
                frmGerarPedido.Show vbModal
            End If

            If lngNUM_PEDIDO > 0 Then
                'finalizar pedido
                txtResposta = "Gerar Pedido Atraves de Cotacao"

                frmFimPedido.Show vbModal
                txtResposta = ""
            End If

        End If
        Screen.MousePointer = vbDefault
    Else
        MsgBox "Pedido " & lngNUM_PEDIDO & " pendente. Finalize-o.", vbExclamation, "Aten��o"
    End If
    FreeLocks
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    Else
        Call Process_Line_Errors(sql)
    End If
End Sub


Private Sub mnuSair_Click()
    Set frmFabrica = Nothing
    Set frmVenda = Nothing
    Set frmAplicacao = Nothing
    Set frmGerarPedido = Nothing
    Set frmFimCotacao = Nothing
    Set frmFimPedido = Nothing
    Set frmAgenda = Nothing
    Set frmBanco = Nothing
    Set frmClienteFimPedido = Nothing
    Set frmListaCotacao = Nothing
    Set frmFornecedor = Nothing
    Set frmPlano = Nothing
    Set frmRepresentante = Nothing
    Set frmTransp = Nothing
    Set frmVendedor = Nothing
    End
End Sub


Private Sub mnuSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub mnuVendas_Click()
    If Not LoginAD Then End 'TI-3818
    frmLogin.Show 1 'TI-2807
    Screen.MousePointer = vbHourglass
    SSpMsg.Visible = False
    'frmVenda.Top = 20
    'frmVenda.Left = 20
    'frmVenda.Width = 9400
    'frmVenda.Height = 6700
    frmVenda.Top = 15
    frmVenda.Left = -25
    frmVenda.Width = 11705
    frmVenda.Height = 8310

    frmVenda.Show
    Screen.MousePointer = vbDefault
End Sub




Private Sub SSCommand1_Click()
    Call mnuVendas_Click
End Sub

Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = "VENDA"

End Sub


Private Sub SSCommand2_Click()
    Call mnuCotacaoManutencao_Click
End Sub

Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = "MANUTEN��O DE COTA��O"

End Sub


'Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
' SSpMsg.Caption = "MANUTEN��O DE COTA��O"
'End Sub


Private Sub SSCommand3_Click()
    Call mnuAgenda_Click
End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = "AGENDA"

End Sub


Private Sub SSCommand4_Click()
    Call mnuSair_Click
End Sub


Private Sub SSCommand4_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = "SAIR DO PROGRAMA"

End Sub


Private Sub SSCommand5_Click()
    Call mnuCotacaoPed_Click
End Sub

Private Sub SSCommand5_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    SSpMsg.Caption = "GERAR PEDIDO ATRAV�S DA COTA��O"

End Sub

'TI-3818
Private Function LoginAD() As Boolean
    On Error GoTo TrataErro

    Dim rs As Object
    Dim vSQL As String
    Dim vUsuario As String
    vUsuario = "CAL\" & UCase(BuscaUSERNAME)

    'criar consulta
    Criar_Cursor
    OraParameters.Remove "login"
    OraParameters.Add "login", UCase(BuscaUSERNAME), 1
    OraParameters.Remove "senha"
    OraParameters.Add "senha", "", 1
    
    vSQL = "Begin producao.pck_vda320.pr_sel_login(:vCursor,:login,:senha,:vErro);END;"

    oradatabase.ExecuteSQL vSQL

    Set rs = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        GoTo TrataErro
    End If

    If Not rs.EOF And Not rs.BOF Then

        MsgBox "Usu�rio de m�quina " & vUsuario & " � do tipo Vendedor, " & vbCrLf & "esse tipo n�o tem permiss�o ao sistema! Verifique com seu Gerente.", vbInformation, "VDA320 - Login"
        Exit Function
    
    End If
        
    LoginAD = True
    
    Exit Function
    
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 5 Or Err = 30009 Then
        Resume Next
    Else
        Call Process_Line_Errors(sql)
    End If
End Function
'FIM TI-3818
