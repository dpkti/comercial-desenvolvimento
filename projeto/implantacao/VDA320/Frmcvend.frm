VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmCVend 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5370
   ClientLeft      =   810
   ClientTop       =   1110
   ClientWidth     =   9090
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5370
   ScaleWidth      =   9090
   Begin VB.TextBox Text1 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1920
      TabIndex        =   0
      Top             =   960
      Width           =   3855
   End
   Begin VB.CommandButton Command1 
      Caption         =   "In�cio"
      Height          =   255
      Left            =   6000
      TabIndex        =   1
      Top             =   960
      Width           =   1095
   End
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   120
      Picture         =   "Frmcvend.frx":0000
      ScaleHeight     =   255
      ScaleWidth      =   855
      TabIndex        =   7
      Top             =   4320
      Visible         =   0   'False
      Width           =   855
   End
   Begin Threed.SSCommand SSCommand4 
      Height          =   495
      Left            =   8160
      TabIndex        =   15
      Top             =   4200
      Visible         =   0   'False
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "Frmcvend.frx":030A
   End
   Begin Threed.SSCommand SSCommand3 
      Height          =   495
      Left            =   120
      TabIndex        =   12
      Top             =   4800
      Visible         =   0   'False
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      ForeColor       =   -2147483640
      Picture         =   "Frmcvend.frx":075C
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   6480
      TabIndex        =   8
      Top             =   120
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "Frmcvend.frx":0A76
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   495
      Left            =   7320
      TabIndex        =   6
      Top             =   120
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "Frmcvend.frx":0D90
   End
   Begin MSGrid.Grid Grid1 
      Height          =   2775
      Left            =   360
      TabIndex        =   5
      Top             =   1320
      Visible         =   0   'False
      Width           =   8535
      _Version        =   65536
      _ExtentX        =   15055
      _ExtentY        =   4895
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      Cols            =   1
      FixedCols       =   0
      MouseIcon       =   "Frmcvend.frx":10AA
   End
   Begin VB.Label lblOK 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0080FFFF&
      Caption         =   "Contatos OK"
      Height          =   195
      Left            =   8160
      TabIndex        =   16
      Top             =   4560
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Label lblSair 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0080FFFF&
      Caption         =   "Sair"
      Height          =   195
      Left            =   8160
      TabIndex        =   14
      Top             =   600
      Visible         =   0   'False
      Width           =   285
   End
   Begin VB.Label lblLista 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H0080FFFF&
      Caption         =   "Refazer Lista"
      Height          =   195
      Left            =   6240
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   945
   End
   Begin VB.Label Label4 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4680
      TabIndex        =   11
      Top             =   4320
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "TOTAL DE LIGA��ES A EXECUTAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1320
      TabIndex        =   10
      Top             =   4320
      Width           =   3255
   End
   Begin VB.Label Label1 
      Caption         =   "PESQUISA"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   840
      TabIndex        =   9
      Top             =   960
      Width           =   975
   End
   Begin VB.Label lblPseudonimo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2640
      TabIndex        =   4
      Top             =   360
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCod_vend 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1920
      TabIndex        =   3
      Top             =   360
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "VENDEDOR"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   840
      TabIndex        =   2
      Top             =   360
      Width           =   975
   End
End
Attribute VB_Name = "frmCVend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'VARIAVEIS UTILIZADAS PARA PESQUISA
Dim CONTINUAPESQUISA As Integer
Dim OCORRENCIA As Integer
Dim CONTADOROCORRENCIA As Long
Dim TEXTOPARAPROCURA As String
Dim INICIOPESQUISA As Long
Dim INILINHA As Long


Private Sub Command1_Click()
    Call Text1_LostFocus
End Sub

Private Sub Form_Load()
    Dim ss As Object
    Dim ss1 As Object
    Dim sql As String
    Dim i As Integer

    Dim V_SQL As String



    On Error GoTo Trata_Erro
    Screen.MousePointer = 11
    frmCVend.lblCod_vend = sCOD_VEND
    'frmCVend.LBLPSEUDONIMO = strPseudo

    frmCVend.Caption = "LISTA DE LIGA��ES A EXECUTAR"
    Set OraParameters = oradatabase.Parameters
    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    OraParameters.Remove "vend"
    OraParameters.Add "vend", sCOD_VEND, 1


    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2
    OraParameters("vErro").serverType = ORATYPE_NUMBER

    V_SQL = "Begin " & strTabela_Banco & "pck_vda630.pr_ligacao(:vCursor,:vend,:vErro);END;"

    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set VarRec = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(sql)
        Exit Sub
    End If
    If VarRec.EOF And VarRec.BOF Then
        MsgBox "N�o h� liga��es para executar", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    Else
        tot_grid = VarRec.RecordCount
        Label4 = tot_grid
        With frmCVend.Grid1
            .Cols = 11
            .Rows = tot_grid + 1
            .ColWidth(0) = 500
            .ColWidth(1) = 2000
            .ColWidth(2) = 1300
            .ColWidth(3) = 600
            .ColWidth(4) = 1000
            .ColWidth(5) = 800
            .ColWidth(6) = 600
            .ColWidth(7) = 1000
            .ColWidth(8) = 300
            .ColWidth(9) = 600
            .ColWidth(10) = 1000

            .Row = 0
            .Col = 0
            .Text = "COD"
            .Col = 1
            .Text = "CLIENTE"
            .Col = 2
            .Text = "CONTATO"
            .Col = 3
            .Text = "DDD1"
            .Col = 4
            .Text = "FONE1"
            .Col = 5
            .Text = "DT.LIG."
            .Col = 6
            .Text = "RESP."
            .Col = 7
            .Text = "CIDADE"
            .Col = 8
            .Text = "UF"
            .Col = 9
            .Text = "DDD2"
            .Col = 10
            .Text = "FONE2"


            VarRec.MoveFirst
            For i = 1 To .Rows - 1
                .Row = i

                .Col = 0
                .Text = VarRec!COD_CLIENTE
                .Col = 1
                .Text = VarRec!NOME_CLIENTE
                .Col = 2
                .Text = IIf(IsNull(VarRec!nome_contato), "", VarRec!nome_contato)
                .Col = 3
                .Text = IIf(IsNull(VarRec!ddd1), 0, VarRec!ddd1)
                .Col = 4
                .Text = IIf(IsNull(VarRec!fone1), 0, VarRec!fone1)
                .Col = 5
                .Text = VarRec!dt_ligacao
                .Col = 6
                .Text = VarRec!cod_repres
                .Col = 7
                .Text = VarRec!nome_cidade
                .Col = 8
                .Text = VarRec!cod_uf
                .Col = 9
                .Text = IIf(IsNull(VarRec!ddd2), 0, VarRec!ddd2)
                .Col = 10
                .Text = IIf(IsNull(VarRec!fone2), 0, VarRec!fone2)


                VarRec.MoveNext
            Next
            .Row = 1
        End With
        Grid1.Visible = True
        frmCVend.Top = 20
        frmCVend.Width = 9500
        frmCVend.Left = 20
        frmCVend.Height = 6200

    End If
    Screen.MousePointer = 0
    Exit Sub
Trata_Erro:
    If Err = 364 Then
        Exit Sub
    Else
        Call Process_Line_Errors(sql)
    End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblLista.Visible = False
    lblSair.Visible = False
End Sub


Private Sub Grid1_Click()
'Grava o codigo do cliente em arquivo
'texto para ser lido pelo programa tlmkt
    On Error GoTo Trata_Erro
    ' num_arq = FreeFile
    ' Open "C:\TESSW\CLIENTE.TXT" For Output As #num_arq

    Grid1.Col = 0

    'Print #num_arq, CStr(Grid1.Text)

    'Close #num_arq
    frmVenda.txtCod_cliente.Text = ""
    frmVenda.txtCod_cliente.Text = CStr(Grid1.Text)
    frmVenda.txtCod_cliente.DataChanged = True
    frmVenda.txtCod_cliente.SetFocus
    Unload Me
    'frmVenda.txtcod_cliente.LostFocus


    Exit Sub
Trata_Erro:
    If Err = 76 Then
        MkDir "C:\TESSW"
        Resume
    End If
End Sub

Private Sub Grid1_DblClick()
'  Dim pedido As String
'  Dim lngCod_cliente As Long

' Grid1.Col = 0

' strOrigem_Ligacao = "L"

'sql = "BEGIN"
'sql = sql & " Update vendas.ligacao set"
'sql = sql & " situacao=9"
'sql = sql & " where cod_cliente =:cod;"
'sql = sql & " COMMIT;"
'sql = sql & "EXCEPTION"
'sql = sql & " WHEN OTHERS THEN"
'sql = sql & " ROLLBACK;"
'sql = sql & " :COD_ERRORA := SQLCODE;"
'sql = sql & " :TXT_ERRORA := SQLERRM;"
'sql = sql & "END;"

'oradatabase.Parameters.Remove "cod"
'oradatabase.Parameters.Add "cod", Grid1.Text, 2
'oradatabase.Parameters.Remove "cod_errora"
'oradatabase.Parameters.Add "cod_errora", 0, 2
'oradatabase.Parameters.Remove "txt_errora"
'oradatabase.Parameters.Add "txt_errora", "", 2

'oradatabase.ExecuteSQL sql

'vErro = IIf(oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
'If vErro <> 0 Then
'   MsgBox "Ocorreu o erro: " & oradatabase.Parameters("txt_errora")
'End If

'Grid1.Picture = Picture1.Picture
'frmVenda.txtcod_cliente.Text = ""
'frmVenda.txtcod_cliente.Text = CStr(Grid1.Text)
'frmLig.Show 1

'pedido = Shell("F:\SISTEMAS\ORACLE\VDA600\vb\desenv\vda600.exe" & " "
'  pedido = Shell("H:\ORACLE\SISTEMAS\VB\32BITS\vda600.exe" & " " _
   '  & Format(Grid1.Text, "000000") & Format(sCOD_VEND, "0000"), 4)
'Unload Me
End Sub

Private Sub SSCommand1_Click()
    Unload Me
    frmCVend.Caption = "LISTA DE LIGA��ES A EXECUTAR"
    Call SSCommand3_Click
End Sub

Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblLista.Visible = True
    lblSair.Visible = False
    lblOK.Visible = False
End Sub


Private Sub SSCommand2_Click()
    Unload Me
End Sub




Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblLista.Visible = False
    lblSair.Visible = True
    lblOK.Visible = False
End Sub


Private Sub SSCommand3_Click()
    If frmCVend.Visible = False Then
        frmCVend.Top = 540
        frmCVend.Width = 9700
        frmCVend.Left = -15
        frmCVend.Height = 6675
        frmCVend.Show
    End If
End Sub

Private Sub SSCommand4_Click()
    Unload Me
    frmCVend.Caption = "LISTA DE CONTATOS J� EFETUADOS NESTA DATA"
End Sub

Private Sub SSCommand4_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    lblLista.Visible = False
    lblSair.Visible = False
    lblOK.Visible = True
End Sub


Private Sub Text1_Change()
    If tot_grid = 0 Then
        Exit Sub
    End If
    'reset nas posi�oes do grid
    Grid1.HighLight = False
    Grid1.TopRow = 1
    Grid1.Row = 1
    Grid1.Col = 2

    'Variaveis que definem inicio ou continuacao da pesquisa
    CONTINUAPESQUISA = 0
    OCORRENCIA = 0
    CONTADOROCORRENCIA = 0

    Command1.Caption = "In�cio"

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub

Private Sub Text1_LostFocus()

    If tot_grid = 0 Then
        Exit Sub
    End If

    If Text1.Text <> "" Then
        On Error GoTo TopRowError
        'N�mero da coluna onde a pesquisa deve agir
        Grid1.Col = 1

        'texto que dever� ser encontrado
        TEXTOPARAPROCURA = Trim(Text1.Text)

        'desabilita o highlight
        Grid1.HighLight = False

        'posiciona no in�cio do grid
        Grid1.Row = 1
        Grid1.TopRow = 1

        For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grid
            Grid1.Row = INICIOPESQUISA

            'verifica se a string digitada est� contida no texto da aplica�ao
            If InStr(Grid1.Text, TEXTOPARAPROCURA) <> 0 Then
                'define nova ocorr�ncia
                OCORRENCIA = OCORRENCIA + 1
            End If

            If InStr(Grid1.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
                'vari�vel que controla quantas vezes determinado string j� foi localizado
                CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1

                'liga o highlight
                Grid1.HighLight = True
                Grid1.SelStartRow = INICIOPESQUISA
                Grid1.SelEndRow = INICIOPESQUISA
                Grid1.SelStartCol = 0
                Grid1.SelEndCol = 2

                'se encontrou o texto posiciona a linha do grid
                If Grid1.CellSelected Then
                    If INICIOPESQUISA > 1 Then
                        For INILINHA = 1 To INICIOPESQUISA - 1
                            'o m�ximo valor que toprow pode ter deve ser igual ao numero de
                            'linhas total do grid menos o numero de linhas visiveis do grid
                            If Grid1.TopRow = Grid1.Row - tot_grid Then
                                Grid1.TopRow = Grid1.Rows - tot_grid
                            Else
                                Grid1.TopRow = Grid1.TopRow + 1
                            End If
                        Next
                    End If
                End If

                'a proxima pesquisa continuara a partir desta variavel
                CONTINUAPESQUISA = INICIOPESQUISA
                Command1.Caption = "Pr�xima"
                Exit Sub
            End If
        Next


        MsgBox "Fim de Pesquisa", vbExclamation, "ATEN��O"
        'reset nas posi�oes do grid
        Grid1.HighLight = False
        Grid1.TopRow = 1
        Grid1.Row = 1
        Grid1.Col = 2

        'Variaveis que definem inicio ou continuacao da pesquisa
        CONTINUAPESQUISA = 0
        OCORRENCIA = 0
        CONTADOROCORRENCIA = 0

        Command1.Caption = "In�cio"
        Exit Sub

TopRowError:
        Grid1.TopRow = 1
        Resume Next
    End If


End Sub


