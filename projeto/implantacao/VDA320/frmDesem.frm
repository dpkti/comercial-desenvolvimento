VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmDesem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Desempenho"
   ClientHeight    =   7260
   ClientLeft      =   165
   ClientTop       =   1320
   ClientWidth     =   11520
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7260
   ScaleWidth      =   11520
   Begin MSFlexGridLib.MSFlexGrid grdTime 
      Height          =   2010
      Left            =   75
      TabIndex        =   14
      Top             =   2790
      Width           =   11460
      _ExtentX        =   20214
      _ExtentY        =   3545
      _Version        =   393216
      ForeColor       =   12582912
   End
   Begin VB.Frame Frame1 
      Caption         =   "PEDIDOS"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1215
      Left            =   1080
      TabIndex        =   0
      Top             =   810
      Width           =   6135
      Begin VB.Image Image1 
         Height          =   480
         Left            =   4440
         Picture         =   "frmDesem.frx":0000
         Top             =   240
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblAtingido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   13
         Top             =   840
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "%ATINGIDO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3360
         TabIndex        =   12
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label lblQuota 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   11
         Top             =   840
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "VALOR QUOTA DIA"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   840
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.Label lblValor 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   4
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label lblQtd 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "VALOR CONT�BIL"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "QUANTIDADE"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid grdFora_Time 
      Height          =   1830
      Left            =   45
      TabIndex        =   17
      Top             =   5130
      Width           =   11460
      _ExtentX        =   20214
      _ExtentY        =   3228
      _Version        =   393216
      ForeColor       =   12582912
   End
   Begin VB.Label lblDt_Base 
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   210
      Left            =   2520
      TabIndex        =   19
      Top             =   2160
      Width           =   60
   End
   Begin VB.Label Label9 
      Caption         =   "DATA DE REFER�NCIA"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   18
      Top             =   2160
      Width           =   2190
   End
   Begin VB.Label Label8 
      Caption         =   "FORA DO TIME"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   16
      Top             =   4860
      Width           =   2055
   End
   Begin VB.Label Label7 
      Caption         =   "TIME"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   135
      TabIndex        =   15
      Top             =   2475
      Width           =   570
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      X1              =   120
      X2              =   10560
      Y1              =   2070
      Y2              =   2070
   End
   Begin VB.Label lblPseudonimo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3840
      TabIndex        =   9
      Top             =   120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblCod_vend 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label5 
      Caption         =   "VENDEDOR"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1080
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblDt_Faturamento 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   6
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "DATA FATURAMENTO"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1080
      TabIndex        =   5
      Top             =   480
      Width           =   1815
   End
End
Attribute VB_Name = "frmDesem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Label12_Click()

End Sub


Private Sub SSCommand1_Click()
    Dim sql As String
    Dim ss As Object
    Dim i As Long

    Screen.MousePointer = 11

    On Error GoTo Trata_Erro


    Set OraParameters = oradatabase.Parameters


    OraParameters.Remove "vCursor"
    OraParameters.Add "vCursor", 0, 2
    OraParameters("vCursor").serverType = ORATYPE_CURSOR
    OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    OraParameters.Remove "fil"
    OraParameters.Add "fil", Mid(FILIAL_PED, 1, 4), 1

    OraParameters.Remove "vErro"
    OraParameters.Add "vErro", 0, 2
    OraParameters("vErro").serverType = ORATYPE_NUMBER

    V_SQL = "Begin " & strTabela_Banco & "pck_vda630.pr_desempenho(:vCursor,:fil,:vErro);END;"
    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set VarRec = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(sql)
        Exit Sub
    End If
    ' Set ss = oradatabase.dbcreatedynaset(sql, 0&)

    ' If ss.EOF Then
    '   MsgBox "N�o h� dados para a consulta", vbInformation, "Aten��o"
    '   Screen.MousePointer = 0
    '   Exit Sub
    ' Else
    With frmRank.Grid1
        .Cols = 5
        .Rows = VarRec.RecordCount + 1
        .ColWidth(0) = 500
        .ColWidth(1) = 1200
        .ColWidth(2) = 900
        .ColWidth(3) = 1000
        .ColWidth(4) = 900

        .Row = 0
        .Col = 0
        .Text = "RANK."
        .Col = 1
        .Text = "TLMKT"
        .Col = 2
        .Text = "  ATIVA"
        .Col = 3
        .Text = "RECEPTIVA"
        .Col = 4
        .Text = "  TOTAL"

        VarRec.MoveFirst
        For i = 1 To VarRec.RecordCount
            ' .Rows -1
            .Row = i

            .Col = 0
            '.ColAlignment(0) = 1
            .Text = i
            .Col = 1
            .ColAlignment(1) = 0
            .Text = CStr(VarRec!pseudonimo)
            .Col = 2
            .ColAlignment(2) = 1
            .Text = CStr(VarRec!ativa)
            .Col = 3
            .ColAlignment(3) = 1
            .Text = CStr(VarRec!receptiva)
            .Col = 4
            .ColAlignment(4) = 1
            .Text = CStr(VarRec!total)
            VarRec.MoveNext
        Next
        .Row = 1
    End With
    frmRank.Grid1.Visible = True
    Screen.MousePointer = 0
    frmRank.Show 1
    'End If
    Exit Sub
Trata_Erro:
    If Err = 30009 Then
        Resume Next
    End If

End Sub


Private Sub lblDt_Real_Click()

End Sub

Private Sub Form_Load()
    Dim intLinha_Time As Integer
    Dim intLinha_Fora_Time As Integer
    Dim ss As Object
    Dim i As Long
    Dim dblVda_Diaria As Double
    Dim dblCota_Ajustada As Double
    Dim dblVda_Acum As Double
    Dim dblCota As Double
    Dim lngNF_Dia As Double
    Dim lngNF_Acum As Double

    Dim dblLC_Diario As Double
    Dim dblBVista_Diario As Double
    Dim dblLC_Acum As Double
    Dim dblBVista_Acum As Double
    Dim dblCota_Total As Double


    dblVda_Diaria = 0
    dblCota_Ajustada = 0
    dblVda_Acum = 0
    dblCota = 0
    lngNF_Dia = 0
    lngNF_Acum = 0
    dblCota_Total = 0

    dblLC_Diario = 0
    dblBVista_Diario = 0
    dblLC_Acum = 0
    dblBVista_Acum = 0


    oradatabase.Parameters.Remove "vend"
    oradatabase.Parameters.Add "vend", sCOD_VEND, 1

    V_SQL = "Begin producao.pck_vda230.pr_sav(:vCursor,:vend,:vErro);END;"

    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set ss = oradatabase.Parameters("vCursor").Value

    If ss.EOF And ss.BOF Then
        grdTime.Visible = False
        grdFora_Time.Visible = False
        MsgBox "N�o h� informa��es dispon�veis sobre o desempenho mensal", vbInformation, "ATEN��O"
        Exit Sub
    End If

    For i = 1 To ss.RecordCount
        If ss!Time = "1" Then
            intLinha_Time = intLinha_Time + 1
        Else
            intLinha_Fora_Time = intLinha_Fora_Time + 1
        End If
        ss.MoveNext
    Next
    ss.MoveFirst
    lblDt_Base = ss(21)
    grdTime.Clear
    grdFora_Time.Clear

    With frmDesem.grdTime
        .Cols = 15
        .Rows = intLinha_Time + 2
        .ColWidth(0) = 600
        .ColWidth(1) = 1100
        .ColWidth(2) = 600
        .ColWidth(3) = 1100
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 400
        .ColWidth(7) = 1000
        .ColWidth(8) = 900
        .ColWidth(9) = 400
        .ColWidth(10) = 800
        .ColWidth(11) = 700
        .ColWidth(12) = 700
        .ColWidth(13) = 400
        .ColWidth(14) = 400

        .Row = 0
        .Col = 0
        .Text = "VEND"
        .Col = 1
        .Text = "PSEUD"
        .Col = 2
        .Text = "REPR"
        .Col = 3
        .Text = "PSEUD"
        .Col = 4
        .Text = "VDA DIARIA"
        .Col = 5
        .Text = "COTA AJUS."
        .Col = 6
        .Text = "PC"
        .Col = 7
        .Text = "VDA ACUM"
        .Col = 8
        .Text = "COTA A."
        .Col = 9
        .Text = "PC"
        .Col = 10
        .Text = "COTA T."
        .Col = 11
        .Text = "QT NF D"
        .Col = 12
        .Text = "QT NF A"
        .Col = 13
        .Text = "MD"
        .Col = 14
        .Text = "MA"

        For i = 1 To intLinha_Time
            .Row = i
            .Col = 0
            If (ss(5) = 1 Or ss(5) = 3) Then
                .Text = ss(1)
            Else
                .Text = ""
            End If
            .Col = 1
            If (ss(5) = 1 Or ss(5) = 3) Then
                .Text = ss(2)
            Else
                .Text = ""
            End If
            .Col = 2
            .Text = ss(3)
            .Col = 3
            .Text = IIf(IsNull(ss(4)), "", ss(4))
            .Col = 4
            .Text = Round(CStr(ss(6)), 0)
            dblVda_Diaria = dblVda_Diaria + CDbl(CStr(ss(6)))
            .Col = 5
            .Text = Round(CStr(ss(7)), 0)
            dblCota_Ajustada = dblCota_Ajustada + CDbl(CStr(ss(7)))
            .Col = 6
            If ss(8) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = ss(8)
            .Col = 7
            .Text = Round(CStr(ss(9)), 0)
            dblVda_Acum = dblVda_Acum + CDbl(CStr(ss(9)))
            .Col = 8
            .Text = Round(CStr(ss(10)), 0)
            dblCota = dblCota + CDbl(CStr(ss(10)))
            .Col = 9
            If ss(11) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = ss(11)
            .Col = 10
            .Text = IIf(IsNull(ss(20)), 0, ss(20))
            .Col = 11
            .Text = IIf(IsNull(ss(14)), 0, ss(14))
            lngNF_Dia = lngNF_Dia + IIf(IsNull(CStr(ss(14))), 0, CStr(ss(14)))
            .Col = 12
            .Text = IIf(IsNull(ss(15)), 0, ss(15))
            lngNF_Acum = lngNF_Acum + IIf(IsNull(CStr(ss(15))), 0, CStr(ss(15)))
            .Col = 13
            .Text = Round(CStr(IIf(IsNull(ss(12)), 0, ss(12))), 0)
            .Col = 14
            .Text = Round(CStr(IIf(IsNull(ss(13)), 0, ss(13))), 0)

            dblLC_Diario = dblLC_Diario + CDbl(ss(16))
            dblBVista_Diario = dblBVista_Diario + CDbl(ss(18))
            dblLC_Acum = dblLC_Acum + CDbl(ss(17))
            dblBVista_Acum = dblBVista_Acum + CDbl(ss(19))
            dblCota_Total = dblCota_Total + CDbl(ss(20))

            ss.MoveNext
        Next

        .Row = intLinha_Time + 1
        .Col = 0
        .Text = ""
        .Col = 1
        .CellForeColor = vbBlue
        .Text = "TOTAL"
        .Col = 2
        .Text = ""
        .Col = 3
        .Text = ""
        .Col = 4
        .Text = Round(dblVda_Diaria, 0)
        .Col = 5
        .Text = Round(dblCota_Ajustada, 0)
        .Col = 6
        If dblVda_Diaria = 0 Or dblCota_Ajustada = 0 Then
            .Text = "0"
            .CellForeColor = vbRed
        Else
            If Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0)
        End If
        .Col = 7
        .Text = Round(dblVda_Acum, 0)
        .Col = 8
        .Text = Round(dblCota, 0)
        .Col = 9
        If dblVda_Acum = 0 Or dblCota = 0 Then
            .CellForeColor = vbRed
            .Text = "0"
        Else
            If Round((dblVda_Acum) / (dblCota) * 100, 0) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = Round((dblVda_Acum) / (dblCota) * 100, 0)
        End If
        .Col = 10
        .Text = dblCota_Total
        .Col = 11
        .Text = lngNF_Dia
        .Col = 12
        .Text = lngNF_Acum
        .Col = 13
        If dblLC_Diario = 0 Or dblBVista_Diario = 0 Then
            .Text = "0"
        Else
            .Text = Round((dblLC_Diario / dblBVista_Diario) * 100, 0)
        End If
        .Col = 14
        If dblLC_Acum = 0 Or dblBVista_Acum = 0 Then
            .Text = "0"
        Else
            .Text = Round((CDbl(dblLC_Acum) / CDbl(dblBVista_Acum)) * 100, 0)
        End If

    End With

    dblVda_Diaria = 0
    dblCota_Ajustada = 0
    dblVda_Acum = 0
    dblCota = 0
    lngNF_Dia = 0
    lngNF_Acum = 0
    dblCota_Total = 0

    dblLC_Diario = 0
    dblBVista_Diario = 0
    dblLC_Acum = 0
    dblBVista_Acum = 0

    With frmDesem.grdFora_Time
        .Cols = 15
        .Rows = intLinha_Fora_Time + 2
        .ColWidth(0) = 600
        .ColWidth(1) = 1100
        .ColWidth(2) = 600
        .ColWidth(3) = 1100
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 400
        .ColWidth(7) = 1000
        .ColWidth(8) = 900
        .ColWidth(9) = 400
        .ColWidth(10) = 800
        .ColWidth(11) = 700
        .ColWidth(12) = 700
        .ColWidth(13) = 400
        .ColWidth(14) = 400

        .Row = 0
        .Col = 0
        .Text = "VEND"
        .Col = 1
        .Text = "PSEUD"
        .Col = 2
        .Text = "REPR"
        .Col = 3
        .Text = "PSEUD"
        .Col = 4
        .Text = "VDA DIARIA"
        .Col = 5
        .Text = "COTA AJUS."
        .Col = 6
        .Text = "PC"
        .Col = 7
        .Text = "VDA ACUM"
        .Col = 8
        .Text = "COTA A."
        .Col = 9
        .Text = "PC"
        .Col = 10
        .Text = "COTA T."
        .Col = 11
        .Text = "QT NF D"
        .Col = 12
        .Text = "QT NF A"
        .Col = 13
        .Text = "MD"
        .Col = 14
        .Text = "MA"

        For i = 1 To intLinha_Fora_Time
            .Row = i
            .Col = 0
            .Text = IIf(IsNull(ss(1)), 0, ss(1))
            .Col = 1
            .Text = IIf(IsNull(ss(2)), 0, ss(2))
            .Col = 2
            .Text = IIf(IsNull(ss(3)), 0, ss(3))
            .Col = 3
            .Text = IIf(IsNull(ss(4)), "", ss(4))
            .Col = 4
            .Text = Round(CStr(ss(6)), 0)
            dblVda_Diaria = dblVda_Diaria + CDbl(CStr(ss(6)))
            .Col = 5
            .Text = Round(CStr(ss(7)), 0)
            dblCota_Ajustada = dblCota_Ajustada + CDbl(CStr(ss(7)))
            .Col = 6
            If ss(8) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = ss(8)
            .Col = 7
            .Text = Round(CStr(ss(9)), 0)
            dblVda_Acum = dblVda_Acum + CDbl(CStr(ss(9)))
            .Col = 8
            .Text = Round(CStr(ss(10)), 0)
            dblCota = dblCota + CDbl(CStr(ss(10)))
            .Col = 9
            If ss(11) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = ss(11)
            .Col = 10
            .Text = dblCota_Total
            .Col = 11
            .Text = IIf(IsNull(ss(14)), 0, ss(14))
            lngNF_Dia = lngNF_Dia + IIf(IsNull(CStr(ss(14))), 0, CStr(ss(14)))
            .Col = 12
            .Text = IIf(IsNull(ss(15)), 0, ss(15))
            lngNF_Acum = lngNF_Acum + IIf(IsNull(CStr(ss(15))), 0, CStr(ss(15)))
            .Col = 13
            .Text = Round(CStr(IIf(IsNull(ss(12)), 0, ss(12))), 0)
            .Col = 14
            .Text = Round(CStr(IIf(IsNull(ss(13)), 0, ss(13))), 0)

            dblLC_Diario = dblLC_Diario + CDbl(ss(16))
            dblBVista_Diario = dblBVista_Diario + CDbl(ss(18))
            dblLC_Acum = dblLC_Acum + CDbl(ss(17))
            dblBVista_Acum = dblBVista_Acum + CDbl(ss(19))
            dblCota_Total = dblCota_Total + CDbl(ss(20))
            ss.MoveNext
        Next

        .Row = intLinha_Fora_Time + 1
        .Col = 0
        .Text = ""
        .Col = 1
        .CellForeColor = vbBlue
        .Text = "TOTAL"
        .Col = 2
        .Text = ""
        .Col = 3
        .Text = ""
        .Col = 4
        .Text = Round(dblVda_Diaria, 0)
        .Col = 5
        .Text = Round(dblCota_Ajustada, 0)
        .Col = 6
        If dblVda_Diaria = 0 Or dblCota_Ajustada = 0 Then
            .Text = "0"
            .CellForeColor = vbRed
        Else
            If Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = Round((dblVda_Diaria) / (dblCota_Ajustada) * 100, 0)
        End If
        .Col = 7
        .Text = Round(dblVda_Acum, 0)
        .Col = 8
        .Text = Round(dblCota, 0)
        .Col = 9
        If dblVda_Acum = 0 Or dblCota = 0 Then
            .CellForeColor = vbRed
            .Text = "0"
        Else
            If Round((dblVda_Acum) / (dblCota) * 100, 0) < 100 Then
                .CellForeColor = vbRed
            Else
                .CellForeColor = vbBlue
            End If
            .Text = Round((dblVda_Acum) / (dblCota) * 100, 0)
        End If
        .Col = 10
        .Text = dblCota_Total
        .Col = 11
        .Text = lngNF_Dia
        .Col = 12
        .Text = lngNF_Acum
        .Col = 13
        If dblLC_Diario = 0 Or dblBVista_Diario = 0 Then
            .Text = "0"
        Else
            .Text = Round((dblLC_Diario / dblBVista_Diario) * 100, 0)
        End If
        .Col = 14
        If dblLC_Acum = 0 Or dblBVista_Acum = 0 Then
            .Text = "0"
        Else
            .Text = Round((dblLC_Acum / dblBVista_Acum) * 100, 0)
        End If

    End With

End Sub

