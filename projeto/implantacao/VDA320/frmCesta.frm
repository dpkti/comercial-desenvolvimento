VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmCesta 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "PRODUTOS EM DESTAQUE"
   ClientHeight    =   5985
   ClientLeft      =   585
   ClientTop       =   1530
   ClientWidth     =   8280
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5985
   ScaleWidth      =   8280
   Begin VB.TextBox txtVeiculo 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2400
      MaxLength       =   20
      TabIndex        =   12
      Top             =   1200
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Sair"
      Height          =   495
      Left            =   6240
      TabIndex        =   5
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.CommandButton cmdBuscar 
      Caption         =   "&Buscar"
      Height          =   495
      Left            =   5040
      TabIndex        =   4
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Frame framCesta 
      Caption         =   "SEGMENTO"
      ForeColor       =   &H00800000&
      Height          =   615
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Visible         =   0   'False
      Width           =   4695
      Begin VB.OptionButton optAcessorios 
         Caption         =   "ACESSORIOS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2880
         TabIndex        =   3
         Top             =   240
         Width           =   1695
      End
      Begin VB.OptionButton optFrotas 
         Caption         =   "FROTAS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optPecas 
         Caption         =   "PECAS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
   End
   Begin Threed.SSFrame ssfraPesq 
      Height          =   735
      Left            =   360
      TabIndex        =   13
      Top             =   5160
      Visible         =   0   'False
      Width           =   5895
      _Version        =   65536
      _ExtentX        =   10398
      _ExtentY        =   1296
      _StockProps     =   14
      Caption         =   "Pesquisa"
      ForeColor       =   255
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.TextBox txtPesquisa 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   3255
      End
      Begin Threed.SSCommand sscmdDesc 
         Height          =   375
         Left            =   3840
         TabIndex        =   15
         Top             =   240
         Width           =   1455
         _Version        =   65536
         _ExtentX        =   2566
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Descri��o"
      End
   End
   Begin MSGrid.Grid grdItem 
      Height          =   3135
      Left            =   240
      TabIndex        =   9
      Top             =   1560
      Visible         =   0   'False
      Width           =   7815
      _Version        =   65536
      _ExtentX        =   13785
      _ExtentY        =   5530
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "frmCesta.frx":0000
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "VE�CULO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1440
      TabIndex        =   11
      Top             =   1200
      Visible         =   0   'False
      Width           =   810
   End
   Begin VB.Label lblObs 
      AutoSize        =   -1  'True
      Caption         =   "ITENS CADASTRADOS NO �LTIMO M�S COM ESTOQUE DISPON�VEL"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   480
      TabIndex        =   10
      Top             =   4920
      Visible         =   0   'False
      Width           =   6270
   End
   Begin VB.Label lblSIGLA 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "WWWWWWWWWW"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2520
      TabIndex        =   8
      Top             =   1080
      Visible         =   0   'False
      Width           =   1710
   End
   Begin VB.Label lblCod_fornecedor 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   1800
      TabIndex        =   7
      Top             =   1080
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblForn 
      AutoSize        =   -1  'True
      Caption         =   "FORNECEDOR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   1455
   End
End
Attribute VB_Name = "frmCesta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public bTipo_Cesta As Byte

Public strPesquisa As String

'VARIAVEIS UTILIZADAS PARA PESQUISA
Public CONTINUAPESQUISA As Integer
Public OCORRENCIA As Integer
Public CONTADOROCORRENCIA As Long
Public TEXTOPARAPROCURA As String
Public INICIOPESQUISA As Long
Public INILINHA As Long
Public tot_grdfabr As Long
Private Sub cmdBuscar_Click()
    Dim ss As Object

    If strTipo_Tela = "V" And txtVeiculo = "" Then
        MsgBox "Digite um ve�culo, para efetuar a consulta"
        Exit Sub
    End If

    If strTipo_Tela = "V" And txtVeiculo <> "" Then
        Screen.MousePointer = 11
        Call Carrega_Kit(txtVeiculo)
        ssfraPesq.Visible = True
        Screen.MousePointer = 0
    Else
        ssfraPesq.Visible = False
        If bTipo_Cesta = 0 Then
            MsgBox "Selecione um segmento para fazer a busca", vbInformation, "Aten��o"
            Exit Sub
        End If


        If Verifica_Cesta(bTipo_Cesta) Then
            Screen.MousePointer = 11
            Call Carrega_Itens(bTipo_Cesta)
            Screen.MousePointer = 0
        Else
            MsgBox "N�o h� itens em destaque para este segmento", vbInformation, "Aten��o"
            Exit Sub
        End If
    End If

End Sub

Private Sub Command1_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    Dim ss As Object
    Screen.MousePointer = 11

    If strTipo_Tela = "D" Then
        frmCesta.Caption = "LISTA DE EXCEL�NCIA (ITENS DIFERENCIADOS, PRIORIT�RIOS)"
        framCesta.Visible = True
        lblCod_fornecedor.Visible = True
        lblForn = True
        lblSIGLA = True
        cmdBuscar.Visible = True
        Command1.Visible = True
        bTipo_Cesta = 1
        optPecas.Value = True
        cmdBuscar_Click
        lblObs.Visible = False
        Label1.Visible = False
        txtVeiculo.Visible = False


        '  bTipo_Cesta = 0
        If frmVenda.txtCOD_FORNECEDOR <> "" Then
            frmCesta.lblForn.Visible = False
            frmCesta.lblSIGLA.Visible = False
            frmCesta.lblCod_fornecedor.Visible = False

            frmCesta.lblCod_fornecedor = frmVenda.txtCOD_FORNECEDOR
            frmCesta.lblSIGLA = frmVenda.lblSIGLA
            DoEvents
        Else
            frmCesta.lblForn.Visible = False
            frmCesta.lblSIGLA.Visible = False
            frmCesta.lblCod_fornecedor.Visible = False

            frmCesta.lblCod_fornecedor = ""
            frmCesta.lblSIGLA = ""
            DoEvents
        End If

    ElseIf strTipo_Tela = "N" Then
        frmCesta.Caption = "ITENS NOVOS"
        framCesta.Visible = False
        lblCod_fornecedor.Visible = False
        lblForn = False
        lblSIGLA = False
        cmdBuscar.Visible = False
        Command1.Visible = False
        lblObs.Visible = True
        Label1.Visible = False
        txtVeiculo.Visible = False


        OraParameters.Remove "loja"
        OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1

        Screen.MousePointer = 11

        V_SQL = "Begin producao.pck_vda230.pr_itens_novos(:vCursor,:loja,:vErro);END;"

        Criar_Cursor
        oradatabase.ExecuteSQL V_SQL

        Set ss = oradatabase.Parameters("vCursor").Value

        If oradatabase.Parameters("vErro").Value <> 0 Then
            Screen.MousePointer = 0
            Call Process_Line_Errors(sql)
            Exit Sub
        End If


        If ss.EOF And ss.BOF Then
            MsgBox "N�o h� itens novos"
        Else
            With frmCesta.grdItem
                .Cols = 5
                .Rows = ss.RecordCount + 1
                .ColWidth(0) = 660
                .ColWidth(1) = 1500
                .ColWidth(2) = 1500
                .ColWidth(3) = 2500
                .ColWidth(4) = 900 ' fpallini - Cit - 19/07/12 - Aumentar coluna para caber 9 d�gitos.


                .Row = 0
                .Col = 0
                .Text = "Cod"
                .Col = 1
                .Text = "Forn"
                .Col = 2
                .Text = "Fabrica"
                .Col = 3
                .Text = "Descri��o"
                .Col = 4
                .Text = "DPK"

                ss.MoveFirst
                For i = 1 To .Rows - 1
                    .Row = i

                    .Col = 0
                    .Text = ss!cod_fornecedor
                    .Col = 1
                    .Text = ss!sigla
                    .Col = 2
                    .Text = ss!cod_fabrica
                    .Col = 3
                    .Text = ss!desc_item
                    .Col = 4
                    .Text = ss!cod_dpk

                    ss.MoveNext
                Next
                .Row = 1
            End With
            frmCesta.grdItem.Visible = True
        End If

    ElseIf strTipo_Tela = "V" Then
        frmCesta.Caption = "KIT POR VEICULO"
        framCesta.Visible = False
        lblCod_fornecedor.Visible = False
        lblForn = False
        lblSIGLA = False
        cmdBuscar.Visible = True
        Command1.Visible = True
        lblObs.Visible = False
        Label1.Visible = True
        txtVeiculo.Visible = True

        bTipo_Cesta = 9

    End If

    Screen.MousePointer = 0

End Sub




Private Sub grdItem_DblClick()
    grdItem.Col = 4
    frmVenda.txtCOD_DPK = grdItem.Text
    If strTipo_Tela = "V" Then
        frmCesta.Hide
    Else
        Unload Me
    End If
End Sub

Private Sub optAcessorios_Click()
    grdItem.Visible = False
    bTipo_Cesta = 3
End Sub

Private Sub optFrotas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 2
End Sub

Private Sub optPecas_Click()
    grdItem.Visible = False
    bTipo_Cesta = 1
End Sub



Public Function Verifica_Cesta(tipo_cesta As Byte) As Boolean
    Dim ss As Object

    sql = "Select dt_vigencia"
    sql = sql & " From cesta_venda"
    sql = sql & " Where tp_cesta = :tp"
    sql = sql & " and cod_loja = :loja"

    oradatabase.Parameters.Remove "tp"
    oradatabase.Parameters.Add "tp", tipo_cesta, 1
    oradatabase.Parameters.Remove "loja"
    oradatabase.Parameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1

    Set ss = oradatabase.dbcreatedynaset(sql, 0&)

    If (ss.BOF And ss.EOF) Or IsNull(ss!dt_vigencia) Then
        Verifica_Cesta = False
    Else
        If ss!dt_vigencia > Format(Data_Faturamento, "YYYYMMDD") Then
            Verifica_Cesta = False
        Else
            Verifica_Cesta = True
        End If
    End If


End Function

Public Sub Carrega_Itens(tipo_cesta As Byte)
    Dim ss As Object


    OraParameters.Remove "loja"
    OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1
    OraParameters.Remove "tp"
    OraParameters.Add "tp", tipo_cesta, 1
    OraParameters.Remove "forn"
    OraParameters.Add "forn", lblCod_fornecedor, 1


    V_SQL = "Begin producao.pck_vda230.pr_cesta_item(:vCursor,:loja,:tp,:vErro);END;"

    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set ss = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Call Process_Line_Errors(sql)
        Exit Sub
    End If


    If ss.EOF And ss.BOF Then
        MsgBox "Lista n�o dispon�vel, em breve itens com condi��es diferenciadas", vbInformation, "Aten��o"
    Else
        With frmCesta.grdItem
            .Cols = 5
            .Rows = ss.RecordCount + 1
            .ColWidth(0) = 660
            .ColWidth(1) = 1500
            .ColWidth(2) = 1500
            .ColWidth(3) = 2500
            .ColWidth(4) = 900 ' fpallini - Cit - 19/07/12 - Aumentar coluna para caber 9 d�gitos.


            .Row = 0
            .Col = 0
            .Text = "Cod"
            .Col = 1
            .Text = "Forn"
            .Col = 2
            .Text = "Fabrica"
            .Col = 3
            .Text = "Descri��o"
            .Col = 4
            .Text = "DPK"

            ss.MoveFirst
            For i = 1 To .Rows - 1
                .Row = i

                .Col = 0
                .Text = ss!cod_fornecedor
                .Col = 1
                .Text = ss!sigla
                .Col = 2
                .Text = ss!cod_fabrica
                .Col = 3
                .Text = ss!desc_item
                .Col = 4
                .Text = ss!cod_dpk

                ss.MoveNext
            Next
            .Row = 1
        End With
        frmCesta.grdItem.Visible = True
    End If

End Sub

Public Sub Carrega_Kit(strVeiculo As String)
    Dim ss As Object
    Dim i As Long
    Dim strFabr As String
    Dim bAltura As Byte

    OraParameters.Remove "loja"
    OraParameters.Add "loja", Mid(Trim(frmVenda.cboDeposito), 1, 2), 1
    OraParameters.Remove "veiculo"
    OraParameters.Add "veiculo", "%" & strVeiculo & "%", 1

    tot_grdfabr = 0


    Screen.MousePointer = 11

    V_SQL = "Begin producao.pck_vda230.pr_kit_veiculo(:vCursor,:loja,:veiculo,:vErro);END;"

    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set ss = oradatabase.Parameters("vCursor").Value

    If oradatabase.Parameters("vErro").Value <> 0 Then
        Screen.MousePointer = 0
        Call Process_Line_Errors(sql)
        Exit Sub
    End If

    tot_grdfabr = ss.RecordCount
    If ss.EOF And ss.BOF Then
        MsgBox "N�o h� itens para este ve�culo"
        Screen.MousePointer = 0
    Else
        With frmCesta.grdItem
            .Cols = 6
            '.Rows = ss.RecordCount + 1
            .Rows = 1
            .ColWidth(0) = 660
            .ColWidth(1) = 1500
            .ColWidth(2) = 1500
            .ColWidth(3) = 2500
            .ColWidth(4) = 900 ' fpallini - Cit - 19/07/12 - Aumentar coluna para caber 9 d�gitos.
            .ColWidth(5) = 3870


            .Row = 0
            .Col = 0
            .Text = "Cod"
            .Col = 1
            .Text = "Forn"
            .Col = 2
            .Text = "Fabrica"
            .Col = 3
            .Text = "Descri��o"
            .Col = 4
            .Text = "DPK"
            .Col = 5
            .Text = "APLICA��O"

            ss.MoveFirst
            strFabr = ""

            For i = 1 To ss.RecordCount
                'cria linha
                If strFabr <> ss!cod_fabrica Then
                    'cria linha
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    .Col = 0
                    .Text = ss!cod_fornecedor
                    .Col = 1
                    .Text = ss!sigla
                    .Col = 2
                    .Text = ss!cod_fabrica
                    .Col = 3
                    .Text = ss!desc_item
                    .Col = 4
                    .Text = ss!cod_dpk
                    .Col = 5
                    If IsNull(ss!desc_aplicacao) Then
                        .Text = ""
                    Else
                        .Text = Trim$(ss!desc_aplicacao)
                    End If
                    'carrega codigo de fabrica
                    strFabr = ss!cod_fabrica
                Else
                    'carrega aplicacao
                    If Not IsNull(ss!desc_aplicacao) Then
                        .Col = 5
                        .Text = .Text & " " & Trim$(ss!desc_aplicacao)
                        bAltura = Len(Trim$(.Text)) \ 30
                        If Len(Trim$(.Text)) > 30 And Len(Trim$(.Text)) Mod 30 > 0 Then
                            bAltura = bAltura + 1
                        End If
                        If bAltura > 1 Then
                            .RowHeight(.Row) = bAltura * 225
                        End If
                    End If
                End If

                ss.MoveNext

            Next
            .Row = 1
            .FixedRows = 1
        End With
        Screen.MousePointer = 0
        frmCesta.grdItem.Visible = True
    End If

End Sub

Private Sub Text1_Change()

End Sub

Private Sub SSCommand2_Click()

End Sub

Private Sub SSCommand1_Click()

End Sub

Private Sub sscmdDesc_Click()
    On Error GoTo TopRowError
    'N�mero da coluna onde a pesquisa deve agir
    grdItem.Col = 3

    'texto que dever� ser encontrado
    TEXTOPARAPROCURA = Trim(txtPesquisa.Text)

    'desabilita o highlight
    grdItem.HighLight = False

    'posiciona no in�cio do grid
    grdItem.Row = 1
    grdItem.TopRow = 1

    For INICIOPESQUISA = CONTINUAPESQUISA + 1 To tot_grdfabr
        grdItem.Row = INICIOPESQUISA

        'verifica se a string digitada est� contida no texto da aplica�ao
        If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 Then
            'define nova ocorr�ncia
            OCORRENCIA = OCORRENCIA + 1
        End If

        If InStr(grdItem.Text, TEXTOPARAPROCURA) <> 0 And OCORRENCIA > CONTADOROCORRENCIA Then
            'vari�vel que controla quantas vezes determinado string j� foi localizado
            CONTADOROCORRENCIA = CONTADOROCORRENCIA + 1

            'liga o highlight
            grdItem.HighLight = True
            grdItem.SelStartRow = INICIOPESQUISA
            grdItem.SelEndRow = INICIOPESQUISA
            grdItem.SelStartCol = 0
            grdItem.SelEndCol = 5

            'se encontrou o texto posiciona a linha do grid
            If grdItem.CellSelected Then
                If INICIOPESQUISA > 1 Then
                    For INILINHA = 1 To INICIOPESQUISA - 1
                        'o m�ximo valor que toprow pode ter deve ser igual ao numero de
                        'linhas total do grid menos o numero de linhas visiveis do grid
                        If grdItem.TopRow = grdItem.Row - tot_grdfabr Then
                            grdItem.TopRow = grdItem.Rows - tot_grdfabr
                        Else
                            grdItem.TopRow = grdItem.TopRow + 1
                        End If
                    Next
                End If
            End If

            'a proxima pesquisa continuara a partir desta variavel
            CONTINUAPESQUISA = INICIOPESQUISA
            sscmdDesc.Caption = "Pr�xima"
            Exit Sub
        End If
    Next


    MsgBox "Fim de Pesquisa", vbExclamation, "ATEN��O"
    sscmdDesc.Caption = "Descri��o"
    Exit Sub

TopRowError:
    If Err = 30009 Then
        grdItem.TopRow = 1
        Resume Next
    End If

End Sub

Private Sub txtPesquisa_GotFocus()
'reset nas posi�oes do grid
    frmCesta.grdItem.HighLight = False
    grdItem.TopRow = 1
    grdItem.Row = 1
    grdItem.Col = 2


    'Variaveis que definem inicio ou continuacao da pesquisa

    CONTINUAPESQUISA = 0
    OCORRENCIA = 0
    CONTADOROCORRENCIA = 0
End Sub


Private Sub txtPesquisa_KeyPress(KeyAscii As Integer)
    KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtVeiculo_Change()
    If txtVeiculo = "" Then
        grdItem.Visible = False
    End If
End Sub

Private Sub txtVeiculo_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub


