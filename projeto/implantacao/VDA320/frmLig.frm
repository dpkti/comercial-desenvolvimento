VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmLig 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Resultado do Contato"
   ClientHeight    =   2595
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   7380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2595
   ScaleWidth      =   7380
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdRecado 
      Caption         =   "Recado para Outro Operador"
      Height          =   615
      Left            =   4800
      TabIndex        =   6
      Top             =   1320
      Width           =   2415
   End
   Begin VB.Frame Frame2 
      Caption         =   "RESULTADO"
      ForeColor       =   &H000000FF&
      Height          =   1095
      Left            =   240
      TabIndex        =   3
      Top             =   960
      Width           =   4215
      Begin VB.ComboBox cboresultado 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   240
         TabIndex        =   4
         Top             =   480
         Width           =   3855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "LIGA��O"
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   480
      TabIndex        =   0
      Top             =   0
      Width           =   3255
      Begin VB.OptionButton Option1 
         Caption         =   "ATIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Caption         =   "RECEPTIVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   1
         Top             =   360
         Width           =   1575
      End
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   4800
      TabIndex        =   5
      Top             =   120
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmLig.frx":0000
   End
End
Attribute VB_Name = "frmLig"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdRecado_Click()
    frmRecado.Show 1
End Sub

Private Sub Form_Load()
    Dim i As Long
    Dim ss As Object


    V_SQL = "Begin producao.pck_vda610.pr_resultados(:vCursor,:vErro);END;"

    Criar_Cursor
    oradatabase.ExecuteSQL V_SQL

    Set ss = oradatabase.Parameters("vCursor").Value

    If ss.BOF And ss.EOF Then
        MsgBox "N�o h� resultados cadastrados", vbInformation, "Aten��o"
        Exit Sub
    End If

    For i = 1 To ss.RecordCount
        cboresultado.AddItem Format(CStr(ss!cod_resultado), "00") & " - " & CStr(ss!desc_resultado)
        ss.MoveNext
    Next

End Sub


Private Sub SSCommand1_Click()

    If Trim(frmLig.cboresultado) = "" Then
        MsgBox "Selecione um resultado para gravar", vbInformation, "Aten��o"
        Exit Sub
        'ElseIf frmVenda.txtCOD_CLIENTE = "" Then
        '  MsgBox "Selecione um cliente para gravar", vbInformation, "Aten��o"
        '  Exit Sub

    End If
    'elinar da lista de liga��es quando o
    'resultado for diferente de (sem contato, agendamento e vda
    ' para outro operador)
    If Mid(Trim(cboresultado), 1, 2) <> 3 And _
       Mid(Trim(cboresultado), 1, 2) <> 4 And _
       Mid(Trim(cboresultado), 1, 2) <> 7 Then

        sql = "BEGIN"
        sql = sql & " Update vendas.ligacao set"
        sql = sql & " situacao=9"
        sql = sql & " where cod_cliente =:cod "
        sql = sql & "  ;"
        sql = sql & " COMMIT;"
        sql = sql & "EXCEPTION"
        sql = sql & " WHEN OTHERS THEN"
        sql = sql & " ROLLBACK;"
        sql = sql & " :COD_ERRORA := SQLCODE;"
        sql = sql & " :TXT_ERRORA := SQLERRM;"
        sql = sql & "END;"

        oradatabase.Parameters.Remove "cod"
        oradatabase.Parameters.Add "cod", frmVenda.txtCod_cliente, 2
        oradatabase.Parameters.Remove "cod_errora"
        oradatabase.Parameters.Add "cod_errora", 0, 2
        oradatabase.Parameters.Remove "txt_errora"
        oradatabase.Parameters.Add "txt_errora", "", 2

        oradatabase.ExecuteSQL sql

        vErro = IIf(oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
        If vErro <> 0 Then
            MsgBox "Ocorreu o erro: " & oradatabase.Parameters("txt_errora")
        End If
    End If


    sql = "BEGIN"
    sql = sql & " Insert into vendas.contato"
    sql = sql & " values(vendas.seq_contato.nextval,:cod,"
    sql = sql & " to_date('" & Format$(data_real, "DD/MM/YY") & "'"
    sql = sql & "||to_char(sysdate,'hh24miss')" & ",'DD/MM/RRHH24MISS'),"
    sql = sql & " :vend,:tp,:res,0);"
    sql = sql & " COMMIT;"
    sql = sql & "EXCEPTION"
    sql = sql & " WHEN OTHERS THEN"
    sql = sql & " ROLLBACK;"
    sql = sql & " :COD_ERRORA := SQLCODE;"
    sql = sql & " :TXT_ERRORA := SQLERRM;"
    sql = sql & "END;"

    oradatabase.Parameters.Remove "cod"
    If frmVenda.txtCod_cliente = "" Then
        oradatabase.Parameters.Add "cod", 52306, 1
    Else
        oradatabase.Parameters.Add "cod", frmVenda.txtCod_cliente, 1
    End If
    oradatabase.Parameters.Remove "vend"
    oradatabase.Parameters.Add "vend", CLng(sCOD_VEND), 1
    oradatabase.Parameters.Remove "tp"
    If Option1.Value = True Then
        oradatabase.Parameters.Add "tp", 1, 1
    Else
        oradatabase.Parameters.Add "tp", 2, 1
    End If
    oradatabase.Parameters.Remove "res"
    oradatabase.Parameters.Add "res", Mid(cboresultado, 1, 2), 1
    oradatabase.Parameters.Remove "cod_errora"
    oradatabase.Parameters.Add "cod_errora", 0, 2
    oradatabase.Parameters.Remove "txt_errora"
    oradatabase.Parameters.Add "txt_errora", "", 2


    oradatabase.ExecuteSQL sql

    vErro = IIf(oradatabase.Parameters("cod_errora") = "", 0, oradatabase.Parameters("cod_errora"))
    If vErro <> 0 Then
        MsgBox oradatabase.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Exit Sub
    End If
    ' Call Consulta_Ligacoes
    Unload Me


End Sub


