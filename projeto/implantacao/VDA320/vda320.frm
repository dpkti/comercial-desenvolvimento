VERSION 4.00
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "Consulta Item"
   ClientHeight    =   4185
   ClientLeft      =   1545
   ClientTop       =   1920
   ClientWidth     =   6705
   Height          =   4875
   Icon            =   "vda320.frx":0000
   Left            =   1485
   LinkTopic       =   "MDIForm1"
   Top             =   1290
   Width           =   6825
   Begin Threed.SSPanel SSpMsg 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   3885
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   529
      _StockProps     =   15
      ForeColor       =   8388608
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Alignment       =   1
   End
   Begin Threed.SSPanel sspMenu 
      Align           =   1  'Align Top
      Height          =   495
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   6705
      _Version        =   65536
      _ExtentX        =   11827
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand SSCommand4 
         Height          =   495
         Left            =   120
         TabIndex        =   2
         Top             =   0
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   873
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "vda320.frx":030A
      End
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False

Option Explicit

Private Sub MDIForm_Load()
    On Error GoTo TrataErro

    Dim strLogin As String
    Dim ss As Object
    
    
    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
      End
    End If
    
    
    'posicionar tela
    MDIForm1.Top = 0
    MDIForm1.Left = 0
    MDIForm1.Width = Screen.Width
    MDIForm1.Height = Screen.Height
        
    'Call Get_CD(Mid(App.Path, 1, 1))

    'If strTp_banco = "U" Then
        strTabela_Banco = "PRODUCAO."
    'Else
     '   strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
    'End If
     
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA320/PROD", 0&)
   ' Set oradatabase = orasession.OpenDatabase("desenv", "VDA320/PROD", 0&)
    

        
    'carregar data de faturamento,uf origem, filial origem
          SQL = "select a.dt_faturamento, a.dt_real, "
    SQL = SQL & " to_char(b.cod_filial,'0009') || '-' || c.sigla filial, "
    SQL = SQL & " d.cod_uf cod_uf,"
    SQL = SQL & " to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default"
    SQL = SQL & " from datas a, " & strTabela_Banco & "deposito b, filial c,  cidade d, loja e "
    SQL = SQL & " where  b.cod_filial=c.cod_filial and "
    SQL = SQL & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
        
    If ss.EOF And ss.BOF Then
        Call Process_Line_Errors(SQL)
    Else
        Data_Faturamento = CDate(ss!DT_FATURAMENTO)
        data_real = CDate(ss!DT_REAL)
        FILIAL_PED = ss!FILIAL
        uf_origem = ss!COD_UF
        deposito_default = ss!deposito_default
    End If
    
    Screen.MousePointer = vbHourglass
    SSpMsg.Visible = False
    frmVenda.Top = 20
    frmVenda.Left = 20
    frmVenda.Width = 9400
    frmVenda.Height = 5920
    
    frmVenda.Show
    Screen.MousePointer = vbDefault
                
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 75 Then
       Resume
    Else
       Call Process_Line_Errors(SQL)
    End If
End Sub


Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
 
 SSpMsg.Caption = ""
 
End Sub


Private Sub mnuSair_Click()
    Set frmFabrica = Nothing
    Set frmVenda = Nothing
    Set frmAplicacao = Nothing
    Set frmBanco = Nothing
    Set frmClienteFimPedido = Nothing
    Set frmFornecedor = Nothing
    Set frmPlano = Nothing
    Set frmRepresentante = Nothing
    Set frmTransp = Nothing
    Set frmVendedor = Nothing
    End
End Sub


Private Sub mnuSobre_Click()

  frmSobre.Show 1

End Sub














Private Sub SSCommand4_Click()
  Call mnuSair_Click
End Sub


Private Sub SSCommand4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

  SSpMsg.Caption = "SAIR DO PROGRAMA"

End Sub





