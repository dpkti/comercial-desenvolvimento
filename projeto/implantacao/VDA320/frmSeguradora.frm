VERSION 5.00
Begin VB.Form frmSeguradora 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informa��es da Seguradora"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7650
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   7650
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdLimpar 
      Caption         =   "&Limpar"
      Height          =   375
      Left            =   3600
      TabIndex        =   6
      Top             =   2640
      Width           =   1095
   End
   Begin VB.CommandButton cmdSalvar 
      Caption         =   "S&alvar"
      Height          =   375
      Left            =   2160
      TabIndex        =   5
      Top             =   2640
      Width           =   1095
   End
   Begin VB.TextBox txtApolice 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   10
      TabIndex        =   4
      Top             =   1680
      Width           =   1695
   End
   Begin VB.TextBox txtSegurado 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   20
      TabIndex        =   3
      Top             =   1320
      Width           =   3975
   End
   Begin VB.TextBox txtPlaca 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   7
      TabIndex        =   2
      Top             =   960
      Width           =   975
   End
   Begin VB.TextBox txtCarro 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   15
      TabIndex        =   1
      Top             =   600
      Width           =   2175
   End
   Begin VB.TextBox txtSinistro 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   2040
      MaxLength       =   15
      TabIndex        =   0
      Top             =   240
      Width           =   2175
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5040
      TabIndex        =   7
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "N�mero da Ap�lice"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   12
      Top             =   1680
      Width           =   1620
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Nome do Segurado"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   11
      Top             =   1320
      Width           =   1635
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Placa"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   10
      Top             =   960
      Width           =   495
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Modelo do Carro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   9
      Top             =   600
      Width           =   1410
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "N�mero Sinistro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   8
      Top             =   240
      Width           =   1350
   End
End
Attribute VB_Name = "frmSeguradora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdLimpar_Click()
    txtSinistro = ""
    txtCarro = ""
    txtPlaca = ""
    txtSegurado = ""
    txtApolice = ""
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Command2_Click()

End Sub

Private Sub cmdSalvar_Click()
    If txtSinistro = "" Or _
       txtCarro = "" Or _
       txtPlaca = "" Or _
       txtSegurado = "" Or _
       txtApolice = "" Then
        MsgBox "Para salvar � necess�rio informar todos os dados", vbInformation, "Aten��o"
        Exit Sub
    End If

    strSinistro = txtSinistro
    strCarro = txtCarro
    strPlaca = txtPlaca
    strSegurado = txtSegurado
    strApolice = txtApolice
    Unload Me
End Sub

Private Sub Form_Load()
    txtSinistro = ""
    txtCarro = ""
    txtPlaca = ""
    txtSegurado = ""
    txtApolice = ""
End Sub

Private Sub txtApolice_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)

End Sub

Private Sub txtCarro_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)

End Sub

Private Sub txtPlaca_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)

End Sub

Private Sub txtSegurado_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)

End Sub

Private Sub txtSinistro_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)

End Sub
