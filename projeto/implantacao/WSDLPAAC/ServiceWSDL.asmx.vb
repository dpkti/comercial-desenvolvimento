Imports System.Web.Services
Imports Oracle.DataAccess
Imports DPKNet


<System.Web.Services.WebService(Namespace:="http://tempuri.org/WSDLPaac/ServiceWSDL")> _
Public Class ServiceWSDL
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub
    

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Con As Client.OracleConnection
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        Dim configurationAppSettings As System.Configuration.AppSettingsReader = New System.Configuration.AppSettingsReader
        Me.Con = New Client.OracleConnection

        '
        'Con
        '
        Me.Con.ConnectionString = CType(configurationAppSettings.GetValue("ConnectionString", GetType(System.String)), String)

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    ' WEB SERVICE EXAMPLE
    ' The HelloWorld() example service returns the string Hello World.
    ' To build, uncomment the following lines then save and build the project.
    ' To test this web service, ensure that the .asmx file is the start page
    ' and press F5.
    '
    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function

    <WebMethod(Description:="Verificar se o sistema est� liberado para uso.")> _
       Public Function VerificarAcesso() As Boolean
        Dim configurationAppSettings As New System.Configuration.AppSettingsReader
        Dim permitirAcesso As Boolean

        permitirAcesso = CType(configurationAppSettings.GetValue("PermitirAcesso", GetType(System.Boolean)), Boolean)

        Return permitirAcesso
    End Function

    <WebMethod(Description:="Essa fun��o busca os planos de pagamento pelo c�digo do cliente.")> _
   Public Function PlanoPagamento(ByVal pcodcliente As Int32) As DataSet

        Dim dt As DataTable
        Dim DS As New DataSet
        Dim enCliente As New Entidades.Cliente

        dt = enCliente.SelecionarPlanoPagto(pcodcliente)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
    <WebMethod(Description:="Essa fun��o busca o produto por C�digo de F�brica.")> _
  Public Function BuscarProduto(ByVal pCodFabrica As String) As DataSet

        '        Dim dt As DataTable
        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto

        '        dt = enProduto.SelecionarProduto(1, 0, 0, 0, "", "", 0, pCodFabrica.ToString, "", 0, "")

        Dim dt As DPKNet.Entidades.dsItemDestaque.pr_cesta_itemDataTable

        ' Utilizar filtros de busca
        dt = enProduto.SelecionarProduto_Tipado( _
             1, _
            0, _
            0, _
            0, _
            "", _
            "", _
            0, _
            pCodFabrica.ToString, _
            "", _
            0, "")

        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function

    <WebMethod(Description:="Essa fun��o busca o produto por de acordo com os par�metros.")> _
 Public Function SelecionarProduto(ByVal pCodLoja As Int16, ByVal pCodFabricante As Int16, ByVal pCodGrupo As Int16, ByVal pCodSubGrupo As Int16, ByVal pDescItem As String, ByVal pCodOriginal As String, ByVal pCodMontadora As Int16, ByVal pCodFabrica As String, ByVal pAplicacao As String, ByVal pPosCodOriginal As Int64, ByVal pPosCodFabrica As String, ByVal pCodDPK As Int32) As DataSet

        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto

        Dim dt As Entidades.dsItemDestaque.pr_cesta_itemDataTable

        ' Utilizar filtros de busca
        dt = enProduto.SelecionarProduto_Tipado(pCodLoja, _
                                                pCodFabricante, _
                                                pCodGrupo, _
                                                pCodSubGrupo, _
                                                pDescItem, _
                                                pCodOriginal, _
                                                pCodMontadora, _
                                                pCodFabrica, _
                                                pAplicacao, _
                                                0, _
                                                "", _
                                                pCodDPK)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
    <WebMethod(Description:="Essa fun��o retorna os fabricantes.")> _
Public Function SelecionarFabricantes(ByVal pCodFabricante As Int32) As DataSet

        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto

        Dim dt As DataTable

        ' Utilizar filtros de busca
        dt = enProduto.GetFabricante(pCodFabricante)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
    <WebMethod(Description:="Essa fun��o retorna os Grupos.")> _
Public Function SelecionarGrupo(ByVal pCodFabricante As Int32) As DataSet

        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto
        Dim dt As DataTable

        ' Utilizar filtros de busca
        dt = enProduto.GetGrupo(pCodFabricante)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
    <WebMethod(Description:="Essa fun��o retorna os Subgrupos.")> _
Public Function SelecionarSubGrupo(ByVal pCodGrupo As Int32, ByVal pCodFabricante As Int32) As DataSet

        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto

        Dim dt As DataTable

        ' Utilizar filtros de busca
        dt = enProduto.GetSubGrupo(pCodGrupo, pCodFabricante)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
    <WebMethod(Description:="Essa fun��o retorna as montadoras.")> _
Public Function SelecionarMontadora(ByVal pCodMontadora As Int32) As DataSet

        Dim DS As New DataSet
        Dim enProduto As New Entidades.Produto

        Dim dt As DataTable

        ' Utilizar filtros de busca
        dt = enProduto.SelecionarMontadora(pCodMontadora)
        DS.Tables.Add(dt)
        DS.Tables(0).TableName = "tabRetorno"
        Return DS

    End Function
End Class
