VERSION 4.00
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SOBRE"
   ClientHeight    =   3240
   ClientLeft      =   1590
   ClientTop       =   1620
   ClientWidth     =   6720
   Height          =   3645
   Left            =   1530
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   6720
   Top             =   1275
   Width           =   6840
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   2640
      TabIndex        =   3
      Top             =   2640
      Width           =   1335
      _version        =   65536
      _extentx        =   2355
      _extenty        =   873
      _stockprops     =   78
      caption         =   "OK"
      forecolor       =   8388608
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Respons�vel:  MARICI - SISTEMAS (Ramal 791/793)"
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   1920
      TabIndex        =   2
      Top             =   2160
      Width           =   2655
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Programa: VDA020  Consulta Cliente   (Vers�o: 2.3)"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   400
         size            =   9.75
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   1320
      TabIndex        =   1
      Top             =   1320
      Width           =   3855
   End
   Begin VB.Label lblDpk 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "DPK Distribuidora de Pe�as Ltda"
      BeginProperty Font 
         name            =   "Arial"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   6015
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub SSCommand1_Click()
  Unload frmSobre
End Sub


