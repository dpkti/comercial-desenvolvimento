VERSION 4.00
Begin VB.Form frmCadastro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "DADOS CADASTRAIS"
   ClientHeight    =   5310
   ClientLeft      =   360
   ClientTop       =   1350
   ClientWidth     =   8895
   Height          =   5715
   Left            =   300
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5310
   ScaleWidth      =   8895
   Top             =   1005
   Width           =   9015
   Begin VB.TextBox txtComprador 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   7200
      MaxLength       =   15
      TabIndex        =   43
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Frame Frame3 
      Height          =   855
      Left            =   3000
      TabIndex        =   30
      Top             =   4200
      Width           =   4695
      Begin VB.CheckBox chkSachs 
         Caption         =   "SACHS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   40
         Top             =   120
         Width           =   855
      End
      Begin VB.CheckBox chkOutros 
         Caption         =   "OUTROS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   37
         Top             =   480
         Width           =   1095
      End
      Begin VB.CheckBox chkSkf 
         Caption         =   "SKF"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   36
         Top             =   480
         Width           =   855
      End
      Begin VB.CheckBox chkVarga 
         Caption         =   "VARGA"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   35
         Top             =   120
         Width           =   855
      End
      Begin VB.CheckBox chkSabo 
         Caption         =   "SABO"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   34
         Top             =   480
         Width           =   855
      End
      Begin VB.CheckBox chkMetal 
         Caption         =   "METAL LEVE"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   33
         Top             =   120
         Width           =   1335
      End
      Begin VB.CheckBox chkBosch 
         Caption         =   "BOSCH"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   120
         Width           =   855
      End
      Begin VB.CheckBox chkCofap 
         Caption         =   "COFAP"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   480
         Width           =   855
      End
   End
   Begin VB.ComboBox cboTp_Cliente 
      ForeColor       =   &H00800000&
      Height          =   315
      Left            =   2880
      TabIndex        =   28
      Top             =   3840
      Width           =   2775
   End
   Begin VB.TextBox txtConectar 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   7320
      MaxLength       =   15
      TabIndex        =   26
      Top             =   3360
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2880
      TabIndex        =   22
      Top             =   3120
      Visible         =   0   'False
      Width           =   1815
      Begin VB.OptionButton optINao 
         Caption         =   "N�O"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   840
         TabIndex        =   24
         Top             =   120
         Width           =   855
      End
      Begin VB.OptionButton optISim 
         Caption         =   "SIM"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   23
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.Frame Frame2 
      Height          =   615
      Left            =   2880
      TabIndex        =   18
      Top             =   2520
      Width           =   1815
      Begin VB.OptionButton optSim 
         Caption         =   "SIM"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   20
         Top             =   120
         Width           =   615
      End
      Begin VB.OptionButton optNao 
         Caption         =   "N�O"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   840
         TabIndex        =   19
         Top             =   120
         Width           =   855
      End
   End
   Begin VB.TextBox txtFax 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   4560
      MaxLength       =   13
      TabIndex        =   16
      Top             =   2040
      Width           =   1335
   End
   Begin VB.TextBox txtFone2 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2640
      MaxLength       =   9
      TabIndex        =   14
      Top             =   2160
      Width           =   1335
   End
   Begin VB.TextBox txtDdd2 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2040
      MaxLength       =   4
      TabIndex        =   13
      Top             =   2160
      Width           =   615
   End
   Begin VB.TextBox txtFone1 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2640
      MaxLength       =   9
      TabIndex        =   12
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox txtDdd1 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2040
      MaxLength       =   4
      TabIndex        =   11
      Top             =   1920
      Width           =   615
   End
   Begin VB.TextBox txtCep 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2040
      MaxLength       =   8
      TabIndex        =   7
      Top             =   1200
      Width           =   855
   End
   Begin VB.TextBox txtBairro 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2040
      MaxLength       =   20
      TabIndex        =   5
      Top             =   840
      Width           =   1935
   End
   Begin VB.TextBox txtEndereco 
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   2040
      MaxLength       =   33
      TabIndex        =   4
      Top             =   480
      Width           =   3375
   End
   Begin VB.Label Label14 
      BackColor       =   &H00C0C0C0&
      Caption         =   "COMPRADOR"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6120
      TabIndex        =   42
      Top             =   2040
      Width           =   1095
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   7200
      TabIndex        =   41
      Top             =   480
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FRMCADAS.frx":0000
   End
   Begin VB.Label lblIE 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4560
      TabIndex        =   39
      Top             =   1560
      Width           =   1815
   End
   Begin VB.Label lblCgc 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   38
      Top             =   1560
      Width           =   1815
   End
   Begin VB.Label Label12 
      BackColor       =   &H00C0C0C0&
      Caption         =   "QUAIS AS LINHAS QUE TRABALHA ?"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   29
      Top             =   4560
      Width           =   2895
   End
   Begin VB.Label Label11 
      BackColor       =   &H00C0C0C0&
      Caption         =   "QUAL O TIPO DE CLIENTE ?"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   3840
      Width           =   2295
   End
   Begin VB.Label Label10 
      BackColor       =   &H00C0C0C0&
      Caption         =   "QUANDO IR� CONECTAR-SE ?"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4920
      TabIndex        =   25
      Top             =   3360
      Visible         =   0   'False
      Width           =   2415
   End
   Begin VB.Label Label9 
      BackColor       =   &H00C0C0C0&
      Caption         =   "EST� CONECTADO NA INTERNET ?"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   21
      Top             =   3240
      Visible         =   0   'False
      Width           =   2775
   End
   Begin VB.Label Label8 
      BackColor       =   &H00C0C0C0&
      Caption         =   "TEM COMPUTADOR NA LOJA ?"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   2760
      Width           =   2415
   End
   Begin VB.Label Label7 
      BackColor       =   &H00C0C0C0&
      Caption         =   "FAX"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4080
      TabIndex        =   15
      Top             =   2040
      Width           =   495
   End
   Begin VB.Label Label6 
      BackColor       =   &H00C0C0C0&
      Caption         =   "FONE"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   2040
      Width           =   975
   End
   Begin VB.Label Label5 
      BackColor       =   &H00C0C0C0&
      Caption         =   "I.E."
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4200
      TabIndex        =   9
      Top             =   1560
      Width           =   375
   End
   Begin VB.Label Label4 
      BackColor       =   &H00C0C0C0&
      Caption         =   "CGC"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   1680
      Width           =   975
   End
   Begin VB.Label Label3 
      BackColor       =   &H00C0C0C0&
      Caption         =   "CEP"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label lblNome_cliente 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   3
      Top             =   120
      Width           =   3375
   End
   Begin VB.Label Label2 
      BackColor       =   &H00C0C0C0&
      Caption         =   "BAIRRO"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label1 
      BackColor       =   &H00C0C0C0&
      Caption         =   "ENDERE�O"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   480
      Width           =   1095
   End
   Begin VB.Label Label13 
      BackColor       =   &H00C0C0C0&
      Caption         =   "NOME DA EMPRESA"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   1695
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit


Private Sub cboTp_Cliente_KeyPress(KeyAscii As Integer)
  KeyAscii = 0
  Beep
End Sub


Private Sub cboTp_Cliente_LostFocus()
  If cboTp_Cliente = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    cboTp_Cliente.SetFocus
    Exit Sub
  End If
End Sub




Private Sub FORM_Load()
  Dim ss As Object
  Dim i As Long
  
  lblNome_cliente = ""
  txtEndereco = ""
  txtBairro = ""
  txtCep = ""
  lblCgc = ""
  lblIe = ""
  txtDdd1 = ""
  txtDdd2 = ""
  txtFone1 = ""
  txtFone2 = ""
  txtFax = ""
  txtComprador = ""
  cboTp_Cliente.Text = ""
  
  SQL = "Select a.endereco,a.bairro,a.cep,"
  SQL = SQL & " to_char(a.cgc,'99999999999999') cgc,"
  SQL = SQL & " a.inscr_estadual, a.ddd1, a.fone1, a.ddd2,a.fone2,"
  SQL = SQL & " a.fax,a.cod_tipo_cliente, desc_tipo_cli,nome_contato"
  SQL = SQL & " from cliente a, tipo_cliente b"
  SQL = SQL & " Where a.cod_cliente = :cli and"
  SQL = SQL & " a.cod_tipo_cliente = b.cod_tipo_cli"
  
  oradatabase.Parameters.Remove "cli"
  oradatabase.Parameters.Add "cli", frmPesquisa.txtCod_cliente, 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
  
  If ss.EOF Then
   lblNome_cliente = ""
   txtEndereco = ""
   txtBairro = ""
   txtCep = ""
   lblCgc = ""
   lblIe = ""
   txtDdd1 = ""
   txtDdd2 = ""
   txtFone1 = ""
   txtFone2 = ""
   txtFax = ""
   cboTp_Cliente.Text = ""
   txtComprador = ""
   Exit Sub
  End If
  
  lblNome_cliente = strRazao
  txtEndereco = ss!endereco
  txtBairro = ss!bairro
  txtCep = ss!cep
  lblCgc = ss!cgc
  lblIe = ss!inscr_estadual
  txtComprador = IIf(IsNull(ss!nome_contato), "", ss!nome_contato)
  txtDdd1 = IIf(IsNull(ss!ddd1), 0, ss!ddd1)
  txtDdd2 = IIf(IsNull(ss!ddd2), 0, ss!ddd2)
  txtFone1 = IIf(IsNull(ss!fone1), 0, ss!fone1)
  txtFone2 = IIf(IsNull(ss!fone2), 0, ss!fone2)
  txtFax = IIf(IsNull(ss!fax), 0, ss!fax)
  cboTp_Cliente = Format(ss!cod_tipo_cliente, "00") & " - " _
     & ss!desc_tipo_cli
  
  SQL = "Select cod_tipo_cli,desc_tipo_cli"
  SQL = SQL & " from tipo_cliente "
  SQL = SQL & " order by desc_tipo_cli"


Set ss = oradatabase.dbcreatedynaset(SQL, 0&)

If cboTp_Cliente.ListCount = 0 Then
  For i = 1 To ss.RecordCount
    cboTp_Cliente.AddItem Format(ss!cod_tipo_cli, "00") & " - " _
     & ss!desc_tipo_cli
    ss.MoveNext
  Next
End If

  
  
End Sub








Private Sub optINao_Click()
  txtConectar.Visible = True
  Label10.Visible = True
End Sub

Private Sub optISim_Click()
  txtConectar.Visible = False
  Label10.Visible = False
End Sub

Private Sub optNao_Click()
  Frame1.Visible = False
  txtConectar.Visible = False
  Label9.Visible = False
End Sub


Private Sub optSim_Click()
  Label9.Visible = True
  Frame1.Visible = True
End Sub




Private Sub SSCommand1_Click()
  Dim strForn As String
     
     
  If optSim.Value = False And optNao.Value = False Then
    MsgBox "Favor verificar se o cliente tem computador", vbInformation, "Aten��o"
    Exit Sub
  ElseIf optISim.Value = False And optINao.Value = False And optSim = True Then
    MsgBox "Favor verificar se o cliente est� conectado a internet", vbInformation, "Aten��o"
    Exit Sub
  ElseIf optISim.Value = False And txtConectar = "" And optSim = True Then
    MsgBox "Favor verificar quando ir� se conectar", vbInformation, "Aten��o"
    Exit Sub
  End If
  
  strForn = ""
  If chkBosch.Value = 0 And _
     chkCofap.Value = 0 And _
     chkMetal.Value = 0 And _
     chksabo.Value = 0 And _
     chkSachs.Value = 0 And _
     chkSkf.Value = 0 And _
     chkVarga.Value = 0 And _
     chkOutros.Value = 0 Then
     MsgBox "Escolha um ou mais fornecedores que o cliente trabalha", vbInformation, "Aten��o"
     Exit Sub
  End If
 If chkBosch.Value = 1 Then
   strForn = "080-"
 End If
 If chkCofap.Value = 1 Then
   strForn = strForn & "578-"
 End If
 If chkMetal.Value = 1 Then
   strForn = strForn & "375-"
 End If
 If chksabo.Value = 1 Then
   strForn = strForn & "672-"
 End If
 If chkSachs.Value = 1 Then
   strForn = strForn & "055-"
 End If
 If chkSkf.Value = 1 Then
   strForn = strForn & "495-"
 End If
 If chkVarga.Value = 1 Then
   strForn = strForn & "585-"
 End If
 If chkOutros.Value = 1 Then
   strForn = strForn & "999-"
 End If
 
   SQL = "BEGIN"
   SQL = SQL & " UPDATE VENDAS.PESQUISA_INATIVO SET"
   SQL = SQL & " FL_COMPUTADOR = :FL_COMPUTADOR,"
   SQL = SQL & " FL_INTERNET = :FL_INTERNET,"
   SQL = SQL & " TEMPO_CONECTAR = :TEMPO,"
   SQL = SQL & " FORNECEDORES = :FORN"
   SQL = SQL & " WHERE COD_CLIENTE = :COD;"
   SQL = SQL & " COMMIT;"
   SQL = SQL & "EXCEPTION"
   SQL = SQL & " WHEN OTHERS THEN"
   SQL = SQL & " ROLLBACK;"
   SQL = SQL & " :cod_errora := SQLCODE;"
   SQL = SQL & " :txt_errora := SQLERRM;"
   SQL = SQL & "END;"
   
   Screen.MousePointer = 11
   oradatabase.Parameters.Remove "cod"
   oradatabase.Parameters.Add "cod", frmPesquisa.txtCod_cliente.Text, 1
   oradatabase.Parameters.Remove "fl_computador"
   If optSim.Value = True Then
     oradatabase.Parameters.Add "fl_computador", "S", 1
   Else
     oradatabase.Parameters.Add "fl_computador", "N", 1
   End If
   oradatabase.Parameters.Remove "fl_internet"
   If optISim.Value = True Then
     oradatabase.Parameters.Add "fl_internet", "S", 1
   Else
     oradatabase.Parameters.Add "fl_internet", "N", 1
   End If
   oradatabase.Parameters.Remove "tempo"
   If optISim.Value = True Then
     oradatabase.Parameters.Add "tempo", "JA CONECTADO", 1
   Else
     oradatabase.Parameters.Add "tempo", Trim(txtConectar), 1
   End If
  oradatabase.Parameters.Remove "forn"
  oradatabase.Parameters.Add "forn", strForn, 1
  oradatabase.Parameters.Remove "cod_errora"
  oradatabase.Parameters.Remove "txt_errora"
  oradatabase.Parameters.Add "cod_errora", 0, 2
  oradatabase.Parameters.Add "txt_errora", "", 2
  oradatabase.ExecuteSQL SQL
  If Val(oradatabase.Parameters("cod_errora").Value) <> 0 Then
    cod_errora = oradatabase.Parameters("cod_errora").Value
    txt_errora = oradatabase.Parameters("txt_errora").Value
    MsgBox cod_errora & " - " & txt_errora
    Screen.MousePointer = 0
    Exit Sub
  End If
   
   
   
   
   SQL = "BEGIN"
   SQL = SQL & " Update cliente set ddd1=nvl(:ddd1,0),"
   SQL = SQL & " ddd2=nvl(:ddd2,0),"
   SQL = SQL & " fone1=nvl(:fone1,0),"
   SQL = SQL & " fone2=nvl(:fone2,0),"
   SQL = SQL & " fax=nvl(:fax,0),"
   SQL = SQL & " bairro = nvl(:bairro,' '),"
   SQL = SQL & " endereco = nvl(:endereco,' '),"
   SQL = SQL & " cep = nvl(:cep,0),"
   SQL = SQL & " cod_tipo_cliente = :tipo,"
   SQL = SQL & " nome_contato=nvl(:contato,' ')"
   SQL = SQL & " where cod_cliente=:cod;"
   SQL = SQL & " COMMIT;"
   SQL = SQL & "EXCEPTION"
   SQL = SQL & " WHEN OTHERS THEN"
   SQL = SQL & " ROLLBACK;"
   SQL = SQL & " :cod_errora := SQLCODE;"
   SQL = SQL & " :txt_errora := SQLERRM;"
   SQL = SQL & "END;"

  oradatabase.Parameters.Remove "cod"
  oradatabase.Parameters.Remove "ddd1"
  oradatabase.Parameters.Remove "fone1"
  oradatabase.Parameters.Remove "ddd2"
  oradatabase.Parameters.Remove "fone2"
  oradatabase.Parameters.Remove "fax"
  oradatabase.Parameters.Remove "contato"
  oradatabase.Parameters.Remove "bairro"
  oradatabase.Parameters.Remove "endereco"
  oradatabase.Parameters.Remove "cep"
  oradatabase.Parameters.Remove "tipo"
  oradatabase.Parameters.Remove "cod_errora"
  oradatabase.Parameters.Remove "txt_errora"
  
  oradatabase.Parameters.Add "cod", frmPesquisa.txtCod_cliente.Text, 1
  oradatabase.Parameters.Add "ddd1", frmCadastro.txtDdd1.Text, 1
  oradatabase.Parameters.Add "fone1", frmCadastro.txtFone1.Text, 1
  oradatabase.Parameters.Add "ddd2", frmCadastro.txtDdd2.Text, 1
  oradatabase.Parameters.Add "fone2", frmCadastro.txtFone2.Text, 1
  oradatabase.Parameters.Add "fax", frmCadastro.txtFax.Text, 1
  oradatabase.Parameters.Add "contato", frmCadastro.txtComprador, 1
  oradatabase.Parameters.Add "bairro", frmCadastro.txtBairro, 1
  oradatabase.Parameters.Add "endereco", frmCadastro.txtEndereco, 1
  oradatabase.Parameters.Add "cep", frmCadastro.txtCep, 1
  oradatabase.Parameters.Add "tipo", Mid(Trim(frmCadastro.cboTp_Cliente), 1, 2), 1
  oradatabase.Parameters.Add "cod_errora", 0, 2
  oradatabase.Parameters.Add "txt_errora", "", 2
  oradatabase.ExecuteSQL SQL
  If Val(oradatabase.Parameters("cod_errora").Value) <> 0 Then
     cod_errora = oradatabase.Parameters("cod_errora").Value
     txt_errora = oradatabase.Parameters("txt_errora").Value
     MsgBox cod_errora & " - " & txt_errora
     Screen.MousePointer = 0
     Exit Sub
   Else
     MsgBox "Atualiza��o OK", vbInformation, "Aten��o"
     Screen.MousePointer = 0
   End If
   Unload Me


End Sub




Private Sub txtBairro_KeyPress(KeyAscii As Integer)
 KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtBairro_LostFocus()
  If txtBairro = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtBairro.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtCep_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtCep_LostFocus()
  If txtCep = "" Or txtCep = "0" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtCep.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtComprador_KeyPress(KeyAscii As Integer)
   KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtComprador_LostFocus()
  If txtComprador = "" Or txtComprador = "0" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtComprador.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtConectar_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtDdd1_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtDdd1_LostFocus()
  If txtDdd1 = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtDdd1.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtDdd2_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtDdd2_LostFocus()
  If txtDdd2 = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtDdd2.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtEndereco_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


Private Sub txtEndereco_LostFocus()
  If txtEndereco = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtEndereco.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtFax_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtFax_LostFocus()
  If txtFax = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtFax.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtFone1_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtFone1_LostFocus()
  If txtFone1 = "" Or txtFone1 = "0" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtFone1.SetFocus
    Exit Sub
  End If
End Sub


Private Sub txtFone2_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtFone2_LostFocus()
  If txtFone2 = "" Then
    MsgBox "Este campo deve ser preenchido", vbInformation, "Aten��o"
    txtFone2.SetFocus
    Exit Sub
  End If
End Sub


