VERSION 4.00
Begin VB.Form frmCliente 
   BorderStyle     =   0  'None
   Caption         =   "Dados Cadastrais"
   ClientHeight    =   4770
   ClientLeft      =   1440
   ClientTop       =   1425
   ClientWidth     =   6735
   ForeColor       =   &H00800000&
   Height          =   5175
   Left            =   1380
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   4770
   ScaleWidth      =   6735
   ShowInTaskbar   =   0   'False
   Top             =   1080
   Width           =   6855
   WindowState     =   2  'Maximized
   Begin Threed.SSFrame SSFrame1 
      Height          =   735
      Left            =   240
      TabIndex        =   3
      Top             =   240
      Width           =   9015
      _version        =   65536
      _extentx        =   15901
      _extenty        =   1296
      _stockprops     =   14
      forecolor       =   16777215
      shadowstyle     =   1
      Begin VB.TextBox txtCgc 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   6960
         MaxLength       =   14
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox txtCodigo 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1200
         MaxLength       =   6
         TabIndex        =   0
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox txtNome 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2040
         TabIndex        =   1
         Top             =   240
         Width           =   4335
      End
      Begin VB.Label lblCliente 
         Caption         =   "C�digo"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   855
      End
      Begin VB.Label lblCgc 
         Caption         =   "CGC"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6480
         TabIndex        =   4
         Top             =   240
         Width           =   495
      End
   End
   Begin Threed.SSFrame SSFrame2 
      Height          =   1575
      Left            =   240
      TabIndex        =   43
      Top             =   4080
      Visible         =   0   'False
      Width           =   9015
      _version        =   65536
      _extentx        =   15901
      _extenty        =   2778
      _stockprops     =   14
      caption         =   "Caracter�stica"
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      Begin VB.Label lblMsg 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   61
         Top             =   1080
         Width           =   4815
      End
      Begin VB.Label Label3 
         Caption         =   "Msg.: "
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   60
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label lblConceito 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5880
         TabIndex        =   59
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Conceito"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   58
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblSituacao 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5880
         TabIndex        =   53
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label lblSit 
         Caption         =   "Situa��o"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   52
         Top             =   360
         Width           =   855
      End
      Begin VB.Label lblCategoria 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   51
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblCa 
         Caption         =   "Categoria"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   50
         Top             =   840
         Width           =   855
      End
      Begin VB.Label lblClass 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   49
         Top             =   1080
         Width           =   375
      End
      Begin VB.Label lblCl 
         Caption         =   "Classif."
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   1080
         Width           =   735
      End
      Begin VB.Label lblTpblau 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   47
         Top             =   600
         Width           =   2895
      End
      Begin VB.Label lblTp2 
         Caption         =   "Tipo BLAU"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   600
         Width           =   975
      End
      Begin VB.Label lblTpdpk 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   45
         Top             =   360
         Width           =   2895
      End
      Begin VB.Label lblTP1 
         Caption         =   "Tipo DPK"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Label lblIsuframa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7560
      TabIndex        =   65
      Top             =   1320
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblIS 
      Caption         =   "I.Suframa"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6600
      TabIndex        =   64
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblLimCredito 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7440
      TabIndex        =   63
      Top             =   3720
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lbllm 
      Caption         =   "Limite Cr�dito"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6240
      TabIndex        =   62
      Top             =   3720
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label lblDtcadastro 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1320
      TabIndex        =   57
      Top             =   3480
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDtcad 
      Caption         =   "Dt.Cadastro"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   56
      Top             =   3480
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSGrid.Grid Grid1 
      Height          =   255
      Left            =   2280
      TabIndex        =   55
      Top             =   240
      Visible         =   0   'False
      Width           =   735
      _version        =   65536
      _extentx        =   1296
      _extenty        =   450
      _stockprops     =   77
      backcolor       =   16777215
      fixedcols       =   0
      mouseicon       =   "FRMCLIEN.frx":0000
   End
   Begin Threed.SSCommand ssDupl 
      Height          =   495
      Left            =   4410
      TabIndex        =   54
      Top             =   3375
      Visible         =   0   'False
      Width           =   1575
      _version        =   65536
      _extentx        =   2778
      _extenty        =   873
      _stockprops     =   78
      caption         =   "Saldo Duplicatas"
      BeginProperty font {FB8F0823-0164-101B-84ED-08002B2EC713} 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblDvencer 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7440
      TabIndex        =   42
      Top             =   3240
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblDVencido 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   7440
      TabIndex        =   41
      Top             =   3480
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.Label lblVcido 
      Caption         =   "Vencido"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6240
      TabIndex        =   40
      Top             =   3480
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblV 
      Caption         =   "� Vencer"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6240
      TabIndex        =   39
      Top             =   3240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblUltc 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1320
      TabIndex        =   38
      Top             =   3240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblUc 
      Caption         =   "Ult.Compra"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   37
      Top             =   3240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblIe 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7560
      TabIndex        =   36
      Top             =   1080
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.Label lblIee 
      Caption         =   "I.E"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7200
      TabIndex        =   35
      Top             =   1080
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Label lblRep2 
      Caption         =   "Rep.BLAU"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5640
      TabIndex        =   34
      Top             =   2760
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDrblau 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7200
      TabIndex        =   33
      Top             =   2640
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCrblau 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6600
      TabIndex        =   32
      Top             =   2640
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblDrdpk 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   7200
      TabIndex        =   31
      Top             =   2400
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label lblCrdpk 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6600
      TabIndex        =   30
      Top             =   2400
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblrep1 
      Caption         =   "Rep. DPK"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   5640
      TabIndex        =   29
      Top             =   2400
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblDtransp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   28
      Top             =   2760
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.Label lblCtransp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   27
      Top             =   2760
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblDbco 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   26
      Top             =   2520
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblCbco 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   25
      Top             =   2520
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblTra 
      Caption         =   "Transp."
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   24
      Top             =   2760
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblbco 
      Caption         =   "Banco"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   23
      Top             =   2520
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblFax 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3600
      TabIndex        =   22
      Top             =   2040
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.Label lblfx 
      Caption         =   "Fax"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2760
      TabIndex        =   21
      Top             =   2040
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblContato 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3600
      TabIndex        =   20
      Top             =   1800
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblCont 
      Caption         =   "Contato"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2760
      TabIndex        =   19
      Top             =   1800
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label lblFone2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   18
      Top             =   2040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDdd2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   17
      Top             =   2040
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblFone1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1680
      TabIndex        =   16
      Top             =   1800
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label lblDdd1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   15
      Top             =   1800
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblFone 
      Caption         =   "Fone"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   1920
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCp 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6240
      TabIndex        =   13
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblCpx 
      Caption         =   "Caixa Postal"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   5400
      TabIndex        =   12
      Top             =   1800
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblCep 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4920
      TabIndex        =   11
      Top             =   1320
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label lblUf 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4320
      TabIndex        =   10
      Top             =   1320
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.Label lblCidade 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   9
      Top             =   1320
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Label lblBairro 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   4920
      TabIndex        =   8
      Top             =   1080
      Visible         =   0   'False
      Width           =   2175
   End
   Begin VB.Label lblEnd 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   7
      Top             =   1080
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.Label lblEnde 
      Caption         =   "Endereco"
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   8.25
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "frmCliente"
Attribute VB_Creatable = False
Attribute VB_Exposed = False





Private Sub Grid1_DblClick()
    
  Dim cod As Double
    
   Grid1.Col = 1
   cod = Grid1.Text
   Grid1.Visible = False
   SSFRAME1.Visible = True
   oradatabase.Parameters.Remove "cod"
   oradatabase.Parameters.Add "cod", cod, 1
   Set oradynaset = oradatabase.dbcreatedynaset(sel1, 0&)
    
   If oradynaset.EOF Then
     MsgBox "N�O LOCALIZOU COD:" & cod, 0, "ATEN��O"
   End If
    
    oradatabase.Parameters.Remove "cod"
    oradatabase.Parameters.Remove "nome"
    oradatabase.Parameters.Remove "cgc"
  
    Call DADOS_TELA
  
  Exit Sub

  
End Sub




Private Sub ssDupl_Click()
 
   frmCliente.ssDupl.Enabled = False
   oradatabase.Parameters.Remove "cod"
   oradatabase.Parameters.Add "cod", frmCliente.txtcodigo.Text, 1
   Set oradynaset = oradatabase.dbcreatedynaset(sel4, 0&)
  
   oradatabase.Parameters.Remove "cod"
   
   If oradynaset.EOF Then
      frmCliente.lblDvencer.Caption = 0
      frmCliente.lblDVencido.Caption = 0
   Else
      frmCliente.lblDvencer.Caption = oradynaset.Fields("valor").Value
      vencer = vencer + oradynaset.Fields("valor").Value
      oradynaset.MoveNext
      frmCliente.lblDVencido.Caption = oradynaset.Fields("valor").Value
      vencido = vencido + oradynaset.Fields("valor").Value
   End If

   oradatabase.Parameters.Remove "cod"
   oradatabase.Parameters.Add "cod", frmCliente.txtcodigo.Text, 1
   Set oradynaset = oradatabase.dbcreatedynaset(sel13, 0&)
   
   saldo = IIf(IsNull(oradynaset!VALOR1), 0, oradynaset!VALOR1)
   saldo_pedidos = 0
   For i = 1 To oradynaset.RecordCount
     saldo_pedidos = saldo_pedidos + oradynaset.Fields("saldo_pedidos").Value
     oradynaset.MoveNext
   Next
   
   
   limite = saldo - (vencer + vencido + saldo_pedidos)
   limite = Format(limite, "###,###,##0.00")
   If limite < 0 Then
     frmCliente.lblLimCredito.Caption = 0
   Else
     frmCliente.lblLimCredito.Caption = limite
   End If
   
   If vencido = 0 Then
     frmCliente.lblDVencido.ForeColor = &H800000
   Else
     frmCliente.lblDVencido.ForeColor = &HFF&
   End If
   
   limite = 0
   saldo = 0
   vencer = 0
   vencido = 0
   saldo_pedidos = 0
   
   
   frmCliente.lblV.Visible = True
   frmCliente.lblVcido.Visible = True
   frmCliente.lblDvencer.Visible = True
   frmCliente.lblDVencido.Visible = True
   frmCliente.lbllm.Visible = True
   frmCliente.lblLimCredito.Visible = True
   
    
   
End Sub


Private Sub txtCgc_Change()

If txtCgc <> "" Then
    txtNome.Enabled = False
    txtcodigo.Enabled = False
Else
    txtNome.Enabled = True
    txtcodigo.Enabled = True
End If

End Sub

Private Sub txtCgc_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCodigo_Change()

If txtcodigo <> "" Then
    txtNome.Enabled = False
    txtCgc.Enabled = False
Else
    txtNome.Enabled = True
    txtCgc.Enabled = True
End If

End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtNome_Change()
txtNome = UCase(txtNome)

If txtNome <> "" Then
    txtcodigo.Enabled = False
    txtCgc.Enabled = False
Else
    txtcodigo.Enabled = True
    txtCgc.Enabled = True
End If

End Sub


Private Sub txtNome_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


