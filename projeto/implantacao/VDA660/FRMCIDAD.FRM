VERSION 4.00
Begin VB.Form frmCidade 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pesquisa de Cliente por Cidade"
   ClientHeight    =   2805
   ClientLeft      =   1260
   ClientTop       =   2250
   ClientWidth     =   6720
   Height          =   3210
   Left            =   1200
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2805
   ScaleWidth      =   6720
   Top             =   1905
   Width           =   6840
   Begin VB.ComboBox cboCidade 
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   2760
      TabIndex        =   3
      Top             =   360
      Width           =   3255
   End
   Begin VB.ComboBox cboUF 
      ForeColor       =   &H00800000&
      Height          =   300
      Left            =   960
      TabIndex        =   1
      Top             =   360
      Width           =   735
   End
   Begin VB.Label Label2 
      Caption         =   "Cidade"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2040
      TabIndex        =   4
      Top             =   360
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "UF"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   360
      Width           =   255
   End
   Begin MSGrid.Grid grdCidade 
      Height          =   1215
      Left            =   600
      TabIndex        =   0
      Top             =   1080
      Visible         =   0   'False
      Width           =   5415
      _version        =   65536
      _extentx        =   9551
      _extenty        =   2143
      _stockprops     =   77
      forecolor       =   8388608
      backcolor       =   16777215
      fixedcols       =   0
      mouseicon       =   "FRMCIDAD.frx":0000
   End
End
Attribute VB_Name = "frmCidade"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Dim arrcidade() As Integer
Dim cidade As Long

Private Sub cboCidade_Click()
 Dim sql As String
 Dim ss As Object
 Dim i As Integer
 
  Screen.MousePointer = 11
  grdCidade.Visible = False
  cidade = arrcidade(cboCidade.ListIndex + 1)
  
   'montar SQL
    sql = "select a.nome_cliente, "
    sql = sql & " a.cod_cliente, "
    sql = sql & " nvl(SUM(b.vl_duplicata - b.vl_desconto - b.vl_abatimento -"
    sql = sql & " b.vl_devolucao - b.vl_pago),0) nr_duplicata"
    sql = sql & " from cliente a,cobranca.duplicatas b"
    sql = sql & " where a.cod_cidade = :cidade and "
    sql = sql & " a.situacao=0 and "
    sql = sql & " b.cod_cliente(+) = a.cod_cliente and"
    sql = sql & " b.situacao_pagto(+) = 0"
    sql = sql & " group by a.nome_cliente,a.cod_cliente"
    sql = sql & " order by nr_duplicata DESC,a.nome_cliente"

    'criar consulta
    oradatabase.Parameters.Remove "cidade"
    oradatabase.Parameters.Add "cidade", cidade, 1
    
    Set ss = oradatabase.dbcreatedynaset(sql, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o cliente cadastrado para a cidade escolhida", vbInformation, "Aten��o"
        Exit Sub
        Unload Me
    End If
    
    'carrega dados
    With grdCidade
        .Cols = 3
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 2800
        .ColWidth(2) = 1500
        
        .Row = 0
        .Col = 0
        .Text = "  C�digo"
        .Col = 1
        .Text = "                Cliente"
        .Col = 2
        .Text = " Vl.Total Aberto"
    
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!COD_CLIENTE
            .Col = 1
            .Text = ss!nome_cliente
            .Col = 2
            .ColAlignment(2) = 1
            .Text = Format(ss!nr_duplicata, "###,###,##0.00")
             
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    grdCidade.Visible = True
    Screen.MousePointer = 0

End Sub


Private Sub cboUF_Click()
  Dim cont As Integer
  
  cboCidade.Text = ""
  Erase arrcidade
  cidade = 0
  grdCidade.Visible = False

  Screen.MousePointer = 11
  cboCidade.Clear
  oradatabase.Parameters.Remove "uf"
  oradatabase.Parameters.Add "uf", cboUF.Text, 1
  Set oradynaset = oradatabase.dbcreatedynaset(sel16, 0&)
  
 oradynaset.MoveFirst
 For cont = 1 To oradynaset.RecordCount
     ReDim Preserve arrcidade(1 To oradynaset.RecordCount)
     arrcidade(cont) = oradynaset!cod_cidade
     cboCidade.AddItem oradynaset!nome_cidade
     oradynaset.MoveNext
 Next
 
 Screen.MousePointer = 0
  
End Sub


Private Sub FORM_Load()
Dim cont As Integer

 If cboUF.ListCount = 0 Then
   Set oradynaset = oradatabase.dbcreatedynaset(sel15, 0&)
  
   oradynaset.MoveFirst
   For cont = 1 To oradynaset.RecordCount
     cboUF.AddItem oradynaset!cod_uf
     oradynaset.MoveNext
   Next
 End If
 
 
End Sub


Private Sub grdCidade_Click()
  grdCidade.Col = 0
  frmCliente.txtcodigo.Text = grdCidade.Text
  frmCidade.Hide
  
  
End Sub

