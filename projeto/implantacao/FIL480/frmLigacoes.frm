VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmLigacoes 
   Caption         =   "Liga��es Ativas/Receptivas"
   ClientHeight    =   5280
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4920
   Icon            =   "frmLigacoes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5280
   ScaleWidth      =   4920
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdExportar 
      Caption         =   "Exportar para Excel"
      Height          =   255
      Left            =   3300
      TabIndex        =   6
      Top             =   4560
      Width           =   1545
   End
   Begin MSComctlLib.StatusBar STB 
      Align           =   2  'Align Bottom
      Height          =   405
      Left            =   0
      TabIndex        =   5
      Top             =   4875
      Width           =   4920
      _ExtentX        =   8678
      _ExtentY        =   714
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   8176
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdPesquisar 
      Caption         =   "Pesquisar"
      Height          =   285
      Left            =   3990
      TabIndex        =   3
      Top             =   30
      Width           =   885
   End
   Begin VB.TextBox txtMesAno 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   2940
      TabIndex        =   2
      Top             =   30
      Width           =   1005
   End
   Begin MSComctlLib.ListView lsvLigacoes 
      Height          =   4185
      Left            =   90
      TabIndex        =   0
      Top             =   330
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   7382
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "CD"
         Object.Width           =   706
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome Loja"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   2
         Text            =   "Ativas"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   1
         SubItemIndex    =   3
         Text            =   "Receptivas"
         Object.Width           =   1764
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "M�s/Ano:"
      Height          =   195
      Left            =   2190
      TabIndex        =   4
      Top             =   90
      Width           =   705
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Liga��es:"
      Height          =   195
      Left            =   90
      TabIndex        =   1
      Top             =   120
      Width           =   690
   End
End
Attribute VB_Name = "frmLigacoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vCdDPK As String

Dim vSessao_CDDPK As Object
Dim vBanco_CDDPK As Object

Dim vSessao_Prod As Object
Dim vBanco_Prod As Object

Dim vDepAtual As String
Dim vCd As Byte

Const ORATYPE_CURSOR = 102
Const ORADYN_NO_BLANKSTRIP = &H2&
Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Const ORATYPE_NUMBER = 2


Private Sub cmdExportar_Click()
    Dim oExcel As Object
    Dim objExlSht As Object
    Dim i As Integer
    Dim ii As Integer

    Set oExcel = CreateObject("Excel.Application")
    oExcel.Workbooks.Add                                           'inclui o workbook
    Set objExlSht = oExcel.ActiveWorkbook.Sheets(1)
 
    oExcel.Visible = True

    oExcel.Cells(1, 1) = "Cod"
    oExcel.Cells(1, 2) = "Loja"
    oExcel.Cells(1, 3) = "Ativas"
    oExcel.Cells(1, 4) = "Receptivas"
    
    For i = 1 To Me.lsvLigacoes.ListItems.Count
        oExcel.Cells(i + 1, 1).Select
        If lsvLigacoes.ListItems(i).ForeColor = vbBlue Then
            oExcel.ActiveCell.Font.Color = vbBlue
        End If
        oExcel.ActiveCell = lsvLigacoes.ListItems(i)
        For ii = 1 To lsvLigacoes.ColumnHeaders.Count - 1
            oExcel.Cells(i + 1, ii + 1).Select
            oExcel.ActiveCell = lsvLigacoes.ListItems(i).SubItems(ii)
        Next
    Next

    Set objExlSht = Nothing
    Set oExcel = Nothing

End Sub

Private Sub cmdPesquisar_Click()
        
    If txtMesAno = "" Or Len(txtMesAno) <> 5 Then
        MsgBox "M�s/Ano incorreto, por favor verifique !", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    Me.MousePointer = vbHourglass
    
    Me.STB.Panels(1) = "Aguarde... Consultando..."
    
    Conectar_Nos_Cds
    
    Me.STB.Panels(1) = "Consulta Concluida!"

    Me.MousePointer = vbNormal

End Sub

Private Sub Form_Load()
    
    Set vSessao_CDDPK = CreateObject("oracleinprocserver.xorasession")
    Set vBanco_CDDPK = vSessao_CDDPK.OpenDatabase("CDDPK", "VDA020/prod", 0&)
                
    Set vSessao_Prod = CreateObject("oracleinprocserver.xorasession")
    Set vBanco_Prod = vSessao_Prod.OpenDatabase("PRODUCAO", "VDA020/prod", 0&)
    
    vCdDPK = Pegar_VL_Parametro("CDDPK")
    
    Preencher_Lsv
    
    Me.txtMesAno = Format(DateAdd("m", -1, Date), "MM/YY")
    
End Sub

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    
    Dim vOwner As String
    Dim vBanco As String
    Dim vObj As Object
    Dim vDep As String
    
    vBanco_Prod.Parameters.Remove "Cod_Sistema"
    vBanco_Prod.Parameters.Add "Cod_Sistema", 1069, 1

    vBanco_Prod.Parameters.Remove "Nome_Parametro"
    vBanco_Prod.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1
    
    vBanco_Prod.Parameters.Remove "DEP"
    vBanco_Prod.Parameters.Add "DEP", "HELPDESK.", 1
        
    Criar_Cursor vBanco_Prod
    vBanco_Prod.executesql ("BEGIN PRODUCAO.PCK_VDA230.PR_SELECT_VL_PARAMETRO2(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP); END;")
    Set vObj = vBanco_Prod.Parameters("vCursor").Value
    
    Pegar_VL_Parametro = vObj("VL_PARAMETRO")
    
End Function

Public Sub Criar_Cursor(pBanco As Object)
    pBanco.Parameters.Remove "vCursor"
    pBanco.Parameters.Add "vCursor", 0, 2
    pBanco.Parameters("vCursor").ServerType = ORATYPE_CURSOR
    pBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    pBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Sub Preencher_Lsv()
    Dim i As Integer
    Dim vObj As Object
    Dim Litem As ListItem
    Set vObj = vBanco_Prod.createdynaset("SELECT A.cod_loja, b.nome_fantasia FROM loja_ativa A, Loja B Where A.cod_loja = b.cod_loja", &O0)
    For i = 1 To vObj.RecordCount
        Set Litem = Me.lsvLigacoes.ListItems.Add
        Litem = vObj!cod_loja
        Litem.SubItems(1) = vObj!nome_fantasia
        
        If InStr(1, vCdDPK, Format(vObj!cod_loja, "00")) > 0 Then
           Litem.ForeColor = vbBlue
        End If
        
        vObj.MOVENEXT
    Next
    Set vObj = Nothing
End Sub

Private Sub Conectar_Nos_Cds()
    Dim vDBLink As String
    Dim vSql As String
    Dim vObj As Object
    Dim i As Integer
    
    For i = 1 To lsvLigacoes.ListItems.Count
        
        STB.Panels(1) = "Consultando CD: " & lsvLigacoes.ListItems(i)
        
        vCd = lsvLigacoes.ListItems(i)
        'If vCd = 2 Or vCd = 6 Or vCd = 20 Then GoTo proximo
        If vCd = 2 Or vCd = 6 Then GoTo proximo
        If InStr(1, vCdDPK, Format(Val(vCd), "00")) > 0 Then
            vSql = "Select decode(tipo_ligacao,1,'Ativas','Passivas'), count(*) From VENDAS.contato "
            vSql = vSql & " Where  trunc(dt_contato) >= TO_DATE('01/" & txtMesAno & "', 'DD/MM/RR') and trunc(dt_contato) < ADD_MONTHS(TO_DATE('01/" & txtMesAno & "', 'DD/MM/RR'), 1) group by tipo_ligacao order by tipo_ligacao"
            Set vObj = vBanco_CDDPK.createdynaset(vSql, 0&)
        Else
            Select Case Val(vCd)
                Case 1
                    vDBLink = ""
                Case Else
                    vDBLink = "@LNK_CD" & Format(vCd, "00") & ".cal.com.br"
            End Select
            vSql = "Select decode(tipo_ligacao,1,'Ativas','Passivas'), count(*) From vendas.contato" & vDBLink
            vSql = vSql & " Where  trunc(dt_contato) >= TO_DATE('01/" & txtMesAno & "', 'DD/MM/RR') and trunc(dt_contato) < ADD_MONTHS(TO_DATE('01/" & txtMesAno & "', 'DD/MM/RR'), 1) group by tipo_ligacao order by tipo_ligacao"
            Set vObj = vBanco_Prod.createdynaset(vSql, &O0)
        End If
        
        If Not vObj.EOF Then
        
            If UCase(vObj.FIELDS(0)) = "ATIVAS" Then lsvLigacoes.ListItems(i).SubItems(2) = vObj.FIELDS(1)
        
            vObj.MOVENEXT
        
        End If
        
        If Not vObj.EOF Then
        
            If UCase(vObj.FIELDS(0)) = "PASSIVAS" Then lsvLigacoes.ListItems(i).SubItems(3) = vObj.FIELDS(1)
        
        End If
        
        Set vObj = Nothing
        
        Me.lsvLigacoes.Refresh
        
proximo:
    Next
    
End Sub

Function fData(pData As String)
    Select Case Val(Month(pData))
        Case 1
            fData = "JAN-" & Format(CDate(pData), "YY")
        Case 2
            fData = "FEB-" & Format(CDate(pData), "YY")
        Case 3
            fData = "MAR-" & Format(CDate(pData), "YY")
        Case 4
            fData = "APR-" & Format(CDate(pData), "YY")
        Case 5
            fData = "MAY-" & Format(CDate(pData), "YY")
        Case 6
            fData = "JUN-" & Format(CDate(pData), "YY")
        Case 7
            fData = "JUL-" & Format(CDate(pData), "YY")
        Case 8
            fData = "AUG-" & Format(CDate(pData), "YY")
        Case 9
            fData = "SEP-" & Format(CDate(pData), "YY")
        Case 10
            fData = "OCT-" & Format(CDate(pData), "YY")
        Case 11
            fData = "NOV-" & Format(CDate(pData), "YY")
        Case 12
            fData = "DEC-" & Format(CDate(pData), "YY")
    End Select
End Function
