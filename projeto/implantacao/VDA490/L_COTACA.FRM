VERSION 4.00
Begin VB.Form frmListaCotacao 
   Caption         =   "Lista de Cotacao"
   ClientHeight    =   4710
   ClientLeft      =   1245
   ClientTop       =   1395
   ClientWidth     =   7875
   ForeColor       =   &H00800000&
   Height          =   5115
   Left            =   1185
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   7875
   Top             =   1050
   Width           =   7995
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   6600
      TabIndex        =   0
      Top             =   4200
      Width           =   1095
   End
   Begin VB.Label lblVendedor 
      AutoSize        =   -1  'True
      Caption         =   "Vendedor "
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   2280
      TabIndex        =   2
      Top             =   240
      Width           =   885
   End
   Begin MSGrid.Grid grdCotacao 
      Height          =   3375
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Visible         =   0   'False
      Width           =   7575
      _Version        =   65536
      _ExtentX        =   13361
      _ExtentY        =   5953
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "L_COTACA.frx":0000
   End
End
Attribute VB_Name = "frmListaCotacao"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String

Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmListaCotacao = Nothing
End Sub


Private Sub Form_Load()
Dim ss As Object
Dim SQL As String
Dim i As Integer

   
    
    'posicao do form
    frmListaCotacao.Top = 1500
    frmListaCotacao.Left = 600
    frmListaCotacao.Width = 7950
    frmListaCotacao.Height = 5115
    
    
    
    'setar vendedor
    SQL = "select PSEUDONIMO from REPRESENTANTE where "
    SQL = SQL & " COD_REPRES = :vend"
    
    oradatabase.Parameters.Remove "vend"
    frmcotacao.Grid1.Col = 0
    oradatabase.Parameters.Add "vend", frmcotacao.Grid1.Text, 1
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
    lblVendedor.Caption = lblVendedor.Caption & ": " & frmcotacao.Grid1.Text & " - " & ss("PSEUDONIMO")
    
    '
    
     'montar SQL
    SQL = "select COD_REPRES from R_REPVEN where COD_VEND = :vend"
    
    oradatabase.Parameters.Remove "vend"
    oradatabase.Parameters.Add "vend", frmcotacao.Grid1.Text, 1
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)

 
    SQL = "select cot.cod_loja,cot.NUM_COTACAO,cot.DT_COTACAO,cot.FL_TIPO,"
    SQL = SQL & " cot.DT_VALIDADE,cli.NOME_CLIENTE "
    SQL = SQL & " from vendas.COTACAO cot, CLIENTE cli,DATAS dt "
    SQL = SQL & " where cot.COD_CLIENTE = cli.COD_CLIENTE and "
    SQL = SQL & " cot.dt_cotacao >= to_date(:inicio,'dd/mm/rr') and "
    SQL = SQL & " cot.dt_cotacao <= to_date(:final,'dd/mm/rr') + 1 and"
    SQL = SQL & " cot.SITUACAO = 0 and "
   ' SQL = SQL & " cot.COD_LOJA = 01 and "
    If ss.EOF And ss.BOF Then
        SQL = SQL & "cot.COD_VEND = :vend "
    Else
        SQL = SQL & "cot.COD_VEND in (:vend "
        Do
            SQL = SQL & "," & ss("COD_REPRES")
            ss.MoveNext
        Loop Until ss.EOF
        SQL = SQL & ")"
    End If
    SQL = SQL & " order by cot.NUM_COTACAO desc"
    
    
    'criar consulta
    oradatabase.Parameters.Remove "vend"
    oradatabase.Parameters.Add "vend", frmcotacao.Grid1.Text, 1
    oradatabase.Parameters.Remove "inicio"
    oradatabase.Parameters.Add "inicio", frmcotacao.txtInicio.Text, 1
    oradatabase.Parameters.Remove "final"
    oradatabase.Parameters.Add "final", frmcotacao.txtFim.Text, 1
        
        
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha Cotacao para o Vendedor " & frmcotacao.Grid1.Text, vbInformation, "Aten��o"
        Exit Sub
    End If

    'carrega dados
    With frmListaCotacao.grdCotacao
        .Cols = 6
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 660
        .ColWidth(2) = 1440
        .ColWidth(3) = 735
        .ColWidth(4) = 495
        .ColWidth(5) = 3907
        
        .Row = 0
        .Col = 0
        .Text = "Loja"
        .Col = 1
        .Text = "C�digo"
        .Col = 2
        .Text = "Data de Digita��o"
        .Col = 3
        .Text = "Validade"
        .Col = 4
        .Text = "Tipo"
        .Col = 5
        .Text = "Cliente"
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss("cod_loja")
            .Col = 1
            .Text = ss("NUM_COTACAO")
            .Col = 2
            .Text = ss("DT_COTACAO")
            .Col = 3
            .Text = ss("DT_VALIDADE")
            .Col = 4
            If ss("FL_TIPO") = 0 Then
                .Text = ""
            Else
                .Text = "    A"
            End If
            .Col = 5
            .Text = ss("NOME_CLIENTE")
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    frmListaCotacao.grdCotacao.Visible = True
    
        
    'mouse
    Screen.MousePointer = vbDefault
        
    Exit Sub


End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmListaCotacao = Nothing
    
    
End Sub


Private Sub grdCotacao_DblClick()
    grdCotacao.Col = 0
    txtResposta = grdCotacao.Text
    Unload Me
End Sub

Private Sub Label1_Click()

End Sub

Private Sub SSCommand1_Click()
End Sub




