VERSION 4.00
Begin VB.Form Form3 
   Caption         =   "GERA��O E ATUALIZA��O DE BASE DE DADOS"
   ClientHeight    =   5670
   ClientLeft      =   675
   ClientTop       =   1515
   ClientWidth     =   8580
   Height          =   6075
   Left            =   615
   LinkTopic       =   "Form3"
   ScaleHeight     =   5670
   ScaleWidth      =   8580
   Top             =   1170
   Width           =   8700
   Begin TabDlg.SSTab SSTABBASE 
      Height          =   3975
      Left            =   120
      TabIndex        =   0
      Top             =   135
      Width           =   8295
      _Version        =   65536
      _ExtentX        =   14631
      _ExtentY        =   7011
      _StockProps     =   15
      Caption         =   "REPRESENTANTE"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabsPerRow      =   3
      Tab             =   2
      TabOrientation  =   0
      Tabs            =   3
      Style           =   0
      TabMaxWidth     =   0
      TabHeight       =   1058
      TabCaption(0)   =   "REGIONAL E FRANQUIA"
      TabPicture(0)   =   "Ger090a.frx":0000
      Tab(0).ControlCount=   5
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Command1"
      Tab(0).Control(1)=   "Command2"
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(3)=   "Frame2"
      Tab(0).Control(4)=   "Label2"
      TabCaption(1)   =   "GERENTE"
      TabPicture(1)   =   "Ger090a.frx":0452
      Tab(1).ControlCount=   6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Command4"
      Tab(1).Control(1)=   "Command3"
      Tab(1).Control(2)=   "Frame4"
      Tab(1).Control(3)=   "Frame3"
      Tab(1).Control(4)=   "Label3"
      Tab(1).Control(5)=   "Label1"
      TabCaption(2)   =   "REPRESENTANTE"
      TabPicture(2)   =   "Ger090a.frx":08A4
      Tab(2).ControlCount=   4
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Command5"
      Tab(2).Control(1)=   "Command6"
      Tab(2).Control(2)=   "Frame5"
      Tab(2).Control(3)=   "Frame6"
      Begin VB.Frame Frame6 
         Height          =   975
         Left            =   1320
         TabIndex        =   28
         Top             =   1200
         Width           =   2655
         Begin VB.CheckBox Check6 
            Caption         =   "Completa"
            Height          =   195
            Left            =   120
            TabIndex        =   30
            Top             =   600
            Width           =   975
         End
         Begin VB.CheckBox Check5 
            Caption         =   "Alterados"
            Height          =   195
            Left            =   120
            TabIndex        =   29
            Top             =   240
            Value           =   1  'Checked
            Width           =   975
         End
      End
      Begin VB.Frame Frame5 
         Height          =   975
         Left            =   4320
         TabIndex        =   25
         Top             =   1200
         Width           =   2775
         Begin VB.OptionButton Option6 
            Caption         =   "Mensal"
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   600
            Width           =   975
         End
         Begin VB.OptionButton Option5 
            Caption         =   "Di�rio"
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Finaliza"
         Height          =   495
         Left            =   4320
         TabIndex        =   24
         Top             =   3240
         Width           =   1815
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Gera Di�rio Gerencial"
         Height          =   495
         Left            =   2295
         TabIndex        =   23
         Top             =   3240
         Width           =   1815
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Gera Di�rio Gerencial"
         Height          =   495
         Left            =   -72960
         TabIndex        =   17
         Top             =   3240
         Width           =   1815
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Finaliza"
         Height          =   495
         Left            =   -70920
         TabIndex        =   16
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Frame Frame4 
         Height          =   975
         Left            =   -70920
         TabIndex        =   13
         Top             =   1200
         Width           =   2775
         Begin VB.OptionButton Option4 
            Caption         =   "Di�rio"
            Height          =   255
            Left            =   120
            TabIndex        =   15
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
         Begin VB.OptionButton Option3 
            Caption         =   "Mensal"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   600
            Width           =   975
         End
      End
      Begin VB.Frame Frame3 
         Height          =   975
         Left            =   -73920
         TabIndex        =   10
         Top             =   1200
         Width           =   2655
         Begin VB.CheckBox Check4 
            Caption         =   "Alterados"
            Height          =   195
            Left            =   120
            TabIndex        =   12
            Top             =   240
            Value           =   1  'Checked
            Width           =   975
         End
         Begin VB.CheckBox Check3 
            Caption         =   "Completa"
            Height          =   195
            Left            =   120
            TabIndex        =   11
            Top             =   600
            Width           =   975
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Gera Di�rio Gerencial"
         Height          =   495
         Left            =   -73080
         TabIndex        =   8
         Top             =   3240
         Width           =   1815
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Finaliza"
         Height          =   495
         Left            =   -71160
         TabIndex        =   7
         Top             =   3240
         Width           =   1815
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Left            =   -70920
         TabIndex        =   4
         Top             =   1200
         Width           =   2775
         Begin VB.OptionButton Option1 
            Caption         =   "Di�rio"
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
         Begin VB.OptionButton Option2 
            Caption         =   "Mensal"
            Height          =   255
            Left            =   120
            TabIndex        =   5
            Top             =   600
            Width           =   975
         End
      End
      Begin VB.Frame Frame2 
         Height          =   975
         Left            =   -74040
         TabIndex        =   1
         Top             =   1200
         Width           =   2775
         Begin VB.CheckBox Check1 
            Caption         =   "Alterados"
            Height          =   195
            Left            =   120
            TabIndex        =   3
            Top             =   240
            Value           =   1  'Checked
            Width           =   975
         End
         Begin VB.CheckBox Check2 
            Caption         =   "Completa"
            Height          =   195
            Left            =   120
            TabIndex        =   2
            Top             =   600
            Width           =   975
         End
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   195
         Left            =   -71040
         TabIndex        =   19
         Top             =   3480
         Width           =   105
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -71040
         TabIndex        =   18
         Top             =   3000
         Width           =   105
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   195
         Left            =   -68520
         TabIndex        =   9
         Top             =   3600
         Width           =   105
      End
   End
   Begin VB.Label Label5 
      Caption         =   "Label5"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   375
      Left            =   3240
      TabIndex        =   22
      Top             =   4680
      Width           =   2655
   End
   Begin VB.Label Label4 
      Caption         =   "Label4"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   2760
      TabIndex        =   21
      Top             =   4200
      Width           =   4455
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   615
      Left            =   7560
      TabIndex        =   20
      Top             =   5040
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1085
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "Ger090a.frx":0CF6
   End
End
Attribute VB_Name = "Form3"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub check1_Click()
  If check1.Value = 0 Then
    Option1.Value = False
    Option1.Enabled = False
    Option2.Value = False
    Option2.Enabled = False
  Else
    Option1.Value = True
    Option1.Enabled = True
    Option2.Value = False
    Option2.Enabled = True
End If

End Sub

Private Sub Check4_Click()
  If Check4.Value = 0 Then
    Option4.Value = False
    Option4.Enabled = False
    Option3.Value = False
    Option3.Enabled = False
  Else
    Option3.Value = True
    Option3.Enabled = True
    Option4.Value = False
    Option4.Enabled = True
End If

End Sub

Private Sub Check5_Click()
  If Check5.Value = 0 Then
    Option5.Value = False
    Option5.Enabled = False
    Option6.Value = False
    Option6.Enabled = False
  Else
    Option5.Value = True
    Option5.Enabled = True
    Option6.Value = False
    Option6.Enabled = True
End If

End Sub

Private Sub Check6_Click()
Form3.Check6.Value = 1
End Sub

Private Sub Command1_Click()
cc
End Sub

Private Sub Command2_Click()
'On Error GoTo TrataErro
  
   dbcontrole.Close
   
   dbextracao.Close
  
    
'TrataErro:
 '   If Err = 91 Then
  '      Resume Next
   ' Else
    '    MsgBox Str(Err) + "-" + Error$
'    End If
    
 '   Resume Next


End Sub

Private Sub Command3_Click()
'On Error GoTo TrataErro
  
   dbcontrole.Close
   dbextracao.Close
 '  End
   
'TrataErro:
 '   If Err = 91 Then
  '      Resume Next
   ' Else
    '    MsgBox Str(Err) + "-" + Error$
'    End If
    
 '   Resume Next

End Sub


Private Sub Command4_Click()
On Error GoTo TrataErro
    
  Screen.MousePointer = 11
   
  tempoini = Time
  
  'Verifica se j� executou o programa
  Call VERIF_EXECUCAO
  If w_termina Then
     Exit Sub
  End If
  
  'Verifica se algum form3.check foi escolhido
  Call VERIF_CHECK
  If w_termina Then
    Exit Sub
  End If
  
  'Verifica a exist�ncia do 0000.MDB, arquivo padr�o da base
  Call VERIF_0000MDB
  
  'Flega o in�cio da execu��o do programa
  w_execucao = True
  
  'Abre o arquivo access de controle
  Call ABRE_CONTROLE
     
  'Busca no CONTROLE a sequencia do arquivo diario do gerente
  Call BUSCA_NUMGERE
  
  'Define os arquivos de trabalho atrav�z do w_num_gerente
  Call DEFINE_NOME(1)
  
  'Remove os arquivos espec�ficos de cada gerente e o
  'arquivo geral (YYYYXXXX.MDB) do diretorio de trabalho, se existirem
  Call REMOVE_MDB
  
  'Abre o(s) arquivo(s) access para leitura da base
  Call ABRE_OUTROS
  
  'Cria o .MDB geral para todos os gerentes no diretorio de trabalho
  Form3.label5.Caption = "BASE GEN�RICA"
  Form3.Refresh
  Call CRIA_TEMP
  
  Call APLICACAO
  Call BANCO
  Call CATEG_SINAL
  Call CIDADE
  Call CLIE_MENSAGEM
  Call DATAS
  Call DOLAR_DIARIO
  Call ESTATISTICA_ITEM
  Call FILIAL
  Call FORNECEDOR
  Call GRUPO
  Call ITEM_ANALITICO
  Call ITEM_CADASTRO
  Call ITEM_CUSTO
  Call ITEM_ESTOQUE
  Call ITEM_GLOBAL
  Call ITEM_PRECO
  Call MONTADORA
  Call PLANO_PGTO
  Call SUBGRUPO
  Call SUBST_TRIBUTARIA
  Call TAXA
  Call TABELA_DESCPER
  Call TABELA_VENDA
  Call TIPO_CLIENTE
  Call TRANSPORTADORA
  Call UF
  Call UF_DEPOSITO
  Call UF_ORIGEM_DESTINO
  Call UF_DPK
  Call SALDO_PEDIDOS
       
       
    'NOVAS TABELAS PARA FILIAL, REGIONAL E REPRESENTANTE
    ' GERENTE N�O UTILIZA ESSAS TABELAS
'  Call DEPOSITO
 ' Call DEPOSITO_VISAO
  'Call FRETE_UF
'  Call FRETE_UF_BLOQ
 ' Call LOJA
'  Call NATUREZA_OPERACAO
 ' Call TIPO_CLIENTE_BLAU
  'Call CANCEL_PEDNOTA
'  Call R_REPVEN
       
  'Tabelas com registros para dele��o
  Call DELETA_APLICACAO
  Call DELETA_TABELA_DESCPER
    
  'Copia o arquivo gen�rico para todos os gerentes cadastrados no controle_bases.gerente
  Call COPIA_ESPECIFICO
  
  'Remove o(s) arquivo(s) gen�rico(s) diario e completo
  Call REMOVE_TEMP
    
  'Gera a base espec�fica atrav�s de selects ao banco EXTRXXXX.MDB
  If Form3.Check4.Value = 1 Then
    arqmdb = Dir(dir_diaria & "????" & w_num_gerente & "*.MDB")
  ElseIf Form3.Check3.Value = 1 Then
    arqmdb = Dir(dir_completa & "*.MDB")
  End If
  Do While arqmdb <> ""
    Form3.label5.Caption = "BASE GERENTE = " & Mid(arqmdb, 1, 4)
    Form3.Refresh
    Call DEFINE_NOME(2)
    Call BUSCA_CICGER
    
    Call REPRESENTANTE
    Call REPR_END_CORRESP
    Call REPRESENTACAO
    Call R_REPCGC
    Call R_REPVEN
    Call CLIENTE
    Call CLIE_CREDITO
    Call CLIE_ENDERECO
    Call DUPLICATAS
    Call DELETA_REPRESENTANTE
    Call DELETA_CLIENTE
    
    arqmdb = Dir
  Loop
        
  'Zipa as bases espec�ficas di�rias dos gerentes
   Form3.label5.Caption = "ZIPA BASES DI�RIAS"
  Form3.Refresh
  Call ZIPA_BASES
  
  'Gera arquivo para e_mail
   Form3.label5.Caption = "GERA E_MAIL"
  Form3.Refresh
  Call GERA_E_MAIL
  
  'Atualiza banco de controle CONTROLE.MDB
   Form3.label5.Caption = "BASE DE CONTROLE"
  Form3.Refresh
  Call NUM_GERENTE
  
  tempofim = Time
  Screen.MousePointer = 0
       
  MsgBox "Inicio: " & tempoini & "   Fim: " & tempofim, 0, "TEMPO EXECU��O"
   Form3.label5.Caption = "EXECU��O OK !!!, FINALIZAR"
   Form3.Label4.Caption = ""
            
  Exit Sub
   
TrataErro:
    If Err = 3022 Then
        Resume Next
    Else
        MsgBox Str(Err) + "-" + Error$
    End If
    
    Resume Next
    
    

End Sub



Private Sub Command5_Click()
On Error GoTo TrataErro
    
  Screen.MousePointer = 11
   
  tempoini = Time
  
  'Verifica se j� executou o programa
  Call VERIF_EXECUCAO
  If w_termina Then
     Exit Sub
  End If
  
  'Verifica se algum form3.check foi escolhido
  Call VERIF_CHECK
  If w_termina Then
    Exit Sub
  End If
  
  'Verifica a exist�ncia do 0000.MDB, arquivo padr�o da base
  Call VERIF_0000MDB
  
  'Flega o in�cio da execu��o do programa
  w_execucao = True
  
  'Abre o arquivo access de controle
  Call ABRE_CONTROLE
     
  'Busca no CONTROLE a sequencia do arquivo diario do gerente
  Call BUSCA_NUMREPR
  
  'Define os arquivos de trabalho atrav�z do w_num_gerente
  Call DEFINE_NOME_REPRESENTANTE(1)
  
  'Remove os arquivos espec�ficos de cada gerente e o
  'arquivo geral (YYYYXXXX.MDB) do diretorio de trabalho, se existirem
  Call REMOVE_MDB
  
  'Abre o(s) arquivo(s) access para leitura da base
  Call ABRE_OUTROS
  
  'Cria o .MDB geral para todos os gerentes no diretorio de trabalho
  Form3.label5.Caption = "BASE GEN�RICA"
  Form3.Refresh
  Call CRIA_TEMP_REPRESENTANTE
  
  Call APLICACAO
  Call BANCO
  Call CATEG_SINAL
  Call CIDADE
  Call CLIE_MENSAGEM
  Call DATAS_REPRES
  Call DOLAR_DIARIO
  Call ESTATISTICA_ITEM
  Call FILIAL
  Call FORNECEDOR
  Call GRUPO
  Call ITEM_ANALITICO
  Call ITEM_CADASTRO
  Call ITEM_CUSTO
  Call ITEM_ESTOQUE
  Call ITEM_GLOBAL
  Call ITEM_PRECO
  Call MONTADORA
  Call PLANO_PGTO
  Call SUBGRUPO
  Call SUBST_TRIBUTARIA
  Call TAXA
  Call TABELA_DESCPER
  Call TABELA_VENDA
  Call TIPO_CLIENTE
  Call TRANSPORTADORA
  Call UF
  Call UF_DEPOSITO
  Call UF_ORIGEM_DESTINO
  Call UF_DPK
       
       
    'NOVAS TABELAS PARA FILIAL, REGIONAL E REPRESENTANTE
'  Call DEPOSITO
  Call DEPOSITO_VISAO
  Call FRETE_UF
  Call FRETE_UF_BLOQ
  Call LOJA
  Call NATUREZA_OPERACAO
  Call TIPO_CLIENTE_BLAU
  Call CANCEL_PEDNOTA
  Call R_REPVEN
  'Call COTACAO
  'Call ITEM_COTACAO
  Call SALDO_PEDIDOS
  Call FRETE_ENTREGA
  
       
  'Tabelas com registros para dele��o
  Call DELETA_APLICACAO
  Call DELETA_TABELA_DESCPER
    
  'Copia o arquivo gen�rico para todos os gerentes cadastrados no controle_bases.gerente
  Call COPIA_ESPECIFICO_REPRESENTANTE
  
  'Remove o(s) arquivo(s) gen�rico(s) diario e completo
  Call REMOVE_TEMP
    
  'Gera a base espec�fica atrav�s de selects ao banco EXTRXXXX.MDB
  If Form3.Check5.Value = 1 Then
    arqmdb = Dir(dir_diaria & "????" & w_num_representante & "*.MDB")
  ElseIf Form3.Check6.Value = 1 Then
    arqmdb = Dir(dir_completa & "*.MDB")
  End If
  Do While arqmdb <> ""
    Form3.label5.Caption = "BASE REPRESENTANTE = " & Mid(arqmdb, 1, 4)
    Form3.Refresh
    Call DEFINE_NOME_REPRESENTANTE(2)
    Call BUSCA_CODREP
    
    Call REPRESENTANTE_REPR
    Call REPR_END_CORRESP_REPRESENTANTE
    Call REPRESENTACAO_REPRESENTANTE
    Call R_REPCGC_REPRESENTANTE
    Call R_REPVEN_REPRESENTANTE
    Call CLIENTE_REPRESENTANTE
    Call CLIE_CREDITO_REPRESENTANTE
    Call CLIE_ENDERECO_REPRESENTANTE
    Call DUPLICATAS_REPRESENTANTE
    Call DELETA_REPRESENTANTE
    Call DELETA_CLIENTE
    
    arqmdb = Dir
  Loop
        
  'Zipa as bases espec�ficas di�rias dos gerentes
   Form3.label5.Caption = "ZIPA BASES DI�RIAS"
  Form3.Refresh
  Call ZIPA_BASES
  
  'Gera arquivo para e_mail
   Form3.label5.Caption = "GERA E_MAIL"
  Form3.Refresh
  Call GERA_E_MAIL
  
  'Atualiza banco de controle CONTROLE.MDB
   Form3.label5.Caption = "BASE DE CONTROLE"
  Form3.Refresh
  Call NUM_REPRESENTANTE
  
  tempofim = Time
  Screen.MousePointer = 0
       
  MsgBox "Inicio: " & tempoini & "   Fim: " & tempofim, 0, "TEMPO EXECU��O"
   Form3.label5.Caption = "EXECU��O OK !!!, FINALIZAR"
   Form3.Label4.Caption = ""
            
  Exit Sub
   
TrataErro:
    If Err = 3022 Then
        Resume Next
    Else
        MsgBox Str(Err) + "-" + Error$
    End If
    
    Resume Next
    
End Sub


Private Sub Command6_Click()
'On Error GoTo TrataErro
  
   dbcontrole.Close
   dbextracao.Close
   End
   
'TrataErro:
 '   If Err = 91 Then
  '      Resume Next
   ' Else
    '    MsgBox Str(Err) + "-" + Error$
'    End If
    
 '   Resume Next


End Sub

Private Sub Form_Load()
    Form3.Label4.Caption = ""
    Form3.label5.Caption = ""
    Form3.check1.Value = 0
    Form3.check2.Value = 0
    Form3.Check3.Value = 0
    Form3.Check4.Value = 0
    Form3.Check5.Value = 0
    Form3.Check6.Value = 0
    Form3.Option1.Value = 0
    Form3.Option2.Value = 0
    Form3.Option3.Value = 0
    Form3.Option4.Value = 0
    Form3.Option5.Value = 0
    Form3.Option6.Value = 0
    SSTABBASE.TabEnabled(1) = False
    SSTABBASE.TabEnabled(2) = False
End Sub

Private Sub SSCommand1_Click()
    End
End Sub





Private Sub SSTABBASE_Click(PreviousTab As Integer)
If SSTABBASE.Tab = 0 Then
    TIPO_BASE = "F"
   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base = "C:\DADOS\FILIAL\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria = "C:\DADOS\FILIAL\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa = "C:\DADOS\FILIAL\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\FILIAL\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
   
ElseIf SSTABBASE.Tab = 1 Then
    TIPO_BASE = "G"

   'Define diretorio base do arquivo fixo 0000.MDB
   'dir_base = "C:\DADOS\GERENTE\0000.MDB"
   dir_base = "C:\DADOS\GERENTE\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   'dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   'dir_diaria = "C:\DADOS\GERENTE\DIARIA\"
   dir_diaria = "C:\DADOS\GERENTE\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   'dir_completa = "C:\DADOS\GERENTE\COMPLETA\"
   dir_completa = "C:\DADOS\GERENTE\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   'dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail = "C:\COM\SAIDA\MSG\"
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\"
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\GERENTE\"

   'Define diretorio do .BAT de pkzip dos arquivos
   'zipa = "C:\DADOS\PTOBAT\ZIPA"
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
   
ElseIf SSTABBASE.Tab = 2 Then

    TIPO_BASE = "R"

   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base = "C:\DADOS\REPRES\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria = "C:\DADOS\REPRES\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa = "C:\DADOS\REPRES\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\REPRES\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
End If



End Sub



Private Sub Sstabbase_GotFocus()
If SSTABBASE.Tab = 0 Then '******* REGIONAL / FRAMNQUIA *******
   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base = "C:\DADOS\FILIAL\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria = "C:\DADOS\FILIAL\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa = "C:\DADOS\FILIAL\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\FILIAL\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False
   
ElseIf SSTABBASE.Tab = 1 Then ' ******* GERENTE *******
   'Define diretorio base do arquivo fixo 0000.MDB
   'dir_base = "C:\DADOS\GERENTE\0000.MDB"
   dir_base = "C:\DADOS\GERENTE\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   'dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   'dir_diaria = "C:\DADOS\GERENTE\DIARIA\"
   dir_diaria = "C:\DADOS\GERENTE\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   'dir_completa = "C:\DADOS\GERENTE\COMPLETA\"
   dir_completa = "C:\DADOS\GERENTE\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   'dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail = "C:\COM\SAIDA\MSG\"
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\"
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\GERENTE\"

   'Define diretorio do .BAT de pkzip dos arquivos
   'zipa = "C:\DADOS\PTOBAT\ZIPA"
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False

ElseIf SSTABBASE.Tab = 2 Then '********** REPRESENTANTE ****************

   'Define diretorio base do arquivo fixo 0000.MDB
   dir_base = "C:\DADOS\REPRES\0000.MDB"
      
   'Define diretorio do arquivo variavel EXTRXXXX.MDB
   ' onde os "X" representam o num. do arquivo a ser gerado para o gerente
   dir_extracao = "C:\DADOS\EXTRACAO\COMPLETA\"
   
   'Define diretorio do arquivo variavel YYYYXXXX.MDB para base diaria
   ' onde os "Y" representam o c�digo do gerente
   dir_diaria = "C:\DADOS\REPRES\DIARIA\"
   
   'Define diretorio do arquivo variavel YYYY0000.MDB para base completa
   dir_completa = "C:\DADOS\REPRES\COMPLETA\"

   'Define diretorio do arquivo fixo de controle
   dir_controle = "C:\DADOS\CONTROLE\CONTROLE.MDB"

   'Define diretorio dos arquivos e_mail
   'dir_e_mail = "C:\COM\SAIDA\MSG\"
   dir_e_mail = "C:\COM\SAIDA\MSG\"

   'Define diretorio dos arquivos e_mail
   dir_e_mail_arq = "C:\COM\SAIDA\ARQUIVOS\REPRES\"

   'Define diretorio do .BAT de pkzip dos arquivos
   zipa = "C:\DADOS\PTOBAT\ZIPA.BAT "

   'Define valores iniciais das flags de controle de execu��o
   w_termina = False
   w_execucao = False

End If

End Sub


