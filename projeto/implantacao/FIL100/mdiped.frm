VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "FIL100 - Consulta Pedido/Nota Fiscal"
   ClientHeight    =   4230
   ClientLeft      =   45
   ClientTop       =   2145
   ClientWidth     =   9450
   Icon            =   "mdiped.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.PictureBox Picture1 
      Align           =   1  'Align Top
      Height          =   675
      Left            =   0
      ScaleHeight     =   615
      ScaleWidth      =   9390
      TabIndex        =   0
      Top             =   0
      Width           =   9450
      Begin Bot�o.cmd SSCommand1 
         Height          =   555
         Left            =   60
         TabIndex        =   1
         ToolTipText     =   "Dados do Pedido"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":030A
         PICN            =   "mdiped.frx":0326
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand2 
         Height          =   555
         Left            =   720
         TabIndex        =   2
         ToolTipText     =   "Consulta Itens"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":0C00
         PICN            =   "mdiped.frx":0C1C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand3 
         Height          =   555
         Left            =   1380
         TabIndex        =   3
         ToolTipText     =   "Dados de Transporte"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":14F6
         PICN            =   "mdiped.frx":1512
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand5 
         Height          =   555
         Left            =   2040
         TabIndex        =   4
         ToolTipText     =   "Limpar a Tela"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":1DEC
         PICN            =   "mdiped.frx":1E08
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand4 
         Height          =   555
         Left            =   3360
         TabIndex        =   5
         ToolTipText     =   "Sair"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":23A2
         PICN            =   "mdiped.frx":23BE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd sscmdVendedor 
         Height          =   555
         Left            =   4500
         TabIndex        =   6
         ToolTipText     =   "Pedido por Vendedor"
         Top             =   30
         Visible         =   0   'False
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":3098
         PICN            =   "mdiped.frx":30B4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd SSCommand6 
         Height          =   555
         Left            =   5160
         TabIndex        =   7
         ToolTipText     =   "Pedido por Filial"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":398E
         PICN            =   "mdiped.frx":39AA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   555
         Left            =   2700
         TabIndex        =   8
         ToolTipText     =   "Sobre"
         Top             =   30
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   979
         BTYPE           =   3
         TX              =   ""
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiped.frx":4284
         PICN            =   "mdiped.frx":42A0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblMensagem 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   6030
         TabIndex        =   9
         Top             =   150
         Width           =   700
      End
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)

Private Sub cmdSobre_Click()
    frmSobre.Show 1
End Sub

Private Sub MDIForm_Load()
          Dim i As Integer
          Dim ss As Snapshot
          Dim strPath As String
          
1         On Error GoTo Trata_Erro
        
2         If App.PrevInstance Then
3             MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
4             End
5         End If
          
6         Abre_Banco = 0
        
         frmLogo.Show 1
        
         '************************
         'Define drive de execu��o
         '************************
7         Path_drv = Left(App.Path, 2)
8         Path_drv = "c:"
9         strPath = Path_drv & "\DADOS\"
          
'10        If GetKeyVal(Path_drv & "\CARGA\PAAC.INI", "VERSAO", "FIL100") <> App.Major & "." & Format(App.Minor, "00") & "." & Format(App.Revision, "000") Then
'11            MsgBox "Sua vers�o do FIL100 est� desatualizada!" & vbCrLf & "Por favor entre em contato com o Helpdesk.", vbOKOnly, "Aten��o"
'12            End
'13        End If
          
14       Me.Caption = Me.Caption & " Vers�o:" & App.Major & "." & App.Minor & "." & App.Revision
          
         'Conexao Access
15        Set dbAccess = OpenDatabase(strPath & "BASE_DPK.MDB")
16        Abre_Banco = 1
         
17        If Command$ = "V" Then
18            MDIForm1.sscmdVendedor.Visible = True
19            SCod_Vend = Environ("USER")
           
20            SQL = "select COD_REPRES"
21            SQL = SQL & " from deposito "
              
22            Set ss = dbAccess.CreateSnapshot(SQL)
              
23            SCod_Vend = ss!COD_REPRES
           
24        Else
25            MDIForm1.sscmdVendedor.Visible = False
26        End If
        
27        If Command$ = "F" Then
28            MDIForm1.SSCommand6.Visible = True
29        Else
30            MDIForm1.SSCommand6.Visible = False
31        End If
         
           
         'carregar data de faturamento,uf origem, filial origem
32              SQL = "select a.cod_loja, b.nome_fantasia,"
33        SQL = SQL & " a.cod_filial      "
34        SQL = SQL & " from deposito a, loja b "
35        SQL = SQL & " where a.cod_loja=b.cod_loja"
          
36        Set ss = dbAccess.CreateSnapshot(SQL)
          'Eduardo
          'tela = ""
           
37        If ss.EOF And ss.BOF Then
38            MsgBox "Problemas na consulta de deposito default. Ligue para Depto Sistemas", vbInformation, "Aten��o"
39            Exit Sub
40        Else
41            deposito_default = Format(ss!cod_loja, "00") & "-" & ss!nome_fantasia
42            FILIAL = ss!cod_filial
43        End If
44        ss.Close
          
45              SQL = " select a.dt_faturamento as dt_faturamento,"
46        SQL = SQL & " b.cod_loja, c.nome_fantasia "
47        SQL = SQL & " from datas a, deposito_visao b, loja c "
48        SQL = SQL & " where b.cod_loja=c.cod_loja  and"
49        SQL = SQL & " b.nome_programa='FIL100'"
50        SQL = SQL & " order by b.cod_loja"
        
51        Set oradynaset = dbAccess.CreateSnapshot(SQL)
          
52        hoje = oradynaset.Fields("dt_faturamento").Value
       
53        frmPed.cboDeposito.Text = Trim(deposito_default)
54        oradynaset.MoveLast
55        oradynaset.MoveFirst
56        For i = 1 To oradynaset.RecordCount
57            frmPed.cboDeposito.AddItem Format(oradynaset!cod_loja, "00") & "-" & oradynaset!nome_fantasia
58            oradynaset.MoveNext
59        Next
       
60        If Mid(Command$, 1, 6) <> "" And Mid(Command$, 1, 6) <> "V" And Mid(Command$, 1, 6) <> "F" Then
61            frmPed.txtNrpedido.Text = Mid(Command$, 1, 7)
62            frmPed.txtNrpedido.Enabled = False
63            frmPed.txtSequencia.Text = Mid(Command$, 8, 8)
64            frmPed.txtSequencia.Enabled = False
65            frmPed.txtNrnotafiscal.Enabled = False
66            frmPed.cboDeposito.Text = Mid(Command$, 9, 13)
67            frmPed.cboDeposito.Enabled = False
68            frmPed.Show
69            Exit Sub
70        End If
         
71        frmPed.Show
       
72    Exit Sub
        
Trata_Erro:
73    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
74       Resume
75    Else
76       MsgBox "Sub MdiForm_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
77    End If

End Sub

Private Sub MDIForm_Resize()
    Dim vLargura As Integer
    Dim i As Integer
    'Ajustar a largura do label na tela
    lblMensagem.Width = 700
    vLargura = lblMensagem.Left + lblMensagem.Width + 500
    For i = vLargura To MDIForm1.Width
        lblMensagem.Width = lblMensagem.Width + 1
    Next
End Sub

Private Sub MDIForm_Terminate()
    End
End Sub

Private Sub sscmdvendedor_Click()
   frmPed.Grid1.Visible = False
   frmPed.Grid2.Visible = False
   frmPed.Grid1.Refresh
   frmPed.Grid2.Refresh
   Call LIMPA_PED
   frmPed.cboDeposito.Text = ""
   Unload frmDados
   Unload frmItem
   Unload frmTransp
   Unload frmConf
   
  frmVend.lblVend.Caption = SCod_Vend
  frmVend.txtdata.Text = hoje
  frmVend.Show vbModal
  
End Sub


Private Sub SSCommand2_Click()
       
    On Error GoTo Trata_Erro
       
1      Screen.MousePointer = 11
2      If frmPed.cboDeposito.Text = "" And _
           frmPed.txtCodigo.Text = "" And frmPed.txtNome.Text = "" And _
           frmPed.txtCgc.Text = "" Then
3          MsgBox "Selecione um dep�sito para efetuar a consulta", vbInformation, "Aten��o"
4         Screen.MousePointer = 0
5         Exit Sub
6       End If
       
7      If frmPed.txtNrpedido.Text = "" Then
8         MsgBox "Entre com um pedido", 0, "ATEN��O"
9         Screen.MousePointer = 0
10        Exit Sub
11      Else
12        botao = 4
13        frmItem.Move 50, 1500, 9400, 5040
        
14        Call INFO_ITEM
          
15      End If
16      Screen.MousePointer = 0
        
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand2_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
        
End Sub

Private Sub SSCommand3_Click()

    On Error GoTo Trata_Erro

1     Screen.MousePointer = 11
2       If frmPed.cboDeposito.Text = "" And _
           frmPed.txtCodigo.Text = "" And frmPed.txtNome.Text = "" And _
           frmPed.txtCgc.Text = "" Then
3         MsgBox "Selecione um dep�sito para efetuar a consulta", vbInformation, "Aten��o"
4         Screen.MousePointer = 0
5         Exit Sub
6       End If

7       If frmPed.txtNrpedido.Text = "" Then
8         MsgBox "Entre com um pedido", 0, "ATEN��O"
9         Screen.MousePointer = 0
10        Exit Sub
11      Else
12        botao = 3
          'frmTransp.Move 100, 1500 ', 9400, 5040
        
13        Call INFO_TRANSP
        
14        If oradynaset.EOF Then
15          Screen.MousePointer = 0
16          Exit Sub
17        Else
18          frmTransp.Show
19        End If
        
20      End If
21      Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand3_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub SSCommand5_Click()
 
    If Mid(Command$, 1, 6) <> "" And Mid(Command$, 1, 6) <> "V" And Mid(Command$, 1, 6) <> "F" Then
      Exit Sub
    Else
      Call LIMPA_PED
      Unload frmDados
      Unload frmTransp
      Unload frmConf
    End If
 
End Sub


Private Sub SSCommand1_Click()
  
    Screen.MousePointer = 11
    
    If frmPed.cboDeposito.Text = "" And _
        frmPed.txtCodigo.Text = "" And frmPed.txtNome.Text = "" And _
        frmPed.txtCgc.Text = "" Then
        MsgBox "Selecione um dep�sito para efetuar a consulta", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
   
    Call INFORMACOES
    
    Screen.MousePointer = 0
  
End Sub

Private Sub SSCommand4_Click()

 Set frmPed = Nothing
 Set frmDados = Nothing
 Set frmTransp = Nothing
 Set frmConf = Nothing
 Set frmItem = Nothing
 Set frmSobre = Nothing
 Set frmVend = Nothing
 End
 
End Sub

Private Sub SSCommand6_Click()
  
    frmPed.Grid1.Visible = False
    frmPed.Grid2.Visible = False
    frmPed.Grid1.Refresh
    frmPed.Grid2.Refresh
    Call LIMPA_PED
    frmPed.cboDeposito.Text = ""
    Unload frmDados
    Unload frmItem
    Unload frmTransp
    Unload frmConf
   
    frmFilial.txtdata.Text = hoje
    
    frmFilial.Show vbModal
End Sub


Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "DPK": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

