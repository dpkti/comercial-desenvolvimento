VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmDcper 
   Caption         =   "Descontos de Per�odo"
   ClientHeight    =   4575
   ClientLeft      =   2370
   ClientTop       =   1650
   ClientWidth     =   3825
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4575
   ScaleWidth      =   3825
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame SSFrmeSuperP 
      Caption         =   "Pre�o Super Promo��o"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   270
      TabIndex        =   29
      Top             =   2820
      Width           =   3285
      Begin Bot�o.cmd lbl_TipoC_SP 
         Height          =   285
         Left            =   375
         TabIndex        =   40
         Top             =   870
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "C"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":0000
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoA_SP 
         Height          =   285
         Left            =   375
         TabIndex        =   41
         Top             =   270
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "A"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoB_SP 
         Height          =   285
         Left            =   375
         TabIndex        =   42
         Top             =   570
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "B"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":0038
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   8
         Left            =   1725
         TabIndex        =   39
         Top             =   930
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   7
         Left            =   1725
         TabIndex        =   38
         Top             =   630
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   6
         Left            =   1725
         TabIndex        =   37
         Top             =   330
         Width           =   180
      End
      Begin VB.Label lblPRECO1_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3015
         TabIndex        =   36
         Top             =   900
         Width           =   120
      End
      Begin VB.Label lblPRECO2_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3015
         TabIndex        =   35
         Top             =   600
         Width           =   120
      End
      Begin VB.Label lblPRECO3_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3015
         TabIndex        =   34
         Top             =   300
         Width           =   120
      End
      Begin VB.Label lblPC_DESC_PERIODO1_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "11,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   33
         Top             =   930
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO2_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "12,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   32
         Top             =   630
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO3_SP 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "13,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   31
         Top             =   330
         Width           =   480
      End
      Begin VB.Label Seta3 
         AutoSize        =   -1  'True
         Caption         =   "=>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   60
         TabIndex        =   30
         Top             =   855
         Width           =   330
      End
   End
   Begin VB.Frame SSFrmeOferta 
      Caption         =   "Pre�o Normal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   270
      TabIndex        =   15
      Top             =   1470
      Width           =   3285
      Begin Bot�o.cmd lbl_TipoC_Oferta 
         Height          =   285
         Left            =   405
         TabIndex        =   26
         Top             =   840
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "C"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":0054
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoA_Oferta 
         Height          =   285
         Left            =   405
         TabIndex        =   27
         Top             =   240
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "A"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":0070
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoB_Oferta 
         Height          =   285
         Left            =   405
         TabIndex        =   28
         Top             =   540
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "B"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":008C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   5
         Left            =   1725
         TabIndex        =   25
         Top             =   900
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   4
         Left            =   1725
         TabIndex        =   24
         Top             =   600
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   3
         Left            =   1725
         TabIndex        =   23
         Top             =   300
         Width           =   180
      End
      Begin VB.Label lblPRECO1_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3000
         TabIndex        =   22
         Top             =   900
         Width           =   120
      End
      Begin VB.Label lblPRECO2_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3000
         TabIndex        =   21
         Top             =   600
         Width           =   120
      End
      Begin VB.Label lblPRECO3_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3000
         TabIndex        =   20
         Top             =   270
         Width           =   120
      End
      Begin VB.Label lblPC_DESC_PERIODO1_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "10,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   19
         Top             =   900
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO2_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "20,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   18
         Top             =   600
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO3_OFERTA 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "30,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1170
         TabIndex        =   17
         Top             =   300
         Width           =   480
      End
      Begin VB.Label Seta2 
         AutoSize        =   -1  'True
         Caption         =   "=>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   90
         TabIndex        =   16
         Top             =   825
         Width           =   330
      End
   End
   Begin VB.Frame SSFrmeNormal 
      Caption         =   "Pre�o Normal"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1245
      Left            =   270
      TabIndex        =   1
      Top             =   120
      Width           =   3285
      Begin Bot�o.cmd lbl_TipoC_Normal 
         Height          =   285
         Left            =   375
         TabIndex        =   12
         Top             =   840
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "C"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":00A8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoA_Normal 
         Height          =   285
         Left            =   375
         TabIndex        =   13
         Top             =   240
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "A"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":00C4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd lbl_TipoB_Normal 
         Height          =   285
         Left            =   375
         TabIndex        =   14
         Top             =   540
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   503
         BTYPE           =   3
         TX              =   "B"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmDcper.frx":00E0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   1
         Left            =   1665
         TabIndex        =   11
         Top             =   600
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   2
         Left            =   1665
         TabIndex        =   10
         Top             =   900
         Width           =   180
      End
      Begin VB.Label lblSinal 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "%"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Index           =   0
         Left            =   1665
         TabIndex        =   9
         Top             =   300
         Width           =   180
      End
      Begin VB.Label lblPRECO3_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3030
         TabIndex        =   8
         Top             =   300
         Width           =   120
      End
      Begin VB.Label lblPRECO2_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3030
         TabIndex        =   7
         Top             =   600
         Width           =   120
      End
      Begin VB.Label lblPRECO1_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "0"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   3030
         TabIndex        =   6
         Top             =   900
         Width           =   120
      End
      Begin VB.Label lblPC_DESC_PERIODO3_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "50,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1080
         TabIndex        =   5
         Top             =   300
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO1_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "10,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1080
         TabIndex        =   4
         Top             =   900
         Width           =   480
      End
      Begin VB.Label lblPC_DESC_PERIODO2_NORMAL 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "20,00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   210
         Left            =   1080
         TabIndex        =   3
         Top             =   600
         Width           =   480
      End
      Begin VB.Label Seta1 
         AutoSize        =   -1  'True
         Caption         =   "=>"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   60
         TabIndex        =   2
         Top             =   825
         Width           =   330
      End
   End
   Begin Bot�o.cmd SSPanelMsg 
      Height          =   315
      Left            =   30
      TabIndex        =   0
      Top             =   4200
      Width           =   3705
      _ExtentX        =   6535
      _ExtentY        =   556
      BTYPE           =   3
      TX              =   "Clique em A, B ou C da tabela desejada."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDcper.frx":00FC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmDcper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Activate()

    On Error GoTo trata_erro

1         If FRMITENS.txtTabela.Text = "" Then
              'Buscar os nomes das tabelas na base
2             SQL1 = "Select tabela_venda,tp_tabela " & _
                     "From tabela_venda " & _
                     "Where ocorr_preco=1"
              
3             Set ssTabela = dbaccess2.CreateSnapshot(SQL1)
4             FreeLocks
              
5             Do While Not ssTabela.EOF
6                 If ssTabela!tp_tabela = 0 Then
7                     SSFrmeNormal.Caption = ssTabela!TABELA_VENDA
8                 End If
9                 If ssTabela!tp_tabela = 1 Then
10                    SSFrmeOferta.Caption = ssTabela!TABELA_VENDA
11                End If
12                If ssTabela!tp_tabela = 2 Then
13                    SSFrmeSuperP.Caption = ssTabela!TABELA_VENDA
14                End If
15                ssTabela.MoveNext
16            Loop
17        Else
18            If frmDcper.Visible = True Then
19                Unload frmDcper
20            End If
21        End If
          
trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Activate - frmDcPer" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
          
End Sub

Private Sub Form_Load()

    On Error GoTo trata_erro
          
1         If FRMITENS.txtTabela.Text = "" Then
              'Buscar os nomes das tabelas na base
2             SQL1 = "Select tabela_venda,tp_tabela " & _
                     "From tabela_venda " & _
                     "Where ocorr_preco=1"
              
3             Set ssTabela = dbaccess2.CreateSnapshot(SQL1)
4             FreeLocks
              
5             Do While Not ssTabela.EOF
6                 If ssTabela!tp_tabela = 0 Then
7                     SSFrmeNormal.Caption = ssTabela!TABELA_VENDA
8                 End If
9                 If ssTabela!tp_tabela = 1 Then
10                    SSFrmeOferta.Caption = ssTabela!TABELA_VENDA
11                End If
12                If ssTabela!tp_tabela = 2 Then
13                    SSFrmeSuperP.Caption = ssTabela!TABELA_VENDA
14                End If
15                ssTabela.MoveNext
16            Loop
17        End If

trata_erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Load - frmDcPer" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
          
          
End Sub


Private Sub lbl_TipoA_Normal_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO3_NORMAL.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 0
    FRMITENS.txtTabela.Text = SSFrmeNormal.Caption
    Unload Me
End Sub

Private Sub lbl_TipoA_Normal_GotFocus()
    Seta1.Top = 264
End Sub

Private Sub lbl_TipoA_Oferta_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO3_OFERTA.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 1
    FRMITENS.txtTabela.Text = SSFrmeOferta.Caption
    Unload Me
End Sub



Private Sub lbl_TipoA_Oferta_GotFocus()
    Seta2.Top = 264
End Sub

Private Sub lbl_TipoA_SP_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO3_SP.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 2
    FRMITENS.txtTabela.Text = SSFrmeSuperP.Caption
    Unload Me
End Sub



Private Sub lbl_TipoA_SP_GotFocus()
    Seta3.Top = 264
End Sub

Private Sub lbl_TipoB_Normal_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO2_NORMAL.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 0
    FRMITENS.txtTabela.Text = SSFrmeNormal.Caption
    Unload Me
End Sub




Private Sub lbl_TipoB_Normal_GotFocus()
    Seta1.Top = 528
End Sub

Private Sub lbl_TipoB_Oferta_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO2_OFERTA.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 1
    FRMITENS.txtTabela.Text = SSFrmeOferta.Caption
    Unload Me
End Sub



Private Sub lbl_TipoB_Oferta_GotFocus()
    Seta2.Top = 528
End Sub

Private Sub lbl_TipoB_SP_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO2_SP.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 2
    FRMITENS.txtTabela.Text = SSFrmeSuperP.Caption
    Unload Me
End Sub



Private Sub lbl_TipoB_SP_GotFocus()
    Seta3.Top = 528
End Sub

Private Sub lbl_TipoC_Normal_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO1_NORMAL.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 0
    FRMITENS.txtTabela.Text = SSFrmeNormal.Caption
    Unload Me
End Sub

Private Sub lbl_TipoC_Normal_GotFocus()
    Seta1.Top = 792
End Sub

Private Sub lbl_TipoC_Oferta_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO1_OFERTA.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 1
    FRMITENS.txtTabela.Text = SSFrmeOferta.Caption
    Unload Me
End Sub

Private Sub lbl_TipoC_Oferta_GotFocus()
    Seta2.Top = 792
End Sub

Private Sub lbl_TipoC_SP_Click()
    FRMITENS.txtPeriodo.Text = frmDcper.lblPC_DESC_PERIODO1_SP.Caption
    'Tipo de tabela Normal=0, Oferta=1, SP=2
    tp_tabela = 2
    FRMITENS.txtTabela.Text = SSFrmeSuperP.Caption
    Unload Me
End Sub

Private Sub lbl_TipoC_SP_GotFocus()
    Seta3.Top = 792
End Sub


