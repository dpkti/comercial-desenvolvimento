VERSION 5.00
Begin VB.Form frmErros 
   BackColor       =   &H00000080&
   Caption         =   "Mensagem de Erro - FIL080"
   ClientHeight    =   3465
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7515
   LinkTopic       =   "Form1"
   ScaleHeight     =   3465
   ScaleWidth      =   7515
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   6600
      Top             =   2370
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00000080&
      Caption         =   "Erro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   3345
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   7335
      Begin VB.Timer Timer_Color 
         Interval        =   3000
         Left            =   5940
         Top             =   2280
      End
      Begin VB.Label lblSub 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   345
         Left            =   2220
         TabIndex        =   8
         Top             =   2790
         Width           =   4995
      End
      Begin VB.Label lblLinha 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   345
         Left            =   2220
         TabIndex        =   7
         Top             =   2310
         Width           =   2145
      End
      Begin VB.Label lblDescricao 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   1095
         Left            =   210
         TabIndex        =   6
         Top             =   1170
         Width           =   6975
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblCodErro 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FFFF&
         Height          =   345
         Left            =   2220
         TabIndex        =   5
         Top             =   300
         Width           =   2145
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Local do Erro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   210
         TabIndex        =   4
         Top             =   2880
         Width           =   1470
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Linha do Erro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   210
         TabIndex        =   3
         Top             =   2370
         Width           =   1455
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descri��o do Erro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   210
         TabIndex        =   2
         Top             =   870
         Width           =   1965
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "C�digo do Erro:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Left            =   210
         TabIndex        =   1
         Top             =   390
         Width           =   1650
      End
   End
End
Attribute VB_Name = "frmErros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
1   Me.lblCodErro = gCod_Erro
2   Me.lblDescricao = gDesc_Erro
3   Me.lblLinha = gLinha_Erro
4   Me.lblSub = gNomeSub_Erro

5   gCod_Erro = ""
6   gDesc_Erro = ""
7   gLinha_Erro = ""
8   gNomeSub_Erro = ""

9   Me.Visible = True
End Sub

Private Sub Timer_Color_Timer()
1   If Me.BackColor = &H80& Then
2       Me.BackColor = vbBlue
3       Frame1.BackColor = vbBlue
4   Else
5       Me.BackColor = &H80&
6       Frame1.BackColor = &H80&
7   End If
End Sub

Private Sub Timer1_Timer()
'EnviarEmail V_BANCO, "EDUARDO.RELVAS@MAXXIPEL.COM.BR", "Erro FIL080", "C�digo Erro:" & lblCodErro & vbCrLf & "Descricao Erro:" & lblDescricao & vbCrLf & "Sub: " & Me.lblSub & vbCrLf & "Linha: " & lblLinha
End Sub

