VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmFimPedido 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIL080 - Processa Consulta de Pedidos - FILIAL"
   ClientHeight    =   1500
   ClientLeft      =   5115
   ClientTop       =   5280
   ClientWidth     =   6555
   Icon            =   "fimpedid.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   NegotiateMenus  =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1500
   ScaleWidth      =   6555
   Begin MSComDlg.CommonDialog CD 
      Left            =   1500
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame FraParametros 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Par�metros"
      Height          =   2925
      Left            =   30
      TabIndex        =   6
      Top             =   1620
      Width           =   6465
      Begin VB.TextBox txtTempo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   60
         MaxLength       =   2
         TabIndex        =   21
         Top             =   390
         Width           =   705
      End
      Begin Bot�o.cmd cmdEntrada 
         Height          =   315
         Left            =   6150
         TabIndex        =   15
         Top             =   900
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "fimpedid.frx":000C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.TextBox txtSaidaPedido 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   60
         TabIndex        =   10
         Top             =   2520
         Width           =   6075
      End
      Begin VB.TextBox txtSaida 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   60
         TabIndex        =   9
         Top             =   1980
         Width           =   6075
      End
      Begin VB.TextBox txtEntradaPedido 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   60
         TabIndex        =   8
         Top             =   1440
         Width           =   6075
      End
      Begin VB.TextBox txtEntrada 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   60
         TabIndex        =   7
         Top             =   900
         Width           =   6075
      End
      Begin Bot�o.cmd cmdEntradaPedido 
         Height          =   315
         Left            =   6150
         TabIndex        =   16
         Top             =   1440
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "fimpedid.frx":0028
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSaida 
         Height          =   315
         Left            =   6150
         TabIndex        =   17
         Top             =   1980
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "fimpedid.frx":0044
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSaidaPedido 
         Height          =   315
         Left            =   6150
         TabIndex        =   18
         Top             =   2520
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "..."
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "fimpedid.frx":0060
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "minutos."
         Height          =   195
         Left            =   780
         TabIndex        =   23
         Top             =   510
         Width           =   585
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "Processar a cada:"
         Height          =   195
         Left            =   60
         TabIndex        =   22
         Top             =   210
         Width           =   1290
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "Sa�da Pedido"
         Height          =   195
         Left            =   60
         TabIndex        =   14
         Top             =   2340
         Width           =   975
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "Sa�da"
         Height          =   195
         Left            =   60
         TabIndex        =   13
         Top             =   1800
         Width           =   435
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "Entrada Pedido"
         Height          =   195
         Left            =   60
         TabIndex        =   12
         Top             =   1260
         Width           =   1095
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         Caption         =   "Entrada"
         Height          =   195
         Left            =   60
         TabIndex        =   11
         Top             =   720
         Width           =   555
      End
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   675
      Left            =   30
      TabIndex        =   4
      ToolTipText     =   "Sair"
      Top             =   0
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "fimpedid.frx":007C
      PICN            =   "fimpedid.frx":0098
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Timer Timer1 
      Interval        =   60000
      Left            =   2040
      Top             =   150
   End
   Begin Bot�o.cmd cmdParametros 
      Height          =   675
      Left            =   750
      TabIndex        =   5
      ToolTipText     =   "Parametros"
      Top             =   0
      Width           =   705
      _ExtentX        =   1244
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "fimpedid.frx":0D72
      PICN            =   "fimpedid.frx":0D8E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGravar 
      Height          =   585
      Left            =   5850
      TabIndex        =   19
      Top             =   4650
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1032
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "fimpedid.frx":1A68
      PICN            =   "fimpedid.frx":1A84
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblCds 
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   285
      Left            =   1500
      TabIndex        =   20
      Top             =   30
      Width           =   4995
   End
   Begin VB.Label lbl_Arquivo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   405
      Left            =   45
      TabIndex        =   3
      Top             =   1080
      Width           =   6465
   End
   Begin VB.Label lblDtFat 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Label3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   5370
      TabIndex        =   2
      Top             =   390
      Width           =   765
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Dt. Faturamento:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   3420
      TabIndex        =   1
      Top             =   390
      Width           =   1890
   End
   Begin VB.Label lblEnvia 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Iniciando Processamento ..."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   45
      TabIndex        =   0
      Top             =   690
      Width           =   6465
   End
End
Attribute VB_Name = "frmFimPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vDep As String
Dim vTempo As Byte

'***************
'ITPEDNOTA_VENDA - ok
'***************
Public Sub ITPEDNOTA_VENDA(pLoja, pPedido, pSeq)

1   On Error GoTo Trata_Erro

    Dim vITPEDNOTA As String
    Dim vTabela As String * 6

2   OraParameters.Remove "PMLOJA"
3   OraParameters.Add "PMLOJA", pLoja, 1
4   OraParameters.Remove "PMPED"
5   OraParameters.Add "PMPED", pPedido, 1
6   OraParameters.Remove "PMSEQ"
7   OraParameters.Add "PMSEQ", pSeq, 1

8   OraParameters.Remove "DEP"
9   vDep = "DEP" & Format(pLoja, "00")
10  OraParameters.Add "DEP", vDep, 1

11  OraParameters.Remove "vErro"
12  OraParameters.Add "vErro", 0, 2

13  Criar_Cursor

14  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_ITPEDNOTA(:VCURSOR,:DEP,:PMLOJA,:PMSEQ,:PMPED,:VERRO);END;"

15  Set VarRec = OraParameters("vCursor").Value

16  V_BANCO.Parameters.Remove "vCursor"

17  If OraParameters("vErro").Value <> 0 Then
18      gFl_Restrito = "S"
19      Set VarRec = Nothing
20      Exit Sub
21  End If

22  If Not VarRec.EOF Then
23      For i = 1 To VarRec.RecordCount
24          vTabela = IIf(IsNull(VarRec("TABELA_VENDA")), 0, VarRec("TABELA_VENDA"))
25          vITPEDNOTA = CStr(Format(VarRec("NUM_ITEM_PEDIDO"), "000")) & "|"
26          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("COD_LOJA_NOTA"), "00")) & "|"
27          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("NUM_NOTA"), "000000")) & "|"
28          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("NUM_ITEM_NOTA"), "000")) & "|"
29          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("COD_DPK"), "00000")) & "|"
30          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("QTD_SOLICITADA"), "00000")) & "|"
31          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("QTD_ATENDIDA"), "00000")) & "|"
32          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PRECO_UNITARIO"), "0000000000.00")) & "|"
33          vITPEDNOTA = vITPEDNOTA & vTabela & "|"
34          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_DESC1"), "00.00")) & "|"
35          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_DESC2"), "00.00")) & "|"
36          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_DESC3"), "00.00")) & "|"
37          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_DIFICM"), "00.00")) & "|"
38          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_IPI"), "00.00")) & "|"
39          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_COMISS"), "00.00")) & "|"
40          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("PC_COMISSTLMK"), "00.00")) & "|"
41          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("COD_TRIB"), "0")) & "|"
42          vITPEDNOTA = vITPEDNOTA & CStr(Format(VarRec("COD_TRIBIPI"), "00.00")) & "|"
43          vITPEDNOTA = vITPEDNOTA & CStr(VarRec("SITUACAO")) & "|"
44          Print #2, "ite|"; vITPEDNOTA
45          VarRec.MoveNext
46      Next

47  End If

48  VarRec.Close
49  Set VarRec = Nothing

50  Exit Sub

Trata_Erro:
51  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
52      gFl_Restrito = "S"
53  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
54      gFl_Restrito = "S"
55  ElseIf Err.Description Like "*12541*" Then
56      gFl_Restrito = "S"
57      Resume Next
58  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
59      VarRec.Close
60      Set VarRec = Nothing
61      Exit Sub
62  ElseIf Err.Number <> 0 Then
63      gCod_Erro = Err.Number
64      gDesc_Erro = Err.Description
65      gNomeSub_Erro = "ITPEDNOTA_VENDA"
66      gLinha_Erro = Erl
67      EnviarEmail "Sub ITPEDNOTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq
68      StayOnTop frmErros
69  End If

End Sub

'*************
'PEDNOTA_VENDA - OK
'*************
Public Sub PEDNOTA_VENDA(pLoja, pPedido, pSeq)

1   On Error GoTo Trata_Erro

    Dim vLoja_Original_Transf
    Dim vPedido_Original_Transf

2   gFl_Transf = "N"
3   gFl_Busca_Pedido_Venda = "N"

    'Identificar a Cotacao
4   If pLoja <> 1 Then               'DEP�SITOS REMOTOS
5       TESTA_CONEXAO pLoja
6       If gFl_Restrito = "S" Then    'N�o tem Comunica��o ou est� Restrito
7           Exit Sub
8       End If
9   End If

10  OraParameters.Remove "PMLJ"
11  OraParameters.Add "PMLJ", pLoja, 1
12  OraParameters.Remove "PMPED"
13  OraParameters.Add "PMPED", pPedido, 1
14  OraParameters.Remove "PMSEQ"
15  OraParameters.Add "PMSEQ", Val(pSeq), 1

16  OraParameters.Remove "DEP"
17  vDep = "DEP" & Format(pLoja, "00")
18  OraParameters.Add "DEP", vDep, 1

19  Criar_Cursor

20  OraParameters.Remove "vErro"
21  OraParameters.Add "vErro", 0, 2

22  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_COTTRAN(:VCURSOR,:DEP,:PMLJ,:PMPED,:VERRO); END;"

23  Set VarRec = OraParameters("vCursor").Value

24  V_BANCO.Parameters.Remove "vCursor"

25  If OraParameters("vErro").Value <> 0 Then
26      gFl_Restrito = "S"
27      Set VarRec = Nothing
28      Exit Sub
29  End If

30  If Not VarRec.EOF Then    'Achou a cota��o, ent�o pega o num do pedido de transfer.

31      'Gerar_Log "Achou a cota��o, ent�o pega o num do pedido de transfer -gNum_Transf:" & VarRec("NUM_PEDIDO") & " -gLoja_Transf:" & VarRec("COD_LOJA_TRANSF") & " -vSeq_transf:" & VarRec("SEQ_PEDIDO") & " -vLoja_Original_Transf:" & VarRec("COD_LOJA_COTACAO") & " -vPedido_Original_Transf:" & VarRec("NUM_COTACAO") & " -vLOJ_COT_TEMP:" & VarRec("COD_LOJA_COTACAO") & " -vNUM_COT_TEMP:" & VarRec("NUM_COTACAO_TEMP") & " -vFl_Transf: S"

32      gNum_transf = VarRec("NUM_PEDIDO")
33      gLoja_transf = VarRec("COD_LOJA_TRANSF")
34      gSeq_transf = VarRec("SEQ_PEDIDO")
35      vLoja_Original_Transf = VarRec("COD_LOJA_COTACAO")
36      vPedido_Original_Transf = VarRec("NUM_COTACAO")
37      gLOJ_COT_TEMP = VarRec("COD_LOJA_COTACAO")
38      gNUM_COT_TEMP = VarRec("NUM_COTACAO_TEMP")
39      gFl_Transf = "S"

40  Else                       'N�o Achou, O Num n�o � Cota��o_Temp, Verificar se � Transf

41      'Gerar_Log "Sub: PEDNOTA_VENDA - PR_BUSCA_TRANSFER - DEP: " & vDep & " - Loja: " & pLoja & " Ped:" & pPedido & " - N�o Achou, O Num n�o � Cota��o_Temp, Verificar se � Transf"

        '41       If gLoja <> 1 Then
        '42          Call TESTA_CONEXAO(gLoja)
        '43          If gFl_Restrito = "S" Then
        '44             Exit sub
        '45          End If
        '46       End If

42      Criar_Cursor

43      V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_TRANSFER(:VCURSOR,:DEP,:PMLJ,:PMPED,:VERRO); END;"

44      Set VarRec = OraParameters("vCursor").Value

45      V_BANCO.Parameters.Remove "vCursor"

46      If OraParameters("vErro").Value <> 0 Then
47          gFl_Restrito = "S"
48          Set VarRec = Nothing
49          Exit Sub
50      End If

51      If Not VarRec.EOF Then   'Achou � Transfer
52          gFl_Transf = "S"
53          gLoja_transf = VarRec("COD_LOJA_TRANSF")
54          gNum_transf = VarRec("NUM_PEDIDO")
55          gSeq_transf = 0
56          vLoja_Original_Transf = VarRec("COD_LOJA_COTACAO")
57          vPedido_Original_Transf = VarRec("NUM_COTACAO")
58          gNUM_COT_TEMP = IIf(IsNull(VarRec("NUM_COTACAO_TEMP")), 0, VarRec("NUM_COTACAO_TEMP"))
59          gLOJ_COT_TEMP = VarRec("COD_LOJA_COTACAO")

60          If (Not IsNull(VarRec("COD_LOJA_VENDA")) Or VarRec("COD_LOJA_VENDA") <> 0) Then
61              gFl_Busca_Pedido_Venda = "S"
62              gLoja_Venda = VarRec("COD_LOJA_venda")
63              gPedido_Venda = VarRec("NUM_PEDIDO_venda")
64              gSeq_Venda = VarRec("SEQ_PEDIDO_venda")
65          End If
66      Else
67          vLoja_Original_Transf = 0
68          vPedido_Original_Transf = 0
69      End If

        '76       If pLoja <> 1 Then
        '77          Call TESTA_CONEXAO(pLoja)
        '78          If gFl_Restrito = "S" Then
        '79             Exit sub
        '80          End If
        '81       End If

70  End If    'NOVO

71  If gFl_Transf = "S" Then
72      OraParameters.Remove "PMLOJA"
73      OraParameters.Add "PMLOJA", gLoja_transf, 1
74      OraParameters.Remove "PMPED"
75      OraParameters.Add "PMPED", gNum_transf, 1
76      OraParameters.Remove "PMSEQ"
77      OraParameters.Add "PMSEQ", gSeq_transf, 1
78      OraParameters.Remove "DEP"
79      vDep = "DEP" & Format(gLoja_transf, "00")
80      OraParameters.Add "DEP", vDep, 1
81  Else
82      OraParameters.Remove "PMLOJA"
83      OraParameters.Add "PMLOJA", Val(gLoja), 1
84      OraParameters.Remove "PMPED"
85      OraParameters.Add "PMPED", Val(gPedido), 1
86      OraParameters.Remove "PMSEQ"
87      OraParameters.Add "PMSEQ", Val(gSeq), 1
88      OraParameters.Remove "DEP"
89      vDep = "DEP" & Format(gLoja, "00")
90      OraParameters.Add "DEP", vDep, 1
91  End If

92  OraParameters.Remove "vErro"
93  OraParameters.Add "vErro", 0, 2

94  Criar_Cursor

95  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_PEDNOTA(:VCURSOR,:DEP,:PMLJ,:PMSEQ,:PMPED,:VERRO);END; "
96  Set VarRec = OraParameters("vCursor").Value
97  V_BANCO.Parameters.Remove "vCursor"

98  If OraParameters("vErro").Value <> 0 Then
99      gFl_Restrito = "S"
100     Set VarRec = Nothing
101     Exit Sub
102 End If

103 If Not VarRec.EOF Then    'Achou no PEDNOTA_VENDA
104     If gFl_Transf = "S" Then    ' � Pedido de Transfer�ncia
105         Call MONTA_RETORNO_PEDNOTA_VENDA(gLoja_transf, gNum_transf, gSeq_transf, gFl_Transf)
106         pLoja = gLoja_transf
107         pPedido = gNum_transf
108         pSeq = gSeq_transf
109         gFl_Busca_Pedido_Venda = "S"
110         Set VarRec = Nothing
111         Exit Sub
112     Else
113         Call MONTA_RETORNO_PEDNOTA_VENDA(pLoja, pPedido, pSeq, gFl_Transf)
114         gFl_Transf = "N"
115         gFl_Busca_Pedido_Venda = "N"
116     End If
117 End If

118 Set VarRec = Nothing

119 Exit Sub

Trata_Erro:
120 If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
121     gFl_Restrito = "S"
122 ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
123     gFl_Restrito = "S"
124 ElseIf Err.Description Like "*12541*" Then
125     gFl_Restrito = "S"
126 ElseIf Err.Description Like "*ORA-00604*" Or Err.Description = "ORA-01003" Then
127     Set VarRec = Nothing
128     Exit Sub
129 ElseIf Err.Number <> 0 Then
130     gCod_Erro = Err.Number
131     gDesc_Erro = Err.Description
132     gNomeSub_Erro = "PEDNOTA_VENDA"
133     gLinha_Erro = Erl
134     EnviarEmail "Sub PEDNOTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq & vbCrLf & "Transf: " & gFl_Transf & vbCrLf & "Arquivo:" & lbl_Arquivo
135     StayOnTop frmErros
136 End If

End Sub

'*************
'TESTA_CONEXAO - OK
'*************
Public Function TESTA_CONEXAO(pLoja) As String

1   On Error GoTo Trata_Erro

2   OraParameters.Remove "PMLOJA"
3   OraParameters.Add "PMLOJA", pLoja, 1

4   Criar_Cursor

5   V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_LOJA_CONEXAO(:VCURSOR,:PMLOJA); END;"
6   Set VarRec = OraParameters("vCursor").Value
7   V_BANCO.Parameters.Remove "vCursor"

8   gFl_Restrito = "N"

9   If Not VarRec.EOF Then
10      SUCCESS = Ping(VarRec("IP"), VarRec("COD_LOJA"), ECHO)
11      If InStr(GetStatusCode(SUCCESS), ("ip success")) = 0 Or ECHO.RoundTripTime > VarRec("TIME_OUT") Then
12          Screen.MousePointer = 0
13          gFl_Restrito = "S"
14          'Gerar_Log "Testa_Conexao: " & gFl_Restrito
15          Exit Function
16      Else
17          If gFl_Restrito = "S" Then
18              Exit Function
19          End If
20      End If
21  Else
22      gFl_Restrito = "N"
        '       'Gerar_Log "Testa_Conexao: " & gFl_Restrito
23  End If

Trata_Erro:
24  If Err.Number <> 0 Then
25      gCod_Erro = Err.Number
26      gDesc_Erro = Err.Description
27      gNomeSub_Erro = "TESTA_CONEXAO"
28      gLinha_Erro = Erl
29      EnviarEmail "Sub TESTA_CONEXAO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
30      StayOnTop frmErros
31  End If

End Function

'*************************
'BUSCA_PEDIDO_VENDA_TRANSF - OK
'*************************
Public Sub BUSCA_PEDIDO_VENDA_TRANSF(Loja_V, Pedido_V, Seq_V)
1   On Error GoTo Trata_Erro

    Dim vFl_Transf As String

2   'Gerar_Log "Entrou na BUSCA_PEDIDO_VENDA_TRANSF - Loja_V:" & Loja_V & " -Pedido_V:" & Pedido_V & " -Seq_V:" & Seq_V

    '***********************************************************
    'BUSCA NUM PEDIDO VENDA PARA ENVIAR O RETORNO DELE JUNTO COM
    'O RETORNO DO PEDIDO DE TRANSFERENCIA
    '***********************************************************
3   If gLoja_transf > 0 Then

4       'Gerar_Log "gLoja_Transf >0 - gLoja_Transf: " & gLoja_transf & " -gNum_transf:" & gNum_transf

5       OraParameters.Remove "PMLOJA"
6       OraParameters.Add "PMLOJA", gLoja_transf, 1
7       OraParameters.Remove "PMPED"
8       OraParameters.Add "PMPED", gNum_transf, 1
9       OraParameters.Remove "DEP"
10      vDep = "DEP" & Format(gLoja_transf, "00")
11      OraParameters.Add "DEP", vDep, 1

12      OraParameters.Remove "vErro"
13      OraParameters.Add "vErro", 0, 2

14      Criar_Cursor

15      V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_PEDIDO_TRANSF(:VCURSOR,:DEP,:PMLOJA,:PMPED,:VERRO); END;"
16      Set VarRec = OraParameters("vCursor").Value
17      V_BANCO.Parameters.Remove "vCursor"

        'Eduardo
        'Aqui estava com NOT
18      If VarRec.EOF Or IsNull(VarRec!cod_loja_venda) Then
19          'Gerar_Log "Executou a PR_BUSCA_PEDIDO_TRAND e nao achou : - gLoja_Transf: " & gLoja_transf & " -gNum_transf:" & gNum_transf
20          gLoja_Venda = 0
21          gPedido_Venda = 0
22          gSeq_Venda = 0
23          gFl_Busca_Pedido_Venda = "N"
24          Exit Sub
25      Else      'Achou
26          If gLoja_transf = 1 Then
27              'Gerar_Log "Achou - Loja Transf = 1 -Varrec.Cod_loja_Venda = " & VarRec("COD_LOJA_VENDA")
28              If (Not IsNull(VarRec("COD_LOJA_VENDA")) Or VarRec("COD_LOJA_VENDA") <> 0) Then
29                  gFl_Busca_Pedido_Venda = "S"
30                  gLoja_Venda = VarRec("COD_LOJA_venda")
31                  gPedido_Venda = VarRec("NUM_PEDIDO_venda")
32                  gSeq_Venda = VarRec("SEQ_PEDIDO_venda")
33              End If
34          Else
35              'Gerar_Log "Achou - Loja Transf <> 1 -Varrec.Cod_loja_Venda = " & VarRec("COD_LOJA_VENDA")
36              If Not IsNull(VarRec("COD_LOJA_VENDA")) Or VarRec("COD_LOJA_VENDA") <> 0 Then    'Se existe o Num Pedido de Venda
37                  gFl_Busca_Pedido_Venda = "S"
38                  gLoja_Venda = VarRec("COD_LOJA_VENDA")
39                  gPedido_Venda = VarRec("NUM_PEDIDO_VENDA")
40                  gSeq_Venda = VarRec("SEQ_PEDIDO_VENDA")
41              End If
42          End If
43          'Gerar_Log "Executou a PR_BUSCA_PEDIDO_TRAND e Achou : - gLoja_venda: " & gLoja_Venda & " -gPedido_Venda:" & gPedido_Venda & " - gSeq_venda:" & gSeq_Venda
44      End If
45  End If

    '*******

46  If gLoja <> 1 Then               '-- DEP�SITOS REMOTOS
47      Call TESTA_CONEXAO(gLoja)
48      If gFl_Restrito = "S" Then    'N�o tem Comunica��o ou est� Restrito
49          Exit Sub
50      End If
51  End If

52  OraParameters.Remove "PMLOJA"
53  OraParameters.Add "PMLOJA", gLoja_Venda, 1
54  OraParameters.Remove "PMPED"
55  OraParameters.Add "PMPED", gPedido_Venda, 1
56  OraParameters.Remove "PMSEQ"
57  OraParameters.Add "PMSEQ", gSeq_Venda, 1

58  OraParameters.Remove "DEP"
59  vDep = "DEP" & Format(gLoja_Venda, "00")
60  OraParameters.Add "DEP", vDep, 1

61  Criar_Cursor

62  OraParameters.Remove "vErro"
63  OraParameters.Add "vErro", 0, 2

64  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_PEDNOTA(:VCURSOR,:DEP,:PMLOJA,:PMSEQ,:PMPED,:VERRO); END;"

65  Set VarRec = OraParameters("vCursor").Value
66  V_BANCO.Parameters.Remove "vCursor"

67  If OraParameters("vErro").Value <> 0 Then
68      'Gerar_Log "BUSCA_PEDIDO_VENDA_TRANSF - vERRO: " & OraParameters("vErro").Value
69      gFl_Restrito = "S"
70      Set VarRec = Nothing
71      Exit Sub
72  End If

73  If Not VarRec.EOF Then
74      'Gerar_Log "BUSCA_PEDIDO_VENDA_TRANSF - vFL_Tranf: S -LOJA:" & gLoja_Venda & " - SEQ:" & gSeq_Venda & " -PED:" & gPedido_Venda
75      vFl_Transf = "S"
76      gPedido = gPedido_Tabela
77      Call MONTA_RETORNO_PEDNOTA_VENDA(gLoja_Venda, gPedido_Venda, gSeq_Venda, vFl_Transf)
78      Call ITPEDNOTA_VENDA(gLoja_Venda, gPedido_Venda, gSeq_Venda)
79      Call ROMANEIO(gLoja_Venda, gPedido_Venda, gSeq_Venda)
80      Call R_PEDIDO_CONF(gLoja_Venda, gPedido_Venda, gSeq_Venda)
81      Call V_PEDLIQ_VENDA(gLoja_Venda, gPedido_Venda, gSeq_Venda)
82      gFl_Busca_Pedido_Venda = "N"
83  End If

84  Set VarRec = Nothing
85  Exit Sub

Trata_Erro:

86  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
87      gFl_Restrito = "S"
88  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
89      gFl_Restrito = "S"
90  ElseIf Err.Description Like "*12541*" Then
91      gFl_Restrito = "S"
92      Resume Next
93  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
94      Exit Sub
95  ElseIf Err.Number <> 0 Then
96      gCod_Erro = Err.Number
97      gDesc_Erro = Err.Description
98      gNomeSub_Erro = "BUSCA_PEDIDO_VENDA_TRANSF"
99      gLinha_Erro = Erl
100     If Erl = 65 Then
101         EnviarEmail "Sub BUSCA_PEDIDO_VENDA_TRANSF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "vDep:" & vDep & vbCrLf & "Loja:" & gLoja_Venda & vbCrLf & "Pedido:" & gPedido_Venda & vbCrLf & "Sequencia:" & gSeq_Venda & vbCrLf & "Parametros: " & vbCrLf & "PM_LOJA:" & OraParameters("PMLOJA").Value & vbCrLf & "PmPed:" & OraParameters("PMPED").Value & vbCrLf & "PmSeq:" & OraParameters("PMSEQ").Value & vbCrLf & "PmDep:" & OraParameters("DEP").Value
102     Else
103         EnviarEmail "Sub BUSCA_PEDIDO_VENDA_TRANSF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "vDep:" & vDep & vbCrLf & "Loja:" & Loja_V & vbCrLf & "Pedido:" & Pedido_V & vbCrLf & "Sequencia:" & Seq_V & vbCrLf & "Parametros: " & vbCrLf & "PM_LOJA:" & OraParameters("PMLOJA").Value & vbCrLf & "PmPed:" & OraParameters("PMPED").Value & vbCrLf & "PmSeq:" & OraParameters("PMSEQ").Value & vbCrLf & "PmDep:" & OraParameters("DEP").Value
104     End If
105     StayOnTop frmErros
106 End If

End Sub

'***************************
'MONTA_RETORNO_PEDNOTA_VENDA - ok
'***************************
Public Sub MONTA_RETORNO_PEDNOTA_VENDA(pLoja_Ret, pPedido_Ret, pSeq_Ret, pFl_Transf)

1   On Error GoTo Trata_Erro

    Dim vPEDNOTA As String
    Dim vMSG_PED As String
    Dim vMSG_NF As String
    Dim vNat_Oper As String
    Dim vCOD_VDR As String
    Dim gNum_cot As String

2   'Gerar_Log "Monta_Retorno_Pednota_Venda - Loja_Ret:" & pLoja_Ret & " - Pedido_Ret:" & pPedido_Ret & " - Seq_Ret:" & pSeq_Ret & " -Fl_Transf: " & pFl_Transf

3   gFl_Conteudo = "S"
4   vPEDNOTA = Format(VarRec("COD_LOJA_NOTA"), "00") & "|"
5   vPEDNOTA = vPEDNOTA & Format(VarRec("NUM_NOTA"), "000000") & "|"

6   vNat_Oper = Format(VarRec("COD_NOPE"), "000")

7   If pFl_Transf = "S" Or vNat_Oper = "A30" Then
        'Num_Pendente vai com o Num. Cotacao, para poder ser localizado na base do PAAC/Repres
        'Num_Pendente - tamanho de 11 caracteres
        'Em r_cotacao_transf: cod_loja 2 caracteres
        '                     num_cotacao 6 caracteres
        'Em Num_Pendente ser� passado o Cod_Loja+Num_Cotacao, sendo:
        'os dois primeiros d�gitos indicam o cod_loja e o restante o Num_Cotacao

        '********************************************************************************
        'EDUARDO - 14/06/2007
        'SE J� EXISTIR NUMERO DE PEDIDO, NAO ENVIAR O gLOJ_COT_TEMP E NEM O NUM_COT_TEMP
        'POIS PODE DAR ERRO DE CHAVE NA HORA QUE O FIL040 VAI ATUALIZAR O NUMERO PENDENTE
        'NA TABELA PEDNOTA_VENDA DO PAAC/REPRES.
        '********************************************************************************
8       If Val(0 & VarRec("COD_LOJA_NOTA")) > 0 And Val(0 & VarRec("NUM_NOTA")) > 0 Then
9           vPEDNOTA = vPEDNOTA & "00" & "000000000" & "|"
10      Else
11          vPEDNOTA = vPEDNOTA & Format(gLOJ_COT_TEMP, "00") & Format(gNUM_COT_TEMP, "000000000") & "|"
12      End If
13      pFl_Transf = "S"
14      gNum_cot = gNUM_COT_TEMP & "|"
15  Else
16      vPEDNOTA = vPEDNOTA & Format(VarRec("NUM_PENDENTE"), "00000000000") & "|"
17      gNum_cot = 0 & "|"
18  End If

19  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_FILIAL"), "0000") & "|"
20  vPEDNOTA = vPEDNOTA & Format(VarRec("TP_PEDIDO"), "00") & "|"
21  vPEDNOTA = vPEDNOTA & Format(VarRec("TP_DPKBLAU"), "0") & "|"
22  vPEDNOTA = vPEDNOTA & Format(VarRec("TP_TRANSACAO"), "0") & "|"
23  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_DIGITACAO"), "dd/mm/yy hh:nn:ss") & "|"
24  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_PEDIDO"), "dd/mm/yy") & "|"
25  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_EMISSAO_NOTA"), "dd/mm/yy hh:nn:ss") & "|"
26  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_SSM"), "dd/mm/yy hh:nn:ss") & "|"
27  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_BLOQ_POLITICA"), "dd/mm/yy hh:nn:ss") & "|"
28  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_BLOQ_CREDITO"), "dd/mm/yy hh:nn:ss") & "|"
29  vPEDNOTA = vPEDNOTA & Format(VarRec("DT_BLOQ_FRETE"), "dd/mm/yy hh:nn:ss") & "|"
30  vPEDNOTA = vPEDNOTA & VarRec("COD_NOPE") & "|"
31  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_CLIENTE"), "000000") & "|"
32  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_FORNECEDOR"), "000") & "|"
33  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_END_ENTREGA"), "000000") & "|"
34  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_END_COBRANCA"), "000000") & "|"
35  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_TRANSP"), "0000") & "|"
36  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_REPRES"), "0000") & "|"
37  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_VEND"), "0000") & "|"
38  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_PLANO"), "000") & "|"
39  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_BANCO"), "000") & "|"
40  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_CFO_DEST"), "0") & "|"
41  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_CFO_OPER"), "00") & "|"
42  vPEDNOTA = vPEDNOTA & VarRec("FRETE_PAGO") & "|"
43  vPEDNOTA = vPEDNOTA & Format(VarRec("PESO_BRUTO"), "00000.00") & "|"
44  vPEDNOTA = vPEDNOTA & Format(VarRec("QTD_SSM"), "00") & "|"
45  vPEDNOTA = vPEDNOTA & Format(VarRec("QTD_ITEM_PEDIDO"), "000") & "|"
46  vPEDNOTA = vPEDNOTA & Format(VarRec("QTD_ITEM_NOTA"), "000") & "|"
47  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_CONTABIL"), "000000000000.00") & "|"
48  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_IPI"), "00000000000.00") & "|"
49  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEICM1"), "000000000000.00") & "|"
50  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEICM2"), "000000000000.00") & "|"
51  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASE_1"), "000000000000.00") & "|"
52  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASE_2"), "000000000000.00") & "|"
53  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASE_3"), "000000000000.00") & "|"
54  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEISEN"), "000000000000.00") & "|"
55  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASE_5"), "000000000000.00") & "|"
56  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASE_6"), "000000000000.00") & "|"
57  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEOUTR"), "000000000000.00") & "|"
58  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEMAJ"), "000000000000.00") & "|"
59  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_ICMRETIDO"), "000000000000.00") & "|"
60  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BASEIPI"), "000000000000.00") & "|"
61  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BISENIPI"), "000000000000.00") & "|"
62  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_BOUTRIPI"), "000000000000.00") & "|"
63  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_FRETE"), "00000000000.00") & "|"
64  vPEDNOTA = vPEDNOTA & Format(VarRec("VL_DESP_ACESS"), "0000000000.00") & "|"
65  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_DESCONTO"), "000.00") & "|"
66  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_DESC_SUFRAMA"), "000.00") & "|"
67  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_ACRESCIMO"), "000.00") & "|"
68  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_SEGURO"), "00.00") & "|"
69  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_ICM1"), "00.00") & "|"
70  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_ICM2"), "00.00") & "|"
71  vPEDNOTA = vPEDNOTA & Format(VarRec("PC_ALIQ_INTERNA"), "00.00") & "|"
72  vPEDNOTA = vPEDNOTA & Format(VarRec("COD_CANCEL"), "00") & "|"
73  vPEDNOTA = vPEDNOTA & VarRec("FL_GER_SSM") & "|"
74  vPEDNOTA = vPEDNOTA & VarRec("FL_GER_NFIS") & "|"
75  vPEDNOTA = vPEDNOTA & VarRec("FL_PENDENCIA") & "|"
76  vPEDNOTA = vPEDNOTA & VarRec("FL_DESP_ACESS") & "|"
77  vPEDNOTA = vPEDNOTA & VarRec("FL_DIF_ICM") & "|"
78  vPEDNOTA = vPEDNOTA & VarRec("SITUACAO") & "|"
79  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_PROTESTO") & "|"
80  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_JUROS") & "|"
81  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_CHEQUE") & "|"
82  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_COMPRA") & "|"
83  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_DUPLICATA") & "|"
84  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_LIMITE") & "|"
85  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_SALDO") & "|"
86  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_CLIE_NOVO") & "|"
87  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_DESCONTO") & "|"
88  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_ACRESCIMO") & "|"
89  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_VLFATMIN") & "|"
90  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_ITEM_DESC1") & "|"
91  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_ITEM_DESC2") & "|"
92  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_ITEM_DESC3") & "|"
93  vPEDNOTA = vPEDNOTA & VarRec("BLOQ_FRETE") & "|"
94  vMSG_PED = VarRec("MENS_PEDIDO") & "|"
95  vMSG_NF = VarRec("MENS_NOTA") & "|"
96  vCOD_VDR = VarRec("Cod_Vdr") & "|"

97  If pFl_Transf = "S" Then
98      Print #2, Format(pLoja_Ret, "00") & "|" & Format(pPedido_Ret, "0000000") & "|" & Format(pSeq_Ret, "0") & "|"
99  Else
100     Print #2, Format(gLoja, "00") & "|" & Format(gPedido, "0000000") & "|" & Format(gSeq, "0") & "|"
101 End If

102 Print #2, "ped|"; vPEDNOTA & vMSG_PED & vMSG_NF & vCOD_VDR

Trata_Erro:
103 If Err.Number <> 0 Then
104     gCod_Erro = Err.Number
105     gDesc_Erro = Err.Description
106     gNomeSub_Erro = "MONTA_RETORNO_PEDNOTA_VENDA"
107     gLinha_Erro = Erl
108     EnviarEmail "Sub MONTA_RETORNO_PEDNOTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja_Ret & vbCrLf & "Pedido:" & pPedido_Ret & vbCrLf & "Sequencia:" & pSeq_Ret
109     StayOnTop frmErros
110 End If

End Sub

'****************
'AUTOLOG.ROMANEIO - ok
'****************
Public Sub ROMANEIO(pLoja, pPedido, pSeq)
    Dim ROMANEIO As String

1   On Error GoTo Trata_Erro

2   If pLoja <> 1 Then               '-- DEP�SITOS REMOTOS
3       Call TESTA_CONEXAO(pLoja)
4       If gFl_Restrito = "S" Then    '-- N�o tem Comunica��o ou est� Restrito
5           Exit Sub
6       End If
7   End If

8   OraParameters.Remove "PMLOJA"
9   OraParameters.Add "PMLOJA", pLoja, 1
10  OraParameters.Remove "PMPED"
11  OraParameters.Add "PMPED", pPedido, 1
12  OraParameters.Remove "PMSEQ"
13  OraParameters.Add "PMSEQ", pSeq, 1

14  OraParameters.Remove "DEP"
15  vDep = "DEP" & Format(pLoja, "00")
16  OraParameters.Add "DEP", vDep, 1

17  Criar_Cursor

18  OraParameters.Remove "vErro"
19  OraParameters.Add "vErro", 0, 2

20  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_ROMANEIO(:VCURSOR,:DEP,:PMLOJA,:PMSEQ,:PMPED,:VERRO); END;"
21  Set VarRec = OraParameters("vCursor").Value
22  V_BANCO.Parameters.Remove "vCursor"

23  If OraParameters("vErro").Value <> 0 Then
24      gFl_Restrito = "S"
25      Set VarRec = Nothing
26      Exit Sub
27  End If

28  If Not VarRec.EOF Then
29      ROMANEIO = Format(VarRec("DT_COLETA"), "dd/mm/yy") & "|"
30      ROMANEIO = ROMANEIO & Format(VarRec("DT_DESPACHO"), "dd/mm/yy") & "|"
31      ROMANEIO = ROMANEIO & Format(VarRec("NUM_ROMANEIO"), "000000") & "|"
32      ROMANEIO = ROMANEIO & Format(VarRec("NUM_CONHECIMENTO"), "000000000") & "|"
33      ROMANEIO = ROMANEIO & Format(VarRec("NUM_CARRO"), "000000") & "|"
34      ROMANEIO = ROMANEIO & Format(VarRec("QTD_VOLUME"), "000000") & "|"
35      Print #2, "rom|"; ROMANEIO

36      VarRec.MoveNext
37  End If

38  Set VarRec = Nothing

39  Exit Sub

Trata_Erro:
40  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
41      gFl_Restrito = "S"
42  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
43      gFl_Restrito = "S"
44  ElseIf Err.Description Like "*12541*" Then
45      gFl_Restrito = "S"
46  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
47      Set VarRec = Nothing
48      Exit Sub
49  ElseIf Err.Number <> 0 Then
50      gCod_Erro = Err.Number
51      gDesc_Erro = Err.Description
52      gNomeSub_Erro = "ROMANEIO"
53      gLinha_Erro = Erl
54      EnviarEmail "Sub ROMANEIO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq
55      StayOnTop frmErros
56  End If

End Sub

'*************************************
'Atualiza Flag de Pedido j� consultado - OK
'*************************************
Public Sub ATUALIZA_TABELA(pLoja, pPedido, pSeq, pSequencia)

1   On Error GoTo Trata_Erro

2   DoEvents

3   OraParameters.Remove "LOJA"
4   OraParameters.Add "LOJA", pLoja, 1
5   OraParameters.Remove "PED"
6   OraParameters.Add "PED", pPedido, 1
7   OraParameters.Remove "SEQ"
8   OraParameters.Add "SEQ", pSeq, 1
9   OraParameters.Remove "SEQUENCIA"
10  OraParameters.Add "SEQUENCIA", pSequencia, 1
11  OraParameters.Remove "DT_RET"
12  OraParameters.Add "DT_RET", Format(Date, "SHORT DATE") & " " & Format(Time, "LONG TIME"), 1

13  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_ATUALIZA_TABELA(:LOJA,:SEQ,:PED,:SEQUENCIA); END;"

Trata_Erro:
14  If Err.Number <> 0 Then
15      gCod_Erro = Err.Number
16      gDesc_Erro = Err.Description
17      gNomeSub_Erro = "ATUALIZA_TABELA"
18      gLinha_Erro = Erl
19      EnviarEmail "Sub ATUALIZA_TABELA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq
20      StayOnTop frmErros
21  End If
End Sub

Public Sub TESTA_RESTRITO(Loja)

1   On Error GoTo Trata_Erro

    Dim ss3 As Object
    Dim SQL_Atu As String
    Dim texto_erro

2   DoEvents

3   gFl_Restrito = "N"
4   OraParameters.Remove "PMLOJA"
5   OraParameters.Add "PMLOJA", Loja, 1
6   OraParameters.Remove "DEP"
7   vDep = "DEP" & Format(Loja, "00")
8   OraParameters.Add "DEP", vDep, 1

9   OraParameters.Remove "vErro"
10  OraParameters.Add "vErro", 0, 2

11  Criar_Cursor
12  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_TESTA_RESTRITO(:VCURSOR,:DEP,:PMLOJA,:VERRO);END;"
13  Set VarRec = OraParameters("vCursor").Value
14  V_BANCO.Parameters.Remove "vCursor"

15  gFl_Restrito = "N"

16  If Val(OraParameters("vErro").Value) <> 0 Then
17      gFl_Restrito = "S"
18      Set VarRec = Nothing
19      Exit Sub
20  End If

21  Set VarRec = Nothing
22  Exit Sub

Trata_Erro:
23  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
24      gFl_Restrito = "S"
25  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*12154*" Then    'Sem Comunica��o
26      gFl_Restrito = "S"
27  ElseIf Err.Description Like "*12541*" Then
28      gFl_Restrito = "S"
29      Resume Next
30  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
31      Set VarRec = Nothing
32      Exit Sub
33  ElseIf Err.Number <> 0 Then
34      gCod_Erro = Err.Number
35      gDesc_Erro = Err.Description
36      gNomeSub_Erro = "TESTA_RESTRITO"
37      gLinha_Erro = Erl
38      EnviarEmail "Sub TESTA_RESTRITO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & Loja
39      StayOnTop frmErros
40  End If

End Sub

'*********************
'AUTOLOG.R_PEDIDO_CONF - ok
'*********************
Public Sub R_PEDIDO_CONF(pLoja, pPedido, pSeq)
    Dim conf As String

1   On Error GoTo Trata_Erro

2   DoEvents

3   If pLoja <> 1 Then               '-- DEP�SITOS REMOTOS
4       Call TESTA_CONEXAO(pLoja)
5       If gFl_Restrito = "S" Then    '-- N�o tem Comunica��o ou est� Restrito
6           Exit Sub
7       End If
8   End If

9   'Gerar_Log "R_PEDIDO_CONF - PR_BUSCA_R_PEDIDO_CONF -Dep: " & pLoja & " -Loja:" & pLoja & " -Seq:" & pSeq & " -Pedido:" & pPedido

10  OraParameters.Remove "PMLOJA"
11  OraParameters.Add "PMLOJA", pLoja, 1
12  OraParameters.Remove "PMPED"
13  OraParameters.Add "PMPED", pPedido, 1
14  OraParameters.Remove "PMSEQ"
15  OraParameters.Add "PMSEQ", pSeq, 1

16  OraParameters.Remove "DEP"
17  vDep = "DEP" & Format(pLoja, "00")
18  OraParameters.Add "DEP", vDep, 1

19  OraParameters.Remove "vErro"
20  OraParameters.Add "vErro", 0, 2
21  Criar_Cursor

22  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_R_PEDIDO_CONF(:VCURSOR,:DEP,:PMLOJA,:PMSEQ,:PMPED,:VERRO);END;"
23  Set VarRec = OraParameters("vCursor").Value
24  V_BANCO.Parameters.Remove "vCursor"

25  If OraParameters("vErro").Value <> 0 Then
26      gFl_Restrito = "S"
27      Set VarRec = Nothing
28      Exit Sub
29  End If

30  If Not VarRec.EOF Then
31      For i = 1 To VarRec.RecordCount
32          conf = ""
33          conf = Format(VarRec("NUM_CAIXA"), "0000") & "|"
34          conf = conf & Format(VarRec("COD_CONFERENTE"), "00000") & "|"
35          conf = conf & Format(VarRec("COD_EMBALADOR"), "00000") & "|"
36          conf = conf & Format(VarRec("COD_EXPEDIDOR"), "00000") & "|"
37          conf = conf & VarRec("FL_PENd_ETIQ") & "|"
38          conf = conf & Format(VarRec("QTD_VOLUME_PARC"), "000000") & "|"
39          Print #2, "r_p|"; conf
40          VarRec.MoveNext
41      Next
42  End If
43  Set VarRec = Nothing

44  Exit Sub

Trata_Erro:
45  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
46      gFl_Restrito = "S"
47  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
48      gFl_Restrito = "S"
49  ElseIf Err.Description Like "*12541*" Then
50      gFl_Restrito = "S"
51  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
52      Set VarRec = Nothing
53      Exit Sub
54  ElseIf Err.Number <> 0 Then
55      gCod_Erro = Err.Number
56      gDesc_Erro = Err.Description
57      gNomeSub_Erro = "R_PEDIDO_CONF"
58      gLinha_Erro = Erl
59      EnviarEmail "Sub R_PEDIDO_CONF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq
60      StayOnTop frmErros
61  End If

End Sub

'**************
'V_PEDLIQ_VENDA - ok
'**************
Public Sub V_PEDLIQ_VENDA(pLoja, pPedido, pSeq)
1   On Error GoTo Trata_Erro

    Dim pedliq As String

2   DoEvents

3   If pLoja <> 1 Then        '-- CD REMOTO
4       Call TESTA_CONEXAO(pLoja)
5       If gFl_Restrito = "S" Then
6           Exit Sub
7       End If
8   End If

9   OraParameters.Remove "PMLOJA"
10  OraParameters.Add "PMLOJA", pLoja, 1
11  OraParameters.Remove "PMPED"
12  OraParameters.Add "PMPED", pPedido, 1
13  OraParameters.Remove "PMSEQ"
14  OraParameters.Add "PMSEQ", pSeq, 1

15  OraParameters.Remove "DEP"
16  vDep = "DEP" & Format(pLoja, "00")
17  OraParameters.Add "DEP", vDep, 1

18  Criar_Cursor

19  OraParameters.Remove "vErro"
20  OraParameters.Add "vErro", 0, 2

21  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_V_PEDLIQ_VENDA(:VCURSOR,:DEP,:PMLOJA,:PMSEQ,:PMPED,:VERRO); END;"
22  Set VarRec = OraParameters("vCursor").Value
23  V_BANCO.Parameters.Remove "vCursor"

24  If OraParameters("vErro").Value <> 0 Then
25      gFl_Restrito = "S"
26      Set VarRec = Nothing
27      Exit Sub
28  End If

29  If Not VarRec.EOF Then
30      For i = 1 To VarRec.RecordCount
31          pedliq = ""
32          pedliq = Format(VarRec("NUM_ITEM_PEDIDO"), "000") & "|"
33          pedliq = pedliq & VarRec("SITUACAO") & "|"
34          pedliq = pedliq & Format(VarRec("PR_LIQUIDO"), "000000000000.00") & "|"
35          pedliq = pedliq & Format(VarRec("VL_LIQUIDO"), "000000000000.00") & "|"
36          pedliq = pedliq & Format(VarRec("PR_LIQUIDO_SUFRAMA"), "000000000000.00") & "|"
37          pedliq = pedliq & Format(VarRec("VL_LIQUIDO_SUFRAMA"), "000000000000.00") & "|"
38          Print #2, "v_p|"; pedliq
39          VarRec.MoveNext
40      Next
41  End If

42  Set VarRec = Nothing
43  Exit Sub

Trata_Erro:
44  If Err.Description Like "*1035*" Or Err.Description Like "*0020*" Or Err.Description Like "*28000*" Then
45      gFl_Restrito = "S"
46  ElseIf Err.Description Like "*12535*" Or Err.Description Like "*2068*" Then    'Sem Comunica��o
47      gFl_Restrito = "S"
48  ElseIf Err.Description Like "*12541*" Then
49      gFl_Restrito = "S"
50      Resume Next
51  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
52      Set VarRec = Nothing
53      Exit Sub
54  ElseIf Err.Number <> 0 Then
55      gCod_Erro = Err.Number
56      gDesc_Erro = Err.Description    '& vbCrLf & "Num_Item_Pedido:" & VarRec("NUM_ITEM_PEDIDO") & vbCrLf & "Situacao:" & VarRec("SITUACAO") & vbCrLf & "Pr_Liquido:" & VarRec("PR_LIQUIDO") & vbCrLf & "VL_LIQUIDO:" & VarRec("VL_LIQUIDO") & vbCrLf & "Pr_Liquido_Suframa:" & VarRec("PR_LIQUIDO_SUFRAMA") & vbCrLf & "Vl_Liquido_Suframa:" & VarRec("VL_LIQUIDO_SUFRAMA")
57      gNomeSub_Erro = "V_PEDLIQ_VENDA"
58      gLinha_Erro = Erl
59      EnviarEmail "Sub V_PEDLIQ_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & pLoja & vbCrLf & "Pedido:" & pPedido & vbCrLf & "Sequencia:" & pSeq
60      StayOnTop frmErros
61  End If

End Sub

Private Sub cmdEntrada_Click()
1   CD.ShowOpen
2   txtEntrada = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdEntradaPedido_Click()
1   CD.ShowOpen
2   txtEntradaPedido = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdGravar_Click()

1   On Error GoTo Trata_Erro

2   If Val(txtTempo) = 0 Then
3       MsgBox "Informe o tempo entre os processamentos.", vbInformation, "Aten��o"
4       txtTempo.SetFocus
5       Exit Sub
6   End If

7   If Trim(txtEntrada) = "" Then
8       MsgBox "Informe o caminho da Entrada.", vbInformation, "Aten��o"
9       txtEntrada.SetFocus
10      Exit Sub
11  End If
12  If Trim(txtEntradaPedido) = "" Then
13      MsgBox "Informe o caminho da Entrada do Pedido.", vbInformation, "Aten��o"
14      txtEntradaPedido.SetFocus
15      Exit Sub
16  End If

17  If Trim(txtSaida) = "" Then
18      MsgBox "Informe o caminho da Saida.", vbInformation, "Aten��o"
19      txtSaida.SetFocus
20      Exit Sub
21  End If
22  If Trim(txtSaidaPedido) = "" Then
23      MsgBox "Informe o caminho da Entrada do Pedido.", vbInformation, "Aten��o"
24      txtSaidaPedido.SetFocus
25      Exit Sub
26  End If

    'Verificar Barra Invertida
27  If Right(Trim(txtEntrada), 1) <> "\" Then
28      MsgBox "O campo deve terminar com \ (Barra Invertida).", vbInformation, "Aten��o"
29      txtEntrada.SetFocus
30      Exit Sub
31  End If

32  If Right(Trim(txtEntradaPedido), 1) <> "\" Then
33      MsgBox "O campo deve terminar com \ (Barra Invertida).", vbInformation, "Aten��o"
34      txtEntrada.SetFocus
35      Exit Sub
36  End If

37  If Right(Trim(txtSaida), 1) <> "\" Then
38      MsgBox "O campo deve terminar com \ (Barra Invertida).", vbInformation, "Aten��o"
39      txtEntrada.SetFocus
40      Exit Sub
41  End If

42  If Right(Trim(txtSaidaPedido), 1) <> "\" Then
43      MsgBox "O campo deve terminar com \ (Barra Invertida).", vbInformation, "Aten��o"
44      txtEntrada.SetFocus
45      Exit Sub
46  End If

47  Gravar_Parametros "TEMPO", txtTempo
48  Gravar_Parametros "Entrada", txtEntrada
49  Gravar_Parametros "EntradaPedido", txtEntradaPedido
50  Gravar_Parametros "Saida", txtSaida
51  Gravar_Parametros "SaidaPedido", txtSaidaPedido

Trata_Erro:
52  If Err.Number <> 0 Then
53      gCod_Erro = Err.Number
54      gDesc_Erro = Err.Description
55      gNomeSub_Erro = "cmdGravar_click"
56      gLinha_Erro = Erl
57      EnviarEmail "Sub cmdGravar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
58      StayOnTop frmErros
59  End If

End Sub

Private Sub cmdParametros_Click()
1   If Me.Height = 1905 Then
2       Me.Height = 5650
3       Me.lblEnvia = "Processo Parado! Altere os Par�metros."
4       Timer1.Enabled = False
5   Else
6       Me.Height = 1905
7       Me.lblEnvia = "Iniciando Processamento..."
8       Timer1.Enabled = True
9   End If
End Sub

Private Sub cmdSaida_Click()
1   CD.ShowOpen
2   txtSaida = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdSaidaPedido_Click()
1   CD.ShowOpen
2   txtSaidaPedido = Left(CD.FileName, Len(CD.FileName) - Len(CD.FileTitle))
End Sub

Private Sub cmdSair_Click()
1   End
End Sub

Private Sub Form_Load()

1   On Error GoTo TrataErro

    Dim rst As Object

2   'Gerar_Log "Iniciou " & Now

3   Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision

4   Set V_SESSAO = CreateObject("oracleinprocserver.xorasession")
5   Set V_BANCO = V_SESSAO.OpenDatabase("PRODUCAO", "FIL080/PROD", 0&)

6   Carregar_Parametros

7   If Command$ = "O" Then
8       lblCds = "CD Remoto: 05"
9   Else
10      Montar_lista_Bancos
11  End If

12  Set OraParameters = V_BANCO.Parameters

13  Criar_Cursor

14  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_DATA(:VCURSOR); END;"

15  Set rst = OraParameters("vCursor").Value

16  V_BANCO.Parameters.Remove "vCursor"

17  lblDtFat.Caption = rst("DT_FATURAMENTO")

18  rst.Close
19  Set rst = Nothing

20  Me.Visible = True

21  vTempo = 2
22  Timer1_Timer

23  Exit Sub

TrataErro:

24  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Or Err = 75 Then
25      Resume
26  ElseIf Err = 75 Then
27      Resume Next
28  Else
29      gCod_Erro = Err.Number
30      gDesc_Erro = Err.Description
31      gNomeSub_Erro = "Form_load"
32      gLinha_Erro = Erl
33      StayOnTop frmErros
34      EnviarEmail "Sub FrmFimPedido_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
35  End If

End Sub

'***************************************** - ok
'DELETA REGISTROS DO DIA ANTERIOR
'Mantem no Log apenas os dois �ltimos dias
'*****************************************
Private Sub cmdIniciar_Click()
1   On Error GoTo TrataErro

2   If Command$ = "L" Then
3       lblEnvia = "Deletando Registros... "
4       OraParameters.Remove "DATA"
5       OraParameters.Add "DATA", CDate(lblDtFat), 1
6       V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_DELETA_CONSULTAS(:DATA); END;"
7       lblEnvia = "Verificando Solicita��o de Consulta..."
8   End If

9   Call Arquivo_Cons_Pedido



10  Exit Sub

TrataErro:

11  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
12      Resume
13  ElseIf Err = 76 Or Err = 53 Or Err = 52 Or Err = 53 Or Err = 75 Or Err = 55 Or Err = 70 Then
14      Resume Next
15      Resume Next
16  ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
17      Exit Sub
18  ElseIf Err.Number <> 0 Then
19      gCod_Erro = Err.Number
20      gDesc_Erro = Err.Description
21      gNomeSub_Erro = "cmdIniciar_click"
22      gLinha_Erro = Erl
23      EnviarEmail "Sub cmdIniciar_click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
24      StayOnTop frmErros
25  End If
End Sub

Private Sub Timer1_Timer()

1   vTempo = vTempo + 1
2   If vTempo >= txtTempo Then
3       lblEnvia.Caption = "Buscando Pr�xima Consulta ..."
4       frmFimPedido.Refresh
5       Me.Timer1.Enabled = False
6       Call cmdIniciar_Click
7       Me.Timer1.Enabled = True
8       vTempo = 0
9   End If

End Sub

'****************************************
'L� a tabela de log e come�a as consultas  -- OK
'****************************************
Private Sub CONS_PEDIDO()

1   On Error GoTo Trata_Erro

    Dim vFl_Aberto As String
    Dim vSequencia As Long
    Dim vEmail As String
    Dim vLoja_Tabela
    Dim vPedido_Tabela
    Dim vSeq_Tabela

2   'Gerar_Log "CONS_PEDIDO - Buscando Novas Consultas - PR_BUSCA_PEDIDOS - Parametro: " & Command$

3   lblEnvia.Caption = "Buscando Novas Consultas ..."

4   frmFimPedido.Refresh

5   vFl_Aberto = "N"
6   gFl_Dados = "N"

    '************************************************************
    'Busca no Oracle, os Pedidos que ainda n�o foram consultados.
    '************************************************************
7   Sair

8   Set V_SESSAO = CreateObject("oracleinprocserver.xorasession")
9   Set V_BANCO = V_SESSAO.OpenDatabase("PRODUCAO", "FIL080/PROD", 0&)

10  Set OraParameters = V_BANCO.Parameters

11  Criar_Cursor
12  OraParameters.Remove "VPAR"
13  OraParameters.Add "VPAR", Command$, 1

14  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_BUSCA_PEDIDOS(:VCURSOR,:VPAR); END;"

15  Set VarPed = OraParameters("vCursor").Value

16  V_BANCO.Parameters.Remove "vCursor"

17  Do While Not VarPed.EOF
18      DoEvents
19      vSequencia = VarPed("Sequencia")
20      vEmail = VarPed("E_MAIL")
21      gLoja = VarPed("COD_LOJA")
22      gPedido = VarPed("NUM_PEDIDO")
23      gSeq = VarPed("SEQ_PEDIDO")

24      'Gerar_Log "Definir nome do arquivo de retorno .CON - vSequencia: " & vSequencia & " gLoja: " & gLoja & " - gSeq:" & gSeq & " - vEmail: " & vEmail

        '****************************************
        'Define o Nome do Arquivo de Retorno .CON
        '****************************************
25      If vFl_Aberto = "N" Then  'SE ARQUIVO AINDA N�O FOI CRIADO E ABERTO
26          gNome_Arquivo = txtSaidaPedido & "CON" & Command$ & Mid(vEmail, 5, 4) & Format(Time, "hhmmss") & ".CON"
27          If Trim(Dir(gNome_Arquivo)) <> "" Then   '-- J� existe Arquivo com este Nome
28              gNome_Arquivo = txtSaidaPedido & "CON" & Command$ & Mid(vEmail, 5, 4) & Format(Time, "hhmmss") & ".CON"
29          End If
30          lbl_Arquivo = "Arquivo: " & Mid(gNome_Arquivo, 8) & " Pedido: " & Format(gLoja, "00") & " " & Format(gPedido, "0000000") & "-" & Format(gSeq, "00")
31          frmFimPedido.Refresh
32      Else
33          lbl_Arquivo = "Arquivo: " & Mid(gNome_Arquivo, 8) & " Pedido: " & Format(gLoja, "00") & " " & Format(gPedido, "0000000") & "-" & Format(gSeq, "00")
34          frmFimPedido.Refresh
35      End If

36      If gLoja <> 1 Then   '-- DEP�SITOS REMOTOS
37          vLoja_Tabela = gLoja
38          vPedido_Tabela = gPedido
39          vSeq_Tabela = gSeq

40          Call TESTA_CONEXAO(gLoja)
41          If gFl_Restrito = "S" Then    '-- N�o tem Comunica��o ou est� Restrito
42              lbl_Arquivo = "Arquivo: " & Mid(gNome_Arquivo, 8) & " Pedido:" & gLoja & " " & gPedido & "-" & gSeq
43              frmFimPedido.Refresh
44          Else
45              If vFl_Aberto = "N" Then
46                  Open gNome_Arquivo For Output As #2
47                  vFl_Aberto = "S"
48              End If
49          End If
50          If gFl_Restrito = "N" Then     '-- Sen�o estiver restrito faz a consulta
51              Call PEDNOTA_VENDA(gLoja, gPedido, gSeq)
52              If gFl_Restrito = "S" Then  '-- Se est� restrito ou sem comunica��o deleta o arquivo aberto
53                  Close #2
54                  Kill gNome_Arquivo
55                  vFl_Aberto = "N"
56                  gFl_Dados = "N"
57              Else
58                  Call ITPEDNOTA_VENDA(gLoja, gPedido, gSeq)
59                  Call ROMANEIO(gLoja, gPedido, gSeq)
60                  Call R_PEDIDO_CONF(gLoja, gPedido, gSeq)
61                  Call V_PEDLIQ_VENDA(gLoja, gPedido, gSeq)
62                  If gFl_Busca_Pedido_Venda = "S" Then
63                      Call BUSCA_PEDIDO_VENDA_TRANSF(gLoja, gPedido, gSeq)
64                  End If
65                  If gFl_Transf = "S" Then
66                      Call ATUALIZA_TABELA(vLoja_Tabela, vPedido_Tabela, vSeq_Tabela, vSequencia)
67                  Else
68                      Call ATUALIZA_TABELA(gLoja, gPedido, gSeq, vSequencia)
69                  End If
70                  gFl_Dados = "S"
71              End If
72          ElseIf gFl_Restrito = "S" Then  '-- Se est� restrito deleta o arquivo aberto
73              Close #2
74              Kill gNome_Arquivo
75              vFl_Aberto = "N"
76              gFl_Dados = "N"
77          End If
            'End If
78      Else                             '-- BANCO PRODUCAO CAMPINAS
79          gFl_Restrito = "N"
80          If vFl_Aberto = "N" Then
81              Open gNome_Arquivo For Output As #2
82              vFl_Aberto = "S"
83          End If

84          vLoja_Tabela = gLoja
85          vPedido_Tabela = gPedido
86          vSeq_Tabela = gSeq

87          Call PEDNOTA_VENDA(gLoja, gPedido, gSeq)
88          Call ITPEDNOTA_VENDA(gLoja, gPedido, gSeq)
89          Call ROMANEIO(gLoja, gPedido, gSeq)
90          Call R_PEDIDO_CONF(gLoja, gPedido, gSeq)
91          Call V_PEDLIQ_VENDA(gLoja, gPedido, gSeq)

92          If gFl_Busca_Pedido_Venda = "S" Then
93              Call BUSCA_PEDIDO_VENDA_TRANSF(gLoja_Venda, gPedido_Venda, gSeq_Venda)
94          End If
95          If gFl_Transf = "S" Then
96              Call ATUALIZA_TABELA(vLoja_Tabela, vPedido_Tabela, vSeq_Tabela, vSequencia)
97          Else
98              Call ATUALIZA_TABELA(gLoja, gPedido, gSeq, vSequencia)
99          End If
100         gFl_Dados = "S"
101     End If

102     Screen.MousePointer = 0

        '*************************************
        'Busca Pr�ximo Pedido a ser Consultado
        '*************************************
103     VarPed.MoveNext

104     If (VarPed.EOF Or (vEmail <> VarPed!E_Mail)) And gFl_Dados = "S" Then
105         Close #2
106     End If

107     If (VarPed.EOF Or (vEmail <> VarPed!E_Mail)) And FileLen(gNome_Arquivo) = 0 Then
108         Kill gNome_Arquivo
109         gFl_Dados = "N"
110         vFl_Aberto = "N"
111     Else
112         If gFl_Restrito = "N" Then
113             If VarPed.EOF And gFl_Dados = "S" Then
114                 Close #2
115                 vFl_Aberto = "N"
116                 gFl_Dados = "N"
117                 gNome_Arquivo = Mid(gNome_Arquivo, 1, InStr(gNome_Arquivo, ".") - 1) & ".HDR"

118                 If Dir(Mid(gNome_Arquivo, 1, 21) & ".CON") <> "" Then
119                     Open gNome_Arquivo For Output As #3
120                     Print #3, "GERACAO   ="; Date
121                     Print #3, "TO        ="; vEmail
122                     Print #3, "FROM      ="; "DPKPEDID"
123                     Print #3, "DESCRICAO ="; "#CON" & Mid(gNome_Arquivo, 11, 11) & ".CON"
124                     Print #3, "FILE      ="; Mid(gNome_Arquivo, 8, 14) & ".CON"
125                     Close #3
126                 End If
127             ElseIf (vEmail <> VarPed!E_Mail Or VarPed.EOF) And gFl_Dados = "S" Then
128                 Close #2
129                 vFl_Aberto = "N"
130                 gFl_Dados = "N"
131                 gNome_Arquivo = Mid(gNome_Arquivo, 1, InStr(gNome_Arquivo, ".") - 1) & ".HDR"

132                 If Dir(Mid(gNome_Arquivo, 1, 21) & ".CON") <> "" Then

133                     Open gNome_Arquivo For Output As #3
134                     Print #3, "GERACAO   ="; Date
135                     Print #3, "TO        ="; vEmail
136                     Print #3, "FROM      ="; "DPKPEDID"
137                     Print #3, "DESCRICAO ="; "#CON" & Mid(gNome_Arquivo, 11, 11) & ".CON"
138                     Print #3, "FILE      ="; Mid(gNome_Arquivo, 8, 14) & ".CON"
139                     Close #3
140                 End If
141             ElseIf gFl_Dados = "N" And (vEmail <> VarPed!E_Mail) Then
                    '-- GEROU UM ARQUIVO VAZIO, PORQUE N�O CONSEGUIU CONSULTAR NADA
                    '-- DELETA O ARQUICO .CON
142                 gFl_Dados = "N"
143                 Close #2
144                 Kill gNome_Arquivo
145                 vFl_Aberto = "N"
146             End If

147         ElseIf gFl_Restrito = "S" And (vEmail <> VarPed!E_Mail) Then
148             gFl_Restrito = "N"
149             Close #2
150             Kill gNome_Arquivo
151             vFl_Aberto = "N"
152         End If
153     End If

154 Loop

155 Set VarPed = Nothing
156 Close #2

157 lblEnvia.Caption = "Buscando Novas Consultas ..."
158 frmFimPedido.Refresh

159 lbl_Arquivo = " "
160 frmFimPedido.Refresh

161 Exit Sub

Trata_Erro:
162 If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
163     Resume
164 ElseIf Err = 76 Or Err = 52 Or Err = 75 Or Err = 70 Or Err = 55 Then
165     Exit Sub
166 ElseIf Err = 53 Then
167     Resume Next
168 ElseIf Err.Description Like "*ORA-00604*" Or Err.Description Like "*ORA-01003*" Then
169     Exit Sub
170 ElseIf Err.Number <> 0 Then
171     gCod_Erro = Err.Number
172     gDesc_Erro = Err.Description
173     gNomeSub_Erro = "CONS_PEDIDO"
174     gLinha_Erro = Erl
175     EnviarEmail "Sub CONS_PEDIDO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & gLoja & vbCrLf & "Pedido:" & gPedido & vbCrLf & "Sequencia:" & gSeq
176     StayOnTop frmErros
177 End If

End Sub

'--OK
Public Sub Arquivo_Cons_Pedido()
1     On Error GoTo TrataErro

    Dim vLinha As String
    Dim vChar As String
    Dim vMsg As String
    Dim vNome_Con As String
    Dim vNomeArquivo As String
    Dim vConteudoLinha As String
    Dim vTo As String
    Dim vFrom As String
    Dim vDate As String
    Dim vSubject As String
    Dim vConteudo As String
    Dim vNPed

2     'Gerar_Log "Entrou na Arquivo_Cons_Pedido - Command: " & Command$

3     gFl_Busca_Pedido_Venda = ""

    '*****************************************
    'VERIFICA SE EXISTE CONSULTA PARA SER LIDA
    '*****************************************
4     If Command$ = "L" Then
5       vNomeArquivo = Dir(txtEntradaPedido & "*.CON")

6       Do While vNomeArquivo <> ""
7           DoEvents
8           vNome_Con = Left(vNomeArquivo, InStrRev(vNomeArquivo, ".") - 1)
9           vNomeArquivo = txtEntradaPedido + vNomeArquivo
10          vMsg = txtEntradaPedido & vNome_Con & ".HDR"

            '****************************************************************
            'L� ARQUIVO DE MSG PARA IDENTIFICAR FRANQUIA/PAAC/REPRES E PEDIDO
            '****************************************************************
11          Open vMsg For Input As #5
12          Do While Not EOF(5)
13              DoEvents
14              Line Input #5, vConteudoLinha
15              i = InStr(vConteudoLinha, "=")
16              If i <> 0 Then
17                  Select Case UCase(Mid(vConteudoLinha, 1, i - 1))
                    Case "TO        "
18                      vTo = Mid(vConteudoLinha, i + 1)
19                  Case "FROM      "
20                      vFrom = Mid(vConteudoLinha, i + 1)
21                  Case "RECEPCAO  "
22                      vDate = Mid(vConteudoLinha, i + 1)
23                  Case "DESCRICAO "
24                      vSubject = Mid(vConteudoLinha, i + 1)
                    'WILLIAM LEITE - CORRE��O PAAC
25                  Case "GERACAO   "
26                      vDate = Format(Mid(vConteudoLinha, i + 1), "DD/MM/YY")
27                  Case Else
28                      vConteudo = vConteudo & vConteudoLinha & vbCrLf
29                  End Select
30              Else
31                  vConteudo = vConteudo & vConteudoLinha & vbCrLf
32              End If
33          Loop
34          Close #5

            '**************************************************
            'vFrom = FRANQUIA/REPRESENTANTE QUE ENVIOU O PEDIDO
            'vNPed = N�MERO DO PEDIDO
            '**************************************************
35          If Mid(vSubject, 1, 1) = "P" Then
36              vNPed = Mid(vSubject, 7, 2)
37          Else
38              vNPed = Mid(vSubject, 5, 4)
39          End If

            '***********************************
            'LEITURA DOS PEDIDOS - ARQUIVO *.CON
            '***********************************
            'WILLIAM LEITE - ERRO
40          Kill txtSaidaPedido & "CON" & vNPed & ".CON"
41          Open vNomeArquivo For Input As #5
42          lblEnvia.Caption = "Verificando pedido para consulta..."

43          If Len(vNomeArquivo) Then
44              frmFimPedido.Refresh
45              vNomeArquivo = vNome_Con & ".CON"
46              Do While Not EOF(5)
                    '41                      vChar = Input(1, #5)
                    '42                      If vChar <> Chr(10) Then     'L� AT� ACHAR O FINAL DE LINHA
                    '43                         vLinha = vLinha & vChar
                    '44                      Else                         'ACHOU O FINAL DE LINHA
47                  Line Input #5, vLinha
48                  vLinha = Replace(vLinha, Chr(10), "")

49                  OraParameters.Remove "LOJA"
50                  OraParameters.Add "LOJA", Mid(vLinha, 1, 2), 1
51                  OraParameters.Remove "PED"
52                  OraParameters.Add "PED", Mid(vLinha, 3, 7), 1
53                  OraParameters.Remove "SEQ"
54                  OraParameters.Add "SEQ", Mid(vLinha, 10, 1), 1
55                  OraParameters.Remove "PMLOJA"
56                  OraParameters.Add "PMLOJA", 0, 3

57                  Criar_Cursor

                    '************************************
                    'VERIFICA PEDIDOS A SEREM CONSULTADOS
                    'Busca Pedido para verificar se j� existe alguma solicita��o de consulta para este
                    'mesmo pedido e que ainda n�o teve retorno.
                    '     Se existir sem retorno n�o grava
                    '     Se existir com retorno ent�o grava para que
                    '     possa ser consultado
                    '************************************
58                  'Gerar_Log "Verificando se existe pedido a ser consultado - PR_CONSULTA_RETORNO - Loja:" & Mid(vLinha, 1, 2) & ", PED:" & Mid(vLinha, 3, 7) & " - Seq:" & Mid(vLinha, 10, 1)

59                  V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_CONSULTA_RETORNO(:VCURSOR,:LOJA,:PED,:SEQ); END;"

60                  Set VarRec = OraParameters("vCursor").Value

61                  V_BANCO.Parameters.Remove "vCursor"

                    '*******************************
                    'GRAVA OS DADOS NA TABELA DE LOG
                    '*******************************
62                  If VarRec.EOF Then  '-- N�o Achou o Pedido sem Retorno, ent�o Inclui

63                      'Gerar_Log "N�o Achou o Pedido sem Retorno, ent�o Inclui - PR_GRAVA_RETORNO - Loja:" & Mid(vLinha, 1, 2) & ", PED:" & Mid(vLinha, 3, 7) & " - Seq:" & Mid(vLinha, 10, 1) & " - Email: " & vFrom & " - DtSol:" & CDate(Format(Left(vDate, 8), "SHORT DATE") & " " & Format(Mid(vDate, 10, 8), "LONG TIME"))

64                      OraParameters.Remove "LOJA"
65                      OraParameters.Add "LOJA", Mid(vLinha, 1, 2), 1
66                      OraParameters.Remove "PED"
67                      OraParameters.Add "PED", Mid(vLinha, 3, 7), 1
68                      OraParameters.Remove "SEQ"
69                      OraParameters.Add "SEQ", Mid(vLinha, 10, 1), 1
70                      OraParameters.Remove "email"
71                      OraParameters.Add "email", vFrom, 1
72                      OraParameters.Remove "dtsol"
73                      OraParameters.Add "dtsol", CDate(Format(Left(vDate, 8), "SHORT DATE") & " " & Format(Mid(vDate, 10, 8), "LONG TIME")), 1
74                      OraParameters.Remove "vErro"
75                      OraParameters.Add "vErro", 0, 2

76                      V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_FIL080.PR_GRAVA_RETORNO(:LOJA,:PED,:SEQ,:EMAIL,:DTSOL); END;"

77                      If OraParameters("vErro").Value <> 0 Then
78                          StayOnTop frmErros
79                          frmErros.lblDescricao = OraParameters("vErro") & vbCrLf & "LOJA:" & Mid(vLinha, 1, 2) & vbCrLf & "PED:" & Mid(vLinha, 3, 7) & vbCrLf & "SEQ:" & Mid(vLinha, 10, 1) & vbCrLf & "email:" & vFrom & vbCrLf & "dtsol:" & CDate(Format(Left(vDate, 8), "SHORT DATE") & " " & Format(Mid(vDate, 10, 8), "LONG TIME"))
80                          frmErros.lblSub = "Arquivo_Cons_Pedido"
81                          frmErros.lblLinha = "Fixa 83"
82                          Resume Next
83                      End If
84                  End If
85                  vLinha = ""
86                  Set VarRec = Nothing
87              Loop
88              Close #5

                '***************************************************************
                'Deleta o Arquivo da Rede que j� foi gravado na tabela do Oracle
                '***************************************************************
89              'Gerar_Log "Apagar arquivo: " & vNomeArquivo & "," & Mid(vNomeArquivo, 1, InStr(vNomeArquivo, ".")) & "hdr"

90              Kill txtEntradaPedido & vNomeArquivo
91              Kill txtEntradaPedido & Mid(vNomeArquivo, 1, InStr(vNomeArquivo, ".")) & "hdr"

92              vNomeArquivo = Dir(txtEntradaPedido & "*.CON")
93          End If
94      Loop
95    End If

    '***********************************************************************************************
    'J� gravou todos os arquivos que tinha na rede para a tabela do Oracle. Pode come�ar a consultar
    '***********************************************************************************************
96    Call CONS_PEDIDO

97    lblEnvia.Caption = "Aguardando Novas Consultas"

98    Screen.MousePointer = 0
99    Exit Sub

TrataErro:
100   If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
101     Resume
102   ElseIf Err = 76 Or Err = 53 Or Err = 52 Or Err = 75 Or Err = 55 Or Err = 70 Then
103     Resume Next
104   ElseIf Err.Description Like "%00001%" Then    '--> Unique Constraint
105     Resume Next
106   ElseIf Err.Number <> 0 Then
107     EnviarEmail "Sub Arquivo_Cons_pedido" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl & vbCrLf & "Loja:" & Mid(vLinha, 1, 2) & vbCrLf & "Pedido:" & Mid(vLinha, 3, 7) & vbCrLf & "Seq:" & Mid(vLinha, 10, 1)
108     gCod_Erro = Err.Number
109     gDesc_Erro = Err.Description
110     gNomeSub_Erro = "Arquivo_Cons_Pedido"
111     gLinha_Erro = Erl
        'WILLIAM LEITE - ACERTO PAAC
112     If vMsg <> "" Then
113        Close #5
114     End If
115   End If

End Sub

Sub Deletar(pNomeParam As String, pValorParam As String)

1   V_BANCO.Parameters.Remove "Cod_Soft": V_BANCO.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
2   V_BANCO.Parameters.Remove "Nome_Param": V_BANCO.Parameters.Add "Nome_Param", pNomeParam, 1
3   V_BANCO.Parameters.Remove "Dep": V_BANCO.Parameters.Add "DEP", "PRODUCAO.", 1
4   V_BANCO.Parameters.Remove "Txt_Erro": V_BANCO.Parameters.Add "Txt_Erro", "", 2

5   V_BANCO.executapl "PRODUCAO.PCK_VDA231.Pr_Deletar_Parametros(:Nome_Param, :Cod_Soft, :DEP, :Txt_Erro)"

6   If Trim(V_BANCO.Parameters("Txt_Erro")) <> "" Then
7       MsgBox "Descri��o do Erro:" & V_BANCO.Parameters("Txt_Erro")
8       Exit Sub
9   End If
End Sub

Public Sub Carregar_Parametros()

1   txtTempo = 0 + Val(Pegar_VL_Parametro("TEMPO"))
2   txtEntrada = "" & Pegar_VL_Parametro("ENTRADA")
3   txtEntradaPedido = "" & Pegar_VL_Parametro("ENTRADAPEDIDO")
4   txtSaida = "" & Pegar_VL_Parametro("SAIDA")
5   txtSaidaPedido = "" & Pegar_VL_Parametro("SAIDAPEDIDO")

End Sub

Public Function Pegar_Cod_Sistema()

1   V_BANCO.Parameters.Remove "Nome_Software"
2   V_BANCO.Parameters.Add "Nome_Software", "FIL080", 1

3   V_BANCO.Parameters.Remove "Cod_Software"
4   V_BANCO.Parameters.Add "Cod_Software", 0, 2

5   V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA231.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software); END;"

6   Pegar_Cod_Sistema = V_BANCO.Parameters("Cod_Software")

7   V_BANCO.Parameters.Remove "Cod_Software"

End Function

Public Sub Criar_Cursor()
1   V_BANCO.Parameters.Remove "vCursor"
2   V_BANCO.Parameters.Add "vCursor", 0, 2
3   V_BANCO.Parameters("vCursor").ServerType = ORATYPE_CURSOR
4   V_BANCO.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
5   V_BANCO.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub

Sub Gravar_Parametros(pNomeParam As String, pValorParam As String)

1   V_BANCO.Parameters.Remove "Cod_Soft": V_BANCO.Parameters.Add "Cod_Soft", Pegar_Cod_Sistema, 1
2   V_BANCO.Parameters.Remove "Nome_Param": V_BANCO.Parameters.Add "Nome_Param", UCase(pNomeParam), 1
3   V_BANCO.Parameters.Remove "VL_PARAM": V_BANCO.Parameters.Add "VL_PARAM", pValorParam, 1
4   V_BANCO.Parameters.Remove "Dep": V_BANCO.Parameters.Add "DEP", "helpdesk.", 1
5   V_BANCO.Parameters.Remove "Txt_Erro": V_BANCO.Parameters.Add "Txt_Erro", "", 2

6   V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA231.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :DEP, :Txt_Erro); END;"

End Sub

Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)

    Dim vObjOracle As Object

1   Criar_Cursor

2   V_BANCO.Parameters.Remove "Cod_Sistema"
3   V_BANCO.Parameters.Add "Cod_Sistema", Pegar_Cod_Sistema, 1

4   V_BANCO.Parameters.Remove "Nome_Parametro"
5   V_BANCO.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1

6   V_BANCO.Parameters.Remove "DEP"
7   V_BANCO.Parameters.Add "DEP", "HELPDESK.", 1

8   V_BANCO.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA231.PR_SELECT_VL_PARAMETRO(:vCursor, :Cod_Sistema, :Nome_Parametro, :DEP); END;"

9   Set vObjOracle = V_BANCO.Parameters("vCursor").Value
10  Pegar_VL_Parametro = IIf(IsNull(vObjOracle("VL_PARAMETRO")), "", vObjOracle("VL_PARAMETRO"))
11  Set vObjOracle = Nothing

12  V_BANCO.Parameters.Remove "vCursor"
End Function

Sub Montar_lista_Bancos()

    Dim rst As Object
1   Set rst = V_BANCO.CreateDynaset("SELECT * FROM TIPO_BANCO_CD order by cod_loja", 0&)
2   For i = 1 To rst.RecordCount
3       If Command$ = "L" Then
4           If rst!tp_banco = "M" Then
5               If rst!cod_loja <> 2 Then
6                   lblCds = lblCds & rst!cod_loja & ","
7               End If
8           End If
9       ElseIf Command$ = "R" Then
10          If rst!tp_banco = "U" Then
11              If rst!cod_loja <> 5 And rst!cod_loja <> 1 Then
12                  lblCds = lblCds & rst!cod_loja & ","
13              End If
14          End If
15      End If
16      rst.MoveNext
17  Next

18  rst.Close
19  Set rst = Nothing

20  If Command$ = "L" Then
21      lblCds = "CD's Locais: 1," & Left(lblCds, Len(lblCds) - 1)
22  ElseIf Command$ = "R" Then
23      lblCds = "CD's Remotos: " & Left(lblCds, Len(lblCds) - 1)
24  End If

End Sub

Private Sub txtTempo_KeyPress(KeyAscii As Integer)
1   If InStr(1, "0123456789", Chr(KeyAscii)) = 0 And KeyAscii <> 8 Then
2       KeyAscii = 0
3   End If
End Sub

