Attribute VB_Name = "Module3"
Option Explicit



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Copyright �1996-2001 VBnet, Randy Birch, All Rights Reserved.
' Some pages may also contain other copyrights by the author.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' You are free to use this code within your own applications,
' but you are expressly forbidden from selling or otherwise
' distributing this source code without prior written consent.
' This includes both posting free demo projects made from this
' code as well as reproducing the code in text or html format.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Const IP_SUCCESS As Long = 0
Private Const IP_STATUS_BASE As Long = 11000
Private Const IP_BUF_TOO_SMALL As Long = (11000 + 1)
Private Const IP_DEST_NET_UNREACHABLE As Long = (11000 + 2)
Private Const IP_DEST_HOST_UNREACHABLE As Long = (11000 + 3)
Private Const IP_DEST_PROT_UNREACHABLE As Long = (11000 + 4)
Private Const IP_DEST_PORT_UNREACHABLE As Long = (11000 + 5)
Private Const IP_NO_RESOURCES As Long = (11000 + 6)
Private Const IP_BAD_OPTION As Long = (11000 + 7)
Private Const IP_HW_ERROR As Long = (11000 + 8)
Private Const IP_PACKET_TOO_BIG As Long = (11000 + 9)
Private Const IP_REQ_TIMED_OUT As Long = (11000 + 10)
Private Const IP_BAD_REQ As Long = (11000 + 11)
Private Const IP_BAD_ROUTE As Long = (11000 + 12)
Private Const IP_TTL_EXPIRED_TRANSIT As Long = (11000 + 13)
Private Const IP_TTL_EXPIRED_REASSEM As Long = (11000 + 14)
Private Const IP_PARAM_PROBLEM As Long = (11000 + 15)
Private Const IP_SOURCE_QUENCH As Long = (11000 + 16)
Private Const IP_OPTION_TOO_BIG As Long = (11000 + 17)
Private Const IP_BAD_DESTINATION As Long = (11000 + 18)
Private Const IP_ADDR_DELETED As Long = (11000 + 19)
Private Const IP_SPEC_MTU_CHANGE As Long = (11000 + 20)
Private Const IP_MTU_CHANGE As Long = (11000 + 21)
Private Const IP_UNLOAD As Long = (11000 + 22)
Private Const IP_ADDR_ADDED As Long = (11000 + 23)
Private Const IP_GENERAL_FAILURE As Long = (11000 + 50)
Private Const MAX_IP_STATUS As Long = (11000 + 50)
Private Const IP_PENDING As Long = (11000 + 255)
Private Const PING_TIMEOUT As Long = 500
Private Const WS_VERSION_REQD As Long = &H101
Private Const MIN_SOCKETS_REQD As Long = 1
Private Const SOCKET_ERROR As Long = -1
Private Const INADDR_NONE As Long = &HFFFF
Private Const MAX_WSADescription As Long = 256
Private Const MAX_WSASYSStatus As Long = 128

Private Type ICMP_OPTIONS
    Ttl As Byte
    Tos As Byte
    Flags As Byte
    OptionsSize As Byte
    OptionsData As Long
End Type

Public Type ICMP_ECHO_REPLY
    Address As Long
    status As Long
    RoundTripTime As Long
    DataSize As Long    'formerly integer
    'Reserved        As Integer
    DataPointer As Long
    Options As ICMP_OPTIONS
    Data As String * 250
End Type

Private Type WSADATA
    wVersion As Integer
    wHighVersion As Integer
    szDescription(0 To MAX_WSADescription) As Byte
    szSystemStatus(0 To MAX_WSASYSStatus) As Byte
    wMaxSockets As Long
    wMaxUDPDG As Long
    dwVendorInfo As Long
End Type

Private Declare Function IcmpCreateFile Lib "icmp.dll" () As Long

Private Declare Function IcmpCloseHandle Lib "icmp.dll" _
                                         (ByVal IcmpHandle As Long) As Long

Private Declare Function IcmpSendEcho Lib "icmp.dll" _
                                      (ByVal IcmpHandle As Long, _
                                       ByVal DestinationAddress As Long, _
                                       ByVal RequestData As String, _
                                       ByVal RequestSize As Long, _
                                       ByVal RequestOptions As Long, _
                                       ReplyBuffer As ICMP_ECHO_REPLY, _
                                       ByVal ReplySize As Long, _
                                       ByVal Timeout As Long) As Long

Private Declare Function WSAGetLastError Lib "WSOCK32.DLL" () As Long

Private Declare Function WSAStartup Lib "WSOCK32.DLL" _
                                    (ByVal wVersionRequired As Long, _
                                     lpWSADATA As WSADATA) As Long

Private Declare Function WSACleanup Lib "WSOCK32.DLL" () As Long

Private Declare Function gethostname Lib "WSOCK32.DLL" _
                                     (ByVal szHost As String, _
                                      ByVal dwHostLen As Long) As Long

Private Declare Function gethostbyname Lib "WSOCK32.DLL" _
                                       (ByVal szHost As String) As Long

Private Declare Sub CopyMemory Lib "kernel32" _
                               Alias "RtlMoveMemory" _
                               (xDest As Any, _
                                xSource As Any, _
                                ByVal nbytes As Long)

Private Declare Function inet_addr Lib "WSOCK32.DLL" _
                                   (ByVal s As String) As Long

Public Function GetStatusCode(status As Long) As String

    Dim msg As String

1   Select Case status
    Case IP_SUCCESS: msg = "ip success"
2   Case INADDR_NONE: msg = "inet_addr: bad IP format"
3   Case IP_BUF_TOO_SMALL: msg = "ip buf too_small"
4   Case IP_DEST_NET_UNREACHABLE: msg = "ip dest net unreachable"
5   Case IP_DEST_HOST_UNREACHABLE: msg = "ip dest host unreachable"
6   Case IP_DEST_PROT_UNREACHABLE: msg = "ip dest prot unreachable"
7   Case IP_DEST_PORT_UNREACHABLE: msg = "ip dest port unreachable"
8   Case IP_NO_RESOURCES: msg = "ip no resources"
9   Case IP_BAD_OPTION: msg = "ip bad option"
10  Case IP_HW_ERROR: msg = "ip hw_error"
11  Case IP_PACKET_TOO_BIG: msg = "ip packet too_big"
12  Case IP_REQ_TIMED_OUT: msg = "ip req timed out"
13  Case IP_BAD_REQ: msg = "ip bad req"
14  Case IP_BAD_ROUTE: msg = "ip bad route"
15  Case IP_TTL_EXPIRED_TRANSIT: msg = "ip ttl expired transit"
16  Case IP_TTL_EXPIRED_REASSEM: msg = "ip ttl expired reassem"
17  Case IP_PARAM_PROBLEM: msg = "ip param_problem"
18  Case IP_SOURCE_QUENCH: msg = "ip source quench"
19  Case IP_OPTION_TOO_BIG: msg = "ip option too_big"
20  Case IP_BAD_DESTINATION: msg = "ip bad destination"
21  Case IP_ADDR_DELETED: msg = "ip addr deleted"
22  Case IP_SPEC_MTU_CHANGE: msg = "ip spec mtu change"
23  Case IP_MTU_CHANGE: msg = "ip mtu_change"
24  Case IP_UNLOAD: msg = "ip unload"
25  Case IP_ADDR_ADDED: msg = "ip addr added"
26  Case IP_GENERAL_FAILURE: msg = "ip general failure"
27  Case IP_PENDING: msg = "ip pending"
28  Case PING_TIMEOUT: msg = "ping timeout"
29  Case Else: msg = "unknown  msg returned"
30  End Select

31  GetStatusCode = CStr(status) & "   [ " & msg & " ]"

End Function

Public Function Ping(sAddress As String, _
                     sDataToSend As String, _
                     ECHO As ICMP_ECHO_REPLY) As Long

'If Ping succeeds :
'.RoundTripTime = time in ms for the ping to complete,
'.Data is the data returned (NULL terminated)
'.Address is the Ip address that actually replied
'.DataSize is the size of the string in .Data
'.Status will be 0
'
'If Ping fails .Status will be the error code

    Dim hPort As Long
    Dim dwAddress As Long

    'convert the address into a long representation
1   dwAddress = inet_addr(sAddress)

    'if a valid address..
2   If dwAddress <> INADDR_NONE Then

        'open a port
3       hPort = IcmpCreateFile()

        'and if successful,
4       If hPort Then

            'ping it.
5           Call IcmpSendEcho(hPort, _
                              dwAddress, _
                              sDataToSend, _
                              Len(sDataToSend), _
                              0, _
                              ECHO, _
                              Len(ECHO), _
                              PING_TIMEOUT)

            'return the status as ping succes and close
6           Ping = ECHO.status
7           Call IcmpCloseHandle(hPort)

8       End If

    Else:
        'the address format was probably invalid
9       Ping = INADDR_NONE

10  End If

End Function

Public Sub SocketsCleanup()

1   If WSACleanup() <> 0 Then
2       MsgBox "Windows Sockets error occurred in Cleanup.", vbExclamation
3   End If

End Sub

Public Function SocketsInitialize() As Boolean

    Dim WSAD As WSADATA

1   SocketsInitialize = WSAStartup(WS_VERSION_REQD, WSAD) = IP_SUCCESS

End Function

