VERSION 5.00
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmGraf2 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Faturas e Pagamentos"
   ClientHeight    =   7380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11985
   Icon            =   "frmGraf2.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   492
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   799
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FF00&
      ForeColor       =   &H80000008&
      Height          =   165
      Left            =   3960
      ScaleHeight     =   135
      ScaleWidth      =   135
      TabIndex        =   5
      Top             =   150
      Width           =   165
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FF0000&
      ForeColor       =   &H80000008&
      Height          =   165
      Left            =   3960
      ScaleHeight     =   135
      ScaleWidth      =   135
      TabIndex        =   4
      Top             =   360
      Width           =   165
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   11865
      _ExtentX        =   20929
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGraf2.frx":23D2
      PICN            =   "frmGraf2.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin GraphLib.Graph Graph1 
      Height          =   6390
      Left            =   60
      TabIndex        =   2
      Top             =   930
      Width           =   11865
      _Version        =   65536
      _ExtentX        =   20929
      _ExtentY        =   11271
      _StockProps     =   96
      BorderStyle     =   1
      Background      =   0
      Foreground      =   15
      Palette         =   1
      PatternedLines  =   1
      RandomData      =   1
      ThickLines      =   0
      ColorData       =   0
      ExtraData       =   0
      ExtraData[]     =   0
      FontFamily      =   4
      FontFamily[0]   =   2
      FontFamily[1]   =   2
      FontSize        =   4
      FontSize[0]     =   70
      FontSize[1]     =   60
      FontSize[2]     =   80
      FontSize[3]     =   70
      FontStyle       =   4
      GraphData       =   0
      GraphData[]     =   0
      LabelText       =   0
      LegendText      =   0
      PatternData     =   0
      SymbolData      =   0
      XPosData        =   0
      XPosData[]      =   0
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   525
      Left            =   4170
      TabIndex        =   3
      Top             =   150
      Width           =   4125
   End
End
Attribute VB_Name = "frmGraf2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public vCodCliente As String

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Dim vRegistrosF As Integer
    Dim vRegistrosP As Integer
    Dim i As Integer
    Dim ii As Integer
    Dim vFaturasPagtos(1, 100) As String 'Impares Faturas / Pares Pagtos
    Dim vColunas As Byte
    Me.Visible = True
    
'FATURAS
    frmGraf2.Caption = frmGraf2.Caption & " Cliente: " & vCodCliente

    db.Parameters.Remove "cli"
    db.Parameters.Add "cli", Val(vCodCliente), 1

    db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Para_Grafico(:vCursor, :Cli); END;"

    Set OraDynaset = db.Parameters("vCursor").Value
    Label1 = "Faturas Acumuladas por M�s"

    vRegistrosF = OraDynaset.RecordCount

    For i = 1 To vRegistrosF * 2 Step 2
        vFaturasPagtos(0, i) = OraDynaset!mes
        vFaturasPagtos(1, i) = OraDynaset!vl
        OraDynaset.MoveNext
    Next
  
'PAGAMENTOS
    db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Grafico_Pagto(:vCursor, :Cli); END;"

    Set OraDynaset = db.Parameters("vCursor").Value

    Me.Label1 = Label1.Caption & vbCrLf & "Pagamentos Acumulados por M�s"

    vRegistrosP = OraDynaset.RecordCount
    
    For i = 2 To vRegistrosP * 2 Step 2
        For ii = 1 To 99
            If vFaturasPagtos(0, ii) = OraDynaset!mes Then
                vFaturasPagtos(0, ii + 1) = OraDynaset!mes
                vFaturasPagtos(1, ii + 1) = OraDynaset!vl
                GoTo proximo
            End If
        Next
proximo:
        OraDynaset.MoveNext
    Next

    For i = 1 To 99
        If vFaturasPagtos(0, i) = "" And vFaturasPagtos(0, i + 1) = "" Then
            vColunas = i
            Exit For
        End If
    Next

    Graph1.NumPoints = vColunas - 1
    Graph1.GraphStyle = 0
    
    Dim Vertical As String
    For i = 1 To 100
        If vFaturasPagtos(1, i) <> "" Then
            Graph1.ThisPoint = i
            Graph1.GraphData = vFaturasPagtos(1, i)
            Graph1.Label(i) = Right(vFaturasPagtos(0, i), 2) & "/" & Mid(vFaturasPagtos(0, i), 3, 2)
                       
            If i Mod 2 = 0 Then
                Graph1.ThisPoint = i
                Graph1.ColorData = 9 'AZUL
            Else
                Graph1.ThisPoint = i
                Graph1.ColorData = 10 'VERDE
            End If
        End If
    Next
    Graph1.DrawMode = 2
    Graph1.Visible = True
    
End Sub
