VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmDocto 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Documento"
   ClientHeight    =   1050
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1050
   ScaleWidth      =   3765
   StartUpPosition =   3  'Windows Default
   Begin Bot�o.cmd cmdOk 
      Default         =   -1  'True
      Height          =   345
      Left            =   600
      TabIndex        =   1
      Top             =   660
      Width           =   1245
      _ExtentX        =   2196
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "OK"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDocto.frx":0000
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox txtDocto 
      Height          =   315
      Left            =   90
      MaxLength       =   32
      TabIndex        =   0
      Top             =   270
      Width           =   3645
   End
   Begin Bot�o.cmd cmdCancelar 
      Cancel          =   -1  'True
      Height          =   345
      Left            =   1920
      TabIndex        =   3
      Top             =   660
      Width           =   1245
      _ExtentX        =   2196
      _ExtentY        =   609
      BTYPE           =   3
      TX              =   "Cancelar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmDocto.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Informe o N�mero do Documento sem os s�mbolos"
      Height          =   255
      Left            =   90
      TabIndex        =   2
      Top             =   30
      Width           =   3675
   End
End
Attribute VB_Name = "frmDocto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public vCodCli As String

Private Sub cmdCancelar_Click()
    Unload Me
End Sub

Private Sub cmdOk_Click()
    If txtDocto = "" Then
       MsgBox "Campo de preenchimento obrigat�rio !", vbCritical, "Aten��o"
       Me.txtDocto.SetFocus
       Exit Sub
    End If
    
    frmSerasa.Documento = txtDocto
    
    Salvar
    
    Unload Me
    
End Sub

Private Sub Form_Activate()
    txtDocto.SetFocus
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
End Sub

Private Sub txtDocto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 95 Then
        KeyAscii = 0
    End If
End Sub

Sub Salvar()
          Dim Data As String
          Dim SalvarEm As String
          Dim fs As New FileSystemObject
        
          Dim SalvarComNome As String
          'tonolli
    
    On Error GoTo erro:
         If Not fs.FolderExists(Caminho & "Serasa") Then
             fs.CreateFolder Caminho & "Serasa"
         End If
         'If Not fs.FolderExists(Caminho & "\Serasa") Then
         '    fs.CreateFolder Caminho & "\Serasa"
         'End If

            If Not fs.FolderExists(Caminho & "\Equifax") Then
             fs.CreateFolder Caminho & "\Equifax"
         End If
             
         Set fs = Nothing
         
         Data = Format(Date, "YYYYMMDD")
         If frmSerasa.Pagina = "Serasa" Then
           'SalvarEm = Caminho & "\Serasa\"
           SalvarEm = Caminho & "Serasa\"
           SalvarComNome = "S_" & Format(vCodCli, "000000") & "_" & frmSerasa.Documento & "_" & Data & ".htm"
        Else
           SalvarEm = Caminho & "\EquiFax\"
           SalvarComNome = "E_" & Format(vCodCli, "000000") & "_" & frmSerasa.Documento & "_" & Data & ".htm"
        End If
          'tonolli
          
        If fs.FileExists(App.Path & "\" & SalvarComNome) Then
            fs.DeleteFile App.Path & "\" & SalvarComNome
        End If
          
        SendKeys ("{ENTER}")
        frmSerasa.Browse.ExecWB OLECMDID_SAVEAS, OLECMDEXECOPT_DONTPROMPTUSER, App.Path & "\" & SalvarComNome
          
        If fs.FileExists(SalvarEm & SalvarComNome) Then
            fs.DeleteFile SalvarEm & SalvarComNome
        End If
          
        fs.MoveFile App.Path & "\" & SalvarComNome, SalvarEm
          
          'TONOLLI
erro:
        
        If Err.Number <> 0 Then
            MsgBox "Sub Salvar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
        End If

'        Grava_Log ("ERRO: " & Err.Number & " - " & Err.Description & " - " & Date)
        
End Sub

Sub Grava_Log(Mensagem)

Dim sNameArq As String
Dim sNovoArq As String

    sNameArq = App.Path & "\LOGSERASA.TXT"
    
    sNovoArq = Dir(sNameArq)
    
    If sNovoArq <> "" Then
        
        Open sNameArq For Append As #1
        
        Print #1, Format(Date, "DDMMYY") & Mid(Mensagem, 1, 80)

        Close #1
        
    End If

End Sub

