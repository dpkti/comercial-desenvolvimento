VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form FRMMSG 
   Caption         =   "Mensagens de Cr�dito"
   ClientHeight    =   4200
   ClientLeft      =   1485
   ClientTop       =   2025
   ClientWidth     =   6855
   LinkTopic       =   "Form1"
   ScaleHeight     =   4200
   ScaleWidth      =   6855
   Begin Bot�o.cmd SSCommand2 
      Default         =   -1  'True
      Height          =   675
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "Fechar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMMSG.frx":0000
      PICN            =   "FRMMSG.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   6720
      _ExtentX        =   11853
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin MSGrid.Grid GRD_msg 
      Height          =   3015
      Left            =   0
      TabIndex        =   2
      Top             =   840
      Width           =   6735
      _Version        =   65536
      _ExtentX        =   11880
      _ExtentY        =   5318
      _StockProps     =   77
      ForeColor       =   16711680
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "FRMMSG.frx":0CF6
   End
End
Attribute VB_Name = "FRMMSG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub GRD_CANC_RowColChange()

End Sub

Private Sub Form_Load()

Dim linha As Long
On Error GoTo trata_erro

         Screen.MousePointer = 11
         DoEvents
      
         db.Parameters.Remove "cod_errora"
         db.Parameters.Add "cod_errora", 0, 2
         
         db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_MSG(:vCursor, :cod_errora); END;"
         Set OraDynaset = db.Parameters("vCursor").Value


         If Val(db.Parameters("cod_errora")) <> 0 Then
           MsgBox "Ocorreu o erro: " & Val(db.Parameters("cod_errora")) & " por favor verifique", vbInformation, "Aten��o"
           Screen.MousePointer = 0
           Exit Sub
         Else
            FRMMSG.GRD_msg.Rows = OraDynaset.RecordCount + 2
            FRMMSG.GRD_msg.Cols = 1
            GRD_msg.Row = 0
            GRD_msg.COL = 0
            GRD_msg.ColWidth(0) = 8000
            GRD_msg.Text = "MENSAGEM"
            linha = 1
            GRD_msg.Row = linha
            GRD_msg.COL = 0
            GRD_msg.Text = ""
            Do While Not OraDynaset.EOF()
              DoEvents
               linha = linha + 1
              GRD_msg.Row = linha
              GRD_msg.COL = 0
              GRD_msg.Text = OraDynaset.Fields("mensagem")
              
              OraDynaset.MoveNext
            Loop
            
         End If

        Screen.MousePointer = 0

trata_erro:
        If Err.Number <> 0 Then
            MsgBox "Sub CmdIndent_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
        End If
End Sub

Private Sub GRD_msg_DblClick()
  frminfo_ger.lblobs.Caption = GRD_msg.Text
  frminfo_ger.txtResp = strNome_usuario
  If Trim(frminfo_ger.lblobs.Caption) <> "" Then
    frminfo_ger.lbldt_mens = data_faturamento
  Else
     frminfo_ger.lbldt_mens = ""
      frminfo_ger.txtResp = ""
  End If
  Unload Me
End Sub

Private Sub SSCommand2_Click()
  Unload Me
End Sub
