VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmBolop 
   Caption         =   "Libera��o A VISTA"
   ClientHeight    =   1680
   ClientLeft      =   3915
   ClientTop       =   2355
   ClientWidth     =   4140
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1680
   ScaleWidth      =   4140
   StartUpPosition =   2  'CenterScreen
   Begin Threed.SSFrame SSFrame1 
      Height          =   765
      Left            =   60
      TabIndex        =   0
      Top             =   840
      Width           =   3975
      _Version        =   65536
      _ExtentX        =   7011
      _ExtentY        =   1349
      _StockProps     =   14
      Caption         =   "Forma de Pagamento"
      ForeColor       =   255
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.OptionButton Option3 
         Appearance      =   0  'Flat
         Caption         =   "NENHUM"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2280
         TabIndex        =   6
         Top             =   360
         Width           =   1425
      End
      Begin VB.OptionButton Option1 
         Appearance      =   0  'Flat
         Caption         =   " Boleto"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   90
         TabIndex        =   2
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton Option2 
         Appearance      =   0  'Flat
         Caption         =   " O.P."
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1290
         TabIndex        =   1
         Top             =   360
         Width           =   825
      End
   End
   Begin Bot�o.cmd Command1 
      Height          =   675
      Left            =   810
      TabIndex        =   3
      ToolTipText     =   "Confirmar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmbolop.frx":0000
      PICN            =   "Frmbolop.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVoltar 
      Default         =   -1  'True
      Height          =   675
      Left            =   30
      TabIndex        =   4
      ToolTipText     =   "Fechar"
      Top             =   0
      Visible         =   0   'False
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmbolop.frx":0CF6
      PICN            =   "Frmbolop.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   5
      Top             =   750
      Width           =   2250
      _ExtentX        =   3969
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
End
Attribute VB_Name = "frmBolop"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdVoltar_Click()
'    Command1_Click
'    Unload Me
End Sub

Private Sub Command1_Click()
          
1         On Error GoTo Trata_Erro
          
          Dim SEL As String
          
          If Option3.Value = True Then
            Unload Me
          End If
          

2         If Plano_Pagamento = 412 And Option1.Value = True Then
3             MsgBox "Para o Plano 412, N�o pode ser Boleto!"
4             Exit Sub
5         End If

          If Plano_Pagamento = 157 And (Option1.Value = True Or Option2.Value = True) Then
             MsgBox "Para o Plano 157, N�o pode ser Boleto nem OP!"
             Exit Sub
         End If

6         If Option1.Value = True Then
7             OPT = "B"
8             SIT = 0
          ElseIf Option2.Value = True Then
              OPT = "O"
11            SIT = 0
9         Else
10            Exit Sub
12        End If

13        db.Parameters.Remove "LOJA"
14        db.Parameters.Remove "PED"
15        db.Parameters.Remove "SEQ"
16        db.Parameters.Remove "Deposito"

17        db.Parameters.Add "LOJA", frmlibera.lbldep, 1
18        db.Parameters.Add "PED", frmlibera.lblped, 1
19        db.Parameters.Add "SEQ", frmlibera.lblseq, 1
20        db.Parameters.Add "Deposito", Format(CStr(deposito), "00"), 1
          
21        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Boleto_OP(:vCursor, :Deposito, :Loja, :Ped, :Seq); END;"

22        Set dbOra = db.Parameters("vCursor").Value

23        If Not dbOra.EOF Then 'UPDATE

24            db.Parameters.Remove "DEPOSITO"
25            db.Parameters.Remove "LOJA"
26            db.Parameters.Remove "PED"
27            db.Parameters.Remove "SEQ"
28            db.Parameters.Remove "OPT"
29            db.Parameters.Remove "COD_ERRORA"
30            db.Parameters.Remove "TXT_ERRORA"

31            db.Parameters.Add "DEPOSITO", Format(CStr(deposito), "00"), 1
32            db.Parameters.Add "LOJA", frmlibera.lbldep, 1
33            db.Parameters.Add "PED", frmlibera.lblped, 1
34            db.Parameters.Add "SEQ", frmlibera.lblseq, 1
35            db.Parameters.Add "OPT", OPT, 1
36            db.Parameters.Add "COD_ERRORA", 0, 2
37            db.Parameters.Add "TXT_ERRORA", "", 2

38            db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Update_Cobranca_Boleto_OP(:Deposito,:OPT,:LOJA,:PED,:SEQ, :COD_ERRORA, :TXT_ERRORA);END;"
              
39            If Val(db.Parameters("COD_ERRORA")) <> 0 Then
                  If db.Parameters("TXT_ERRORA") Like "*BOLETO/OP NAO PERMITIDO*" Then
                    Unload Me
                  Else
40                  MsgBox db.Parameters("COD_ERRORA") & " - " & db.Parameters("TXT_ERRORA")
41                  Exit Sub
                  End If
42            End If

43        Else                       'INSERT

44            db.Parameters.Remove "DEPOSITO"
45            db.Parameters.Remove "LOJA"
46            db.Parameters.Remove "PED"
47            db.Parameters.Remove "SEQ"
48            db.Parameters.Remove "OPT"
49            db.Parameters.Remove "SIT"
50            db.Parameters.Remove "cod_errora"
51            db.Parameters.Remove "txt_errora"

52            db.Parameters.Add "DEPOSITO", Format(CStr(deposito), "00"), 1
53            db.Parameters.Add "LOJA", frmlibera.lbldep, 1
54            db.Parameters.Add "PED", frmlibera.lblped, 1
55            db.Parameters.Add "SEQ", frmlibera.lblseq, 1
56            db.Parameters.Add "SIT", SIT, 1
57            db.Parameters.Add "OPT", OPT, 1
58            db.Parameters.Add "cod_errora", 0, 2
59            db.Parameters.Add "txt_errora", "", 2

60            db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Insert_Cobranca_Boleto_OP(:Deposito, :Loja, :Ped, :Seq, :Sit, :Opt, :Cod_Errora, :Txt_Errora); END;"

61            If Val(db.Parameters("COD_ERRORA")) <> 0 Then
62                If db.Parameters("TXT_ERRORA") Like "*BOLETO/OP NAO PERMITIDO*" Then
                    Unload Me
                  Else
                  MsgBox db.Parameters("COD_ERRORA") & " - " & db.Parameters("TXT_ERRORA")
                  Exit Sub
                  End If
64            End If

65        End If
66        Unload Me

Trata_Erro:
    If Err.Number <> 0 Then
        If Err.Description Like "*BOLETO/OP NAO PERMITIDO*" Then
           Unload Me
        Else
          MsgBox "Sub: Command1_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
        End If
    End If

End Sub

Private Sub Form_Load()
  If Plano_Pagamento = 157 Then
    Unload Me
  End If
End Sub

