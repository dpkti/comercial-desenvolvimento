VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "ieframe.dll"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSerasa 
   Caption         =   "Consultar"
   ClientHeight    =   5820
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9765
   LinkTopic       =   "Form1"
   ScaleHeight     =   5820
   ScaleWidth      =   9765
   WindowState     =   2  'Maximized
   Begin VB.ComboBox cboSite 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   360
      ItemData        =   "frmSerasa.frx":0000
      Left            =   0
      List            =   "frmSerasa.frx":000A
      TabIndex        =   3
      Top             =   5040
      Visible         =   0   'False
      Width           =   9510
   End
   Begin SHDocVwCtl.WebBrowser Browse 
      Height          =   3900
      Left            =   30
      TabIndex        =   0
      Top             =   60
      Width           =   7215
      ExtentX         =   12726
      ExtentY         =   6879
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   780
      Left            =   0
      TabIndex        =   1
      Top             =   4200
      Visible         =   0   'False
      Width           =   9645
      _ExtentX        =   17013
      _ExtentY        =   1376
      ButtonWidth     =   1296
      ButtonHeight    =   1376
      Style           =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   14
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Serasa"
            Key             =   "Serasa"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "EquiFax"
            Key             =   "EquiFax"
            ImageIndex      =   5
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Salvar"
            Key             =   "Salvar"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Listagem"
            Key             =   "Listagem"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Parar"
            Key             =   "Parar"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Refresh"
            Key             =   "Refresh"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Voltar"
            Key             =   "Voltar"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Pr�ximo"
            Key             =   "Pr�ximo"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Sair"
            Key             =   "Sair"
            ImageIndex      =   4
         EndProperty
      EndProperty
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   7470
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   9
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":0035
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":090F
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":6671
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":67CB
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":CA65
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":D33F
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":DC19
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":E4F3
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "frmSerasa.frx":E945
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.CommandButton Command1 
         BackColor       =   &H00E0E0E0&
         Caption         =   "Navigate"
         Default         =   -1  'True
         Height          =   315
         Left            =   8760
         TabIndex        =   2
         Top             =   180
         Visible         =   0   'False
         Width           =   1395
      End
   End
End
Attribute VB_Name = "frmSerasa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmSerasa
' Author    : c.samuel.oliveira
' Date      : 17/11/16
' Purpose   : TI-5021
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmSerasa
' Author    : c.samuel.oliveira
' Date      : 22/02/16
' Purpose   : TI-4188
'---------------------------------------------------------------------------------------

Option Explicit
Public Documento As String
Public Pagina As String
 
Private Sub Browse_DocumentComplete(ByVal pDisp As Object, URL As Variant)
'    If UCase("https://sitenet06.serasa.com.br/novorelato/PJNovoRelatoPrincipal") = UCase(URL) Then
'        Toolbar1.Buttons(5).Enabled = True
'    ElseIf InStr(1, UCase(URL), "CGC1") Then
'        Toolbar1.Buttons(5).Enabled = True
'    Else
'        Toolbar1.Buttons(5).Enabled = False
'    End If
End Sub

Private Sub cboSite_Click()
    Command1_Click
End Sub


Private Sub Command1_Click()
    Browse.Navigate cboSite
End Sub

Private Sub Form_Load()
    'TI-5021
    Dim vCPFCNPJ As String
    Dim vTpDocto As Byte
    Dim vSite As String
    
    'Caminho = Pegar_VL_Parametro("CAMINHOSERASA")
    'Browse.Navigate "ABOUT:BLANK"
    
    db.Parameters.Remove "cli"
    db.Parameters.Add "cli", Val(frmDocto.vCodCli), 1
    db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Cliente(:vCursor, :Cli); END;"
    Set OraDynaset = db.Parameters("vCursor").Value
    
    If Not OraDynaset.BOF And Not OraDynaset.EOF Then
        vCPFCNPJ = IIf(IsNull(OraDynaset.Fields("cgc")), "", OraDynaset.Fields("cgc"))
        vTpDocto = IIf(IsNull(OraDynaset.Fields("Tp_Docto")), "", OraDynaset.Fields("Tp_Docto"))
    End If
    
    If vTpDocto = 1 Then
        vSite = "http://dpaonline.dpaschoal.com.br/credito/Relatorio_Consulta_Credito.asp?acesso_direto_sistemas=0&vciccgc=" & vCPFCNPJ & "&rel=3&PID_USUARIO=MARICI.QUITERIO"
    Else
        vSite = "http://dpaonline.dpaschoal.com.br/credito/Relatorio_Consulta_Credito.asp?acesso_direto_sistemas=0&vciccgc=" & vCPFCNPJ & "&rel=2&PID_USUARIO=MARICI.QUITERIO"
    End If
    Browse.Navigate vSite
    'FIM TI-5021
End Sub

Private Sub Form_Resize()
Dim altura As Integer

    Browse.Width = Me.Width - 200
altura = Me.Height '- (Toolbar1.Height) - 1000
If altura <= 0 Then altura = 1


    Browse.Height = altura
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
        Case "Serasa"
            Pagina = "Serasa"
            'cboSite = "https://sitenet11.serasa.com.br"
            cboSite = "" & Pegar_VL_Parametro("SITESERASA") 'TI-4188
            Browse.Navigate cboSite.Text
        
        Case "EquiFax"
            Pagina = "Equifax"
            cboSite = "www.equifax.com.br"
            Browse.Navigate cboSite.Text
        
        Case "Salvar"
            frmDocto.Show 1
            
        Case "Listagem"
            frmConsulta.Show 1
            
        Case "Parar"
            Browse.Stop
        
        Case "Refresh"
            Browse.Refresh
        
        Case "Voltar"
            Browse.GoBack
            
        Case "Pr�ximo"
            Browse.GoForward
        
        Case "Sair"
            Unload Me
            
    End Select
End Sub
