VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form FRMREF_BANCA 
   Caption         =   "REFER�NCIAS BANC�RIAS"
   ClientHeight    =   6885
   ClientLeft      =   1590
   ClientTop       =   1635
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6885
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin MSGrid.Grid Grid1 
      Height          =   2535
      Left            =   30
      TabIndex        =   5
      Top             =   4320
      Width           =   11805
      _Version        =   65536
      _ExtentX        =   20823
      _ExtentY        =   4471
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
   End
   Begin MSGrid.Grid GRD_BCO 
      Height          =   3195
      Left            =   30
      TabIndex        =   3
      Top             =   1080
      Width           =   11805
      _Version        =   65536
      _ExtentX        =   20823
      _ExtentY        =   5636
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Cols            =   8
      FixedCols       =   0
      MouseIcon       =   "Frmrefb.frx":0000
   End
   Begin Bot�o.cmd cmdfat 
      Height          =   615
      Left            =   3240
      TabIndex        =   6
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":001C
      PICN            =   "Frmrefb.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmddup 
      Height          =   615
      Left            =   3960
      TabIndex        =   7
      ToolTipText     =   "Duplicatas"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":0912
      PICN            =   "Frmrefb.frx":092E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdtitav 
      Height          =   615
      Left            =   4680
      TabIndex        =   8
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":0C48
      PICN            =   "Frmrefb.frx":0C64
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdrcom 
      Height          =   615
      Left            =   6840
      TabIndex        =   9
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":0F7E
      PICN            =   "Frmrefb.frx":0F9A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   615
      Left            =   7560
      TabIndex        =   10
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":12B4
      PICN            =   "Frmrefb.frx":12D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand1 
      Height          =   615
      Left            =   30
      TabIndex        =   11
      ToolTipText     =   "Sair"
      Top             =   30
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":1BAA
      PICN            =   "Frmrefb.frx":1BC6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   12
      Top             =   690
      Width           =   11790
      _ExtentX        =   20796
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   615
      Left            =   9090
      TabIndex        =   13
      ToolTipText     =   "Imprimir"
      Top             =   30
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":28A0
      PICN            =   "Frmrefb.frx":28BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand6 
      Height          =   615
      Left            =   6120
      TabIndex        =   14
      ToolTipText     =   "Duplicatas Canceladas"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":3596
      PICN            =   "Frmrefb.frx":35B2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdinfo 
      Height          =   615
      Left            =   2520
      TabIndex        =   15
      ToolTipText     =   "Informa��es"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":38CC
      PICN            =   "Frmrefb.frx":38E8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdpag 
      Height          =   615
      Left            =   5400
      TabIndex        =   16
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":3D3A
      PICN            =   "Frmrefb.frx":3D56
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSci 
      Height          =   615
      Left            =   9810
      TabIndex        =   17
      Top             =   30
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "SCI Serasa"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":4070
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultarSCI 
      Height          =   615
      Left            =   10530
      TabIndex        =   18
      Tag             =   "1"
      ToolTipText     =   "Consultar SCI/SERASA j� Gravados"
      Top             =   30
      Visible         =   0   'False
      Width           =   1305
      _ExtentX        =   2302
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "SCI Serasa"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":408C
      PICN            =   "Frmrefb.frx":40A8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCA 
      Height          =   615
      Left            =   8280
      TabIndex        =   19
      Top             =   0
      Width           =   795
      _ExtentX        =   1402
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "Agenda CA"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefb.frx":4D82
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Line Line1 
      X1              =   1050
      X2              =   1170
      Y1              =   870
      Y2              =   870
   End
   Begin VB.Label Label4 
      Caption         =   "Label4"
      Height          =   15
      Left            =   1050
      TabIndex        =   4
      Top             =   870
      Width           =   135
   End
   Begin VB.Label lblcli 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   30
      TabIndex        =   2
      Top             =   750
      Width           =   975
   End
   Begin VB.Label lblnome 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1200
      TabIndex        =   1
      Top             =   750
      Width           =   9045
   End
   Begin VB.Label lbldata 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   10290
      TabIndex        =   0
      Top             =   750
      Width           =   1545
   End
End
Attribute VB_Name = "FRMREF_BANCA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCA_Click()

    On Error GoTo Trata_Erro

1        If ESCOLHA = 0 Or ESCOLHA = 7 Then
2           lngCOD_CLIENTE = w_cod
             
3         Else
4            lngCOD_CLIENTE = frmlibera.lblcod_cli
5         End If

6         frmCA.Show 1

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdCa_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdConsultarSCI_Click()
    frmConsulta.vCodCli = lblcli
    frmConsulta.Show
End Sub

Private Sub cmddup_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If

7         Unload Me

8         frmavencer.Show
9         frmavencer.SetFocus
10        frmavencer.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdDup_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmddup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "T�tulos a Vencer"
End Sub


Private Sub cmdfat_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If
7         Unload Me
8         frmfatura.Show
9         frmfatura.SetFocus
10        frmfatura.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CdmFat_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdfat_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Faturas"

End Sub


Private Sub cmdinfo_Click()
    tela_sumir = tela_atual

    Unload Me
    frminfo_ger.Show
    frminfo_ger.SetFocus
    frminfo_ger.WindowState = 2
End Sub

Private Sub cmdinfo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Informa��es Gerais"

End Sub


Private Sub cmdpag_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If
7         Unload Me

8         FRMPAGAM.Show
9         FRMPAGAM.SetFocus
10        FRMPAGAM.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdPag_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdpag_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Pagamentos"
End Sub


Private Sub cmdrcom_Click()
    tela_sumir = tela_atual

    Unload Me

    FRMREF_COM.Show
    FRMREF_COM.SetFocus
    FRMREF_COM.WindowState = 2

    If Fl_Registros = "S" Then
        FRMREF_COM.Show
        FRMREF_COM.SetFocus
        FRMREF_COM.WindowState = 2
    End If
    If Fl_Registros = "N" Then
        Unload FRMREF_COM
    End If
End Sub

Private Sub cmdrcom_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Refer�ncias Comerciais"

End Sub


Private Sub cmdSci_Click()
    frmDocto.vCodCli = lblcli
    frmSerasa.Show
End Sub

Private Sub cmdtitav_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If

7         Unload Me

8         frmvencido.Show
9         frmvencido.SetFocus
10        frmvencido.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdTitAv_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdtitav_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Titulos Vencidos"
End Sub


Private Sub Form_Load()

    On Error GoTo Trata_Erro
          
1         Me.Top = ((MDIForm1.Height - Me.Height) / 2) - MDIForm1.Picture1.Height
2         Me.Left = (MDIForm1.Width - Me.Width) / 2
          
3         If ESCOLHA = 0 Or ESCOLHA = 7 Then
4             FRMREF_BANCA.lblcli = w_cod
5             FRMREF_BANCA.lblnome = w_cli
6         Else
7             FRMREF_BANCA.lblcli = frmlibera.lblcod_cli
8             FRMREF_BANCA.lblnome = frmlibera.lblcliente
9         End If

10        lbldata = data_faturamento

          'BUSCA REFER�NCIAS BANC�RIAS

11        db.Parameters.Remove "cli"

12        If ESCOLHA = 0 Or ESCOLHA = 7 Then
13            db.Parameters.Add "cli", w_cod, 1
14        Else
15            db.Parameters.Add "cli", frmlibera.lblcod_cli, 1
16        End If

17        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Ref_Bancarias_2(:vCursor, :Cli); END;"

18        Set OraDynaset = db.Parameters("vCursor").Value

19        db.Parameters.Remove "cli"

20        Grid1.Rows = 20
21        Grid1.COL = 0
22        Grid1.ColWidth(0) = 400
23        Grid1.ColWidth(1) = 12000
24        Grid1.Row = 0
25        Grid1.Text = "SEQ."
26        Grid1.COL = 1
27        Grid1.Row = 0
28        Grid1.Text = "OBSERVA��O"
29        GRD_BCO.Rows = 8
30        GRD_BCO.Cols = OraDynaset.RecordCount + 1
31        GRD_BCO.COL = 0
32        GRD_BCO.ColWidth(0) = 1400
33        GRD_BCO.Row = 1
34        GRD_BCO.Text = "SEQUENCIA"
35        GRD_BCO.Row = 2
36        GRD_BCO.Text = "DT_CONSULTA"
37        GRD_BCO.Row = 3
38        GRD_BCO.Text = "AGENCIA"
39        GRD_BCO.Row = 4
40        GRD_BCO.Text = "DDD"
41        GRD_BCO.Row = 5
42        GRD_BCO.Text = "FONE"
43        GRD_BCO.Row = 6
44        GRD_BCO.Text = "CONCEITO"
45        GRD_BCO.Row = 7
46        GRD_BCO.Text = "OBS."

47        coluna = 1
48        linha = 1

49        Do While Not OraDynaset.EOF
50            GRD_BCO.Rows = 8
51            GRD_BCO.Row = 0
52            GRD_BCO.COL = coluna
53            GRD_BCO.FixedAlignment(coluna) = 2
54            GRD_BCO.ColWidth(coluna) = 1200
55            GRD_BCO.Text = OraDynaset!banco
56            GRD_BCO.Row = 1
57            GRD_BCO.COL = coluna
58            GRD_BCO.Text = OraDynaset.Fields("sequencia")
59            GRD_BCO.Row = 2
60            If OraDynaset.Fields("agencia") <> "" Then
61                GRD_BCO.Text = IIf(IsNull(OraDynaset.Fields("dt_consulta")), "", OraDynaset.Fields("dt_consulta"))
62            End If
63            GRD_BCO.Row = 3
64            If OraDynaset.Fields("agencia") <> "" Then
65                GRD_BCO.Text = IIf(IsNull(OraDynaset.Fields("agencia")), "", OraDynaset.Fields("agencia"))
66            End If
67            GRD_BCO.Row = 4
68            If OraDynaset.Fields("ddd") <> "" Then
69                GRD_BCO.Text = IIf(IsNull(OraDynaset.Fields("ddd")), "", OraDynaset.Fields("ddd"))
70            End If
71            GRD_BCO.Row = 5
72            If OraDynaset.Fields("fone") <> "" Then
73                GRD_BCO.Text = IIf(IsNull(OraDynaset.Fields("fone")), "", OraDynaset.Fields("fone"))
74            End If
75            GRD_BCO.Row = 6
76            If OraDynaset.Fields("conceito") <> "" Then
77                GRD_BCO.Text = IIf(IsNull(OraDynaset.Fields("conceito")), "", OraDynaset.Fields("conceito"))
78            End If
79            GRD_BCO.Row = 7

80            Grid1.Row = coluna
81            Grid1.COL = 0
82            Grid1.Text = OraDynaset.Fields("sequencia")
83            Grid1.COL = 1
84            If OraDynaset.Fields("observacao") <> "" Then
85                Grid1.Text = IIf(IsNull(OraDynaset.Fields("observacao")), "", OraDynaset.Fields("observacao"))
86            End If

87            OraDynaset.MoveNext
88            coluna = coluna + 1
89        Loop
          
90        Me.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Load" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub GRD_BCO_DblClick()

1         On Error GoTo Trata_Erro

2         If GRD_BCO.Cols < 2 Then Exit Sub

3         db.Parameters.Remove "cli"

4         If ESCOLHA = 0 Or ESCOLHA = 7 Then
5             db.Parameters.Add "cli", w_cod, 1
6         Else
7             db.Parameters.Add "cli", frmlibera.lblcod_cli, 1
8         End If

9         GRD_BCO.Row = 1
10        seq = GRD_BCO.Text
11        db.Parameters.Remove "seq"
12        db.Parameters.Add "seq", seq, 1

13        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Observacoes(:vCursor, :Cli, :Seq); END;"
          
14        Set OraDynaset = db.Parameters("vCursor").Value
          
Trata_Erro:
15        If Err.Number <> 0 Then
16            MsgBox "Sub Grd_Bco_DblClick" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
17        End If
          
End Sub

Private Sub SSCommand1_Click()

    On Error GoTo Trata_Erro

1         Unload Me
2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             frminfo_ger.Show
4             frminfo_ger.SetFocus
5             frminfo_ger.WindowState = 2
6         Else
7             frmlibera.Show
8             frmlibera.SetFocus
9             frmlibera.WindowState = 2
10        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand1_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub SSCommand2_Click()
    Imprimir FRMREF_BANCA
End Sub

Private Sub SSCommand3_Click()
1       On Error GoTo Erro
         
2          If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             cliente = "h:\oracle\sistemas\vb\32BITS\rec034.exe " & "Vda070;" & w_cod & ";" & frmlibera.lblcliente & ";" & _
              lngCod_Usuario & ";" & strNome_usuario & ";" & strDepto
4             Shell cliente, 1
5         Else
6             cliente = "h:\oracle\sistemas\vb\32BITS\rec034.exe " & "Vda070;" & frmlibera.lblcod_cli & ";" & frmlibera.lblcliente & ";" & _
              lngCod_Usuario & ";" & strNome_usuario & ";" & strDepto
7             Shell cliente, 1
8         End If
          
Erro:
9         If Err.Number = 53 Then
10            MsgBox "Agenda n�o dispon�vel", vbInformation, "Aten��o"
11            Exit Sub
12        ElseIf Err.Number <> 0 Then
13            MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
14        End If
End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Agenda"
End Sub

Private Sub SSCommand6_Click()
    tela_sumir = tela_atual
    Unload Me
    frmdupcanc.Show
    frmdupcanc.SetFocus
    frmdupcanc.WindowState = 2
End Sub

Private Sub SSCommand6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Duplicatas Canceladas"
End Sub


