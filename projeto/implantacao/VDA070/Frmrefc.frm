VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form FRMREF_COM 
   Caption         =   "REFER�NCIAS COMERCIAIS"
   ClientHeight    =   6885
   ClientLeft      =   300
   ClientTop       =   1410
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6885
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin MSGrid.Grid Grid1 
      Height          =   1725
      Left            =   30
      TabIndex        =   4
      Top             =   5130
      Visible         =   0   'False
      Width           =   11805
      _Version        =   65536
      _ExtentX        =   20823
      _ExtentY        =   3043
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin MSGrid.Grid GRD_COM 
      Height          =   3975
      Left            =   30
      TabIndex        =   3
      Top             =   1110
      Width           =   11805
      _Version        =   65536
      _ExtentX        =   20823
      _ExtentY        =   7011
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Rows            =   12
      Cols            =   9
      FixedRows       =   2
      MouseIcon       =   "Frmrefc.frx":0000
   End
   Begin Bot�o.cmd cmdfat 
      Height          =   615
      Left            =   3240
      TabIndex        =   6
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":001C
      PICN            =   "Frmrefc.frx":0038
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmddup 
      Height          =   615
      Left            =   3960
      TabIndex        =   7
      ToolTipText     =   "Duplicatas"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":0912
      PICN            =   "Frmrefc.frx":092E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdtitav 
      Height          =   615
      Left            =   4680
      TabIndex        =   8
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":0C48
      PICN            =   "Frmrefc.frx":0C64
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand3 
      Height          =   615
      Left            =   7560
      TabIndex        =   9
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":0F7E
      PICN            =   "Frmrefc.frx":0F9A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand1 
      Height          =   615
      Left            =   30
      TabIndex        =   10
      ToolTipText     =   "Sair"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":1874
      PICN            =   "Frmrefc.frx":1890
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   11
      Top             =   660
      Width           =   11850
      _ExtentX        =   20902
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd SSCommand2 
      Height          =   615
      Left            =   9090
      TabIndex        =   12
      ToolTipText     =   "Imprimir"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":256A
      PICN            =   "Frmrefc.frx":2586
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd SSCommand6 
      Height          =   615
      Left            =   6120
      TabIndex        =   13
      ToolTipText     =   "Duplicatas Canceladas"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":3260
      PICN            =   "Frmrefc.frx":327C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdinfo 
      Height          =   615
      Left            =   2520
      TabIndex        =   14
      ToolTipText     =   "Informa��es"
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":3596
      PICN            =   "Frmrefc.frx":35B2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdpag 
      Height          =   615
      Left            =   5400
      TabIndex        =   15
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":3A04
      PICN            =   "Frmrefc.frx":3A20
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdrban 
      Height          =   615
      Left            =   6840
      TabIndex        =   5
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":3D3A
      PICN            =   "Frmrefc.frx":3D56
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSci 
      Height          =   615
      Left            =   9810
      TabIndex        =   16
      Top             =   0
      Width           =   675
      _ExtentX        =   1191
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "SCI Serasa"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":4070
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultarSCI 
      Height          =   615
      Left            =   10530
      TabIndex        =   17
      Tag             =   "1"
      ToolTipText     =   "Consultar SCI/SERASA j� Gravados"
      Top             =   0
      Visible         =   0   'False
      Width           =   1305
      _ExtentX        =   2302
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "SCI Serasa"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":408C
      PICN            =   "Frmrefc.frx":40A8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCA 
      Height          =   615
      Left            =   8280
      TabIndex        =   18
      Top             =   0
      Width           =   795
      _ExtentX        =   1402
      _ExtentY        =   1085
      BTYPE           =   3
      TX              =   "Agenda CA"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Frmrefc.frx":4D82
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Line Line1 
      X1              =   960
      X2              =   1080
      Y1              =   870
      Y2              =   870
   End
   Begin VB.Label lblcli 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label4"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   60
      TabIndex        =   2
      Top             =   750
      Width           =   855
   End
   Begin VB.Label lblnome 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label3"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1125
      TabIndex        =   1
      Top             =   750
      Width           =   9405
   End
   Begin VB.Label lbldata 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   10620
      TabIndex        =   0
      Top             =   750
      Width           =   1215
   End
End
Attribute VB_Name = "FRMREF_COM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCA_Click()

    On Error GoTo Trata_Erro

1        If ESCOLHA = 0 Or ESCOLHA = 7 Then
2           lngCOD_CLIENTE = w_cod
             
3         Else
4            lngCOD_CLIENTE = frmlibera.lblcod_cli
5         End If

6         frmCA.Show 1

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdCa_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdConsultarSCI_Click()
    frmConsulta.vCodCli = lblcli
    frmConsulta.Show
End Sub

Private Sub cmddup_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If

7         Unload Me

8         frmavencer.Show
9         frmavencer.SetFocus
10        frmavencer.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdDup_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmddup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "T�tulos a Vencer"
End Sub

Private Sub cmdfat_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If

7         Unload Me

8         frmfatura.Show
9         frmfatura.SetFocus
10        frmfatura.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdFat_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub cmdfat_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Faturas"
End Sub


Private Sub cmdinfo_Click()
    tela_sumir = tela_atual

    Unload Me

    frminfo_ger.Show
    frminfo_ger.SetFocus
    frminfo_ger.WindowState = 2
End Sub


Private Sub cmdinfo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Informa��es Gerais"

End Sub


Private Sub cmdpag_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If
7         Unload Me

8         FRMPAGAM.Show
9         FRMPAGAM.SetFocus
10        FRMPAGAM.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdPag_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdpag_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Pagamentos"

End Sub


Private Sub cmdrban_Click()
    tela_sumir = tela_atual
    Unload Me

    FRMREF_BANCA.Show
    FRMREF_BANCA.SetFocus
    FRMREF_BANCA.WindowState = 2

End Sub

Private Sub cmdrban_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Refer�ncias  Banc�rias"

End Sub


Private Sub cmdSci_Click()
    frmDocto.vCodCli = lblcli
    frmSerasa.Show
End Sub

Private Sub cmdtitav_Click()

    On Error GoTo Trata_Erro

1         tela_sumir = tela_atual

2         If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             mdepdest = 1
4         Else
5             mdepdest = frmlibera.lbldep
6         End If

7         Unload Me

8         frmvencido.Show
9         frmvencido.SetFocus
10        frmvencido.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub CmdTitAv_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub cmdtitav_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "T�tulos Vencidos"

End Sub


Private Sub Form_Load()

    On Error GoTo Trata_Erro

1         Me.Top = ((MDIForm1.Height - Me.Height) / 2) - MDIForm1.Picture1.Height
2         Me.Left = (MDIForm1.Width - Me.Width) / 2
          
3         If ESCOLHA = 0 Or ESCOLHA = 7 Then
4             FRMREF_COM.lblcli = w_cod
5             FRMREF_COM.lblnome = w_cli
6         Else
7             FRMREF_COM.lblcli = frmlibera.lblcod_cli
8             FRMREF_COM.lblnome = frmlibera.lblcliente
9         End If

10        lbldata = data_faturamento

          'BUSCA REFER�NCIAS COMERCIAIS

11        db.Parameters.Remove "cli"
12        If ESCOLHA = 0 Or ESCOLHA = 7 Then
13            db.Parameters.Remove "cli"
14            db.Parameters.Add "cli", w_cod, 1
15        Else
16            db.Parameters.Remove "cli"
17            db.Parameters.Add "cli", frmlibera.lblcod_cli, 1
18        End If

19        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Maior_Seq_Clie_Refer(:vCursor, :cli); END;"

20        Set OraDynaset = db.Parameters("vCursor").Value

21        If Not OraDynaset.EOF() Then
22            qtde_col = OraDynaset.Fields("qtde_col")
23        End If

24        db.Parameters.Remove "cli"
25        If ESCOLHA = 0 Or ESCOLHA = 7 Then
26            db.Parameters.Add "cli", w_cod, 1
27        Else
28            db.Parameters.Add "cli", frmlibera.lblcod_cli, 1
29        End If

30        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Clie_Refer(:vCursor, :Cli); END;"

31        Set OraDynaset = db.Parameters("vCursor").Value

32        If Not OraDynaset.EOF() Then
33            FRMREF_COM.Show
34            FRMREF_COM.SetFocus
35            FRMREF_COM.WindowState = 2
36            Fl_Registros = "S"
37        ElseIf OraDynaset.EOF Then
38            MsgBox "N�o Existem Refer�ncias Comerciais! "
39            Fl_Registros = "N"
40            Exit Sub
41        End If

42        db.Parameters.Remove "cli"

43        Grid1.Rows = 20
44        Grid1.COL = 0
45        Grid1.ColWidth(0) = 400
46        Grid1.ColWidth(1) = 1200
47        Grid1.Row = 0
48        Grid1.Text = "Seq."
49        Grid1.COL = 1
50        Grid1.Row = 0
51        Grid1.Text = "OBSERVA��O"

52        GRD_COM.Rows = 20
53        GRD_COM.Cols = qtde_col + 1
54        GRD_COM.COL = 0
55        GRD_COM.ColWidth(0) = 1000
56        GRD_COM.Row = 1
57        GRD_COM.Text = "Seq.      ==>"
58        GRD_COM.Row = 0
59        GRD_COM.Text = "Empresa==>"
60        GRD_COM.Row = 2
61        GRD_COM.Text = "Dt. Con."
62        GRD_COM.Row = 3
63        GRD_COM.Text = "Dt. Cad."
64        GRD_COM.Row = 4
65        GRD_COM.Text = "Ult. Fat."
66        GRD_COM.Row = 5
67        GRD_COM.Text = "Dt. Ult. Fat."
68        GRD_COM.Row = 6
69        GRD_COM.Text = "M.S.Dev."
70        GRD_COM.Row = 7
71        GRD_COM.Text = "Atraso"
72        GRD_COM.Row = 8
73        GRD_COM.Text = "Conceito"
74        GRD_COM.Row = 9

75        linha = 1
76        coluna = 1
          'A pedido da Rosangela em 16/08/01 o numero de colunas mostradas foi reduzido para 15
          'Do While Not oradynaset.EOF() And coluna < 14
          'Em 18/10 voltou como era antigamente sem limitar a qtde de Ref Com a serem visualizadas
77        Do While Not OraDynaset.EOF()
78            DoEvents
79            GRD_COM.Rows = 9
80            GRD_COM.Row = 1
              'Inserido dia 16/12/2004 por Eduardo Relvas
81            If coluna >= GRD_COM.Cols - 1 Then
82               GRD_COM.Cols = GRD_COM.Cols + 1
83            End If
84            GRD_COM.COL = coluna
85            GRD_COM.Text = OraDynaset.Fields("sequencia")
86            GRD_COM.Row = 0
87            GRD_COM.COL = coluna
88            GRD_COM.Text = OraDynaset.Fields("empresa")
89            GRD_COM.Row = 2
90            GRD_COM.COL = coluna
91            GRD_COM.ColWidth(coluna) = 1000
92            GRD_COM.FixedAlignment(coluna) = 2

93            If OraDynaset.Fields("dt_consulta") <> "" Then
94                GRD_COM.Text = OraDynaset.Fields("dt_consulta")
95            End If
96            GRD_COM.Row = 3
97            If OraDynaset.Fields("dt_cadastr") <> "" Then
98                GRD_COM.Text = OraDynaset.Fields("dt_cadastr")
99            End If
100           GRD_COM.Row = 4
101           If OraDynaset.Fields("ult_fatura") <> "" Then
102               GRD_COM.Text = OraDynaset.Fields("ult_fatura")
103           End If
104           GRD_COM.Row = 5
105           If OraDynaset.Fields("dt_ult_fatura") <> "" Then
106               GRD_COM.Text = OraDynaset.Fields("dt_ult_fatura")
107           End If
108           GRD_COM.Row = 6
109           GRD_COM.Text = Format(OraDynaset.Fields("maior_saldev"), "#,0.00")
110           GRD_COM.Row = 7
111           If OraDynaset.Fields("atraso_medio") <> "" Then
112               GRD_COM.Text = OraDynaset.Fields("atraso_medio")
113           End If
114           GRD_COM.Row = 8
115           If OraDynaset.Fields("conceito_empresa") <> "" Then
116               GRD_COM.Text = OraDynaset.Fields("conceito_empresa")
117           End If

118           OraDynaset.MoveNext
119           linha = linha + 1
120           coluna = coluna + 1
121       Loop

122       Me.WindowState = 2

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Form_Load" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub GRD_COM_Click()

    On Error GoTo Trata_Erro

1         Grid1.Visible = False

2         GRD_COM.Row = 1
3         Sequencia = GRD_COM.Text

4         db.Parameters.Remove "cli"
5         If ESCOLHA = 0 Or ESCOLHA = 7 Then
6             db.Parameters.Add "cli", w_cod, 1
7         Else
8             db.Parameters.Add "cli", frmlibera.lblcod_cli, 1
9         End If
10        db.Parameters.Remove "seq"
11        db.Parameters.Add "seq", Sequencia, 1

12        db.ExecuteSQL "BEGIN PRODUCAO.PCK_VDA070.Pr_Select_Obs_Seq(:vCursor, :cli, :Seq); END;"

13        Set OraDynaset = db.Parameters("vCursor").Value

14        db.Parameters.Remove "cli"
15        db.Parameters.Remove "seq"

16        If Not OraDynaset.EOF Then
17            Grid1.Row = 0
18            Grid1.COL = 0
19            Grid1.FixedAlignment(1) = 2
20            Grid1.Text = "Seq."
21            Grid1.Rows = OraDynaset.RecordCount + 1
22            Grid1.Row = 1
23            Grid1.COL = 0
24            Grid1.Text = OraDynaset.Fields("sequencia")
25            Grid1.COL = 1
26            Grid1.ColWidth(1) = 10000
27            If OraDynaset.Fields("observacao") <> "" Then
28                Grid1.Text = OraDynaset.Fields("observacao")
29                Grid1.Visible = True
30            Else
31                MsgBox " N�o existem Oberva��es ", vbInformation, "Aten��o"
32            End If
33        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Grd_Com_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub SSCommand1_Click()

    On Error GoTo Trata_Erro

1         GRD_COM.Cols = 1

2         Unload Me
3         If ESCOLHA = 0 Or ESCOLHA = 7 Then
4             frminfo_ger.Show
5             frminfo_ger.SetFocus
6             frminfo_ger.WindowState = 2
7         Else
8             frmlibera.Show
9             frmlibera.SetFocus
10            frmlibera.WindowState = 2
11        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub SSCommand1_Click" & vbclrf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub SSCommand2_Click()
    Imprimir FRMREF_COM
End Sub

Private Sub SSCommand3_Click()
1       On Error GoTo Erro
         
2          If ESCOLHA = 0 Or ESCOLHA = 7 Then
3             cliente = "h:\oracle\sistemas\vb\32BITS\rec034.exe " & "Vda070;" & w_cod & ";" & frmlibera.lblcliente & ";" & _
              lngCod_Usuario & ";" & strNome_usuario & ";" & strDepto
4             Shell cliente, 1
5         Else
6             cliente = "h:\oracle\sistemas\vb\32BITS\rec034.exe " & "Vda070;" & frmlibera.lblcod_cli & ";" & frmlibera.lblcliente & ";" & _
              lngCod_Usuario & ";" & strNome_usuario & ";" & strDepto
7             Shell cliente, 1
8         End If
          
          
Erro:
9         If Err.Number = 53 Then
10            MsgBox "Agenda n�o dispon�vel", vbInformation, "Aten��o"
11            Exit Sub
12        ElseIf Err.Number <> 0 Then
13            MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
14        End If
End Sub

Private Sub SSCommand3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Agenda"
End Sub

Private Sub SSCommand6_Click()
    tela_sumir = tela_atual
    Unload Me
    frmdupcanc.Show
    frmdupcanc.SetFocus
    frmdupcanc.WindowState = 2
End Sub

Private Sub SSCommand6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    MDIForm1.stbBarra.Panels(1).Text = "Duplicatas Canceladas"
End Sub

