CREATE OR REPLACE PACKAGE PRODUCAO.PCK_VDA070
--------------------------------------------
--- SISTEMA	: BLOQUEIO DE CR�DITO
--- ANALISTA    : MARICI
--- DATA	: 31/05/2004

IS
 -- variavel tipo cursor
 TYPE tp_cursor IS REF CURSOR;
 vSql Varchar2(4000);
 
 PROCEDURE PR_SENHA(p_cursor    IN OUT tp_cursor,
                    p_login     IN     helpdesk.usuario.login%TYPE,
                    p_senha     IN     VARCHAR2,
                    p_erro     OUT     NUMBER);

 PROCEDURE PR_ATUALIZA_LOG(p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
                  	   p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                  	   p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                  	   p_fl_lib        IN     VARCHAR2,
                  	   p_cod_usuario   IN     helpdesk.usuario.cod_usuario%TYPE,
                  	   p_cod_usuario_r IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_erro          OUT    NUMBER);

PROCEDURE PR_RESPONSAVEL(p_cursor    IN OUT tp_cursor,
                    	 p_erro     OUT     NUMBER);


PROCEDURE PR_CONSULTA_LOG(p_cursor    IN OUT tp_cursor,
             	          p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
			  p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                  	  p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
             		  p_erro          OUT     NUMBER);

PROCEDURE PR_DELETA_LOG(p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
                  	p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                  	p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                  	p_erro          OUT    NUMBER);

Procedure Pr_Select_Saldo_Vencer (
                                  PM_CURSOR1 IN OUT TP_CURSOR,
                                  Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                                 );
Procedure Pr_Select_Saldo_Vencido (
                                   PM_CURSOR1 IN OUT TP_CURSOR,
                                   Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                                   );
Procedure Pr_Select_Duplicatas (
                                Pm_Cursor  IN OUT TP_CURSOR,
                                Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                               );
  Procedure Pr_Select_Boleto_OP (
                                 Pm_Cursor IN OUT TP_CURSOR,
                                 Pm_Deposito IN varchar2,
                                 Pm_Loja     IN COBRANCA.BOLETO_OP.COD_LOJA%type,
                                 Pm_Ped      IN COBRANCA.BOLETO_OP.NUM_PEDIDO%type,
                                 Pm_Seq      IN COBRANCA.BOLETO_OP.SEQ_PEDIDO%type
                                );
  Procedure Pr_Update_Cobranca_Boleto_OP (
                                          Pm_Deposito IN varchar2,
                                          Pm_Opt      IN Cobranca.Boleto_Op.Tp_Pgto%type,
                                          Pm_Loja     IN Cobranca.Boleto_Op.Cod_Loja%type,
                                          Pm_Ped      IN Cobranca.Boleto_Op.Num_Pedido%type,
                                          Pm_Seq      IN Cobranca.Boleto_Op.Seq_Pedido%type,
                                          Pm_Cod_Erro OUT Number,
                                          Pm_Txt_Erro OUT Varchar2                                         
                                         );
  Procedure Pr_Insert_Cobranca_Boleto_OP (
                                          Pm_Deposito IN Varchar2,
                                          Pm_Loja     IN Cobranca.Boleto_Op.Cod_Loja%type,
                                          Pm_Ped      IN Cobranca.Boleto_Op.Num_Pedido%type,
                                          Pm_Seq      IN CObranca.Boleto_Op.Seq_Pedido%type,
                                          Pm_Sit      IN Cobranca.Boleto_Op.Situacao%type,
                                          Pm_Opt      IN Cobranca.Boleto_Op.Tp_Pgto%type,
                                          Pm_Cod_Erro OUT Number,
                                          Pm_Txt_Erro OUT Varchar2
                                         );
  Procedure Pr_Select_Loja (
                            Pm_Cursor IN OUT TP_CURSOR
                           );
                           
  Procedure Pr_Select_Pedidos (
                               PM_CURSOR IN OUT TP_CURSOR,
                               Pm_Usuario IN Varchar2,
                               Pm_Loja    IN PedNota_venda.Cod_Loja%type
                              );
  Procedure Pr_Select_Ordenando_Pedidos (
                                         Pm_Cursor IN OUT TP_CURSOR,
                                         Pm_COL    IN Number
                                        );
  Procedure Pr_Select_Duplicatas_2 (
                                    PM_Cursor IN OUT TP_CURSOR,
                                    PM_Cli    IN Cobranca.Duplicatas.Cod_Cliente%type
                                   );
  Procedure Pr_Select_Nome_Cliente (
                                    Pm_CURSOR IN OUT TP_CURSOR,
                                    Pm_Cli    IN Cliente.Cod_Cliente%Type
                                   );
  Procedure Pr_Select_Duplicatas_3 (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    Pm_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                   );
  Procedure Pr_Select_Dupl_Em_Aberto (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      Pm_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                     );
  Procedure Pr_Select_Faturas_Cps (
                                   PM_CURSOR IN OUT TP_CURSOR,
                                   PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                  );
  Procedure Pr_Update_PedNota_Venda (
                                     Pm_Deposito IN Varchar2,
                                     Pm_Data_Fat IN Varchar2,
                                     Pm_Ped      IN PedNota_venda.Num_Pedido%type,
                                     Pm_Loja     IN PedNota_Venda.Cod_Loja%type,
                                     Pm_Cod_Erro OUT Number,
                                     Pm_Txt_Erro OUT varchar2
                                    );
  Procedure Pr_Select_Para_Grafico (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Cobranca.duplicatas.cod_cliente%type
                                   );
  Procedure Pr_Select_Pedidos_Indent(
                                     Pm_Cursor IN OUT TP_CURSOR,
                                     Pm_Cod    IN Indent.Pedido_Indent.Cod_Cliente%type
                                    );
  Procedure Pr_Select_Cliente (
                               PM_CURSOR IN OUT TP_CURSOR,
                               PM_CLI    IN Cliente.Cod_Cliente%type
                              );
  Procedure Pr_Select_Cliente_2 (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 PM_CLI    IN Cliente.Cod_Cliente%type
                                );
  Procedure Pr_Select_Maior_Fatura (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Clie_credito.Cod_Cliente%type
                                   );
  Procedure Pr_Select_Maior_Saldo_Devedor (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN OUT Cliente.Cod_Cliente%type
                                          );
  Procedure Pr_Select_Saldo_Pedidos_CPS (
                                         PM_CURSOR IN OUT TP_CURSOR,
                                         PM_CLI    IN v_saldo_pedidos.cod_cliente%type
                                        );
  Procedure Pr_Select_Saldo_Pedidos (
                                     PM_CURSOR  IN OUT TP_CURSOR,
                                     PM_CODCLI  IN v_saldo_pedidos.cod_cliente%type,
                                     PM_DEPDEST IN varchar2
                                    ) ;
  Procedure Pr_Select_SaldoVencer_Venc_Cps (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                           );
  Procedure Pr_Select_Saldo_Vencidas (
                                      Pm_CURSOR IN OUT TP_CURSOR,
                                      PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                     );
  Procedure Pr_Select_limite_Credito (
                                      PM_CURSOR IN OUT TP_CURSOR
                                     );
  Procedure Pr_Select_Dupl_Ab_Venc_Vencer (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                          ) ;
  Procedure Pr_Select_Saldo_Pedido_Vendor (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                          );
  Procedure Pr_Update_PedNota_Venda_Libera (
                                            Pm_Deposito IN Varchar2,
                                            Pm_Ped      IN Pednota_Venda.Num_Pedido%type,
                                            Pm_Seq      IN Pednota_Venda.Seq_Pedido%type,
                                            Pm_Dt_Fat   IN Varchar2,
                                            Pm_Loja     IN PedNota_venda.Cod_Loja%type,
                                            Pm_Cod_Erro OUT Number,
                                            Pm_Txt_Erro OUT Varchar2
                                           );
  Procedure Pr_Select_Conexao (
                               PM_CURSOR IN OUT TP_CURSOR,
                               PM_Deposito IN Producao.Loja_Conexao.Cod_Loja%type
                              ) ;
  Procedure Pr_Select_BL_CL(
                            Pm_Cursor   IN OUT TP_CURSOR,
                            Pm_Deposito IN varchar2,
                            Pm_Loja     Number
                           );
  Procedure Pr_Select_Cliente (
                               PM_CURSOR       IN OUT TP_CURSOR,
                               PM_Total_Pedido IN Number,
                               Pm_Deposito     IN Varchar2,
                               Pm_Loja         Number
                              );
  Procedure Pr_Select_INFO_PEDIDOS (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    Pm_Ped    IN Pednota_Venda.Num_Pedido%type,
                                    Pm_Seq    IN Pednota_Venda.Seq_Pedido%type,
                                    Pm_Dep    IN Pednota_Venda.Cod_Loja%type,
                                    Pm_Cli    IN Pednota_Venda.Cod_Cliente%type
                                   );
  Procedure Pr_Select_Infor_Representante (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           Pm_Rep    Representante.Cod_Repres%type
                                          ) ;
  Procedure Pr_Select_Ref_Comerciais (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      Pm_CodCli IN Clie_Refer.Cod_Cliente%type
                                     );
  Procedure Pr_Select_Ref_Bancarias (
                                     PM_CURSOR IN OUT TP_CURSOR,
                                     Pm_Cod_Cli   IN Clie_Refbco.Cod_Cliente%type
                                    );
  Procedure Pr_Select_Depositos (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 Pm_Escolha   IN Number
                                );
  Procedure Pr_Select_PedNota_Venda (
                                     Pm_Cursor IN OUT TP_CURSOR,
                                     Pm_Ped    IN Pednota_Venda.Num_Pedido%type,
                                     Pm_Seq    IN PednOta_venda.Seq_Pedido%type,
                                     Pm_Loja   IN PedNota_Venda.Cod_Loja%type,
                                     Pm_Dep    IN Varchar2
                                    );
  Procedure Pr_Select_Divisao (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 Pm_Divisao IN Number
                                );                                    
  Procedure Pr_Select_Flags_Plano_Pagto (
                                         Pm_Cursor IN OUT TP_CURSOR,
                                         Pm_Plano  IN Plano_Pgto.Cod_Plano%type
                                        );
  Procedure Pr_Select_Info_Repres_Vend_CPS (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_BAN    IN Banco.Cod_Banco%type,
                                            Pm_Tra    IN transportadora.cod_transp%type,
                                            Pm_Pla    IN Plano_pgto.Cod_Plano%type,
                                            Pm_Rep    IN Representante.Cod_Repres%type,
                                            Pm_Cli    IN Cliente.Cod_Cliente%type
                                           ) ;
  Procedure Pr_Select_Pseudonimo (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_rep    IN Representante.Cod_Repres%type
                                 );
  Procedure Pr_Select_Pseudonimo_Franquia (
                                           Pm_Cursor IN OUT TP_CURSOR,
                                           Pm_rep    IN Representante.Cod_Repres%type
                                          );
  Procedure Pr_Select_Dados_cliente (
                                     Pm_CURSOR      IN OUT TP_CURSOR,
                                     Pm_Cod_Cliente IN Cliente.Cod_Cliente%type
                                    );
  Procedure Pr_Select_Plano_Pgto (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_cod_plano IN Plano_Pgto.Cod_Plano%type
                                 );
  Procedure Pr_Select_Dados_Transportadora (
                                            PM_CURSOR     IN OUT TP_CURSOR,
                                            Pm_cod_transp IN Transportadora.Cod_Transp%type
                                           );
  Procedure Pr_Select_Dados_Representante (
                                           Pm_Cursor IN OUT TP_CURSOR,
                                           Pm_Rep    IN Representante.Cod_Repres%type
                                          ) ;
  Procedure Pr_Select_Pedido (
                              PM_CURSOR IN OUT TP_CURSOR,
                              PM_PED    IN Pednota_Venda.Num_Pedido%type,
                              Pm_SEQ    IN Pednota_venda.Seq_Pedido%type,
                              Pm_Loja   IN Pednota_venda.Cod_Loja%type
                             );
  Procedure Pr_Select_Info_Transportadora (
                                           Pm_CURSOR IN OUT TP_CURSOR,
                                           Pm_Cod_Tra IN Transportadora.Cod_Transp%type
                                          );
  Procedure Pr_Update_PedNota_Venda_GO_FO (
                                           Pm_Deposito IN Varchar2,
                                           Pm_Dt_Fat   IN Varchar2,
                                           Pm_Seq      IN Pednota_Venda.Seq_Pedido%type,
                                           Pm_Ped      IN PedNOta_Venda.Num_Pedido%type,
                                           Pm_Loja     IN PedNota_Venda.Cod_Loja%type,
                                           Pm_Cod_Erro OUT Number,
                                           Pm_Txt_Erro OUT Varchar2
                                          );
  Procedure Pr_Select_Pagamentos (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_Cli    IN Cobranca.Duplicatas.Cod_Cliente%type
                                 );
  Procedure Pr_Select_Ref_Bancarias_2 (
                                       PM_CURSOR IN OUT TP_CURSOR,
                                       PM_CLI    IN Clie_Refbco.Cod_Cliente%type
                                      );
  Procedure Pr_Select_Observacoes (
                                   Pm_Cursor IN OUT TP_CURSOR,
                                   Pm_Cli    Clie_Refbco.Cod_Cliente%type,
                                   Pm_Seq    Clie_Refbco.Sequencia%type
                                  );
  Procedure Pr_Select_Maior_Seq_Clie_Refer (
                                            Pm_CURSOR IN OUT TP_CURSOR,
                                            Pm_Cli    IN Clie_Refer.Cod_Cliente%type
                                           );
  Procedure Pr_Select_Clie_Refer (
                                  Pm_CURSOR IN OUT TP_CURSOR,
                                  Pm_CLi    IN Clie_Refer.Cod_Cliente%type
                                 );
  Procedure Pr_Select_Obs_Seq (
                               PM_Cursor IN OUT TP_CURSOR,
                               Pm_Cli    IN Clie_Refer.Cod_Cliente%type,
                               Pm_Seq    IN Clie_Refer.Sequencia%type
                              );
  Procedure Pr_Select_Saldo_Venc_Venci_Cps(
                                                Pm_Cursor IN OUT TP_CURSOR,
                                                Pm_cli    IN Cobranca.duplicatas.Cod_Cliente%type
                                               );
  Procedure Pr_Select_Duplicatas_4 (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                   );
  Procedure Pr_Select_Pedido_ECT (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                 );
  Procedure Pr_Select_Dolar (
                             PM_CURSOR IN OUT TP_CURSOR
                            );
  Procedure Pr_Select_PEdido_Local (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_LOJA   IN Pednota_venda.Cod_loja%type
                                   );
  Procedure Pr_Select_Pedido_Local_Repres(
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_LOJA   IN PedNOta_Venda.Cod_loja%type
                                         );
  Procedure Pr_Select_Pedido_Retira_local(
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_LOJA   IN Pednota_Venda.Cod_Loja%type
                                         );
  Procedure Pr_Select_Retira_Remoto (
                                     PM_CURSOR IN OUT TP_CURSOR,
                                     PM_DEPOSITO IN Varchar2,
                                     PM_LOJA     IN Number
                                    );
  Procedure Pr_Select_Clie_Novos_Ina_SP_GO (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            Pm_Deposito  IN Varchar2,
                                            Pm_Loja      IN Number
                                           );
  Procedure Pr_Select_Pedido_Vista_Local (
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_Loja   IN Number
                                         );
  Procedure Pr_Select_Pedido_Credito (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      PM_PED    IN PedNota_Venda.Num_Pedido%type,
                                      PM_SEQ    IN PedNota_Venda.Seq_Pedido%type,
                                      PM_LOJA   IN PedNota_Venda.Cod_Loja%type
                                     ) ;
  Procedure Pr_Select_Cliente_Novo_SP_GO(
                                         PM_CURSOR   IN OUT TP_CURSOR,
                                         PM_Deposito IN Varchar2,
                                         PM_LOJA     IN Number
                                        );
  Procedure Pr_Select_Cliente_Novo_SP_GO_2 (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_DEPOSITO IN Varchar2,
                                            PM_LOJA     IN Number
                                           );
  Procedure Pr_Select_Pedido_Vista_Remoto (
                                    PM_CURSOR   IN OUT TP_CURSOR,
                                    PM_DEPOSITO IN Varchar2,
                                    PM_LOJA     IN Number
                                   );
  Procedure Pr_Select_Pedido_Remoto_TLMK (
                                          PM_CURSOR   IN OUT TP_CURSOR,
                                          PM_DEPOSITO IN Varchar2,
                                          PM_LOJA     IN Number
                                         );

  Procedure Pr_Select_Pedido_Remoto_Repres (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_DEPOSITO  IN Varchar2,
                                            PM_LOJa      IN Number
                                           );
  Procedure Pr_Select_Pedido_local_tlmk (
                                         PM_CURSOR IN OUT TP_CURSOR,
                                         PM_LOJA   IN Number
                                        );
  Procedure Pr_Insert_Update_Parametros (
                                         PM_COD_SISTEMA    in NUMBER,
					                          PM_NOME_PARAMETRO in VARCHAR2,
					                          PM_VLR_PARAMETRO  IN VARCHAR2,
                                         PM_ERRO           OUT Number,
                                         PM_TXTERRO        OUT Varchar2
                                        );
  Procedure Pr_Select_Vl_Parametro (
                                    PM_CURSOR1 IN OUT TP_CURSOR,
                                    PM_COD_SISTEMA IN NUMBER,
  					                     PM_NOME_PARAM  IN VARCHAR2
                                   );
  Procedure Pr_Select_Cod_Software (
                                    PM_NOME_SISTEMA in HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
  					                     PM_COD_SISTEMA  OUT Number
                                   ) ;
  Procedure Pr_Select_Dt_Faturamento (
                                      PM_CURSOR IN OUT TP_CURSOR
                                     );
END PCK_VDA070;
/
CREATE OR REPLACE PACKAGE BODY PRODUCAO.PCK_VDA070 IS


  PROCEDURE PR_SENHA(p_cursor    IN OUT tp_cursor,
                     p_login     IN     helpdesk.usuario.login%TYPE,
                     p_senha     IN     VARCHAR2,
                     p_erro     OUT    NUMBER)
  IS

  BEGIN

    p_erro := 0;


    --Abrindo Cursor
    OPEN p_cursor FOR Select cod_usuario
 		      from helpdesk.usuario
 		       where
 			 PRODUCAO.DESCRIPTOGRAFA_SENHA(SENHA) = p_senha AND
 			 upper(login) = upper(p_login);


    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;


  END;



  PROCEDURE PR_ATUALIZA_LOG(p_cod_loja     IN     pednota_venda.cod_loja%TYPE,
                     	   p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                    	   p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                    	   p_fl_lib        IN     VARCHAR2,
                    	   p_cod_usuario   IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_cod_usuario_r IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_erro          OUT    NUMBER)

  IS

   BEGIN

     p_erro := 0;


     Update producao.r_pedido_credito set
       fl_liberacao = p_fl_lib,
       dt_liberacao = sysdate,
       cod_usuario_credito = p_cod_usuario,
       cod_usuario_responsavel = p_cod_usuario_r
       where seq_pedido = p_seq_pedido and
         num_pedido = p_num_pedido and
         cod_loja= p_cod_loja;
     IF SQL%NOTFOUND THEN
       Insert into producao.r_pedido_credito(cod_loja,num_pedido,
         seq_pedido,fl_liberacao,dt_liberacao,cod_usuario_credito,
         cod_usuario_responsavel) values(p_cod_loja,p_num_pedido,
         p_seq_pedido,p_fl_lib,sysdate,p_cod_usuario,p_cod_usuario_r);

     END IF;
     COMMIT;




     EXCEPTION
     WHEN OTHERS THEN
     p_erro := SQLCODE;


  END;

  PROCEDURE PR_RESPONSAVEL(p_cursor    IN OUT tp_cursor,
                    	   p_erro     OUT     NUMBER)

   IS

    BEGIN

      p_erro := 0;


      --Abrindo Cursor
      OPEN p_cursor FOR select LTRIM(TO_CHAR(COD_USUARIO,'00000'),5) COD,NOME_USUARIO
		from helpdesk.usuario
		where tp_usuario IN ('G', 'T')
		AND FL_ATIVO='S'
		AND cod_usuario  not in (358,375,161,346,64,308,187,579)
		ORDER BY NOME_USUARIO;



      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;


  END;

  PROCEDURE PR_CONSULTA_LOG(p_cursor    IN OUT tp_cursor,
               	            p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
  			    p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                    	    p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
             		    p_erro          OUT     NUMBER)

  IS

    BEGIN

      p_erro := 0;


      --Abrindo Cursor
      OPEN p_cursor FOR select rpad(b.nome_usuario,15),a.fl_liberacao
		from r_pedido_credito a,
		helpdesk.usuario b
		WHERE a.seq_pedido = p_seq_pedido and
		a.num_pedido = p_num_pedido and
		a.cod_loja = p_cod_loja and
		a.cod_usuario_credito=b.cod_usuario;



      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;


  END;

  PROCEDURE PR_DELETA_LOG(p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
                    	  p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                    	  p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                    	  p_erro          OUT    NUMBER)

   IS

     BEGIN

       p_erro := 0;

       DELETE FROM PRODUCAO.R_PEDIDO_CREDITO
       WHERE FL_LIBERACAO IS NULL AND
          SEQ_PEDIDO = P_SEQ_PEDIDO AND
         NUM_PEDIDO = P_NUM_PEDIDO AND
         COD_LOJA = P_COD_LOJA;

       COMMIT;




       EXCEPTION
       WHEN OTHERS THEN
       p_erro := SQLCODE;


  END;

  Procedure Pr_Select_Saldo_Vencer (
                                    PM_CURSOR1 IN OUT TP_CURSOR,
                                    Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                                   ) is
     BEGIN
        OPEN Pm_Cursor1 FOR
             SELECT nvl(sum(vl_duplicata-vl_desconto-vl_abatimento-vl_devolucao-vl_pago),0) vencer
             FROM   cobranca.duplicatas,datas
             WHERE  cod_cliente= Pm_Cod_cli
             AND    situacao_pagto = 0
             AND    dt_vencimento > = dt_faturamento;
  END;

  Procedure Pr_Select_Saldo_Vencido (
                                     PM_CURSOR1 IN OUT TP_CURSOR,
                                     Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                                    ) is
     BEGIN
        OPEN Pm_Cursor1 FOR
             SELECT nvl(sum(vl_duplicata-vl_desconto-vl_abatimento-vl_devolucao-vl_pago),0) vencido
             FROM   cobranca.duplicatas,datas
             WHERE  cod_cliente= Pm_Cod_cli
             AND    situacao_pagto = 0
             AND    dt_vencimento < dt_faturamento;

  End;

  Procedure Pr_Select_Duplicatas (
                                  Pm_Cursor  IN OUT TP_CURSOR,
                                  Pm_Cod_Cli IN cobranca.duplicatas.cod_cliente%type
                                 ) is
     BEGIN
        OPEN PM_CURSOR FOR
           SELECT cod_loja,
                  num_fatura,
                  num_ordem,
                  dt_vencimento,
                  lpad(to_char(vl_duplicata-vl_desconto-vl_abatimento-vl_devolucao-vl_pago,'9999999999990.00'),17, '  ') vl ,
                  a.cod_banco,
                  c.sigla ,
                  a.tp_pagamento
           FROM   cobranca.duplicatas a ,
                  datas b,
                  banco c
           WHERE  cod_cliente = Pm_Cod_cli
           AND    situacao_pagto=0
           AND    dt_vencimento >= dt_faturamento
           AND    a.cod_banco=c.cod_banco
           ORDER BY
                  dt_vencimento,
                  cod_loja;
  End;

  Procedure Pr_Select_Boleto_OP (
                                 Pm_Cursor IN OUT TP_CURSOR,
                                 Pm_Deposito IN varchar2,
                                 Pm_Loja     IN COBRANCA.BOLETO_OP.COD_LOJA%type,
                                 Pm_Ped      IN COBRANCA.BOLETO_OP.NUM_PEDIDO%type,
                                 Pm_Seq      IN COBRANCA.BOLETO_OP.SEQ_PEDIDO%type
                                ) is
     BEGIN
          vSql := 'SELECT * FROM ';

          IF Pm_Deposito <> '01' Then
             vSql := vSql || 'dep' || Pm_deposito || '.BOLETO_OP ';
          Else
             vSql := vSql || 'COBRANCA.BOLETO_OP ';
          End If;

          vSql := vSql || 'WHERE COD_LOJA =' || Pm_LOJA || ' AND ';
          vSql := vSql || 'NUM_PEDIDO = ' || Pm_Ped || ' and ';
          vSql := vSql || 'SEQ_PEDIDO = ' || Pm_Seq;

     OPEN PM_CURSOR FOR
          vSql;
  End;

  Procedure Pr_Update_Cobranca_Boleto_OP (
                                          Pm_Deposito IN varchar2,
                                          Pm_Opt      IN Cobranca.Boleto_Op.Tp_Pgto%type,
                                          Pm_Loja     IN Cobranca.Boleto_Op.Cod_Loja%type,
                                          Pm_Ped      IN Cobranca.Boleto_Op.Num_Pedido%type,
                                          Pm_Seq      IN Cobranca.Boleto_Op.Seq_Pedido%type,
                                          Pm_Cod_Erro OUT Number,
                                          Pm_Txt_Erro OUT Varchar2
                                         ) is
     BEGIN
        Pm_Cod_Erro := 0;
        Pm_Txt_Erro := '';

        IF Pm_deposito <> '01' Then
            vSql := 'UPDATE dep' || Pm_deposito || '.BOLETO_OP ';
        Else
            vSql := 'UPDATE COBRANCA.BOLETO_OP ';
        End If;
        vSql := vSql || 'SET   TP_PGTO =' || chr(39) || PM_OPT || chr(39);
        vSql := vSql || ' WHERE COD_LOJA = ' || PM_LOJA ;
        vSql := vSql || ' AND   NUM_PEDIDO = ' || PM_PED;
        vSql := vSql || ' AND   SEQ_PEDIDO = ' || PM_SEQ;

        execute immediate vsql;

        COMMIT;

        EXCEPTION
           WHEN OTHERS THEN
                ROLLBACK;
                Pm_COD_ERRO:=SQLCODE;
                Pm_txt_erro:=SQLERRM;

  End;

  Procedure Pr_Insert_Cobranca_Boleto_OP (
                                          Pm_Deposito IN Varchar2,
                                          Pm_Loja     IN Cobranca.Boleto_Op.Cod_Loja%type,
                                          Pm_Ped      IN Cobranca.Boleto_Op.Num_Pedido%type,
                                          Pm_Seq      IN CObranca.Boleto_Op.Seq_Pedido%type,
                                          Pm_Sit      IN Cobranca.Boleto_Op.Situacao%type,
                                          Pm_Opt      IN Cobranca.Boleto_Op.Tp_Pgto%type,
                                          Pm_Cod_Erro OUT Number,
                                          Pm_Txt_Erro OUT Varchar2
                                         ) is
     BEGIN
        Pm_Cod_Erro := 0;
        Pm_Txt_Erro := '';

        If Pm_Deposito <> '01' Then
            vSql := 'INSERT INTO dep' || Pm_Deposito || '.BOLETO_OP ';
        Else
            vSql := 'INSERT INTO COBRANCA.BOLETO_OP ';
        End If;

        vSql := vSql || '(COD_LOJA,NUM_PEDIDO,SEQ_PEDIDO,SITUACAO,TP_PGTO) ';
        vSql := vSql || 'VALUES (' || Pm_Loja || ',' || PM_PED || ',' || PM_SEQ || ',' || PM_SIT || ',' || chr(39) || PM_OPT || chr(39) || ') ';

        execute immediate vSql;

        COMMIT;

        EXCEPTION
        WHEN OTHERS THEN
           ROLLBACK;
           Pm_COD_ERRO :=SQLCODE;
           Pm_TXT_ERRO :=SQLERRM;
  End;

  Procedure Pr_Select_Loja (
                            Pm_Cursor IN OUT TP_CURSOR
                           ) is
     BEGIN
         OPEN PM_CURSOR FOR
           SELECT a.cod_loja,
                  a.nome_fantasia
           FROM   loja a,
                  deposito_visao b
           WHERE  a.cod_loja=b.cod_loja
           AND    nome_programa='VDA070';

  End;

  Procedure Pr_Select_Pedidos (
                               PM_CURSOR IN OUT TP_CURSOR,
                               Pm_Usuario IN Varchar2,
                               Pm_Loja    IN PedNota_venda.Cod_Loja%type
                              ) is
     BEGIN
         IF Pm_Usuario = 'SUPCRED' THEN
            OPEN PM_CURSOR FOR
                 SELECT   a.bloq_juros,
                          a.bloq_protesto,
                          a.bloq_limite,
                          a.bloq_compra,
                          a.bloq_cheque,
                          a.bloq_duplicata,
                          a.bloq_saldo,
                          a.bloq_clie_novo,
                          a.num_pedido ,
                          a.seq_pedido,
                          a.cod_cliente,
                          b.nome_cliente ,
                          c.nome_transp
                  FROM    pednota_venda a ,
                          cliente b,
                          transportadora c
                  WHERE   a.situacao=0
                  AND     a.fl_ger_nfis='N'
                  AND     a.bloq_duplicata ='S'
                  AND     a.bloq_clie_novo='N'
                  AND     a.bloq_compra='N'
                  AND     a.bloq_protesto='N'
                  AND     a.bloq_juros = 'N'
                  AND     a.bloq_limite = 'N'
                  AND     a.bloq_saldo='N'
                  AND     ((a.bloq_desconto='L'
                          OR a.bloq_desconto ='N')
                          OR (a.bloq_acrescimo='L'
                          OR a.bloq_acrescimo='N')
                          OR (a.bloq_vlfatmin='L'
                          OR a.bloq_vlfatmin='N')
                          OR (a.bloq_item_desc1='L'
                          OR a.bloq_item_desc1='N')
                          OR (a.bloq_item_desc2='L'
                          OR a.bloq_item_desc2='N')
                          OR (a.bloq_item_desc3='L'
                          OR a.bloq_item_desc3='N'))
                          OR b.cod_cliente=a.cod_cliente
                  AND     c.cod_transp=a.cod_transp
                  AND     a.cod_plano=69
                  AND     a.cod_loja= Pm_loja;
        ELSE
            OPEN PM_CURSOR FOR
                  SELECT  a.dt_digitacao,
                          a.cod_cliente,
                          c.nome_cliente,
                          a.cod_repres,
                          a.cod_vend,
                          a.bloq_protesto,
                          a.bloq_juros,
                          a.bloq_cheque,
                          a.bloq_compra,
                          a.bloq_duplicata,
                          a.bloq_limite,
                          a.bloq_saldo,
                          e.pseudonimo,
                          b.nome_transp,
                          a.num_pedido,
                          a.seq_pedido,
                          a.cod_loja,
                          A.VL_CONTABIL
                  FROM    pednota_venda a,
                          transportadora b,
                          cliente c,
                          representante d,
                          representante e,
                          filial f
                  WHERE   a.situacao=0
                  AND     a.fl_ger_nfis='N'
                  AND     a.cod_loja= Pm_loja
                  AND     (a.bloq_clie_novo='S'
                          OR a.bloq_compra='S'
                          OR a.bloq_protesto='S'
                          OR a.bloq_juros='S'
                          OR a.bloq_cheque='S'
                          OR a.bloq_duplicata='S'
                          OR a.bloq_limite='S'
                          OR a.bloq_saldo='S')
                  AND     (( a.bloq_desconto = 'L'
                          OR a.bloq_desconto = 'N')
                  AND     (a.bloq_acrescimo = 'L'
                          OR a.bloq_acrescimo = 'N')
                  AND     (a.bloq_vlfatmin = 'L'
                          OR a.bloq_vlfatmin = 'N')
                  AND     (a.bloq_item_desc1 = 'L'
                          OR a.bloq_item_desc1 =  'N' )
                  AND     (a.bloq_item_desc2 = 'L'
                          OR a.bloq_item_desc2 =  'N' )
                  AND     (a.bloq_item_desc3 = 'L'
                          OR  a.bloq_item_desc3 =  'N' ))
                  AND     b.cod_transp=a.cod_transp
                  AND     c.cod_cliente=a.cod_cliente
                  AND     f.cod_filial=d.cod_filial
                  AND     (a.cod_repres=d.cod_repres
                          OR a.cod_vend=d.cod_repres)
                  AND     e.cod_repres=decode(d.seq_franquia,0,d.cod_repres,f.cod_franqueador)
                  ORDER BY
                          a.dt_digitacao;
         END IF;
  END;

  Procedure Pr_Select_Ordenando_Pedidos (
                                         Pm_Cursor IN OUT TP_CURSOR,
                                         Pm_COL    IN Number
                                        ) is

     BEGIN
          vSql := 'SELECT a.dt_digitacao,
                          a.cod_cliente,
                          c.nome_cliente,
                          a.cod_repres,
                          a.cod_vend,
                          a.bloq_protesto,
                          a.bloq_juros,
                          a.bloq_cheque,
                          a.bloq_compra,
                          a.bloq_duplicata,
                          a.bloq_limite,
                          a.bloq_saldo,
                          e.pseudonimo,
                          b.nome_transp,
                          a.num_pedido,
                          a.seq_pedido,
                          a.cod_loja
                   FROM   pednota_venda a,
                          transportadora b,
                          cliente c,
                          representante d,
                          representante e,
                          filial f
                   WHERE  a.situacao=0
                   AND    a.fl_ger_nfis=' || chr(39) || 'N' || chr(39);
          vSql := vSql || ' AND a.cod_loja=1 ';
          vSql := vSql || ' AND (a.bloq_clie_novo=' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_compra='  || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_protesto='|| chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_juros='|| chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_cheque='|| chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_duplicata='|| chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_limite='|| chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_saldo='|| chr(39) || 'S' || chr(39) || ')';
          vSql := vSql || ' AND (( a.bloq_desconto = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR  a.bloq_desconto = '|| chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.bloq_acrescimo = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_acrescimo = '|| chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.bloq_vlfatmin = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_vlfatmin = '|| chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.bloq_item_desc1 = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc1 =  '|| chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.bloq_item_desc2 = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc2 =  '|| chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.bloq_item_desc3 = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc3 =  '|| chr(39) || 'N' || chr(39) || '))';
          vSql := vSql || ' AND    b.cod_transp=a.cod_transp
                            AND    a.cod_cliente= c.cod_cliente
                            AND    (d.cod_repres=a.cod_vend
                                   OR d.cod_repres=a.cod_repres)
                            AND    f.cod_filial=d.cod_filial
                            AND    e.cod_repres=decode(d.seq_franquia,0,d.cod_repres,f.cod_franqueador)';

        If PM_COL = 1 Then
            vSql := vSql || ' order by C.COD_CLIENTE ';
        ElsIf PM_COL = 4 Then
            vSql := vSql || ' order by B.NOME_TRANSP ';
        ElsIf PM_COL = 2 Then
            vSql := vSql || ' order by C.NOME_CLIENTE';
        End If;

        OPEN PM_CURSOR FOR
             vSql;
  End;

  Procedure Pr_Select_Duplicatas_2 (
                                    PM_Cursor IN OUT TP_CURSOR,
                                    PM_Cli    IN Cobranca.Duplicatas.Cod_Cliente%type
                                   ) is
     BEGIN
        OPEN PM_CURSOR FOR
            SELECT cod_loja,
                   num_fatura,
                   num_ordem ,
                   cod_banco,
                   dt_vencimento,
                   vl_duplicata,
                   to_char((vl_desconto + vl_abatimento + vl_devolucao),'fm9999999999990.00') vl1,
                   to_char((vl_juros + vl_despesas),'fm9999999999990.00') vl2,
                   decode(situacao_pagto,0,decode(datas.dt_faturamento-dt_vencimento,
                   abs(datas.dt_faturamento-dt_vencimento), datas.dt_faturamento-dt_vencimento,NULL),
                   1,decode(dt_baixa_banco-dt_vencimento,abs(dt_baixa_banco-dt_vencimento),
                   dt_baixa_banco-dt_vencimento,NULL)) sit
            FROM   cobranca.duplicatas,
                   datas
            WHERE  cod_cliente= Pm_cli
            AND    situacao_pagto=9
            ORDER BY
                   duplicatas.dt_emissao_dupl desc;

  End;

  Procedure Pr_Select_Nome_Cliente (
                                    Pm_CURSOR IN OUT TP_CURSOR,
                                    Pm_Cli    IN Cliente.Cod_Cliente%Type
                                   ) is

     BEGIN
        OPEN PM_CURSOR FOR
             SELECT nome_cliente
             FROM   cliente
             WHERE  cod_cliente= PM_cli;
  End;

  Procedure Pr_Select_Duplicatas_3 (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    Pm_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                   ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT to_char(sum(a.vl_duplicata-(a.vl_desconto+a.vl_abatimento+a.vl_devolucao+a.vl_pago)),'999999999990.99') venc
               FROM   cobranca.duplicatas a,
                      datas b
               WHERE  a.cod_cliente= Pm_cli
               AND    a.situacao_pagto = 0
               AND    b.dt_faturamento > a.dt_vencimento;

  End;
  Procedure Pr_Select_Dupl_Em_Aberto (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      Pm_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                     ) is
     BEGIN
        OPEN PM_CURSOR FOR
              SELECT to_char(sum(vl_duplicata - (vl_desconto + vl_devolucao + vl_abatimento + vl_pago)), 'fm9999999999990.99') aberto
              FROM   cobranca.duplicatas
              WHERE  cod_cliente= Pm_cli
              AND    situacao_pagto = 0;
  End;

  Procedure Pr_Select_Faturas_Cps (
                                   PM_CURSOR IN OUT TP_CURSOR,
                                   PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                  ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT cod_loja,
                      num_fatura,
                      lpad(to_char(sum(vl_duplicata),'9999999999990.00'),17,' ') vl ,
                      dt_emissao_dupl,
                      nvl(desc_plano,0) desc_plano
               FROM   cobranca.duplicatas,
                      dolar_diario,
                      plano_pgto
               WHERE  cod_cliente = Pm_cli
               AND    data_uss=dt_emissao_dupl
               AND    plano_pgto.cod_plano(+)=duplicatas.cod_plano
               AND    situacao_pagto <> 9
               AND    tp_duplicata in (0,7)
               GROUP BY
                      cod_loja ,
                      num_fatura,
                      dt_emissao_dupl,
                      desc_plano
               ORDER BY
                      dt_emissao_dupl desc;
  End;

  Procedure Pr_Update_PedNota_Venda (
                                     Pm_Deposito IN Varchar2,
                                     Pm_Data_Fat IN Varchar2,
                                     Pm_Ped      IN PedNota_venda.Num_Pedido%type,
                                     Pm_Loja     IN PedNota_Venda.Cod_Loja%type,
                                     Pm_Cod_Erro OUT Number,
                                     Pm_Txt_Erro OUT varchar2
                                    ) is
     BEGIN
        Pm_Cod_Erro := 0;
        Pm_Txt_Erro := '';

        vSql := 'UPDATE ';

        IF Pm_Deposito <> '01' Then
            vSql := vSql || ' dep' || Pm_Deposito || '.pednota_venda ';
        Else
            vSql := vSql || 'pednota_venda ';
        End If;
        vSql := vSql || 'SET bloq_protesto=decode(bloq_protesto,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '),';
        vSql := vSql || 'bloq_juros=decode(bloq_juros,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_cheque=decode(bloq_cheque,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_compra=decode(bloq_compra,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_duplicata=decode(bloq_duplicata,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_limite=decode(bloq_limite,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_saldo=decode(bloq_saldo,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'bloq_clie_novo=decode(bloq_clie_novo,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
        vSql := vSql || 'dt_bloq_credito=to_date(' || Pm_data_fat || ' to_char(sysdate,' || CHR(39) || 'HH24MISS' || chr(39) || ')' || ',' || chr(39) || 'DD/MM/RRHH24MISS' || chr(39) || ') ';
        vSql := vSql || 'WHERE num_pedido=' || Pm_Ped ;
        vSql := vSql || 'AND cod_loja= ' || Pm_loja;
        vSql := vSql || 'AND situacao = 0';

        Execute Immediate vsql;

        COMMIT;

        EXCEPTION
        WHEN OTHERS THEN
             ROLLBACK;
             Pm_COD_ERRO:=SQLCODE;
             Pm_TXT_ERRO:=SQLERRM;
  End;

  Procedure Pr_Select_Para_Grafico (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Cobranca.duplicatas.cod_cliente%type
                                   ) is
     BEGIN
        OPEN PM_CURSOR FOR
           SELECT lpad(to_char(sum(vl_duplicata),'9999999999990.00'),17,' ') vl ,
                  to_char(trunc(dt_emissao_dupl),'YYYYMM') Mes
           FROM   cobranca.duplicatas,
                  dolar_diario,
                  plano_pgto
           Where  cod_cliente = PM_CLI
           AND    data_uss=dt_emissao_dupl
           AND    plano_pgto.cod_plano(+)=duplicatas.cod_plano
           AND    situacao_pagto <> 9
           AND    tp_duplicata in (0,7)
           GROUP BY
                  to_char(trunc(dt_emissao_dupl),'YYYYMM')
           ORDER BY
                  to_char(trunc(dt_emissao_dupl),'YYYYMM');
  End;

  Procedure Pr_Select_Pedidos_Indent(
                                     Pm_Cursor IN OUT TP_CURSOR,
                                     Pm_Cod    IN Indent.Pedido_Indent.Cod_Cliente%type
                                    ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT dt_emissao_nota,
                     num_nota,
                     vl_contabil,
                     desc_plano,
                     desc_tipo
              FROM   indent.pedido_indent p,
                     indent.tipo_indent T
              WHERE p.tp_indent=t.cod_tipo
              AND   situacao=0
              AND   fl_ger_nfis='S'
              AND   cod_cliente = Pm_Cod
              Order By Dt_Emissao_Nota desc;
  End;

  Procedure Pr_Select_Cliente (
                               PM_CURSOR IN OUT TP_CURSOR,
                               PM_CLI    IN Cliente.Cod_Cliente%type
                              ) is
     BEGIN
        OPEN PM_CURSOR FOR
         SELECT a.nome_cliente,
                a.cgc,
                a.classificacao,
                a.endereco,
                a.ddd1,
                a.ddd2,
                a.fone1,
                a.fone2,
                b.nome_cidade,
                b.cod_uf ,
                a.cod_repr_vend,
                a.cod_repres_blau
         FROM   cliente a,
                cidade b,
                tipo_cliente c
         WHERE  cod_cliente = Pm_cli
         AND    cod_tipo_cli(+)=cod_tipo_cliente
         AND    a.cod_cidade=b.cod_cidade;

  End;

  Procedure Pr_Select_Cliente_2 (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 PM_CLI    IN Cliente.Cod_Cliente%type
                                ) is
     BEGIN
         OPEN PM_CURSOR FOR
               SELECT c.endereco,
                      c.bairro,
                      nome_contato,
                      cod_repr_vend,
                      dt_fundacao,
                      propriedade,
                      dt_cadastr,
                      o.maior_fatura,
                      o.dt_maior_fatura,
                      o.maior_saldev,
                      o.nota_credito,
                      o.media_compra,
                      o.atraso_medio,
                      o.intervalo_atr_medio,
                      m.desc_mens,
                      c.nome_respons,
                      c.dt_mensagem,
                      c.situacao,
                      t.cod_segmento,
                      s.desc_segmento,
                      b.cod_banco,
                      b.sigla
               FROM   cliente c,
                      cidade e,
                      banco b,
                      clie_credito o,
                      clie_mensagem m,
                      tipo_cliente t,
                      segmento s
               WHERE  c.cod_cliente = Pm_cli
               AND    e.cod_cidade(+)=c.cod_cidade
               AND    b.cod_banco(+)=c.cod_banco
               AND    o.cod_cliente(+) = c.cod_cliente
               AND    m.cod_mensagem(+)=c.cod_mensagem
               AND    t.cod_tipo_cli(+)=c.cod_tipo_cliente;

  End;

  Procedure Pr_Select_Maior_Fatura (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Clie_credito.Cod_Cliente%type
                                   ) is
     BEGIN
        OPEN PM_CURSOR FOR
             SELECT f.valor_uss,
                    o.maior_fatura
             FROM   clie_credito o,
                    dolar_diario f ,
                    datas d
             WHERE  o.cod_cliente = Pm_cli
             AND    ( f.data_uss=o.dt_maior_fatura
                    OR f.data_uss=d.dt_faturamento );

  End;

  Procedure Pr_Select_Maior_Saldo_Devedor (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN OUT Cliente.Cod_Cliente%type
                                          ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT s.valor_uss,
                      o.maior_saldev
               FROM   cliente c,
                      clie_credito o,
                      dolar_diario s,
                      datas d
               WHERE  c.cod_cliente = PM_cli
               AND    o.cod_cliente(+) = c.cod_cliente
               AND    (s.data_uss=o.dt_maior_saldev
                      OR s.data_uss=d.dt_faturamento );
  End;

  Procedure Pr_Select_Saldo_Pedidos_CPS (
                                         PM_CURSOR IN OUT TP_CURSOR,
                                         PM_CLI    IN v_saldo_pedidos.cod_cliente%type
                                        ) is

     BEGIN
          OPEN PM_CURSOR FOR
               SELECT saldo_pedidos
               FROM   v_saldo_pedidos
               WHERE  cod_cliente= Pm_cli
               AND    cod_loja = 1;
  End;

  Procedure Pr_Select_Saldo_Pedidos (
                                     PM_CURSOR  IN OUT TP_CURSOR,
                                     PM_CODCLI  IN v_saldo_pedidos.cod_cliente%type,
                                     PM_DEPDEST IN varchar2
                                    ) is

     BEGIN
           vSql := 'SELECT saldo_pedidos ';
           vSql := vSql || 'FROM   dep' || Pm_depdest || '.v_saldo_pedidos ';
           vSql := vSql || 'WHERE  cod_cliente= ' || Pm_codcli;
           vSql := vSql || ' and cod_loja = ' || Pm_depdest;

         OPEN PM_CURSOR FOR
              vSql;

  End;

  Procedure Pr_Select_SaldoVencer_Venc_Cps (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                           ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT nvl(sum(vl_duplicata-(vl_desconto+vl_abatimento+vl_devolucao+vl_pago) ),0) vencer
               FROM   cobranca.duplicatas,
                      datas
               WHERE  cod_cliente= Pm_cli
               AND    situacao_pagto = 0
               AND    dt_vencimento > = dt_faturamento;
  End;
  Procedure Pr_Select_Saldo_Vencidas (
                                      Pm_CURSOR IN OUT TP_CURSOR,
                                      PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                     ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT nvl(sum(vl_duplicata-(vl_desconto+vl_abatimento+vl_devolucao+vl_pago) ),0) vencida
              FROM   cobranca.duplicatas,
                     datas
              WHERE  cod_cliente= Pm_cli
              AND    situacao_pagto = 0
              AND    dt_vencimento < dt_faturamento;
  End;

  Procedure Pr_Select_limite_Credito (
                                      PM_CURSOR IN OUT TP_CURSOR
                                     ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT pc_nota_inic,
                     pc_nota_final,
                     pc_limite
              FROM   pont_credito
              ORDER BY pc_limite;
  End;

  Procedure Pr_Select_Dupl_Ab_Venc_Vencer (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT nvl(sum(vl_duplicata-(vl_desconto+vl_abatimento+vl_devolucao+vl_pago) ),0) Val
              FROM   cobranca.duplicatas d,
                     plano_pgto p
              WHERE  cod_cliente= Pm_cli
              AND    situacao_pagto = 0
              AND    d.cod_plano=p.cod_plano
              AND    p.fl_vendor='S';
  End;

  Procedure Pr_Select_Saldo_Pedido_Vendor (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT saldo_pedidos_vendor
              FROM   v_saldo_pedidos
              WHERE  cod_cliente= Pm_cli;
  END;

/*  Procedure Pr_Update_PedNota_Venda_Libera (
                                            Pm_Deposito IN Varchar2,
                                            Pm_Ped      IN Pednota_Venda.Num_Pedido%type,
                                            Pm_Seq      IN Pednota_Venda.Seq_Pedido%type,
                                            Pm_Dt_Fat   IN Varchar2,
                                            Pm_Loja     IN PedNota_venda.Cod_Loja%type,
                                            Pm_Cod_Erro OUT Number,
                                            Pm_Txt_Erro OUT Varchar2
                                           ) is

     BEGIN
          Pm_Cod_Erro := 0;
          Pm_Txt_Erro := '';

          IF Pm_deposito <> '01' Then
             vSql := 'UPDATE dep' || Pm_deposito || '.pednota_venda ';
          Else
             vSql := 'UPDATE pednota_venda ';
          End If;

         vSql := vSql || 'SET bloq_protesto=decode(bloq_protesto,' || Chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_juros=decode(bloq_juros,'|| Chr(39) || 'S'|| Chr(39) || ','|| Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_cheque=decode(bloq_cheque,'|| Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_compra=decode(bloq_compra,'|| Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_duplicata=decode(bloq_duplicata,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_limite=decode(bloq_limite,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_saldo=decode(bloq_saldo,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_clie_novo=decode(bloq_clie_novo,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'dt_bloq_credito=to_date(' || Pm_dt_fat || ',' || chr(39) || 'DD-MM-YY' || chr(39) || ') ||' || chr(39) || ' ' || chr(39) || ' || to_char(sysdate,' || chr(39) || 'HH24MISS' || chr(39) || '),' || chr(39) || 'DD/MM/RRHH24MISS' || chr(39) || ') ';
         vSql := vSql || 'WHERE num_pedido=' || Pm_ped ;
         vSql := vSql || ' AND  seq_pedido=' || Pm_seq ;
         vSql := vSql || ' AND  cod_loja=' || Pm_loja ;
         vSql := vSql || ' AND  situacao = 0';

         execute immediate vSql;

         COMMIT;

         EXCEPTION
            WHEN OTHERS THEN
                 ROLLBACK;
                 Pm_Cod_ERRO:=SQLCODE;
                 Pm_Txt_ERRO:=SQLERRM;
  End;
*/
 Procedure Pr_Update_PedNota_Venda_Libera (
                                            Pm_Deposito IN Varchar2,
                                            Pm_Ped      IN Pednota_Venda.Num_Pedido%type,
                                            Pm_Seq      IN Pednota_Venda.Seq_Pedido%type,
                                            Pm_Dt_Fat   IN Varchar2,
                                            Pm_Loja     IN PedNota_venda.Cod_Loja%type,
                                            Pm_Cod_Erro OUT Number,
                                            Pm_Txt_Erro OUT Varchar2
                                           ) is

     BEGIN
          Pm_Cod_Erro := 0;
          Pm_Txt_Erro := '';

          IF Pm_deposito <> '01' Then
             vSql := 'UPDATE dep' || Pm_deposito || '.pednota_venda ';
          Else
             vSql := 'UPDATE pednota_venda ';
          End If;

         vSql := vSql || 'SET bloq_protesto=decode(bloq_protesto,' || Chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_juros=decode(bloq_juros,'|| Chr(39) || 'S'|| Chr(39) || ','|| Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_cheque=decode(bloq_cheque,'|| Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_compra=decode(bloq_compra,'|| Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_duplicata=decode(bloq_duplicata,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_limite=decode(bloq_limite,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'bloq_saldo=decode(bloq_saldo,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || chr(39) || '), ';
         vSql := vSql || 'bloq_clie_novo=decode(bloq_clie_novo,' || Chr(39) || 'S' || Chr(39) || ',' || Chr(39) || 'L' || Chr(39) || ',' || Chr(39) || 'N' || Chr(39) || '), ';
         vSql := vSql || 'dt_bloq_credito=to_date(to_date(' || chr(39) || Pm_dt_fat || chr(39) || ',' || chr(39) || 'DD-MM-YY' || chr(39) || ') || ' || chr(39) || ' ' || chr(39) || ' || to_char(sysdate,' || chr(39) || 'HH24MISS' || chr(39) || '),' || chr(39) || 'DD/MM/RR HH24MISS' || chr(39) || ') ';
         vSql := vSql || 'WHERE num_pedido=' || Pm_ped ;
         vSql := vSql || ' AND  seq_pedido=' || Pm_seq ;
         vSql := vSql || ' AND  cod_loja=' || Pm_loja ;
         vSql := vSql || ' AND  situacao = 0';

         execute immediate vSql;

         COMMIT;

         EXCEPTION
            WHEN OTHERS THEN
                 ROLLBACK;
                 Pm_Cod_ERRO:=SQLCODE;
                 Pm_Txt_ERRO:=SQLERRM;
  End;

  Procedure Pr_Select_Conexao (
                               PM_CURSOR IN OUT TP_CURSOR,
                               PM_Deposito IN Producao.Loja_Conexao.Cod_Loja%type
                              ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT *
               FROM   producao.loja_conexao
               WHERE  cod_loja= Pm_deposito;
  End;

  Procedure Pr_Select_BL_CL(
                            Pm_Cursor   IN OUT TP_CURSOR,
                            Pm_Deposito IN varchar2,
                            Pm_Loja     Number
                           ) is
     BEGIN
          vSql := 'SELECT a.blcl FROM dep'  || Pm_Deposito || '.v_pednota a ';
          vSql := vSql || 'WHERE a.fgnf =' || Chr(39) || 'N' || chr(39);
          vSql := vSql || ' and a.loj =' || Pm_loja;
          vSql := vSql || ' and a.blcl = ' || chr(39) || 'S' || ' and a.sit = 0';

          OPEN PM_CURSOR FOR
               vSql;
  End;

  Procedure Pr_Select_Cliente (
                               PM_CURSOR       IN OUT TP_CURSOR,
                               PM_Total_Pedido IN Number,
                               Pm_Deposito     IN Varchar2,
                               Pm_Loja         Number
                              ) is
     BEGIN
          IF PM_Total_pedido = 0 Then
             vSql := 'SELECT a.dtped,a.ven,a.rep, a.cli,';
             vSql := vSql || 'a.blcl, a.blpr, a.blju,a.blch,a.blco,a.bldu,a.blli,a.blsa , ';
             vSql := vSql || 'a.pla, a.ban, a.tra, a.nped, a.sped, a.loj, c.nome_cliente,p.fl_avista ';
             vSql := vSql || 'FROM dep' || Pm_Deposito || '.v_pednota a , cliente c, plano_pgto p ';
             vSql := vSql || 'WHERE a.fgnf = ' || chr(39) || 'N' || chr(39);
             vSql := vSql || ' AND  a.loj = ' || Pm_Loja;
             vSql := vSql || ' AND (a.blcl = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blco = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blpr = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blju = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blch = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.bldu = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blli = ' || chr(39) || 'S' || chr(39);
             vSql := vSql || '      OR a.blsa = ' || chr(39) || 'S' || chr(39) || ')';
             vSql := vSql || ' AND (( a.blde = ' || chr(39) || 'L' || chr(39) || ' OR a.blde = ' || chr(39) || 'N' || chr(39) || ') ';
             vSql := vSql || ' AND (a.blac = ' || chr(39) || 'L' || chr(39) || ' OR a.blac = ' || chr(39) || 'N' || chr(39) || ') ';
             vSql := vSql || ' AND (a.blvl = ' || chr(39) || 'L' || chr(39) || ' OR a.blvl = ' || chr(39) || 'N' || chr(39) || ') ';
             vSql := vSql || ' AND (a.blit1 = ' || chr(39) || 'L' || chr(39) || ' OR a.blit1 = ' || chr(39) || 'N' || chr(39) || ') ';
             vSql := vSql || ' AND (a.blit2 = ' || chr(39) || 'L' || chr(39) || ' OR a.blit2 = ' || chr(39) || 'N' || chr(39) || ') ';
             vSql := vSql || ' AND (a.blit3 = ' || chr(39) || 'L' || chr(39) || ' OR a.blit3 = ' || chr(39) || 'N' || chr(39) || ')) ';
             vSql := vSql || ' AND a.sit = 0 and a.cli=c.cod_Cliente ';
             vSql := vSql || ' AND a.pla=p.cod_plano and p.fl_avista<>' || chr(39) || 'S' || chr(39);
             vSql := vSql || ' AND a.tpt = 1 ';
             vSql := vSql || ' ORDER BY a.dtdig ';

          Else

              vSql := 'SELECT a.dtped,a.ven,a.rep, a.cli, ';
              vSql := vSql || ' a.blcl, a.blpr, a.blju,a.blch,a.blco,a.bldu,a.blli,a.blsa , ';
              vSql := vSql || ' a.pla, a.ban, a.tra, a.nped, a.sped, a.loj, c.nome_cliente,p.fl_avista ';
              vSql := vSql || ' from dep' || Pm_Deposito || '.v_pednota a , cliente c, plano_pgto p';
              vSql := vSql || ' where a.fgnf = ' || chr(39) || 'N' || chr(39);
              vSql := vSql || ' AND a.loj =' || Pm_loja;
              vSql := vSql || ' AND (a.blcl =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blco =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blpr =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blju =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blch =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.bldu =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blli =' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' OR a.blsa =' || chr(39) || 'S' || chr(39) || ') ';
              vSql := vSql || ' AND (( a.blde =' || chr(39) || 'L' || chr(39) || ' OR a.blde =' || chr(39) || 'N' || chr(39) || ') ';
              vSql := vSql || ' AND (a.blac =' || chr(39) || 'L' || chr(39) || ' or a.blac =' || chr(39) || 'N' || chr(39) || ') ';
              vSql := vSql || ' AND (a.blvl =' || chr(39) || 'L' || chr(39) || ' or a.blvl =' || chr(39) || 'N' || chr(39) || ') ';
              vSql := vSql || ' AND (a.blit1 =' || chr(39) || 'L' || chr(39) || ' or a.blit1 =' || chr(39) || 'N' || chr(39) || ') ';
              vSql := vSql || ' AND (a.blit2 =' || chr(39) || 'L' || chr(39) || ' or a.blit2 =' || chr(39) || 'N' || chr(39) || ') ';
              vSql := vSql || ' AND (a.blit3 =' || chr(39) || 'L' || chr(39) || ' or a.blit3 =' || chr(39) || 'N' || chr(39) || ')) ';
              vSql := vSql || ' AND a.sit = 0  and a.cli = c.cod_cliente ';
              vSql := vSql || ' AND a.pla=p.cod_plano and p.fl_avista<> ' || chr(39) || 'S' || chr(39);
              vSql := vSql || ' AND a.tpt = 1 ';
              vSql := vSql || ' order by a.dtdig ';
          End If;

          OPEN PM_CURSOR FOR
               vSql;
  End;

  Procedure Pr_Select_INFO_PEDIDOS (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    Pm_Ped    IN Pednota_Venda.Num_Pedido%type,
                                    Pm_Seq    IN Pednota_Venda.Seq_Pedido%type,
                                    Pm_Dep    IN Pednota_Venda.Cod_Loja%type,
                                    Pm_Cli    IN Pednota_Venda.Cod_Cliente%type
                                   ) is
     BEGIN
          OPEN PM_CURSOR FOR
                SELECT a.vl_contabil,
                       a.cod_cliente,
                       p.desc_plano,
                       c.cgc,
                       c.nome_cliente,
                       c.endereco,
                       c.bairro,
                       c.cep,
                       c.inscr_estadual,
                       c.fax,
                       c.ddd1,
                       c.fone1,
                       c.ddd2,
                       c.fone2,
                       c.nome_contato,
                       c.dt_fundacao,
                       c.propriedade,
                       c.nome_proprietario,
                       decode(a.tp_pedido,10,a.cod_repres,a.cod_vend) rep,
                       c.cod_tipo_cliente,
                       t.desc_tipo_cli,
                       c.cod_cidade,
                       e.nome_cidade,
                       e.cod_uf
                 FROM  cliente c,
                       cidade e,
                       tipo_cliente t,
                       plano_pgto p,
                       pednota_venda a
                WHERE  num_pedido= Pm_ped
                AND    seq_pedido= PM_SEQ
                AND    cod_loja= PM_Dep
                AND    c.cod_cliente = Pm_cli
                AND    e.cod_cidade=c.cod_cidade
                AND    t.cod_tipo_cli(+)=c.cod_tipo_cliente
                AND    p.cod_plano=a.cod_plano;

  End;

  Procedure Pr_Select_Infor_Representante (
                                           PM_CURSOR IN OUT TP_CURSOR,
                                           Pm_Rep    Representante.Cod_Repres%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT decode(a.seq_franquia,0,to_char(a.cod_repres,'fm0009') || '  ',
                     to_char(b.cod_franqueador ,'fm0009')) || to_char(a.seq_franquia,'fm09') ven,
                     c.pseudonimo
              FROM   representante a, representante c, filial b
              WHERE  a.cod_repres= Pm_rep
              AND    b.cod_filial = a.cod_filial
              AND    c.cod_repres=decode(a.seq_franquia,0,a.cod_repres,b.cod_franqueador);
  End;

  Procedure Pr_Select_Ref_Comerciais (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      Pm_CodCli IN Clie_Refer.Cod_Cliente%type
                                     ) is
    BEGIN
        OPEN PM_CURSOR FOR
             SELECT empresa,
                    observacao
             FROM   clie_refer
             WHERE  cod_cliente= Pm_Codcli;

  End;

  Procedure Pr_Select_Ref_Bancarias (
                                     PM_CURSOR IN OUT TP_CURSOR,
                                     Pm_Cod_Cli   IN Clie_Refbco.Cod_Cliente%type
                                    ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT banco,
                     agencia,
                     ddd,
                     fone
              FROM   clie_refbco
              WHERE  cod_cliente= Pm_Cod_Cli;
  End;

  Procedure Pr_Select_Depositos (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 Pm_Escolha   IN Number
                                ) is
     BEGIN
          vSql := 'SELECT a.cod_loja,
                          a.nome_fantasia
                   FROM   Loja a,
                          deposito_visao b
                   WHERE  a.cod_loja=b.cod_loja
                   AND    nome_programa=' || chr(39) || 'VDA070' || chr(39);

          IF PM_ESCOLHA = 5 THEN
             vSql := vSql || ' and a.cod_loja<>1';
          END IF;

          OPEN PM_CURSOR FOR
               vSql;
  End;

  Procedure Pr_Select_PedNota_Venda (
                                     Pm_Cursor IN OUT TP_CURSOR,
                                     Pm_Ped    IN Pednota_Venda.Num_Pedido%type,
                                     Pm_Seq    IN PednOta_venda.Seq_Pedido%type,
                                     Pm_Loja   IN PedNota_Venda.Cod_Loja%type,
                                     Pm_Dep    IN Varchar2
                                    ) is
     BEGIN
         vSql :='SELECT a.blcl,
                       a.blli,
                       a.bldu,
                       a.blco,
                       a.blju,
                       a.blpr,
                       a.blch,
                       a.blsa,
                       a.dtped,
                       a.ven,
                       a.rep,
                       a.cli,
                       a.ban,
                       a.vcon,
                       a.fre,
                       a.peso,
                       a.qtdp,
                       a.tra,
                       a.pla,
                       a.fdicm,
                       a.sit,
                       a.pdes,
                       a.pacr,
                       a.mpe,
                       a.mno,
                       a.tpd,
                       a.cli
                FROM   dep' || Pm_Dep || '.v_pednota a ';
         vSql := vSql || ' WHERE a.nped=' || Pm_ped;
         vSql := vSql || ' AND a.sped=' || Pm_seq;
         vSql := vSql || ' AND a.loj =' || Pm_loja;

         OPEN PM_CURSOR FOR
              vSql;
  End;

  Procedure Pr_Select_Divisao (
                                 PM_CURSOR IN OUT TP_CURSOR,
                                 Pm_Divisao IN Number
                                ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT descricao
               FROM   divisao
               WHERE  tp_divisao= Pm_Divisao;
  End;

  Procedure Pr_Select_Flags_Plano_Pagto (
                                         Pm_Cursor IN OUT TP_CURSOR,
                                         Pm_Plano  IN Plano_Pgto.Cod_Plano%type
                                        ) is
     BEGIN
         OPEN Pm_CURSOR FOR
              SELECT fl_vendor,
                     fl_avista
              FROM   plano_pgto pp
              WHERE  pp.cod_plano = Pm_Plano;
  End;

  Procedure Pr_Select_Info_Repres_Vend_CPS (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_BAN    IN Banco.Cod_Banco%type,
                                            Pm_Tra    IN transportadora.cod_transp%type,
                                            Pm_Pla    IN Plano_pgto.Cod_Plano%type,
                                            Pm_Rep    IN Representante.Cod_Repres%type,
                                            Pm_Cli    IN Cliente.Cod_Cliente%type
                                           ) is
     BEGIN
         OPEN PM_CURSOR FOR
           Select a.sigla,
                  b.nome_transp,
                  c.desc_plano,
                  d.pseudonimo,
                  e.cgc,
                  e.nome_cliente,
                  e.classificacao,
                  e.ddd1,
                  e.ddd2,
                  e.fone1,
                  e.fone2,
                  f.cod_uf,
                  f.nome_cidade
           FROM   banco a,
                  transportadora b,
                  plano_pgto c,
                  representante d,
                  cliente e,
                  cidade f
           WHERE   a.cod_banco= Pm_ban
           AND    b.cod_transp= Pm_tra
           AND    c.cod_plano= Pm_pla
           AND    d.cod_repres= Pm_rep
           AND    e.cod_cliente= Pm_cli
           AND    e.cod_cidade=f.cod_cidade;
  End;

  Procedure Pr_Select_Pseudonimo (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_rep    IN Representante.Cod_Repres%type
                                 ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT decode(a.seq_franquia,0,to_char(a.cod_repres,'fm0009')||'  ',to_char(b.cod_franqueador,'fm0009'))||to_char(a.seq_franquia,'fm09') cod,
                     c.pseudonimo
              FROM   representante a,
                     representante c,
                     filial b
              WHERE  a.cod_repres = Pm_rep
              AND    b.cod_filial=a.cod_filial
              AND    c.cod_repres =decode(a.seq_franquia,0,a.cod_repres,b.cod_franqueador);

  End;

  Procedure Pr_Select_Pseudonimo_Franquia (
                                           Pm_Cursor IN OUT TP_CURSOR,
                                           Pm_rep    IN Representante.Cod_Repres%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT decode(a.seq_franquia,0,to_char(a.cod_repres,'fm0009')||'  ',to_char(b.cod_franqueador,'fm0009'))||to_char(a.seq_franquia,'fm09') cod,
                     a.seq_franquia,
                     c.pseudonimo
              FROM   representante a,
                     representante c,
                     filial b
              WHERE  a.cod_repres= Pm_rep
              AND    b.cod_filial =a.cod_filial
              AND    c.cod_repres= decode(a.seq_franquia,0,a.cod_repres, b.cod_franqueador);
  End;

  Procedure Pr_Select_Dados_cliente (
                                     Pm_CURSOR      IN OUT TP_CURSOR,
                                     Pm_Cod_Cliente IN Cliente.Cod_Cliente%type
                                    ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT ddd1,
                      ddd2,
                      fone1,
                      fone2,
                      fax,
                      nome_cidade,
                      cod_uf,
                      banco.cod_banco,
                      banco.sigla
               FROM   cliente,
                      cidade,
                      banco
               WHERE  cliente.cod_cidade=cidade.cod_cidade
               AND    cod_cliente= Pm_cod_cliente
               AND    banco.cod_banco=cliente.cod_banco;

  End;

  Procedure Pr_Select_Plano_Pgto (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_cod_plano IN Plano_Pgto.Cod_Plano%type
                                 ) is
     BEGIN
         OPEN PM_CURSOR FOR
           SELECT *
           FROM   plano_pgto
           WHERE  cod_plano= Pm_cod_plano;
  End;

  Procedure Pr_Select_Dados_Transportadora (
                                            PM_CURSOR     IN OUT TP_CURSOR,
                                            Pm_cod_transp IN Transportadora.Cod_Transp%type
                                           ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT *
              FROM   transportadora
              WHERE  cod_transp= Pm_cod_transp;
  End;

  Procedure Pr_Select_Dados_Representante (
                                           Pm_Cursor IN OUT TP_CURSOR,
                                           Pm_Rep    IN Representante.Cod_Repres%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT decode(a.seq_franquia,0,to_char(a.cod_repres,'fm0009')||'  ',to_char(b.cod_franqueador,'fm0009'))||to_char(a.seq_franquia,'fm09') cod,
                     a.seq_franquia,
                     c.pseudonimo
              FROM   representante a,
                     representante c,
                     filial b
              WHERE  a.cod_repres= Pm_rep
              AND    b.cod_filial =a.cod_filial
              AND    c.cod_repres= decode(a.seq_franquia,0,a.cod_repres, b.cod_franqueador);

  End;

  Procedure Pr_Select_Pedido (
                              PM_CURSOR IN OUT TP_CURSOR,
                              PM_PED    IN Pednota_Venda.Num_Pedido%type,
                              Pm_SEQ    IN Pednota_venda.Seq_Pedido%type,
                              Pm_Loja   IN Pednota_venda.Cod_Loja%type
                             ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT a.bloq_clie_novo,
                     a.bloq_limite,
                     a.bloq_duplicata,
                     a.bloq_compra,
                     a.bloq_juros,
                     a.bloq_protesto,
                     a.bloq_cheque,
                     a.bloq_saldo,
                     a.dt_pedido,
                     a.cod_vend,
                     a.cod_repres,
                     a.cod_cliente,
                     a.cod_banco,
                     a.vl_contabil,
                     a.frete_pago,
                     a.peso_bruto,
                     a.qtd_item_pedido,
                     a.cod_transp,
                     a.cod_plano,
                     a.fl_dif_icm,
                     a.situacao,
                     a.pc_desconto,
                     a.pc_acrescimo,
                     a.mens_pedido,
                     a.mens_nota,
                     b.sigla,
                     c.nome_cliente,
                     c.cgc,
                     c.classificacao,
                     c.fone1,
                     c.ddd1,
                     c.fone2,
                     c.ddd2,
                     c.fax,
                     d.nome_cidade,
                     d.cod_uf,
                     t.nome_transp,
                     t.via_transp,
                     p.desc_plano,
                     p.fl_vendor,
                     o.cod_segmento,
                     s.desc_segmento,
                     a.tp_dpkblau
              FROM   pednota_venda a,
                     cliente c,
                     transportadora t,
                     banco b,
                     plano_pgto p,
                     cidade d,
                     tipo_cliente o,
                     segmento s
              WHERE  a.num_pedido= Pm_ped
              AND    a.seq_pedido= Pm_seq
              AND    a.cod_loja =  Pm_loja
              AND    c.cod_cliente=a.cod_cliente
              AND    d.cod_cidade=c.cod_cidade
              AND    t.cod_transp=a.cod_transp
              AND    p.cod_plano = a.cod_plano
              AND    b.cod_banco=a.cod_banco
              AND    o.cod_tipo_cli(+)=c.cod_tipo_cliente
              AND    s.cod_segmento(+)=o.cod_segmento;

  End;

  Procedure Pr_Select_Info_Transportadora (
                                           Pm_CURSOR IN OUT TP_CURSOR,
                                           Pm_Cod_Tra IN Transportadora.Cod_Transp%type
                                          ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT fl_retira_local,
                     fl_redespacho
              FROM   transportadora
              WHERE  cod_transp = Pm_cod_tra;
  End;

  Procedure Pr_Update_PedNota_Venda_GO_FO (
                                           Pm_Deposito IN Varchar2,
                                           Pm_Dt_Fat   IN Varchar2,
                                           Pm_Seq      IN Pednota_Venda.Seq_Pedido%type,
                                           Pm_Ped      IN PedNOta_Venda.Num_Pedido%type,
                                           Pm_Loja     IN PedNota_Venda.Cod_Loja%type,
                                           Pm_Cod_Erro OUT Number,
                                           Pm_Txt_Erro OUT Varchar2
                                          ) is
     BEGIN
            Pm_Cod_Erro := 0;
            Pm_Txt_Erro := '';

            IF Pm_Deposito <> 1 Then
               vSql := ' UPDATE dep' || Pm_Deposito || '.pednota_venda ';
            Else
               vSql := 'UPDATE pednota_venda ';
            End If;

            vSql := vSql || 'SET bloq_protesto=decode(bloq_protesto,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_juros=decode(bloq_juros,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_cheque=decode(bloq_cheque,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_compra=decode(bloq_compra,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ','  || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_duplicata=decode(bloq_duplicata,' || chr(39) || 'S' || chr(39) || ','  || chr(39) || 'L'  || chr(39) || ','  || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_limite=decode(bloq_limite,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_saldo=decode(bloq_saldo,' || chr(39) || 'S' || chr(39) || ',' || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    bloq_clie_novo=decode(bloq_clie_novo,' || chr(39) || 'S' || chr(39) || ','  || chr(39) || 'L' || chr(39) || ',' || chr(39) || 'N' || chr(39) || '), ';
            vSql := vSql || '    dt_bloq_credito=to_date(' || chr(39) || Pm_dt_fat || chr(39) || ' || to_char(sysdate,' || chr(39) || 'HH24MISS' || chr(39) || '),' || chr(39) || 'DD/MM/RRHH24MISS' || chr(39) || ')';
            vSql := vSql || ' WHERE num_pedido=' || Pm_ped;
            vSql := vSql || ' AND   seq_pedido=' || Pm_seq;
            vSql := vSql || ' AND   cod_loja=' || Pm_loja;
            vSql := vSql || ' AND   situacao = 0';

            execute immediate vSql;

            COMMIT;

            EXCEPTION
               WHEN OTHERS THEN
                    ROLLBACK;
                    PM_COD_ERRO:=SQLCODE;
                    PM_TXT_ERRO:=SQLERRM;
  End;

  Procedure Pr_Select_Pagamentos (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  Pm_Cli    IN Cobranca.Duplicatas.Cod_Cliente%type
                                 ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT DISTINCT a.cod_loja,
                              a.num_fatura,
                              a.num_ordem,
                              lpad(to_char(a.vl_pago_dolar,'fm999999999990.00'),15,'  ' ) vl,
                              to_char(a.dt_baixa_banco,'DD/MM/RR'),
                              to_char(decode(sign(a.dt_baixa_banco-a.dt_vencimento),-1,0,
                              (a.dt_baixa_banco-a.dt_vencimento)),'fm9990') atraso,
                              decode(decode(sign(a.dt_baixa_banco-a.dt_vencimento),-1,0,
                              (a.dt_baixa_banco-a.dt_vencimento)),0,' ' ,
                              decode(sign(15 - ((a.dt_baixa_banco-a.dt_vencimento)*
                              a.pc_juro_dias_atraso/100*a.vl_duplicata)),1,'S' ,
                              decode(sign((a.vl_juros+a.vl_despesas)-((a.dt_baixa_banco-a.dt_vencimento)
                              * a.pc_juro_dias_atraso / 100*a.vl_duplicata*0.25)),-1,
                              decode(a.tp_duplicata,0,decode(nvl(b.situacao_pagto,0),1,'S','N'),'N'),'S')))  juros ,
                              a.tp_pagamento,a.dt_baixa_banco
              FROM            cobranca.duplicatas a,
                              cobranca.duplicatas b
              WHERE           a.cod_cliente= Pm_cli
              AND             a.situacao_pagto=1
              AND             b.num_fatura_ant(+)=a.num_fatura
              AND             b.num_ordem_ant(+)=a.num_ordem
              AND             b.cod_loja_ant(+)=a.cod_loja
              AND             b.situacao_pagto(+)<>9
              AND             b.tp_duplicata(+)=1
              ORDER BY        a.dt_baixa_banco desc;
  End;

  Procedure Pr_Select_Ref_Bancarias_2 (
                                       PM_CURSOR IN OUT TP_CURSOR,
                                       PM_CLI    IN Clie_Refbco.Cod_Cliente%type
                                      ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT dt_consulta,
                     sequencia,
                     banco,
                     agencia,
                     ddd,
                     fone,
                     conceito,
                     observacao
              FROM   clie_refbco
              WHERE  cod_cliente = Pm_cli;
  End;

  Procedure Pr_Select_Observacoes (
                                   Pm_Cursor IN OUT TP_CURSOR,
                                   Pm_Cli    Clie_Refbco.Cod_Cliente%type,
                                   Pm_Seq    Clie_Refbco.Sequencia%type
                                  ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT observacao
              FROM   clie_refbco
              WHERE  cod_cliente = PM_Cli
              AND    sequencia= Pm_Seq;

  End;

  Procedure Pr_Select_Maior_Seq_Clie_Refer (
                                            Pm_CURSOR IN OUT TP_CURSOR,
                                            Pm_Cli    IN Clie_Refer.Cod_Cliente%type
                                           ) is
      BEGIN
          OPEN PM_CURSOR FOR
               SELECT max(sequencia) qtde_col
               FROM clie_refer
               WHERE cod_cliente = Pm_cli;

  End;

  Procedure Pr_Select_Clie_Refer (
                                  Pm_CURSOR IN OUT TP_CURSOR,
                                  Pm_CLi    IN Clie_Refer.Cod_Cliente%type
                                 ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT empresa,
                     dt_consulta,
                     dt_cadastr,
                     ult_fatura,
                     dt_ult_fatura,
                     maior_saldev,
                     atraso_medio,
                     conceito_empresa,
                     observacao ,
                     sequencia
              FROM   clie_refer
              WHERE cod_cliente = Pm_Cli
              ORDER BY  sequencia desc;
  End;

  Procedure Pr_Select_Obs_Seq (
                               PM_Cursor IN OUT TP_CURSOR,
                               Pm_Cli    IN Clie_Refer.Cod_Cliente%type,
                               Pm_Seq    IN Clie_Refer.Sequencia%type
                              ) is
      BEGIN
          OPEN PM_CURSOR FOR
           SELECT observacao,
                  sequencia
           FROM   clie_refer
           WHERE  cod_cliente = Pm_cli
           AND    sequencia= Pm_Seq;
  End;

  Procedure Pr_Select_Saldo_Venc_Venci_Cps(
                                           Pm_Cursor IN OUT TP_CURSOR,
                                           Pm_cli    IN Cobranca.duplicatas.Cod_Cliente%type
                                          ) is

      BEGIN
          OPEN PM_CURSOR FOR
               SELECT nvl(sum(vl_duplicata-vl_desconto-vl_abatimento-vl_devolucao-vl_pago ),0) vencer
               FROM   cobranca.duplicatas,
                      datas
               WHERE  cod_cliente= Pm_cli
               AND    situacao_pagto = 0
               AND    dt_vencimento > = dt_faturamento;
  End;

  Procedure Pr_Select_Duplicatas_4 (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_CLI    IN Cobranca.duplicatas.Cod_Cliente%type
                                   ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT  nvl(sum(vl_duplicata-(vl_desconto+vl_abatimento+vl_devolucao+vl_pago) ),0) vencida
              FROM    cobranca.duplicatas,
                      datas
              WHERE   cod_cliente= PM_CLI
              AND     situacao_pagto = 0
              AND     dt_vencimento < dt_faturamento;

  End;
  Procedure Pr_Select_Pedido_ECT (
                                  PM_CURSOR IN OUT TP_CURSOR,
                                  PM_CLI    IN Cobranca.Duplicatas.Cod_Cliente%type
                                 ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT cod_loja,
                      num_fatura,
                      num_ordem,
                      dt_vencimento,
                      lpad(to_char(vl_duplicata-vl_desconto-vl_abatimento-vl_devolucao-vl_pago,'9999999999990.00'),17, '  ') vl ,
                      a.cod_banco, to_char(dt_faturamento - dt_vencimento, 'fm9999')  atraso ,
                      a.tp_pagamento
               FROM   cobranca.duplicatas a ,
                      datas b
               WHERE  cod_cliente = PM_CLI
               AND    situacao_pagto=0
               AND    dt_vencimento < dt_faturamento
               ORDER BY dt_vencimento;
  End;

  Procedure Pr_Select_Dolar (
                             PM_CURSOR IN OUT TP_CURSOR
                            ) is
     BEGIN
          OPEN PM_CURSOR FOR
               SELECT valor_uss,
                      to_char(dt_faturamento,'DD/MM/YY')
               FROM   dolar_diario,
                      datas
               WHERE  data_uss=dt_faturamento;
  End;

  Procedure Pr_Select_PEdido_Local (
                                    PM_CURSOR IN OUT TP_CURSOR,
                                    PM_LOJA   IN Pednota_venda.Cod_loja%type
                                   ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT a.dt_digitacao,
                     a.num_pedido,
                     a.seq_pedido,
                     b.nome_cliente,
                     d.pseudonimo,
                     c.nome_transp,
                     a.vl_contabil,
                     a.bloq_protesto,
                     a.bloq_juros,
                     a.bloq_cheque,
                     a.bloq_compra,
                     a.bloq_duplicata,
                     a.bloq_limite,
                     a.bloq_saldo,
                     a.bloq_clie_novo,
                     a.cod_cliente,
                     a.cod_loja ,
                     a.cod_repres,
                     a.cod_vend,
                     i.descricao,
                     nvl(t.hora_saida,0) hora
              FROM   pednota_venda a,
                     cliente b,
                     transportadora c,
                     representante e,
                     filial f,
                     representante d ,
                     divisao i ,
                     plano_pgto p,
                     transp_hsaida t
              WHERE  a.cod_loja= Pm_loja
              AND    a.fl_ger_nfis='N'
              AND    (a.bloq_clie_novo='S'
                     OR a.bloq_compra='S')
              AND    a.cod_transp=t.cod_transp(+)
              AND    b.cod_cliente=a.cod_cliente
              AND    c.cod_transp=a.cod_transp
              AND    e.cod_repres=decode(a.tp_pedido,10,a.cod_repres,a.cod_vend)
              AND    f.cod_filial=e.cod_filial
              AND    d.cod_repres=decode(e.seq_franquia,0,e.cod_repres,f.cod_franqueador)
              AND    a.tp_dpkblau=i.tp_divisao
              AND    a.situacao=0
              AND    a.cod_plano = p.cod_plano
              AND    P.FL_AVISTA <> 'S'
              ORDER BY
                     a.dt_digitacao,
                     t.hora_saida;
  End;

  Procedure Pr_Select_Pedido_Local_Repres(
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_LOJA   IN PedNOta_Venda.Cod_loja%type
                                         ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT a.dt_digitacao,
                     a.cod_cliente,
                     c.nome_cliente,
                     a.cod_repres,
                     a.cod_vend,
                     a.bloq_protesto,
                     a.bloq_juros,
                     a.bloq_cheque,
                     a.bloq_compra,
                     a.bloq_duplicata,
                     a.bloq_limite,
                     a.bloq_saldo,
                     e.pseudonimo,
                     b.nome_transp,
                     a.num_pedido,
                     a.seq_pedido,
                     a.cod_loja ,
                     i.descricao,
                     nvl(t.hora_saida,0) hora
               FROM  pednota_venda a,
                     transportadora b,
                     cliente c,
                     representante d,
                     representante e,
                     filial f ,
                     plano_pgto p,
                     divisao i,
                     transp_hsaida t
              WHERE  a.tp_transacao = 1
              AND    a.fl_ger_nfis = 'N'
              AND    a.cod_loja= Pm_Loja
              AND    a.tp_pedido = 10
              AND    (a.bloq_clie_novo='N'
                     OR a.bloq_clie_novo='L' )
              AND    (a.bloq_compra = 'N'
                     OR a.bloq_compra = 'L' )
              AND    (a.bloq_protesto = 'S'
                     OR a.bloq_juros= 'S'
                     OR a.bloq_cheque = 'S'
                     OR a.bloq_duplicata = 'S'
                     OR a.bloq_limite = 'S'
                     OR a.bloq_saldo = 'S' )
              AND    (( a.bloq_desconto = 'L'
                     OR a.bloq_desconto = 'N')
              AND    (a.bloq_acrescimo = 'L'
                     OR a.bloq_acrescimo = 'N' )
              AND    (a.bloq_vlfatmin = 'L'
                     OR a.bloq_vlfatmin = 'N' )
              AND    (a.bloq_item_desc1 = 'L'
                     OR a.bloq_item_desc1 = 'N' )
              AND    (a.bloq_item_desc2 = 'L'
                     OR a.bloq_item_desc2 = 'N' )
              AND    (a.bloq_item_desc3 = 'L'
                     OR a.bloq_item_desc3 = 'N' ))
              AND    a.situacao = 0
              AND    a.cod_transp = t.cod_transp(+)
              AND    b.cod_transp=a.cod_transp
              AND    c.cod_cliente=a.cod_cliente
              AND    d.cod_repres=a.cod_repres
              ANd    f.cod_filial=d.cod_filial
              AND    a.cod_plano = p.cod_plano
              AND    e.cod_repres=decode(d.seq_franquia,0,d.cod_repres,f.cod_franqueador)
              AND    p.fl_avista <> 'S'
              AND    a.tp_dpkblau=i.tp_divisao
              ORDER BY
                     a.dt_digitacao,
                     t.hora_saida;
  End;

  Procedure Pr_Select_Pedido_Retira_local(
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_LOJA   IN Pednota_Venda.Cod_Loja%type
                                         ) is
      BEGIN
            OPEN PM_CURSOR FOR
                 SELECT a.dt_digitacao,
                        a.cod_cliente,
                        c.nome_cliente,
                        a.cod_repres,
                        a.cod_vend,
                        a.bloq_protesto,
                        a.bloq_juros,
                        a.bloq_cheque,
                        a.bloq_compra,
                        a.bloq_duplicata,
                        a.bloq_limite,
                        a.bloq_saldo,
                        e.pseudonimo,
                        b.nome_transp,
                        a.num_pedido,
                        a.seq_pedido,
                        a.cod_loja ,
                        i.descricao,
                        nvl(t.hora_saida,0) hora
                  FROM  pednota_venda a,
                        transportadora b,
                        cliente c,
                        representante d,
                        representante e,
                        filial f ,
                        plano_pgto p,
                        divisao i,
                        transp_hsaida t
                 WHERE  a.tp_transacao = 1
                 AND    a.fl_ger_nfis = 'N'
                 AND    a.cod_loja= Pm_loja
                 AND   (a.bloq_protesto = 'S'
                        OR a.bloq_juros= 'S'
                        OR a.bloq_cheque = 'S'
                        OR a.bloq_duplicata = 'S'
                        OR a.bloq_limite = 'S'
                        OR a.bloq_clie_novo = 'S'
                        OR a.bloq_compra = 'S'
                        OR a.bloq_saldo = 'S' )
                 AND    (( a.bloq_desconto = 'L'
                        OR a.bloq_desconto = 'N')
                 AND    (a.bloq_acrescimo = 'L'
                        OR a.bloq_acrescimo = 'N' )
                 AND    (a.bloq_vlfatmin = 'L'
                        OR a.bloq_vlfatmin = 'N' )
                 AND    (a.bloq_item_desc1 = 'L'
                        OR a.bloq_item_desc1 = 'N' )
                 AND    (a.bloq_item_desc2 = 'L'
                        OR a.bloq_item_desc2 = 'N' )
                 AND    (a.bloq_item_desc3 = 'L'
                        OR a.bloq_item_desc3 = 'N' ))
                 AND    a.situacao = 0
                 AND    b.fl_retira_local='S'
                 AND    a.cod_transp=t.cod_transp(+)
                 AND    b.cod_transp=a.cod_transp
                 AND    c.cod_cliente=a.cod_cliente
                 AND    d.cod_repres=a.cod_repres
                 AND    f.cod_filial=d.cod_filial
                 AND    a.cod_plano = p.cod_plano
                 AND    e.cod_repres=decode(d.seq_franquia,0,d.cod_repres,f.cod_franqueador)
                 AND    a.tp_dpkblau=i.tp_divisao
                 ORDER BY
                        a.dt_digitacao,
                        t.hora_saida;
  End;

  Procedure Pr_Select_Retira_Remoto (
                                     PM_CURSOR IN OUT TP_CURSOR,
                                     PM_DEPOSITO IN Varchar2,
                                     PM_LOJA     IN Number
                                    ) is

     BEGIN
          vSql := 'SELECT a.dtdig,
                          a.cli,
                          blcl,
                          blco,
                          a.blpr,
                          a.blju,
                          a.blch,
                          a.blco,
                          a.bldu,
                          a.blli,
                          a.blsa ,
                          a.rep,
                          a.ven,
                          a.tra,
                          a.nped,
                          a.sped,
                          a.loj,
                          C.NOME_CLIENTE,
                          descricao,
                          nvl(tt.hora_saida,0) hora
                   FROM   dep' || PM_DEPOSITO || '.v_pednota a ,
                          CLIENTE C,
                          plano_pgto p,
                          divisao i,
                          transportadora t
                          dep' || PM_DEPOSITO || '.transp_hsaida tt
                   WHERE  a.tpt = 1
                   AND    a.fgnf = ' || chr(39) || 'N' || chr(39);
          vSql := vSql || ' AND a.loj = ' || Pm_loja ;
          vSql := vSql || ' AND (a.blpr = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blju =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blch =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bldu =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blli =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blcl =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blco =' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blsa =' || chr(39) || 'S' || chr(39) || ') ';
          vSql := vSql || ' AND    (( a.blde =' || chr(39) ||  'L' || chr(39);
          vSql := vSql || ' OR a.blde =' || chr(39) ||  'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.blac =' || chr(39) || 'L' || chr(39) || ' or a.blac =' || chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.blvl =' || chr(39) || 'L' || chr(39) || ' or a.blvl =' || chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.blit1 =' || chr(39) || 'L' || chr(39) || ' or a.blit1 =' || chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.blit2 =' || chr(39) || 'L' || chr(39) || ' or a.blit2 =' || chr(39) || 'N' || chr(39) || ')';
          vSql := vSql || ' AND    (a.blit3 =' || chr(39) || 'L' || chr(39) || ' or a.blit3 =' || chr(39) || 'N' || chr(39) || '))';
          vSql := vSql || ' AND    a.sit = 0
                            AND    a.tra = tt.cod_transp(+)
                            AND    a.pla = p.cod_plano
                            AND    a.cli = c.cod_cliente
                            AND    a.tpd = i.tp_divisao
                            AND    a.tra = t.cod_transp
                            AND    t.fl_retira_local=' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' order by a.dtdig,tt.hora_saida';

          OPEN PM_CURSOR FOR
               vSql;

  End;

  Procedure Pr_Select_Clie_Novos_Ina_SP_GO (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            Pm_Deposito  IN Varchar2,
                                            Pm_Loja      IN Number
                                           ) is
     BEGIN
          vSql := 'SELECT a.dtped,
                          a.ven,
                          a.rep,
                          a.cli,
                          a.blcl,
                          a.blpr,
                          a.blju,
                          a.blch,
                          a.blco,
                          a.bldu,
                          a.blli,
                          a.blsa ,
                          a.pla,
                          a.ban,
                          a.tra,
                          a.nped,
                          a.sped,
                          a.loj,
                          c.nome_cliente ,
                          i.descricao,
                          nvl(t.hora_saida,0) hora
                   FROM   dep' || Pm_Deposito || '.v_pednota a ,
                          cliente c,
                          plano_pgto p ,
                          divisao i,
                          dep' || Pm_Deposito || '.transp_hsaida t
                   WHERE  a.fgnf = ' || chr(39) || 'N' || chr(39);

          vSql := vSql || ' AND a.loj = ' || PM_loja;
          vSql := vSql || ' AND (a.blcL=' || chr(39) || 'S' || chr(39) || ' OR a.blco=' || Chr(39) || 'S' || chr(39) || ') ';
          vSql := vSql || ' AND a.tra = t.cod_transp(+) ';
          vSql := vSql || ' AND a.pla =p.cod_plano ';
          vSql := vSql || ' AND P.FL_AVISTA <> ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' AND a.sit = 0 ';
          vSql := vSql || ' AND a.cli = c.cod_cliente ';
          vSql := vSql || ' AND a.tpd=i.tp_divisao ';
          vSql := vSql || ' order by a.dtdig, t.hora_saida ';

       OPEN PM_CURSOR FOR
            vSql;

  End;

  Procedure Pr_Select_Pedido_Vista_Local (
                                          PM_CURSOR IN OUT TP_CURSOR,
                                          PM_Loja   IN Number
                                         ) is
     BEGIN
          vSql :='SELect a.dt_digitacao,
                        a.cod_cliente,
                        c.nome_cliente,
                        a.bloq_protesto,
                        a.bloq_juros,
                        a.bloq_cheque,
                        a.bloq_compra,
                        a.bloq_duplicata,
                        a.bloq_limite,
                        a.bloq_clie_novo,
                        a.bloq_saldo,
                        e.pseudonimo,
                        b.nome_transp,
                        b.fl_retira_local,
                        a.num_pedido,
                        a.seq_pedido,
                        a.cod_loja,
                        a.cod_repres,
                        a.cod_vend ,
                        i.descricao,
                        nvl(t.hora_saida,0) hora
                 FROM   pednota_venda a,
                        transportadora b,
                        cliente c,
                        representante d,
                        representante e,
                        filial f ,
                        plano_pgto p,
                        divisao i,
                        transp_hsaida t
                 WHERE  a.tp_transacao = 1
                 AND    a.fl_ger_nfis = ' || chr(39) || 'N' || chr(39);
          vSql := vSql || ' AND a.cod_loja=' || PM_Loja;
          vSql := vSql || ' AND (((a.bloq_clie_novo= ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_compra = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_protesto = ' || chr(39) || 'S' || chr(39);
          vsql := vSql || ' OR a.bloq_juros= ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_cheque = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_duplicata = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_limite = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_saldo = ' || chr(39) || 'S' || chr(39) || ') ';
          vSql := vSql || ' AND ((a.bloq_desconto = ' || chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_desconto = ' || chr(39) || 'N' || chr(39) || ') ';
          vSql := vSql || ' AND (a.bloq_acrescimo = ' || chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_acrescimo = ' || chr(39) || 'N' || chr(39) || ') ';
          vSql := vSql || ' AND (a.bloq_vlfatmin = '|| chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_vlfatmin = ' || chr(39) || 'N' || chr(39) || ') ';
          vSql := vSql || ' AND (a.bloq_item_desc1 = ' || chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc1 = ' || chr(39) || 'N' || chr(39) || ') ';
          vSql := vSql || ' AND (a.bloq_item_desc2 = ' || chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc2 = ' || chr(39) || 'N' || chr(39) || ') ';
          vSql := vSql || ' AND (a.bloq_item_desc3 = ' || chr(39) || 'L' || chr(39);
          vSql := vSql || ' OR a.bloq_item_desc3 = ' || chr(39) || 'N' || chr(39) || '))) ';
          vSql := vSql || ' OR ( a.bloq_compra = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bloq_clie_novo = ' || chr(39) || 'S' || chr(39) || ')) ';
          vSql := vSql || ' AND a.situacao = 0 AND a.cod_transp=t.cod_transp(+)
                            AND b.cod_transp=a.cod_transp
                            AND c.cod_cliente=a.cod_cliente
                            AND a.cod_cliente <> 25977
                            AND f.cod_filial=d.cod_filial
                            AND a.cod_plano=p.cod_plano
                            AND p.fl_avista = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' AND d.cod_repres=decode(a.tp_pedido,10,a.cod_repres,a.cod_vend)
                            AND e.cod_repres=decode(d.seq_franquia,0,d.cod_repres, f.cod_franqueador)
                            AND a.tp_dpkblau=i.tp_divisao
                            ORDER BY a.dt_digitacao,t.hora_saida';

         OPEN PM_CURSOR FOR
              vSql;
  End;

  Procedure Pr_Select_Pedido_Credito (
                                      PM_CURSOR IN OUT TP_CURSOR,
                                      PM_PED    IN PedNota_Venda.Num_Pedido%type,
                                      PM_SEQ    IN PedNota_Venda.Seq_Pedido%type,
                                      PM_LOJA   IN PedNota_Venda.Cod_Loja%type
                                     ) is
     BEGIN
         OPEN PM_CURSOR FOR
              SELECT a.dt_pedido,
                     a.cod_vend,
                     a.cod_repres,
                     a.cod_cliente,
                     a.cod_transp,
                     a.cod_plano,
                     a.cod_banco,
                     a.vl_contabil,
                     a.qtd_item_pedido,
                     a.mens_pedido,
                     a.mens_nota,
                     a.pc_desconto,
                     a.pc_acrescimo,
                     a.peso_bruto,
                     a.frete_pago,
                     a.fl_dif_icm,
                     a.bloq_protesto,
                     a.bloq_juros,
                     a.bloq_juros,
                     a.bloq_cheque,
                     a.bloq_compra,
                     a.bloq_duplicata,
                     a.bloq_limite ,
                     a.bloq_saldo,
                     a.situacao,
                     c.nome_cliente,
                     c.cgc,
                     c.classificacao,
                     d.nome_cidade,
                     d.cod_uf,
                     t.nome_transp,
                     t.via_transp,
                     p.desc_plano,
                     b.sigla,
                     o.cod_segmento,
                     s.desc_segmento,
                     c.fone1,
                     c.ddd1,
                     c.fone2,
                     c.ddd2,
                     c.fax
             FROM    pednota_venda a,
                     cliente c,
                     transportadora t ,
                     banco b,
                     plano_pgto p,
                     cidade d,
                     tipo_cliente o,
                     segmento s
             WHERE   a.num_pedido= Pm_ped
             AND     a.seq_pedido= Pm_seq
             AND     a.cod_loja= Pm_loja
             AND     c.cod_cliente=a.cod_cliente
             AND     d.cod_cidade=c.cod_cidade
             AND     t.cod_transp=a.cod_transp
             AND     p.cod_plano=a.cod_plano
             AND     b.cod_banco=a.cod_banco
             AND     o.cod_tipo_cli(+)=c.cod_tipo_cliente
             AND     s.cod_segmento(+)=o.cod_segmento;

  End;

  Procedure Pr_Select_Cliente_Novo_SP_GO(
                                         PM_CURSOR   IN OUT TP_CURSOR,
                                         PM_Deposito IN Varchar2,
                                         PM_LOJA     IN Number
                                        ) is
     BEGIN
          vSql := vSql || '
                  SELECT a.dtped,
                         a.ven,
                         a.rep,
                         a.cli,
                         a.blcl,
                         a.blpr,
                         a.blju,
                         a.blch,
                         a.blco,
                         a.bldu,
                         a.blli,
                         a.blsa,
                         a.pla,
                         a.ban,
                         a.tra,
                         a.nped,
                         a.sped,
                         a.loj,
                         c.nome_cliente
                   FROM  dep' || PM_Deposito || '.v_pednota a ,
                         cliente c
                   WHERE a.fgnf = ' || chr(39) || 'N' || chr(39);
           vSql := vSql || ' AND a.loj =' || Pm_loja;
           vSql := vSql || ' AND (a.blcl = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blco = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blpr = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blju = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blch = ' || 'S' || chr(39);
           vSql := vSql || ' OR a.bldu = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blli = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blsa = ' || chr(39) || 'S' || chr(39) || ') ';
           vSql := vSql || ' AND (( a.blde = ' || chr(39) || 'L' || chr(39) || ' or a.blde = ' || chr(39) || 'N' || chr(39) || ' ) and ';
           vSql := vSql || ' (a.blac = ' || chr(39) || 'L' || chr(39) || ' or a.blac = ' || chr(39) || 'N' || chr(39) || ') and ';
           vSql := vSql || ' (a.blvl = ' || chr(39) || 'L' || chr(39) || ' or a.blvl = ' || chr(39) || 'N' || chr(39) || ') and ';
           vSql := vSql || ' (a.blit1 = ' || chr(39) || 'L' || chr(39) || ' or a.blit1 = ' || chr(39) || 'N' || chr(39) || ') and ';
           vSql := vSql || ' (a.blit2 = ' || chr(39) || 'L' || chr(39) || ' or a.blit2 = ' || chr(39) || 'N' || chr(39) || ') and ';
           vSql := vSql || ' (a.blit3 = ' || chr(39) || 'L' || chr(39) || ' or a.blit3 = ' || chr(39) || 'N' || chr(39) || ')) and ';
           vSql := vSql || ' a.sit = 0  and a.cli=c.cod_Cliente ' ;
           vSql := vSql || ' order by a.dtdig';

         OPEN PM_CURSOR FOR
              vSql;
  End;

  Procedure Pr_Select_Cliente_Novo_SP_GO_2 (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_DEPOSITO IN Varchar2,
                                            PM_LOJA     IN Number
                                           ) is
     BEGIN
          vSql := vSql || '
                SELECT a.dtped,
                       a.ven,
                       a.rep,
                       a.cli,
                       a.blcl,
                       a.blpr,
                       a.blju,
                       a.blch,
                       a.blco,
                       a.bldu,
                       a.blli,
                       a.blsa,
                       a.pla,
                       a.ban,
                       a.tra,
                       a.nped,
                       a.sped,
                       a.loj,
                       c.nome_cliente ,
                       t.fl_retira_local
                FROM   dep' || PM_DEPOSITO || '.v_pednota a ,
                       cliente c,
                       transportadora t
                where a.fgnf = ' || chr(39) || 'N' || chr(39);
           vSql := vSql || ' AND a.loj = ' || PM_Loja;
           vSql := vSql || ' AND (a.blcl = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blco = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blpr = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blju = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blch = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.bldu = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blli = ' || chr(39) || 'S' || chr(39);
           vSql := vSql || ' OR a.blsa = ' || chr(39) || 'S' || chr(39) || ') ';
           vSql := vSql || ' AND a.sit = 0  and a.cli = c.cod_cliente ';
           vSql := vSql || ' AND a.tra=t.cod_transp ' ;
           vSql := vSql || ' order by a.dtdig ';

           OPEN PM_CURSOR FOR
                vSql;

  End;

  Procedure Pr_Select_Pedido_Vista_Remoto (
                                    PM_CURSOR   IN OUT TP_CURSOR,
                                    PM_DEPOSITO IN Varchar2,
                                    PM_LOJA     IN Number
                                   ) is
     BEGIN
         vSql := 'SELECT a.dtdig, a.cli,  a.blpr,
                        a.blju,   a.blch, a.blco,
                        a.bldu,   a.blli, a.blsa ,
                        a.blcl,   a.rep,  a.ven,
                        a.tra,    a.nped, a.sped,
                        a.loj ,   t.fl_retira_local,
                        i.descricao,
                        nvl(tt.hora_saida,0) hora
                 FROM   dep' || PM_DEPOSITO || '.v_pednota a ,
                        plano_pgto p,
                        transportadora t,
                        divisao i,
                        dep' || PM_DEPOSITO || '.transp_hsaida tt
                 WHERE  a.tpt = 1
                 AND    a.fgnf = ' || chr(39) || 'N' || chr(39);
          vSql := vSql || ' AND a.loj = ' || PM_LOJA;
          vSql := vSql || ' AND ((( a.blcl = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blco = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blpr = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blju = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blch = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.bldu = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blli = ' || chr(39) || 'S' || chr(39);
          vSql := vSql || ' OR a.blsa = ' || chr(39) || 'S' || chr(39) || ') and ';
          vSql := vSql || ' (( a.blde = ' || chr(39) || 'L' || chr(39) || ' or a.blde  = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blac  = ' || chr(39) || 'L' || chr(39) || ' or a.blac  = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blvl  = ' || chr(39) || 'L' || chr(39) || ' or a.blvl  = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blit1 = ' || chr(39) || 'L' || chr(39) || ' or a.blit1 = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blit2 = ' || chr(39) || 'L' || chr(39) || ' or a.blit2 = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blit3 = ' || chr(39) || 'L' || chr(39) || ' or a.blit3 = ' || chr(39) || 'N' || chr(39) || '))) OR ';
          vSql := vSql || ' (a.blcl=' || chr(39) || 'S' || chr(39) || ' or a.blco=' || chr(39) || 'S' || chr(39) || ')) and ';
          vSql := vSql || ' a.tra = tt.cod_transp(+) and ';
          vSql := vSql || ' a.pla = p.cod_plano and ';
          vSql := vSql || ' a.sit = 0 and ';
          vSql := vSql || ' p.fl_avista = ' || chr(39) || 'S' || chr(39) || ' and ';
          vSql := vSql || ' t.cod_transp=a.tra and ';
          vSql := vSql || ' a.cli <> 25977 and ';
          vSql := vSql || ' a.tpd=i.tp_divisao ';
          vSql := vSql || ' order by a.dtdig, tt.hora_saida ';

          OPEN PM_CURSOR FOR
               vSql;

  End;

  Procedure Pr_Select_Pedido_Remoto_TLMK (
                                          PM_CURSOR   IN OUT TP_CURSOR,
                                          PM_DEPOSITO IN Varchar2,
                                          PM_LOJA     IN Number
                                         ) is
     BEGIN
          vSql := 'SELECT ';
          vSql := vSql || ' a.dtdig, a.cli,blcl,blco, ';
          vSql := vSql || ' a.blpr, a.blju,a.blch,a.blco,a.bldu,a.blli,a.blsa , ';
          vSql := vSql || ' a.rep, a.ven,a.tra, a.nped, a.sped, a.loj, C.NOME_CLIENTE, descricao,';
          vSql := vSql || ' nvl(t.hora_saida,0) hora ';
          vSql := vSql || ' from dep' || PM_DEPOSITO || '.v_pednota a , CLIENTE C, plano_pgto p, divisao i,';
          vSql := vSql || ' dep' || PM_DEPOSITO || '.transp_hsaida t ';
          vSql := vSql || ' where a.tpt = 1 and ';
          vSql := vSql || ' a.fgnf = ' || chr(39) || 'N' || chr(39) || ' and ';
          vSql := vSql || ' a.loj = ' || PM_Loja || ' and ';
          vSql := vSql || ' A.TPP<>10 AND ';
          vSql := vSql || ' (a.blcl = ' || chr(39) || 'N' || chr(39) || ' or A.BLCL = ' || chr(39) || 'L' || chr(39) || ') AND ';
          vSql := vSql || ' (a.blco = ' || chr(39) || 'N' || chr(39) || ' or A.BLCO = ' || chr(39) || 'L' || chr(39) || ') AND ';
          vSql := vSql || ' (a.blpr = ' || chr(39) || 'S' || chr(39) || ' or ';
          vSql := vSql || '  a.blju = ' || chr(39) || 'S' || chr(39) || ' or ';
          vSql := vSql || '  a.blch = ' || chr(39) || 'S' || chr(39) || ' or ';
          vSql := vSql || '  a.bldu = ' || chr(39) || 'S' || chr(39) || ' or ';
          vSql := vSql || '  a.blli = ' || chr(39) || 'S' || chr(39) || ' or ';
          vSql := vSql || '  a.blsa = ' || chr(39) || 'S' || chr(39) || ') and ';
          vSql := vSql || ' (( a.blde = ' || chr(39) || 'L' || chr(39) || ' or a.blde = ' || chr(39) || 'N' || chr(39) || ' ) and ';
          vSql := vSql || ' (a.blac = ' || chr(39) || 'L' || chr(39) || '  or a.blac = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blvl = ' || chr(39) || 'L' || chr(39) || ' or a.blvl = ' || chr(39) || 'N' || chr(39) || ') and ';
          vSql := vSql || ' (a.blit1 = ' || chr(39) || 'L' || chr(39) || ' or a.blit1 = ' || chr(39) || 'N' || chr(39) || ') and';
          vSql := vSql || ' (a.blit2 = ' || chr(39) || 'L' || chr(39) || ' or a.blit2 = ' || chr(39) || 'N' || chr(39) || ') and';
          vSql := vSql || ' (a.blit3 = ' || chr(39) || 'L' || chr(39) || ' or a.blit3 = ' || chr(39) || 'N' || chr(39) || ')) and ';
          vSql := vSql || ' a.sit = 0 and ';
          vSql := vSql || ' a.tra = t.cod_transp(+)  and  ';
          vSql := vSql || ' a.pla = p.cod_plano  and  ';
          vSql := vSql || ' p.fl_avista<>' || chr(39) || 'S' || chr(39) || ' and ';
          vSql := vSql || ' a.cli = c.cod_cliente and ';
          vSql := vSql || ' a.tpd = i.tp_divisao ';
          vSql := vSql || ' order by a.dtdig,t.hora_saida ';

          OPEN PM_CURSOR FOR
               vSql;
  End;

  Procedure Pr_Select_Pedido_Remoto_Repres (
                                            PM_CURSOR IN OUT TP_CURSOR,
                                            PM_DEPOSITO  IN Varchar2,
                                            PM_LOJa      IN Number
                                           ) is
     BEGIN
            vSql := 'SELECT ';
            vSql := vSql || ' a.dtdig, a.cli,blcl,blco, ';
            vSql := vSql || ' a.blpr, a.blju,a.blch,a.blco,a.bldu,a.blli,a.blsa , ';
            vSql := vSql || ' a.rep, a.ven,a.tra, a.nped, a.sped, a.loj, C.NOME_CLIENTE, i.descricao,';
            vSql := vSql || ' nvl(t.hora_saida,0) hora ';
            vSql := vSql || ' from dep' || PM_DEPOSITO || '.v_pednota a , CLIENTE C, plano_pgto p , divisao i,';
            vSql := vSql || ' dep' || PM_DEPOSITO || '.transp_hsaida t ';
            vSql := vSql || ' where a.tpt = 1 and ';
            vSql := vSql || ' a.fgnf = ' || CHR(39) || 'N' || chr(39) || ' and ';
            vSql := vSql || ' a.loj = ' || PM_Loja || ' and';
            vSql := vSql || ' A.TPP=10 AND ';
            vSql := vSql || ' (a.blcl = ' || CHR(39) || 'N' || CHR(39) || ' or A.BLCL = ' || CHR(39) || 'L' || CHR(39) || ') AND ';
            vSql := vSql || ' (a.blco = ' || CHR(39) || 'N' || CHR(39) || ' or A.BLCO = ' || CHR(39) || 'L' || CHR(39) || ') AND ';
            vSql := vSql || ' (a.blpr = ' || CHR(39) || 'S' || CHR(39) || ' or ';
            vSql := vSql || '  a.blju = ' || CHR(39) || 'S' || CHR(39) || ' or ';
            vSql := vSql || '  a.blch = ' || CHR(39) || 'S' || CHR(39) || ' or ';
            vSql := vSql || '  a.bldu = ' || CHR(39) || 'S' || CHR(39) || ' or ';
            vSql := vSql || '  a.blli = ' || CHR(39) || 'S' || CHR(39) || ' or ';
            vSql := vSql || '  a.blsa = ' || CHR(39) || 'S' || CHR(39) || ') and ';
            vSql := vSql || ' (( a.blde = ' || CHR(39) || 'L' || CHR(39) || ' or a.blde = ' || CHR(39) || 'N' || CHR(39) || ' ) and ';
            vSql := vSql || ' (a.blac = ' || CHR(39) || 'L' || CHR(39) || '  or a.blac = ' || CHR(39) || 'N' || CHR(39) || ') and ';
            vSql := vSql || ' (a.blvl = ' || CHR(39) || 'L' || CHR(39) || ' or a.blvl = ' || CHR(39) || 'N' || CHR(39) || ' ) and ';
            vSql := vSql || ' (a.blit1 = ' || CHR(39) || 'L' || CHR(39) || ' or a.blit1 = ' || CHR(39) || 'N' || CHR(39) || ') and ';
            vSql := vSql || ' (a.blit2 = ' || CHR(39) || 'L' || CHR(39) || ' or a.blit2 = ' || CHR(39) || 'N' || CHR(39) || ') and ';
            vSql := vSql || ' (a.blit3 = ' || CHR(39) || 'L' || CHR(39) || ' or a.blit3 = ' || CHR(39) || 'N' || CHR(39) || ')) and ';
            vSql := vSql || ' a.sit = 0 and ';
            vSql := vSql || ' a.tra = t.cod_transp(+) and';
            vSql := vSql || ' a.pla = p.cod_plano and ';
            vSql := vSql || ' p.fl_avista<>' || CHR(39) || 'S' || CHR(39) || ' and ';
            vSql := vSql || ' a.cli = c.cod_cliente and ';
            vSql := vSql || ' a.tpd = i.tp_divisao ';
            vSql := vSql || ' order by a.dtdig, t.hora_saida ';

            OPEN PM_CURSOR FOR
                 vSql;
  End;

  Procedure Pr_Select_Pedido_local_tlmk (
                                         PM_CURSOR IN OUT TP_CURSOR,
                                         PM_LOJA   IN Number
                                        ) is
     BEGIN
            vSql := 'SELECT ';
            vSql := vSql || ' a.dt_digitacao, a.cod_cliente, c.nome_cliente,a.cod_repres,a.cod_vend, ';
            vSql := vSql || ' a.bloq_protesto, a.bloq_juros, a.bloq_cheque, ';
            vSql := vSql || ' a.bloq_compra,a.bloq_duplicata,a.bloq_limite, ';
            vSql := vSql || ' a.bloq_saldo,e.pseudonimo,b.nome_transp, ';
            vSql := vSql || ' a.num_pedido,a.seq_pedido,a.cod_loja , i.descricao, nvl(t.hora_saida,0) hora ';
            vSql := vSql || ' from pednota_venda a, ';
            vSql := vSql || ' transportadora b, cliente c, representante d, ';
            vSql := vSql || ' representante e,  filial f, plano_pgto p, divisao i,  ';
            vSql := vSql || ' transp_hsaida t';
            vSql := vSql || ' where a.tp_transacao=1 and ';
            vSql := vSql || ' a.fl_ger_nfis=' || chr(39) || 'N' || chr(39) || ' and ';
            vSql := vSql || ' a.cod_loja=' || PM_Loja || ' and ';
            vSql := vSql || ' a.tp_pedido <> 10 and ';
            vSql := vSql || ' (a.bloq_clie_novo=' || chr(39) || 'N' || chr(39) || ' or a.bloq_clie_novo=' || chr(39) || 'L' || chr(39) || ') and ';
            vSql := vSql || ' (a.bloq_compra=' || chr(39) || 'N' || chr(39) || ' or a.bloq_compra=' || chr(39) || 'L' || chr(39) || ') and ';
            vSql := vSql || ' (a.bloq_protesto=' || chr(39) || 'S' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_juros=' || chr(39) || 'S' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_cheque=' || chr(39) || 'S' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_duplicata=' || chr(39) || 'S' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_limite=' || chr(39) || 'S' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_saldo=' || chr(39) || 'S' || chr(39) || ') and ';
            vSql := vSql || '(( a.bloq_desconto = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_desconto = ' || chr(39) || 'N' || chr(39) || ') and ';
            vSql := vSql || '(a.bloq_acrescimo = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_acrescimo = ' || chr(39) || 'N' || chr(39) || ') and ';
            vSql := vSql || ' (a.bloq_vlfatmin = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_vlfatmin = ' || chr(39) || 'N' || chr(39) || ') and ';
            vSql := vSql || ' (a.bloq_item_desc1 = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_item_desc1 =  ' || chr(39) || 'N' || chr(39) || ' ) and ';
            vSql := vSql || ' (a.bloq_item_desc2 = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_item_desc2 =  ' || chr(39) || 'N' || chr(39) || ' ) and ';
            vSql := vSql || ' (a.bloq_item_desc3 = ' || chr(39) || 'L' || chr(39) || ' or ';
            vSql := vSql || ' a.bloq_item_desc3 =  ' || chr(39) || 'N' || chr(39) || ' )) and ';
            vSql := vSql || ' a.situacao = 0 and ';
            vSql := vSql || ' a.cod_transp=t.cod_transp(+) and ';
            vSql := vSql || ' b.cod_transp=a.cod_transp and ';
            vSql := vSql || ' c.cod_cliente=a.cod_cliente and ';
            vSql := vSql || ' d.cod_repres=a.cod_vend and ';
            vSql := vSql || ' f.cod_filial=d.cod_filial and ';
            vSql := vSql || ' a.cod_plano = p.cod_plano and ';
            vSql := vSql || ' p.fl_avista <> ' || chr(39) || 'S' || chr(39) || ' and ';
            vSql := vSql || ' e.cod_repres=decode(d.seq_franquia,0,d.cod_repres,f.cod_franqueador) and ';
            vSql := vSql || ' a.tp_dpkblau=i.tp_divisao ';
            vSql := vSql || ' order by a.dt_digitacao,t.hora_saida ';

            OPEN PM_CURSOR FOR
                 vSql;

  End;

  Procedure Pr_Insert_Update_Parametros (
                                         PM_COD_SISTEMA    in NUMBER,
					                          PM_NOME_PARAMETRO in VARCHAR2,
					                          PM_VLR_PARAMETRO  IN VARCHAR2,
                                         PM_ERRO           OUT Number,
                                         PM_TXTERRO        OUT Varchar2
                                        ) IS
    BEGIN

         Pm_Erro:=0;
         Pm_TxtErro :=' ';

         UPDATE HELPDESK.PARAMETROS
         SET	 VL_PARAMETRO  = PM_VLR_PARAMETRO
         WHERE	 COD_SOFTWARE = PM_COD_SISTEMA AND
                NOME_PARAMETRO = PM_NOME_PARAMETRO;

         IF SQL%NOTFOUND THEN
            INSERT INTO HELPDESK.PARAMETROS (COD_SOFTWARE, NOME_PARAMETRO, VL_PARAMETRO)
            VALUES (PM_COD_SISTEMA, PM_NOME_PARAMETRO, PM_VLR_PARAMETRO);
         END IF;

         COMMIT;

         Exception
            WHEN OTHERS THEN
                 ROLLBACK;
                 PM_ERRO:=SQLCODE;
                 PM_TXTERRO:=SQLERRM;

  End;
  Procedure Pr_Select_Cod_Software (
                                    PM_NOME_SISTEMA in HELPDESK.SOFTWARE.NOME_SOFTWARE%TYPE,
  					                     PM_COD_SISTEMA  OUT Number
                                   ) is
      BEGIN
           SELECT COD_SOFTWARE
           INTO   PM_COD_SISTEMA
           FROM   HELPDESK.SOFTWARE
           WHERE  NOME_SOFTWARE = PM_NOME_SISTEMA;

  End;

  Procedure Pr_Select_Vl_Parametro (
                                    PM_CURSOR1 IN OUT TP_CURSOR,
                                    PM_COD_SISTEMA IN NUMBER,
  					                     PM_NOME_PARAM  IN VARCHAR2
                                   ) is
      BEGIN
           OPEN PM_CURSOR1 FOR
             SELECT *
             FROM   HELPDESK.PARAMETROS
             WHERE  HELPDESK.PARAMETROS.COD_SOFTWARE = PM_COD_SISTEMA AND
                    HELPDESK.PARAMETROS.NOME_PARAMETRO = PM_NOME_PARAM;

  End;

  Procedure Pr_Select_Dt_Faturamento (
                                      PM_CURSOR IN OUT TP_CURSOR
                                     ) is
     BEGIN
        OPEN PM_CURSOR FOR
             SELECT dt_faturamento
             FROM   datas;
  End;
END PCK_VDA070;
/
