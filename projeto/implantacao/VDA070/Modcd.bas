Attribute VB_Name = "ModCD"
Option Explicit

Public lngCD As Long
Public strTp_banco As String * 1
Public strPath As String
Public lngArq As Long
Public TextLine As String

Public StrTipo_Ordem
Public lngCOD_PLANO
Public Sub Get_CD(strUnidade As String)
    On Error GoTo Erro

    strPath = strUnidade & ":\oracle\dados\32bits\cd.txt"
    lngArq = FreeFile

    Open strPath For Input Shared As #lngArq
    Do While Not EOF(lngArq)
        Line Input #lngArq, TextLine
        lngCD = Mid(TextLine, 1, 2)
        strTp_banco = Mid(TextLine, 3, 1)
    Loop
    Close #lngArq

    Exit Sub

Erro:

    If Err = 53 Then
        MsgBox "O Arquivo de controle de tipo de Banco n�o foi encontrado !!!", 16, "Controle de Banco"
        End
    Else
        MsgBox Err.Description, 16, "Controle de Banco"
    End If

End Sub
