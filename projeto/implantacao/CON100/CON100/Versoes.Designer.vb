<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Versoes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboPrimeiroCD = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.lsvPrimeiroCD = New System.Windows.Forms.ListView
        Me.exePriCD = New System.Windows.Forms.ColumnHeader
        Me.DescrPrg_PrimCD = New System.Windows.Forms.ColumnHeader
        Me.VersaoPrimCD = New System.Windows.Forms.ColumnHeader
        Me.DataPrimCD = New System.Windows.Forms.ColumnHeader
        Me.lsvSegundoCD = New System.Windows.Forms.ListView
        Me.exeSegCD = New System.Windows.Forms.ColumnHeader
        Me.DescrPrgSegCD = New System.Windows.Forms.ColumnHeader
        Me.VersaoSegCD = New System.Windows.Forms.ColumnHeader
        Me.dateSegCD = New System.Windows.Forms.ColumnHeader
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboSegundoCD = New System.Windows.Forms.ComboBox
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboPrimeiroCD
        '
        Me.cboPrimeiroCD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPrimeiroCD.FormattingEnabled = True
        Me.cboPrimeiroCD.Items.AddRange(New Object() {"CD01 - Campinas", "CD04 - Goiania"})
        Me.cboPrimeiroCD.Location = New System.Drawing.Point(12, 25)
        Me.cboPrimeiroCD.Name = "cboPrimeiroCD"
        Me.cboPrimeiroCD.Size = New System.Drawing.Size(175, 21)
        Me.cboPrimeiroCD.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Primeiro CD"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 66)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.lsvPrimeiroCD)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.lsvSegundoCD)
        Me.SplitContainer1.Size = New System.Drawing.Size(749, 458)
        Me.SplitContainer1.SplitterDistance = 375
        Me.SplitContainer1.TabIndex = 5
        '
        'lsvPrimeiroCD
        '
        Me.lsvPrimeiroCD.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.exePriCD, Me.DescrPrg_PrimCD, Me.VersaoPrimCD, Me.DataPrimCD})
        Me.lsvPrimeiroCD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvPrimeiroCD.Location = New System.Drawing.Point(0, 0)
        Me.lsvPrimeiroCD.Name = "lsvPrimeiroCD"
        Me.lsvPrimeiroCD.Size = New System.Drawing.Size(375, 458)
        Me.lsvPrimeiroCD.TabIndex = 0
        Me.lsvPrimeiroCD.UseCompatibleStateImageBehavior = False
        Me.lsvPrimeiroCD.View = System.Windows.Forms.View.Details
        '
        'exePriCD
        '
        Me.exePriCD.Text = "Nome do Execut�vel"
        Me.exePriCD.Width = 77
        '
        'DescrPrg_PrimCD
        '
        Me.DescrPrg_PrimCD.Text = "Descri��o do Programa"
        Me.DescrPrg_PrimCD.Width = 105
        '
        'VersaoPrimCD
        '
        Me.VersaoPrimCD.Text = "Vers�o do Executavel"
        Me.VersaoPrimCD.Width = 83
        '
        'DataPrimCD
        '
        Me.DataPrimCD.Text = "Data de cria��o"
        Me.DataPrimCD.Width = 71
        '
        'lsvSegundoCD
        '
        Me.lsvSegundoCD.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.exeSegCD, Me.DescrPrgSegCD, Me.VersaoSegCD, Me.dateSegCD})
        Me.lsvSegundoCD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSegundoCD.Location = New System.Drawing.Point(0, 0)
        Me.lsvSegundoCD.Name = "lsvSegundoCD"
        Me.lsvSegundoCD.Size = New System.Drawing.Size(370, 458)
        Me.lsvSegundoCD.TabIndex = 1
        Me.lsvSegundoCD.UseCompatibleStateImageBehavior = False
        Me.lsvSegundoCD.View = System.Windows.Forms.View.Details
        '
        'exeSegCD
        '
        Me.exeSegCD.Text = "Nome do Executavel"
        '
        'DescrPrgSegCD
        '
        Me.DescrPrgSegCD.Text = "Descri��o do Programa"
        '
        'VersaoSegCD
        '
        Me.VersaoSegCD.Text = "Vers�o do Executavel"
        '
        'dateSegCD
        '
        Me.dateSegCD.Text = "Data de Cria��o"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(200, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Segundo CD:"
        '
        'cboSegundoCD
        '
        Me.cboSegundoCD.FormattingEnabled = True
        Me.cboSegundoCD.Location = New System.Drawing.Point(203, 25)
        Me.cboSegundoCD.Name = "cboSegundoCD"
        Me.cboSegundoCD.Size = New System.Drawing.Size(175, 21)
        Me.cboSegundoCD.TabIndex = 6
        '
        'Versoes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(763, 528)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboSegundoCD)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboPrimeiroCD)
        Me.Name = "Versoes"
        Me.Text = ".: CON100 - Controle de Versoes :."
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboPrimeiroCD As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lsvPrimeiroCD As System.Windows.Forms.ListView
    Friend WithEvents lsvSegundoCD As System.Windows.Forms.ListView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboSegundoCD As System.Windows.Forms.ComboBox
    Friend WithEvents exePriCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents DescrPrg_PrimCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents VersaoPrimCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents DataPrimCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents exeSegCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents DescrPrgSegCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents VersaoSegCD As System.Windows.Forms.ColumnHeader
    Friend WithEvents dateSegCD As System.Windows.Forms.ColumnHeader

End Class
