VERSION 4.00
Begin VB.Form frmTran 
   Caption         =   "PROCESSA TRANSFERENCIA"
   ClientHeight    =   3105
   ClientLeft      =   750
   ClientTop       =   1455
   ClientWidth     =   9105
   Height          =   3510
   Left            =   690
   LinkTopic       =   "Form1"
   ScaleHeight     =   3105
   ScaleWidth      =   9105
   Top             =   1110
   Width           =   9225
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   360
      Top             =   2520
   End
   Begin VB.Label lblStatus 
      AutoSize        =   -1  'True
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   240
      TabIndex        =   0
      Top             =   1200
      Width           =   255
   End
End
Attribute VB_Name = "frmTran"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  
  On Error GoTo Trata_erro
  
  
  If App.PrevInstance Then
    MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
    End
  End If
  
  strDiretorio = "P:\in\transfer\"
  strDiretorio_Salva = "P:\in\transfer\salva\"
'  strDiretorio = "f:\com\entrada\pedidos\transf\"
'  strDiretorio_Salva = "f:\com\entrada\pedidos\transf\salva\"
  
  
  Set orasession = CreateObject("oracleinprocserver.xorasession")
 Set oradatabase = orasession.OpenDatabase("PRODUCAO", "vda820/PROD", 0&)
  'Set oradatabase = orasession.OpenDatabase("desenv", "vda020/prod", 0&)
  
  
  
 Exit Sub
Trata_erro:
   Call Process_Line_Errors(SQL)
End Sub



Public Sub Verifica_Arquivo()
  Dim TMP_LINHA As String
  Dim strChar As String
  Dim strLinha As String
  Dim strTp_Linha As String * 1
  Dim SQL1 As String
  Dim dDigitacao As Date
  Dim dProxSeg As Date
  Dim lngLoja As Long
  Dim ss As Object
  Dim SQL As String
  Dim bSinal As String * 1
  
  
  lblStatus = "VERIFICANDO TRANSFER�NCIAS"
  
  DoEvents
  If Dir(strDiretorio & "*.TXT") = "" Then ' N�O EXISTE TRANSFERENCIA
   lblStatus = "N�O H� SOLICITA��O DE TRANSFER�NCIA"
   DoEvents
   Exit Sub
  Else
    Do While Dir(strDiretorio & "*.TXT") <> ""
      strArquivo = strDiretorio & Dir(strDiretorio & "*.TXT")
      SQL = "Select vendas.seq_cotacao.nextval cotacao from dual"
      
      Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
      
      lngCotacao_Definitiva = ss!cotacao
      
      lblStatus = "PROCESSANDO ARQUIVO DE TRANSFER�NCIA: " & strArquivo
      DoEvents
      Open strArquivo For Input As #1
        Line Input #1, strLinha
        Do While Not EOF(1)
           'strChar = Input(1, #1)
           'If strChar <> Chr(10) Then
           '  strLinha = strLinha & strChar
           'Else
             strTp_Linha = Mid(strLinha, 1, 1)
             If strTp_Linha = "T" Then
               lngCod_Loja_Destino = Mid(strLinha, 9, 2)
               Line Input #1, strLinha
             End If
             If Mid(strLinha, 1, 1) = "I" Then
Item:
               'verifica se j� foi feita transferencia para
               'a cota��o enviada
               lngLoja = Mid(strLinha, 2, 2)
               lngNum_Cotacao = lngCotacao_Definitiva
               lngCotacao_Temp = Mid(strLinha, 4, 9)
               If Verifica_Transferencia(lngLoja, lngNum_Cotacao) = True Then
                 
                 Call Salva_Arquivo(strArquivo)
                 Exit Sub
               End If
               
               If lngCod_Loja_Destino = "01" Then
                 SQL1 = " insert into vendas.item_cotacao "
               Else
                 SQL1 = " insert into dep" & Format(lngCod_Loja_Destino, "00") & ".item_cotacao "
               End If
                 SQL = "BEGIN "
                 SQL = SQL & SQL1
                 SQL = SQL & " (cod_loja,num_cotacao,num_item_cotacao,"
                 SQL = SQL & " cod_dpk,preco_unitario,pc_desc1,pc_desc2,"
                 SQL = SQL & " pc_desc3,pc_dificm,qtd_solicitada,"
                 SQL = SQL & " tabela_venda, situacao,fl_semana,complemento)"
                 SQL = SQL & " Values(:loja,:cot,:item,:dpk,:preco,:desc1,"
                 SQL = SQL & " :desc2,:desc3,:dif,:qtd,:tab,0,0,null);"
                 SQL = SQL & " COMMIT;"
                 SQL = SQL & " EXCEPTION"
                 SQL = SQL & " WHEN OTHERS THEN"
                 SQL = SQL & " ROLLBACK;"
                 SQL = SQL & " :cod_errora := SQLCODE;"
                 SQL = SQL & " :txt_errora := SQLERRM;"
                 SQL = SQL & " END;"
                 
                 bSinal = 0
                 oradatabase.Parameters.Remove "loja"
                 oradatabase.Parameters.Add "loja", Mid(strLinha, 2, 2), 1
                 lngCod_Loja = Mid(strLinha, 2, 2)
                 oradatabase.Parameters.Remove "cot"
                 oradatabase.Parameters.Add "cot", lngNum_Cotacao, 1
                 oradatabase.Parameters.Remove "item"
                 oradatabase.Parameters.Add "item", Mid(strLinha, 15, 3), 1
                 oradatabase.Parameters.Remove "dpk"
                 oradatabase.Parameters.Add "dpk", Mid(strLinha, 18, 5), 1
                 oradatabase.Parameters.Remove "preco"
                 oradatabase.Parameters.Add "preco", Mid(strLinha, 29, 13), 1
                 oradatabase.Parameters.Remove "desc1"
                 oradatabase.Parameters.Add "desc1", CDbl(Mid(strLinha, 48, 5)), 1
                 oradatabase.Parameters.Remove "desc2"
                 If Mid(strLinha, 53, 1) = "-" Then
                   bSinal = 1
                 End If
                 
                 oradatabase.Parameters.Add "desc2", CDbl(Mid(strLinha, 53, 5 + bSinal)), 1
                 oradatabase.Parameters.Remove "desc3"
                 If Mid(strLinha, 58, 1) = "-" Then
                   bSinal = bSinal + 1
                 End If
                 
                   oradatabase.Parameters.Add "desc3", CDbl(Mid(strLinha, 58 + bSinal, 5)), 1
                 
                 oradatabase.Parameters.Remove "dif"
                 oradatabase.Parameters.Add "dif", Mid(strLinha, 63 + bSinal, 5), 1
                 oradatabase.Parameters.Remove "qtd"
                 oradatabase.Parameters.Add "qtd", Mid(strLinha, 23, 6), 1
                 oradatabase.Parameters.Remove "tab"
                 oradatabase.Parameters.Add "tab", Mid(strLinha, 42, 6), 1
                 oradatabase.Parameters.Remove "cod_errora"
                 oradatabase.Parameters.Add "cod_errora", 0, 2
                 oradatabase.Parameters.Remove "txt_errora"
                 oradatabase.Parameters.Add "txt_errora", "", 2
           
                oradatabase.ExecuteSQL SQL
                If Val(oradatabase.Parameters!txt_errora) <> 0 Then
                  MsgBox "Ocorreu o erro: " & oradatabase.Parameters!txt_errora & " avise o analista responsavel", vbCritical, "Aten��o"
                  Call Salva_Arquivo(strArquivo)
                  Close #1
                  Exit Sub
                End If
                Line Input #1, strLinha
                If Mid(strLinha, 1, 1) = "I" Then
                  GoTo Item
                End If
             End If
             If Mid(strLinha, 1, 1) = "C" Then
               If lngCod_Loja_Destino = "01" Then
                 SQL1 = " insert into vendas.cotacao "
               Else
                 SQL1 = " insert into dep" & Format(lngCod_Loja_Destino, "00") & ".cotacao "
               End If
               SQL = "BEGIN "
               SQL = SQL & SQL1
               SQL = SQL & " (cod_loja,num_cotacao,"
               SQL = SQL & " dt_cotacao, dt_validade, cod_cliente,cod_vend,"
               SQL = SQL & " cod_plano,pc_desconto,pc_desc_suframa,pc_acrescimo,"
               SQL = SQL & " fl_tipo,fl_dif_icm,situacao,observ,cod_repres,cod_vdr)"
               SQL = SQL & " Values(:loja,:cot,to_date(:dt,'dd/mm/rr'),"
               SQL = SQL & " to_date(:dt_val,'dd/mm/rr'),:cli,"
               SQL = SQL & " decode(:vend,'0000',null,:vend),"
               SQL = SQL & " :plano,:desc,:suf,:acres,0,:dif,0,:msg,"
               SQL = SQL & " decode(:rep,'0000',null,:rep),:vdr);"
               SQL = SQL & " COMMIT;"
               SQL = SQL & " EXCEPTION"
               SQL = SQL & " WHEN OTHERS THEN"
               SQL = SQL & " ROLLBACK;"
               SQL = SQL & " :cod_errora := SQLCODE;"
               SQL = SQL & " :txt_errora := SQLERRM;"
               SQL = SQL & " END;"
               
               dDigitacao = CDate(Mid(strLinha, 22, 8))
               If WeekDay(dDigitacao) = 0 Then
                 'domingo
                 dProxSeg = dDigitacao + 1
               ElseIf WeekDay(dDigitacao) = 1 Then
                 'segunda
                dProxSeg = dDigitacao + 7
               Else
                 'ter�a � sabado
                dProxSeg = dDigitacao + (9 - WeekDay(dDigitacao))
               End If
               
               oradatabase.Parameters.Remove "loja"
               oradatabase.Parameters.Add "loja", Mid(strLinha, 2, 2), 1
               oradatabase.Parameters.Remove "cot"
               oradatabase.Parameters.Add "cot", lngNum_Cotacao, 1
               oradatabase.Parameters.Remove "cli"
               oradatabase.Parameters.Add "cli", Mid(strLinha, 47, 6), 1
               oradatabase.Parameters.Remove "rep"
               oradatabase.Parameters.Add "rep", Mid(strLinha, 63, 4), 1
               oradatabase.Parameters.Remove "vend"
               oradatabase.Parameters.Add "vend", Mid(strLinha, 67, 4), 1
               oradatabase.Parameters.Remove "plano"
               oradatabase.Parameters.Add "plano", Mid(strLinha, 71, 3), 1
               oradatabase.Parameters.Remove "msg"
               oradatabase.Parameters.Add "msg", IIf(Mid(strLinha, 77, 50) = "", Null, Mid(strLinha, 77, 50)), 1
               oradatabase.Parameters.Remove "desc"
               oradatabase.Parameters.Add "desc", Mid(strLinha, 220, 6), 1
               oradatabase.Parameters.Remove "suf"
               oradatabase.Parameters.Add "suf", Mid(strLinha, 226, 6), 1
               oradatabase.Parameters.Remove "acres"
               oradatabase.Parameters.Add "acres", Mid(strLinha, 232, 6), 1
               oradatabase.Parameters.Remove "dif"
               oradatabase.Parameters.Add "dif", Mid(strLinha, 241, 1), 1
               oradatabase.Parameters.Remove "dt"
               oradatabase.Parameters.Add "dt", CDate(Mid(strLinha, 36, 8)), 1
               oradatabase.Parameters.Remove "dt_val"
               oradatabase.Parameters.Add "dt_val", CDate(dProxSeg), 1
               oradatabase.Parameters.Remove "vdr"
               oradatabase.Parameters.Add "vdr", IIf(Mid(strLinha, 243, 1) = "", 0, Mid(strLinha, 243, 1)), 1
               
               oradatabase.ExecuteSQL SQL
               
               If Val(oradatabase.Parameters!cod_errora) <> 0 Then
                 MsgBox "Ocorreu o erro: " & oradatabase.Parameters!txt_errora & " avise o analista responsavel", vbCritical, "Aten��o"

                 Call Salva_Arquivo(strArquivo)
                 Close #1
                 Exit Sub
               End If
             'End If
             strLinha = ""
             'Line Input #1, strLinha
             
           End If
        Loop
               SQL = "BEGIN "
               If lngCod_Loja_Destino = 1 Then
                 SQL = SQL & " PRODUCAO.PR_GERA_TRANSFERENCIA"
               Else
                 SQL = SQL & "DEP" & Format(lngCod_Loja_Destino, "00") & "."
                 SQL = SQL & "PR_GERA_TRANSFERENCIA"
               End If
               SQL = SQL & "(:loja,:cot,:loja_dest,:temp);"
               SQL = SQL & " COMMIT;"
               SQL = SQL & " EXCEPTION"
               SQL = SQL & " WHEN OTHERS THEN"
               SQL = SQL & " ROLLBACK;"
               SQL = SQL & " :cod_errora := SQLCODE;"
               SQL = SQL & " :txt_errora := SQLERRM;"
               SQL = SQL & " END;"
               oradatabase.Parameters.Remove "loja"
               oradatabase.Parameters.Add "loja", lngCod_Loja, 1
               oradatabase.Parameters.Remove "cot"
               oradatabase.Parameters.Add "cot", lngNum_Cotacao, 1
               oradatabase.Parameters.Remove "loja_dest"
               oradatabase.Parameters.Add "loja_dest", lngCod_Loja_Destino, 1
               oradatabase.Parameters.Remove "temp"
               oradatabase.Parameters.Add "temp", lngCotacao_Temp, 1
               
               oradatabase.ExecuteSQL SQL
               If Val(oradatabase.Parameters!cod_errora) <> 0 Then
                  MsgBox "OCORREU O ERRO: " & oradatabase.Parameters!txt_errora
                  Close #1
                  Exit Sub
               End If
               
               Call Salva_Arquivo(strArquivo)
               lblStatus = "TRANSFERENCIA PROCESSADA COM SUCESSO"
               DoEvents

    
    Loop
  End If
   
End Sub

Private Sub Timer1_Timer()
  Call Verifica_Arquivo
End Sub



Public Sub Salva_Arquivo(strArquivo As String)
  Close #1
 FileCopy strArquivo, strDiretorio_Salva & Right(strArquivo, InStr(strArquivo, "\PED") - 1)
'  FileCopy strArquivo, strDiretorio_Salva & Right(strArquivo, InStr(strArquivo, "\PED") - 21)
  
  Kill strArquivo
  
End Sub

Function Verifica_Transferencia(lngLoja As Long, lngcotacao As Long) As Boolean
  Dim ss As Object

  SQL = "Select count(*) total"
  If lngLoja = 1 Then
    SQL = SQL & " From transferencia.r_cotacao_transf "
  Else
    SQL = SQL & " From DEP" & Format(lngLoja, "00") & ".r_cotacao_transf "
  End If
  SQL = SQL & " Where num_cotacao = :cot and"
  SQL = SQL & " cod_loja_cotacao = :loja"
  
  
  oradatabase.Parameters.Remove "loja"
  oradatabase.Parameters.Add "loja", lngLoja, 1
  oradatabase.Parameters.Remove "cot"
  oradatabase.Parameters.Add "cot", lngcotacao, 1
  
  Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
  
  If ss.EOF And ss.BOF Then
    Verifica_Transferencia = False
  Else
    If ss!total = 0 Then
      Verifica_Transferencia = False
    Else
      Verifica_Transferencia = True
    End If
  End If
  
  If Verifica_Transferencia = False Then
    SQL = "Select count(*) total"
    If lngLoja = 1 Then
        SQL = SQL & " From vendas.cotacao "
    Else
        SQL = SQL & " From DEP" & Format(lngLoja, "00") & ".cotacao "
    End If
    SQL = SQL & " Where num_cotacao = :cot and"
    SQL = SQL & " cod_loja = :loja"
  
  
    oradatabase.Parameters.Remove "loja"
    oradatabase.Parameters.Add "loja", lngLoja, 1
    oradatabase.Parameters.Remove "cot"
    oradatabase.Parameters.Add "cot", lngcotacao, 1
  
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    If ss.EOF And ss.BOF Then
      Verifica_Transferencia = False
    Else
      If ss!total = 0 Then
        Verifica_Transferencia = False
      Else
        Verifica_Transferencia = True
      End If
  End If
    
  End If
  

End Function


