VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCadastro 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cadastro"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4140
   ControlBox      =   0   'False
   Icon            =   "frmCadastro.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   276
   Begin VB.ComboBox cboBase 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmCadastro.frx":23D2
      Left            =   1470
      List            =   "frmCadastro.frx":23D4
      TabIndex        =   8
      Top             =   2730
      Width           =   1035
   End
   Begin VB.ComboBox cbo_VDR 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmCadastro.frx":23D6
      Left            =   1470
      List            =   "frmCadastro.frx":23D8
      TabIndex        =   7
      Top             =   2301
      Width           =   2595
   End
   Begin VB.TextBox txtEmail 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1470
      TabIndex        =   5
      Top             =   1485
      Width           =   1200
   End
   Begin VB.TextBox txtCodigo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1470
      TabIndex        =   3
      Top             =   1050
      Width           =   1200
   End
   Begin VB.ComboBox cboCD 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1470
      TabIndex        =   2
      Top             =   1874
      Width           =   2595
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   4035
      _ExtentX        =   7117
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":23DA
      PICN            =   "frmCadastro.frx":23F6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   2640
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":30D0
      PICN            =   "frmCadastro.frx":30EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   690
      Left            =   3390
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCadastro.frx":3DC6
      PICN            =   "frmCadastro.frx":3DE2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Gera Base:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   525
      TabIndex        =   11
      Top             =   2880
      Width           =   870
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Sistema VDR:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   300
      TabIndex        =   10
      Top             =   2445
      Width           =   1095
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Vis�o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   915
      TabIndex        =   9
      Top             =   2025
      Width           =   480
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Caixa Postal:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   390
      TabIndex        =   6
      Top             =   1590
      Width           =   1005
   End
   Begin VB.Label lblNome 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Representante:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   120
      TabIndex        =   4
      Top             =   1170
      Width           =   1275
   End
End
Attribute VB_Name = "frmCadastro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public vCodRepres_Paac As String

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()

	'RTI-143 - Definindo o t�tulo
    If vTipoBase = "R" Then
       frmCadastro.Caption = "CD�s por Representante"
       lblNome.Caption = "Representante"
    ElseIf vTipoBase = "G" Then
       frmCadastro.Caption = "CD�s por Gerente"
       lblNome.Caption = "Gerente"
    Else
       frmCadastro.Caption = "CD�s por Filial"
       lblNome.Caption = "Filial"
    End If
    
    txtCodigo = vCodRepres_Paac
            
    Criar_Cursor vBanco_Prod
    vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_BUSCA_CD(:VCURSOR); END;"
    vBanco_Prod.ExecuteSQL vSql
    
    Set VarRec_PROD = vBanco_Prod.Parameters("vCursor").Value
    
    Do While Not VarRec_PROD.EOF
       TIPO = VarRec_PROD!TIPO_LOJA
       CONTEUDO = Format(VarRec_PROD!TIPO_LOJA, "00") & "-"
       Do While TIPO = VarRec_PROD!TIPO_LOJA
          CONTEUDO = CONTEUDO & VarRec_PROD!cod_Loja & ","
          VarRec_PROD.MoveNext
          If VarRec_PROD.EOF Then
             Exit Do
          End If
       Loop
       cboCD.AddItem CONTEUDO
       TIPO = VarRec_PROD!TIPO_LOJA
       CONTEUDO = ""
       CONTEUDO = Format(VarRec_PROD!TIPO_LOJA, "00") & "-"
    Loop
    
    cbo_VDR.AddItem "S-Sim"
    cbo_VDR.AddItem "N-N�o"
    
    cboBase.AddItem "S-Sim"
    cboBase.AddItem "N-N�o"
    
    FL_BASE = Left(frmCadastro.cboBase, 1)

End Sub

Private Sub CmdSalvar_Click()

    vBanco_Prod.Parameters.Remove "PM_CODIGO"
    vBanco_Prod.Parameters.Add "PM_CODIGO", Val(vCodRepres_Paac), 1
    vBanco_Prod.Parameters.Remove "PM_TIPO_USUARIO"
    vBanco_Prod.Parameters.Add "PM_TIPO_USUARIO", TIPO_USUARIO, 1
    vBanco_Prod.Parameters.Remove "PM_EMAIL"
    vBanco_Prod.Parameters.Add "PM_EMAIL", txtEmail, 1
    vBanco_Prod.Parameters.Remove "PM_VDR"
    vBanco_Prod.Parameters.Add "PM_VDR", Left(cbo_VDR, 1), 1
    vBanco_Prod.Parameters.Remove "PM_TIPO"
    vBanco_Prod.Parameters.Add "PM_TIPO", Val(Left(cboCD, 2)), 1
    vBanco_Prod.Parameters.Remove "PM_BASE"
    vBanco_Prod.Parameters.Add "PM_BASE", Left(cboBase, 1), 1
    
    vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_ATUALIZA_CADASTRO(:PM_TIPO_USUARIO,:PM_CODIGO,:PM_EMAIL,:PM_VDR,:PM_BASE,:PM_TIPO,:verro_prod); END;"
    V_BANCO_PROD.ExecuteSQL V_SQL
    
    Set VarRec_PROD = vBanco_Prod.Parameters("vCursor_PROD").Value
    
    If vBanco_Prod.Parameters("vErro_prod").Value <> 0 Then
        Fl_Restrito = "S"
        Exit Sub
    Else
        If TIPO_USUARIO = "R" Then
            SQL = "select * from PAAC.CONTROLE_REPRESENTANTE " & _
                  "WHERE COD_REPRES= :PM_CODIGO "
        
            Set Oradynaset = V_BANCO_PROD.dbcreatedynaset(SQL, 0&)
               
            If Not Oradynaset.EOF Then
                SQL = " Update PAAC.CONTROLE_REPRESENTANTE SET FL_RODA = 'N' " & _
                      " Where COD_REPRES = :PM_CODIGO "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            Else
                SQL = " INSERT INTO PAAC.CONTROLE_REPRESENTANTE " & _
                      " VALUES (:PM_CODIGO, 'N') "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            End If
			'RTI-143 - Verificando se o tipo do usu�rio � Gerente
            ElseIf Tipo_Usuario = "G" Then
            SQL = "select * from PAAC.CONTROLE_REPRESENTANTE " & _
                  "WHERE COD_REPRES= :PM_CODIGO "
        
            Set OraDynaset = V_BANCO_PROD.dbcreatedynaset(SQL, 0&)
               
            If Not OraDynaset.EOF Then
                SQL = " Update PAAC.CONTROLE_REPRESENTANTE SET FL_RODA = 'N' " & _
                      " Where COD_REPRES = :PM_CODIGO "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            Else
                SQL = " INSERT INTO PAAC.CONTROLE_REPRESENTANTE " & _
                      " VALUES (:PM_CODIGO, 'N') "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            End If
        Else
            SQL = "select * from PAAC.CONTROLE_PAAC " & _
                  "WHERE COD_FILIAL= :PM_CODIGO "
        
            Set Oradynaset = V_BANCO_PROD.dbcreatedynaset(SQL, 0&)
            
            If Not Oradynaset.EOF Then
                SQL = " Update PAAC.CONTROLE_PAAC SET FL_RODA = 'N' " & _
                      " Where COD_FILIAL = :PM_CODIGO "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            Else
                SQL = " INSERT INTO PAAC.CONTROLE_PAAC " & _
                      " VALUES (:PM_CODIGO, 'N') "
                      
                V_BANCO_PROD.ExecuteSQL SQL
            End If
        End If
    
        MsgBox "Cadastro OK!"
        Screen.MousePointer = 0
    End If
    
    FL_BASE = Left(frmCadastro.cboBase, 1)
    
    frmCadastro.txtCodigo.Text = ""
    frmCadastro.txtEmail.Text = ""
    frmCadastro.cboCD.Text = ""
    frmCadastro.cbo_VDR.Clear
    frmCadastro.cboBase.Clear
    
    Exit Sub
    
Trata_Erro:
        
        If Err.Number > 0 Then
           MsgBox Err & Err.Description
        End If

End Sub

Private Sub CmdExcluir_Click()

    vBanco_Prod.Parameters.Remove "PM_CODIGO"
    vBanco_Prod.Parameters.Add "PM_CODIGO", Val(vCodRepres_Paac), 1
    vBanco_Prod.Parameters.Remove "PM_TIPO_USUARIO"
    vBanco_Prod.Parameters.Add "PM_TIPO_USUARIO", TIPO_USUARIO, 1
    
    vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_EXCLUI(:PM_TIPO_USUARIO,:PM_CODIGO,:VERRO_Prod); END;"
    V_BANCO_PROD.ExecuteSQL vSql
    
    Set VarRec_PROD = vBanco_Prod.Parameters("vCursor_Prod").Value
    
    If vBanco_Prod.Parameters("vErro_prod").Value <> 0 Then
       Fl_Restrito = "S"
       Exit Sub
    Else
       MsgBox "Dele��o OK!"
       Screen.MousePointer = 0
    End If
        
    frmCadastro.txtCodigo.Text = ""
    frmCadastro.txtEmail.Text = ""
    frmCadastro.cboCD.Text = ""
    frmCadastro.cbo_VDR.Text = ""
    
    Exit Sub

Trata_Erro:
    
    If Err.Number > 0 Then
       MsgBox Err & Err.Description
    End If
End Sub

Private Sub TxtEMail_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub




