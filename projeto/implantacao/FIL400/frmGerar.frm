VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmGerar 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gerar Base"
   ClientHeight    =   8685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12510
   ControlBox      =   0   'False
   Icon            =   "frmGerar.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   579
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   834
   Begin MSComctlLib.ProgressBar PB1 
      Height          =   165
      Left            =   5010
      TabIndex        =   20
      Top             =   570
      Width           =   7425
      _ExtentX        =   13097
      _ExtentY        =   291
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin VB.ListBox lstExtras 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1605
      ItemData        =   "frmGerar.frx":23D2
      Left            =   9450
      List            =   "frmGerar.frx":2415
      Sorted          =   -1  'True
      TabIndex        =   18
      Top             =   6990
      Width           =   3000
   End
   Begin VB.ListBox lstErros 
      Appearance      =   0  'Flat
      Height          =   1590
      Left            =   60
      TabIndex        =   16
      Top             =   7020
      Width           =   9255
   End
   Begin VB.ListBox lstEspecificasPorLojaRepres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      ItemData        =   "frmGerar.frx":257A
      Left            =   3180
      List            =   "frmGerar.frx":25AE
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   14
      Top             =   2340
      Width           =   3000
   End
   Begin VB.Data DataCompleta 
      Caption         =   "DataCompleta"
      Connect         =   "Access"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   8790
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   120
      Visible         =   0   'False
      Width           =   2295
   End
   Begin VB.ListBox lstEspecificas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      ItemData        =   "frmGerar.frx":2684
      Left            =   6300
      List            =   "frmGerar.frx":26C4
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   9
      Top             =   2340
      Width           =   3000
   End
   Begin VB.Frame fraTipo 
      Appearance      =   0  'Flat
      Caption         =   "Tipo Base"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   2715
      Begin VB.OptionButton optGerente 
         Appearance      =   0  'Flat
         Caption         =   "Gerente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   22
         Top             =   660
         Width           =   1050
      End
      Begin VB.TextBox txtCodPaacRepres 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1770
         MaxLength       =   6
         TabIndex        =   8
         Top             =   360
         Width           =   840
      End
      Begin VB.OptionButton optRepres 
         Appearance      =   0  'Flat
         Caption         =   "Representante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   6
         Top             =   450
         Width           =   1530
      End
      Begin VB.OptionButton optPAAC 
         Appearance      =   0  'Flat
         Caption         =   "PAAC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   5
         Top             =   240
         Value           =   -1  'True
         Width           =   1050
      End
   End
   Begin VB.ListBox lstVDR 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      ItemData        =   "frmGerar.frx":27E7
      Left            =   9450
      List            =   "frmGerar.frx":27FD
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   3
      Top             =   2340
      Width           =   3000
   End
   Begin VB.ListBox lstGenericas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4365
      ItemData        =   "frmGerar.frx":287A
      Left            =   60
      List            =   "frmGerar.frx":28F0
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   2
      Top             =   2340
      Width           =   3000
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   12435
      _ExtentX        =   21934
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":2B00
      PICN            =   "frmGerar.frx":2B1C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerar 
      Height          =   690
      Left            =   2880
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1080
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":37F6
      PICN            =   "frmGerar.frx":3812
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGenericas 
      Height          =   390
      Left            =   60
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1920
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Gen�ricas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":44EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEspecificas 
      Height          =   390
      Left            =   6300
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1920
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Espec�ficas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":4508
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVDR 
      Height          =   390
      Left            =   9450
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1920
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "VDR"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":4524
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEspecificasDep 
      Height          =   390
      Left            =   3180
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1920
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Espec�ficas Dep�sito x Filial"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerar.frx":4540
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Status:"
      Height          =   195
      Left            =   5010
      TabIndex        =   21
      Top             =   360
      Width           =   495
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Tabelas em Branco"
      Height          =   195
      Left            =   9450
      TabIndex        =   19
      Top             =   6780
      Width           =   1380
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Lista de Erros Ocorridos:"
      Height          =   195
      Left            =   60
      TabIndex        =   17
      Top             =   6810
      Width           =   1725
   End
   Begin VB.Label lblMensagem 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   405
      Left            =   5010
      TabIndex        =   10
      Top             =   930
      Width           =   7425
   End
End
Attribute VB_Name = "frmGerar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdEspecificas_Click()
    frmGerar.lstEspecificas.Tag = "TODOS"
    For i = 0 To frmGerar.lstEspecificas.ListCount - 1
        frmGerar.lstEspecificas.Selected(i) = Not (frmGerar.lstEspecificas.Selected(i))
    Next
    frmGerar.lstEspecificas.Tag = ""
End Sub

Private Sub cmdEspecificasDep_Click()

    For i = 0 To frmGerar.lstEspecificasPorLojaRepres.ListCount - 1
        frmGerar.lstEspecificasPorLojaRepres.Selected(i) = Not (frmGerar.lstEspecificasPorLojaRepres.Selected(i))
    Next

End Sub

Private Sub cmdGenericas_Click()
    For i = 0 To frmGerar.lstGenericas.ListCount - 1
        frmGerar.lstGenericas.Selected(i) = Not (frmGerar.lstGenericas.Selected(i))
    Next
End Sub

Private Sub cmdGerar_Click()
    On Error GoTo Trata_Erro

    If lstGenericas.SelCount <= 0 And Me.lstEspecificasPorLojaRepres.SelCount <= 0 And Me.lstEspecificas.SelCount <= 0 And Me.lstVDR.SelCount <= 0 Then
        MsgBox "Selecione uma tabela.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If txtCodPaacRepres = "" Then
        MsgBox "Informe o C�digo.", vbInformation, "Aten��o"
        txtCodPaacRepres.SetFocus
        Exit Sub
    End If
    'RTI-143 - Definindo o Tipo do Banco como Gerente
    If optPAAC.Value = True Then
        vTipoBase = "P"
    ElseIf optGerente.Value = True Then
        vTipoBase = "G"
    Else
        vTipoBase = "R"
    End If
    
    'vGerarBaseEm = Pegar_VL_Parametro("DIR_GERARBASEEM")
    'vGerarLog = Pegar_VL_Parametro("DIR_GERARLOGEM")
    'vMdbVazio = Pegar_VL_Parametro("DIR_MDBVAZIO")
    
    vGerarBaseEm = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "GERARBASEEM")
    vGerarLog = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "GERARLOGEM")
    vMdbVazio = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "MDBVAZIO")
    
    vCD_Novo = 0
    
    FileCopy vMdbVazio & "0000.MDB", vGerarBaseEm & "0000.MDB"
    
    frmCadastro.vCodRepres_Paac = frmGerar.txtCodPaacRepres
    
    VERIFICA_CADASTRO
    
	'RTI-143 - Verificando se o Usu�rio informado � um Gerente
    If (vTipoBase = "G") Then
        
        If Not (VerificaGerente(CInt(txtCodPaacRepres.Text))) Then
            MsgBox "O usu�rio informado n�o � um Gerente!" & vbCrLf & "Favor informar um usu�rio do tipo Gerente.", vbExclamation
            Exit Sub
        End If
    End If
    
    
    Gerar

    DataCompleta.DatabaseName = ""
    DataCompleta.Database.Close
    
Trata_Erro:
    If Err.Number = 91 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub CmdGerar" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub cmdVDR_Click()
    For i = 0 To frmGerar.lstVDR.ListCount - 1
        frmGerar.lstVDR.Selected(i) = Not (frmGerar.lstVDR.Selected(i))
    Next
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0

    
End Sub

Private Sub lstEspecificas_ItemCheck(Item As Integer)
    If frmGerar.lstEspecificas.Tag <> "" Then Exit Sub
    If UCase(lstEspecificas.List(Item)) = "FRANQUEADOR" Then
        For i = 0 To lstEspecificas.ListCount - 1
            If UCase(lstEspecificas.List(i)) = "REPRESENTANTE" Then
                lstEspecificas.Selected(i) = lstEspecificas.Selected(Item)
                Exit Sub
            End If
        Next
    End If
    If UCase(lstEspecificas.List(Item)) = "REPRESENTANTE" Then
        For i = 0 To lstEspecificas.ListCount - 1
            If UCase(lstEspecificas.List(i)) = "FRANQUEADOR" Then
                lstEspecificas.Selected(i) = lstEspecificas.Selected(Item)
                Exit Sub
            End If
        Next
    End If
End Sub

Private Sub txtCodPaacRepres_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub


