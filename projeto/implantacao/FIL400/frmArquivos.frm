VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Begin VB.Form frmArquivos 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecione os Arquivos para Gerar a Carga"
   ClientHeight    =   7950
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10815
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmArquivos.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   530
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   721
   StartUpPosition =   2  'CenterScreen
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   7080
      Top             =   30
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   17
      Top             =   7575
      Width           =   10815
      _ExtentX        =   19076
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19024
         EndProperty
      EndProperty
   End
   Begin VB.Frame FraCargas 
      Caption         =   "Gerar Cargas para os DBFs"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6255
      Left            =   7860
      TabIndex        =   8
      Top             =   1140
      Width           =   2865
      Begin VB.ListBox lstPAACs 
         Appearance      =   0  'Flat
         Height          =   4980
         ItemData        =   "frmArquivos.frx":23D2
         Left            =   90
         List            =   "frmArquivos.frx":23D9
         Style           =   1  'Checkbox
         TabIndex        =   13
         Top             =   450
         Width           =   1245
      End
      Begin VB.ListBox lstRepresentantes 
         Appearance      =   0  'Flat
         Height          =   4980
         ItemData        =   "frmArquivos.frx":23EB
         Left            =   1440
         List            =   "frmArquivos.frx":23F2
         Style           =   1  'Checkbox
         TabIndex        =   12
         Top             =   465
         Width           =   1245
      End
      Begin VB.CommandButton Command1 
         Caption         =   "PAACs"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   11
         ToolTipText     =   "Marcar/Desmarcar Paacs"
         Top             =   5520
         Width           =   1245
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Repres"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1470
         TabIndex        =   10
         ToolTipText     =   "Marcar/Desmarcar Repres"
         Top             =   5520
         Width           =   1245
      End
      Begin VB.FileListBox FileGenerica 
         Height          =   870
         Left            =   180
         TabIndex        =   9
         Top             =   1410
         Visible         =   0   'False
         Width           =   1005
      End
      Begin Bot�o.cmd cmdCarga 
         Height          =   300
         Left            =   90
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Gerar"
         Top             =   5850
         Width           =   2640
         _ExtentX        =   4657
         _ExtentY        =   529
         BTYPE           =   3
         TX              =   "Gerar Carga"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmArquivos.frx":2404
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Representantes"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1440
         TabIndex        =   16
         Top             =   240
         Width           =   1350
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "PAAC�s"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   90
         TabIndex        =   15
         Top             =   240
         Width           =   645
      End
   End
   Begin VB.FileListBox File_Esp 
      Height          =   675
      Left            =   4950
      Pattern         =   "*.DBF"
      TabIndex        =   7
      Top             =   90
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.FileListBox File_Gen 
      Height          =   675
      Left            =   930
      Pattern         =   "*.DBF"
      TabIndex        =   6
      Top             =   30
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.ListBox lstEsp 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6105
      Left            =   3990
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   3
      Top             =   1140
      Width           =   3825
   End
   Begin VB.ListBox lstGen 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6330
      Left            =   60
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   2
      Top             =   1140
      Width           =   3825
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   10695
      _ExtentX        =   18865
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":2420
      PICN            =   "frmArquivos.frx":243C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGenericas 
      Height          =   270
      Left            =   3150
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   840
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   476
      BTYPE           =   3
      TX              =   "Todas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":3116
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEspecificas 
      Height          =   270
      Left            =   7080
      TabIndex        =   19
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   840
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   476
      BTYPE           =   3
      TX              =   "Todas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmArquivos.frx":3132
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Espec�ficas"
      Height          =   195
      Left            =   3990
      TabIndex        =   5
      Top             =   930
      Width           =   840
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Gen�ricas"
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   720
   End
End
Attribute VB_Name = "frmArquivos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCarga_Click()
    If lstPAACs.SelCount = 0 And lstRepresentantes.SelCount = 0 Then
        MsgBox "Informe o PAAC/Repres para gerar carga.", vbInformation, "Aten��o"
        Exit Sub
    End If

    If Dir("C:\OUT\ZIP\*.DBF") <> "" And (lstPAACs.SelCount + lstRepresentantes.SelCount > 1) Then
        MsgBox "N�o � possivel criar cargas para mais de um PAAC/Representante, " & vbCrLf & "Porque existem arquivos Espec�ficos.", vbInformation, "Aten��o"
        Exit Sub
    End If
    StatusBar1.Panels(1).Text = "Iniciando gera��o de Cargas..."
    Gerar_Carga
    StatusBar1.Panels(1).Text = "Cargas Geradas."
    FraCargas.Visible = False
End Sub

Private Sub cmdEspecificas_Click()
    For i = 0 To Me.lstEsp.ListCount - 1
        lstEsp.Selected(i) = Not (lstEsp.Selected(i))
    Next
End Sub

Private Sub cmdGenericas_Click()
    For i = 0 To Me.lstGen.ListCount - 1
        lstGen.Selected(i) = Not (lstGen.Selected(i))
    Next
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub


Private Sub Command1_Click()
    For i = 0 To lstPAACs.ListCount - 1
        lstPAACs.Selected(i) = IIf(lstPAACs.Selected(i) = True, False, True)
    Next
End Sub

Private Sub Command2_Click()
    For i = 0 To lstRepresentantes.ListCount - 1
        lstRepresentantes.Selected(i) = IIf(lstRepresentantes.Selected(i) = True, False, True)
    Next
End Sub

Private Sub Form_Load()
    '--------------------------------------------
    '-- Variaveis de Lican�a para uso do BWZIP
    MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    '--------------------------------------------
    
    Me.File_Esp.Path = "C:\OUT\ZIP"
    Me.File_Gen.Path = "C:\OUT\GENERICA"
    
    Preencher_Lists
    
    Carregar_PAACREPRES
    
    Me.Refresh
End Sub

Sub Preencher_Lists()

    lstEsp.Clear
    lstGen.Clear

    For i = 0 To File_Gen.ListCount - 1
        lstGen.AddItem File_Gen.List(i)
    Next
    For i = 0 To File_Esp.ListCount - 1
        lstEsp.AddItem File_Esp.List(i)
    Next
End Sub

Sub Carregar_PAACREPRES()
    Dim reg As Integer
    Dim rst As Object

    lstPAACs.Clear
    lstRepresentantes.Clear
    
    Set rst = vBanco_Prod.CreateDynaset("select B.Cod_filial,0 Cic_Gerente,E_mail,FL_VDR FRom   paac.controle_paac A, " & _
                                  " paac.filial_base B, paac.deposito_filial c Where a.cod_filial = B.cod_filial " & _
                                  " and    b.cod_filial = c.cod_filial and    c.fl_base = 'S' ORDER BY B.COD_FILIAL", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstPAACs.AddItem rst!cod_filial & vbTab & vbTab & ";" & rst!Cic_Gerente & ";" & rst!E_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

    Set rst = vBanco_Prod.CreateDynaset("select B.Cod_Repres, 0 Cic_Gerente,E_mail,FL_VDR From   paac.controle_representante A," & _
                                  " paac.representante_base B, paac.deposito_repres c Where  a.cod_repres = b.cod_repres and " & _
                                  " B.Cod_Repres = c.cod_repres and c.fl_base = 'S' ORDER BY A.COD_REPRES", 0&)
    
    For reg = 1 To rst.RecordCount
    
        lstRepresentantes.AddItem rst!cod_repres & vbTab & vbTab & ";" & rst!Cic_Gerente & ";" & rst!E_mail & ";" & rst!FL_VDR
        
        rst.MoveNext
    Next

End Sub


Sub Gerar_Carga()

    On Error GoTo Trata_Erro

    Dim Arquivo As String
    Dim num_arq As Integer
    Dim rst As Object
    Dim vCampos
    
    FileGenerica.Path = "C:\OUT\GENERICA"
    
    'Copiar arquivos selecionados, da Pasta C:\OUT\GENERICA para a pasta C:\OUT\ZIP
    StatusBar1.Panels(1).Text = "Copiando arquivos Genericos..."
    
    Mover_Copiar 1, "C:\OUT\GENERICA\", "C:\OUT\ZIP\", "FIL240.MDB"
    
    For i = 0 To lstGen.ListCount - 1
        If lstGen.Selected(i) = True Then
            Mover_Copiar 1, "C:\OUT\GENERICA\", "C:\OUT\ZIP\", lstGen.List(i)
        End If
    Next

'*****
'PAACs
'*****
    For i = 0 To lstPAACs.ListCount - 1
        
        If lstPAACs.Selected(i) = False Then GoTo proximo
        
        vCampos = Split(lstPAACs.List(i), ";")
        Arquivo = "CAR" & Format(vCampos(0), "0000")
        
        If Dir("C:\OUT\CARGA\" & Arquivo & ".HDR") <> "" Then
            Kill "C:\OUT\CARGA\" & Arquivo & ".HDR"
            Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        End If
        
        StatusBar1.Panels(1).Text = "Gerando carga para PAAC: " & vCampos(0)
       
       'MONTA O ARQUIVO DE HEADER DA CARGA
        num_arq = FreeFile
        Open "C:\OUT\CARGA\" & Arquivo & ".HDR" For Output As #num_arq
        
        Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
        Print #num_arq, "TO        =" & vCampos(2)
        Print #num_arq, "FROM      =DPKPEDID"
        Print #num_arq, "DESCRICAO =" & "#CAR"; Format(Val(vCampos(0)), "0000")
        Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
       'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
        Print #num_arq, "NRQUEBRA  =0"
        
        Close #num_arq
        
        Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        
        MaqZip1.ZipCompress "C:\OUT\ZIP", "*.*", "C:\OUT\CARGA\", Arquivo & ".ZIP", "-6"

proximo:
     Next
'**************
'Representantes
'**************
    For i = 0 To lstRepresentantes.ListCount - 1
        
        If lstRepresentantes.Selected(i) = False Then GoTo proximo1
        
        vCampos = Split(lstRepresentantes.List(i), ";")
        Arquivo = "CAR" & Format(vCampos(0), "0000")
        
        If Dir("C:\OUT\ZIP\" & Arquivo & ".HDR") <> "" Then
            Kill "C:\OUT\CARGA\" & Arquivo & ".HDR"
            Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        End If
        
        StatusBar1.Panels(1).Text = "Gerando carga para Representante: " & vCampos(0)
        
       'MONTA O ARQUIVO DE HEADER DA CARGA
        num_arq = FreeFile
        Open "C:\OUT\CARGA\" & Arquivo & ".HDR" For Output As #num_arq
        
        Print #num_arq, "GERACAO   =" & Format(Now, " mmm, dd yyyy ")
        Print #num_arq, "TO        =" & vCampos(2)
        Print #num_arq, "FROM      =DPKPEDID"
        Print #num_arq, "DESCRICAO =" & "#CAR"; Format(vCampos(0), "0000")
        Print #num_arq, "FILE      =" & Arquivo & ".ZIP"
       'A LINHA ABAIXO DIZ QUE N�O � PARA QUEBRAR O ARQUIVO EM V�RIOS MESMO QUE SEJA GRANDE
        Print #num_arq, "NRQUEBRA  =0"
        
        Close #num_arq
        
        If Dir("C:\OUT\CARGA\" & Arquivo & ".ZIP") <> "" Then
            Kill "C:\OUT\CARGA\" & Arquivo & ".ZIP"
        End If
        
        MaqZip1.ZipCompress "C:\OUT\ZIP\", "*.*", "C:\OUT\CARGA\", Arquivo & ".ZIP", "-6"
       
proximo1:
     Next

    'Deletar todos os arquivos DBFs existentes no C:\OUT\ZIP
    StatusBar1.Panels(1).Text = "Excluindo arquivos da Pasta ZIP."
    Do While Dir("C:\OUT\ZIP\*.DBF", vbArchive) <> ""
       Arquivo = Dir("C:\OUT\ZIP\*.DBF")
       Kill "c:\OUT\ZIP\" & Arquivo
    Loop
    
    If Dir("C:\OUT\ZIP\FIL240.MDB") <> "" Then
        Kill "C:\OUT\ZIP\FIL240.MDB"
    End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Gerar_carga" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description
    End If
End Sub


