VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{4CE56793-802C-4711-9B30-453D91A33B40}#5.0#0"; "bw6zp16r.ocx"
Begin VB.Form frmGerarDBF 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gerar tabelas em Arquivos DBFs para envio para o PAAC/Repres"
   ClientHeight    =   8265
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8265
   ScaleWidth      =   12540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkOverflow 
      Appearance      =   0  'Flat
      Caption         =   "Validar"
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   10290
      TabIndex        =   28
      Top             =   480
      Width           =   915
   End
   Begin VB.Data DataDbf 
      Caption         =   "Data1"
      Connect         =   "dBASE IV;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   345
      Left            =   9480
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   4080
      Visible         =   0   'False
      Width           =   2325
   End
   Begin BWZipCompress316b.MaqZip MaqZip1 
      Left            =   4410
      Top             =   1120
      _ExtentX        =   1058
      _ExtentY        =   1058
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   22
      Top             =   7890
      Width           =   12540
      _ExtentX        =   22119
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   22066
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   675
      Left            =   5130
      TabIndex        =   18
      Top             =   1020
      Width           =   7365
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Se gerar tabelas Especificas ou VDR, vc tem que gerar por PAAC/Repres e gerar cargas individuais."
         Height          =   195
         Left            =   90
         TabIndex        =   20
         Top             =   390
         Width           =   7125
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Se vc gerar Tabelas Genericas, elas podem ser enviadas como carga para todos os PAACs e Repres"
         Height          =   195
         Left            =   90
         TabIndex        =   19
         Top             =   150
         Width           =   7170
      End
   End
   Begin VB.Frame fraTipo 
      Appearance      =   0  'Flat
      Caption         =   "Tipo Base"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   975
      Left            =   90
      TabIndex        =   14
      Top             =   900
      Width           =   3495
      Begin VB.OptionButton optGerente 
         Appearance      =   0  'Flat
         Caption         =   "Gerente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   29
         Top             =   690
         Width           =   1530
      End
      Begin VB.OptionButton optPAAC 
         Appearance      =   0  'Flat
         Caption         =   "PAAC"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   17
         Top             =   210
         Value           =   -1  'True
         Width           =   1050
      End
      Begin VB.OptionButton optRepres 
         Appearance      =   0  'Flat
         Caption         =   "Representante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   30
         TabIndex        =   16
         Top             =   450
         Width           =   1530
      End
      Begin VB.TextBox txtCodPaacRepres 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   1770
         MaxLength       =   6
         TabIndex        =   15
         Top             =   240
         Width           =   840
      End
      Begin VB.Label lblVDR 
         AutoSize        =   -1  'True
         Caption         =   "VDR:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2730
         TabIndex        =   21
         Top             =   330
         Width           =   465
      End
   End
   Begin VB.ListBox lstVDR 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1560
      ItemData        =   "frmGerarDBF.frx":0000
      Left            =   9480
      List            =   "frmGerarDBF.frx":0016
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   9
      Top             =   2370
      Width           =   3000
   End
   Begin VB.ListBox lstEspecificas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5385
      ItemData        =   "frmGerarDBF.frx":0093
      Left            =   6330
      List            =   "frmGerarDBF.frx":00D3
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   8
      Top             =   2370
      Width           =   3000
   End
   Begin VB.ListBox lstEspecificasPorLojaRepres 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5385
      ItemData        =   "frmGerarDBF.frx":01F6
      Left            =   3210
      List            =   "frmGerarDBF.frx":022A
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   7
      Top             =   2370
      Width           =   3000
   End
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   2430
      Pattern         =   "*.DBF"
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   2085
   End
   Begin VB.Data DataInsert 
      Caption         =   "Data1"
      Connect         =   "dBASE IV;"
      DatabaseName    =   ""
      DefaultCursorType=   0  'DefaultCursor
      DefaultType     =   2  'UseODBC
      Exclusive       =   0   'False
      Height          =   315
      Left            =   870
      Options         =   0
      ReadOnly        =   0   'False
      RecordsetType   =   1  'Dynaset
      RecordSource    =   ""
      Top             =   240
      Visible         =   0   'False
      Width           =   1425
   End
   Begin VB.ListBox lstGenericas 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5385
      ItemData        =   "frmGerarDBF.frx":0300
      Left            =   60
      List            =   "frmGerarDBF.frx":0379
      Sorted          =   -1  'True
      Style           =   1  'Checkbox
      TabIndex        =   2
      Top             =   2370
      Width           =   3060
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   0
      Top             =   795
      Width           =   12465
      _ExtentX        =   21987
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":0598
      PICN            =   "frmGerarDBF.frx":05B4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerar 
      Height          =   660
      Left            =   3630
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1110
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1164
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":128E
      PICN            =   "frmGerarDBF.frx":12AA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEspecificas 
      Height          =   390
      Left            =   6330
      TabIndex        =   10
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1950
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Espec�ficas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":1F84
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdVDR 
      Height          =   390
      Left            =   9480
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1950
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "VDR"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":1FA0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdEspecificasDep 
      Height          =   390
      Left            =   3210
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1950
      Width           =   3000
      _ExtentX        =   5292
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Espec�ficas Dep�sito x Filial"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":1FBC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGenericas 
      Height          =   390
      Left            =   60
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Gerar"
      Top             =   1950
      Width           =   3060
      _ExtentX        =   5398
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   "Gen�ricas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":1FD8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdGerarCargas 
      Height          =   690
      Left            =   11340
      TabIndex        =   23
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   60
      Width           =   1170
      _ExtentX        =   2064
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   "Gerar Cargas"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGerarDBF.frx":1FF4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblHoraFim 
      Height          =   195
      Left            =   6600
      TabIndex        =   27
      Top             =   480
      Width           =   1395
   End
   Begin VB.Label lblHoraInicio 
      Height          =   195
      Left            =   5130
      TabIndex        =   26
      Top             =   480
      Width           =   1395
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Registros Inseridos:"
      Height          =   195
      Left            =   5130
      TabIndex        =   25
      Top             =   240
      Width           =   1380
   End
   Begin VB.Label lblRegistros 
      Height          =   195
      Left            =   6540
      TabIndex        =   24
      Top             =   240
      Width           =   1515
   End
   Begin VB.Label lblTabela 
      Caption         =   "Label2"
      Height          =   195
      Left            =   5130
      TabIndex        =   5
      Top             =   0
      Width           =   6075
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Tabela:"
      Height          =   195
      Left            =   90
      TabIndex        =   4
      Top             =   4860
      Width           =   540
   End
End
Attribute VB_Name = "frmGerarDBF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



'Private Sub cmdCarga_Click()
'    If Dir("C:\OUT\ZIP\*.DBF") <> "" And (lstPAACs.SelCount + lstRepresentantes.SelCount > 1) Then
'        MsgBox "N�o � possivel criar cargas para mais de um PAAC/Representante, " & vbCrLf & "Porque existem arquivos Espec�ficos.", vbInformation, "Aten��o"
'        Exit Sub
'    End If
'    StatusBar1.Panels(1).Text = "Iniciando gera��o de Cargas..."
'    Gerar_Carga
'    StatusBar1.Panels(1).Text = "Cargas Geradas."
'    FraCargas.Visible = False
'End Sub

Private Sub cmdEspecificas_Click()
    For i = 0 To frmGerarDBF.lstEspecificas.ListCount - 1
        frmGerarDBF.lstEspecificas.Selected(i) = Not (frmGerarDBF.lstEspecificas.Selected(i))
    Next
End Sub

Private Sub cmdEspecificasDep_Click()
    For i = 0 To frmGerarDBF.lstEspecificasPorLojaRepres.ListCount - 1
        frmGerarDBF.lstEspecificasPorLojaRepres.Selected(i) = Not (frmGerarDBF.lstEspecificasPorLojaRepres.Selected(i))
    Next
End Sub

Private Sub cmdGenericas_Click()
    For i = 0 To frmGerarDBF.lstGenericas.ListCount - 1
        frmGerarDBF.lstGenericas.Selected(i) = Not (frmGerarDBF.lstGenericas.Selected(i))
    Next
End Sub

Private Sub cmdGerar_Click()
    Dim vArquivo As String
    Dim vFL_VDR As String
    
    On Error GoTo Trata_Erro

    If lstGenericas.SelCount <= 0 And lstEspecificasPorLojaRepres.SelCount <= 0 And Me.lstEspecificas.SelCount <= 0 And Me.lstVDR.SelCount <= 0 Then
        MsgBox "Selecione uma tabela.", vbInformation, "Aten��o"
        Exit Sub
    End If
    If txtCodPaacRepres = "" And (lstEspecificasPorLojaRepres.SelCount > 0 Or Me.lstEspecificas.SelCount > 0 Or lstVDR.SelCount > 0) Then
        MsgBox "Informe o C�digo.", vbInformation, "Aten��o"
        txtCodPaacRepres.SetFocus
        Exit Sub
    End If
    
    Erase vArqsExcluir
    lblHoraInicio = Now
    lblTabela.Caption = "Iniciando... Aguarde..."
    Me.Refresh
    lblRegistros.Caption = ""
    
	'RTI-143 - Definindo o Tipo do Banco como Gerente
    If optPAAC.Value = True Then
        vTipoBase = "P"
    ElseIf optGerente.Value = True Then
        vTipoBase = "G"
    Else
        vTipoBase = "R"
    End If
    
	'RTI-143 - Verificando se o Usu�rio informado � um Gerente
    If (vTipoBase = "G") Then
        
        If Not (VerificaGerente(CInt(txtCodPaacRepres.Text))) Then
            MsgBox "O usu�rio informado n�o � um Gerente!" & vbCrLf & "Favor informar um usu�rio do tipo Gerente.", vbExclamation
            Exit Sub
        End If
    End If
    
    'Deletar todos os arquivos DBFs existentes no C:\OUT\COMPLETA
    lblTabela.Caption = "Excluindo arquivo da pasta C:\OUT\COMPLETA\*.DBF"
    Me.Refresh
    Do While Dir("C:\OUT\COMPLETA\*.dbf", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\COMPLETA\*.DBF")
        Kill "c:\OUT\COMPLETA\" & vArquivo
    Loop
    
    lblTabela.Caption = "Excluindo arquivo da pasta C:\OUT\COMPLETA\*.MDB"
    Me.Refresh
    Do While Dir("C:\OUT\COMPLETA\*.MDB", vbArchive) <> ""
        vArquivo = Dir("C:\OUT\COMPLETA\*.MDB")
        Kill "c:\OUT\COMPLETA\" & vArquivo
    Loop
    
    'Descompactar os arquivos DBFs
    lblTabela.Caption = "Descompactando arquivo VAZIO.ZIP"
    Me.Refresh
    MaqZip1.ZipUncompress "C:\OUT\COMPLETA\VAZIO.ZIP", "*", "C:\OUT\COMPLETA\", "-o"
    
    'Copiar o arquivo FIL240.MDB para Generica
    lblTabela.Caption = "Copiando arquivo FIL240.MDB"
    Me.Refresh
    FileCopy "C:\OUT\COMPLETA\FIL240.MDB", "C:\OUT\ZIP\FIL240.MDB"
    FileCopy "C:\OUT\COMPLETA\FIL240.MDB", "C:\OUT\GENERICA\FIL240.MDB"
    Kill "C:\OUT\COMPLETA\FIL240.MDB"
    
    Criar_Cursor vBanco
    Criar_Cursor vBanco_Prod
    DataInsert.DatabaseName = "C:\OUT\COMPLETA\"

    vDe = "C:\OUT\COMPLETA\"
    vPara = "C:\OUT\GENERICA\"
    
    If lstGenericas.SelCount > 0 Then
        
        If Dir("C:\OUT\GENERICA\*.*", vbArchive) <> "" Then
            
            Me.File1.Path = "C:\OUT\GENERICA"
            
            lblTabela.Caption = "Excluindo arquivos Genericos"
            Me.Refresh
            
            'Verificar se existe algum arquivo no diretorio de genericas com data de ontem
            'se existir, apagar todos os arquivos.
            For i = 0 To File1.ListCount
               vArquivo = File1.List(i)
               If vArquivo = "" Then GoTo Iniciar
               lblTabela.Caption = "Excluindo arquivo: " & vArquivo
               If FileDateTime("C:\OUT\GENERICA\" & vArquivo) < Date Then
                  lblTabela.Caption = "Excluindo arquivo: " & vArquivo
                  Me.Refresh
                  Kill "c:\OUT\GENERICA\" & vArquivo
               End If
            Next
            
            If Dir("C:\OUT\GENERICA\*.*", vbArchive) <> "" Then
                If MsgBox("Confirma a exclus�o dos arquivos da pasta GENERICA ?", vbInformation + vbYesNo, "Ante��o") = vbYes Then
                    'Deletar todos os arquivos DBFs existentes no C:\OUT\GENERICA
                    Do While Dir("C:\OUT\GENERICA\*.dbf", vbArchive) <> ""
                        vArquivo = Dir("C:\OUT\GENERICA\*.DBF")
                        Kill "c:\OUT\GENERICA\" & vArquivo
                    Loop
                End If
            End If
        End If
        
Iniciar:
        
        lblTabela.Caption = ""
        
        Me.Refresh
                    
'        'Copiar o arquivo FILIAL_E.DBF para a pasta C:\OUT\ZIP
'        If Gerar_Especificas_Por_Dep_Filial_DBF("FILIAL_ENDERECO") = True Then
'            FileCopy vDe & "FILIAL_E.DBF", vPara & "FILIAL_E.DBF"
'        End If
                    
        If Gerar_Genericas_Dbf("ANTECIPACAO_TRIBUTARIA") = True Then Call ANTECIPACAO_TRIBUTARIA_dbf
        If Gerar_Genericas_Dbf("APLICACAO") = True Then Call APLICACAO_DBF
        If Gerar_Genericas_Dbf("banco") = True Then Call BANCO_DBF
        If Gerar_Genericas_Dbf("CATEG_SINAL") = True Then Call CATEG_SINAL_dbf
        If Gerar_Genericas_Dbf("CANCEL_PEDNOTA") = True Then Call CANCEL_PEDNOTA_dbf
        If Gerar_Genericas_Dbf("CIDADE") = True Then Call CIDADE_DBF
        If Gerar_Genericas_Dbf("CLASS_ANTEC_ENTRADA") = True Then Call CLASS_ANTEC_ENTRADA_DBF
        If Gerar_Genericas_Dbf("CLASS_FISCAL_RED_BASE") = True Then Call CLASS_FISCAL_RED_BASE_DBF
        If Gerar_Genericas_Dbf("CLIE_MENSAGEM") = True Then Call CLIE_MENSAGEM_DBF
        If Gerar_Genericas_Dbf("DATAS") = True Then Call DATAS_DBF
        If Gerar_Genericas_Dbf("DOLAR_DIARIO") = True Then Call DOLAR_DIARIO_DBF
        If Gerar_Genericas_Dbf("EMBALADOR") = True Then Call EMBALADOR_DBF
        If Gerar_Genericas_Dbf("Filial") = True Then Call Filial_DBF
        If Gerar_Genericas_Dbf("FORNECEDOR") = True Then Call FORNECEDOR_DBF
        If Gerar_Genericas_Dbf("FORNECEDOR_ESPECIFICO") = True Then Call FORNECEDOR_ESPECIFICO_DBF
        If Gerar_Genericas_Dbf("FRETE_UF_BLOQ") = True Then Call FRETE_UF_BLOQ_DBF
        If Gerar_Genericas_Dbf("GRUPO") = True Then Call GRUPO_DBF
        If Gerar_Genericas_Dbf("ITEM_CADASTRO") = True Then Call ITEM_CADASTRO_DBF
        If Gerar_Genericas_Dbf("MONTADORA") = True Then Call MONTADORA_DBF
        If Gerar_Genericas_Dbf("NATUREZA_OPERACAO") = True Then Call NATUREZA_OPERACAO_DBF
        If Gerar_Genericas_Dbf("PLANO_PGTO") = True Then Call PLANO_PGTO_DBF
        If Gerar_Genericas_Dbf("R_DPK_EQUIV") = True Then Call R_DPK_EQUIV_DBF
        If Gerar_Genericas_Dbf("R_FABRICA_DPK") = True Then Call R_FABRICA_DPK_DBF
        If Gerar_Genericas_Dbf("R_FILDEP") = True Then Call R_FILDEP_DBF
        If Gerar_Genericas_Dbf("R_UF_DEPOSITO") = True Then Call R_UF_DEPOSITO_DBF
        If Gerar_Genericas_Dbf("RESULTADO") = True Then Call RESULTADO_DBF
        If Gerar_Genericas_Dbf("SUBGRUPO") = True Then Call SUBGRUPO_DBF
        If Gerar_Genericas_Dbf("SUBST_TRIBUTARIA") = True Then Call SUBST_TRIBUTARIA_DBF
        If Gerar_Genericas_Dbf("TABELA_DESCPER") = True Then Call TABELA_DESCPER_DBF
        If Gerar_Genericas_Dbf("TABELA_VENDA") = True Then Call TABELA_VENDA_DBF
        If Gerar_Genericas_Dbf("TIPO_CLIENTE") = True Then Call TIPO_CLIENTE_DBF
        If Gerar_Genericas_Dbf("TRANSPORTADORA") = True Then Call TRANSPORTADORA_DBF
        If Gerar_Genericas_Dbf("UF") = True Then Call UF_DBF
        If Gerar_Genericas_Dbf("UF_DPK") = True Then Call UF_DPK_DBF
        If Gerar_Genericas_Dbf("UF_ORIGEM_DESTINO") = True Then Call UF_ORIGEM_DESTINO_DBF
        If Gerar_Genericas_Dbf("UF_TPCLIENTE") = True Then Call UF_TPCLIENTE_DBF
        If Gerar_Genericas_Dbf("TIPO_CLIENTE_BLAU") = True Then Call TIPO_CLIENTE_BLAU_DBF
        If Gerar_Genericas_Dbf("VENDA_LIMITADA") = True Then Call VENDA_LIMITADA_DBF
    
        '26/02/2007
        If Gerar_Genericas_Dbf("TRANSFERENCIA") = True Then Call Transferencia_DBF
    
    
    End If
    
    If lstEspecificasPorLojaRepres.SelCount > 0 Or lstEspecificas.SelCount > 0 Or lstVDR.SelCount > 0 Then
        
        vDe = "C:\OUT\COMPLETA\"
        vPara = "C:\OUT\ZIP\"
        
        '----------------------------------------------
        '-- TABELAS ESPECIFICAS POR DEPOSITO x FILIAL
        
        vBanco_Prod.Parameters.Remove "vTipoBase"
        vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
        vBanco_Prod.Parameters.Remove "TIPO_BASE"
        vBanco_Prod.Parameters.Add "TIPO_BASE", "C", 1
        vBanco_Prod.Parameters.Remove "PM_CODIGO"
        vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerarDBF.txtCodPaacRepres, 1
        Criar_Cursor vBanco_Prod
    
        '---------------> Seleciona quem ser� gerada base <-----------------
        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_VDR(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
        vBanco_Prod.ExecuteSQL vSql
        
        Set vObjOracle = vBanco_Prod.Parameters("vCursor").Value
            
        If vTipoBase = "P" Then
           vCodFilial = vObjOracle!cod_filial
        Else
           vCodFilial = vObjOracle!cod_repres
        End If
        
        vFL_VDR = vObjOracle!FL_VDR
        
        Me.lblVDR.Caption = "VDR: " & vFL_VDR
        
        If Gerar_Especificas_Por_Dep_Filial_DBF("DEPOSITO_VISAO") = True Then Call DEPOSITO_VISAO_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("ITEM_ANALITICO") = True Then Call ITEM_ANALITICO_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("ITEM_ESTOQUE") = True Then Call ITEM_ESTOQUE_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("ITEM_GLOBAL") = True Then Call ITEM_GLOBAL_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("ITEM_PRECO") = True Then Call ITEM_PRECO_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("TAXA") = True Then Call TAXA_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("UF_DEPOSITO") = True Then Call UF_DEPOSITO_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("FRETE_UF") = True Then Call FRETE_UF_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("FRETE_UF_TRANSP") = True Then Call FRETE_UF_TRANSP_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("LOJA") = True Then Call LOJA_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("FRETE_ENTREGA") = True Then Call FRETE_ENTREGA_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("CLIENTE_CARACTERISTICA") = True Then Call CLIENTE_CARACTERISTICA_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("CESTA_ITEM") = True Then Call CESTA_ITEM_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("CESTA_VENDA") = True Then Call CESTA_VENDA_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("UF_CATEG") = True Then Call UF_CATEG_DBF
        If Gerar_Especificas_Por_Dep_Filial_DBF("ALIQUOTA_ME") = True Then Call ALIQUOTA_ME_DBF
    
        '-----------------
        'Bases Especificas
        '-----------------
        
        ''If Gerar_Especificas("DEPOSITO") = True Then Call DEPOSITO
        If Gerar_Especificas_DBF("REPRESENTANTE") = True Then Call REPRESENTANTE_DBF
        If Gerar_Especificas_DBF("FRANQUEADOR") = True Then Call FRANQUEADOR_DBF
        If Gerar_Especificas_DBF("REPR_END_CORRESP") = True Then Call REPR_END_CORRESP_DBF
        If Gerar_Especificas_DBF("REPRESENTACAO") = True Then Call REPRESENTACAO_DBF
        If Gerar_Especificas_DBF("R_REPCGC") = True Then Call R_REPCGC_DBF
        If Gerar_Especificas_DBF("R_REPVEN") = True Then Call R_REPVEN_DBF
        If Gerar_Especificas_DBF("CLIENTE") = True Then Call CLIENTE_DBF
        If Gerar_Especificas_DBF("CLIE_CREDITO") = True Then Call CLIE_CREDITO_DBF
        If Gerar_Especificas_DBF("CLIE_ENDERECO") = True Then Call CLIE_ENDERECO_DBF
        If Gerar_Especificas_DBF("CLIENTE_INTERNET") = True Then Call CLIE_INTERNET_DBF
        If Gerar_Especificas_DBF("SALDO_PEDIDOS") = True Then Call SALDO_PEDIDOS_DBF
        If Gerar_Especificas_DBF("DUPLICATAS") = True Then Call DUPLICATAS_DBF
        If Gerar_Especificas_DBF("PEDNOTA_VENDA") = True Then Call PEDNOTA_VENDA_DBF
        If Gerar_Especificas_DBF("ITPEDNOTA_VENDA") = True Then Call ITPEDNOTA_VENDA_DBF
        If Gerar_Especificas_DBF("R_PEDIDO_CONF") = True Then Call R_PEDIDO_CONF_DBF
        If Gerar_Especificas_DBF("ROMANEIO") = True Then Call ROMANEIO_DBF
        If Gerar_Especificas_DBF("V_CLIENTE_FIEL") = True Then Call V_CLIENTE_FIEL_DBF
        If Gerar_Especificas_DBF("V_PEDLIQ_VENDA") = True Then Call V_PEDLIQ_VENDA_DBF
        If Gerar_Especificas_DBF("R_CLIE_REPRES") = True Then Call R_CLIE_REPRES_DBF
        If Gerar_Especificas_DBF("CLIENTE_ACUMULADO") = True Then Call CLIENTE_ACUMULADO_DBF
        
        '---
        'VDR
        '---
        
        If vFL_VDR = "S" Then
           If Gerar_VDR_DBF("VDR_CLIENTE_CATEG_VDR") = True Then Call CLIENTE_CATEG_VDR_DBF
           If Gerar_VDR_DBF("VDR_CONTROLE_VDR") = True Then Call VDR_CONTROLE_VDR_DBF
           If Gerar_VDR_DBF("VDR_ITEM_PRECO") = True Then Call VDR_ITEM_PRECO_DBF
           If Gerar_VDR_DBF("VDR_R_CLIE_LINHA_PRODUTO") = True Then Call VDR_R_CLIE_LINHA_PRODUTO_DBF
           If Gerar_VDR_DBF("VDR_DESCONTO_CATEG") = True Then Call VDR_DESCONTO_CATEG_DBF
           If Gerar_VDR_DBF("VDR_TABELA_VENDA") = True Then Call VDR_TABELA_VENDA_DBF
        End If
        
        
        
    End If
    
    Deletar_Arquivos
    
'    Verificar_Tabelas
    
    frmGerarDBF.lblTabela.Caption = "Gera��o Completa."
    
    lblHoraFim = Now
    
Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox Err.Number & vbCrLf & Err.Description
    End If

End Sub

Private Sub cmdGerarCargas_Click()
    frmArquivos.Show 1
'    FraCargas.Visible = True
End Sub

Private Sub cmdVDR_Click()
    For i = 0 To frmGerarDBF.lstVDR.ListCount - 1
        frmGerarDBF.lstVDR.Selected(i) = Not (frmGerarDBF.lstVDR.Selected(i))
    Next
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    Me.Refresh
End Sub


Sub Deletar_Arquivos()
    On Error Resume Next
    Dim vArq As String
    Dim vExcluir As Boolean
    Dim ii As Integer
    
    File1.Path = "C:\OUT\COMPLETA\"
    
    For i = 0 To File1.ListCount - 1
        vExcluir = True
        For ii = 0 To 99
            If vArqsExcluir(ii) = "" Then Exit For
            If UCase(File1.List(i)) = UCase(vArqsExcluir(ii)) & ".DBF" = True Then
                vExcluir = False
                Exit For
            End If
        Next
        If vExcluir = True Then Kill "C:\OUT\COMPLETA\" & File1.List(i)
    Next

End Sub


Private Sub Form_Load()
    '--------------------------------------------
    '-- Variaveis de Lican�a para uso do BWZIP
    MaqZip1.LicenseUserName "0e4e2c04d621384948480bd33d37c3123017d1fe77a01911f0099"
    MaqZip1.LicenseUserEmail "03fdef4cd76fbdf532557d9bb920e8e4969b3b5892c369a"
    MaqZip1.LicenseUserData "0fd726597fefc824282b0a04cb96bce512a637c19390e02"
    '--------------------------------------------

End Sub


Public Function Verificar_OverFlow(pTabela As String, pTipo As String, pOracle As Object) As Boolean
    Dim i As Integer
    Dim vCaminho As String
    Dim vArq As String
    
    If Me.chkOverflow.Value = 0 Then Exit Function
    
    If pTipo = "E" Then
        vCaminho = "C:\OUT\COMPLETA"
    ElseIf pTipo = "G" Then
        vCaminho = "C:\OUT\GENERICA"
    End If
    
    If Dir(vCaminho & "\") = "" Then Exit Function
    
    DataInsert.DatabaseName = vCaminho
    
    Do While Dir(vCaminho & "\") <> ""
        If vArq <> Dir(vCaminho & "\") Then
            vArq = Dir(vCaminho & "\")
        Else
            Exit Do
        End If
        If UCase(vArq) = pTabela & ".DBF" Then
            GoTo Prosseguir
        End If
    Loop
    DataInsert.DatabaseName = "C:\OUT\COMPLETA\"
    Exit Function
    
Prosseguir:
    DataInsert.RecordSource = ""
    DataInsert.Refresh
    
    DataInsert.RecordSource = pTabela
    DataInsert.Refresh
    
    If DataInsert.Recordset.RecordCount = 0 Then
        Verificar_OverFlow = False
        Exit Function
    End If
    
    Do While DataInsert.Recordset.EOF = False
       For i = 0 To DataInsert.Recordset.Fields.Count - 1
           If DataInsert.Recordset.Fields(i) <> pOracle.Fields(i) Then
              Verificar_OverFlow = True
              MsgBox "A tabela " & pTabela & " possui campos com OverFlow." & vbCrLf & _
                     "N�o envie estes dados para o PAAC ou Representante." & vbCrLf & _
                     "Por favor avise o Analista.", vbInformation, "Aten��o"
              Exit Function
            End If
        Next
        DataInsert.Recordset.MoveNext
    Loop
    
    Verificar_OverFlow = False
    DataInsert.DatabaseName = "C:\OUT\COMPLETA\"
                        
End Function

Sub Verificar_Tabelas()

    Debug.Print Now
    
    File1.Path = "C:\OUT\ZIP\*.DBF"
    
    For i = 0 To File1.ListCount - 1
        
        DataInsert.RecordSource = File1.List(i)
        DataInsert.Refresh
        
        Select Case UCase(File1.List(i))
            Case "ANTECIPA.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "APLICACA.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "BANCO.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CANCEL_P.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CATEG_S.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CIDADE.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CLASSANT.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CLIE_MEN.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CLIENTE.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CLIEACUM.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "CLIE_CAR.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "DATAS.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "DEPOSITO.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "DEP_VISA.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "DOLAR_DI.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FILIAL.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FORNECED.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FORN_ESP.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FRETE_EN.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FRETE_UF.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FRETEUFB.DBF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "FRETE_UF_TRANSP"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "GRUPO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "ITEM_ANALITICO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "ITEM_CADASTRO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "ITEM_ESTOQUE"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "ITEM_GLOBAL"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "ITEM_PRECO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "LOJA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "MONTADORA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "NATUREZA_OPERACAO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "PLANO_PGTO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_CLIE_REPRES"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_DPK_EQUIV"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_FABRICA_DPK"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_FILDEP"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_REPCGC"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "R_REPVEN"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "REPRESENTACAO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "REPRESENTANTE"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "SUBGRUPO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "SUBST_TRIBUTARIA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "TABELA_DESCPER"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "TABELA_VENDA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "TIPO_CLIENTE"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "TIPO_CLIENTE_BLAU"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "TRANSPORTADORA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF_CATEG"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF_DEPOSITO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF_DPK"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF_ORIGEM_DESTINO"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "UF_TPCLIENTE"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "V_CLIENTE_FIEL"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
            Case "VENDA_LIMITADA"
                If DataInsert.Recordset.RecordCount = 0 Then GoTo Mensagem
        End Select
    Next
Mensagem:
    'MsgBox "Tabela " & UCase(dbAccess2.TableDefs(i).Name) & " vazia." & vbCrLf & "Avise o Suporte", vbInformation, "Aten��o"

Debug.Print Now
End Sub

