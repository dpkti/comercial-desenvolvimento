VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.MDIForm mdiFil400 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "Fil400 - Gera��o de Base Completa"
   ClientHeight    =   8175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   Icon            =   "mdiFil400.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11880
      _Version        =   65536
      _ExtentX        =   20955
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin Bot�o.cmd cmdSobre 
         Height          =   1005
         Left            =   9750
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":23D2
         PICN            =   "mdiFil400.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   10800
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":30C8
         PICN            =   "mdiFil400.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   4
         Top             =   1125
         Width           =   11760
         _ExtentX        =   20743
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdGerar 
         Height          =   1005
         Left            =   1500
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Base Completa"
         Top             =   60
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Gerar MDB"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":3DBE
         PICN            =   "mdiFil400.frx":3DDA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdParametros 
         Height          =   1005
         Left            =   3900
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Base Completa"
         Top             =   60
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Parametros"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":4AB4
         PICN            =   "mdiFil400.frx":4AD0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdCadastro 
         Height          =   1005
         Left            =   2850
         TabIndex        =   7
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Base Completa"
         Top             =   60
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Cadastro"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":524A
         PICN            =   "mdiFil400.frx":5266
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdRepresentantes 
         Height          =   1005
         Left            =   5160
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Base Completa"
         Top             =   60
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Gerar Txt de Representantes"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":5F40
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmd1 
         Height          =   1005
         Left            =   6870
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Gerar Base Completa"
         Top             =   60
         Width           =   1665
         _ExtentX        =   2937
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Gerar Tabelas Completas DBF"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiFil400.frx":5F5C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiFil400.frx":5F78
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7845
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   7805
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "15/02/08"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "08:54"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "mdiFil400"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmd1_Click()
    frmGerarDBF.Show 1
End Sub

Private Sub cmdCadastro_Click()
    frmCadastro.Show 1
End Sub

Private Sub cmdGerar_Click()
    frmGerar.Show
End Sub

Private Sub cmdParametros_Click()
    frmParametros.Show 1
End Sub

Private Sub cmdRepresentantes_Click()
    frmRepresentantes.Show 1
End Sub

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then
    
        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing
        
        End
    
    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub MDIForm_Load()

    frmLogo.Show 1
    
    'Banco Batch para Gera��o da Base
    Set vSessao = CreateObject("oracleinprocserver.xorasession")
    Set vBanco = vSessao.OpenDatabase("batch", "fil400/prod", 0&)
      
    'Banco Produ��o para Cadastros dos Dados do Novo Repres / PAAC
    Set vSessao_Prod = CreateObject("oracleinprocserver.xorasession")
    Set vBanco_Prod = vSessao_Prod.OpenDatabase("PRODUCAO", "fil400/prod", 0&)
    
End Sub
