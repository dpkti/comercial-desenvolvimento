﻿namespace Presentation
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpArq = new System.Windows.Forms.GroupBox();
            this.btnExcluirTodos = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.cboColForn = new System.Windows.Forms.ComboBox();
            this.pnlItem = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNrItem = new System.Windows.Forms.TextBox();
            this.txtCodFabrica = new System.Windows.Forms.TextBox();
            this.txtCodDPK = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDescItem = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cboTipoItem = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSalvarItem = new System.Windows.Forms.Button();
            this.btnFecharItem = new System.Windows.Forms.Button();
            this.lblMargem = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.cboColFab = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cboColQtd = new System.Windows.Forms.ComboBox();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnExportar = new System.Windows.Forms.Button();
            this.grdArquivo = new System.Windows.Forms.DataGridView();
            this.chkExcluir = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnBuscarCotacao = new System.Windows.Forms.Button();
            this.txtCodCotacao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.grpCad = new System.Windows.Forms.GroupBox();
            this.pnlBusca = new System.Windows.Forms.Panel();
            this.btnFechar = new System.Windows.Forms.Button();
            this.grdBusca = new System.Windows.Forms.DataGridView();
            this.txtMargem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDesc = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboTabela = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboTipo = new System.Windows.Forms.ComboBox();
            this.btnBuscarVend = new System.Windows.Forms.Button();
            this.txtCodVend = new System.Windows.Forms.TextBox();
            this.txtVend = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnBuscarPlano = new System.Windows.Forms.Button();
            this.txtCodPlano = new System.Windows.Forms.TextBox();
            this.txtPlano = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnBuscarCliente = new System.Windows.Forms.Button();
            this.txtCaminhoArquivo = new System.Windows.Forms.TextBox();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.txtCodCliente = new System.Windows.Forms.TextBox();
            this.cboDeposito = new System.Windows.Forms.ComboBox();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.mskDtValidade = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtValorTotal = new System.Windows.Forms.TextBox();
            this.txtQtdeTotal = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.grpArq.SuspendLayout();
            this.pnlItem.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdArquivo)).BeginInit();
            this.grpCad.SuspendLayout();
            this.pnlBusca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdBusca)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.InitialDirectory = "c:\\";
            this.openFileDialog1.Title = "Selecionar Arquivo";
            // 
            // grpArq
            // 
            this.grpArq.Controls.Add(this.txtValorTotal);
            this.grpArq.Controls.Add(this.txtQtdeTotal);
            this.grpArq.Controls.Add(this.label22);
            this.grpArq.Controls.Add(this.label21);
            this.grpArq.Controls.Add(this.btnExcluirTodos);
            this.grpArq.Controls.Add(this.label20);
            this.grpArq.Controls.Add(this.cboColForn);
            this.grpArq.Controls.Add(this.pnlItem);
            this.grpArq.Controls.Add(this.lblMargem);
            this.grpArq.Controls.Add(this.dataGridView1);
            this.grpArq.Controls.Add(this.label14);
            this.grpArq.Controls.Add(this.cboColFab);
            this.grpArq.Controls.Add(this.label13);
            this.grpArq.Controls.Add(this.cboColQtd);
            this.grpArq.Controls.Add(this.btnFinalizar);
            this.grpArq.Controls.Add(this.btnExportar);
            this.grpArq.Controls.Add(this.grdArquivo);
            this.grpArq.Location = new System.Drawing.Point(12, 168);
            this.grpArq.Name = "grpArq";
            this.grpArq.Size = new System.Drawing.Size(807, 422);
            this.grpArq.TabIndex = 4;
            this.grpArq.TabStop = false;
            this.grpArq.Text = "Dados do Arquivo";
            // 
            // btnExcluirTodos
            // 
            this.btnExcluirTodos.Location = new System.Drawing.Point(647, 393);
            this.btnExcluirTodos.Name = "btnExcluirTodos";
            this.btnExcluirTodos.Size = new System.Drawing.Size(154, 23);
            this.btnExcluirTodos.TabIndex = 38;
            this.btnExcluirTodos.Text = "Excluir Selecionados";
            this.btnExcluirTodos.UseVisualStyleBackColor = true;
            this.btnExcluirTodos.Click += new System.EventHandler(this.btnExcluirTodos_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(297, 23);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(61, 13);
            this.label20.TabIndex = 37;
            this.label20.Text = "Fornecedor";
            // 
            // cboColForn
            // 
            this.cboColForn.FormattingEnabled = true;
            this.cboColForn.Location = new System.Drawing.Point(365, 15);
            this.cboColForn.Name = "cboColForn";
            this.cboColForn.Size = new System.Drawing.Size(65, 21);
            this.cboColForn.TabIndex = 36;
            // 
            // pnlItem
            // 
            this.pnlItem.Controls.Add(this.groupBox2);
            this.pnlItem.Controls.Add(this.groupBox1);
            this.pnlItem.Location = new System.Drawing.Point(263, 60);
            this.pnlItem.Name = "pnlItem";
            this.pnlItem.Size = new System.Drawing.Size(311, 204);
            this.pnlItem.TabIndex = 35;
            this.pnlItem.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtNrItem);
            this.groupBox2.Controls.Add(this.txtCodFabrica);
            this.groupBox2.Controls.Add(this.txtCodDPK);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtDescItem);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.cboTipoItem);
            this.groupBox2.Location = new System.Drawing.Point(3, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 140);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados do Item";
            // 
            // txtNrItem
            // 
            this.txtNrItem.Location = new System.Drawing.Point(164, 9);
            this.txtNrItem.MaxLength = 3;
            this.txtNrItem.Name = "txtNrItem";
            this.txtNrItem.ReadOnly = true;
            this.txtNrItem.Size = new System.Drawing.Size(135, 20);
            this.txtNrItem.TabIndex = 43;
            // 
            // txtCodFabrica
            // 
            this.txtCodFabrica.Location = new System.Drawing.Point(164, 35);
            this.txtCodFabrica.MaxLength = 19;
            this.txtCodFabrica.Name = "txtCodFabrica";
            this.txtCodFabrica.ReadOnly = true;
            this.txtCodFabrica.Size = new System.Drawing.Size(135, 20);
            this.txtCodFabrica.TabIndex = 42;
            // 
            // txtCodDPK
            // 
            this.txtCodDPK.Location = new System.Drawing.Point(164, 61);
            this.txtCodDPK.MaxLength = 18;
            this.txtCodDPK.Name = "txtCodDPK";
            this.txtCodDPK.ReadOnly = true;
            this.txtCodDPK.Size = new System.Drawing.Size(135, 20);
            this.txtCodDPK.TabIndex = 41;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 40;
            this.label19.Text = "Código DPK";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 42);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "Código Fábrica";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 13);
            this.label17.TabIndex = 38;
            this.label17.Text = "Número Item";
            // 
            // txtDescItem
            // 
            this.txtDescItem.Location = new System.Drawing.Point(164, 114);
            this.txtDescItem.MaxLength = 6;
            this.txtDescItem.Name = "txtDescItem";
            this.txtDescItem.Size = new System.Drawing.Size(65, 20);
            this.txtDescItem.TabIndex = 35;
            this.txtDescItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescItem_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 121);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(157, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Desconto ou Acréscimo Padrão";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 95);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 36;
            this.label16.Text = "Tabela de Venda";
            // 
            // cboTipoItem
            // 
            this.cboTipoItem.FormattingEnabled = true;
            this.cboTipoItem.Location = new System.Drawing.Point(164, 87);
            this.cboTipoItem.Name = "cboTipoItem";
            this.cboTipoItem.Size = new System.Drawing.Size(65, 21);
            this.cboTipoItem.TabIndex = 34;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSalvarItem);
            this.groupBox1.Controls.Add(this.btnFecharItem);
            this.groupBox1.Location = new System.Drawing.Point(3, 151);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 50);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            // 
            // btnSalvarItem
            // 
            this.btnSalvarItem.Location = new System.Drawing.Point(131, 19);
            this.btnSalvarItem.Name = "btnSalvarItem";
            this.btnSalvarItem.Size = new System.Drawing.Size(75, 23);
            this.btnSalvarItem.TabIndex = 20;
            this.btnSalvarItem.Text = "Salvar";
            this.btnSalvarItem.UseVisualStyleBackColor = true;
            this.btnSalvarItem.Click += new System.EventHandler(this.btnSalvarItem_Click);
            // 
            // btnFecharItem
            // 
            this.btnFecharItem.Location = new System.Drawing.Point(212, 19);
            this.btnFecharItem.Name = "btnFecharItem";
            this.btnFecharItem.Size = new System.Drawing.Size(75, 23);
            this.btnFecharItem.TabIndex = 18;
            this.btnFecharItem.Text = "Fechar";
            this.btnFecharItem.UseVisualStyleBackColor = true;
            this.btnFecharItem.Click += new System.EventHandler(this.btnFecharItem_Click);
            // 
            // lblMargem
            // 
            this.lblMargem.AutoSize = true;
            this.lblMargem.Location = new System.Drawing.Point(445, 23);
            this.lblMargem.Name = "lblMargem";
            this.lblMargem.Size = new System.Drawing.Size(0, 13);
            this.lblMargem.TabIndex = 31;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(430, -145);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(370, 130);
            this.dataGridView1.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Código Fábrica";
            // 
            // cboColFab
            // 
            this.cboColFab.FormattingEnabled = true;
            this.cboColFab.Location = new System.Drawing.Point(87, 15);
            this.cboColFab.Name = "cboColFab";
            this.cboColFab.Size = new System.Drawing.Size(65, 21);
            this.cboColFab.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(158, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Quantidade";
            // 
            // cboColQtd
            // 
            this.cboColQtd.FormattingEnabled = true;
            this.cboColQtd.Location = new System.Drawing.Point(226, 15);
            this.cboColQtd.Name = "cboColQtd";
            this.cboColQtd.Size = new System.Drawing.Size(65, 21);
            this.cboColQtd.TabIndex = 22;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Enabled = false;
            this.btnFinalizar.Location = new System.Drawing.Point(645, 13);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(75, 23);
            this.btnFinalizar.TabIndex = 23;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            this.btnFinalizar.Click += new System.EventHandler(this.btnFinalizar_Click);
            // 
            // btnExportar
            // 
            this.btnExportar.Location = new System.Drawing.Point(726, 13);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(75, 23);
            this.btnExportar.TabIndex = 14;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // grdArquivo
            // 
            this.grdArquivo.AllowUserToAddRows = false;
            this.grdArquivo.AllowUserToDeleteRows = false;
            this.grdArquivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdArquivo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkExcluir});
            this.grdArquivo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.grdArquivo.Location = new System.Drawing.Point(6, 42);
            this.grdArquivo.Name = "grdArquivo";
            this.grdArquivo.Size = new System.Drawing.Size(795, 345);
            this.grdArquivo.TabIndex = 4;
            this.grdArquivo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdArquivo_CellDoubleClick);
            // 
            // chkExcluir
            // 
            this.chkExcluir.HeaderText = "Excluir";
            this.chkExcluir.Name = "chkExcluir";
            this.chkExcluir.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.chkExcluir.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.chkExcluir.Width = 55;
            // 
            // btnBuscarCotacao
            // 
            this.btnBuscarCotacao.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarCotacao.Image")));
            this.btnBuscarCotacao.Location = new System.Drawing.Point(160, 121);
            this.btnBuscarCotacao.Name = "btnBuscarCotacao";
            this.btnBuscarCotacao.Size = new System.Drawing.Size(25, 23);
            this.btnBuscarCotacao.TabIndex = 21;
            this.btnBuscarCotacao.UseVisualStyleBackColor = true;
            this.btnBuscarCotacao.Click += new System.EventHandler(this.btnBuscarCotacao_Click);
            // 
            // txtCodCotacao
            // 
            this.txtCodCotacao.Location = new System.Drawing.Point(76, 121);
            this.txtCodCotacao.MaxLength = 7;
            this.txtCodCotacao.Name = "txtCodCotacao";
            this.txtCodCotacao.Size = new System.Drawing.Size(78, 20);
            this.txtCodCotacao.TabIndex = 20;
            this.txtCodCotacao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCotacao_KeyPress);
            this.txtCodCotacao.Leave += new System.EventHandler(this.txtCodCotacao_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Cód Cotação";
            // 
            // grpCad
            // 
            this.grpCad.Controls.Add(this.pnlBusca);
            this.grpCad.Controls.Add(this.txtMargem);
            this.grpCad.Controls.Add(this.label3);
            this.grpCad.Controls.Add(this.txtDesc);
            this.grpCad.Controls.Add(this.label12);
            this.grpCad.Controls.Add(this.label11);
            this.grpCad.Controls.Add(this.btnBuscarCotacao);
            this.grpCad.Controls.Add(this.label10);
            this.grpCad.Controls.Add(this.cboTabela);
            this.grpCad.Controls.Add(this.txtCodCotacao);
            this.grpCad.Controls.Add(this.label9);
            this.grpCad.Controls.Add(this.label5);
            this.grpCad.Controls.Add(this.cboTipo);
            this.grpCad.Controls.Add(this.btnBuscarVend);
            this.grpCad.Controls.Add(this.txtCodVend);
            this.grpCad.Controls.Add(this.txtVend);
            this.grpCad.Controls.Add(this.label8);
            this.grpCad.Controls.Add(this.btnBuscarPlano);
            this.grpCad.Controls.Add(this.txtCodPlano);
            this.grpCad.Controls.Add(this.txtPlano);
            this.grpCad.Controls.Add(this.label7);
            this.grpCad.Controls.Add(this.label6);
            this.grpCad.Controls.Add(this.btnBuscarCliente);
            this.grpCad.Controls.Add(this.txtCaminhoArquivo);
            this.grpCad.Controls.Add(this.btnAbrir);
            this.grpCad.Controls.Add(this.txtCodCliente);
            this.grpCad.Controls.Add(this.cboDeposito);
            this.grpCad.Controls.Add(this.btnLimpar);
            this.grpCad.Controls.Add(this.btnSalvar);
            this.grpCad.Controls.Add(this.mskDtValidade);
            this.grpCad.Controls.Add(this.label4);
            this.grpCad.Controls.Add(this.txtCliente);
            this.grpCad.Controls.Add(this.label2);
            this.grpCad.Controls.Add(this.label1);
            this.grpCad.Location = new System.Drawing.Point(12, 12);
            this.grpCad.Name = "grpCad";
            this.grpCad.Size = new System.Drawing.Size(807, 150);
            this.grpCad.TabIndex = 5;
            this.grpCad.TabStop = false;
            this.grpCad.Text = "Cadastrar Cotação";
            // 
            // pnlBusca
            // 
            this.pnlBusca.Controls.Add(this.btnFechar);
            this.pnlBusca.Controls.Add(this.grdBusca);
            this.pnlBusca.Location = new System.Drawing.Point(426, 14);
            this.pnlBusca.Name = "pnlBusca";
            this.pnlBusca.Size = new System.Drawing.Size(376, 129);
            this.pnlBusca.TabIndex = 34;
            this.pnlBusca.Visible = false;
            // 
            // btnFechar
            // 
            this.btnFechar.Location = new System.Drawing.Point(299, 103);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(75, 23);
            this.btnFechar.TabIndex = 15;
            this.btnFechar.Text = "Fechar";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // grdBusca
            // 
            this.grdBusca.AllowUserToAddRows = false;
            this.grdBusca.AllowUserToDeleteRows = false;
            this.grdBusca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdBusca.Location = new System.Drawing.Point(3, 3);
            this.grdBusca.Name = "grdBusca";
            this.grdBusca.ReadOnly = true;
            this.grdBusca.Size = new System.Drawing.Size(370, 98);
            this.grdBusca.TabIndex = 5;
            this.grdBusca.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdBusca_CellDoubleClick);
            // 
            // txtMargem
            // 
            this.txtMargem.Location = new System.Drawing.Point(517, 69);
            this.txtMargem.MaxLength = 7;
            this.txtMargem.Name = "txtMargem";
            this.txtMargem.Size = new System.Drawing.Size(57, 20);
            this.txtMargem.TabIndex = 14;
            this.txtMargem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMargem_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Arquivo";
            // 
            // txtDesc
            // 
            this.txtDesc.Location = new System.Drawing.Point(737, 71);
            this.txtDesc.MaxLength = 6;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Size = new System.Drawing.Size(65, 20);
            this.txtDesc.TabIndex = 15;
            this.txtDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesc_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(580, 77);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(157, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "Desconto ou Acréscimo Padrão";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(427, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Margem de Lucro";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(644, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(89, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Tabela de Venda";
            // 
            // cboTabela
            // 
            this.cboTabela.FormattingEnabled = true;
            this.cboTabela.Location = new System.Drawing.Point(737, 17);
            this.cboTabela.Name = "cboTabela";
            this.cboTabela.Size = new System.Drawing.Size(65, 21);
            this.cboTabela.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(427, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Tipo de Venda";
            // 
            // cboTipo
            // 
            this.cboTipo.FormattingEnabled = true;
            this.cboTipo.Location = new System.Drawing.Point(517, 42);
            this.cboTipo.Name = "cboTipo";
            this.cboTipo.Size = new System.Drawing.Size(121, 21);
            this.cboTipo.TabIndex = 9;
            // 
            // btnBuscarVend
            // 
            this.btnBuscarVend.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarVend.Image")));
            this.btnBuscarVend.Location = new System.Drawing.Point(396, 68);
            this.btnBuscarVend.Name = "btnBuscarVend";
            this.btnBuscarVend.Size = new System.Drawing.Size(25, 22);
            this.btnBuscarVend.TabIndex = 13;
            this.btnBuscarVend.UseVisualStyleBackColor = true;
            this.btnBuscarVend.Click += new System.EventHandler(this.btnBuscarVend_Click);
            // 
            // txtCodVend
            // 
            this.txtCodVend.Location = new System.Drawing.Point(76, 68);
            this.txtCodVend.MaxLength = 4;
            this.txtCodVend.Name = "txtCodVend";
            this.txtCodVend.Size = new System.Drawing.Size(48, 20);
            this.txtCodVend.TabIndex = 11;
            this.txtCodVend.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodVend_KeyPress);
            this.txtCodVend.Leave += new System.EventHandler(this.txtCodVend_Leave);
            // 
            // txtVend
            // 
            this.txtVend.Location = new System.Drawing.Point(130, 68);
            this.txtVend.MaxLength = 40;
            this.txtVend.Name = "txtVend";
            this.txtVend.Size = new System.Drawing.Size(261, 20);
            this.txtVend.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Vendedor";
            // 
            // btnBuscarPlano
            // 
            this.btnBuscarPlano.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarPlano.Image")));
            this.btnBuscarPlano.Location = new System.Drawing.Point(396, 41);
            this.btnBuscarPlano.Name = "btnBuscarPlano";
            this.btnBuscarPlano.Size = new System.Drawing.Size(25, 22);
            this.btnBuscarPlano.TabIndex = 8;
            this.btnBuscarPlano.UseVisualStyleBackColor = true;
            this.btnBuscarPlano.Click += new System.EventHandler(this.btnBuscarPlano_Click);
            // 
            // txtCodPlano
            // 
            this.txtCodPlano.Location = new System.Drawing.Point(76, 42);
            this.txtCodPlano.MaxLength = 3;
            this.txtCodPlano.Name = "txtCodPlano";
            this.txtCodPlano.Size = new System.Drawing.Size(48, 20);
            this.txtCodPlano.TabIndex = 6;
            this.txtCodPlano.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodPlano_KeyPress);
            this.txtCodPlano.Leave += new System.EventHandler(this.txtCodPlano_Leave);
            // 
            // txtPlano
            // 
            this.txtPlano.Location = new System.Drawing.Point(130, 42);
            this.txtPlano.MaxLength = 20;
            this.txtPlano.Name = "txtPlano";
            this.txtPlano.ReadOnly = true;
            this.txtPlano.Size = new System.Drawing.Size(261, 20);
            this.txtPlano.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Plano Pgto.";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(427, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Depósito";
            // 
            // btnBuscarCliente
            // 
            this.btnBuscarCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscarCliente.Image")));
            this.btnBuscarCliente.Location = new System.Drawing.Point(396, 15);
            this.btnBuscarCliente.Name = "btnBuscarCliente";
            this.btnBuscarCliente.Size = new System.Drawing.Size(25, 22);
            this.btnBuscarCliente.TabIndex = 3;
            this.btnBuscarCliente.UseVisualStyleBackColor = true;
            this.btnBuscarCliente.Click += new System.EventHandler(this.btnBuscarCliente_Click);
            // 
            // txtCaminhoArquivo
            // 
            this.txtCaminhoArquivo.Location = new System.Drawing.Point(76, 95);
            this.txtCaminhoArquivo.Name = "txtCaminhoArquivo";
            this.txtCaminhoArquivo.Size = new System.Drawing.Size(498, 20);
            this.txtCaminhoArquivo.TabIndex = 16;
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(587, 95);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(62, 22);
            this.btnAbrir.TabIndex = 17;
            this.btnAbrir.Text = "Abrir...";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // txtCodCliente
            // 
            this.txtCodCliente.Location = new System.Drawing.Point(76, 16);
            this.txtCodCliente.MaxLength = 6;
            this.txtCodCliente.Name = "txtCodCliente";
            this.txtCodCliente.Size = new System.Drawing.Size(48, 20);
            this.txtCodCliente.TabIndex = 1;
            this.txtCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCliente_KeyPress);
            this.txtCodCliente.Leave += new System.EventHandler(this.txtCodCliente_Leave);
            // 
            // cboDeposito
            // 
            this.cboDeposito.FormattingEnabled = true;
            this.cboDeposito.Location = new System.Drawing.Point(517, 15);
            this.cboDeposito.Name = "cboDeposito";
            this.cboDeposito.Size = new System.Drawing.Size(121, 21);
            this.cboDeposito.TabIndex = 4;
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(655, 95);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 22);
            this.btnLimpar.TabIndex = 18;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(736, 95);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(67, 22);
            this.btnSalvar.TabIndex = 19;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // mskDtValidade
            // 
            this.mskDtValidade.Location = new System.Drawing.Point(737, 44);
            this.mskDtValidade.Mask = "00/00/0000";
            this.mskDtValidade.Name = "mskDtValidade";
            this.mskDtValidade.Size = new System.Drawing.Size(65, 20);
            this.mskDtValidade.TabIndex = 10;
            this.mskDtValidade.ValidatingType = typeof(System.DateTime);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(644, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Data de Validade";
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(130, 16);
            this.txtCliente.MaxLength = 30;
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(261, 20);
            this.txtCliente.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(278, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Descrição";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cod. Cliente";
            // 
            // txtValorTotal
            // 
            this.txtValorTotal.Location = new System.Drawing.Point(279, 396);
            this.txtValorTotal.MaxLength = 18;
            this.txtValorTotal.Name = "txtValorTotal";
            this.txtValorTotal.ReadOnly = true;
            this.txtValorTotal.Size = new System.Drawing.Size(114, 20);
            this.txtValorTotal.TabIndex = 47;
            // 
            // txtQtdeTotal
            // 
            this.txtQtdeTotal.Location = new System.Drawing.Point(144, 396);
            this.txtQtdeTotal.MaxLength = 18;
            this.txtQtdeTotal.Name = "txtQtdeTotal";
            this.txtQtdeTotal.ReadOnly = true;
            this.txtQtdeTotal.Size = new System.Drawing.Size(62, 20);
            this.txtQtdeTotal.TabIndex = 46;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(212, 403);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 13);
            this.label22.TabIndex = 45;
            this.label22.Text = "Valor Total:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 403);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 13);
            this.label21.TabIndex = 44;
            this.label21.Text = "Total de Itens da Cotação:";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 593);
            this.Controls.Add(this.grpCad);
            this.Controls.Add(this.grpArq);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DPKOTA - Sistema de Cotação";
            this.grpArq.ResumeLayout(false);
            this.grpArq.PerformLayout();
            this.pnlItem.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdArquivo)).EndInit();
            this.grpCad.ResumeLayout(false);
            this.grpCad.PerformLayout();
            this.pnlBusca.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdBusca)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox grpArq;
        private System.Windows.Forms.DataGridView grdArquivo;
        private System.Windows.Forms.GroupBox grpCad;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox mskDtValidade;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.TextBox txtCaminhoArquivo;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox txtCodCotacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.TextBox txtCodCliente;
        private System.Windows.Forms.ComboBox cboDeposito;
        private System.Windows.Forms.Button btnBuscarCliente;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnBuscarPlano;
        private System.Windows.Forms.TextBox txtCodPlano;
        private System.Windows.Forms.TextBox txtPlano;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnBuscarVend;
        private System.Windows.Forms.TextBox txtCodVend;
        private System.Windows.Forms.TextBox txtVend;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboTipo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboTabela;
        private System.Windows.Forms.TextBox txtDesc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBuscarCotacao;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboColQtd;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cboColFab;
        private System.Windows.Forms.TextBox txtMargem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel pnlBusca;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.DataGridView grdBusca;
        private System.Windows.Forms.Label lblMargem;
        private System.Windows.Forms.Panel pnlItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDescItem;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboTipoItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSalvarItem;
        private System.Windows.Forms.Button btnFecharItem;
        private System.Windows.Forms.TextBox txtNrItem;
        private System.Windows.Forms.TextBox txtCodFabrica;
        private System.Windows.Forms.TextBox txtCodDPK;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cboColForn;
        private System.Windows.Forms.Button btnExcluirTodos;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkExcluir;
        private System.Windows.Forms.TextBox txtValorTotal;
        private System.Windows.Forms.TextBox txtQtdeTotal;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
    }
}

