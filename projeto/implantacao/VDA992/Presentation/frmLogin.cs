﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace Presentation
{
    public partial class frmLogin : Form
    {
        private Usuario usuario = new Usuario();

        public frmLogin()
        {
            InitializeComponent();
            //txtUsuario.Text = "marici";
           // txtSenha.Text = "mike0410";
            this.CenterToScreen();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            ValidarUsuario();
        }

        private void ValidarUsuario()
        {
            Usuario userLogado = new Usuario();
            usuario.Login = txtUsuario.Text;
            usuario.Senha = txtSenha.Text;

            if (String.IsNullOrEmpty(txtUsuario.Text) || String.IsNullOrEmpty(txtSenha.Text))
            {
                informarMensagem("Usuário e/ou Senha vazios!", true);
            }
            else
            {
                controleAtivo(false);

                informarMensagem("Realizando autenticação, aguarde...", false);

                userLogado = UsuarioBLL.Instance().ValidarUsuario(usuario);

                efetuarLogin(userLogado);

                controleAtivo(true);
            }
        }

        private void efetuarLogin(Usuario userLogado)
        {
            try
            {
                if (userLogado != null)
                {
                    if (userLogado.Acesso)
                    {
                        Session.Cod_Usuario = userLogado.CodUsuario;
                        this.Hide();
                        frmPrincipal frm = new frmPrincipal();
                        frm.ShowDialog();
                    }
                    else
                    {
                        informarMensagem("Login não autorizado! Tente novamente.", true);
                    }
                }
                else
                {
                    informarMensagem("Usuário inválido! Tente novamente.", true);
                }
            }
            catch
            {
                informarMensagem("Ocorreu um erro, contate o suporte", true);
                this.Show();
            }

        }


        private void informarMensagem(string mensagem, bool erro)
        {
            tslMensagem.Text = mensagem;
            if (erro)
            {
                tslMensagem.ForeColor = Color.Red;
            }
            else
            {
                tslMensagem.ForeColor = Color.Black;
            }
        }

        private void controleAtivo(bool ativacao)
        {
            txtUsuario.Enabled = ativacao;
            txtSenha.Enabled = ativacao;
            btnEntrar.Enabled = ativacao;
            btnSair.Enabled = ativacao;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtSenha.Select();
            }
        }

        private void frmLogin_Move(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void txtSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ValidarUsuario();
            }
        }

    }
}
