﻿using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Business;
using System.Threading;
using System.Diagnostics;
using System;
using System.Drawing;
using Entities;
using System.Data;
using System.Configuration; //TI-5051

namespace Presentation
{
    public partial class frmPrincipal : Form
    {
        byte tpBusca = 0;
        int codLinhaGrid = 0; //TI-5015
        public frmPrincipal()
        {
            InitializeComponent();

            Session.Deposito_Visao = ConfigurationManager.AppSettings["DEPOSITO_VISAO"].ToString(); //TI-5015

            CarregarCombos();

            this.FormClosing += frmPrincipal_FormClosing;
            mskDtValidade.ValidatingType = typeof(System.DateTime);
            mskDtValidade.TypeValidationCompleted += new TypeValidationEventHandler(mskDtValidade_TypeValidationCompleted);

        }

        public void mskDtValidade_TypeValidationCompleted(object sender, TypeValidationEventArgs e)
        {

            if (!e.IsValidInput && this.mskDtValidade.MaskedTextProvider.AssignedEditPositionCount > 0)
            {
                toolTip1.ToolTipTitle = "Data Inválida";
                toolTip1.Show("Por favor, preencher a data no formata DD/MM/YYYY", mskDtValidade, 5000);
                e.Cancel = true;
            }
        }

        private void txtMargem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',')
                e.KeyChar = '.';

            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back) && !(e.KeyChar == Char.Parse(".")) && !(e.KeyChar == Char.Parse("-")))
                e.Handled = true;
        }

        private void txtDesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',')
                e.KeyChar = '.';

            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back) && !(e.KeyChar == Char.Parse(".")) && !(e.KeyChar == Char.Parse("-")))
                e.Handled = true;
        }

        private void frmPrincipal_FormClosing(Object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            var item = new ItemCotacao();
            var cotacao = new Cotacao();
            int colFab = cboColFab.SelectedIndex;
            int colQtd = cboColQtd.SelectedIndex;
            int colForn = cboColForn.SelectedIndex;
            int codForn = 0;
            int qtde = 0;
            int codigo = int.TryParse(this.txtCodCotacao.Text.Trim(), out codigo) ? codigo : 0;

            try
            {
                if (ValidarCadastro())
                {
                    cotacao.Cod_Loja = (int)cboDeposito.SelectedValue;
                    cotacao.Dt_Validade = mskDtValidade.Text;
                    cotacao.Cod_Cliente = int.Parse(txtCodCliente.Text);
                    cotacao.Cod_Vend = int.Parse(txtCodVend.Text);
                    cotacao.Cod_Plano = int.Parse(txtCodPlano.Text);
                    cotacao.Pc_Desconto = txtDesc.Text.Trim().Length > 0 ? decimal.Parse(txtDesc.Text) : 0;
                    cotacao.Cod_Vdr = cboTabela.SelectedIndex;                    
                    cotacao.Pc_Param_Mg = txtMargem.Text.Trim().Length > 0 ? decimal.Parse(txtMargem.Text) : 0;
                    cotacao.Tp_tabela = cboTipo.SelectedItem.ToString();
                    cotacao.Cod_Usuario = Session.Cod_Usuario;

                    if (codigo > 0)
                    {
                        cotacao.Id_Cotacao = codigo;
                        CotacaoBLL.Alterar(cotacao);
                    }
                    else if ( Session.Id_Cotacao > 0)
                    {
                        cotacao.Id_Cotacao = Session.Id_Cotacao;
                        CotacaoBLL.Alterar(cotacao);
                    }
                    else
                    {
                        Session.Id_Cotacao = CotacaoBLL.Inserir(cotacao);
                    }

                    if (File.Exists(this.txtCaminhoArquivo.Text))
                    {
                        item.Id_Cotacao = Session.Id_Cotacao;
                        item.Cod_Loja = cotacao.Cod_Loja;
                        item.Id_Item_Cotacao = 0;
                        item.Pc_Desc3 = cotacao.Pc_Desconto;
                        item.Tp_tabela = cotacao.Tp_tabela;
                        string msgItem = ItemCotacaoBLL.Apagar(item);

                        //Adding the Rows
                        foreach (DataGridViewRow dr in grdArquivo.Rows)
                        {
                            item.Fabrica_Arquivo = grdArquivo.Rows[dr.Index].Cells[colFab].Value.ToString().Trim();
                            item.Cod_Fabrica = item.Fabrica_Arquivo.Replace(" ", "").Replace(".", "").Replace("-", "").Replace("%","").ToUpper();
                            if (item.Cod_Fabrica.IndexOf("/") != -1)
                            {
                                item.Cod_Fabrica = item.Cod_Fabrica.Substring(0, item.Cod_Fabrica.IndexOf("/"));
                            }

                            qtde = int.TryParse(grdArquivo.Rows[dr.Index].Cells[colQtd].Value.ToString(), out qtde) ? qtde : 0;
                            item.Qtd_Solicitada = qtde;

                            if (this.cboColForn.SelectedIndex > 0)
                            {
                                codForn = int.TryParse(grdArquivo.Rows[dr.Index].Cells[colForn].Value.ToString(), out codForn) ? codForn : 0;
                            }                            
                            item.Cod_Forn = codForn; 
                            if (codForn > 0)
                            {
                                item.Cod_Fabrica = "%" + item.Cod_Fabrica + "%";
                            }
                            else
                            {
                                item.Cod_Fabrica = item.Cod_Fabrica + "%";
                            }

                            if (!string.IsNullOrEmpty(item.Fabrica_Arquivo) && item.Qtd_Solicitada > 0)
                            {
                                ItemCotacaoBLL.Inserir(item);
                            }
                        }
                        this.txtCaminhoArquivo.Text = string.Empty;
                        this.cboColFab.SelectedIndex = -1;
                        this.cboColQtd.SelectedIndex = -1;
                        this.cboColForn.SelectedIndex = -1;
                    }

                    CalcularPreco();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao gravar dados! - " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            LimparCadastro();
            controleAtivo(true);
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            controleAtivo(false);
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();
                dt.TableName = "Plan1";

                //Adding the Columns
                foreach (DataGridViewColumn column in grdArquivo.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in grdArquivo.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value == null ? "" : cell.Value.ToString();
                    }
                }

                if (grdArquivo.Rows.Count < 100)
                {
                    for (int j = grdArquivo.Rows.Count; j <= (grdArquivo.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= grdArquivo.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Falha ao exportar excel. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            try
            {
                grdArquivo.DataSource = null;
                this.txtCaminhoArquivo.Text = string.Empty;
                this.cboColFab.SelectedIndex = -1;
                this.cboColQtd.SelectedIndex = -1;
                this.cboColForn.SelectedIndex = -1;
                cboColFab.Items.Clear();
                cboColQtd.Items.Clear();
                cboColForn.Items.Clear();
                this.btnFinalizar.Enabled = false;
                controleAtivo(false);
                this.openFileDialog1.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";

                 if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.txtCaminhoArquivo.Text = this.openFileDialog1.FileName;
                    grdArquivo.DataSource = ArquivoBLL.CarregarExcel(this.txtCaminhoArquivo.Text);

                    int qtdCols = 0;
                    foreach (DataGridViewColumn item in grdArquivo.Columns)
                    {
                        qtdCols++;
                        item.SortMode = DataGridViewColumnSortMode.NotSortable;
                        item.HeaderText = "Coluna " + qtdCols;
                        cboColFab.Items.Add("Coluna " + qtdCols);
                        cboColQtd.Items.Add("Coluna " + qtdCols);
                        cboColForn.Items.Add("Coluna " + qtdCols);
                        
                        item.ReadOnly = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Falha ao carregar o arquivo excel. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            string msgCotacao = string .Empty ;
            string msgFinalizar = "Ao finalizar a cotação, a mesma não poderá mais ser alterada. Se for uma cotação do tipo VDR, não será criada no sistema CRM. confirma finalização?";
            controleAtivo(false);
            try
            {
                if (ValidarCotacao())
                {
                    if (MessageBox.Show(msgFinalizar, "VDA992", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        if(grdArquivo.RowCount > 999)
                            MessageBox.Show("A cotação no CRM aceita apenas 999 itens, os demais itens não serão gravados!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        int Num_Cotacao = CotacaoBLL.Finalizar(Session.Id_Cotacao);

                        if (Num_Cotacao > 0)
                        {
                            msgCotacao = string.Format("Cotação gerada com sucesso! número: {0}", Num_Cotacao);
                        }
                        else
                        {
                            msgCotacao = "Cotação gerada com sucesso!";
                        }
                        MessageBox.Show(msgCotacao, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LimparCadastro();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao finalizar Cotação! - " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            controleAtivo(true);
        }

        private void CalcularPreco()
        {
            AtualizarDescontoAdicional(Session.Id_Cotacao);//TI-6424
            decimal margem =ItemCotacaoBLL.CalcularPreco(Session.Id_Cotacao);
            decimal paramMg = decimal.TryParse(this.txtMargem.Text.Trim(), out paramMg) ? paramMg : 0;
            if (margem < paramMg || margem == 0)
            {
                lblMargem.ForeColor = Color.Red;
                btnFinalizar.Enabled = false; 
            }
            else
            {
                lblMargem.ForeColor = Color.Green;
                if (grdArquivo.RowCount > 0 && !File.Exists(this.txtCaminhoArquivo.Text))
                {
                    btnFinalizar.Enabled = true;
                }
            }
            lblMargem.Text = string.Format("Margem Total da Cotação: {0}", margem.ToString());
            CarregarItemCotacao(Session.Id_Cotacao);

        }

        private void CarregarCombos()
        {
            var loja = LojaBLL.Instance().ListarTodas();
            cboDeposito.DataSource = loja;
            cboDeposito.DisplayMember = "NomeLoja";
            cboDeposito.ValueMember = "CodLoja";
            //TI-5015
            if (Session.Deposito_Visao.ToUpper() == "PRODUCAO.")
            {
                cboDeposito.SelectedValue = 1;
            }
            else
            {
                cboDeposito.SelectedValue = Convert.ToInt32(Session.Deposito_Visao.Substring(3,2));
            }
            //FIM TI-5015

            cboTabela.Items.Add("Normal");
            cboTabela.Items.Add("VDR");
            cboTabela.SelectedIndex = 0;

            cboTipo.Items.Add("A");
            cboTipo.Items.Add("B");
            cboTipo.Items.Add("C");
            cboTipo.SelectedIndex = 0;

            cboTipoItem.Items.Add("A");
            cboTipoItem.Items.Add("B");
            cboTipoItem.Items.Add("C");
            cboTipoItem.SelectedIndex = 0;

        }

        private void CarregarItemCotacao(int Id_Cotacao)
        {
            int coluna = 0;
            decimal valorTotal = 0;//TI-5015
            string Cod_fabrica = string.Empty;

            var arquivo = ArquivoBLL.Instance().Selecionar(Session.Id_Cotacao);
            grdArquivo.DataSource = arquivo;

            //TI-5015
            txtQtdeTotal.Text = arquivo.FindAll(o => o.Disponivel.Equals("SIM")).Count.ToString();
            arquivo.FindAll(o => o.Disponivel.Equals("SIM")).ForEach(a => valorTotal += a.Preco_Liquido * a.Qtd_Solicitada);
            txtValorTotal.Text = valorTotal.ToString();
            //FIM TI-5015

            foreach (DataGridViewRow item in grdArquivo.Rows)
            {
                Cod_fabrica = item.Cells[1].Value.ToString().Trim();

                if (arquivo.FindAll(o => o.Fabrica_Arquivo == Cod_fabrica).Count > 1)
                {
                    item.DefaultCellStyle.BackColor = Color.Yellow;
                }

            }

            foreach (DataGridViewColumn col in grdArquivo.Columns)
            {
                coluna++;

                if (coluna == 1)
                {
                    col.ReadOnly = false;
                    col.HeaderText = "Excluir";
                }
                else
                {
                    col.ReadOnly = true;
                }
            }
        }

        private bool ValidarCadastro()
        {
            decimal valor = 0;
            if (string.IsNullOrEmpty(txtCliente.Text.Trim()))
            {
                MessageBox.Show("Preencha o campo Cliente!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodCliente.Focus();
                return false;
            }
            if (cboDeposito.SelectedIndex == -1)
            {
                MessageBox.Show("Preencha o campo Depósito!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboDeposito.Focus();
                return false;
            }
            if (cboTabela.SelectedIndex == -1)
            {
                MessageBox.Show("Preencha o campo Tabela de Venda!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTabela.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.txtPlano.Text.Trim()))
            {
                MessageBox.Show("Preencha o campo Plano de Pgto.!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtCodPlano.Focus();
                return false;
            }
            if (cboTipo.SelectedIndex == -1)
            {
                MessageBox.Show("Preencha o campo Tipo de Venda!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTipo.Focus();
                return false;
            }
            if (mskDtValidade.Text == "  /  /")
            {
                MessageBox.Show("Preencha o campo Data de Validade!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskDtValidade.Focus();
                return false;
            }
            if (Convert.ToDateTime(mskDtValidade.Text) < DateTime.Today || Convert.ToDateTime(mskDtValidade.Text) > DateTime.Today.AddDays(6 - (int)DateTime.Today.DayOfWeek))
            {
                MessageBox.Show("Data de Validade deve ser entre " + DateTime.Today.ToShortDateString()
                    + " e " + DateTime.Today.AddDays(6 - (int)DateTime.Today.DayOfWeek).ToShortDateString() + "!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mskDtValidade.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.txtVend.Text.Trim()))
            {
                MessageBox.Show("Preencha o campo Vendedor!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtCodVend.Focus();
                return false;
            }
            if (txtMargem.Text.Trim().Length > 0 && !decimal.TryParse(txtMargem.Text.Trim(), out valor))
            {
                MessageBox.Show("Valor da Margem de Lucro inválido! Formato correto: 999.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMargem.Focus();
                return false;
            }
            if (!(valor >= -99.99m && valor <= 999.99m))
            {
                MessageBox.Show("Valor da Margem de Lucro muito grande! Formato correto: 999.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMargem.Focus();
                return false;
            }
            if (txtDesc.Text.Trim().Length > 0 && !decimal.TryParse(txtDesc.Text.Trim(), out valor))
            {
                MessageBox.Show("Valor do Desconto ou Acréscimo Padrão inválido! Formato correto: 99.99 ou -99.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDesc.Focus();
                return false;
            }
            if (!(valor >= -99.99m && valor <= 99.99m))
            {
                MessageBox.Show("Valor do Desconto ou Acréscimo Padrão muito grande! Formato correto: 99.99 ou -99.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDesc.Focus();
                return false;
            }                        
            if (!File.Exists(this.txtCaminhoArquivo.Text) && txtCodCotacao.Text.Trim().Length == 0 && Session.Id_Cotacao == 0 )
            {
                MessageBox.Show("O arquivo inválido ou não informado!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCaminhoArquivo.Focus();
                return false;
            }

            if (cboColFab.SelectedIndex < 1 && File.Exists(this.txtCaminhoArquivo.Text))
            {
                MessageBox.Show("Preencha o campo Código Fábrica corretamente!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboColFab.Focus();
                return false;
            }
            if (cboColQtd.SelectedIndex < 1 && File.Exists(this.txtCaminhoArquivo.Text))
            {
                MessageBox.Show("Preencha o campo Quantidade corretamente!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboColQtd.Focus();
                return false;
            }
            return true;
        }
        
        private bool ValidarCotacao()
        {
            if (grdArquivo.RowCount == 0)
            {
                MessageBox.Show("Não existem itens na cotação!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            foreach (DataGridViewRow item in grdArquivo.Rows)
            {
                if (item.DefaultCellStyle.BackColor.Name == Color.Yellow.Name)
                {
                    MessageBox.Show("Existem itens duplicados na cotação, exclua esses itens para finalizar a cotação!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
            }
            return true;
        }

        private void LimparCadastro()
        {
            this.txtCodCliente.Text = string.Empty;
            this.txtCliente.Text = string.Empty;
            this.txtCodPlano.Text = string.Empty;
            this.txtPlano.Text = string.Empty;
            this.mskDtValidade.Text = string.Empty;
            this.txtCodVend.Text = string.Empty;
            this.txtVend.Text = string.Empty;
            this.txtMargem.Text = string.Empty;
            this.txtDesc.Text = string.Empty;
            this.txtCaminhoArquivo.Text = string.Empty;
            this.cboColFab.SelectedIndex = -1;
            this.cboColQtd.SelectedIndex = -1;
            this.cboColForn.SelectedIndex = -1;
            cboDeposito.SelectedIndex = 0;
            cboTipo.SelectedIndex = 0;
            cboTabela.SelectedIndex = 0;
            this.txtCodCotacao.Text = string.Empty;
            this.txtCodCotacao.ReadOnly = false;
            Session.Id_Cotacao = 0;
            grdArquivo.DataSource = null;
            lblMargem.Text = string.Empty;
            txtQtdeTotal.Text = string.Empty;//TI-5015
            txtValorTotal.Text = string.Empty;//TI-5015
        }

        private void controleAtivo(bool ativacao)
        {
            if (!ativacao)
                this.Cursor = Cursors.WaitCursor;
            else
                this.Cursor = Cursors.Default;
            grpCad.Enabled = ativacao;
            grpArq.Enabled = ativacao;
        }
        //TI-6424
        private bool AtualizarDescontoAdicional(int IdCotacao)
        {
            decimal desconto = txtDesc.Text.Trim().Length > 0 ? decimal.Parse(txtDesc.Text) : 0;
            var lst = DescontoBLL.Instance().Selecionar(IdCotacao, 0);
            if (lst == null) return false;
            var api = new ApiProduto();
            api.CodigoFilial = lst[0].CodigoFilial;
            api.Canal = lst[0].Canal;
            api.Caracteristica = lst[0].Caracteristica;
            api.CodigoCliente = lst[0].CodigoCliente;
            api.Produto = new List<ApiItem>();
            lst.ForEach(o =>
            {
                api.Produto.Add(new ApiItem
                {
                    CodigoFornecedor = o.CodigoFornecedor,
                    CodigoDPK = o.CodigoDPK,
                    CodigoGrupo = o.CodigoGrupo,
                    CodigoSubGrupo = o.CodigoSubGrupo,
                    DescontoAcrescimo = o.DescontoAcrescimo,
                    Tabela = o.Tabela
                });
            });

            var result = DescontoBLL.DescontoAdicional(api);

            result.Produto.ForEach(o =>
            {
                var item = new ItemCotacao();
                item.Id_Cotacao = IdCotacao;
                item.Cod_Dpk = o.CodigoDPK;
                if (o.DescontoAcrescimo == -100)
                {
                    item.Pc_Desc3 = 0;
                }
                else if (o.DescontoAcrescimo <= desconto && o.DescontoAcrescimo < 0)
                {
                    item.Pc_Desc3 = o.DescontoAcrescimo;
                }
                else if (o.DescontoAcrescimo <= desconto && o.DescontoAcrescimo >= 0)
                {
                    item.Pc_Desc3 = o.DescontoAcrescimo;
                }
                else
                {
                    item.Pc_Desc3 = desconto;
                }
                DescontoBLL.Alterar(item);
            });
            return true;
        }
        //FIM TI-6424
        #region busca genérica

        #region busca de cliente

        private void txtCodCliente_Leave(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                var result = new Cliente();
                int codigo = int.TryParse(this.txtCodCliente.Text.Trim(), out codigo) ? codigo : 0;
                try
                {
                    if (codigo > 0)
                    {
                        var cliente = new Cliente();
                        cliente.Cod_Cliente = codigo;
                        cliente.Nome_Cliente = string.Empty;
                        result = ClienteBLL.Instance().Selecionar(cliente).Find(o => o.Cod_Cliente == cliente.Cod_Cliente);

                        if (result == null)
                        {
                            MessageBox.Show("Cliente não encontrado!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (result.Ativo == "NÃO")
                        {
                            MessageBox.Show("Cliente desativado, informe um cliente ativo!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            result = null;
                        }


                    }
                    else
                    {
                        result = null;
                    }
                    this.txtCliente.Text = result == null ? string.Empty : result.Nome_Cliente;
                    btnBuscarCliente.Enabled = result == null ? true : false;
                    txtCliente.ReadOnly = result == null ? false : true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Cliente. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }

            private void txtCodCliente_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
                    e.Handled = true;
            }

            private void btnBuscarCliente_Click(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                this.txtCodCliente.Text = string.Empty;
                try
                {
                    if (this.txtCliente.Text.Trim().Length < 3 || string.IsNullOrEmpty(this.txtCliente.Text.Trim()))
                    {
                        MessageBox.Show("Preencha o nome do cliente com no mínimo 3 caracteres para realizar a busca!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCliente.Focus();
                    }
                    else
                    {
                        var cliente = new Cliente();
                        cliente.Cod_Cliente = 0;
                        cliente.Nome_Cliente = this.txtCliente.Text.ToUpper().Trim();
                        grdBusca.DataSource = ClienteBLL.Instance().Selecionar(cliente);
                        pnlBusca.Visible = true;
                        tpBusca = 1;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Cliente. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { this.Cursor = Cursors.Default; }
            }

            #endregion 

            #region busca plano de pgto

            private void txtCodPlano_Leave(object sender, EventArgs e)
            {

                this.Cursor = Cursors.WaitCursor;
                var result = new Plano();
                int codigo = int.TryParse(this.txtCodPlano.Text.Trim(), out codigo) ? codigo : 0;
                try
                {
                    if (codigo > 0)
                    {
                        var plano = new Plano();
                        plano.Cod_Plano = codigo;
                        result = PlanoBLL.Instance().Selecionar(plano).Find(o => o.Cod_Plano == plano.Cod_Plano);

                        if (result == null)
                        {
                            MessageBox.Show("Plano de Pgto. não encontrado!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        result = null;
                    }
                    this.txtPlano.Text = result == null ? string.Empty : result.Desc_Plano;
                    btnBuscarPlano.Enabled = result == null ? true : false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Plano de Pgto! " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }

            private void txtCodPlano_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
                    e.Handled = true;
            }

            private void btnBuscarPlano_Click(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                this.txtPlano.Text = string.Empty;
                try
                {
                    var plano = new Plano();
                    plano.Cod_Plano = 0;
                    grdBusca.DataSource = PlanoBLL.Instance().Selecionar(plano);
                    pnlBusca.Visible = true;
                    tpBusca = 2;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Plano. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { this.Cursor = Cursors.Default; }
            }

            #endregion

            #region busca vendedor
            private void txtCodVend_Leave(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                var result = new Vendedor();
                int codigo = int.TryParse(this.txtCodVend.Text.Trim(), out codigo) ? codigo : 0;
                try
                {
                    if (codigo > 0)
                    {
                        var vend = new Vendedor();
                        vend.Cod_Repres = codigo;
                        vend.Nome_Repres = string.Empty;
                        result = VendedorBLL.Instance().Selecionar(vend).Find(o => o.Cod_Repres == vend.Cod_Repres);

                        if (result == null)
                        {
                            MessageBox.Show("Vendedor não encontrado!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (result.Ativo == "NÃO")
                        {
                            MessageBox.Show("Vendedor desativado, informe um vendedor ativo!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            result = null;
                        }
                    }
                    else
                    {
                        result = null;
                    }
                    this.txtVend.Text = result == null ? string.Empty : result.Nome_Repres;
                    btnBuscarVend.Enabled = result == null ? true : false;
                    txtVend.ReadOnly = result == null ? false : true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Vendedor! " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }

            private void txtCodVend_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
                    e.Handled = true;
            }

            private void btnBuscarVend_Click(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                this.txtCodVend.Text = string.Empty;
                try
                {
                    if (this.txtVend.Text.Trim().Length < 3 || string.IsNullOrEmpty(this.txtVend.Text.Trim()))
                    {
                        MessageBox.Show("Preencha o nome do vendedor com no mínimo 3 caracteres para realizar a busca!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtVend.Focus();
                    }
                    else
                    {
                        var vend = new Vendedor();
                        vend.Cod_Repres = 0;
                        vend.Nome_Repres = this.txtVend.Text.ToUpper().Trim();
                        grdBusca.DataSource = VendedorBLL.Instance().Selecionar(vend);
                        pnlBusca.Visible = true;
                        tpBusca = 3;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Vendedor. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { this.Cursor = Cursors.Default; }
            }

            #endregion

            #region busca cotação

            private void txtCodCotacao_Leave(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                var result = new Cotacao();
                int codigo = int.TryParse(this.txtCodCotacao.Text.Trim(), out codigo) ? codigo : 0;
                try
                {
                    if (codigo > 0)
                    {
                        var cotacao = new Cotacao();
                        cotacao.Id_Cotacao = codigo;
                        cotacao.Cod_Cliente = 0;
                        this.txtCaminhoArquivo.Text = string.Empty;
                        result = CotacaoBLL.Instance().Selecionar(cotacao).Find(o => o.Id_Cotacao == cotacao.Id_Cotacao);

                        if (result == null)
                        {
                            this.txtCodCotacao.Text = string.Empty;
                            MessageBox.Show("Cotação não encontrado!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            this.txtCodCliente.Text = result.Cod_Cliente.ToString();
                            txtCodCliente.Focus();
                            SendKeys.Send("{TAB}");
                            this.cboDeposito.SelectedValue = result.Cod_Loja;
                            this.cboTabela.SelectedIndex = result.Cod_Vdr;
                            this.cboTipo.SelectedItem = result.Tp_tabela;
                            this.txtCodPlano.Text = result.Cod_Plano.ToString();
                            SendKeys.Send("{TAB}");
                            this.mskDtValidade.Text = result.Dt_Validade;
                            this.txtCodVend.Text = result.Cod_Vend.ToString();
                            SendKeys.Send("{TAB}");
                            SendKeys.Send("{TAB}");
                            SendKeys.Send("{TAB}");
                            this.txtDesc.Text = result.Pc_Desconto.ToString();
                            this.txtMargem.Text = result.Pc_Param_Mg.ToString();
                            if (result.Pc_Margem < result.Pc_Param_Mg || result.Pc_Margem ==0)
                            {
                                lblMargem.ForeColor = Color.Red;
                                btnFinalizar.Enabled = false;
                            }
                            else
                            {
                                lblMargem.ForeColor = Color.Green;
                                if (grdArquivo.RowCount > 0 && !File.Exists(this.txtCaminhoArquivo.Text))
                                {
                                    btnFinalizar.Enabled = true;
                                }
                            }
                            lblMargem.Text = string.Format("Margem Total da Cotação: {0}", result.Pc_Margem.ToString());

                            Session.Id_Cotacao = result.Id_Cotacao;
                            CarregarItemCotacao(Session.Id_Cotacao);
                        }
                    }
                    else
                    {
                        result = null;
                    }
                    txtCodCotacao.ReadOnly = result == null ? false : true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Cotação! " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }

            private void txtCodCotacao_KeyPress(object sender, KeyPressEventArgs e)
            {
                if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back))
                    e.Handled = true;
            }

            private void btnBuscarCotacao_Click(object sender, EventArgs e)
            {
                this.Cursor = Cursors.WaitCursor;
                int codigo = int.TryParse(this.txtCodCliente.Text.Trim(), out codigo) ? codigo : 0;
                try
                {
                    if (codigo == 0)
                    {
                        MessageBox.Show("Preencha o campo Cliente para buscar Cotação!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        var cotacao = new Cotacao();
                        cotacao.Cod_Cliente = codigo;
                        cotacao.Id_Cotacao = 0;
                        grdBusca.DataSource = CotacaoBLL.Instance().Selecionar(cotacao);
                        pnlBusca.Visible = true;
                        tpBusca = 4;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Falha ao buscar Cotação. " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally { this.Cursor = Cursors.Default; }
            }

            #endregion

        private void btnFechar_Click(object sender, EventArgs e)
        {
            pnlBusca.Visible = false;
        }

        private void grdBusca_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                if (tpBusca == 1)
                {
                    this.txtCodCliente.Text = grdBusca.Rows[e.RowIndex].Cells[0].Value.ToString();
                    this.txtCodCliente.Focus();
                 }
                else if (tpBusca == 2)
                {
                    this.txtCodPlano.Text = grdBusca.Rows[e.RowIndex].Cells[0].Value.ToString();
                    this.txtCodPlano.Focus();
                }
                else if (tpBusca == 3)
                {
                    this.txtCodVend.Text = grdBusca.Rows[e.RowIndex].Cells[0].Value.ToString();
                    this.txtCodVend.Focus();
                }
                else if (tpBusca == 4)
                {
                    this.txtCodCotacao.Text = grdBusca.Rows[e.RowIndex].Cells[0].Value.ToString();
                    this.txtCodCotacao.Focus();
                }
            }
        }

        #endregion

        #region item

        private bool ValidarItem()
        {
            decimal valor = 0;
            if (cboTipoItem.SelectedIndex == -1)
            {
                MessageBox.Show("Preencha o campo Tipo de Venda!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cboTipo.Focus();
                return false;
            }
            if (txtDescItem.Text.Trim().Length > 0 && !decimal.TryParse(txtDescItem.Text.Trim(), out valor))
            {
                MessageBox.Show("Valor do Desconto ou Acréscimo Padrão inválido! Formato correto: 999.99 ou -99.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescItem.Focus();
                return false;
            }
            if (!(valor >= -99.99m && valor <= 99.99m))
            {
                MessageBox.Show("Valor do Desconto ou Acréscimo Padrão muito grande! Formato correto: 99.99 ou -99.99", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDescItem.Focus();
                return false;
            }
            if (!ValidarDescontoAdicional(valor)) return false;//TI-6424
            return true;
        }

        private void Fechar()
        {
            pnlItem.Visible = false;
            grpCad.Enabled = true;
            btnExportar.Enabled = true;
        }

        private void txtDescItem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ',')
                e.KeyChar = '.';

            if (!(char.IsNumber(e.KeyChar)) && !(e.KeyChar == (char)Keys.Back) && !(e.KeyChar == Char.Parse(".")) && !(e.KeyChar == Char.Parse("-")))
                e.Handled = true;
        }

        private void btnFecharItem_Click(object sender, EventArgs e)
        {
            Fechar();
        }

        private void btnSalvarItem_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.btnFinalizar.Enabled = false;
            btnSalvarItem.Enabled = false;
            try
            {
                if (ValidarItem())
                {
                    var item = new ItemCotacao();
                    item.Id_Cotacao = Session.Id_Cotacao;
                    item.Id_Item_Cotacao = txtNrItem.Text.Trim().Length > 0 ? int.Parse(txtNrItem.Text) : 0;
                    item.Cod_Dpk = txtCodDPK.Text.Trim().Length > 0 ? int.Parse(txtCodDPK.Text) : 0;
                    item.Pc_Desc3 = txtDescItem.Text.Trim().Length > 0 ? decimal.Parse(txtDescItem.Text) : 0;
                    item.Tp_tabela = cboTipoItem.SelectedItem.ToString();
                    ItemCotacaoBLL.Alterar(item);
                    CarregarItemCotacao(item.Id_Cotacao); //TI-5015
                    grdArquivo.CurrentCell = grdArquivo.Rows[codLinhaGrid].Cells[1];//TI-5015
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao gravar dados do item! - " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Cursor = Cursors.Default;
            btnSalvarItem.Enabled = true;
        }

        private void grdArquivo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
                {
                    grpCad.Enabled = false;
                    btnExportar.Enabled = false;
                    pnlItem.Visible = true;
                    codLinhaGrid = e.RowIndex;//TI-5015
                    txtNrItem.Text = grdArquivo.Rows[e.RowIndex].Cells[15].Value.ToString();//TI-6289
                    txtCodFabrica.Text = grdArquivo.Rows[e.RowIndex].Cells[2].Value.ToString();
                    txtCodDPK.Text = grdArquivo.Rows[e.RowIndex].Cells[10].Value.ToString();//TI-6289
                    cboTipoItem.SelectedItem = grdArquivo.Rows[e.RowIndex].Cells[13].Value.ToString();//TI-6289
                    txtDescItem.Text = grdArquivo.Rows[e.RowIndex].Cells[14].Value.ToString();//TI-6289
                }
            }
            catch (Exception)
            {
                grpCad.Enabled = true;
                pnlItem.Visible = false;
                btnExportar.Enabled = true;
            }
        }

        private void btnExcluirTodos_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;            
            try
            {
                if (MessageBox.Show("Confirma exclusão dos itens selecionados?", "VDA992", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    var item = new ItemCotacao();
                    item.Id_Cotacao = Session.Id_Cotacao;
                    string msgItem = string.Empty;
                    string msgItens = string.Empty;
                    this.btnFinalizar.Enabled = false;

                    foreach (DataGridViewRow dr in grdArquivo.Rows)
                    {
                        DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dr.Cells[0];
                        if (chk.Value != null)
                        {
                            item.Id_Item_Cotacao = Convert.ToInt32(grdArquivo.Rows[dr.Index].Cells[15].Value);//TI-6289
                            if ((bool)chk.Value == true)
                            {
                                msgItem = ItemCotacaoBLL.Apagar(item);

                                if (msgItem != "OK")
                                {
                                    msgItens += msgItem + "\n";
                                }
                            }
                        }
                    }
                    
                    if (msgItens.Length > 0)
                    {
                        MessageBox.Show(msgItens, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    CarregarItemCotacao(Session.Id_Cotacao);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao excluir itens! - " + ex.Message, "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Cursor = Cursors.Default;
        }
        //TI-6424
        private bool ValidarDescontoAdicional(decimal valor)
        {
            var cod_DPK = txtCodDPK.Text.Trim().Length > 0 ? int.Parse(txtCodDPK.Text) : 0;
            var lst = DescontoBLL.Instance().Selecionar(Session.Id_Cotacao, cod_DPK);
            if (lst == null) return false;
            var api = new Api();
            api.CodigoFilial = lst[0].CodigoFilial;
            api.Canal = lst[0].Canal;
            api.Caracteristica = lst[0].Caracteristica;
            api.CodigoCliente = lst[0].CodigoCliente;
            api.CodigoFornecedor = lst[0].CodigoFornecedor;
            api.CodigoGrupo = lst[0].CodigoGrupo;
            api.CodigoSubGrupo = lst[0].CodigoSubGrupo;
            api.CodigoDPK = lst[0].CodigoDPK;

            var ret = DescontoBLL.DescontoAdicional(api);
            if (ret.DescontoAcrescimo == -100)
            {
                MessageBox.Show("Desconto adicional não é permitido!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if (ret.DescontoAcrescimo > -100 && valor > ret.DescontoAcrescimo)
            {
                MessageBox.Show("Desconto adicional maior que o permitido!", "VDA992", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }
        //FIM TI-6424
        #endregion

    }
}
