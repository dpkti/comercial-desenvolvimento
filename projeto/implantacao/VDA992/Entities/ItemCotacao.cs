﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ItemCotacao
    {
        public int Id_Cotacao { get; set; }
        public int Cod_Loja { get; set; }
        public int Id_Item_Cotacao { get; set; }
        public long Cod_Dpk { get; set; }
        public string Cod_Fabrica { get; set; }
        public string Fabrica_Arquivo { get; set; }
        public decimal Pc_Desc3 { get; set; }
        public int Qtd_Solicitada { get; set; }
        public string Tp_tabela { get; set; }
        public int Cod_Forn { get; set; }
    }
}
