﻿//TI-6424
using System.Collections.Generic;

namespace Entities
{
    public class ApiProduto 
    {
        public int CodigoFilial { get; set; }
        public string Canal { get; set; }
        public string Caracteristica { get; set; }
        public int CodigoCliente { get; set; }
        public List<ApiItem> Produto { get; set; }
    }
}
