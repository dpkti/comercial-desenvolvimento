﻿//TI-6424
namespace Entities
{
    public class Api : ApiItem
    {
        public int CodigoFilial { get; set; }
        public string Canal { get; set; }
        public string Caracteristica { get; set; }
        public int CodigoCliente { get; set; }
    }
}
