﻿//TI-6424
namespace Entities
{
    public class ApiItem
    {
        public int CodigoFornecedor { get; set; }
        public int CodigoGrupo { get; set; }
        public int CodigoSubGrupo { get; set; }
        public long CodigoDPK { get; set; }
        public decimal DescontoAcrescimo { get; set; }
        public string Tabela { get; set; }
    }
}
