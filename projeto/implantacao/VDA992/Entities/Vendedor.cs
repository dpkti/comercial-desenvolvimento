﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Vendedor
    {
        public int Cod_Repres { get; set; }
        public string Nome_Repres { get; set; }
        public string Ativo { get; set; }
    }
}
