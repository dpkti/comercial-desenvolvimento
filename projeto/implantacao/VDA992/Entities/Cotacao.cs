﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Cotacao
    {
        public int Id_Cotacao { get; set; }
        public int Cod_Loja { get; set; }
        public string Dt_Validade { get; set; }
        public int Cod_Cliente { get; set; }
        public int Cod_Vend { get; set; }
        public int Cod_Plano { get; set; }
        public decimal Pc_Desconto { get; set; }
        public int Cod_Vdr { get; set; }
        public decimal Pc_Margem { get; set; }
        public decimal Pc_Param_Mg { get; set; }
        public int Cod_Usuario { get; set; }
        public string Tp_tabela { get; set; }
        public string Dt_Inclusao { get; set; }
    }
}
