﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Plano
    {
        public int Cod_Plano { get; set; }
        public string Desc_Plano { get; set; }
        public string Fl_Avista { get; set; }
    }
}
