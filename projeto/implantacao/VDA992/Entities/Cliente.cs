﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Cliente
    {
        public int Cod_Cliente { get; set; }
        public string Nome_Cliente { get; set; }
        public string Ativo { get; set; }
    }
}
