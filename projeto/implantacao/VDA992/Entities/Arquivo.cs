﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class Arquivo
    {
        public string Fabrica_Arquivo { get; set; }
        public string Cod_Fabrica { get; set; }        
        public string Sigla { get; set; }
        public string Desc_Item { get; set; }
        public string Disponivel { get; set; }
        public int Qtd_Solicitada { get; set; }
        public decimal Preco_Liquido { get; set; }
        public string Tabela { get; set; } //TI-6289
        public decimal Vl_Icms_Retido { get; set; }
        public long Cod_Dpk { get; set; }
        public string Alterado { get; set; }
        public decimal Pc_Margem { get; set; }
        public string Tp_Tabela { get; set; }
        public decimal Pc_Desc3 { get; set; }
        public int Id_Item_Cotacao { get; set; }        
    }
}