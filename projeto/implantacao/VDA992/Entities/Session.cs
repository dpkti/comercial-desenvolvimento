﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class Session
    {
        public static int Id_Cotacao { get; set; }
        public static int Cod_Usuario { get; set; }
        public static string Deposito_Visao { get; set; } //TI-5015
    }
}
