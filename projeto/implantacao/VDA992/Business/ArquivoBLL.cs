﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using DALC;
using Entities;
using Excel;
using UtilGeralDPA;
using System.Data;


namespace Business
{
    public class ArquivoBLL : BaseBLL<ArquivoBLL>
    {
        private List<Arquivo> lst = null;

        public List<Arquivo> Selecionar(int Id_Cotacao)
        {
            try
            {
                lst = ArquivoDALC.Instance().Selecionar(Id_Cotacao);
            }
            catch (Exception)
            {
                throw;
            }
            return lst;
        }

        /// <summary>
        /// Método responsável por carregar o arquivo excel que será importado.
        /// </summary>
        /// <returns></returns>
        public static DataTable CarregarExcel(string caminhoArquivo)
        {
            string caminhoOrigem;
            string arquivo = "";
            IExcelDataReader excelReader = null;
            DataTable dt = new DataTable();

            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                FileStream stream = File.Open(arquivo, FileMode.Open, FileAccess.Read);

                string extension = Path.GetExtension(arquivo);
                if (extension.Equals(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                dt = excelReader.AsDataSet().Tables[0];
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                excelReader.Close();

                File.Delete(arquivo);
            }

            return dt;
        }
    }
}