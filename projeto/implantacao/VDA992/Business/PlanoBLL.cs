﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class PlanoBLL : BaseBLL<PlanoBLL>
    {
        private List<Plano> lst = null;

        public List<Plano> Selecionar(Plano oEntity)
        {
            try
            {
                lst = PlanoDALC.Instance().Selecionar(oEntity);
            }
            catch (Exception)
            {                
                throw;
            }            
            return lst;
        }
    }
}