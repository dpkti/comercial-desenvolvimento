﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using DALC;
using Entities;
using UtilGeralDPA;
using System.Data;


namespace Business
{
    public class ItemCotacaoBLL : BaseBLL<ItemCotacaoBLL>
    {

        public static decimal CalcularPreco(int Id_Cotacao)
        {
            try
            {
                return ItemCotacaoDALC.CalcularPreco(Id_Cotacao);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Inserir(ItemCotacao oEntity)
        {
            try
            {
                ItemCotacaoDALC.Inserir(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Alterar(ItemCotacao oEntity)
        {
            try
            {
                ItemCotacaoDALC.Alterar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string Apagar(ItemCotacao oEntity)
        {
            try
            {
                return ItemCotacaoDALC.Apagar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
