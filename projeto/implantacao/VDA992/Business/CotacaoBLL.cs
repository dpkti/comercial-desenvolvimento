﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel;
using System.Data;
using System.IO;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class CotacaoBLL : BaseBLL<CotacaoBLL>
    {
        private List<Cotacao> lst = null;

        public List<Cotacao> Selecionar(Cotacao oEntity)
        {
            try
            {
                lst = CotacaoDALC.Instance().Selecionar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
            return lst;
        }

        public static int Inserir(Cotacao oEntity)
        {
            try
            {
                return CotacaoDALC.Inserir(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void Alterar(Cotacao oEntity)
        {
            try
            {
                CotacaoDALC.Alterar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int Finalizar(int oEntity)
        {
            try
            {
                return CotacaoDALC.Finalizar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
