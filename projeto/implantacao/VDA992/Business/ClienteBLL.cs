﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class ClienteBLL : BaseBLL<ClienteBLL>
    {
        private List<Cliente> lst = null;

        public List<Cliente> Selecionar(Cliente oEntity)
        {
            try
            {
                lst = ClienteDALC.Instance().Selecionar(oEntity);
            }
            catch (Exception)
            {                
                throw;
            }            
            return lst;
        }
    }
}
