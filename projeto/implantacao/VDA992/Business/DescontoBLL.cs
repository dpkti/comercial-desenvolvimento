﻿using System.Net;
using System.Collections.Generic;
using Entities;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using DALC;
using System;
//TI-6424
namespace Business
{
    public class DescontoBLL : BaseBLL<DescontoBLL>
    {
        private static string usuario = ConfigurationManager.AppSettings["USUARIO"].ToString();
        private static string senha = ConfigurationManager.AppSettings["SENHA"];
        private static string endPoint = ConfigurationManager.AppSettings["ENDPOINT"];
        public static Api DescontoAdicional(Api api)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", endPoint, "api/desconto"));
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization", string.Format("Basic {0}:{1}", usuario, senha));
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = JsonConvert.SerializeObject(api);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var desconto = JsonConvert.DeserializeObject<Api>(result);
                return desconto;
            }

        }
        public static ApiProduto DescontoAdicional(ApiProduto api)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(string.Format("{0}{1}", endPoint, "api/produto-desconto"));
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Authorization", string.Format("Basic {0}:{1}", usuario, senha));
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var json = JsonConvert.SerializeObject(api);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var desconto = JsonConvert.DeserializeObject<ApiProduto>(result);
                return desconto;
            }

        }
        private List<Api> lst = null;
        public List<Api> Selecionar(int IdCotacao, int CodDpk)
        {
            try
            {
                lst = DescontoDALC.Instance().Selecionar(IdCotacao, CodDpk);
            }
            catch (Exception)
            {
                throw;
            }
            return lst;
        }
        public static void Alterar(ItemCotacao oEntity)
        {
            try
            {
                DescontoDALC.Alterar(oEntity);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
