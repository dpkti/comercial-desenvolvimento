﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class VendedorBLL : BaseBLL<VendedorBLL>
    {
        private List<Vendedor> lst = null;

        public List<Vendedor> Selecionar(Vendedor oEntity)
        {
            try
            {
                lst = VendedorDALC.Instance().Selecionar(oEntity);
            }
            catch (Exception)
            {                
                throw;
            }            
            return lst;
        }
    }
}