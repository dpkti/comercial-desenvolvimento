﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DALC;
using Entities;
using UtilGeralDPA;

namespace Business
{
    public class UsuarioBLL : BaseBLL<UsuarioBLL>
    {
        public Usuario ValidarUsuario(Usuario usuario)
        {
            try
            {
                Usuario usuarioValidado = new Usuario();

                usuarioValidado = UsuarioDALC.Instance().SelecionarUsuario(usuario.Login, usuario.Senha);

                return usuarioValidado;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }
    }
}
