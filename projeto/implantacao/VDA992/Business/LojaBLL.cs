﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using DALC;
using Entities;
using Excel;
using UtilGeralDPA;

namespace Business
{
    public class LojaBLL : BaseBLL<LojaBLL>
    {
        private List<Loja> listaLoja = null;

        public List<Loja> ListarTodas()
        {
            try
            {
                //cache da pesquisa de lojas
                if (listaLoja == null)
                {
                    listaLoja = LojaDALC.Instance().SelecionarTodas();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listaLoja;
        }

    }
}
