﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;
//TI-6424
namespace DALC
{
    public class DescontoDALC : BaseDALC<DescontoDALC>
    {
        public List<Api> Selecionar(int IdCotacao,int CodDpk)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Api item = null;
            List<Api> lst = new List<Api>();
            IRowMapper<Api> mapper = MapBuilder<Api>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_API"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, IdCotacao);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int32, CodDpk);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        item = mapper.MapRow(reader);
                        lst.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }
        public static void Alterar(ItemCotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_UPD_API"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, oEntity.Cod_Dpk);
                    db.AddInParameter(cmd, "PM_DESC3", DbType.Decimal, oEntity.Pc_Desc3);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }
    }
}
