﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;
namespace DALC
{
    public class LojaDALC : BaseDALC<LojaDALC>
    {
        public List<Loja> SelecionarTodas()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Loja loja = null;
            List<Loja> listaRetorno = new List<Loja>();
            IRowMapper<Loja> mapper = MapBuilder<Loja>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_LOJA"))
                {
                    db.AddInParameter(cmd, "PM_TABELA_BANCO", DbType.String, Session.Deposito_Visao); //TI-5015

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        loja = mapper.MapRow(reader);
                        listaRetorno.Add(loja);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaRetorno;
        }

    }
}
