﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class ClienteDALC : BaseDALC<ClienteDALC>
    {
        public List<Cliente> Selecionar(Cliente oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Cliente cliente = null;
            List<Cliente> lst = new List<Cliente>();
            IRowMapper<Cliente> mapper = MapBuilder<Cliente>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_CLIENTE"))
                {
                    db.AddInParameter(cmd, "PM_NOME_CLIENTE", DbType.String, oEntity.Nome_Cliente);
                    db.AddInParameter(cmd, "PM_COD_CLIENTE", DbType.Int32, oEntity.Cod_Cliente);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        cliente = mapper.MapRow(reader);
                        lst.Add(cliente);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }
    }
}
