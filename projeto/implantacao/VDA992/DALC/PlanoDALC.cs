﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class PlanoDALC : BaseDALC<PlanoDALC>
    {
        public List<Plano> Selecionar(Plano oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Plano plano = null;
            List<Plano> lst = new List<Plano>();
            IRowMapper<Plano> mapper = MapBuilder<Plano>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_PLANO"))
                {
                    db.AddInParameter(cmd, "PM_COD_PLANO", DbType.Int32, oEntity.Cod_Plano);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        plano = mapper.MapRow(reader);
                        lst.Add(plano);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }
    }
}