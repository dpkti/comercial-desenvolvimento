﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class CotacaoDALC : BaseDALC<CotacaoDALC>
    {
        public List<Cotacao> Selecionar(Cotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Cotacao cotacao = null;
            List<Cotacao> lst = new List<Cotacao>();
            IRowMapper<Cotacao> mapper = MapBuilder<Cotacao>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_STG_COTACAO"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.String, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_COD_CLIENTE", DbType.Int32, oEntity.Cod_Cliente);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        cotacao = mapper.MapRow(reader);
                        lst.Add(cotacao);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }

        public static int Inserir(Cotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            int ret = 0;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_INS_STG_COTACAO"))
                {
                    db.AddInParameter(cmd, "PM_COD_LOJA", DbType.Int32, oEntity.Cod_Loja);
                    db.AddInParameter(cmd, "PM_DT_VALIDADE", DbType.Date, oEntity.Dt_Validade);
                    db.AddInParameter(cmd, "PM_COD_CLIENTE", DbType.Int32, oEntity.Cod_Cliente);
                    db.AddInParameter(cmd, "PM_COD_VEND", DbType.Int32, oEntity.Cod_Vend);
                    db.AddInParameter(cmd, "PM_COD_PLANO", DbType.Int32, oEntity.Cod_Plano);
                    db.AddInParameter(cmd, "PM_PC_DESCONTO", DbType.Decimal, oEntity.Pc_Desconto);
                    db.AddInParameter(cmd, "PM_COD_VDR", DbType.Int32, oEntity.Cod_Vdr);
                    db.AddInParameter(cmd, "PM_PC_PARAM_MG", DbType.Decimal, oEntity.Pc_Param_Mg);
                    db.AddInParameter(cmd, "PM_COD_USUARIO", DbType.Int32, Session.Cod_Usuario);
                    db.AddInParameter(cmd, "PM_TP_TABELA", DbType.String, oEntity.Tp_tabela);
                    db.AddOutParameter(cmd, "PM_ID_COTACAO", DbType.Int32, 8);
                    db.ExecuteNonQuery(cmd);
                    ret = (int)db.GetParameterValue(cmd, "PM_ID_COTACAO");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {                
                Logger.LogFinalMetodo(dataInicio);
            }
            return ret;
        }

        public static void Alterar(Cotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_UPD_STG_COTACAO"))
                {
                    db.AddInParameter(cmd, "PM_COD_LOJA", DbType.Int32, oEntity.Cod_Loja);
                    db.AddInParameter(cmd, "PM_DT_VALIDADE", DbType.Date, oEntity.Dt_Validade);
                    db.AddInParameter(cmd, "PM_COD_CLIENTE", DbType.Int32, oEntity.Cod_Cliente);
                    db.AddInParameter(cmd, "PM_COD_VEND", DbType.Int32, oEntity.Cod_Vend);
                    db.AddInParameter(cmd, "PM_COD_PLANO", DbType.Int32, oEntity.Cod_Plano);
                    db.AddInParameter(cmd, "PM_PC_DESCONTO", DbType.Decimal, oEntity.Pc_Desconto);
                    db.AddInParameter(cmd, "PM_COD_VDR", DbType.Int32, oEntity.Cod_Vdr);
                    db.AddInParameter(cmd, "PM_PC_PARAM_MG", DbType.Decimal, oEntity.Pc_Param_Mg);
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_TP_TABELA", DbType.String, oEntity.Tp_tabela);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static int Finalizar(int oEntity)
        {
            int ret = 0;
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_INS_FINALIZAR"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity);
                    db.AddOutParameter(cmd, "PM_NUM_COTACAO", DbType.Int32, 7);
                    db.ExecuteNonQuery(cmd);
                    ret = (int)db.GetParameterValue(cmd, "PM_NUM_COTACAO");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return ret;
        }

    }
}
