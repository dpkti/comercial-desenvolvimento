﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class VendedorDALC : BaseDALC<VendedorDALC>
    {
        public List<Vendedor> Selecionar(Vendedor oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Vendedor vend = null;
            List<Vendedor> lst = new List<Vendedor>();
            IRowMapper<Vendedor> mapper = MapBuilder<Vendedor>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_VENDEDOR"))
                {
                    db.AddInParameter(cmd, "PM_NOME_VEND", DbType.String, oEntity.Nome_Repres);
                    db.AddInParameter(cmd, "PM_COD_VEND", DbType.Int32, oEntity.Cod_Repres);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        vend = mapper.MapRow(reader);
                        lst.Add(vend);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }
    }
}

