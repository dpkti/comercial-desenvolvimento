﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class ArquivoDALC : BaseDALC<ArquivoDALC>
    {
        public List<Arquivo> Selecionar(int Id_Cotacao)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Arquivo arquivo = null;
            List<Arquivo> lst = new List<Arquivo>();
            IRowMapper<Arquivo> mapper = MapBuilder<Arquivo>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_STG_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.String, Id_Cotacao);

                    reader = db.ExecuteReader(cmd);
                    while (reader.Read())
                    {
                        arquivo = mapper.MapRow(reader);
                        lst.Add(arquivo);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return lst;
        }
    }
}
