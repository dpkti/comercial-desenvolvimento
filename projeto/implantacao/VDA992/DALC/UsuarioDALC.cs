﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class UsuarioDALC : BaseDALC<UsuarioDALC>
    {
        public Usuario SelecionarUsuario(string usuario, string senha)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            IDataReader reader = null;
            Usuario usuarioLogado = null;
            IRowMapper<Usuario> mapper = MapBuilder<Usuario>.MapAllProperties().Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");
                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_SEL_LOGIN"))
                {
                    db.AddInParameter(cmd, "PM_LOGIN", DbType.String, usuario);
                    db.AddInParameter(cmd, "PM_SENHA", DbType.String, senha);

                    reader = db.ExecuteReader(cmd);
                    if (reader.Read())
                    {
                        usuarioLogado = mapper.MapRow(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Erro ao validar usuário.",ex);
                throw;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                Logger.LogFinalMetodo(dataInicio);
            }

            return usuarioLogado;
        }
    }
}
