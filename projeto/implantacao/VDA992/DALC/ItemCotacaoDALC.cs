﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Entities;
using UtilGeralDPA;

namespace DALC
{
    public class ItemCotacaoDALC : BaseDALC<ItemCotacaoDALC>
    {
        public static void Inserir(ItemCotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_INS_STG_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_COD_LOJA", DbType.Int32, oEntity.Cod_Loja);
                    db.AddInParameter(cmd, "PM_FABRICA", DbType.String, oEntity.Fabrica_Arquivo);
                    db.AddInParameter(cmd, "PM_COD_FABRICA", DbType.String, oEntity.Cod_Fabrica);
                    db.AddInParameter(cmd, "PM_DESC3", DbType.Decimal, oEntity.Pc_Desc3);
                    db.AddInParameter(cmd, "PM_TABELA", DbType.String, oEntity.Tp_tabela);
                    db.AddInParameter(cmd, "PM_QTDE", DbType.Int32, oEntity.Qtd_Solicitada);
                    db.AddInParameter(cmd, "PM_COD_FORN", DbType.Int32, oEntity.Cod_Forn);
                    db.ExecuteNonQuery(cmd);                    
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static void Alterar(ItemCotacao oEntity)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_UPD_STG_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_ID_ITEM_COTACAO", DbType.Int32, oEntity.Id_Item_Cotacao);
                    db.AddInParameter(cmd, "PM_COD_DPK", DbType.Int64, oEntity.Cod_Dpk);
                    db.AddInParameter(cmd, "PM_DESC3", DbType.Decimal, oEntity.Pc_Desc3);
                    db.AddInParameter(cmd, "PM_TP_TABELA", DbType.String, oEntity.Tp_tabela);
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        public static string Apagar(ItemCotacao oEntity)
        {
            string ret=string.Empty;
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_DEL_STG_ITEM"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, oEntity.Id_Cotacao);
                    db.AddInParameter(cmd, "PM_ID_ITEM_COTACAO", DbType.Int32, oEntity.Id_Item_Cotacao);
                    db.AddOutParameter(cmd, "PM_MSG", DbType.String, 100);
                    db.ExecuteNonQuery(cmd);
                    ret = db.GetParameterValue(cmd, "PM_MSG").ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return ret;
        }

        public static decimal CalcularPreco(int Id_Cotacao)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            decimal ret = 0;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ORA_SDPK_CONN");

                using (DbCommand cmd = db.GetStoredProcCommand("PRODUCAO.PCK_VDA992.PR_UPD_CALCULO_PRECO"))
                {
                    db.AddInParameter(cmd, "PM_ID_COTACAO", DbType.Int32, Id_Cotacao);
                    db.AddOutParameter(cmd, "PM_MARGEM", DbType.Decimal, 8);
                    db.ExecuteNonQuery(cmd);
                    ret = (decimal)db.GetParameterValue(cmd, "PM_MARGEM");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                //throw ex;
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            return ret;
        }

    }
}
