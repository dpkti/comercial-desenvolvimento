VERSION 4.00
Begin VB.Form frmPesquisa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�ltimo Contatos"
   ClientHeight    =   3960
   ClientLeft      =   1335
   ClientTop       =   2400
   ClientWidth     =   7470
   Height          =   4365
   Left            =   1275
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3960
   ScaleWidth      =   7470
   Top             =   2055
   Width           =   7590
   Begin VB.Label lblSair 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "sair"
      Height          =   255
      Left            =   6480
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.Label lblExcluir 
      Alignment       =   2  'Center
      BackColor       =   &H0080FFFF&
      Caption         =   "excluir"
      Height          =   255
      Left            =   5640
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   615
   End
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   5760
      TabIndex        =   2
      Top             =   240
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmPesq.frx":0000
   End
   Begin Threed.SSCommand SSCommand2 
      Height          =   495
      Left            =   6480
      TabIndex        =   1
      Top             =   240
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   78
      Picture         =   "frmPesq.frx":031A
   End
   Begin MSGrid.Grid Grid1 
      Height          =   2055
      Left            =   120
      TabIndex        =   0
      Top             =   960
      Width           =   7095
      _Version        =   65536
      _ExtentX        =   12515
      _ExtentY        =   3625
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      Cols            =   1
      FixedCols       =   0
      MouseIcon       =   "frmPesq.frx":0634
   End
End
Attribute VB_Name = "frmPesquisa"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Public lngSeq As Long

Private Sub Form_Load()
  lngSeq = 0
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lblExcluir.Visible = False
  lblSair.Visible = False
  
End Sub


Private Sub grid1_DblClick()
  
  grid1.Col = 5
  lngSeq = grid1.Text
  
End Sub

Private Sub SSCommand1_Click()
  On Error GoTo Trata_Erro
  
  If lngSeq = 0 Then
    MsgBox "Selecione o contato que deseja excluir com <duplo click>", _
     vbInformation, "Aten��o"
    Exit Sub
  End If
  Screen.MousePointer = 11
  SQL = "BEGIN"
  SQL = SQL & " delete from vendas.contato"
  SQL = SQL & " Where sequencia = :seq;"
  SQL = SQL & " COMMIT;"
  SQL = SQL & "EXCEPTION"
  SQL = SQL & " WHEN OTHERS THEN"
  SQL = SQL & " ROLLBACK;"
  SQL = SQL & " :COD_ERRORA := SQLCODE;"
  SQL = SQL & " :TXT_ERRORA := SQLERRM;"
  SQL = SQL & "END;"
  
  oradatabase.Parameters.Remove "seq"
  oradatabase.Parameters.Add "seq", lngSeq, 1
  oradatabase.Parameters.Remove "cod_errora"
  oradatabase.Parameters.Add "cod_errora", 0, 2
  oradatabase.Parameters.Remove "txt_errora"
  oradatabase.Parameters.Add "txt_errora", "", 2


  oradatabase.ExecuteSQL SQL
  Screen.MousePointer = 0

  If oradatabase.Parameters("cod_errora") = 0 Then
    MsgBox "Exclus�o OK", vbInformation, "Aten��o"
    lngSeq = 0
    Unload Me
  Else
    MsgBox "Ocorreu o erro: " & oradatabase.Parameters("txt_errora")
  End If
  Exit Sub
  
Trata_Erro:
  Call Process_Line_Errors(SQL)
End Sub


Private Sub SSCommand1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lblExcluir.Visible = True
  lblSair.Visible = False
  
End Sub


Private Sub SSCommand2_Click()
  Unload Me
End Sub


Private Sub SSCommand2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  lblExcluir.Visible = False
  lblSair.Visible = True
  
End Sub


