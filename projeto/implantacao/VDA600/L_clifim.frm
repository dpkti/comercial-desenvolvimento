VERSION 4.00
Begin VB.Form frmClienteFimPedido 
   Caption         =   "Lista de Cliente"
   ClientHeight    =   3075
   ClientLeft      =   1170
   ClientTop       =   2010
   ClientWidth     =   7020
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   Height          =   3480
   Left            =   1110
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   7020
   Top             =   1665
   Width           =   7140
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   4320
      TabIndex        =   6
      Top             =   2640
      Width           =   1095
   End
   Begin VB.TextBox txtNomeCliente 
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1800
      MaxLength       =   30
      TabIndex        =   4
      Top             =   1320
      Width           =   4215
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
   Begin VB.Label lblNomeCliente 
      AutoSize        =   -1  'True
      Caption         =   "Cliente"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   720
      TabIndex        =   5
      Top             =   1320
      Width           =   600
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   5175
   End
   Begin MSGrid.Grid grdCliente 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   6855
      _Version        =   65536
      _ExtentX        =   12091
      _ExtentY        =   3836
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "L_clifim.frx":0000
   End
End
Attribute VB_Name = "frmClienteFimPedido"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String


Private Sub cmdOk_Click()
    On Error GoTo TrataErro
    
    
    Dim ss As Object
    Dim i As Integer
    Dim j As Integer
    
    'mouse
    Screen.MousePointer = vbHourglass
    
    'nome do form
    frmClienteFimPedido.Caption = frmClienteFimPedido.Caption
    
    'montar SQL
          SQL = "select cli.COD_CLIENTE,cli.NOME_CLIENTE,"
    SQL = SQL & " cid.nome_cidade, cid.cod_uf"
    SQL = SQL & " from CLIENTE cli,CIDADE cid"
    SQL = SQL & " where cli.COD_CIDADE = cid.COD_CIDADE and"
    SQL = SQL & " cli.SITUACAO = 0"
    SQL = SQL & " and NOME_CLIENTE like :nome"
    SQL = SQL & " order by cli.NOME_CLIENTE"
    
    'criar consulta
    oradatabase.Parameters.Remove "nome"
    oradatabase.Parameters.Add "nome", txtNomeCliente.Text, 1
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
    
    If ss.EOF And ss.BOF Then
        txtResposta = "0"
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha cliente cadastrado", vbInformation, "Aten��o"
        Unload Me
        Exit Sub
    End If
    
    
    'carrega dados
     With grdCliente
        .Cols = 4
        .Rows = ss.RecordCount + 1
        j = .Rows - 1
        .ColWidth(0) = 660
        .ColWidth(1) = 3800
        .ColWidth(2) = 1800
        .ColWidth(3) = 550
        
        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Nome da Cliente"
        .Col = 2
        .Text = "Cidade"
        .Col = 3
        .Text = "UF"
        
        
        For i = 1 To j
            .Row = i
            .Col = 0
            .Text = ss!COD_CLIENTE
            .Col = 1
            .Text = ss!NOME_CLIENTE
            .Col = 2
            .Text = ss!NOME_CIDADE
            .Col = 3
            .Text = ss!COD_UF
            ss.MoveNext
        Next
        .Row = 0
        .FixedRows = 1
    End With

            
    'mouse
    Screen.MousePointer = vbDefault
    
    'desabilitar botao/label/textboxes combo
    cmdOk.Visible = False
    
    lblNomeCliente.Visible = False
    txtNomeCliente.Visible = False
    
    
    'habilitar label de pesquisa e grid
    lblPesquisa.Visible = True
    grdCliente.Visible = True
        
    Exit Sub

TrataErro:
  If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3189 Then
     FreeLocks
     DoEvents
     Resume
   Else
     Call Process_Line_Errors(SQL)
   End If
End Sub

Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmClienteFimPedido = Nothing
End Sub


Private Sub Form_Load()
    Dim i As Byte
    Dim tam As Byte

    
    'nome do cliente
    frmClienteFimPedido.txtNomeCliente.Text = ""
    
End Sub


Private Sub Form_Unload(Cancel As Integer)
    Set frmClienteFimPedido = Nothing
End Sub


Private Sub grdCliente_DblClick()
    grdCliente.Col = 0
    frmContato.txtCod_Cliente = grdCliente.Text
    
    Unload Me
End Sub

Private Sub grdCliente_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdCliente.Col = 1
    If KeyAscii = 13 Then
        grdCliente.Col = 0
        txtResposta = grdCliente.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdCliente.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdCliente
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
        
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdCliente.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    
    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdCliente
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdCliente.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                    
                End If
            End If
        Else
            Beep
        End If
    End If
    
    'mouse
    Screen.MousePointer = vbDefault

End Sub

Private Sub txtNomeCliente_Change()
    If txtNomeCliente.Text = "" Then
        cmdOk.Enabled = False
    Else
        cmdOk.Enabled = True
    End If
End Sub


Private Sub txtNomeCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = Texto(KeyAscii)
End Sub

