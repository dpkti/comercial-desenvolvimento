CREATE OR REPLACE Package PCK_VDA060 is

	Type tp_Cursor is Ref Cursor;


	PROCEDURE pr_CON_DEPOSITO_DEFAULT( PM_BANCO   IN VARCHAR2,
									   PM_CURSOR1 IN OUT tp_Cursor,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_DOLAR( PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_TAXAS( PM_CODLOJA IN NUMBER,
							PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_DOLARTAXAS( PM_DTCOMIS  IN VARCHAR2,
								 PM_DTICM    IN VARCHAR2,
								 PM_DTPIS    IN VARCHAR2,
								 PM_DTIPI    IN VARCHAR2,
								 PM_DTCOFINS IN VARCHAR2,
								 PM_CURSOR1  IN OUT tp_Cursor,
								 PM_CODERRO  OUT NUMBER,
								 PM_TXTERRO  OUT VARCHAR2);



	PROCEDURE pr_CON_PEDIDOSREPR( PM_CODLOJA          IN VARCHAR2,
				      PM_TPCONS           IN VARCHAR2,
				      PM_CURSOR1          IN OUT tp_Cursor,
				      PM_LOJA_DEFAULT     IN VARCHAR2,
				      PM_TP_BANCO         IN VARCHAR2,
				      PM_TP_BANCO_DESTINO IN VARCHAR2,
				      PM_CODERRO OUT NUMBER,
				      PM_TXTERRO OUT VARCHAR2) ;


	PROCEDURE pr_CON_DADOSCABECALHO( PM_NUMPED  IN NUMBER,
					 PM_SEQPED  IN NUMBER,
					 PM_CODLOJA IN NUMBER,
					 PM_CURSOR1 IN OUT tp_Cursor,
					 PM_LOJA_DEFAULT     IN VARCHAR2,
					 PM_TP_BANCO         IN VARCHAR2,
				         PM_TP_BANCO_DESTINO IN VARCHAR2,
					 PM_CODERRO  OUT NUMBER,
					 PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_UF_ORIGEM_DESTINO( PM_UFORIGEM  IN VARCHAR2,
										PM_UFDESTINO IN VARCHAR2,
										PM_CURSOR1   IN OUT tp_Cursor,
										PM_CODERRO   OUT NUMBER,
										PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_DOLARUF( PM_DTUF     IN VARCHAR2,
							  PM_DTFATURA IN VARCHAR2,
							  PM_PMF      IN NUMBER,
							  PM_CURSOR1  IN OUT tp_Cursor,
							  PM_CODERRO  OUT NUMBER,
							  PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_FRETEMEDIO( PM_CODLOJA   IN NUMBER,
			   	     PM_UFDESTINO IN VARCHAR2,
	    			     PM_CURSOR1   IN OUT tp_Cursor,
	    			     PM_LOJA_DEFAULT     IN VARCHAR2,
				     PM_TP_BANCO         IN VARCHAR2,
				     PM_TP_BANCO_DESTINO IN VARCHAR2,
	    			     PM_CODERRO   OUT NUMBER,
		  		     PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_ICMS( PM_UFORIGEM  IN VARCHAR2,
						   PM_UFDESTINO IN VARCHAR2,
						   PM_CURSOR1   IN OUT tp_Cursor,
						   PM_CODERRO   OUT NUMBER,
						   PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_DADOSITEM( PM_NUMPED  IN NUMBER,
			   	    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO  OUT NUMBER,
				     PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_ITEMVDR( PM_CODDPK  IN NUMBER,
							  PM_CURSOR1 IN OUT tp_Cursor,
							  PM_CODERRO  OUT NUMBER,
							  PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_MARGEM( PM_NUMPED  IN NUMBER,
				 PM_SEQPED  IN NUMBER,
				 PM_CODLOJA IN NUMBER,
				 PM_CURSOR1 IN OUT tp_Cursor,
				 PM_LOJA_DEFAULT     IN VARCHAR2,
				 PM_TP_BANCO         IN VARCHAR2,
				 PM_TP_BANCO_DESTINO IN VARCHAR2,
				 PM_CODERRO  OUT NUMBER,
				 PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_CLASSRJ( PM_CLASSFISCAL IN NUMBER,
							  PM_CURSOR1     IN OUT tp_Cursor,
							  PM_CODERRO     OUT NUMBER,
							  PM_TXTERRO     OUT VARCHAR2);


	PROCEDURE pr_CON_SUBST_TRIB( PM_CLASSFISCAL IN NUMBER,
								 PM_UFORIGEM    IN VARCHAR2,
								 PM_UFDESTINO   IN VARCHAR2,
								 PM_CURSOR1     IN OUT tp_Cursor,
								 PM_CODERRO     OUT NUMBER,
								 PM_TXTERRO     OUT VARCHAR2);


	PROCEDURE pr_CON_PIS_COFINS( PM_CLASSFISCAL IN NUMBER,
								 PM_CURSOR1     IN OUT tp_Cursor,
								 PM_CODERRO     OUT NUMBER,
								 PM_TXTERRO     OUT VARCHAR2);


	PROCEDURE pr_CON_PLANOPAGTO( PM_CODPLANO IN NUMBER,
								 PM_CURSOR1  IN OUT tp_Cursor,
								 PM_CODERRO  OUT NUMBER,
								 PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_DIESEL( PM_NUMPED  IN NUMBER,
				 PM_SEQPED  IN NUMBER,
				 PM_CODLOJA IN NUMBER,
				 PM_CURSOR1 IN OUT tp_Cursor,
				 PM_LOJA_DEFAULT     IN VARCHAR2,
			         PM_TP_BANCO         IN VARCHAR2,
				 PM_TP_BANCO_DESTINO IN VARCHAR2,
				 PM_CODERRO  OUT NUMBER,
				 PM_TXTERRO  OUT VARCHAR2);


	PROCEDURE pr_CON_PEDNOTAVENDA( PM_NUMPED  IN NUMBER,
				       PM_SEQPED  IN NUMBER,
				       PM_CODLOJA IN NUMBER,
				       PM_CURSOR1 IN OUT tp_Cursor,
				       PM_LOJA_DEFAULT     IN VARCHAR2,
				       PM_TP_BANCO         IN VARCHAR2,
				       PM_TP_BANCO_DESTINO IN VARCHAR2,
				       PM_CODERRO OUT NUMBER,
				       PM_TXTERRO OUT VARCHAR2);

	PROCEDURE pr_CON_ITENSVDR( PM_NUMPED  IN NUMBER,
	  		           PM_SEQPED  IN NUMBER,
				   PM_CODLOJA IN NUMBER,
				   PM_CURSOR1 IN OUT tp_Cursor,
				   PM_LOJA_DEFAULT     IN VARCHAR2,
				   PM_TP_BANCO         IN VARCHAR2,
				   PM_TP_BANCO_DESTINO IN VARCHAR2,
				   PM_CODERRO OUT NUMBER,
				   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_PLANO414( PM_NUMPED  IN NUMBER,
				   PM_SEQPED  IN NUMBER,
				   PM_CODLOJA IN NUMBER,
				   PM_CURSOR1 IN OUT tp_Cursor,
				   PM_LOJA_DEFAULT     IN VARCHAR2,
				   PM_TP_BANCO         IN VARCHAR2,
				   PM_TP_BANCO_DESTINO IN VARCHAR2,
				   PM_CODERRO OUT NUMBER,
				   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_ITEMPST( PM_NUMPED  IN NUMBER,
				  PM_SEQPED  IN NUMBER,
				  PM_CODLOJA IN NUMBER,
				  PM_CURSOR1 IN OUT tp_Cursor,
				  PM_LOJA_DEFAULT     IN VARCHAR2,
				  PM_TP_BANCO         IN VARCHAR2,
				  PM_TP_BANCO_DESTINO IN VARCHAR2,
				  PM_CODERRO OUT NUMBER,
				  PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_DESCITEMPST( PM_NUMPED  IN NUMBER,
				      PM_SEQPED  IN NUMBER,
				      PM_CODLOJA IN NUMBER,
				      PM_CURSOR1 IN OUT tp_Cursor,
				      PM_LOJA_DEFAULT     IN VARCHAR2,
				      PM_TP_BANCO         IN VARCHAR2,
				      PM_TP_BANCO_DESTINO IN VARCHAR2,
				      PM_CODERRO OUT NUMBER,
				      PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_COUNTFARM( PM_NUMPED  IN NUMBER,
				    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO OUT NUMBER,
				    PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_UFPEDFARM( PM_NUMPED  IN NUMBER,
		  		    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO OUT NUMBER,
				    PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_DESCADICIONAL( PM_NUMPED  IN NUMBER,
					PM_SEQPED  IN NUMBER,
					PM_CODLOJA IN NUMBER,
					PM_CURSOR1 IN OUT tp_Cursor,
					PM_LOJA_DEFAULT     IN VARCHAR2,
					PM_TP_BANCO         IN VARCHAR2,
				    	PM_TP_BANCO_DESTINO IN VARCHAR2,
					PM_CODERRO OUT NUMBER,
   					PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_MOTIVOSCANCEL( PM_CURSOR1 IN OUT tp_Cursor,
									PM_CODERRO OUT NUMBER,
									PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_MOTIVOBLOQUEIO( PM_NUMPED  IN NUMBER,
					 PM_SEQPED  IN NUMBER,
					 PM_CODLOJA IN NUMBER,
					 PM_CURSOR1 IN OUT tp_Cursor,
					 PM_LOJA_DEFAULT     IN VARCHAR2,
					 PM_TP_BANCO         IN VARCHAR2,
				    	 PM_TP_BANCO_DESTINO IN VARCHAR2,
					 PM_CODERRO OUT NUMBER,
				         PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_DADOSITEMPED( PM_NUMPED  IN NUMBER,
				       PM_SEQPED  IN NUMBER,
				       PM_CODLOJA IN NUMBER,
				       PM_ITEMPED IN NUMBER,
				       PM_CURSOR1 IN OUT tp_Cursor,
				       PM_LOJA_DEFAULT     IN VARCHAR2,
				       PM_TP_BANCO         IN VARCHAR2,
				       PM_TP_BANCO_DESTINO IN VARCHAR2,
				       PM_CODERRO OUT NUMBER,
				       PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_ACT_DADOSITEMPED( PM_NUMPED  IN NUMBER,
								   PM_SEQPED  IN NUMBER,
								   PM_CODLOJA IN NUMBER,
								   PM_ITEMPED IN NUMBER,
								   PM_DESC1   IN NUMBER,
								   PM_DESC2   IN NUMBER,
								   PM_DESC3   IN NUMBER,
								   PM_LOJA_DEFAULT     IN VARCHAR2,
							    	   PM_TP_BANCO         IN VARCHAR2,
								   PM_TP_BANCO_DESTINO IN VARCHAR2,
								   PM_CODERRO OUT NUMBER,
								   PM_TXTERRO OUT VARCHAR2);


	PROCEDURE pr_CON_REPRES( PM_CODREPRES IN NUMBER,
							 PM_CURSOR1   IN OUT tp_Cursor,
							 PM_CODERRO   OUT NUMBER,
							 PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_VENDEDOR( PM_CODREPRES IN NUMBER,
							   PM_TPREPRES IN NUMBER,
							   PM_CURSOR1   IN OUT tp_Cursor,
							   PM_CODERRO   OUT NUMBER,
							   PM_TXTERRO   OUT VARCHAR2);


	PROCEDURE pr_CON_TRANSPORTADORA( PM_CODTRANSP IN NUMBER,
									 PM_CURSOR1   IN OUT tp_Cursor,
									 PM_CODERRO   OUT NUMBER,
									 PM_TXTERRO   OUT VARCHAR2);

       PROCEDURE pr_CON_TEXACO( PM_NUMPED  IN NUMBER,
				PM_SEQPED  IN NUMBER,
				PM_CODLOJA IN NUMBER,
				PM_CURSOR1 IN OUT tp_Cursor,
				PM_LOJA_DEFAULT     IN VARCHAR2,
				PM_TP_BANCO         IN VARCHAR2,
				PM_TP_BANCO_DESTINO IN VARCHAR2,
				PM_CODERRO  OUT NUMBER,
				PM_TXTERRO  OUT VARCHAR2);


   PROCEDURE PR_SENHA(p_cursor    IN OUT tp_cursor,
                      p_login     IN     VARCHAR2,
                      p_senha     IN     VARCHAR2,
                      p_erro      OUT    NUMBER);

   PROCEDURE PR_RESPONSAVEL(p_cursor    IN OUT tp_cursor,
                      	    p_erro     OUT     NUMBER);


 PROCEDURE PR_ATUALIZA_LOG(p_cod_loja      IN     pednota_venda.cod_loja%TYPE,
                  	   p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                  	   p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                  	   p_dt_pedido     IN     pednota_venda.dt_pedido%TYPE,
                  	   p_fl_lib        IN     VARCHAR2,
                  	   p_cod_usuario   IN     helpdesk.usuario.cod_usuario%TYPE,
                  	   p_cod_usuario_r IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_erro          OUT    NUMBER);

PROCEDURE pr_email_politica
	(p_cod_loja              IN     pednota_venda.cod_loja%TYPE,
        p_num_pedido             IN     pednota_venda.num_pedido%TYPE,
        p_seq_pedido             IN     pednota_venda.seq_pedido%TYPE,
        p_cod_cliente            IN     pednota_venda.cod_cliente%TYPE,
        p_nome_cliente           IN     cliente.nome_cliente%TYPE,
        p_vl_contabil            IN     pednota_venda.vl_contabil%TYPE,
        p_cod_usuario_politica   IN     NUMBER,
        p_cod_usuario            IN     NUMBER,
        p_mg_cuema               IN     NUMBER,
        p_mg_repos               IN     NUMBER);

Procedure Pr_Select_Loja (Pm_Cursor IN OUT TP_CURSOR);

Procedure Pr_Tipo_Banco_CD (Pm_Cursor   IN OUT TP_CURSOR,
			    pm_cod_loja NUMBER );



End PCK_VDA060;
---------------

/
CREATE OR REPLACE Package Body PCK_VDA060 is


	PROCEDURE pr_CON_DEPOSITO_DEFAULT( PM_BANCO   IN VARCHAR2,
									   PM_CURSOR1 IN OUT tp_Cursor,
									   PM_CODERRO OUT NUMBER,
									   PM_TXTERRO OUT VARCHAR2) AS

		STRSQL varchar2(2000);

	BEGIN

		PM_CODERRO := 0;

		STRSQL := STRSQL || ' select a.dt_faturamento, a.dt_real, ';
		STRSQL := STRSQL || ' to_char(b.cod_filial,''009'') || ''-'' || c.sigla filial, ';
		STRSQL := STRSQL || ' d.cod_uf cod_uf,';
		STRSQL := STRSQL || ' to_char(b.cod_loja,''09'') ||''-''||e.nome_fantasia deposito_default';
		STRSQL := STRSQL || ' from datas a, ' || PM_BANCO || 'deposito b, filial c,  cidade d, loja e ';
		STRSQL := STRSQL || ' where  b.cod_filial=c.cod_filial and ';
		STRSQL := STRSQL || ' b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade ';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DEPOSITO_DEFAULT;


	PROCEDURE pr_CON_DOLAR( PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select avg(a.valor_uss) dolar_medio
		from dolar_diario a, datas b
		Where a.data_uss >= '01-'||to_char(b.dt_faturamento,'MON-RR') and
		a.data_uss <= last_day(b.dt_faturamento);

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DOLAR;


	PROCEDURE pr_CON_TAXAS( PM_CODLOJA IN NUMBER,
							PM_CURSOR1 IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select substr(to_char(data_1,'DD/MM/RR'),1,8) dt_icm,
		substr(to_char(data_2,'DD/MM/RR'),1,8) dt_pis,
		substr(to_char(data_3,'DD/MM/RR'),1,8) dt_comis,
		substr(to_char(data_4,'DD/MM/RR'),1,8) dt_ipi,
		substr(to_char(data_5,'DD/MM/RR'),1,8) dt_cofins,
		substr(to_char(valor_6),1,16) pc_compra,
		substr(to_char(valor_7),1,16) pc_venda,
		substr(to_char(valor_2),1,16) pc_pis,
		substr(to_char(valor_13),1,16) pc_cofins
		From taxa
		Where divisao ='D' and
		cod_loja = PM_CODLOJA;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_TAXAS;


	PROCEDURE pr_CON_DOLARTAXAS( PM_DTCOMIS  IN VARCHAR2,
								 PM_DTICM    IN VARCHAR2,
								 PM_DTPIS    IN VARCHAR2,
								 PM_DTIPI    IN VARCHAR2,
								 PM_DTCOFINS IN VARCHAR2,
								 PM_CURSOR1  IN OUT tp_Cursor,
								 PM_CODERRO  OUT NUMBER,
								 PM_TXTERRO  OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select substr(to_char(a.valor_uss),1,12) dolar_comis,
		substr(to_char(b.valor_uss),1,12) dolar_icm,
		substr(to_char(c.valor_uss),1,12) dolar_pis,
		substr(to_char(d.valor_uss),1,12) dolar_ipi,
		substr(to_char(e.valor_uss),1,12) dolar_cofins,
		substr(to_char(f.valor_uss),1,12) dolar_dia
		From dolar_diario a, dolar_diario b, dolar_diario c,
		dolar_diario d, dolar_diario e, dolar_diario f, datas g
		Where a.data_uss = to_date(PM_DTCOMIS,'DD/MM/RR') and
		      b.data_uss = to_date(PM_DTICM,'DD/MM/RR') and
		      c.data_uss = to_date(PM_DTPIS,'DD/MM/RR') and
		      d.data_uss = to_date(PM_DTIPI,'DD/MM/RR') and
		      e.data_uss = to_date(PM_DTCOFINS,'DD/MM/RR') and
		      f.data_uss = g.dt_faturamento;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DOLARTAXAS;


	PROCEDURE pr_CON_PEDIDOSREPR( PM_CODLOJA          IN VARCHAR2,
				      PM_TPCONS           IN VARCHAR2,
				      PM_CURSOR1          IN OUT tp_Cursor,
				      PM_LOJA_DEFAULT     IN VARCHAR2,
				      PM_TP_BANCO         IN VARCHAR2,
				      PM_TP_BANCO_DESTINO IN VARCHAR2,
				      PM_CODERRO OUT NUMBER,
				      PM_TXTERRO OUT VARCHAR2) AS

		STRSQL varchar2(2000);
		w_deposito varchar2(7);


	BEGIN


		PM_CODERRO := 0;



		STRSQL := STRSQL || ' Select /*+ index(a IX_FLNFSITNOPLOJ_PEDNOTA) INDEX(C PK_TRANSPORTADORA) INDEX(D PK_REPRESENTANTE) INDEX(E PK_FILIAL) */ ';
		STRSQL := STRSQL || ' A.COD_LOJA "LOJA", a.num_pedido "NR. PEDIDO", a.seq_pedido "SEQ.",';
		STRSQL := STRSQL || ' d.cod_filial "FIL",e.sigla "FILIAL",b.nome_cliente "CLIENTE",c.nome_transp "TRANSPORTADORA"';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' From producao.pednota_venda a, producao.cliente b, producao.transportadora c, producao.representante d,';
		   STRSQL := STRSQL || ' producao.filial e';
		ELSE
	      	   w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');
		   STRSQL := STRSQL || ' From ' || w_deposito || '.pednota_venda a,';
		   STRSQL := STRSQL ||      w_deposito || '.cliente b,';
		   STRSQL := STRSQL ||      w_deposito || '.transportadora c,';
		   STRSQL := STRSQL ||      w_deposito || '.representante d,';
		   STRSQL := STRSQL ||      w_deposito || '.filial e';

		END IF;

		IF PM_TPCONS = 'R' THEN
			STRSQL := STRSQL || ' Where a.tp_pedido+0 in (10,11) and';
			STRSQL := STRSQL || ' d.cod_repres = a.cod_repres and';
		ELSIF PM_TPCONS = 'T' THEN
			STRSQL := STRSQL || ' Where a.tp_pedido+0 in (21,22) and';
			STRSQL := STRSQL || ' d.cod_repres = a.cod_vend and';
		END IF;

		STRSQL := STRSQL || ' a.tp_dpkblau = 0 and';
		STRSQL := STRSQL || ' a.tp_transacao = 1 and';
		STRSQL := STRSQL || ' a.situacao = 0 and';
		STRSQL := STRSQL || ' a.fl_ger_nfis = ''N'' and';
		STRSQL := STRSQL || ' a.bloq_clie_novo <> ''S'' and';
		STRSQL := STRSQL || ' (a.bloq_desconto = ''S'' or';
		STRSQL := STRSQL || ' a.bloq_acrescimo = ''S'' or';
		STRSQL := STRSQL || ' a.bloq_vlfatmin = ''S'' or';
		STRSQL := STRSQL || ' a.bloq_item_desc1 = ''S'' or';
		STRSQL := STRSQL || ' a.bloq_item_desc2 = ''S'' or';
		STRSQL := STRSQL || ' a.bloq_item_desc3 = ''S'') and';
		STRSQL := STRSQL || ' e.cod_filial = d.cod_filial and';

		STRSQL := STRSQL || ' c.cod_transp = a.cod_transp and';
		STRSQL := STRSQL || ' b.cod_cliente = a.cod_cliente and';
		STRSQL := STRSQL || ' a.cod_loja+0 = '|| PM_CODLOJA;
		STRSQL := STRSQL || ' order by d.cod_filial,a.num_pedido,a.seq_pedido';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_PEDIDOSREPR;


	PROCEDURE pr_CON_DADOSCABECALHO( PM_NUMPED  IN NUMBER,
					 PM_SEQPED  IN NUMBER,
					 PM_CODLOJA IN NUMBER,
					 PM_CURSOR1 IN OUT tp_Cursor,
					 PM_LOJA_DEFAULT     IN VARCHAR2,
					 PM_TP_BANCO         IN VARCHAR2,
				         PM_TP_BANCO_DESTINO IN VARCHAR2,
					 PM_CODERRO  OUT NUMBER,
					 PM_TXTERRO  OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);


	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'Select TO_CHAR(a.cod_loja,''00'')||''-''|| b.nome_fantasia deposito,';
		STRSQL := STRSQL || 'a.num_pedido,a.seq_pedido,a.cod_vdr,a.dt_pedido,';
		STRSQL := STRSQL || 'a.cod_cliente,c.nome_cliente,c.inscr_estadual,c.fl_cons_final,';
                STRSQL := STRSQL || 'c.cod_tipo_cliente,c.classificacao,i.desc_tipo_cli,a.cod_nope,';
	        STRSQL := STRSQL || 'd.nome_cidade,d.cod_uf,a.qtd_item_pedido,a.cod_transp,e.nome_transp,';
                STRSQL := STRSQL || 'a.frete_pago,a.tp_pedido,nvl(a.cod_repres,0) cod_repres,';
                STRSQL := STRSQL || ' nvl(a.cod_vend,0) cod_vend,a.cod_plano,f.desc_plano,f.fl_fora_semana,';
                STRSQL := STRSQL || 'a.pc_desconto,a.pc_acrescimo,a.pc_desc_suframa,a.vl_contabil,a.fl_dif_icm,';
                STRSQL := STRSQL || 'f.prazo_medio,a.cod_filial,h.sigla,nvl(a.mens_pedido,'''') mens_pedido,';
		STRSQL := STRSQL || 'nvl(a.mens_nota,'''') mens_nota,nvl(c.cod_tare,0) cod_tare,h.sigla,';
                STRSQL := STRSQL || 'nvl(c.dt_tare,'''') dt_tare,substr(to_char(g.dt_aliq_interna,''dd/mm/rr''),1,8) dt_aliq_interna ';
                STRSQL := STRSQL || 'From tipo_cliente i, filial h, uf g, plano_pgto f, transportadora e, cidade d,';
	        STRSQL := STRSQL || 'cliente c, loja b ';
                IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
                   STRSQL := STRSQL || ' , producao.pednota_venda a ';
       		ELSE

        	   STRSQL := STRSQL || ' ,' || w_deposito || '.pednota_venda a ';
                END IF;
                STRSQL := STRSQL || ' Where a.num_pedido ='|| PM_NUMPED || ' and';
                STRSQL := STRSQL || ' a.seq_pedido ='|| PM_SEQPED || ' and';
		STRSQL := STRSQL || ' a.cod_loja ='|| PM_CODLOJA || ' and';
		STRSQL := STRSQL || ' i.cod_tipo_cli = c.cod_tipo_cliente and';
		STRSQL := STRSQL || ' h.cod_filial = a.cod_filial and';
		STRSQL := STRSQL || ' g.cod_uf = d.cod_uf and';
		STRSQL := STRSQL || ' f.cod_plano = a.cod_plano and';
		STRSQL := STRSQL || ' e.cod_transp = a.cod_transp and';
		STRSQL := STRSQL || ' d.cod_cidade = c.cod_cidade and';
		STRSQL := STRSQL || ' c.cod_cliente = a.cod_cliente and';
		STRSQL := STRSQL || ' b.cod_loja = a.cod_loja';



		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;



	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DADOSCABECALHO;



	PROCEDURE pr_CON_UF_ORIGEM_DESTINO( PM_UFORIGEM  IN VARCHAR2,
					    PM_UFDESTINO IN VARCHAR2,
					    PM_CURSOR1   IN OUT tp_Cursor,
					    PM_CODERRO   OUT NUMBER,
					    PM_TXTERRO   OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select pc_icm
		From uf_origem_destino
		Where cod_uf_origem = PM_UFORIGEM
		and cod_uf_destino = PM_UFDESTINO;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_UF_ORIGEM_DESTINO;


	PROCEDURE pr_CON_DOLARUF( PM_DTUF     IN VARCHAR2,
				  PM_DTFATURA IN VARCHAR2,
				  PM_PMF      IN NUMBER,
				  PM_CURSOR1  IN OUT tp_Cursor,
				  PM_CODERRO  OUT NUMBER,
				  PM_TXTERRO  OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select substr(to_char(a.valor_uss),1,12) dolar_uf,
		substr(to_char(b.valor_uss),1,12) dolar_pmf
		From dolar_diario a,dolar_diario b
		Where a.data_uss = to_date(PM_DTUF,'DD/MM/RR') and
		b.data_uss = to_date(PM_DTFATURA,'DD/MM/RR') + PM_PMF;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DOLARUF;


	PROCEDURE pr_CON_FRETEMEDIO( PM_CODLOJA   IN NUMBER,
			   	     PM_UFDESTINO IN VARCHAR2,
	    			     PM_CURSOR1   IN OUT tp_Cursor,
	    			     PM_LOJA_DEFAULT     IN VARCHAR2,
				     PM_TP_BANCO         IN VARCHAR2,
				     PM_TP_BANCO_DESTINO IN VARCHAR2,
	    			     PM_CODERRO   OUT NUMBER,
		  		     PM_TXTERRO   OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito VARCHAR2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');


		STRSQL := 'Select pc_frete from  ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		    STRSQL := STRSQL || '  PRODUCAO.uf_deposito ';
		ELSE
	  	    STRSQL := STRSQL || w_deposito || '.uf_deposito  ';
		END IF;
                STRSQL := STRSQL || ' Where cod_loja = '|| PM_CODLOJA || ' and ';
                STRSQL := STRSQL || ' cod_uf_destino = '''|| PM_UFDESTINO || '''';


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_FRETEMEDIO;


	PROCEDURE pr_CON_ICMS( PM_UFORIGEM  IN VARCHAR2,
			       PM_UFDESTINO IN VARCHAR2,
			       PM_CURSOR1   IN OUT tp_Cursor,
			       PM_CODERRO   OUT NUMBER,
			       PM_TXTERRO   OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select pc_icm, pc_icm_gerencial
		From uf_origem_destino
		Where cod_uf_origem = PM_UFORIGEM
		and cod_uf_destino = PM_UFDESTINO;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_ICMS;



	PROCEDURE pr_CON_DADOSITEM( PM_NUMPED  IN NUMBER,
			   	    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO  OUT NUMBER,
				    PM_TXTERRO  OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7) ;


	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');



		STRSQL := 'Select a.num_item_pedido,';
 		STRSQL := STRSQL || 'decode(a.bloq_desc1,''S'',''p'','' '') bloq_periodo,';
 		STRSQL := STRSQL || 'decode(a.bloq_desc2,''S'',''p'','' '') bloq_uf,';
 		STRSQL := STRSQL || 'decode(a.bloq_desc3,''S'',''p'','' '') bloq_adicional,';
 		STRSQL := STRSQL || 'a.qtd_atendida,b.cod_fornecedor,f.sinal,b.cod_fabrica,';
 		STRSQL := STRSQL || 'a.tabela_venda,a.pc_desc1,a.pc_desc2,a.pc_desc3,';
 		STRSQL := STRSQL || 'a.pc_dificm, a.cod_dpk, a.cod_trib ';
 		STRSQL := STRSQL || ' From categ_sinal f,item_analitico c, item_cadastro b ';

		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		    STRSQL := STRSQL || ' , PRODUCAO.itpednota_venda a, PRODUCAO.pednota_venda d';
		ELSE
		    STRSQL := STRSQL || ' , ' || w_deposito || '.itpednota_venda a ';
		    STRSQL := STRSQL || ' , ' || w_deposito || '.pednota_venda d ';
		END IF;

		STRSQL := STRSQL || ' Where d.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' d.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' d.cod_loja = '|| PM_CODLOJA || ' and ';
		STRSQL := STRSQL || ' a.qtd_atendida > 0 and ';
		STRSQL := STRSQL || ' a.situacao = 0 and ';
		STRSQL := STRSQL || ' d.situacao = 0 and ';
		STRSQL := STRSQL || ' f.categoria = c.categoria and ';
		STRSQL := STRSQL || ' c.cod_loja = a.cod_loja and ';
		STRSQL := STRSQL || ' c.cod_dpk = a.cod_dpk and ';
		STRSQL := STRSQL || ' b.cod_dpk = a.cod_dpk and ';
		STRSQL := STRSQL || ' a.cod_loja = d.cod_loja and ';
		STRSQL := STRSQL || ' a.seq_pedido = d.seq_pedido and ';
		STRSQL := STRSQL || ' a.num_pedido = d.num_pedido ';
		STRSQL := STRSQL || ' order by 1 asc';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;



	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DADOSITEM;


	PROCEDURE pr_CON_ITEMVDR( PM_CODDPK  IN NUMBER,
				  PM_CURSOR1 IN OUT tp_Cursor,
				  PM_CODERRO  OUT NUMBER,
				  PM_TXTERRO  OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select nvl(count(*),0) total
		from item_cadastro b,
		vdr.controle_vdr c
		where b.cod_fornecedor = c.cod_fornecedor and
		b.cod_grupo = c.cod_grupo and
		b.cod_subgrupo = c.cod_subgrupo and
		(b.cod_fornecedor,b.cod_grupo,b.cod_subgrupo) not in
		(select x.cod_fornecedor,x.cod_grupo,x.cod_subgrupo
		from vdr.controle_vdr x
		where cod_fornecedor=080 and
		((cod_grupo = 3 and cod_subgrupo =75) or
		cod_grupo=7 or
		cod_grupo=10 or
		cod_grupo=33 or
		cod_grupo=34)
		Group by x.cod_fornecedor,x.cod_grupo,x.cod_subgrupo) and
		B.cod_dpk = PM_CODDPK;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_ITEMVDR;


	PROCEDURE pr_CON_MARGEM( PM_NUMPED  IN NUMBER,
				 PM_SEQPED  IN NUMBER,
				 PM_CODLOJA IN NUMBER,
				 PM_CURSOR1 IN OUT tp_Cursor,
				 PM_LOJA_DEFAULT     IN VARCHAR2,
				 PM_TP_BANCO         IN VARCHAR2,
				 PM_TP_BANCO_DESTINO IN VARCHAR2,
				 PM_CODERRO  OUT NUMBER,
				 PM_TXTERRO  OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'Select b.num_item_pedido,';
		STRSQL := STRSQL || 'b.qtd_atendida,';
		STRSQL := STRSQL || 'a.cod_nope,';
		STRSQL := STRSQL || 'c.vl_liquido,';
		STRSQL := STRSQL || 'b.cod_tribipi,';
		STRSQL := STRSQL || 'b.cod_trib,';
		STRSQL := STRSQL || 'b.pc_ipi,';
		STRSQL := STRSQL || 'c.vl_liquido_suframa,';
		STRSQL := STRSQL || 'a.pc_icm1,';
		STRSQL := STRSQL || 'a.pc_icm2,';
		STRSQL := STRSQL || 'b.pc_comiss,';
		STRSQL := STRSQL || 'b.pc_comisstlmk,';
		STRSQL := STRSQL || 'd.class_fiscal,';
		STRSQL := STRSQL || 'e.cuema,';
		STRSQL := STRSQL || 'g.valor_uss uss_cuema,';
		STRSQL := STRSQL || 'f.custo_reposicao,';
		STRSQL := STRSQL || 'h.valor_uss uss_repos, (e.vl_ult_compra * b.qtd_atendida) vl_ult_compra ';
		STRSQL := STRSQL || ' From datas i,dolar_diario h,dolar_diario g, ';
		STRSQL := STRSQL || ' item_custo f, item_estoque e,item_cadastro d ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		    STRSQL := STRSQL || ', PRODUCAO.v_pedliq_venda c , PRODUCAO.itpednota_venda b, PRODUCAO.pednota_venda A';
		ELSE
		    STRSQL := STRSQL || ' , ' || w_deposito || '.v_pedliq_venda c';
		    STRSQL := STRSQL || ' , ' || w_deposito || '.itpednota_venda b ';
		    STRSQL := STRSQL || ' , ' || w_deposito || '.pednota_venda a ';
		END IF;
		STRSQL := STRSQL || ' Where h.data_uss(+) = (i.dt_faturamento - 1) and ';
		STRSQL := STRSQL || 'g.data_uss(+) = e.dt_cuema and ';
		STRSQL := STRSQL || 'f.cod_loja = b.cod_loja and ';
		STRSQL := STRSQL || 'f.cod_dpk = b.cod_dpk and ';
		STRSQL := STRSQL || 'e.cod_loja = b.cod_loja and ';
		STRSQL := STRSQL || 'e.cod_dpk = b.cod_dpk and ';
		STRSQL := STRSQL || 'd.cod_dpk = b.cod_dpk and ';
		STRSQL := STRSQL || 'c.num_item_pedido = b.num_item_pedido and ';
		STRSQL := STRSQL || 'c.cod_loja = b.cod_loja and ';
		STRSQL := STRSQL || 'c.seq_pedido = b.seq_pedido and ';
		STRSQL := STRSQL || 'c.num_pedido = b.num_pedido and ';
		STRSQL := STRSQL || 'b.cod_loja = a.cod_loja and ';
		STRSQL := STRSQL || 'b.seq_pedido = a.seq_pedido and ';
		STRSQL := STRSQL || 'b.num_pedido = a.num_pedido and ';
		STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA ;
		STRSQL := STRSQL || ' order by 1 asc ';


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_MARGEM;


	PROCEDURE pr_CON_CLASSRJ( PM_CLASSFISCAL IN NUMBER,
							  PM_CURSOR1     IN OUT tp_Cursor,
							  PM_CODERRO     OUT NUMBER,
							  PM_TXTERRO     OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select count(*) total
		From compras.class_antec_entrada
		where class_fiscal = PM_CLASSFISCAL
		and cod_uf_destino = 'RJ';

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_CLASSRJ;


	PROCEDURE pr_CON_SUBST_TRIB( PM_CLASSFISCAL IN NUMBER,
								 PM_UFORIGEM    IN VARCHAR2,
								 PM_UFDESTINO   IN VARCHAR2,
								 PM_CURSOR1     IN OUT tp_Cursor,
								 PM_CODERRO     OUT NUMBER,
								 PM_TXTERRO     OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select fator_revendedor, fator_inscrito,
		fator_isento, pc_margem_lucro, fator_tare,fl_cred_suspenso
		From compras.subst_tributaria
		Where  class_fiscal = PM_CLASSFISCAL and
		cod_uf_origem = PM_UFORIGEM and
		cod_uf_destino = PM_UFDESTINO;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_SUBST_TRIB;


	PROCEDURE pr_CON_PIS_COFINS( PM_CLASSFISCAL IN NUMBER,
								 PM_CURSOR1     IN OUT tp_Cursor,
								 PM_CODERRO     OUT NUMBER,
								 PM_TXTERRO     OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select count(*) total
       	        From PRODUCAO.class_fiscal_pis_cofins
        	where class_fiscal = PM_CLASSFISCAL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_PIS_COFINS;


	PROCEDURE pr_CON_PLANOPAGTO( PM_CODPLANO IN NUMBER,
			   	     PM_CURSOR1  IN OUT tp_Cursor,
	    			     PM_CODERRO  OUT NUMBER,
	   			     PM_TXTERRO  OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		Select prazo_medio, fl_avista,fl_vendor,
		COD_PLANO,DESC_PLANO,
		PC_DESC_FIN_DPK,PC_ACRES_FIN_DPK,
		PC_DESC_FIN_VDR,PC_ACRES_FIN_BLAU,
		SITUACAO,PRAZO_MEDIO
		from plano_pgto
		Where situacao=0 and cod_plano = DECODE(PM_CODPLANO,0,COD_PLANO,PM_CODPLANO);

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_PLANOPAGTO;



	PROCEDURE pr_CON_DIESEL( PM_NUMPED  IN NUMBER,
				 PM_SEQPED  IN NUMBER,
				 PM_CODLOJA IN NUMBER,
				 PM_CURSOR1 IN OUT tp_Cursor,
				 PM_LOJA_DEFAULT     IN VARCHAR2,
			         PM_TP_BANCO         IN VARCHAR2,
				 PM_TP_BANCO_DESTINO IN VARCHAR2,
				 PM_CODERRO  OUT NUMBER,
				 PM_TXTERRO  OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');



		STRSQL := 'select count(*) total_diesel FROM ';

		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		    STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a, PRODUCAO.item_cadastro b';
		ELSE
		    STRSQL := STRSQL ||  w_deposito || '.itpednota_venda a';
		    STRSQL := STRSQL || ' , ' || w_deposito || '.item_cadastro b ';
		END IF;
		STRSQL := STRSQL || ' Where b.cod_fornecedor in (080,082) and ';
		STRSQL := STRSQL || '  b.cod_grupo = 3 and ';
		STRSQL := STRSQL || '  a.situacao <> 9 and ';
		STRSQL := STRSQL || '  a.qtd_atendida <> 0 and ';

		STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DIESEL;


	PROCEDURE pr_CON_PEDNOTAVENDA( PM_NUMPED  IN NUMBER,
				       PM_SEQPED  IN NUMBER,
				       PM_CODLOJA IN NUMBER,
				       PM_CURSOR1 IN OUT tp_Cursor,
				       PM_LOJA_DEFAULT     IN VARCHAR2,
				       PM_TP_BANCO         IN VARCHAR2,
				       PM_TP_BANCO_DESTINO IN VARCHAR2,
				       PM_CODERRO OUT NUMBER,
				       PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'Select pc_desconto,pc_acrescimo,pc_desc_suframa FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.pednota_venda a ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.pednota_venda a';
		END IF;
		STRSQL := STRSQL || ' Where a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA ;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;



	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_PEDNOTAVENDA;


	PROCEDURE pr_CON_ITENSVDR( PM_NUMPED  IN NUMBER,
	  		           PM_SEQPED  IN NUMBER,
				   PM_CODLOJA IN NUMBER,
				   PM_CURSOR1 IN OUT tp_Cursor,
				   PM_LOJA_DEFAULT     IN VARCHAR2,
				   PM_TP_BANCO         IN VARCHAR2,
				   PM_TP_BANCO_DESTINO IN VARCHAR2,
				   PM_CODERRO OUT NUMBER,
				   PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);
	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');


		STRSQL := 'select count(*) total FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a';
		END IF;
		STRSQL := STRSQL || ' ,item_cadastro b ,vdr.controle_vdr c';
		STRSQL := STRSQL || ' where b.cod_fornecedor = c.cod_fornecedor and ';
		STRSQL := STRSQL || ' b.cod_grupo = c.cod_grupo and ';
		STRSQL := STRSQL || ' a.situacao = 0 and ';
		STRSQL := STRSQL || ' a.qtd_atendida > 0 and ';
		STRSQL := STRSQL || ' (b.cod_fornecedor,b.cod_grupo,b.cod_subgrupo)  in ';
		STRSQL := STRSQL || ' (select x.cod_fornecedor,x.cod_grupo,x.cod_subgrupo ';
		STRSQL := STRSQL || ' from vdr.controle_vdr x ';
		STRSQL := STRSQL || ' where cod_fornecedor=080 and ';
		STRSQL := STRSQL || ' ((cod_grupo = 3 and cod_subgrupo <> 75)) ';
		STRSQL := STRSQL || ' Group by x.cod_fornecedor,x.cod_grupo,x.cod_subgrupo) and ';
		STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_ITENSVDR;


	PROCEDURE pr_CON_PLANO414( PM_NUMPED  IN NUMBER,
				   PM_SEQPED  IN NUMBER,
				   PM_CODLOJA IN NUMBER,
				   PM_CURSOR1 IN OUT tp_Cursor,
				   PM_LOJA_DEFAULT     IN VARCHAR2,
				   PM_TP_BANCO         IN VARCHAR2,
				   PM_TP_BANCO_DESTINO IN VARCHAR2,
				   PM_CODERRO OUT NUMBER,
				   PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'select count(*) total FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a,  PRODUCAO.item_cadastro b  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a,';
		   STRSQL := STRSQL || w_deposito || '.item_cadastro b';
		END IF;
		 STRSQL := STRSQL || ' where b.cod_fornecedor <> 127 and ';
		 STRSQL := STRSQL || ' a.situacao <> 9 and a.qtd_atendida <> 0 and  ';
		 STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		 STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		 STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		 STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_PLANO414;


	PROCEDURE pr_CON_ITEMPST( PM_NUMPED  IN NUMBER,
				  PM_SEQPED  IN NUMBER,
				  PM_CODLOJA IN NUMBER,
				  PM_CURSOR1 IN OUT tp_Cursor,
				  PM_LOJA_DEFAULT     IN VARCHAR2,
				  PM_TP_BANCO         IN VARCHAR2,
				  PM_TP_BANCO_DESTINO IN VARCHAR2,
				  PM_CODERRO OUT NUMBER,
				  PM_TXTERRO OUT VARCHAR2) AS
	 STRSQL varchar2(32000);
	 w_deposito varchar2(7);


	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'select count(*) total FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a,  PRODUCAO.item_cadastro b  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a,';
		   STRSQL := STRSQL || w_deposito || '.item_cadastro b';
		END IF;
		 STRSQL := STRSQL || ' where b.cod_fornecedor = 127 and ';
		 STRSQL := STRSQL || ' a.situacao <> 9 and a.qtd_atendida <> 0 and  ';
		 STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		 STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		 STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		 STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_ITEMPST;


	PROCEDURE pr_CON_DESCITEMPST( PM_NUMPED  IN NUMBER,
				      PM_SEQPED  IN NUMBER,
				      PM_CODLOJA IN NUMBER,
				      PM_CURSOR1 IN OUT tp_Cursor,
				      PM_LOJA_DEFAULT     IN VARCHAR2,
				      PM_TP_BANCO         IN VARCHAR2,
				      PM_TP_BANCO_DESTINO IN VARCHAR2,
				      PM_CODERRO OUT NUMBER,
				      PM_TXTERRO OUT VARCHAR2) AS

	  STRSQL varchar2(32000);
	  w_deposito varchar2(7);
	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'select a.pc_desc1,a.pc_desc2,a.pc_desc3 FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a,  PRODUCAO.item_cadastro b  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a,';
		   STRSQL := STRSQL || w_deposito || '.item_cadastro b';
		END IF;
		 STRSQL := STRSQL || ' where b.cod_fornecedor = 127 and ';
		 STRSQL := STRSQL || ' a.situacao <> 9 and a.qtd_atendida <> 0 and  ';
		 STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		 STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		 STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		 STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DESCITEMPST;


	PROCEDURE pr_CON_COUNTFARM( PM_NUMPED  IN NUMBER,
				    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO OUT NUMBER,
				    PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		--STRSQL := 'select a.pc_desc1,a.pc_desc2,a.pc_desc3 FROM ';
		STRSQL := 'select count(*) TOTAL FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a,  PRODUCAO.item_cadastro b  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a,';
		   STRSQL := STRSQL || w_deposito || '.item_cadastro b';
		END IF;
		 STRSQL := STRSQL || ' where b.cod_fornecedor = 223 and ';
		 STRSQL := STRSQL || ' a.situacao <> 9 and a.qtd_atendida <> 0 and  ';
		 STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		 STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		 STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		 STRSQL := STRSQL || ' a.cod_dpk = b.cod_dpk ';


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_COUNTFARM;


	PROCEDURE pr_CON_UFPEDFARM( PM_NUMPED  IN NUMBER,
		  		    PM_SEQPED  IN NUMBER,
				    PM_CODLOJA IN NUMBER,
				    PM_CURSOR1 IN OUT tp_Cursor,
				    PM_LOJA_DEFAULT     IN VARCHAR2,
				    PM_TP_BANCO         IN VARCHAR2,
				    PM_TP_BANCO_DESTINO IN VARCHAR2,
				    PM_CODERRO OUT NUMBER,
				    PM_TXTERRO OUT VARCHAR2) AS
	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'select c.cod_uf, b.cod_tipo_cliente FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.pednota_venda a ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.pednota_venda a';
		END IF;
		STRSQL := STRSQL || ' , cliente b, cidade c ';
		STRSQL := STRSQL || 'WHERE a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA || ' and ';
		STRSQL := STRSQL || ' b.cod_cidade = c.cod_cidade and ';
		STRSQL := STRSQL || ' a.cod_cliente=b.cod_cliente';

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_UFPEDFARM;


	PROCEDURE pr_CON_DESCADICIONAL( PM_NUMPED  IN NUMBER,
					PM_SEQPED  IN NUMBER,
					PM_CODLOJA IN NUMBER,
					PM_CURSOR1 IN OUT tp_Cursor,
					PM_LOJA_DEFAULT     IN VARCHAR2,
					PM_TP_BANCO         IN VARCHAR2,
				    	PM_TP_BANCO_DESTINO IN VARCHAR2,
					PM_CODERRO OUT NUMBER,
   					PM_TXTERRO OUT VARCHAR2) AS

   	STRSQL varchar2(32000);
   	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'Select count(*) total FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.itpednota_venda a ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda a';
		END IF;
		STRSQL := STRSQL || ' where tp_tabela_venda = 1 and ';
		STRSQL := STRSQL || ' pc_desc3 <> 0 AND ';
		STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA;


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DESCADICIONAL;

	PROCEDURE pr_CON_MOTIVOSCANCEL( PM_CURSOR1 IN OUT tp_Cursor,
									PM_CODERRO OUT NUMBER,
									PM_TXTERRO OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select COD_CANCEL "C�D. CANCEL.",DESC_CANCEL "DESC. CANCEL."
		from CANCEL_PEDNOTA
		order by DESC_CANCEL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_MOTIVOSCANCEL;


	PROCEDURE pr_CON_MOTIVOBLOQUEIO( PM_NUMPED  IN NUMBER,
					 PM_SEQPED  IN NUMBER,
					 PM_CODLOJA IN NUMBER,
					 PM_CURSOR1 IN OUT tp_Cursor,
					 PM_LOJA_DEFAULT     IN VARCHAR2,
					 PM_TP_BANCO         IN VARCHAR2,
				    	 PM_TP_BANCO_DESTINO IN VARCHAR2,
					 PM_CODERRO OUT NUMBER,
				         PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);
	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

		STRSQL := 'Select bloq_desconto,bloq_acrescimo,bloq_vlfatmin, ';
		STRSQL := STRSQL || ' bloq_item_desc1,bloq_item_desc2,bloq_item_desc3 FROM ';
		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' PRODUCAO.pednota_venda  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.pednota_venda ';
		END IF;
		STRSQL := STRSQL || ' WHERE num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' cod_loja = '|| PM_CODLOJA;


		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_MOTIVOBLOQUEIO;


	PROCEDURE pr_CON_DADOSITEMPED( PM_NUMPED  IN NUMBER,
				       PM_SEQPED  IN NUMBER,
				       PM_CODLOJA IN NUMBER,
				       PM_ITEMPED IN NUMBER,
				       PM_CURSOR1 IN OUT tp_Cursor,
				       PM_LOJA_DEFAULT     IN VARCHAR2,
				       PM_TP_BANCO         IN VARCHAR2,
				       PM_TP_BANCO_DESTINO IN VARCHAR2,
				       PM_CODERRO OUT NUMBER,
				       PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);
	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');


		STRSQL := 'Select b.num_item_pedido, b.qtd_atendida,b.cod_dpk,b.tabela_venda,';
		STRSQL := STRSQL || ' b.pc_desc1, b.pc_desc3,b.pc_desc2,b.pc_dificm,a.pc_desc_suframa,';
		STRSQL := STRSQL || ' c.pr_liquido, d.cod_fornecedor, d.cod_fabrica, d.desc_item FROM ';

		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		   STRSQL := STRSQL || ' producao.item_cadastro d, PRODUCAO.v_pedliq_venda c,  ';
    		   STRSQL := STRSQL || ' producao.itpednota_venda b, PRODUCAO.pednota_venda a  ';
		ELSE
		   STRSQL := STRSQL || w_deposito || '.item_cadastro d, ';
		   STRSQL := STRSQL || w_deposito || '.V_PEDLIQ_VENDA c, ';
		   STRSQL := STRSQL || w_deposito || '.itpednota_venda b, ';
	           STRSQL := STRSQL || w_deposito || '.pednota_venda a ';
		END IF;
		STRSQL := STRSQL || ' Where c.num_item_pedido = b.num_item_pedido and ';
		STRSQL := STRSQL || ' c.cod_loja = b.cod_loja and ';
		STRSQL := STRSQL || ' c.seq_pedido = b.seq_pedido and ';
		STRSQL := STRSQL || ' c.num_pedido = b.num_pedido and ';
		STRSQL := STRSQL || ' d.cod_dpk = b.cod_dpk and ';
		STRSQL := STRSQL || ' b.cod_loja= a.cod_loja and ';
		STRSQL := STRSQL || ' b.seq_pedido = a.seq_pedido and ';
		STRSQL := STRSQL || ' b.num_pedido = a.num_pedido and ';
		STRSQL := STRSQL || ' b.num_item_pedido = '|| PM_ITEMPED || ' and ';
		STRSQL := STRSQL || ' a.num_pedido = '|| PM_NUMPED || ' and ';
		STRSQL := STRSQL || ' a.seq_pedido = '|| PM_SEQPED || ' and ';
		STRSQL := STRSQL || ' a.cod_loja = '|| PM_CODLOJA;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR STRSQL;


	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_DADOSITEMPED;


	PROCEDURE pr_ACT_DADOSITEMPED( PM_NUMPED  IN NUMBER,
				       PM_SEQPED  IN NUMBER,
				       PM_CODLOJA IN NUMBER,
				       PM_ITEMPED IN NUMBER,
				       PM_DESC1   IN NUMBER,
				       PM_DESC2   IN NUMBER,
				       PM_DESC3   IN NUMBER,
				       PM_LOJA_DEFAULT     IN VARCHAR2,
				       PM_TP_BANCO         IN VARCHAR2,
				       PM_TP_BANCO_DESTINO IN VARCHAR2,
				       PM_CODERRO OUT NUMBER,
				       PM_TXTERRO OUT VARCHAR2) AS

	STRSQL varchar2(32000);
	w_deposito varchar2(7);

	BEGIN

		PM_CODERRO := 0;
		w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');


		IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
		  STRSQL := 'Update PRODUCAO.itpednota_venda set ';
		ELSE
		  STRSQL := 'Update  '|| w_deposito || '.itpednota_venda set ';
		END IF;

		STRSQL := STRSQL || ' pc_desc1 = '  || PM_DESC1 ;
		STRSQL := STRSQL || ' ,pc_desc2 = ' || PM_DESC2;
		STRSQL := STRSQL || ' ,pc_desc3 = ' || PM_DESC3;
		STRSQL := STRSQL || ' Where cod_loja = '|| PM_CODLOJA;
		STRSQL := STRSQL || ' and seq_pedido = '|| PM_SEQPED ;
		STRSQL := STRSQL || ' and num_pedido = ' || PM_NUMPED;
		STRSQL := STRSQL || ' and num_item_pedido = ' || PM_ITEMPED;

		execute immediate (STRSQL);

		COMMIT;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_ACT_DADOSITEMPED;


	PROCEDURE pr_CON_REPRES( PM_CODREPRES IN NUMBER,
				 PM_CURSOR1   IN OUT tp_Cursor,
				 PM_CODERRO   OUT NUMBER,
				 PM_TXTERRO   OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select repr.PSEUDONIMO REPR_PSEUDO,
		repr.SEQ_FRANQUIA,
		prep.PSEUDONIMO PREP_PSEUDO,
		prep.COD_REPRES PREP_COD_REPRES
		from REPRESENTANTE repr,
		REPRESENTANTE prep,
		FILIAL fil
		where fil.COD_FILIAL = repr.COD_FILIAL and
		prep.COD_REPRES(+) = fil.COD_FRANQUEADOR and
		repr.DIVISAO = 'D' and repr.SITUACAO = 0 and
		repr.TIPO = 'R' and
		repr.COD_REPRES = PM_CODREPRES;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_REPRES;


	PROCEDURE pr_CON_VENDEDOR( PM_CODREPRES IN NUMBER,
				   PM_TPREPRES IN NUMBER,
				   PM_CURSOR1   IN OUT tp_Cursor,
				   PM_CODERRO   OUT NUMBER,
				   PM_TXTERRO   OUT VARCHAR2) AS
	STRSQL varchar2(2000);

	BEGIN

		PM_CODERRO := 0;

		STRSQL := ' select PSEUDONIMO from REPRESENTANTE where ';
		STRSQL := STRSQL || ' SITUACAO = 0 and DIVISAO = ''D'' and TIPO in (''M'','''|| PM_TPREPRES ||''') ';
		STRSQL := STRSQL || ' and COD_REPRES = '|| PM_CODREPRES;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		STRSQL;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END PR_CON_VENDEDOR;


	PROCEDURE pr_CON_TRANSPORTADORA( PM_CODTRANSP IN NUMBER,
					 PM_CURSOR1   IN OUT tp_Cursor,
					 PM_CODERRO   OUT NUMBER,
					 PM_TXTERRO   OUT VARCHAR2) AS
	BEGIN

		PM_CODERRO := 0;

		-- Abrindo Cursor
		OPEN PM_CURSOR1 FOR
		select NOME_TRANSP,SITUACAO from TRANSPORTADORA
        where COD_TRANSP = PM_CODTRANSP;

	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;

	END pr_CON_TRANSPORTADORA;


   PROCEDURE pr_CON_TEXACO( PM_NUMPED  IN NUMBER,
				PM_SEQPED  IN NUMBER,
				PM_CODLOJA IN NUMBER,
				PM_CURSOR1 IN OUT tp_Cursor,
				PM_LOJA_DEFAULT     IN VARCHAR2,
				PM_TP_BANCO         IN VARCHAR2,
				PM_TP_BANCO_DESTINO IN VARCHAR2,
				PM_CODERRO  OUT NUMBER,
				PM_TXTERRO  OUT VARCHAR2)
  AS

  STRSQL varchar2(32000);
  w_deposito varchar2(7);

  	BEGIN

  	w_deposito := 'DEP' || LPAD(TO_CHAR(PM_CODLOJA), 2, '0');

  	STRSQL := 'SELECT A.TOTAL_TEXACO + B.TOTAL_TEXACO TOTAL_TEXACO ' ;
	STRSQL := STRSQL || ' From ';
	STRSQL := STRSQL || ' (select count(*) total_texaco ';
	IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
	  STRSQL := STRSQL || ' from itpednota_venda b, ';
          STRSQL := STRSQL || '      pednota_venda a, ';
          STRSQL := STRSQL || '      plano_pgto c ';
        ELSE
	  STRSQL := STRSQL || ' from ' || w_deposito || '.itpednota_venda b, ';
          STRSQL := STRSQL || w_deposito || '.pednota_venda a, ';
          STRSQL := STRSQL || w_deposito || '.plano_pgto c ';
        END IF;
	STRSQL := STRSQL || ' Where ';
	STRSQL := STRSQL || ' a.pc_desconto > 0 and ';
	STRSQL := STRSQL || ' b.cod_dpk in (61521,61564,61523, ';
	STRSQL := STRSQL || ' 64098,61518) and ';
	STRSQL := STRSQL || ' b.qtd_atendida <> 0 and ';
	STRSQL := STRSQL || ' b.situacao=0 and ';
	STRSQL := STRSQL || ' c.prazo_medio > 7 and ';
	STRSQL := STRSQL || ' a.cod_plano = c.cod_plano and ';
	STRSQL := STRSQL || ' b.seq_pedido = ' || PM_SEQPED || ' and ';
	STRSQL := STRSQL || ' b.num_pedido = ' || PM_NUMPED  || ' and ';
	STRSQL := STRSQL || ' b.cod_loja   = ' || PM_CODLOJA || ' and ';
	STRSQL := STRSQL || ' a.seq_pedido = b.seq_pedido and ';
	STRSQL := STRSQL || ' a.num_pedido = b.num_pedido and ';
	STRSQL := STRSQL || ' a.cod_loja=b.cod_loja) A, ';
	STRSQL := STRSQL || '  (select count(*) total_texaco ';
        IF (PM_TP_BANCO = 'M' and PM_TP_BANCO_DESTINO = 'M') OR (PM_CODLOJA = PM_LOJA_DEFAULT) THEN
	  STRSQL := STRSQL || ' from producao.itpednota_venda b, ';
          STRSQL := STRSQL || '      producao.pednota_venda a, ';
          STRSQL := STRSQL || '      producao.plano_pgto c ';
        ELSE
	  STRSQL := STRSQL || ' from ' || w_deposito || '.itpednota_venda b, ';
          STRSQL := STRSQL ||   w_deposito || '.pednota_venda a, ';
          STRSQL := STRSQL ||   w_deposito || '.plano_pgto c ';
        END IF;
	STRSQL := STRSQL || '  Where ';
	STRSQL := STRSQL || '  a.pc_desconto > 3 and ';
	STRSQL := STRSQL || '  b.cod_dpk in (61521,61564,61523, ';
	STRSQL := STRSQL || '  64098,61518) and ';
	STRSQL := STRSQL || '  b.qtd_atendida <> 0 and ';
	STRSQL := STRSQL || '  b.situacao=0 and ';
	STRSQL := STRSQL || '  c.prazo_medio <= 7 and ';
	STRSQL := STRSQL || '  a.cod_plano = c.cod_plano and ';
	STRSQL := STRSQL || '  b.seq_pedido = '|| PM_SEQPED || ' and ';
	STRSQL := STRSQL || '  b.num_pedido = '||  PM_NUMPED || ' and ';
	STRSQL := STRSQL || '  b.cod_loja   = '|| PM_CODLOJA || ' and ';
	STRSQL := STRSQL || '  a.seq_pedido = b.seq_pedido and ';
	STRSQL := STRSQL || '  a.num_pedido = b.num_pedido and ';
 	STRSQL := STRSQL || '  a.cod_loja=b.cod_loja) B ';

  		PM_CODERRO := 0;

  		-- Abrindo Cursor
  		OPEN PM_CURSOR1 FOR STRSQL ;

  	EXCEPTION
  		WHEN OTHERS THEN
  		ROLLBACK;
  		PM_CODERRO := SQLCODE;
  		PM_TXTERRO := SQLERRM;

	END pr_CON_TEXACO;

 PROCEDURE PR_SENHA(p_cursor    IN OUT tp_cursor,
                     p_login     IN     VARCHAR2,
                     p_senha     IN     VARCHAR2,
                     p_erro     OUT    NUMBER)
  IS

  BEGIN

    p_erro := 0;


    --Abrindo Cursor
    OPEN p_cursor FOR Select cod_usuario,nome_usuario
 		      from helpdesk.usuario a
 		       where
 			 PRODUCAO.DESCRIPTOGRAFA_SENHA(SENHA) = p_senha AND
 			 upper(login) = upper(p_login);


    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;


  END;

  PROCEDURE PR_RESPONSAVEL(p_cursor    IN OUT tp_cursor,
                      	   p_erro     OUT     NUMBER)

     IS

      BEGIN

        p_erro := 0;


        --Abrindo Cursor
        OPEN p_cursor FOR select LTRIM(TO_CHAR(COD_USUARIO,'00000'),5) COD,NOME_USUARIO
  		from helpdesk.usuario
  		where ((tp_usuario IN ('G', 'T','C','D')
  		AND FL_ATIVO='S') OR
       		COD_USUARIO IN (346,187,200))
  		AND cod_usuario  not in (358,375,161,64,308,579,166)
  		ORDER BY NOME_USUARIO;


       /* select LTRIM(TO_CHAR(COD_USUARIO,'00000'),5) COD,NOME_USUARIO
  		from helpdesk.usuario
  		where tp_usuario IN ('G', 'T','C')
  		AND FL_ATIVO='S'
  		AND cod_usuario  not in (358,375,161,346,64,308,187,579)
  		ORDER BY NOME_USUARIO;*/



        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;


  END;


  PROCEDURE PR_ATUALIZA_LOG(p_cod_loja     IN     pednota_venda.cod_loja%TYPE,
                     	   p_num_pedido    IN     pednota_venda.num_pedido%TYPE,
                    	   p_seq_pedido    IN     pednota_venda.seq_pedido%TYPE,
                    	   p_dt_pedido     IN     pednota_venda.dt_pedido%TYPE,
                    	   p_fl_lib        IN     VARCHAR2,
                    	   p_cod_usuario   IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_cod_usuario_r IN     helpdesk.usuario.cod_usuario%TYPE,
                    	   p_erro          OUT    NUMBER)

  IS

   BEGIN

     p_erro := 0;


     Update producao.r_pedido_politica set
       fl_liberacao = p_fl_lib,
       dt_liberacao = sysdate,
       cod_usuario_politica = p_cod_usuario,
       cod_usuario_responsavel = p_cod_usuario_r
       where seq_pedido = p_seq_pedido and
         num_pedido = p_num_pedido and
         cod_loja= p_cod_loja;
     IF SQL%NOTFOUND THEN
       Insert into producao.r_pedido_politica(cod_loja,num_pedido,
         seq_pedido,dt_pedido,fl_liberacao,dt_liberacao,cod_usuario_politica,
         cod_usuario_responsavel) values(p_cod_loja,p_num_pedido,
         p_seq_pedido,p_dt_pedido,p_fl_lib,sysdate,p_cod_usuario,p_cod_usuario_r);

     END IF;
     COMMIT;




     EXCEPTION
     WHEN OTHERS THEN
     p_erro := SQLCODE;


  END;

PROCEDURE pr_email_politica
	(p_cod_loja              IN     pednota_venda.cod_loja%TYPE,
        p_num_pedido             IN     pednota_venda.num_pedido%TYPE,
        p_seq_pedido             IN     pednota_venda.seq_pedido%TYPE,
        p_cod_cliente            IN     pednota_venda.cod_cliente%TYPE,
        p_nome_cliente           IN     cliente.nome_cliente%TYPE,
        p_vl_contabil            IN     pednota_venda.vl_contabil%TYPE,
        p_cod_usuario_politica   IN     NUMBER,
        p_cod_usuario            IN     NUMBER,
        p_mg_cuema               IN     NUMBER,
        p_mg_repos               IN     NUMBER)

IS

  w_pedidos	   varchar2(32767) ;
  w_margem	   varchar2(32767) ;
  w_deposito       VARCHAR2(30) ;
  w_email_aut             varchar2(100) ;
  w_nome_usuario_aut      VARCHAR2(100) ;
  w_email_politica        varchar2(100) ;
  w_nome_usuario_politica varchar2(100) ;
  w_email_gerente_nacional varchar2(100) ;
  w_email_gerente_geral    varchar2(100) ;

CURSOR verifica0 IS
 Select a.e_mail,a.nome_usuario,
   d.e_mail email_gerente_nacional
   from v_cadastro_usuario a,
   filial b,
   r_gerente_nacional c,
   v_cadastro_usuario d
   Where a.cod_usuario = p_cod_usuario and
   c.cod_gerente = d.cod_repres(+) and
   decode(b.cod_regional,0,b.cod_filial,b.cod_regional) = c.cod_regional(+) and
   a.cod_filial = b.cod_filial(+);

CURSOR verifica1 IS
 Select e_mail,nome_usuario
   from v_cadastro_usuario
   Where cod_usuario = p_cod_usuario_politica;

CURSOR verifica2 IS
 select e.e_mail email_gerente_nacional
   from r_clie_repres a,
   representante b,
   filial c,
   r_gerente_nacional d,
   v_cadastro_usuario e
   where
   a.cod_cliente = p_cod_cliente and
   d.cod_gerente = e.cod_repres and
   decode(c.cod_regional,0,c.cod_filial,c.cod_regional) = d.cod_regional(+) and
   b.cod_filial = c.cod_filial and
   a.cod_repres=b.cod_repres ;

BEGIN



w_email_gerente_geral := 'heliojr@dpk.com.br';
IF p_cod_usuario <> 0 THEN
  OPEN verifica0;
   FETCH verifica0 INTO w_email_aut,w_nome_usuario_aut,
   w_email_gerente_nacional ;
   IF verifica0%NOTFOUND THEN
      w_email_aut := '';
      w_nome_usuario_aut := '';
      w_email_gerente_nacional := '';
   END IF;
  CLOSE verifica0;
END IF;

OPEN verifica1;
   FETCH verifica1 INTO w_email_politica,w_nome_usuario_politica;
   IF verifica1%NOTFOUND THEN
      w_email_politica := '';
      w_nome_usuario_politica := '';
   END IF;
CLOSE verifica1;

IF p_cod_usuario = 0 or w_email_gerente_nacional = '' THEN
  FOR cont IN verifica2 LOOP
    w_email_gerente_nacional := cont.email_gerente_nacional;
  END LOOP;

END IF;



  w_pedidos :=  'Pedido Lib. Politica Comercial     ' || TO_CHAR(sysdate,'DD/MM/RR HH24:MI');
  w_pedidos := w_pedidos || chr(10);
  IF p_cod_usuario > 0 THEN
    w_pedidos := w_pedidos || 'Autorizado por: ' || w_nome_usuario_aut;
  END IF;
  w_pedidos := w_pedidos || chr(10);

  w_pedidos := w_pedidos || 'Liberado por: ' || w_nome_usuario_politica;

  w_pedidos := w_pedidos || chr(10);
  w_pedidos := w_pedidos || chr(10);

  w_pedidos := w_pedidos || RPAD('DEPOSITO',15) ;
  w_pedidos := w_pedidos || RPAD('PEDIDO',12);
  w_pedidos := w_pedidos || rpad('CLIENTE ',40);
  w_pedidos := w_pedidos || rpad('MG.CUEMA ',10);
  w_pedidos := w_pedidos || rpad('MG.REPOS',10);
  w_pedidos := w_pedidos || rpad('VL.CONTABIL',15);
  w_pedidos := w_pedidos || chr(10);

  w_pedidos := w_pedidos || RPAD(p_cod_loja,15) ;
  w_pedidos := w_pedidos || RPAD(p_num_pedido || '-' || p_seq_pedido,12);
  w_pedidos := w_pedidos ||  rpad(p_cod_cliente || '-' || p_nome_cliente,40) ;
  w_pedidos := w_pedidos ||  rpad(p_mg_cuema,10) ;
  w_pedidos := w_pedidos ||  rpad(p_mg_repos,10) ;
  w_pedidos := w_pedidos ||  rpad(p_vl_contabil,10) ;

  w_pedidos := w_pedidos || chr(10);


  IF (p_mg_cuema < 5 or p_mg_repos < 5) AND p_cod_usuario > 0 THEN

       -- ENVIA EMAIL PARA ZEZE, MARCELO, FRED E USUARIO QUE AUTORIZOU A LIBERA��O
     --  INTRANET.ENVIA_EMAIL@LNK_IDPK('maricicp@maxxipel.com.br','MARGEM < 5%',w_pedidos);
     --  INTRANET.ENVIA_EMAIL@LNK_IDPK(w_email_aut,'MARGEM < 5%',w_pedidos);
 --      IF p_cod_usuario <> 0 and w_email_aut <> '' THEN
         INTRANET.ENVIA_EMAIL@LNK_IDPK(w_email_aut,'MARGEM < 5%',w_pedidos);
 --      END IF;
  ELSIF p_mg_cuema > 5 AND p_mg_repos > 5 AND p_cod_usuario > 0 THEN

       -- SOMENTE PARA USUARIO QUE AUTORIZOU A LIBERACAO
     --  INTRANET.ENVIA_EMAIL@LNK_IDPK('maricicp@maxxipel.com.br','PEDIDO LIB.POLITICA',w_pedidos);
      INTRANET.ENVIA_EMAIL@LNK_IDPK(w_email_aut,'PEDIDO LIB.POLITICA',w_pedidos);
  END IF;



END;

Procedure Pr_Select_Loja (
                            Pm_Cursor IN OUT TP_CURSOR
                           ) is
     BEGIN
         OPEN PM_CURSOR FOR
           SELECT a.cod_loja,
                  a.nome_fantasia
           FROM   loja a,
                  deposito_visao b
           WHERE  a.cod_loja=b.cod_loja
           AND    nome_programa='VDA060'
           Order by a.cod_loja;

  End;


  Procedure Pr_Tipo_Banco_CD (Pm_Cursor   IN OUT TP_CURSOR,
			      pm_cod_loja NUMBER ) IS

   BEGIN

           OPEN PM_CURSOR FOR
             SELECT tp_banco
             FROM producao.tipo_banco_cd
             WHERE cod_loja = pm_cod_loja;


  End;





End PCK_VDA060;
---------------




/


