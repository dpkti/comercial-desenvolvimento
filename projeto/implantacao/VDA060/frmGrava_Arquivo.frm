VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmGrava_Arquivo 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Gravar Arquivo"
   ClientHeight    =   4215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3825
   Icon            =   "frmGrava_Arquivo.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   281
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   255
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   2895
      Left            =   45
      ScaleHeight     =   2895
      ScaleWidth      =   3735
      TabIndex        =   4
      Top             =   945
      Width           =   3735
      Begin VB.TextBox txtNome_Arquivo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1710
         MaxLength       =   8
         TabIndex        =   7
         Top             =   135
         Width           =   1860
      End
      Begin VB.DirListBox dirList 
         Height          =   1440
         Left            =   375
         TabIndex        =   6
         Top             =   1095
         Width           =   3030
      End
      Begin VB.DriveListBox drvList 
         Height          =   315
         Left            =   375
         TabIndex        =   5
         Top             =   735
         Width           =   3030
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Nome do Arquivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   8
         Top             =   180
         Width           =   1545
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   3885
      Width           =   3825
      _ExtentX        =   6747
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6694
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   4785
      _ExtentX        =   8440
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrava_Arquivo.frx":23D2
      PICN            =   "frmGrava_Arquivo.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSearch 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmGrava_Arquivo.frx":30C8
      PICN            =   "frmGrava_Arquivo.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmGrava_Arquivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim SearchFlag As Integer   ' Used as flag for cancel and other operations.

Private Sub cmdSearch_Click()
    Dim nome_arquivo As String
    Dim RESPOSTA As String

    On Error GoTo Trata_erro

    If txtNome_Arquivo = "" Then
        MsgBox "Selecione um nome para o arquivo", vbExclamation, "Aten��o"
        txtNome_Arquivo.SetFocus
        Exit Sub
    End If
    Screen.MousePointer = 11
    dirList.Path = dirList.List(dirList.ListIndex)
    nome_arquivo = dirList.Path & "\" & txtNome_Arquivo & ".txt"

    FileCopy "C:\TEMP.TXT", nome_arquivo

    Screen.MousePointer = 0
    MsgBox "Arquivo: " & nome_arquivo & " gravado com sucesso.", vbExclamation, "Aten��o"

    Unload Me
    Exit Sub

Trata_erro:
    If Err = 55 Then
        RESPOSTA = MsgBox("Substituir " & txtNome_Arquivo & " j� existente ?", vbYesNo, "Aten��o")
        If RESPOSTA = vbYes Then
            'Kill nome_arquivo
            Resume
        Else
            Screen.MousePointer = 0
            Exit Sub
        End If
    ElseIf Err = 76 Then
        nome_arquivo = dirList.Path & txtNome_Arquivo & ".txt"

        FileCopy "C:\TEMP.TXT", nome_arquivo

        Screen.MousePointer = 0
        MsgBox "Arquivo: " & nome_arquivo & " gravado com sucesso.", vbExclamation, "Aten��o"

        Unload Me
        Exit Sub
    ElseIf Err = 70 Then
        MsgBox "Este arquivo esta aberto por outro aplicativo, feche para poder salva-lo", vbExclamation, "Aten�ao"
        Resume
    End If


End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Function DirDiver(NewPath As String, DirCount As Integer, BackUp As String) As Integer

End Function

Private Sub DirList_LostFocus()
    dirList.Path = dirList.List(dirList.ListIndex)
End Sub

Private Sub DrvList_Change()
    On Error GoTo DriveHandler
    dirList.Path = drvList.Drive
    Exit Sub

DriveHandler:
    drvList.Drive = dirList.Path
    Exit Sub
End Sub

Private Sub Form_Load()

    Me.Top = 0
    Me.Left = 0

End Sub

Private Sub txtNome_Arquivo_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
End Sub

