VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "mswinsck.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmSobre 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Sobre"
   ClientHeight    =   3660
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5235
   ClipControls    =   0   'False
   Icon            =   "frmSobre.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   244
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   349
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Analista"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   915
      Left            =   45
      TabIndex        =   5
      Top             =   675
      Width           =   5145
      Begin VB.Label lblResponsavel 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   285
         Left            =   1395
         TabIndex        =   9
         ToolTipText     =   "Analista Respons�vel"
         Top             =   225
         Width           =   3660
      End
      Begin VB.Label lblBackup 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   900
         TabIndex        =   8
         ToolTipText     =   "Analista Respons�vel"
         Top             =   585
         Width           =   4155
      End
      Begin VB.Label lbl1 
         Appearance      =   0  'Flat
         Caption         =   "Respons�vel:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   135
         TabIndex        =   7
         ToolTipText     =   "Analista Respons�vel"
         Top             =   225
         Width           =   1320
      End
      Begin VB.Label lbl2 
         Appearance      =   0  'Flat
         Caption         =   "Backup:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         ToolTipText     =   "Analista Respons�vel"
         Top             =   585
         Width           =   780
      End
   End
   Begin MSWinsockLib.Winsock WinSock 
      Left            =   45
      Top             =   2025
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   555
      Left            =   45
      TabIndex        =   0
      Top             =   45
      Width           =   5145
      _ExtentX        =   9075
      _ExtentY        =   979
      Gradient        =   12
      Color1          =   0
      BackColor       =   -2147483633
      Begin VB.Label lblNomeSistema 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   435
         Left            =   60
         TabIndex        =   1
         ToolTipText     =   "Nome do Sistema"
         Top             =   60
         Width           =   5040
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "by Maxxipel"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   45
      TabIndex        =   4
      Top             =   3465
      Width           =   915
   End
   Begin VB.Image Image1 
      Height          =   1725
      Left            =   1485
      Picture         =   "frmSobre.frx":23D2
      Stretch         =   -1  'True
      Top             =   1935
      Width           =   2040
   End
   Begin VB.Label lblVersao 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   3600
      TabIndex        =   3
      Top             =   3465
      Width           =   1590
   End
   Begin VB.Label lblIp 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Caption         =   "Seu I.P. para acesso remoto �: "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   45
      TabIndex        =   2
      Top             =   1665
      Width           =   5145
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, UCase(App.Title))

    Call DefinirTelaSobre

    lblIp = lblIp + WinSock.LocalIP

    lblVersao = "Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision

End Sub

