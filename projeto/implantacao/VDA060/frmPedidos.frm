VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmPedidos 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Pedidos"
   ClientHeight    =   9345
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   16590
   ForeColor       =   &H00000000&
   Icon            =   "frmPedidos.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   623
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   1106
   Begin VB.ComboBox cboTimer 
      Height          =   315
      ItemData        =   "frmPedidos.frx":23D2
      Left            =   4800
      List            =   "frmPedidos.frx":23E2
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   300
      Width           =   1095
   End
   Begin VB.Timer Timer 
      Interval        =   10000
      Left            =   8640
      Top             =   240
   End
   Begin VB.Frame fraCd 
      Caption         =   "Filiais"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   4815
      Left            =   11520
      TabIndex        =   7
      Top             =   720
      Visible         =   0   'False
      Width           =   4215
      Begin VB.CheckBox chkTodas 
         Caption         =   "Todas"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   4440
         Width           =   1095
      End
      Begin Bot�o.cmd cmdOK 
         Height          =   375
         Left            =   3225
         TabIndex        =   8
         Top             =   4380
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Ok"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPedidos.frx":23F5
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdFecharImp 
         Height          =   375
         Left            =   2280
         TabIndex        =   9
         Top             =   4380
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Fechar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmPedidos.frx":2411
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvLoja 
         Height          =   4035
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   3960
         _ExtentX        =   6985
         _ExtentY        =   7117
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   0
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "C�d."
            Object.Width           =   1411
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Filial"
            Object.Width           =   5115
         EndProperty
      End
   End
   Begin VB.TextBox lblPesquisa 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   900
      Visible         =   0   'False
      Width           =   8340
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   9015
      Width           =   16590
      _ExtentX        =   29263
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   29210
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   16425
      _ExtentX        =   28972
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPedidos.frx":242D
      PICN            =   "frmPedidos.frx":2449
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd sscmdPesquisa 
      Height          =   675
      Left            =   720
      TabIndex        =   5
      ToolTipText     =   "Consultar"
      Top             =   0
      Width           =   735
      _ExtentX        =   1296
      _ExtentY        =   1191
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPedidos.frx":3123
      PICN            =   "frmPedidos.frx":313F
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid grdPedido 
      Height          =   7635
      Left            =   45
      TabIndex        =   6
      Top             =   1305
      Width           =   16410
      _ExtentX        =   28945
      _ExtentY        =   13467
      _Version        =   393216
      Cols            =   10
      FixedCols       =   0
      BackColor       =   16777215
      ForeColor       =   8388608
      ForeColorFixed  =   8388608
      BackColorSel    =   16777215
      ForeColorSel    =   8388608
      Enabled         =   -1  'True
      FocusRect       =   0
      HighLight       =   2
      SelectionMode   =   1
      AllowUserResizing=   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image imgAnalise 
      Height          =   240
      Left            =   9600
      Picture         =   "frmPedidos.frx":3E19
      Top             =   360
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "Tempo de Atualiza��o em segundos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1680
      TabIndex        =   12
      Top             =   360
      Width           =   3060
   End
   Begin VB.Label lblPesq 
      Appearance      =   0  'Flat
      Caption         =   "Procurando por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   90
      TabIndex        =   4
      Top             =   945
      Visible         =   0   'False
      Width           =   1500
   End
   Begin VB.Menu mnuMenu 
      Caption         =   "Ordenar"
      Visible         =   0   'False
      Begin VB.Menu mnuSort 
         Caption         =   "Data Pol�tica"
         Index           =   0
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Data Digita��o"
         Index           =   1
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Filial"
         Index           =   2
      End
      Begin VB.Menu mnuSort 
         Caption         =   "C�d."
         Index           =   3
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Nome do Cliente"
         Index           =   4
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Transportadora"
         Index           =   5
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Pgto."
         Index           =   6
      End
      Begin VB.Menu mnuSort 
         Caption         =   "CD"
         Index           =   7
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Pedido"
         Index           =   8
      End
      Begin VB.Menu mnuSort 
         Caption         =   "Seq."
         Index           =   9
      End
   End
End
Attribute VB_Name = "frmPedidos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPedidos
' Author    : c.samuel.oliveira
' Date      : 24/02/16
' Purpose   : TI-4211
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmPedidos
' Author    : c.samuel.oliveira
' Date      : 15/02/16
' Purpose   : TI-4160
'---------------------------------------------------------------------------------------

Dim strPesquisa As String
Dim strFilial As String 'TI-4211
Private Sub cmdVoltar_Click()

    strPesquisa = ""

    Unload Me

End Sub

Private Sub Form_Load()

    Me.Top = 1500
    Me.Left = 200
    
    'TI-4211
    cboTimer.ListIndex = 1
    CarregarLoja
    'FIM TI-4211
    
    sscmdPesquisa_Click
    
End Sub

Private Sub grdPedido_DblClick()

    Dim ss As Object 'TI-4160
    Dim vErro As String 'TI-4160
    Dim vSql As String 'TI-4160

    On Error GoTo TrataErro
    
    'TI-4211
    Screen.MousePointer = 11
    grdPedido.Col = 8
    lngNUM_PEDIDO = grdPedido.Text
    txtResposta = grdPedido.Text
    grdPedido.Col = 9
    lngSEQ_PEDIDO = grdPedido.Text
    grdPedido.Col = 7
    lngCod_Loja = Val(Left(grdPedido.Text, 2))
    
    Timer.Enabled = False
    
    'BUSCAR NOVA UF_ORIGEM
    strSQL = "SELECT cod_uf FROM loja a, cidade b WHERE a.cod_cidade =b.cod_cidade and COD_loja = " & lngCod_Loja
    vBanco.Parameters.Remove "PM_SQL"
    vBanco.Parameters.Add "PM_SQL", strSQL, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2


    vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    If Not vObjOracle.BOF And Not vObjOracle.EOF Then
        uf_origem = vObjOracle!cod_uf
    End If

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vSql = "PRODUCAO.pck_vda060.pr_TIPO_BANCO_CD(:PM_CURSOR1,:PM_CODLOJA)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF Then
        strTipo_CD_Pedido = "U"
    Else
        strTipo_CD_Pedido = vObjOracle!tp_banco
    End If
    'FIM TI-4211
    
    'TI-4160
    SQL = "Select * "
    SQL = SQL & " from VENDAS.CONTROLE" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "")
    SQL = SQL & " where num_PEDIDO =" & lngNUM_PEDIDO
    SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
    SQL = SQL & " AND COD_LOJA = " & lngCod_Loja

    vBanco.Parameters.Remove "PM_SQL"
    vBanco.Parameters.Add "PM_SQL", SQL, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
    Set ss = vBanco.Parameters("PM_CURSOR1").Value
    FreeLocks

    If ss.EOF Then
        SQL = "insert into vendas.controle" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & " values("
        SQL = SQL & "VENDAS.SEQ_CONTROLE.NEXTVAL" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & "," & lngCod_Loja & "," & lngNUM_PEDIDO
        SQL = SQL & "," & lngSEQ_PEDIDO & ",'" & sCOD_VEND & "',SYSDATE)"
        fAtualizaVerificaErro (SQL)
        FreeLocks
    Else
        If sCOD_VEND <> ss!Usuario Then
            MsgBox "O pedido " & lngNUM_PEDIDO & " est� sendo analisado pelo usu�rio: " & ss!Usuario & " - " & ss!DT_INCLUSAO, vbExclamation, "Aten��o"
            'Exit Sub 'TI-4211 TI-4364 - 11/03/2016
        Else
            SQL = "DELETE FROM  vendas.controle " & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "")
            SQL = SQL & " WHERE NUM_PEDIDO = " & lngNUM_PEDIDO
            SQL = SQL & "   AND   SEQ_PEDIDO = " & lngSEQ_PEDIDO
            SQL = SQL & "   AND   COD_LOJA   = " & lngCod_Loja
            'fAtualizaVerificaErro (SQL) TI-4211
            FreeLocks
        End If

    End If
    Screen.MousePointer = 0
    'FIM TI-4160
    frmPedidos.Hide
    DoEvents
    frmVisPedido.Show 1
    DoEvents
    frmPedidos.Show 1

    '  If Opcao = 1 Then
    '      Call cmdRepr_Click
    '  Else
    '      Call cmdTlmk_Click
    '  End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    ElseIf Err = 364 Then
        Resume Next
    ElseIf Err = 440 Then 'TI-4211
        strTipo_CD_Pedido = "U" 'TI-4211
        Resume Next 'TI-4211
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
    Screen.MousePointer = 0 'TI-4211
End Sub

Private Sub grdPedido_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
    Dim ss As Object 'TI-4160
    Dim vErro As String 'TI-4160
    Dim vSql As String 'TI-4160


    On Error GoTo TrataErro
    
    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdPedido.Col = 2

    If KeyAscii = 13 Then

        Screen.MousePointer = 11
        grdPedido.Col = 2
        lngNUM_PEDIDO = grdPedido.Text
        txtResposta = grdPedido.Text
        grdPedido.Col = 3
        lngSEQ_PEDIDO = grdPedido.Text
        grdPedido.Col = 1
        lngCod_Loja = Mid(deposito_default, 1, 3)

        'TI-4160
        SQL = "Select * "
        SQL = SQL & " from vendas.CONTROLE" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "")
        SQL = SQL & " where num_PEDIDO =" & lngNUM_PEDIDO
        SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
        SQL = SQL & " AND COD_LOJA = " & lngCod_Loja

        vBanco.Parameters.Remove "PM_SQL"
        vBanco.Parameters.Add "PM_SQL", SQL, 1
    
        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
    
        vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"
    
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            
        Set ss = vBanco.Parameters("PM_CURSOR1").Value
        FreeLocks

        If ss.EOF Then
            SQL = "insert into vendas.controle" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & " values("
            SQL = SQL & "VENDAS.SEQ_CONTROLE.NEXTVAL" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & "," & lngCod_Loja & "," & lngNUM_PEDIDO
            SQL = SQL & "," & lngSEQ_PEDIDO & ",'" & sCOD_VEND & "',SYSDATE)"
            fAtualizaVerificaErro (SQL)
            FreeLocks
        Else
            MsgBox "O pedido " & lngNUM_PEDIDO & " est� sendo analisado pelo usu�rio: " & ss!Usuario & " - " & ss!DT_INCLUSAO, vbExclamation, "Aten��o"
            Screen.MousePointer = 0
            Exit Sub
        End If
        'FIM TI-4160

        frmPedidos.Hide
        frmVisPedido.Show 1
        'TI-4211
'        If Opcao = 1 Then
'            Call cmdRepr_Click
'        Else
'            Call cmdTlmk_Click
'        End If
        'FIM TI-4211
        'frmPedidos.Show 1


        Exit Sub

    ElseIf KeyAscii = 8 Then    'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Text = strPesquisa
                lblPesquisa.Visible = False
                lblPesq.Visible = False
                grdPedido.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Text = strPesquisa
                DoEvents
                With grdPedido
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If

    ElseIf KeyAscii = 27 Then

        strPesquisa = ""
        lblPesquisa.Text = strPesquisa
        lblPesquisa.Visible = False
        lblPesq.Visible = False
        grdPedido.Row = 1
        grdPedido.Col = 1
        SendKeys "{LEFT}{RIGHT}"

    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = vVB_Generica_001.Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesq.Visible = True
                    lblPesquisa.Text = strPesquisa
                    DoEvents
                    With grdPedido
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                            .Col = 1
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdPedido.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Text = strPesquisa
                            Beep
                        End If
                        .Row = iLinha
                        .Col = 1
                        SendKeys "{LEFT}+{END}"
                    End With

                End If
            End If
        Else
            Beep
        End If
    End If

    'mouse
    Screen.MousePointer = vbDefault
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub

Private Sub sscmdPesquisa_Click()

    On Error GoTo Trata_erro
    
    'TI-4211
    Dim j As Integer, i As Integer
    Dim vetor As Variant
    
    Timer.Enabled = False
    
    For j = 1 To lsvLoja.ListItems.count
         lsvLoja.ListItems(j).Checked = False
         lsvLoja.ListItems(j).Selected = False
    Next j
       
    vetor = Split(strFilial, ",")

    fraCd.Top = 48
    fraCd.Left = 48
    j = 0
    For j = 1 To lsvLoja.ListItems.count
        For i = LBound(vetor) To UBound(vetor)
            If Val(lsvLoja.ListItems(j)) = vetor(i) Then
                lsvLoja.ListItems(j).Checked = True
            End If
        Next
    Next j
    
    fraCd.Visible = True
    'FIM TI-4211
    
    Exit Sub

Trata_erro:
    If Err = 440 Then
        Resume Next
    End If
    '   If Err <> 0 Then
    '    MsgBox "Sem comunica��o no momento com o Cd escolhido, tente mais tarde", vbInformation, "Aten��o"
    '    Screen.MousePointer = 0
    '    Exit Sub
    '  End If
End Sub
'TI-4211
Private Sub CarregarPedidos()

On Error GoTo Trata_erro
    Screen.MousePointer = 11
    
    Dim i As Long
    
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", strFilial, 1

    vBanco.Parameters.Remove "PM_TPCONS"
    vBanco.Parameters.Add "PM_TPCONS", "T", 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_PEDIDOSREPR(:PM_CODLOJA,:PM_TPCONS,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco, :pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF Then
        MsgBox "N�o existe pedido bloqueado para telemarketing", vbExclamation, "Aten��o"
        Screen.MousePointer = vbDefault
    Else
        
        MontarGrid
        
        'carrega dados
        With grdPedido
            .Rows = vObjOracle.RecordCount + 1
    
            For i = 1 To .Rows - 1
                .Row = i
    
                .Col = 0
                .ColAlignment(0) = 0
                .Text = vObjOracle("dt_politica")
                .Col = 1
                .ColAlignment(1) = 0
                If vObjOracle("usuario") = "N" Then
                  Set .CellPicture = LoadPicture()
                  .Text = vObjOracle("dt_dig")
                Else
                  Set .CellPicture = frmPedidos.imgAnalise
                  .Text = "  " & vObjOracle("dt_dig") & " - " & vObjOracle("usuario")
                End If
                .Col = 2
                .Text = vObjOracle("nome_filial")
                .Col = 3
                .Text = vObjOracle("cod_cliente")
                .Col = 4
                .Text = vObjOracle("nome_cliente")
                .Col = 5
                .Text = vObjOracle("nome_transp")
                .Col = 6
                .ColAlignment(6) = 0
                .Text = vObjOracle("Pgto")
                .Col = 7
                .ColAlignment(7) = 0
                .Text = vObjOracle("CD")
                .Col = 8
                .Text = vObjOracle("num_pedido")
                .Col = 9
                .Text = vObjOracle("seq_pedido")
                
                vObjOracle.MoveNext
            Next
            .Row = 1
        End With

        frmPedidos.grdPedido.Row = 1
        frmPedidos.grdPedido.Col = 1
        'mouse
        Screen.MousePointer = vbDefault

        frmPedidos.grdPedido.Visible = True
        
    End If

    Exit Sub

Trata_erro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
    Screen.MousePointer = vbDefault
End Sub
Private Sub MontarGrid()

On Error GoTo Trata_erro

        grdPedido.Row = 0
        grdPedido.Col = 0
    
        grdPedido.ColWidth(0) = 1600
        grdPedido.FixedAlignment(0) = 2
        grdPedido.Text = "Data Pol�tica"
        
        grdPedido.Col = 1
        grdPedido.ColWidth(1) = 1600
        grdPedido.FixedAlignment(1) = 2
        grdPedido.Text = "Data Digita��o"
        
        grdPedido.Col = 2
        grdPedido.ColWidth(2) = 1700
        grdPedido.FixedAlignment(2) = 2
        grdPedido.Text = "Filial"
        
        grdPedido.Col = 3
        grdPedido.ColWidth(3) = 1000
        grdPedido.FixedAlignment(3) = 2
        grdPedido.Text = "C�d."
        
        grdPedido.Col = 4
        grdPedido.ColWidth(4) = 3300
        grdPedido.FixedAlignment(4) = 2
        grdPedido.Text = "Nome do Cliente"
        
        grdPedido.Col = 5
        grdPedido.ColWidth(5) = 2000
        grdPedido.FixedAlignment(5) = 2
        grdPedido.Text = "Transportadora"
        
        grdPedido.Col = 6
        grdPedido.ColWidth(6) = 2000
        grdPedido.FixedAlignment(6) = 2
        grdPedido.Text = "Pgto."
        
        grdPedido.Col = 7
        grdPedido.ColWidth(7) = 1400
        grdPedido.FixedAlignment(7) = 2
        grdPedido.Text = "CD"
        
        grdPedido.Col = 8
        grdPedido.ColWidth(8) = 1000
        grdPedido.FixedAlignment(8) = 2
        grdPedido.Text = "Pedido"
                
        grdPedido.Col = 9
        grdPedido.ColWidth(9) = 500
        grdPedido.FixedAlignment(9) = 2
        grdPedido.Text = "Seq."

    Exit Sub

Trata_erro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
    Screen.MousePointer = vbDefault
End Sub
Private Sub grdPedido_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button = 2 Then
        PopupMenu mnuMenu
    End If
End Sub
Private Sub mnuSort_Click(index As Integer)
    grdPedido.Col = index
    grdPedido.Sort = flexSortGenericAscending
End Sub
Private Sub cmdFecharImp_Click()
    fraCd.Visible = False
    Timer.Enabled = True
End Sub
Private Sub cmdOK_Click()
    Dim j As Integer
    Dim count As Integer

    On Error GoTo Trata_erro
    
    For j = 1 To lsvLoja.ListItems.count
        If lsvLoja.ListItems(j).Checked = True Then
            count = count + 1
        End If
    Next j
    
    If count = 0 Then
        MsgBox "Escolha pelo menos uma Filial para realizar a busca!", vbExclamation, "Aten��o"
        Exit Sub
    End If
    
    strFilial = ""
    For j = 1 To lsvLoja.ListItems.count
        If lsvLoja.ListItems(j).Checked = True Then
            strFilial = strFilial & Val(lsvLoja.ListItems(j)) & ","
        End If
    Next j
    strFilial = Mid(strFilial, 1, Len(strFilial) - 1)
    'MsgBox strFilial
    grdPedido.Visible = False
    CarregarPedidos
    
    fraCd.Visible = False
    
    Timer.Enabled = True
    
    Exit Sub

Trata_erro:
    If Err = 440 Then
        Resume Next
    End If

End Sub
Private Sub CarregarLoja()

On Error GoTo Trata_erro

Dim litem As ListItem

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vSql = "PRODUCAO.pck_vda060.Pr_Sel_Filial(:PM_CURSOR1)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

   lsvLoja.ListItems.Clear
   Do While vObjOracle.EOF = False
      Set litem = Me.lsvLoja.ListItems.Add
      litem = Format(vObjOracle!COD_FILIAL, "000")
      litem.SubItems(1) = vObjOracle!nome_filial
      vObjOracle.MoveNext
   Loop
   
    Exit Sub
    
Trata_erro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub
Private Sub Timer_Timer()

If lblPesquisa.Visible = False And fraCd.Visible = False And Me.Visible = True And Len(Trim(strFilial)) > 0 Then
    grdPedido.Visible = False
    CarregarPedidos
End If

End Sub
Private Sub Form_Activate()
    Timer.Enabled = True
    If fraCd.Visible = False Then CarregarPedidos
End Sub
Private Sub chkTodas_Click()
    On Error GoTo Trata_erro
    
    Dim count As Integer
    Dim i As Integer
    
    count = 0
    
    If chkTodas.Value = vbChecked Then    'select all
    With lsvLoja
        For i = 1 To .ListItems.count
            .ListItems(i).Checked = True
            .ListItems(i).Selected = True
        Next i
        
    End With
    
    Else                                'desel all
    With lsvLoja
        For i = 1 To .ListItems.count
            .ListItems(i).Checked = False
            .ListItems(i).Selected = False
            
       Next i
      End With
    
    End If
    
'    For j = 1 To ListView1.ListItems.count
'        If ListView1.ListItems(j).Checked = True Then
'            count = count + 1
'        End If
'    Next j
'    Text1.Text = count

    Exit Sub
Trata_erro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub
Private Sub cboTimer_Click()
    Timer.Enabled = False
    Debug.Print Timer.Interval
    Timer.Interval = Val(cboTimer.Text & "000")
    Timer.Enabled = True
    Debug.Print Timer.Interval
End Sub
'FIM TI-4211
