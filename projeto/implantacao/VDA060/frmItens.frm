VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmItens 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Itens do Pedido"
   ClientHeight    =   3420
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9975
   Icon            =   "frmItens.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   228
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   665
   Begin VB.TextBox txtQtde 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3375
      MaxLength       =   6
      TabIndex        =   2
      Top             =   990
      Width           =   870
   End
   Begin VB.TextBox txtPrLiquido 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1125
      MaxLength       =   6
      TabIndex        =   10
      Top             =   2655
      Width           =   2850
   End
   Begin VB.TextBox lblDescSuframa 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9135
      Locked          =   -1  'True
      TabIndex        =   26
      Top             =   2250
      Width           =   780
   End
   Begin VB.TextBox lblPcIcm 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7650
      Locked          =   -1  'True
      TabIndex        =   24
      Top             =   2250
      Width           =   735
   End
   Begin VB.TextBox txtUf 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5805
      MaxLength       =   5
      TabIndex        =   9
      Top             =   2250
      Width           =   780
   End
   Begin VB.TextBox txtAdicional 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4455
      MaxLength       =   5
      TabIndex        =   8
      Top             =   2250
      Width           =   735
   End
   Begin VB.TextBox txtPeriodo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2610
      MaxLength       =   5
      TabIndex        =   7
      Top             =   2250
      Width           =   735
   End
   Begin VB.TextBox txtTabela 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1125
      MaxLength       =   6
      TabIndex        =   6
      Top             =   2250
      Width           =   780
   End
   Begin VB.TextBox txtDescricao 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1125
      MaxLength       =   30
      TabIndex        =   5
      Top             =   1845
      Width           =   8790
   End
   Begin VB.TextBox txtCOD_FABRICA 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5400
      MaxLength       =   19
      TabIndex        =   4
      Top             =   1440
      Width           =   2850
   End
   Begin VB.TextBox txtCOD_FORNECEDOR 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3690
      MaxLength       =   3
      TabIndex        =   3
      Top             =   1440
      Width           =   555
   End
   Begin VB.TextBox lblDigito 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2070
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   1440
      Width           =   330
   End
   Begin VB.TextBox txtCOD_DPK 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1125
      MaxLength       =   5
      TabIndex        =   1
      Top             =   1440
      Width           =   870
   End
   Begin VB.TextBox lblNrItens 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1125
      Locked          =   -1  'True
      TabIndex        =   13
      Top             =   990
      Width           =   690
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   3090
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17542
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   11
      Top             =   810
      Width           =   9870
      _ExtentX        =   17410
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   12
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmItens.frx":23D2
      PICN            =   "frmItens.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   29
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmItens.frx":30C8
      PICN            =   "frmItens.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label12 
      Appearance      =   0  'Flat
      Caption         =   "Quantidade:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2295
      TabIndex        =   30
      Top             =   1035
      Width           =   1095
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      Caption         =   "Pr. L�quido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   135
      TabIndex        =   28
      Top             =   2700
      Width           =   1095
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      Caption         =   "% Suf.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   8460
      TabIndex        =   27
      Top             =   2295
      Width           =   735
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      Caption         =   "%Dif. ICMS:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   6660
      TabIndex        =   25
      Top             =   2295
      Width           =   1005
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      Caption         =   "% UF:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5265
      TabIndex        =   23
      Top             =   2295
      Width           =   735
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      Caption         =   "% Adicional:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   3420
      TabIndex        =   22
      Top             =   2295
      Width           =   1005
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      Caption         =   "% Per.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   1935
      TabIndex        =   21
      Top             =   2295
      Width           =   735
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      Caption         =   "Tabela:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   450
      TabIndex        =   20
      Top             =   2295
      Width           =   735
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      Caption         =   "Descri��o:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   225
      TabIndex        =   19
      Top             =   1890
      Width           =   1095
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "Fabricante:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   4455
      TabIndex        =   18
      Top             =   1485
      Width           =   1095
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "Fornecedor:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2610
      TabIndex        =   17
      Top             =   1485
      Width           =   1095
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "C�digo DPK:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   45
      TabIndex        =   15
      Top             =   1485
      Width           =   1095
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "ITEM"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   585
      TabIndex        =   14
      Top             =   1035
      Width           =   690
   End
End
Attribute VB_Name = "frmItens"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public desc_vdr As Boolean

Private Sub cmd1_Click()

    If txtCOD_FORNECEDOR = 127 And Val(txtAdicional) > 0 Then
        MsgBox "O fornecedor 127-PST n�o permite desconto adicional", vbCritical, "Aten��o"
        'txtPeriodo = 0
        txtAdicional = 0
        'txtUf = 0
        DoEvents
    End If


    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_ITEMPED"
    vBanco.Parameters.Add "PM_ITEMPED", lblNrItens.Text, 1

    vBanco.Parameters.Remove "PM_DESC1"
    ' If Val(txtPeriodo.Text) < 0.01 Then
    '     vBanco.Parameters.Add "PM_DESC1", 0, 1
    ' Else
    vBanco.Parameters.Add "PM_DESC1", Val(txtPeriodo.Text), 1
    ' End If

    vBanco.Parameters.Remove "PM_DESC2"
    'If Val(txtUf.Text) < 0.01 Then
    '    vBanco.Parameters.Add "PM_DESC2", 0, 1
    'Else
    vBanco.Parameters.Add "PM_DESC2", Val(txtUf.Text), 1
    'End If

    vBanco.Parameters.Remove "PM_DESC3"
    'If Val(txtAdicional.Text) < 0.01 Then
    '    vBanco.Parameters.Add "PM_DESC3", txtAdicional.Text, 1
    'Else
    vBanco.Parameters.Add "PM_DESC3", Val(txtAdicional.Text), 1
    'End If

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_ACT_DADOSITEMPED(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_ITEMPED,:PM_DESC1,:PM_DESC2,:PM_DESC3,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If Val(vBanco.Parameters("PM_CODERRO").Value) <> 0 Then
        If Val(vBanco.Parameters("PM_CODERRO").Value) = -1438 Then
            MsgBox "Verique o valor, foi digitado um n�mero maior que o permitido", vbCritical, "Aten��o"
            Exit Sub
        End If

        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = vbDefault
        Exit Sub
    End If

    Unload Me
    
    'BCAMPOS - CIT - 21/11/2013
    'POLITICA DE MARGEM - RECALCULO
    'Desabilita bot�o de libera��o se o item foi alterado
    frmVisPedido.Command5.Enabled = False
    

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    Me.Top = 1500
    Me.Left = 200


    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_ITEMPED"
    vBanco.Parameters.Add "PM_ITEMPED", frmVisPedido.Grid2.Text, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_DADOSITEMPED(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_ITEMPED,:PM_CURSOR1,:pm_loja_default, :pm_tp_bco,:pm_tp_bco_ped,  :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value


    frmItens.txtCOD_DPK.Text = vObjOracle!cod_dpk
    frmItens.lblNrItens.Text = vObjOracle!num_item_pedido
    frmItens.txtQtde.Text = vObjOracle!qtd_atendida
    frmItens.txtTabela.Text = vObjOracle!TABELA_VENDA
    frmItens.txtPeriodo.Text = FmtBR(vObjOracle!pc_desc1)
    frmItens.txtAdicional.Text = FmtBR(vObjOracle!pc_desc3)
    frmItens.txtUf.Text = FmtBR(vObjOracle!pc_desc2)
    frmItens.lblPcIcm.Text = FmtBR(vObjOracle!pc_dificm)
    frmItens.lblDescSuframa.Text = FmtBR(vObjOracle!pc_desc_suframa)
    frmItens.txtPrLiquido.Text = Format(FmtBR(vObjOracle!pr_liquido), "######0.00")
    txtCOD_FORNECEDOR.Text = vObjOracle!cod_fornecedor
    If txtCOD_FORNECEDOR.Text = 127 Then
        frmItens.txtPeriodo.Enabled = False
        'liberado conf. solicita��o Ferreira / Helio
        ' em 01/09/04
        frmItens.txtUf.Enabled = True
    Else
        frmItens.txtPeriodo.Enabled = True
        frmItens.txtUf.Enabled = True
    End If
    txtCOD_FABRICA.Text = vObjOracle!cod_fabrica
    txtDescricao.Text = vObjOracle!DESC_ITEM

    'PSERGIO 30/09/13 DESCONTO VDR
    If desc_vdr = True Then
        txtPeriodo.Enabled = False
        txtAdicional.Enabled = False
        txtUf.Enabled = True
        desc_vdr = False
    End If

End Sub

Private Sub txtAdicional_GotFocus()
    txtAdicional.BackColor = Vermelho
End Sub

Private Sub txtAdicional_KeyPress(KeyAscii As Integer)
'    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtAdicional)
End Sub

Private Sub txtAdicional_LostFocus()
    txtAdicional.BackColor = Branco

    If txtTabela.Text <> "" Then
        If Val(txtAdicional.Text) = 0 Then
            txtAdicional.Text = 0#
        Else
            If CDbl(txtAdicional.Text) > 99.99 Or _
               CDbl(txtAdicional.Text) < -99.99 Then
                MsgBox "O desconto adicional n�o pode ser superior a 99.99% e inferior a -99.99%", vbInformation, "Aten��o"
                txtAdicional.SetFocus
                Exit Sub
            End If
        End If
        If lblDescSuframa.Text = "" Then
            lblDescSuframa.Text = 0#
        End If
    End If
    If txtAdicional.Text = "" Then
        txtAdicional.Text = "0"
    End If
End Sub

Private Sub txtCOD_DPK_GotFocus()
    txtCOD_DPK.BackColor = Vermelho
    txtCOD_DPK.DataChanged = False
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCOD_FABRICA_GotFocus()
    txtCOD_FABRICA.BackColor = Vermelho
    txtCOD_FABRICA.DataChanged = False
End Sub

Private Sub txtCOD_FABRICA_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtCOD_FORNECEDOR_GotFocus()
    txtCOD_FORNECEDOR.BackColor = Vermelho
    txtCOD_FORNECEDOR.DataChanged = False
End Sub

Private Sub txtCOD_FORNECEDOR_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtPeriodo_GotFocus()
    txtPeriodo.BackColor = Vermelho
End Sub

Private Sub txtPeriodo_KeyPress(KeyAscii As Integer)
'    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtPeriodo)
End Sub

Private Sub txtPeriodo_LostFocus()
    If Val(txtPeriodo.Text) > 99 Then
        MsgBox "O desconto deve ser < 99", vbInformation, "Aten��o"
        txtPeriodo.SetFocus
        Exit Sub
    End If
    If txtPeriodo.Text = "" Then
        txtPeriodo.Text = "0"
    End If
    txtPeriodo.BackColor = Branco
End Sub

Private Sub txtPrLiquido_GotFocus()
    txtPrLiquido.BackColor = Vermelho
End Sub

Private Sub txtPrLiquido_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtPrLiquido.Text)
End Sub

Private Sub txtPrLiquido_LostFocus()
    txtPrLiquido.BackColor = Branco
    If txtQtde.Text <> "" Then
        If txtPrLiquido.Text = "" Then
            txtPrLiquido.Text = 0
        End If
        If txtPrLiquido.Text <= 0 Then
            MsgBox "Entre com o Pre�o do Produto", vbInformation, "Aten��o"
            txtPrLiquido.SetFocus
            Exit Sub
        End If
    End If
End Sub

Private Sub txtQtde_GotFocus()
    txtQtde.BackColor = Vermelho
End Sub

Private Sub txtQtde_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtTabela_Change()
    If txtTabela.Text = "" Then
        txtPeriodo.Text = ""
        txtAdicional.Text = ""
        txtPrLiquido.Text = ""
    End If
End Sub

Private Sub txtTabela_GotFocus()
    txtTabela.BackColor = Vermelho
End Sub

Private Sub txtTabela_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtUf_GotFocus()
    txtUf.BackColor = Vermelho
End Sub

Private Sub txtUf_KeyPress(KeyAscii As Integer)
'    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtUf)
End Sub

Private Sub txtUf_LostFocus()
    If Val(txtUf.Text) > 99.99 Then
        MsgBox "O desconto deve ser < 99.99", vbInformation, "Aten��o"
        txtUf.SetFocus
        Exit Sub
    End If
    If txtUf.Text = "" Then
        txtUf.Text = "0"
    End If
    txtUf.BackColor = Branco
End Sub

