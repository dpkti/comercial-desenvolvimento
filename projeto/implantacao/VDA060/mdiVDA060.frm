VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.MDIForm mdiVDA060 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000F&
   Caption         =   "VDA060 - An�lise Pol�tica de Comercializa��o"
   ClientHeight    =   8175
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8880
   Icon            =   "mdiVDA060.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1230
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8880
      _Version        =   65536
      _ExtentX        =   15663
      _ExtentY        =   2170
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      BorderWidth     =   0
      BevelOuter      =   0
      Begin Bot�o.cmd cmdSobre 
         Height          =   1005
         Left            =   9675
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   45
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA060.frx":23D2
         PICN            =   "mdiVDA060.frx":23EE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   10755
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   45
         Width           =   1005
         _ExtentX        =   1773
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA060.frx":30C8
         PICN            =   "mdiVDA060.frx":30E4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin pkGradientControl.pkGradient pkGradient1 
         Height          =   30
         Left            =   45
         TabIndex        =   4
         Top             =   1125
         Width           =   11760
         _ExtentX        =   20743
         _ExtentY        =   53
         Color1          =   0
         Color2          =   -2147483633
         BackColor       =   -2147483633
      End
      Begin Bot�o.cmd cmdBuscar 
         Height          =   1005
         Left            =   1485
         TabIndex        =   5
         ToolTipText     =   "Buscar Pedidos"
         Top             =   45
         Width           =   1635
         _ExtentX        =   2884
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Buscar Pedidos"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiVDA060.frx":3DBE
         PICN            =   "mdiVDA060.frx":3DDA
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   -90
         Picture         =   "mdiVDA060.frx":4AB4
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -45
         Width           =   1455
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7845
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   6
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2646
            MinWidth        =   2646
            Object.ToolTipText     =   "Usu�rio da rede"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Object.ToolTipText     =   "Usu�rio do banco de dados"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   3528
            MinWidth        =   3528
            Object.ToolTipText     =   "Banco de dados conectado"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            TextSave        =   "08/04/2019"
            Object.ToolTipText     =   "Data"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   1
            Object.Width           =   1587
            MinWidth        =   1587
            TextSave        =   "16:23"
            Object.ToolTipText     =   "Hora"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "mdiVDA060"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : mdiVDA060
' Author    : c.samuel.oliveira
' Date      : 28/03/2019
' Purpose   : DPK-74
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : mdiVDA060
' Author    : c.samuel.oliveira
' Date      : 15/02/16
' Purpose   : TI-4160
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub cmdSair_Click()

    If vVB_Generica_001.Sair = 6 Then

        Set vObjOracle = Nothing
        Set vBanco = Nothing
        Set vSessao = Nothing

        End

    End If

End Sub

Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub MDIForm_Load()

    Dim dt_icm As Date
    Dim dt_pis As Date
    Dim dt_comis As Date
    Dim dt_ipi As Date
    Dim dt_cofins As Date

    'INCLUIR OS ARQUIVOS: (PROJECT -> ADD FILE OU CTRL+D)
    'H:\Usr\Padroes\VB6\Forms\Aguardar.frm
    'H:\Usr\Padroes\VB6\Forms\Logo.frm
    'H:\Usr\Padroes\VB6\M�dulos\M�dulo padr�o.bas
    
    frmLogo.Show 1

    Me.Top = 0
    Me.Left = 0

    sCOD_VEND = vVB_Generica_001.NomeUsuarioRede

    If Command$ = "T" Then
        sCOD_VEND = "BONAS"
    End If
    
    '------------------------------------------------------------------------------------------------
    ' Alexsandro de Macedo - 05/05/2011
    ' Para testes comentar a linha abaixo e liberar a mensagem e a que conecta no banco de teste!
    '
    'Para banco producao...
    vErro = vVB_Generica_001.ConectaOracle("PRODUCAO", "BLOQ_" & sCOD_VEND, True, Me)
    '
    'Para banco teste...
    'Call MsgBox("Lembre de retornar a conex�o para o banco de produ��o", vbExclamation Or vbOKOnly, "EM TESTE!!!")
    'vErro = vVB_Generica_001.ConectaOracle("SDPKT", "BLOQ_" & sCOD_VEND, True, Me)
    '------------------------------------------------------------------------------------------------

    If vErro <> "" Then
        If vErro Like "*username*" Then
            MsgBox "Usu�rio: " & "BLOQ_" & sCOD_VEND & " n�o autorizado para utilizar este programa.", vbCritical, "Aten��o"
            End
        ElseIf vErro Like "*2391*" Then
            MsgBox "Usu�rio: " & "BLOQ_" & sCOD_VEND & " j� est� logando no banco de dados, n�o � poss�vel logar novamente", vbCritical, "Aten��o"
            End
        Else
            MsgBox "OCORREU O ERRO: " & vErro, vbCritical, "Aten��o"
            End
        End If
    End If

    vCD = vVB_Generica_001.vCD
    vTipoCD = vVB_Generica_001.vTipoCD
    
    Set vSessao = vVB_Generica_001.vSessao
    Set vBanco = vVB_Generica_001.vBanco

    If vTipoCD = "U" Then
        strTabela_Banco = "PRODUCAO."
    Else
        strTabela_Banco = "DEP" & Format(vCD, "00") & "."
    End If

    'sCOD_VEND = "655"
    'sCOD_VEND = "BONAS"
    arq_output = "C:\temp.txt"

    '------------------------------------------------------------------------------------------------
    ' Alexsandro de Macedo - 05/05/2011
    ' Para testes comentar a linha abaixo que se referer a produ��o e liberar as linhas abaixo...
    '
    'path na rede produ��o...
    strPath = "H:\ORACLE\DADOS\32Bits\"
    '
    'path para testes...
    'Call MsgBox("Lembre de retornar o path para a rede de produ��o", vbExclamation Or vbOKOnly, "EM TESTE!!!")
    'strPath = "\\OFFICE_DPK\SISTEMAS$\ORACLE\DADOS\32BITS\"
    '------------------------------------------------------------------------------------------------

    Call frmSenha.Show(vbModal)

    'Conexao Access
    'Set dbAccess = OpenDatabase(strPath & "CONTROLE.MDB")'TI-4160
    
    Fl_Banco = "S"

    vBanco.Parameters.Remove "PM_BANCO"
    vBanco.Parameters.Add "PM_BANCO", strTabela_Banco, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_DEPOSITO_DEFAULT(:PM_BANCO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        
        MsgBox "Nenhum dep�sito padr�o encontrado. Contate o Depto. de Sistemas", , "ATEN��O"
        Exit Sub

    Else
        
        'DPK-74
'        FILIAL_PED = vObjOracle!filial
'        uf_origem = vObjOracle!cod_uf
'        deposito_default = vObjOracle!deposito_default

        For i = 1 To vObjOracle.RecordCount
            If Val(vCD) = Val(vObjOracle!deposito_default) Then
                FILIAL_PED = vObjOracle!filial
                uf_origem = vObjOracle!cod_uf
                deposito_default = vObjOracle!deposito_default
            End If
            vObjOracle.MoveNext
        Next
        'FIM DPK-74
        
        Set vObjOracle = vVB_Generica_001.TabelaDatas(vBanco)

        Data_Faturamento = CDate(vObjOracle("DT. FATURAMENTO"))
        data_real = CDate(vObjOracle("DT. REAL"))
        Data_fora_Semana = data_real = CDate(vObjOracle("DT. FORA SEMANA"))
        lngDif_Semana = Data_fora_Semana - Data_Faturamento
        If lngDif_Semana < 0 Then
            lngDif_Semana = 0
        End If

    End If

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_DOLAR(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "IDP m�dio n�o localizado, entre em contato com Controladoria", vbInformation, "Aten��o"
        End
    Else
        Dolar_Medio = IIf(IsNull(vObjOracle!Dolar_Medio), 0, vObjOracle!Dolar_Medio)
    End If

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", CLng(Mid(deposito_default, 1, 3)), 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_TAXAS(:PM_CODLOJA,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "Informa��es de Taxas n�o cadastradas, verifique com Controladoria", vbInformation, "Aten��o"
        End
    Else
        dt_icm = vObjOracle!dt_icm
        dt_pis = vObjOracle!dt_pis
        dt_comis = vObjOracle!dt_comis
        dt_ipi = vObjOracle!dt_ipi
        dt_cofins = vObjOracle!dt_cofins
        Pc_Compra = vObjOracle!Pc_Compra
        Pc_Venda = vObjOracle!Pc_Venda
        Pc_Pis = vObjOracle!Pc_Pis
        Pc_Cofins = vObjOracle!Pc_Cofins
    End If

    Tx_Deflacao = (1 + (Pc_Venda / 100)) / (1 + (Pc_Compra / 100))

    vBanco.Parameters.Remove "PM_DTCOMIS"
    vBanco.Parameters.Add "PM_DTCOMIS", dt_comis, 1

    vBanco.Parameters.Remove "PM_DTICM"
    vBanco.Parameters.Add "PM_DTICM", dt_icm, 1

    vBanco.Parameters.Remove "PM_DTPIS"
    vBanco.Parameters.Add "PM_DTPIS", dt_pis, 1

    vBanco.Parameters.Remove "PM_DTIPI"
    vBanco.Parameters.Add "PM_DTIPI", dt_ipi, 1

    vBanco.Parameters.Remove "PM_DTCOFINS"
    vBanco.Parameters.Add "PM_DTCOFINS", dt_cofins, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_DOLARTAXAS(:PM_DTCOMIS,:PM_DTICM,:PM_DTPIS,:PM_DTIPI,:PM_DTCOFINS,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "Valor de IDP n�o localizado, verifique com a Controladoria", vbInformation, "Aten��o"
        End
    Else
        Dolar_Comis = vObjOracle!Dolar_Comis
        Dolar_Icm = vObjOracle!Dolar_Icm
        Dolar_Pis = vObjOracle!Dolar_Pis
        Dolar_Ipi = vObjOracle!Dolar_Ipi
        Dolar_Cofins = vObjOracle!Dolar_Cofins
        Dolar_Dia = vObjOracle!Dolar_Dia
        '--------------
        'INDEXAR (IDP)
        '--------------
        Dolar_Comis = (1 / (Dolar_Dia / Dolar_Comis))
        Dolar_Icm = (1 / (Dolar_Dia / Dolar_Icm))
        Dolar_Pis = (1 / (Dolar_Dia / Dolar_Pis))
        Dolar_Ipi = (1 / (Dolar_Dia / Dolar_Ipi))
        Dolar_Cofins = (1 / (Dolar_Dia / Dolar_Cofins))
        Dolar_Medio = (1 / (Dolar_Dia / Dolar_Medio))

    End If

    'PARA SISTEMAS QUE FOREM CONECTAR NOS CD'S, DESCOMENTAR A LINHA
    'ABAIXO E INCLUIR O FORM ABAIXO (PROJECT -> ADD FORM)
    'Conex�o CD's
    'dlgConexao.Show 1

    Set vObjOracle = vVB_Generica_001.InformacoesSistema(vBanco, App.Title)

    If vObjOracle.EOF Then

        Call vVB_Generica_001.Informar("Sistema n�o cadastrado. � necess�rio fazer o cadastro para executar o programa.")
        End

    End If

    Call DefinirTelaSobre
    

End Sub
'TI-4211
Private Sub cmdBuscar_Click()
    frmPedidos.Show 1
End Sub
'FIM TI-4211
