Attribute VB_Name = "Module1"
'---------------------------------------------------------------------------------------
' Module    : Module1
' Author    : c.samuel.oliveira
' Date      : 15/02/16
' Purpose   : TI-4160
'---------------------------------------------------------------------------------------

Global Const Branco = &HFFFFFF
Global Const Vermelho = &HC0C0FF
Global Const Cinza = &H8000000F

Public strTipo_CD_Pedido As String * 1
Public strTipo_Autorizacao As String
Public lngCod_Usuario As Long
Public lngCod_Usuario_R As Long
Public strNome_usuario As String
Public Usuario As String
Public fl_Diesel As String * 1
Public fl_Fram As String * 1
Public fl_PSTAVista As String * 1
Public fl_PSTAcrescimo As String * 1
Public fl_PSTSuframa As String * 1
Public fl_Diesel_Desconto As String * 1
Public fl_Diesel_Desconto30 As String * 1
Public fl_PST_Desc1 As String * 1
Public fl_PST_Desc2 As String * 1
Public fl_PST_Desc3 As String * 1
Public fl_PST_Plano1 As String * 1
Public fl_PST_Plano2 As String * 1
Public fl_PST_Plano3 As String * 1
Public fl_PST_Plano4 As String * 1
Public fl_PST_Plano5 As String * 1
Public fl_Plano_414 As String * 1
Public fl_TEXACO As String * 1


Public fl_vendor_PST As String * 1
Public fl_vendor_VDR As String * 1
Public fl_vendor_DIESEL As String * 1

Public fl_Tare As String
Public fl_FornE_Dif As String * 1
Public fl_FornE_Adic As String * 1
Public fl_CNAE As String * 1

Public Dolar_Pmf As Double
Public Dolar_Comis As Double
Public Dolar_Icm As Double
Public Dolar_Pis As Double
Public Dolar_Ipi As Double
Public Dolar_Cofins As Double
Public Dolar_Dia As Double
Public Dolar_Medio As Double
Public Dolar_UF As Double

Public Pc_Compra As Double
Public Pc_Venda As Double
Public Pc_Pis As Double
Public Pc_Cofins As Double
Public Pc_Aliq_Interna As Double    '%MARGEM INTERNA DE ICMS
Public Pc_Frete As Double
Public Pc_ICMS As Double
Public Pc_ICMS_Gerencial As Double
Public Margem_Cuema_T As Double
Public Margem_Repos_T As Double
Public Margem_Cuema() As Double
Public Margem_Repos() As Double

Public strTabela_Banco As String
Public SQL As String

Global sCOD_VEND As String        'CODIGO DE VENDEDOR
Global strPath As String        'PATH DOS DADOS NA REDE

Public arq_output As String
Public dbAccess As Database     'BANCO DE DADOS ACCESS
Public Fl_Banco As String

Public deposito_default As String
Public UF_Destino As String
Public uf_origem As String
Public FILIAL_PED As String
Public tipo_cliente As String     'categoria do cliente

Public data_real As Date
Public Data_Faturamento As Date
Public Data_fora_Semana As Date
Public lngDif_Semana As Long
Public Tx_Deflacao As Double

Public Opcao As Byte
Public txtResposta As String           'RESPOSTA DE UMA LISTA

Public lngNUM_PEDIDO As Long    'NUMERO DE PEDIDO
Public lngSEQ_PEDIDO As Long
Public lngCod_Loja As Long
Public lngCod_VDR As Long
Public PMF As Integer
Public tp_cliente As String
Public sPC_DESCONTO_PLANO As Single
Public sPC_ACRESCIMO_PLANO As Single
Public cod_cancel As Long

Function Busca_Aliq_Interna(vUF_Origem As String, vUF_Destino As String, vCod_Dpk As Long) As Double

    vBanco.Parameters.Remove "PM_UFORIGEM"
    vBanco.Parameters.Add "PM_UFORIGEM", vUF_Origem, 1

    vBanco.Parameters.Remove "PM_UFDESTINO"
    vBanco.Parameters.Add "PM_UFDESTINO", vUF_Destino, 1

    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", vCod_Dpk, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_UF_ORIGEM_DESTINO(:PM_UFORIGEM,:PM_UFDESTINO,:PM_CODDPK,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle6 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle6.EOF Then
        Busca_Aliq_Interna = 0
    Else
        Busca_Aliq_Interna = vObjOracle6!pc_icm
    End If

    vObjOracle6.Close

End Function

Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer

    Temp = Trim(Valor)

    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Public Sub Margem()
'Dim ss As Object
'Dim ss2 As Object

    Dim Vl_Fat As Double
    Dim Vl_Total As Double
    Dim Vl_Icms As Double
    Dim Taxa As Double
    Dim Vl_Base_Maj As Double
    Dim Pc_Margem As Double
    Dim Vl_Icm_Maj As Double
    Dim Vl_Icm_Retido As Double
    Dim Vl_Icm_Ret As Double
    Dim Fator_Trib As Double
    Dim class_fiscal As String
    Dim Vl_Icm_cuema As Double
    Dim Vl_Icm_repos As Double
    Dim Vl_BaseVis As Double
    Dim Vl_Comiss As Double
    Dim Vl_Frete As Double
    Dim Vl_Cofins As Double
    Dim Vl_Pis As Double
    Dim Vl_Ipi As Double
    Dim Vl_LiqBasevis As Double
    Dim Vl_Lucro_Cuema As Double
    Dim Vl_Lucro_Repos As Double
    Dim Vl_LiqBaseVis_T As Double
    Dim Vl_Lucro_Cuema_T As Double
    Dim Vl_Lucro_Repos_T As Double
    Dim i As Long
    'Dim ss1 As Object
    Dim w_class_antec_RJ As Integer
    Dim dblVl_Ult_Compra As Double
    Dim strFL_Credito_suspenso As String * 1
    Dim v_cliente_simples_nacional As String  'SDS2375 - 08/08/2011 - consultor30
    'Dim ss3 As Object

    'SDS2557 - Marco Aur�lio Andrade Jr. - Guardando os valores das variavies do cabe�alho
    'INICIO 2557
    Dim strDt_Pedido As String
    Dim strUF_destino As String
    Dim strCGC As String
    Dim VL_PC_ICM As Double
    Dim Pc_Margem_CNAE As Double
    
    '--> Ci&T - Paulo Carrara - 13/08/2012
    '-- Pneu DPK Margem de lucro para o VDA060 para pneu Importado - For�ar CT do item importado
    Dim v_cod_trib As String
    '--<
    
    strDt_Pedido = vObjOracle!dt_pedido
    strUF_destino = vObjOracle!cod_uf
    strCGC = vObjOracle!CGC
    VL_PC_ICM = vObjOracle!Pc_Icm1
    'TEMINO 2557

    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_MARGEM(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    Vl_LiqBaseVis_T = 0
    Vl_Lucro_Cuema_T = 0
    Vl_Lucro_Repos_T = 0

    ReDim Preserve Margem_Cuema(1 To vObjOracle.RecordCount + 1)
    ReDim Preserve Margem_Repos(1 To vObjOracle.RecordCount + 1)

    For i = 1 To vObjOracle.RecordCount

        '--> Ci&T - Paulo Carrara - 13/08/2012
        '-- Pneu DPK Margem de lucro para o VDA060 para pneu Importado - For�ar CT do item importado
        '-- Substitui��o das vari�veis vObjOracle!cod_trib por v_cod_trib
        v_cod_trib = vObjOracle!cod_trib
        
        '-- Verifica se item � importado
        If vObjOracle!cod_procedencia = 2 And vObjOracle!cod_tribipi = 1 And vObjOracle!cod_grupo = 24 Then
        '-- Verifica se o Item  � importado para tratar a Margem de lucro
        '-- Valida o tipo de cliente para alterar o cod_trib e calcular a margem correta
            If tp_cliente = "REVENDEDOR" Then
                v_cod_trib = 1
            ElseIf tp_cliente = "CONS.FINAL" Then
                v_cod_trib = 1
            ElseIf tp_cliente = "ISENTO" Then
                v_cod_trib = 0
            End If
        End If
        '--<

        Pc_Aliq_Interna = Busca_Aliq_Interna(uf_origem, UF_Destino, vObjOracle!cod_dpk)

        class_fiscal = vObjOracle!class_fiscal
        If uf_origem = "RJ" And UF_Destino = "RJ" Then

            vBanco.Parameters.Remove "PM_CLASSFISCAL"
            vBanco.Parameters.Add "PM_CLASSFISCAL", class_fiscal, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "PRODUCAO.pck_vda060.pr_CON_CLASSRJ(:PM_CLASSFISCAL,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle1 = vBanco.Parameters("PM_CURSOR1").Value

            If vObjOracle1.EOF And vObjOracle1.BOF Then
                w_class_antec_RJ = 0
            Else
                w_class_antec_RJ = vObjOracle1!total
            End If
        End If

        'Valor l�quido faturado
        Vl_Fat = vObjOracle!vl_liquido
        dblVl_Ult_Compra = IIf(IsNull(vObjOracle!vl_ult_compra), 0, vObjOracle!vl_ult_compra)

        'Valor de IPI para itens importados
        If vObjOracle!cod_tribipi = 1 Then
            If vObjOracle!cod_nope = "B20" Then
                Vl_Ipi = 0
            Else
                Vl_Ipi = Vl_Fat * (vObjOracle!pc_ipi / 100)
            End If
        Else
            Vl_Ipi = 0
        End If

        'Valor liquido faturado  + IPI
        Vl_Total = Vl_Fat + Vl_Ipi

        'C�lculo do ICMS
        Vl_Icm_Ret = 0
        Vl_Icm_Retido = 0
        Vl_Icm_cuema = 0
        Vl_Icm_repos = 0
        Vl_Icms = 0

        If uf_origem <> "SP" Then
            If (tp_cliente = "REVENDEDOR" And (v_cod_trib = "0" Or _
                                               v_cod_trib = "2" Or v_cod_trib = "8")) Or _
                                               (uf_origem = "GO" And UF_Destino = "GO" And tp_cliente = "REVENDEDOR" And v_cod_trib = "1") Or _
                                               (uf_origem = "DF" And UF_Destino <> "DF" And (v_cod_trib = "0" Or v_cod_trib = "8")) Then
                Pc_ICMS = Pc_ICMS_Gerencial
            End If

        End If

        If v_cod_trib = 0 Or v_cod_trib = 8 Or v_cod_trib = 4 Or v_cod_trib = 2 Then

            If (v_cod_trib = 8 And uf_origem = "SP") Or (v_cod_trib = 8 And vObjOracle!cod_nope = "B20") Then
                If tp_cliente = "REVENDEDOR" Then
                    Vl_Icms = Vl_Fat * (12 / 100) / Dolar_Icm
                Else
                    Vl_Icms = (Vl_Fat + Vl_Ipi) * (12 / 100) / Dolar_Icm
                End If
            ElseIf v_cod_trib = 8 And uf_origem <> "SP" Then
                If tp_cliente = "REVENDEDOR" Then
                    Vl_Icms = Vl_Fat * (Pc_ICMS / 100) / Dolar_Icm
                Else
                    Vl_Icms = (Vl_Fat + Vl_Ipi) * (Pc_ICMS / 100) / Dolar_Icm
                End If
            ElseIf v_cod_trib = 0 Then
                If tp_cliente = "REVENDEDOR" Then
                    Vl_Icms = Vl_Fat * (Pc_ICMS / 100) / Dolar_Icm
                Else
                    Vl_Icms = (Vl_Fat + Vl_Ipi) * (Pc_ICMS / 100) / Dolar_Icm
                End If
            ElseIf v_cod_trib = 2 Then
                If tp_cliente = "REVENDEDOR" Then
                    Pc_ICMS = Pc_ICMS_Gerencial
                    Vl_Icms = Vl_Fat * (Pc_ICMS / 100) / Dolar_Icm
                Else
                    Vl_Icms = Vl_Fat * (Pc_ICMS / 100) / Dolar_Icm
                End If
            Else
                Vl_Icms = 0
            End If

            Vl_Icm_Ret = 0
            Vl_Icm_Retido = 0
            Vl_Icm_cuema = 0
            Vl_Icm_repos = 0

        ElseIf v_cod_trib = 1 Or v_cod_trib = 3 Then

            Vl_Icms = 0
            '--> Ci&T - Paulo Carrara - 13/08/2012
            '-- Pneu DPK Margem de lucro para o VDA060 para pneu Importado
            '-- Inclus�o de regra apenas para pneu importado: Somar IPI para cons. final
            'And tp_cliente <> "CONS.FINAL" _
            'And vObjOracle!cod_procedencia <> 2 _
            'And vObjOracle!cod_tribipi <> 1 _
            'And vObjOracle!cod_grupo <> 24 Then
            
            If v_cod_trib = 1 And tp_cliente = "CONS.FINAL" _
                              And vObjOracle!cod_procedencia = 2 _
                              And vObjOracle!cod_tribipi = 1 _
                              And vObjOracle!cod_grupo = 24 Then
                Vl_Icms = (Vl_Fat + Vl_Ipi) * (Pc_ICMS / 100)
            ElseIf v_cod_trib = 1 Then
                Vl_Icms = Vl_Fat * (Pc_ICMS / 100)
            End If

            vBanco.Parameters.Remove "PM_CLASSFISCAL"
            vBanco.Parameters.Add "PM_CLASSFISCAL", class_fiscal, 1

            vBanco.Parameters.Remove "PM_UFORIGEM"
            vBanco.Parameters.Add "PM_UFORIGEM", uf_origem, 1

            vBanco.Parameters.Remove "PM_UFDESTINO"
            vBanco.Parameters.Add "PM_UFDESTINO", UF_Destino, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "PRODUCAO.pck_vda060.pr_CON_SUBST_TRIB(:PM_CLASSFISCAL,:PM_UFORIGEM,:PM_UFDESTINO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value

            If vObjOracle2.EOF Then
                Fator_Trib = 0
                Pc_Margem = 0
                strFL_Credito_suspenso = "N"
            Else
                strFL_Credito_suspenso = vObjOracle2!fl_cred_suspenso
                'SDS2557 - Marco Aurelio Jr. - Buscar valor do CNAE
                Pc_Margem_CNAE = Busca_MgLucro_CNAE(strUF_destino, Pc_Aliq_Interna, VL_PC_ICM, strCGC, strDt_Pedido)
                Pc_Margem = IIf(Pc_Margem_CNAE = 0, vObjOracle2!pc_margem_lucro, Pc_Margem_CNAE)
                
                If vObjOracle!cod_procedencia = 2 And vObjOracle!cod_tribipi = 1 And vObjOracle!cod_grupo = 24 Then
                    Fator_Trib = 0
                Else
                    If tp_cliente = "REVENDEDOR" Then
                        If fl_Tare = "N" Then
                            Fator_Trib = vObjOracle2!fator_revendedor
                        Else
                            Fator_Trib = vObjOracle2!fator_tare
                        End If
                    ElseIf tp_cliente = "CONS.FINAL" Then
                        If fl_Tare = "N" Then
                            Fator_Trib = vObjOracle2!fator_inscrito
                        Else
                            Fator_Trib = vObjOracle2!fator_tare
                        End If
                    ElseIf tp_cliente = "ISENTO" Then
                        Fator_Trib = vObjOracle2!fator_isento
                    End If
                End If
            End If

            'SDS2375 - 08/08/2011 - consultor30
            v_cliente_simples_nacional = Busca_Aliq_Interna_ME(vObjOracle!cod_cliente)

            If v_cod_trib = 1 And strFL_Credito_suspenso = "N" Then
                'SDS2375 - 08/08/2011 - consultor30
                If v_cod_trib = 1 And uf_origem = "GO" And UF_Destino = "GO" Then
                    Vl_Icms = ((Vl_Fat * 10) / 17) * (Pc_ICMS / 100)
                
                '--> Ci&T - Paulo Carrara - 13/08/2012
                '-- Pneu DPK Margem de lucro para o VDA060 para pneu Importado
                '-- Inclus�o de regra apenas para pneu importado: Somar IPI para cons. final
                'And tp_cliente = "CONS.FINAL" _
                'And vObjOracle!cod_procedencia = 2 _
                'And vObjOracle!cod_tribipi = 1 _
                'And vObjOracle!cod_grupo = 24 Then
                
                ElseIf tp_cliente = "CONS.FINAL" And vObjOracle!cod_procedencia = 2 _
                                                 And vObjOracle!cod_tribipi = 1 _
                                                 And vObjOracle!cod_grupo = 24 Then
                    Vl_Icms = (Vl_Fat + Vl_Ipi) * (Pc_ICMS / 100)
                Else
                    Vl_Icms = Vl_Fat * (Pc_ICMS / 100)
                End If
            ElseIf (v_cod_trib = 1 And strFL_Credito_suspenso = "S") Then
                Vl_Icms = 0
            ElseIf v_cod_trib = 3 Or v_cod_trib = 6 Then
                Vl_Icms = 0
            End If
            
            '--> Ci&T - Paulo Carrara - 13/08/2012
            '-- Pneu DPK Margem de lucro para o VDA060 para pneu Importado/nacional
            '-- Inclus�o de regra apenas para pneu importado: N�o deve possuir ICMS ST para cons. Final
            '-- Base ICMS ST deve somar IPI para revendedor
            'And tp_cliente <> "CONS.FINAL" _

            If tp_cliente <> "CONS.FINAL" Then
                Taxa = 1 + (Pc_Margem / 100)
                'SDS2375 - 08/08/2011 - consultor30
                If v_cod_trib = 1 And v_cliente_simples_nacional = "S" And _
                      uf_origem = "GO" And UF_Destino = "GO" Then
                    Vl_Base_Maj = ((Vl_Fat * 12) / 17) * Taxa
                ElseIf tp_cliente = "REVENDEDOR" Then
                    Vl_Base_Maj = (Vl_Fat + Vl_Ipi) * Taxa
                Else
                    Vl_Base_Maj = Vl_Fat * Taxa
                End If
                Vl_Icm_Maj = Vl_Base_Maj * (Pc_Aliq_Interna / 100)
                Vl_Icm_Ret = (Vl_Icm_Maj - Vl_Icms) / Dolar_UF
                Vl_Icm_Retido = Vl_Icm_Maj - Vl_Icms
            End If

        End If

        vBanco.Parameters.Remove "PM_CLASSFISCAL"
        vBanco.Parameters.Add "PM_CLASSFISCAL", class_fiscal, 1

        vBanco.Parameters.Remove "PM_UFORIGEM"
        vBanco.Parameters.Add "PM_UFORIGEM", uf_origem, 1

        vBanco.Parameters.Remove "PM_UFDESTINO"
        vBanco.Parameters.Add "PM_UFDESTINO", UF_Destino, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vSql = "PRODUCAO.pck_vda060.pr_CON_SUBST_TRIB(:PM_CLASSFISCAL,:PM_UFORIGEM,:PM_UFDESTINO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value

        If vObjOracle2.EOF Then
            Fator_Trib = 0
            Pc_Margem = 0
        Else
            'SDS2557 - Marco Aurelio Jr. - Buscar valor do CNAE
            Pc_Margem_CNAE = Busca_MgLucro_CNAE(strUF_destino, Pc_Aliq_Interna, VL_PC_ICM, strCGC, strDt_Pedido)
             If (Pc_Margem_CNAE < 0) Then
                Exit Sub
            Else
                Pc_Margem = IIf(Pc_Margem_CNAE = 0, vObjOracle2!pc_margem_lucro, Pc_Margem_CNAE)
            End If
            
            If vObjOracle!cod_procedencia = 2 And vObjOracle!cod_tribipi = 1 And vObjOracle!cod_grupo = 24 Then
                    Fator_Trib = 0
            ElseIf tp_cliente = "REVENDEDOR" Then
                If fl_Tare = "N" Then
                    Fator_Trib = vObjOracle2!fator_revendedor
                Else
                    Fator_Trib = vObjOracle2!fator_tare
                End If
            ElseIf tp_cliente = "CONS.FINAL" Then
                If fl_Tare = "N" Then
                    Fator_Trib = vObjOracle2!fator_inscrito
                Else
                    Fator_Trib = vObjOracle2!fator_tare
                End If
            ElseIf tp_cliente = "ISENTO" Then
                Fator_Trib = vObjOracle2!fator_isento
            End If
        End If

        If IsNull(vObjOracle!uss_cuema) Then
            Vl_Icm_cuema = vObjOracle!cuema * (Dolar_Dia / Dolar_Dia) * Fator_Trib * vObjOracle!qtd_atendida
        Else
            Vl_Icm_cuema = vObjOracle!cuema * (Dolar_Dia / vObjOracle!uss_cuema) * Fator_Trib * vObjOracle!qtd_atendida
        End If

        Vl_Icm_repos = vObjOracle!custo_reposicao * (Dolar_Dia / vObjOracle!uss_repos) * Fator_Trib * vObjOracle!qtd_atendida


        ' COMENTADO EM 19/06 - CARGA 20/06
        ' CONF. SOLICITA��O DO LUIS HENRIQUE
        ' RJ PASSA A TER O C�LCULO ID�NTICO
        ' AOS DEMAIS CDS
        'If uf_origem = "RJ" And uf_destino = "RJ" And vObjOracle!cod_trib = 6 Then
        '    If w_class_antec_RJ <> 0 Then
        '        'Vl_Icms = ((dblVl_Ult_Compra + ((dblVl_Ult_Compra * (20 / 100))) * (18 / 100))) / Dolar_Icm
        '        Vl_Icms = dblVl_Ult_Compra + (dblVl_Ult_Compra * (30 / 100))
        '        Vl_Icms = Vl_Icms * (19 / 100)
        '        Vl_Icms = Vl_Icms / Dolar_Icm
        '    End If
        'End If

        'Base a Vista
        Vl_BaseVis = (Vl_Total + Vl_Icm_Retido) / Dolar_Pmf
        Vl_BaseVis = Vl_BaseVis / ((Tx_Deflacao) ^ (PMF / 30))

        'Calculo Comissao
        Vl_Comiss = ((vObjOracle!pc_comiss / 100 * Vl_Total) + (vObjOracle!pc_comisstlmk / 100 * Vl_Total)) / Dolar_Comis

        'Calculo Frete
        Vl_Frete = Vl_BaseVis * (Pc_Frete / 100)

        vBanco.Parameters.Remove "PM_CLASSFISCAL"
        vBanco.Parameters.Add "PM_CLASSFISCAL", class_fiscal, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vSql = "PRODUCAO.pck_vda060.pr_CON_PIS_COFINS(:PM_CLASSFISCAL,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle3 = vBanco.Parameters("PM_CURSOR1").Value
        
        'CIT 12/04/12 - Quando for item importado e pneu, n�o deve calcular pis e cofins
        If vObjOracle!cod_procedencia = 2 And vObjOracle!cod_grupo = 24 Then
            Vl_Cofins = 0
            Vl_Pis = 0
        Else
            If vObjOracle3!total = 0 Or vObjOracle!cod_tribipi = 1 Then
            'Calculo Cofins
            Vl_Cofins = Vl_Fat * (Pc_Cofins / 100) / Dolar_Cofins

            'Calculo PIS
            Vl_Pis = Vl_Fat * (Pc_Pis / 100) / Dolar_Pis
            Else
                Vl_Cofins = 0
                Vl_Pis = 0
            End If
        End If


        'Calculo IPI
        If vObjOracle!cod_tribipi = 1 Then
            If vObjOracle!cod_nope = "B20" Then
                Vl_Ipi = 0
            Else
                Vl_Ipi = vObjOracle!vl_liquido_suframa * (vObjOracle!pc_ipi / 100) / Dolar_Ipi
            End If
        Else
            Vl_Ipi = 0
        End If

        'Base a vista liquido
        'Vl_LiqBasevis = Vl_BaseVis - Vl_Comiss - Vl_Frete
        'altera��o feita em 07/01/2002
        Vl_LiqBasevis = Vl_BaseVis
        Vl_LiqBaseVis_T = Vl_LiqBaseVis_T + Vl_LiqBasevis

        '---------------------------------------------------------------------------------------------------------
        ' ALEXSANDRO DE MACEDO - 28/11/2011
        ' NOVO CALCULO DE MARGEM - DPK
        ' SDS2384
        '
        '' COMENTADO EM 19/06 - CARGA 20/06
        '' CONF. SOLICITA��O DO LUIS HENRIQUE
        '' RJ PASSA A TER O C�LCULO ID�NTICO
        '' AOS DEMAIS CDS
        ''Lucro
        ''If uf_origem = "RJ" And uf_destino = "RJ" And vObjOracle!cod_trib = 6 And w_class_antec_RJ <> 0 Then
        ''    Vl_Lucro_Cuema = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        ''    Vl_Lucro_Repos = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        ''Else
        'If vObjOracle!cod_trib = 1 Or vObjOracle!cod_trib = 3 Or vObjOracle!cod_trib = 6 Then
        '    Vl_Lucro_Cuema = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        '    Vl_Lucro_Repos = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        'Else
        '    Vl_Lucro_Cuema = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        '    Vl_Lucro_Repos = Vl_BaseVis - Vl_Comiss - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        'End If
        ''End If
        '
        Vl_Lucro_Cuema = Vl_BaseVis - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        '
        Vl_Lucro_Repos = Vl_BaseVis - Vl_Frete - Vl_Pis - Vl_Ipi - Vl_Cofins - Vl_Icms
        '
        '---------------------------------------------------------------------------------------------------------

        If IsNull(vObjOracle!uss_cuema) Then
            Vl_Lucro_Cuema = Vl_Lucro_Cuema - (vObjOracle!cuema * (Dolar_Dia / Dolar_Dia) * vObjOracle!qtd_atendida)
        Else
            Vl_Lucro_Cuema = Vl_Lucro_Cuema - (vObjOracle!cuema * (Dolar_Dia / vObjOracle!uss_cuema) * vObjOracle!qtd_atendida)
        End If
        Vl_Lucro_Cuema = Vl_Lucro_Cuema - Vl_Icm_Ret + Vl_Icm_cuema
        Vl_Lucro_Cuema_T = Vl_Lucro_Cuema_T + Vl_Lucro_Cuema


        Vl_Lucro_Repos = Vl_Lucro_Repos - (vObjOracle!custo_reposicao * (Dolar_Dia / IIf(IsNull(vObjOracle!uss_repos), Dolar_Dia, vObjOracle!uss_repos)) * vObjOracle!qtd_atendida)
        Vl_Lucro_Repos = Vl_Lucro_Repos - Vl_Icm_Ret + Vl_Icm_repos
        Vl_Lucro_Repos_T = Vl_Lucro_Repos_T + Vl_Lucro_Repos

        If Vl_Lucro_Cuema = 0 And Vl_Lucro_Repos = 0 Or (Vl_LiqBasevis = 0) Then
            Margem_Cuema(vObjOracle!num_item_pedido) = 0
            Margem_Repos(vObjOracle!num_item_pedido) = 0
        Else
            Margem_Cuema(vObjOracle!num_item_pedido) = Vl_Lucro_Cuema * 100 / Vl_LiqBasevis
            Margem_Repos(vObjOracle!num_item_pedido) = Vl_Lucro_Repos * 100 / Vl_LiqBasevis
        End If
        vObjOracle.MoveNext

        If Vl_Lucro_Cuema_T = 0 And Vl_Lucro_Repos_T = 0 Or (Vl_LiqBaseVis_T) = 0 Then
            Margem_Cuema_T = 0
            Margem_Repos_T = 0
        Else
            Margem_Cuema_T = Vl_Lucro_Cuema_T * 100 / Vl_LiqBaseVis_T
            Margem_Repos_T = Vl_Lucro_Repos_T * 100 / Vl_LiqBaseVis_T
        End If
    Next

End Sub

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If

    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If

    Texto = KeyAscii
End Function

Function tira_caracter(Palavra)
    Dim proibir, trocar, waux, wlen, letra, wpos

    If Not IsNull(Palavra) Then
        proibir = "��������������������������������������'`^~"
        trocar = "cCaAeEiIoOuUaAeEiIoOuUaAeEiIoOuUaAoOyY    "
        wlen = Len(Palavra)
        For waux = 1 To wlen
            letra = Mid(Palavra, waux, 1)
            wpos = InStr(proibir, letra)
            If wpos > 0 Then
                Palavra = Mid(Palavra, 1, waux - 1) & Mid(trocar, wpos, 1) & Mid(Palavra, waux + 1)
            End If
        Next
    End If
    tira_caracter = Palavra
End Function

Function Busca_Aliq_Interna_ME(vCliente As Long) As String
    
     'SDS2375 - 08/08/2011 - consultor30
    vBanco.Parameters.Remove "PM_COD_CLIENTE"
    vBanco.Parameters.Add "PM_COD_CLIENTE", vCliente, 1

    vBanco.Parameters.Remove "PM_COD_LOJA"
    vBanco.Parameters.Add "PM_COD_LOJA", vCD, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.Pr_Aliquota_ME(:PM_COD_CLIENTE,:PM_COD_LOJA,:PM_CURSOR1)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle6 = vBanco.Parameters("PM_CURSOR1").Value

    If Not vObjOracle6.EOF And Not vObjOracle6.BOF Then
        Busca_Aliq_Interna_ME = vObjOracle6!tp_cliente
    Else
        Busca_Aliq_Interna_ME = "N"
    End If

    vObjOracle6.Close

End Function

Function Busca_MgLucro_CNAE(pUF_Destino As String, p_Aliq_Interna As Double, p_PC_ICM As Double, p_CGC As String, p_Dt_Pedido As String) As Double
'SDS2557 - Marco Aurelio Andrade Jr. - Criado esta fun��o para retornar o valor do CNAE

    On Error GoTo TrataErro
    
    strSQL = "SELECT PRODUCAO.FN_CALC_MAJORA_CNAE('" & pUF_Destino & "', " & p_Aliq_Interna & ", " & p_PC_ICM & "," & p_CGC & ",'" & p_Dt_Pedido & "') MG_LUCRO FROM DUAL"
        
        vBanco.Parameters.Remove "PM_SQL"
        vBanco.Parameters.Add "PM_SQL", strSQL, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2


        vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        If (vErro <> "") Then
            Busca_MgLucro_CNAE = -1
            fl_CNAE = "S"
        Else
            Set vObjOracle5 = vBanco.Parameters("PM_CURSOR1").Value
            Busca_MgLucro_CNAE = IIf(IsNull(vObjOracle5("mg_lucro")), 0, vObjOracle5("mg_lucro"))
            fl_CNAE = "N"
        End If
        
        Exit Function
        
TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    ElseIf Err = 364 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Function
'TI-4160
'Vai comitar transa��o e fazer verifica��o de erros, se for o caso
Function fAtualizaVerificaErro(p_strSQL As String)
    
    Dim strSQL As String
    Dim pCodErro As Long
    strSQL = ""
    strSQL = strSQL & "begin " & p_strSQL
    strSQL = strSQL & " ;  commit;"
    strSQL = strSQL & " exception when others then"
    strSQL = strSQL & "    rollback;"
    strSQL = strSQL & "    :cod_errora := sqlcode;"
    strSQL = strSQL & "    :txt_errora := sqlerrm;"
    strSQL = strSQL & "end;"

    vBanco.Parameters.Remove "cod_errora"
    vBanco.Parameters.Add "cod_errora", 0, 2
    vBanco.Parameters.Remove "txt_errora"
    vBanco.Parameters.Add "txt_errora", 0, 2

    vBanco.ExecuteSQL strSQL

    'pCodErro = IIf(IsNull(vBanco.Parameters("cod_errora")), 0, Val(vBanco.Parameters("cod_errora")))
    'vBanco.Parameters("txt_errora")

End Function

Function fLogControle(pRotina As String)
    
   On Error GoTo Trata_erro
    
    Dim strSQL As String
    
    strSQL = "insert into vendas.log_controle" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & " values("
    strSQL = strSQL & "VENDAS.SEQ_LOG_CONTROLE.NEXTVAL" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & "," & lngCod_Loja & "," & lngNUM_PEDIDO
    strSQL = strSQL & "," & lngSEQ_PEDIDO & ",'" & pRotina & "',SYSDATE)"
    fAtualizaVerificaErro (strSQL)
    
   Exit Function

Trata_erro:
    vVB_Generica_001.ProcessaErro Err.Description
    DoEvents
    
End Function
Function fFormataDataOracle(pData As String) As String

    Dim vDia As Byte
    Dim vMes As Byte
    Dim vAno As Integer
    Dim vMesExtenso As String
    Dim vHora As String
    
    vDia = Day(CDate(pData))
    vMes = Month(CDate(pData))
    vAno = Year(CDate(pData))
    vHora = Trim(Right(pData, 8))

    vMesExtenso = Choose(vMes, "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")
    
    fFormataDataOracle = Format(vDia, "00") & "-" & vMesExtenso & "-" & Format(vAno, "00")

End Function
'FIM TI-4160

