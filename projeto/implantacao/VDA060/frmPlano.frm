VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmPlano 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista de Plano de Pagamento"
   ClientHeight    =   4995
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7830
   Icon            =   "frmPlano.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   333
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   522
   Begin VB.TextBox lblPesquisa 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1485
      Locked          =   -1  'True
      TabIndex        =   3
      Top             =   900
      Visible         =   0   'False
      Width           =   6270
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4665
      Width           =   7830
      _ExtentX        =   13811
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13758
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7755
      _ExtentX        =   13679
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPlano.frx":23D2
      PICN            =   "frmPlano.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid grdPlano 
      Height          =   3300
      Left            =   45
      TabIndex        =   4
      Top             =   1305
      Width           =   7710
      _Version        =   65536
      _ExtentX        =   13600
      _ExtentY        =   5821
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblPesq 
      Appearance      =   0  'Flat
      Caption         =   "Procurando por:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   90
      TabIndex        =   5
      Top             =   945
      Visible         =   0   'False
      Width           =   1500
   End
End
Attribute VB_Name = "frmPlano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmPlano
' Author    : c.samuel.oliveira
' Date      : 10/02/2017
' Purpose   : TI-5583
'---------------------------------------------------------------------------------------

Option Explicit
Dim strPesquisa As String

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    Me.Top = 1700
    Me.Left = 300

    vBanco.Parameters.Remove "PM_CODPLANO"
    vBanco.Parameters.Add "PM_CODPLANO", 0, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_PLANOPAGTO(:PM_CODPLANO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha plano cadastrado", vbInformation, "Aten��o"
        Exit Sub
    End If

    'carrega dados
    With frmPlano.grdPlano
        .Cols = 5
        .Rows = vObjOracle.RecordCount + 1
        .ColWidth(0) = 660
        .ColWidth(1) = 2035
        .ColWidth(2) = 660
        .ColWidth(3) = 660
        .ColWidth(4) = 660

        .Row = 0
        .Col = 0
        .Text = "C�digo"
        .Col = 1
        .Text = "Nome do Plano"
        .Col = 2
        .Text = "PMF"
        .Col = 3
        .Text = "% Desc"
        .Col = 4
        .Text = "% Acres"

        vObjOracle.MoveFirst
        For i = 1 To .Rows - 1
            .Row = i

            .Col = 0
            .Text = vObjOracle("COD_PLANO")
            .Col = 1
            .Text = vObjOracle("DESC_PLANO")
            .Col = 2
            .Text = vObjOracle("PRAZO_MEDIO")
            If lngCod_VDR = 0 Then
                .Col = 3
                .Text = vObjOracle("PC_DESC_FIN_DPK")
                .Col = 4
                .Text = vObjOracle("PC_ACRES_FIN_DPK")
            Else
                .Col = 3
                .Text = vObjOracle("PC_DESC_FIN_VDR")
                .Col = 4
                .Text = vObjOracle("PC_ACRES_FIN_BLAU")
            End If

            vObjOracle.MoveNext
        Next
        .Row = 1
    End With

End Sub

Private Sub grdPlano_DblClick()
'TI-5583
Dim vCodPlano As Integer
grdPlano.Col = 0
vCodPlano = Val(grdPlano.Text)
'FIM TI-5583

    grdPlano.Col = 2
    If lngCod_VDR <> 0 And grdPlano.Text > 42 And vCodPlano <> 157 Then 'TI-5583
        SQL = "Este pedido � VDR, portanto "
        SQL = SQL & "n�o pode ser  utilizado prazo com PMF maior"
        SQL = SQL & " que 42 dias"
        MsgBox SQL, vbExclamation, "ATEN��O"
        Exit Sub
    End If
    grdPlano.Col = 0
    txtResposta = grdPlano.Text
    Unload Me
    Set frmPlano = Nothing
End Sub

Private Sub grdPlano_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer

    'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdPlano.Col = 1
    If KeyAscii = 13 Then
        grdPlano.Col = 0
        txtResposta = grdPlano.Text
        Unload Me
    ElseIf KeyAscii = 8 Then    'backspace
        'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Text = strPesquisa
                lblPesquisa.Visible = False
                grdPlano.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Text = strPesquisa
                DoEvents
                With grdPlano
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If

    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Text = strPesquisa
        lblPesquisa.Visible = False
        grdPlano.Row = 1
        SendKeys "{LEFT}{RIGHT}"

    Else
        'mouse
        Screen.MousePointer = vbHourglass

        If tam < 33 Then
            iAscii = vVB_Generica_001.Texto(KeyAscii)
            If iAscii > 0 Then
                'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesquisa.Text = strPesquisa
                    DoEvents
                    With grdPlano
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdPlano.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Text = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With

                End If
            End If
        Else
            Beep
        End If
    End If

    'mouse
    Screen.MousePointer = vbDefault

End Sub

