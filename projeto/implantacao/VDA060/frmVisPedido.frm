VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmVisPedido 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "An�lise de Pedido"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11100
   ClipControls    =   0   'False
   Icon            =   "frmVisPedido.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   458
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   740
   Begin VB.Frame fraMargemPedido 
      Appearance      =   0  'Flat
      Caption         =   "Margem Total Pedido"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1095
      Left            =   8520
      TabIndex        =   81
      Top             =   3600
      Visible         =   0   'False
      Width           =   2535
      Begin VB.TextBox txtMargemPadraoPedido 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1450
         TabIndex        =   83
         Top             =   650
         Width           =   950
      End
      Begin VB.TextBox txtMargemCalculadaPedido 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1450
         TabIndex        =   82
         Top             =   240
         Width           =   950
      End
      Begin VB.Label lblMargemPadraoPedido 
         Appearance      =   0  'Flat
         Caption         =   "% Mg. Padr�o:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   85
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label lblMargemCalculadaPedido 
         Appearance      =   0  'Flat
         Caption         =   "% Mg. Calculada:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   84
         Top             =   300
         Width           =   1335
      End
   End
   Begin VB.PictureBox pctMargemErro 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   9360
      Picture         =   "frmVisPedido.frx":23D2
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   80
      ToolTipText     =   "Margem n�o atendida"
      Top             =   4800
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox pctMargemOK 
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   9000
      Picture         =   "frmVisPedido.frx":24E2
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   79
      ToolTipText     =   "Margem atendida"
      Top             =   4800
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox pctIndevido 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   135
      Left            =   4800
      Picture         =   "frmVisPedido.frx":25FD
      ScaleHeight     =   135
      ScaleWidth      =   255
      TabIndex        =   76
      Top             =   4920
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox pctAlerta 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   840
      Picture         =   "frmVisPedido.frx":2B87
      ScaleHeight     =   255
      ScaleWidth      =   255
      TabIndex        =   74
      Top             =   4920
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture5 
      BorderStyle     =   0  'None
      Height          =   675
      Left            =   9900
      ScaleHeight     =   675
      ScaleWidth      =   630
      TabIndex        =   69
      Top             =   4545
      Visible         =   0   'False
      Width           =   630
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   495
         Left            =   120
         Picture         =   "frmVisPedido.frx":2C41
         ScaleHeight     =   495
         ScaleWidth      =   495
         TabIndex        =   70
         Top             =   0
         Visible         =   0   'False
         Width           =   495
      End
   End
   Begin VB.TextBox lblDados1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   68
      Top             =   1350
      Width           =   1095
   End
   Begin VB.TextBox txtMsgNota 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   1200
      MaxLength       =   60
      TabIndex        =   61
      Top             =   4560
      Width           =   7155
   End
   Begin VB.TextBox txtMsgPedido 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1200
      MaxLength       =   50
      TabIndex        =   59
      Top             =   4200
      Width           =   7155
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Margem Total"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1095
      Left            =   8370
      TabIndex        =   54
      Top             =   3240
      Width           =   2175
      Begin VB.TextBox lblMg_Cuema 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         TabIndex        =   57
         Top             =   600
         Width           =   960
      End
      Begin VB.TextBox lblMg_Reposicao 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         TabIndex        =   55
         Top             =   240
         Width           =   960
      End
      Begin VB.Label Label19 
         Appearance      =   0  'Flat
         Caption         =   "CUEMA:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   360
         TabIndex        =   58
         Top             =   600
         Width           =   825
      End
      Begin VB.Label Label18 
         Appearance      =   0  'Flat
         Caption         =   "REP.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   600
         TabIndex        =   56
         Top             =   240
         Width           =   825
      End
   End
   Begin VB.CheckBox chkDifIcm 
      Appearance      =   0  'Flat
      Caption         =   "Dif.de ICM"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   3960
      TabIndex        =   53
      Top             =   3720
      Width           =   1275
   End
   Begin VB.TextBox lblVl_Contabil 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   51
      Top             =   3720
      Width           =   1995
   End
   Begin VB.TextBox lblDescSuframa 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7155
      Locked          =   -1  'True
      TabIndex        =   49
      Top             =   3840
      Width           =   960
   End
   Begin VB.TextBox txtDesc_2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7155
      TabIndex        =   47
      Top             =   3600
      Width           =   960
   End
   Begin VB.TextBox txtPC_PLANO 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7155
      TabIndex        =   45
      Top             =   3330
      Width           =   960
   End
   Begin VB.TextBox lblDESC_PLANO 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      TabIndex        =   44
      Top             =   3360
      Width           =   3435
   End
   Begin VB.TextBox txtCOD_PLANO 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      TabIndex        =   42
      Top             =   3360
      Width           =   960
   End
   Begin VB.TextBox lblPSEUDOVEND 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   41
      Top             =   2925
      Width           =   2265
   End
   Begin VB.TextBox txtCOD_VENDEDOR 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7695
      TabIndex        =   39
      Top             =   2925
      Width           =   555
   End
   Begin VB.TextBox lblPSEUDOREPR 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4050
      Locked          =   -1  'True
      TabIndex        =   38
      Top             =   2925
      Width           =   2850
   End
   Begin VB.TextBox txtSEQ_REPRES 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3645
      TabIndex        =   37
      Top             =   2925
      Width           =   330
   End
   Begin VB.TextBox txtCOD_REPRESENTANTE 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3060
      TabIndex        =   35
      Top             =   2925
      Width           =   555
   End
   Begin VB.TextBox txtTpPedido 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   33
      Top             =   2925
      Width           =   960
   End
   Begin VB.OptionButton optCIF 
      Appearance      =   0  'Flat
      Caption         =   "CIF"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   8505
      TabIndex        =   32
      Top             =   2565
      Width           =   915
   End
   Begin VB.OptionButton optFOBD 
      Appearance      =   0  'Flat
      Caption         =   "FOBD"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   9630
      TabIndex        =   31
      Top             =   2565
      Width           =   915
   End
   Begin VB.OptionButton optFOB 
      Appearance      =   0  'Flat
      Caption         =   "FOB"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   7290
      TabIndex        =   30
      Top             =   2565
      Width           =   915
   End
   Begin VB.TextBox lblNOME_TRANSP 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   29
      Top             =   2520
      Width           =   4650
   End
   Begin VB.TextBox txtCOD_TRANSP 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   27
      Top             =   2520
      Width           =   960
   End
   Begin VB.TextBox lblTare 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   5805
      Locked          =   -1  'True
      TabIndex        =   24
      Top             =   2115
      Width           =   960
   End
   Begin VB.TextBox lblDesc_Tipo 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2250
      Locked          =   -1  'True
      TabIndex        =   23
      Top             =   2115
      Width           =   2715
   End
   Begin VB.TextBox lblCod_Tp_Cli 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   2115
      Width           =   960
   End
   Begin VB.PictureBox Picture2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   255
      Left            =   8505
      Picture         =   "frmVisPedido.frx":3083
      ScaleHeight     =   255
      ScaleWidth      =   495
      TabIndex        =   20
      Top             =   2205
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.TextBox txtQtdeItens 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9900
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   1710
      Width           =   645
   End
   Begin VB.TextBox lblNatureza 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9900
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   1350
      Width           =   645
   End
   Begin VB.TextBox lblDados4 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   7605
      Locked          =   -1  'True
      TabIndex        =   15
      Top             =   1710
      Width           =   780
   End
   Begin VB.TextBox lblDados3 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   1215
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   1710
      Width           =   5685
   End
   Begin VB.TextBox lblDados2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   2385
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   1350
      Width           =   6000
   End
   Begin VB.TextBox lblSigla_Filial 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   9000
      Locked          =   -1  'True
      TabIndex        =   11
      Top             =   945
      Width           =   1545
   End
   Begin VB.TextBox lblCod_Filial 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   8595
      Locked          =   -1  'True
      TabIndex        =   9
      Top             =   945
      Width           =   375
   End
   Begin VB.TextBox lblDt_Pedido 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   6345
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   945
      Width           =   1140
   End
   Begin VB.TextBox lblSeq_Pedido 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   4995
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   945
      Width           =   330
   End
   Begin VB.TextBox lblNr_Pedido 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   3870
      Locked          =   -1  'True
      TabIndex        =   4
      Top             =   945
      Width           =   1095
   End
   Begin VB.TextBox lblDeposito 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   360
      Left            =   1215
      TabIndex        =   2
      Text            =   "CAMPINAS"
      Top             =   945
      Width           =   1635
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   10590
      _ExtentX        =   18680
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":34C5
      PICN            =   "frmVisPedido.frx":34E1
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSGrid.Grid Grid2 
      Height          =   1620
      Left            =   45
      TabIndex        =   63
      Top             =   5190
      Width           =   10980
      _Version        =   65536
      _ExtentX        =   19368
      _ExtentY        =   2857
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      HighLight       =   0   'False
   End
   Begin Bot�o.cmd Command5 
      Height          =   690
      Left            =   720
      TabIndex        =   65
      TabStop         =   0   'False
      ToolTipText     =   "Liberar Pedido"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":41BB
      PICN            =   "frmVisPedido.frx":41D7
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command1 
      Height          =   690
      Left            =   2160
      TabIndex        =   66
      TabStop         =   0   'False
      ToolTipText     =   "Cancelar Pedido"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":4EB1
      PICN            =   "frmVisPedido.frx":4ECD
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd Command2 
      Height          =   690
      Left            =   1440
      TabIndex        =   67
      TabStop         =   0   'False
      ToolTipText     =   "Motivo Bloqueio"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":57A7
      PICN            =   "frmVisPedido.frx":57C3
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   7800
      TabIndex        =   71
      TabStop         =   0   'False
      ToolTipText     =   "Gravar Arquivo"
      Top             =   45
      Visible         =   0   'False
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":5ADD
      PICN            =   "frmVisPedido.frx":5AF9
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsulta 
      Height          =   690
      Left            =   2880
      TabIndex        =   72
      TabStop         =   0   'False
      ToolTipText     =   "Recalcula Margem"
      Top             =   0
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmVisPedido.frx":67D3
      PICN            =   "frmVisPedido.frx":67EF
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblDPKNET 
      Caption         =   "PEDIDO FEITO VIA DPKNET"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   4200
      TabIndex        =   78
      Top             =   240
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.Label lblPreco 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Pre�o do item <> Pre�o Atual da Tabela"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   5160
      TabIndex        =   77
      Top             =   4920
      Visible         =   0   'False
      Width           =   3495
   End
   Begin VB.Label Label14 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Tabela de venda n�o vigente"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   285
      Left            =   1200
      TabIndex        =   75
      Top             =   4920
      Width           =   2895
   End
   Begin VB.Label lblVenda_Casada 
      AutoSize        =   -1  'True
      BackColor       =   &H0080FFFF&
      Caption         =   "VENDA CASADA"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   210
      Left            =   5160
      TabIndex        =   73
      Top             =   480
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label lblVDR 
      Caption         =   "PEDIDO VDR"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   8775
      TabIndex        =   64
      Top             =   315
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.Label Label21 
      Appearance      =   0  'Flat
      Caption         =   "Msg. Nota:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   62
      Top             =   4560
      Width           =   960
   End
   Begin VB.Label Label20 
      Appearance      =   0  'Flat
      Caption         =   "Msg. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   120
      TabIndex        =   60
      Top             =   4200
      Width           =   1230
   End
   Begin VB.Label Label17 
      Appearance      =   0  'Flat
      Caption         =   "Vl. Cont�bil:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   180
      TabIndex        =   52
      Top             =   3720
      Width           =   1230
   End
   Begin VB.Label Label16 
      Appearance      =   0  'Flat
      Caption         =   "% SUFRAMA:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5985
      TabIndex        =   50
      Top             =   3840
      Width           =   1230
   End
   Begin VB.Label lblDesc_2 
      Appearance      =   0  'Flat
      Caption         =   "% Desconto:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5985
      TabIndex        =   48
      Top             =   3600
      Width           =   1230
   End
   Begin VB.Label lblDescAcres 
      Appearance      =   0  'Flat
      Caption         =   "% Desconto:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5985
      TabIndex        =   46
      Top             =   3375
      Width           =   1230
   End
   Begin VB.Label Label13 
      Appearance      =   0  'Flat
      Caption         =   "Plano:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   630
      TabIndex        =   43
      Top             =   3360
      Width           =   645
   End
   Begin VB.Label Label12 
      Appearance      =   0  'Flat
      Caption         =   "Vend.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   7110
      TabIndex        =   40
      Top             =   2970
      Width           =   915
   End
   Begin VB.Label Label11 
      Appearance      =   0  'Flat
      Caption         =   "Repres.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2340
      TabIndex        =   36
      Top             =   2970
      Width           =   915
   End
   Begin VB.Label Label10 
      Appearance      =   0  'Flat
      Caption         =   "Tp. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   180
      TabIndex        =   34
      Top             =   2970
      Width           =   1230
   End
   Begin VB.Label Label9 
      Appearance      =   0  'Flat
      Caption         =   "Transp.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   450
      TabIndex        =   28
      Top             =   2565
      Width           =   825
   End
   Begin VB.Label lblDt_tare 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   6840
      TabIndex        =   26
      Top             =   2160
      Width           =   1095
   End
   Begin VB.Label Label8 
      Appearance      =   0  'Flat
      Caption         =   "Tare:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5265
      TabIndex        =   25
      Top             =   2160
      Width           =   690
   End
   Begin VB.Label Label7 
      Appearance      =   0  'Flat
      Caption         =   "Tipo Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   90
      TabIndex        =   22
      Top             =   2160
      Width           =   1275
   End
   Begin VB.Line ln1 
      BorderColor     =   &H00800000&
      BorderWidth     =   2
      X1              =   488
      X2              =   480
      Y1              =   126
      Y2              =   126
   End
   Begin VB.Label Label6 
      Appearance      =   0  'Flat
      Caption         =   "Qtd. Itens:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   8910
      TabIndex        =   19
      Top             =   1755
      Width           =   1005
   End
   Begin VB.Label Label5 
      Appearance      =   0  'Flat
      Caption         =   "Natureza OP.:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   8685
      TabIndex        =   17
      Top             =   1395
      Width           =   1275
   End
   Begin VB.Label Label4 
      Appearance      =   0  'Flat
      Caption         =   "Cliente:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   495
      TabIndex        =   13
      Top             =   1395
      Width           =   825
   End
   Begin VB.Label Label3 
      Appearance      =   0  'Flat
      Caption         =   "Filial Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   7605
      TabIndex        =   10
      Top             =   990
      Width           =   1050
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      Caption         =   "Dt. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   5400
      TabIndex        =   8
      Top             =   990
      Width           =   1005
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      Caption         =   "Nr. Pedido:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   2925
      TabIndex        =   5
      Top             =   990
      Width           =   915
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      Caption         =   "Dep�sito:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Left            =   360
      TabIndex        =   3
      Top             =   990
      Width           =   915
   End
End
Attribute VB_Name = "frmVisPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmVisPedido
' Author    : c.samuel.oliveira
' Date      : 28/03/2019
' Purpose   : DPK-74
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVisPedido
' Author    : c.samuel.oliveira
' Date      : 09/02/2017
' Purpose   : TI-5583
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVisPedido
' Author    : c.samuel.oliveira
' Date      : 15/02/16
' Purpose   : TI-4160
'---------------------------------------------------------------------------------------

'---------------------------------------------------------------------------------------
' Module    : frmVisPedido
' Author    : c.samuel.oliveira
' Date      : 10/12/15
' Purpose   : TI-3765
'---------------------------------------------------------------------------------------
Option Explicit

Dim bDifIcm As Byte
Dim strTipoRespCliente As String
Dim lngCOD_PREPOSTO As Long

Private w_nat_oper As String
Private w_uf_dest As String
Private w_cod_loja As Long


Private Sub chkDifIcm_Click()
'chkDifIcm.Value = bDifIcm
End Sub

Private Sub chkDifIcm_GotFocus()
    chkDifIcm.BackColor = Vermelho
End Sub

Private Sub chkDifIcm_LostFocus()
    chkDifIcm.BackColor = Cinza
End Sub

Private Sub cmd1_Click()
    frmGrava_Arquivo.Show 1
End Sub

Private Sub cmdConsulta_Click()

    'BCAMPOS - CIT - 21/11/2013
    'POLITICA DE MARGEM - RECALCULO
    'Form_Load
    
    On Error GoTo TrataErro

    vSql = "BEGIN "
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        vSql = vSql & strTabela_Banco & "PR_CABECCOMIS(:loja,:NUM,:SEQ,:DESC,:ACRES,:PLANO,:BANCO,:MARGEM);"
    Else
        'TI-3765
        If vTipoCD = "U" And strTipo_CD_Pedido = "M" Then
            vSql = vSql & " dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PR_CABECCOMIS@LNK_CDDPK(:loja,:NUM,:SEQ,:DESC,:ACRES,:PLANO,:BANCO,:MARGEM);"
        Else
            vSql = vSql & " dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PR_CABECCOMIS(:loja,:NUM,:SEQ,:DESC,:ACRES,:PLANO,:BANCO,:MARGEM);"
        End If
        'FIM TI-3765
    End If
    vSql = vSql & " COMMIT;"
    vSql = vSql & "EXCEPTION"
    vSql = vSql & " WHEN OTHERS THEN"
    vSql = vSql & " ROLLBACK;"
    vSql = vSql & " :cod_errora := SQLCODE;"
    vSql = vSql & " :txt_errora := SQLERRM;"
    vSql = vSql & " END;"

    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1
    vBanco.Parameters.Remove "num"
    vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSEQ_PEDIDO, 1
    vBanco.Parameters.Remove "desc"
    vBanco.Parameters.Remove "acres"
    If lblDescAcres.Caption = "%Desconto" Then
        vBanco.Parameters.Add "desc", txtPC_PLANO.Text, 1
        vBanco.Parameters.Add "acres", 0, 1
    Else
        vBanco.Parameters.Add "acres", txtPC_PLANO.Text, 1

        If txtDesc_2.Text = "" Then
            vBanco.Parameters.Add "desc", 0, 1
        ElseIf CDbl(txtDesc_2.Text) < 0.1 Then
            vBanco.Parameters.Add "desc", 0, 1
        Else
            vBanco.Parameters.Add "desc", txtDesc_2.Text, 1
        End If
    End If
    
    vBanco.Parameters.Remove "plano"
    vBanco.Parameters.Add "plano", txtCOD_PLANO.Text, 1
    vBanco.Parameters.Remove "cod_errora"
    vBanco.Parameters.Add "cod_errora", 0, 2
    vBanco.Parameters.Remove "txt_errora"
    vBanco.Parameters.Add "txt_errora", "", 2
    vBanco.Parameters.Remove "banco"
    
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        If Mid(strTabela_Banco, 1, 3) = "DEP" Then
            vBanco.Parameters.Add "banco", Mid(strTabela_Banco, 1, 5), 1
        Else
            vBanco.Parameters.Add "banco", Mid(strTabela_Banco, 1, 8), 1
        End If
        
    Else
        vBanco.Parameters.Add "banco", "dep" & Trim(Format(lngCod_Loja, "00")), 1
    End If

    'MARGEM = 1 (Recalcular Magem do Pedido)
    vBanco.Parameters.Remove "margem"
    vBanco.Parameters.Add "margem", 1, 1

    vBanco.ExecuteSQL vSql

    vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

    If vErro <> 0 Then
        MsgBox vBanco.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = vbDefault
        Exit Sub
    End If

    Form_Load
    
    'Habilita bot�o de libera��o do pedido
    Command5.Enabled = True

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    ElseIf Err = 440 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
    
End Sub

Private Sub cmdEnvia_Email_Click()

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Command1_Click()

    Dim response As String

    On Error GoTo TrataErro

    frmCancela.Show vbModal

    strTipo_Autorizacao = "C"


    If cod_cancel = "0" Then
        Exit Sub
    Else
        response = MsgBox("Confirma o cancelamento do Pedido ?", vbYesNo, "Aten��o")
        If response = vbNo Then
            Exit Sub
        End If

        FrmAutorizacao.Show 1

        'Dispara procedure no Oracle
        If strTipo_CD_Pedido = "U" Then 'DPK-74
            vSql = "BEGIN " & strTabela_Banco & "PR_CANCELA (:loja,:NUM,:SEQ,9,:cod_cancel);"
        Else
            vSql = "BEGIN dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PR_CANCELA (:loja,:NUM,:SEQ,9,:cod_cancel);"
        End If
        vSql = vSql & " COMMIT;"
        vSql = vSql & "EXCEPTION"
        vSql = vSql & " WHEN OTHERS THEN"
        vSql = vSql & " ROLLBACK;"
        vSql = vSql & " :cod_errora := SQLCODE;"
        vSql = vSql & " :txt_errora := SQLERRM;"
        vSql = vSql & " END;"

        vBanco.Parameters.Remove "loja"
        vBanco.Parameters.Add "loja", lngCod_Loja, 1
        vBanco.Parameters.Remove "num"
        vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
        vBanco.Parameters.Remove "SEQ"
        vBanco.Parameters.Add "SEQ", lngSEQ_PEDIDO, 1
        vBanco.Parameters.Remove "cod_cancel"
        vBanco.Parameters.Add "cod_cancel", cod_cancel, 1
        vBanco.Parameters.Remove "cod_errora"
        vBanco.Parameters.Add "cod_errora", 0, 2
        vBanco.Parameters.Remove "txt_errora"
        vBanco.Parameters.Add "txt_errora", "", 2


        vBanco.ExecuteSQL vSql

        vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

        If vErro <> 0 Then
            MsgBox vBanco.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
            Screen.MousePointer = vbDefault
            Exit Sub
        Else
            MsgBox "Pedido Cancelado", vbInformation, "Aten��o"
        End If

    End If

    Unload Me

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If

End Sub

Private Sub Command2_Click()
    frmMotiv_bloq.Show 1
End Sub

Private Sub Command5_Click()

    Dim response As String
    Dim strMSG As String
    'Dim ss As Object
    Dim bancoDest As String
    
    On Error GoTo TrataErro

    strTipo_Autorizacao = "L"
    'verifica se itens diesel est�o sendo vendidos com pmf > 49 DD
    fl_Diesel = "N"
    fl_Diesel_Desconto = "N"
    fl_Diesel_Desconto30 = "N"
    fl_Fram = "N"
    fl_PSTAVista = "N"
    fl_PSTAcrescimo = "N"
    fl_PSTSuframa = "N"
    fl_FornE_Dif = "N"
    fl_FornE_Adic = "N"
    fl_PST_Desc1 = "N"
    fl_PST_Desc2 = "N"
    fl_PST_Desc3 = "N"
    fl_PST_Plano1 = "N"
    fl_PST_Plano2 = "N"
    fl_PST_Plano3 = "N"
    fl_PST_Plano4 = "N"
    fl_PST_Plano5 = "N"
    fl_Plano_414 = "N"


    fl_vendor_PST = "N"
    fl_vendor_VDR = "N"
    fl_vendor_DIESEL = "N"

    ' Call Verifica_Fornecedor_Especifico(lngCod_Loja, lngNUM_PEDIDO, lngSEQ_PEDIDO)
    If fl_FornE_Dif = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS COM DESCONTO DIF. DE ICMS QUE  "
        strMSG = strMSG & " N�O � PERMITIDO PELO FORNECEDOR, PORTANTO N�O PODE SER LIBERADO. "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_FornE_Adic = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS COM DESCONTO ADICIONAL QUE  "
        strMSG = strMSG & " N�O � PERMITIDO PELO FORNECEDOR, PORTANTO N�O PODE SER LIBERADO. "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    Call Verifica_Diesel(lngCod_Loja, lngNUM_PEDIDO, lngSEQ_PEDIDO, txtCOD_PLANO.Text)

    If fl_vendor_VDR = "S" Then
        strMSG = "ESTE PEDIDO � VDR E N�O PODE SER  "
        strMSG = strMSG & " LIBERADO COM PLANO DE PGTO VENDOR "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    'comentado conforme solicitacao do Celso
    ' em 05/04/06, carga em 13/04/06
    ' If fl_TEXACO = "S" Then
    '   strMSG = "ESTE PEDIDO TEM ITENS TEXACO E N�O PODE SER  "
    '   strMSG = strMSG & " LIBERADO COM PLANO COM DESCONTO > 0% PARA PMF >  7DD E "
    '   strMSG = strMSG & " N�O PODE SER LIBERADO COM DESCONTO > 3% PARA PMF <= 7DD "
    '   MsgBox strMSG, vbCritical, "ATEN��O"
    '   Exit Sub
    ' End If

    'COMENTADO EM 28/10 SOLICITA��O MARCELO HUDSON
    ' If fl_vendor_PST = "S" Then
    '   strMSG = "ESTE PEDIDO CONT�M ITEM PST E N�O PODE SER  "
    '   strMSG = strMSG & " LIBERADO COM PLANO DE PGTO VENDOR "
    '   MsgBox strMSG, vbCritical, "ATEN��O"
    '   Exit Sub
    ' End If

    If fl_vendor_DIESEL = "S" Then
        strMSG = "ESTE PEDIDO CONT�M ITEM BOSCH DIESEL E N�O PODE SER  "
        strMSG = strMSG & " LIBERADO COM PLANO DE PGTO VENDOR "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_Plano_414 = "S" Then
        strMSG = "ESTE PEDIDO ESTA COM O PLANO 414 E "
        strMSG = strMSG & " N�O PODE SER LIBERADO PARA OUTROS FORNECEDORES AL�M "
        strMSG = strMSG & " DE PST "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_Diesel = "S" Then
        
        '------------------------------------------------------------------------
        ' Alexsandro de Macedo - 26/10/2011
        '
        'strMSG = "ESTE PEDIDO POSSUI ITENS VDR E N�O PODE SER LIBERADO COM "
        ''------------------------------------------------------------------------
        '' Alexsandro de Macedo - 04/05/2011
        '' Branche: http://10.10.16.115/svn/dpk-comercial/branches/SDS_2334
        '' RC# 25:  http://10.10.16.115/tracker/dpk-comercial/requirementchange/25
        ''strMSG = strMSG & " PLANO DE PAGTO QUE POSSUA PMF MAIOR QUE 49 DD "
        'strMSG = strMSG & " PLANO DE PAGTO QUE POSSUA PMF MAIOR QUE 60 DD "
        ''------------------------------------------------------------------------
        '
        strMSG = ""
        strMSG = strMSG & "Pedido possui itens Bosch Diesel, n�o � " & vbCrLf
        strMSG = strMSG & "permitido a libera��o para PMF maior que " & vbCrLf
        strMSG = strMSG & "49 dias."
        '
        MsgBox strMSG, vbCritical, "ATEN��O"
        '
        '------------------------------------------------------------------------
        
        Exit Sub
        
    End If

    If fl_Diesel_Desconto = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS VDR E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO MAIOR QUE 4% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_Diesel_Desconto30 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS VDR E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO MAIOR OU IGUAL A 1% PARA PMF 30 DD "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PSTAVista = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO MAIOR QUE 11.16% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PSTAcrescimo = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF >= 60 DIAS, E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " ACRESCIMO DO PLANO DE PAGTO MENOR QUE 2.2% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Plano1 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF >= 0 e <=14 DIAS, E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO MAIOR QUE 9.25% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Plano5 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF >= 15 e <=30 DIAS, E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO MAIOR QUE 4.47% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Plano2 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF > 30 e <=48 DIAS, E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO > QUE 0% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Plano3 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF = 49 DIAS, E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DO PLANO DE PAGTO > QUE 0% "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Plano4 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST E PMF > 60 DIAS, E N�O PODE SER LIBERADO. "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PSTSuframa = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST  E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DE SUFRAMA "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Desc1 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST  E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DE PER�ODO <> 0 "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Desc2 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST  E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO DE UF <> 0 "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    If fl_PST_Desc3 = "S" Then
        strMSG = "ESTE PEDIDO POSSUI ITENS PST  E N�O PODE SER LIBERADO COM "
        strMSG = strMSG & " DESCONTO ADICIONAL <> 0 "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    'COMENTADO EM 19/05/06 CONFORME SOLICITA��O MARCELO HUDSON
    '==========================================================
    'Call Verifica_Fram(lngCod_Loja, lngNUM_PEDIDO, lngSEQ_PEDIDO)
    'If fl_Fram = "S" Then
    '    strMSG = "ESTE PEDIDO POSSUI ITENS FRAM, O CLIENTE � DO RIO GRANDE DO SUL"
    '    strMSG = strMSG & " E � POSTO DE COMBUST�VEL, PORTANTO N�O PODE SER LIBERADO "
    '     MsgBox strMSG, vbCritical, "ATEN��O"
    '     Exit Sub
    ' End If
    
    If fl_CNAE = "S" Then
        strMSG = "CLIENTE DESTE PEDIDO ESTA COM CNAE INVALIDO, E N�O PODE SER LIBERADO."
        strMSG = strMSG & vbCrLf & " ENTRE EM CONTATO COM A CENTRAL! "
        MsgBox strMSG, vbCritical, "ATEN��O"
        Exit Sub
    End If

    response = MsgBox("Confirma a libera��o do Pedido ?", vbYesNo, "Aten��o")

    If response = vbNo Then
        Exit Sub
    End If

    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_DESCADICIONAL(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle!total > 0 Then
        response = MsgBox("Este pedido possui itens em Oferta com desconto adicional, tem " & _
                          " certeza que deseja liberar ?", vbYesNo + vbDefaultButton2, "Aten��o")
        If response = vbNo Then
            Exit Sub
        End If
    End If

    'WILLIAM LEITE - TRACKER 148
    'response = MsgBox("Aten��o para a margem do pedido. Tem certeza que deseja liberar ?", vbYesNo, "Aten��o")

    'If response = vbNo Then
    '    Exit Sub
    'End If

    'Dispara procedure no Oracle
    vSql = "BEGIN "
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        vSql = vSql & " UPDATE producao.PEDNOTA_VENDA SET"
    Else
        vSql = vSql & " UPDATE dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PEDNOTA_VENDA SET"
    End If
    vSql = vSql & " MENS_PEDIDO = :MENSP,"
    vSql = vSql & " MENS_NOTA = :MENSN"
    vSql = vSql & " WHERE SEQ_PEDIDO = :SEQ AND"
    vSql = vSql & " NUM_PEDIDO = :NUM AND"
    vSql = vSql & " COD_LOJA = :LOJA;"
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        'SDS2604 - Marco Jr - Incluso parametro de banco.
        vSql = vSql & strTabela_Banco & "PR_CABECCOMIS(:loja,:NUM,:SEQ,:DESC,:ACRES,:PLANO,:BANCO);"
    Else
        vSql = vSql & " dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PR_CABECCOMIS(:loja,:NUM,:SEQ,:DESC,:ACRES,:PLANO,:BANCO);"
    End If
    vSql = vSql & " COMMIT;"
    vSql = vSql & "EXCEPTION"
    vSql = vSql & " WHEN OTHERS THEN"
    vSql = vSql & " ROLLBACK;"
    vSql = vSql & " :cod_errora := SQLCODE;"
    vSql = vSql & " :txt_errora := SQLERRM;"
    vSql = vSql & " END;"

    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1
    vBanco.Parameters.Remove "num"
    vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSEQ_PEDIDO, 1
    vBanco.Parameters.Remove "MENSP"
    vBanco.Parameters.Add "MENSP", IIf(frmVisPedido.txtMsgPedido = "", Null, frmVisPedido.txtMsgPedido), 1
    vBanco.Parameters.Remove "MENSN"
    vBanco.Parameters.Add "MENSN", IIf(frmVisPedido.txtMsgNota = "", Null, frmVisPedido.txtMsgNota), 1
    vBanco.Parameters.Remove "desc"
    vBanco.Parameters.Remove "acres"
    If lblDescAcres.Caption = "%Desconto" Then
        vBanco.Parameters.Add "desc", txtPC_PLANO.Text, 1
        vBanco.Parameters.Add "acres", 0, 1
    Else
        vBanco.Parameters.Add "acres", txtPC_PLANO.Text, 1

        If txtDesc_2.Text = "" Then
            vBanco.Parameters.Add "desc", 0, 1
        ElseIf CDbl(txtDesc_2.Text) < 0.1 Then
            vBanco.Parameters.Add "desc", 0, 1
        Else
            vBanco.Parameters.Add "desc", txtDesc_2.Text, 1
        End If
    End If
    vBanco.Parameters.Remove "plano"
    vBanco.Parameters.Add "plano", txtCOD_PLANO.Text, 1
    vBanco.Parameters.Remove "cod_errora"
    vBanco.Parameters.Add "cod_errora", 0, 2
    vBanco.Parameters.Remove "txt_errora"
    vBanco.Parameters.Add "txt_errora", "", 2
        'SDS2604 - Marco Jr - Incluso parametro de banco.
        ' Tratado para verificar em qual banco que esta sendo executado.
    vBanco.Parameters.Remove "banco"
    'William Leite - Corre��o para liberar pedido por outro CD
    'If vTipoCD = "U" Then
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        If Mid(strTabela_Banco, 1, 3) = "DEP" Then
            vBanco.Parameters.Add "banco", Mid(strTabela_Banco, 1, 5), 1
        Else
            vBanco.Parameters.Add "banco", Mid(strTabela_Banco, 1, 8), 1
        End If
        
    Else
        vBanco.Parameters.Add "banco", "dep" & Trim(Format(lngCod_Loja, "00")), 1
    End If

    vBanco.ExecuteSQL vSql

    vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

    If vErro <> 0 Then
        MsgBox vBanco.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = vbDefault
        Exit Sub
    End If


    'Calcula Margem
    ' Call Margem

    ' lblMg_Reposicao.Text = Format(Margem_Repos_T, "#0")
    ' lblMg_Cuema.Text = Format(Margem_Cuema_T, "#0")



    Form_Load



    FrmAutorizacao.Show 1


    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", Mid(Trim(frmVisPedido.lblDeposito), 1, 2), 1
    vBanco.Parameters.Remove "pedido"
    vBanco.Parameters.Add "pedido", frmVisPedido.lblNr_Pedido, 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", frmVisPedido.lblSeq_Pedido, 1
    vBanco.Parameters.Remove "cod"
    vBanco.Parameters.Add "cod", frmVisPedido.lblDados1, 1
    vBanco.Parameters.Remove "cliente"
    vBanco.Parameters.Add "cliente", frmVisPedido.lblDados2, 1
    vBanco.Parameters.Remove "vl"
    vBanco.Parameters.Add "vl", frmVisPedido.lblVl_Contabil, 1
    vBanco.Parameters.Remove "usu_pol"
    vBanco.Parameters.Add "usu_pol", lngCod_Usuario, 1
    vBanco.Parameters.Remove "usu"
    vBanco.Parameters.Add "usu", lngCod_Usuario_R, 1
    vBanco.Parameters.Remove "mgc"
    vBanco.Parameters.Add "mgc", frmVisPedido.txtMargemCalculadaPedido, 1  'Hemerson - TI-4866
    'vBanco.Parameters.Add "mgc", frmVisPedido.lblMg_Cuema, 1
    vBanco.Parameters.Remove "mgr"
    vBanco.Parameters.Add "mgr", frmVisPedido.txtMargemPadraoPedido, 1  'Hemerson - TI-4866
    'vBanco.Parameters.Add "mgr", frmVisPedido.lblMg_Reposicao, 1



    vSql = "PRODUCAO.pck_vda060.pr_email_politica(:loja,:pedido,:seq,:cod,:cliente,:vl,:usu_pol,:usu,:mgc,:mgr)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)



    Unload Me
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    ElseIf Err = 440 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If

End Sub

Private Sub Form_Activate()
    'WILLIAM LEITE - TRACKER 148
    fra.Visible = False
    lngNUM_PEDIDO = lblNr_Pedido.Text
    lngSEQ_PEDIDO = lblSeq_Pedido.Text
    lngCod_Loja = Mid(Trim(lblDeposito.Text), 1, 2)

End Sub

Private Sub Form_Load()

    Me.Top = 1500
    Me.Left = 200



    On Error GoTo TrataErro

    'Dim ss As Object
    Dim vObjOracle2 As Object
    Dim vObjOracle3 As Object
    Dim vObjOracle4 As Object
    Dim vObjOracle5 As Object
    Dim vObjOracle6 As Object
    'Dim ss6 As Object
    'Dim ss7 As Object
    Dim vObjOracle8 As Object
    Dim CONT_L As Integer
    Dim cod As Long
    Dim plano As Long
    Dim sq As Byte        'sequencia de endere�o
    Dim lojavis As Integer
    Dim dt_uf_aliq As Date
    Dim strClass As String
    Dim lngCliente_Fiel As Long
    Dim i As Long
    Dim lngCod_Grupo As Long
    Dim lngCod_Subgrupo As Long
    Dim lngCod_Fornecedor As Long
    Dim lngCod_DPK As Long
    Dim strSQL As String

    'setar cursor
    Screen.MousePointer = vbHourglass
    fl_Tare = "N"
    frmVisPedido.Label14.Visible = False
    frmVisPedido.pctAlerta.Visible = False

    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_DADOSCABECALHO") 'TI-4160
    lblDescSuframa = 0
    'consulta dados do cabe�alho
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_DADOSCABECALHO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "CONSULTA ORACLE PEDIDO  " & lngNUM_PEDIDO & " FALHOU!!!", vbCritical, "ATEN��O"
        'setar cursor
        Screen.MousePointer = vbDefault
        Unload Me
        Exit Sub
    End If

    If vObjOracle!COD_VDR <> "0" Then
        lblVDR.Visible = True
    Else
        lblVDR.Visible = False
    End If
    lngCod_VDR = vObjOracle!COD_VDR

    If vObjOracle!cod_internet = "A" Then
        lblDPKNET.Visible = True
    Else
        lblDPKNET.Visible = False
    End If

    strClass = vObjOracle!classificacao
    dt_uf_aliq = Format(vObjOracle!dt_aliq_interna, "dd/mm/yy")
    If vObjOracle!fl_fora_semana = "S" Then
        PMF = vObjOracle!prazo_medio + 1 + lngDif_Semana
    Else
        PMF = vObjOracle!prazo_medio + 1
    End If

    'display dados na tela
    lblNr_Pedido.Text = vObjOracle!num_pedido
    lblSeq_Pedido.Text = vObjOracle!seq_pedido
    lblDt_Pedido.Text = Format$(vObjOracle!dt_pedido, "dd/mm/yy")
    lblNatureza.Text = vObjOracle!cod_nope
    lblCod_Filial.Text = vObjOracle!COD_FILIAL
    lblCod_Tp_Cli.Text = vObjOracle!cod_tipo_cliente
    lblDesc_Tipo.Text = vObjOracle!desc_tipo_cli
    lblSigla_Filial.Text = vObjOracle!sigla
    If IsNull(vObjOracle!mens_pedido) Then
        txtMsgPedido.Text = ""
    Else
        txtMsgPedido.Text = vObjOracle!mens_pedido
    End If
    If IsNull(vObjOracle!mens_nota) Then
        txtMsgNota.Text = ""
    Else
        txtMsgNota.Text = vObjOracle!mens_nota
    End If
    txtTpPedido.Text = vObjOracle!tp_pedido
    'strTipoRespCliente = ss!tipo

    If Trim$(vObjOracle!inscr_estadual) <> "ISENTO" And vObjOracle!fl_cons_final = "N" Then
        tp_cliente = "REVENDEDOR"
    ElseIf Trim$(vObjOracle!inscr_estadual) <> "ISENTO" And vObjOracle!fl_cons_final = "S" Then
        tp_cliente = "CONS.FINAL"
    ElseIf Trim$(vObjOracle!inscr_estadual) = "ISENTO" And vObjOracle!fl_cons_final = "N" Then
        tp_cliente = "ISENTO"
    Else
        tp_cliente = "REVENDEDOR"
    End If
    lblTare.Text = vObjOracle!cod_tare
    lblDt_tare.Caption = IIf(IsNull(vObjOracle!DT_TARE), "", vObjOracle!DT_TARE)
    ' If lblTare.TabIndex <> "0" Then
    If lblTare.Text <> "0" Then
        fl_Tare = "S"
    Else
        fl_Tare = "N"
    End If

    txtCOD_TRANSP.Text = vObjOracle!cod_transp
    lblNOME_TRANSP.Text = vObjOracle!nome_transp
    txtCOD_REPRESENTANTE.Text = vObjOracle!cod_repres
    txtSEQ_REPRES.Text = 0

    If vObjOracle!cod_repres = "0" Then
        lblPSEUDOREPR.Text = ""
    Else

        strSQL = "SELECT PSEUDONIMO FROM REPRESENTANTE WHERE COD_REPRES = " & txtCOD_REPRESENTANTE.Text
        vBanco.Parameters.Remove "PM_SQL"
        vBanco.Parameters.Add "PM_SQL", strSQL, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2


        vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle5 = vBanco.Parameters("PM_CURSOR1").Value


        ' Set vObjOracle5 = vVB_Venda_001.TabelaRepresentante(vBanco, txtCOD_REPRESENTANTE.Text)
        lblPSEUDOREPR.Text = IIf(IsNull(vObjOracle5("PSEUDONIMO")), "", vObjOracle5("PSEUDONIMO"))

    End If

    txtCOD_VENDEDOR.Text = vObjOracle!cod_vend

    If vObjOracle!cod_vend = "0" Then
        lblPSEUDOVEND.Text = ""
    Else


        strSQL = "SELECT PSEUDONIMO FROM REPRESENTANTE WHERE COD_REPRES = " & txtCOD_VENDEDOR.Text
        vBanco.Parameters.Remove "PM_SQL"
        vBanco.Parameters.Add "PM_SQL", strSQL, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2


        vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle5 = vBanco.Parameters("PM_CURSOR1").Value


        ' Set vObjOracle5 = vVB_Venda_001.TabelaRepresentante(vBanco, txtCOD_VENDEDOR.Text)
        lblPSEUDOVEND.Text = vObjOracle5("PSEUDONIMO")

    End If


    If vObjOracle!frete_pago = "C" Then
        optFOB.Visible = False
        optCIF.Visible = True
        optCIF.Value = 1
        optFOBD.Visible = False
    ElseIf vObjOracle!frete_pago = "F" Then
        optFOB.Visible = True
        optFOB.Value = 1
        optCIF.Visible = False
        optFOBD.Visible = False
    ElseIf vObjOracle!frete_pago = "D" Then
        optFOB.Visible = False
        optCIF.Visible = False
        optFOBD.Visible = True
        optFOBD.Value = 1
    End If


    If vObjOracle!cod_cliente = 0 Then
        lblDados1.Visible = False
        'lblDados2.AutoSize = True
        'lblDados2.Left = 555
        lblDados2.Text = "n�o selecionou cliente para este pedido"
        'lblDados3.Left = 555
        'lblDados3.BorderStyle = 0
        lblDados3.Text = "Uf"
        ln1.Visible = False
        'lblDados4.Left = 1275
        lblDados4.Text = vObjOracle!cod_uf
    Else
        lblDeposito.Text = vObjOracle!deposito
        UF_Destino = vObjOracle!cod_uf
        lblDados1.Text = vObjOracle!cod_cliente
        lblDados2.Text = vObjOracle!NOME_CLIENTE
        lblDados3.Text = vObjOracle!NOME_CIDADE
        ln1.X1 = lblDados3.Width + 1520
        ln1.X2 = ln1.X1 - 120
        'lblDados4.Left = lblDados3.Width + 1590
        lblDados4.Text = vObjOracle!cod_uf
        UF_Destino = lblDados4.Text
    End If

    If vObjOracle!fl_dif_icm = "S" Then
        bDifIcm = vbChecked
    Else
        bDifIcm = vbUnchecked
    End If

    chkDifIcm.Value = bDifIcm
    lblVl_Contabil = vObjOracle!vl_contabil
    txtCOD_PLANO.Text = vObjOracle!cod_plano
    lblDESC_PLANO.Text = Trim$(vObjOracle!DESC_PLANO) & " dd"
    If vObjOracle!pc_acrescimo > 0 Then
        lblDescAcres.Caption = "%Acrescimo"
        lblDesc_2.Visible = True
        txtDesc_2.Visible = True
        txtDesc_2.Text = 0
        txtPC_PLANO.Text = vObjOracle!pc_acrescimo
        sPC_ACRESCIMO_PLANO = vObjOracle!pc_acrescimo
    Else
        lblDesc_2.Visible = False
        txtDesc_2.Visible = False
        lblDescAcres.Caption = "%Desconto"
        txtPC_PLANO.Text = vObjOracle!pc_desconto
        sPC_DESCONTO_PLANO = vObjOracle!pc_desconto
    End If
    If lngCod_VDR = 0 Then
        txtPC_PLANO.Enabled = False
        txtDesc_2.Enabled = False
    Else
        txtPC_PLANO.Enabled = False
        txtDesc_2.Enabled = False
    End If

    If optFOB.Value = True And lblDescAcres.Caption = "%Acrescimo" Then
        lblDesc_2.Visible = True
        txtDesc_2.Visible = True
        txtDesc_2.Enabled = True
        txtDesc_2.Text = 0
    ElseIf optFOB.Value = True And lblDescAcres.Caption = "%Desconto" Then
        txtPC_PLANO.Enabled = True
    End If

    txtQtdeItens.Text = vObjOracle!qtd_item_pedido
    txtQtdeItens.Enabled = False


    'psergio 30/09/13
    w_cod_loja = Mid(Trim(vObjOracle!deposito), 1, 2)
    w_nat_oper = vObjOracle!cod_nope
    w_uf_dest = UF_Destino
 
    'gera arquivo texto
    ' Open arq_output For Output As #1
    ' Print #1, "NR.PEDIDO: " & lblNr_Pedido & "-" & lblSeq_Pedido
    ' Print #1, "FILIAL:    " & lblCod_Filial
    ' Print #1, "CLIENTE:   " & lblDados1 & " - " & lblDados2
    ' Print #1, "PLANO:     " & txtCOD_PLANO & " - " & lblDESC_PLANO
    ' If lblDescAcres.Caption = "%Acrescimo" Then
    '   Print #1, "ACRESCIMO: " & txtPC_PLANO
    ' Else
    '   Print #1, "DESCONTO:  " & txtPC_PLANO
    ' End If

    ''Aliquota interna
    'vBanco.Parameters.Remove "PM_UFORIGEM"
    'vBanco.Parameters.Add "PM_UFORIGEM", uf_origem, 1
    '
    'vBanco.Parameters.Remove "PM_UFDESTINO"
    'vBanco.Parameters.Add "PM_UFDESTINO", uf_destino, 1
    '
    'vBanco.Parameters.Remove "PM_CODDPK"
    'vBanco.Parameters.Add "PM_CODDPK", 0, 1
    '
    'vBanco.Parameters.Remove "PM_CURSOR1"
    'vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    'vBanco.Parameters("PM_CURSOR1").ServerType = 102
    'vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
    '
    'vBanco.Parameters.Remove "PM_CODERRO"
    'vBanco.Parameters.Add "PM_CODERRO", 0, 2
    'vBanco.Parameters.Remove "PM_TXTERRO"
    'vBanco.Parameters.Add "PM_TXTERRO", "", 2
    '
    'vSql = "PRODUCAO.pck_vda060.pr_CON_UF_ORIGEM_DESTINO(:PM_UFORIGEM,:PM_UFDESTINO,:PM_CODDPK,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
    '
    'vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    '
    'Set vObjOracle3 = vBanco.Parameters("PM_CURSOR1").Value
    '
    'If vObjOracle3.EOF Then
    '    Pc_Aliq_Interna = 0
    'Else
    '    Pc_Aliq_Interna = vObjOracle3!pc_icm
    'End If

    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_DADOSCABECALHO") 'TI-4160
    
    'Call fLogControle("IN�CIO:Pc_Aliq_Interna") 'TI-4160
    Pc_Aliq_Interna = Busca_Aliq_Interna(uf_origem, UF_Destino, 0)
    'Call fLogControle("FIM:Pc_Aliq_Interna") 'TI-4160
    
    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_DOLARUF") 'TI-4160

    'dolar uf(aliquota interna)
    vBanco.Parameters.Remove "PM_DTUF"
    vBanco.Parameters.Add "PM_DTUF", dt_uf_aliq, 1

    vBanco.Parameters.Remove "PM_DTFATURA"
    vBanco.Parameters.Add "PM_DTFATURA", Data_Faturamento, 1
    vBanco.Parameters.Remove "PM_PMF"
    vBanco.Parameters.Add "PM_PMF", PMF, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_DOLARUF(:PM_DTUF,:PM_DTFATURA,:PM_PMF,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle4 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle4.EOF Then
        Dolar_UF = 0
    Else
        Dolar_UF = vObjOracle4!Dolar_UF
        Dolar_Pmf = vObjOracle4!Dolar_Pmf
        '---------------
        'INDEXAR (IDP)
        '---------------
        Dolar_UF = (1 / (Dolar_Dia / Dolar_UF))
        Dolar_Pmf = (1 / (Dolar_Dia / Dolar_Pmf))
    End If

    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_DOLARUF") 'TI-4160
    
    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_FRETEMEDIO") 'TI-4160
    'Percentual Medio de Frete
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_UFDESTINO"
    vBanco.Parameters.Add "PM_UFDESTINO", UF_Destino, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_FRETEMEDIO(:PM_CODLOJA,:PM_UFDESTINO,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle4 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle4.EOF Then
        Pc_Frete = 0
    Else
        Pc_Frete = vObjOracle4!Pc_Frete
    End If

    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_FRETEMEDIO") 'TI-4160
    
    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_ICMS") 'TI-4160
    'Calculo ICMS
    vBanco.Parameters.Remove "PM_UFORIGEM"
    vBanco.Parameters.Add "PM_UFORIGEM", uf_origem, 1

    vBanco.Parameters.Remove "PM_UFDESTINO"
    If tp_cliente = "ISENTO" Then
        vBanco.Parameters.Add "PM_UFDESTINO", uf_origem, 1
    Else
        vBanco.Parameters.Add "PM_UFDESTINO", UF_Destino, 1
    End If

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_ICMS(:PM_UFORIGEM,:PM_UFDESTINO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle4 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle4.EOF Then
        Pc_ICMS = 0
    Else
        Pc_ICMS = vObjOracle4!pc_icm
        Pc_ICMS_Gerencial = vObjOracle4!pc_icm_gerencial
    End If
    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_ICMS") 'TI-4160
    
    'Call fLogControle("IN�CIO:Margem") 'TI-4160
    'Calcula Margem
    Call Margem
    If (fl_CNAE = "S") Then
        Exit Sub
    End If
    
    lblMg_Reposicao.Text = Format(Margem_Repos_T, "#0")
    lblMg_Cuema.Text = Format(Margem_Cuema_T, "#0")
    'Call fLogControle("FIM:Margem") 'TI-4160
    
    '  Print #1,
    '  Print #1, "MARGEM TOTAL"
    '  Print #1, "   REPOSICAO: " & lblMg_Reposicao.Text
    '  Print #1, "   CUEMA: " & lblMg_Cuema.Text
    '  Print #1,

    'BCAMPOS - 31/10/2013
    'POLITICA COMERCIAL - MARGEM
    'Call fLogControle("IN�CIO:Verificar_Margem_Pedido") 'TI-4160
    Call Verificar_Margem_Pedido
    'Call fLogControle("FIM:Verificar_Margem_Pedido") 'TI-4160
    
    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_DADOSITEM") 'TI-4160

    'consulta dados dos itens
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


    vSql = "PRODUCAO.pck_vda060.pr_CON_DADOSITEM(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle2 = vBanco.Parameters("PM_CURSOR1").Value

    Grid2.Rows = vObjOracle2.RecordCount + 1
    
    'BCAMPOS - 31/10/2013
    'POLITICA COMERCIAL - MARGEM
    'Grid2.Cols = 13
    Grid2.Cols = 14
    
    Grid2.FontBold = True

    Grid2.ColWidth(0) = 1000
    Grid2.ColWidth(1) = 800
    Grid2.ColWidth(2) = 700
    Grid2.ColWidth(3) = 600
    Grid2.ColWidth(4) = 1700
    Grid2.ColWidth(5) = 1500
    Grid2.ColWidth(6) = 600
    Grid2.ColWidth(7) = 500
    Grid2.ColWidth(8) = 600
    Grid2.ColWidth(9) = 600
    'WILLIAM LEITE - TRACKER 148
    'Grid2.ColWidth(10) = 600
    'Grid2.ColWidth(11) = 600
    Grid2.ColWidth(10) = 1
    Grid2.ColWidth(11) = 1
    Grid2.ColWidth(12) = 600

    'BCAMPOS - 31/10/2013
    'POLITICA COMERCIAL - MARGEM
    Grid2.ColWidth(13) = 1300

    Grid2.Row = 0

    'CABE�ALHO
    Grid2.Col = 0
    Grid2.Text = " Item"
    Grid2.Col = 1
    Grid2.Text = "Bloq."
    Grid2.Col = 2
    Grid2.Text = " Qtde"
    Grid2.Col = 3
    Grid2.Text = " Forn."
    Grid2.Col = 4
    Grid2.Text = "     Fabrica"
    Grid2.Col = 5
    Grid2.Text = "  Tabela"
    Grid2.Col = 6
    Grid2.Text = "%Per."
    Grid2.Col = 7
    Grid2.Text = "%UF"
    Grid2.Col = 8
    Grid2.Text = "%Adic."
    Grid2.Col = 9
    Grid2.Text = "%ICMS"
    'WILLIAM LEITE - TRACKER 148
    'Grid2.Col = 10
    'Grid2.Text = " MR"
    'Grid2.Col = 11
    'Grid2.Text = " MC"
    Grid2.Col = 12
    Grid2.Text = "TRIB"

    'BCAMPOS - 31/10/2013
    'POLITICA COMERCIAL - MARGEM
    Grid2.Col = 13
    Grid2.Text = "Mg. Bruta"

    ' Print #1, "ITEM;"; "BLOQ.;"; "QTDE;"; "FORN;"; "FABRICA;"; "TABELA;"; "%PER;" _
      '         ; "%UF;"; "%ADIC.;"; "%ICMS;"; "MR;"; "MC;"; "TRIB;"

    For CONT_L = 1 To vObjOracle2.RecordCount
        lngCod_DPK = IIf(IsNull(vObjOracle2!cod_dpk), 0, vObjOracle2!cod_dpk)

        'verifica se existe item VDR

        Grid2.Row = CONT_L
        Grid2.Col = 0
        'Grid2.ColAlignment(0) = 2

        If vObjOracle2!TABELA_VENDA Like "OR*" And vObjOracle2!pc_desc3 <> 0 Then
            Grid2.Picture = Picture1
        End If

        Grid2.Text = vObjOracle2!num_item_pedido
        Grid2.Col = 1
        If lngCod_VDR = 0 Then
        
            'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_CON_ITEMVDR - DPK:" & lngCod_DPK) 'TI-4160
            vBanco.Parameters.Remove "PM_CODDPK"
            vBanco.Parameters.Add "PM_CODDPK", lngCod_DPK, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2



            vSql = "PRODUCAO.pck_vda060.pr_CON_ITEMVDR(:PM_CODDPK,:PM_CURSOR1, :PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle8 = vBanco.Parameters("PM_CURSOR1").Value

            If vObjOracle8!total <> 0 Then
                Grid2.Picture = Picture2
            End If
            'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_ITEMVDR") 'TI-4160
                
        End If
        Grid2.Text = vObjOracle2!bloq_periodo & vObjOracle2!bloq_uf & vObjOracle2!bloq_adicional
        Grid2.Col = 2
        Grid2.ColAlignment(2) = 1
        Grid2.Text = vObjOracle2!qtd_atendida
        Grid2.Col = 3
        Grid2.ColAlignment(3) = 2
        Grid2.Text = vObjOracle2!cod_fornecedor
        Grid2.Col = 4
        Grid2.ColAlignment(4) = 0

        If vObjOracle2!preco_indevido = 1 And lblVDR.Visible = False Then
            Grid2.Picture = pctIndevido
            frmVisPedido.lblPreco.Visible = True
            frmVisPedido.pctIndevido.Visible = True
        End If



        Grid2.Text = vObjOracle2!sinal & vObjOracle2!cod_fabrica
        Grid2.Col = 5
        Grid2.ColAlignment(5) = 2
        If lngCod_VDR = 0 Then
            If vObjOracle2!tp_tab = "TAB.NAO VALIDA" Then
                Grid2.Picture = pctAlerta
                frmVisPedido.Label14.Visible = True
                frmVisPedido.pctAlerta.Visible = True
            End If
        End If
        Grid2.Text = vObjOracle2!TABELA_VENDA

        Grid2.Col = 6
        Grid2.ColAlignment(6) = 1
        Grid2.Text = vObjOracle2!pc_desc1
        Grid2.Col = 7
        Grid2.ColAlignment(7) = 1
        Grid2.Text = vObjOracle2!pc_desc2
        Grid2.Col = 8
        Grid2.ColAlignment(8) = 1
        Grid2.Text = vObjOracle2!pc_desc3
        Grid2.Col = 9
        Grid2.ColAlignment(9) = 1
        Grid2.Text = vObjOracle2!pc_dificm
        'WILLIAM LEITE - TRACKER 148
        'Grid2.Col = 11
        'Grid2.ColAlignment(9) = 2
        'Grid2.Text = Format(Margem_Cuema(vObjOracle2!num_item_pedido), "#0")
        'Grid2.Col = 10
        'Grid2.ColAlignment(10) = 2
        'Grid2.Text = Format(Margem_Repos(vObjOracle2!num_item_pedido), "#0")
        Grid2.Col = 12
        Grid2.ColAlignment(12) = 2
        Grid2.Text = Format(vObjOracle2!cod_trib)

        'BCAMPOS - 31/10/2013
        'POLITICA COMERCIAL - MARGEM
        Grid2.Col = 13
        Grid2.ColAlignment(13) = 2
        If Trim(vObjOracle2!bloq_adicional) = "" Then
            Grid2.Picture = pctMargemOK
            Grid2.Text = "OK"
        Else
            Grid2.Picture = pctMargemErro
            Grid2.Text = "VERIFICAR"
        End If
        
        ' Print #1, vObjOracle2!num_item_pedido & ";"; vObjOracle2!bloq_periodo & vObjOracle2!bloq_uf & vObjOracle2!bloq_adicional & ";"; _
          '    vObjOracle2!qtd_atendida & ";"; vObjOracle2!cod_fornecedor & ";"; _
          '    " " & vObjOracle2!sinal & vObjOracle2!cod_fabrica; ";"; vObjOracle2!TABELA_VENDA & ";" & vObjOracle2!pc_desc1 & ";" & vObjOracle2!pc_desc2 & ";" & _
          '    vObjOracle2!pc_desc3 & ";" & vObjOracle2!pc_dificm & ";" & Format(Margem_Repos(vObjOracle2!num_item_pedido), "#0") & ";" & _
          '    Format(Margem_Cuema(vObjOracle2!num_item_pedido), "#0") & ";" & vObjOracle2!cod_trib & ";"


        vObjOracle2.MoveNext
    Next
    ' Close #1
    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_CON_DADOSITEM") 'TI-4160
    
    'Call fLogControle("IN�CIO:PRODUCAO.pck_vda060.pr_VENDA_CASADA") 'TI-4160
    'verifica se pedido � venda casada
    ' tratamento incluso em 10/03/06
    'consulta dados do cabe�alho
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_VENDA_CASADA(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle6 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle6.EOF And vObjOracle6.BOF Then
        lblVenda_Casada.Visible = False
    Else
        If vObjOracle6(0) = 0 Then
            lblVenda_Casada.Visible = False
        Else
            lblVenda_Casada.Visible = True
        End If
    End If
    'Call fLogControle("FIM:PRODUCAO.pck_vda060.pr_VENDA_CASADA") 'TI-4160

    Screen.MousePointer = 0

    Grid2.Visible = True
    'setar cursor
    Screen.MousePointer = vbDefault

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    ElseIf Err = 364 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If



End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo TrataErro

    lngCod_VDR = 0

    'TI-4160
    SQL = "delete from vendas.controle" & IIf(vTipoCD = "M", "@LNK_PRODUCAO", "") & " where "
    SQL = SQL & " num_pedido = " & lngNUM_PEDIDO & " and "
    SQL = SQL & " seq_pedido = " & lngSEQ_PEDIDO & " and "
    SQL = SQL & " cod_loja = " & lngCod_Loja

    'fAtualizaVerificaErro (SQL) TI-4211
    FreeLocks
    'FIM TI-4160
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    Else
        '  Call Process_Line_Errors(SQL)
    End If
End Sub

Private Sub Grid2_DblClick()
    'BCAMPOS - 31/10/2013
    'POLITICA COMERCIAL - MARGEM
    If Grid2.Col <> 13 Then
        Grid2.Col = 0
        If lngCod_VDR = 0 Then
            frmItens.desc_vdr = False
            frmItens.Show 1
            '      Form_Load
        ElseIf lngCod_VDR = 1 Then 'Pedidos VDR psergio 30/09/13
        
            If Libera_Desconto_VDR(w_cod_loja, w_uf_dest, w_nat_oper) = True Then
            
                frmItens.desc_vdr = True
                frmItens.Show 1
                
            End If
            
            End If
    Else
        Grid2.Col = 0
        Call Verificar_Margem_Item
    End If
End Sub

Private Sub Grid2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        Grid2.Col = 0
        frmItens.Show 1
        '   Form_Load
    End If
End Sub

Private Sub MsgNota()
    Dim bTam As Byte
    bTam = Len(txtMsgNota.Text)

    If bTam <= 5 Then
        If optCIF.Value Then
            txtMsgNota.Text = "CIF. "
        ElseIf optFOB.Value Then
            txtMsgNota.Text = "FOB. "
        ElseIf optFOBD.Value Then
            txtMsgNota.Text = "CIFD."
        End If
    Else
        If optCIF.Value Then
            txtMsgNota.Text = "CIF. " & Mid$(txtMsgNota.Text, 6)
        ElseIf optFOB.Value Then
            txtMsgNota.Text = "FOB. " & Mid$(txtMsgNota.Text, 6)
        ElseIf optFOBD.Value Then
            txtMsgNota.Text = "CIFD." & Mid$(txtMsgNota.Text, 6)
        End If
    End If

End Sub

Private Sub optCIF_Click()
'mensagem da nota
    Call MsgNota
End Sub

Private Sub optCIF_GotFocus()
    optCIF.BackColor = Vermelho
End Sub

Private Sub optCIF_LostFocus()
    optCIF.BackColor = Cinza
End Sub

Private Sub optFOB_Click()
'mensagem da nota
    Call MsgNota
End Sub

Private Sub optFOB_GotFocus()
    optFOB.BackColor = Vermelho
End Sub

Private Sub optFOB_LostFocus()
    optFOB.BackColor = Cinza
End Sub

Private Sub optFOBD_Click()
'mensagem da nota
    Call MsgNota
End Sub

Private Sub optFOBD_GotFocus()
    optFOBD.BackColor = Vermelho
End Sub

Private Sub optFOBD_LostFocus()
    optFOBD.BackColor = Cinza
End Sub

Private Sub txtCOD_PLANO_DblClick()
    txtResposta = "0"
    frmPlano.Show vbModal
    If txtResposta <> "0" Then
        txtCOD_PLANO.Text = txtResposta
        Call txtCOD_PLANO_LostFocus
    End If
End Sub

Private Sub txtCOD_PLANO_GotFocus()
    txtCOD_PLANO.BackColor = Vermelho
    txtCOD_PLANO.DataChanged = False
End Sub

Private Sub txtCOD_PLANO_KeyPress(KeyAscii As Integer)
    If Asc(KeyAscii) = 49 And txtCOD_PLANO = "" Then
        Call txtCOD_PLANO_DblClick
    Else
        KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    End If
End Sub

Private Sub txtCOD_PLANO_LostFocus()

    On Error GoTo TrataErro

    txtCOD_PLANO.BackColor = Branco

    'carrega plano de pagamento
    vBanco.Parameters.Remove "PM_CODPLANO"
    vBanco.Parameters.Add "PM_CODPLANO", txtCOD_PLANO.Text, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_PLANOPAGTO(:PM_CODPLANO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        MsgBox "C�digo de plano " & txtCOD_PLANO.Text & " n�o cadastrado", vbInformation, "Aten��o"
        txtCOD_PLANO.Text = "0"
        txtCOD_PLANO.SetFocus
        Exit Sub
    ElseIf vObjOracle("SITUACAO") = 9 Then
        MsgBox "C�digo de plano " & txtCOD_PLANO.Text & " desativado", vbInformation, "Aten��o"
        txtCOD_PLANO.Text = "0"
        txtCOD_PLANO.SetFocus
        Exit Sub
    Else
        If lngCod_VDR <> 0 And vObjOracle!prazo_medio > 42 And vObjOracle!cod_plano <> 157 Then 'TI-5583
            vSql = "Este pedido � VDR, portanto "
            vSql = vSql & "n�o pode ser  utilizado prazo com PMF maior"
            vSql = vSql & " que 42 dias"
            MsgBox vSql, vbExclamation, "ATEN��O"
            Exit Sub
        End If
        lblDESC_PLANO.Text = Trim$(vObjOracle("DESC_PLANO")) & " dd"
        If (lngCod_VDR = 0 And vObjOracle!PC_ACRES_FIN_DPK > 0) Or (lngCod_VDR <> 0 And vObjOracle!PC_ACRES_FIN_BLAU > 0) Then
            lblDescAcres.Caption = "%Acrescimo"
            lblDesc_2.Visible = True
            txtDesc_2.Visible = True
            If lblNatureza.Text <> "J08" And _
               lblNatureza.Text <> "J12" And _
               lblNatureza.Text <> "B18" And _
               lblNatureza.Text <> "B26" Then
                If lngCod_VDR = 0 Then
                    txtPC_PLANO.Text = vObjOracle("PC_ACRES_FIN_DPK")
                    sPC_ACRESCIMO_PLANO = vObjOracle("PC_ACRES_FIN_DPK")
                    sPC_DESCONTO_PLANO = 0
                Else
                    txtPC_PLANO.Text = vObjOracle!PC_ACRES_FIN_BLAU
                    sPC_ACRESCIMO_PLANO = vObjOracle!PC_ACRES_FIN_BLAU
                    sPC_DESCONTO_PLANO = 0
                End If
            Else
                txtPC_PLANO.Text = 0
                sPC_ACRESCIMO_PLANO = 0
                sPC_DESCONTO_PLANO = 0
            End If
        Else
            lblDesc_2.Visible = False
            txtDesc_2.Visible = False
            lblDescAcres.Caption = "%Desconto"
            If lblNatureza.Text <> "J08" And _
               lblNatureza.Text <> "J12" And _
               lblNatureza.Text <> "B18" And _
               lblNatureza.Text <> "B26" Then

                If lngCod_VDR = 0 Then
                    txtPC_PLANO.Text = vObjOracle("PC_DESC_FIN_DPK")
                    sPC_DESCONTO_PLANO = vObjOracle("PC_DESC_FIN_DPK")
                    sPC_ACRESCIMO_PLANO = 0
                Else
                    txtPC_PLANO.Text = vObjOracle("PC_DESC_FIN_VDR")
                    sPC_DESCONTO_PLANO = vObjOracle("PC_DESC_FIN_VDR")
                    sPC_ACRESCIMO_PLANO = 0
                End If
            Else
                txtPC_PLANO.Text = 0
                sPC_DESCONTO_PLANO = 0
                sPC_ACRESCIMO_PLANO = 0
            End If
        End If

    End If

    If optFOB.Value = True Then
        lblDesc_2.Visible = True
        txtDesc_2.Visible = True
        txtDesc_2.Enabled = True
    Else
        lblDesc_2.Visible = False
        txtDesc_2.Visible = False
    End If

    If txtCOD_PLANO <> 0 Then
        vSql = "BEGIN "
        If strTipo_CD_Pedido = "U" Then 'DPK-74
            vSql = vSql & " UPDATE producao.PEDNOTA_VENDA SET"
        Else
            vSql = vSql & " UPDATE dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PEDNOTA_VENDA SET"
        End If
        vSql = vSql & " cod_plano = :plano,"
        vSql = vSql & " pc_desconto = :desc,"
        vSql = vSql & " pc_acrescimo = :acres"
        vSql = vSql & " WHERE SEQ_PEDIDO = :SEQ AND"
        vSql = vSql & " NUM_PEDIDO = :NUM AND"
        vSql = vSql & " COD_LOJA = :LOJA;"
        If strTipo_CD_Pedido = "U" Then 'DPK-74
            vSql = vSql & strTabela_Banco & "PR_CABEC(:loja,:NUM,:SEQ);"
        Else
            vSql = vSql & " dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PR_CABEC(:loja,:NUM,:SEQ);"
        End If
        vSql = vSql & " COMMIT;"
        vSql = vSql & "EXCEPTION"
        vSql = vSql & " WHEN OTHERS THEN"
        vSql = vSql & " ROLLBACK;"
        vSql = vSql & " :cod_errora := SQLCODE;"
        vSql = vSql & " :txt_errora := SQLERRM;"
        vSql = vSql & " END;"

        vBanco.Parameters.Remove "loja"
        vBanco.Parameters.Add "loja", lngCod_Loja, 1
        vBanco.Parameters.Remove "num"
        vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
        vBanco.Parameters.Remove "seq"
        vBanco.Parameters.Add "seq", lngSEQ_PEDIDO, 1
        vBanco.Parameters.Remove "plano"
        vBanco.Parameters.Add "plano", frmVisPedido.txtCOD_PLANO, 1
        vBanco.Parameters.Remove "desc"
        vBanco.Parameters.Remove "acres"
        If lblDescAcres.Caption = "%Desconto" Then
            vBanco.Parameters.Add "desc", txtPC_PLANO.Text, 1
            vBanco.Parameters.Add "acres", 0, 1
        Else
            vBanco.Parameters.Add "acres", txtPC_PLANO.Text, 1
            If txtDesc_2.Text = "" Then
                vBanco.Parameters.Add "desc", 0, 1
            ElseIf CDbl(txtDesc_2.Text) < 0.1 Then
                vBanco.Parameters.Add "desc", 0, 1
            Else
                vBanco.Parameters.Add "desc", txtDesc_2.Text, 1
            End If
        End If


        vBanco.Parameters.Remove "cod_errora"
        vBanco.Parameters.Add "cod_errora", 0, 2
        vBanco.Parameters.Remove "txt_errora"
        vBanco.Parameters.Add "txt_errora", "", 2

        vBanco.ExecuteSQL vSql

        vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

        If vErro <> 0 Then
            MsgBox "Erro na atualiza��o do plano de pagamento: " & vBanco.Parameters("txt_errora")
            Screen.MousePointer = vbDefault
            Exit Sub
        End If

        'BCAMPOS - CIT - 21/11/2013
        'POLITICA DE MARGEM - RECALCULO
        'Desabilita bot�o de libera��o se o plano de pagamento foi alterado
        If txtCOD_PLANO.DataChanged Then
            Command5.Enabled = False
        End If
            
    End If
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub

Private Sub txtCOD_REPRESENTANTE_Change()
    If txtCOD_REPRESENTANTE.Text = "" Then
        txtCOD_REPRESENTANTE.Text = 0
    End If
End Sub

Private Sub txtCOD_REPRESENTANTE_DblClick()

    txtResposta = "0"

    If txtResposta <> "0" Then
        txtCOD_REPRESENTANTE.Text = txtResposta
        Call txtCOD_REPRESENTANTE_LostFocus
    End If


End Sub

Private Sub txtCOD_REPRESENTANTE_GotFocus()
    txtCOD_REPRESENTANTE.BackColor = Vermelho
End Sub

Private Sub txtCOD_REPRESENTANTE_KeyPress(KeyAscii As Integer)

    If Asc(KeyAscii) = 49 And txtCOD_REPRESENTANTE = "" Then
        Call txtCOD_REPRESENTANTE_DblClick
    Else
        KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    End If

End Sub

Private Sub txtCOD_REPRESENTANTE_LostFocus()
    On Error GoTo TrataErro

    txtCOD_REPRESENTANTE.BackColor = Branco

    If txtCOD_REPRESENTANTE.DataChanged And txtCOD_VENDEDOR.Text <> "599" Then

        If ((txtTpPedido.Text = "21" Or txtTpPedido.Text = "22") And strTipoRespCliente = " ") _
           Or strTipoRespCliente = "V" _
           Or strTipoRespCliente = "M" Then
            txtCOD_REPRESENTANTE.Text = "0"
            txtSEQ_REPRES.Text = "0"
            lblPSEUDOREPR.Text = ""
        Else

            If strTipoRespCliente = " " Or _
               strTipoRespCliente = "R" Or _
               strTipoRespCliente = "P" Or _
               strTipoRespCliente = "V" Or _
               strTipoRespCliente = "M" Then

                vBanco.Parameters.Remove "PM_CODREPRES"
                vBanco.Parameters.Add "PM_CODREPRES", txtCOD_REPRESENTANTE.Text, 1

                vBanco.Parameters.Remove "PM_CURSOR1"
                vBanco.Parameters.Add "PM_CURSOR1", 0, 3
                vBanco.Parameters("PM_CURSOR1").ServerType = 102
                vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
                vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2

                vSql = "PRODUCAO.pck_vda060.pr_CON_REPRES(:PM_CODREPRES,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

                Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

                If vObjOracle.EOF And vObjOracle.BOF Then
                    MsgBox "Representante n�o cadastrado", vbInformation, "Aten��o"
                    txtCOD_REPRESENTANTE.Text = "0"
                    txtCOD_REPRESENTANTE.SetFocus
                    txtSEQ_REPRES.Text = "0"
                    lblPSEUDOREPR.Text = ""
                Else
                    lngCOD_PREPOSTO = txtCOD_REPRESENTANTE.Text
                    If vObjOracle("SEQ_FRANQUIA") = 0 Then
                        lblPSEUDOREPR.Text = vObjOracle("REPR_PSEUDO")
                        txtSEQ_REPRES.Text = "0"
                    Else
                        'preposto
                        txtCOD_REPRESENTANTE.Text = vObjOracle("PREP_COD_REPRES")
                        txtSEQ_REPRES.Text = vObjOracle("SEQ_FRANQUIA")
                        lblPSEUDOREPR.Text = vObjOracle("PREP_PSEUDO")
                    End If
                End If
                'If frmRepresentante.Visible = False Then
                '  txtMsgPedido.SetFocus
                'End If

            Else
                MsgBox "RESPONSAVEL PELO CLIENTE, TIPO [" & strTipoRespCliente & "]", vbCritical, "ATEN��O"
                Exit Sub
            End If

        End If

    End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub

Private Sub txtCOD_TRANSP_Change()
    Call MsgNota
End Sub

Private Sub txtCOD_TRANSP_DblClick()
    txtResposta = "0"
    txtCOD_TRANSP = "0"
    lblNOME_TRANSP = ""

    If txtResposta <> "0" Then
        txtCOD_TRANSP.Text = txtResposta
        Call txtCOD_TRANSP_LostFocus
    End If
End Sub

Private Sub txtCOD_TRANSP_GotFocus()
    txtCOD_TRANSP.BackColor = Vermelho
End Sub

Private Sub txtCOD_TRANSP_KeyPress(KeyAscii As Integer)
    If Asc(KeyAscii) = 49 And txtCOD_TRANSP = "" Then
        Call txtCOD_TRANSP_DblClick
    Else
        KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    End If
End Sub

Private Sub txtCOD_TRANSP_LostFocus()

    On Error GoTo TrataErro

    Dim ss As Object
    Dim SQL As String

    txtCOD_TRANSP.BackColor = Branco
    If txtCOD_TRANSP.DataChanged Then
        If txtCOD_TRANSP.Text = "" Or txtCOD_TRANSP.Text = "0" Then
            txtCOD_TRANSP.Text = "0"
            txtCOD_TRANSP.DataChanged = False
            lblNOME_TRANSP.Text = ""
        Else

            vBanco.Parameters.Remove "PM_CODTRANSP"
            vBanco.Parameters.Add "PM_CODTRANSP", txtCOD_TRANSP.Text, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "PRODUCAO.pck_vda060.pr_CON_TRANSPORTADORA(:PM_CODTRANSP,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

            If vObjOracle.EOF And vObjOracle.BOF Then
                MsgBox "C�digo de transportadora " & txtCOD_TRANSP.Text & " n�o cadastrado", vbInformation, "Aten��o"
                txtCOD_TRANSP.Text = "0"
                lblNOME_TRANSP.Text = ""
                txtCOD_TRANSP.SetFocus
                Exit Sub
            ElseIf vObjOracle("SITUACAO") = 9 Then
                MsgBox "C�digo da transportadora " & txtCOD_TRANSP.Text & " desativado", vbInformation, "Aten��o"
                txtCOD_TRANSP.Text = "0"
                lblNOME_TRANSP.Text = ""
                txtCOD_TRANSP.SetFocus
                Exit Sub
            Else
                lblNOME_TRANSP.Text = vObjOracle("NOME_TRANSP")
            End If

        End If

        'validar tipo de frete
        If CLng(txtCOD_TRANSP.Text) = 497 Then
            optFOB.Value = True
            optFOB.Enabled = True
            optFOBD.Enabled = False
            optCIF.Enabled = False


        ElseIf CLng(txtCOD_TRANSP.Text) = 1 Then
            optCIF.Value = True
            optCIF.Enabled = True
            optFOBD.Enabled = False
            optFOB.Enabled = False

        Else
            optCIF.Enabled = True
            optFOBD.Enabled = True
            optFOB.Enabled = True

        End If

    End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If

End Sub

Private Sub txtCOD_VENDEDOR_DblClick()
    txtCOD_VENDEDOR.DataChanged = False
    txtResposta = strTipoRespCliente

    If txtResposta <> strTipoRespCliente Then
        txtCOD_VENDEDOR.Text = txtResposta
        Call txtCOD_VENDEDOR_LostFocus
    End If
End Sub

Private Sub txtCOD_VENDEDOR_GotFocus()
    txtCOD_VENDEDOR.BackColor = Vermelho
End Sub

Private Sub txtCOD_VENDEDOR_KeyPress(KeyAscii As Integer)
    If Asc(KeyAscii) = 49 And txtCOD_VENDEDOR = "" Then
        Call txtCOD_VENDEDOR_DblClick
    Else
        KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    End If
End Sub

Private Sub txtCOD_VENDEDOR_LostFocus()

    Dim TPREPRES As String

    On Error GoTo TrataErro

    txtCOD_VENDEDOR.BackColor = Branco
    If txtCOD_VENDEDOR.DataChanged Then

        If CLng(txtCOD_VENDEDOR.Text) = 599 Then
            txtTpPedido.Text = "21"

            txtCOD_REPRESENTANTE.Text = "0"
            lblPSEUDOREPR.Text = ""

            '  Set vObjOracle = vVB_Venda_001.TabelaRepresentante(vBanco, 599)

            txtCOD_VENDEDOR = "599"
            lblPSEUDOVEND.Text = "S/ COMISSAO"

            'txtCOD_REPRESENTANTE.Enabled = False
            txtSEQ_REPRES.Enabled = False

        Else
            If txtTpPedido.Text = "10" Then
                'pedido tipo 10
                txtCOD_VENDEDOR.Text = "0"
                lblPSEUDOVEND.Text = ""
                If strTipoRespCliente <> "V" And strTipoRespCliente <> "M" Then
                    'txtCOD_REPRESENTANTE.Enabled = True
                    txtSEQ_REPRES.Enabled = True
                End If

            Else

                'pedido tipo 21 ou 22

                If strTipoRespCliente = " " Then
                    TPREPRES = "V"
                ElseIf strTipoRespCliente = "R" Or strTipoRespCliente = "P" Then
                    TPREPRES = "A"
                ElseIf strTipoRespCliente = "V" Or strTipoRespCliente = "M" Then
                    TPREPRES = "V"
                Else
                    MsgBox "RESPONSAVEL PELO CLIENTE, TIPO [" & strTipoRespCliente & "]", vbCritical, "ATEN��O"
                    Exit Sub
                End If

                vBanco.Parameters.Remove "PM_CODREPRES"
                vBanco.Parameters.Add "PM_CODREPRES", txtCOD_VENDEDOR.Text, 1

                vBanco.Parameters.Remove "PM_TPREPRES"
                vBanco.Parameters.Add "PM_TPREPRES", TPREPRES, 1

                vBanco.Parameters.Remove "PM_CURSOR1"
                vBanco.Parameters.Add "PM_CURSOR1", 0, 3
                vBanco.Parameters("PM_CURSOR1").ServerType = 102
                vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
                vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2

                vSql = "PRODUCAO.pck_vda060.pr_CON_VENDEDOR(:PM_CODREPRES,:PM_TPREPRES,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

                Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

                If vObjOracle.EOF And vObjOracle.BOF Then
                    MsgBox "Vendedor " & txtCOD_VENDEDOR.Text & " n�o cadastrado", vbInformation, "Aten��o"
                    txtCOD_VENDEDOR.Text = "0"
                    lblPSEUDOVEND.Text = ""
                    'txtCOD_VENDEDOR.SetFocus
                    Exit Sub
                Else
                    If IsNull(vObjOracle!PSEUDONIMO) Then
                        lblPSEUDOVEND.Text = ""
                    Else
                        lblPSEUDOVEND.Text = vObjOracle("PSEUDONIMO")
                    End If
                End If

            End If

        End If

    End If

    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If
End Sub

Private Sub txtDesc_2_GotFocus()
    txtDesc_2.BackColor = Vermelho
End Sub

Private Sub txtDesc_2_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtDesc_2.Text)

End Sub

Private Sub txtDesc_2_LostFocus()
    txtDesc_2.BackColor = Branco
    If txtDesc_2.Text = "" Then
        txtDesc_2.Text = "0"
    End If

    If CSng(txtDesc_2.Text) > 99 Then
        MsgBox "O Desconto deve ser menor que 100%", vbInformation, "Aten��o"
        txtDesc_2.SetFocus
        Exit Sub
    End If

    txtDesc_2.Text = FmtBR(txtDesc_2.Text)

    If optFOB.Value = True And CDbl(txtDesc_2.Text) > 2 Then
        MsgBox "O desconto para frete tipo FOB n�o pode ser maior que 2%", vbCritical, "Aten��o"
        txtDesc_2.SetFocus
        Exit Sub
    End If

End Sub

Private Sub txtMsgNota_GotFocus()
    txtMsgNota.BackColor = Vermelho


End Sub

Private Sub txtMsgNota_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtMsgNota_LostFocus()
    txtMsgNota = tira_caracter(txtMsgNota)
    vSql = "BEGIN "
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        vSql = vSql & " UPDATE producao.PEDNOTA_VENDA SET"
    Else
        vSql = vSql & " UPDATE dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PEDNOTA_VENDA SET"
    End If
    vSql = vSql & " MENS_NOTA = :MENSN"
    vSql = vSql & " WHERE SEQ_PEDIDO = :SEQ AND"
    vSql = vSql & " NUM_PEDIDO = :NUM AND"
    vSql = vSql & " COD_LOJA = :LOJA;"
    vSql = vSql & " COMMIT;"
    vSql = vSql & "EXCEPTION"
    vSql = vSql & " WHEN OTHERS THEN"
    vSql = vSql & " ROLLBACK;"
    vSql = vSql & " :cod_errora := SQLCODE;"
    vSql = vSql & " :txt_errora := SQLERRM;"
    vSql = vSql & " END;"

    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1
    vBanco.Parameters.Remove "num"
    vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSEQ_PEDIDO, 1
    vBanco.Parameters.Remove "MENSN"
    vBanco.Parameters.Add "MENSN", IIf(frmVisPedido.txtMsgNota = "", Null, frmVisPedido.txtMsgNota), 1
    vBanco.Parameters.Remove "cod_errora"
    vBanco.Parameters.Add "cod_errora", 0, 2
    vBanco.Parameters.Remove "txt_errora"
    vBanco.Parameters.Add "txt_errora", "", 2

    vBanco.ExecuteSQL vSql

    vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

    If vErro <> 0 Then
        MsgBox "Erro na atualiza��o na mensagem da NF : " & vBanco.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = vbDefault
        Exit Sub
    End If



    txtMsgNota.BackColor = Branco
End Sub

Private Sub txtMsgPedido_GotFocus()
    txtMsgPedido.BackColor = Vermelho
End Sub

Private Sub txtMsgPedido_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Texto(KeyAscii)
End Sub

Private Sub txtMsgPedido_LostFocus()
    txtMsgPedido = tira_caracter(txtMsgPedido)

    vSql = "BEGIN "
    If strTipo_CD_Pedido = "U" Then 'DPK-74
        vSql = vSql & " UPDATE producao.PEDNOTA_VENDA SET"
    Else
        vSql = vSql & " UPDATE dep" & Trim(Format(lngCod_Loja, "00")) & "." & "PEDNOTA_VENDA SET"
    End If
    vSql = vSql & " MENS_PEDIDO = :MENSP"
    vSql = vSql & " WHERE SEQ_PEDIDO = :SEQ AND"
    vSql = vSql & " NUM_PEDIDO = :NUM AND"
    vSql = vSql & " COD_LOJA = :LOJA;"
    vSql = vSql & " COMMIT;"
    vSql = vSql & "EXCEPTION"
    vSql = vSql & " WHEN OTHERS THEN"
    vSql = vSql & " ROLLBACK;"
    vSql = vSql & " :cod_errora := SQLCODE;"
    vSql = vSql & " :txt_errora := SQLERRM;"
    vSql = vSql & " END;"

    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1
    vBanco.Parameters.Remove "num"
    vBanco.Parameters.Add "num", lngNUM_PEDIDO, 1
    vBanco.Parameters.Remove "seq"
    vBanco.Parameters.Add "seq", lngSEQ_PEDIDO, 1
    vBanco.Parameters.Remove "MENSP"
    vBanco.Parameters.Add "MENSP", IIf(frmVisPedido.txtMsgPedido = "", Null, frmVisPedido.txtMsgPedido), 1
    vBanco.Parameters.Remove "cod_errora"
    vBanco.Parameters.Add "cod_errora", 0, 2
    vBanco.Parameters.Remove "txt_errora"
    vBanco.Parameters.Add "txt_errora", "", 2

    vBanco.ExecuteSQL vSql

    vErro = IIf(vBanco.Parameters("cod_errora") = "", 0, vBanco.Parameters("cod_errora"))

    If vErro <> 0 Then
        MsgBox "Erro na atualiza��o na mensagem do pedido: " & vBanco.Parameters("txt_errora") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = vbDefault
        Exit Sub
    End If




    txtMsgPedido.BackColor = Branco
End Sub

Private Sub txtPC_PLANO_GotFocus()
    txtPC_PLANO.BackColor = Vermelho
End Sub

Private Sub txtPC_PLANO_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtPC_PLANO)
End Sub

Private Sub txtPC_PLANO_LostFocus()
    Dim dblCalculo_Desconto As Double

    txtPC_PLANO.BackColor = Branco

    If txtPC_PLANO.DataChanged Then

        If txtPC_PLANO.Text = "" Then
            txtPC_PLANO.Text = "0"

        Else
            txtPC_PLANO.Text = FmtBR(txtPC_PLANO.Text)
            'If sPC_ACRESCIMO_PLANO > 0 Then
            'If CDbl(txtPC_PLANO.Text) < sPC_ACRESCIMO_PLANO Then
            '    MsgBox "Acrescimo deve ser maior ou igual a " & CStr(sPC_ACRESCIMO_PLANO), vbInformation, "Aten��o"
            '    txtPC_PLANO.Text = CStr(sPC_ACRESCIMO_PLANO)
            '    txtPC_PLANO.DataChanged = False
            'Else

            'End If
            'Else
            'desconto permite desconto ou � zero
            'validar entrada
            If CSng(txtPC_PLANO.Text) > 99 Then
                MsgBox "O Desconto deve ser menor que 100%", vbInformation, "Aten��o"
                txtPC_PLANO.Text = CStr(sPC_DESCONTO_PLANO)
                txtPC_PLANO.DataChanged = False
                'ElseIf txtPC_PLANO.DataChanged Then
                'If sPC_DESCONTO_PLANO = 0 And CSng(txtPC_PLANO) <> 0 Then
                '    MsgBox "O Desconto deve ser igual a zero", vbInformation, "Aten��o"
                '    txtPC_PLANO.Text = CStr(sPC_DESCONTO_PLANO)
                '    txtPC_PLANO.DataChanged = False
                'ElseIf CLng(txtPC_PLANO.Text) > sPC_DESCONTO_PLANO Then
                '    MsgBox "Desconto deve ser menor ou igual a " & CStr(sPC_DESCONTO_PLANO), vbInformation, "Aten��o"
                '    txtPC_PLANO.Text = CStr(sPC_DESCONTO_PLANO)
                '    txtPC_PLANO.DataChanged = False
                'Else

            End If
            dblCalculo_Desconto = 100 - ((100 - (100 * CDbl(sPC_DESCONTO_PLANO) / 100) - ((100 - 100 * CDbl(sPC_DESCONTO_PLANO) / 100) * 2 / 100)))

            If (lblDescAcres.Caption <> "%Acrescimo") And CDbl(txtPC_PLANO.Text) > CDbl(dblCalculo_Desconto) Then
                MsgBox "Desconto deve ser menor ou igual a " & CStr(dblCalculo_Desconto), vbInformation, "Aten��o"
                txtPC_PLANO.Text = CDbl(sPC_DESCONTO_PLANO)
                txtPC_PLANO.DataChanged = False
            End If
            'End If
            'End If

        End If

    End If


End Sub

Public Sub Verifica_Diesel(cod_loja, num_pedido, seq_pedido, cod_plano)
    Dim w_prazo_medio As Long
    Dim w_qtd_diesel As Long
    Dim dblDesconto As Double
    Dim dblAcrescimo As Double
    Dim strFl_avista As String
    Dim dblPC_Desc_Suframa As Double
    Dim i As Long
    Dim strFl_Vendor

    w_qtd_diesel = 0
    dblDesconto = 0


    'consulta dados do cabe�alho
    'tratamento de TEXACO retido conf. solic.
    ' do Celso em 05/04/06, carga em 13/04/06
    'vBanco.Parameters.Remove "PM_NUMPED"
    'vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    'vBanco.Parameters.Remove "PM_SEQPED"
    'vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    'vBanco.Parameters.Remove "PM_CODLOJA"
    'vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    'vBanco.Parameters.Remove "PM_CURSOR1"
    'vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    'vBanco.Parameters("PM_CURSOR1").ServerType = 102
    'vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    'vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    'vBanco.Parameters.Remove "PM_CODERRO"
    'vBanco.Parameters.Add "PM_CODERRO", 0, 2
    'vBanco.Parameters.Remove "PM_TXTERRO"
    'vBanco.Parameters.Add "PM_TXTERRO", "", 2

    'vBanco.Parameters.Remove "pm_loja_default"
    'vBanco.Parameters.Add "pm_loja_default", vCd, 1

    'vBanco.Parameters.Remove "pm_tp_bco"
    'vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    'vBanco.Parameters.Remove "pm_tp_bco_ped"
    'vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


    'vSql = "PRODUCAO.pck_vda060.pr_CON_TEXACO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,  :PM_CODERRO,:PM_TXTERRO)"

    'vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    'Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    'if Val(vObjOracle!total_texaco) <> 0 Then
    '  fl_TEXACO = "S"
    'Else
    fl_TEXACO = "N"
    'End If


    vBanco.Parameters.Remove "PM_CODPLANO"
    vBanco.Parameters.Add "PM_CODPLANO", cod_plano, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.pr_CON_PLANOPAGTO(:PM_CODPLANO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF And vObjOracle.BOF Then
        Exit Sub
    End If

    '------------------------------------------------------------------------
    ' Alexsandro de Macedo - 04/05/2011
    ' Branche: http://10.10.16.115/svn/dpk-comercial/branches/SDS_2334
    ' RC# 25:  http://10.10.16.115/tracker/dpk-comercial/requirementchange/25
    If CLng(vObjOracle!prazo_medio) <= 49 Then
    'If CLng(vObjOracle!prazo_medio) <= 60 Then
        fl_Diesel = "N"
    End If
    '------------------------------------------------------------------------

    w_prazo_medio = CLng(vObjOracle!prazo_medio)
    strFl_avista = vObjOracle!fl_avista
    strFl_Vendor = vObjOracle!fl_vendor

    If strFl_Vendor = "S" Then

        'DIESEL
        vBanco.Parameters.Remove "PM_NUMPED"
        vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

        vBanco.Parameters.Remove "PM_SEQPED"
        vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

        vBanco.Parameters.Remove "PM_CODLOJA"
        vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vBanco.Parameters.Remove "pm_loja_default"
        vBanco.Parameters.Add "pm_loja_default", vCD, 1

        vBanco.Parameters.Remove "pm_tp_bco"
        vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

        vBanco.Parameters.Remove "pm_tp_bco_ped"
        vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1



        vSql = "PRODUCAO.pck_vda060.pr_CON_DIESEL(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,  :PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

        If vObjOracle.EOF And vObjOracle.BOF Then
            fl_vendor_DIESEL = "N"
        Else
            If vObjOracle!total_diesel <> 0 Then
                fl_vendor_DIESEL = "S"
            Else
                fl_vendor_DIESEL = "N"
            End If
        End If

        If lngCod_VDR <> 0 Then
            fl_vendor_VDR = "S"
        Else
            fl_vendor_VDR = "N"
        End If
    End If

    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


    vSql = "PRODUCAO.pck_vda060.pr_CON_PEDNOTAVENDA(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    dblDesconto = sPC_DESCONTO_PLANO
    dblAcrescimo = sPC_ACRESCIMO_PLANO

    dblPC_Desc_Suframa = vObjOracle!pc_desc_suframa

    'verifica a existencia de itens VDR
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


    vSql = "PRODUCAO.pck_vda060.pr_CON_ITENSVDR(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If (vObjOracle.EOF And vObjOracle.BOF) Or CLng(vObjOracle!total) = 0 Then
        w_qtd_diesel = CLng(IIf(IsNull(vObjOracle!total), 0, vObjOracle!total))
    Else
        w_qtd_diesel = CLng(IIf(IsNull(vObjOracle!total), 0, vObjOracle!total))
    End If

    '------------------------------------------------------------------------
    ' Alexsandro de Macedo - 04/05/2011
    ' Branche: http://10.10.16.115/svn/dpk-comercial/branches/SDS_2334
    ' RC# 25:  http://10.10.16.115/tracker/dpk-comercial/requirementchange/25
    If w_prazo_medio > 49 And w_qtd_diesel > 0 And cod_plano <> 157 Then 'TI-5583
    'If w_prazo_medio > 60 And w_qtd_diesel > 0 Then
        fl_Diesel = "S"
    End If
    '------------------------------------------------------------------------

    If w_prazo_medio = 30 And w_qtd_diesel > 0 And _
       dblDesconto >= 1 Then
        fl_Diesel_Desconto30 = "S"
    End If

    If dblDesconto > 4 And w_qtd_diesel > 0 Then
        fl_Diesel_Desconto = "S"
    End If

    If cod_plano = 414 Then

        vBanco.Parameters.Remove "PM_NUMPED"
        vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

        vBanco.Parameters.Remove "PM_SEQPED"
        vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

        vBanco.Parameters.Remove "PM_CODLOJA"
        vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vBanco.Parameters.Remove "pm_loja_default"
        vBanco.Parameters.Add "pm_loja_default", vCD, 1

        vBanco.Parameters.Remove "pm_tp_bco"
        vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

        vBanco.Parameters.Remove "pm_tp_bco_ped"
        vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


        vSql = "PRODUCAO.pck_vda060.pr_CON_PLANO414(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default, :pm_tp_bco, :pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

        If vObjOracle!total <> 0 Then
            fl_Plano_414 = "S"
        End If

    End If



    'verifica a existencia de item PST
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1


    vSql = "PRODUCAO.pck_vda060.pr_CON_ITEMPST(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default, :pm_tp_bco, :pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle!total <> 0 Then

        If strFl_Vendor = "S" Then
            'COMENTADO EM 28/10 SOLICITA��O MARCELO HUDSON
            'fl_vendor_PST = "S"
            fl_vendor_PST = "N"
        Else
            fl_vendor_PST = "N"
        End If

        'n�o permitir desconto maior que 4.47 para avista PST
        If strFl_avista = "S" And dblDesconto > 11.16 Then
            fl_PSTAVista = "S"
        Else
            fl_PSTAVista = "N"
        End If

        'n�o permitir acresc. menor que 2.2 para pmf at� 60 PST
        '10/06/13 - WILLIAM LEITE SDS 2700 - SOLICITA��O MARCELO HUDSON
        'If w_prazo_medio >= 50 And w_prazo_medio <= 60 And _
        '   dblAcrescimo < 2.2 And strFl_Vendor = "N" Then
        '    fl_PSTAcrescimo = "S"
        'Else
         fl_PSTAcrescimo = "N"
        'End If

        If strFl_avista = "N" Then
            If w_prazo_medio >= 0 And w_prazo_medio <= 14 And _
               dblDesconto > 9.25 And strFl_Vendor = "N" Then
                fl_PST_Plano1 = "S"
            ElseIf w_prazo_medio >= 15 And w_prazo_medio <= 30 And _
                   dblDesconto > 4.47 And strFl_Vendor = "N" Then
                fl_PST_Plano5 = "S"
            ElseIf w_prazo_medio >= 31 And w_prazo_medio <= 48 And _
                   dblDesconto > 0 And strFl_Vendor = "N" Then
                fl_PST_Plano2 = "S"
            ElseIf w_prazo_medio = 49 And _
                   dblDesconto <> 0 And strFl_Vendor = "N" Then
                fl_PST_Plano3 = "S"
            '10/06/13 - WILLIAM LEITE SDS 2700 - SOLICITA��O MARCELO HUDSON
            'ElseIf w_prazo_medio > 60 And strFl_Vendor = "N" Then
            '    fl_PST_Plano4 = "S"
            End If
        End If

        'n�o permitir desconto suframa para PST
        If dblPC_Desc_Suframa <> 0 And strFl_Vendor = "N" Then
            fl_PSTSuframa = "S"
        Else
            fl_PSTSuframa = "N"
        End If
    End If

    'VERIFICA SE EXISTE DESCONTO PARA ITENS PST
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_DESCITEMPST(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco, :pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    For i = 1 To vObjOracle.RecordCount
        ' If ss!pc_desc1 > 0 Then
        '   fl_PST_Desc1 = "S"
        ' End If

        ' If ss!pc_desc2 > 0 Then
        '   fl_PST_Desc2 = "S"
        ' End If

        If vObjOracle!pc_desc3 > 0 Then
            fl_PST_Desc3 = "S"
        End If
        vObjOracle.MoveNext
    Next

End Sub

Public Sub Verifica_Fram(cod_loja, num_pedido, seq_pedido)
'Dim ss As Object
    Dim w_qtd_fram As Long
    Dim strCod_UF As String
    Dim strTipo_cliente As String

    w_qtd_fram = 0

    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_COUNTFARM(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    w_qtd_fram = vObjOracle!total

    If w_qtd_fram = 0 Then
        fl_Fram = "N"
    Else

        vBanco.Parameters.Remove "PM_NUMPED"
        vBanco.Parameters.Add "PM_NUMPED", num_pedido, 1

        vBanco.Parameters.Remove "PM_SEQPED"
        vBanco.Parameters.Add "PM_SEQPED", seq_pedido, 1

        vBanco.Parameters.Remove "PM_CODLOJA"
        vBanco.Parameters.Add "PM_CODLOJA", cod_loja, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vBanco.Parameters.Remove "pm_loja_default"
        vBanco.Parameters.Add "pm_loja_default", vCD, 1

        vBanco.Parameters.Remove "pm_tp_bco"
        vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

        vBanco.Parameters.Remove "pm_tp_bco_ped"
        vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

        vSql = "PRODUCAO.pck_vda060.pr_CON_UFPEDFARM(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco,:pm_tp_bco_ped, :PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

        If vObjOracle.EOF And vObjOracle.BOF Then
            fl_Fram = "N"
        Else
            strCod_UF = vObjOracle!cod_uf
            strTipo_cliente = vObjOracle!cod_tipo_cliente
            If strCod_UF = "RS" And strTipo_cliente = "50" Then
                fl_Fram = "S"
            End If
        End If

    End If


End Sub

Private Function Libera_Desconto_VDR(p_cod_loja As Long, p_uf_destino As String, p_natureza_oper As String) As Boolean
Dim p_nat_vendas As String
Dim vObjOracle9 As Object
Dim strSQL As String

    'Verifica se a opera��o � de vendas
    strSQL = "SELECT COD_NATOPE_SAP FROM SAP.NATUREZA_OPER_SAP WHERE COD_NATUREZA = '" & p_natureza_oper & "'"
    vBanco.Parameters.Remove "PM_SQL"
    vBanco.Parameters.Add "PM_SQL", strSQL, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_sql.pr_sql(:PM_CURSOR1,:PM_SQL,:PM_CODERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle9 = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle9.EOF And vObjOracle9.BOF Then
        p_nat_vendas = ""
    Else
        p_nat_vendas = vObjOracle9!cod_natope_sap
    End If

    'Somente pedidos de vendas tipo VDR e dentro do estado de PE
    If p_cod_loja = 12 And p_uf_destino = "PE" And p_nat_vendas = "V" Then
        Libera_Desconto_VDR = True
    End If

End Function

Public Sub Verificar_Margem_Item()

    On Error GoTo TrataErro

    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_ITEMPED"
    vBanco.Parameters.Add "PM_ITEMPED", frmVisPedido.Grid2.Text, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_LOJA_DEFAULT"
    vBanco.Parameters.Add "PM_LOJA_DEFAULT", vCD, 1

    vBanco.Parameters.Remove "PM_TP_BCO"
    vBanco.Parameters.Add "PM_TP_BCO", vTipoCD, 1

    vBanco.Parameters.Remove "PM_TP_BCO_PED"
    vBanco.Parameters.Add "PM_TP_BCO_PED", strTipo_CD_Pedido, 1

    'TI-4160
    vBanco.Parameters.Remove "PM_DT_PEDIDO"
    vBanco.Parameters.Add "PM_DT_PEDIDO", fFormataDataOracle(lblDt_Pedido), 1
    'FIM TI-4160
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.Pr_Con_Margem_Bruta(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_ITEMPED,:PM_CURSOR1,:PM_LOJA_DEFAULT,:PM_TP_BCO,:PM_TP_BCO_PED,:PM_DT_PEDIDO,:PM_CODERRO,:PM_TXTERRO)" 'TI-4160

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF Then
        MsgBox "N�o existe an�lise de pol�tica de margem para o item " & frmVisPedido.Grid2.Text & ".", vbExclamation, "Aten��o"
        Screen.MousePointer = vbDefault
    Else
        Call Carregar_Margem_Item(vObjOracle)
        frmMargemBruta.Show vbModal
    End If

    Exit Sub

TrataErro:

    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    ElseIf Err = 364 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If

End Sub

Public Sub Carregar_Margem_Item(vObjOracle As Object)
    
    frmVisPedido.Grid2.Col = 0
    frmMargemBruta.txtItem.Text = frmVisPedido.Grid2.Text
    
    frmVisPedido.Grid2.Col = 4
    frmMargemBruta.txtCodigoFabrica.Text = frmVisPedido.Grid2.Text

    frmMargemBruta.grdMargem.Visible = True
    frmMargemBruta.grdMargem.Rows = vObjOracle.RecordCount + 1
    frmMargemBruta.grdMargem.Cols = 5
    frmMargemBruta.grdMargem.FontBold = True
    
    frmMargemBruta.grdMargem.ColWidth(0) = 200
    frmMargemBruta.grdMargem.ColWidth(1) = 2400
    frmMargemBruta.grdMargem.ColWidth(2) = 1500
    frmMargemBruta.grdMargem.ColWidth(3) = 1200
    frmMargemBruta.grdMargem.ColWidth(4) = 2300

    frmMargemBruta.grdMargem.Row = 0

    frmMargemBruta.grdMargem.Col = 1
    frmMargemBruta.grdMargem.Text = "              Pol�tica"
    frmMargemBruta.grdMargem.Col = 2
    frmMargemBruta.grdMargem.Text = "%Per. Pol�tica"
    frmMargemBruta.grdMargem.Col = 3
    frmMargemBruta.grdMargem.Text = "%Per. Item"
    frmMargemBruta.grdMargem.Col = 4
    frmMargemBruta.grdMargem.Text = "              Status"

    For i = 1 To vObjOracle.RecordCount
    
        frmMargemBruta.grdMargem.Row = i
        
        frmMargemBruta.grdMargem.Col = 1
        frmMargemBruta.grdMargem.ColAlignment(1) = 2
        frmMargemBruta.grdMargem.Text = vObjOracle!descricao
                
        frmMargemBruta.grdMargem.Col = 2
        frmMargemBruta.grdMargem.ColAlignment(2) = 2
        frmMargemBruta.grdMargem.Text = IIf(vObjOracle!pc_analise_margem = 0, "-", vObjOracle!pc_analise_margem)
        
        frmMargemBruta.grdMargem.Col = 3
        frmMargemBruta.grdMargem.ColAlignment(3) = 2
        frmMargemBruta.grdMargem.Text = vObjOracle!pc_mg_bruta
                
        frmMargemBruta.grdMargem.Col = 4
        frmMargemBruta.grdMargem.ColAlignment(4) = 2
        
        If vObjOracle!fl_analise_bloqueio = "N" Then
            frmMargemBruta.grdMargem.Text = "Margem OK"
        Else
            frmMargemBruta.grdMargem.Text = "Margem n�o atendida"
        End If
        
        vObjOracle.MoveNext
    Next
    
End Sub

Public Sub Verificar_Margem_Pedido()

    On Error GoTo TrataErro
        
    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    'An�lise por pedido (Num_Item_Pedido = 0)
    vBanco.Parameters.Remove "PM_ITEMPED"
    vBanco.Parameters.Add "PM_ITEMPED", 0, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_LOJA_DEFAULT"
    vBanco.Parameters.Add "PM_LOJA_DEFAULT", vCD, 1

    vBanco.Parameters.Remove "PM_TP_BCO"
    vBanco.Parameters.Add "PM_TP_BCO", vTipoCD, 1

    vBanco.Parameters.Remove "PM_TP_BCO_PED"
    vBanco.Parameters.Add "PM_TP_BCO_PED", strTipo_CD_Pedido, 1
    
    'TI-4160
    vBanco.Parameters.Remove "PM_DT_PEDIDO"
    vBanco.Parameters.Add "PM_DT_PEDIDO", fFormataDataOracle(lblDt_Pedido), 1
    'FIM TI-4160
    
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "PRODUCAO.pck_vda060.Pr_Con_Margem_Bruta(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_ITEMPED,:PM_CURSOR1,:PM_LOJA_DEFAULT,:PM_TP_BCO,:PM_TP_BCO_PED,:PM_DT_PEDIDO,:PM_CODERRO,:PM_TXTERRO)" 'TI-4160

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    If vObjOracle.EOF Then
        fraMargemPedido.Visible = False
    Else
        fraMargemPedido.Top = 253
        fraMargemPedido.Left = 566
    
        fraMargemPedido.Visible = True
        lblMargemPadraoPedido.Visible = True
        txtMargemPadraoPedido.Visible = True
            
        txtMargemCalculadaPedido.Text = FmtBR(vObjOracle!pc_mg_bruta)
        txtMargemPadraoPedido.Text = FmtBR(vObjOracle!pc_analise_margem)
        
        If IsNull(vObjOracle!fl_analise_bloqueio) Then
            lblMargemPadraoPedido.Visible = False
            txtMargemPadraoPedido.Visible = False
            txtMargemCalculadaPedido.BackColor = Branco
        Else
            If Trim(vObjOracle!fl_analise_bloqueio) = "S" Then
                txtMargemCalculadaPedido.BackColor = Vermelho
            Else
                'Verde
                txtMargemCalculadaPedido.BackColor = &HC0FFC0
            End If
        End If
    End If

    Exit Sub

TrataErro:

    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
        Resume
    ElseIf Err = 364 Then
        Resume Next
    Else
        vVB_Generica_001.ProcessaErro Err.Description
    End If

End Sub
