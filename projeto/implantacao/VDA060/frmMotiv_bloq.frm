VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmMotiv_bloq 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Motivos de Bloqueio"
   ClientHeight    =   5310
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5460
   Icon            =   "frmMotiv_bloq.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   354
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   364
   Begin VB.CheckBox Check6 
      Appearance      =   0  'Flat
      Caption         =   "Margem"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   8
      Top             =   2880
      Width           =   2625
   End
   Begin VB.CheckBox Check5 
      Appearance      =   0  'Flat
      Caption         =   "Desconto de UF"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   7
      Top             =   2520
      Width           =   2625
   End
   Begin VB.CheckBox Check4 
      Appearance      =   0  'Flat
      Caption         =   "Desconto do Per�odo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   6
      Top             =   2160
      Width           =   2625
   End
   Begin VB.CheckBox Check3 
      Appearance      =   0  'Flat
      Caption         =   "Faturamento M�nimo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   5
      Top             =   1800
      Width           =   2625
   End
   Begin VB.CheckBox Check2 
      Appearance      =   0  'Flat
      Caption         =   "Acr�scimo do Plano"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   4
      Top             =   1440
      Width           =   2625
   End
   Begin VB.CheckBox Check1 
      Appearance      =   0  'Flat
      Caption         =   "Desconto do Plano"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   1170
      TabIndex        =   3
      Top             =   1080
      Width           =   2625
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4980
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   9578
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   5235
      _ExtentX        =   9234
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmMotiv_bloq.frx":23D2
      PICN            =   "frmMotiv_bloq.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Caption         =   "Para pedido feito via DPKNET verifique a margem"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   375
      Left            =   840
      TabIndex        =   10
      Top             =   240
      Width           =   4455
   End
   Begin VB.Label lblFat_min 
      Caption         =   $"frmMotiv_bloq.frx":30C8
      ForeColor       =   &H000000FF&
      Height          =   1215
      Left            =   360
      TabIndex        =   9
      Top             =   3360
      Width           =   4575
   End
End
Attribute VB_Name = "frmMotiv_bloq"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    Me.Top = 2000
    Me.Left = 700


    vBanco.Parameters.Remove "PM_NUMPED"
    vBanco.Parameters.Add "PM_NUMPED", lngNUM_PEDIDO, 1

    vBanco.Parameters.Remove "PM_SEQPED"
    vBanco.Parameters.Add "PM_SEQPED", lngSEQ_PEDIDO, 1

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", lngCod_Loja, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vBanco.Parameters.Remove "pm_loja_default"
    vBanco.Parameters.Add "pm_loja_default", vCD, 1

    vBanco.Parameters.Remove "pm_tp_bco"
    vBanco.Parameters.Add "pm_tp_bco", vTipoCD, 1

    vBanco.Parameters.Remove "pm_tp_bco_ped"
    vBanco.Parameters.Add "pm_tp_bco_ped", strTipo_CD_Pedido, 1

    vSql = "PRODUCAO.pck_vda060.pr_CON_MOTIVOBLOQUEIO(:PM_NUMPED,:PM_SEQPED,:PM_CODLOJA,:PM_CURSOR1,:pm_loja_default,:pm_tp_bco, :pm_tp_bco_ped,  :PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

    lblFat_min.Visible = False
    If vObjOracle!bloq_desconto <> "N" Then
        Check1.Value = 1
    End If

    If vObjOracle!bloq_acrescimo <> "N" Then
        Check2.Value = 1
    End If

    If vObjOracle!bloq_vlfatmin <> "N" Then
        Check3.Value = 1
        lblFat_min.Visible = True
    End If

    If vObjOracle!bloq_item_desc1 <> "N" Then
        Check4.Value = 1
    End If

    If vObjOracle!bloq_item_desc2 <> "N" Then
        Check5.Value = 1
    End If

    If vObjOracle!bloq_item_desc3 <> "N" Then
        Check6.Value = 1
    End If

End Sub

