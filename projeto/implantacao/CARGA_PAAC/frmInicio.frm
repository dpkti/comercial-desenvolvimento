VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmInicio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIL450 - Carga PAACs/Representantes"
   ClientHeight    =   5205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8970
   Icon            =   "frmInicio.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   8970
   StartUpPosition =   2  'CenterScreen
   Begin Bot�o.cmd cmdContinuar 
      Height          =   525
      Left            =   60
      TabIndex        =   1
      Top             =   4650
      Width           =   8865
      _ExtentX        =   15637
      _ExtentY        =   926
      BTYPE           =   3
      TX              =   "Clique aqui para continuar com o Processo de Atualiza��o/Carga"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   128
      FCOLO           =   128
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmInicio.frx":0CCA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.ListBox lstMensagem 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   4590
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   8895
   End
End
Attribute VB_Name = "frmInicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vBancoFIL240 As Database
Dim rst As Object

'Private Sub cmdIniciar_Click()
'
'1     On Error GoTo Trata_Erro
'
'2     Set vBancoFIL240 = OpenDatabase(vPath & "\CARGA\FIL240.MDB")
'3     Set rst = vBancoFIL240.CreateSnapshot("Select * from Programas")
'
'4     If rst.EOF = False Then CARGA_PROGRAMA
'
'5     lstMensagem.AddItem "Carga efetuada com sucesso !!"
'
'6     vBancoFIL240.Close
'7     Set vBancoFIL240 = Nothing
'8     Set rst = Nothing
'
'Trata_Erro:
'9     If Err.Number <> 0 Then
'10       MsgBox "Sub CMDINICIAR_CLICK" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
'11    End If
'End Sub


'Private Sub CARGA_PROGRAMA()
'1     On Error GoTo Trata_Erro
'
'2     Screen.MousePointer = 13
'
'3     lstMensagem.AddItem "Deletando OLD e Copiando EXE novos."
'4     Kill vPath & "\PGMS\*.OLD"
'5     Do While Not rst.EOF
'8        Name vPath & "\PGMS\" & rst!nome_programa & ".EXE" As vPath & "\PGMS\" & rst!nome_programa & ".OLD"
'10       FileCopy vPath & "\CARGA\" & rst!nome_programa & ".EXE", vPath & "\PGMS\" & rst!nome_programa & ".EXE"
'11       rst.MoveNext
'12    Loop
'
'22    Screen.MousePointer = 0
'
'23    Exit Sub
'
'Trata_Erro:
'24    If Err = 53 Then
'25       Resume Next
'26    ElseIf Err <> 0 Then
'29       MsgBox "Sub Carga_Programa" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
'30    End If
'
'End Sub

Private Sub cmdContinuar_Click()
    On Error GoTo Trata_Erro
    Me.Visible = False
    Shell vPath & "\pgms\fil050in.exe", 1
    End

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub cmdContinuar_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Private Sub Form_Load()
    Dim vRetorno As Boolean
    Dim vArqExe As String
    
1     On Error GoTo Trata_Erro
    
2     If App.PrevInstance = True Then
3       MsgBox "J� existe uma inst�ncia deste programa aberta."
4       End
5     End If
    
6     cmdContinuar.Enabled = False
    
7     Me.Visible = True
        
8     Me.Caption = Me.Caption & " - Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
    
9     vPath = Pegar_Drive
    
      'vPath = "c:"
    
10    If Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP")) <> "" Then
11       If MsgBox("Para a perfeita finaliza��o deste processo todas as " & vbCrLf & _
                   "op��es do Sistema DPK devem estar finalizadas. !!" & vbCrLf & _
                   "TODAS AS OP��ES DO SISTEMA EST�O FINALIZADAS ?", vbQuestion + vbYesNo, "Aten��o") = vbNo Then
12                 MsgBox "Primeiramente finalize todos os sistemas DPK " & Chr(13) & " e depois execute novamente este sistema.", 64, "ATEN��O!!!!!!!!!"
13          End
14       End If

15       If Processos("FIL010.EXE", "Digitacao de Pedido") = True Then End
16       If Processos("FIL020.EXE", "Conferencia de Pedido") = True Then End
17       If Processos("FIL030.EXE", "Telemarketing") = True Then End
18       If Processos("GER030.EXE", "Consulta Item") = True Then End
19       If Processos("FIL100.EXE", "Consulta Pedido") = True Then End
20    End If
    
21    If Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP")) = "" Then
22      lstMensagem.AddItem "N�o existe uma nova carga a ser feita, ou o arquivo foi gravado incorretamente!"
23      lstMensagem.AddItem "Clique no Bot�o abaixo para continuar..."
24      cmdContinuar.Enabled = True
25    Else
26      lstMensagem.AddItem "Descompactando Arquivo..." 'FIL240.MDB
27      Me.Refresh
28      Copiar_Descompactar_Carga_ZIP
29      lstMensagem.List(lstMensagem.ListCount - 1) = lstMensagem.List(lstMensagem.ListCount - 1) & "OK"
            
30      lstMensagem.AddItem "Verificar Copia de Seguran�a..."
31      Me.Refresh
32      COPIAR_MDB_RENOMEAR_MDB
33      lstMensagem.List(lstMensagem.ListCount - 1) = lstMensagem.List(lstMensagem.ListCount - 1) & "OK"

34      Do While Dir(vPath & "\Carga\*.EXE") <> ""

35         vArqExe = Dir(vPath & "\carga\*.exe")

36         If UCase(vArqExe) = "FIL240.EXE" Then
37            Name vPath & "\CARGA\FIL240.EXE" As vPath & "\CARGA\FIL240.RES"
38         Else
39            If Dir(vPath & "\PGMS\*.OLD") <> "" Then Kill vPath & "\PGMS\*.OLD"
'42            If Dir(vPath & "\PGMS\" & Mid(vArqExe, 1, Len(vArqExe) - 4) & ".OLD") <> "" Then Kill vPath & "\PGMS\" & Mid(vArqExe, 1, Len(vArqExe) - 4) & ".OLD"
40            If Dir(vPath & "\PGMS\" & vArqExe) <> "" Then Name vPath & "\PGMS\" & vArqExe As vPath & "\PGMS\" & Mid(vArqExe, 1, Len(vArqExe) - 4) & ".OLD"
41            FileCopy vPath & "\CARGA\" & vArqExe, vPath & "\PGMS\" & vArqExe
42            Kill vPath & "\CARGA\" & vArqExe
43         End If
           
44      Loop
        
45      If Dir(vPath & "\CARGA\FIL240.RES") <> "" Then
46          Name vPath & "\CARGA\FIL240.RES" As vPath & "\CARGA\FIL240.EXE"
47      Else
48          If Dir(vPath & "\CARGA\FIL240.EXE") = "" Then Name vPath & "\CARGA\FIL240.OLD" As vPath & "\CARGA\FIL240.EXE"
49      End If

50      lstMensagem.AddItem "Verificando Usu�rios Conectados..."
51      Me.Refresh
52      If Dir(vPath & "\DADOS\BASE_DPK.LDB") <> "" Then
53         frmConectados.Show 1
54      End If
55      lstMensagem.List(Me.lstMensagem.ListCount - 1) = lstMensagem.List(Me.lstMensagem.ListCount - 1) & "OK"
        
56      lstMensagem.AddItem "Executando Mover De / Para..."
57      Me.Refresh
58      Mover_De_Para
            
59      lstMensagem.AddItem "Executando Comandos SQL..."
60      Me.Refresh
61      Comandos_SQL
            
62      lstMensagem.AddItem "Executando Arquivos .BAT..."
63      Me.Refresh
64      Executar_Bat
            
65      lstMensagem.AddItem "Executando Arquivos .EXE..."
66      Me.Refresh
67      Executar_EXE
    
68    End If
    
69    Gerar_log
    
70    cmdContinuar.Visible = True
71    cmdContinuar.Enabled = True
    
Trata_Erro:
72    If Err.Number <> 0 Then
73       If Err.Number = 55 Then
74          MsgBox "O programa " & vArqExe & " est� sendo usado.", vbInformation, "Aten��o"
75          Exit Sub
76       End If

77       MsgBox "Sub FrmInicio_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
         
78       If Dir(vPath & "\CARGA\FIL240.RES") <> "" Then
79          Name vPath & "\CARGA\FIL240.RES" As vPath & "\CARGA\FIL240.EXE"
80       Else
81          If Dir(vPath & "\CARGA\FIL240.EXE") = "" Then Name vPath & "\CARGA\FIL240.OLD" As vPath & "\CARGA\FIL240.EXE"
82       End If
83    End If
End Sub

Sub Copiar_Descompactar_Carga_ZIP()

1         On Error GoTo Trata_Erro

      Dim vGuarda As String
      Dim vCopiarDe As String
      Dim vCopiarPara As String
      Dim vExiste As String
      Dim vArquivo As String
      Dim vArq As String
    
2     vGuarda = Trim(Dir(vPath & "\COMUNICA\IN\CAR*.ZIP"))
3     If vGuarda <> "" Then
4        vCopiarDe = vPath & "\COMUNICA\IN\" & vGuarda
5        vCopiarPara = vPath & "\CARGA\" & vGuarda
6        FileCopy vCopiarDe, vCopiarPara
                   
7        While vArq = ""
8           vArq = Dir(vPath & "\CARGA\" & vGuarda)
9        Wend
10       vExiste = Trim(Dir(vPath & "\CARGA\CAR*.ZIP"))
11       If vExiste = "" Then
12          MsgBox "N�o existe uma nova carga a ser feita, ou o arquivo foi gravado incorretamente!"
13          Exit Sub
14       Else
15          vArquivo = vExiste
16       End If
                 
17       If Dir(vPath & "\CARGA\FIL240.OLD") <> "" Then Kill vPath & "\CARGA\FIL240.OLD"
18       If Dir(vPath & "\CARGA\FIL240.EXE") <> "" Then Name vPath & "\CARGA\FIL240.EXE" As vPath & "\CARGA\FIL240.OLD"
        
        'Nova Rotina para Descompacta��o do Carga.
19      DesCompacta vPath & "\CARGA\" & vGuarda, "*.*", vPath & "\CARGA\", False
20      If Dir(vPath & "\CARGA\" & vGuarda) <> "" Then
21          Kill vPath & "\CARGA\CAR*.ZIP"
22      End If
'19       RunCmd vPath & "\CARGA\" + "UNZIPA.BAT " + vArquivo, vbNormalFocus

23    Else
24       MsgBox "N�o existe uma nova carga a ser feita, ou o arquivo foi gravado incorretamente!"
25    End If
        
Trata_Erro:
26    If Err.Number <> 0 Then
27       MsgBox "Sub: Copiar_Descompactar_Carga_ZIP" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
28       frmInicio.lstMensagem.AddItem Space(5) & "Erro na Sub Copiar_Descompactar_Carga_ZIP " & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
29    End If
End Sub


Sub COPIAR_MDB_RENOMEAR_MDB()
          
1         On Error GoTo Trata_Erro
          
          Dim SS As Object
          Dim Banco As Database
          
          '**********************************************************************************************
          'Copia o arquivo de C:\DADOS\BASE_DPK.MDB para C:\CARGA\BASE_DPK.MDB e ser� com o arquivo neste
          'diretorio que ser�o feitas as cargas.
          '**********************************************************************************************
2         If Dir(vPath & "\CARGA\BASE_DPK.MDB") <> "" Then Kill vPath & "\CARGA\BASE_DPK.MDB"
          
          '***********************************************************
          'Somente fazer a copia da base se houver alteracoes no Banco
          '***********************************************************
3         If Dir(vPath & "\CARGA\FIL240.MDB") <> "" Then
4             Set Banco = OpenDatabase(vPath & "\CARGA\FIL240.MDB")
          
5             Set SS = Banco.CreateSnapshot("SELECT * FROM CARGA")
6             FreeLocks
7             If SS.RecordCount > 0 Then
8                 If SS!tipo2 = "S" Or SS!tipo3 = "S" Or SS!tipo4 = "S" Or SS!tipo6 = "S" Or SS!tipo5 = "S" Or SS!TIPO7 = "S" Then
9                     lstMensagem.AddItem "Fazendo C�pia da Base de Dados..."
10                    Me.Refresh
11                    FileCopy vPath & "\DADOS\BASE_DPK.MDB", vPath & "\CARGA\BASE_DPK.MDB"
                      'Name vPath & "\DADOS\BASE_DPK.MDB" As vPath & "\DADOS\BASE_DPK.CAR"
12                    lstMensagem.List(Me.lstMensagem.ListCount - 1) = lstMensagem.List(Me.lstMensagem.ListCount - 1) & "OK"
13                Else
14                    lstMensagem.AddItem "N�o h� necessidade de fazer a c�pia do Banco."
15                End If
16            Else
17                lstMensagem.AddItem "N�o h� necessidade de fazer a c�pia do Banco."
18            End If
19            Banco.Close
          
20        Else
21            lstMensagem.AddItem "N�o h� necessidade de fazer a c�pia do Banco."
22        End If
          
23        Set Banco = Nothing
24        Set SS = Nothing

Trata_Erro:
25        If Err.Number <> 0 Then
26            MsgBox "Sub Copiar_MDB_Ronemear_MDB" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
27        End If

End Sub

Sub Gerar_log()
    On Error Resume Next
    
    Dim vArq As Integer
    Dim i As Integer
    
    vArq = FreeFile
    
    If Dir(vPath & "\FIL450.LOG") <> "" Then
        Open vPath & "\FIL450.LOG" For Append As #vArq
    Else
        Open vPath & "\FIL450.LOG" For Output As #vArq
    End If
    
    For i = 0 To lstMensagem.ListCount - 1
        Print #vArq, Now & " - " & Me.lstMensagem.List(i)
    Next
    
    Close #vArq
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = -1
    End If
End Sub

Sub Mover_De_Para()
1     On Error GoTo Trata_Erro
          
    Dim i As Integer
    Dim vDePara As String
    Dim vMoverDePara() As String

2     For i = 1 To 1000 'Arquivos
3       vDePara = GetKeyVal(vPath & "\CARGA\PAAC.INI", "MOVER", "MOVER" & i)

4       If Left(UCase(vDePara), 10) <> "EXECUTADO" Then

5          If vDePara = "" Then Exit Sub

6          vMoverDePara = Split(vDePara, ";")

7          If InStr(1, vMoverDePara(0), "EXECUTADO") > 0 Then GoTo Proximo

8          If Dir(vMoverDePara(0)) = "" Then
9              MsgBox "O Arquivo " & vMoverDePara(0) & " N�O foi encontrado." & vbCrLf & "Os arquivos n�o foram Movidos." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
10             frmInicio.lstMensagem.AddItem Space(5) & "O Arquivo " & vMoverDePara(0) & " N�O foi encontrado. Os arquivos n�o foram Movidos e o Processo foi Finalizado."
11             Exit Sub
12         End If
        
13         frmInicio.lstMensagem.AddItem Space(5) & "Movendo arquivo DE: " & vMoverDePara(0) & " Para: " & vMoverDePara(1)

14         If Dir(vMoverDePara(0), vbArchive) = "" Then
15             MsgBox "O Arquivo " & vMoverDePara(0) & " N�O foi encontrado." & vbCrLf & "Os arquivos n�o foram Movidos." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
16             frmInicio.lstMensagem.AddItem Space(5) & "O Arquivo " & vMoverDePara(0) & " N�O foi encontrado. Os arquivos n�o foram Movidos e o Processo foi finalizado."
17             Exit Sub
18         Else
19            If Dir(Mid(vMoverDePara(1), 1, Len(vMoverDePara(1)) - 4) & ".OLD") <> "" Then Kill Mid(vMoverDePara(1), 1, Len(vMoverDePara(1)) - 4) & ".OLD"
20            If Dir(vMoverDePara(1)) <> "" Then Name vMoverDePara(1) As Mid(vMoverDePara(1), 1, Len(vMoverDePara(1)) - 4) & ".OLD"
              
21            FileCopy vMoverDePara(0), vMoverDePara(1)
22            If vMoverDePara(2) = "SIM" Then Kill vMoverDePara(0)
23         End If

24         If Left(UCase(vDePara), 10) <> "EXECUTADO " Then
25            SetKeyVal vPath & "\CARGA\PAAC.INI", "MOVER", "MOVER" & i, "EXECUTADO " & vDePara
26         End If
27      End If
Proximo:
28    Next
                
Trata_Erro:
29    If Err.Number <> 0 Then
30      MsgBox "Sub Mover_De_Para" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
31      frmInicio.lstMensagem.AddItem Space(5) & "Erro na Execu��o do Mover De / Para."
32    End If
    
End Sub

