VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmConectados 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Usu�rios Conectados"
   ClientHeight    =   3960
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4905
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmConectados.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   264
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   327
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox LstConectados 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1950
      Left            =   30
      TabIndex        =   2
      Top             =   1920
      Width           =   4845
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   4845
      _ExtentX        =   8546
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConectados.frx":23D2
      PICN            =   "frmConectados.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Solicite que os usu�rios listados abaixo fechem o sistema. Depois disso tente efetuar a carga novamente."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   885
      Left            =   60
      TabIndex        =   3
      Top             =   930
      Width           =   4785
   End
End
Attribute VB_Name = "frmConectados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdVoltar_Click()

    Unload Me
    
    If LstConectados.ListCount > 0 Then End

End Sub

Private Sub Form_Load()
    Dim vArq As Integer
    Dim vNome As String
    Dim vConteudo As String
    
    vArq = FreeFile
    
    frmConectados.LstConectados.Clear
    
    If Dir(Path_Drv & "\VENDAS.LDB") <> "" Then
        
        vArq = FreeFile
        Open Path_Drv & "\vendas.LDB" For Input As #vArq
            
        Input #vArq, vConteudo
        
        While Trim(vConteudo) <> ""
            vNome = Left(vConteudo, 36)
            vConteudo = Mid(vConteudo, 37)
            LstConectados.AddItem vNome
        Wend
        
    End If
    
    Close #vArq
    
End Sub
