Attribute VB_Name = "modPAAC"
Option Explicit

Public vPath As String
'RunCMD
Private Const INFINITE = &HFFFF
Private Const SYNCHRONIZE = &H100000
Private Declare Sub WaitForSingleObject Lib "kernel32.dll" (ByVal hHandle As Long, ByVal dwMilliseconds As Long)
'Fim RunCMD

Private Type PROCESSENTRY32
    dwSize As Long
    cntUsage As Long
    th32ProcessID As Long
    th32DefaultHeapID As Long
    th32ModuleID As Long
    cntThreads As Long
    th32ParentProcessID As Long
    pcPriClassBase As Long
    dwFlags As Long
    szexeFile As String * 260
End Type

Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, ByVal blnheritHandle As Long, ByVal dwAppProcessId As Long) As Long
Private Declare Function ProcessFirst Lib "kernel32.dll" Alias "Process32First" (ByVal hSnapshot As Long, uProcess As PROCESSENTRY32) As Long
Private Declare Function ProcessNext Lib "kernel32.dll" Alias "Process32Next" (ByVal hSnapshot As Long, uProcess As PROCESSENTRY32) As Long
Private Declare Function CreateToolhelpSnapshot Lib "kernel32.dll" Alias "CreateToolhelp32Snapshot" (ByVal lFlags As Long, lProcessID As Long) As Long
Private Declare Function TerminateProcess Lib "kernel32.dll" (ByVal ApphProcess As Long, ByVal uExitCode As Long) As Long
Private Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Private Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal filename$)


Public Function Processos(NameProcess As String, Programa As String) As Boolean
1         Processos = False
          Const PROCESS_ALL_ACCESS = 0
          Const TH32CS_SNAPPROCESS As Long = 2&
          Dim uProcess  As PROCESSENTRY32
          Dim RProcessFound As Long
          Dim hSnapshot As Long
          Dim SzExename As String
          Dim ExitCode As Long
          Dim MyProcess As Long
          Dim AppKill As Boolean
          Dim AppCount As Integer
          Dim i As Integer
          Dim WinDirEnv As String
              
2         If NameProcess <> "" Then
3            AppCount = 0

4            uProcess.dwSize = Len(uProcess)
5            hSnapshot = CreateToolhelpSnapshot(TH32CS_SNAPPROCESS, 0&)
6            RProcessFound = ProcessFirst(hSnapshot, uProcess)
        
7            Do
8                 i = InStr(1, uProcess.szexeFile, Chr(0))
9                 SzExename = LCase$(Left$(uProcess.szexeFile, i - 1))
10                WinDirEnv = Environ("Windir") + "\"
11                WinDirEnv = LCase$(WinDirEnv)
              
12                If Right$(SzExename, Len(NameProcess)) = LCase$(NameProcess) Then
13                   MsgBox "Voc� tem que fechar o Programa: " & Programa
14                   Processos = True
15                   Exit Function
                     'AppCount = AppCount + 1
                     'MyProcess = OpenProcess(PROCESS_ALL_ACCESS, False, uProcess.th32ProcessID)
                     'AppKill = TerminateProcess(MyProcess, ExitCode)
16                   Call CloseHandle(MyProcess)
17                End If
18                RProcessFound = ProcessNext(hSnapshot, uProcess)
19           Loop While RProcessFound
20           Call CloseHandle(hSnapshot)
21        End If
          
End Function

Sub Comandos_SQL()

1         On Error GoTo Trata_Erro
          Dim i As Integer, ii As Integer
          Dim Banco As String
          Dim Comando As String
          Dim vBanco As Database
          Dim vSeqComando As Integer
          Dim vRetorno As Boolean
          
2         frmInicio.lstMensagem.AddItem "Gerando arquivo BASE_DPK.ZIP..."
3         'vRetorno = frmInicio.MaqZip1.ZipCompress(vPath & "\DADOS", "BASE_DPK.MDB", vPath & "\DADOS", "BASE_DPK.ZIP", "-6")
          Compacta vPath & "\DADOS\BASE_DPK.ZIP", vPath & "\DADOS\BASE_DPK.MDB"
          
4         'If vRetorno = False Then
5         '   frmInicio.lstMensagem.AddItem "Arquivo BASE_DPK.MDB n�o foi zipado."
6         'Else
7         '   frmInicio.lstMensagem.List(frmInicio.lstMensagem.ListCount - 1) = frmInicio.lstMensagem.List(frmInicio.lstMensagem.ListCount - 1) & "OK"
8         'End If
          
9         vSeqComando = 1
          
10        For i = 1 To 1000 'BANCOS
11            Banco = GetKeyVal(vPath & "\CARGA\PAAC.INI", "SQL", "BANCO" & i)
12            If Banco = "" Then Exit Sub
13            If Dir(vPath & "\DADOS\" & Banco) = "" Then
14               MsgBox "O Arquivo " & Banco & " N�o foi encontrado." & vbCrLf & "Comandos SQL n�o foram executados." & vbCrLf & "Entre em contato com o Helpdesk.", vbInformation, "Aten��o"
15               frmInicio.lstMensagem.AddItem Space(5) & "O Arquivo " & Banco & " N�o foi encontrado. Comandos SQL n�o foram executados."
16               Exit Sub
17            End If
18            Set vBanco = OpenDatabase(vPath & "\DADOS\" & Banco, , , True)

19            If Dir(vPath & "\DADOS\" & Banco) = "" Then
20               MsgBox "Sub Comandos_SQL" & vbCrLf & "Descri��o: Arquivo " & vPath & "\DADOS\" & Banco & " n�o econtrado.", vbInformation, "Aten��o"
21               Exit Sub
22            End If
              
23            Comando = GetKeyVal(vPath & "\CARGA\PAAC.ini", "SQL", "SQL" & i)
24            If Comando = "" Then Exit For
25            If Left(UCase(Comando), 10) <> "EXECUTADO " Then
26               frmInicio.lstMensagem.AddItem Space(5) & "Executando: " & Banco & " - " & Comando
27               vBanco.Execute Comando
28               frmInicio.lstMensagem.AddItem Space(5) & "Comando acima executado com Sucesso !"
29               SetKeyVal vPath & "\CARGA\PAAC.INI", "SQL", "SQL" & i, "EXECUTADO " & Comando
30            End If

31            vBanco.Close
32            Set vBanco = Nothing
33       Next
                
Trata_Erro:
34    If Err.Number <> 0 Then
35       If Err.Number = 3010 Then
36          frmInicio.lstMensagem.AddItem Space(5) & "O Comando: " & Comando & " n�o foi executado. Porque a tabela j� existe."
37          frmInicio.lstMensagem.AddItem Space(5) & "Erro na Execu��o do Comando SQL. " & "Tabela j� existe."
38       Else
39          frmInicio.lstMensagem.AddItem "Sub Comandos_SQL - C�digo:" & Err.Number & "Descri��o:" & Err.Description & "Linha:" & Erl
41       End If
42       frmInicio.lstMensagem.AddItem Space(5) & "Erro na Execu��o do Comando SQL. As atualiza�oes continuarao normalmente."
         Resume Next
45    End If
End Sub

Sub Executar_Bat()
1         On Error GoTo Trata_Erro
          
          Dim vArquivo As String
          Dim i As Byte
              
2         For i = 1 To 100
3             vArquivo = GetKeyVal(vPath & "\CARGA\PAAC.INI", "BAT", "BAT" & i)
4             If vArquivo = "" Then Exit For
              
5             If Left(UCase(vArquivo), 10) <> "EXECUTADO " Then
6                 frmInicio.lstMensagem.AddItem "Executando: " & vArquivo
7                 If Dir(vArquivo) = "" Then
8                    frmInicio.lstMensagem.AddItem Space(5) & "Arquivo " & vArquivo & " n�o encontrado."
9                 Else
10                   RunCmd vArquivo, vbNormalFocus
11                   SetKeyVal vPath & "\CARGA\PAAC.INI", "BAT", "BAT" & i, "EXECUTADO " & vArquivo
12                End If
13            End If
              
14        Next
          
Trata_Erro:
15        If Err.Number <> 0 Then
16            MsgBox "Sub Executar_Bat" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
17            frmInicio.lstMensagem.AddItem Space(5) & "Erro na Execu��o do Arquivo BAT."
18        End If
End Sub

Sub Executar_EXE()
1         On Error GoTo Trata_Erro
          
          Dim vArquivo As String
          Dim i As Byte
              
2         For i = 1 To 100
3             vArquivo = GetKeyVal(vPath & "\CARGA\PAAC.INI", "EXE", "EXE" & i)
4             If vArquivo = "" Then Exit For
5             frmInicio.lstMensagem.AddItem Space(5) & "Executando: " & vArquivo
6             If Dir(vArquivo) = "" Then
7                frmInicio.lstMensagem.AddItem Space(5) & "Arquivo " & vArquivo & " n�o encontrado."
8             Else
9               RunCmd vArquivo, vbNormalFocus
10              SetKeyVal vPath & "\CARGA\PAAC.INI", "EXE", "EXE" & i, "EXECUTADO " & vArquivo
11            End If
12        Next
          
Trata_Erro:
13        If Err.Number <> 0 Then
14            MsgBox "Sub Executar_Bat" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
15            frmInicio.lstMensagem.AddItem Space(5) & "Erro na Execu��o do Arquivo EXE."
16        End If
End Sub


Function GetKeyVal(ByVal filename As String, ByVal Section As String, ByVal Key As String)
    Dim RetVal As String, Worked As Integer
    If Dir(filename) = "" Then MsgBox UCase(filename) & " N�o Encontrado.", vbInformation, ".:: Notifica��o ::. ": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), filename)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Function SetKeyVal(ByVal filename As String, ByVal Section As String, ByVal Key As String, ByVal KeyValue As String) As Integer
    'Add info to an INI file
    'Function returns 1 if successful and 0 if unsuccessful
    WritePrivateProfileString Section, Key, KeyValue, filename
    SetKeyVal = 1
End Function

Public Function Pegar_Drive() As String
    Dim fs, d, dc, n
    Dim vArq As Integer
    
    
    Pegar_Drive = Left(App.Path, 2)
    If Pegar_Drive = "\\" Then
        Set fs = CreateObject("Scripting.FileSystemObject")
        Set dc = fs.Drives
        For Each d In dc
            If d.DriveType = 3 Then
                n = d.ShareName
            Else
                n = d.Path
            End If
            If InStr(1, App.Path, n) > 0 Then
                Pegar_Drive = d.DriveLetter & ":"
                Exit For
            End If
        Next
    End If

    If Pegar_Drive = "\\" Then
        vArq = FreeFile
        If Dir("C:\DRIVE_PAAC.INI") <> "" Then
            Open "C:\DRIVE_PAAC.INI" For Input As #vArq
            Input #vArq, Pegar_Drive
        Else
            Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
            Open "C:\DRIVE_PAAC.INI" For Output As #vArq
            Print #vArq, Pegar_Drive
        End If
        Close #vArq
    End If
End Function


Public Sub RunCmd(CmdPath As String, _
   Optional WindowStyle As VbAppWinStyle = vbNormalFocus)

   Dim hProcess As Long

   On Error GoTo Err_RunCmd

   hProcess = OpenProcess(SYNCHRONIZE, 0, Shell(CmdPath, WindowStyle))

   If hProcess Then
       WaitForSingleObject hProcess, INFINITE
       CloseHandle hProcess
   End If

   Exit Sub

Err_RunCmd:

   Err.Clear

End Sub


