VERSION 4.00
Begin VB.Form FRMITENS 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ITENS DO PEDIDO"
   ClientHeight    =   3315
   ClientLeft      =   105
   ClientTop       =   2955
   ClientWidth     =   9390
   Height          =   3720
   Left            =   45
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3315
   ScaleWidth      =   9390
   Top             =   2610
   Width           =   9510
   Begin VB.CommandButton Command1 
      Caption         =   "&Sair"
      Height          =   495
      Left            =   7920
      TabIndex        =   32
      Top             =   2760
      Width           =   975
   End
   Begin VB.Frame frmeProduto 
      ClipControls    =   0   'False
      BeginProperty Font 
         name            =   "MS Sans Serif"
         charset         =   1
         weight          =   700
         size            =   12
         underline       =   0   'False
         italic          =   0   'False
         strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   2535
      Left            =   0
      TabIndex        =   9
      Top             =   120
      Width           =   9375
      Begin VB.TextBox txtDescricao 
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1320
         Width           =   6375
      End
      Begin VB.CheckBox chkSemanaPassada 
         Caption         =   "Semana Passada"
         Enabled         =   0   'False
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   9.75
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Left            =   4440
         TabIndex        =   29
         Top             =   2160
         Width           =   2175
      End
      Begin VB.TextBox txtPrLiquido 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1320
         MaxLength       =   6
         TabIndex        =   26
         Top             =   2160
         Width           =   1815
      End
      Begin VB.TextBox txtUf 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   5520
         MaxLength       =   5
         TabIndex        =   8
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox txtAdicional 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   4320
         MaxLength       =   5
         TabIndex        =   7
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox txtPeriodo 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   2640
         MaxLength       =   5
         TabIndex        =   6
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox txtTabela 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1320
         MaxLength       =   6
         TabIndex        =   5
         Top             =   1800
         Width           =   735
      End
      Begin VB.TextBox txtQtde 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   3000
         MaxLength       =   6
         TabIndex        =   0
         Top             =   360
         Width           =   855
      End
      Begin VB.TextBox txtCOD_DPK 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   1320
         MaxLength       =   5
         TabIndex        =   1
         Top             =   840
         Width           =   615
      End
      Begin VB.TextBox txtCOD_FORNECEDOR 
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   3000
         MaxLength       =   3
         TabIndex        =   2
         Top             =   840
         Width           =   375
      End
      Begin VB.TextBox txtCOD_FABRICA 
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   6000
         MaxLength       =   19
         TabIndex        =   3
         Top             =   840
         Width           =   3255
      End
      Begin VB.Label lblDescSuframa 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8520
         TabIndex        =   31
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "%Suf."
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   7920
         TabIndex        =   30
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label lblPcIcm 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7200
         TabIndex        =   28
         Top             =   1800
         Width           =   615
      End
      Begin VB.Label lblDifIcm 
         AutoSize        =   -1  'True
         Caption         =   "%Dif.ICMS"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   6240
         TabIndex        =   27
         Top             =   1800
         Width           =   900
      End
      Begin VB.Label lblPrLiquido 
         AutoSize        =   -1  'True
         Caption         =   "Pr. L�quido"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   25
         Top             =   2160
         Width           =   960
      End
      Begin VB.Label lblUf 
         AutoSize        =   -1  'True
         Caption         =   "%UF"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5040
         TabIndex        =   24
         Top             =   1800
         Width           =   390
      End
      Begin VB.Label lblAdicional 
         AutoSize        =   -1  'True
         Caption         =   "%Adicional"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3360
         TabIndex        =   23
         Top             =   1800
         Width           =   930
      End
      Begin VB.Label lblPeriodo 
         AutoSize        =   -1  'True
         Caption         =   "%Per."
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2160
         TabIndex        =   22
         Top             =   1800
         Width           =   495
      End
      Begin VB.Label lblTabela 
         AutoSize        =   -1  'True
         Caption         =   "Tabela"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1800
         Width           =   600
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Qtde"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2520
         TabIndex        =   20
         Top             =   360
         Width           =   420
      End
      Begin VB.Label lblNrItens 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H000000FF&
         Height          =   300
         Left            =   720
         TabIndex        =   19
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "ITEM"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   360
         Width           =   465
      End
      Begin VB.Label lblCOD_DPK 
         AutoSize        =   -1  'True
         Caption         =   "C�digo DPK"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   840
         Width           =   1035
      End
      Begin VB.Line lneDigito 
         BorderWidth     =   2
         X1              =   1995
         X2              =   2085
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Label lblCOD_FORNECEDOR 
         AutoSize        =   -1  'True
         Caption         =   "Forn"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2520
         TabIndex        =   16
         Top             =   840
         Width           =   390
      End
      Begin VB.Label lblSIGLA 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   15
         Top             =   840
         Width           =   1710
      End
      Begin VB.Label lblCOD_FABRICA 
         AutoSize        =   -1  'True
         Caption         =   "Fabr"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   5400
         TabIndex        =   14
         Top             =   840
         Width           =   390
      End
      Begin VB.Label lblCATEG 
         AutoSize        =   -1  'True
         Caption         =   "Escala"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   7920
         TabIndex        =   13
         Top             =   1320
         Width           =   585
      End
      Begin VB.Label lblCATEGORIA 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   400
            size            =   12
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8640
         TabIndex        =   12
         Top             =   1320
         Width           =   165
      End
      Begin VB.Label lblDigito 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   300
         Left            =   2160
         TabIndex        =   11
         Top             =   840
         Width           =   195
      End
      Begin VB.Label lblDescr 
         AutoSize        =   -1  'True
         Caption         =   "Descri��o"
         BeginProperty Font 
            name            =   "MS Sans Serif"
            charset         =   1
            weight          =   700
            size            =   8.25
            underline       =   0   'False
            italic          =   0   'False
            strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   870
      End
   End
End
Attribute VB_Name = "FRMITENS"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit


Private Sub GetItem(lngCod_Dpk As Long, iCOD_FORNECEDOR As Integer, strCOD_FABRICA As String)
    On Error GoTo TrataErro
    
    Dim SQL As String
    Dim ss As Snapshot
    Static FechaConsultaItem As Boolean

    'monta SQL
          SQL = "select e.COD_DPK,"
    SQL = SQL & " e.COD_FORNECEDOR,"
    SQL = SQL & " nvl(f.SIGLA,' ') sigla,"
    SQL = SQL & " nvl(e.COD_FABRICA,0) cod_fabrica,"
    SQL = SQL & " nvl(e.DESC_ITEM,' ') desc_item,"
    SQL = SQL & " nvl(e.cod_grupo,0) cod_grupo,"
    SQL = SQL & " nvl(e.cod_subgrupo,0) cod_subgrupo,"
    SQL = SQL & " nvl(a.SINAL,' ') sinal,"
    SQL = SQL & " nvl(e.class_fiscal,0) class_fiscal,"
    SQL = SQL & " nvl(e.QTD_MINVDA,1) qtd_minvda,"
    SQL = SQL & " nvl(b.QTD_MAXVDA,1) qtd_maxvda,"
    SQL = SQL & " e.COD_TRIBUTACAO cod_tributacao,"
    SQL = SQL & " e.COD_TRIBUTACAO_IPI,"
    SQL = SQL & " e.PC_IPI,"
    SQL = SQL & " nvl(d.PRECO_VENDA,0) PRECO_VENDA,"
    SQL = SQL & " nvl(d.PRECO_VENDA_ANT,0) PRECO_VENDA_ANT,"
    SQL = SQL & " nvl(d.PRECO_OF,0) PRECO_OF,"
    SQL = SQL & " nvl(d.PRECO_OF_ANT,0) PRECO_OF_ANT,"
    SQL = SQL & " nvl(d.PRECO_SP,0) PRECO_SP,"
    SQL = SQL & " nvl(d.PRECO_SP_ANT,0) PRECO_SP_ANT,"
    SQL = SQL & " b.SITUACAO,"
    SQL = SQL & " nvl(e.COD_DPK_ANT,0) COD_DPK_ANT,"
    SQL = SQL & " f.DIVISAO"
    SQL = SQL & " from categ_sinal a,"
    SQL = SQL & " item_estoque b,"
    SQL = SQL & " item_analitico c,"
    SQL = SQL & " item_preco d,"
    SQL = SQL & " item_cadastro e,"
    SQL = SQL & " fornecedor f"
    SQL = SQL & " where c.categoria = a.categoria and"
    'SQL = SQL & " b.situacao = 0 and"
    SQL = SQL & " b.cod_loja = c.cod_loja and"
    SQL = SQL & " b.cod_dpk = c.cod_dpk and"
    SQL = SQL & " c.cod_loja = d.cod_loja and"
    SQL = SQL & " c.cod_dpk = d.cod_dpk and"
    SQL = SQL & " d.cod_loja+0 = :loja and"
    SQL = SQL & " d.cod_dpk = e.cod_dpk and"
    SQL = SQL & " e.cod_fornecedor = f.cod_fornecedor"
    
    'inicializa frame de precos
   ' Call InitFrmePrecoVenda
    
    
    If lngCod_Dpk > 0 Then
        SQL = SQL & " and e.COD_DPK = :dpk"
    ElseIf iCOD_FORNECEDOR <> "-1" And strCOD_FABRICA <> "-1" Then
        SQL = SQL & " and e.COD_FORNECEDOR = :forn and"
        SQL = SQL & " e.COD_FABRICA = :fabrica"
    Else
        'Inicializar frame de preco de venda
        Call InitFrmeProduto
        Exit Sub
    End If
    
    'criar consulta
    If FechaConsultaItem Then
        FechaConsultaItem = True
    End If
    
    oradatabase.Parameters.Remove "loja"
    oradatabase.Parameters.Remove "forn"
    oradatabase.Parameters.Remove "dpk"
    oradatabase.Parameters.Remove "fabrica"
       
    oradatabase.Parameters.Add "loja", lngCod_Loja, 1
    oradatabase.Parameters.Add "forn", CStr(iCOD_FORNECEDOR), 1
    oradatabase.Parameters.Add "dpk", txtCOD_DPK.Text, 1
    oradatabase.Parameters.Add "fabrica", strCOD_FABRICA, 1
    
    Set ssItem = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If ssItem.EOF() And ssItem.BOF Then
        MsgBox "Item n�o Cadastrado para o dep�sito: " & lngCod_Loja, vbInformation, "Aten��o"
        'Inicializar frame
        'Call InitFrmeProduto
        Call ReInitProduto
        FRMITENS.txtCOD_FABRICA.Enabled = True
    ElseIf ssItem!DIVISAO = "B" Then
        MsgBox "Este item pertence a divis�o BLAU", vbInformation, "Aten��o"
        'Inicializar frame
        Call InitFrmeProduto
    ElseIf ssItem!situacao = 8 Then
        'item substituido
        MsgBox "Item Substituido", vbInformation, "Aten��o"
        Call GetItem(ssItem!COD_DPK_ANT, -1, -1)
    ElseIf ssItem!situacao = 9 Then
        MsgBox "Item Desativado", vbInformation, "Aten��o"
        'Inicializar frame
        'Call InitFrmeProduto
        Call ReInitProduto
        FRMITENS.txtCOD_FABRICA.Enabled = True
    Else
    
        
        'carregar dados
        txtCOD_DPK.Text = ssItem!cod_dpk
        lblDigito.Caption = iRetornaDigito(ssItem!cod_dpk)
       
        txtCOD_FORNECEDOR.Text = ssItem!cod_fornecedor
        lblSIGLA.Caption = ssItem!sigla
        
        txtCOD_FABRICA.Text = ssItem!cod_fabrica
        txtCOD_FABRICA.Enabled = True
        txtDescricao.Text = ssItem!DESC_ITEM
        txtDescricao.Enabled = True
        qtd_max = ssItem!qtd_maxvda
        qtd_min = ssItem!qtd_minvda
        grupo = ssItem!COD_GRUPO
        subgrupo = ssItem!COD_SUBGRUPO
        Tribut = ssItem!COD_TRIBUTACAO
        class_fiscal = ssItem!class_fiscal
              
        lblCATEGORIA.Caption = ssItem!SINAL
               
        If tela <> "A" Then
                SQL = "Select cod_dpk, num_item_pedido "
          SQL = SQL & " From  item_pedido "
          SQL = SQL & " Where num_pendente = " & lngNUM_PEDIDO
          SQL = SQL & " and seq_pedido = " & lngSEQ_PEDIDO
          SQL = SQL & " and cod_dpk = " & FRMITENS.txtCOD_DPK.Text
          SQL = SQL & " and cod_loja = " & lngCod_Loja
        
          Set ss = dbAccess.CreateSnapshot(SQL)
          FreeLocks
  
          If Not ss.EOF Then
            MsgBox "O item " & txtCOD_DPK.Text & " j� foi digitado. Item nr.: " & ss!num_item_pedido, vbExclamation, "Aten��o"
            ReInitProduto
            FRMITENS.txtQtde.SetFocus
            Exit Sub
          Else
            Call Checa_Qtde
          End If
          'calculo de icm
          'Call GetDifIcm(NaoCalcItem)
        
          'calcula pre�os
          'Call CalculaPrecos(BuscaDescTab)
        End If
        
    End If
      
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
    
End Sub

Private Sub ReInitProduto()
    'reinicializa frame com excessao do fornecedor
    txtCOD_DPK.Text = ""
    lblDigito.Caption = ""
    txtCOD_FORNECEDOR.Text = ""
    lblSIGLA.Caption = ""
    
    txtCOD_FABRICA.Text = ""
    txtCOD_FABRICA.Enabled = False
    txtDescricao.Text = ""
    txtDescricao.Enabled = False
    
    txtTabela.Text = ""
    txtPeriodo.Text = ""
    txtAdicional.Text = ""
    txtUf.Text = ""
    lblDescSuframa.Caption = ""
    lblpcIcm.Caption = ""
    txtQtde.Text = ""
    txtPrLiquido.Text = ""
    Preco_Bruto = 0#
    
    
    lblCATEGORIA.Caption = ""
        
    'inicializar preco de venda
    'Call InitFrmePrecoVenda
    
End Sub

Private Sub GetFornecedor(iCOD_FORNECEDOR As Integer)
'    On Error GoTo TrataErro

    Dim SQL As String
    Dim ss As Object
    
    'criar consulta
          SQL = "select nvl(SIGLA,' ') sigla, SITUACAO "
    SQL = SQL & " from FORNECEDOR "
    SQL = SQL & " where SITUACAO = 0 and "
    SQL = SQL & " COD_FORNECEDOR = :forn"
        
    oradatabase.Parameters.Remove "forn"
    oradatabase.Parameters.Add "forn", iCOD_FORNECEDOR, 1
    
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
    
    If ss.EOF() And ss.BOF Then
        MsgBox "Fornecedor n�o cadastrado", vbInformation, "Aten��o"
        FRMITENS.txtCOD_FORNECEDOR.SetFocus
        'Inicializar frame
        Call InitFrmeProduto
    ElseIf ss.Fields("SITUACAO").Value <> 0 Then
        MsgBox "Fornecedor Desativado", vbInformation, "Aten��o"
        'Inicializar frame
        Call InitFrmeProduto
    Else
        'carregar dados
        lblSIGLA.Caption = ss.Fields("SIGLA").Value
        
        'Habilita codigo de fabrica
        txtCOD_FABRICA.Enabled = True
        txtCOD_FABRICA.SetFocus
     End If
    
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If
    
End Sub

Private Sub chkSemanaPassada_Click()
  Call Descontos
  Call Calcula_Preco
End Sub

Private Sub chkSemanaPassada_GotFocus()
  chkSemanaPassada.BackColor = Vermelho
End Sub


Private Sub chkSemanaPassada_LostFocus()
  chkSemanaPassada.BackColor = Cinza
  
End Sub



Private Sub Command1_Click()
  Unload FRMITENS
End Sub

Private Sub Form_Load()
  Dim ss As Snapshot
  Dim ss2 As Object

 If tela <> "A" Then
        SQL = "Select max(b.num_item_pedido) as num_item,"
  SQL = SQL & " a.qtd_itens,"
  SQL = SQL & " a.pc_desc_suframa"
  SQL = SQL & " from pedido AS a LEFT JOIN  item_pedido AS b "
  SQL = SQL & " ON (a.num_pendente = b.num_pendente "
  SQL = SQL & " and a.seq_pedido = b.seq_pedido and"
  SQL = SQL & " a.cod_loja = b.cod_loja) "
  SQL = SQL & " Where a.num_pendente = " & lngNUM_PEDIDO
  SQL = SQL & " and a.seq_pedido = " & lngSEQ_PEDIDO
  SQL = SQL & " and a.cod_loja = " & lngCod_Loja
  SQL = SQL & " group by a.qtd_itens,pc_desc_suframa order by a.qtd_itens,pc_desc_suframa"
  
  
  Set ss = dbAccess.CreateSnapshot(SQL)
  
  
  If IsNull(ss!num_item) Then
     FRMITENS.lblNrItens.Caption = 1
  Else
     FRMITENS.lblNrItens.Caption = ss!num_item + 1
  End If
  
  
 FRMITENS.lblDescSuframa.Caption = ss!PC_DESC_SUFRAMA
  

Else
        SQL = "Select *"
  SQL = SQL & " from pedido AS a LEFT JOIN  item_pedido AS b "
  SQL = SQL & " ON (a.num_pendente = b.num_pendente "
  SQL = SQL & " and a.seq_pedido = b.seq_pedido and"
  SQL = SQL & " a.cod_loja = b.cod_loja) "
  SQL = SQL & " Where a.num_pendente = " & lngNUM_PEDIDO
  SQL = SQL & " and a.seq_pedido = " & lngSEQ_PEDIDO
  SQL = SQL & " and a.cod_loja = " & lngCod_Loja
  frmVisPedido.grdItem.Col = 0
  SQL = SQL & " and num_item_pedido = " & frmVisPedido.grdItem.Text
  
  
  Set ss = dbAccess.CreateSnapshot(SQL)
  
  
  FRMITENS.txtCOD_DPK.Text = ss!cod_dpk
  FRMITENS.lblNrItens.Caption = ss!num_item_pedido
  FRMITENS.txtQtde.Text = ss!qtd_solicitada
  FRMITENS.txtTabela.Text = ss!tabela_compra
  FRMITENS.txtPeriodo.Text = FmtBR(ss!pc_desconto1)
  FRMITENS.txtAdicional.Text = FmtBR(ss!pc_desconto3)
  FRMITENS.txtUf.Text = FmtBR(ss!pc_desconto2)
  FRMITENS.lblpcIcm.Caption = FmtBR(ss!pc_desc_icm)
  FRMITENS.lblDescSuframa.Caption = FmtBR(ss!PC_DESC_SUFRAMA)
  FRMITENS.txtPrLiquido.Text = FmtBR(ss!preco_liquido)

  Call GetItem(ss!cod_dpk, -1, -1)
  
  
 
End If

  ss.Close

End Sub

Private Sub Form_Unload(Cancel As Integer)
  Set FRMITENS = Nothing
End Sub


Private Sub txtAdicional_GotFocus()
  txtAdicional.BackColor = Vermelho
End Sub

Private Sub txtAdicional_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtAdicional)
End Sub


Private Sub txtAdicional_LostFocus()
  txtAdicional.BackColor = Branco
  
  If txtTabela.Text <> "" Then
    If txtAdicional.Text = "" Then
      txtAdicional.Text = 0#
    Else
      If txtAdicional.Text > 99 Then
        MsgBox "O desconto adicional n�o pode ser superior a 99%", vbInformation, "Aten��o"
        txtAdicional.SetFocus
        Exit Sub
      End If
    End If
    If lblDescSuframa.Caption = "" Then
      lblDescSuframa.Caption = 0#
    End If
     Call Calcula_Preco
  End If
  If txtPrLiquido.Text = "" Or txtPrLiquido.Text = "0" Then
      Call txtTabela_LostFocus
  End If
End Sub

Private Sub txtCOD_DPK_GotFocus()
  txtCOD_DPK.BackColor = Vermelho
  txtCOD_DPK.DataChanged = False
      
End Sub

Private Sub txtCOD_DPK_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCOD_DPK_LostFocus()

 txtCOD_DPK.BackColor = Branco
 
     If txtCOD_DPK.DataChanged Then
        If txtCOD_DPK.Text = "0" Or txtCOD_DPK.Text = "" Then
            'Inicializar frame
            Call InitFrmeProduto
        Else
            'carrega dados do item
            item = True
            Call GetItem(txtCOD_DPK.Text, -1, -1)
            If item = False Then
              InitFrmeProduto
              txtQtde.SetFocus
            End If
            
            montadora = 0
            
            If tp_item = 1 And txtQtde.Text <> "" Then
              txtTabela.SetFocus
            ElseIf tp_item = 2 And txtQtde.Text <> "" Then
              txtPrLiquido.SetFocus
            End If
         End If
    End If
 

End Sub


Private Sub txtCOD_FABRICA_DblClick()
   
   txtCOD_FABRICA.DataChanged = False
    txtResposta = txtCOD_FORNECEDOR
    
    
      
    If txtResposta <> "" Then
       txtCOD_FABRICA.Text = txtResposta
       Call txtCOD_FABRICA_LostFocus
    End If

End Sub

Private Sub txtCOD_FABRICA_GotFocus()
  txtCOD_FABRICA.BackColor = Vermelho
  txtCOD_FABRICA.DataChanged = False
End Sub


Private Sub txtCOD_FABRICA_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtCOD_FABRICA_LostFocus()
       
  txtCOD_FABRICA.BackColor = Branco
   If txtCOD_FABRICA.Text = "" Then
         'Inicializar frame
         'Call InitFrmeProduto
         txtDescricao.Enabled = True
   Else
     If txtCOD_FABRICA.DataChanged Then
         'carrega dados do item
          Call GetItem(-1, txtCOD_FORNECEDOR, txtCOD_FABRICA.Text)
     End If
  End If
End Sub

Private Sub txtCOD_FORNECEDOR_Change()

If txtCOD_DPK.Text = "" Then
    If Len(txtCOD_FORNECEDOR.Text) = 3 Then
      Call txtCOD_FORNECEDOR_LostFocus
    End If
 End If

End Sub

Private Sub txtCOD_FORNECEDOR_Click()
  txtCOD_FORNECEDOR.Text = ""
  
  Call InitFrmeProduto
End Sub


Private Sub txtCOD_FORNECEDOR_DblClick()
  txtResposta = "0"
  
  If txtResposta <> "0" Then
      txtCOD_FORNECEDOR.Text = txtResposta
      txtCOD_FORNECEDOR.DataChanged = True
      Call txtCOD_FORNECEDOR_LostFocus
  End If
End Sub

Private Sub txtCOD_FORNECEDOR_GotFocus()
  txtCOD_FORNECEDOR.BackColor = Vermelho
  txtCOD_FORNECEDOR.DataChanged = False
End Sub

Private Sub txtCOD_FORNECEDOR_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtCOD_FORNECEDOR_LostFocus()

  txtCOD_FORNECEDOR.BackColor = Branco
  If txtCOD_DPK.Text = "" Then
     If txtCOD_FORNECEDOR.DataChanged Then
        If txtCOD_FORNECEDOR.Text = "0" Or txtCOD_FORNECEDOR.Text = "" Then
            'Inicializar frame
            Call InitFrmeProduto
        Else
            Call GetFornecedor(txtCOD_FORNECEDOR.Text)
            
        End If
     End If
   End If

End Sub

Private Sub txtDescricao_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtPeriodo_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtPeriodo)
End Sub


Private Sub txtPrLiquido_GotFocus()
  txtPrLiquido.BackColor = Vermelho
End Sub

Private Sub txtPrLiquido_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtPrLiquido.Text)
End Sub


Private Sub txtPrLiquido_LostFocus()
  
  txtPrLiquido.BackColor = Branco
 If txtQtde.Text <> "" Then
    If txtPrLiquido.Text = "" Then
      txtPrLiquido.Text = 0
    End If
    If txtPrLiquido.Text <= 0 Then
      MsgBox "Entre com o Pre�o do Produto", vbInformation, "Aten��o"
      txtPrLiquido.SetFocus
      Exit Sub
    End If
    
 End If
End Sub


Private Sub txtQtde_GotFocus()
  txtQtde.BackColor = Vermelho
End Sub

Private Sub txtQtde_KeyPress(KeyAscii As Integer)
  KeyAscii = Numerico(KeyAscii)
End Sub


Private Sub txtQtde_LostFocus()
  
  txtQtde.BackColor = Branco
  If txtCOD_DPK <> "" Then
    Call txtCOD_DPK_LostFocus
    Call Checa_Qtde
  End If
End Sub


Private Sub txtTabela_Change()
  If txtTabela.Text = "" Then
    txtPeriodo.Text = ""
    txtAdicional.Text = ""
    txtPrLiquido.Text = ""
  End If
  
End Sub

Private Sub txtTabela_GotFocus()
  txtTabela.BackColor = Vermelho
End Sub

Private Sub txtTabela_KeyPress(KeyAscii As Integer)
  KeyAscii = Texto(KeyAscii)
End Sub


Private Sub txtTabela_LostFocus()

 
  txtTabela.BackColor = Branco
  If txtCOD_DPK.Text <> "" Then
     Call TABELA_VENDA(Format$(strDt_Tabela, "DD/MM/YY"))
  End If
  
  If txtTabela.Text <> "" Then
    Call Descontos
    Call Preco_Venda
    txtAdicional.SetFocus
  Else
    Exit Sub
  End If
  
End Sub


Private Sub txtUf_KeyPress(KeyAscii As Integer)
  KeyAscii = Valor(KeyAscii, txtUf)
End Sub



Private Sub InitFrmeProduto()
 'inicializa fornecedor
    txtCOD_FORNECEDOR.Text = ""
    lblSIGLA.Caption = ""
    Call ReInitProduto

End Sub

Private Sub Checa_Qtde()
'validar quantidade
  If txtQtde.Text = "" Then
    txtQtde.Text = 0
  End If
    
   If CLng(qtd_min) = 0 And _
       CLng(qtd_max) = 0 Then
       'n�o verifica max e min
       
   ElseIf CLng(qtd_min) > 0 And _
       CLng(qtd_max) > 0 Then
       
       If CLng(txtQtde.Text) >= CLng(qtd_min) Then
            If CLng(txtQtde.Text) Mod CLng(qtd_min) = 0 Then
                If CLng(txtQtde.Text) > CLng(qtd_max) Then
                    MsgBox "A quantidade maxima deste item � " & qtd_max, vbInformation, "Aten��o"
                    txtQtde.SetFocus
                    txtQtde.Text = ""
                    Exit Sub
                End If
            Else
                MsgBox "A quantidade deve ser multipla de " & qtd_min, vbInformation, "Aten��o"
                txtQtde.SetFocus
                txtQtde.Text = ""
                Exit Sub
            End If
        Else
            MsgBox "A quantidade minima deste item � " & qtd_min, vbInformation, "Aten��o"
            txtQtde.Text = ""
            'txtQtde.SetFocus
            Exit Sub
        End If
       
       
    ElseIf CLng(qtd_min) > 0 Then
    
        If CLng(txtQtde.Text) >= CLng(qtd_min) Then
            If CLng(txtQtde.Text) Mod CLng(qtd_min) <> 0 Then
                MsgBox "A quantidade deve ser multipla de " & qtd_min, vbInformation, "Aten��o"
                txtQtde.Text = ""
                txtQtde.SetFocus
                Exit Sub
            End If
        Else
            MsgBox "A quantidade minima deste item � " & qtd_min, vbInformation, "Aten��o"
            txtQtde.Text = ""
            'txtQtde.SetFocus
            Exit Sub
        End If
        
    ElseIf CLng(qtd_max) > 0 Then
        If CLng(txtQtde.Text) > CLng(qtd_max) Then
            MsgBox "A quantidade maxima deste item � " & qtd_max, vbInformation, "Aten��o"
            txtQtde.Text = ""
            txtQtde.SetFocus
            Exit Sub
        End If
    End If

End Sub

Private Sub InputItem()
  Dim ss As Snapshot
  Dim dblVl_Ipi As Double
  Dim dblVl_Preco_Liq As Double
  Dim dblVl_Ipi_Ant As Double
  
  'verificar existencia do item
   SQL = "select QTD_SOLICITADA,"
   SQL = SQL & "PRECO_LIQUIDO * QTD_SOLICITADA as VL_CONTB_ANT "
   SQL = SQL & "from ITEM_PEDIDO where "
   SQL = SQL & "NUM_PENDENTE = " & lngNUM_PEDIDO
   SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
   SQL = SQL & " and COD_LOJA = " & lngCod_Loja
   SQL = SQL & " and COD_DPK = " & txtCOD_DPK.Text
   Set ss = dbAccess.CreateSnapshot(SQL)
   FreeLocks
        
   If ss.EOF And ss.BOF Then
      If txtQtde.Text = "0" Then
         MsgBox "Item com quantidade 0 (zero) n�o incluido", vbExclamation
         Exit Sub
      End If
        
      SQL = " Insert into ITEM_PEDIDO ( NUM_PENDENTE,"
      SQL = SQL & " SEQ_PEDIDO,"
      SQL = SQL & " COD_LOJA,"
      SQL = SQL & " NUM_ITEM_PEDIDO,"
      SQL = SQL & " COD_DPK,"
      SQL = SQL & " COD_FORNECEDOR,"
      SQL = SQL & " COD_FABRICA,"
      SQL = SQL & " COD_TRIBUTACAO,"
      SQL = SQL & " COD_TRIBUTACAO_IPI,"
      SQL = SQL & " QTD_SOLICITADA,"
      SQL = SQL & " TABELA_COMPRA,"
      SQL = SQL & " PRECO_UNITARIO,"
      SQL = SQL & " PRECO_LIQUIDO,"
      SQL = SQL & " PC_DESCONTO1,"
      SQL = SQL & " PC_DESCONTO2,"
      SQL = SQL & " PC_DESCONTO3,"
      SQL = SQL & " PC_DESC_ICM,"
      SQL = SQL & " PC_IPI, SITUACAO) "
      SQL = SQL & " values(" & lngNUM_PEDIDO & ","
      SQL = SQL & lngSEQ_PEDIDO & ","
      SQL = SQL & lngCod_Loja & ","
      SQL = SQL & lblNrItens.Caption & ","
      SQL = SQL & txtCOD_DPK.Text & ","
      SQL = SQL & txtCOD_FORNECEDOR & ","
      SQL = SQL & "'" & txtCOD_FABRICA & "',"
      SQL = SQL & ssItem!COD_TRIBUTACAO & ","
      SQL = SQL & ssItem!COD_TRIBUTACAO_IPI & ","
      SQL = SQL & txtQtde.Text & ","
      If tp_item = 1 Then
        SQL = SQL & "'" & txtTabela & "',"
        SQL = SQL & FmtBR(Preco_Bruto) & ","
        SQL = SQL & FmtBR(txtPrLiquido.Text) & ","
        SQL = SQL & FmtBR(txtPeriodo.Text) & ","
        SQL = SQL & FmtBR(txtUf.Text) & ","
        SQL = SQL & FmtBR(txtAdicional.Text) & ","
        SQL = SQL & FmtBR(lblpcIcm.Caption) & ","
        SQL = SQL & FmtBR(ssItem!pc_ipi) & "," & 0 & ")"
      Else
        SQL = SQL & "'',"
        SQL = SQL & FmtBR(txtPrLiquido.Text) & ","
        SQL = SQL & FmtBR(txtPrLiquido.Text) & ","
        SQL = SQL & FmtBR(0#) & ","
        SQL = SQL & FmtBR(0#) & ","
        SQL = SQL & FmtBR(0#) & ","
        SQL = SQL & FmtBR(0#) & ","
        SQL = SQL & FmtBR(0#) & "," & 0 & ")"
      End If
            
            
      'criar item de pedido
       dbAccess.Execute SQL, dbFailOnError
       FreeLocks
            
       'atualizar pedido de venda
       SQL = "update PEDIDO set "
       If ssItem!COD_TRIBUTACAO_IPI = 1 Then
         SQL = SQL & "VL_IPI = VL_IPI + " & FmtBR(FmtBR(txtQtde.Text) * FmtBR(txtPrLiquido.Text) * (1 - FmtBR(ssItem!pc_ipi) / 100)) & ","
       End If
       SQL = SQL & "VL_CONTABIL = VL_CONTABIL + " & FmtBR(FmtBR(txtQtde.Text) * FmtBR(txtPrLiquido.Text))
       SQL = SQL & " where NUM_PENDENTE = " & lngNUM_PEDIDO
       SQL = SQL & " and SEQ_PEDIDO = " & lngSEQ_PEDIDO
       SQL = SQL & " and cod_loja = " & lngCod_Loja
      
      'executa atualizacao
      dbAccess.Execute SQL, dbFailOnError
      FreeLocks
          
  Else
    MsgBox "Item j� incluido", vbExclamation, "Aten��o"
    Exit Sub
  End If
  ss.Close
End Sub
