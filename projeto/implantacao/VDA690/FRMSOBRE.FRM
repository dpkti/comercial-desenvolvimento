VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "Threed32.OCX"
Begin VB.Form frmSobre 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SOBRE"
   ClientHeight    =   3795
   ClientLeft      =   1590
   ClientTop       =   1620
   ClientWidth     =   6720
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3795
   ScaleWidth      =   6720
   Begin Threed.SSCommand SSCommand1 
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   3240
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "OK"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   3090
      Left            =   1560
      Picture         =   "FRMSOBRE.frx":0000
      Top             =   -240
      Width           =   3765
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      Caption         =   "Respons�vel:  MARICI - MAXXIPEL"
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   2040
      TabIndex        =   1
      Top             =   3360
      Width           =   2655
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      Caption         =   "Programa: VDA690 Confer�ncia/Libera��o de Pedido  VDR (Vers�o: 2.0 32Bits) "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   1080
      TabIndex        =   0
      Top             =   2760
      Width           =   4815
   End
End
Attribute VB_Name = "frmSobre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SSCommand1_Click()
  Unload frmSobre
  Set frmSobre = Nothing
End Sub


