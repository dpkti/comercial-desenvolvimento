VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "gradient.ocx"
Begin VB.Form frmPendente 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "ALTERA PENDENTE"
   ClientHeight    =   5565
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7650
   Icon            =   "frmPendente.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   371
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   510
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2985
      Left            =   45
      TabIndex        =   19
      Top             =   2205
      Width           =   7530
      Begin VB.TextBox txtMotivo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   780
         Left            =   1665
         MultiLine       =   -1  'True
         TabIndex        =   29
         Top             =   2070
         Width           =   5460
      End
      Begin VB.TextBox txtCP 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5310
         TabIndex        =   12
         Top             =   1665
         Width           =   1815
      End
      Begin VB.TextBox txtFAT 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5310
         TabIndex        =   10
         Top             =   1260
         Width           =   1815
      End
      Begin VB.TextBox txtNT 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5310
         TabIndex        =   8
         Top             =   855
         Width           =   1815
      End
      Begin VB.TextBox txtVendaCP 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5310
         TabIndex        =   6
         Top             =   450
         Width           =   1815
      End
      Begin VB.TextBox txtVendaSP 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         TabIndex        =   5
         Top             =   450
         Width           =   1815
      End
      Begin VB.TextBox txtOA 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         TabIndex        =   7
         Top             =   855
         Width           =   1815
      End
      Begin VB.TextBox txtTrans 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         TabIndex        =   9
         Top             =   1260
         Width           =   1815
      End
      Begin VB.TextBox txtSP 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         TabIndex        =   11
         Top             =   1665
         Width           =   1815
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Motivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   900
         TabIndex        =   30
         Top             =   2070
         Width           =   600
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. CP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   4545
         TabIndex        =   27
         Top             =   1755
         Width           =   705
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. FAT Forn.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3960
         TabIndex        =   26
         Top             =   1350
         Width           =   1305
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. NT:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   4500
         TabIndex        =   25
         Top             =   945
         Width           =   735
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. Venda CP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   3960
         TabIndex        =   24
         Top             =   540
         Width           =   1290
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. Venda SP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   315
         TabIndex        =   23
         Top             =   540
         Width           =   1290
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. OA:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   855
         TabIndex        =   22
         Top             =   945
         Width           =   750
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. Transito:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   450
         TabIndex        =   21
         Top             =   1350
         Width           =   1155
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Qtd. SP:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   900
         TabIndex        =   20
         Top             =   1755
         Width           =   705
      End
   End
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1185
      Left            =   45
      TabIndex        =   15
      Top             =   945
      Width           =   7530
      Begin VB.TextBox txtPedido 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3510
         TabIndex        =   2
         Top             =   540
         Width           =   1050
      End
      Begin VB.TextBox txtSeq 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5085
         TabIndex        =   3
         Top             =   540
         Width           =   285
      End
      Begin VB.ComboBox cmbLoja 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   540
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   540
         Width           =   2040
      End
      Begin VB.TextBox txtDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   6120
         TabIndex        =   4
         Top             =   540
         Width           =   1275
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Pedido:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   2880
         TabIndex        =   32
         Top             =   630
         Width           =   615
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Seq.:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   4635
         TabIndex        =   31
         Top             =   630
         Width           =   435
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "Loja:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   90
         TabIndex        =   17
         Top             =   630
         Width           =   390
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         Caption         =   "DPK:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   210
         Left            =   5670
         TabIndex        =   16
         Top             =   630
         Width           =   390
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   5235
      Width           =   7650
      _ExtentX        =   13494
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12991
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   13
      Top             =   810
      Width           =   7530
      _ExtentX        =   13282
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendente.frx":23D2
      PICN            =   "frmPendente.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   1665
      TabIndex        =   18
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendente.frx":30C8
      PICN            =   "frmPendente.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   2430
      TabIndex        =   28
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendente.frx":3DBE
      PICN            =   "frmPendente.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   900
      TabIndex        =   33
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmPendente.frx":4AB4
      PICN            =   "frmPendente.frx":4AD0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmPendente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim vVendaSP As Double
Dim vVendaCP As Double
Dim vOA As Double
Dim vNT As Double
Dim vTrans As Double
Dim vFAT As Double
Dim vSP As Double
Dim vCP As Double

Dim vDTNF As String
Dim vNUM_ITEM As Integer

Private Sub cmd1_Click()

    If CDbl(Val(txtVendaSP.Text)) > vVendaSP Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtVendaSP.Text = vVendaSP
        txtVendaSP.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtVendaCP.Text)) > vVendaCP Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtVendaCP.Text = vVendaCP
        txtVendaCP.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtOA.Text)) > vOA Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtOA.Text = vOA
        txtOA.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtNT.Text)) > vNT Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtNT.Text = vNT
        txtNT.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtTrans.Text)) > vTrans Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtTrans.Text = vTrans
        txtTrans.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtFAT.Text)) > vFAT Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtFAT.Text = vFAT
        txtFAT.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtSP.Text)) > vSP Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtSP.Text = vSP
        txtSP.SetFocus
        Exit Sub
    End If
    
    If CDbl(Val(txtCP.Text)) > vCP Then
        MsgBox "Os valores s� devem ser alterados para MENOR", , "Aten��o"
        txtCP.Text = vCP
        txtCP.SetFocus
        Exit Sub
    End If
    
    If txtMotivo.Text = "" Then
        MsgBox "Digite o motivo para a altera��o", , "Aten��o"
        txtMotivo.SetFocus
        Exit Sub
    End If

    If Trim(Left(cmbLoja.Text, InStr(cmbLoja.Text, " -"))) = "1" Then
        DEP = "PRODUCAO"
    Else
        CODDEP = Trim(Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")))
        If Len(CODDEP) = 1 Then
            CODDEP = "0" & CODDEP
        End If
        DEP = "DEP" & CODDEP
    End If

    vBanco.Parameters.Remove "PM_DEP"
    vBanco.Parameters.Add "PM_DEP", DEP, 1
    
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", Left(cmbLoja.Text, InStr(cmbLoja.Text, " -") - 1), 1
    
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", txtDPK.Text, 1
    
    vBanco.Parameters.Remove "PM_PEDIDO"
    vBanco.Parameters.Add "PM_PEDIDO", txtPedido.Text, 1
    
    vBanco.Parameters.Remove "PM_SEQ"
    vBanco.Parameters.Add "PM_SEQ", txtSeq.Text, 1
    
    vBanco.Parameters.Remove "PM_VendaSP"
    vBanco.Parameters.Add "PM_VendaSP", txtVendaSP.Text, 1
    
    vBanco.Parameters.Remove "PM_VendaCP"
    vBanco.Parameters.Add "PM_VendaCP", txtVendaCP.Text, 1
    
    vBanco.Parameters.Remove "PM_OA"
    vBanco.Parameters.Add "PM_OA", txtOA.Text, 1
    
    vBanco.Parameters.Remove "PM_NT"
    vBanco.Parameters.Add "PM_NT", txtNT.Text, 1
    
    vBanco.Parameters.Remove "PM_Trans"
    vBanco.Parameters.Add "PM_Trans", txtTrans.Text, 1
    
    vBanco.Parameters.Remove "PM_FAT"
    vBanco.Parameters.Add "PM_FAT", txtFAT.Text, 1
    
    vBanco.Parameters.Remove "PM_SP"
    vBanco.Parameters.Add "PM_SP", txtSP.Text, 1
    
    vBanco.Parameters.Remove "PM_CP"
    vBanco.Parameters.Add "PM_CP", txtCP.Text, 1

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_VDA920.PR_ACT_PENDENTE(:PM_DEP,:PM_CODLOJA,:PM_CODDPK,:PM_PEDIDO,:PM_SEQ,:PM_VendaSP,:PM_VendaCP,:PM_OA,:PM_NT,:PM_Trans,:PM_FAT,:PM_SP,:PM_CP,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    
    If CDbl(txtVendaSP.Text) <> vVendaSP Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_VENDA_SP", txtVendaSP.Text, vVendaSP, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtVendaCP.Text) <> vVendaCP Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_VENDA_CP", txtVendaCP.Text, vVendaCP, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtOA.Text) <> vOA Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_OA", txtOA.Text, vOA, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtNT.Text) <> vNT Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_NT", txtNT.Text, vNT, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtTrans.Text) <> vTrans Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_TRANSITO", txtTrans.Text, vTrans, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtFAT.Text) <> vFAT Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_FAT_FORN", txtFAT.Text, vFAT, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtSP.Text) <> vSP Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_SP", txtSP.Text, vSP, txtMotivo.Text, mdiVDA920.vCODUSR
    End If
    
    If CDbl(txtCP.Text) <> vCP Then
        LOG_PENDENTE Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")), txtDPK.Text, txtPedido.Text, txtSeq.Text, "QTD_CP", txtCP.Text, vCP, txtMotivo.Text, mdiVDA920.vCODUSR
    End If

    MsgBox "Item pendente alterada com sucesso"
    Call cmd3_Click
    
End Sub

Private Sub cmd2_Click()

    If cmbLoja.Text = "" Then
        MsgBox "Escolha uma Loja", , "Aten��o"
        cmbLoja.SetFocus
        Exit Sub
    End If
    
    If txtDPK.Text = "" Then
        MsgBox "Entre com o c�digo DPK", , "Aten��o"
        txtDPK.SetFocus
        Exit Sub
    End If
    
    If txtPedido.Text = "" Or txtSeq.Text = "" Then
        MsgBox "Entre com o numero do Pedido e a Sequ�ncia", , "Aten��o"
        txtPedido.SetFocus
        Exit Sub
    End If

    If Trim(Left(cmbLoja.Text, InStr(cmbLoja.Text, " -"))) = "1" Then
        DEP = "PRODUCAO"
    Else
        CODDEP = Trim(Left(cmbLoja.Text, InStr(cmbLoja.Text, " -")))
        If Len(CODDEP) = 1 Then
            CODDEP = "0" & CODDEP
        End If
        DEP = "DEP" & CODDEP
    End If

    vBanco.Parameters.Remove "PM_DEP"
    vBanco.Parameters.Add "PM_DEP", DEP, 1
    
    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", Left(cmbLoja.Text, InStr(cmbLoja.Text, " -") - 1), 1
    
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", txtDPK.Text, 1
    
    vBanco.Parameters.Remove "PM_PEDIDO"
    vBanco.Parameters.Add "PM_PEDIDO", txtPedido.Text, 1
    
    vBanco.Parameters.Remove "PM_SEQ"
    vBanco.Parameters.Add "PM_SEQ", txtSeq.Text, 1

    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_VDA920.PR_CON_PENDENTE(:PM_DEP,:PM_CODLOJA,:PM_CODDPK,:PM_PEDIDO,:PM_SEQ,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF Then
        MsgBox "DPK n�o encontrado", , "Aten��o"
    Else
    
        cmd1.Enabled = True
        Frame1.Enabled = True
    
        
        vVendaSP = IIf(IsNull(vObjOracle(0)), 0, vObjOracle(0))
        vVendaCP = IIf(IsNull(vObjOracle(1)), 0, vObjOracle(1))
        vOA = IIf(IsNull(vObjOracle(2)), 0, vObjOracle(2))
        vNT = IIf(IsNull(vObjOracle(3)), 0, vObjOracle(3))
        vTrans = IIf(IsNull(vObjOracle(4)), 0, vObjOracle(4))
        vFAT = IIf(IsNull(vObjOracle(5)), 0, vObjOracle(5))
        vSP = IIf(IsNull(vObjOracle(6)), 0, vObjOracle(6))
        vCP = IIf(IsNull(vObjOracle(7)), 0, vObjOracle(7))
        
        vDTNF = vObjOracle("DT_EMISSAO_NOTA")
        vNUM_ITEM = vObjOracle("NUM_ITEM_PEDIDO")
        
        txtVendaSP.Text = IIf(IsNull(vObjOracle(0)), 0, vObjOracle(0))
        txtVendaCP.Text = IIf(IsNull(vObjOracle(1)), 0, vObjOracle(1))
        txtOA.Text = IIf(IsNull(vObjOracle(2)), 0, vObjOracle(2))
        txtNT.Text = IIf(IsNull(vObjOracle(3)), 0, vObjOracle(3))
        txtTrans.Text = IIf(IsNull(vObjOracle(4)), 0, vObjOracle(4))
        txtFAT.Text = IIf(IsNull(vObjOracle(5)), 0, vObjOracle(5))
        txtSP.Text = IIf(IsNull(vObjOracle(6)), 0, vObjOracle(6))
        txtCP.Text = IIf(IsNull(vObjOracle(7)), 0, vObjOracle(7))
    
    End If


End Sub

Private Sub cmd3_Click()


    cmbLoja.ListIndex = -1
    txtDPK.Text = ""
    txtPedido.Text = ""
    txtSeq.Text = ""
    
    txtVendaCP.Text = ""
    txtVendaSP.Text = ""
    txtOA.Text = ""
    txtNT.Text = ""
    txtTrans.Text = ""
    txtFAT.Text = ""
    txtSP.Text = ""
    txtCP.Text = ""
    
    txtMotivo.Text = ""
    
    cmd1.Enabled = False

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300


    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_VDA920.PR_CON_LOJAS(:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    vVB_Generica_001.PreencheComboList vObjOracle, cmbLoja, "COD_LOJA", "NOME_FANTASIA"
    
    
End Sub


Private Sub txtANOMES_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub


Private Sub txtDPK_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtVendaSP_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVendaSP)
End Sub

Private Sub txtVendaCP_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtVendaCP)
End Sub

Private Sub txtOA_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtOA)
End Sub

Private Sub txtNT_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtNT)
End Sub

Private Sub txtTrans_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtTrans)
End Sub

Private Sub txtFAT_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtFAT)
End Sub

Private Sub txtSP_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtSP)
End Sub

Private Sub txtCP_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Valor(KeyAscii, txtCP)
End Sub


Sub LOG_PENDENTE(LOJA, DPK, PEDIDO, SEQ, CAMPO, QTDATUAL, QTDANT, MOTIVO, CODUSR)

    vBanco.Parameters.Remove "PM_CODLOJA"
    vBanco.Parameters.Add "PM_CODLOJA", Trim(LOJA), 1
    
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", DPK, 1
    
    vBanco.Parameters.Remove "PM_PEDIDO"
    vBanco.Parameters.Add "PM_PEDIDO", PEDIDO, 1
    
    vBanco.Parameters.Remove "PM_SEQ"
    vBanco.Parameters.Add "PM_SEQ", SEQ, 1
    
    vBanco.Parameters.Remove "PM_DTNF"
    vBanco.Parameters.Add "PM_DTNF", vDTNF, 1
    
    vBanco.Parameters.Remove "PM_NUMITEM"
    vBanco.Parameters.Add "PM_NUMITEM", vNUM_ITEM, 1
    
    vBanco.Parameters.Remove "PM_MOTIVO"
    vBanco.Parameters.Add "PM_MOTIVO", MOTIVO, 1
    
    vBanco.Parameters.Remove "PM_CAMPO"
    vBanco.Parameters.Add "PM_CAMPO", CAMPO, 1
    
    vBanco.Parameters.Remove "PM_QTDANT"
    vBanco.Parameters.Add "PM_QTDANT", QTDANT, 1
    
    vBanco.Parameters.Remove "PM_QTDATUAL"
    vBanco.Parameters.Add "PM_QTDATUAL", QTDATUAL, 1
    
    vBanco.Parameters.Remove "PM_CODUSR"
    vBanco.Parameters.Add "PM_CODUSR", CODUSR, 1
    

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_VDA920.PR_ACT_LOG_PENDENTE(:PM_CODLOJA,:PM_CODDPK,:PM_PEDIDO,:PM_SEQ,:PM_DTNF,:PM_NUMITEM,:PM_MOTIVO,:PM_CAMPO,:PM_QTDANT,:PM_QTDATUAL,:PM_CODUSR,:PM_CODERRO,:PM_TXTERRO)"
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)


End Sub
