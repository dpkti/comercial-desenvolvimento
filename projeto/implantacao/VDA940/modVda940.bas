Attribute VB_Name = "modVda940"
'---------------------------------------------------------------------------------------
' Module    : modVda940
' Author    : c.samuel.oliveira
' Date      : 11/07/16
' Purpose   : TI-4684
'---------------------------------------------------------------------------------------


Global lngNUM_COTACAO As Long   'NUMERO DE COTACAO

Public lngLinha As Long
Public lngCod_Usuario As Long
Public V_SQL As String
Public dblMIN_PLANO As Double
Public dblMAX_PLANO As Double
Public lngCod_Loja As Long
Public dblMG_Cuema As Double
Public dblMG_Repos As Double
Public oradatabase As Object
Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public Data_Faturamento As Date
Public txtResposta As String
Public sCod_vend As Long
Public lngCod_repres As Long

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public Sub Criar_Cursor()
    
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

End Sub

'RESPOSTA DE UMA LISTA
Public Sub Carregar_Dados()

    Dim ss As Object
    Dim i As Integer
    Dim dblValorContabil As Double
    Dim dblPrecoUnitario As Double
    Dim iQtdItens As Integer
    Dim sngDescAcresPlano As Single
    Dim sngPc_Sufr As Single
    Dim ss2 As Object
    'TI-4684
    Dim rst As Object
    Dim vVlrIcmRetidoItem As Double
    Dim vSql As String
    Dim vVlrTotalIcmRetido As Double
    Dim lngCodCli As Long
    'FIM TI-4684
    
    On Error GoTo TrataErro

    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "COT"
    vBanco.Parameters.Add "COT", lngNUM_COTACAO, 1
    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1

    vSql = "PRODUCAO.Pck_VDA230.Pr_Select_Cotacao_5(:vCursor, :Cot, :Loja)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        End
    Else
        Set ss = vBanco.Parameters("vCursor").Value
    End If

    If ss.EOF And ss.BOF Then
        ss.Close
        Set ss = Nothing
        MsgBox "Falha da consulta da cota��o", vbInformation, "Aten��o"
        Unload frmVisCotacao
        Exit Sub
    End If

    lngCod_repres = Format(IIf(IsNull(CStr(ss!cod_repres)), 0, CStr(ss!cod_repres)), "0000")
    lngCodCli = IIf(IsNull(ss!COD_CLIENTE), 0, ss!COD_CLIENTE) 'TI-4684
    frmVisCotacao.lblDados(0) = lngNUM_COTACAO
    frmVisCotacao.lblDeposito.Caption = ss!deposito
    frmVisCotacao.lblDados(2) = Format(CStr(ss("DT_COTACAO")), "DD/MM/YY")
    frmVisCotacao.lblDados(3) = Format(CStr(ss("DT_VALIDADE")), "DD/MM/YY")
    frmVisCotacao.lblDados(1) = CStr(ss("COD_VEND")) & " - " & CStr(ss("PSEUDONIMO"))
    frmVisCotacao.lblDados(4) = CStr(ss("COD_CLIENTE")) & " - " & CStr(ss("NOME_CLIENTE"))
    frmVisCotacao.lblDados(5) = CStr(ss("COD_PLANO")) & " - " & CStr(ss("DESC_PLANO"))
    frmVisCotacao.lblDescSuframa.Caption = Format$(CStr(ss!pc_desc_suframa), "STANDARD")
    sngPc_Sufr = 1 - CStr(ss("PC_DESC_SUFRAMA")) / 100
    If ss("PC_ACRESCIMO") > 0 Then
        frmVisCotacao.lbl(6).Caption = "%Acrescimo"
        frmVisCotacao.lblDados(6).Caption = Format$(CStr(ss("PC_ACRESCIMO")), "STANDARD")
        sngDescAcresPlano = 1 + CStr(ss("PC_ACRESCIMO")) / 100
    Else
        frmVisCotacao.lbl(6).Caption = "%Desconto"
        frmVisCotacao.lblDados(6).Caption = Format$(CStr(ss("PC_DESCONTO")), "STANDARD")
        sngDescAcresPlano = 1 - CStr(ss("PC_DESCONTO")) / 100
    End If
    If ss("FL_DIF_ICM") = "S" Then
        bDifIcm = vbChecked
    Else
        bDifIcm = vbUnchecked
    End If
    frmVisCotacao.chkDifIcm.Value = bDifIcm
    If IsNull(ss("OBSERV")) Then
        frmVisCotacao.lblDados(7) = ""
    Else
        frmVisCotacao.lblDados(7) = ss("OBSERV")
    End If

    'carregar dados dos itens da cotacao
    'criar consulta
    vBanco.Parameters.Remove "vCursor"
    vBanco.Parameters.Add "vCursor", 0, 3
    vBanco.Parameters("vCursor").ServerType = 102
    vBanco.Parameters("vCursor").DynasetOption = &H2&
    vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Remove "COT"
    vBanco.Parameters.Add "COT", lngNUM_COTACAO, 1
    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1

    vSql = "PRODUCAO.Pck_VDA940.Pr_Select_Item_Cotacao(:vCursor, :Cot, :Loja)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
        End
    Else
        Set ss = vBanco.Parameters("vCursor").Value
    End If

    If ss.EOF And ss.BOF Then
        ss.Close
        Set ss = Nothing
        MsgBox "Falha da consulta da cota��o", vbInformation, "Aten��o"
        Unload frmVisCotacao
        Exit Sub
    End If

    'oradatabase.Parameters.Remove ("vCursor")
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "Falha na consulta da cota��o", vbInformation, "Aten��o"
        Set ss = Nothing
        Exit Sub
    End If

    'carrega dados
    With frmVisCotacao.grdCotacao
        
        .Cols = 14 'TI-4684
        
        .Rows = ss.RecordCount + 1
        
        .ColWidth(0) = 570
        .ColWidth(1) = 450
        .ColWidth(2) = 1610
        .ColWidth(3) = 560
        .ColWidth(4) = 570
        .ColWidth(5) = 895
        .ColWidth(6) = 945
        .ColWidth(7) = 450
        .ColWidth(8) = 450
        .ColWidth(9) = 450
        .ColWidth(10) = 450
        'TI-4684
        .ColWidth(11) = 750
        .ColWidth(12) = 800
        .ColWidth(13) = 800
        'FIM TI-4684
    
        .Row = 0
        .Col = 0
        .Text = "N� Item"
        .Col = 1
        .Text = "Forn"
        .Col = 2
        .Text = "F�brica"
        .Col = 3
        .Text = "Tabela"
        .Col = 4
        .Text = "Qtd Solic"
        .Col = 5
        .Text = "Unit�rio"
        .Col = 6
        .Text = "L�quido"
        .Col = 7
        .Text = "%Per"
        .Col = 8
        .Text = "%Uf"
        .Col = 9
        .Text = "%Adic"
        .Col = 10
        .Text = "%Icm"
        'TI-4684
        .Col = 11
        .Text = "Icm Ret."
        .Col = 12
        .Text = "Mg.Cuema"
        .Col = 13
        .Text = "Mg.Repos"
        'FIM TI-4684

        ss.MoveFirst
        
        For i = 1 To .Rows - 1
            
            .Row = i

            'calcular preco unitario
            dblPrecoUnitario = CDbl(Format$(CStr(ss("PRECO_UNITARIO")) * _
                                            (1 - CStr(ss("PC_DESC1")) / 100) * _
                                            (1 - CStr(ss("PC_DESC2")) / 100) * _
                                            (1 - CStr(ss("PC_DESC3")) / 100) * _
                                            (1 - CStr(ss("PC_DIFICM")) / 100) * _
                                            sngPc_Sufr * _
                                            sngDescAcresPlano, "standard"))

            'TI-4684
            vVlrIcmRetidoItem = CalcularIcmRetido(lngCod_Loja, dblPrecoUnitario, lngCodCli, ss!cod_tributacao, ss!cod_dpk)
            vVlrTotalIcmRetido = vVlrTotalIcmRetido + (vVlrIcmRetidoItem * ss!qtd_solicitada)
            dblPrecoUnitario = dblPrecoUnitario + vVlrIcmRetidoItem
            'FIM TI-4684
        
            iQtdItens = iQtdItens + 1

            dblValorContabil = Format$(dblValorContabil + dblPrecoUnitario * CStr(ss("QTD_SOLICITADA")), "standard")

            .Col = 0
            .Text = ss("NUM_ITEM_COTACAO")
            .Col = 1
            .ColAlignment(1) = 1
            .Text = ss("COD_FORNECEDOR")
            .Col = 2
            .Text = ss("COD_FABRICA")
            .Col = 3
            .ColAlignment(3) = 2
            .Text = ss("TABELA_VENDA")
            .Col = 4
            .ColAlignment(4) = 1
            .Text = ss("QTD_SOLICITADA")
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format$(dblPrecoUnitario, "###,##0.00")
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format$(CStr(ss("QTD_SOLICITADA")) * dblPrecoUnitario, "###,###,##0.00")
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Format$(CStr(ss("PC_DESC1")), "#0.00")
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format$(CStr(ss("PC_DESC2")), "#0.00")
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format$(CStr(ss("PC_DESC3")), "#0.00")
            .Col = 10
            .ColAlignment(10) = 1
            .Text = Format$(CStr(ss("PC_DIFICM")), "#0.00")
            'TI-4684
            .Col = 11
            .ColAlignment(11) = 1
            .Text = Format$(CStr(ss("QTD_SOLICITADA")) * vVlrIcmRetidoItem, "###,###,##0.00")
            'FIM TI-4684
            
            vBanco.Parameters.Remove "LOJA"
            vBanco.Parameters.Add "LOJA", lngCod_Loja, 1
            vBanco.Parameters.Remove "COT"
            vBanco.Parameters.Add "COT", lngNUM_COTACAO, 1
            vBanco.Parameters.Remove "DPK"
            vBanco.Parameters.Add "DPK", ss!cod_dpk, 1
            vBanco.Parameters.Remove "MG_CUEMA"
            vBanco.Parameters.Add "MG_CUEMA", 0, 2
            vBanco.Parameters.Remove "MG_REPOS"
            vBanco.Parameters.Add "MG_REPOS", 0, 2

            vSql = "PRODUCAO.PR_MARGEM_ITEM_COTACAO(:loja, :cot, :dpk, :mg_cuema,:mg_repos)"
            
            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
            
            'TI-4684
            If vErro <> "" Then
                .Col = 12
                .ColAlignment(11) = 1
                .Text = 0
                .Col = 13
                .ColAlignment(12) = 1
                .Text = 0

            Else
                Set ss2 = vBanco.Parameters("vCursor").Value
            End If

            If ss2.EOF And ss2.BOF Then
                .Col = 12
                .ColAlignment(11) = 1
                .Text = 0
                .Col = 13
                .ColAlignment(12) = 1
                .Text = 0
            Else

                .Col = 12
                .ColAlignment(11) = 1
                .Text = Format(IIf(IsNull(vBanco.Parameters("mg_cuema").Value), 0, vBanco.Parameters("mg_cuema").Value), "0.00")
                .Col = 13
                .ColAlignment(12) = 1
                .Text = Format(IIf(IsNull(vBanco.Parameters("mg_REPOS").Value), 0, vBanco.Parameters("mg_REPOS").Value), "0.00")
            End If
            'FIM TI-4684
            
            ss.MoveNext
            
        Next
        
        .Row = 1
    
    End With

    'carregar qtd e valor da cotacao
    frmVisCotacao.lblDados(8) = iQtdItens
    frmVisCotacao.lblDados(9) = Format$(dblValorContabil, "###,###,###,#0.00")
    frmVisCotacao.lblMg_Cuema(10) = Format(dblMG_Cuema, "0.00")
    frmVisCotacao.lblMG_Repos(0) = Format(dblMG_Repos, "0.00")
    frmVisCotacao.lblTotalRetido = Format$(vVlrTotalIcmRetido, "###,###,###,#0.00") 'TI-4684

    vBanco.Parameters.Remove "loja"
    vBanco.Parameters.Add "loja", lngCod_Loja, 1
    vBanco.Parameters.Remove "cotacao"
    vBanco.Parameters.Add "cotacao", lngNUM_COTACAO, 1
    vBanco.Parameters.Remove "user"
    vBanco.Parameters.Add "user", lngCod_Usuario, 1

    vSql = "Producao.PCK_VDA940.Pr_Log_manutencao(:loja,:cotacao,:user)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    'mouse
    Screen.MousePointer = vbDefault

    Set ss = Nothing
    '  frmVisCotacao.Show
    
    Exit Sub
TrataErro:
    
    If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
        Resume
    ElseIf Err = 30009 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Ocorreu o erro: " & Err & " - " & Err.Description
    End If

End Sub

'TI-4684
Function CalcularIcmRetido(pCodLoja As Long, pVlrPreco As Double, pCodCliente As Long, pCodTrib As Byte, pCodDpk As Long) As Double
    On Error GoTo Trata_erro
    Dim vFlgRegime As Integer
    Dim vCont As Long
    
    CalcularIcmRetido = 0
    
    vBanco.Parameters.Remove "Pm_Flg_Regime"
    vBanco.Parameters.Add "Pm_Flg_Regime", 0, 2
    
    vBanco.Parameters.Remove "Cod_DPK"
    vBanco.Parameters.Add "Cod_DPK", pCodDpk, 1
    
    vBanco.Parameters.Remove "Loja"
    vBanco.Parameters.Add "Loja", pCodLoja, 1
    
    vBanco.Parameters.Remove "Cliente"
    vBanco.Parameters.Add "Cliente", pCodCliente, 1
    
    vSql = "PRODUCAO.pck_vda940.Pr_Select_regime_especial(:Pm_Flg_Regime, :Cod_DPK, :loja, :cliente)"
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

    If vErro <> "" Then
        Call vVB_Generica_001.ProcessaErro(vErro)
    Else
        vFlgRegime = vBanco.Parameters("Pm_Flg_Regime").Value
    End If
   
    If pVlrPreco > 0 And vFlgRegime = 1 Then
            
        vBanco.Parameters.Remove "loja"
        vBanco.Parameters.Add "loja", pCodLoja, 1
        vBanco.Parameters.Remove "cliente"
        vBanco.Parameters.Add "cliente", pCodCliente, 1
        vBanco.Parameters.Remove "dpk"
        vBanco.Parameters.Add "dpk", pCodDpk, 1
        vBanco.Parameters.Remove "trib"
        vBanco.Parameters.Add "trib", pCodTrib, 1
        vBanco.Parameters.Remove "preco"
        vBanco.Parameters.Add "preco", FmtBR(pVlrPreco), 1
        vBanco.Parameters.Remove "retido"
        vBanco.Parameters.Add "retido", 0, 2
        vBanco.Parameters.Remove "vErro"
        vBanco.Parameters.Add "vErro", 0, 2
        
        vSql = "producao.pck_vda230.pr_icms_retido(:loja,:cliente,:dpk,:trib,:preco,:retido,:vErro)"
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
        CalcularIcmRetido = vBanco.Parameters("retido").Value
    
    End If
    
Trata_erro:
    
End Function
Public Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer

    Temp = Trim(Valor)

    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function
'FIM TI-4684


