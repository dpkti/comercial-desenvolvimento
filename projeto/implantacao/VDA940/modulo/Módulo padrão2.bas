Attribute VB_Name = "modPadrao"
Option Explicit
Public vSessao As Object                            'SESS�O DO ORACLE
Public vBanco As Object                             'BANCO DO ORACLE
Public vNomeBanco As String                         'NOME DO BANCO DO ARQUIVO .INI
Public vObjOracle As Object                         'OBJETO DO ORACLE
Public vCd As Integer                               'C�DIGO DO CD CONECTADO
Public vTipoCD As String                            'TIPO DO CD CONECTADO (U, M)
Public vUsuarioBanco As String                      'USU�RIO DO BANCO
Public vUsuarioRede As String                       'USU�RIO DA REDE
Public vNomeSistema As String                       'NOME DO SISTEMA
Public vDescricaoSistema As String                  'DESCRI��O DO SISTEMA
Public vAnalistaResponsavel As String               'ANALISTA RESPONS�VEL PELO SISTEMA
Public vAnalistaBackup As String                    'ANALISTA BACKUP
Public vVB_Generica_001 As New clsVB_Generica_001
Public vErro As String
Public vSql As String
Public i As Double
Public j As Double

Sub DefinirTelaSobre()

    vNomeSistema = UCase(App.Title)
    vDescricaoSistema = "Antecipa��o Parcial da Bahia"
    vAnalistaResponsavel = "Eduardo Relvas"
    vAnalistaBackup = "Renato Henrique"
    
    frmSobre.lblNomeSistema = vNomeSistema & " - " & vDescricaoSistema
    frmSobre.lblResponsavel = UCase(vAnalistaResponsavel)
    frmSobre.lblBackup = UCase(vAnalistaBackup)
    
End Sub
