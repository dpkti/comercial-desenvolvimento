Attribute VB_Name = "modPrint"

Option Explicit

Public Enum enumAlinhamento
    [Direita] = 0
    [Esquerda] = 1
    [Centro] = 2
End Enum

Public Sub ImprimirLinha(ByVal lngX_Inicio As Long, ByVal lngY_Final As Long)

    Dim p As Printer

    Set p = Printer

    p.Line (lngX_Inicio, lngY_Final)-((p.ScaleWidth - lngX_Inicio), lngY_Final)

    Set p = Nothing

End Sub

Public Sub Imprimir(ByVal strTexto As String, _
                    ByVal intAlinhamento As enumAlinhamento, _
                    ByVal lngCurrentX As Long, _
                    ByVal lngCurrentY As Long, _
                    Optional ByVal strTamanhoTexto As String = "10", _
                    Optional ByVal blnNegrito As Boolean = False)
    
    Dim p As Printer
    
    Set p = Printer
    
    p.Font.Size = strTamanhoTexto
    p.FontBold = blnNegrito
    p.CurrentY = lngCurrentY
    
    Select Case intAlinhamento
        Case Esquerda
            p.CurrentX = lngCurrentX
        Case Direita
            p.CurrentX = lngCurrentX + p.TextWidth(strTexto)
        Case Centro
            p.CurrentX = (p.ScaleWidth - p.TextWidth(strTexto)) \ 2
    End Select

    p.Print strTexto
    
    Set p = Nothing

End Sub

Public Function SelecionaImpressora(ByVal lnghwnd As Long) As Boolean

    Dim printDlg As PrinterDlg
    Dim NewPrinterName As String
    Dim objPrinter As Printer
    Dim strsetting As String

    On Error Resume Next

    Set printDlg = New PrinterDlg

    SelecionaImpressora = False

    If Printers.Count = 0 Then
        
        Call MsgBox(vbCrLf & "N�O H� IMPRESSORA MAPEADA!     " & vbCrLf & _
                    vbCrLf & "Favor procurar pelo servicedesk", _
                    vbExclamation Or vbOKOnly, _
                    "ATEN��O!")
        
        Exit Function
    
    End If
    
    printDlg.PrinterName = Printer.DeviceName
    printDlg.DriverName = Printer.DriverName
    printDlg.Port = Printer.Port

    printDlg.PaperBin = Printer.PaperBin

    printDlg.Flags = VBPrinterConstants.cdlPDNoSelection _
                     Or VBPrinterConstants.cdlPDNoPageNums _
                     Or VBPrinterConstants.cdlPDReturnDC
                     
    Printer.TrackDefault = False
    Printer.ColorMode = vbPRCMColor
    printDlg.Copies = Printer.Copies
    printDlg.Orientation = Printer.Orientation

    If Not printDlg.ShowPrinter(lnghwnd) Then
        Exit Function
    End If

    NewPrinterName = UCase$(printDlg.PrinterName)
    
    If Printer.DeviceName <> NewPrinterName Then
        
        For Each objPrinter In Printers
            
            If UCase$(objPrinter.DeviceName) = NewPrinterName Then
                Set Printer = objPrinter
            End If
        
        Next
    
    End If

    Printer.Copies = printDlg.Copies
    Printer.Orientation = printDlg.Orientation
    Printer.ColorMode = printDlg.ColorMode
    Printer.Duplex = printDlg.Duplex
    'Printer.PaperBin = printDlg.PaperBin
    'Printer.PaperSize = printDlg.PaperSize
    Printer.PrintQuality = printDlg.PrintQuality

    With Printer

        'Debug.Print .DeviceName
        If .Orientation = 1 Then
            strsetting = "Portrait. "
        Else
            strsetting = "Landscape. "
        End If
        
        'Debug.Print "Copies = " & .Copies, "Orientation = " & _
         strsetting
        If .ColorMode = 1 Then
            strsetting = "Black and White. "
        Else
            strsetting = "Color. "
        End If
        
        'Debug.Print "ColorMode = " & strsetting
        If .Duplex = 1 Then
            strsetting = "None. "
        ElseIf .Duplex = 2 Then
            strsetting = "Horizontal/Long Edge. "
        ElseIf .Duplex = 3 Then
            strsetting = "Vertical/Short Edge. "
        Else
            strsetting = "Unknown. "
        End If
        
        'Debug.Print "Duplex = " & strsetting
        'Debug.Print "PaperBin = " & .PaperBin
        'Debug.Print "PaperSize = " & .PaperSize
        'Debug.Print "PrintQuality = " & .PrintQuality
        
        'If (printDlg.Flags And VBPrinterConstants.cdlPDPrintToFile) = VBPrinterConstants.cdlPDPrintToFile Then
        '    Debug.Print "Print to File Selected"
        'Else
        '    Debug.Print "Print to File Not Selected"
        'End If
        
        'Debug.Print "hDC = " & printDlg.hdc
    
    End With
    
    Set printDlg = Nothing
    
    SelecionaImpressora = True
    
    'Exit Function
    '**Cancel:
    '**If Err.Number = 32755 Then
    '**    Debug.Print "Cancel Selected"
    '**Else
    '**    Debug.Print "A nonCancel Error Occured - "; Err.Number
    '**End If
    
End Function

