VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmAlterarTabela 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Alterar Tabelas"
   ClientHeight    =   6585
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10095
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6585
   ScaleWidth      =   10095
   Begin TabDlg.SSTab sstAlterarTab 
      Height          =   5715
      Left            =   0
      TabIndex        =   4
      Top             =   870
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   10081
      _Version        =   393216
      Tabs            =   5
      Tab             =   4
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Alterar Campos"
      TabPicture(0)   =   "frmAlterarTabela.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "txtTamanhoAlterar"
      Tab(0).Control(1)=   "cboTipoDadosAlterar"
      Tab(0).Control(2)=   "chkPermNullAlterar"
      Tab(0).Control(3)=   "chkAutoNumAlterar"
      Tab(0).Control(4)=   "txtNomeCampoAlterar"
      Tab(0).Control(5)=   "lsvCamposSemAlteracao"
      Tab(0).Control(6)=   "cmdGravarAlterar"
      Tab(0).Control(7)=   "LsvCamposAlterar"
      Tab(0).Control(8)=   "cmdExcluirCamposAlterar"
      Tab(0).Control(9)=   "Label4"
      Tab(0).Control(10)=   "Label2"
      Tab(0).Control(11)=   "Label3"
      Tab(0).ControlCount=   12
      TabCaption(1)   =   "Excluir Campos"
      TabPicture(1)   =   "frmAlterarTabela.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lstCamposAExcluir"
      Tab(1).Control(1)=   "lstCamposExcluir"
      Tab(1).Control(2)=   "cmdAddCampoAExcluir"
      Tab(1).Control(3)=   "cmdRemoverCampoAExcluir"
      Tab(1).Control(4)=   "Label10"
      Tab(1).Control(5)=   "Label9"
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "Adicionar Campos"
      TabPicture(2)   =   "frmAlterarTabela.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "chkAutoNumAdd"
      Tab(2).Control(1)=   "txtTamanhoAdd"
      Tab(2).Control(2)=   "cboTipoDadosAdd"
      Tab(2).Control(3)=   "txtNomeCampoAdd"
      Tab(2).Control(4)=   "chkPermNullAdd"
      Tab(2).Control(5)=   "cmdGravarAdd"
      Tab(2).Control(6)=   "lsvCamposAdicionar"
      Tab(2).Control(7)=   "cmdExcluirAdd"
      Tab(2).Control(8)=   "Label8"
      Tab(2).Control(9)=   "Label7"
      Tab(2).Control(10)=   "Label6"
      Tab(2).Control(11)=   "Label5"
      Tab(2).ControlCount=   12
      TabCaption(3)   =   "Adicionar Indices"
      TabPicture(3)   =   "frmAlterarTabela.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "chkPrimary"
      Tab(3).Control(1)=   "chkUnique"
      Tab(3).Control(2)=   "chkIgnoreNulls"
      Tab(3).Control(3)=   "lstCamposIndices"
      Tab(3).Control(4)=   "txtNomeIndice"
      Tab(3).Control(5)=   "lsvIndicesExistentes"
      Tab(3).Control(6)=   "lsvIndices"
      Tab(3).Control(7)=   "cmdSalvarIndice"
      Tab(3).Control(8)=   "cmdExcluirIndice"
      Tab(3).Control(9)=   "Label16"
      Tab(3).Control(10)=   "Label13"
      Tab(3).Control(11)=   "Label14"
      Tab(3).Control(12)=   "Label15"
      Tab(3).ControlCount=   13
      TabCaption(4)   =   "Excluir Indices"
      TabPicture(4)   =   "frmAlterarTabela.frx":0070
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "Label11"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "Label12"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).Control(2)=   "cmdExcluirIndices"
      Tab(4).Control(2).Enabled=   0   'False
      Tab(4).Control(3)=   "cmdVoltarExcluir"
      Tab(4).Control(3).Enabled=   0   'False
      Tab(4).Control(4)=   "lstIndicesDisponiveis"
      Tab(4).Control(4).Enabled=   0   'False
      Tab(4).Control(5)=   "lstIndicesAExcluir"
      Tab(4).Control(5).Enabled=   0   'False
      Tab(4).ControlCount=   6
      Begin VB.CheckBox chkPrimary 
         Caption         =   "Chave Prim�ria"
         Height          =   195
         Left            =   -66510
         TabIndex        =   46
         Top             =   2700
         Width           =   1425
      End
      Begin VB.CheckBox chkUnique 
         Caption         =   "Registro Unico"
         Height          =   195
         Left            =   -66510
         TabIndex        =   45
         Top             =   2910
         Width           =   1425
      End
      Begin VB.CheckBox chkIgnoreNulls 
         Caption         =   "Ignorar Null"
         Height          =   195
         Left            =   -66510
         TabIndex        =   44
         Top             =   3120
         Width           =   1425
      End
      Begin VB.ListBox lstCamposIndices 
         Height          =   2310
         Left            =   -74880
         Style           =   1  'Checkbox
         TabIndex        =   43
         Top             =   3300
         Width           =   1845
      End
      Begin VB.TextBox txtNomeIndice 
         Height          =   315
         Left            =   -74880
         TabIndex        =   42
         Top             =   2730
         Width           =   1875
      End
      Begin VB.ListBox lstIndicesAExcluir 
         Height          =   4935
         Left            =   5430
         TabIndex        =   36
         Top             =   660
         Width           =   4575
      End
      Begin VB.ListBox lstIndicesDisponiveis 
         Height          =   4935
         Left            =   90
         TabIndex        =   35
         Top             =   660
         Width           =   4575
      End
      Begin VB.CheckBox chkAutoNumAdd 
         Caption         =   "Auto Numera��o"
         Height          =   255
         Left            =   -70080
         TabIndex        =   27
         Top             =   510
         Width           =   1545
      End
      Begin VB.TextBox txtTamanhoAdd 
         Height          =   315
         Left            =   -70950
         TabIndex        =   26
         Top             =   600
         Width           =   705
      End
      Begin VB.ComboBox cboTipoDadosAdd 
         Height          =   315
         ItemData        =   "frmAlterarTabela.frx":008C
         Left            =   -72540
         List            =   "frmAlterarTabela.frx":00AE
         Style           =   2  'Dropdown List
         TabIndex        =   25
         Top             =   600
         Width           =   1545
      End
      Begin VB.TextBox txtNomeCampoAdd 
         Height          =   315
         Left            =   -74910
         TabIndex        =   24
         Top             =   600
         Width           =   2295
      End
      Begin VB.CheckBox chkPermNullAdd 
         Caption         =   "Permitir Null"
         Height          =   255
         Left            =   -70080
         TabIndex        =   23
         Top             =   750
         Value           =   1  'Checked
         Width           =   1185
      End
      Begin VB.ListBox lstCamposAExcluir 
         Height          =   4935
         Left            =   -69510
         TabIndex        =   18
         Top             =   660
         Width           =   4515
      End
      Begin VB.ListBox lstCamposExcluir 
         Height          =   4935
         Left            =   -74910
         TabIndex        =   17
         Top             =   660
         Width           =   4515
      End
      Begin VB.TextBox txtTamanhoAlterar 
         Height          =   315
         Left            =   -70950
         TabIndex        =   10
         Top             =   2760
         Width           =   735
      End
      Begin VB.ComboBox cboTipoDadosAlterar 
         Height          =   315
         ItemData        =   "frmAlterarTabela.frx":00FC
         Left            =   -72510
         List            =   "frmAlterarTabela.frx":011E
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   2760
         Width           =   1515
      End
      Begin VB.CheckBox chkPermNullAlterar 
         Alignment       =   1  'Right Justify
         Caption         =   "Permitir Null"
         Height          =   255
         Left            =   -68520
         TabIndex        =   8
         Top             =   2790
         Value           =   1  'Checked
         Width           =   1155
      End
      Begin VB.CheckBox chkAutoNumAlterar 
         Alignment       =   1  'Right Justify
         Caption         =   "Auto Numera��o"
         Height          =   255
         Left            =   -70140
         TabIndex        =   7
         Top             =   2790
         Width           =   1515
      End
      Begin VB.TextBox txtNomeCampoAlterar 
         Height          =   315
         Left            =   -74910
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   2760
         Width           =   2295
      End
      Begin MSComctlLib.ListView lsvCamposSemAlteracao 
         Height          =   2175
         Left            =   -74940
         TabIndex        =   5
         Top             =   390
         Width           =   8955
         _ExtentX        =   15796
         _ExtentY        =   3836
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome Campo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo Dados"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tamanho"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Auto Numera��o"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Perm Null"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Valor Padr�o"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Indexado"
            Object.Width           =   2540
         EndProperty
      End
      Begin Bot�o.cmd cmdGravarAlterar 
         Height          =   495
         Left            =   -67050
         TabIndex        =   11
         Top             =   2610
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   873
         BTYPE           =   3
         TX              =   "Salvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":016C
         PICN            =   "frmAlterarTabela.frx":0188
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView LsvCamposAlterar 
         Height          =   1965
         Left            =   -74940
         TabIndex        =   12
         Top             =   3120
         Width           =   8925
         _ExtentX        =   15743
         _ExtentY        =   3466
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome Campo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo Dados"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tamanho"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Auto Numera��o"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Perm Null"
            Object.Width           =   1764
         EndProperty
      End
      Begin Bot�o.cmd cmdExcluirCamposAlterar 
         Height          =   525
         Left            =   -67110
         TabIndex        =   16
         Top             =   5100
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":0A62
         PICN            =   "frmAlterarTabela.frx":0A7E
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAddCampoAExcluir 
         Height          =   525
         Left            =   -70260
         TabIndex        =   19
         Top             =   1950
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   ">>"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":0BD8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdRemoverCampoAExcluir 
         Height          =   525
         Left            =   -70230
         TabIndex        =   20
         Top             =   3060
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   "<<"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":0BF4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdGravarAdd 
         Height          =   585
         Left            =   -66360
         TabIndex        =   28
         Top             =   510
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   1032
         BTYPE           =   3
         TX              =   "Adicionar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":0C10
         PICN            =   "frmAlterarTabela.frx":0C2C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvCamposAdicionar 
         Height          =   3915
         Left            =   -74910
         TabIndex        =   29
         Top             =   1170
         Width           =   9915
         _ExtentX        =   17489
         _ExtentY        =   6906
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome Campo"
            Object.Width           =   4410
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo Dados"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tamanho"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Auto Numera��o"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Perm Null"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Campo Origem"
            Object.Width           =   4410
         EndProperty
      End
      Begin Bot�o.cmd cmdExcluirAdd 
         Height          =   525
         Left            =   -66300
         TabIndex        =   30
         Top             =   5130
         Width           =   1305
         _ExtentX        =   2302
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":1906
         PICN            =   "frmAlterarTabela.frx":1922
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdVoltarExcluir 
         Height          =   525
         Left            =   4770
         TabIndex        =   37
         Top             =   3240
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   "<<"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":1A7C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirIndices 
         Height          =   525
         Left            =   4770
         TabIndex        =   38
         Top             =   1950
         Width           =   585
         _ExtentX        =   1032
         _ExtentY        =   926
         BTYPE           =   3
         TX              =   ">>"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":1A98
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvIndicesExistentes 
         Height          =   1845
         Left            =   -74910
         TabIndex        =   41
         Top             =   600
         Width           =   9945
         _ExtentX        =   17542
         _ExtentY        =   3254
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome do Indice"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campos do Indice"
            Object.Width           =   3175
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Primary"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Unique"
            Object.Width           =   1235
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Ignore Nulls"
            Object.Width           =   1235
         EndProperty
      End
      Begin MSComctlLib.ListView lsvIndices 
         Height          =   2925
         Left            =   -72960
         TabIndex        =   47
         Top             =   2700
         Width           =   6375
         _ExtentX        =   11245
         _ExtentY        =   5159
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome do Indice"
            Object.Width           =   5292
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campos do Indice"
            Object.Width           =   5292
         EndProperty
      End
      Begin Bot�o.cmd cmdSalvarIndice 
         Height          =   345
         Left            =   -66540
         TabIndex        =   48
         Top             =   4860
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Salvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":1AB4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirIndice 
         Height          =   345
         Left            =   -66540
         TabIndex        =   52
         Top             =   5250
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmAlterarTabela.frx":1AD0
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Indices que j� existem no banco a ser alterado"
         Height          =   195
         Left            =   -74910
         TabIndex        =   53
         Top             =   390
         Width           =   3285
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "�ndices Existentes"
         Height          =   195
         Left            =   -72960
         TabIndex        =   51
         Top             =   2520
         Width           =   1275
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Campos Dispon�veis"
         Height          =   195
         Left            =   -74880
         TabIndex        =   50
         Top             =   3090
         Width           =   1455
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Indice"
         Height          =   195
         Left            =   -74880
         TabIndex        =   49
         Top             =   2520
         Width           =   1125
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "�ndices que ser�o exclu�dos"
         Height          =   195
         Left            =   5430
         TabIndex        =   40
         Top             =   450
         Width           =   1995
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "�ndices Existentes na tabela"
         Height          =   195
         Left            =   90
         TabIndex        =   39
         Top             =   450
         Width           =   1980
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "Tamanho"
         Height          =   195
         Left            =   -70950
         TabIndex        =   34
         Top             =   390
         Width           =   675
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Dados"
         Height          =   195
         Left            =   -72540
         TabIndex        =   33
         Top             =   390
         Width           =   1050
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Campo"
         Height          =   195
         Left            =   -74910
         TabIndex        =   32
         Top             =   390
         Width           =   1185
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Lista de Campos"
         Height          =   195
         Left            =   -74910
         TabIndex        =   31
         Top             =   960
         Width           =   1170
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         Caption         =   "Campos que ser�o Exclu�dos"
         Height          =   195
         Left            =   -69510
         TabIndex        =   22
         Top             =   450
         Width           =   2070
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         Caption         =   "Campos Dispon�veis"
         Height          =   195
         Left            =   -74910
         TabIndex        =   21
         Top             =   450
         Width           =   1455
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Dados"
         Height          =   195
         Left            =   -72540
         TabIndex        =   15
         Top             =   2580
         Width           =   1050
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Tamanho"
         Height          =   195
         Left            =   -70980
         TabIndex        =   14
         Top             =   2580
         Width           =   675
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Campo"
         Height          =   195
         Left            =   -74910
         TabIndex        =   13
         Top             =   2580
         Width           =   1185
      End
   End
   Begin VB.ComboBox cboTabelas 
      Height          =   315
      Left            =   6060
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   390
      Width           =   3915
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   0
      Top             =   765
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAlterarTabela.frx":1AEC
      PICN            =   "frmAlterarTabela.frx":1B08
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Tabelas"
      Height          =   195
      Left            =   6060
      TabIndex        =   3
      Top             =   150
      Width           =   570
   End
End
Attribute VB_Name = "frmAlterarTabela"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim litem As ListItem
Dim iii As Integer
Dim vCod_Tabela As Double
Dim vCod_Indice As Double

Private Sub cboCampoAdd_Click()
    If InStr(1, "Integer, Double, Date, Currency, Byte, Boolean, Long, Memo", cboTipoDadosAdd) > 0 Then
        txtTamanhoAdd.Enabled = False
    Else
        txtTamanhoAdd.Enabled = True
    End If
End Sub

Private Sub cboTabelas_Click()
    Dim vAdicionarNoLst As Boolean
    
    'Pegar o codigo da tabela, se a tabela n�o existir ent�o inserir
    vCod_Tabela = Get_Cod_Tabela(cboTabelas)
    
    'Preencher A lista de campos a serem excluidos da tabela selecionada
    Preencher_Lst_Campos_A_Excluir

    '*************************************************************************
    'Preencher o ListView dos campos sem altera��es
    'Este listview conter� os campos do banco de dados ainda sem as altera��es
    'Que seria na verdade o banco de dados que est� atualmente no Cliente.
    '*************************************************************************
    If cboTabelas.ListIndex <> -1 Then
       lsvCamposSemAlteracao.ListItems.Clear
       For i = 0 To cboTabelas.ListCount - 1
           If UCase(DB_Remoto.TableDefs(i).Name) = cboTabelas Then
              For ii = 0 To DB_Remoto.TableDefs(i).Fields.Count - 1
                  Set litem = lsvCamposSemAlteracao.ListItems.Add
                  litem = DB_Remoto.TableDefs(i).Fields(ii).Name
                  litem.SubItems(1) = CampoTipo(DB_Remoto.TableDefs(i).Fields(ii).Type).Nome
                  litem.SubItems(2) = DB_Remoto.TableDefs(i).Fields(ii).Size
                  litem.SubItems(3) = IIf(InStr(1, DB_Remoto.TableDefs(i).Fields(ii).Attributes, "AutoIncrement") > 0, "Sim", "N�o")
                  litem.SubItems(4) = IIf(DB_Remoto.TableDefs(i).Fields(ii).AllowZeroLength = True, "N�o", "Sim")
                  litem.SubItems(5) = DB_Remoto.TableDefs(i).Fields(ii).DefaultValue
              
                  'Se o campo que estou inserindo no LstCamposExcluir j� existir no a Excluir
                  'ent�o n�o inserir o campo no lstCamposAExcluir
                  vAdicionarNoLst = True
                  For iii = 0 To lstCamposAExcluir.ListCount - 1
                      If DB_Remoto.TableDefs(i).Fields(ii).Name = UCase(lstCamposAExcluir.List(i)) Then
                         vAdicionarNoLst = False
                         Exit For
                      End If
                  Next
                  If vAdicionarNoLst = True Then lstCamposExcluir.AddItem DB_Remoto.TableDefs(i).Fields(ii).Name
              Next
           End If
       Next
    End If

    '*************************************************************************
    'Preencher o ListView com os campos j� alterados
    'Este listview conter� os campos do banco de dados j� com as altera��es
    'Que seria na verdade o banco de dados que ir� alterar o existente no cliente
    '*************************************************************************
    If cboTabelas.ListIndex <> -1 Then
        Preencher_Lsv_Campos_Alterados
        Preencher_Lsv_Campos_Adicionados
        Preenvher_Lst_Indices_Excluir
        
        Pegar_Indices
    End If
    
    
End Sub


Private Sub cboTipoDadosAdd_Click()
    If InStr(1, "Integer, Double, Date, Currency, Byte, Boolean, Long, Memo", cboTipoDadosAdd) > 0 Then
        txtTamanhoAdd.Enabled = False
    Else
        txtTamanhoAdd.Enabled = True
    End If
End Sub

Private Sub cboTipoDadosAlterar_Click()
    'Date
    If lsvCamposSemAlteracao.SelectedItem.SubItems(1) = "Date" Then
       If cboTipoDadosAlterar <> "Text" And cboTipoDadosAlterar <> "Date" Then
          MsgBox "N�o � poss�vel alterar o tipo de dados " & lsvCamposSemAlteracao.SelectedItem.SubItems(1) & " para " & cboTipoDadosAlterar, vbInformation, "Aten��o"
       End If
    'Integer
    ElseIf lsvCamposSemAlteracao.SelectedItem.SubItems(1) = "Integer" Then
       If InStr(1, "Date, Boolean, Byte, Memo", cboTipoDadosAlterar) > 0 Then
          MsgBox "N�o � poss�vel alterar o tipo de dados " & lsvCamposSemAlteracao.SelectedItem.SubItems(1) & " para " & cboTipoDadosAlterar, vbInformation, "Aten��o"
       End If
    'Double
    ElseIf lsvCamposSemAlteracao.SelectedItem.SubItems(1) = "Double" Then
       If InStr(1, "Currency, Text, Double", cboTipoDadosAlterar) = 0 Then
          MsgBox "N�o � poss�vel alterar o tipo de dados " & lsvCamposSemAlteracao.SelectedItem.SubItems(1) & " para " & cboTipoDadosAlterar, vbInformation, "Aten��o"
       End If
    'Text
    ElseIf lsvCamposSemAlteracao.SelectedItem.SubItems(1) = "Text" Then
       If InStr(1, "Text, Memo", cboTipoDadosAlterar) = 0 Then
          MsgBox "N�o � poss�vel alterar o tipo de dados " & lsvCamposSemAlteracao.SelectedItem.SubItems(1) & " para " & cboTipoDadosAlterar, vbInformation, "Aten��o"
       End If
    'Memo
    ElseIf lsvCamposSemAlteracao.SelectedItem.SubItems(1) = "Memo" Then
       If cboTipoDadosAlterar = "Text" Then
          MsgBox "Caso seja feita a altera��o do tipo Memo para tipo Text," & vbCrLf & _
                 "os caracteres que excederem o tamanho do campo ser�o perdidos."
       ElseIf InStr(1, "Memo", cboTipoDadosAlterar) = 0 Then
          MsgBox "N�o � poss�vel alterar o tipo de dados " & lsvCamposSemAlteracao.SelectedItem.SubItems(1) & " para " & cboTipoDadosAlterar, vbInformation, "Aten��o"
       End If
    End If
    
    If InStr(1, "Integer, Double, Date, Currency, Byte, Boolean, Long, Memo", cboTipoDadosAlterar) > 0 Then
        txtTamanhoAlterar.Enabled = False
    Else
        txtTamanhoAlterar.Enabled = True
    End If
    
End Sub

Private Sub cmdAddCampoAExcluir_Click()
    If lstCamposExcluir.ListIndex = -1 Then Exit Sub
    
    For i = 1 To LsvCamposAlterar.ListItems.Count
        If LsvCamposAlterar.ListItems(i) = lstCamposExcluir Then
           MsgBox "Este campo n�o pode ser exclu�do pois j� est� como um campo a ser alterado.", vbInformation, "Aten��o"
           Exit Sub
        End If
    Next

    If lstCamposAExcluir.ListCount = 0 Then
       Conn.Execute "Insert into Tabelas (Nome_tabela) values ('" & cboTabelas & "')"
    End If
    
    Conn.Execute "insert into Campos_excluir (Cod_tabela, Nome_Campo) Values (" & vCod_Tabela & ",'" & lstCamposExcluir & "')"
    
    lstCamposAExcluir.AddItem lstCamposExcluir
    lstCamposExcluir.RemoveItem lstCamposExcluir.ListIndex
    
End Sub

Private Sub cmdExcluirAdd_Click()
    If lsvCamposAdicionar.ListItems.Count <= 0 Then Exit Sub
        
    Conn.Execute "Delete from Campos_Adicionar where Cod_tabela=" & vCod_Tabela & " and Nome_Campo='" & lsvCamposAdicionar.SelectedItem & "'"
    Preencher_Lsv_Campos_Adicionados
    
End Sub

Private Sub cmdExcluirCamposAlterar_Click()
    If LsvCamposAlterar.SelectedItem Is Nothing Then Exit Sub
    If MsgBox("Confirma Exclus�o do item selecionado ?", vbQuestion + vbYesNo) = vbYes Then
        Conn.Execute "Delete from Campos where Cod_tabela = " & vCod_Tabela & " and Nome_campo='" & LsvCamposAlterar.SelectedItem & "'"
        LsvCamposAlterar.ListItems.Remove LsvCamposAlterar.SelectedItem.Index
        Preencher_Lsv_Campos_Alterados
    End If
End Sub

Private Sub cmdExcluirIndice_Click()
    Dim Nome_Indice As String
    
    If MsgBox("Confirma exclus�o do indice selecionado.", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
       If lsvIndices.SelectedItem = "" Then
          For i = lsvIndices.SelectedItem.Index To 1 Step -1
              If lsvIndices.ListItems(i) <> "" Then
                 Nome_Indice = lsvIndices.ListItems(i)
                 Exit For
              End If
          Next
       Else
          Nome_Indice = lsvIndices.SelectedItem
       End If
       Conn.Execute "Delete from Indices where Nome_indice ='" & Nome_Indice & "'"
       Preenche_lsv_indices
    End If
End Sub

Private Sub cmdExcluirIndices_Click()
    
    Conn.Execute "Insert into Indices_Excluir Values (" & vCod_Tabela & ",'" & lstIndicesDisponiveis & "')"

    lstIndicesAExcluir.AddItem lstIndicesDisponiveis
    lstIndicesDisponiveis.RemoveItem lstIndicesDisponiveis.ListIndex
End Sub

Private Sub cmdGravarAdd_Click()
    Dim Regs As Integer
    If cboTabelas.ListIndex = -1 Then
        MsgBox "Digite um nome para a tabela.", vbInformation, "Aten��o"
        cboTabelas.SetFocus
        Exit Sub
    End If
        
    If cboTipoDadosAdd = "" Then
        MsgBox "Escolha um tipo de dados", vbInformation, "Aten��o"
        cboTipoDadosAdd.SetFocus
        Exit Sub
    End If
    
    If txtTamanhoAdd.Enabled = True Then
       If txtTamanhoAdd = "" Then
          MsgBox "Digite o tamanho do campo", vbInformation, "Aten��o"
          txtTamanhoAdd.SetFocus
          Exit Sub
       End If
    End If
    
    Conn.Execute "Update Campos_Adicionar Set " & _
                 "Tipo_Dados='" & cboTipoDadosAdd & "'," & _
                 "Tamanho_Campo=" & IIf(txtTamanhoAdd = "", 0, txtTamanhoAdd) & "," & _
                 "AutoNumeracao=" & chkAutoNumAdd.Value & "," & _
                 "PermNull=" & chkPermNullAdd.Value & _
                 " WHERE COD_TABELA = " & vCod_Tabela & _
                 " AND Nome_Campo = '" & txtNomeCampoAdd & "'", Regs
                 
    If Regs <= 0 Then
        Conn.Execute "Insert into Campos_Adicionar (Cod_Tabela, Nome_Campo, Tipo_Dados, Tamanho_Campo, AutoNumeracao, PermNull) Values (" & _
                     vCod_Tabela & ",'" & txtNomeCampoAdd & "','" & cboTipoDadosAdd & "'," & IIf(txtTamanhoAdd = "", 0, txtTamanhoAdd) & "," & chkAutoNumAdd.Value & "," & chkPermNullAdd & ")"
    End If
    
    Limpar_campo
    
    Preencher_Lsv_Campos_Adicionados
End Sub

Private Sub cmdGravarAlterar_Click()
    Dim Regs As Integer
    If cboTabelas.ListIndex = -1 Then
        MsgBox "Digite um nome para a tabela.", vbInformation, "Aten��o"
        cboTabelas.SetFocus
        Exit Sub
    End If
        
    If cboTipoDadosAlterar = "" Then
        MsgBox "Escolha um tipo de dados", vbInformation, "Aten��o"
        cboTipoDadosAlterar.SetFocus
        Exit Sub
    End If
    
    If txtTamanhoAlterar.Enabled = True Then
       If txtTamanhoAlterar = "" Then
          MsgBox "Digite o tamanho do campo", vbInformation, "Aten��o"
          txtTamanhoAlterar.SetFocus
          Exit Sub
       End If
    End If
    
    If vCod_Tabela = 0 Then vCod_Tabela = Get_Cod_Tabela(cboTabelas)
    
    Conn.Execute "Update Campos_Alterar Set " & _
                 "Tipo_Dados='" & cboTipoDadosAlterar & "'," & _
                 "Tamanho_Campo=" & IIf(txtTamanhoAlterar = "", 0, txtTamanhoAlterar) & "," & _
                 "AutoNumeracao=" & chkAutoNumAlterar.Value & "," & _
                 "PermNull=" & chkPermNullAlterar.Value & _
                 " WHERE COD_TABELA = " & vCod_Tabela & _
                 " AND Nome_Campo = '" & txtNomeCampoAlterar & "'", Regs
                 
    If Regs <= 0 Then
        Conn.Execute "Insert into Campos_Alterar (Cod_Tabela, Nome_Campo, Tipo_Dados, Tamanho_Campo, AutoNumeracao, PermNull) Values (" & _
                     vCod_Tabela & ",'" & txtNomeCampoAlterar & "','" & cboTipoDadosAlterar & "'," & IIf(txtTamanhoAlterar = "", 0, txtTamanhoAlterar) & "," & chkAutoNumAlterar.Value & "," & chkPermNullAlterar & ")"
    End If
    
    Limpar_campo
    
    Preencher_Lsv_Campos_Alterados
End Sub

Private Sub cmdRemoverCampoAExcluir_Click()
    If lstCamposAExcluir.ListCount <= 0 Or Me.lstCamposAExcluir.ListIndex = -1 Then
       Exit Sub
    Else
       Conn.Execute "Delete from Campos_Excluir where Cod_tabela=" & vCod_Tabela & " And Nome_campo='" & lstCamposAExcluir & "'"
       lstCamposExcluir.AddItem lstCamposAExcluir
       lstCamposAExcluir.RemoveItem lstCamposAExcluir.ListIndex
    
       If lstCamposAExcluir.ListCount = 0 Then
          Conn.Execute "Delete from Tabelas where cod_tabela=" & vCod_Tabela & " and Tipo_acao=3"
       End If
    End If
End Sub

Private Sub cmdSalvarIndice_Click()
    Dim Item_lista As Byte
    
    If lstCamposIndices.SelCount <= 0 Then
        MsgBox "Marque os campos para compor o Indice.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If txtNomeIndice = "" Then
        MsgBox "Digite um nome para o indice.", vbInformation, "Aten��o"
        txtNomeIndice.SetFocus
        Exit Sub
    End If
    
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Indices Where Cod_Tabela=" & vCod_Tabela & _
             " AND Nome_indice = '" & UCase(txtNomeIndice) & "'", Conn, adOpenKeyset, adLockOptimistic
    
    If rst.RecordCount <= 0 Then
       Conn.Execute "Insert into Indices (Cod_tabela, Nome_Indice, Chave_Primaria, Unico, Ignorar_null) Values (" & vCod_Tabela & ",'" & UCase(txtNomeIndice) & "'," & chkPrimary.Value & "," & chkUnique.Value & "," & chkIgnoreNulls.Value & ")"
       Set rst = New ADODB.Recordset
       rst.Open "Select cod_indice from Indices where cod_tabela=" & vCod_Tabela & " and Nome_indice='" & UCase(txtNomeIndice) & "'", Conn, adOpenKeyset, adLockOptimistic
    End If
    
    vCod_Indice = rst!Cod_indice
    
    For i = 0 To lstCamposIndices.ListCount - 1
        If lstCamposIndices.Selected(i) = True Then
            Conn.Execute "Insert into Campos_indices Values (" & vCod_Indice & ",'" & lstCamposIndices.List(i) & "')"
        End If
    Next
    
    Preenche_lsv_indices

    txtNomeIndice = ""
    
    Preencher_Campos_Indices
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub cmdVoltarExcluir_Click()
    Conn.Execute "Delete from Indices_Excluir where Cod_Tabela=" & vCod_Tabela & " and Nome_indice='" & lstIndicesAExcluir & "'"
    lstIndicesDisponiveis.AddItem lstIndicesAExcluir
    lstIndicesAExcluir.RemoveItem lstIndicesAExcluir.ListIndex
End Sub

Private Sub Form_Load()
    
    Me.Top = (FrmMDI.Height - (Me.Height + FrmMDI.SSPanel1.Height + 400)) / 2
    Me.Left = (FrmMDI.Width - Me.Width) / 2
    
    For i = 0 To DB_Remoto.TableDefs.Count - 1
        If InStr(UCase(DB_Remoto.TableDefs(i).Name), "MSYS") = 0 Then
            cboTabelas.AddItem UCase(DB_Remoto.TableDefs(i).Name)
        End If
    Next
    
    Informacao_TipoDadosCampo
End Sub


Private Sub lsvCamposAdicionar_DblClick()
    If cboTabelas.ListIndex = -1 Then
       MsgBox "Selecione uma tabela.", vbInformation, "Aten��o"
       cboTabelas.SetFocus
       Exit Sub
    End If
        
    frmCampoOrigem.Show 1
    
    Conn.Execute "Update Campos_Adicionar set Campo_origem = '" & lsvCamposAdicionar.SelectedItem.SubItems(5) & "' Where Cod_Tabela=" & vCod_Tabela & " and Nome_campo='" & lsvCamposAdicionar.SelectedItem & "'"

End Sub

Private Sub LsvCamposAlterar_ItemClick(ByVal Item As MSComctlLib.ListItem)
    For i = 1 To Me.lsvCamposSemAlteracao.ListItems.Count
        If lsvCamposSemAlteracao.ListItems(i) = LsvCamposAlterar.SelectedItem Then
            lsvCamposSemAlteracao.ListItems(i).Selected = True
            Exit For
        End If
    Next
End Sub

Private Sub lsvCamposSemAlteracao_Click()
    txtNomeCampoAlterar = lsvCamposSemAlteracao.SelectedItem
    cboTipoDadosAlterar = lsvCamposSemAlteracao.SelectedItem.SubItems(1)
    If InStr(1, "Integer, Double, Date, Currency, Byte, Boolean, Long, Memo", cboTipoDadosAlterar) > 0 Then
        txtTamanhoAlterar.Enabled = False
    Else
        txtTamanhoAlterar.Enabled = True
    End If
    txtTamanhoAlterar = lsvCamposSemAlteracao.SelectedItem.SubItems(2)
    chkAutoNumAlterar = IIf(lsvCamposSemAlteracao.SelectedItem.SubItems(3) = "Sim", 1, 0)
    chkPermNullAlterar = IIf(lsvCamposSemAlteracao.SelectedItem.SubItems(4) = "Sim", 1, 0)
End Sub

Sub Limpar_campo()
    txtNomeCampoAlterar = ""
    cboTipoDadosAlterar.ListIndex = -1
    txtTamanhoAlterar = ""
    chkAutoNumAlterar.Value = 0
End Sub

Sub Preencher_Lsv_Campos_Alterados()
    Set rst = New ADODB.Recordset
    rst.Open "Select * from campos_Alterar, tabelas where campos_alterar.cod_tabela = tabelas.cod_tabela and tabelas.cod_tabela =" & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    LsvCamposAlterar.ListItems.Clear
    
    For i = 1 To rst.RecordCount
        Set litem = LsvCamposAlterar.ListItems.Add
        litem = rst!Nome_Campo
        litem.SubItems(1) = IIf(IsNull(rst!Tipo_Dados), "", rst!Tipo_Dados)
        litem.SubItems(2) = IIf(IsNull(rst!Tamanho_Campo), "", rst!Tamanho_Campo)
        litem.SubItems(3) = IIf(rst!AutoNumeracao = 1, "Sim", "N�o")
        litem.SubItems(4) = IIf(rst!PermNull = 1, "Sim", "N�o")
        rst.MoveNext
    Next
End Sub

Private Sub lsvCamposSemAlteracao_ItemClick(ByVal Item As MSComctlLib.ListItem)
    For i = 1 To LsvCamposAlterar.ListItems.Count
        If LsvCamposAlterar.ListItems(i) = lsvCamposSemAlteracao.SelectedItem Then
            LsvCamposAlterar.ListItems(i).Selected = True
            Exit For
        End If
    Next
End Sub

Sub Preencher_Lst_Campos_A_Excluir()
    Set rst = New ADODB.Recordset
    rst.Open "Select * from campos_excluir Where cod_tabela=" & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    If rst.RecordCount > 0 Then
       For i = 1 To rst.RecordCount
          lstCamposAExcluir.AddItem rst!Nome_Campo
          rst.MoveNext
       Next
    End If
End Sub

Sub Preencher_Lsv_Campos_Adicionados()
    Set rst = New ADODB.Recordset
    rst.Open "Select * from campos_Adicionar, tabelas where campos_adicionar.cod_tabela = tabelas.cod_tabela and tabelas.cod_tabela =" & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lsvCamposAdicionar.ListItems.Clear
    
    For i = 1 To rst.RecordCount
        Set litem = lsvCamposAdicionar.ListItems.Add
        litem = rst!Nome_Campo
        litem.SubItems(1) = IIf(IsNull(rst!Tipo_Dados), "", rst!Tipo_Dados)
        litem.SubItems(2) = IIf(IsNull(rst!Tamanho_Campo), "", rst!Tamanho_Campo)
        litem.SubItems(3) = IIf(rst!AutoNumeracao = 1, "Sim", "N�o")
        litem.SubItems(4) = IIf(rst!PermNull = 1, "Sim", "N�o")
        rst.MoveNext
    Next
End Sub

Sub Pegar_Indices()
    Dim Existe_no_Excluir As Boolean
    
    lstIndicesDisponiveis.Clear
    
    For i = 0 To DB_Remoto.TableDefs.Count - 1
        If UCase(DB_Remoto.TableDefs(i).Name) = UCase(cboTabelas) Then
            Set tb = DB_Remoto.TableDefs(i)
            Exit For
        End If
    Next

    'Preencher LST de Indices Disponiveis se o indice n�o existir no LST indice a Excluir
    For i = 0 To tb.Indexes.Count - 1
        Existe_no_Excluir = False
        For ii = 0 To lstIndicesAExcluir.ListCount - 1
            If UCase(tb.Indexes(i).Name) = UCase(lstIndicesAExcluir.List(i)) Then
               Existe_no_Excluir = True
               Exit For
            End If
        Next
        If Existe_no_Excluir = False Then
            lstIndicesDisponiveis.AddItem tb.Indexes(i).Name
        End If
    Next

    'Preencher o Lst de Campos da tabela selecionada
    lstCamposIndices.Clear
    For i = 0 To tb.Fields.Count - 1
        lstCamposIndices.AddItem tb.Fields(i).Name
    Next

    Preencher_Lsv_Indices_Existentes_Bco_Alterar tb
    Preenche_lsv_indices

End Sub

Sub Preenvher_Lst_Indices_Excluir()
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Indices_Excluir where cod_tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lstIndicesAExcluir.Clear
    
    For i = 1 To rst.RecordCount
        lstIndicesAExcluir.AddItem rst!Nome_Indice
        rst.MoveNext
    Next
End Sub

Sub Preencher_Lsv_Indices_Existentes_Bco_Alterar(tb As TableDef)
    Dim Campo_Index() As String
    
    lsvIndicesExistentes.ListItems.Clear
    
    For i = 0 To tb.Indexes.Count - 1
        Set litem = lsvIndicesExistentes.ListItems.Add
        litem = tb.Indexes(i).Name
        Campo_Index = Split(Replace(tb.Indexes(i).Fields, "+", ""), ";")
        For ii = 0 To UBound(Campo_Index)
            If ii = 0 Then
                litem.SubItems(1) = Campo_Index(ii)
                litem.SubItems(2) = IIf(tb.Indexes(i).Primary = True, "Sim", "N�o")
                litem.SubItems(3) = IIf(tb.Indexes(i).Unique = True, "Sim", "N�o")
                litem.SubItems(4) = IIf(tb.Indexes(i).IgnoreNulls = True, "Sim", "N�o")
            Else
                Set litem = lsvIndicesExistentes.ListItems.Add
                litem = ""
                litem.SubItems(1) = Campo_Index(ii)
            End If
        Next
    Next
End Sub

Private Sub lsvIndices_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim Nome_Indice As String
    
    If lsvIndices.SelectedItem <> "" Then
        Nome_Indice = lsvIndices.SelectedItem
    Else
        For i = lsvIndices.SelectedItem.Index To 1 Step -1
            If lsvIndices.ListItems(i) <> "" Then
               Nome_Indice = lsvIndices.ListItems(i)
            End If
        Next
    End If
    
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Indices where Nome_indice = '" & Nome_Indice & "'", Conn, adOpenKeyset, adLockOptimistic
    
    chkPrimary.Value = rst!chave_primaria
    chkUnique.Value = rst!Unico
    chkIgnoreNulls.Value = rst!ignorar_null
    
    'Marcar os campos que compoem o indice
    For i = 1 To lsvIndices.ListItems.Count
        For ii = 0 To lstCamposIndices.ListCount - 1
            If lstCamposIndices.List(ii) = lsvIndices.ListItems(i).SubItems(1) Then
               lstCamposIndices.Selected(ii) = True
               Exit For
            Else
               lstCamposIndices.Selected(ii) = False
            End If
        Next
    Next
End Sub

Private Sub lsvIndices_LostFocus()
    chkPrimary.Value = 0
    chkUnique.Value = 0
    chkIgnoreNulls.Value = 0
End Sub

Sub Preenche_lsv_indices()
    Dim Nome_Indice As String
    
    Set rst = New ADODB.Recordset
    rst.Open "Select A.Nome_Indice, B.Nome_campo from Indices A, Campos_indices B where A.Cod_indice = B.Cod_indice and A.cod_tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lsvIndices.ListItems.Clear
    
    For i = 1 To rst.RecordCount
        If rst!Nome_Indice <> Nome_Indice Then
            Set litem = lsvIndices.ListItems.Add
            litem = rst!Nome_Indice
            litem.SubItems(1) = rst!Nome_Campo
            Nome_Indice = rst!Nome_Indice
        Else
            Set litem = lsvIndices.ListItems.Add
            litem.SubItems(1) = rst!Nome_Campo
        End If
        rst.MoveNext
    Next
End Sub
Sub Preencher_Campos_Indices()
        
    For i = 0 To cboTabelas.ListCount - 1
        If UCase(DB_Remoto.TableDefs(i).Name) = cboTabelas Then
           Set tb = DB_Remoto.TableDefs(i)
           Exit For
        End If
    Next
    
    lstCamposIndices.Clear
    
    For i = 0 To tb.Fields.Count - 1
        lstCamposIndices.AddItem tb.Fields(i).Name
    Next
End Sub

Private Sub txtTamanhoAdd_KeyPress(KeyAscii As Integer)
    If KeyAscii < 48 Or KeyAscii > 57 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub
