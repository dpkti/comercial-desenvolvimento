VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm FrmMDI 
   BackColor       =   &H8000000C&
   Caption         =   "MDIForm1"
   ClientHeight    =   5700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8820
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   Tag             =   "0"
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1125
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8820
      _Version        =   65536
      _ExtentX        =   15557
      _ExtentY        =   1984
      _StockProps     =   15
      BackColor       =   8421504
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Autosize        =   3
      Begin Bot�o.cmd cmdExcluirTabelas 
         Height          =   1005
         Left            =   5670
         TabIndex        =   3
         TabStop         =   0   'False
         Tag             =   "4"
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Excluir Tabelas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":0000
         PICN            =   "FrmMDI.frx":001C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAdicionarTabelas 
         Height          =   1005
         Left            =   6720
         TabIndex        =   1
         TabStop         =   0   'False
         Tag             =   "2"
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Adicionar Tabelas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":62B6
         PICN            =   "FrmMDI.frx":62D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdAlterarTabelas 
         Height          =   1005
         Left            =   4620
         TabIndex        =   2
         TabStop         =   0   'False
         Tag             =   "3"
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Alterar Tabelas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":6FAC
         PICN            =   "FrmMDI.frx":6FC8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSair 
         Height          =   1005
         Left            =   7770
         TabIndex        =   4
         TabStop         =   0   'False
         Tag             =   "5"
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":7CA2
         PICN            =   "FrmMDI.frx":7CBE
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdLimpar 
         Height          =   1005
         Left            =   1440
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Limpar Tabelas"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":DF58
         PICN            =   "FrmMDI.frx":DF74
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdProcessos 
         Height          =   1005
         Left            =   2490
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Processos Existentes"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":13CD6
         PICN            =   "FrmMDI.frx":13CF2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExecutaveis 
         Height          =   1005
         Left            =   3570
         TabIndex        =   7
         TabStop         =   0   'False
         Tag             =   "1"
         ToolTipText     =   "Sobre"
         Top             =   60
         Width           =   1065
         _ExtentX        =   1879
         _ExtentY        =   1773
         BTYPE           =   3
         TX              =   "Execut�vel"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   12640511
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "FrmMDI.frx":19914
         PICN            =   "FrmMDI.frx":19930
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Image Image1 
         Height          =   1185
         Left            =   0
         MouseIcon       =   "FrmMDI.frx":1A60A
         MousePointer    =   99  'Custom
         Picture         =   "FrmMDI.frx":1A914
         Stretch         =   -1  'True
         ToolTipText     =   "Acessar a Intranet"
         Top             =   -60
         Width           =   1455
      End
   End
End
Attribute VB_Name = "FrmMDI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAdicionarTabelas_Click()
    frmCriarTabelas.Show
End Sub

Private Sub cmdAlterarTabelas_Click()
    frmAlterarTabela.Show
End Sub

Private Sub cmdExcluirTabelas_Click()
    frmExcluirTabelas.Show
End Sub

Private Sub cmdExecutaveis_Click()
    frmAplicacoes.Show
End Sub

Private Sub cmdLimpar_Click()
    For i = 0 To Db_DPK.TableDefs.Count - 1
        If InStr(UCase(Db_DPK.TableDefs(i).Name), "SYS") = 0 Then
            Db_DPK.Execute "Delete from " & Db_DPK.TableDefs(i).Name
        End If
    Next
End Sub

Private Sub cmdProcessos_Click()
    frmProcessos.Show
End Sub

Private Sub cmdSair_Click()
    Verificar_Tabelas
    Conn.Close
    Set Conn = Nothing
    Set Db_DPK = Nothing
    Set DB_Remoto = Nothing
    End
End Sub

Private Sub MDIForm_Load()
    Set Conn = New ADODB.Connection
    Conn.Open "Driver={Microsoft Access Driver (*.mdb)};" & _
              "Dbq=" & App.Path & "\FIL240_NOVO.MDB;" & _
              "Uid=Admin;" & _
              "Pwd=;"

    Set Conn_Alterar = New ADODB.Connection
    Conn_Alterar.Open "Driver={Microsoft Access Driver (*.mdb)};" & _
              "Dbq=F:\USR\jose_eduardo\PAAC\FIL240_Novo\BASE_ATU.MDB;" & _
              "Uid=Admin;Pwd=;"

    Set Db_DPK = OpenDatabase(App.Path & "\FIL240_NOVO.MDB")
    Set DB_Remoto = OpenDatabase("F:\USR\jose_eduardo\PAAC\FIL240_Novo\BASE_ATU.MDB")
End Sub

Function Posicionar_Botoes()
    Dim seq As Byte
    Dim controle As Control
    Dim Ordem(15, 1) As String
    
    For i = 0 To 14
        Ordem(i, 1) = 0
    Next
    For Each controle In Me.Controls
        If TypeOf controle Is cmd And controle.Tag <> "" Then
            Ordem(seq, 0) = controle.Name
            Ordem(seq, 1) = controle.Tag
            seq = seq + 1
        Else
            Ordem(seq, 1) = 0
        End If
    Next
    
    Dim X As Integer
    Dim Y As Integer
    Dim Aux_Nome As String
    Dim Aux_Seq As Integer
    Dim Posicao As Double
    ' Ordenar em Ordem Decrescente
    For X = 0 To 15 - 1 Step 1
        For Y = X + 1 To 14 Step 1
            If CDbl(Ordem(X, 1)) < CDbl(Ordem(Y, 1)) Then
                Aux_Nome = Ordem(Y, 0)
                Aux_Seq = Ordem(Y, 1)
                Ordem(Y, 0) = Ordem(X, 0)
                Ordem(Y, 1) = Ordem(X, 1)
                
                Ordem(X, 0) = Aux_Nome
                Ordem(X, 1) = Aux_Seq
            End If
        Next Y
    Next X
    
    Me.Visible = True
    Posicao = Me.Width - 100
    
    Dim Seq_Controle As Byte
    Seq_Controle = 1
    For i = 0 To 14
        If Ordem(i, 0) = "" Then Exit For
        For Each controle In Me.Controls
            If TypeOf controle Is cmd Then
                If controle.Name = Ordem(i, 0) Then
                   controle.Left = Posicao - controle.Width * Seq_Controle - 100
                   Seq_Controle = Seq_Controle + 1
                   Exit For
                End If
            End If
        Next
    Next
    
End Function

Private Sub MDIForm_Resize()
    Posicionar_Botoes
End Sub
