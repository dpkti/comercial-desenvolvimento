VERSION 5.00
Begin VB.Form frmFIL260 
   Caption         =   "Atualizações"
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   ScaleHeight     =   6105
   ScaleWidth      =   9735
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "frmFIL260"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'This example creates an index consisting of the fields Home Phone and Extension in the Employees table.

Sub CreateIndexX1()

   Dim dbs As Database

   ' Modify this line to include the path to Northwind
   ' on your computer.
   Set dbs = OpenDatabase("Northwind.mdb")

   ' Create the NewIndex index on the Employees table.
   dbs.Execute "CREATE INDEX NewIndex ON Employees " _
      & "(HomePhone, Extension);"

   dbs.Close

End Sub


'"ALTER TABLE Employees " _
      & "ADD COLUMN Salary CURRENCY;"

'This example creates a new table called MyTable with two Text fields, a Date/Time field, and a unique index made up of all three fields.

Sub CreateTableX2()

   Dim dbs As Database

   ' Modify this line to include the path to Northwind
   ' on your computer.
   Set dbs = OpenDatabase("Northwind.mdb")

   ' Create a table with three fields and a unique
   ' index made up of all three fields.
   dbs.Execute "CREATE TABLE MyTable " _
      & "(FirstName TEXT, LastName TEXT, " _
      & "DateOfBirth DATETIME, " _
      & "CONSTRAINT MyTableConstraint UNIQUE " _
      & "(FirstName, LastName, DateOfBirth));"

   dbs.Close

End Sub



Private Sub Command1_Click()

End Sub
