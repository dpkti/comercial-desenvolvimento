VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmAplicacoes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Nomes dos Arquivos"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4365
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4605
   ScaleWidth      =   4365
   Begin VB.Frame Frame1 
      Caption         =   "Tipo de Arquivo"
      Height          =   525
      Left            =   60
      TabIndex        =   7
      Top             =   1350
      Width           =   3825
      Begin VB.OptionButton optDBF 
         Caption         =   ".DBF"
         Height          =   195
         Left            =   3090
         TabIndex        =   11
         Top             =   240
         Width           =   705
      End
      Begin VB.OptionButton optTxt 
         Caption         =   ".TXT"
         Height          =   195
         Left            =   2090
         TabIndex        =   10
         Top             =   240
         Width           =   705
      End
      Begin VB.OptionButton optMDB 
         Caption         =   ".MDB"
         Height          =   195
         Left            =   1090
         TabIndex        =   9
         Top             =   240
         Width           =   705
      End
      Begin VB.OptionButton optExe 
         Caption         =   ".EXE"
         Height          =   195
         Left            =   90
         TabIndex        =   8
         Top             =   240
         Value           =   -1  'True
         Width           =   705
      End
   End
   Begin VB.TextBox txtNomeArquivo 
      Height          =   315
      Left            =   30
      TabIndex        =   3
      Top             =   990
      Width           =   4305
   End
   Begin VB.ListBox lstArquivos 
      Height          =   2205
      Left            =   60
      TabIndex        =   2
      Top             =   1920
      Width           =   4275
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   0
      Top             =   735
      Width           =   4305
      _ExtentX        =   7594
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAplicacoes.frx":0000
      PICN            =   "frmAplicacoes.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAdicionar 
      Height          =   390
      Left            =   3930
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   1470
      Width           =   420
      _ExtentX        =   741
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAplicacoes.frx":0CF6
      PICN            =   "frmAplicacoes.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluir 
      Height          =   390
      Left            =   3930
      TabIndex        =   6
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   4170
      Width           =   420
      _ExtentX        =   741
      _ExtentY        =   688
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmAplicacoes.frx":10D4
      PICN            =   "frmAplicacoes.frx":10F0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Nome do Arquivo:"
      Height          =   195
      Left            =   30
      TabIndex        =   4
      Top             =   810
      Width           =   1275
   End
End
Attribute VB_Name = "frmAplicacoes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Item_Selecionado  As String

Private Sub cmdAdicionar_Click()
    Dim Extensao As String
    If optExe.Value = True Then
        Extensao = "EXE"
    ElseIf optMDB.Value = True Then
        Extensao = "MDB"
    ElseIf optTxt.Value = True Then
        Extensao = "TXT"
    ElseIf optDBF.Value = True Then
        Extensao = "DBF"
    End If
    
    If Trim(txtNomeArquivo) = "" Then
        MsgBox "Digite o nome do Arquivo.", vbInformation, "Aten��o"
        txtNomeArquivo.SetFocus
        Exit Sub
    End If
    For i = 0 To lstArquivos.ListCount - 1
        If lstArquivos.List(i) = txtNomeArquivo Then
           MsgBox "Programa j� est� na lista.", vbInformation, "Aten��o"
           Exit Sub
        End If
    Next
    lstArquivos.AddItem UCase(txtNomeArquivo)
    Conn.Execute "Insert into Arquivos Values ('" & UCase(txtNomeArquivo) & "','" & Extensao & "')"
End Sub

Private Sub cmdExcluir_Click()
    Conn.Execute "Delete from Arquivos Where Nome_Arquivo='" & Item_Selecionado & "'"
    Carregar_Arquivos
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = ((FrmMDI.Height - Me.Height) / 2)
    Me.Left = (FrmMDI.Width - Me.Width) / 2

    Carregar_Arquivos

End Sub

Sub Carregar_Arquivos()
    lstArquivos.Clear
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Arquivos", Conn, adOpenKeyset, adLockOptimistic
    For i = 1 To rst.RecordCount
        lstArquivos.AddItem rst!Nome_Arquivo & "." & IIf(IsNull(rst!Tipo_Arquivo), "", rst!Tipo_Arquivo)
        rst.MoveNext
    Next
End Sub

Private Sub lstArquivos_Click()
    If InStr(1, lstArquivos, ".") > 0 Then
        Item_Selecionado = Mid(lstArquivos, 1, InStr(lstArquivos, ".") - 1)
    Else
        Item_Selecionado = lstArquivos
    End If
    
End Sub

Private Sub txtNomeArquivo_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub
