VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTabelas 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cria��o de Tabelas"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9390
   ControlBox      =   0   'False
   Icon            =   "frmTabelas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   424
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   626
   Begin Threed.SSFrame fraCriarTabela 
      Height          =   5025
      Left            =   60
      TabIndex        =   6
      Top             =   930
      Width           =   9285
      _Version        =   65536
      _ExtentX        =   16378
      _ExtentY        =   8864
      _StockProps     =   14
      Caption         =   "Criar Tabela"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ComboBox cmb 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "frmTabelas.frx":23D2
         Left            =   4290
         List            =   "frmTabelas.frx":23F7
         Style           =   2  'Dropdown List
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   300
         Visible         =   0   'False
         Width           =   2265
      End
      Begin MSFlexGridLib.MSFlexGrid FlxCampos 
         Height          =   2355
         Left            =   60
         TabIndex        =   7
         Top             =   270
         Width           =   9135
         _ExtentX        =   16113
         _ExtentY        =   4154
         _Version        =   393216
         Rows            =   256
         Cols            =   4
         RowHeightMin    =   330
         HighLight       =   2
         GridLinesFixed  =   1
         AllowUserResizing=   3
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSFlexGridLib.MSFlexGrid flxPropriedades 
         Height          =   2355
         Left            =   60
         TabIndex        =   9
         Top             =   2670
         Width           =   4485
         _ExtentX        =   7911
         _ExtentY        =   4154
         _Version        =   393216
         Rows            =   10
         FixedRows       =   0
         RowHeightMin    =   330
         HighLight       =   2
         GridLinesFixed  =   1
         AllowUserResizing=   3
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6030
      Width           =   9390
      _ExtentX        =   16563
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   16510
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   11805
      _ExtentX        =   20823
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTabelas.frx":2464
      PICN            =   "frmTabelas.frx":2480
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExcluirTabela 
      Height          =   690
      Left            =   8640
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Excluir Tabela"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTabelas.frx":315A
      PICN            =   "frmTabelas.frx":3176
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdAlterarTabela 
      Height          =   690
      Left            =   7920
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Alterar Tabela"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTabelas.frx":3E50
      PICN            =   "frmTabelas.frx":3E6C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdCriarTabela 
      Height          =   690
      Left            =   7200
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Criar Tabela"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTabelas.frx":4B46
      PICN            =   "frmTabelas.frx":4B62
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmTabelas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub cmb_Click()
    FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col) = cmb
End Sub

Private Sub cmdCriarTabela_Click()
    fraCriarTabela.Visible = True
    FormatarFlx
    
    'fraAlterarTabela.Visible = False
    'fraExcluirTabela.Visible = False
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub FlxCampos_EnterCell()
    FlxCampos.TextMatrix(FlxCampos.Row, 0) = ">"
    If FlxCampos.Col = 2 Then
        cmb.Top = FlxCampos.CellTop + 270
        cmb.Left = FlxCampos.CellLeft + 60
        cmb.Width = FlxCampos.CellWidth
        cmb.Visible = True
    Else
        cmb.Visible = False
    End If
End Sub

Private Sub FlxCampos_KeyPress(KeyAscii As Integer)
    If FlxCampos.Col <> 2 Then
        If KeyAscii <> 13 Then
            If KeyAscii = 8 Then
                If Len(FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col)) = 0 Then
                    Exit Sub
                Else
                    FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col) = Left(FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col), Len(FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col)) - 1)
                End If
            Else
                FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col) = FlxCampos.TextMatrix(FlxCampos.Row, FlxCampos.Col) + Chr(KeyAscii)
            End If
        Else
            SendKeys "{TAB}"
        End If
    End If
End Sub

Private Sub FlxCampos_LeaveCell()
    FlxCampos.TextMatrix(FlxCampos.Row, 0) = ""
End Sub

Private Sub FlxCampos_Scroll()
    cmb.Visible = False
End Sub

Private Sub Form_Load()
    Me.Top = 0
    Me.Left = 0
End Sub

Sub FormatarFlx()
    
    FlxCampos.ColWidth(0) = 200
    FlxCampos.ColWidth(1) = 2500
    FlxCampos.ColWidth(2) = 2500
    FlxCampos.ColWidth(3) = 3500
    
    FlxCampos.TextMatrix(0, 1) = "Campos"
    FlxCampos.TextMatrix(0, 2) = "Tipo de Dados"
    FlxCampos.TextMatrix(0, 3) = "Descri��o"
    
    'Propriedades
    flxPropriedades.TextMatrix(0, 0) = "Tamanho"
    flxPropriedades.TextMatrix(0, 1) = "Permitir NULL"
    
End Sub

