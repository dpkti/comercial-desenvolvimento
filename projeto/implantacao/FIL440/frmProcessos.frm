VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmProcessos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Processos que ser�o executados"
   ClientHeight    =   5940
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7185
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   7185
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   3510
      Top             =   90
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   14
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProcessos.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmProcessos.frx":03C2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView trvProcessos 
      Height          =   5055
      Left            =   30
      TabIndex        =   0
      Top             =   840
      Width           =   7125
      _ExtentX        =   12568
      _ExtentY        =   8916
      _Version        =   393217
      Indentation     =   707
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   765
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmProcessos.frx":0784
      PICN            =   "frmProcessos.frx":07A0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExpandir 
      Height          =   690
      Left            =   6510
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmProcessos.frx":147A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmProcessos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim N� As Node

Private Sub cmdExpandir_Click()
    For i = 1 To trvProcessos.Nodes.Count
        trvProcessos.Nodes(i).Expanded = Not (trvProcessos.Nodes(i).Expanded)
    Next
    If trvProcessos.Nodes(i - 1).Expanded = True Then
       Set cmdExpandir.PictureNormal = ImageList1.ListImages(2).Picture
    Else
       Set cmdExpandir.PictureNormal = Me.ImageList1.ListImages(1).Picture
    End If
End Sub
    
Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Me.Top = FrmMDI.SSPanel1.Height + (FrmMDI.SSPanel1.Height / 2)
    Me.Left = (FrmMDI.Width - Me.Width) / 2
    
    Montar_Trv "Campos_Criar", "Criar Tabelas", "Criar_Tabelas"
        Tabelas "Criar_Tabelas", "Select A.Cod_Tabela, A.Nome_tabela from Tabelas A, Campos_criar B Where A.Cod_tabela = B.Cod_Tabela"
            
    Montar_Trv "Tabelas where excluir=1", "Excluir Tabelas", "Excluir_Tabelas"
        Tabelas "Excluir_Tabelas", "Select Cod_Tabela, Nome_tabela from Tabelas Where Excluir=1"
            
    Montar_Trv "Campos_Adicionar", "Adicionar Campos", "Adicionar_Campos"
        Tabelas "Adicionar_Campos", "Select A.Cod_tabela, A.Nome_tabela from Tabelas A, Campos_Adicionar B Where A.Cod_tabela = B.Cod_Tabela"
            
    Montar_Trv "Campos_Excluir", "Excluir Campos", "Excluir_Campos"
        Tabelas "Excluir_Campos", "Select A.cod_tabela, A.Nome_tabela from Tabelas A, Campos_Excluir B Where A.Cod_tabela = B.Cod_Tabela"
            
    Montar_Trv "Campos_indices", "Adicionar Indices", "Adicionar_Indices"
        Tabelas "Adicionar_Indices", "Select A.cod_tabela, A.Nome_tabela from Tabelas A, Indices B Where A.Cod_tabela = B.Cod_Tabela"
    
    Montar_Trv "Indices_Excluir", "Excluir Indices", "Excluir_Indices"
        Tabelas "Excluir_Indices", "Select A.cod_tabela, A.Nome_tabela from Tabelas A, Indices_excluir B Where A.Cod_tabela = B.Cod_Tabela"
    
    Set cmdExpandir.PictureNormal = Me.ImageList1.ListImages(1).Picture
    
End Sub

Sub Criar_N�(Texto As String, Chave As String)
    Set N� = trvProcessos.Nodes.Add
    N�.Text = Texto
    N�.Key = Chave
End Sub

Sub Montar_Trv(Tabela As String, Nome_N� As String, Chave_N� As String)
    Set rst = New ADODB.Recordset
    rst.Open "Select * from " & Tabela, Conn, adOpenKeyset, adLockOptimistic
    If rst.RecordCount > 0 Then Criar_N� Nome_N�, Chave_N�
End Sub

Sub Tabelas(Nome_pai As String, pSelect As String)
On Error GoTo Erro
    Dim rst_tab As ADODB.Recordset
    Set rst_tab = New ADODB.Recordset
    rst_tab.Open pSelect, Conn, adOpenKeyset, adLockOptimistic
    
    'Exibir os campos que ser�o criados em cada tabela
    For i = 1 To rst_tab.RecordCount
        trvProcessos.Nodes.Add Trim(Nome_pai), tvwChild, Trim(Nome_pai) & "_" & rst_tab!Nome_tabela & i, "Tabela: " & rst_tab!Nome_tabela
        Filhos Trim(Nome_pai), Trim(Nome_pai) & "_" & rst_tab!Nome_tabela & i, rst_tab!Cod_tabela
        rst_tab.MoveNext
    Next
Erro:
    If Err.Number = 35601 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & _
               "Descri��o: " & Err.Description
    End If
End Sub

Sub Filhos(Nome_processo As String, Nome_pai As String, Cod_tabela As Double)
    Dim iii As Integer
    Dim rst_filho As ADODB.Recordset
    Dim rst_filho_ind As ADODB.Recordset
    
    Set rst_filho = New ADODB.Recordset
    Set rst_filho_ind = New ADODB.Recordset
    
    Select Case Nome_processo
        Case "Criar_Tabelas"
            rst_filho.Open "Select * from Campos_criar where cod_tabela=" & Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        
        Case "Adicionar_Campos"
            rst_filho.Open "Select * from Campos_Adicionar where cod_tabela=" & Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        
        Case "Excluir_Campos"
            rst_filho.Open "Select * from Campos_Excluir where cod_tabela=" & Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
                
        Case "Adicionar_Indices"
            rst_filho.Open "Select * from Indices where cod_tabela=" & Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
            For ii = 1 To rst_filho.RecordCount
                trvProcessos.Nodes.Add Nome_pai, tvwChild, Nome_processo & "_" & rst_filho!Nome_Indice & ii, rst_filho!Nome_Indice
                    rst_filho_ind.Open "Select Nome_campo from Campos_indices Where Cod_indice=" & rst_filho!Cod_indice, Conn, adOpenKeyset, adLockOptimistic
                    For iii = 1 To rst_filho_ind.RecordCount
                        trvProcessos.Nodes.Add Nome_processo & "_" & rst_filho!Nome_Indice & ii, tvwChild, rst_filho_ind!Nome_Campo & iii, rst_filho_ind!Nome_Campo
                        rst_filho_ind.MoveNext
                    Next iii
                rst_filho.MoveNext
            Next
        
        Case "Excluir_Indices"
            rst_filho.Open "Select * from Indices_excluir where cod_tabela=" & Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
            For ii = 1 To rst_filho.RecordCount
                trvProcessos.Nodes.Add Nome_pai, tvwChild, Nome_processo & "_" & rst_filho!Nome_Indice & ii, rst_filho!Nome_Indice
                rst_filho.MoveNext
            Next
                
        Case Else
            Set rst_filho = Nothing
    End Select
    
    If Not rst_filho Is Nothing And Nome_processo <> "Adicionar_Indices" And Nome_processo <> "Excluir_Indices" Then
        For ii = 1 To rst_filho.RecordCount
            trvProcessos.Nodes.Add Nome_pai, tvwChild, Nome_processo & "_" & rst_filho!Nome_Campo & ii, rst_filho!Nome_Campo
            rst_filho.MoveNext
        Next
    End If
End Sub
