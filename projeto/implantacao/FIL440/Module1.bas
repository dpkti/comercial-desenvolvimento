Attribute VB_Name = "Module1"
Option Explicit

Public Conn As ADODB.Connection
Public Conn_Alterar As ADODB.Connection
Public Db_DPK As Database
Public DB_Remoto As Database
Public db As Database

Public rst As ADODB.Recordset
Public tb As TableDef

Public i As Integer
Public ii As Integer

Public Type TipoDadosCampo
    Nome As String
    Codigo As String
End Type

Public CampoTipo(0 To 23) As TipoDadosCampo

Public Sub Informacao_TipoDadosCampo()
    CampoTipo(DataTypeEnum.dbBigInt).Codigo = "dbBigInt"
    CampoTipo(DataTypeEnum.dbBigInt).Nome = "Big Integer"
    CampoTipo(DataTypeEnum.dbBinary).Codigo = "dbBinary"
    CampoTipo(DataTypeEnum.dbBinary).Nome = "Binary"
    CampoTipo(DataTypeEnum.dbBoolean).Codigo = "dbBoolean"
    CampoTipo(DataTypeEnum.dbBoolean).Nome = "Boolean (True/False)"
    CampoTipo(DataTypeEnum.dbByte).Codigo = "dbByte"
    CampoTipo(DataTypeEnum.dbByte).Nome = "Byte"
    CampoTipo(DataTypeEnum.dbChar).Codigo = "dbChar"
    CampoTipo(DataTypeEnum.dbChar).Nome = "Fixed String"
    CampoTipo(DataTypeEnum.dbCurrency).Codigo = "dbCurrency"
    CampoTipo(DataTypeEnum.dbCurrency).Nome = "Currency"
    CampoTipo(DataTypeEnum.dbDate).Codigo = "dbDate"
    CampoTipo(DataTypeEnum.dbDate).Nome = "Date"
    CampoTipo(DataTypeEnum.dbDecimal).Codigo = "dbDecimal"
    CampoTipo(DataTypeEnum.dbDecimal).Nome = "Decimal"
    CampoTipo(DataTypeEnum.dbDouble).Codigo = "dbDouble"
    CampoTipo(DataTypeEnum.dbDouble).Nome = "Double"
    CampoTipo(DataTypeEnum.dbFloat).Codigo = "dbFloat"
    CampoTipo(DataTypeEnum.dbFloat).Nome = "Float"
    CampoTipo(DataTypeEnum.dbGUID).Codigo = "dbGUID"
    CampoTipo(DataTypeEnum.dbGUID).Nome = "GUID (Globally Unique Identifier)"
    CampoTipo(DataTypeEnum.dbInteger).Codigo = "dbInteger"
    CampoTipo(DataTypeEnum.dbInteger).Nome = "Integer"
    CampoTipo(DataTypeEnum.dbLong).Codigo = "dbLong"
    CampoTipo(DataTypeEnum.dbLong).Nome = "Long"
    CampoTipo(DataTypeEnum.dbLongBinary).Codigo = "dbLongBinary"
    CampoTipo(DataTypeEnum.dbLongBinary).Nome = "Long Binary"
    CampoTipo(DataTypeEnum.dbMemo).Codigo = "dbMemo"
    CampoTipo(DataTypeEnum.dbMemo).Nome = "Memo"
    CampoTipo(DataTypeEnum.dbNumeric).Codigo = "dbNumeric"
    CampoTipo(DataTypeEnum.dbNumeric).Nome = "Numeric"
    CampoTipo(DataTypeEnum.dbSingle).Codigo = "dbSingle"
    CampoTipo(DataTypeEnum.dbSingle).Nome = "Single"
    CampoTipo(DataTypeEnum.dbText).Codigo = "dbText"
    CampoTipo(DataTypeEnum.dbText).Nome = "Text"
    CampoTipo(DataTypeEnum.dbTime).Codigo = "dbTime"
    CampoTipo(DataTypeEnum.dbTime).Nome = "Time"
    CampoTipo(DataTypeEnum.dbTimeStamp).Codigo = "dbTimeStamp"
    CampoTipo(DataTypeEnum.dbTimeStamp).Nome = "Time Stamp"
    CampoTipo(DataTypeEnum.dbVarBinary).Codigo = "dbVarBinary"
    CampoTipo(DataTypeEnum.dbVarBinary).Nome = "Variable length Binary"
End Sub

'Esta rotina tem como finalidade excluir as tabelas que n�o est�o marcadas para Excluir e
'tamb�m n�o possuem registros em nenhuma outra tabela, s�o tabelas que foram inseridas na
'tabela TABELAS e devem ser excluidas
Public Sub Verificar_Tabelas()
    Dim rst_tabelas As ADODB.Recordset
    
    Dim Qtd As Byte
    
    Set rst_tabelas = New ADODB.Recordset
    rst_tabelas.Open "Select * from tabelas", Conn, adOpenKeyset, adLockOptimistic
    
    For i = 1 To rst_tabelas.RecordCount
        
        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Campos_Adicionar Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)
        
        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Campos_Alterar Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)
        
        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Campos_Criar Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)

        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Campos_Excluir Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)

        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Indices Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)

        Set rst = New ADODB.Recordset
        rst.Open "Select count(*) from Indices_Excluir Where Cod_tabela = " & rst_tabelas!Cod_tabela, Conn, adOpenKeyset, adLockOptimistic
        Qtd = Qtd + rst.Fields(0)

        If Qtd = 0 Then
            Conn.Execute "Delete from Tabelas where nome_tabela='" & rst_tabelas!Nome_tabela & "'"
        Else
            Qtd = 0
        End If
        
        rst_tabelas.MoveNext
    Next
    
End Sub

Public Function Get_Cod_Tabela(NomeTabela As String) As String
    Set rst = New ADODB.Recordset
    rst.Open "Select Cod_Tabela from Tabelas Where Nome_Tabela ='" & UCase(NomeTabela) & "'", Conn, adOpenKeyset, adLockOptimistic
    If rst.EOF = True Then
       Conn.Execute "Insert into tabelas (Nome_tabela, Excluir) Values ('" & NomeTabela & "', 0)"
       Set rst = New ADODB.Recordset
       rst.Open "Select Cod_Tabela from Tabelas Where Nome_Tabela ='" & UCase(NomeTabela) & "'", Conn, adOpenKeyset, adLockOptimistic
    End If
    
    Get_Cod_Tabela = rst!Cod_tabela
End Function


