VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmExcluirTabelas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Excluir Tabelas"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3945
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   3945
   Begin MSGrid.Grid FlxTabelas 
      Height          =   4335
      Left            =   30
      TabIndex        =   1
      Top             =   1050
      Width           =   3885
      _Version        =   65536
      _ExtentX        =   6853
      _ExtentY        =   7646
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   30
      TabIndex        =   2
      Top             =   765
      Width           =   3885
      _ExtentX        =   6853
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   30
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExcluirTabelas.frx":0000
      PICN            =   "frmExcluirTabelas.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdMarcar 
      Height          =   690
      Left            =   2430
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Marcar Todas as Tabelas"
      Top             =   30
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExcluirTabelas.frx":0CF6
      PICN            =   "frmExcluirTabelas.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDesmarcarTabelas 
      Height          =   690
      Left            =   3210
      TabIndex        =   5
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   30
      Width           =   720
      _ExtentX        =   1270
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmExcluirTabelas.frx":8214
      PICN            =   "frmExcluirTabelas.frx":8230
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   270
      Left            =   900
      Picture         =   "frmExcluirTabelas.frx":838A
      Stretch         =   -1  'True
      Top             =   60
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Lista de Tabelas Existentes no Banco"
      Height          =   195
      Left            =   30
      TabIndex        =   0
      Top             =   840
      Width           =   2670
   End
End
Attribute VB_Name = "frmExcluirTabelas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDesmarcarTabelas_Click()
    For i = 1 To FlxTabelas.Rows - 1
        FlxTabelas.Row = i
        FlxTabelas.Col = 1
        Set FlxTabelas.Picture = Nothing
        
        Set rst = New ADODB.Recordset
        rst.Open "Select * from Tabelas Where Nome_tabela='" & FlxTabelas.Text & "' AND Excluir=0", Conn, adOpenKeyset, adLockOptimistic
        If rst.RecordCount > 0 Then
            Conn.Execute "Delete from Tabelas where Nome_Tabela='" & UCase(FlxTabelas.Text) & "' AND Excluir=1"
        End If
    Next
End Sub

Private Sub cmdMarcar_Click()
    For i = 1 To FlxTabelas.Rows - 1
        FlxTabelas.Row = i
        FlxTabelas.Col = 1
        Set FlxTabelas.Picture = Image1
        Set rst = New ADODB.Recordset
        rst.Open "Select * from Tabelas Where Nome_tabela='" & FlxTabelas.Text & "'", Conn, adOpenKeyset, adLockOptimistic
        If rst.RecordCount = 0 Then
           Conn.Execute "Insert into Tabelas (Nome_tabela, Excluir) Values ('" & FlxTabelas.Text & "',1)"
        Else
           Conn.Execute "Update Tabelas Set Excluir=1 where Nome_Tabela='" & UCase(FlxTabelas.Text) & "' AND Excluir=0"
        End If
    Next
End Sub

Private Sub cmdVoltar_Click()
    Unload Me
End Sub

Private Sub FlxTabelas_DblClick()
    Dim Regs As Integer
    
    If FlxTabelas.Col = 1 Then
       FlxTabelas.Row = FlxTabelas.Row
       FlxTabelas.Col = 1
       If FlxTabelas.Picture = 0 Then
          FlxTabelas.ColAlignment(1) = 2
          Set FlxTabelas.Picture = Image1
          FlxTabelas.Col = 0
          Set rst = New ADODB.Recordset
          rst.Open "Select * from Tabelas Where Nome_tabela='" & FlxTabelas.Text & "'", Conn, adOpenKeyset, adLockOptimistic
          If rst.RecordCount = 0 Then
             Conn.Execute "Insert into Tabelas (Nome_tabela, Excluir) Values ('" & FlxTabelas.Text & "', 1)"
          Else
             Conn.Execute "Update Tabelas Set Excluir=1 where Nome_Tabela='" & UCase(FlxTabelas.Text) & "' AND Excluir=0"
          End If
       Else
          Set FlxTabelas.Picture = Nothing
          FlxTabelas.Col = 0
          Conn.Execute "Update Tabelas Set excluir = 0 where Nome_Tabela='" & UCase(FlxTabelas.Text) & "' AND Excluir=1"
       End If
    End If
End Sub

Private Sub Form_Load()
    Me.Top = (FrmMDI.Height - (Me.Height + FrmMDI.SSPanel1.Height + 400)) / 2
    Me.Left = (FrmMDI.Width - Me.Width) / 2
    
    FlxTabelas.ColWidth(0) = 3000
    FlxTabelas.ColWidth(1) = 500
    FlxTabelas.ColAlignment(1) = 2
    
    FlxTabelas.Rows = DB_Remoto.TableDefs.Count + 1
    
    FlxTabelas.Row = 0
    FlxTabelas.Col = 0
    FlxTabelas.Text = "Nome Tabela"
    FlxTabelas.Col = 1
    FlxTabelas.Text = "Excluir"
    
    For i = 0 To DB_Remoto.TableDefs.Count - 1
        FlxTabelas.Row = i + 1
        FlxTabelas.Col = 0
        FlxTabelas = DB_Remoto.TableDefs(i).Name
    Next
    
    Set rst = New ADODB.Recordset
    rst.Open "Select * from tabelas Where Excluir = 1", Conn, adOpenKeyset, adLockOptimistic
    For i = 1 To rst.RecordCount
        For ii = 0 To FlxTabelas.Rows - 1
            FlxTabelas.Col = 0
            FlxTabelas.Row = ii
            If UCase(FlxTabelas.Text) = UCase(rst!Nome_tabela) Then
               FlxTabelas.Col = 1
               FlxTabelas.Row = ii
               Set FlxTabelas.Picture = Image1.Picture
            End If
        Next
        rst.MoveNext
    Next

End Sub
