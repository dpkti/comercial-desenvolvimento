VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmListaTabelas 
   Caption         =   "Form1"
   ClientHeight    =   6105
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6210
   LinkTopic       =   "Form1"
   ScaleHeight     =   6105
   ScaleWidth      =   6210
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   795
      Left            =   3570
      TabIndex        =   4
      Top             =   120
      Width           =   1605
   End
   Begin VB.TextBox txtCaminhoMDB 
      Height          =   285
      Left            =   1170
      TabIndex        =   2
      Top             =   5670
      Width           =   4485
   End
   Begin VB.CommandButton cmdDirMDB 
      Caption         =   "..."
      Height          =   285
      Left            =   5700
      TabIndex        =   1
      Top             =   5670
      Width           =   375
   End
   Begin MSComctlLib.TreeView TVTab 
      Height          =   5565
      Left            =   60
      TabIndex        =   0
      Top             =   30
      Width           =   3315
      _ExtentX        =   5847
      _ExtentY        =   9816
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Sorted          =   -1  'True
      Style           =   7
      SingleSel       =   -1  'True
      BorderStyle     =   1
      Appearance      =   0
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Caminho MDB:"
      Height          =   195
      Left            =   60
      TabIndex        =   3
      Top             =   5730
      Width           =   1065
   End
End
Attribute VB_Name = "frmListaTabelas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Janela de Pastas - Inicio
    Private Type BROWSEINFO
      hOwner          As Long
      pidlRoot        As Long
      pszDisplayName  As String
      lpszTitle       As String
      ulFlags         As Long
      lpfn            As Long
      lParam          As Long
      iImage          As Long
    End Type
        
    Private Declare Function SHGetPathFromIDList Lib _
       "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, _
        ByVal pszPath As String) As Long
    
    Private Declare Function SHBrowseForFolder Lib _
       "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long
'Janela de pastas - Fim

Dim fName As String
Dim fPath As String
Dim bi As BROWSEINFO
Dim tmpPath As String

Dim db As Database
Dim tb As TableDef

Private Sub cmdDirMDB_Click()
    txtCaminhoMDB = vbGetBrowseDirectory
End Sub

Private Sub Command1_Click()
    Set db = OpenDatabase(txtCaminhoMDB & "BASE_ATU.MDB")
    Montar_Treeview
End Sub

'Abrir tela de Pastas
Public Function vbGetBrowseDirectory() As String

    pidl = SHBrowseForFolder(bi)
    
    tmpPath = Space$(512)
    R = SHGetPathFromIDList(ByVal pidl, ByVal tmpPath)
      
    If R Then
          pos = InStr(tmpPath, Chr$(0))
          tmpPath = Left(tmpPath, pos - 1)
          vbGetBrowseDirectory = ValidateDir(tmpPath)
    Else: vbGetBrowseDirectory = ""
    End If

End Function

Private Function ValidateDir(tmpPath As String) As String

    If Right$(tmpPath, 1) = "\" Then
          ValidateDir = tmpPath
    Else: ValidateDir = tmpPath & "\"
    End If

End Function

Sub Montar_Treeview()
On Error GoTo erro
    Dim No As Node
    Dim i As Integer
    Dim ii As Integer
    Dim Campo As Fields
    Dim valor As String
    
    For i = 0 To db.TableDefs.Count - 1
        Set tb = db.TableDefs(i)
        Set No = TVTab.Nodes.Add(, , db.TableDefs(i).Name & "_" & i, db.TableDefs(i).Name)
        For ii = 0 To tb.Fields.Count - 1
            Set No = TVTab.Nodes.Add(db.TableDefs(i).Name & "_" & i, tvwChild, db.TableDefs(i).Name & "_" & tb.Fields(ii).Name, tb.Fields(ii).Name)
            For iii = 0 To tb.Fields(ii).Properties.Count - 1
                valor = tb.Fields(ii).Properties(iii).Value
                TVTab.Nodes.Add db.TableDefs(i).Name & "_" & tb.Fields(ii).Name, tvwChild, db.TableDefs(i).Name & "_" & tb.Fields(ii).Name & "_" & iii, tb.Fields(ii).Properties(iii).Name & " = " & valor
            Next iii
        Next ii
    Next
erro:
    If Err.Number = 3219 Or Err.Number = 3267 Or Err.Number = 3251 Then
        Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If
End Sub

