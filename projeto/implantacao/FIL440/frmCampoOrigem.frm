VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmCampoOrigem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Campo de Origem dos dados"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3300
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   3300
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstCampoOrigem 
      Height          =   2400
      Left            =   0
      TabIndex        =   0
      Top             =   780
      Width           =   3255
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   0
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   0
      Width           =   660
      _ExtentX        =   1164
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmCampoOrigem.frx":0000
      PICN            =   "frmCampoOrigem.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmCampoOrigem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
    
    lstCampoOrigem.AddItem "<Nenhum>"
    
    For i = 0 To frmAlterarTabela.cboTabelas.ListCount - 1
        If UCase(db_Remoto.TableDefs(i).Name) = frmAlterarTabela.cboTabelas Then
           For ii = 0 To db_Remoto.TableDefs(i).Fields.Count - 1
               lstCampoOrigem.AddItem db_Remoto.TableDefs(i).Fields(ii).Name
           Next
        End If
    Next
End Sub

Private Sub lstCampoOrigem_Click()
    If lstCampoOrigem = "<Nenhum>" Then
        frmAlterarTabela.lsvCamposAdicionar.SelectedItem.SubItems(5) = ""
    Else
        frmAlterarTabela.lsvCamposAdicionar.SelectedItem.SubItems(5) = lstCampoOrigem
    End If
    
    Unload Me
End Sub
