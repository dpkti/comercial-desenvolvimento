VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCriarTabelas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Criar Tabelas"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8595
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5460
   ScaleWidth      =   8595
   Begin VB.Frame Frame2 
      Caption         =   "Campos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2325
      Left            =   0
      TabIndex        =   11
      Top             =   540
      Width           =   8565
      Begin VB.CheckBox chkPermNull 
         Caption         =   "Permitir Null"
         Height          =   255
         Left            =   60
         TabIndex        =   23
         Top             =   1620
         Value           =   1  'Checked
         Width           =   1605
      End
      Begin VB.TextBox txtNomeCampo 
         Height          =   315
         Left            =   60
         TabIndex        =   16
         Top             =   420
         Width           =   2475
      End
      Begin VB.ComboBox cboTipoDados 
         Height          =   315
         ItemData        =   "Form2.frx":0000
         Left            =   60
         List            =   "Form2.frx":0022
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   990
         Width           =   1545
      End
      Begin VB.TextBox txtTamanho 
         Height          =   315
         Left            =   1650
         TabIndex        =   14
         Top             =   990
         Width           =   705
      End
      Begin VB.CheckBox chkAutoNum 
         Caption         =   "Auto Numera��o"
         Height          =   255
         Left            =   60
         TabIndex        =   13
         Top             =   1380
         Width           =   1605
      End
      Begin Bot�o.cmd cmdAdicionar 
         Height          =   315
         Left            =   60
         TabIndex        =   12
         Top             =   1950
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "Adicionar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form2.frx":0070
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin MSComctlLib.ListView lsvCampos 
         Height          =   1875
         Left            =   2580
         TabIndex        =   17
         Top             =   390
         Width           =   5925
         _ExtentX        =   10451
         _ExtentY        =   3307
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome Campo"
            Object.Width           =   2117
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Tipo Dados"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Tamanho"
            Object.Width           =   1587
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Auto Numera��o"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Perm Null"
            Object.Width           =   1764
         EndProperty
      End
      Begin Bot�o.cmd cmdExcluir 
         Height          =   315
         Left            =   960
         TabIndex        =   18
         Top             =   1950
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form2.frx":008C
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Lista de Campos"
         Height          =   195
         Left            =   2580
         TabIndex        =   22
         Top             =   210
         Width           =   1170
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Campo"
         Height          =   195
         Left            =   60
         TabIndex        =   21
         Top             =   210
         Width           =   1185
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Tipo de Dados"
         Height          =   195
         Left            =   60
         TabIndex        =   20
         Top             =   780
         Width           =   1050
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Tamanho"
         Height          =   195
         Left            =   1650
         TabIndex        =   19
         Top             =   780
         Width           =   675
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "�ndices"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2475
      Left            =   30
      TabIndex        =   2
      Top             =   2940
      Width           =   7365
      Begin VB.CheckBox chkPrimary 
         Caption         =   "Chave Prim�ria"
         Height          =   195
         Left            =   5880
         TabIndex        =   28
         Top             =   330
         Width           =   1425
      End
      Begin VB.CheckBox chkUnique 
         Caption         =   "Registro Unico"
         Height          =   195
         Left            =   5880
         TabIndex        =   27
         Top             =   540
         Width           =   1425
      End
      Begin VB.CheckBox chkIgnoreNulls 
         Caption         =   "Ignorar Null"
         Height          =   195
         Left            =   5880
         TabIndex        =   26
         Top             =   750
         Width           =   1425
      End
      Begin VB.ListBox lstCamposIndices 
         Height          =   1410
         Left            =   60
         Style           =   1  'Checkbox
         TabIndex        =   6
         Top             =   990
         Width           =   1845
      End
      Begin VB.TextBox txtNomeIndice 
         Height          =   315
         Left            =   60
         TabIndex        =   3
         Top             =   420
         Width           =   1875
      End
      Begin MSComctlLib.ListView lsvIndices 
         Height          =   2085
         Left            =   1980
         TabIndex        =   7
         Top             =   330
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   3678
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nome do Indice"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Campos do Indice"
            Object.Width           =   2646
         EndProperty
      End
      Begin Bot�o.cmd cmdSalvarIndice 
         Height          =   345
         Left            =   5880
         TabIndex        =   9
         Top             =   1650
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Salvar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form2.frx":00A8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdExcluirIndice 
         Height          =   345
         Left            =   5880
         TabIndex        =   10
         Top             =   2040
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   609
         BTYPE           =   3
         TX              =   "Excluir"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   -1  'True
         BCOL            =   13160660
         BCOLO           =   13160660
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "Form2.frx":00C4
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   0
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         Caption         =   "�ndices Existentes"
         Height          =   195
         Left            =   1980
         TabIndex        =   8
         Top             =   150
         Width           =   1275
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         Caption         =   "Campos Dispon�veis"
         Height          =   195
         Left            =   60
         TabIndex        =   5
         Top             =   780
         Width           =   1455
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Nome do Indice"
         Height          =   195
         Left            =   60
         TabIndex        =   4
         Top             =   210
         Width           =   1125
      End
   End
   Begin VB.TextBox txtNomeTabela 
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   210
      Width           =   2505
   End
   Begin Bot�o.cmd cmdExcluirTabela 
      Height          =   405
      Left            =   7320
      TabIndex        =   24
      Top             =   30
      Width           =   1245
      _ExtentX        =   2196
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Excluir Tabela"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form2.frx":00E0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvarTabela 
      Height          =   405
      Left            =   6030
      TabIndex        =   25
      Top             =   30
      Width           =   1245
      _ExtentX        =   2196
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "Salvar Tabela"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "Form2.frx":00FC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Nome Tabela"
      Height          =   195
      Left            =   0
      TabIndex        =   0
      Top             =   30
      Width           =   960
   End
End
Attribute VB_Name = "frmCriarTabelas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim litem As ListItem
Dim vCod_Tabela As Long
Dim vCod_Indice As Long

Private Sub cboTipoDados_Click()
    If InStr(1, UCase(cboTipoDados), "TEXT") > 0 Then
       txtTamanho = ""
       txtTamanho.Enabled = True
    Else
       txtTamanho = ""
       txtTamanho.Enabled = False
    End If
End Sub

Private Sub cmdAdicionar_Click()
    Dim Regs As Integer
    
    If txtNomeTabela = "" Then
        MsgBox "Digite um nome para a tabela.", vbInformation, "Aten��o"
        txtNomeTabela.SetFocus
        Exit Sub
    End If
        
    If IsNumeric(Left(txtNomeCampo, 1)) Or InStr(1, ",/.?:;-�&*()#@!<>", Left(txtNomeCampo, 1)) > 0 Then
        MsgBox "Nome inv�lido para o campo.", vbInformation, "Aten��o"
        txtNomeCampo.SetFocus
        Exit Sub
    End If
        
    If txtNomeCampo = "" Then
        MsgBox "Digite o nome do campo", vbInformation, "Aten��o"
        txtNomeCampo.SetFocus
        Exit Sub
    End If
    
    If cboTipoDados = "" Then
        MsgBox "Escolha um tipo de dados", vbInformation, "Aten��o"
        cboTipoDados.SetFocus
        Exit Sub
    End If
    
    If txtTamanho.Enabled = True Then
       If txtTamanho = "" Then
          MsgBox "Digite o tamanho do campo", vbInformation, "Aten��o"
          txtTamanho.SetFocus
          Exit Sub
       End If
    End If
       
    Conn.Execute "Update Campos_Criar Set " & _
                 "Tipo_Dados='" & cboTipoDados & "'," & _
                 "Tamanho_Campo=" & IIf(txtTamanho = "", 0, txtTamanho) & "," & _
                 "AutoNumeracao=" & chkAutoNum.Value & "," & _
                 "PermNull=" & chkPermNull.Value & _
                 " WHERE COD_TABELA = " & vCod_Tabela & _
                 " AND Nome_Campo = '" & txtNomeCampo & "'", Regs
                 
    If Regs <= 0 Then
        Conn.Execute "Insert into Campos_Criar (Cod_Tabela, Nome_Campo, Tipo_Dados, Tamanho_Campo, AutoNumeracao, PermNull) Values (" & _
                     vCod_Tabela & ",'" & txtNomeCampo & "','" & cboTipoDados & "'," & IIf(txtTamanho = "", 0, txtTamanho) & "," & chkAutoNum.Value & "," & chkPermNull & ")"
    End If
    
    Limpar_campo
    
    Preencher_Lsv_Campos
    
End Sub

Private Sub cmdExcluir_Click()
    If MsgBox("Confirma Exclus�o do campo selecionado ?", vbInformation + vbYesNo, "Aten��o") = vbYes Then
        Conn.Execute "Delete from Campos where Cod_tabela=" & vCod_Tabela & " and nome_campo='" & lstCamposIndices.List(lsvCampos.SelectedItem.Index - 1) & "'"
        lsvCampos.ListItems.Remove (lsvCampos.SelectedItem.Index)
        Limpar_campo
        Preencher_Campos_Indices
    End If
End Sub

Private Sub cmdExcluirIndice_Click()
    Dim Nome_Indice As String
    
    If MsgBox("Confirma exclus�o do indice selecionado.", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
       If lsvIndices.SelectedItem = "" Then
          For i = lsvIndices.SelectedItem.Index To 1 Step -1
              If lsvIndices.ListItems(i) <> "" Then
                 Nome_Indice = lsvIndices.ListItems(i)
                 Exit For
              End If
          Next
       Else
          Nome_Indice = lsvIndices.SelectedItem
       End If
       Conn.Execute "Delete from Indices where Nome_indice ='" & Nome_Indice & "'"
       Preenche_lsv_indices
    End If
End Sub

Private Sub cmdExcluirTabela_Click()
    If vCod_Tabela = 0 Then
        MsgBox "N�o ser� poss�vel excluir esta tabela pois ela n�o existe no banco."
        Exit Sub
    End If
    
    If MsgBox("Confirma Exclus�o da Tabela ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
       Conn.Execute "Delete from Tabelas Where Cod_tabela = " & vCod_Tabela
       Conn.Execute "Delete from Campos where Cod_tabela = " & vCod_Tabela
       
       'Indices
       Set rst = New ADODB.Recordset
       rst.Open "Select * from Indices where cod_tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
       For i = 1 To rst.RecordCount
           Conn.Execute "Delete from Campos_indices Where cod_indice=" & rst!Cod_indice
           rst.MoveNext
       Next
       Conn.Execute "Delete from Indices Where cod_tabela = " & vCod_Tabela
       
       'Relacionamentos
       Set rst = New ADODB.Recordset
       rst.Open "Select * from Relacionamentos where cod_tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
       For i = 1 To rst.RecordCount
           Conn.Execute "Delete from Campos_relacionamento Where cod_Relacionamento=" & rst!Cod_Relacionamento
           rst.MoveNext
       Next
       Conn.Execute "Delete from Relacionamentos Where cod_tabela = " & vCod_Tabela
       
    End If
End Sub

Private Sub cmdSalvarIndice_Click()
    Dim Item_lista As Byte
    
    If lstCamposIndices.SelCount <= 0 Then
        MsgBox "Marque os campos para compor o Indice.", vbInformation, "Aten��o"
        Exit Sub
    End If
    
    If txtNomeIndice = "" Then
        MsgBox "Digite um nome para o indice.", vbInformation, "Aten��o"
        txtNomeIndice.SetFocus
        Exit Sub
    End If
    
    'Verificar se na tabela TABELAS existe um registro com o nome da tabela em questao
    Set rst = New ADODB.Recordset
    rst.Open "Select * from TABELAS where Nome_tabela = '" & txtNomeTabela & "'", Conn, adOpenKeyset, adLockOptimistic
    If rst.RecordCount <= 0 Then
       Conn.Execute "Insert into Tabelas (Nome_Tabela) Values ('" & UCase(txtNomeTabela) & "')"
    End If
    
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Indices Where Cod_Tabela=" & vCod_Tabela & _
             " AND Nome_indice = '" & UCase(txtNomeIndice) & "'", Conn, adOpenKeyset, adLockOptimistic
    
    If rst.RecordCount <= 0 Then
       Conn.Execute "Insert into Indices (Cod_tabela, Nome_Indice, Chave_Primaria, Unico, Ignorar_null) Values (" & vCod_Tabela & ",'" & UCase(txtNomeIndice) & "'," & chkPrimary.Value & "," & chkUnique.Value & "," & chkIgnoreNulls.Value & ")"
       Set rst = New ADODB.Recordset
       rst.Open "Select cod_indice from Indices where cod_tabela=" & vCod_Tabela & " and Nome_indice='" & UCase(txtNomeIndice) & "'", Conn, adOpenKeyset, adLockOptimistic
    End If
    
    vCod_Indice = rst!Cod_indice
    
    For i = 0 To lstCamposIndices.ListCount - 1
        If lstCamposIndices.Selected(i) = True Then
            Conn.Execute "Insert into Campos_indices Values (" & vCod_Indice & ",'" & lstCamposIndices.List(i) & "')"
        End If
    Next
    
    Preenche_lsv_indices

    txtNomeIndice = ""
    Preencher_Campos_Indices
End Sub

Private Sub cmdSalvarTabela_Click()
    Set rst = New ADODB.Recordset
    rst.Open "Select Cod_Tabela from Tabelas Where Nome_Tabela ='" & UCase(txtNomeTabela) & "'", Conn, adOpenKeyset, adLockOptimistic
    
    If rst.RecordCount <= 0 Then
        Conn.Execute "Insert into Tabelas (Nome_Tabela) Values ('" & UCase(txtNomeTabela) & "')"
        Set rst = New ADODB.Recordset
        rst.Open "Select Cod_Tabela from Tabelas Where Nome_Tabela ='" & UCase(txtNomeTabela) & "'", Conn, adOpenKeyset, adLockOptimistic
    End If
    
    vCod_Tabela = rst!Cod_tabela
    
    Preencher_Lsv_Campos
    
End Sub


Private Sub Form_Load()
    Me.Top = (FrmMDI.Height - (Me.Height + FrmMDI.SSPanel1.Height + 300)) / 2
    Me.Left = (FrmMDI.Width - Me.Width) / 2
End Sub

Private Sub lstCamposIndices_GotFocus()
    txtNomeIndice_GotFocus
End Sub

Private Sub lsvCampos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    txtNomeCampo = lsvCampos.SelectedItem
    cboTipoDados = lsvCampos.SelectedItem.SubItems(1)
    txtTamanho = lsvCampos.SelectedItem.SubItems(2)
    chkAutoNum.Value = IIf(lsvCampos.SelectedItem.SubItems(3) = "Sim", 1, 0)
    chkPermNull.Value = IIf(lsvCampos.SelectedItem.SubItems(4) = "Sim", 1, 0)
End Sub

Private Sub lsvIndices_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim Nome_Indice As String
    
    If lsvIndices.SelectedItem <> "" Then
        Nome_Indice = lsvIndices.SelectedItem
    Else
        For i = lsvIndices.SelectedItem.Index To 1 Step -1
            If lsvIndices.ListItems(i) <> "" Then
               Nome_Indice = lsvIndices.ListItems(i)
            End If
        Next
    End If
    
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Indices where Nome_indice = '" & Nome_Indice & "'", Conn, adOpenKeyset, adLockOptimistic
    
    chkPrimary.Value = rst!chave_primaria
    chkUnique.Value = rst!Unico
    chkIgnoreNulls.Value = rst!ignorar_null
End Sub

Private Sub txtNomeIndice_GotFocus()
    chkPrimary.Value = 0
    chkUnique.Value = 0
    chkIgnoreNulls.Value = 0
End Sub

Private Sub txtNomeTabela_LostFocus()
    cmdSalvarTabela_Click
    Preenche_lsv_indices
End Sub

Sub Preenche_lsv_indices()
    Dim rst As ADODB.Recordset
    Dim Nome_Indice As String
    
    Set rst = New ADODB.Recordset
    rst.Open "Select A.Nome_Indice, B.Nome_campo from Indices A, Campos_indices B where A.Cod_indice = B.Cod_indice and A.cod_tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lsvIndices.ListItems.Clear
    
    For i = 1 To rst.RecordCount
        If rst!Nome_Indice <> Nome_Indice Then
            Set litem = lsvIndices.ListItems.Add
            litem = rst!Nome_Indice
            litem.SubItems(1) = rst!Nome_Campo
            Nome_Indice = rst!Nome_Indice
        Else
            Set litem = lsvIndices.ListItems.Add
            litem.SubItems(1) = rst!Nome_Campo
        End If
        rst.MoveNext
    Next
End Sub

Sub Preencher_Lsv_Campos()
    Set rst = New ADODB.Recordset
    rst.Open "Select * from Campos_Criar where Cod_Tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lsvCampos.ListItems.Clear
    
    For i = 1 To rst.RecordCount
        Set litem = Me.lsvCampos.ListItems.Add
        litem = rst!Nome_Campo
        litem.SubItems(1) = rst!Tipo_Dados
        litem.SubItems(2) = rst!Tamanho_Campo
        litem.SubItems(3) = IIf(rst!AutoNumeracao = 0, "N�o", "Sim")
        litem.SubItems(4) = IIf(rst!PermNull = 0, "N�o", "Sim")
        rst.MoveNext
    Next
    Preencher_Campos_Indices
End Sub

Sub Preencher_Campos_Indices()
    
    Set rst = New ADODB.Recordset
    rst.Open "Select Nome_campo from Campos_Criar where Cod_Tabela = " & vCod_Tabela, Conn, adOpenKeyset, adLockOptimistic
    
    lstCamposIndices.Clear
    
    For i = 1 To rst.RecordCount
        lstCamposIndices.AddItem rst!Nome_Campo
        rst.MoveNext
    Next
End Sub

Sub Limpar_campo()
    txtNomeCampo = ""
    cboTipoDados.ListIndex = -1
    txtTamanho = ""
    chkAutoNum.Value = False
End Sub

Sub Limpar_Tudo()
    Limpar_campo
    lstCamposIndices.Clear
    lsvCampos.ListItems.Clear
    lsvIndices.ListItems.Clear
End Sub

Private Sub txtTamanho_KeyPress(KeyAscii As Integer)
    If KeyAscii < 48 Or KeyAscii > 57 Then
        If KeyAscii <> 8 Then
            KeyAscii = 0
        End If
    End If
End Sub
