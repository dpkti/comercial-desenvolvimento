VERSION 4.00
Begin VB.Form frmGrava_Arquivo 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Gravar Arquivo"
   ClientHeight    =   4020
   ClientLeft      =   2220
   ClientTop       =   1845
   ClientWidth     =   4050
   BeginProperty Font 
      name            =   "MS Sans Serif"
      charset         =   1
      weight          =   700
      size            =   8.25
      underline       =   0   'False
      italic          =   0   'False
      strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000080&
   Height          =   4425
   Left            =   2160
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   4050
   Top             =   1500
   Width           =   4170
   Begin VB.PictureBox Picture1 
      BorderStyle     =   0  'None
      Height          =   2895
      Left            =   150
      ScaleHeight     =   2895
      ScaleWidth      =   3735
      TabIndex        =   2
      Top             =   120
      Width           =   3735
      Begin VB.DriveListBox drvList 
         Height          =   315
         Left            =   375
         TabIndex        =   6
         Top             =   600
         Width           =   3030
      End
      Begin VB.DirListBox dirList 
         Height          =   1605
         Left            =   375
         TabIndex        =   5
         Top             =   960
         Width           =   3030
      End
      Begin VB.TextBox txtNome_Arquivo 
         ForeColor       =   &H000000FF&
         Height          =   285
         Left            =   1920
         MaxLength       =   8
         TabIndex        =   4
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label lblArquivo 
         Caption         =   "Nome do Arquivo"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   120
         Width           =   1695
      End
   End
   Begin VB.CommandButton cmdSearch 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Gravar"
      Default         =   -1  'True
      Height          =   720
      Left            =   450
      TabIndex        =   0
      Top             =   3000
      Width           =   1200
   End
   Begin VB.CommandButton cmdExit 
      BackColor       =   &H00C0C0C0&
      Caption         =   "&Sair"
      Height          =   720
      Left            =   2040
      TabIndex        =   1
      Top             =   3000
      Width           =   1200
   End
End
Attribute VB_Name = "frmGrava_Arquivo"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Dim SearchFlag As Integer   ' Used as flag for cancel and other operations.

Private Sub cmdExit_Click()

  Unload Me

End Sub

Private Sub cmdSearch_Click()
  Dim nome_arquivo As String
  Dim RESPOSTA As String
     
 On Error GoTo TRATA_ERRO
     
  If txtNome_Arquivo = "" Then
    MsgBox "Selecione um nome para o arquivo", vbExclamation, "Aten��o"
    txtNome_Arquivo.SetFocus
    Exit Sub
  End If
  Screen.MousePointer = 11
  dirList.Path = dirList.List(dirList.ListIndex)
  nome_arquivo = dirList.Path & "\" & txtNome_Arquivo & ".txt"
  
  FileCopy "C:\TEMP.TXT", nome_arquivo
  
  Screen.MousePointer = 0
  MsgBox "Arquivo: " & nome_arquivo & " gravado com sucesso.", vbExclamation, "Aten��o"

  Unload Me
  Exit Sub
  
TRATA_ERRO:
  If Err = 55 Then
    RESPOSTA = MsgBox("Substituir " & txtNome_Arquivo & " j� existente ?", vbYesNo, "Aten��o")
    If RESPOSTA = vbYes Then
      'Kill nome_arquivo
      Resume
    Else
      Screen.MousePointer = 0
      Exit Sub
    End If
  ElseIf Err = 76 Then
    nome_arquivo = dirList.Path & txtNome_Arquivo & ".txt"
  
    FileCopy "C:\TEMP.TXT", nome_arquivo
  
    Screen.MousePointer = 0
    MsgBox "Arquivo: " & nome_arquivo & " gravado com sucesso.", vbExclamation, "Aten��o"

    Unload Me
    Exit Sub
  ElseIf Err = 70 Then
    Screen.MousePointer = 0
    MsgBox "O arquivo que voc� est� tentando gravar est� aberto, feche-o e tente novamente", _
        vbInformation, "Aten��o"
    Exit Sub
  End If
  
  
End Sub

Private Function DirDiver(NewPath As String, DirCount As Integer, BackUp As String) As Integer

End Function

Private Sub DirList_LostFocus()
    dirList.Path = dirList.List(dirList.ListIndex)
End Sub

Private Sub DrvList_Change()
    On Error GoTo DriveHandler
    dirList.Path = drvList.Drive
    Exit Sub

DriveHandler:
    drvList.Drive = dirList.Path
    Exit Sub
End Sub

Private Sub Form_Load()
    Picture1.Move 0, 0
    'Picture1.Width = WinSeek.ScaleWidth
    'Picture1.BackColor = WinSeek.BackColor
End Sub

Private Sub txtNome_Arquivo_KeyPress(KeyAscii As Integer)
  KeyAscii = Maiusculo(KeyAscii)
End Sub


