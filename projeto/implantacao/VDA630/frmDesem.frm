VERSION 4.00
Begin VB.Form frmDesem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Desempenho Di�rio"
   ClientHeight    =   5535
   ClientLeft      =   1605
   ClientTop       =   1365
   ClientWidth     =   6675
   Height          =   5940
   Left            =   1545
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5535
   ScaleWidth      =   6675
   Top             =   1020
   Width           =   6795
   Begin VB.Frame Frame2 
      Caption         =   "LIGA��ES"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1815
      Left            =   840
      TabIndex        =   9
      Top             =   3480
      Width           =   5175
      Begin Threed.SSCommand SSCommand1 
         Height          =   375
         Left            =   3720
         TabIndex        =   19
         Top             =   840
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "Ranking Geral"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1920
         TabIndex        =   15
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label Label11 
         Caption         =   "TOTAL"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   14
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label10 
         Caption         =   "ATIVAS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   13
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "RECEPTIVAS"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   840
         Width           =   1095
      End
      Begin VB.Label lblAtiva 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1920
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label lblReceptiva 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1920
         TabIndex        =   10
         Top             =   840
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "PEDIDOS"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   1815
      Left            =   840
      TabIndex        =   0
      Top             =   960
      Width           =   5175
      Begin VB.Image Image1 
         Height          =   480
         Left            =   4440
         Picture         =   "frmDesem.frx":0000
         Top             =   720
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblAtingido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4440
         TabIndex        =   23
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label6 
         Caption         =   "%ATINGIDO"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3360
         TabIndex        =   22
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label lblQuota 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   21
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "VALOR QUOTA DIA"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   20
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label lblValor 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   4
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label lblQtd 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2040
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "VALOR"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "QUANTIDADE"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00800000&
      X1              =   0
      X2              =   6720
      Y1              =   3000
      Y2              =   3000
   End
   Begin VB.Label lblPseudonimo 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3840
      TabIndex        =   18
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label14 
      Caption         =   "DATA REAL"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1200
      TabIndex        =   17
      Top             =   3120
      Width           =   1575
   End
   Begin VB.Label lblDt_Real 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   16
      Top             =   3120
      Width           =   855
   End
   Begin VB.Label lblCod_vend 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   8
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label5 
      Caption         =   "VENDEDOR"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1080
      TabIndex        =   7
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblDt_Faturamento 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3120
      TabIndex        =   6
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "DATA FATURAMENTO"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1080
      TabIndex        =   5
      Top             =   600
      Width           =   1815
   End
End
Attribute VB_Name = "frmDesem"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub Label12_Click()

End Sub


Private Sub SSCommand1_Click()
  Dim sql As String
  Dim ss As Object
  Dim i As Long
  
  Screen.MousePointer = 11
  
  On Error GoTo Trata_Erro
    
    
  Set OraParameters = oradatabase.Parameters
  
  
  OraParameters.Remove "vCursor"
  OraParameters.Add "vCursor", 0, 2
  OraParameters("vCursor").serverType = ORATYPE_CURSOR
  OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
  OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
 
  OraParameters.Remove "fil"
  OraParameters.Add "fil", Filial, 1
 
  OraParameters.Remove "vErro"
  OraParameters.Add "vErro", 0, 2
  OraParameters("vErro").serverType = ORATYPE_NUMBER
  
  V_SQL = "Begin " & strTabela_Banco & "pck_vda630.pr_desempenho(:vCursor,:fil,:vErro);END;"
  
  oradatabase.ExecuteSQL V_SQL
  
  Set VarRec = oradatabase.Parameters("vCursor").Value
  
  If oradatabase.Parameters("vErro").Value <> 0 Then
    Call Process_Line_Errors(sql)
    Exit Sub
  End If
 ' Set ss = oradatabase.dbcreatedynaset(sql, 0&)
  
 ' If ss.EOF Then
 '   MsgBox "N�o h� dados para a consulta", vbInformation, "Aten��o"
 '   Screen.MousePointer = 0
 '   Exit Sub
 ' Else
     With frmRank.Grid1
        .Cols = 5
        .Rows = VarRec.RecordCount + 1
        .ColWidth(0) = 500
        .ColWidth(1) = 1200
        .ColWidth(2) = 900
        .ColWidth(3) = 1000
        .ColWidth(4) = 900
                
        .Row = 0
        .Col = 0
        .Text = "RANK."
        .Col = 1
        .Text = "TLMKT"
        .Col = 2
        .Text = "  ATIVA"
        .Col = 3
        .Text = "RECEPTIVA"
        .Col = 4
        .Text = "  TOTAL"
              
        VarRec.MoveFirst
        For i = 1 To VarRec.RecordCount
           ' .Rows -1
            .Row = i
            
            .Col = 0
            '.ColAlignment(0) = 1
            .Text = i
            .Col = 1
            .ColAlignment(1) = 0
            .Text = VarRec!pseudonimo
            .Col = 2
            .ColAlignment(2) = 1
            .Text = VarRec!ativa
            .Col = 3
            .ColAlignment(3) = 1
            .Text = VarRec!receptiva
            .Col = 4
            .ColAlignment(4) = 1
            .Text = VarRec!total
            VarRec.MoveNext
        Next
        .Row = 1
    End With
   frmRank.Grid1.Visible = True
   Screen.MousePointer = 0
   frmRank.Show 1
  'End If
  Exit Sub
Trata_Erro:
    If Err = 30009 Then
      Resume Next
    End If

End Sub


