﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class EnumAplicacao
    {

        public enum ParametrosEnum
        { 
            PM_MARGEM,
            PM_FATURAMENTO,
            PM_PERIODO,
            PM_PERIODO_Q,
            PM_PERIODO_SC6,
            PM_PERIODO_SC12,
            PM_PED_CONSECUTIVOS,
            PM_LIMITE_COMPRA,
            PM_DIAS_ATRASO,
            PM_COD_MENSAGEM,
            PM_MEDIA_CONTATO,
            PM_VARIACAO_CONTATO
        };

        public enum TipoCampo
        {
            STRING,
            NUMERICO,
            NUMERICO3,
            DECIMAL,
            DECIMAL12

        };

        public enum StatusClienteExcecao
        {
            InicioValidacaoArquivo,
            FimValidacaoArquivo,
            InicioGeracaoCSV,
            FimGeracaoCSV,
            InicioInsercaoClienteExcecao,
            FimInsercaoClienteExcecao,
            Erro
        }

        public enum ColunaPropriedade
        {
            CDCLI = 0,
            DTVIGENCIAINI = 1,
            DTVIGENCIAFIM = 2,
            SEGUNDA1 = 3,
            TERCA1 = 4,
            QUARTA1 = 5,
            QUINTA1 = 6,
            SEXTA1 = 7,
            SEGUNDA2 = 8, 
            TERCA2 = 9,
            QUARTA2 = 10,
            QUINTA2 = 11,
            SEXTA2 = 12,
            SEGUNDA3 = 13,
            TERCA3 = 14,
            QUARTA3 = 15,
            QUINTA3 = 16,
            SEXTA3 = 17,
            SEGUNDA4 = 18,
            TERCA4 = 19,
            QUARTA4 = 20,
            QUINTA4 = 21,
            SEXTA4 = 22,
            SEGUNDA5 = 23,
            TERCA5 = 24,
            QUARTA5 = 25,
            QUINTA5 = 26,
            SEXTA5 = 27,
            MOTIVO = 28
        }

        public enum rangePM_MARGEM : long { Min = 0, Max = 100 };
        public enum rangePM_FATURAMENTO : long { Min = 0, Max = 100 };
        public enum rangePM_PERIODO : long { Min = 1, Max = 12 };
        public enum rangePM_PED_CONSEC : long { Min = 1, Max = 5 };
        public enum rangePM_VARIACAO_CONTATO : long { Min = 0, Max = 100 };

    }
}
