﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public static class GlobalBE
    {
        public static UsuarioBE usuarioLogado = null;
        public static RetornoCliente FiltroRetorno = null;

    }

    public class FiltroPeriodo
    {
        public DateTime DATA_INICIAL { get; set; }
        public DateTime DATA_FINAL { get; set; }
        public long? CODCOORD { get; set; }
    
    }
}
