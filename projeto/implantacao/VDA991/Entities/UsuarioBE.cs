﻿
using System.Collections.Generic;

namespace Entities
{
    public class UsuarioBE
    {
        public string NOME_USUARIO { get; set; }
        public string SENHA { get; set; }
        public string LOGIN { get; set; }
        public bool PERMISSAO_ACESSO { get; set; }
        public long COD_USUARIO { get; set; }
        public int COD_FILIAL { get; set; }

        public bool ACESSOCOORDENADOR { get; set; }
        public bool ACESSOSUPERVISOR { get; set; }

    }

}
