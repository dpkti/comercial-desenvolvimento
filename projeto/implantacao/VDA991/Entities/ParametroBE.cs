﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ParametroBE
    {
        public string PARAMETRO { get; set; }
        public string DESCRICAO { get; set; }
        public string VALOR { get; set; }
    }

    public class ParametroEXC
    {
        public int CDFILIAL { get; set; }
        public string PARAMETRO { get; set; }
        public string VALOR { get; set; }
    }

    public class ParametroCons
    {
        public string PARAMETRO { get; set; }
        public int CDFILIAL { get; set; }
        public string NOME_FILIAL { get; set; }
        public string VALOR { get; set; }
    }

    public class FiltroParametro
    {
        public string PARAMETRO { get; set; }
        public long? CODCOORD { get; set; }
    }

    public class ParametroGeral
    {
        public string PARAMETRO { get; set; }
        public string DESCRICAO { get; set; }
        public int CDFILIAL { get; set; }
        public string VALOR { get; set; }
    }

}
