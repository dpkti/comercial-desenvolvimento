﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class VendedorBE
    {
        public DateTime DTPROCESS { get; set; }
        public Int64 CDVENDEDOR { get; set; }
        public Int64 CDCLI { get; set; }
        
    }

    public class RetornoExcedente
    {
        public DateTime DATA_ROTEIRO { get; set; }
        public string FILIAL { get; set; }
        public string VENDEDOR { get; set; }
        public string CLIENTE { get; set; }
        public string QUADRANTE { get; set; }
    }

    public class RetornoCarteirasIncompletas
    {

        public DateTime DTPROCESS { get; set; }
        public Int64 CDVENDEDOR { get; set; }
        public string VENDEDOR { get; set; }
        public Int64 QTDE_CLIENTES { get; set; }
        public Int64 QTDE_ALOCADOS { get; set; }
        public Int64 QTDE_FALTANTES { get; set; }

    }

    public class RetornoCarteiraVendedor
    {
        public string CLIENTE { get; set; }
        public string REPRESENTANTE { get; set; }
        public string QUADRANTE { get; set; }
    }

    public class FiltroContatosPend
    {
        public Int64 CDVENDEDOR { get; set; }
        public int CDFILIAL { get; set; }
        public DateTime DATA_INICIAL { get; set; }
        public DateTime DATA_FINAL { get; set; }
    
    }

    public class RetornoContatosPend
    {

        public DateTime DATA_ROTEIRO { get; set; }
        public int CDFILIAL { get; set; }
        public string FILIAL { get; set; }
        public Int64 CDVENDEDOR { get; set; }
        public string VENDEDOR { get; set; }
        public Int64 CDCLI { get; set; }
        public string CLIENTE { get; set; }
        public string QUADRANTE { get; set; }

    }

    public class FiltroVendedor
    {
        public Int64 CDVENDEDOR { get; set; }
        public string VENDEDOR { get; set; }
    }

}
