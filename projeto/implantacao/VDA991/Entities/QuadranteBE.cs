﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class QuadranteBE
    {
        public string QUADRANTE { get; set; }
        public double QTDCONTATO { get; set; }
    }

    public class QuadranteEXC
    {
        public int CDFILIAL { get; set; }
        public string QUADRANTE { get; set; }
        public double QTDCONTATO { get; set; }
    }

    public class QuadranteCons
    {
        public string QUADRANTE { get; set; }
        public int CDFILIAL { get; set; }
        public string NOME_FILIAL { get; set; }
        public double QTDCONTATO { get; set; }
    }

    public class FiltroQuadrante
    {
        public string QUADRANTE { get; set; }
        public long? CODCOORD { get; set; }
    }

    //TI-4404
    public class AcaoQuadrante
    {
        public int COD_LOJA { get; set; }        
        public string COD_QUADRANTE { get; set; }
        public int SEQUENCIA { get; set; }
        public int NUM_PRIORIDADE { get; set; }
        public string DESC_ACAO { get; set; }
    }

}
