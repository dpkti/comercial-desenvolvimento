﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
    public class ClienteBE
    {

        public Int64 CDCLI { get; set; }
        public DateTime DTVIGENCIAINI { get; set; }
        public DateTime DTVIGENCIAFIM { get; set; }
        public int SEGUNDA1 { get; set; }
        public int TERCA1 { get; set; }
        public int QUARTA1 { get; set; }
        public int QUINTA1 { get; set; }
        public int SEXTA1 { get; set; }
        public int SEGUNDA2 { get; set; }
        public int TERCA2 { get; set; }
        public int QUARTA2 { get; set; }
        public int QUINTA2 { get; set; }
        public int SEXTA2 { get; set; }
        public int SEGUNDA3 { get; set; }
        public int TERCA3 { get; set; }
        public int QUARTA3 { get; set; }
        public int QUINTA3 { get; set; }
        public int SEXTA3 { get; set; }
        public int SEGUNDA4 { get; set; }
        public int TERCA4 { get; set; }
        public int QUARTA4 { get; set; }
        public int QUINTA4 { get; set; }
        public int SEXTA4 { get; set; }
        public int SEGUNDA5 { get; set; }
        public int TERCA5 { get; set; }
        public int QUARTA5 { get; set; }
        public int QUINTA5 { get; set; }
        public int SEXTA5 { get; set; }
        public string MOTIVO { get; set; }

    }

    public class ClienteBEValidacao : ClienteBE
    {
        public Int32 linha { get; set; }
    }

    public class FiltroCliente
    {
        public Int64 CDCLI { get; set; }
        public DateTime DTVIGENCIAINI { get; set; }
        public DateTime DTVIGENCIAFIM { get; set; }
        public DateTime DTVIGENCIAINI_ORIG { get; set; }
        public DateTime DTVIGENCIAFIM_ORIG{ get; set; }
        public int TIPO { get; set; }
    }

    public class FiltroConsulta
    {
        public int CDFILIAL { get; set; }
        public Int64 CNPJ { get; set; }
        public Int64 CDCLI { get; set; }
        public string NOMECLI { get; set; }
        public string STATUS { get; set; }
        public long? CODCOORD { get; set; }
        public int INATIVOS { get; set; }

    }

    public class RetornoCliente 
    {
        public Int64 CDCLI { get; set; }
        public string NOMECLI { get; set; }
        public Int64 CNPJ { get; set; }
    }


    public class RetornoClienteExc
    {
        public Int64 CDCLI { get; set; }
        public string CLIENTE { get; set; }
        public string MOTIVO { get; set; }
        public DateTime DTVIGENCIAINI { get; set; }
        public DateTime DTVIGENCIAFIM { get; set; }
        public int STATUS { get; set; }

    }

    public class FiltroUpCliente : ClienteBE
    {

        public DateTime DTVIGENCIAINI_ORIG { get; set; }
        public DateTime DTVIGENCIAFIM_ORIG { get; set; }
    
    }

    public class FiltroCli
    { 
        public Int64 CDCLI { get; set; }
        public long? CODCOORD { get; set; }
        public int TIPO_USER { get; set; }
    }

}
