﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;
using UtilGeralDPA;

namespace Business
{
    public class LoginBO
    {
        /// <summary>
        /// Valida o usuário 
        /// </summary>
        /// <param name="oUsuarioLogado">objeto do usuario logado</param>
        /// <returns></returns>
        public static UsuarioBE ValidaUsuario(UsuarioBE oUsuarioLogado)
        {
            UsuarioBE oUser = new UsuarioBE();
            try
            {
                oUser = LoginDALC.ValidaUsuario(oUsuarioLogado);

            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);
                throw new Exception(String.Format(ResourceMensagem.ErroPadrao));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);

            }
            return oUser;
        }

        /// <summary>
        /// Verifica se o usuario tem permissão de acesso ao painel. 
        /// </summary>
        /// <param name="oUsuarioLogado">objeto do usuario logado</param>
        /// <returns></returns>
        public static Boolean ValidaAcessoPainel(Int32 loginUser)
        {
            Boolean permissao;
            try
            {
                permissao = LoginDALC.ValidaAcessoPainel(loginUser);

            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);
                throw new Exception(String.Format(ResourceMensagem.ErroConectarBanco));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);

            }
            return permissao;
        }

        /// <summary>
        /// Verifica se o usuario tem permissão aos Menus de Inserção/Alteração. 
        /// </summary>
        /// <param name="oUsuarioLogado">objeto do usuario logado</param>
        /// <returns></returns>
        public static Boolean ValidaAcessoMenu(string loginUser)
        {
            Boolean permissao;
            try
            {
                permissao = LoginDALC.ValidaAcessoMenu(loginUser);

            }
            catch (DALCException ex)
            {
                Logger.LogError(ex);
                throw new Exception(String.Format(ResourceMensagem.ErroConectarBanco));
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception(ResourceMensagem.ErroPadrao);

            }
            return permissao;
        }

    }
}
