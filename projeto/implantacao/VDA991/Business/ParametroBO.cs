﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class ParametroBO
    {
        /// <summary>
        /// Método responsável por buscar os Parametros.
        /// </summary>
        /// <returns>Parametro/Descricao/Valor</returns>
        public static List<ParametroBE> BuscarParametro()
        {
            List<ParametroBE> lista = new List<ParametroBE>();
            lista = ParametroDALC.BuscarParametro();
            return lista;
        }

        /// <summary>
        /// Método responsável por alterar Parametro.
        /// </summary>
        public static void AlterarParametro(ParametroBE oFiltro)
        {
            ParametroDALC.AlterarParametro(oFiltro);
        }

        /// <summary>
        /// Método responsável por buscar os Parametros Exceção.
        /// </summary>
        /// <returns>Parametro/Filial/Descricao/Valor</returns>
        public static List<ParametroCons> BuscarParametroExcecao(FiltroParametro oFiltro)
        {
            List<ParametroCons> lista = new List<ParametroCons>();
            lista = ParametroDALC.BuscarParametroExcecao(oFiltro);
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar os Parametros.
        /// </summary>
        /// <returns>Parametro/Valor</returns>
        public static List<ParametroEXC> BuscarParametroFilialExcecao(ParametroEXC oFiltro)
        {
            List<ParametroEXC> lista = new List<ParametroEXC>();
            lista = ParametroDALC.BuscarParametroFilialExcecao(oFiltro);
            return lista;
        }

        /// <summary>
        /// Método responsável por Inserir/Alterar Parametro Exceção.
        /// </summary>
        public static void InserirAlterarParametroExcecao(ParametroEXC oFiltro)

        {
            ParametroDALC.InserirAlterarParametroExcecao(oFiltro);
        }

        /// <summary>
        /// Método responsável por validar os valores do campo Valor.
        /// </summary>
        /// <returns>true/false</returns>
        public static bool ValidaValores(ParametroGeral oFiltro)
        {

            bool opcRetorno = false;

            //List<ParametroEXC> ListaParametros = new List<ParametroEXC>();
            //ListaParametros = ParametroDALC.BuscarParametroFilial(oFiltro);

            if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_MARGEM.ToString())
            {
                if (Convert.ToDouble(oFiltro.VALOR) >= (double)EnumAplicacao.rangePM_MARGEM.Min && Convert.ToDouble(oFiltro.VALOR) <= (double)EnumAplicacao.rangePM_MARGEM.Max) { opcRetorno = true; }
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_FATURAMENTO.ToString())
            {
                if (Convert.ToDouble(oFiltro.VALOR) >= (double)EnumAplicacao.rangePM_FATURAMENTO.Min && Convert.ToDouble(oFiltro.VALOR) <= (double)EnumAplicacao.rangePM_FATURAMENTO.Max) { opcRetorno = true; }
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO.ToString())
            {
                if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PERIODO.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PERIODO.Max) { opcRetorno = true; }

            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_Q.ToString())
            {
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC6.ToString())
            {
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC12.ToString())
            {
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PED_CONSECUTIVOS.ToString())
            {
                if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Max) { opcRetorno = true; }
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_LIMITE_COMPRA.ToString())
            {
                //if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Max) { opcRetorno = true; }
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_DIAS_ATRASO.ToString())
            {
                //if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Max) { opcRetorno = true; }
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_COD_MENSAGEM.ToString())
            {
                //if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Max) { opcRetorno = true; }
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_MEDIA_CONTATO.ToString())
            {
                //if (Convert.ToInt32(oFiltro.VALOR) >= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Min && Convert.ToDouble(oFiltro.VALOR) <= (Int32)EnumAplicacao.rangePM_PED_CONSEC.Max) { opcRetorno = true; }
                return true;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_VARIACAO_CONTATO.ToString())
            {
                if (Convert.ToDouble(oFiltro.VALOR) >= (double)EnumAplicacao.rangePM_VARIACAO_CONTATO.Min && Convert.ToDouble(oFiltro.VALOR) <= (double)EnumAplicacao.rangePM_VARIACAO_CONTATO.Max) { opcRetorno = true; }                
            }

            return opcRetorno;
        }

        /// <summary>
        /// Método responsável por validar o KeyPress do campo Valor.
        /// </summary>
        /// <returns>EnumeradoresAplicacao.TipoCampo</returns>
        public static EnumAplicacao.TipoCampo ValidaKeyPressValor(string valorParametro)
        {
            if (valorParametro == EnumAplicacao.ParametrosEnum.PM_MARGEM.ToString())
            {
                return EnumAplicacao.TipoCampo.DECIMAL;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_FATURAMENTO.ToString())
            {
                return EnumAplicacao.TipoCampo.DECIMAL;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_PERIODO.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_PERIODO_Q.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC6.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC12.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_PED_CONSECUTIVOS.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_LIMITE_COMPRA.ToString())
            {
                return EnumAplicacao.TipoCampo.DECIMAL12;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_DIAS_ATRASO.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO3;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_COD_MENSAGEM.ToString())
            {
                return EnumAplicacao.TipoCampo.STRING;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_MEDIA_CONTATO.ToString())
            {
                return EnumAplicacao.TipoCampo.NUMERICO3;
            }
            else if (valorParametro == EnumAplicacao.ParametrosEnum.PM_VARIACAO_CONTATO.ToString())
            {
                return EnumAplicacao.TipoCampo.DECIMAL;
            }

            else
            {
                return EnumAplicacao.TipoCampo.NUMERICO;
            }

        }

        /// <summary>
        /// Método responsável por retornar mensagens de erro,
        /// Para o campo valor
        /// </summary>
        /// <returns>Mensagem</returns>
        public static string ValidaMensagens(ParametroGeral oFiltro)
        {
            string msgRetorno = "";

            if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_MARGEM.ToString())
            {
				msgRetorno = "Valor Inválido para o Parâmetro " + oFiltro.PARAMETRO + "\n Valores Aceitos " + (double)EnumAplicacao.rangePM_MARGEM.Min + " até " + (double)EnumAplicacao.rangePM_MARGEM.Max;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_FATURAMENTO.ToString())
            {
                msgRetorno = "Valor Inválido para o Parâmetro " + oFiltro.PARAMETRO + "\n Valores Aceitos " + (double)EnumAplicacao.rangePM_FATURAMENTO.Min + " até " + (double)EnumAplicacao.rangePM_FATURAMENTO.Max;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO.ToString())
            {
                msgRetorno = "Valor Inválido para o Parâmetro " + oFiltro.PARAMETRO + "\n Valores Aceitos " + (double)EnumAplicacao.rangePM_PERIODO.Min + " até " + (double)EnumAplicacao.rangePM_PERIODO.Max;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_Q.ToString())
            {
                msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC6.ToString())
            {
                msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PERIODO_SC12.ToString())
            {
                msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_PED_CONSECUTIVOS.ToString())
            {
                msgRetorno = "Valor Inválido para o Parâmetro " + oFiltro.PARAMETRO + "\n Valores Aceitos " + (double)EnumAplicacao.rangePM_PED_CONSEC.Min + " até " + (double)EnumAplicacao.rangePM_PED_CONSEC.Max;
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_LIMITE_COMPRA.ToString())
            {
				msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";            
            }
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_DIAS_ATRASO.ToString())
            {
				msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";           
			}
            else if (oFiltro.PARAMETRO == EnumAplicacao.ParametrosEnum.PM_COD_MENSAGEM.ToString())
            {
				msgRetorno = "Valor Incorreto para o Parâmetro Selecionado.";
			}

            return msgRetorno;
        }


        /// <summary>
        /// Método responsável por desativar Parâmetro Exceção.
        /// </summary>
        public static void Update_Parametro_Exc(ParametroEXC oFiltro)
        {
            ParametroDALC.Update_Parametro_Exc(oFiltro);
        }


    }
}
