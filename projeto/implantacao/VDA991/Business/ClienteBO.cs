﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class ClienteBO
    {
        /// <summary>
        /// Método responsável por Inserir/Alterar Cliente Exceção.
        /// </summary>
        public static void InserirClienteExcecao(FiltroUpCliente oFiltro)
        {
            ClienteDALC.InserirClienteExcecao(oFiltro);
        }

        /// <summary>
        /// Método responsável por Buscar Cliente Exceção.
        /// </summary>
        public static List<ClienteBE> BuscarClienteExcecao(FiltroCliente oFiltro)
        {
            
            List<ClienteBE> lista = new List<ClienteBE>();
            lista = ClienteDALC.BuscarClienteExcecao(oFiltro);
            return lista;

        }

        /// <summary>
        /// Método responsável por Buscar Nome do CLiente
        /// </summary>
        public static string BuscarNomeCliente(FiltroUpCliente oFiltro)
        {

            string nomeCLi = "";
            nomeCLi = ClienteDALC.BuscarNomeCliente(oFiltro);
            return nomeCLi;

        }

        /// <summary>
        /// Método responsável por Validar o Periodo SEM USO
        /// </summary>
        public static bool ValidaPeriodo(FiltroCliente oFiltro)
        {

            bool periodo = false;
            periodo = ClienteDALC.ValidaPeriodo(oFiltro);
            return periodo;

        }

        /// <summary>
        /// Método responsável por Buscar Clientes
        /// </summary>
        public static List<RetornoCliente> BuscarClientes(FiltroConsulta oFiltro)
        {

            List<RetornoCliente> lista = new List<RetornoCliente>();
            lista = ClienteDALC.BuscarClientes(oFiltro);
            return lista;

        }


        public static List<RetornoClienteExc> BuscarDiasClientesExc(FiltroConsulta oFiltro)
        {
            List<RetornoClienteExc> lista = new List<RetornoClienteExc>();
            lista = ClienteDALC.BuscarDiasClientesExc(oFiltro);
            return lista;
        }


        public static void AlterarClienteExcecao(FiltroUpCliente oFiltro)
        {
            ClienteDALC.AlterarClienteExcecao(oFiltro);
        }


        /// <summary>
        /// Método Responsável por Validar a Filial do Cliente
        /// </summary>
        /// <param name="oFiltro"></param>
        /// <returns></returns>
        public static bool ValidaCliente(FiltroCli oFiltro)
        {
            
            bool vcliente = false;
            vcliente = ClienteDALC.ValidaCliente(oFiltro);
            return vcliente;

        }

        public static bool ValidaExcecaoAtiva(FiltroCliente oFiltroPeriodo)
        {
            bool vcliente = false;
            vcliente = ClienteDALC.ValidaExcecaoAtiva(oFiltroPeriodo);
            return vcliente;
        }

        public static void DeletaClienteExcecao(FiltroUpCliente oFiltroCli)
        {
            ClienteDALC.DeletaClienteExcecao(oFiltroCli);
        }
    }
}
