﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;
using System.ComponentModel;
using UtilGeralDPA;
using System.IO;
using Excel;
using System.Diagnostics;

namespace Business
{
    public class QuadranteBO
    {
        /// <summary>
        /// Método responsável por buscar os Quadrantes.
        /// </summary>
        /// <returns>Quadrante/QtdContato</returns>
        public static List<QuadranteBE> BuscarQuadrante()
        {
            List<QuadranteBE> lista = new List<QuadranteBE>();
            lista = QuadranteDALC.BuscarQuadrante();
            return lista;
        }

        /// <summary>
        /// Método responsável por alterar Quadrante.
        /// </summary>
        public static void AlterarQuadrante(QuadranteBE oFiltro)
        {
            QuadranteDALC.AlterarQuadrante(oFiltro);
        }

        /// <summary>
        /// Método responsável por buscar os Quadrantes.
        /// </summary>
        /// <returns>Quadrante/QtdContato</returns>
        public static List<QuadranteCons> BuscarQuadranteExcecao(FiltroQuadrante oFiltro)
        {
            List<QuadranteCons> lista = new List<QuadranteCons>();
            lista = QuadranteDALC.BuscarQuadranteExcecao(oFiltro);
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar os Quadrantes.
        /// </summary>
        /// <returns>Quadrante/QtdContato</returns>
        public static List<QuadranteEXC> BuscarQuadranteFilialExcecao(QuadranteEXC oFiltro)
        {
            List<QuadranteEXC> lista = new List<QuadranteEXC>();
            lista = QuadranteDALC.BuscarQuadranteFilialExcecao(oFiltro);
            return lista;
        }

        /// <summary>
        /// Método responsável por Inserir/Alterar Quadrante Exceção.
        /// </summary>
        public static void InserirAlterarQuadranteExcecao(QuadranteEXC oFiltro)
        {
            QuadranteDALC.InserirAlterarQuadranteExcecao(oFiltro);
        }

        public static bool ValidaValoresQuadrante(string valorCampo)
        {

            double valor;

            if (string.IsNullOrEmpty(valorCampo))
            {
                valorCampo = "0";
            }

            valor = Convert.ToDouble(valorCampo);

            if (valor == 0 || valor == 0.25 || valor == 0.50 ||
               valor == 1 || valor == 2 || valor == 3 ||
               valor == 4 || valor == 5)
            {
                return true;
            }

            else
            {
                return false;
            }

        }

        /// <summary>
        /// Método responsável por desativar Quadrante Exceção.
        /// </summary>
        public static void Update_Quadrante_Exc(QuadranteEXC oFiltro)
        {
            QuadranteDALC.Update_Quadrante_Exc(oFiltro);
            
        }
        
        /// <summary>
        /// Método responsável por buscar os Quadrantes relacionados as depósitos. TI-4404
        /// </summary>
        /// <returns>Quadrantes</returns>
        public static List<QuadranteBE> BuscarAcaoQuadrante()
        {
            List<QuadranteBE> lista = new List<QuadranteBE>();
            lista = QuadranteDALC.BuscarAcaoQuadrante();
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar todos os Quadrantes relacionados as depósitos. TI-4404
        /// </summary>
        /// <returns>Quadrantes</returns>
        public static List<AcaoQuadrante> BuscarListaAcaoQuadrante(AcaoQuadrante oFiltro)
        {
            List<AcaoQuadrante> lista = new List<AcaoQuadrante>();
            lista = QuadranteDALC.BuscarListaAcaoQuadrante(oFiltro);
            return lista;
        }

        /// <summary>
        /// Método responsável por Inserir/Alterar Quadrante. TI-4404
        /// </summary>
        public static void AtualizarQuadrante(AcaoQuadrante oFiltro)
        {
            QuadranteDALC.AtualizarQuadrante(oFiltro);
        }

        /// <summary>
        /// Método responsável por carregar o arquivo excel que será importado. TI-4404
        /// </summary>
        /// <returns></returns>
        public static List<AcaoQuadrante> CarregarExcel(string caminhoArquivo)
        {
            string caminhoOrigem;
            string arquivo="";
            List<AcaoQuadrante> lstArquivo = new List<AcaoQuadrante>();
            IExcelDataReader excelReader = null;

            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                int codLoja;
                int Seq;
                int ordem;
                int count = 0;

                FileStream stream = File.Open(arquivo, FileMode.Open, FileAccess.Read);

                string extension = Path.GetExtension(arquivo);
                if (extension.Equals(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;

                while (excelReader.Read())
                {
                    if (count > 0)
                    {
                        AcaoQuadrante arq = new AcaoQuadrante();
                        if ((excelReader.GetValue(0) != null && excelReader.GetValue(1) != null && excelReader.GetValue(2) != null)
                            && (int.TryParse(excelReader.GetValue(0).ToString(), out codLoja))
                            && (int.TryParse(excelReader.GetValue(2).ToString(), out Seq))
                            && (int.TryParse(excelReader.GetValue(3).ToString(), out ordem))
                            && (int.Parse(excelReader.GetValue(3).ToString()) < 10))
                        {
                            arq.COD_LOJA = excelReader.GetInt32(0);
                            arq.COD_QUADRANTE = excelReader.GetString(1);
                            arq.SEQUENCIA = excelReader.GetInt32(2);
                            arq.NUM_PRIORIDADE = excelReader.GetInt32(3);
                            arq.DESC_ACAO = excelReader.GetString(4);
                            lstArquivo.Add(arq);
                        }
                        else
                        {
                            if ((excelReader.GetValue(0) != null || excelReader.GetValue(1) != null && excelReader.GetValue(2) != null))
                            {
                                if (excelReader.GetValue(0).ToString().Length>0)
                                    Logger.LogError(String.Format("Linha: {0} - Erro ao atualizar o loja: {1}, quadrante: {2}, sequencia: {3}. Registro ignorado!", excelReader.Depth, excelReader.GetValue(0), excelReader.GetValue(1), excelReader.GetValue(2)));
                            }
                        }
                    }
                    count++;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                excelReader.Close();

                File.Delete(arquivo);
            }

            return lstArquivo;

        }
    }
}
