﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class FilialBO
    {
        /// <summary>
        /// Método responsável por buscar as Filiais.
        /// </summary>
        /// <returns>CODFILIAL/NOMEFILIAL</returns>
        public static List<FilialBE> BuscarFilial()
        {
            
            
            List<FilialBE> lista = new List<FilialBE>();
            if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
            {
                lista = FilialDALC.BuscarFilialCoordenador(Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN));
            }
            else
            {
                lista = FilialDALC.BuscarFilial();
            }
            return lista;
        }

        /// <summary>
        /// Método responsável por buscar os Depósitos. TI-4404
        /// </summary>
        /// <returns>CODFILIAL/NOMEFILIAL</returns>
        public static List<FilialBE> BuscarLoja()
        {
            List<FilialBE> lista = new List<FilialBE>();
            lista = FilialDALC.BuscarLoja();
            return lista;
        }
    }
}
