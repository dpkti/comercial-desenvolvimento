﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;

namespace Business
{
    public class VendedorBO
    {
        /// <summary>
        /// Método responsável por lista de Excedentes
        /// </summary>
        public static List<RetornoExcedente> BuscarListaExcedentes(FiltroPeriodo oFiltro)
        {
            List<RetornoExcedente> listaExcedentes = new List<RetornoExcedente>();
            listaExcedentes = VendedorDALC.BuscarListaExcedentes(oFiltro);
            return listaExcedentes;
        }

        /// <summary>
        /// Método responsável por lista Carteiras Incompletas
        /// </summary>
        public static List<RetornoCarteirasIncompletas> BuscarCarteirasIncompletas(FiltroPeriodo oFiltro)
        {
            List<RetornoCarteirasIncompletas> listaCarteiras = new List<RetornoCarteirasIncompletas>();
            listaCarteiras = VendedorDALC.BuscarCarteirasIncompletas(oFiltro);
            return listaCarteiras;

        }

        public static List<RetornoCarteiraVendedor> BuscarCarteiraVendedor(VendedorBE oFiltro)
        {
            List<RetornoCarteiraVendedor> listaCarteiraVendedor = new List<RetornoCarteiraVendedor>();
            listaCarteiraVendedor = VendedorDALC.BuscarCarteiraVendedor(oFiltro);
            return listaCarteiraVendedor;

        }

        public static List<RetornoContatosPend> BuscarContatosPend(FiltroContatosPend oFiltro)
        {
            List<RetornoContatosPend> listaContatosPend = new List<RetornoContatosPend>();
            listaContatosPend = VendedorDALC.BuscarContatosPend(oFiltro);
            return listaContatosPend;
        }

        public static void UpdateContatosPend(VendedorBE oFiltro)
        {
            VendedorDALC.UpdateContatosPend(oFiltro);
        }


        public static List<FiltroVendedor> BuscarVendedor(long? lngCodCoord)
        {
            List<FiltroVendedor> ListaVendedor = new List<FiltroVendedor>();
            ListaVendedor = VendedorDALC.BuscarVendedor(lngCodCoord);
            return ListaVendedor;

        }
    }
}
