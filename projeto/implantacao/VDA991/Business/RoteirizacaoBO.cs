﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using DALC;
using System.ComponentModel;
using UtilGeralDPA;
using System.IO;
using Excel;
using System.Diagnostics;
using System.Globalization;
//using System.Transactions;

namespace Business
{
    public class RoteirizacaoBO
    {
        
        /// <summary>
        /// Método responsável por gerar arquivo CSV a partir do arquivo excel importado
        /// </summary>
        /// <param name="bgWorkerRoteirizacao">background para mostrar em qual passo esta</param>
        /// <param name="listaClienteExc">Lista dos clientes</param>
        /// <returns>sucesso ou erro</returns>
        public static bool GerarArquivoCSV(BackgroundWorker bgWorkerRoteirizacao, List<ClienteBE> listaClienteExc)
        {
            bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.InicioGeracaoCSV);
            DateTime dataInicio = Logger.LogInicioMetodo();
            string caminhoArquivoCSV = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.DIR_TEMP_SQL_LOADER_ROTEIRIZACAO) + "ROTEIRIZACAOCSV.csv";
            string caminhoArquivoCTL = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.DIR_TEMP_SQL_LOADER_ROTEIRIZACAO) + "ROTEIRIZACAOCTL.ctl";
            string caminhoArquivoLog = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.DIR_TEMP_SQL_LOADER_ROTEIRIZACAO) + "logSQLLoader.log";
            string caminhoBatSqlLoader = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.DIR_TEMP_SQL_LOADER_ROTEIRIZACAO) + "\\ROTEIRIZACAOSQLLoader.bat";
            string usuarioConexaoBanco = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.USERID_SQL_LOADER_ROTEIRIZACAO);
            try
            {
                CriarDiretorio();
                GerarArquivoCSV(listaClienteExc, caminhoArquivoCSV);
                GerarSqlLoader(caminhoArquivoCSV, caminhoArquivoCTL);
                GerarBatSqlLoader(usuarioConexaoBanco, caminhoArquivoCTL, caminhoArquivoLog, caminhoBatSqlLoader);
                RoteirizacaoDALC.LimparStageClienteExc();
                ProcessStartInfo psi = new ProcessStartInfo(caminhoBatSqlLoader);

                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;

                Process process = new Process();
                process.EnableRaisingEvents = false;
                process.StartInfo = psi;

                process.Start();
                process.WaitForExit();
                process.Close();

                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.FimGeracaoCSV);

                return true;
            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.Erro);
                throw new Exception("Falha ao processar o arquivo.", ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Cria diretório para o SQLLoader
        /// </summary>
        /// <returns></returns>
        private static Boolean CriarDiretorio()
        {
            try
            {
                DirectoryInfo dirArquivoSQLLoader = new DirectoryInfo(UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.DIR_TEMP_SQL_LOADER_ROTEIRIZACAO));

                if (!dirArquivoSQLLoader.Exists)
                {
                    dirArquivoSQLLoader.Create();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Transforma a lista de Clientes em string separada por virgula
        /// </summary>
        /// <param name="listaClientesExc">lista de Clientes Exceção</param>
        /// <param name="caminhoArquivoCSV">caminho que deverá ser gravado o csv</param>
        private static void GerarArquivoCSV(List<ClienteBE> listaClientesExc, string caminhoArquivoCSV)
        {
            //Cria uma string em formatação CSV com os dados do objeto item
            String csv = ToCsv(",", listaClientesExc);

            //Cria arquivo CSV com os dados a serem gravados no banco
            using (StreamWriter sw = new StreamWriter(caminhoArquivoCSV))
            {
                sw.WriteLine(csv);
            }
        }

        /// <summary>
        /// Auxilia a geração do arquivo csv
        /// </summary>
        /// <param name="separator">separador das colunas no arquivo csv</param>
        /// <param name="listaClientes">lista de Clientes</param>
        /// <returns>string de arquivo csv</returns>
        private static string ToCsv(string separator, List<ClienteBE> listaClientes)
        {
            StringBuilder csvdata = new StringBuilder();

            foreach (var item in listaClientes)
                csvdata.AppendLine(ToCsvFields(separator, item));

            return csvdata.ToString();
        }

        /// <summary>
        /// Auxilia a geração do arquivo csv
        /// </summary>
        /// <param name="separator">separado dos campos CSV</param>
        /// <param name="ClienteBE">Objeto com Clientes</param>
        /// <returns></returns>
        private static string ToCsvFields(string separator, ClienteBE ClienteExc)
        {
            StringBuilder linie = new StringBuilder();
            linie.Append(ClienteExc.CDCLI.ToString() + separator +
                String.Format("{0:dd-MMM-yyyy}", ClienteExc.DTVIGENCIAINI) + separator +
                String.Format("{0:dd-MMM-yyyy}", ClienteExc.DTVIGENCIAFIM) + separator +
                ClienteExc.SEGUNDA1.ToString() + separator +
                ClienteExc.TERCA1.ToString() + separator +
                ClienteExc.QUARTA1.ToString() + separator +
                ClienteExc.QUINTA1.ToString() + separator +
                ClienteExc.SEXTA1.ToString() + separator +
                ClienteExc.SEGUNDA2.ToString() + separator +
                ClienteExc.TERCA2.ToString() + separator +
                ClienteExc.QUARTA2.ToString() + separator +
                ClienteExc.QUINTA2.ToString() + separator +
                ClienteExc.SEXTA2.ToString() + separator +
                ClienteExc.SEGUNDA3.ToString() + separator +
                ClienteExc.TERCA3.ToString() + separator +
                ClienteExc.QUARTA3.ToString() + separator +
                ClienteExc.QUINTA3.ToString() + separator +
                ClienteExc.SEXTA3.ToString() + separator +
                ClienteExc.SEGUNDA4.ToString() + separator +
                ClienteExc.TERCA4.ToString() + separator +
                ClienteExc.QUARTA4.ToString() + separator +
                ClienteExc.QUINTA4.ToString() + separator +
                ClienteExc.SEXTA4.ToString() + separator +
                ClienteExc.SEGUNDA5.ToString() + separator +
                ClienteExc.TERCA5.ToString() + separator +
                ClienteExc.QUARTA5.ToString() + separator +
                ClienteExc.QUINTA5.ToString() + separator +
                ClienteExc.SEXTA5.ToString() + separator + 
                ClienteExc.MOTIVO.ToString()); 

            return linie.ToString();
        }

        /// <summary>
        /// Gera arquivo para o SQLLoader
        /// </summary>
        /// <param name="caminhoArquivoCSV">caminho do arquivo CSV</param>
        /// <param name="caminhoArquivoCTL">caminho do arquivo CLT</param>
        private static void GerarSqlLoader(string caminhoArquivoCSV, string caminhoArquivoCTL)
        {
            string cmd = @"OPTIONS 
                            (SKIP = 0, 
                             ERRORS = 1000, 
                             ROWS = 66796, 
                             DIRECT = FALSE, 
                             PARALLEL = FALSE 
                            ) 
                           LOAD DATA
                           CHARACTERSET UTF8
                           INFILE '" + caminhoArquivoCSV +
                         @"'APPEND INTO TABLE PRODUCAO.STG_ROTEIRIZACAO_CLIENTE_EXC
                           FIELDS TERMINATED BY ','
                           TRAILING NULLCOLS
                           (
                            CDCLI               INTEGER EXTERNAL,     
                            DTVIGENCIAINI       DATE EXTERNAL,
                            DTVIGENCIAFIM       DATE EXTERNAL,
                            SEGUNDA1            INTEGER EXTERNAL,
                            TERCA1              INTEGER EXTERNAL,
                            QUARTA1             INTEGER EXTERNAL,
                            QUINTA1             INTEGER EXTERNAL,
                            SEXTA1              INTEGER EXTERNAL,
                            SEGUNDA2            INTEGER EXTERNAL,
                            TERCA2              INTEGER EXTERNAL,
                            QUARTA2             INTEGER EXTERNAL,
                            QUINTA2             INTEGER EXTERNAL,
                            SEXTA2              INTEGER EXTERNAL,
                            SEGUNDA3            INTEGER EXTERNAL,
                            TERCA3              INTEGER EXTERNAL,
                            QUARTA3             INTEGER EXTERNAL,
                            QUINTA3             INTEGER EXTERNAL,
                            SEXTA3              INTEGER EXTERNAL,
                            SEGUNDA4            INTEGER EXTERNAL,
                            TERCA4              INTEGER EXTERNAL,
                            QUARTA4             INTEGER EXTERNAL,
                            QUINTA4             INTEGER EXTERNAL,
                            SEXTA4              INTEGER EXTERNAL,
                            SEGUNDA5            INTEGER EXTERNAL,
                            TERCA5              INTEGER EXTERNAL,
                            QUARTA5             INTEGER EXTERNAL,
                            QUINTA5             INTEGER EXTERNAL,
                            SEXTA5              INTEGER EXTERNAL,
                            MOTIVO              CHAR)";

            //Cria arquivo CTL com os comandos de SqlLoader
            using (StreamWriter sw = new StreamWriter(caminhoArquivoCTL))
            {
                sw.WriteLine(cmd);
            }
        }

        /// <summary>
        /// Gera bat para o SQLLoader
        /// </summary>
        /// <param name="usuarioConexaoBanco">usuário de conexão</param>
        /// <param name="caminhoArquivoCTL">caminho do arquivo CLT</param>
        /// <param name="caminhoArquivoLog">caminho do arquivo de log</param>
        /// <param name="caminhoBatSqlLoader">caminho do bat</param>
        private static void GerarBatSqlLoader(string usuarioConexaoBanco, string caminhoArquivoCTL, string caminhoArquivoLog, string caminhoBatSqlLoader)
        {
            string cmd = @"sqlldr userid=" + usuarioConexaoBanco + " control=" + caminhoArquivoCTL + " log=" + caminhoArquivoLog;

            //Cria arquivo CTL com os comandos de SqlLoader
            using (StreamWriter sw = new StreamWriter(caminhoBatSqlLoader))
            {
                sw.WriteLine(cmd);
                //sw.WriteLine("pause"); //para adicionar uma pause para testes
            }
        }

        /// <summary>
        /// Método responsável por inserir o Cliente Exceção no banco
        /// </summary>
        /// <param name="bgWorkerRoteirizacao"></param>
        /// <param name="qtdClienteExc"></param>
        /// <returns></returns>
        public static bool InserirClienteExcecao(BackgroundWorker bgWorkerRoteirizacao, out int qtdClienteExc)
        {
            bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.InicioInsercaoClienteExcecao);
            try
            {
                qtdClienteExc = RoteirizacaoDALC.InserirClienteExcecao();
                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.FimInsercaoClienteExcecao);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.Erro);
                throw new Exception("Erro ao inserir tabela PRODUCAO.ROTEIRIZACAO_CLIENTE_EXCECAO: " + ex.Message);
            }
        }

        /// <summary>
        /// Método responsável por validar o arquivo excel importado
        /// </summary>
        /// <param name="caminhoArquivo">caminho do arquivo excel</param>
        /// <param name="bgWorkerRoteirizacao">backgroundworker para mostrar o passo em que esta</param>
        /// <param name="retLogGeraArq">log de erro</param>
        /// <returns>lista com o Cliente Exceção</returns>
        public static List<ClienteBE> ValidarArquivoExcel(string caminhoArquivo, BackgroundWorker bgWorkerRoteirizacao, out string retLogGeraArq)
        {
            bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.InicioValidacaoArquivo);
            DateTime dataInicio = Logger.LogInicioMetodo();
            string caminhoOrigem;
            string arquivo;
            string arqListErro = string.Empty;
            string arqListSuc = string.Empty;
            List<String> listaErro = new List<String>();
            List<String> listaSucesso = new List<String>();
            bool erro = false;
            int count = 0;

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            try
            {
                caminhoOrigem = caminhoArquivo;
                arquivo = "C:\\Windows\\Temp\\" + Path.GetFileName(caminhoOrigem).ToString();
                File.Copy(caminhoOrigem, arquivo, true);

                List<ClienteBE> listaClientesExc = new List<ClienteBE>();

                FileStream stream = File.Open(arquivo, FileMode.Open, FileAccess.Read);

                string extension = Path.GetExtension(arquivo);
                IExcelDataReader excelReader;
                if (extension.Equals(".xls"))
                {
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;

                while (excelReader.Read())
                {
                    if (count > 0 && (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.CDCLI) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAINI) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAFIM) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA1) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA1) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA1) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA1) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA1) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA2) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA2) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA2) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA2) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA2) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA3) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA3) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA3) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA3) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA3) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA4) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA4) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA4) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA4) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA4) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA5) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA5) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA5) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA5) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA5) != null
                                      || excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.MOTIVO) != null))

                    {
                        ClienteBE oClienteExc = new ClienteBE();

                        oClienteExc = ValidarTipagem(excelReader, listaErro);

                        if (oClienteExc != null)
                        {

                            if (!ValidaCliente(excelReader.Depth, oClienteExc, listaErro))
                            {
                                erro = true;
                            }

                            if (!ValidaClienteFilial(excelReader.Depth, oClienteExc, listaErro))
                            {
                                erro = true;
                            }

                            if (!ValidaDataVigenciaInicial(excelReader.Depth, oClienteExc.DTVIGENCIAINI, listaErro))
                            {
                                erro = true;
                            }

                            if (!ValidaDataVigenciaFinal(excelReader.Depth, oClienteExc.DTVIGENCIAFIM, listaErro))
                            {
                                erro = true;
                            }

                            if (!ValidaDataVigenciaInicialFinal(excelReader.Depth, oClienteExc.DTVIGENCIAINI,oClienteExc.DTVIGENCIAFIM, listaErro))
                            {
                                erro = true;
                            }

                            //if (!ValidarDataVigenteCliente(excelReader.Depth, oClienteExc, listaErro))
                            //{
                            //    erro = true;
                            //}

                            if (!erro)
                            {
                                listaClientesExc.Add(oClienteExc);
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            erro = true;
                        
                        }
                    }
                    count++;
                }

                excelReader.Close();

                if (erro)
                {
                    arqListErro = gravarLista(listaErro, caminhoArquivo, "Erro");
                }
                else
                {
                    arqListErro = string.Empty;
                }

                retLogGeraArq = arqListErro;

                File.Delete(arquivo);

                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.FimValidacaoArquivo);

                return listaClientesExc;

            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex);
                bgWorkerRoteirizacao.ReportProgress(1, EnumAplicacao.StatusClienteExcecao.Erro);
                throw new Exception("Falha ao processar o arquivo.", ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por gravar lista de erro do arquivo importado
        /// </summary>
        /// <param name="listaErro"></param>
        /// <param name="nomeArquivo"></param>
        /// <param name="tipolog"></param>
        /// <returns></returns>
        private static String gravarLista(List<string> listaErro, string nomeArquivo, string tipolog)
        {
            String nomeLog = String.Empty;

            try
            {
                if (listaErro != null && !String.IsNullOrEmpty(nomeArquivo))
                {
                    nomeLog = String.Format("LOG_{0}_{1}_{2}.txt",
                        tipolog.ToString(),
                        Path.GetFileName(nomeArquivo).ToString(),
                        DateTime.Now.ToString("ddMMyyyy"));

                    nomeLog = System.AppDomain.CurrentDomain.BaseDirectory.ToString() + @"\Log.Roteirizacao\" + nomeLog;

                    if (File.Exists(nomeLog))
                    {
                        File.Delete(nomeLog);
                    }

                    using (var arqGravar = File.CreateText(nomeLog))
                    {
                        arqGravar.WriteLine("<---Log de Importação Cliente Exceção--->");
                        listaErro.ForEach(i => arqGravar.WriteLine(i));
                        arqGravar.WriteLine("<---Fim arquivo Log--->");
                        arqGravar.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro ao criar arquivo de log: " + ex.Message);
            }

            return nomeLog;
        }

        /// <summary>
        /// Método responsável por validar se a data vigência inicial é maior que a data atual
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="dataVigenciaInicial"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaDataVigenciaInicial(int posicao, DateTime dataVigencia, List<string> listaErro)
        {
            bool dataValida;

            if (dataVigencia <= DateTime.Now)
            {
                dataValida = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, DATA_VIGENCIA_INICIAL deve ser maior que a data atual. - Em {1}", posicao, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }
            else
            {
                dataValida = true;
            }

            return dataValida;
        }

        /// <summary>
        /// Método responsável por validar se a data vigência final é maior que a data atual
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="dataVigenciaInicial"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaDataVigenciaFinal(int posicao, DateTime dataVigencia, List<string> listaErro)
        {
            bool dataValida;

            if (dataVigencia <= DateTime.Now)
            {
                dataValida = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, DATA_VIGENCIA_FINAL deve ser maior que a data atual. - Em {1}", posicao, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }
            else
            {
                dataValida = true;
            }

            return dataValida;
        }

        /// <summary>
        /// Método responsável por validar se a data vigência inicial é maior que a data final
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="dataVigenciaInicial"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaDataVigenciaInicialFinal(int posicao, DateTime dataVigenciaInicial,DateTime dataVigenciaFinal, List<string> listaErro)
        {
            bool dataValida;

            if (dataVigenciaInicial > dataVigenciaFinal)
            {
                dataValida = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, DATA_VIGENCIA_INICIAL deve ser menor que a DATA_VIGENCIA_FINAL. - Em {1}", posicao, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
            }
            else
            {
                dataValida = true;
            }

            return dataValida;
        }

        
        /// <summary>
        /// Método responsável por verificar se existe o cliente
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="oClienteExc"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaCliente(int posicao, ClienteBE oClienteExc, List<string> listaErro)
        {
            bool existeCliente = true;
            string nomeCliente = "";

            if (oClienteExc.CDCLI != 0)
            {

                FiltroUpCliente oFiltro = new FiltroUpCliente();

                oFiltro.CDCLI = oClienteExc.CDCLI;
                nomeCliente = ClienteBO.BuscarNomeCliente(oFiltro);

                if (nomeCliente == "")
                {
                    existeCliente = false;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Cliente não encontrado ({1}) ", posicao, oClienteExc.CDCLI));
                }
            }
            else
            {
                existeCliente = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, Cliente não encontrado ({1}) ", posicao, oClienteExc.CDCLI));
            }

            return existeCliente;
        }

        /// <summary>
        /// Método responsável por verificar se existe o cliente
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="oClienteExc"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidaClienteFilial(int posicao, ClienteBE oClienteExc, List<string> listaErro)
        {
            FiltroCli oFiltro = new FiltroCli();

            bool existeCliente = true;

            oFiltro.CDCLI = oClienteExc.CDCLI;
            oFiltro.TIPO_USER = 0;

            if (GlobalBE.usuarioLogado.ACESSOSUPERVISOR == true)
            {
                oFiltro.TIPO_USER = 1;
            }
            else
            {
                oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
            }


            if (ClienteBO.ValidaCliente(oFiltro) == false)
            {

                existeCliente = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, Cliente não pertence a Filial do Coordenador.", posicao));

            }

            return existeCliente;
        
        }


        /// <summary>
        /// Método responsável por validar os dias (SEM USO)
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="oClienteExc"></param>
        /// <param name="listaErro"></param>
        /// <returns></returns>
        private static bool ValidarDataVigenteCliente(int posicao, ClienteBE oClienteExc, List<string> listaErro)
        {
            bool existeVigencia = true;

            FiltroCliente oFiltro = new FiltroCliente();

            oFiltro.CDCLI = oClienteExc.CDCLI;
            oFiltro.DTVIGENCIAINI = oClienteExc.DTVIGENCIAINI;
            oFiltro.DTVIGENCIAFIM = oClienteExc.DTVIGENCIAFIM;

            if (ClienteBO.ValidaPeriodo(oFiltro) == true)
            {
                existeVigencia = false;
                listaErro.Add(String.Format("Linha: {0} - Erro, Vigência já cadastrada para o cliente ({1}) ", posicao, oClienteExc.CDCLI));
                
            }
            
            return existeVigencia;
        }

        private static ClienteBE ValidarTipagem(IExcelDataReader excelReader, List<String> listaErro)
        {

            int CDCLI;
            DateTime dataVigInicial;
            DateTime dataVigFinal;
            int diaSemana;

            bool cdcliValido;
            bool dataVigInicialValido;
            bool dataVigFinalValido;
            bool diaSemanaValido;
            string motivoValido;

            bool erro;
            ClienteBEValidacao oClienteExc = new ClienteBEValidacao();

            erro = false;
            oClienteExc.linha = excelReader.Depth;

            try
            {
                //Valida Campo Código Cliente
                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.CDCLI) != null)
                {
                    cdcliValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.CDCLI).ToString(), out CDCLI);

                    if (cdcliValido && CDCLI > 0)
                    {
                        oClienteExc.CDCLI = CDCLI;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, COD_CLIENTE em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, COD_CLIENTE é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                //Valida Data Inicial
                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAINI) != null)
                {
                    dataVigInicialValido = DateTime.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAINI).ToString(), out dataVigInicial);

                    if (!dataVigInicialValido)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, DATA_INICIAL em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                    else
                    {
                        oClienteExc.DTVIGENCIAINI = dataVigInicial;
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, DATA_INICIAL é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                //Valida Data Final
                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAFIM) != null)
                {
                    dataVigFinalValido = DateTime.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.DTVIGENCIAFIM).ToString(), out dataVigFinal);

                    if (!dataVigFinalValido)
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, DATA_FINAL em formato incorreto. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                    else
                    {
                        oClienteExc.DTVIGENCIAFIM = dataVigFinal;
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, DATA_FINAL é obrigatório. - Em {1}", excelReader.Depth, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                //Valida dias da semana
                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA1) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA1).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEGUNDA1 = diaSemana;
                        
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth,EnumAplicacao.ColunaPropriedade.SEGUNDA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA2) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA2).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEGUNDA2 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA3) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA3).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEGUNDA3 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA4) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA4).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEGUNDA4 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA5) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEGUNDA5).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEGUNDA5 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEGUNDA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA1) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA1).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.TERCA1 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA2) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA2).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.TERCA2 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA3) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA3).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.TERCA3 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA4) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA4).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.TERCA4 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA5) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.TERCA5).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.TERCA5 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.TERCA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA1) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA1).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUARTA1 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA2) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA2).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUARTA2 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA3) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA3).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUARTA3 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA4) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA4).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUARTA4 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA5) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUARTA5).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUARTA5 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUARTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA1) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA1).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUINTA1 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA2) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA2).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUINTA2 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA3) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA3).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUINTA3 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA4) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA4).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUINTA4 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA5) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.QUINTA5).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.QUINTA5 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.QUINTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA1) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA1).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEXTA1 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA1, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA2) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA2).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEXTA2 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA2, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA3) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA3).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEXTA3 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA3, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA4) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA4).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEXTA4 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA4, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA5) != null)
                {
                    diaSemanaValido = Int32.TryParse(excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.SEXTA5).ToString(), out diaSemana);

                    if (diaSemanaValido && (diaSemana == 0 || diaSemana == 1))
                    {
                        oClienteExc.SEXTA5 = diaSemana;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} em formato incorreto. Informar 0 ou 1 - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Dia Semana {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.SEXTA5, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.MOTIVO) != null)
                {
                    motivoValido = excelReader.GetValue((int)EnumAplicacao.ColunaPropriedade.MOTIVO).ToString();


                    if (motivoValido.Trim().Length <= 20)
                    {
                        oClienteExc.MOTIVO = motivoValido;
                    }
                    else
                    {
                        erro = true;
                        listaErro.Add(String.Format("Linha: {0} - Erro, Coluna {1} em formato incorreto. Informar com até 20 caracter. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.MOTIVO, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                    }
                }
                else
                {
                    erro = true;
                    listaErro.Add(String.Format("Linha: {0} - Erro, Coluna {1} é obrigatório. - Em {2}", excelReader.Depth, EnumAplicacao.ColunaPropriedade.MOTIVO, String.Format("{0:d/M/yyyy HH:mm:ss}", DateTime.Now.ToString())));
                }

                if (erro)
                {
                    return null;
                }
                else
                {
                    return oClienteExc;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new Exception("Erro na importação do Excel", ex);
            }
        }

    }
}
