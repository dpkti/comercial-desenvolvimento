﻿namespace VDA991
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.miQuadrante = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuadConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuadManutencao = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuadManutAlterar = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuadManutInserir = new System.Windows.Forms.ToolStripMenuItem();
            this.miQuadManutImp = new System.Windows.Forms.ToolStripMenuItem();
            this.miParametro = new System.Windows.Forms.ToolStripMenuItem();
            this.miParamConsulta = new System.Windows.Forms.ToolStripMenuItem();
            this.miParamManutencao = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.miCliExcecao = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manutencaoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miVendedor = new System.Windows.Forms.ToolStripMenuItem();
            this.miVendContatos = new System.Windows.Forms.ToolStripMenuItem();
            this.exclusaoDeContatosPendentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miVendProdutividade = new System.Windows.Forms.ToolStripMenuItem();
            this.roteirosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaDeExcedentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carteirasIncompletasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miSair = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miQuadrante,
            this.miParametro,
            this.miCliente,
            this.miVendedor,
            this.miSair});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(858, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "msMenu";
            // 
            // miQuadrante
            // 
            this.miQuadrante.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miQuadConsulta,
            this.miQuadManutencao});
            this.miQuadrante.Name = "miQuadrante";
            this.miQuadrante.Size = new System.Drawing.Size(76, 20);
            this.miQuadrante.Text = "&Quadrantes";
            // 
            // miQuadConsulta
            // 
            this.miQuadConsulta.Name = "miQuadConsulta";
            this.miQuadConsulta.Size = new System.Drawing.Size(152, 22);
            this.miQuadConsulta.Text = "&Consulta";
            this.miQuadConsulta.Click += new System.EventHandler(this.miQuadConsulta_Click);
            // 
            // miQuadManutencao
            // 
            this.miQuadManutencao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miQuadManutAlterar,
            this.miQuadManutInserir,
            this.miQuadManutImp});
            this.miQuadManutencao.Name = "miQuadManutencao";
            this.miQuadManutencao.Size = new System.Drawing.Size(152, 22);
            this.miQuadManutencao.Text = "&Manutenção";
            // 
            // miQuadManutAlterar
            // 
            this.miQuadManutAlterar.Name = "miQuadManutAlterar";
            this.miQuadManutAlterar.Size = new System.Drawing.Size(196, 22);
            this.miQuadManutAlterar.Text = "&Alterar";
            this.miQuadManutAlterar.Click += new System.EventHandler(this.miQuadManutAlterar_Click);
            // 
            // miQuadManutInserir
            // 
            this.miQuadManutInserir.Name = "miQuadManutInserir";
            this.miQuadManutInserir.Size = new System.Drawing.Size(196, 22);
            this.miQuadManutInserir.Text = "&Inserir/Alterar Exceção";
            this.miQuadManutInserir.Click += new System.EventHandler(this.miQuadManutInserir_Click);
            // 
            // miQuadManutImp
            // 
            this.miQuadManutImp.Name = "miQuadManutImp";
            this.miQuadManutImp.Size = new System.Drawing.Size(196, 22);
            this.miQuadManutImp.Text = "I&mportar Quadrante";
            this.miQuadManutImp.Click += new System.EventHandler(this.miQuadManutImp_Click);
            // 
            // miParametro
            // 
            this.miParametro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miParamConsulta,
            this.miParamManutencao});
            this.miParametro.Name = "miParametro";
            this.miParametro.Size = new System.Drawing.Size(74, 20);
            this.miParametro.Text = "&Parâmetros";
            // 
            // miParamConsulta
            // 
            this.miParamConsulta.Name = "miParamConsulta";
            this.miParamConsulta.Size = new System.Drawing.Size(144, 22);
            this.miParamConsulta.Text = "&Consulta";
            this.miParamConsulta.Click += new System.EventHandler(this.miParamConsulta_Click);
            // 
            // miParamManutencao
            // 
            this.miParamManutencao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alterarToolStripMenuItem1,
            this.inserirToolStripMenuItem});
            this.miParamManutencao.Name = "miParamManutencao";
            this.miParamManutencao.Size = new System.Drawing.Size(144, 22);
            this.miParamManutencao.Text = "&Manutenção";
            // 
            // alterarToolStripMenuItem1
            // 
            this.alterarToolStripMenuItem1.Name = "alterarToolStripMenuItem1";
            this.alterarToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.alterarToolStripMenuItem1.Text = "&Alterar";
            this.alterarToolStripMenuItem1.Click += new System.EventHandler(this.alterarToolStripMenuItem1_Click);
            // 
            // inserirToolStripMenuItem
            // 
            this.inserirToolStripMenuItem.Name = "inserirToolStripMenuItem";
            this.inserirToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.inserirToolStripMenuItem.Text = "&Inserir/Alterar Exceção";
            this.inserirToolStripMenuItem.Click += new System.EventHandler(this.inserirToolStripMenuItem_Click);
            // 
            // miCliente
            // 
            this.miCliente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miCliExcecao});
            this.miCliente.Name = "miCliente";
            this.miCliente.Size = new System.Drawing.Size(57, 20);
            this.miCliente.Text = "&Clientes";
            // 
            // miCliExcecao
            // 
            this.miCliExcecao.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultaToolStripMenuItem,
            this.manutencaoToolStripMenuItem});
            this.miCliExcecao.Name = "miCliExcecao";
            this.miCliExcecao.Size = new System.Drawing.Size(172, 22);
            this.miCliExcecao.Text = "&Cadastro Exceção";
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.consultaToolStripMenuItem.Text = "&Consulta";
            this.consultaToolStripMenuItem.Click += new System.EventHandler(this.consultaToolStripMenuItem_Click);
            // 
            // manutencaoToolStripMenuItem
            // 
            this.manutencaoToolStripMenuItem.Name = "manutencaoToolStripMenuItem";
            this.manutencaoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.manutencaoToolStripMenuItem.Text = "&Manutenção";
            this.manutencaoToolStripMenuItem.Click += new System.EventHandler(this.manutencaoToolStripMenuItem_Click);
            // 
            // miVendedor
            // 
            this.miVendedor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miVendContatos,
            this.miVendProdutividade});
            this.miVendedor.Name = "miVendedor";
            this.miVendedor.Size = new System.Drawing.Size(76, 20);
            this.miVendedor.Text = "&Vendedores";
            // 
            // miVendContatos
            // 
            this.miVendContatos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exclusaoDeContatosPendentesToolStripMenuItem});
            this.miVendContatos.Name = "miVendContatos";
            this.miVendContatos.Size = new System.Drawing.Size(151, 22);
            this.miVendContatos.Text = "&Contatos";
            // 
            // exclusaoDeContatosPendentesToolStripMenuItem
            // 
            this.exclusaoDeContatosPendentesToolStripMenuItem.Name = "exclusaoDeContatosPendentesToolStripMenuItem";
            this.exclusaoDeContatosPendentesToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.exclusaoDeContatosPendentesToolStripMenuItem.Text = "&Exclusão de Contatos Pendentes";
            this.exclusaoDeContatosPendentesToolStripMenuItem.Click += new System.EventHandler(this.exclusaoDeContatosPendentesToolStripMenuItem_Click);
            // 
            // miVendProdutividade
            // 
            this.miVendProdutividade.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.roteirosToolStripMenuItem,
            this.listaDeExcedentesToolStripMenuItem,
            this.carteirasIncompletasToolStripMenuItem});
            this.miVendProdutividade.Name = "miVendProdutividade";
            this.miVendProdutividade.Size = new System.Drawing.Size(151, 22);
            this.miVendProdutividade.Text = "&Produtividade";
            // 
            // roteirosToolStripMenuItem
            // 
            this.roteirosToolStripMenuItem.Name = "roteirosToolStripMenuItem";
            this.roteirosToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.roteirosToolStripMenuItem.Text = "&Roteiros";
            this.roteirosToolStripMenuItem.Visible = false;
            this.roteirosToolStripMenuItem.Click += new System.EventHandler(this.roteirosToolStripMenuItem_Click);
            // 
            // listaDeExcedentesToolStripMenuItem
            // 
            this.listaDeExcedentesToolStripMenuItem.Name = "listaDeExcedentesToolStripMenuItem";
            this.listaDeExcedentesToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.listaDeExcedentesToolStripMenuItem.Text = "&Lista de Excedentes";
            this.listaDeExcedentesToolStripMenuItem.Click += new System.EventHandler(this.listaDeExcedentesToolStripMenuItem_Click);
            // 
            // carteirasIncompletasToolStripMenuItem
            // 
            this.carteirasIncompletasToolStripMenuItem.Name = "carteirasIncompletasToolStripMenuItem";
            this.carteirasIncompletasToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.carteirasIncompletasToolStripMenuItem.Text = "&Carteiras Incompletas";
            this.carteirasIncompletasToolStripMenuItem.Click += new System.EventHandler(this.carteirasIncompletasToolStripMenuItem_Click);
            // 
            // miSair
            // 
            this.miSair.Name = "miSair";
            this.miSair.Size = new System.Drawing.Size(37, 20);
            this.miSair.Text = "&Sair";
            this.miSair.Click += new System.EventHandler(this.miSair_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::VDA991.Properties.Resources.Logo_DPK_2015;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(858, 562);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Roteirização";
            this.TransparencyKey = System.Drawing.Color.DarkGray;
            this.Resize += new System.EventHandler(this.FrmPrincipal_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem miQuadrante;
        private System.Windows.Forms.ToolStripMenuItem miQuadConsulta;
        private System.Windows.Forms.ToolStripMenuItem miQuadManutencao;
        private System.Windows.Forms.ToolStripMenuItem miQuadManutAlterar;
        private System.Windows.Forms.ToolStripMenuItem miQuadManutInserir;
        private System.Windows.Forms.ToolStripMenuItem miParametro;
        private System.Windows.Forms.ToolStripMenuItem miParamConsulta;
        private System.Windows.Forms.ToolStripMenuItem miParamManutencao;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inserirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miCliente;
        private System.Windows.Forms.ToolStripMenuItem miCliExcecao;
        private System.Windows.Forms.ToolStripMenuItem miVendedor;
        private System.Windows.Forms.ToolStripMenuItem miVendContatos;
        private System.Windows.Forms.ToolStripMenuItem miVendProdutividade;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manutencaoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exclusaoDeContatosPendentesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roteirosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listaDeExcedentesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miSair;
        private System.Windows.Forms.ToolStripMenuItem carteirasIncompletasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miQuadManutImp;

    }
}