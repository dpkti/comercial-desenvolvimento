﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace VDA991
{
    public partial class FrmConsultaExcecaoCliente : Form
    {
        public FrmConsultaExcecaoCliente()
        {
            InitializeComponent();
            cmbFilial.Focus();
            CarregarComboFilial(cmbFilial);
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            try
            {

                FiltroConsulta oFiltro = new FiltroConsulta();

                if (String.IsNullOrEmpty(txtValorFiltro.Text) && (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1))
                { 
                    string mensagem = "Necessário Preencher Algum Filtro Para a Pesquisa.";
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
            
                }
            
                oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
                oFiltro.INATIVOS = Convert.ToInt32(chkInativos.Checked);

                if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
                {
                    oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
                }
                else
                {
                    oFiltro.CODCOORD = null;
                }

                if (!String.IsNullOrEmpty(txtValorFiltro.Text))
                {
                    if (rbCnpj.Checked) { oFiltro.CNPJ = Convert.ToInt64(txtValorFiltro.Text); }
                    if (rbCdCli.Checked) { oFiltro.CDCLI = Convert.ToInt64(txtValorFiltro.Text); }
                }

                List<RetornoClienteExc> listaDiasCliente = new List<RetornoClienteExc>();
                listaDiasCliente = ClienteBO.BuscarDiasClientesExc(oFiltro);
                dgvCliente.DataSource = listaDiasCliente;

                if (dgvCliente.RowCount == 0)
                {
                    MessageBox.Show("Nenhum Registro Encontrado.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no método btnConsulta_Click.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtValorFiltro_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

                if (rbCnpj.Checked)
                {
                    txtValorFiltro.MaxLength = 14;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                
                }else if(rbCdCli.Checked)
                {
                    txtValorFiltro.MaxLength = 6;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                
                }
                else
                {
                    txtValorFiltro.MaxLength = 50;
                    if (e.KeyChar == (char)Keys.Space)
                    {
                        e.Handled = true;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no método txtValorFiltro_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void rbCnpj_Click(object sender, EventArgs e)
        {
            lblCampoFiltro.Text = "CNPJ:";
            txtValorFiltro.Text = "";
            txtValorFiltro.Focus();
        }

        private void rbCdCli_Click(object sender, EventArgs e)
        {
            lblCampoFiltro.Text = "Código:";
            txtValorFiltro.Text = "";
            txtValorFiltro.Focus();
        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {

            List<FilialBE> listaFilial = new List<FilialBE>();

            FilialBE filial = new FilialBE();
            filial.COD_FILIAL = 0;
            filial.NOME_FILIAL = "Selecione";

            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "NOME_FILIAL";
            cmbFilial.ValueMember = "COD_FILIAL";

            try
            {
                listaFilial = FilialBO.BuscarFilial();
                listaFilial.Insert(0, filial);
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void txtValorFiltro_DoubleClick(object sender, EventArgs e)
        {
            FrmConsultaNomeCliente oFrmConsultaNomeCliente = new FrmConsultaNomeCliente();
            oFrmConsultaNomeCliente.StartPosition = FormStartPosition.CenterParent;
            oFrmConsultaNomeCliente.ShowDialog();


            if (GlobalBE.FiltroRetorno != null)
            {
                if (GlobalBE.FiltroRetorno.CDCLI != 0)
                {

                    if (rbCnpj.Checked)
                    {
                        txtValorFiltro.Text = GlobalBE.FiltroRetorno.CNPJ.ToString();
                    }

                    if (rbCdCli.Checked)
                    {
                        txtValorFiltro.Text = GlobalBE.FiltroRetorno.CDCLI.ToString();
                    }
                }
            }

        }

        private void FrmConsultaExcecaoCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }

        }

        private void dgvCliente_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            FiltroConsulta oFiltroTela = new FiltroConsulta();
            RetornoClienteExc oFiltro = new RetornoClienteExc();
            
            oFiltro.CDCLI = Convert.ToInt32(dgvCliente.CurrentRow.Cells[0].Value);
            oFiltro.DTVIGENCIAINI = Convert.ToDateTime(dgvCliente.CurrentRow.Cells[3].Value);
            oFiltro.DTVIGENCIAFIM = Convert.ToDateTime(dgvCliente.CurrentRow.Cells[4].Value);

            FrmManutencaoExcecaoCliente oFrmManutencaoExcecaoCliente = new FrmManutencaoExcecaoCliente(oFiltro);
            oFrmManutencaoExcecaoCliente.StartPosition = FormStartPosition.CenterParent;
            oFrmManutencaoExcecaoCliente.ShowDialog();

            //Atualiza Grid
            oFiltroTela.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
            oFiltroTela.INATIVOS = Convert.ToInt32(chkInativos.Checked);

            if (!String.IsNullOrEmpty(txtValorFiltro.Text))
            {
                if (rbCnpj.Checked) { oFiltroTela.CNPJ = Convert.ToInt64(txtValorFiltro.Text); }
                if (rbCdCli.Checked) { oFiltroTela.CDCLI = Convert.ToInt64(txtValorFiltro.Text); }
            }

            List<RetornoClienteExc> listaDiasCliente = new List<RetornoClienteExc>();
            listaDiasCliente = ClienteBO.BuscarDiasClientesExc(oFiltroTela);
            dgvCliente.DataSource = listaDiasCliente;


        }

    }
}
