﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;
using UtilGeralDPA;

namespace VDA991
{
    public partial class FrmPrincipal : Form
    {

        public FrmPrincipal()
        {
            InitializeComponent();

            this.Text = String.Format("Roteirização - {0} - {1}.", GlobalBE.usuarioLogado.NOME_USUARIO, GlobalBE.usuarioLogado.ACESSOCOORDENADOR ? "Coordenador":"Supervisor");

            //Acesso ao Menu de Quadrante/Parametros consulta para coordenadores
            miQuadManutencao.Enabled = GlobalBE.usuarioLogado.ACESSOSUPERVISOR;
            miParamManutencao.Enabled = GlobalBE.usuarioLogado.ACESSOSUPERVISOR;

            MdiClient ctlMDI = (MdiClient)this.Controls[this.Controls.Count - 1];
            ctlMDI.BackColor = Color.White;

        }

        private void miQuadConsulta_Click(object sender, EventArgs e)
        {
            Form frmConsultaQuadrante = new FrmConsultaQuadrantes();
            frmConsultaQuadrante.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmConsultaQuadrante.MdiParent = this;
            frmConsultaQuadrante.Show();
        }

        private void miQuadManutAlterar_Click(object sender, EventArgs e)
        {
            Form frmManutencaoQuadrante = new FrmManutencaoQuadrante();
            frmManutencaoQuadrante.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmManutencaoQuadrante.MdiParent = this;
            frmManutencaoQuadrante.Show();
        }

        private void miQuadManutInserir_Click(object sender, EventArgs e)
        {
            Form frmExcecaoQuadrante = new FrmExcecaoQuadrante();
            frmExcecaoQuadrante.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmExcecaoQuadrante.MdiParent = this;
            frmExcecaoQuadrante.Show();
        }

        private void miParamConsulta_Click(object sender, EventArgs e)
        {
            Form frmConsultaParametro = new FrmConsultaParametro();
            frmConsultaParametro.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmConsultaParametro.MdiParent = this;
            frmConsultaParametro.Show();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form frmManutencaoParam = new FrmManuntencaoParametro();
            frmManutencaoParam.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmManutencaoParam.MdiParent = this;
            frmManutencaoParam.Show();
        }

        private void inserirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmExcecaoParam = new FrmExcecaoParametro();
            frmExcecaoParam.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmExcecaoParam.MdiParent = this;
            frmExcecaoParam.Show();
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmConsulaExcecaoCli = new FrmConsultaExcecaoCliente();
            frmConsulaExcecaoCli.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmConsulaExcecaoCli.MdiParent = this;
            frmConsulaExcecaoCli.Show();
        }

        private void manutencaoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            RetornoClienteExc oFiltro = new RetornoClienteExc();
            Form frmManutencaoExceCli = new FrmManutencaoExcecaoCliente(oFiltro);
            frmManutencaoExceCli.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmManutencaoExceCli.MdiParent = this;
            frmManutencaoExceCli.Show();
        }

        private void exclusaoDeContatosPendentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmExclContPendVend = new FrmExclusaoContatosPendenteVend();
            frmExclContPendVend.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmExclContPendVend.MdiParent = this;
            frmExclContPendVend.Show();
        }

        private void listaDeExcedentesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmListExcedente = new FrmListaExcedentes();
            frmListExcedente.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmListExcedente.MdiParent = this;
            frmListExcedente.Show();

        }

        private void carteirasIncompletasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frmCartIncompleta = new FrmCarteirasIncompletas();
            frmCartIncompleta.WindowState = System.Windows.Forms.FormWindowState.Normal;
            frmCartIncompleta.MdiParent = this;
            frmCartIncompleta.Show();
        }


        private void miSair_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Deseja Encerrar a Aplicação?", "Sair", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Environment.Exit(0);
            }

        }

        private void roteirosToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                string VDA610 = ConfigurationAccess.ObterConfiguracao("diretorioVDA610").ToString();

                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                proc.StartInfo.FileName = VDA610;
                proc.Start();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show(String.Format("{0}", "Erro ao abrir o VDA610."), VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
                
            }

        }

        private void FrmPrincipal_Resize(object sender, EventArgs e)
        {
            this.BackgroundImage = VDA991.Properties.Resources.Logo_DPK_2015;
            this.BackgroundImageLayout = ImageLayout.Zoom;
        }

        //TI-4404
        private void miQuadManutImp_Click(object sender, EventArgs e)
        {
            Form FrmImportarQuadrante = new FrmImportarQuadrante();
            FrmImportarQuadrante.WindowState = System.Windows.Forms.FormWindowState.Normal;
            FrmImportarQuadrante.MdiParent = this;
            FrmImportarQuadrante.Show();
        }
        //FIM TI-4404
    }
}
