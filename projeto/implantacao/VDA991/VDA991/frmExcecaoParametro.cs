﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmExcecaoParametro : Form
    {
        public FrmExcecaoParametro()
        {
            InitializeComponent();
            CarregarComboParametro(cmbParametro);
            CarregarComboFilial(cmbFilial);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregarComboParametro(ComboBox cmbParametro)
        {

            List<ParametroBE> listaParametro = new List<ParametroBE>();

            ParametroBE parametro = new ParametroBE();
            parametro.VALOR = "";
            parametro.PARAMETRO = "Selecione";

            cmbParametro.DataSource = null;
            cmbParametro.Items.Clear();
            cmbParametro.DisplayMember = "PARAMETRO";
            cmbParametro.ValueMember = "PARAMETRO";

            try
            {
                listaParametro = ParametroBO.BuscarParametro();
                listaParametro.Insert(0, parametro);
                cmbParametro.DataSource = listaParametro;
                cmbParametro.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboParametro, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {

            List<FilialBE> listaFilial = new List<FilialBE>();

            FilialBE filial = new FilialBE();
            filial.COD_FILIAL = 0;
            filial.NOME_FILIAL = "Selecione";

            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "NOME_FILIAL";
            cmbFilial.ValueMember = "COD_FILIAL";

            try
            {
                listaFilial = FilialBO.BuscarFilial();
                listaFilial.Insert(0, filial);
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

/*        private void txtQdtContato_Leave(object sender, EventArgs e)
        {
            try
            {
                double valor = 0.0d;
                if (Double.TryParse(txtQtdContato.Text, NumberStyles.Currency, null, out valor))
                {
                    txtQtdContato.Text = valor.ToString("");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtQtdContato_Leave.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }*/

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {

            try
            {

                if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.NUMERICO)
                {
                    txtValor.MaxLength = 2;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.NUMERICO3)
                {
                    txtValor.MaxLength = 3;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.DECIMAL)
                {
                    txtValor.MaxLength = 6;
                    if (e.KeyChar == '.' || e.KeyChar == ',')
                    {
                        e.KeyChar = ',';
                        if (txtValor.Text.Contains(","))
                            e.Handled = true;
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.DECIMAL12)
                {
                    txtValor.MaxLength = 12;
                    if (e.KeyChar == '.' || e.KeyChar == ',')
                    {
                        e.KeyChar = ',';
                        if (txtValor.Text.Contains(","))
                            e.Handled = true;
                    }
                    else if (e.KeyChar == '-') //TI-6036
                    {
                        e.KeyChar = '-';
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else
                {
                    txtValor.MaxLength = 100;
                    if (e.KeyChar == ';')
                    {
                        e.KeyChar = ';';
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtValor_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void btnInserirAlterar_Click(object sender, EventArgs e)
        {

            ParametroEXC oFiltro = new ParametroEXC();
            ParametroGeral oFiltroGeral = new ParametroGeral();

            try
            {
                
                if (cmbParametro.SelectedIndex == 0 || cmbParametro.SelectedIndex == -1)
                {
                    string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Parâmetro");
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else if (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1)
                {
                    string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Filial");
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {

                    if (String.IsNullOrEmpty(txtValor.Text))
                    {
                        string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Quantidade Contato");
                        MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;

                    }
                    else
                    {

                        oFiltro.PARAMETRO = cmbParametro.Text;
                        oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
                        oFiltro.VALOR = txtValor.Text;

                        oFiltroGeral.PARAMETRO = cmbParametro.Text;
                        oFiltroGeral.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
                        oFiltroGeral.VALOR = txtValor.Text;

                        if (ParametroBO.ValidaValores(oFiltroGeral) == true)
                        {

                            ParametroBO.InserirAlterarParametroExcecao(oFiltro);

                            if (btnInserirAlterar.Text == "Inserir")
                            {
                                MessageBox.Show("Parâmetro Exceção Incluído com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Parâmetro Exceção Alterado com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            

                            CarregarComboParametro(cmbParametro);
                            CarregarComboFilial(cmbFilial);
                        }
                        else
                        {
                            string mensagem = ParametroBO.ValidaMensagens(oFiltroGeral);
                            MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                            
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao salvar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }

        private void ValidaCampos()
        {
            ParametroEXC oFiltro = new ParametroEXC();
            List<ParametroEXC> listaParametroFilial = new List<ParametroEXC>();

            if ((cmbParametro.SelectedIndex == 0 || cmbParametro.SelectedIndex == -1) ||
                (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1))
            {
                btnInserirAlterar.Text = "       Inserir/Alterar";
                txtValor.Text = "";
                txtValor.Enabled = false;
            }
            else
            {
                txtValor.Text = "";
                txtValor.Enabled = true;

                oFiltro.PARAMETRO = cmbParametro.Text;
                oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);

                listaParametroFilial = ParametroBO.BuscarParametroFilialExcecao(oFiltro);

                if (listaParametroFilial.Count == 0)
                {
                    btnInserirAlterar.Text = "Inserir";
                }
                else
                {
                    btnInserirAlterar.Text = "Alterar";
                    txtValor.Text = listaParametroFilial[0].VALOR.ToString();
                }


            }

        }

        private void cmbFilial_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidaCampos();
        }

        private void cmbParametro_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidaCampos();
        }


    }
}
