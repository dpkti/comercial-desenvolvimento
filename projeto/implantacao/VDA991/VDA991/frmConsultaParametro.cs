﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmConsultaParametro : Form
    {
        public FrmConsultaParametro()
        {
            InitializeComponent();
            CarregarParametros();

        }

        private void dgvParametro_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                switch (e.Button)
                {
                    case MouseButtons.Right:

                        ContextMenu myContextMenu = new ContextMenu();

                        MenuItem menuItem0 = new MenuItem("Consultar Exceções");

                        myContextMenu.MenuItems.Add(menuItem0);

                        if (dgvParametro.SelectedRows.Count == 1)
                        {
                            myContextMenu.MenuItems.Add(menuItem0);

                            menuItem0.Click += new EventHandler(delegate(Object o, EventArgs a)
                            {

                                FiltroParametro oFiltro = new FiltroParametro();

                                oFiltro.PARAMETRO = dgvParametro.CurrentRow.Cells[0].Value.ToString();

                                if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
                                {
                                    oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
                                }
                                else
                                {
                                    oFiltro.CODCOORD = null;
                                   
                                }

                                FrmConsultaParametroExcecao oFrmConsultaParametroExcecao = new FrmConsultaParametroExcecao(oFiltro);
                                oFrmConsultaParametroExcecao.StartPosition = FormStartPosition.CenterParent;
                                oFrmConsultaParametroExcecao.ShowDialog();

                            });
                        }


                        myContextMenu.Show(dgvParametro, e.Location, LeftRightAlignment.Right);

                        break;
                }

            }
            catch (Exception ex)
            {
            }

        }

        private void CarregarParametros()
        {
            List<ParametroBE> listaParametro = new List<ParametroBE>();
            listaParametro = ParametroBO.BuscarParametro();
            dgvParametro.DataSource = listaParametro;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    
    }
}
