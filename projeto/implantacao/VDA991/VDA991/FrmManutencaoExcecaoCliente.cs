﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmManutencaoExcecaoCliente : Form
    {
        FiltroUpCliente oFiltroCli = new FiltroUpCliente();

        public FrmManutencaoExcecaoCliente(RetornoClienteExc oFiltro)
        {
            InitializeComponent();
            LimpaCampos();
            btnInserirAlterar.Text = "Inserir";
            dtpDataInicio.Enabled = true;

            //Busca pela tela de consulta
            if (oFiltro.CDCLI != 0)
            {
                btnInserirAlterar.Text = "Alterar";
                txtCodCli.Text = oFiltro.CDCLI.ToString();
                dtpDataInicio.Value = oFiltro.DTVIGENCIAINI;
                if (oFiltro.DTVIGENCIAINI <= DateTime.Now)
                {
                    dtpDataInicio.Enabled = false;
                }
                


                dtpDataFim.Value = oFiltro.DTVIGENCIAFIM;
                
                oFiltroCli.CDCLI = Convert.ToInt64(txtCodCli.Text);
                oFiltroCli.DTVIGENCIAINI_ORIG = oFiltro.DTVIGENCIAINI;
                oFiltroCli.DTVIGENCIAFIM_ORIG = oFiltro.DTVIGENCIAFIM;
                
                lblNomeCli.Text = ClienteBO.BuscarNomeCliente(oFiltroCli);
                ValidaCampos();

            }
            
        }

        private void txtCodCli_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCodCli_Leave(object sender, EventArgs e)
        {
            
            if (!String.IsNullOrEmpty(txtCodCli.Text))
            {
                FiltroUpCliente oFiltro = new FiltroUpCliente();
                oFiltro.CDCLI = Convert.ToInt64(txtCodCli.Text);
                lblNomeCli.Text = ClienteBO.BuscarNomeCliente(oFiltro);
                //ValidaCampos();
            }
        }

        private void btnInserirAlterar_Click(object sender, EventArgs e)
        {
            try
            {

                FiltroCli oFiltro = new FiltroCli();

                if (String.IsNullOrEmpty(txtCodCli.Text))
                {
                    string mensagem = String.Format(ResourceRoteirizacao.Erro_ClienteNaoEncontrado, "Código Cliente");
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;

                }

                if (String.IsNullOrEmpty(lblNomeCli.Text))
                {
                    MessageBox.Show(ResourceRoteirizacao.Erro_ClienteNaoEncontrado, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error); 
                    return;
                }

                oFiltro.CDCLI = Convert.ToInt64(txtCodCli.Text);
                oFiltro.TIPO_USER = 0;

                if (GlobalBE.usuarioLogado.ACESSOSUPERVISOR == true)
                {
                    oFiltro.TIPO_USER = 1;
                }
                else
                {
                    oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
                }


                if (ClienteBO.ValidaCliente(oFiltro) == false)
                {
                    MessageBox.Show(String.Format("Cliente não pertence a Filial do Coordenador."), VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                

                if (dtpDataInicio.Checked == false && dtpDataFim.Checked == true ||
                   dtpDataInicio.Checked == true && dtpDataFim.Checked == false)
                {
                    MessageBox.Show("Para os filtros de data serem válidos, é necessario marcar Data Início e Data Fim.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (dtpDataInicio.Value.Date > dtpDataFim.Value.Date)
                {
                    MessageBox.Show("Data Fim deve ser maior que a Data Início.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (String.IsNullOrEmpty(txtMotivo.Text))
                {
                    MessageBox.Show("O motivo deve ser informado para o cadastro dessa exceção.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                
                //oFiltro.CDCLI = Convert.ToInt64(txtCodCli.Text);
                //oFiltro.DTVIGENCIAINI = dtpDataInicio.Value;
                //oFiltro.DTVIGENCIAFIM = dtpDataFim.Value;

                oFiltroCli.CDCLI = Convert.ToInt64(txtCodCli.Text);
                oFiltroCli.DTVIGENCIAINI = dtpDataInicio.Value;
                oFiltroCli.DTVIGENCIAFIM = dtpDataFim.Value;

                oFiltroCli.SEGUNDA1 = Convert.ToInt32(chkS1.Checked);
                oFiltroCli.SEGUNDA2 = Convert.ToInt32(chkS2.Checked);
                oFiltroCli.SEGUNDA3 = Convert.ToInt32(chkS3.Checked);
                oFiltroCli.SEGUNDA4 = Convert.ToInt32(chkS4.Checked);
                oFiltroCli.SEGUNDA5 = Convert.ToInt32(chkS5.Checked);

                oFiltroCli.TERCA1 = Convert.ToInt32(chkTE1.Checked);
                oFiltroCli.TERCA2 = Convert.ToInt32(chkTE2.Checked);
                oFiltroCli.TERCA3 = Convert.ToInt32(chkTE3.Checked);
                oFiltroCli.TERCA4 = Convert.ToInt32(chkTE4.Checked);
                oFiltroCli.TERCA5 = Convert.ToInt32(chkTE5.Checked);

                oFiltroCli.QUARTA1 = Convert.ToInt32(chkQA1.Checked);
                oFiltroCli.QUARTA2 = Convert.ToInt32(chkQA2.Checked);
                oFiltroCli.QUARTA3 = Convert.ToInt32(chkQA3.Checked);
                oFiltroCli.QUARTA4 = Convert.ToInt32(chkQA4.Checked);
                oFiltroCli.QUARTA5 = Convert.ToInt32(chkQA5.Checked);

                oFiltroCli.QUINTA1 = Convert.ToInt32(chkQI1.Checked);
                oFiltroCli.QUINTA2 = Convert.ToInt32(chkQI2.Checked);
                oFiltroCli.QUINTA3 = Convert.ToInt32(chkQI3.Checked);
                oFiltroCli.QUINTA4 = Convert.ToInt32(chkQI4.Checked);
                oFiltroCli.QUINTA5 = Convert.ToInt32(chkQI5.Checked);

                oFiltroCli.SEXTA1 = Convert.ToInt32(chkSE1.Checked);
                oFiltroCli.SEXTA2 = Convert.ToInt32(chkSE2.Checked);
                oFiltroCli.SEXTA3 = Convert.ToInt32(chkSE3.Checked);
                oFiltroCli.SEXTA4 = Convert.ToInt32(chkSE4.Checked);
                oFiltroCli.SEXTA5 = Convert.ToInt32(chkSE5.Checked);

                oFiltroCli.MOTIVO = txtMotivo.Text;

                //if (btnInserirAlterar.Text == "Inserir")
               // {
                //    oFiltroCli.TIPO = 1;
               // }
               // else
               // { 
               //     oFiltroCli.TIPO = 0;
                
               // }

                //if (ClienteBO.ValidaPeriodo(oFiltroCli) == false)
                //{

                if (btnInserirAlterar.Text == "Inserir")
                {
                    if (dtpDataInicio.Value.Date <= DateTime.Now.Date)
                    {
                        MessageBox.Show("Data Inicial deve ser maior que a Data de Atual.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    FiltroCliente oFiltroPeriodo = new FiltroCliente();
                    oFiltroPeriodo.CDCLI = oFiltroCli.CDCLI;
                    oFiltroPeriodo.DTVIGENCIAINI = oFiltroCli.DTVIGENCIAINI;
                    oFiltroPeriodo.DTVIGENCIAFIM = oFiltroCli.DTVIGENCIAFIM;
                    oFiltroPeriodo.DTVIGENCIAINI_ORIG = oFiltroCli.DTVIGENCIAINI_ORIG;
                    oFiltroPeriodo.DTVIGENCIAFIM_ORIG = oFiltroCli.DTVIGENCIAFIM_ORIG;
                    oFiltroPeriodo.TIPO = 1;
                                        
                    if (ClienteBO.ValidaPeriodo(oFiltroPeriodo) == true)
                    {
                        MessageBox.Show(ResourceRoteirizacao.Erro_PeriodoCadastrado, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (ClienteBO.ValidaExcecaoAtiva(oFiltroPeriodo) == true)
                    {
                        MessageBox.Show("Já existe uma exceção ativa para este cliente, Favor verificar.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    ClienteBO.InserirClienteExcecao(oFiltroCli);
                }
                else
                {
                    if (dtpDataInicio.Enabled == true && dtpDataInicio.Value.Date < DateTime.Now.Date)
                    {
                        MessageBox.Show("Data Inicial não deve ser anterior a Data de Atual.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (dtpDataFim.Value.Date < DateTime.Now.Date)
                    {
                        MessageBox.Show("Data Final não deve ser anterior a Data de Atual.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    if (oFiltroCli.DTVIGENCIAINI == DateTime.Now.Date &&
                        oFiltroCli.DTVIGENCIAFIM == DateTime.Now.Date)
                    {
                        ClienteBO.DeletaClienteExcecao(oFiltroCli);
                    }
                    else
                    {
                        ClienteBO.AlterarClienteExcecao(oFiltroCli);
                    }
                }

                //}
                //else
               // {
               //     MessageBox.Show(ResourceRoteirizacao.Erro_PeriodoCadastrado, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
               //     return;
               // }


                if (btnInserirAlterar.Text == "Inserir")
                {
                    MessageBox.Show("Cliente Exceção Incluído com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LimpaCampos();
                    txtCodCli.Focus();
                }
                else 
                {
                    MessageBox.Show("Cliente Exceção Alterado com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao salvar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }

        private void ValidaCampos()
        {
            FiltroCliente oFiltro = new FiltroCliente();
            List<ClienteBE> listaFiltroCliente = new List<ClienteBE>();

            txtCodCli.Enabled = false;
            btnImportarExcel.Visible = false;

            oFiltro.CDCLI = Convert.ToInt64(txtCodCli.Text);
            oFiltro.DTVIGENCIAINI = dtpDataInicio.Value;
            oFiltro.DTVIGENCIAFIM = dtpDataFim.Value;

            listaFiltroCliente = ClienteBO.BuscarClienteExcecao(oFiltro);

            if (listaFiltroCliente.Count == 0)
            {
                btnInserirAlterar.Text = "Inserir";
            }
            else
            {
                btnInserirAlterar.Text = "Alterar";

                chkS1.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEGUNDA1);
                chkS2.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEGUNDA2);
                chkS3.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEGUNDA3);
                chkS4.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEGUNDA4);
                chkS5.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEGUNDA5);
                chkTE1.Checked = Convert.ToBoolean(listaFiltroCliente[0].TERCA1);
                chkTE2.Checked = Convert.ToBoolean(listaFiltroCliente[0].TERCA2);
                chkTE3.Checked = Convert.ToBoolean(listaFiltroCliente[0].TERCA3);
                chkTE4.Checked = Convert.ToBoolean(listaFiltroCliente[0].TERCA4);
                chkTE5.Checked = Convert.ToBoolean(listaFiltroCliente[0].TERCA5);
                chkQA1.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUARTA1);
                chkQA2.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUARTA2);
                chkQA3.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUARTA3);
                chkQA4.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUARTA4);
                chkQA5.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUARTA5);
                chkQI1.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUINTA1);
                chkQI2.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUINTA2);
                chkQI3.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUINTA3);
                chkQI4.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUINTA4);
                chkQI5.Checked = Convert.ToBoolean(listaFiltroCliente[0].QUINTA5);
                chkSE1.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEXTA1);
                chkSE2.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEXTA2);
                chkSE3.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEXTA3);
                chkSE4.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEXTA4);
                chkSE5.Checked = Convert.ToBoolean(listaFiltroCliente[0].SEXTA5);

                txtMotivo.Text = listaFiltroCliente[0].MOTIVO;

                //Bloqueia Campos
                if (listaFiltroCliente[0].DTVIGENCIAFIM <= DateTime.Today)
                {
                    
                    dtpDataInicio.Enabled = false;
                    dtpDataFim.Enabled = false;
                    chkS1.Enabled = false;
                    chkS2.Enabled = false;
                    chkS3.Enabled = false;
                    chkS4.Enabled = false;
                    chkS5.Enabled = false;
                    chkTE1.Enabled = false;
                    chkTE2.Enabled = false;
                    chkTE3.Enabled = false;
                    chkTE4.Enabled = false;
                    chkTE5.Enabled = false;
                    chkQA1.Enabled = false;
                    chkQA2.Enabled = false;
                    chkQA3.Enabled = false;
                    chkQA4.Enabled = false;
                    chkQA5.Enabled = false;
                    chkQI1.Enabled = false;
                    chkQI2.Enabled = false;
                    chkQI3.Enabled = false;
                    chkQI4.Enabled = false;
                    chkQI5.Enabled = false;
                    chkSE1.Enabled = false;
                    chkSE2.Enabled = false;
                    chkSE3.Enabled = false;
                    chkSE4.Enabled = false;
                    chkSE5.Enabled = false;
                    txtMotivo.Enabled = false;

                    btnInserirAlterar.Visible = false;
                }
                else if (DateTime.Today >= listaFiltroCliente[0].DTVIGENCIAFIM && DateTime.Today <= listaFiltroCliente[0].DTVIGENCIAFIM)
                {

                    dtpDataInicio.Enabled = false;

                }

            }

        }

        private void LimpaCampos()
        {
            lblNomeCli.Text = "";
            lblNomeCli.Visible = true;
            txtCodCli.Text = "";
            dtpDataInicio.Value = DateTime.Now;
            dtpDataFim.Value = DateTime.Now;
            chkS1.Checked = false;
            chkS2.Checked = false;
            chkS3.Checked = false;
            chkS4.Checked = false;
            chkS5.Checked = false;
            chkTE1.Checked = false;
            chkTE2.Checked = false;
            chkTE3.Checked = false;
            chkTE4.Checked = false;
            chkTE5.Checked = false;
            chkQA1.Checked = false;
            chkQA2.Checked = false;
            chkQA3.Checked = false;
            chkQA4.Checked = false;
            chkQA5.Checked = false;
            chkQI1.Checked = false;
            chkQI2.Checked = false;
            chkQI3.Checked = false;
            chkQI4.Checked = false;
            chkQI5.Checked = false;
            chkSE1.Checked = false;
            chkSE2.Checked = false;
            chkSE3.Checked = false;
            chkSE4.Checked = false;
            chkSE5.Checked = false;

            txtMotivo.Text = string.Empty;
            
        }

        private void txtCodCli_DoubleClick(object sender, EventArgs e)
        {
            FrmConsultaNomeCliente oFrmConsultaNomeCliente = new FrmConsultaNomeCliente();
            oFrmConsultaNomeCliente.StartPosition = FormStartPosition.CenterParent;
            oFrmConsultaNomeCliente.ShowDialog();

            if (GlobalBE.FiltroRetorno != null)
            {
                if (GlobalBE.FiltroRetorno.CDCLI != 0)
                {
                    txtCodCli.Text = "";
                    txtCodCli.Text = GlobalBE.FiltroRetorno.CDCLI.ToString();
                    lblNomeCli.Text = GlobalBE.FiltroRetorno.NOMECLI.ToString();

                    //ValidaCampos();
                }
            }
        }

        private void btnImportarExcel_Click(object sender, EventArgs e)
        {
            frmImportarExcel ofrmImportarExcel = new frmImportarExcel();
            ofrmImportarExcel.StartPosition = FormStartPosition.CenterScreen;
            ofrmImportarExcel.ShowDialog();
        }

        private void FrmManutencaoExcecaoCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }

        }



    }
}
