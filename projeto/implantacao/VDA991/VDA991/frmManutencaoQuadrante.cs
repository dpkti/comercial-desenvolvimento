﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmManutencaoQuadrante : Form
    {
        public FrmManutencaoQuadrante()
        {
            InitializeComponent();
            CarregarComboQuadrante(cmbQuadrante);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregarComboQuadrante(ComboBox cmbQuadrante)
        {
            
            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();
            
            QuadranteBE quadrante = new QuadranteBE();
            quadrante.QTDCONTATO = 0;
            quadrante.QUADRANTE = "Selecione";

            cmbQuadrante.DataSource = null;
            cmbQuadrante.Items.Clear();
            cmbQuadrante.DisplayMember = "QUADRANTE";
            cmbQuadrante.ValueMember = "QUADRANTE";

            try
            {
                listaQuadrante = QuadranteBO.BuscarQuadrante();
                listaQuadrante.Insert(0, quadrante);
                cmbQuadrante.DataSource = listaQuadrante;
                cmbQuadrante.SelectedIndex = 0;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboQuadrante, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cmbQuadrante_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbQuadrante.SelectedIndex == 0 || cmbQuadrante.SelectedIndex == -1)
            {
                txtQtdContato.Text = "";
                txtQtdContato.Enabled = false;
            }
            else
            {
                txtQtdContato.Text = ((QuadranteBE)cmbQuadrante.SelectedItem).QTDCONTATO.ToString();
                txtQtdContato.Enabled = true;
            }
        }

        private void txtQtdContato_Leave(object sender, EventArgs e)
        {
            try
            {
                double valor = 0.0d;
                if (Double.TryParse(txtQtdContato.Text, NumberStyles.Currency, null, out valor))
                {
                    txtQtdContato.Text = valor.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtQtdContato_Leave.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void txtQtdContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == '.' || e.KeyChar == ',')
                {
                    e.KeyChar = ',';
                    if (txtQtdContato.Text.Contains(","))
                        e.Handled = true;
                }
                else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtQtdContato_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            //string valorcampo;
            //valorcampo = txtQtdContato.Text;
            
            try
            {
                if (QuadranteBO.ValidaValoresQuadrante(txtQtdContato.Text) == true)
                {

                    QuadranteBE oFiltro = new QuadranteBE();
                    if (cmbQuadrante.SelectedIndex == 0 || cmbQuadrante.SelectedIndex == -1)
                    {
                        string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Quadrante");
                        MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(txtQtdContato.Text))
                        {
                            string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Quantidade Contato");
                            MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        
                        }
                        else
                        {
                            oFiltro.QUADRANTE = ((QuadranteBE)cmbQuadrante.SelectedItem).QUADRANTE;
                            oFiltro.QTDCONTATO = string.IsNullOrEmpty(txtQtdContato.Text) ? 0 : Convert.ToDouble(txtQtdContato.Text);

                            QuadranteBO.AlterarQuadrante(oFiltro);
                            MessageBox.Show("Quadrante alterado com sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            CarregarComboQuadrante(cmbQuadrante);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(String.Format("Qtde Contato deve estar entre (0 - 0.25 - 0.50 - 1 a 5)"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao salvar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }



        }

    }
}
