﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Entities;
using Business;
using System.Diagnostics;

namespace VDA991
{
    public partial class frmImportarExcel : Form
    {
        private bool erro;
        private int qtdInserido;

        public frmImportarExcel()
        {
            InitializeComponent();
            lbQtdInserido.Visible = false;
            lbQtdInseridoAlt.Visible = false;
            txtCaminhoArquivo.Enabled = true;
        }

        private void bgWorkerRoteirizacao_DoWork(object sender, DoWorkEventArgs e)
        {
            erro = false;
            string arqErro = string.Empty;
            List<ClienteBE> listaClienteExc;
            string retLogGeraArq = string.Empty;
            int qtdClienteExc;

            try
            {
                listaClienteExc = RoteirizacaoBO.ValidarArquivoExcel(txtCaminhoArquivo.Text, bgWorkerRoteirizacao, out retLogGeraArq);
                if (!retLogGeraArq.Equals(string.Empty))
                {
                    erro = true;

                    MessageBox.Show("Ocorreram problemas durante a inserção.", "Roteirização - Cliente Exceção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.Start("notepad.exe", retLogGeraArq);
                }
                else
                {
                    bool sucessoCSV = RoteirizacaoBO.GerarArquivoCSV(bgWorkerRoteirizacao, listaClienteExc);
                    bool sucessoInsercaoClienteExc = RoteirizacaoBO.InserirClienteExcecao(bgWorkerRoteirizacao, out qtdClienteExc);

                    if (sucessoCSV && sucessoInsercaoClienteExc)
                    {
                        qtdInserido = qtdClienteExc;
                        MessageBox.Show("O arquivo foi processado com sucesso!", "Roteirização", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        erro = true;
                        MessageBox.Show("Falha ao processar o arquivo.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                this.erro = true;
            }
        }

        private void bgWorkerRoteirizacao_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch ((EnumAplicacao.StatusClienteExcecao)e.UserState)
            {
                case EnumAplicacao.StatusClienteExcecao.InicioValidacaoArquivo:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Validando Arquivo Excel...";
                    break;
                case EnumAplicacao.StatusClienteExcecao.FimValidacaoArquivo:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Validação Arquivo Excel.";
                    break;
                case EnumAplicacao.StatusClienteExcecao.InicioGeracaoCSV:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Gerando Arquivo CSV...";
                    break;
                case EnumAplicacao.StatusClienteExcecao.FimGeracaoCSV:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Geração Arquivo CSV.";
                    break;
                case EnumAplicacao.StatusClienteExcecao.InicioInsercaoClienteExcecao:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Inserindo Tabela ROTEIRIZACAO_CLIENTE_EXCECAO...";
                    break;
                case EnumAplicacao.StatusClienteExcecao.FimInsercaoClienteExcecao:
                    lbStatus.ForeColor = Color.Blue;
                    lbStatus.Text = "Fim Inserção Tabela ROTEIRIZACAO_CLIENTE_EXCECAO.";
                    break;
                case EnumAplicacao.StatusClienteExcecao.Erro:
                    this.erro = true;
                    break;
            }
        }

        private void bgWorkerRoteirizacao_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (this.erro)
            {
                lbStatus.Visible = true;
                lbStatusProc.Visible = true;
                lbQtdInserido.Visible = false;
                lbQtdInseridoAlt.Visible = false;
                lbStatus.Text = "Erro no processamento.";
                lbStatus.ForeColor = Color.Red;
            }
            else
            {
                lbStatus.Visible = false;
                lbStatusProc.Visible = false;
                lbQtdInseridoAlt.Text = qtdInserido.ToString();
                lbQtdInserido.Visible = true;
                lbQtdInseridoAlt.Visible = true;
            }

            btnAbrir.Enabled = true;
            btnImportar.Enabled = true;
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            this.oFDPlanilha.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";
            this.oFDPlanilha.ShowDialog();
            this.txtCaminhoArquivo.Text = this.oFDPlanilha.FileName;

        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            if (File.Exists(this.txtCaminhoArquivo.Text))
            {
                btnAbrir.Enabled = false;
                btnImportar.Enabled = false;
                lbStatus.Visible = true;
                lbStatusProc.Visible = true;
                btnImportar.Enabled = false;
                btnAbrir.Enabled = false;
                bgWorkerRoteirizacao.RunWorkerAsync();
            }
            else
            {
                MessageBox.Show(ResourceRoteirizacao.Erro_ArquivoInvalido, ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }
    }
}
