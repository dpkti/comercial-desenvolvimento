﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmManuntencaoParametro : Form
    {
        public FrmManuntencaoParametro()
        {
            InitializeComponent();
            CarregarComboParametro(cmbParametro);

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            ParametroBE oFiltro = new ParametroBE();
            ParametroGeral oFiltroGeral = new ParametroGeral();

            try
            {
                if (cmbParametro.SelectedIndex == 0 || cmbParametro.SelectedIndex == -1)
                {
                    string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Parâmetro");
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {

                    if (String.IsNullOrEmpty(txtDescricao.Text))
                    {
                        string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Descrição");
                        MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;

                    }
                    else
                    {


                        if (String.IsNullOrEmpty(txtValor.Text))
                        {
                            string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Valor");
                            MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;

                        }
                        else
                        {
                            oFiltro.PARAMETRO = ((ParametroBE)cmbParametro.SelectedItem).PARAMETRO;
                            oFiltro.DESCRICAO = txtDescricao.Text;
                            oFiltro.VALOR = txtValor.Text;

                            oFiltroGeral.PARAMETRO = ((ParametroBE)cmbParametro.SelectedItem).PARAMETRO;
                            oFiltroGeral.DESCRICAO = txtDescricao.Text;
                            oFiltroGeral.VALOR = txtValor.Text;

                            if (ParametroBO.ValidaValores(oFiltroGeral) == true)
                            {

                                ParametroBO.AlterarParametro(oFiltro);
                                MessageBox.Show("Parâmetro alterado com sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                CarregarComboParametro(cmbParametro);
                            }
                            else
                            { 
                                string mensagem = ParametroBO.ValidaMensagens(oFiltroGeral);
                                MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao salvar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }

        private void CarregarComboParametro(ComboBox cmbParametro)
        {
            List<ParametroBE> listaParametro = new List<ParametroBE>();

            ParametroBE parametro = new ParametroBE();
            
            parametro.DESCRICAO = "";
            parametro.VALOR = "";
            parametro.PARAMETRO = "Selecione";

            cmbParametro.DataSource = null;
            cmbParametro.Items.Clear();
            cmbParametro.DisplayMember = "PARAMETRO";
            cmbParametro.ValueMember = "PARAMETRO";

            try
            {
                listaParametro = ParametroBO.BuscarParametro();
                listaParametro.Insert(0, parametro);
                cmbParametro.DataSource = listaParametro;
                cmbParametro.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboParametro, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }


        private void cmbParametro_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbParametro.SelectedIndex == 0 || cmbParametro.SelectedIndex == -1)
            {
                txtDescricao.Text = "";
                txtDescricao.Enabled = false;
                txtValor.Text = "";
                txtValor.Enabled = false;

            }
            else
            {
                txtDescricao.Text =  ((ParametroBE)cmbParametro.SelectedItem).DESCRICAO;
                txtDescricao.Enabled = true;
                txtValor.Text = ((ParametroBE)cmbParametro.SelectedItem).VALOR;
                txtValor.Enabled = true;
            }

        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

                if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.NUMERICO)
                {
                    txtValor.MaxLength = 2;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.NUMERICO3)
                {
                    txtValor.MaxLength = 3;
                    if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.DECIMAL)
                {
                    txtValor.MaxLength = 6;
                    if (e.KeyChar == '.' || e.KeyChar == ',')
                    {
                        e.KeyChar = ',';
                        if (txtValor.Text.Contains(","))
                            e.Handled = true;
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else if (ParametroBO.ValidaKeyPressValor(cmbParametro.Text) == EnumAplicacao.TipoCampo.DECIMAL12)
                {
                    txtValor.MaxLength = 12;
                    if (e.KeyChar == '.' || e.KeyChar == ',')
                    {
                        e.KeyChar = ',';
                        if (txtValor.Text.Contains(","))
                            e.Handled = true;
                    }
                    else if (e.KeyChar == '-') //TI-6036
                    {
                        e.KeyChar = '-';
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
                else
                {
                    txtValor.MaxLength = 100;
                    if (e.KeyChar == ';')
                    {
                        e.KeyChar = ';';
                    }
                    else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtValor_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }
    
    }
}
