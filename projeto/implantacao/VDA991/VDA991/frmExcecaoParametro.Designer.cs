﻿namespace VDA991
{
    partial class FrmExcecaoParametro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbExcecaoParam = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnInserirAlterar = new System.Windows.Forms.Button();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.lblFilial = new System.Windows.Forms.Label();
            this.cmbParametro = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gbExcecaoParam.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbExcecaoParam
            // 
            this.gbExcecaoParam.Controls.Add(this.btnCancelar);
            this.gbExcecaoParam.Controls.Add(this.btnInserirAlterar);
            this.gbExcecaoParam.Controls.Add(this.txtValor);
            this.gbExcecaoParam.Controls.Add(this.lblValor);
            this.gbExcecaoParam.Controls.Add(this.cmbFilial);
            this.gbExcecaoParam.Controls.Add(this.lblFilial);
            this.gbExcecaoParam.Controls.Add(this.cmbParametro);
            this.gbExcecaoParam.Controls.Add(this.label1);
            this.gbExcecaoParam.Location = new System.Drawing.Point(12, 12);
            this.gbExcecaoParam.Name = "gbExcecaoParam";
            this.gbExcecaoParam.Size = new System.Drawing.Size(326, 155);
            this.gbExcecaoParam.TabIndex = 0;
            this.gbExcecaoParam.TabStop = false;
            this.gbExcecaoParam.Text = "Inserir/Alterar Exceção";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::VDA991.Properties.Resources.cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(203, 110);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(112, 35);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnInserirAlterar
            // 
            this.btnInserirAlterar.Image = global::VDA991.Properties.Resources.ok;
            this.btnInserirAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInserirAlterar.Location = new System.Drawing.Point(73, 110);
            this.btnInserirAlterar.Name = "btnInserirAlterar";
            this.btnInserirAlterar.Size = new System.Drawing.Size(112, 35);
            this.btnInserirAlterar.TabIndex = 6;
            this.btnInserirAlterar.Text = "       Inserir/Alterar";
            this.btnInserirAlterar.UseVisualStyleBackColor = true;
            this.btnInserirAlterar.Click += new System.EventHandler(this.btnInserirAlterar_Click);
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(73, 73);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(121, 20);
            this.txtValor.TabIndex = 5;
            this.txtValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValor_KeyPress);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Location = new System.Drawing.Point(9, 76);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(34, 13);
            this.lblValor.TabIndex = 4;
            this.lblValor.Text = "Valor:";
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(73, 46);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(242, 21);
            this.cmbFilial.TabIndex = 3;
            this.cmbFilial.SelectedIndexChanged += new System.EventHandler(this.cmbFilial_SelectedIndexChanged);
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Location = new System.Drawing.Point(9, 49);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(30, 13);
            this.lblFilial.TabIndex = 2;
            this.lblFilial.Text = "Filial:";
            // 
            // cmbParametro
            // 
            this.cmbParametro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbParametro.FormattingEnabled = true;
            this.cmbParametro.Location = new System.Drawing.Point(73, 19);
            this.cmbParametro.Name = "cmbParametro";
            this.cmbParametro.Size = new System.Drawing.Size(242, 21);
            this.cmbParametro.TabIndex = 1;
            this.cmbParametro.SelectedIndexChanged += new System.EventHandler(this.cmbParametro_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Parâmetro:";
            // 
            // FrmExcecaoParametro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(354, 181);
            this.Controls.Add(this.gbExcecaoParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmExcecaoParametro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar Exceção Parâmetro";
            this.gbExcecaoParam.ResumeLayout(false);
            this.gbExcecaoParam.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbExcecaoParam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbParametro;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label lblFilial;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnInserirAlterar;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblValor;
    }
}