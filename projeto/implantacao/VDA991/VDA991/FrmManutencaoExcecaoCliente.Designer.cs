﻿namespace VDA991
{
    partial class FrmManutencaoExcecaoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbManutExcCli = new System.Windows.Forms.GroupBox();
            this.dtpDataFim = new System.Windows.Forms.DateTimePicker();
            this.dtpDataInicio = new System.Windows.Forms.DateTimePicker();
            this.chkSE5 = new System.Windows.Forms.CheckBox();
            this.chkQI5 = new System.Windows.Forms.CheckBox();
            this.chkQA5 = new System.Windows.Forms.CheckBox();
            this.chkTE5 = new System.Windows.Forms.CheckBox();
            this.chkS5 = new System.Windows.Forms.CheckBox();
            this.chkSE4 = new System.Windows.Forms.CheckBox();
            this.chkQI4 = new System.Windows.Forms.CheckBox();
            this.chkQA4 = new System.Windows.Forms.CheckBox();
            this.chkTE4 = new System.Windows.Forms.CheckBox();
            this.chkS4 = new System.Windows.Forms.CheckBox();
            this.chkSE3 = new System.Windows.Forms.CheckBox();
            this.chkQI3 = new System.Windows.Forms.CheckBox();
            this.chkQA3 = new System.Windows.Forms.CheckBox();
            this.chkTE3 = new System.Windows.Forms.CheckBox();
            this.chkS3 = new System.Windows.Forms.CheckBox();
            this.chkSE2 = new System.Windows.Forms.CheckBox();
            this.chkQI2 = new System.Windows.Forms.CheckBox();
            this.chkQA2 = new System.Windows.Forms.CheckBox();
            this.chkTE2 = new System.Windows.Forms.CheckBox();
            this.chkS2 = new System.Windows.Forms.CheckBox();
            this.chkSE1 = new System.Windows.Forms.CheckBox();
            this.chkQI1 = new System.Windows.Forms.CheckBox();
            this.chkQA1 = new System.Windows.Forms.CheckBox();
            this.chkTE1 = new System.Windows.Forms.CheckBox();
            this.chkS1 = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnImportarExcel = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnInserirAlterar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDtIniVig = new System.Windows.Forms.Label();
            this.lblNomeCli = new System.Windows.Forms.Label();
            this.txtCodCli = new System.Windows.Forms.TextBox();
            this.lblCodCli = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape10 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape9 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape8 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape7 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape6 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape5 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape4 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMotivo = new System.Windows.Forms.TextBox();
            this.gbManutExcCli.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbManutExcCli
            // 
            this.gbManutExcCli.Controls.Add(this.txtMotivo);
            this.gbManutExcCli.Controls.Add(this.label12);
            this.gbManutExcCli.Controls.Add(this.dtpDataFim);
            this.gbManutExcCli.Controls.Add(this.dtpDataInicio);
            this.gbManutExcCli.Controls.Add(this.chkSE5);
            this.gbManutExcCli.Controls.Add(this.chkQI5);
            this.gbManutExcCli.Controls.Add(this.chkQA5);
            this.gbManutExcCli.Controls.Add(this.chkTE5);
            this.gbManutExcCli.Controls.Add(this.chkS5);
            this.gbManutExcCli.Controls.Add(this.chkSE4);
            this.gbManutExcCli.Controls.Add(this.chkQI4);
            this.gbManutExcCli.Controls.Add(this.chkQA4);
            this.gbManutExcCli.Controls.Add(this.chkTE4);
            this.gbManutExcCli.Controls.Add(this.chkS4);
            this.gbManutExcCli.Controls.Add(this.chkSE3);
            this.gbManutExcCli.Controls.Add(this.chkQI3);
            this.gbManutExcCli.Controls.Add(this.chkQA3);
            this.gbManutExcCli.Controls.Add(this.chkTE3);
            this.gbManutExcCli.Controls.Add(this.chkS3);
            this.gbManutExcCli.Controls.Add(this.chkSE2);
            this.gbManutExcCli.Controls.Add(this.chkQI2);
            this.gbManutExcCli.Controls.Add(this.chkQA2);
            this.gbManutExcCli.Controls.Add(this.chkTE2);
            this.gbManutExcCli.Controls.Add(this.chkS2);
            this.gbManutExcCli.Controls.Add(this.chkSE1);
            this.gbManutExcCli.Controls.Add(this.chkQI1);
            this.gbManutExcCli.Controls.Add(this.chkQA1);
            this.gbManutExcCli.Controls.Add(this.chkTE1);
            this.gbManutExcCli.Controls.Add(this.chkS1);
            this.gbManutExcCli.Controls.Add(this.label11);
            this.gbManutExcCli.Controls.Add(this.label10);
            this.gbManutExcCli.Controls.Add(this.label9);
            this.gbManutExcCli.Controls.Add(this.label8);
            this.gbManutExcCli.Controls.Add(this.label7);
            this.gbManutExcCli.Controls.Add(this.label6);
            this.gbManutExcCli.Controls.Add(this.label5);
            this.gbManutExcCli.Controls.Add(this.label4);
            this.gbManutExcCli.Controls.Add(this.label3);
            this.gbManutExcCli.Controls.Add(this.label2);
            this.gbManutExcCli.Controls.Add(this.btnImportarExcel);
            this.gbManutExcCli.Controls.Add(this.btnCancelar);
            this.gbManutExcCli.Controls.Add(this.btnInserirAlterar);
            this.gbManutExcCli.Controls.Add(this.label1);
            this.gbManutExcCli.Controls.Add(this.lblDtIniVig);
            this.gbManutExcCli.Controls.Add(this.lblNomeCli);
            this.gbManutExcCli.Controls.Add(this.txtCodCli);
            this.gbManutExcCli.Controls.Add(this.lblCodCli);
            this.gbManutExcCli.Controls.Add(this.shapeContainer1);
            this.gbManutExcCli.Location = new System.Drawing.Point(12, 12);
            this.gbManutExcCli.Name = "gbManutExcCli";
            this.gbManutExcCli.Size = new System.Drawing.Size(463, 308);
            this.gbManutExcCli.TabIndex = 0;
            this.gbManutExcCli.TabStop = false;
            this.gbManutExcCli.Text = "Manutenção Exceção de Cliente";
            // 
            // dtpDataFim
            // 
            this.dtpDataFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFim.Location = new System.Drawing.Point(333, 60);
            this.dtpDataFim.Name = "dtpDataFim";
            this.dtpDataFim.ShowCheckBox = true;
            this.dtpDataFim.Size = new System.Drawing.Size(100, 20);
            this.dtpDataFim.TabIndex = 3;
            // 
            // dtpDataInicio
            // 
            this.dtpDataInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataInicio.Location = new System.Drawing.Point(121, 60);
            this.dtpDataInicio.Name = "dtpDataInicio";
            this.dtpDataInicio.ShowCheckBox = true;
            this.dtpDataInicio.Size = new System.Drawing.Size(100, 20);
            this.dtpDataInicio.TabIndex = 2;
            // 
            // chkSE5
            // 
            this.chkSE5.AutoSize = true;
            this.chkSE5.Location = new System.Drawing.Point(400, 197);
            this.chkSE5.Name = "chkSE5";
            this.chkSE5.Size = new System.Drawing.Size(15, 14);
            this.chkSE5.TabIndex = 29;
            this.chkSE5.UseVisualStyleBackColor = true;
            // 
            // chkQI5
            // 
            this.chkQI5.AutoSize = true;
            this.chkQI5.Location = new System.Drawing.Point(330, 197);
            this.chkQI5.Name = "chkQI5";
            this.chkQI5.Size = new System.Drawing.Size(15, 14);
            this.chkQI5.TabIndex = 24;
            this.chkQI5.UseVisualStyleBackColor = true;
            // 
            // chkQA5
            // 
            this.chkQA5.AutoSize = true;
            this.chkQA5.Location = new System.Drawing.Point(260, 197);
            this.chkQA5.Name = "chkQA5";
            this.chkQA5.Size = new System.Drawing.Size(15, 14);
            this.chkQA5.TabIndex = 19;
            this.chkQA5.UseVisualStyleBackColor = true;
            // 
            // chkTE5
            // 
            this.chkTE5.AutoSize = true;
            this.chkTE5.Location = new System.Drawing.Point(190, 197);
            this.chkTE5.Name = "chkTE5";
            this.chkTE5.Size = new System.Drawing.Size(15, 14);
            this.chkTE5.TabIndex = 14;
            this.chkTE5.UseVisualStyleBackColor = true;
            // 
            // chkS5
            // 
            this.chkS5.AutoSize = true;
            this.chkS5.Location = new System.Drawing.Point(110, 197);
            this.chkS5.Name = "chkS5";
            this.chkS5.Size = new System.Drawing.Size(15, 14);
            this.chkS5.TabIndex = 9;
            this.chkS5.UseVisualStyleBackColor = true;
            // 
            // chkSE4
            // 
            this.chkSE4.AutoSize = true;
            this.chkSE4.Location = new System.Drawing.Point(400, 177);
            this.chkSE4.Name = "chkSE4";
            this.chkSE4.Size = new System.Drawing.Size(15, 14);
            this.chkSE4.TabIndex = 28;
            this.chkSE4.UseVisualStyleBackColor = true;
            // 
            // chkQI4
            // 
            this.chkQI4.AutoSize = true;
            this.chkQI4.Location = new System.Drawing.Point(330, 177);
            this.chkQI4.Name = "chkQI4";
            this.chkQI4.Size = new System.Drawing.Size(15, 14);
            this.chkQI4.TabIndex = 23;
            this.chkQI4.UseVisualStyleBackColor = true;
            // 
            // chkQA4
            // 
            this.chkQA4.AutoSize = true;
            this.chkQA4.Location = new System.Drawing.Point(260, 177);
            this.chkQA4.Name = "chkQA4";
            this.chkQA4.Size = new System.Drawing.Size(15, 14);
            this.chkQA4.TabIndex = 18;
            this.chkQA4.UseVisualStyleBackColor = true;
            // 
            // chkTE4
            // 
            this.chkTE4.AutoSize = true;
            this.chkTE4.Location = new System.Drawing.Point(190, 177);
            this.chkTE4.Name = "chkTE4";
            this.chkTE4.Size = new System.Drawing.Size(15, 14);
            this.chkTE4.TabIndex = 13;
            this.chkTE4.UseVisualStyleBackColor = true;
            // 
            // chkS4
            // 
            this.chkS4.AutoSize = true;
            this.chkS4.Location = new System.Drawing.Point(110, 177);
            this.chkS4.Name = "chkS4";
            this.chkS4.Size = new System.Drawing.Size(15, 14);
            this.chkS4.TabIndex = 8;
            this.chkS4.UseVisualStyleBackColor = true;
            // 
            // chkSE3
            // 
            this.chkSE3.AutoSize = true;
            this.chkSE3.Location = new System.Drawing.Point(400, 157);
            this.chkSE3.Name = "chkSE3";
            this.chkSE3.Size = new System.Drawing.Size(15, 14);
            this.chkSE3.TabIndex = 27;
            this.chkSE3.UseVisualStyleBackColor = true;
            // 
            // chkQI3
            // 
            this.chkQI3.AutoSize = true;
            this.chkQI3.Location = new System.Drawing.Point(330, 157);
            this.chkQI3.Name = "chkQI3";
            this.chkQI3.Size = new System.Drawing.Size(15, 14);
            this.chkQI3.TabIndex = 22;
            this.chkQI3.UseVisualStyleBackColor = true;
            // 
            // chkQA3
            // 
            this.chkQA3.AutoSize = true;
            this.chkQA3.Location = new System.Drawing.Point(260, 157);
            this.chkQA3.Name = "chkQA3";
            this.chkQA3.Size = new System.Drawing.Size(15, 14);
            this.chkQA3.TabIndex = 17;
            this.chkQA3.UseVisualStyleBackColor = true;
            // 
            // chkTE3
            // 
            this.chkTE3.AutoSize = true;
            this.chkTE3.Location = new System.Drawing.Point(190, 157);
            this.chkTE3.Name = "chkTE3";
            this.chkTE3.Size = new System.Drawing.Size(15, 14);
            this.chkTE3.TabIndex = 12;
            this.chkTE3.UseVisualStyleBackColor = true;
            // 
            // chkS3
            // 
            this.chkS3.AutoSize = true;
            this.chkS3.Location = new System.Drawing.Point(110, 157);
            this.chkS3.Name = "chkS3";
            this.chkS3.Size = new System.Drawing.Size(15, 14);
            this.chkS3.TabIndex = 7;
            this.chkS3.UseVisualStyleBackColor = true;
            // 
            // chkSE2
            // 
            this.chkSE2.AutoSize = true;
            this.chkSE2.Location = new System.Drawing.Point(400, 137);
            this.chkSE2.Name = "chkSE2";
            this.chkSE2.Size = new System.Drawing.Size(15, 14);
            this.chkSE2.TabIndex = 26;
            this.chkSE2.UseVisualStyleBackColor = true;
            // 
            // chkQI2
            // 
            this.chkQI2.AutoSize = true;
            this.chkQI2.Location = new System.Drawing.Point(330, 137);
            this.chkQI2.Name = "chkQI2";
            this.chkQI2.Size = new System.Drawing.Size(15, 14);
            this.chkQI2.TabIndex = 21;
            this.chkQI2.UseVisualStyleBackColor = true;
            // 
            // chkQA2
            // 
            this.chkQA2.AutoSize = true;
            this.chkQA2.Location = new System.Drawing.Point(260, 137);
            this.chkQA2.Name = "chkQA2";
            this.chkQA2.Size = new System.Drawing.Size(15, 14);
            this.chkQA2.TabIndex = 16;
            this.chkQA2.UseVisualStyleBackColor = true;
            // 
            // chkTE2
            // 
            this.chkTE2.AutoSize = true;
            this.chkTE2.Location = new System.Drawing.Point(190, 137);
            this.chkTE2.Name = "chkTE2";
            this.chkTE2.Size = new System.Drawing.Size(15, 14);
            this.chkTE2.TabIndex = 11;
            this.chkTE2.UseVisualStyleBackColor = true;
            // 
            // chkS2
            // 
            this.chkS2.AutoSize = true;
            this.chkS2.Location = new System.Drawing.Point(110, 137);
            this.chkS2.Name = "chkS2";
            this.chkS2.Size = new System.Drawing.Size(15, 14);
            this.chkS2.TabIndex = 6;
            this.chkS2.UseVisualStyleBackColor = true;
            // 
            // chkSE1
            // 
            this.chkSE1.AutoSize = true;
            this.chkSE1.Location = new System.Drawing.Point(400, 117);
            this.chkSE1.Name = "chkSE1";
            this.chkSE1.Size = new System.Drawing.Size(15, 14);
            this.chkSE1.TabIndex = 25;
            this.chkSE1.UseVisualStyleBackColor = true;
            // 
            // chkQI1
            // 
            this.chkQI1.AutoSize = true;
            this.chkQI1.Location = new System.Drawing.Point(330, 117);
            this.chkQI1.Name = "chkQI1";
            this.chkQI1.Size = new System.Drawing.Size(15, 14);
            this.chkQI1.TabIndex = 20;
            this.chkQI1.UseVisualStyleBackColor = true;
            // 
            // chkQA1
            // 
            this.chkQA1.AutoSize = true;
            this.chkQA1.Location = new System.Drawing.Point(260, 117);
            this.chkQA1.Name = "chkQA1";
            this.chkQA1.Size = new System.Drawing.Size(15, 14);
            this.chkQA1.TabIndex = 15;
            this.chkQA1.UseVisualStyleBackColor = true;
            // 
            // chkTE1
            // 
            this.chkTE1.AutoSize = true;
            this.chkTE1.Location = new System.Drawing.Point(190, 117);
            this.chkTE1.Name = "chkTE1";
            this.chkTE1.Size = new System.Drawing.Size(15, 14);
            this.chkTE1.TabIndex = 10;
            this.chkTE1.UseVisualStyleBackColor = true;
            // 
            // chkS1
            // 
            this.chkS1.AutoSize = true;
            this.chkS1.Location = new System.Drawing.Point(110, 117);
            this.chkS1.Name = "chkS1";
            this.chkS1.Size = new System.Drawing.Size(15, 14);
            this.chkS1.TabIndex = 5;
            this.chkS1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(373, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Sexta-Feira";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(303, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Quinta-Feira";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(232, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Quarta-Feira";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(164, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Terça-Feira";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(80, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Segunda-Feira";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 197);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Semana 5:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Semana 4:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Semana 3:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Semana 2:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Semana 1:";
            // 
            // btnImportarExcel
            // 
            this.btnImportarExcel.Image = global::VDA991.Properties.Resources.xls;
            this.btnImportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportarExcel.Location = new System.Drawing.Point(85, 267);
            this.btnImportarExcel.Name = "btnImportarExcel";
            this.btnImportarExcel.Size = new System.Drawing.Size(120, 35);
            this.btnImportarExcel.TabIndex = 30;
            this.btnImportarExcel.Text = "&Importar Excel";
            this.btnImportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImportarExcel.UseVisualStyleBackColor = true;
            this.btnImportarExcel.Click += new System.EventHandler(this.btnImportarExcel_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::VDA991.Properties.Resources.cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(337, 267);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 35);
            this.btnCancelar.TabIndex = 31;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnInserirAlterar
            // 
            this.btnInserirAlterar.Image = global::VDA991.Properties.Resources.ok;
            this.btnInserirAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInserirAlterar.Location = new System.Drawing.Point(211, 267);
            this.btnInserirAlterar.Name = "btnInserirAlterar";
            this.btnInserirAlterar.Size = new System.Drawing.Size(120, 35);
            this.btnInserirAlterar.TabIndex = 4;
            this.btnInserirAlterar.Text = "       Inserir/Alterar";
            this.btnInserirAlterar.UseVisualStyleBackColor = true;
            this.btnInserirAlterar.Click += new System.EventHandler(this.btnInserirAlterar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(232, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Data Final Vigência:";
            // 
            // lblDtIniVig
            // 
            this.lblDtIniVig.AutoSize = true;
            this.lblDtIniVig.Location = new System.Drawing.Point(17, 64);
            this.lblDtIniVig.Name = "lblDtIniVig";
            this.lblDtIniVig.Size = new System.Drawing.Size(105, 13);
            this.lblDtIniVig.TabIndex = 7;
            this.lblDtIniVig.Text = "Data Inicio Vigência:";
            // 
            // lblNomeCli
            // 
            this.lblNomeCli.AutoSize = true;
            this.lblNomeCli.Location = new System.Drawing.Point(197, 30);
            this.lblNomeCli.Name = "lblNomeCli";
            this.lblNomeCli.Size = new System.Drawing.Size(106, 13);
            this.lblNomeCli.TabIndex = 2;
            this.lblNomeCli.Text = "NOME DO CLIENTE";
            this.lblNomeCli.Visible = false;
            // 
            // txtCodCli
            // 
            this.txtCodCli.Location = new System.Drawing.Point(121, 27);
            this.txtCodCli.MaxLength = 6;
            this.txtCodCli.Name = "txtCodCli";
            this.txtCodCli.Size = new System.Drawing.Size(70, 20);
            this.txtCodCli.TabIndex = 1;
            this.txtCodCli.DoubleClick += new System.EventHandler(this.txtCodCli_DoubleClick);
            this.txtCodCli.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodCli_KeyPress);
            this.txtCodCli.Leave += new System.EventHandler(this.txtCodCli_Leave);
            // 
            // lblCodCli
            // 
            this.lblCodCli.AutoSize = true;
            this.lblCodCli.Location = new System.Drawing.Point(17, 30);
            this.lblCodCli.Name = "lblCodCli";
            this.lblCodCli.Size = new System.Drawing.Size(93, 13);
            this.lblCodCli.TabIndex = 0;
            this.lblCodCli.Text = "Código do Cliente:";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(3, 16);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape10,
            this.lineShape9,
            this.lineShape8,
            this.lineShape7,
            this.lineShape6,
            this.lineShape5,
            this.lineShape4,
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(457, 289);
            this.shapeContainer1.TabIndex = 26;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape10
            // 
            this.lineShape10.Name = "lineShape10";
            this.lineShape10.X1 = 440;
            this.lineShape10.X2 = 440;
            this.lineShape10.Y1 = 90;
            this.lineShape10.Y2 = 198;
            // 
            // lineShape9
            // 
            this.lineShape9.Name = "lineShape9";
            this.lineShape9.X1 = 365;
            this.lineShape9.X2 = 365;
            this.lineShape9.Y1 = 90;
            this.lineShape9.Y2 = 198;
            // 
            // lineShape8
            // 
            this.lineShape8.Name = "lineShape8";
            this.lineShape8.X1 = 295;
            this.lineShape8.X2 = 295;
            this.lineShape8.Y1 = 90;
            this.lineShape8.Y2 = 198;
            // 
            // lineShape7
            // 
            this.lineShape7.Name = "lineShape7";
            this.lineShape7.X1 = 225;
            this.lineShape7.X2 = 225;
            this.lineShape7.Y1 = 90;
            this.lineShape7.Y2 = 198;
            // 
            // lineShape6
            // 
            this.lineShape6.Name = "lineShape6";
            this.lineShape6.X1 = 155;
            this.lineShape6.X2 = 155;
            this.lineShape6.Y1 = 90;
            this.lineShape6.Y2 = 198;
            // 
            // lineShape5
            // 
            this.lineShape5.Name = "lineShape5";
            this.lineShape5.X1 = 18;
            this.lineShape5.X2 = 440;
            this.lineShape5.Y1 = 198;
            this.lineShape5.Y2 = 198;
            // 
            // lineShape4
            // 
            this.lineShape4.Name = "lineShape4";
            this.lineShape4.X1 = 18;
            this.lineShape4.X2 = 440;
            this.lineShape4.Y1 = 178;
            this.lineShape4.Y2 = 178;
            // 
            // lineShape3
            // 
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 18;
            this.lineShape3.X2 = 440;
            this.lineShape3.Y1 = 158;
            this.lineShape3.Y2 = 158;
            // 
            // lineShape2
            // 
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 18;
            this.lineShape2.X2 = 440;
            this.lineShape2.Y1 = 138;
            this.lineShape2.Y2 = 138;
            // 
            // lineShape1
            // 
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 18;
            this.lineShape1.X2 = 440;
            this.lineShape1.Y1 = 118;
            this.lineShape1.Y2 = 118;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(21, 233);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Motivo:";
            // 
            // txtMotivo
            // 
            this.txtMotivo.Location = new System.Drawing.Point(69, 230);
            this.txtMotivo.MaxLength = 20;
            this.txtMotivo.Name = "txtMotivo";
            this.txtMotivo.Size = new System.Drawing.Size(206, 20);
            this.txtMotivo.TabIndex = 33;
            // 
            // FrmManutencaoExcecaoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 333);
            this.Controls.Add(this.gbManutExcCli);
            this.KeyPreview = true;
            this.Name = "FrmManutencaoExcecaoCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manutenção Exceção de Cliente";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmManutencaoExcecaoCliente_KeyDown);
            this.gbManutExcCli.ResumeLayout(false);
            this.gbManutExcCli.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbManutExcCli;
        private System.Windows.Forms.Label lblNomeCli;
        private System.Windows.Forms.TextBox txtCodCli;
        private System.Windows.Forms.Label lblCodCli;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnInserirAlterar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDtIniVig;
        private System.Windows.Forms.Button btnImportarExcel;
        private System.Windows.Forms.CheckBox chkSE5;
        private System.Windows.Forms.CheckBox chkQI5;
        private System.Windows.Forms.CheckBox chkQA5;
        private System.Windows.Forms.CheckBox chkTE5;
        private System.Windows.Forms.CheckBox chkS5;
        private System.Windows.Forms.CheckBox chkSE4;
        private System.Windows.Forms.CheckBox chkQI4;
        private System.Windows.Forms.CheckBox chkQA4;
        private System.Windows.Forms.CheckBox chkTE4;
        private System.Windows.Forms.CheckBox chkS4;
        private System.Windows.Forms.CheckBox chkSE3;
        private System.Windows.Forms.CheckBox chkQI3;
        private System.Windows.Forms.CheckBox chkQA3;
        private System.Windows.Forms.CheckBox chkTE3;
        private System.Windows.Forms.CheckBox chkS3;
        private System.Windows.Forms.CheckBox chkSE2;
        private System.Windows.Forms.CheckBox chkQI2;
        private System.Windows.Forms.CheckBox chkQA2;
        private System.Windows.Forms.CheckBox chkTE2;
        private System.Windows.Forms.CheckBox chkS2;
        private System.Windows.Forms.CheckBox chkSE1;
        private System.Windows.Forms.CheckBox chkQI1;
        private System.Windows.Forms.CheckBox chkQA1;
        private System.Windows.Forms.CheckBox chkTE1;
        private System.Windows.Forms.CheckBox chkS1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape10;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape9;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape8;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape7;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape6;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape5;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape4;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.DateTimePicker dtpDataFim;
        private System.Windows.Forms.DateTimePicker dtpDataInicio;
        private System.Windows.Forms.TextBox txtMotivo;
        private System.Windows.Forms.Label label12;
    }
}