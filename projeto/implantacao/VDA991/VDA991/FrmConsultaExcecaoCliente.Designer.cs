﻿namespace VDA991
{
    partial class FrmConsultaExcecaoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpResultado = new System.Windows.Forms.GroupBox();
            this.chkInativos = new System.Windows.Forms.CheckBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.dgvCliente = new System.Windows.Forms.DataGridView();
            this.grpFiltro = new System.Windows.Forms.GroupBox();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.lblFilial = new System.Windows.Forms.Label();
            this.lblTipoFiltro = new System.Windows.Forms.Label();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.txtValorFiltro = new System.Windows.Forms.TextBox();
            this.lblCampoFiltro = new System.Windows.Forms.Label();
            this.rbCnpj = new System.Windows.Forms.RadioButton();
            this.rbCdCli = new System.Windows.Forms.RadioButton();
            this.cdcli = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOTIVO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTVIGENCIAINI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTVIGENCIAFIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpResultado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).BeginInit();
            this.grpFiltro.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpResultado
            // 
            this.grpResultado.Controls.Add(this.chkInativos);
            this.grpResultado.Controls.Add(this.btnSair);
            this.grpResultado.Controls.Add(this.dgvCliente);
            this.grpResultado.Location = new System.Drawing.Point(15, 89);
            this.grpResultado.Name = "grpResultado";
            this.grpResultado.Size = new System.Drawing.Size(743, 347);
            this.grpResultado.TabIndex = 0;
            this.grpResultado.TabStop = false;
            this.grpResultado.Text = "Resultado:";
            // 
            // chkInativos
            // 
            this.chkInativos.AutoSize = true;
            this.chkInativos.Location = new System.Drawing.Point(9, 306);
            this.chkInativos.Name = "chkInativos";
            this.chkInativos.Size = new System.Drawing.Size(63, 17);
            this.chkInativos.TabIndex = 6;
            this.chkInativos.Text = "Inativos";
            this.chkInativos.UseVisualStyleBackColor = true;
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(617, 306);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 7;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // dgvCliente
            // 
            this.dgvCliente.AllowUserToAddRows = false;
            this.dgvCliente.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCliente.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cdcli,
            this.cliente,
            this.MOTIVO,
            this.DTVIGENCIAINI,
            this.DTVIGENCIAFIM,
            this.status});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCliente.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCliente.Location = new System.Drawing.Point(6, 19);
            this.dgvCliente.MultiSelect = false;
            this.dgvCliente.Name = "dgvCliente";
            this.dgvCliente.ReadOnly = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCliente.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvCliente.RowHeadersWidth = 30;
            this.dgvCliente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCliente.Size = new System.Drawing.Size(731, 281);
            this.dgvCliente.TabIndex = 4;
            this.dgvCliente.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCliente_CellDoubleClick);
            // 
            // grpFiltro
            // 
            this.grpFiltro.Controls.Add(this.cmbFilial);
            this.grpFiltro.Controls.Add(this.lblFilial);
            this.grpFiltro.Controls.Add(this.lblTipoFiltro);
            this.grpFiltro.Controls.Add(this.btnConsulta);
            this.grpFiltro.Controls.Add(this.txtValorFiltro);
            this.grpFiltro.Controls.Add(this.lblCampoFiltro);
            this.grpFiltro.Controls.Add(this.rbCnpj);
            this.grpFiltro.Controls.Add(this.rbCdCli);
            this.grpFiltro.Location = new System.Drawing.Point(15, 12);
            this.grpFiltro.Name = "grpFiltro";
            this.grpFiltro.Size = new System.Drawing.Size(743, 71);
            this.grpFiltro.TabIndex = 3;
            this.grpFiltro.TabStop = false;
            this.grpFiltro.Text = "Filtros:";
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(6, 42);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(286, 21);
            this.cmbFilial.TabIndex = 0;
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Location = new System.Drawing.Point(6, 20);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(30, 13);
            this.lblFilial.TabIndex = 7;
            this.lblFilial.Text = "Filial:";
            // 
            // lblTipoFiltro
            // 
            this.lblTipoFiltro.AutoSize = true;
            this.lblTipoFiltro.Location = new System.Drawing.Point(344, 20);
            this.lblTipoFiltro.Name = "lblTipoFiltro";
            this.lblTipoFiltro.Size = new System.Drawing.Size(71, 13);
            this.lblTipoFiltro.TabIndex = 6;
            this.lblTipoFiltro.Text = "Tipo de Filtro:";
            // 
            // btnConsulta
            // 
            this.btnConsulta.Image = global::VDA991.Properties.Resources.find;
            this.btnConsulta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsulta.Location = new System.Drawing.Point(617, 27);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(120, 35);
            this.btnConsulta.TabIndex = 4;
            this.btnConsulta.Text = "&Consulta";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // txtValorFiltro
            // 
            this.txtValorFiltro.Location = new System.Drawing.Point(347, 42);
            this.txtValorFiltro.MaxLength = 50;
            this.txtValorFiltro.Name = "txtValorFiltro";
            this.txtValorFiltro.Size = new System.Drawing.Size(222, 20);
            this.txtValorFiltro.TabIndex = 3;
            this.txtValorFiltro.DoubleClick += new System.EventHandler(this.txtValorFiltro_DoubleClick);
            this.txtValorFiltro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorFiltro_KeyPress);
            // 
            // lblCampoFiltro
            // 
            this.lblCampoFiltro.AutoSize = true;
            this.lblCampoFiltro.Location = new System.Drawing.Point(298, 45);
            this.lblCampoFiltro.Name = "lblCampoFiltro";
            this.lblCampoFiltro.Size = new System.Drawing.Size(37, 13);
            this.lblCampoFiltro.TabIndex = 3;
            this.lblCampoFiltro.Text = "CNPJ:";
            // 
            // rbCnpj
            // 
            this.rbCnpj.AutoSize = true;
            this.rbCnpj.Checked = true;
            this.rbCnpj.Location = new System.Drawing.Point(421, 18);
            this.rbCnpj.Name = "rbCnpj";
            this.rbCnpj.Size = new System.Drawing.Size(52, 17);
            this.rbCnpj.TabIndex = 1;
            this.rbCnpj.TabStop = true;
            this.rbCnpj.Text = "CNPJ";
            this.rbCnpj.UseVisualStyleBackColor = true;
            this.rbCnpj.Click += new System.EventHandler(this.rbCnpj_Click);
            // 
            // rbCdCli
            // 
            this.rbCdCli.AutoSize = true;
            this.rbCdCli.Location = new System.Drawing.Point(479, 18);
            this.rbCdCli.Name = "rbCdCli";
            this.rbCdCli.Size = new System.Drawing.Size(93, 17);
            this.rbCdCli.TabIndex = 2;
            this.rbCdCli.Text = "Código Cliente";
            this.rbCdCli.UseVisualStyleBackColor = true;
            this.rbCdCli.Click += new System.EventHandler(this.rbCdCli_Click);
            // 
            // cdcli
            // 
            this.cdcli.DataPropertyName = "cdcli";
            this.cdcli.HeaderText = "Codigo Cliente";
            this.cdcli.Name = "cdcli";
            this.cdcli.ReadOnly = true;
            this.cdcli.Visible = false;
            // 
            // cliente
            // 
            this.cliente.DataPropertyName = "cliente";
            this.cliente.HeaderText = "Cliente";
            this.cliente.Name = "cliente";
            this.cliente.ReadOnly = true;
            this.cliente.Width = 350;
            // 
            // MOTIVO
            // 
            this.MOTIVO.DataPropertyName = "motivo";
            this.MOTIVO.HeaderText = "Motivo";
            this.MOTIVO.Name = "MOTIVO";
            this.MOTIVO.ReadOnly = true;
            this.MOTIVO.Width = 119;
            // 
            // DTVIGENCIAINI
            // 
            this.DTVIGENCIAINI.DataPropertyName = "DTVIGENCIAINI";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DTVIGENCIAINI.DefaultCellStyle = dataGridViewCellStyle2;
            this.DTVIGENCIAINI.HeaderText = "Vigência Inicial";
            this.DTVIGENCIAINI.Name = "DTVIGENCIAINI";
            this.DTVIGENCIAINI.ReadOnly = true;
            this.DTVIGENCIAINI.Width = 110;
            // 
            // DTVIGENCIAFIM
            // 
            this.DTVIGENCIAFIM.DataPropertyName = "DTVIGENCIAFIM";
            this.DTVIGENCIAFIM.HeaderText = "Vigência Final";
            this.DTVIGENCIAFIM.Name = "DTVIGENCIAFIM";
            this.DTVIGENCIAFIM.ReadOnly = true;
            this.DTVIGENCIAFIM.Width = 120;
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.status.DefaultCellStyle = dataGridViewCellStyle3;
            this.status.HeaderText = "Status";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Visible = false;
            this.status.Width = 120;
            // 
            // FrmConsultaExcecaoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 448);
            this.Controls.Add(this.grpResultado);
            this.Controls.Add(this.grpFiltro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "FrmConsultaExcecaoCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta Exceções de Clientes";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmConsultaExcecaoCliente_KeyDown);
            this.grpResultado.ResumeLayout(false);
            this.grpResultado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCliente)).EndInit();
            this.grpFiltro.ResumeLayout(false);
            this.grpFiltro.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpResultado;
        private System.Windows.Forms.DataGridView dgvCliente;
        private System.Windows.Forms.GroupBox grpFiltro;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.TextBox txtValorFiltro;
        private System.Windows.Forms.Label lblCampoFiltro;
        private System.Windows.Forms.RadioButton rbCnpj;
        private System.Windows.Forms.RadioButton rbCdCli;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Label lblTipoFiltro;
        private System.Windows.Forms.CheckBox chkInativos;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label lblFilial;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdcli;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOTIVO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTVIGENCIAINI;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTVIGENCIAFIM;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
    }
}