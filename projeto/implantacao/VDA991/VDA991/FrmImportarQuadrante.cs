﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using UtilGeralDPA;
using Business;
using Entities;
using System.IO;

//TI-4404
namespace VDA991
{
    public partial class FrmImportarQuadrante : Form
    {
        public FrmImportarQuadrante()
        {
            InitializeComponent();
            CarregarComboFilial(cmbLoja);
            CarregarComboQuadrante(cmbQuadrante);
        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {

            List<FilialBE> listaFilial = new List<FilialBE>();

            FilialBE filial = new FilialBE();
            filial.COD_FILIAL = 0;
            filial.NOME_FILIAL = "Selecione";

            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "NOME_FILIAL";
            cmbFilial.ValueMember = "COD_FILIAL";

            try
            {
                listaFilial = FilialBO.BuscarLoja();
                listaFilial.Insert(0, filial);
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboQuadrante(ComboBox cmbQuadrante)
        {

            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();

            QuadranteBE quadrante = new QuadranteBE();
            quadrante.QTDCONTATO = 0;
            quadrante.QUADRANTE = "Selecione";

            cmbQuadrante.DataSource = null;
            cmbQuadrante.Items.Clear();
            cmbQuadrante.DisplayMember = "QUADRANTE";
            cmbQuadrante.ValueMember = "QUADRANTE";

            try
            {
                listaQuadrante = QuadranteBO.BuscarAcaoQuadrante();
                listaQuadrante.Insert(0, quadrante);
                cmbQuadrante.DataSource = listaQuadrante;
                cmbQuadrante.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboQuadrante, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            try
            {
                List<AcaoQuadrante> lista = new List<AcaoQuadrante>();
                AcaoQuadrante oFiltro = new AcaoQuadrante();

                oFiltro.COD_LOJA = Convert.ToInt16(cmbLoja.SelectedValue);
                oFiltro.COD_QUADRANTE = cmbQuadrante.SelectedValue.ToString().ToUpper();

                lista = QuadranteBO.BuscarListaAcaoQuadrante(oFiltro);
                dgvListaQuadrante.DataSource = lista;

                if (dgvListaQuadrante.RowCount == 0)
                {
                    MessageBox.Show("Nenhum Registro Encontrado.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    btnExportarExcel.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha na consulta.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();
                dt.TableName="Plan1";

                //Adding the Columns
                foreach (DataGridViewColumn column in dgvListaQuadrante.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in dgvListaQuadrante.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                if (dgvListaQuadrante.Rows.Count < 100)
                {
                    for (int j = dgvListaQuadrante.Rows.Count; j <= (dgvListaQuadrante.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= dgvListaQuadrante.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx";
            this.openFileDialog1.ShowDialog();
            this.txtCaminhoArquivo.Text = this.openFileDialog1.FileName;
        }

        private void btnImportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                AcaoQuadrante oFiltro = new AcaoQuadrante();

                if (File.Exists(this.txtCaminhoArquivo.Text))
                {                    

                    var lstArquivo = QuadranteBO.CarregarExcel(this.txtCaminhoArquivo.Text);
                    lstArquivo.ForEach(o =>
                    {
                        oFiltro.COD_LOJA = o.COD_LOJA;
                        oFiltro.COD_QUADRANTE = o.COD_QUADRANTE;
                        oFiltro.SEQUENCIA = o.SEQUENCIA;
                        oFiltro.NUM_PRIORIDADE = o.NUM_PRIORIDADE;
                        oFiltro.DESC_ACAO = o.DESC_ACAO;

                        QuadranteBO.AtualizarQuadrante(oFiltro);

                    });
                    MessageBox.Show("Dados importados com sucesso!", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Arquivo não encontrado.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.txtCaminhoArquivo.Text = string.Empty;
            }
            catch (Exception ex)
            {
                this.txtCaminhoArquivo.Text = string.Empty;
                Logger.LogError(ex);
                MessageBox.Show("Falha ao importar excel.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
