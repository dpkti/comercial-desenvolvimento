﻿namespace VDA991
{
    partial class FrmImportarQuadrante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbListaQuadrante = new System.Windows.Forms.GroupBox();
            this.btnImportarExcel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.txtCaminhoArquivo = new System.Windows.Forms.TextBox();
            this.gbConsulta = new System.Windows.Forms.GroupBox();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.cmbQuadrante = new System.Windows.Forms.ComboBox();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.dgvListaQuadrante = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.gbListaQuadrante.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbConsulta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaQuadrante)).BeginInit();
            this.SuspendLayout();
            // 
            // gbListaQuadrante
            // 
            this.gbListaQuadrante.Controls.Add(this.btnImportarExcel);
            this.gbListaQuadrante.Controls.Add(this.groupBox1);
            this.gbListaQuadrante.Controls.Add(this.gbConsulta);
            this.gbListaQuadrante.Controls.Add(this.btnSair);
            this.gbListaQuadrante.Controls.Add(this.btnExportarExcel);
            this.gbListaQuadrante.Controls.Add(this.dgvListaQuadrante);
            this.gbListaQuadrante.Location = new System.Drawing.Point(12, 12);
            this.gbListaQuadrante.Name = "gbListaQuadrante";
            this.gbListaQuadrante.Size = new System.Drawing.Size(826, 448);
            this.gbListaQuadrante.TabIndex = 1;
            this.gbListaQuadrante.TabStop = false;
            this.gbListaQuadrante.Text = "Lista de Quadrantes";
            // 
            // btnImportarExcel
            // 
            this.btnImportarExcel.Image = global::VDA991.Properties.Resources.xls;
            this.btnImportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImportarExcel.Location = new System.Drawing.Point(8, 407);
            this.btnImportarExcel.Name = "btnImportarExcel";
            this.btnImportarExcel.Size = new System.Drawing.Size(120, 35);
            this.btnImportarExcel.TabIndex = 32;
            this.btnImportarExcel.Text = "&Importar Excel";
            this.btnImportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImportarExcel.UseVisualStyleBackColor = true;
            this.btnImportarExcel.Click += new System.EventHandler(this.btnImportarExcel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAbrir);
            this.groupBox1.Controls.Add(this.txtCaminhoArquivo);
            this.groupBox1.Location = new System.Drawing.Point(8, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(812, 48);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Importar";
            // 
            // btnAbrir
            // 
            this.btnAbrir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbrir.Location = new System.Drawing.Point(751, 10);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(54, 35);
            this.btnAbrir.TabIndex = 31;
            this.btnAbrir.Text = "&Abrir";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // txtCaminhoArquivo
            // 
            this.txtCaminhoArquivo.Location = new System.Drawing.Point(10, 17);
            this.txtCaminhoArquivo.MaxLength = 2000;
            this.txtCaminhoArquivo.Name = "txtCaminhoArquivo";
            this.txtCaminhoArquivo.Size = new System.Drawing.Size(671, 20);
            this.txtCaminhoArquivo.TabIndex = 7;
            // 
            // gbConsulta
            // 
            this.gbConsulta.Controls.Add(this.cmbLoja);
            this.gbConsulta.Controls.Add(this.cmbQuadrante);
            this.gbConsulta.Controls.Add(this.btnConsulta);
            this.gbConsulta.Controls.Add(this.label2);
            this.gbConsulta.Controls.Add(this.label1);
            this.gbConsulta.Location = new System.Drawing.Point(8, 70);
            this.gbConsulta.Name = "gbConsulta";
            this.gbConsulta.Size = new System.Drawing.Size(812, 55);
            this.gbConsulta.TabIndex = 3;
            this.gbConsulta.TabStop = false;
            this.gbConsulta.Text = "Consulta";
            // 
            // cmbLoja
            // 
            this.cmbLoja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.Location = new System.Drawing.Point(42, 22);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(186, 21);
            this.cmbLoja.TabIndex = 11;
            // 
            // cmbQuadrante
            // 
            this.cmbQuadrante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQuadrante.FormattingEnabled = true;
            this.cmbQuadrante.Location = new System.Drawing.Point(322, 22);
            this.cmbQuadrante.Name = "cmbQuadrante";
            this.cmbQuadrante.Size = new System.Drawing.Size(186, 21);
            this.cmbQuadrante.TabIndex = 10;
            // 
            // btnConsulta
            // 
            this.btnConsulta.Image = global::VDA991.Properties.Resources.find;
            this.btnConsulta.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnConsulta.Location = new System.Drawing.Point(686, 14);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(120, 35);
            this.btnConsulta.TabIndex = 2;
            this.btnConsulta.Text = "&Consulta";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(256, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Quadrante:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loja:";
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(693, 407);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 4;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Enabled = false;
            this.btnExportarExcel.Image = global::VDA991.Properties.Resources.xls;
            this.btnExportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportarExcel.Location = new System.Drawing.Point(555, 407);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(120, 35);
            this.btnExportarExcel.TabIndex = 3;
            this.btnExportarExcel.Text = "&Exportar Excel";
            this.btnExportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // dgvListaQuadrante
            // 
            this.dgvListaQuadrante.AllowUserToAddRows = false;
            this.dgvListaQuadrante.AllowUserToDeleteRows = false;
            this.dgvListaQuadrante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaQuadrante.Location = new System.Drawing.Point(7, 131);
            this.dgvListaQuadrante.Name = "dgvListaQuadrante";
            this.dgvListaQuadrante.ReadOnly = true;
            this.dgvListaQuadrante.RowHeadersWidth = 20;
            this.dgvListaQuadrante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListaQuadrante.Size = new System.Drawing.Size(810, 270);
            this.dgvListaQuadrante.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.InitialDirectory = "c:\\";
            this.openFileDialog1.Title = "Selecionar Arquivo";
            // 
            // FrmImportarQuadrante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 473);
            this.Controls.Add(this.gbListaQuadrante);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmImportarQuadrante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Importar Quadrante";
            this.gbListaQuadrante.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbConsulta.ResumeLayout(false);
            this.gbConsulta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaQuadrante)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbListaQuadrante;
        private System.Windows.Forms.GroupBox gbConsulta;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.DataGridView dgvListaQuadrante;
        private System.Windows.Forms.ComboBox cmbQuadrante;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCaminhoArquivo;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnImportarExcel;
    }
}