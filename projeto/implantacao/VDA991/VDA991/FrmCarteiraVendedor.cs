﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;
using VDA991;

namespace VDA991
{
    public partial class FrmCarteiraVendedor : Form
    {
        public FrmCarteiraVendedor(VendedorBE oFiltro)
        {
            InitializeComponent();
            CarregaGrid(oFiltro);
        }

        private void CarregaGrid(VendedorBE oFiltro)
        {

            List<RetornoCarteiraVendedor> listaCarteiraVendedor = new List<RetornoCarteiraVendedor>();

            try
            {
                listaCarteiraVendedor = VendedorBO.BuscarCarteiraVendedor(oFiltro);
                dgvCliente.DataSource = listaCarteiraVendedor;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.Erro_CarregarGrid, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    
    
    
    }

}
