﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace VDA991
{
    public partial class FrmExclusaoContatosPendenteVend : Form
    {
        public FrmExclusaoContatosPendenteVend()
        {
            InitializeComponent();
            CarregarComboFilial(cmbFilial);
            CarregarComboVendedor(cmbVendedor);

            //Bloqueia as colunas para não deixar editável
            //Somente o check para excluir
            dgvContPend.Columns[0].ReadOnly = true;
            dgvContPend.Columns[1].ReadOnly = true;
            dgvContPend.Columns[2].ReadOnly = true;
            dgvContPend.Columns[3].ReadOnly = true;
            dgvContPend.Columns[4].ReadOnly = true;
            dgvContPend.Columns[5].ReadOnly = true;
            dgvContPend.Columns[6].ReadOnly = true;
            dgvContPend.Columns[7].ReadOnly = true;

        }

        private void FrmExclusaoContatosPendenteVend_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
            {
                e.Handled = true;
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {

            bool atualiza = false;
            try
            {


                if (dgvContPend.RowCount == 0)
                {
                    MessageBox.Show("Não Existem Registros para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return; 
                }


                if (MessageBox.Show("Confirma a Exclusão do(s) Contato(s)?", VDA991.ResourceRoteirizacao.Titulo_MessageBoxConfirmacao, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                

                    foreach (DataGridViewRow dataGridViewRow in dgvContPend.Rows)
                    {
                        VendedorBE oFiltro = new VendedorBE();

                        if (Convert.ToBoolean(dataGridViewRow.Cells["EXCLUIR"].Value) == true)
                        {

                            oFiltro.DTPROCESS = Convert.ToDateTime(dataGridViewRow.Cells["DATA_ROTEIRO"].Value);
                            oFiltro.CDVENDEDOR = Convert.ToInt64(dataGridViewRow.Cells["CDVENDEDOR"].Value);
                            oFiltro.CDCLI = Convert.ToInt64(dataGridViewRow.Cells["CDCLI"].Value);

                            VendedorBO.UpdateContatosPend(oFiltro);
                            atualiza = true;
                        }

                    }


                    if (atualiza == true)
                    {
                        MessageBox.Show("Contato(s) Excluído(s) com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Selecionado para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    CarregarGrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao excluir as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {

            List<FilialBE> listaFilial = new List<FilialBE>();

            FilialBE filial = new FilialBE();
            filial.COD_FILIAL = 0;
            filial.NOME_FILIAL = "Selecione";

            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "NOME_FILIAL";
            cmbFilial.ValueMember = "COD_FILIAL";

            try
            {
                listaFilial = FilialBO.BuscarFilial();
                listaFilial.Insert(0, filial);
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboVendedor(ComboBox cmbVendedor)
        {

            List<FiltroVendedor> listaVendedor = new List<FiltroVendedor>();

            FiltroVendedor vendedor = new FiltroVendedor();
            vendedor.CDVENDEDOR = 0;
            vendedor.VENDEDOR = "Selecione";

            cmbVendedor.DataSource = null;
            cmbVendedor.Items.Clear();
            cmbVendedor.DisplayMember = "VENDEDOR";
            cmbVendedor.ValueMember = "CDVENDEDOR";

            try
            {
                long? lngCodCoord;
                if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
                {
                    lngCodCoord = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
                }
                else
                {
                    lngCodCoord = null;
                }
                
                listaVendedor = VendedorBO.BuscarVendedor(lngCodCoord);
                listaVendedor.Insert(0, vendedor);
                cmbVendedor.DataSource = listaVendedor;
                cmbVendedor.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboVendedor, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void chkSelTodos_CheckedChanged(object sender, EventArgs e)
        {

            foreach (DataGridViewRow dataGridViewRow in dgvContPend.Rows)
            {

                dataGridViewRow.Cells["EXCLUIR"].Value = chkSelTodos.Checked;

            }
        }

        private void CarregarGrid()
        {
            try
            {
                FiltroContatosPend oFiltro = new FiltroContatosPend();

                if (dtInicio.Value.Date > dtFinal.Value.Date)
                {
                    MessageBox.Show("Data Final deve ser maior que a Data Inicial.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (dtInicio.Value.Date > DateTime.Now)
                {
                    MessageBox.Show("Data Inicial Deve ser Menor que a Data Atual.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (dtFinal.Value.Date > DateTime.Now)
                {
                    MessageBox.Show("Data Final Deve ser Menor que a Data Atual.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                

                oFiltro.CDVENDEDOR = Convert.ToInt32(cmbVendedor.SelectedValue);
                oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
                oFiltro.DATA_INICIAL = dtInicio.Value;
                oFiltro.DATA_FINAL = dtFinal.Value;

                if (oFiltro.CDVENDEDOR == 0 && oFiltro.CDFILIAL == 0 )
                {
                    string mensagem = "Necessário informar o Vendedor ou Filial para consulta.";
                    MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                List<RetornoContatosPend> listaContatosPend = new List<RetornoContatosPend>();
                listaContatosPend = VendedorBO.BuscarContatosPend(oFiltro);
                dgvContPend.DataSource = listaContatosPend;

                if (dgvContPend.RowCount == 0)
                {
                    btnExcluir.Enabled = false;
                    MessageBox.Show("Nenhum Registro Encontrado.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    btnExcluir.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no método CarregarGrid.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

    }
}
