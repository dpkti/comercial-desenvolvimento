﻿namespace VDA991
{
    partial class FrmConsultaQuadrantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvQuadrante = new System.Windows.Forms.DataGridView();
            this.quadrante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdcontato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbQuadrante = new System.Windows.Forms.GroupBox();
            this.btnSair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadrante)).BeginInit();
            this.gbQuadrante.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvQuadrante
            // 
            this.dgvQuadrante.AllowUserToAddRows = false;
            this.dgvQuadrante.AllowUserToDeleteRows = false;
            this.dgvQuadrante.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvQuadrante.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvQuadrante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuadrante.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.quadrante,
            this.qtdcontato});
            this.dgvQuadrante.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvQuadrante.Location = new System.Drawing.Point(7, 19);
            this.dgvQuadrante.MultiSelect = false;
            this.dgvQuadrante.Name = "dgvQuadrante";
            this.dgvQuadrante.ReadOnly = true;
            this.dgvQuadrante.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvQuadrante.Size = new System.Drawing.Size(645, 280);
            this.dgvQuadrante.TabIndex = 0;
            this.dgvQuadrante.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dgvQuadrante_MouseUp);
            // 
            // quadrante
            // 
            this.quadrante.DataPropertyName = "quadrante";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            this.quadrante.DefaultCellStyle = dataGridViewCellStyle2;
            this.quadrante.HeaderText = "Quadrante";
            this.quadrante.Name = "quadrante";
            this.quadrante.ReadOnly = true;
            this.quadrante.Width = 300;
            // 
            // qtdcontato
            // 
            this.qtdcontato.DataPropertyName = "qtdcontato";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            this.qtdcontato.DefaultCellStyle = dataGridViewCellStyle3;
            this.qtdcontato.HeaderText = "Quantidade de Contato";
            this.qtdcontato.Name = "qtdcontato";
            this.qtdcontato.ReadOnly = true;
            this.qtdcontato.Width = 300;
            // 
            // gbQuadrante
            // 
            this.gbQuadrante.Controls.Add(this.btnSair);
            this.gbQuadrante.Controls.Add(this.dgvQuadrante);
            this.gbQuadrante.Location = new System.Drawing.Point(12, 12);
            this.gbQuadrante.Name = "gbQuadrante";
            this.gbQuadrante.Size = new System.Drawing.Size(658, 346);
            this.gbQuadrante.TabIndex = 1;
            this.gbQuadrante.TabStop = false;
            this.gbQuadrante.Text = "Quadrantes";
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(532, 305);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // FrmConsultaQuadrantes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(682, 370);
            this.Controls.Add(this.gbQuadrante);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmConsultaQuadrantes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta Quadrante";
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadrante)).EndInit();
            this.gbQuadrante.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvQuadrante;
        private System.Windows.Forms.GroupBox gbQuadrante;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.DataGridViewTextBoxColumn quadrante;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdcontato;
    }
}