﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using UtilGeralDPA;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmListaExcedentes : Form
    {
        public FrmListaExcedentes()
        {
            InitializeComponent();
        }

        private void FrmListaExcedentes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {
            
            try
            {
                List<RetornoExcedente> listaExcedentes = new List<RetornoExcedente>();
                FiltroPeriodo oFiltro = new FiltroPeriodo();


                oFiltro.DATA_INICIAL = dtInicio.Value;
                oFiltro.DATA_FINAL = dtFim.Value;

                if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
                {
                    oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);
                }
                else
                {
                    oFiltro.CODCOORD = null;
                }

                listaExcedentes = VendedorBO.BuscarListaExcedentes(oFiltro);
                dgvListExcedentes.DataSource = listaExcedentes;

                if (dgvListExcedentes.RowCount == 0)
                {
                    MessageBox.Show("Nenhum Registro Encontrado.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    btnExportarExcel.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha na consulta.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                //Creating DataTable
                DataTable dt = new DataTable();

                //Adding the Columns
                foreach (DataGridViewColumn column in dgvListExcedentes.Columns)
                {
                    dt.Columns.Add(column.HeaderText);
                }

                //Adding the Rows
                foreach (DataGridViewRow row in dgvListExcedentes.Rows)
                {
                    dt.Rows.Add();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }

                if (dgvListExcedentes.Rows.Count < 100)
                {
                    for (int j = dgvListExcedentes.Rows.Count; j <= (dgvListExcedentes.Rows.Count + 100); j++)
                    {
                        dt.Rows.Add();
                        for (int i = 0; i <= dgvListExcedentes.ColumnCount - 1; i++)
                        {
                            dt.Rows[j][i] = "";
                        }
                    }
                }

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Excel Documents (*.xls)|*.xls";
                DataSet ds = new DataSet("Teste");
                ds.Locale = System.Threading.Thread.CurrentThread.CurrentCulture;
                ds.Tables.Add(dt);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    ExcelLibrary.DataSetHelper.CreateWorkbook(sfd.FileName, ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                MessageBox.Show("Falha ao exportar excel.", ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

    }
}
