﻿namespace VDA991
{
    partial class FrmConsultaQuadrantesExcecao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpQuadrante = new System.Windows.Forms.GroupBox();
            this.btExcecaoSair = new System.Windows.Forms.Button();
            this.dgvQuadranteFilial = new System.Windows.Forms.DataGridView();
            this.btExcluir = new System.Windows.Forms.Button();
            this.filial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdcontatofilial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quadrante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdfilial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.excluir = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.grpQuadrante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadranteFilial)).BeginInit();
            this.SuspendLayout();
            // 
            // grpQuadrante
            // 
            this.grpQuadrante.Controls.Add(this.btExcluir);
            this.grpQuadrante.Controls.Add(this.btExcecaoSair);
            this.grpQuadrante.Controls.Add(this.dgvQuadranteFilial);
            this.grpQuadrante.Location = new System.Drawing.Point(2, 9);
            this.grpQuadrante.Name = "grpQuadrante";
            this.grpQuadrante.Size = new System.Drawing.Size(529, 254);
            this.grpQuadrante.TabIndex = 0;
            this.grpQuadrante.TabStop = false;
            this.grpQuadrante.Text = "Exceções";
            // 
            // btExcecaoSair
            // 
            this.btExcecaoSair.Image = global::VDA991.Properties.Resources.off;
            this.btExcecaoSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcecaoSair.Location = new System.Drawing.Point(403, 213);
            this.btExcecaoSair.Name = "btExcecaoSair";
            this.btExcecaoSair.Size = new System.Drawing.Size(120, 35);
            this.btExcecaoSair.TabIndex = 3;
            this.btExcecaoSair.Text = "&Sair";
            this.btExcecaoSair.UseVisualStyleBackColor = true;
            this.btExcecaoSair.Click += new System.EventHandler(this.btExcecaoSair_Click);
            // 
            // dgvQuadranteFilial
            // 
            this.dgvQuadranteFilial.AllowUserToAddRows = false;
            this.dgvQuadranteFilial.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvQuadranteFilial.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvQuadranteFilial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvQuadranteFilial.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.filial,
            this.qtdcontatofilial,
            this.quadrante,
            this.cdfilial,
            this.excluir});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvQuadranteFilial.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvQuadranteFilial.Location = new System.Drawing.Point(12, 22);
            this.dgvQuadranteFilial.Name = "dgvQuadranteFilial";
            this.dgvQuadranteFilial.Size = new System.Drawing.Size(511, 185);
            this.dgvQuadranteFilial.TabIndex = 2;
            // 
            // btExcluir
            // 
            this.btExcluir.Image = global::VDA991.Properties.Resources.cancel;
            this.btExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btExcluir.Location = new System.Drawing.Point(264, 213);
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(120, 35);
            this.btExcluir.TabIndex = 4;
            this.btExcluir.Text = "&Excluir";
            this.btExcluir.UseVisualStyleBackColor = true;
            this.btExcluir.Click += new System.EventHandler(this.btExcluir_Click);
            // 
            // filial
            // 
            this.filial.DataPropertyName = "nome_filial";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.filial.DefaultCellStyle = dataGridViewCellStyle2;
            this.filial.HeaderText = "Filial";
            this.filial.Name = "filial";
            this.filial.Width = 226;
            // 
            // qtdcontatofilial
            // 
            this.qtdcontatofilial.DataPropertyName = "qtdcontato";
            this.qtdcontatofilial.HeaderText = "Quantidade Contato";
            this.qtdcontatofilial.Name = "qtdcontatofilial";
            this.qtdcontatofilial.Width = 150;
            // 
            // quadrante
            // 
            this.quadrante.DataPropertyName = "quadrante";
            this.quadrante.HeaderText = "Quadrante";
            this.quadrante.Name = "quadrante";
            this.quadrante.Visible = false;
            // 
            // cdfilial
            // 
            this.cdfilial.DataPropertyName = "cdfilial";
            this.cdfilial.HeaderText = "cdfilial";
            this.cdfilial.Name = "cdfilial";
            this.cdfilial.Visible = false;
            // 
            // excluir
            // 
            this.excluir.HeaderText = "Excluir";
            this.excluir.Name = "excluir";
            this.excluir.Width = 92;
            // 
            // FrmConsultaQuadrantesExcecao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(543, 275);
            this.Controls.Add(this.grpQuadrante);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmConsultaQuadrantesExcecao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta Quadrante Exceção";
            this.grpQuadrante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvQuadranteFilial)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpQuadrante;
        private System.Windows.Forms.Button btExcecaoSair;
        private System.Windows.Forms.DataGridView dgvQuadranteFilial;
        private System.Windows.Forms.Button btExcluir;
        private System.Windows.Forms.DataGridViewTextBoxColumn filial;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdcontatofilial;
        private System.Windows.Forms.DataGridViewTextBoxColumn quadrante;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdfilial;
        private System.Windows.Forms.DataGridViewCheckBoxColumn excluir;
    }
}