﻿namespace VDA991
{
    partial class FrmExclusaoContatosPendenteVend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbExclCont = new System.Windows.Forms.GroupBox();
            this.chkSelTodos = new System.Windows.Forms.CheckBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.dgvContPend = new System.Windows.Forms.DataGridView();
            this.DATA_ROTEIRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CDFILIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FILIAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CDVENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VENDEDOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CDCLI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLIENTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QUADRANTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.excluir = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.gbBuscar = new System.Windows.Forms.GroupBox();
            this.lblAte = new System.Windows.Forms.Label();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.dtInicio = new System.Windows.Forms.DateTimePicker();
            this.lblDtRoteiro = new System.Windows.Forms.Label();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.lblFilial = new System.Windows.Forms.Label();
            this.cmbVendedor = new System.Windows.Forms.ComboBox();
            this.lblVendedor = new System.Windows.Forms.Label();
            this.gbExclCont.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContPend)).BeginInit();
            this.gbBuscar.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbExclCont
            // 
            this.gbExclCont.Controls.Add(this.chkSelTodos);
            this.gbExclCont.Controls.Add(this.btnSair);
            this.gbExclCont.Controls.Add(this.btnExcluir);
            this.gbExclCont.Controls.Add(this.dgvContPend);
            this.gbExclCont.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbExclCont.Location = new System.Drawing.Point(12, 106);
            this.gbExclCont.Name = "gbExclCont";
            this.gbExclCont.Size = new System.Drawing.Size(772, 357);
            this.gbExclCont.TabIndex = 0;
            this.gbExclCont.TabStop = false;
            this.gbExclCont.Text = "Exclusão de Contato Pendente por Vendedor";
            // 
            // chkSelTodos
            // 
            this.chkSelTodos.AutoSize = true;
            this.chkSelTodos.Location = new System.Drawing.Point(6, 317);
            this.chkSelTodos.Name = "chkSelTodos";
            this.chkSelTodos.Size = new System.Drawing.Size(109, 17);
            this.chkSelTodos.TabIndex = 7;
            this.chkSelTodos.Text = "Selecionar Todos";
            this.chkSelTodos.UseVisualStyleBackColor = true;
            this.chkSelTodos.CheckedChanged += new System.EventHandler(this.chkSelTodos_CheckedChanged);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(647, 316);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 6;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnExcluir
            // 
            this.btnExcluir.Enabled = false;
            this.btnExcluir.Image = global::VDA991.Properties.Resources.cancel;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(521, 316);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(120, 35);
            this.btnExcluir.TabIndex = 5;
            this.btnExcluir.Text = "&Excluir";
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // dgvContPend
            // 
            this.dgvContPend.AllowUserToAddRows = false;
            this.dgvContPend.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvContPend.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvContPend.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContPend.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DATA_ROTEIRO,
            this.CDFILIAL,
            this.FILIAL,
            this.CDVENDEDOR,
            this.VENDEDOR,
            this.CDCLI,
            this.CLIENTE,
            this.QUADRANTE,
            this.excluir});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvContPend.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvContPend.Location = new System.Drawing.Point(6, 19);
            this.dgvContPend.Name = "dgvContPend";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvContPend.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvContPend.RowHeadersWidth = 30;
            this.dgvContPend.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvContPend.Size = new System.Drawing.Size(760, 291);
            this.dgvContPend.TabIndex = 1;
            // 
            // DATA_ROTEIRO
            // 
            this.DATA_ROTEIRO.DataPropertyName = "DATA_ROTEIRO";
            this.DATA_ROTEIRO.HeaderText = "Data Roteiro";
            this.DATA_ROTEIRO.Name = "DATA_ROTEIRO";
            this.DATA_ROTEIRO.Width = 70;
            // 
            // CDFILIAL
            // 
            this.CDFILIAL.DataPropertyName = "CDFILIAL";
            this.CDFILIAL.HeaderText = "CDFILIAL";
            this.CDFILIAL.Name = "CDFILIAL";
            this.CDFILIAL.Visible = false;
            this.CDFILIAL.Width = 10;
            // 
            // FILIAL
            // 
            this.FILIAL.DataPropertyName = "FILIAL";
            this.FILIAL.HeaderText = "Filial";
            this.FILIAL.Name = "FILIAL";
            this.FILIAL.Width = 172;
            // 
            // CDVENDEDOR
            // 
            this.CDVENDEDOR.DataPropertyName = "CDVENDEDOR";
            this.CDVENDEDOR.HeaderText = "CDVENDEDOR";
            this.CDVENDEDOR.Name = "CDVENDEDOR";
            this.CDVENDEDOR.Visible = false;
            this.CDVENDEDOR.Width = 10;
            // 
            // VENDEDOR
            // 
            this.VENDEDOR.DataPropertyName = "VENDEDOR";
            this.VENDEDOR.HeaderText = "Vendedor";
            this.VENDEDOR.Name = "VENDEDOR";
            this.VENDEDOR.Width = 180;
            // 
            // CDCLI
            // 
            this.CDCLI.DataPropertyName = "CDCLI";
            this.CDCLI.HeaderText = "CDCLI";
            this.CDCLI.Name = "CDCLI";
            this.CDCLI.Visible = false;
            this.CDCLI.Width = 10;
            // 
            // CLIENTE
            // 
            this.CLIENTE.DataPropertyName = "CLIENTE";
            this.CLIENTE.HeaderText = "Cliente";
            this.CLIENTE.Name = "CLIENTE";
            this.CLIENTE.Width = 180;
            // 
            // QUADRANTE
            // 
            this.QUADRANTE.DataPropertyName = "QUADRANTE";
            this.QUADRANTE.HeaderText = "Quadrante";
            this.QUADRANTE.Name = "QUADRANTE";
            this.QUADRANTE.Width = 65;
            // 
            // excluir
            // 
            this.excluir.HeaderText = "Excluir";
            this.excluir.Name = "excluir";
            this.excluir.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.excluir.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.excluir.Width = 61;
            // 
            // gbBuscar
            // 
            this.gbBuscar.Controls.Add(this.lblAte);
            this.gbBuscar.Controls.Add(this.dtFinal);
            this.gbBuscar.Controls.Add(this.btnConsultar);
            this.gbBuscar.Controls.Add(this.dtInicio);
            this.gbBuscar.Controls.Add(this.lblDtRoteiro);
            this.gbBuscar.Controls.Add(this.cmbFilial);
            this.gbBuscar.Controls.Add(this.lblFilial);
            this.gbBuscar.Controls.Add(this.cmbVendedor);
            this.gbBuscar.Controls.Add(this.lblVendedor);
            this.gbBuscar.Location = new System.Drawing.Point(12, 12);
            this.gbBuscar.Name = "gbBuscar";
            this.gbBuscar.Size = new System.Drawing.Size(772, 88);
            this.gbBuscar.TabIndex = 0;
            this.gbBuscar.TabStop = false;
            this.gbBuscar.Text = "Consultar Contatos Pendentes";
            // 
            // lblAte
            // 
            this.lblAte.AutoSize = true;
            this.lblAte.Location = new System.Drawing.Point(186, 55);
            this.lblAte.Name = "lblAte";
            this.lblAte.Size = new System.Drawing.Size(22, 13);
            this.lblAte.TabIndex = 8;
            this.lblAte.Text = "até";
            // 
            // dtFinal
            // 
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(214, 52);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(100, 20);
            this.dtFinal.TabIndex = 3;
            // 
            // btnConsultar
            // 
            this.btnConsultar.Image = global::VDA991.Properties.Resources.find;
            this.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConsultar.Location = new System.Drawing.Point(646, 47);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(120, 35);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "&Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // dtInicio
            // 
            this.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicio.Location = new System.Drawing.Point(80, 52);
            this.dtInicio.Name = "dtInicio";
            this.dtInicio.Size = new System.Drawing.Size(100, 20);
            this.dtInicio.TabIndex = 2;
            // 
            // lblDtRoteiro
            // 
            this.lblDtRoteiro.AutoSize = true;
            this.lblDtRoteiro.Location = new System.Drawing.Point(4, 55);
            this.lblDtRoteiro.Name = "lblDtRoteiro";
            this.lblDtRoteiro.Size = new System.Drawing.Size(70, 13);
            this.lblDtRoteiro.TabIndex = 4;
            this.lblDtRoteiro.Text = "Data Roteiro:";
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(498, 20);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(268, 21);
            this.cmbFilial.TabIndex = 1;
            // 
            // lblFilial
            // 
            this.lblFilial.AutoSize = true;
            this.lblFilial.Location = new System.Drawing.Point(462, 23);
            this.lblFilial.Name = "lblFilial";
            this.lblFilial.Size = new System.Drawing.Size(30, 13);
            this.lblFilial.TabIndex = 2;
            this.lblFilial.Text = "Filial:";
            // 
            // cmbVendedor
            // 
            this.cmbVendedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVendedor.FormattingEnabled = true;
            this.cmbVendedor.Location = new System.Drawing.Point(80, 20);
            this.cmbVendedor.Name = "cmbVendedor";
            this.cmbVendedor.Size = new System.Drawing.Size(361, 21);
            this.cmbVendedor.TabIndex = 0;
            // 
            // lblVendedor
            // 
            this.lblVendedor.AutoSize = true;
            this.lblVendedor.Location = new System.Drawing.Point(7, 23);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(56, 13);
            this.lblVendedor.TabIndex = 0;
            this.lblVendedor.Text = "Vendedor:";
            // 
            // FrmExclusaoContatosPendenteVend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 475);
            this.Controls.Add(this.gbExclCont);
            this.Controls.Add(this.gbBuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "FrmExclusaoContatosPendenteVend";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exclusão de Contatos Pendentes";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmExclusaoContatosPendenteVend_KeyPress);
            this.gbExclCont.ResumeLayout(false);
            this.gbExclCont.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContPend)).EndInit();
            this.gbBuscar.ResumeLayout(false);
            this.gbBuscar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbExclCont;
        private System.Windows.Forms.GroupBox gbBuscar;
        private System.Windows.Forms.DataGridView dgvContPend;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.DateTimePicker dtInicio;
        private System.Windows.Forms.Label lblDtRoteiro;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label lblFilial;
        private System.Windows.Forms.ComboBox cmbVendedor;
        private System.Windows.Forms.Label lblVendedor;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.Label lblAte;
        private System.Windows.Forms.CheckBox chkSelTodos;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_ROTEIRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CDFILIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn FILIAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn CDVENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn VENDEDOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn CDCLI;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLIENTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn QUADRANTE;
        private System.Windows.Forms.DataGridViewCheckBoxColumn excluir;
    }
}