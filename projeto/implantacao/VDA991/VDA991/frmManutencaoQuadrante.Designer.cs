﻿namespace VDA991
{
    partial class FrmManutencaoQuadrante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblQuadrante = new System.Windows.Forms.Label();
            this.lblContato = new System.Windows.Forms.Label();
            this.txtQtdContato = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbQuadrante = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblQuadrante
            // 
            this.lblQuadrante.AutoSize = true;
            this.lblQuadrante.Location = new System.Drawing.Point(11, 31);
            this.lblQuadrante.Name = "lblQuadrante";
            this.lblQuadrante.Size = new System.Drawing.Size(60, 13);
            this.lblQuadrante.TabIndex = 0;
            this.lblQuadrante.Text = "Quadrante:";
            // 
            // lblContato
            // 
            this.lblContato.AutoSize = true;
            this.lblContato.Location = new System.Drawing.Point(11, 68);
            this.lblContato.Name = "lblContato";
            this.lblContato.Size = new System.Drawing.Size(120, 13);
            this.lblContato.TabIndex = 5;
            this.lblContato.Text = "Quantidade de Contato:";
            // 
            // txtQtdContato
            // 
            this.txtQtdContato.Location = new System.Drawing.Point(137, 65);
            this.txtQtdContato.MaxLength = 4;
            this.txtQtdContato.Name = "txtQtdContato";
            this.txtQtdContato.Size = new System.Drawing.Size(127, 20);
            this.txtQtdContato.TabIndex = 6;
            this.txtQtdContato.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtdContato_KeyPress);
            this.txtQtdContato.Leave += new System.EventHandler(this.txtQtdContato_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbQuadrante);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.txtQtdContato);
            this.groupBox1.Controls.Add(this.lblContato);
            this.groupBox1.Controls.Add(this.lblQuadrante);
            this.groupBox1.Location = new System.Drawing.Point(15, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(273, 168);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alterar Quantidade de Contato";
            // 
            // cmbQuadrante
            // 
            this.cmbQuadrante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQuadrante.FormattingEnabled = true;
            this.cmbQuadrante.Location = new System.Drawing.Point(78, 28);
            this.cmbQuadrante.Name = "cmbQuadrante";
            this.cmbQuadrante.Size = new System.Drawing.Size(186, 21);
            this.cmbQuadrante.TabIndex = 9;
            this.cmbQuadrante.SelectedIndexChanged += new System.EventHandler(this.cmbQuadrante_SelectedIndexChanged);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::VDA991.Properties.Resources.cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(144, 112);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(120, 35);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Image = global::VDA991.Properties.Resources.ok;
            this.btnSalvar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalvar.Location = new System.Drawing.Point(14, 112);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(120, 35);
            this.btnSalvar.TabIndex = 7;
            this.btnSalvar.Text = "&Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // FrmManutencaoQuadrante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(301, 196);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmManutencaoQuadrante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manutenção Quadrante";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblQuadrante;
        private System.Windows.Forms.Label lblContato;
        private System.Windows.Forms.TextBox txtQtdContato;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.ComboBox cmbQuadrante;
    }
}