﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Entities;
using Business;

namespace VDA991
{
    public partial class FrmConsultaNomeCliente : Form
    {
        RetornoCliente oRetorno = new RetornoCliente();
        
        public FrmConsultaNomeCliente()
        {
            InitializeComponent();

            GlobalBE.FiltroRetorno = oRetorno;

            txtNome.Focus();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {

            if (!String.IsNullOrEmpty(txtNome.Text))
            {
                FiltroConsulta oFiltro = new FiltroConsulta();

                oFiltro.NOMECLI = txtNome.Text;

                List<RetornoCliente> listaClientes = new List<RetornoCliente>();
                listaClientes = ClienteBO.BuscarClientes(oFiltro);
                dgvCliente.DataSource = listaClientes;

                if (dgvCliente.RowCount == 0)
                {
                    MessageBox.Show("Nenhum Registro Encontrado.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else
            {
                string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Nome");
                MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void dgvCliente_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            oRetorno.CDCLI = Convert.ToInt64(dgvCliente.CurrentRow.Cells[0].Value);
            oRetorno.NOMECLI = dgvCliente.CurrentRow.Cells[1].Value.ToString();
            oRetorno.CNPJ = Convert.ToInt64(dgvCliente.CurrentRow.Cells[2].Value);

            GlobalBE.FiltroRetorno = oRetorno;

            this.Close();

        }

        private void FrmConsultaNomeCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
            }
        }
    }
}
