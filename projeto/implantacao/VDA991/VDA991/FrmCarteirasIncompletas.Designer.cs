﻿namespace VDA991
{
    partial class FrmCarteirasIncompletas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbCartIncomp = new System.Windows.Forms.GroupBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.dgvVendedores = new System.Windows.Forms.DataGridView();
            this.gbConsulta = new System.Windows.Forms.GroupBox();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtInicio = new System.Windows.Forms.DateTimePicker();
            this.dtprocess = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cdvendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdTotalCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdClientesAlocados = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtdClientesFaltantes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbCartIncomp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedores)).BeginInit();
            this.gbConsulta.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCartIncomp
            // 
            this.gbCartIncomp.Controls.Add(this.btnSair);
            this.gbCartIncomp.Controls.Add(this.btnExportarExcel);
            this.gbCartIncomp.Controls.Add(this.dgvVendedores);
            this.gbCartIncomp.Controls.Add(this.gbConsulta);
            this.gbCartIncomp.Location = new System.Drawing.Point(4, 13);
            this.gbCartIncomp.Name = "gbCartIncomp";
            this.gbCartIncomp.Size = new System.Drawing.Size(836, 451);
            this.gbCartIncomp.TabIndex = 0;
            this.gbCartIncomp.TabStop = false;
            this.gbCartIncomp.Text = "Carteiras Incompletas";
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(710, 410);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 3;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Enabled = false;
            this.btnExportarExcel.Image = global::VDA991.Properties.Resources.xls;
            this.btnExportarExcel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnExportarExcel.Location = new System.Drawing.Point(557, 410);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(147, 35);
            this.btnExportarExcel.TabIndex = 2;
            this.btnExportarExcel.Text = "&Exportar Excel";
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // dgvVendedores
            // 
            this.dgvVendedores.AllowUserToAddRows = false;
            this.dgvVendedores.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVendedores.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVendedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVendedores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dtprocess,
            this.cdvendedor,
            this.vendedor,
            this.qtdTotalCliente,
            this.qtdClientesAlocados,
            this.qtdClientesFaltantes});
            this.dgvVendedores.Location = new System.Drawing.Point(7, 82);
            this.dgvVendedores.Name = "dgvVendedores";
            this.dgvVendedores.ReadOnly = true;
            this.dgvVendedores.RowHeadersWidth = 30;
            this.dgvVendedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVendedores.Size = new System.Drawing.Size(823, 322);
            this.dgvVendedores.TabIndex = 1;
            this.dgvVendedores.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVendedores_CellDoubleClick);
            // 
            // gbConsulta
            // 
            this.gbConsulta.Controls.Add(this.btnConsulta);
            this.gbConsulta.Controls.Add(this.label1);
            this.gbConsulta.Controls.Add(this.dtInicio);
            this.gbConsulta.Location = new System.Drawing.Point(7, 20);
            this.gbConsulta.Name = "gbConsulta";
            this.gbConsulta.Size = new System.Drawing.Size(823, 55);
            this.gbConsulta.TabIndex = 0;
            this.gbConsulta.TabStop = false;
            this.gbConsulta.Text = "Consulta";
            // 
            // btnConsulta
            // 
            this.btnConsulta.Image = global::VDA991.Properties.Resources.find;
            this.btnConsulta.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnConsulta.Location = new System.Drawing.Point(697, 14);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(120, 35);
            this.btnConsulta.TabIndex = 1;
            this.btnConsulta.Text = "&Consulta";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Roteiro:";
            // 
            // dtInicio
            // 
            this.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicio.Location = new System.Drawing.Point(82, 22);
            this.dtInicio.Name = "dtInicio";
            this.dtInicio.Size = new System.Drawing.Size(102, 20);
            this.dtInicio.TabIndex = 0;
            // 
            // dtprocess
            // 
            this.dtprocess.DataPropertyName = "dtprocess";
            this.dtprocess.HeaderText = "dtprocess";
            this.dtprocess.Name = "dtprocess";
            this.dtprocess.ReadOnly = true;
            this.dtprocess.Visible = false;
            this.dtprocess.Width = 5;
            // 
            // cdvendedor
            // 
            this.cdvendedor.DataPropertyName = "cdvendedor";
            this.cdvendedor.HeaderText = "cdvendedor";
            this.cdvendedor.Name = "cdvendedor";
            this.cdvendedor.ReadOnly = true;
            this.cdvendedor.Visible = false;
            this.cdvendedor.Width = 5;
            // 
            // vendedor
            // 
            this.vendedor.DataPropertyName = "vendedor";
            this.vendedor.HeaderText = "Vendedor";
            this.vendedor.Name = "vendedor";
            this.vendedor.ReadOnly = true;
            this.vendedor.Width = 400;
            // 
            // qtdTotalCliente
            // 
            this.qtdTotalCliente.DataPropertyName = "qtde_clientes";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.qtdTotalCliente.DefaultCellStyle = dataGridViewCellStyle2;
            this.qtdTotalCliente.HeaderText = "Qtde Total de Clientes";
            this.qtdTotalCliente.Name = "qtdTotalCliente";
            this.qtdTotalCliente.ReadOnly = true;
            this.qtdTotalCliente.Width = 130;
            // 
            // qtdClientesAlocados
            // 
            this.qtdClientesAlocados.DataPropertyName = "qtde_alocados";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.qtdClientesAlocados.DefaultCellStyle = dataGridViewCellStyle3;
            this.qtdClientesAlocados.HeaderText = "Qtde Clientes Alocados no Roteiro";
            this.qtdClientesAlocados.Name = "qtdClientesAlocados";
            this.qtdClientesAlocados.ReadOnly = true;
            this.qtdClientesAlocados.Width = 130;
            // 
            // qtdClientesFaltantes
            // 
            this.qtdClientesFaltantes.DataPropertyName = "qtde_faltantes";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.qtdClientesFaltantes.DefaultCellStyle = dataGridViewCellStyle4;
            this.qtdClientesFaltantes.HeaderText = "Qtde Clientes Faltantes";
            this.qtdClientesFaltantes.Name = "qtdClientesFaltantes";
            this.qtdClientesFaltantes.ReadOnly = true;
            this.qtdClientesFaltantes.Width = 130;
            // 
            // FrmCarteirasIncompletas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 473);
            this.Controls.Add(this.gbCartIncomp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "FrmCarteirasIncompletas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carteiras Incompletas";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCarteirasIncompletas_KeyDown);
            this.gbCartIncomp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVendedores)).EndInit();
            this.gbConsulta.ResumeLayout(false);
            this.gbConsulta.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCartIncomp;
        private System.Windows.Forms.GroupBox gbConsulta;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtInicio;
        private System.Windows.Forms.DataGridView dgvVendedores;
        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtprocess;
        private System.Windows.Forms.DataGridViewTextBoxColumn cdvendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdTotalCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdClientesAlocados;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtdClientesFaltantes;
    }
}