﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmConsultaQuadrantes : Form
    {
        public FrmConsultaQuadrantes()
        {
            InitializeComponent();
            CarregarQuadrante();

        }

        private void dgvQuadrante_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                switch (e.Button)
                {
                    case MouseButtons.Right:

                        ContextMenu myContextMenu = new ContextMenu();

                        MenuItem menuItem0 = new MenuItem("Consultar Exceções");

                        myContextMenu.MenuItems.Add(menuItem0);

                        if (dgvQuadrante.SelectedRows.Count == 1)
                        {
                            myContextMenu.MenuItems.Add(menuItem0);

                            menuItem0.Click += new EventHandler(delegate(Object o, EventArgs a)
                            {

                                FiltroQuadrante oFiltro = new FiltroQuadrante();
                                oFiltro.QUADRANTE = dgvQuadrante.CurrentRow.Cells[0].Value.ToString();

                                if (GlobalBE.usuarioLogado.ACESSOCOORDENADOR)
                                {
                                    oFiltro.CODCOORD = Convert.ToInt64(GlobalBE.usuarioLogado.LOGIN);;
                                }
                                else
                                {
                                    oFiltro.CODCOORD = null;
                                }

                                FrmConsultaQuadrantesExcecao oFrmConsultaQuadrantesExcecao = new FrmConsultaQuadrantesExcecao(oFiltro);
                                oFrmConsultaQuadrantesExcecao.StartPosition = FormStartPosition.CenterParent;
                                oFrmConsultaQuadrantesExcecao.ShowDialog();
                            
                            });
                        }


                        myContextMenu.Show(dgvQuadrante, e.Location, LeftRightAlignment.Right);

                        break;
                }
                
            }
            catch (Exception ex)
            {
            }

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregarQuadrante()
        {
            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();
            listaQuadrante = QuadranteBO.BuscarQuadrante();
            dgvQuadrante.DataSource = listaQuadrante;
        }

    
    }
}
