﻿namespace VDA991
{
    partial class FrmExcecaoQuadrante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnInserirAlterar = new System.Windows.Forms.Button();
            this.txtQtdContato = new System.Windows.Forms.TextBox();
            this.lblQtdContato = new System.Windows.Forms.Label();
            this.cmbFilial = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbQuadrante = new System.Windows.Forms.ComboBox();
            this.lblQuadrante = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnInserirAlterar);
            this.groupBox1.Controls.Add(this.txtQtdContato);
            this.groupBox1.Controls.Add(this.lblQtdContato);
            this.groupBox1.Controls.Add(this.cmbFilial);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbQuadrante);
            this.groupBox1.Controls.Add(this.lblQuadrante);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 154);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inserir/Alterar Exceção";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::VDA991.Properties.Resources.cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(201, 111);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(116, 35);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnInserirAlterar
            // 
            this.btnInserirAlterar.Image = global::VDA991.Properties.Resources.ok;
            this.btnInserirAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInserirAlterar.Location = new System.Drawing.Point(75, 111);
            this.btnInserirAlterar.Name = "btnInserirAlterar";
            this.btnInserirAlterar.Size = new System.Drawing.Size(116, 35);
            this.btnInserirAlterar.TabIndex = 6;
            this.btnInserirAlterar.Text = "       Inserir/Alterar";
            this.btnInserirAlterar.UseVisualStyleBackColor = true;
            this.btnInserirAlterar.Click += new System.EventHandler(this.btnInserirAlterar_Click);
            // 
            // txtQtdContato
            // 
            this.txtQtdContato.Location = new System.Drawing.Point(119, 73);
            this.txtQtdContato.MaxLength = 4;
            this.txtQtdContato.Name = "txtQtdContato";
            this.txtQtdContato.Size = new System.Drawing.Size(76, 20);
            this.txtQtdContato.TabIndex = 5;
            this.txtQtdContato.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQtdContato_KeyPress);
            this.txtQtdContato.Leave += new System.EventHandler(this.txtQdtContato_Leave);
            // 
            // lblQtdContato
            // 
            this.lblQtdContato.AutoSize = true;
            this.lblQtdContato.Location = new System.Drawing.Point(10, 76);
            this.lblQtdContato.Name = "lblQtdContato";
            this.lblQtdContato.Size = new System.Drawing.Size(105, 13);
            this.lblQtdContato.TabIndex = 4;
            this.lblQtdContato.Text = "Quantidade Contato:";
            // 
            // cmbFilial
            // 
            this.cmbFilial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFilial.FormattingEnabled = true;
            this.cmbFilial.Location = new System.Drawing.Point(75, 46);
            this.cmbFilial.Name = "cmbFilial";
            this.cmbFilial.Size = new System.Drawing.Size(242, 21);
            this.cmbFilial.TabIndex = 3;
            this.cmbFilial.SelectedIndexChanged += new System.EventHandler(this.cmbFilial_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Filial:";
            // 
            // cmbQuadrante
            // 
            this.cmbQuadrante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQuadrante.FormattingEnabled = true;
            this.cmbQuadrante.Location = new System.Drawing.Point(75, 19);
            this.cmbQuadrante.Name = "cmbQuadrante";
            this.cmbQuadrante.Size = new System.Drawing.Size(242, 21);
            this.cmbQuadrante.TabIndex = 1;
            this.cmbQuadrante.SelectedIndexChanged += new System.EventHandler(this.cmbQuadrante_SelectedIndexChanged);
            // 
            // lblQuadrante
            // 
            this.lblQuadrante.AutoSize = true;
            this.lblQuadrante.Location = new System.Drawing.Point(11, 22);
            this.lblQuadrante.Name = "lblQuadrante";
            this.lblQuadrante.Size = new System.Drawing.Size(60, 13);
            this.lblQuadrante.TabIndex = 0;
            this.lblQuadrante.Text = "Quadrante:";
            // 
            // FrmExcecaoQuadrante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(354, 181);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmExcecaoQuadrante";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastrar Exceção Quadrante";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbQuadrante;
        private System.Windows.Forms.Label lblQuadrante;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnInserirAlterar;
        private System.Windows.Forms.TextBox txtQtdContato;
        private System.Windows.Forms.Label lblQtdContato;
        private System.Windows.Forms.ComboBox cmbFilial;
        private System.Windows.Forms.Label label1;
    }
}