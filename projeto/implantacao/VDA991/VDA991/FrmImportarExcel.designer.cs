﻿namespace VDA991
{
    partial class frmImportarExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.oFDPlanilha = new System.Windows.Forms.OpenFileDialog();
            this.lbStatus = new System.Windows.Forms.Label();
            this.lbStatusProc = new System.Windows.Forms.Label();
            this.bgWorkerRoteirizacao = new System.ComponentModel.BackgroundWorker();
            this.lbQtdInserido = new System.Windows.Forms.Label();
            this.lbQtdInseridoAlt = new System.Windows.Forms.Label();
            this.grbArquivo = new System.Windows.Forms.GroupBox();
            this.btnImportar = new System.Windows.Forms.Button();
            this.btnAbrir = new System.Windows.Forms.Button();
            this.lbTipoArquivo = new System.Windows.Forms.Label();
            this.lbArquivo = new System.Windows.Forms.Label();
            this.txtCaminhoArquivo = new System.Windows.Forms.TextBox();
            this.grbArquivo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lbStatus.Location = new System.Drawing.Point(246, 119);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(142, 25);
            this.lbStatus.TabIndex = 11;
            this.lbStatus.Text = "Processando...";
            this.lbStatus.Visible = false;
            // 
            // lbStatusProc
            // 
            this.lbStatusProc.AutoSize = true;
            this.lbStatusProc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusProc.Location = new System.Drawing.Point(16, 119);
            this.lbStatusProc.Name = "lbStatusProc";
            this.lbStatusProc.Size = new System.Drawing.Size(242, 25);
            this.lbStatusProc.TabIndex = 10;
            this.lbStatusProc.Text = "Status do Processamento:";
            this.lbStatusProc.Visible = false;
            // 
            // bgWorkerRoteirizacao
            // 
            this.bgWorkerRoteirizacao.WorkerReportsProgress = true;
            this.bgWorkerRoteirizacao.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerRoteirizacao_DoWork);
            this.bgWorkerRoteirizacao.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorkerRoteirizacao_ProgressChanged);
            this.bgWorkerRoteirizacao.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerRoteirizacao_RunWorkerCompleted);
            // 
            // lbQtdInserido
            // 
            this.lbQtdInserido.AutoSize = true;
            this.lbQtdInserido.Location = new System.Drawing.Point(12, 175);
            this.lbQtdInserido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbQtdInserido.Name = "lbQtdInserido";
            this.lbQtdInserido.Size = new System.Drawing.Size(104, 13);
            this.lbQtdInserido.TabIndex = 12;
            this.lbQtdInserido.Text = "Quantidade inserido:";
            // 
            // lbQtdInseridoAlt
            // 
            this.lbQtdInseridoAlt.AutoSize = true;
            this.lbQtdInseridoAlt.Location = new System.Drawing.Point(118, 175);
            this.lbQtdInseridoAlt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbQtdInseridoAlt.Name = "lbQtdInseridoAlt";
            this.lbQtdInseridoAlt.Size = new System.Drawing.Size(0, 13);
            this.lbQtdInseridoAlt.TabIndex = 13;
            // 
            // grbArquivo
            // 
            this.grbArquivo.Controls.Add(this.btnImportar);
            this.grbArquivo.Controls.Add(this.btnAbrir);
            this.grbArquivo.Controls.Add(this.lbTipoArquivo);
            this.grbArquivo.Controls.Add(this.lbArquivo);
            this.grbArquivo.Controls.Add(this.txtCaminhoArquivo);
            this.grbArquivo.Location = new System.Drawing.Point(12, 12);
            this.grbArquivo.Name = "grbArquivo";
            this.grbArquivo.Size = new System.Drawing.Size(589, 100);
            this.grbArquivo.TabIndex = 14;
            this.grbArquivo.TabStop = false;
            this.grbArquivo.Text = "Informe o Arquivo";
            // 
            // btnImportar
            // 
            this.btnImportar.Location = new System.Drawing.Point(495, 63);
            this.btnImportar.Margin = new System.Windows.Forms.Padding(2);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(75, 23);
            this.btnImportar.TabIndex = 14;
            this.btnImportar.Text = "Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.Location = new System.Drawing.Point(495, 35);
            this.btnAbrir.Margin = new System.Windows.Forms.Padding(2);
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(75, 23);
            this.btnAbrir.TabIndex = 13;
            this.btnAbrir.Text = "Abrir...";
            this.btnAbrir.UseVisualStyleBackColor = true;
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // lbTipoArquivo
            // 
            this.lbTipoArquivo.AutoSize = true;
            this.lbTipoArquivo.Location = new System.Drawing.Point(282, 57);
            this.lbTipoArquivo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTipoArquivo.Name = "lbTipoArquivo";
            this.lbTipoArquivo.Size = new System.Drawing.Size(205, 13);
            this.lbTipoArquivo.TabIndex = 12;
            this.lbTipoArquivo.Text = "Microsoft Excel (*.XLS;*.XLSX)|*.xls;*.xlsx\"";
            // 
            // lbArquivo
            // 
            this.lbArquivo.AutoSize = true;
            this.lbArquivo.Location = new System.Drawing.Point(14, 36);
            this.lbArquivo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbArquivo.Name = "lbArquivo";
            this.lbArquivo.Size = new System.Drawing.Size(46, 13);
            this.lbArquivo.TabIndex = 11;
            this.lbArquivo.Text = "Arquivo:";
            // 
            // txtCaminhoArquivo
            // 
            this.txtCaminhoArquivo.Location = new System.Drawing.Point(103, 34);
            this.txtCaminhoArquivo.Margin = new System.Windows.Forms.Padding(2);
            this.txtCaminhoArquivo.Name = "txtCaminhoArquivo";
            this.txtCaminhoArquivo.Size = new System.Drawing.Size(383, 20);
            this.txtCaminhoArquivo.TabIndex = 10;
            // 
            // frmImportarExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 199);
            this.Controls.Add(this.grbArquivo);
            this.Controls.Add(this.lbQtdInseridoAlt);
            this.Controls.Add(this.lbQtdInserido);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.lbStatusProc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmImportarExcel";
            this.Text = "Manutenção Cliente Exceção - Importação Planilha";
            this.grbArquivo.ResumeLayout(false);
            this.grbArquivo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog oFDPlanilha;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Label lbStatusProc;
        private System.ComponentModel.BackgroundWorker bgWorkerRoteirizacao;
        private System.Windows.Forms.Label lbQtdInserido;
        private System.Windows.Forms.Label lbQtdInseridoAlt;
        private System.Windows.Forms.GroupBox grbArquivo;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.Button btnAbrir;
        private System.Windows.Forms.Label lbTipoArquivo;
        private System.Windows.Forms.Label lbArquivo;
        private System.Windows.Forms.TextBox txtCaminhoArquivo;
    }
}