﻿namespace VDA991
{
    partial class FrmListaExcedentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbListExcedentes = new System.Windows.Forms.GroupBox();
            this.gbConsulta = new System.Windows.Forms.GroupBox();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.dtFim = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtInicio = new System.Windows.Forms.DateTimePicker();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnExportarExcel = new System.Windows.Forms.Button();
            this.dgvListExcedentes = new System.Windows.Forms.DataGridView();
            this.dataroteiro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quadrante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbListExcedentes.SuspendLayout();
            this.gbConsulta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListExcedentes)).BeginInit();
            this.SuspendLayout();
            // 
            // gbListExcedentes
            // 
            this.gbListExcedentes.Controls.Add(this.gbConsulta);
            this.gbListExcedentes.Controls.Add(this.btnSair);
            this.gbListExcedentes.Controls.Add(this.btnExportarExcel);
            this.gbListExcedentes.Controls.Add(this.dgvListExcedentes);
            this.gbListExcedentes.Location = new System.Drawing.Point(4, 15);
            this.gbListExcedentes.Name = "gbListExcedentes";
            this.gbListExcedentes.Size = new System.Drawing.Size(835, 448);
            this.gbListExcedentes.TabIndex = 0;
            this.gbListExcedentes.TabStop = false;
            this.gbListExcedentes.Text = "Lista de Excedentes";
            // 
            // gbConsulta
            // 
            this.gbConsulta.Controls.Add(this.btnConsulta);
            this.gbConsulta.Controls.Add(this.dtFim);
            this.gbConsulta.Controls.Add(this.label2);
            this.gbConsulta.Controls.Add(this.label1);
            this.gbConsulta.Controls.Add(this.dtInicio);
            this.gbConsulta.Location = new System.Drawing.Point(7, 19);
            this.gbConsulta.Name = "gbConsulta";
            this.gbConsulta.Size = new System.Drawing.Size(823, 55);
            this.gbConsulta.TabIndex = 3;
            this.gbConsulta.TabStop = false;
            this.gbConsulta.Text = "Consulta";
            // 
            // btnConsulta
            // 
            this.btnConsulta.Image = global::VDA991.Properties.Resources.find;
            this.btnConsulta.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btnConsulta.Location = new System.Drawing.Point(697, 14);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(120, 35);
            this.btnConsulta.TabIndex = 2;
            this.btnConsulta.Text = "&Consulta";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // dtFim
            // 
            this.dtFim.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFim.Location = new System.Drawing.Point(241, 23);
            this.dtFim.Name = "dtFim";
            this.dtFim.Size = new System.Drawing.Size(102, 20);
            this.dtFim.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(183, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data Fim:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Inicio:";
            // 
            // dtInicio
            // 
            this.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicio.Location = new System.Drawing.Point(74, 23);
            this.dtInicio.Name = "dtInicio";
            this.dtInicio.Size = new System.Drawing.Size(103, 20);
            this.dtInicio.TabIndex = 0;
            // 
            // btnSair
            // 
            this.btnSair.Image = global::VDA991.Properties.Resources.off;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(709, 407);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(120, 35);
            this.btnSair.TabIndex = 4;
            this.btnSair.Text = "&Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnExportarExcel
            // 
            this.btnExportarExcel.Enabled = false;
            this.btnExportarExcel.Image = global::VDA991.Properties.Resources.xls;
            this.btnExportarExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportarExcel.Location = new System.Drawing.Point(583, 407);
            this.btnExportarExcel.Name = "btnExportarExcel";
            this.btnExportarExcel.Size = new System.Drawing.Size(120, 35);
            this.btnExportarExcel.TabIndex = 3;
            this.btnExportarExcel.Text = "&Exportar Excel";
            this.btnExportarExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportarExcel.UseVisualStyleBackColor = true;
            this.btnExportarExcel.Click += new System.EventHandler(this.btnExportarExcel_Click);
            // 
            // dgvListExcedentes
            // 
            this.dgvListExcedentes.AllowUserToAddRows = false;
            this.dgvListExcedentes.AllowUserToDeleteRows = false;
            this.dgvListExcedentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListExcedentes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataroteiro,
            this.filial,
            this.vendedor,
            this.cliente,
            this.quadrante});
            this.dgvListExcedentes.Location = new System.Drawing.Point(7, 88);
            this.dgvListExcedentes.Name = "dgvListExcedentes";
            this.dgvListExcedentes.ReadOnly = true;
            this.dgvListExcedentes.RowHeadersWidth = 20;
            this.dgvListExcedentes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListExcedentes.Size = new System.Drawing.Size(822, 313);
            this.dgvListExcedentes.TabIndex = 0;
            // 
            // dataroteiro
            // 
            this.dataroteiro.DataPropertyName = "data_roteiro";
            this.dataroteiro.HeaderText = "Data Roteiro";
            this.dataroteiro.Name = "dataroteiro";
            this.dataroteiro.ReadOnly = true;
            this.dataroteiro.Width = 95;
            // 
            // filial
            // 
            this.filial.DataPropertyName = "filial";
            this.filial.HeaderText = "Filial";
            this.filial.Name = "filial";
            this.filial.ReadOnly = true;
            this.filial.Width = 215;
            // 
            // vendedor
            // 
            this.vendedor.DataPropertyName = "vendedor";
            this.vendedor.HeaderText = "Vendedor";
            this.vendedor.Name = "vendedor";
            this.vendedor.ReadOnly = true;
            this.vendedor.Width = 215;
            // 
            // cliente
            // 
            this.cliente.DataPropertyName = "cliente";
            this.cliente.HeaderText = "Cliente";
            this.cliente.Name = "cliente";
            this.cliente.ReadOnly = true;
            this.cliente.Width = 215;
            // 
            // quadrante
            // 
            this.quadrante.DataPropertyName = "quadrante";
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.quadrante.DefaultCellStyle = dataGridViewCellStyle1;
            this.quadrante.HeaderText = "Quadrante";
            this.quadrante.Name = "quadrante";
            this.quadrante.ReadOnly = true;
            this.quadrante.Width = 60;
            // 
            // FrmListaExcedentes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 475);
            this.Controls.Add(this.gbListExcedentes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "FrmListaExcedentes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lista de Excedentes";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmListaExcedentes_KeyDown);
            this.gbListExcedentes.ResumeLayout(false);
            this.gbConsulta.ResumeLayout(false);
            this.gbConsulta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListExcedentes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbListExcedentes;
        private System.Windows.Forms.DataGridView dgvListExcedentes;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnExportarExcel;
        private System.Windows.Forms.GroupBox gbConsulta;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.DateTimePicker dtFim;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtInicio;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataroteiro;
        private System.Windows.Forms.DataGridViewTextBoxColumn filial;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn quadrante;
    }
}