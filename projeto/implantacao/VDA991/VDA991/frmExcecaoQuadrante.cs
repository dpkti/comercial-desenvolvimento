﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmExcecaoQuadrante : Form
    {
        public FrmExcecaoQuadrante()
        {
            InitializeComponent();
            CarregarComboQuadrante(cmbQuadrante);
            CarregarComboFilial(cmbFilial);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregarComboQuadrante(ComboBox cmbQuadrante)
        {

            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();

            QuadranteBE quadrante = new QuadranteBE();
            quadrante.QTDCONTATO = 0;
            quadrante.QUADRANTE = "Selecione";

            cmbQuadrante.DataSource = null;
            cmbQuadrante.Items.Clear();
            cmbQuadrante.DisplayMember = "QUADRANTE";
            cmbQuadrante.ValueMember = "QUADRANTE";

            try
            {
                listaQuadrante = QuadranteBO.BuscarQuadrante();
                listaQuadrante.Insert(0, quadrante);
                cmbQuadrante.DataSource = listaQuadrante;
                cmbQuadrante.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboQuadrante, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void CarregarComboFilial(ComboBox cmbFilial)
        {

            List<FilialBE> listaFilial = new List<FilialBE>();

            FilialBE filial = new FilialBE();
            filial.COD_FILIAL = 0;
            filial.NOME_FILIAL = "Selecione";

            cmbFilial.DataSource = null;
            cmbFilial.Items.Clear();
            cmbFilial.DisplayMember = "NOME_FILIAL";
            cmbFilial.ValueMember = "COD_FILIAL";

            try
            {
                listaFilial = FilialBO.BuscarFilial();
                listaFilial.Insert(0, filial);
                cmbFilial.DataSource = listaFilial;
                cmbFilial.SelectedIndex = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", ResourceRoteirizacao.ErroCarregarComboFilial, ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void txtQdtContato_Leave(object sender, EventArgs e)
        {
            try
            {
                double valor = 0.0d;
                if (Double.TryParse(txtQtdContato.Text, NumberStyles.Currency, null, out valor))
                {
                    txtQtdContato.Text = valor.ToString("");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtQtdContato_Leave.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

        }

        private void txtQtdContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == '.' || e.KeyChar == ',')
                {
                    e.KeyChar = ',';
                    if (txtQtdContato.Text.Contains(","))
                        e.Handled = true;
                }
                else if (!char.IsNumber(e.KeyChar) && !(e.KeyChar == (char)Keys.Back))
                {
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro no metodo txtQtdContato_KeyPress.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void btnInserirAlterar_Click(object sender, EventArgs e)
        {

            //string valorcampo;
            //valorcampo = txtQtdContato.Text;

            try
            {
                if (QuadranteBO.ValidaValoresQuadrante(txtQtdContato.Text) == true)
                {

                    QuadranteEXC oFiltro = new QuadranteEXC();
                    if (cmbQuadrante.SelectedIndex == 0 || cmbQuadrante.SelectedIndex == -1)
                    {
                        string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Quadrante");
                        MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else if (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1)
                    {
                        string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoSelecionado, "Filial");
                        MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(txtQtdContato.Text))
                        {
                            string mensagem = String.Format(ResourceRoteirizacao.Erro_CampoObrigatorioNaoPreenchido, "Quantidade Contato");
                            MessageBox.Show(mensagem, VDA991.ResourceRoteirizacao.Titulo_MessageBoxErro, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;

                        }
                        else
                        {

                            oFiltro.QUADRANTE = cmbQuadrante.Text;
                            oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);
                            oFiltro.QTDCONTATO = string.IsNullOrEmpty(txtQtdContato.Text) ? 0 : Convert.ToDouble(txtQtdContato.Text);

                            QuadranteBO.InserirAlterarQuadranteExcecao(oFiltro);

                            if (btnInserirAlterar.Text == "Inserir")
                            {
                                MessageBox.Show("Quadrante Exceção Incluído com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else 
                            {
                                MessageBox.Show("Quadrante Exceção Alterado com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                            CarregarComboQuadrante(cmbQuadrante);
                            CarregarComboFilial(cmbFilial);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(String.Format("Qtde Contato deve estar entre (0 - 0.25 - 0.50 - 1 a 5)"), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao salvar as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }

        private void ValidaCampos()
        {
            QuadranteEXC oFiltro = new QuadranteEXC();
            List<QuadranteEXC> listaQuadranteFilial = new List<QuadranteEXC>();

            if ((cmbQuadrante.SelectedIndex == 0 || cmbQuadrante.SelectedIndex == -1) ||
                (cmbFilial.SelectedIndex == 0 || cmbFilial.SelectedIndex == -1))
            {
                btnInserirAlterar.Text = "       Inserir/Alterar";
                txtQtdContato.Text = "";
                txtQtdContato.Enabled = false;
            }
            else
            {
                txtQtdContato.Text = "";
                txtQtdContato.Enabled = true;

                oFiltro.QUADRANTE = cmbQuadrante.Text;
                oFiltro.CDFILIAL = Convert.ToInt32(cmbFilial.SelectedValue);

                listaQuadranteFilial = QuadranteBO.BuscarQuadranteFilialExcecao(oFiltro);

                if (listaQuadranteFilial.Count == 0)
                {
                    btnInserirAlterar.Text = "Inserir";
                }
                else
                {
                    btnInserirAlterar.Text = "Alterar";
                    txtQtdContato.Text = listaQuadranteFilial[0].QTDCONTATO.ToString();
                }
                
                
            }
        
        }

        private void cmbQuadrante_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidaCampos();

        }

        private void cmbFilial_SelectedIndexChanged(object sender, EventArgs e)
        {
            ValidaCampos();
        }
    
    }
}
