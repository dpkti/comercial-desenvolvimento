﻿using System;
using System.Windows.Forms;
using UtilGeralDPA;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Globalization;
using Business;
using Entities;

namespace VDA991
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            txtUsuario.Select();
            this.CenterToScreen();

            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ",";
            customCulture.NumberFormat.NumberGroupSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            string versao = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersao.Text = "Versão: " + versao.Substring(0, versao.LastIndexOf('.'));
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            btnEntrar.Select();
            this.Refresh();

            Int32 codCoord;

            UsuarioBE oUser = new UsuarioBE();
            oUser.LOGIN = txtUsuario.Text;
            oUser.SENHA = txtSenha.Text;
            lblErro.Text = String.Empty;
            

            try
            {
                string nomeAplicacao = ConfigurationAccess.ObterConfiguracao("nomeAplicacao").ToString();

                lblErro.Text = "Validando o usuário...";
                lblErro.Update();

                if (string.IsNullOrEmpty(oUser.LOGIN) || string.IsNullOrEmpty(oUser.SENHA))
                {
                    lblErro.Text = "";
                    lblErro.Update();
                    MessageBox.Show(ResourceRoteirizacao.Erro_UsuarioSenhaInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {

                    oUser = LoginBO.ValidaUsuario(oUser);
                    
                    if (oUser != null)
                    {
                        if (Int32.TryParse(oUser.LOGIN, out codCoord))
                        {
                            oUser.ACESSOCOORDENADOR = LoginBO.ValidaAcessoPainel(codCoord);
                        }
                        else
                        {
                            oUser.ACESSOCOORDENADOR = false;
                        }
                        oUser.ACESSOSUPERVISOR = LoginBO.ValidaAcessoMenu(oUser.LOGIN);

                        if (oUser.PERMISSAO_ACESSO && (oUser.ACESSOCOORDENADOR || oUser.ACESSOSUPERVISOR))
                        {
                            lblErro.Text = "Validação OK";
                            lblErro.Update();

                            this.Hide();
                            GlobalBE.usuarioLogado = oUser;
                            FrmPrincipal frmPrincipal = new FrmPrincipal();

                            lblErro.Text = "";
                            lblErro.Update();

                            frmPrincipal.ShowDialog();
                        }
                        else
                        {
                            lblErro.Text = "";
                            lblErro.Update();
                            MessageBox.Show(ResourceRoteirizacao.Erro_UsuarioSemPermissaoDeAcesso, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);    
                        }
                    }
                    else
                    {
                        lblErro.Text = "";
                        lblErro.Update();
                        MessageBox.Show(ResourceRoteirizacao.Erro_UsuarioSenhaInvalido, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

