﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmConsultaParametroExcecao : Form
    {
        private FiltroParametro oFiltro;

        public FrmConsultaParametroExcecao(Entities.FiltroParametro oFiltro)
        {
            this.oFiltro = oFiltro;
            InitializeComponent();
            grpParametro.Text = string.Format("Exceções do Parametro = {0}", oFiltro.PARAMETRO);
            btExcluir.Enabled = GlobalBE.usuarioLogado.ACESSOSUPERVISOR; //coordenador não poderá excluir
            
            CarregarParametroExcecao(oFiltro);

            //Bloqueia as colunas para não deixar editável
            //Somente o check para excluir
            dgvParametroFilial.Columns[1].ReadOnly = true;
            dgvParametroFilial.Columns[2].ReadOnly = true;
            dgvParametroFilial.Columns[3].ReadOnly = true;
            dgvParametroFilial.Columns[4].ReadOnly = true;
        }

        private void CarregarParametroExcecao(FiltroParametro oFiltro)
        {
            List<ParametroCons> listaParametroExcecao = new List<ParametroCons>();
            listaParametroExcecao = ParametroBO.BuscarParametroExcecao(oFiltro);
            dgvParametroFilial.DataSource = listaParametroExcecao;
        }

        private void btExcecaoSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            bool atualiza = false;
            try
            {
 
                foreach (DataGridViewRow dataGridViewRow in dgvParametroFilial.Rows)
                {
                    ParametroEXC oFiltroParam = new ParametroEXC();

                    if (Convert.ToBoolean(dataGridViewRow.Cells["EXCLUIR"].Value) == true)
                    {
                        oFiltroParam.CDFILIAL = Convert.ToInt32(dataGridViewRow.Cells["CDFILIAL"].Value);
                        oFiltroParam.PARAMETRO = Convert.ToString(dataGridViewRow.Cells["PARAMETRO"].Value);
                        ParametroBO.Update_Parametro_Exc(oFiltroParam);
                        atualiza = true;
                    }
                }

                if (atualiza == true)
                {
                    MessageBox.Show("Parâmetros Excluídos com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (dgvParametroFilial.RowCount == 0)
                    {
                        MessageBox.Show("Não Existem Registros para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Selecionado para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                CarregarParametroExcecao(oFiltro);

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao excluir as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }

        }
       
    }
}
