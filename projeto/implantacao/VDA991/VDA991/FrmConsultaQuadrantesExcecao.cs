﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Business;
using Entities;

namespace VDA991
{
    public partial class FrmConsultaQuadrantesExcecao : Form
    {

        private FiltroQuadrante oFiltro;

        public FrmConsultaQuadrantesExcecao(Entities.FiltroQuadrante oFiltro)
        {
            this.oFiltro = oFiltro;
            InitializeComponent();
            grpQuadrante.Text = string.Format("Exceções do Quadrante = {0}", oFiltro.QUADRANTE);

            btExcluir.Enabled = GlobalBE.usuarioLogado.ACESSOSUPERVISOR;  //coordenador não poderá excluir

            CarregarQuadranteExcecao(oFiltro);

            dgvQuadranteFilial.Columns[1].ReadOnly = true;
            dgvQuadranteFilial.Columns[2].ReadOnly = true;
            dgvQuadranteFilial.Columns[3].ReadOnly = true;
            dgvQuadranteFilial.Columns[4].ReadOnly = true;

        }

        private void CarregarQuadranteExcecao(FiltroQuadrante oFiltro)
        {
            List<QuadranteCons> listaQuadrante = new List<QuadranteCons>();
            listaQuadrante = QuadranteBO.BuscarQuadranteExcecao(oFiltro);
            dgvQuadranteFilial.DataSource = listaQuadrante;
        }

        private void btExcecaoSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {

            bool atualiza = false;
            try
            {

                foreach (DataGridViewRow dataGridViewRow in dgvQuadranteFilial.Rows)
                {
                    QuadranteEXC oFiltroParam = new QuadranteEXC();

                    if (Convert.ToBoolean(dataGridViewRow.Cells["EXCLUIR"].Value) == true)
                    {
                        oFiltroParam.CDFILIAL = Convert.ToInt32(dataGridViewRow.Cells["CDFILIAL"].Value);
                        oFiltroParam.QUADRANTE = Convert.ToString(dataGridViewRow.Cells["QUADRANTE"].Value);
                        QuadranteBO.Update_Quadrante_Exc(oFiltroParam);
                        atualiza = true;
                    }
                }

                if (atualiza == true)
                {
                    MessageBox.Show("Parâmetros Excluídos com Sucesso.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxSucesso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (dgvQuadranteFilial.RowCount == 0)
                    {
                        MessageBox.Show("Não Existem Registros para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Nenhum Registro Selecionado para Exclusão.", VDA991.ResourceRoteirizacao.Titulo_MessageBoxAviso, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                CarregarQuadranteExcecao(oFiltro);

            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("{0} /n ERRO: {1}", "Erro ao excluir as informações.", ex.Message), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }


        }
    
    }
}
