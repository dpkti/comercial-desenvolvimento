﻿using System;
using System.Data.OracleClient;
using Entities;

namespace DALC
{
    public class DALCException : Exception
    {
        public bool ErroDeConexao
        {
            get;
            private set;
        }

        public DALCException(String mensagem)
            : base(mensagem)
        {
            this.ErroDeConexao = false;
        }

        public DALCException(long codigoErro, String mensagem)
            : base("Erro de BD " + codigoErro.ToString() + ": " + mensagem)
        {
            this.ErroDeConexao = VerificarExcecaoBancoOffline(codigoErro);
        }

        public DALCException(Exception innerexception)
            : base(innerexception.Message, innerexception)
        {
            if (innerexception is OracleException)
            {
                this.ErroDeConexao = VerificarExcecaoBancoOffline(((OracleException)innerexception).Code);
            }
            else if (innerexception is DALCException)
            {
                this.ErroDeConexao = ((DALCException)innerexception).ErroDeConexao;
            }
            else
            {
                this.ErroDeConexao = false;
            }
        }

        public DALCException(OracleException innerexception)
            : base(innerexception.Message, innerexception)
        {
            this.ErroDeConexao = VerificarExcecaoBancoOffline(innerexception.Code);
        }

        /// <summary>
        /// Método responsável por verificar se o código do erro Oracle é um erro de conexão.
        /// Códigos de conexão configurados no APP.config.
        /// </summary>
        /// <param name="codErro">Código do erro.</param>
        /// <returns>Indicador do erro.</returns>
        private bool VerificarExcecaoBancoOffline(long codErro)
        {
            string[] arrayErros = UtilGeralDPA.ConfigurationAccess.ObterConfiguracao(ParametrosBase.ERROS_GERENCIAVEIS_CONEXAO).Split(',');

            foreach (string item in arrayErros)
            {
                if (codErro == long.Parse(item.Trim()))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
