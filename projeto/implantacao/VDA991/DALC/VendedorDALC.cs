﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class VendedorDALC
    {

        /// <summary>
        /// Método responsável por lista de Excedentes
        /// </summary>
        public static List<RetornoExcedente> BuscarListaExcedentes(FiltroPeriodo oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoExcedente> listaExcedentes = new List<RetornoExcedente>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoExcedente> mapper = MapBuilder<RetornoExcedente>.BuildAllProperties();
            IDataReader reader = null;
            RetornoExcedente excedentes = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_EXCEDENTES))
                {

                    db.AddInParameter(cmd, "P_DTINICIAL", DbType.Date, oFiltro.DATA_INICIAL);
                    db.AddInParameter(cmd, "P_DTFINAL", DbType.Date, oFiltro.DATA_FINAL);
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int64, oFiltro.CODCOORD);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        excedentes = new RetornoExcedente();
                        excedentes = mapper.MapRow(reader);
                        listaExcedentes.Add(excedentes);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaExcedentes;

        }

        /// <summary>
        /// Método responsável por lista Carteiras Incompletas
        /// </summary>
        public static List<RetornoCarteirasIncompletas> BuscarCarteirasIncompletas(FiltroPeriodo oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoCarteirasIncompletas> listaCarteiras = new List<RetornoCarteirasIncompletas>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoCarteirasIncompletas> mapper = MapBuilder<RetornoCarteirasIncompletas>.BuildAllProperties();
            IDataReader reader = null;
            RetornoCarteirasIncompletas carteiras = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_CARTEIRAS_INC))
                {

                    db.AddInParameter(cmd, "P_DATA", DbType.Date, oFiltro.DATA_INICIAL);
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int64, oFiltro.CODCOORD);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        carteiras = new RetornoCarteirasIncompletas();
                        carteiras = mapper.MapRow(reader);
                        listaCarteiras.Add(carteiras);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCarteiras;

        }


        /// <summary>
        /// Método responsável por listar a Carteira do Vendedor
        /// </summary>
        /// <param name="vendedor"></param>
        /// <returns></returns>
        public static List<RetornoCarteiraVendedor> BuscarCarteiraVendedor(VendedorBE oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoCarteiraVendedor> listaCarteiraVendedor = new List<RetornoCarteiraVendedor>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoCarteiraVendedor> mapper = MapBuilder<RetornoCarteiraVendedor>.BuildAllProperties();
            IDataReader reader = null;
            RetornoCarteiraVendedor carteiravendedor = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_CARTEIRA_VEND))
                {

                    db.AddInParameter(cmd, "P_DATA", DbType.DateTime, oFiltro.DTPROCESS);
                    db.AddInParameter(cmd, "P_VENDEDOR", DbType.Int64,oFiltro.CDVENDEDOR);
                    
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        carteiravendedor = new RetornoCarteiraVendedor();
                        carteiravendedor = mapper.MapRow(reader);
                        listaCarteiraVendedor.Add(carteiravendedor);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaCarteiraVendedor;

        }

        /// <summary>
        /// Método responsável por listar os Contatos Pendentes
        /// </summary>
        /// <param name="oFiltro"></param>
        /// <returns></returns>
        public static List<RetornoContatosPend> BuscarContatosPend(FiltroContatosPend oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoContatosPend> listaContatosPend = new List<RetornoContatosPend>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoContatosPend> mapper = MapBuilder<RetornoContatosPend>.BuildAllProperties();
            IDataReader reader = null;
            RetornoContatosPend contatospend = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_CONTATOS_PEND))
                {

                    
                    db.AddInParameter(cmd, "P_VENDEDOR", DbType.Int64, oFiltro.CDVENDEDOR);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);
                    db.AddInParameter(cmd, "P_DATA_INICIAL", DbType.DateTime, oFiltro.DATA_INICIAL);
                    db.AddInParameter(cmd, "P_DATA_FINAL", DbType.DateTime, oFiltro.DATA_FINAL);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        contatospend = new RetornoContatosPend();
                        contatospend = mapper.MapRow(reader);
                        listaContatosPend.Add(contatospend);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaContatosPend;

        }

        public static void UpdateContatosPend(VendedorBE oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
                
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_CONTATOS_PEND))
                {

                    db.AddInParameter(cmd, "P_DATA", DbType.DateTime, oFiltro.DTPROCESS);
                    db.AddInParameter(cmd, "P_VENDEDOR", DbType.Int64, oFiltro.CDVENDEDOR);
                    db.AddInParameter(cmd, "P_CLIENTE", DbType.Int64, oFiltro.CDCLI);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }

        public static List<FiltroVendedor> BuscarVendedor(long? lngCodCoord)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<FiltroVendedor> listaVendedor = new List<FiltroVendedor>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<FiltroVendedor> mapper = MapBuilder<FiltroVendedor>.BuildAllProperties();
            IDataReader reader = null;
            FiltroVendedor vendedor = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_VENDEDOR))
                {
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int32, lngCodCoord);
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        vendedor = new FiltroVendedor();
                        vendedor = mapper.MapRow(reader);
                        listaVendedor.Add(vendedor);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaVendedor;

        }
    }
}
