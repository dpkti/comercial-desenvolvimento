﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class QuadranteDALC
    {

        /// <summary>
        /// Método responsável por buscar a lista de Quadrantes.
        /// </summary>
        /// <returns>lista com Quadrantes.</returns>

        public static List<QuadranteBE> BuscarQuadrante()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            
            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<QuadranteBE> mapper = MapBuilder<QuadranteBE>.BuildAllProperties();
            IDataReader reader = null;
            QuadranteBE quadrante = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_QUADRANTE))
                {
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        quadrante = new QuadranteBE();
                        quadrante = mapper.MapRow(reader);
                        listaQuadrante.Add(quadrante);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaQuadrante;

        }

        /// <summary>
        /// Método responsável por gravar alterar Quadrante
        /// </summary>
        public static void AlterarQuadrante(QuadranteBE oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_QUADRANTE_PADRAO))
                {

                    db.AddInParameter(cmd, "P_QUADRANTE", DbType.String, oFiltro.QUADRANTE);
                    db.AddInParameter(cmd, "P_QTDCONTATO", DbType.Int64, oFiltro.QTDCONTATO);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar a lista de Quadrante Exceção.
        /// </summary>
        /// <returns>lista com Quadrante Exceção.</returns>

        public static List<QuadranteCons> BuscarQuadranteExcecao(FiltroQuadrante oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<QuadranteCons> listaQuadranteExcecao = new List<QuadranteCons>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<QuadranteCons> mapper = MapBuilder<QuadranteCons>.BuildAllProperties();
            IDataReader reader = null;
            QuadranteCons quadranteexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_QUADRANTE_EXC))
                {

                    db.AddInParameter(cmd, "P_QUADRANTE", DbType.String, oFiltro.QUADRANTE);
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int32, oFiltro.CODCOORD);
                    
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        quadranteexc = new QuadranteCons();
                        quadranteexc = mapper.MapRow(reader);
                        listaQuadranteExcecao.Add(quadranteexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaQuadranteExcecao;

        }

        /// <summary>
        /// Método responsável por buscar a lista de Quadrante Filial Exceção.
        /// </summary>
        /// <returns>lista com Quadrante Filial Exceção.</returns>

        public static List<QuadranteEXC> BuscarQuadranteFilialExcecao(QuadranteEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<QuadranteEXC> listaQuadranteExcecao = new List<QuadranteEXC>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<QuadranteEXC> mapper = MapBuilder<QuadranteEXC>.BuildAllProperties();
            IDataReader reader = null;
            QuadranteEXC quadranteexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_QUADRANTE_FILIAL))
                {

                    db.AddInParameter(cmd, "P_QUADRANTE", DbType.String, oFiltro.QUADRANTE);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        quadranteexc = new QuadranteEXC();
                        quadranteexc = mapper.MapRow(reader);
                        listaQuadranteExcecao.Add(quadranteexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaQuadranteExcecao;

        }

        /// <summary>
        /// Método responsável por gravar alterar Quadrante
        /// </summary>
        public static void InserirAlterarQuadranteExcecao(QuadranteEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_INS_UP_QUADRANTE_EXC))
                {

                    db.AddInParameter(cmd, "P_QUADRANTE", DbType.String, oFiltro.QUADRANTE);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);
                    db.AddInParameter(cmd, "P_QTDCONTATO", DbType.Int64, oFiltro.QTDCONTATO);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por desativar Quadrante Exceção.
        /// </summary>
        public static void Update_Quadrante_Exc(QuadranteEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_QUADRANTE_EXC))
                {

                    db.AddInParameter(cmd, "P_QUADRANTE", DbType.String, oFiltro.QUADRANTE);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar a lista de Quadrantes relacionados a Depósitos. TI-4404
        /// </summary>
        /// <returns>lista com Quadrantes relacionados a Depósitos.</returns>

        public static List<QuadranteBE> BuscarAcaoQuadrante()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<QuadranteBE> listaQuadrante = new List<QuadranteBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<QuadranteBE> mapper = MapBuilder<QuadranteBE>.BuildAllProperties();
            IDataReader reader = null;
            QuadranteBE quadrante = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_QUADRANTE))
                {
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        quadrante = new QuadranteBE();
                        quadrante = mapper.MapRow(reader);
                        listaQuadrante.Add(quadrante);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaQuadrante;

        }

        /// <summary>
        /// Método responsável por buscar todos os Quadrantes relacionados a Depósitos. TI-4404
        /// </summary>
        /// <returns>lista com todos os Quadrantes relacionados a Depósitos.</returns>

        public static List<AcaoQuadrante> BuscarListaAcaoQuadrante(AcaoQuadrante oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<AcaoQuadrante> listaQuadrante = new List<AcaoQuadrante>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<AcaoQuadrante> mapper = MapBuilder<AcaoQuadrante>.BuildAllProperties();
            IDataReader reader = null;
            AcaoQuadrante quadrante = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_ACAO_QUADRANTE))
                {

                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, oFiltro.COD_LOJA);
                    db.AddInParameter(cmd, "P_COD_QUADRANTE", DbType.String, oFiltro.COD_QUADRANTE);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        quadrante = new AcaoQuadrante();
                        quadrante = mapper.MapRow(reader);
                        listaQuadrante.Add(quadrante);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaQuadrante;

        }

        /// <summary>
        /// Método responsável por inserir / atualizar quadrantes. TI-4404
        /// </summary>
        /// <returns></returns>
        public static void AtualizarQuadrante(AcaoQuadrante oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UPD_ACAO_QUADRANTE))
                {

                    db.AddInParameter(cmd, "P_COD_LOJA", DbType.Int32, oFiltro.COD_LOJA);
                    db.AddInParameter(cmd, "P_COD_QUADRANTE", DbType.String, oFiltro.COD_QUADRANTE);
                    db.AddInParameter(cmd, "P_SEQUENCIA", DbType.Int32, oFiltro.SEQUENCIA);
                    db.AddInParameter(cmd, "P_NUM_PRIORIDADE", DbType.Int32, oFiltro.NUM_PRIORIDADE);
                    db.AddInParameter(cmd, "P_DESC_ACAO", DbType.String, oFiltro.DESC_ACAO);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }

    }
}
