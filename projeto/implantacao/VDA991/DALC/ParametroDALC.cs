﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class ParametroDALC
    {

        /// <summary>
        /// Método responsável por buscar a lista de Parametros.
        /// </summary>
        /// <returns>lista com Parametros.</returns>

        public static List<ParametroBE> BuscarParametro()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<ParametroBE> listaParametro = new List<ParametroBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<ParametroBE> mapper = MapBuilder<ParametroBE>.BuildAllProperties();
            IDataReader reader = null;
            ParametroBE parametro = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_PARAMETRO))
                {
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        parametro = new ParametroBE();
                        parametro = mapper.MapRow(reader);
                        listaParametro.Add(parametro);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaParametro;

        }

        /// <summary>
        /// Método responsável por gravar alterar Parametro.
        /// </summary>
        public static void AlterarParametro(ParametroBE oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_PARAMETRO_PADRAO))
                {

                    db.AddInParameter(cmd, "P_PARAMETRO", DbType.String, oFiltro.PARAMETRO);
                    db.AddInParameter(cmd, "P_DESCRICAO", DbType.String, oFiltro.DESCRICAO);
                    db.AddInParameter(cmd, "P_VALOR", DbType.String, oFiltro.VALOR);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar a lista de Parametros Exceção.
        /// </summary>
        /// <returns>lista com Parametros Exceção.</returns>

        public static List<ParametroCons> BuscarParametroExcecao(FiltroParametro oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<ParametroCons> listaParametroExcecao = new List<ParametroCons>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<ParametroCons> mapper = MapBuilder<ParametroCons>.BuildAllProperties();
            IDataReader reader = null;
            ParametroCons parametroexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_PARAMETRO_EXC))
                {
                    db.AddInParameter(cmd, "P_PARAMETRO", DbType.String, oFiltro.PARAMETRO);
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int64, oFiltro.CODCOORD);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        parametroexc = new ParametroCons();
                        parametroexc = mapper.MapRow(reader);
                        listaParametroExcecao.Add(parametroexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaParametroExcecao;

        }

        /// <summary>
        /// Método responsável por buscar a lista de Parametro Filial Exceção.
        /// </summary>
        /// <returns>lista com Parametro Filial Exceção.</returns>

        public static List<ParametroEXC> BuscarParametroFilialExcecao(ParametroEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<ParametroEXC> listaParametroExcecao = new List<ParametroEXC>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<ParametroEXC> mapper = MapBuilder<ParametroEXC>.BuildAllProperties();
            IDataReader reader = null;
            ParametroEXC parametroexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_PARAMETRO_FILIAL))
                {

                    db.AddInParameter(cmd, "P_PARAMETRO", DbType.String, oFiltro.PARAMETRO);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        parametroexc = new ParametroEXC();
                        parametroexc = mapper.MapRow(reader);
                        listaParametroExcecao.Add(parametroexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaParametroExcecao;

        }

        /// <summary>
        /// Método responsável por gravar alterar Parametro
        /// </summary>
        public static void InserirAlterarParametroExcecao(ParametroEXC oFiltro)

        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_INS_UP_PARAMETRO_EXC))
                {

                    db.AddInParameter(cmd, "P_PARAMETRO", DbType.String, oFiltro.PARAMETRO);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);
                    db.AddInParameter(cmd, "P_VALOR", DbType.String, oFiltro.VALOR);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar todos os parametros exceção
        /// </summary>
        /// <returns>lista com todos os Parametros  Exceção.</returns>

        public static List<ParametroEXC> BuscarParametroFilial(ParametroEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<ParametroEXC> listaParametroExcecao = new List<ParametroEXC>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<ParametroEXC> mapper = MapBuilder<ParametroEXC>.BuildAllProperties();
            IDataReader reader = null;
            ParametroEXC parametroexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_PARAM_EXC))
                {

                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        parametroexc = new ParametroEXC();
                        parametroexc = mapper.MapRow(reader);
                        listaParametroExcecao.Add(parametroexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaParametroExcecao;

        }

        /// <summary>
        /// Método responsável por desativar Parametro Exceção.
        /// </summary>
        public static void Update_Parametro_Exc(ParametroEXC oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_PARAMETRO_EXC))
                {

                    db.AddInParameter(cmd, "P_PARAMETRO", DbType.String, oFiltro.PARAMETRO);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }


    }
}
