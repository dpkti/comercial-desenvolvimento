﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class RoteirizacaoDALC
    {

        /// <summary>
        /// Método responsável por limpar a tabela temporária
        /// </summary>
        public static void LimparStageClienteExc()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_DEL_STG_CLIENTE_EXC))
                {
                    db.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por inserir novos Clientes Exceção vindos do excel
        /// </summary>
        /// <returns></returns>
        public static int InserirClienteExcecao()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();
            int qtdInserida;
            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_INS_CLI_EXC_TRANSP_EXCEL))
                {
                    db.AddOutParameter(cmd, "P_QTE_INSERIDA", DbType.Int32, 8);
                    db.ExecuteNonQuery(cmd);

                    qtdInserida = Convert.ToInt32(db.GetParameterValue(cmd, "P_QTE_INSERIDA"));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return qtdInserida;
        }


    }
}
