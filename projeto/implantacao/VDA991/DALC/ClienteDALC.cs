﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;

namespace DALC
{
    public class ClienteDALC
    {

        /// <summary>
        /// Método responsável por Inserir Cliente Exceção
        /// </summary>
        public static void InserirClienteExcecao(FiltroUpCliente oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_INS_CLIENTE_EXC))
                {

                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAINI", DbType.Date, oFiltro.DTVIGENCIAINI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAFIM", DbType.Date, oFiltro.DTVIGENCIAFIM);

                    db.AddInParameter(cmd, "P_SEGUNDA1", DbType.Int32, oFiltro.SEGUNDA1);
                    db.AddInParameter(cmd, "P_SEGUNDA2", DbType.Int32, oFiltro.SEGUNDA2);
                    db.AddInParameter(cmd, "P_SEGUNDA3", DbType.Int32, oFiltro.SEGUNDA3);
                    db.AddInParameter(cmd, "P_SEGUNDA4", DbType.Int32, oFiltro.SEGUNDA4);
                    db.AddInParameter(cmd, "P_SEGUNDA5", DbType.Int32, oFiltro.SEGUNDA5);

                    db.AddInParameter(cmd, "P_TERCA1", DbType.Int32, oFiltro.TERCA1);
                    db.AddInParameter(cmd, "P_TERCA2", DbType.Int32, oFiltro.TERCA2);
                    db.AddInParameter(cmd, "P_TERCA3", DbType.Int32, oFiltro.TERCA3);
                    db.AddInParameter(cmd, "P_TERCA4", DbType.Int32, oFiltro.TERCA4);
                    db.AddInParameter(cmd, "P_TERCA5", DbType.Int32, oFiltro.TERCA5);

                    db.AddInParameter(cmd, "P_QUARTA1", DbType.Int32, oFiltro.QUARTA1);
                    db.AddInParameter(cmd, "P_QUARTA2", DbType.Int32, oFiltro.QUARTA2);
                    db.AddInParameter(cmd, "P_QUARTA3", DbType.Int32, oFiltro.QUARTA3);
                    db.AddInParameter(cmd, "P_QUARTA4", DbType.Int32, oFiltro.QUARTA4);
                    db.AddInParameter(cmd, "P_QUARTA5", DbType.Int32, oFiltro.QUARTA5);

                    db.AddInParameter(cmd, "P_QUINTA1", DbType.Int32, oFiltro.QUINTA1);
                    db.AddInParameter(cmd, "P_QUINTA2", DbType.Int32, oFiltro.QUINTA2);
                    db.AddInParameter(cmd, "P_QUINTA3", DbType.Int32, oFiltro.QUINTA3);
                    db.AddInParameter(cmd, "P_QUINTA4", DbType.Int32, oFiltro.QUINTA4);
                    db.AddInParameter(cmd, "P_QUINTA5", DbType.Int32, oFiltro.QUINTA5);

                    db.AddInParameter(cmd, "P_SEXTA1", DbType.Int32, oFiltro.SEXTA1);
                    db.AddInParameter(cmd, "P_SEXTA2", DbType.Int32, oFiltro.SEXTA2);
                    db.AddInParameter(cmd, "P_SEXTA3", DbType.Int32, oFiltro.SEXTA3);
                    db.AddInParameter(cmd, "P_SEXTA4", DbType.Int32, oFiltro.SEXTA4);
                    db.AddInParameter(cmd, "P_SEXTA5", DbType.Int32, oFiltro.SEXTA5);

                    db.AddInParameter(cmd, "P_MOTIVO", DbType.String, oFiltro.MOTIVO);

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
        }

        /// <summary>
        /// Método responsável por buscar cliente Exceção
        /// </summary>
        /// <returns>lista com todos os Clientes  Exceção.</returns>

        public static List<ClienteBE> BuscarClienteExcecao(FiltroCliente oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<ClienteBE> listaParametroExcecao = new List<ClienteBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<ClienteBE> mapper = MapBuilder<ClienteBE>.BuildAllProperties();
            IDataReader reader = null;
            ClienteBE clienteexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_CLIENTE_EXC))
                {

                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAINI", DbType.Date, oFiltro.DTVIGENCIAINI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAFIM", DbType.Date, oFiltro.DTVIGENCIAFIM);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        clienteexc = new ClienteBE();
                        clienteexc = mapper.MapRow(reader);
                        listaParametroExcecao.Add(clienteexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaParametroExcecao;

        }

        /// <summary>
        /// Método responsável por buscar o nome do cliente.
        /// </summary>
        public static string BuscarNomeCliente(FiltroUpCliente oFiltro)
        {
            string nomeCli = "";
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_NOME_CLIENTE))
                {
                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                    db.AddOutParameter(cmd, "P_NOME", DbType.String,30);
                    db.ExecuteNonQuery(cmd);

                    nomeCli = db.GetParameterValue(cmd, "P_NOME").ToString();

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return nomeCli;
        }

        /// <summary>
        /// Método responsável por Validar o Periodo.
        /// </summary>
        public static bool ValidaPeriodo(FiltroCliente oFiltro)
        {
            bool periodo = false;
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_VALIDA_PERIODO))
            {
                db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                db.AddInParameter(cmd, "P_DTVIGENCIAINI", DbType.Date, oFiltro.DTVIGENCIAINI);
                db.AddInParameter(cmd, "P_DTVIGENCIAFIM", DbType.Date, oFiltro.DTVIGENCIAFIM);
                db.AddInParameter(cmd, "P_DTVIGENCIAINI_ORIG", DbType.Date, oFiltro.DTVIGENCIAINI_ORIG);
                db.AddInParameter(cmd, "P_DTVIGENCIAFIM_ORIG", DbType.Date, oFiltro.DTVIGENCIAFIM_ORIG);
                db.AddInParameter(cmd, "P_TIPO", DbType.Int32, oFiltro.TIPO);
                db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 1);
                db.ExecuteNonQuery(cmd);

                periodo = Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) > 0 ? true : false;

            }

            return periodo;
        }

        /// <summary>
        /// Método responsável por buscar cliente Exceção
        /// </summary>
        /// <returns>lista com todos os Clientes  Exceção.</returns>

        public static List<RetornoClienteExc> BuscarDiasClientesExc(FiltroConsulta oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoClienteExc> listaDiasClientesExc = new List<RetornoClienteExc>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoClienteExc> mapper = MapBuilder<RetornoClienteExc>.BuildAllProperties();
            IDataReader reader = null;
            RetornoClienteExc clienteexc = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_CONS_CLIENTE_EXC))
                {

                    db.AddInParameter(cmd, "P_CNPJ", DbType.Int64, oFiltro.CNPJ);
                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                    db.AddInParameter(cmd, "P_FILIAL", DbType.Int32, oFiltro.CDFILIAL);
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int32, oFiltro.CODCOORD);
                    db.AddInParameter(cmd, "P_INATIVOS", DbType.Int32, oFiltro.INATIVOS);

                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        clienteexc = new RetornoClienteExc();
                        clienteexc = mapper.MapRow(reader);
                        listaDiasClientesExc.Add(clienteexc);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaDiasClientesExc;

        }

        /// <summary>
        /// Método responsável por buscar cliente Exceção
        /// </summary>
        /// <returns>lista com todos os Clientes  Exceção.</returns>

        public static List<RetornoCliente> BuscarClientes(FiltroConsulta oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<RetornoCliente> listaClientes = new List<RetornoCliente>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<RetornoCliente> mapper = MapBuilder<RetornoCliente>.BuildAllProperties();
            IDataReader reader = null;
            RetornoCliente cliente = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_CLIENTES))
                {

                    db.AddInParameter(cmd, "P_NOME", DbType.String, oFiltro.NOMECLI);
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        cliente = new RetornoCliente();
                        cliente = mapper.MapRow(reader);
                        listaClientes.Add(cliente);
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaClientes;

        }

        /// <summary>
        /// Método responsável por alterar cliente Exceção
        /// </summary>
        /// <param name="oFiltro"></param>
        public static void AlterarClienteExcecao(FiltroUpCliente oFiltro)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_UP_CLIENTE_EXC))
                {

                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAINI", DbType.Date, oFiltro.DTVIGENCIAINI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAFIM", DbType.Date, oFiltro.DTVIGENCIAFIM);
                    db.AddInParameter(cmd, "P_DTVIGENCIAINI_ORIG", DbType.Date, oFiltro.DTVIGENCIAINI_ORIG);
                    db.AddInParameter(cmd, "P_DTVIGENCIAFIM_ORIG", DbType.Date, oFiltro.DTVIGENCIAFIM_ORIG);

                    db.AddInParameter(cmd, "P_SEGUNDA1", DbType.Int32, oFiltro.SEGUNDA1);
                    db.AddInParameter(cmd, "P_SEGUNDA2", DbType.Int32, oFiltro.SEGUNDA2);
                    db.AddInParameter(cmd, "P_SEGUNDA3", DbType.Int32, oFiltro.SEGUNDA3);
                    db.AddInParameter(cmd, "P_SEGUNDA4", DbType.Int32, oFiltro.SEGUNDA4);
                    db.AddInParameter(cmd, "P_SEGUNDA5", DbType.Int32, oFiltro.SEGUNDA5);

                    db.AddInParameter(cmd, "P_TERCA1", DbType.Int32, oFiltro.TERCA1);
                    db.AddInParameter(cmd, "P_TERCA2", DbType.Int32, oFiltro.TERCA2);
                    db.AddInParameter(cmd, "P_TERCA3", DbType.Int32, oFiltro.TERCA3);
                    db.AddInParameter(cmd, "P_TERCA4", DbType.Int32, oFiltro.TERCA4);
                    db.AddInParameter(cmd, "P_TERCA5", DbType.Int32, oFiltro.TERCA5);

                    db.AddInParameter(cmd, "P_QUARTA1", DbType.Int32, oFiltro.QUARTA1);
                    db.AddInParameter(cmd, "P_QUARTA2", DbType.Int32, oFiltro.QUARTA2);
                    db.AddInParameter(cmd, "P_QUARTA3", DbType.Int32, oFiltro.QUARTA3);
                    db.AddInParameter(cmd, "P_QUARTA4", DbType.Int32, oFiltro.QUARTA4);
                    db.AddInParameter(cmd, "P_QUARTA5", DbType.Int32, oFiltro.QUARTA5);

                    db.AddInParameter(cmd, "P_QUINTA1", DbType.Int32, oFiltro.QUINTA1);
                    db.AddInParameter(cmd, "P_QUINTA2", DbType.Int32, oFiltro.QUINTA2);
                    db.AddInParameter(cmd, "P_QUINTA3", DbType.Int32, oFiltro.QUINTA3);
                    db.AddInParameter(cmd, "P_QUINTA4", DbType.Int32, oFiltro.QUINTA4);
                    db.AddInParameter(cmd, "P_QUINTA5", DbType.Int32, oFiltro.QUINTA5);

                    db.AddInParameter(cmd, "P_SEXTA1", DbType.Int32, oFiltro.SEXTA1);
                    db.AddInParameter(cmd, "P_SEXTA2", DbType.Int32, oFiltro.SEXTA2);
                    db.AddInParameter(cmd, "P_SEXTA3", DbType.Int32, oFiltro.SEXTA3);
                    db.AddInParameter(cmd, "P_SEXTA4", DbType.Int32, oFiltro.SEXTA4);
                    db.AddInParameter(cmd, "P_SEXTA5", DbType.Int32, oFiltro.SEXTA5);

                    db.AddInParameter(cmd, "P_MOTIVO", DbType.String, oFiltro.MOTIVO);
                    
                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }

        /// <summary>
        /// Método responsável por validar a filial do cliente
        /// </summary>
        /// <param name="oFiltro"></param>
        public static bool ValidaCliente(FiltroCli oFiltro)
        {
            bool vcliente = false;
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_VALIDA_CLIENTE))
            {
                db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltro.CDCLI);
                db.AddInParameter(cmd, "P_CODCOORD", DbType.Int32, oFiltro.CODCOORD);
                db.AddInParameter(cmd, "P_TIPO", DbType.Int32, oFiltro.TIPO_USER);
                db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 1);
                db.ExecuteNonQuery(cmd);

                vcliente = Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) > 0 ? true : false;

            }

            return vcliente;

        }

        public static bool ValidaExcecaoAtiva(FiltroCliente oFiltroPeriodo)
        {
            bool ExcAtiva = false;
            DateTime dataInicio = Logger.LogInicioMetodo();

            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

            using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_EXCECAO_ATIVA))
            {
                db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltroPeriodo.CDCLI);
                db.AddOutParameter(cmd, "P_EXISTE", DbType.Int16, 1);
                db.ExecuteNonQuery(cmd);

                ExcAtiva = Convert.ToInt16(db.GetParameterValue(cmd, "P_EXISTE")) > 0 ? true : false;

            }

            return ExcAtiva;
        }

        public static void DeletaClienteExcecao(FiltroUpCliente oFiltroCli)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_DEL_CLIENTE_EXC))
                {

                    db.AddInParameter(cmd, "P_CDCLI", DbType.Int64, oFiltroCli.CDCLI);
                    db.AddInParameter(cmd, "P_DTVIGENCIAINI_ORIG", DbType.Date, oFiltroCli.DTVIGENCIAINI_ORIG);
                    db.AddInParameter(cmd, "P_DTVIGENCIAFIM_ORIG", DbType.Date, oFiltroCli.DTVIGENCIAFIM_ORIG);
                    

                    db.ExecuteNonQuery(cmd);
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

        }
    }
}
