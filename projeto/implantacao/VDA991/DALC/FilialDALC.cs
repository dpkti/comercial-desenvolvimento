﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;


namespace DALC
{
    public class FilialDALC
    {

        /// <summary>
        /// Método responsável por buscar a lista de Filiais.
        /// </summary>
        /// <returns>lista com Filiais.</returns>

        public static List<FilialBE> BuscarFilial()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<FilialBE> listaFilial = new List<FilialBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<FilialBE> mapper = MapBuilder<FilialBE>.BuildAllProperties();
            IDataReader reader = null;
            FilialBE filial = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_FILIAL))
                {
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        filial = new FilialBE();
                        filial = mapper.MapRow(reader);
                        listaFilial.Add(filial);

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaFilial;

        }



        public static List<FilialBE> BuscarFilialCoordenador(long codCoord)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<FilialBE> listaFilial = new List<FilialBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<FilialBE> mapper = MapBuilder<FilialBE>.BuildAllProperties();
            IDataReader reader = null;
            FilialBE filial = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LISTA_FILIAL_COORD))
                {
                    db.AddInParameter(cmd, "P_CODCOORD", DbType.Int64, codCoord);
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        filial = new FilialBE();
                        filial = mapper.MapRow(reader);
                        listaFilial.Add(filial);

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaFilial;
        }

        /// <summary>
        /// Método responsável por buscar a lista de Depósitos. TI-4404
        /// </summary>
        /// <returns>lista com Depósitos.</returns>

        public static List<FilialBE> BuscarLoja()
        {
            DateTime dataInicio = Logger.LogInicioMetodo();

            List<FilialBE> listaFilial = new List<FilialBE>();
            Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);
            IRowMapper<FilialBE> mapper = MapBuilder<FilialBE>.BuildAllProperties();
            IDataReader reader = null;
            FilialBE filial = null;

            try
            {
                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_LOJA))
                {
                    reader = db.ExecuteReader(cmd);

                    while (reader.Read())
                    {
                        filial = new FilialBE();
                        filial = mapper.MapRow(reader);
                        listaFilial.Add(filial);

                    }

                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return listaFilial;

        }

    }
}
