﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities;
using UtilGeralDPA;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.Common;
using System.Data.OracleClient;

namespace DALC
{
    public class LoginDALC
    {
        public static UsuarioBE ValidaUsuario(UsuarioBE oUser)
        {
            DateTime dataInicio = Logger.LogInicioMetodo();


            IDataReader reader = null;
            UsuarioBE usuarioLogado = null;
            IRowMapper<UsuarioBE> mapper = MapBuilder<UsuarioBE>.MapAllProperties().DoNotMap(x => x.ACESSOCOORDENADOR).DoNotMap(y => y.ACESSOSUPERVISOR).Build();

            try
            {
                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_USUARIO))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, oUser.LOGIN);
                    db.AddInParameter(cmd, "P_SENHA", DbType.String, oUser.SENHA);

                    reader = db.ExecuteReader(cmd);
                    if (reader.Read())
                    {
                        usuarioLogado = mapper.MapRow(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                if (reader != null)
                {
                    reader.Close();
                }

                Logger.LogError(ex);

                //no data found - usuário não encontrado
                if (((OracleException)ex).Code == 1403)
                    usuarioLogado = null;
                else
                    throw new DALCException(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                Logger.LogFinalMetodo(dataInicio);
            }

            return usuarioLogado;
        }

        public static Boolean ValidaAcessoPainel(Int32 loginUser)
        {
            Boolean permissao;
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {

                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_ACESSO_PAINEL))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.Int32, loginUser);
                    db.AddOutParameter(cmd, "P_PERMISSAO", DbType.Int16,1);
                    db.ExecuteNonQuery(cmd);

                    permissao = Convert.ToBoolean(db.GetParameterValue(cmd,"P_PERMISSAO"));

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }
            
            return permissao;
        }

        public static Boolean ValidaAcessoMenu(string loginUser)
        {
            Boolean permissao;
            DateTime dataInicio = Logger.LogInicioMetodo();

            try
            {

                Database db = DatabaseFactory.CreateDatabase(ParametrosBase.CONN_ORA_DPK);

                using (DbCommand cmd = db.GetStoredProcCommand(ParametrosBase.PR_SEL_ACESSO_MENU))
                {
                    db.AddInParameter(cmd, "P_USUARIO", DbType.String, loginUser);
                    db.AddOutParameter(cmd, "P_PERMISSAO", DbType.Int16, 1);
                    db.ExecuteNonQuery(cmd);

                    permissao = Convert.ToBoolean(db.GetParameterValue(cmd, "P_PERMISSAO"));

                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                throw new DALCException(ex);
            }
            finally
            {
                Logger.LogFinalMetodo(dataInicio);
            }

            return permissao;
        }

    }
}
