VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form dlgFornecedores 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Fornecedores"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5505
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   364
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   367
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fra 
      Caption         =   "Par�metros"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   735
      Left            =   45
      TabIndex        =   5
      Top             =   900
      Width           =   5415
      Begin VB.TextBox txtCGC 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3690
         MaxLength       =   14
         TabIndex        =   1
         Top             =   270
         Width           =   1590
      End
      Begin VB.TextBox txtSigla 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   630
         MaxLength       =   10
         TabIndex        =   0
         Top             =   270
         Width           =   1230
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "CGC:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3240
         TabIndex        =   8
         Top             =   315
         Width           =   375
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Sigla:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   135
         TabIndex        =   6
         Top             =   315
         Width           =   420
      End
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   810
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   855
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgFornecedores.frx":0000
      PICN            =   "dlgFornecedores.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdConsultar 
      Height          =   690
      Left            =   45
      TabIndex        =   4
      TabStop         =   0   'False
      ToolTipText     =   "Consultar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "dlgFornecedores.frx":0CF6
      PICN            =   "dlgFornecedores.frx":0D12
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid grdFornecedores 
      Height          =   3705
      Left            =   45
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1710
      Visible         =   0   'False
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   6535
      _Version        =   393216
      BackColorBkg    =   -2147483633
      HighLight       =   0
      ScrollBars      =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "dlgFornecedores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdConsultar_Click()

    If txtSigla = "" And _
        txtCGC = "" Then
        
        Call vVB_Generica_001.Informar("Digita a Sigla ou o CGC para consultar o(s) fornecedor(es)")
        Exit Sub
        
    End If
    
    If txtSigla <> "" Then
        
        grdFornecedores.Visible = False
        Call vVB_Generica_001.ExcluiBind(vBanco)
        
        Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, 0, txtSigla)
        
        If vObjOracle.EOF Then
            
            grdFornecedores.Visible = False
            Call vVB_Generica_001.Informar("Nenhuma informa��o encontrada")
            Exit Sub
        
        Else
            
            Call vVB_Generica_001.LimpaFlexGridComTitulo(grdFornecedores)
            Call vVB_Generica_001.CarregaGridTabela(grdFornecedores, vObjOracle, 5, "0;5;4;2")
            grdFornecedores.Visible = True
        
        End If
        
        Exit Sub
    
    End If

    If txtCGC <> "" Then
        
        grdFornecedores.Visible = False
        Call vVB_Generica_001.ExcluiBind(vBanco)
        
        Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, 0, , Val(txtCGC))
        
        If vObjOracle.EOF Then
            
            grdFornecedores.Visible = False
            Call vVB_Generica_001.Informar("Nenhuma informa��o encontrada")
            Exit Sub
        
        Else
            
            Call vVB_Generica_001.LimpaFlexGridComTitulo(grdFornecedores)
            Call vVB_Generica_001.CarregaGridTabela(grdFornecedores, vObjOracle, 5, "0;5;4;2")
            grdFornecedores.Visible = True
        
        End If
        
        Exit Sub
    
    End If

End Sub
Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 0
    Me.Left = 0
    
End Sub

Private Sub grdFornecedores_DblClick()

    grdFornecedores.Col = 1
    frmIncluir.txtCodFornecedor = grdFornecedores.Text
    
    Unload Me

End Sub

Private Sub txtCGC_Change()

    grdFornecedores.Visible = False

End Sub
Private Sub txtCGC_GotFocus()
    
    Call vVB_Generica_001.SelecionaCampo(txtCGC)

End Sub
Private Sub txtCGC_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
    
        cmdConsultar_Click
    
    End If

End Sub

Private Sub txtSigla_Change()

    grdFornecedores.Visible = False

End Sub
Private Sub txtSigla_GotFocus()

    Call vVB_Generica_001.SelecionaCampo(txtSigla)

End Sub
Private Sub txtSigla_KeyPress(KeyAscii As Integer)

    If KeyAscii = 13 Then
    
        cmdConsultar_Click
    
    End If
    
    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)

End Sub
