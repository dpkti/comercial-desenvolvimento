VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{7E65892D-F38B-11D2-BC07-0055003B26DE}#3.0#0"; "HyperLabel.ocx"
Begin VB.Form frmIncluir 
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inclus�o de Par�metro"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "frmIncluir.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   466
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   451
   Begin VB.Frame fra 
      Caption         =   "Dados do Par�metro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   5640
      Left            =   45
      TabIndex        =   18
      Top             =   945
      Width           =   6675
      Begin VB.ComboBox cmbFornecedor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   405
         Width           =   2310
      End
      Begin VB.TextBox txtFabrica 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4365
         TabIndex        =   5
         Top             =   1845
         Width           =   1815
      End
      Begin VB.TextBox txtPCDescSU 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         MaxLength       =   5
         TabIndex        =   12
         Top             =   4185
         Width           =   1185
      End
      Begin VB.TextBox txtPCAcres 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         MaxLength       =   5
         TabIndex        =   11
         Top             =   3735
         Width           =   1185
      End
      Begin VB.TextBox txtPCDesc 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         MaxLength       =   5
         TabIndex        =   10
         Top             =   3285
         Width           =   1185
      End
      Begin VB.ComboBox cmbTpCLiente 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1395
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   14
         Top             =   5130
         Width           =   3120
      End
      Begin VB.ComboBox cmbUF 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1395
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   4635
         Width           =   3120
      End
      Begin VB.CheckBox chkAvista 
         Appearance      =   0  'Flat
         Caption         =   "� Vista"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   4275
         TabIndex        =   7
         Top             =   2385
         Width           =   1050
      End
      Begin VB.TextBox txtMDFinal 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4995
         MaxLength       =   2
         TabIndex        =   9
         Top             =   2835
         Width           =   1185
      End
      Begin VB.TextBox txtMDInicial 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1935
         MaxLength       =   2
         TabIndex        =   8
         Top             =   2835
         Width           =   1185
      End
      Begin VB.ComboBox cmbPlano 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   810
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   2340
         Width           =   3165
      End
      Begin VB.TextBox txtCodFornecedor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1665
         MaxLength       =   3
         TabIndex        =   1
         Top             =   900
         Width           =   510
      End
      Begin VB.TextBox txtDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         TabIndex        =   4
         Top             =   1845
         Width           =   1815
      End
      Begin VB.ComboBox cmbSubGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4185
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1350
         Width           =   2355
      End
      Begin VB.ComboBox cmbGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   810
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1350
         Width           =   2310
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         Caption         =   "Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   34
         Top             =   450
         Width           =   960
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         Caption         =   "Cod. Fabrica:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3240
         TabIndex        =   32
         Top             =   1890
         Width           =   1050
      End
      Begin VB.Label Label10 
         Appearance      =   0  'Flat
         Caption         =   "Pc. Desc. SUFRAMA:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   31
         Top             =   4230
         Width           =   1725
      End
      Begin VB.Label Label9 
         Appearance      =   0  'Flat
         Caption         =   "Pc Acr�scimo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   30
         Top             =   3780
         Width           =   1725
      End
      Begin VB.Label Label8 
         Appearance      =   0  'Flat
         Caption         =   "Pc. Desconto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   29
         Top             =   3330
         Width           =   1725
      End
      Begin VB.Label Label7 
         Appearance      =   0  'Flat
         Caption         =   "Tipo Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   28
         Top             =   5175
         Width           =   1095
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         Caption         =   "UF:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   225
         TabIndex        =   27
         Top             =   4725
         Width           =   465
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         Caption         =   "Prazo M�dio Final:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3420
         TabIndex        =   26
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         Caption         =   "Prazo M�dio Inicial:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   25
         Top             =   2880
         Width           =   1725
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Plano:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   24
         Top             =   2385
         Width           =   600
      End
      Begin VB.Label lblNomeFornecedor 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2250
         TabIndex        =   23
         Top             =   945
         Width           =   4200
      End
      Begin HyperLinkLabel.HyperLabel lnk 
         Height          =   210
         Left            =   180
         TabIndex        =   22
         ToolTipText     =   "Clique aqui para buscar o fornecedor"
         Top             =   945
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   370
         Caption         =   "Cod. Fornecedor:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Cod. DPK:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   21
         Top             =   1890
         Width           =   915
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Subgrupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   3240
         TabIndex        =   20
         Top             =   1395
         Width           =   915
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   19
         Top             =   1395
         Width           =   915
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6660
      Width           =   6765
      _ExtentX        =   11933
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   11880
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   15
      Top             =   810
      Width           =   6675
      _ExtentX        =   11774
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   16
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":23D2
      PICN            =   "frmIncluir.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   1260
      TabIndex        =   17
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmIncluir.frx":30C8
      PICN            =   "frmIncluir.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmIncluir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vGRUPO, vSUBGRUPO, vDPK, vPLANO, vAVISTA
Dim strResposta, RespBloqueio, RespPolitica

Private Sub cmbFornecedor_Click()

    Dim cod As Integer
      
    If cmbFornecedor <> " " And cmbFornecedor <> "" Then
    
        cod = Mid(cmbFornecedor, InStr(1, cmbFornecedor, "-") + 1, Len(cmbFornecedor))
    
        If Not IsNull(cod) Then
        
            txtCodFornecedor.Text = cod
        
        End If
    Else
        txtCodFornecedor.Text = ""
    End If

End Sub

Private Sub cmbGrupo_click()

    Dim codgrupo As Integer
      
    If cmbGrupo <> " " Then
    
        codgrupo = Mid(Trim(cmbGrupo), 1, InStr(1, cmbGrupo, "-") - 1)
    
        If Not IsNull(codgrupo) Then
        
            Set vObjOracle = vVB_Generica_001.TabelaSubGrupo(vBanco, CInt(codgrupo))
            Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbSubGrupo, "C�D. SUBGRUPO", "DESC. SUBGRUPO")
            cmbSubGrupo.AddItem " "
        
        End If
    Else
        cmbSubGrupo.Clear
    End If

End Sub

Private Sub cmbPlano_Click()
    
    If cmbPlano.Text = " " Then
        chkAvista.Enabled = True
    Else
        chkAvista.Enabled = False
    End If
    
End Sub

Private Sub cmd1_Click()

    Screen.MousePointer = 11
    
    If txtCodFornecedor.Text = "" Then
        MsgBox "Fornecedor � Obrigat�rio", , "Aten��o!"
        txtCodFornecedor.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    If txtMDFinal.Text <> "" And txtMDInicial.Text <> "" Then
        If Val(txtMDFinal.Text) < Val(txtMDInicial.Text) Then
            MsgBox "Prazo Final menor que o Inicial", , "Aten��o!"
            txtMDFinal.SetFocus
            Screen.MousePointer = 0
            Exit Sub
        End If
    End If

    strMsg = "BLOQUEIA O PEDIDO?"
    strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
    If strResposta = vbYes Then
        RespBloqueio = "S"
    Else
        RespBloqueio = "N"
    End If

    strMsg = "PERMITIR LIBERAR NA POLITICA?"
    strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
    If strResposta = vbYes Then
        RespPolitica = "S"
    Else
        RespPolitica = "N"
    End If
    
    If txtDPK.Text = "" Then
        vDPK = 0
    Else
        vDPK = txtDPK.Text
    End If
       
    If cmbGrupo.Text = "" Then
        vGRUPO = 0
    Else
        vGRUPO = Mid(Trim(cmbGrupo), 1, InStr(1, cmbGrupo, "-") - 1)
    End If
    
    If cmbSubGrupo.Text = "" Then
        vSUBGRUPO = 0
    Else
        vSUBGRUPO = Mid(Trim(cmbSubGrupo), 1, InStr(1, cmbSubGrupo, "-") - 1)
    End If
    
    If cmbPlano.Text = "" Then
        vPLANO = ""
    Else
        vPLANO = Mid(Trim(cmbPlano), 1, InStr(1, cmbPlano, "-") - 1)
    End If
    
    If chkAvista.Value = True Then
        vAVISTA = "S"
    Else
        vAVISTA = ""
    End If
    
    vSql = "PRODUCAO.PCK_VDA900.pr_INC_BLOQUEIO(:PM_CODDPK,:PM_CODFORNEC,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_PLANO,:PM_FLAVISTA," & _
                                                ":PM_PRAZOMEDIOINICIO,:PM_PRAZOMEDIOFINAL,:PM_PCDESC,:PM_PCACRES,:PM_PCDESCSURAMA," & _
                                                ":PM_UF,:PM_TPCLIENTE,:PM_FLBLOQUEIO,:PM_FLPOLITICA,:PM_CODERRO,:PM_TXTERRO)"
     
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", vDPK, 1
    vBanco.Parameters.Remove "PM_CODFORNEC"
    vBanco.Parameters.Add "PM_CODFORNEC", txtCodFornecedor.Text, 1
    vBanco.Parameters.Remove "PM_CODGRUPO"
    vBanco.Parameters.Add "PM_CODGRUPO", vGRUPO, 1
    vBanco.Parameters.Remove "PM_CODSUBGRUPO"
    vBanco.Parameters.Add "PM_CODSUBGRUPO", vSUBGRUPO, 1
    vBanco.Parameters.Remove "PM_PLANO"
    vBanco.Parameters.Add "PM_PLANO", IIf(vPLANO <> "", vPLANO, 0), 1
    vBanco.Parameters.Remove "PM_FLAVISTA"
    vBanco.Parameters.Add "PM_FLAVISTA", vAVISTA, 1
    vBanco.Parameters.Remove "PM_PRAZOMEDIOINICIO"
    vBanco.Parameters.Add "PM_PRAZOMEDIOINICIO", IIf(Trim(txtMDInicial) <> "", Trim(txtMDInicial), 0), 1
    vBanco.Parameters.Remove "PM_PRAZOMEDIOFINAL"
    vBanco.Parameters.Add "PM_PRAZOMEDIOFINAL", IIf(Trim(txtMDFinal) <> "", Trim(txtMDFinal), 0), 1
    vBanco.Parameters.Remove "PM_PCDESC"
    vBanco.Parameters.Add "PM_PCDESC", IIf(Trim(txtPCDesc) <> "", Trim(txtPCDesc), 0), 1
    vBanco.Parameters.Remove "PM_PCACRES"
    vBanco.Parameters.Add "PM_PCACRES", IIf(Trim(txtPCAcres) <> "", Trim(txtPCAcres), 0), 1
    vBanco.Parameters.Remove "PM_PCDESCSURAMA"
    vBanco.Parameters.Add "PM_PCDESCSURAMA", IIf(Trim(txtPCDescSU) <> "", Trim(txtPCDescSU), 0), 1
    vBanco.Parameters.Remove "PM_UF"
    vBanco.Parameters.Add "PM_UF", Mid(Trim(cmbUF), 1, 2), 1
    vBanco.Parameters.Remove "PM_TPCLIENTE"
    vBanco.Parameters.Add "PM_TPCLIENTE", Mid(Trim(cmbTpCLiente), 1, 2), 1
    vBanco.Parameters.Remove "PM_FLBLOQUEIO"
    vBanco.Parameters.Add "PM_FLBLOQUEIO", RespBloqueio, 1
    vBanco.Parameters.Remove "PM_FLPOLITICA"
    vBanco.Parameters.Add "PM_FLPOLITICA", RespPolitica, 1
     
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
     
    vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
     
    If vErro <> 0 Then
        MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    Call REPLICACDS
    
    Limpar frmIncluir
    Screen.MousePointer = 0

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 1000
    
    Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, , , , 0, "A")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbFornecedor, "SIGLA FORN.", "C�D. FORNECEDOR")
    cmbFornecedor.AddItem " "
    
    Set vObjOracle = vVB_Generica_001.TipoCliente(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbTpCLiente, "C�D. TIPO", "DESC. TIPO")
    cmbTpCLiente.AddItem " "
    
    Set vObjOracle = vVB_Generica_001.TabelaUF(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbUF, "UF", "DESC. UF")
    cmbUF.AddItem " "
    
    Set vObjOracle = vVB_Generica_001.TabelaPlanoPagamento(vBanco)
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbPlano, "C�D. PLANO", "DESC. PLANO")
    cmbPlano.AddItem " "
    
End Sub

Private Sub lnk_Click()

    dlgFornecedores.Show 1

End Sub

Private Sub txtCodFornecedor_Change()

    cmbGrupo.Clear
    txtDPK.Text = ""
    txtFabrica.Text = ""
    
    If txtCodFornecedor <> "" Then
        
        Call vVB_Generica_001.ExcluiBind(vBanco)
        Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, CDbl(txtCodFornecedor), , , 0, "A")
        
        If vObjOracle.EOF Then
            
            lblNomeFornecedor = ""
            Exit Sub
        
        Else
            
            lblNomeFornecedor = vObjOracle.Fields(4)
        
        End If
        
        Set vObjOracle = vVB_Generica_001.TabelaGrupo(vBanco, , Val(txtCodFornecedor))
        Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbGrupo, "C�D. GRUPO", "DESC. GRUPO")
        cmbGrupo.AddItem " "
    
    Else
    
        lblNomeFornecedor = ""
        Exit Sub
    
    End If

End Sub


Private Sub txtCodFornecedor_LostFocus()
    If lblNomeFornecedor.Caption = "" And txtCodFornecedor <> "" Then
        MsgBox "Codigo do Fornecedor inv�lido", , "Aten��o!"
        txtCodFornecedor.SetFocus
        Exit Sub
    End If
End Sub

Private Sub txtDPK_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtDPK_LostFocus()

  If txtDPK.Text <> "" Then
    If txtCodFornecedor.Text = "" Then
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, txtDPK.Text)
       
        If Not vObjOracle.EOF Then
        
            txtFabrica = vObjOracle("C�D. F�BRICA")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente", , "Aten��o!"
            txtDPK.Text = ""
            txtDPK.SetFocus
            
        End If
    
    Else
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, txtDPK.Text, txtCodFornecedor.Text)
        
        If Not vObjOracle.EOF Then
        
            txtFabrica = vObjOracle("C�D. F�BRICA")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente para este Fornecedor", , "Aten��o!"
            txtDPK.Text = ""
            txtDPK.SetFocus
            
        End If
    End If
  End If

End Sub

    
Private Sub txtFabrica_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Maiuscula(KeyAscii)
    
End Sub


Private Sub txtFabrica_LostFocus()

  If txtFabrica <> "" Then
    If txtCodFornecedor.Text = "" Then
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, , , txtFabrica.Text)
       
        If Not vObjOracle.EOF Then
        
            txtDPK = vObjOracle("C�D. DPK")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente", , "Aten��o!"
            txtFabrica.Text = ""
            txtFabrica.SetFocus
            
        End If
    
    Else
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, , txtCodFornecedor.Text, txtFabrica.Text)
        
        If Not vObjOracle.EOF Then
        
            txtDPK = vObjOracle("C�D. DPK")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo Fabrica inixistente para este Fornecedor", , "Aten��o!"
            txtFabrica.Text = ""
            txtFabrica.SetFocus
            
        End If
    End If
  End If

End Sub

Private Sub txtCodFornecedor_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtMDFinal_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtMDInicial_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
    
End Sub

Private Sub txtPCAcres_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii)

End Sub

Private Sub txtPCDesc_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii)
    
End Sub

Private Sub txtPCDescSU_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Valor(KeyAscii)

End Sub


Sub REPLICACDS()

Dim ECHO As ICMP_ECHO_REPLY
Dim success As Long
Dim strPing As String
Dim codloja As String
Dim lngTime_Out As Long
Dim MsgErro As String

    Set vObjOracle = vVB_Generica_001.TabelaLojaVisao(vBanco, "VDA900")
    
    
    While Not vObjOracle.EOF
    
        codloja = Trim(Mid(vObjOracle("DEP�SITO"), 1, InStr(1, vObjOracle("DEP�SITO"), "-") - 1))
        
        If codloja <> "01" Then
            If Not IsNull(vObjOracle!ip) Then
                strPing = vObjOracle!ip
                lngTime_Out = IIf(vObjOracle!Time_out = 0, 300, vObjOracle!Time_out)
                success = Ping(strPing, codloja, ECHO)
                If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                    ECHO.RoundTripTime > lngTime_Out Then
                    MsgErro = MsgErro & vObjOracle("DEP�SITO") & vbCrLf
                    GoTo proximo_CD
                End If
                
                vSql = "PRODUCAO.PCK_VDA900.pr_INC_CDS_BLOQUEIO(:PM_CODLOJA,:PM_CODDPK,:PM_CODFORNEC,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_PLANO,:PM_FLAVISTA," & _
                                                                ":PM_PRAZOMEDIOINICIO,:PM_PRAZOMEDIOFINAL,:PM_PCDESC,:PM_PCACRES,:PM_PCDESCSURAMA," & _
                                                                ":PM_UF,:PM_TPCLIENTE,:PM_FLBLOQUEIO,:PM_FLPOLITICA,:PM_CODERRO,:PM_TXTERRO)"
     
                vBanco.Parameters.Remove "PM_CODLOJA"
                vBanco.Parameters.Add "PM_CODLOJA", codloja, 1
                vBanco.Parameters.Remove "PM_CODDPK"
                vBanco.Parameters.Add "PM_CODDPK", vDPK, 1
                vBanco.Parameters.Remove "PM_CODFORNEC"
                vBanco.Parameters.Add "PM_CODFORNEC", txtCodFornecedor.Text, 1
                vBanco.Parameters.Remove "PM_CODGRUPO"
                vBanco.Parameters.Add "PM_CODGRUPO", vGRUPO, 1
                vBanco.Parameters.Remove "PM_CODSUBGRUPO"
                vBanco.Parameters.Add "PM_CODSUBGRUPO", vSUBGRUPO, 1
                vBanco.Parameters.Remove "PM_PLANO"
                vBanco.Parameters.Add "PM_PLANO", IIf(vPLANO <> "", vPLANO, 0), 1
                vBanco.Parameters.Remove "PM_FLAVISTA"
                vBanco.Parameters.Add "PM_FLAVISTA", vAVISTA, 1
                vBanco.Parameters.Remove "PM_PRAZOMEDIOINICIO"
                vBanco.Parameters.Add "PM_PRAZOMEDIOINICIO", IIf(Trim(txtMDInicial) <> "", Trim(txtMDInicial), 0), 1
                vBanco.Parameters.Remove "PM_PRAZOMEDIOFINAL"
                vBanco.Parameters.Add "PM_PRAZOMEDIOFINAL", IIf(Trim(txtMDFinal) <> "", Trim(txtMDFinal), 0), 1
                vBanco.Parameters.Remove "PM_PCDESC"
                vBanco.Parameters.Add "PM_PCDESC", IIf(Trim(txtPCDesc) <> "", Trim(txtPCDesc), 0), 1
                vBanco.Parameters.Remove "PM_PCACRES"
                vBanco.Parameters.Add "PM_PCACRES", IIf(Trim(txtPCAcres) <> "", Trim(txtPCAcres), 0), 1
                vBanco.Parameters.Remove "PM_PCDESCSURAMA"
                vBanco.Parameters.Add "PM_PCDESCSURAMA", IIf(Trim(txtPCDescSU) <> "", Trim(txtPCDescSU), 0), 1
                vBanco.Parameters.Remove "PM_UF"
                vBanco.Parameters.Add "PM_UF", Mid(Trim(cmbUF), 1, 2), 1
                vBanco.Parameters.Remove "PM_TPCLIENTE"
                vBanco.Parameters.Add "PM_TPCLIENTE", Mid(Trim(cmbTpCLiente), 1, 2), 1
                vBanco.Parameters.Remove "PM_FLBLOQUEIO"
                vBanco.Parameters.Add "PM_FLBLOQUEIO", RespBloqueio, 1
                vBanco.Parameters.Remove "PM_FLPOLITICA"
                vBanco.Parameters.Add "PM_FLPOLITICA", RespPolitica, 1
                             
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
                      
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                
                If vBanco.Parameters("PM_CODERRO") <> 0 Then
                    MsgBox "Erro " & vBanco.Parameters("PM_CODERRO") & " - " & vBanco.Parameters("PM_TXTERRO"), , "ATEN��O!"
                End If
                
            End If
        End If
proximo_CD:
        vObjOracle.MoveNext
    Wend

    If MsgErro <> "" Then
        MsgErro = MsgErro & "SEM COMUNICA��O"
        MsgBox MsgErro, , "ATEN��O!"
    End If
    

End Sub
