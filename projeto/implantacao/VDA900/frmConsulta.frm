VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Object = "{7E65892D-F38B-11D2-BC07-0055003B26DE}#3.0#0"; "HyperLabel.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmConsulta 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consulta Bloqueio"
   ClientHeight    =   7110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6870
   Icon            =   "frmConsulta.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   474
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   458
   Begin VB.Frame Frame1 
      Caption         =   "Bloqueios"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   3165
      Left            =   45
      TabIndex        =   10
      Top             =   3555
      Width           =   6765
      Begin MSFlexGridLib.MSFlexGrid mfgCons 
         Height          =   2400
         Left            =   90
         TabIndex        =   17
         Top             =   315
         Width           =   6585
         _ExtentX        =   11615
         _ExtentY        =   4233
         _Version        =   393216
         BackColorBkg    =   -2147483633
         ScrollBars      =   1
         AllowUserResizing=   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Duplo click no Bloqueio para excluir"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   225
         Left            =   1485
         TabIndex        =   18
         Top             =   2790
         Width           =   3795
      End
   End
   Begin VB.Frame fra 
      Caption         =   "Frame1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2580
      Left            =   45
      TabIndex        =   9
      Top             =   900
      Width           =   6765
      Begin VB.ComboBox cmbFornecedor 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1260
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   315
         Width           =   2310
      End
      Begin VB.ComboBox cmbGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   810
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1170
         Width           =   2310
      End
      Begin VB.ComboBox cmbSubGrupo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   4185
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   1170
         Width           =   2355
      End
      Begin VB.TextBox txtDPK 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1080
         TabIndex        =   4
         Top             =   1620
         Width           =   1815
      End
      Begin VB.TextBox txtCodFornecedor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1260
         MaxLength       =   3
         TabIndex        =   1
         Top             =   765
         Width           =   510
      End
      Begin VB.TextBox txtFabrica 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   4365
         TabIndex        =   5
         Top             =   1620
         Width           =   1815
      End
      Begin Bot�o.cmd cmd1 
         Height          =   375
         Left            =   5445
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   2070
         Width           =   1185
         _ExtentX        =   2090
         _ExtentY        =   661
         BTYPE           =   3
         TX              =   "Consultar"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   1
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "frmConsulta.frx":23D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   0
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label Label12 
         Appearance      =   0  'Flat
         Caption         =   "Fornecedor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   180
         TabIndex        =   20
         Top             =   360
         Width           =   960
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Grupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   180
         TabIndex        =   16
         Top             =   1215
         Width           =   915
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "Subgrupo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   3240
         TabIndex        =   15
         Top             =   1215
         Width           =   915
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         Caption         =   "Cod. DPK:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   180
         TabIndex        =   14
         Top             =   1665
         Width           =   915
      End
      Begin HyperLinkLabel.HyperLabel lnk 
         Height          =   210
         Left            =   180
         TabIndex        =   13
         ToolTipText     =   "Clique aqui para buscar o fornecedor"
         Top             =   810
         Width           =   990
         _ExtentX        =   1746
         _ExtentY        =   370
         Caption         =   "Fornecedor:"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblNomeFornecedor 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   1890
         TabIndex        =   12
         Top             =   765
         Width           =   4515
      End
      Begin VB.Label Label11 
         Appearance      =   0  'Flat
         Caption         =   "Cod. Fabrica:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   3240
         TabIndex        =   11
         Top             =   1665
         Width           =   1050
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   6780
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12065
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   7
      Top             =   810
      Width           =   6765
      _ExtentX        =   11933
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmConsulta.frx":23EE
      PICN            =   "frmConsulta.frx":240A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmConsulta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim vGRUPO, vSUBGRUPO, vDPK, vFORNECEDOR, Linha

Private Sub cmbGrupo_click()

    Dim codgrupo As Integer
      
    If cmbGrupo <> " " Then
    
        codgrupo = Mid(Trim(cmbGrupo), 1, InStr(1, cmbGrupo, "-") - 1)
    
        If Not IsNull(codgrupo) Then
        
            Set vObjOracle = vVB_Generica_001.TabelaSubGrupo(vBanco, CInt(codgrupo))
            Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbSubGrupo, "C�D. SUBGRUPO", "DESC. SUBGRUPO")
            cmbSubGrupo.AddItem " "
        
        End If
    Else
        cmbSubGrupo.Clear
    End If

End Sub


Private Sub cmd1_Click()
Dim strResposta

    Screen.MousePointer = 11
    
    mfgCons.Clear
    
    If txtCodFornecedor.Text = "" Then
        MsgBox "Fornecedor � Obrigat�rio", , "Aten��o!"
        txtCodFornecedor.SetFocus
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    
    If txtDPK.Text = "" Then
        vDPK = 0
    Else
        vDPK = txtDPK.Text
    End If
       
    If cmbGrupo.Text = "" Then
        vGRUPO = 0
    Else
        vGRUPO = Mid(Trim(cmbGrupo), 1, InStr(1, cmbGrupo, "-") - 1)
    End If
    
    If cmbSubGrupo.Text = "" Then
        vSUBGRUPO = 0
    Else
        vSUBGRUPO = Mid(Trim(cmbSubGrupo), 1, InStr(1, cmbSubGrupo, "-") - 1)
    End If


    vSql = "PRODUCAO.PCK_VDA900.pr_CON_BLOQUEIO(:PM_CODDPK,:PM_CODFORNEC,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"
     
    vBanco.Parameters.Remove "PM_CODDPK"
    vBanco.Parameters.Add "PM_CODDPK", vDPK, 1
    vBanco.Parameters.Remove "PM_CODFORNEC"
    vBanco.Parameters.Add "PM_CODFORNEC", txtCodFornecedor.Text, 1
    vBanco.Parameters.Remove "PM_CODGRUPO"
    vBanco.Parameters.Add "PM_CODGRUPO", vGRUPO, 1
    vBanco.Parameters.Remove "PM_CODSUBGRUPO"
    vBanco.Parameters.Add "PM_CODSUBGRUPO", vSUBGRUPO, 1
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
     
    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2
          
    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
    
    If vObjOracle.EOF And vObjOracle.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha Politica de Bloqueio para esta consulta", vbInformation, "Aten��o"
        Screen.MousePointer = 0
        Exit Sub
    End If
    
    vVB_Generica_001.CarregaGridTabela mfgCons, vObjOracle, 16
       
    Screen.MousePointer = 0

End Sub



Private Sub mfgCons_DblClick()
    
    Linha = mfgCons.Row
    
    With mfgCons
    
        .Col = 1
         vFORNECEDOR = .Text
        
        .Col = 2
        vGRUPO = .Text
        
        .Col = 3
        vSUBGRUPO = .Text
        
        .Col = 4
        vDPK = .Text
        
    End With
    
    strMsg = "Confirma a exclus�o?"
    strResposta = MsgBox(strMsg, vbYesNo, "ATEN��O")
    If strResposta = vbYes Then
    
        vSql = "PRODUCAO.PCK_VDA900.pr_ACT_BLOQUEIO(:PM_CODDPK,:PM_CODFORNEC,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
         
        vBanco.Parameters.Remove "PM_CODDPK"
        vBanco.Parameters.Add "PM_CODDPK", vDPK, 1
        vBanco.Parameters.Remove "PM_CODFORNEC"
        vBanco.Parameters.Add "PM_CODFORNEC", vFORNECEDOR, 1
        vBanco.Parameters.Remove "PM_CODGRUPO"
        vBanco.Parameters.Add "PM_CODGRUPO", vGRUPO, 1
        vBanco.Parameters.Remove "PM_CODSUBGRUPO"
        vBanco.Parameters.Add "PM_CODSUBGRUPO", vSUBGRUPO, 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "D", 1
         
        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2
              
        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
         
        vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
         
        If vErro <> 0 Then
            MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
            Screen.MousePointer = 0
            Exit Sub
        End If
        
        If mfgCons.Rows = 2 Then
            For i = 1 To mfgCons.Cols - 1
                mfgCons.TextMatrix(1, i) = ""
            Next
        Else
            mfgCons.RemoveItem Linha
        End If
        
        Call RemoveCDs
        
        MsgBox "Exclus�o OK", , ""
    
    End If
    
    
End Sub

Private Sub txtCodFornecedor_Change()

    cmbGrupo.Clear
    txtDPK.Text = ""
    txtFabrica.Text = ""
    
    If txtCodFornecedor <> "" Then
        
        Call vVB_Generica_001.ExcluiBind(vBanco)
        Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, CDbl(txtCodFornecedor))
        
        If vObjOracle.EOF Then
            
            lblNomeFornecedor = ""
            Exit Sub
        
        Else
            
            lblNomeFornecedor = vObjOracle.Fields(4)
        
        End If
        
        Set vObjOracle = vVB_Generica_001.TabelaGrupo(vBanco, , Val(txtCodFornecedor))
        Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbGrupo, "C�D. GRUPO", "DESC. GRUPO")
        cmbGrupo.AddItem " "
    
    Else
    
        lblNomeFornecedor = ""
        Exit Sub
    
    End If

End Sub


Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 2000
    Me.Left = 1000
    
    Set vObjOracle = vVB_Generica_001.TabelaFornecedor(vBanco, , , , 0, "A")
    Call vVB_Generica_001.PreencheComboList(vObjOracle, cmbFornecedor, "SIGLA FORN.", "C�D. FORNECEDOR")
    cmbFornecedor.AddItem " "
    
End Sub

Private Sub cmbFornecedor_Click()

    Dim cod As Integer
      
    If cmbFornecedor <> " " Then
    
        cod = Mid(cmbFornecedor, InStr(1, cmbFornecedor, "-") + 1, Len(cmbFornecedor))
    
        If Not IsNull(cod) Then
        
            txtCodFornecedor.Text = cod
        
        End If
    Else
        txtCodFornecedor.Text = ""
    End If

End Sub

Private Sub txtCodFornecedor_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtDPK_KeyPress(KeyAscii As Integer)

    KeyAscii = vVB_Generica_001.Numero(KeyAscii)

End Sub

Private Sub txtDPK_LostFocus()

  If txtDPK.Text <> "" Then
    If txtCodFornecedor.Text = "" Then
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, txtDPK.Text)
       
        If Not vObjOracle.EOF Then
        
            txtFabrica = vObjOracle("C�D. F�BRICA")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente", , "Aten��o!"
            txtDPK.Text = ""
            txtDPK.SetFocus
            
        End If
    
    Else
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, txtDPK.Text, txtCodFornecedor.Text)
        
        If Not vObjOracle.EOF Then
        
            txtFabrica = vObjOracle("C�D. F�BRICA")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente para este Fornecedor", , "Aten��o!"
            txtDPK.Text = ""
            txtDPK.SetFocus
            
        End If
    End If
  End If

End Sub

Private Sub txtFabrica_LostFocus()

  If txtFabrica <> "" Then
    If txtCodFornecedor.Text = "" Then
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, , , txtFabrica.Text)
       
        If Not vObjOracle.EOF Then
        
            txtDPK = vObjOracle("C�D. DPK")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo DPK inixistente", , "Aten��o!"
            txtFabrica.Text = ""
            txtFabrica.SetFocus
            
        End If
    
    Else
        
        vBanco.Parameters.Remove ("P_SITUACAO")
        Set vObjOracle = vVB_Venda_001.InformacoesItem(vBanco, 1, , txtCodFornecedor.Text, txtFabrica.Text)
        
        If Not vObjOracle.EOF Then
        
            txtDPK = vObjOracle("C�D. DPK")
            txtCodFornecedor = vObjOracle("C�D. FORNECEDOR")
            
        Else
            
            MsgBox "Codigo Fabrica inixistente para este Fornecedor", , "Aten��o!"
            txtFabrica.Text = ""
            txtFabrica.SetFocus
            
        End If
    End If
  End If

End Sub


Sub RemoveCDs()

Dim ECHO As ICMP_ECHO_REPLY
Dim success As Long
Dim strPing As String
Dim codloja As String
Dim lngTime_Out As Long
Dim MsgErro As String

Set vObjOracle = vVB_Generica_001.TabelaLojaVisao(vBanco, "VDA900")
    
    
    While Not vObjOracle.EOF
    
        codloja = Trim(Mid(vObjOracle("DEP�SITO"), 1, InStr(1, vObjOracle("DEP�SITO"), "-") - 1))
        
        If codloja <> "01" Then
            If Not IsNull(vObjOracle!ip) Then
                strPing = vObjOracle!ip
                lngTime_Out = IIf(vObjOracle!Time_out = 0, 300, vObjOracle!Time_out)
                success = Ping(strPing, codloja, ECHO)
                If InStr(GetStatusCode(success), ("ip success")) = 0 Or _
                    ECHO.RoundTripTime > lngTime_Out Then
                    MsgErro = MsgErro & vObjOracle("DEP�SITO") & vbCrLf
                    GoTo proximo_CD
                End If

                vSql = "PRODUCAO.PCK_VDA900.pr_ACT_CDS_BLOQUEIO(:PM_CODLOJA,:PM_CODDPK,:PM_CODFORNEC,:PM_CODGRUPO,:PM_CODSUBGRUPO,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"
                 
                vBanco.Parameters.Remove "PM_CODLOJA"
                vBanco.Parameters.Add "PM_CODLOJA", codloja, 1
                vBanco.Parameters.Remove "PM_CODDPK"
                vBanco.Parameters.Add "PM_CODDPK", vDPK, 1
                vBanco.Parameters.Remove "PM_CODFORNEC"
                vBanco.Parameters.Add "PM_CODFORNEC", vFORNECEDOR, 1
                vBanco.Parameters.Remove "PM_CODGRUPO"
                vBanco.Parameters.Add "PM_CODGRUPO", vGRUPO, 1
                vBanco.Parameters.Remove "PM_CODSUBGRUPO"
                vBanco.Parameters.Add "PM_CODSUBGRUPO", vSUBGRUPO, 1
                
                vBanco.Parameters.Remove "PM_ACT"
                vBanco.Parameters.Add "PM_ACT", "D", 1
                 
                vBanco.Parameters.Remove "PM_CODERRO"
                vBanco.Parameters.Add "PM_CODERRO", 0, 2
                vBanco.Parameters.Remove "PM_TXTERRO"
                vBanco.Parameters.Add "PM_TXTERRO", "", 2
                      
                vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
                 
                vErro = IIf(vBanco.Parameters("PM_CODERRO") = "", 0, vBanco.Parameters("PM_CODERRO"))
                 
                If vErro <> 0 Then
                    MsgBox vBanco.Parameters("PM_TXTERRO") & ".Ligue para o Depto de Sistemas"
                End If
                
            End If
        End If
proximo_CD:
        vObjOracle.MoveNext
    Wend

    If MsgErro <> "" Then
        MsgErro = "SEM COMUNICA��O"
        MsgBox MsgErro, , "ATEN��O!"
    End If

End Sub
