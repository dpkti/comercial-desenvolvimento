VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmGER0401 
   Caption         =   "ATUALIZA��O"
   ClientHeight    =   4500
   ClientLeft      =   105
   ClientTop       =   1545
   ClientWidth     =   9465
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4500
   ScaleWidth      =   9465
   Begin VB.CommandButton Command1 
      Caption         =   "INICIALIZA ATUALIZA��O"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   2700
      TabIndex        =   0
      Top             =   1260
      Width           =   4335
   End
   Begin Threed.SSPanel SSPanel1 
      Height          =   735
      Left            =   1035
      TabIndex        =   1
      Top             =   2670
      Width           =   7455
      _Version        =   65536
      _ExtentX        =   13150
      _ExtentY        =   1296
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin MSComctlLib.ProgressBar prgbar1 
         Height          =   495
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   873
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin Threed.SSPanel SSPanel2 
      Height          =   495
      Left            =   45
      TabIndex        =   2
      Top             =   4005
      Width           =   9495
      _Version        =   65536
      _ExtentX        =   16748
      _ExtentY        =   873
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin Threed.SSPanel SSPanel3 
      Height          =   780
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   9465
      _Version        =   65536
      _ExtentX        =   16695
      _ExtentY        =   1376
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand scmdSair 
         Height          =   735
         Left            =   45
         TabIndex        =   5
         Top             =   0
         Width           =   780
         _Version        =   65536
         _ExtentX        =   1376
         _ExtentY        =   1296
         _StockProps     =   78
         Picture         =   "fil0401.frx":0000
      End
   End
   Begin VB.Label lblPercent 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "0%"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   4590
      TabIndex        =   3
      Top             =   3510
      Width           =   330
   End
End
Attribute VB_Name = "frmGER0401"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit
Private Sub INICIA_ATUALIZACAO()
      Dim BOERRO As Boolean
      Dim RESP As Integer
      Dim inicio As String, fim As String
      Dim dblAtual As Double

1     inicio = Time

2     Screen.MousePointer = 11

      '45 � o n�mero de tabelas a atualizar
3     prgbar1.Max = 45 * numArq
4     dblAtual = 0

5     Do While contArq <= numArq

          'DELETA_APLICACAO - 1
6         If Not DEL_APLICACAO Then
7             GoTo Trata_Erro
8         End If
9         dblAtual = dblAtual + 1
10        prgbar1.Value = dblAtual
11        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
12        DoEvents
          
          'DELETA_CLIENTE - 2
13        If Not DEL_CLIENTE Then
14            GoTo Trata_Erro
15        End If
16        dblAtual = dblAtual + 1
17        prgbar1.Value = dblAtual
18        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
19        DoEvents
          
          'DELETA_REPRESENTANTE - 3
20        If Not DEL_REPR Then
21            GoTo Trata_Erro
22        End If
23        dblAtual = dblAtual + 1
24        prgbar1.Value = dblAtual
25        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
26        DoEvents
          
          'DELETA_TABELA_DESCPER - 4
27        If Not DEL_TABDESC Then
28            GoTo Trata_Erro
29        End If
30        dblAtual = dblAtual + 1
31        prgbar1.Value = dblAtual
32        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
33        DoEvents

          'DELETA_PEDNOTA_VENDA E ITPEDNOTA_VENDA - 45
34        If Not DEL_PEDNOTA Then
35            GoTo Trata_Erro
36        End If
37        dblAtual = dblAtual + 1
38        prgbar1.Value = dblAtual
39        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
40        DoEvents

          'APLICACAO - 5
41        If Not ATU_APLICACAO Then
42            GoTo Trata_Erro
43        End If
44        dblAtual = dblAtual + 1
45        prgbar1.Value = dblAtual
46        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
47        DoEvents

          ' BANCO - 6
48        If Not ATU_BANCO Then
49            GoTo Trata_Erro
50        End If
51        dblAtual = dblAtual + 1
52        prgbar1.Value = dblAtual
53        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
54        DoEvents

          ' CATEG_SINAL - 7
55        If Not ATU_CATEGSINAL Then
56            GoTo Trata_Erro
57        End If
58        dblAtual = dblAtual + 1
59        prgbar1.Value = dblAtual
60        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
61        DoEvents
          
          ' CIDADE - 8
62        If Not ATU_CIDADE Then
63            GoTo Trata_Erro
64        End If
65        dblAtual = dblAtual + 1
66        prgbar1.Value = dblAtual
67        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
68        DoEvents
          
          'CLIE_CREDITO - 9
69        If Not ATU_CLIECRED Then
70            GoTo Trata_Erro
71        End If
72        dblAtual = dblAtual + 1
73        prgbar1.Value = dblAtual
74        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
75        DoEvents
          
          'CLIE_ENDERECO - 10
76        If Not ATU_CLIEEND Then
77            GoTo Trata_Erro
78        End If
79        dblAtual = dblAtual + 1
80        prgbar1.Value = dblAtual
81        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
82        DoEvents
          
          'CLIE_MENSAGEM - 11
83        If Not ATU_CLIEMSG Then
84            GoTo Trata_Erro
85        End If
86        dblAtual = dblAtual + 1
87        prgbar1.Value = dblAtual
88        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
89        DoEvents
          
          'CLIENTE - 12
90        If Not ATU_CLIENTE Then
91            GoTo Trata_Erro
92        End If
93        dblAtual = dblAtual + 1
94        prgbar1.Value = dblAtual
95        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
96        DoEvents
          
          'DATAS - 13
97        If Not ATU_DATAS Then
98            GoTo Trata_Erro
99        End If
100       dblAtual = dblAtual + 1
101       prgbar1.Value = dblAtual
102       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
103       DoEvents
              
          'DOLAR_DIARIO - 14
104       If Not ATU_DOLARDIARIO Then
105           GoTo Trata_Erro
106       End If
107       dblAtual = dblAtual + 1
108       prgbar1.Value = dblAtual
109       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
110       DoEvents

          'DUPLICATA - 15
111       If Not ATU_DUPLICATA Then
112           GoTo Trata_Erro
113       End If
114       dblAtual = dblAtual + 1
115       prgbar1.Value = dblAtual
116       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
117       DoEvents
          
          'ESTATISTICA_ITEM - 16
          'If Not ATU_ESTAITEM Then
          '    GoTo TRATA_ERRO
          'End If
          'dblAtual = dblAtual + 1
          'PrgBar1.Value = dblAtual
          'lblPercent.Caption = Format(dblAtual * 100 / PrgBar1.Max, "##0") & "%"
          'DoEvents
          
          'FILIAL - 17
118       If Not ATU_FILIAL Then
119           GoTo Trata_Erro
120       End If
121       dblAtual = dblAtual + 1
122       prgbar1.Value = dblAtual
123       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
124       DoEvents
          
          'FORNECEDOR - 18
125       If Not ATU_FORNECEDOR Then
126           GoTo Trata_Erro
127       End If
128       dblAtual = dblAtual + 1
129       prgbar1.Value = dblAtual
130       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
131       DoEvents

          'GRUPO - 19
132       If Not ATU_GRUPO Then
133           GoTo Trata_Erro
134       End If
135       dblAtual = dblAtual + 1
136       prgbar1.Value = dblAtual
137       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
138       DoEvents
          
          'ITEM_ANALITICO - 20
139       If Not ATU_ITEMANAL Then
140           GoTo Trata_Erro
141       End If
142       dblAtual = dblAtual + 1
143       prgbar1.Value = dblAtual
144       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
145       DoEvents
          
          'ITEM_CADASTRO - 21
146       If Not ATU_ITEMCAD Then
147           GoTo Trata_Erro
148       End If
149       dblAtual = dblAtual + 1
150       prgbar1.Value = dblAtual
151       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
152       DoEvents
          
          'ITEM_CUSTO - 22
153       If Not ATU_ITEMCUSTO Then
154           GoTo Trata_Erro
155       End If
156       dblAtual = dblAtual + 1
157       prgbar1.Value = dblAtual
158       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
159       DoEvents
          
          'ITEM_ESTOQUE - 23
160       If Not ATU_ITEMESTOQUE Then
161           GoTo Trata_Erro
162       End If
163       dblAtual = dblAtual + 1
164       prgbar1.Value = dblAtual
165       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
166       DoEvents
          
          'ITEM_GLOBAL - 24
167       If Not ATU_ITEMGLOBAL Then
168           GoTo Trata_Erro
169       End If
170       dblAtual = dblAtual + 1
171       prgbar1.Value = dblAtual
172       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
173       DoEvents
          
          'ITEM_PRECO - 25
174       If Not ATU_ITEMPRECO Then
175           GoTo Trata_Erro
176       End If
177       dblAtual = dblAtual + 1
178       prgbar1.Value = dblAtual
179       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
180       DoEvents

          'MONTADORA - 26
181       If Not ATU_MONTADORA Then
182           GoTo Trata_Erro
183       End If
184       dblAtual = dblAtual + 1
185       prgbar1.Value = dblAtual
186       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
187       DoEvents
          
          'PLANO_PGTO - 27
188       If Not ATU_PLANO Then
189           GoTo Trata_Erro
190       End If
191       dblAtual = dblAtual + 1
192       prgbar1.Value = dblAtual
193       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
194       DoEvents
          
          'R_REPCGC - 28
195       If Not ATU_REPCGC Then
196           GoTo Trata_Erro
197       End If
198       dblAtual = dblAtual + 1
199       prgbar1.Value = dblAtual
200       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
201       DoEvents
          
          'R_REPVEN - 29
202       If Not ATU_REPVEN Then
203           GoTo Trata_Erro
204       End If
205       dblAtual = dblAtual + 1
206       prgbar1.Value = dblAtual
207       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
208       DoEvents
          
          'REPR_END_CORRESP - 30
209       If Not ATU_REPEND Then
210           GoTo Trata_Erro
211       End If
212       dblAtual = dblAtual + 1
213       prgbar1.Value = dblAtual
214       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
215       DoEvents

          'REPRESENTACAO - 31
216       If Not ATU_REPRESENTACAO Then
217           GoTo Trata_Erro
218       End If
219       dblAtual = dblAtual + 1
220       prgbar1.Value = dblAtual
221       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
222       DoEvents
          
          'REPRESENTANTE - 32
223       If Not ATU_REPRESENTANTE Then
224            GoTo Trata_Erro
225       End If
226       dblAtual = dblAtual + 1
227       prgbar1.Value = dblAtual
228       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
229       DoEvents

          'SUBGRUPO - 33
230       If Not ATU_SUBGRUPO Then
231           GoTo Trata_Erro
232       End If
233       dblAtual = dblAtual + 1
234       prgbar1.Value = dblAtual
235       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
236       DoEvents
          
          'SUBST_TRIBUTARIA - 34
237       If Not ATU_SUBTRIB Then
238           GoTo Trata_Erro
239       End If
240       dblAtual = dblAtual + 1
241       prgbar1.Value = dblAtual
242       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
243       DoEvents
          
          'TABELA_DESCPER - 35
244       If Not ATU_TABDESCPER Then
245           GoTo Trata_Erro
246       End If
247       dblAtual = dblAtual + 1
248       prgbar1.Value = dblAtual
249       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
250       DoEvents
          
          'TABELA_VENDA - 36
251       If Not ATU_TABVENDA Then
252           GoTo Trata_Erro
253       End If
254       dblAtual = dblAtual + 1
255       prgbar1.Value = dblAtual
256       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
257       DoEvents

          'TAXA - 37
          'If Not ATU_TAXA Then
          '    GoTo TRATA_ERRO
          'End If
          'dblAtual = dblAtual + 1
          'PrgBar1.Value = dblAtual
          'lblPercent.Caption = Format(dblAtual * 100 / PrgBar1.Max, "##0") & "%"
          'DoEvents
          
          'TIPO DE CLIENTE - 38
258       If Not ATU_TPCLIE Then
259           GoTo Trata_Erro
260       End If
261       dblAtual = dblAtual + 1
262       prgbar1.Value = dblAtual
263       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
264       DoEvents
          
          'TRANSPORTADORA - 39
265       If Not ATU_TRANSPORTADORA Then
266           GoTo Trata_Erro
267       End If
268       dblAtual = dblAtual + 1
269       prgbar1.Value = dblAtual
270       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
271       DoEvents
          
          'UF - 40
272       If Not ATU_UF Then
273           GoTo Trata_Erro
274       End If
275       dblAtual = dblAtual + 1
276       prgbar1.Value = dblAtual
277       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
278       DoEvents
          
          'UF_DEPOSITO - 41
279       If Not ATU_UF_DEPOSITO Then
280           GoTo Trata_Erro
281       End If
282       dblAtual = dblAtual + 1
283       prgbar1.Value = dblAtual
284       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
285       DoEvents
          
          'UF_ORIGEM_DESTINO - 42
286       If Not ATU_UF_ORIGDEST Then
287           GoTo Trata_Erro
288       End If
289       dblAtual = dblAtual + 1
290       prgbar1.Value = dblAtual
291       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
292       DoEvents

          'UF_DPK - 43
293       If Not ATU_UFDPK Then
294           GoTo Trata_Erro
295       End If
296       dblAtual = dblAtual + 1
297       prgbar1.Value = dblAtual
298       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
299       DoEvents
         
          'SALDO_PEDIDOS - 44
300       If Not ATU_SALDOPED Then
301           GoTo Trata_Erro
302       End If
303       dblAtual = dblAtual + 1
304       prgbar1.Value = dblAtual
305       lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
306       DoEvents
         
307      contArq = contArq + 1
308      If contArq <= numArq Then
309           dbExt.Close
310           mdiGER040.File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
311           mdiGER040.File1.Pattern = "*.mdb"
312           nomeArq = mdiGER040.File1.List(contArq - 1)

313           On Error GoTo TRATA_ERRO_AUXILIAR
314           Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
              
315      End If
         
316   Loop

      'SSPanel2.Caption = "Atualiza��o Finalizada com " & dblatu & " Registros Atualizados Ok! "
317   If Trim(Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB")) <> "" Then
318       Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"
319   End If
         
320   Screen.MousePointer = 1
         
321   fim = Time

322   MsgBox ("Inicio : " & inicio & " Fim: " & fim)

      'Call FUNCAO_ERRO(1, "Retorno da Atualiza��o", "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim, "Dep. Opera��o(CPD) ", "")

323   strSQL = "INSERT INTO LOGDIV " & _
              " ( DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL ) VALUES " & _
              " ( '" & Now & "' , 1, 'Retorno da Atualiza��o', '" & _
              "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim & "', '" & _
              "Dep. Opera��o (CPD) ' ) "
              
324   dbControle.Execute strSQL, dbFailOnError

325   Screen.MousePointer = 1

326   Exit Sub

Trata_Erro:

327   If BOERRO Then
328       SSPanel2.Caption = "Problemas na Atualiza��o !!! "
329       Screen.MousePointer = 1

330       Call FUNCAO_ERRO("")
331   End If

332   RESP = MsgBox("Continua a Atualiza��o ?", vbYesNo)
333   If RESP = vbYes Then
334       Resume Next
335   End If

336   Exit Sub

TRATA_ERRO_AUXILIAR:

337   If Err = 3049 Then

338       MousePointer = 11
339       DoEvents

          ' Reparar Banco de Dados
340       RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
          
341       COMPACTA_AUXILIAR
          
342       MousePointer = 0
          ' Ap�s corrigir o banco danificado processa novamente a linha do programa
          ' que gerou o erro.
343       Resume 0
344   Else

345       Call FUNCAO_ERRO("")
346       MsgBox "Sub Inicia_Atualizacao" & vbCrLf & "ERRO: " & Err.Number & " - " & Err.Description

347   End If

End Sub

Private Sub Command1_Click()
Command1.Enabled = False
INICIA_ATUALIZACAO
Command1.Enabled = True
End Sub

Private Sub Form_Load()
'    If numArq > 0 Then
'        Command1_Click
'    End If
End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
SSPanel2.Caption = ""
End Sub


Private Sub scmdSair_Click()
If numArq > 0 Then
    dbExt.Close
    dbLocal.Close
    dbControle.Close
End If
Unload Me
End Sub


Private Sub scmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
SSPanel2.Caption = "Abandona Atualiza��o ..."
End Sub


Private Sub SSPanel3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
SSPanel2.Caption = ""
End Sub


