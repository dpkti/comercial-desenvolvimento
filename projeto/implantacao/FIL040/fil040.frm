VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.MDIForm mdiGER040 
   BackColor       =   &H00C0C0C0&
   Caption         =   "ATUALIZA��O "
   ClientHeight    =   8430
   ClientLeft      =   2160
   ClientTop       =   2265
   ClientWidth     =   12780
   Icon            =   "fil040.frx":0000
   LinkTopic       =   "MDIForm1"
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   10000
      Left            =   1950
      Top             =   1290
   End
   Begin Threed.SSPanel sspFaixa 
      Align           =   1  'Align Top
      Height          =   1185
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   12780
      _Version        =   65536
      _ExtentX        =   22542
      _ExtentY        =   2090
      _StockProps     =   15
      ForeColor       =   -2147483630
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.FileListBox File1 
         Height          =   870
         Left            =   5520
         Pattern         =   "*.MDB"
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   1215
      End
      Begin Threed.SSCommand scmdBackup 
         Height          =   735
         Left            =   3465
         TabIndex        =   9
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Picture         =   "fil040.frx":08CA
      End
      Begin Threed.SSCommand scmdCompacta 
         Height          =   735
         Left            =   2610
         TabIndex        =   6
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Picture         =   "fil040.frx":15A4
      End
      Begin Threed.SSCommand scmdAtualiza 
         Height          =   735
         Left            =   90
         TabIndex        =   4
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         ForeColor       =   -2147483630
         Enabled         =   0   'False
         Picture         =   "fil040.frx":227E
      End
      Begin Threed.SSCommand scmdDiv 
         Height          =   735
         Left            =   930
         TabIndex        =   3
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         ForeColor       =   -2147483630
         Picture         =   "fil040.frx":2F58
      End
      Begin Threed.SSCommand scmdComunica 
         Height          =   735
         Left            =   1770
         TabIndex        =   2
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         ForeColor       =   -2147483630
         Picture         =   "fil040.frx":3C32
      End
      Begin Threed.SSCommand scmdSair 
         Height          =   735
         Left            =   4305
         TabIndex        =   1
         Top             =   90
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         ForeColor       =   -2147483630
         Picture         =   "fil040.frx":490C
      End
      Begin VB.Label lblHelp 
         AutoSize        =   -1  'True
         BackColor       =   &H0000FFFF&
         Caption         =   "Back-up"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3465
         TabIndex        =   5
         Top             =   870
         Visible         =   0   'False
         Width           =   750
      End
   End
   Begin Threed.SSPanel SSPanel1 
      Align           =   4  'Align Right
      Height          =   6750
      Index           =   0
      Left            =   -2970
      TabIndex        =   10
      Top             =   1185
      Width           =   15750
      _Version        =   65536
      _ExtentX        =   27781
      _ExtentY        =   11906
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSPanel SSPanel5 
         Height          =   645
         Left            =   4200
         TabIndex        =   11
         Top             =   1950
         Visible         =   0   'False
         Width           =   7455
         _Version        =   65536
         _ExtentX        =   13150
         _ExtentY        =   1138
         _StockProps     =   15
         ForeColor       =   -2147483630
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin MSComctlLib.ProgressBar prgbar1 
            Height          =   435
            Left            =   120
            TabIndex        =   16
            Top             =   120
            Width           =   7215
            _ExtentX        =   12726
            _ExtentY        =   767
            _Version        =   393216
            Appearance      =   1
            Scrolling       =   1
         End
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   510
         Left            =   3135
         TabIndex        =   13
         Top             =   4410
         Width           =   9600
         _Version        =   65536
         _ExtentX        =   16933
         _ExtentY        =   900
         _StockProps     =   15
         ForeColor       =   255
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label2 
         Caption         =   "Label2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000C0&
         Height          =   495
         Left            =   5280
         TabIndex        =   15
         Top             =   3000
         Visible         =   0   'False
         Width           =   5415
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4800
         TabIndex        =   14
         Top             =   3360
         Width           =   6135
      End
      Begin VB.Label lblPercent 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "0%"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7755
         TabIndex        =   12
         Top             =   2790
         Visible         =   0   'False
         Width           =   330
      End
   End
   Begin VB.PictureBox stbRodape 
      Align           =   2  'Align Bottom
      Height          =   495
      Left            =   0
      ScaleHeight     =   435
      ScaleWidth      =   12720
      TabIndex        =   7
      Top             =   7935
      Width           =   12780
   End
   Begin VB.Menu mnuAtu 
      Caption         =   "&Atualiza��o"
   End
   Begin VB.Menu mnuLOG 
      Caption         =   "&Consulta LOG's"
      Begin VB.Menu mnuDIV 
         Caption         =   "&Diverg�ncias"
      End
      Begin VB.Menu mnuComunica 
         Caption         =   "C&omunica��o"
      End
   End
   Begin VB.Menu mnuCompacta 
      Caption         =   "Com&pacta Banco de Dados"
   End
   Begin VB.Menu mnuBackup 
      Caption         =   "&Back-up"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "So&bre"
      NegotiatePosition=   3  'Right
   End
End
Attribute VB_Name = "mdiGER040"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim vCamposJoin
Private Sub COMECA_ATUAL()

    On Error GoTo Trata_Erro

    Dim BOERRO As Boolean
    Dim RESP As Integer
    Dim inicio As String, fim As String
    Dim dblAtual As Double
    Dim FL_VDR As String
    Dim SQL As String

    inicio = Time
    
    Screen.MousePointer = 11
   
   '72 tabelas de atualiza��o
   '11 tabelas de dele��o
   '02 tabelas desativadas
   '06 tabelas VDR
   '02 tabelas de dele��o VDR
   
    prgbar1.Max = 100 * numArq
    dblAtual = 0
    FL_VDR = "N"
    
   '-- Verifica se � VDR
    Set dbaccess1 = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB")
    
    SQL = " SELECT FL_VDR FROM DEPOSITO"
    
    Set ss = dbaccess1.CreateDynaset(SQL)
        
        
    If Not ss.EOF Then
        FL_VDR = ss!FL_VDR
    Else
        FL_VDR = "N"
    End If
    
    dbaccess1.Close
        
   '---------------------
    Do While contArq <= numArq
    
       'Controle - 0
       Label1.Caption = "Tabela em Atualiza��o: Controle"
       If Not ATU_CONTROLE Then
          GoTo Trata_Erro
       End If
       
       'DATAS - 1
        Label1.Caption = "Tabela em Atualiza��o: Datas "
        If Not ATU_DATAS Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
        'DEL_CLASS_FISCAL_RED_BASE - 1
        Label1.Caption = "Tabela em Atualiza��o: Deleta Class_Fiscal_Red_Base "
        If Not DEL_CLASS_FISCAL_RED_BASE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'DEL_R_FABRICA_DPK - 2
        Label1.Caption = "Tabela em Atualiza��o: Deleta R_Fabrica_DPK "
        If Not DEL_R_FABRICA_DPK Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'DELETA_APLICACAO - 3
        Label1.Caption = "Tabela em Atualiza��o: Deleta Aplica��o "
        If Not DEL_APLICACAO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DELETA_CLIENTE - 4
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Cliente "
        If Not DEL_CLIENTE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DELETA_FORNECEDOR_ESPECIFICO - 5
        Label1.Caption = "Tabela em Atualiza��o: Dele��o Fornecedor_Especifico "
        If Not DEL_FORNECEDOR_ESPECIFICO Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DELETA_REPRESENTANTE - 6
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Representante "
        If Not DEL_REPR Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DELETA_R_CLIE_REPRES - 7
        Label1.Caption = "Tabela em Atualiza��o: Dele��o da R_CLIE_REPRES "
        If Not DEL_R_CLIE_REPRES Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DELETA_TABELA_DESCPER - 8
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de Tabela_Descper "
        If Not DEL_TABDESC Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'DELETA_PEDNOTA_VENDA - 9
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de PEDNOTA_VENDA "
        If Not DEL_PEDNOTA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'DELETA_R_UF_DEPOSITO - 10
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de R_UF_DEPOSITO "
        If Not DEL_R_UF_DEPOSITO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'DELETA_R_DPK_EQUIV - 11
        Label1.Caption = "Tabela em Atualiza��o: Dele��o de R_DPK_EQUIV "
        If Not DEL_R_DPK_EQUIV Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'CLASS_FISCAL_RED_BASE - 11
        Label1.Caption = "Tabela em Atualiza��o: DEL_CLASS_FISCAL_RED_BASE"
        If Not CLASS_FISCAL_RED_BASE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'ALIQUOTA_ME - 12
        Label1.Caption = "Tabela em Atualiza��o: Al�quota "
        If Not ATU_ALIQUOTA_ME Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'ANTECIPACAO_TRIBUTARIA - 13
        Label1.Caption = "Tabela em Atualiza��o: Antecipa��o Tribut�ria "
        If Not ATU_ANTECIPACAO_TRIBUTARIA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'APLICACAO - 14
        Label1.Caption = "Tabela em Atualiza��o: Aplica��o "
        If Not ATU_APLICACAO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'BANCO - 15
        Label1.Caption = "Tabela em Atualiza��o: Banco "
        If Not ATU_BANCO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'CANCEL_PEDNOTA - 16
        Label1.Caption = "Tabela em Atualiza��o: Cancel_Pednota "
        If Not ATU_CANCEL_PEDNOTA Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
     
       'CATEG_SINAL - 17
        Label1.Caption = "Tabela em Atualiza��o: Categ_Sinal "
        If Not ATU_CATEGSINAL Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CESTA_ITEM - 18
        Label1.Caption = "Tabela em Atualiza��o: CESTA_ITEM "
        If Not ATU_CESTAITEM Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'CESTA_VENDA - 19
        Label1.Caption = "Tabela em Atualiza��o: CESTA_VENDA "
        If Not ATU_CESTAVENDA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'CIDADE - 20
        Label1.Caption = "Tabela em Atualiza��o: Cidade "
        If Not ATU_CIDADE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLASS_ANTEC_ENTRADA - 21
        Label1.Caption = "Tabela em Atualiza��o: Class_Antec_Entrada"
        If Not ATU_CLASS_ANTEC_ENTRADA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIE_CREDITO - 22
        Label1.Caption = "Tabela em Atualiza��o: Clie_Credito"
        If Not ATU_CLIECRED Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIE_ENDERECO - 23
        Label1.Caption = "Tabela em Atualiza��o: Clie_Endereco "
        If Not ATU_CLIEEND Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIE_INTERNET - 24
        Label1.Caption = "Tabela em Atualiza��o: Clie_Internet "
        If Not ATU_CLIEINT Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIE_MENSAGEM - 25
        Label1.Caption = "Tabela em Atualiza��o: Clie_Mensagem "
        If Not ATU_CLIEMSG Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIENTE - 26
        Label1.Caption = "Tabela em Atualiza��o: Cliente "
        If Not ATU_CLIENTE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'R_CLIENTE_QUADRANTE
        Label1.Caption = "Tabela em Atualiza��o: R_Cliente_Quadrante "
        If Not ATU_R_CLIENTE_QUADRANTE Then
          GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'R_ACAO_QUADRANTE
        Label1.Caption = "Tabela em Atualiza��o: R_Acao_Quadrante "
        If Not ATU_R_ACAO_QUADRANTE Then
          GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'DICAS - 26
        Label1.Caption = "Tabela em Atualiza��o: Dicas "
        If Not ATU_DICAS Then
          GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'STATUS_VISITA
        Label1.Caption = "Tabela em Atualiza��o: Status_Visita "
        If Not ATU_STATUS_VISITA Then
          GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        'VISITA_PROMOTOR
        Label1.Caption = "Tabela em Atualiza��o: Visita_Promotor "
        If Not ATU_VISITA_PROMOTOR Then
          GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        
       'CLIENTE_ACUMULADO - 27
        Label1.Caption = "Tabela em Atualiza��o: Cliente Acumulado "
        If Not ATU_CLIENTE_ACUMULADO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'CLIENTE_CARACTERISTICA - 28
        Label1.Caption = "Tabela em Atualiza��o: CLIENTE_CARACTERISTICA "
        If Not ATU_CLIENTE_CARACTERISTICA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DEPOSITO_VISAO - 29
        Label1.Caption = "Tabela em Atualiza��o: Deposito "
        If Not ATU_DEPOSITO_VISAO Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents

       'DEPOSITO - 29.1
        Label1.Caption = "Tabela em Atualiza��o: Deposito "
        If Not ATU_DEPOSITO Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'DOLAR_DIARIO - 30
        Label1.Caption = "Tabela em Atualiza��o: Dolar_Diario "
        If Not ATU_DOLARDIARIO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'DUPLICATA - 31
        Label1.Caption = "Tabela em Atualiza��o: Duplicatas "
        If Not ATU_DUPLICATA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'EMBALADOR - 32
        Label1.Caption = "Tabela em Atualiza��o: Embalador "
        If Not ATU_EMBALADOR Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ESTATISTICA_ITEM - 33
       'Label1.Caption = "Tabela em Atualiza��o: Estatistica_Item "
       'If Not ATU_ESTAITEM Then
           'GoTo TRATA_ERRO
       'End If
       'dblAtual = dblAtual + 1
       'PrgBar1.Value = dblAtual
       'lblPercent.Caption = Format(dblAtual * 100 / PrgBar1.Max, "##0") & "%"
       'DoEvents
        
       'FILIAL - 34
        Label1.Caption = "Tabela em Atualiza��o: Filial "
        If Not ATU_FILIAL Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'FILIAL_FRANQUIA - 35
        Label1.Caption = "Tabela em Atualiza��o: Filial_Franquia "
        If Not ATU_FILIAL_FRANQUIA Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'FORNECEDOR - 36
        Label1.Caption = "Tabela em Atualiza��o: Fornecedor "
        mdiGER040.Label1.Refresh
        If Not ATU_FORNECEDOR Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'FORNECEDOR_ESPECIFICO - 37
        Label1.Caption = "Tabela em Atualiza��o: Fornecedor_Especifico "
        If Not ATU_FORNECEDOR_ESPECIFICO Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'FRETE_ENTREGA - 38
        Label1.Caption = "Tabela em Atualiza��o: Frete_Entrega "
        If Not ATU_FRETE_ENTREGA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'FRETE_UF - 39
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF "
        If Not ATU_FRETE_UF Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'FRETE_UF_BLOQ - 40
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF_BLOQ "
        If Not ATU_FRETE_UF_BLOQ Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'FRETE_UF_TRANSP - 41
        Label1.Caption = "Tabela em Atualiza��o: FRETE_UF_TRANSP "
        If Not ATU_FRETE_UF_TRANSP Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'GRUPO - 42
        Label1.Caption = "Tabela em Atualiza��o: Grupo "
        mdiGER040.Label1.Refresh
        If Not ATU_GRUPO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITPEDNOTA_VENDA - 43
        Label1.Caption = "Tabela em Atualiza��o: ITPEDNOTA_VENDA "
        If Not ATU_ITPEDNOTA_VENDA Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
          
       'ITEM_ANALITICO - 44
        Label1.Caption = "Tabela em Atualiza��o: Item_Analitico "
        If Not ATU_ITEMANAL Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITEM_CADASTRO - 45
        Label1.Caption = "Tabela em Atualiza��o: Item_Cadastro "
        If Not ATU_ITEMCAD Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITEM_CUSTO - 46
        Label1.Caption = "Tabela em Atualiza��o: Item_Custo "
        If Not ATU_ITEMCUSTO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITEM_ESTOQUE - 47
        Label1.Caption = "Tabela em Atualiza��o: Item_Estoque "
        If Not ATU_ITEMESTOQUE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITEM_GLOBAL - 48
        Label1.Caption = "Tabela em Atualiza��o: Item_Global "
        If Not ATU_ITEMGLOBAL Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'ITEM_PRECO - 49
        Label1.Caption = "Tabela em Atualiza��o: Item_Pre�o "
        If Not ATU_ITEMPRECO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'LOJA - 50
        Label1.Caption = "Tabela em Atualiza��o: Loja "
        If Not ATU_LOJA Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'MONTADORA - 51
        Label1.Caption = "Tabela em Atualiza��o: Item_Montadora "
        If Not ATU_MONTADORA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'NATUREZA_OPERACAO - 52
        Label1.Caption = "Tabela em Atualiza��o: Natureza_Opera��o "
        If Not ATU_NAT_OPERACAO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'PEDNOTA_VENDA - 53
        Label1.Caption = "Tabela em Atualiza��o: PEDNOTA_VENDA "
        If Not ATU_PEDNOTA_VENDA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'PLANO_PGTO - 54
        Label1.Caption = "Tabela em Atualiza��o: Plano_Pgto "
        If Not ATU_PLANO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'R_CLIE_REPRES - 55
        Label1.Caption = "Tabela em Atualiza��o: R_CLIE_REPRES "
        If Not ATU_R_CLIE_REPRES Then
             GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'R_DPK_EQUIV - 56
        Label1.Caption = "Tabela em Atualiza��o: R_DPK_EQUIV "
        If Not ATU_R_DPK_EQUIV Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'R_FILDEP - 57
        Label1.Caption = "Tabela em Atualiza��o: R_FilDep "
        If Not ATU_R_FILDEP Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'R_PEDIDO_CONF - 58
        Label1.Caption = "Tabela em Atualiza��o: R_pedido_conf "
        If Not ATU_R_PEDIDO_CONF Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'R_REPCGC - 59
        Label1.Caption = "Tabela em Atualiza��o: RepCgc "
        If Not ATU_REPCGC Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'R_REPVEN - 60
        Label1.Caption = "Tabela em Atualiza��o: RepVen "
        If Not ATU_REPVEN Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'R_UF_DEPOSITO - 61
        Label1.Caption = "Tabela em Atualiza��o: R_UF_DEPOSITO "
        If Not ATU_R_UF_DEPOSITO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'REPR_END_CORRESP - 62
        Label1.Caption = "Tabela em Atualiza��o: Repr_End_Corresp "
        If Not ATU_REPEND Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'REPRESENTACAO - 63
        Label1.Caption = "Tabela em Atualiza��o: Representa��o "
        If Not ATU_REPRESENTACAO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'REPRESENTANTE - 64
        Label1.Caption = "Tabela em Atualiza��o: Representante "
        If Not ATU_REPRESENTANTE Then
             GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'RESULTADO - 65
        Label1.Caption = "Tabela em Atualiza��o: Resultado "
        If Not ATU_RESULTADO Then
             GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'ROMANEIO - 66
        Label1.Caption = "Tabela em Atualiza��o: Romaneio "
        If Not ATU_ROMANEIO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'SALDO_PEDIDOS - 67
        Label1.Caption = "Tabela em Atualiza��o: Saldo_Pedidos "
        If Not ATU_SALDO_PEDIDO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'SUBGRUPO - 68
        Label1.Caption = "Tabela em Atualiza��o: Subgrupo "
        If Not ATU_SUBGRUPO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'SUBST_TRIBUTARIA - 69
        Label1.Caption = "Tabela em Atualiza��o: Substitui��o Tributaria "
        If Not ATU_SUBTRIB Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'TABELA_DESCPER - 70
        Label1.Caption = "Tabela em Atualiza��o: Tabela_Descper "
        If Not ATU_TABDESCPER Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'TABELA_VENDA - 71
        Label1.Caption = "Tabela em Atualiza��o: Tabela_Venda "
        If Not ATU_TABVENDA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'TAXA - 72
       'Label1.Caption = "Tabela em Atualiza��o: Taxa "
       'If Not ATU_TAXA Then
           'GoTo TRATA_ERRO
       'End If
       'dblAtual = dblAtual + 1
       'PrgBar1.Value = dblAtual
       'lblPercent.Caption = Format(dblAtual * 100 / PrgBar1.Max, "##0") & "%"
       'DoEvents
        
       'TP_CLIENTE - 73
        Label1.Caption = "Tabela em Atualiza��o: Tipo Cliente "
        If Not ATU_TPCLIE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'TIPO_CLIENTE_BLAU - 74
        Label1.Caption = "Tabela em Atualiza��o: Tipo_Cliente_Blau "
        If Not ATU_TIPO_CLIENTE_BLAU Then
           GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'TRANSPORTADORA - 75
        Label1.Caption = "Tabela em Atualiza��o: Transportadora "
        If Not ATU_TRANSPORTADORA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'UF - 76
        Label1.Caption = "Tabela em Atualiza��o: Uf "
        If Not ATU_UF Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'UF_CATEG - 77
        Label1.Caption = "Tabela em Atualiza��o: Uf_CATEG "
        If Not ATU_UF_CATEG Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'UF_DEPOSITO - 78
        Label1.Caption = "Tabela em Atualiza��o: Uf_Deposito "
        If Not ATU_UF_DEPOSITO Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
       'UF_DPK - 79
        Label1.Caption = "Tabela em Atualiza��o: Uf_DPK "
        If Not ATU_UFDPK Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
       
       'UF_ORIGEM_DESTINO - 80
        Label1.Caption = "Tabela em Atualiza��o: Uf_Origem_Destino "
        If Not ATU_UF_ORIGDEST Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'UF_TPCLIENTE - 81
        Label1.Caption = "Tabela em Atualiza��o: UF_TPCLIENTE "
        If Not ATU_UF_TPCLIENTE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
            
       'V_CLIENTE_FIEL - 82
        Label1.Caption = "Tabela em Atualiza��o: V_Cliente_Fiel "
        If Not ATU_V_CLIENTE_FIEL Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'V_PEDLIQ_VENDA - 83
        Label1.Caption = "Tabela em Atualiza��o: V_Pedliq_venda "
        If Not ATU_V_PEDLIQ_VENDA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
       'VENDA_LIMITADA - 84
        Label1.Caption = "Tabela em Atualiza��o: VENDA_LIMITADA "
        If Not ATU_VENDA_LIMITADA Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
    
    
        If FL_VDR = "S" Then
           'VDR_CONTROLE_VDR - 85
            Label1.Caption = "Tabela em Atualiza��o: VDR_CONTROLE_VDR "
            If Not ATU_VDR_CONTROLE_VDR Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
           'VDR_ITEM_PRECO - 86
            Label1.Caption = "Tabela em Atualiza��o: VDR_ITEM_PRECO "
            If Not ATU_VDR_ITEM_PRECO Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
           'DELETA_VDR_R_CLIE_LINHA_PRODUTO - 87
            Label1.Caption = "Dele��o da tabela: VDR_R_CLIE_LINHA_PRODUTO "
            If Not DEL_VDR_R_CLIE_LINHA_PRODUTO Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
           
           'VDR_R_CLIE_LINHA_PRODUTO - 88
            Label1.Caption = "Tabela em Atualiza��o: VDR_R_CLIE_LINHA_PRODUTO "
            If Not ATU_VDR_R_CLIE_LINHA_PRODUTO Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
           'DELETA_VDR_CLIENTE_CATEG_VDR - 89
            Label1.Caption = "Dele��o da tabela: VDR_CLIENTE_CATEG_VDR "
            If Not DEL_VDR_CLIENTE_CATEG_VDR Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
           
           'VDR_CLIENTE_CATEG_VDR - 90
            Label1.Caption = "Tabela em Atualiza��o: VDR_CLIENTE_CATEG_VDR "
            If Not ATU_VDR_CLIENTE_CATEG_VDR Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
           'VDR_DESCONTO_CATEG - 91
            Label1.Caption = "Tabela em Atualiza��o: VDR_DESCONTO_CATEG "
            If Not ATU_VDR_DESCONTO_CATEG Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
           'VDR_TABELA_VENDA - 92
            Label1.Caption = "Tabela em Atualiza��o: VDR_TABELA_VENDA "
            If Not ATU_VDR_TABELA_VENDA Then
                GoTo Trata_Erro
            End If
            dblAtual = dblAtual + 1
            prgbar1.Value = dblAtual
            lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
            DoEvents
        
        End If
        
        '--------------
        'Eduardo Relvas
        '18/01/2005
        '--------------
        'R_FABRICA_DPK - 92
        Label1.Caption = "Tabela em Atualiza��o: R_FABRICA_DPK"
        If Not R_FABRICA_DPK Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        '--------------
        'Eduardo Relvas
        '26/07/05
        '--------------
        'CLASS_FISCAL_RED_BASE - 93
        Label1.Caption = "Tabela em Atualiza��o: CLASS_FISCAL_RED_BASE"
        If Not CLASS_FISCAL_RED_BASE Then
            GoTo Trata_Erro
        End If
        dblAtual = dblAtual + 1
        prgbar1.Value = dblAtual
        lblPercent.Caption = Format(dblAtual * 100 / prgbar1.Max, "##0") & "%"
        DoEvents
        
        Label1.Visible = False
        prgbar1.Visible = False
        lblPercent.Visible = False
        Label2.Visible = True
        Label2.Caption = "AGUARDE - Otimizando Base de Dados !!!!!"
        DoEvents
        
        contArq = contArq + 1
       
        If contArq <= numArq Then
            dbExt.Close
            mdiGER040.File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
            mdiGER040.File1.Pattern = "*.mdb"
            nomeArq = mdiGER040.File1.List(contArq - 1)
            On Error GoTo TRATA_ERRO_AUXILIAR
            Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
        End If
       
    Loop
    
    If Trim(Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB")) <> "" Then
        dbExt.Close
        Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\*.MDB"
    End If
    
    Screen.MousePointer = 1
       
    fim = Time
    
    strSQL = "INSERT INTO LOGDIV " & _
            " ( DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL ) VALUES " & _
            " ( '" & Now & "' , 1, 'Retorno da Atualiza��o', '" & _
            "Rotina de Atualiza��o Processada Ok inicio: " & inicio & " e fim: " & fim & "', '" & _
            "Dep. Opera��o (CPD) ' ) "
    dbControle.Execute strSQL, dbFailOnError
    
    Screen.MousePointer = 1

Exit Sub

Trata_Erro:

    If BOERRO Then
        SSPanel2.Caption = "Problemas na Atualiza��o !!! "
        Screen.MousePointer = 1
    
        Call FUNCAO_ERRO("")
    End If
    
    RESP = MsgBox("Continua a Atualiza��o ?", vbYesNo)
    If RESP = vbYes Then
        Resume Next
    End If

Exit Sub


TRATA_ERRO_AUXILIAR:
    
    If Err = 3049 Then
    
        MousePointer = 11
        DoEvents
    
       'Reparar Banco de Dados
        RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
        
        COMPACTA_AUXILIAR
        
        MousePointer = 0
       'Ap�s corrigir o banco danificado processa novamente a linha do programa
       'que gerou o erro.
        Resume 0
    Else
    
        Call FUNCAO_ERRO("")
        MsgBox "Sub Comeca_Atual" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub GERA_E_MAIL()
         'GERA E-MAL PARA CONTROLE DE ATUALIZA��O EM CAMPINAS E GRAVA
         'SEQUENCIA DE ATUALIZA��O NA TABELA FILIAL_FRANQUIA
        
          Dim dir_saida As String
          Dim SQL As String
1         On Error GoTo TrataBanco
2         If controle_atual > 0 Then
             'Verifica se arquivo zipado j� foi gerado
3             dir_saida = Path_drv & "\COM\SAIDA\MSG\ATU" & filial_atual & controle_atual & ".MSG"
4             Open dir_saida For Output As #1
             'Grava linha da data
5             Print #1, "Date: " & Format(Now, "mmm, dd yyyy")
             'Grava linha do endereco
6             Print #1, "To: " & "dpkpedid@dpk.com.br"
             'Grava linha do Subject
7             Print #1, "Subject: #ATU" & filial_atual & controle_atual
             'Fecha arquivo texto
8             Print #1, filial_atual & controle_atual
9             Close #1
        
10            dir_saida = Path_drv & "\COM\SAIDA\PEDIDOS\ATU" & filial_atual & controle_atual & ".ATU"
11            Open dir_saida For Output As #1
             'Grava linha da data
12            Print #1, filial_atual & controle_atual
13            Close #1

             'GRAVA ATUALIZA��O
14                  SQL = "UPDATE FILIAL_FRANQUIA SET SEQ_BATCH= " & icontrole_atual & " "
15            SQL = SQL & " WHERE COD_FILIAL= " & ifilial_atual
16            dbLocal.Execute SQL
17        End If

18    Exit Sub

TrataBanco:
19        If Err = 3024 Then
20            MsgBox "ARQUIVO" & Path_drv & "\COM\DBMAIL.MDB N�O ENCONTRADO!!!", 48, "    ARQUIVO N�O ENCONTRADO"
21        ElseIf Err <> 0 Then
22            MsgBox "Sub Gera_E_Mail" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If
          
End Sub

Private Sub OTIMIZAR_BASES()
          Dim SQL As String
          Dim SQL2 As String
          Dim sql3 As String
          Dim ss As Dynaset
          Dim ss3 As Dynaset
          Dim i As Long
          Dim TOT_ITEM As Integer
          Dim dbaccess
          Dim dbaccess1
          Dim dbaccess2
          Dim dbaccess3
          Dim DT_HOJE
          Dim loja
          Dim cotacao

1         Set dbaccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")

2         On Error GoTo Trata_Erro
3         Screen.MousePointer = 11

         '************************************ DIGITA.MDB ************************************

         '*****************************************************
         'DELETA DE DIGITA.MDB PEDIDOS QUE J� FORAM FINALIZADOS
         '*****************************************************
4         SSPanel2.Caption = "Otimizando VENDAS!"

5               SQL = "SELECT NUM_PENDENTE"
6         SQL = SQL & " FROM PEDIDO"
7         SQL = SQL & " WHERE FL_PEDIDO_FINALIZADO <> 'N'  "
8         Set ss = dbaccess.CreateDynaset(SQL)
        
9         If ss.EOF And ss.BOF Then
10            Screen.MousePointer = 0
11        Else
12            ss.MoveLast
13            TOT_ITEM = ss.RecordCount
14            ss.MoveFirst
15            For i = 1 To TOT_ITEM
16                SQL2 = "delete from pedido where"
17                SQL2 = SQL2 & " num_pendente = " & ss("num_pendente")
18                sql3 = "delete from item_pedido where"
19                sql3 = sql3 & " num_pendente = " & ss("num_pendente")
20                dbaccess.Execute SQL2
21                dbaccess.Execute sql3
22                ss.MoveNext
23            Next
24        End If
             
25        dbaccess.Close
26        Screen.MousePointer = 11
         
         '**************************
         'OTIMIZA /REPARA DIGITA.MDB
         '**************************
27        SSPanel2.Caption = "Otimizando DIGITA!"

28        If Dir(Path_drv & "\DADOS\DIGITA.BAK") <> "" Then
29            Kill Path_drv & "\DADOS\DIGITA.BAK"
30        End If
         
31        FileCopy Path_drv & "\DADOS\DIGITA.MDB", Path_drv & "\DADOS\DIGITA.BAK"
32        DBEngine.CompactDatabase Path_drv & "\DADOS\DIGITA.MDB", Path_drv & "\DADOS\DIGNEW.MDB", "", dbEncrypt
33        Kill Path_drv & "\DADOS\DIGITA.MDB"
34        Name Path_drv & "\DADOS\DIGNEW.MDB" As Path_drv & "\DADOS\DIGITA.MDB"
35        DBEngine.RepairDatabase Path_drv & "\DADOS\DIGITA.MDB"
         
         '************************************ VENDAS.MDB ************************************
         
         '****************************
         'VERIFICA SEQUENCE DA COTA��O
         '****************************
36        SSPanel2.Caption = "Otimizando VENDAS!"
          
37        Set dbaccess = OpenDatabase(Path_drv & "\DADOS\" & "VENDAS.MDB")
          
38              SQL = "Select proximo_pedido"
39        SQL = SQL & " From sequencia"
          
40        Set ss = dbaccess.CreateSnapshot(SQL)
41        FreeLocks
         
42        Set dbaccess3 = OpenDatabase(Path_drv & "\COM\" & "DBMAIL.MDB")
          
43              SQL = "Select TIPO_USUARIO"
44        SQL = SQL & " From USUARIO"
          
45        Set ss3 = dbaccess3.CreateSnapshot(SQL)
46        FreeLocks
          
		  'Login - RLIMA (Ci&T) Data - 15/06/2012
		  'Jira - RTI-136
		  'Descri��o - Adicionando condi��o de tipo de usu�rio igual a Gerente.
47        If ss3!TIPO_USUARIO = "R" Or ss3!TIPO_USUARIO = "G" Then
             
48            If ss!proximo_pedido >= 995 Then
49                SQL = "update sequencia set proximo_pedido=0"
50                dbaccess.Execute SQL
51                FreeLocks
52            End If
              
53        Else
          
54            If ss!proximo_pedido >= 9995 Then
55                SQL = "update sequencia set proximo_pedido=0"
56                dbaccess.Execute SQL
57                FreeLocks
58            End If
          
59        End If
          
60        dbaccess.Close
         
         '***************************
         'OTIMIZA / REPARA VENDAS.MDB
         '***************************

61        If Dir(Path_drv & "\DADOS\VENDAS.BAK") <> "" Then
62            Kill Path_drv & "\DADOS\VENDAS.BAK"
63        End If
          
64        FileCopy Path_drv & "\DADOS\VENDAS.MDB", Path_drv & "\DADOS\VENDAS.BAK"
65        DBEngine.CompactDatabase Path_drv & "\DADOS\VENDAS.MDB", Path_drv & "\DADOS\VDANEW.MDB", "", dbEncrypt
66        Kill Path_drv & "\DADOS\VENDAS.MDB"
67        Name Path_drv & "\DADOS\VDANEW.MDB" As Path_drv & "\DADOS\VENDAS.MDB"
68        DBEngine.RepairDatabase Path_drv & "\DADOS\VENDAS.MDB"
          
69        Screen.MousePointer = 11
          
         '************************************ BASE_DPK.MDB ************************************
          
         '********************************************************************
         'FAZ DELE��O NA TABELA PEDNOTA_VENDA DO BASE_DPK.MDB
         'DELETA OS PEDIDOS PARA GOIANIA COM RETORNO DO DIA DE ENVIO DO PEDIDO
         '********************************************************************
70        SSPanel2.Caption = "Otimizando BASE_DPK!"
             
71        Set dbaccess2 = OpenDatabase(Path_drv & "\DADOS\" & "BASE_DPK.MDB", False, False)
                
72              SQL = "DELETE  "
73        SQL = SQL & " FROM PEDNOTA_VENDA"
74        SQL = SQL & " WHERE cod_cliente IS NULL  "
             
75        dbaccess2.Execute SQL
            
76        SSPanel2.Caption = "Deletando ITENS COTA��ES Antigas!"
          
77              SQL = "SELECT DT_FATURAMENTO "
78        SQL = SQL & " FROM DATAS"
          
79        Set ss = dbaccess2.CreateDynaset(SQL)
          
80        DT_HOJE = ss!DT_FATURAMENTO
                
81              SQL = "SELECT COD_LOJA, NUM_COTACAO"
82        SQL = SQL & " FROM COTACAO "
83        SQL = SQL & " WHERE DT_VALIDADE < cdate('" & CDate(DT_HOJE) & "')"
         
84        Set ss = dbaccess2.CreateDynaset(SQL)
         
85        Do While Not ss.EOF
86            loja = ss!COD_LOJA
87            cotacao = ss!NUM_COTACAO
              
88                  SQL = " DELETE "
89            SQL = SQL & " FROM ITEM_COTACAO "
90            SQL = SQL & " WHERE COD_LOJA = " & loja & " AND "
91            SQL = SQL & " NUM_COTACAO = " & cotacao & " "
              
92            dbaccess2.Execute SQL
93            ss.MoveNext
94        Loop
             
95        SSPanel2.Caption = "Deletando COTA��ES Antigas!"
            
            
96              SQL = "DELETE "
97        SQL = SQL & " FROM COTACAO "
98        SQL = SQL & " WHERE dt_validade < cdate('" & CDate(DT_HOJE) & "')"
          
99        dbaccess2.Execute SQL
            
100       dbaccess2.Close
            
         '****************************
         'OTIMIZA /REPARA BASE_DPK.MDB
         '****************************
         'If Dir(Path_drv & "\DADOS\BASE_DPK.BAK") <> "" Then
             'Kill Path_drv & "\DADOS\DIGITA.BAK"
         'End If
              
         'FileCopy Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASE_DPK.BAK"
         'DBEngine.CompactDatabase Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASENEW.MDB", "", dbEncrypt
         'Kill Path_drv & "\DADOS\BASE_DPK.MDB"
         'Name Path_drv & "\DADOS\BASENEW.MDB" As Path_drv & "\DADOS\BASE_DPK.MDB"
         'DBEngine.RepairDatabase Path_drv & "\DADOS\BASE_DPK.MDB"
            
         
         '************************************ VDR.MDB ************************************

101       Set dbaccess2 = OpenDatabase(Path_drv & "\DADOS\" & "VDR.MDB")

         '*****************************************************
         'DELETA DA VDR.MDB PEDIDOS QUE J� FORAM FINALIZADOS
         '*****************************************************
102       SSPanel2.Caption = "Otimizando VDR!"

103             SQL = "SELECT NUM_PENDENTE"
104       SQL = SQL & " FROM PEDIDO"
105       SQL = SQL & " WHERE FL_PEDIDO_FINALIZADO <> 'N'  "
106       Set ss = dbaccess2.CreateDynaset(SQL)
        
107       If ss.EOF And ss.BOF Then
108           Screen.MousePointer = 0
109       Else
110           ss.MoveLast
111           TOT_ITEM = ss.RecordCount
112           ss.MoveFirst
113           For i = 1 To TOT_ITEM
114               SQL2 = "delete from pedido where"
115               SQL2 = SQL2 & " num_pendente = " & ss!num_pendente
116               sql3 = "delete from item_pedido where"
117               sql3 = sql3 & " num_pendente = " & ss!num_pendente
118               dbaccess2.Execute SQL2
119               dbaccess2.Execute sql3
120               ss.MoveNext
121           Next
122       End If
123       dbaccess2.Close
          
         '**************************
         'OTIMIZA/REPARA VDR.MDB
         '**************************
124       SSPanel2.Caption = "Otimizando VDR!"

125       If Dir(Path_drv & "\DADOS\VDR.BAK") <> "" Then
126           Kill Path_drv & "\DADOS\VDR.BAK"
127       End If
         
128       FileCopy Path_drv & "\DADOS\VDR.MDB", Path_drv & "\DADOS\VDR.BAK"
129       DBEngine.CompactDatabase Path_drv & "\DADOS\VDR.MDB", Path_drv & "\DADOS\VDRNEW.MDB", "", dbEncrypt
130       Kill Path_drv & "\DADOS\VDR.MDB"
131       Name Path_drv & "\DADOS\VDRNEW.MDB" As Path_drv & "\DADOS\VDR.MDB"
132       DBEngine.RepairDatabase Path_drv & "\DADOS\VDR.MDB"

133       Screen.MousePointer = 0


134   Exit Sub
          
Trata_Erro:
135       If Err = 70 Then
136           MsgBox "O Banco de Vendas/Digita��o/VDR est� sendo utilizado, verifique se h� algum micro ligado no Telemarketing.", vbExclamation, "Aten��o"
137           Screen.MousePointer = 0
138           Unload Me
139           Exit Sub
          ElseIf Err.Number <> 0 Then
              MsgBox "Sub Otimizar_Bases" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
140       End If

End Sub

Private Sub MDIForm_Load()

          Dim strSQL As String
          Dim strOrigem As String
          Dim strDestino As String
          Dim vdata As String
          Dim vdata1 As Date
        
1         vdata = CDate(Format(Now, "DD/MM/YYYY"))
          
2         If App.PrevInstance Then End
              
3         Me.Caption = Me.Caption & " Vers�o: " & App.Major & "." & App.Minor & "." & App.Revision
         
4        Path_drv = Pegar_Drive
        'Path_drv = "c:"

          
5         File1.Path = Path_drv & "\COM\ENTRADA\ARQUIVOS\"
6         File1.Pattern = "*.mdb"
          
7         numArq = File1.ListCount
          'numArq = 1
8         contArq = 0
          
9         mHoje = Date

      '########################Verifica os indices
10             mdiGER040.Show
11            On Error GoTo TRATA_ERRO_LOCAL
12            Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
13              strSQL = "Select * From PARAMETROS WHERE NOME_SOFTWARE  = 'FIL040' "
14              Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
15              If recTab.EOF = False Then
16                  If CDate(recTab!VL_PARAMETRO) <> CDate(vdata) Then
17                      dbLocal.Execute ("UPDATE PARAMETROS SET VL_PARAMETRO = '" & vdata & "' WHERE NOME_SOFTWARE = 'FIL040' AND NOME_PARAMETRO = 'INDICES'")
      '18                      Call geraIndices
18                      Verificar_PKs
19                  End If
20              Else
21                  dbLocal.Execute ("INSERT INTO PARAMETROS (NOME_SOFTWARE, NOME_PARAMETRO, VL_PARAMETRO) VALUES('FIL040', 'INDICES', '" & vdata & "')")
      '23                  Call geraIndices
22                  Verificar_PKs
23              End If
24          dbLocal.Close
25          Set dbLocal = Nothing
            
26        If numArq > 0 Then
27            scmdAtualiza.Enabled = True
28            strOrigem = Path_drv & "\DADOS\BASE_DPK.MDB"
29            strDestino = Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
             'FileCopy strOrigem, strDestino
             
30            On Error GoTo TRATA_ERRO_CTL
31            Set dbControle = OpenDatabase(Path_drv & "\LOG\CONTROLE.MDB", False, False)
          
32            On Error GoTo TRATA_ERRO_LOCAL
33            Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
34            contArq = contArq + 1
35            nomeArq = File1.List(contArq - 1)
              
36            On Error GoTo TRATA_ERRO_AUXILIAR
              'MsgBox Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq
37            Set dbExt = OpenDatabase(Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, False, False)
              
38            FL_TIPO = "D"
39            If Mid(nomeArq, 1, 1) <> "B" Then
40                FL_TIPO = "C"
41            End If
              
42            strSQL = "SELECT DT_FATURAMENTO FROM DATAS where dt_faturamento  "
          
43            Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
          
44            If Not recTab.EOF Then
45                mHoje = Mid(recTab!DT_FATURAMENTO, 1, 8)
46            Else
47                mHoje = Date
48            End If
              
49        End If
          
50        If numArq > 0 Then

51            SSPanel2.Caption = "Aguarde Atualizando Base Local ..."
52            SSPanel5.Visible = True
53            lblPercent.Visible = True
54            prgbar1.Visible = True
              
55            Call COMECA_ATUAL
              
56            Screen.MousePointer = 11
57            SSPanel2.Caption = "Atualiza��o Finalizada!"
58            Screen.MousePointer = 0
59            Label1.Caption = ""
60            dbLocal.Close
61            dbControle.Close
             
             'OTIMIZA��O DE BASE DE DADOS
62            Screen.MousePointer = 1
63            SSPanel2.Caption = "Inicio OTIMIZA��O!"
64            SSPanel5.Visible = False
65            lblPercent.Visible = False
66            prgbar1.Visible = False
67            Label2.Visible = True
68            Label2.Caption = "AGUARDE - Otimizando Base de Dados !!!!!"
              
69            Call OTIMIZAR_BASES
              
70            SSPanel2.Caption = "T�rmino OTIMIZA��O!"
71        End If
          
          '****** ATUALIZA OS RETORNOS DAS DICAS
          Dim vNomeArquivo As String
          
         '****** ATUALIZA POSI��O DE PEDIDOS
         '****** VOLTA FLAG DE PEDIDOS QUANDO O MESMO D� ERRO DE FINALIZA��O
72        While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> "" Or _
                Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
73              While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> ""
74                    mdiGER040.Show
75                    Call ATU_PEDIDOS
76              Wend
77              While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
78                    mdiGER040.Show
79                    vNomeArquivo = Mid(Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT"), 1, 8)
80                    If UCase(vNomeArquivo) = "PEDDICAS" Then
81                        Call ATU_DICAS_RETORNO(Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT"))
82                    End If
83                    Call ATU_FLAG
84              Wend
85        Wend
86        End

87    Exit Sub

TRATA_ERRO_CTL:
88        If Err = 3049 Then
89            MousePointer = 11
90            DoEvents
             'Reparar Banco de Dados
91            RepairDatabase (Path_drv & "\LOG\CONTROLE.MDB")
92            COMPACTA_CONTROLE
93            MousePointer = 0
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
94            Resume 0
95        Else
96            Call FUNCAO_ERRO("")
97            MsgBox "Sub Mdiform_Load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
98            Resume Next
99        End If
100       Exit Sub

TRATA_ERRO_LOCAL:
101       If Err = 3049 Then
102           MousePointer = 11
103           DoEvents
             'Reparar Banco de Dados
104           RepairDatabase (Path_drv & "\DADOS\BASE_DPK.MDB")
105           COMPACTA_LOCAL
106           MousePointer = 0
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
107           Resume 0
108       Else
109           Call FUNCAO_ERRO("")
110           MsgBox "Sub Mdiform_load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
111       End If

TRATA_ERRO_AUXILIAR:
112       If Err = 3049 Then
Resume
113           MousePointer = 11
114           DoEvents
             'Reparar Banco de Dados
115           RepairDatabase (Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq)
116           COMPACTA_AUXILIAR
117           MousePointer = 0
             'Ap�s corrigir o banco danificado processa novamente a linha do programa
             'que gerou o erro.
118           Resume 0
119       Else
120           MsgBox "Sub Mdiform_load" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
121       End If
End Sub
Private Sub MDIForm_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = False
End Sub


Private Sub mnuAtu_Click()
    scmdAtualiza_Click
End Sub


Private Sub mnuBackup_Click()
    scmdBackup_Click
End Sub

Private Sub mnuCompacta_Click()
   'scmdCompacta_Click
End Sub

Private Sub mnuComunica_Click()
    scmdComunica_Click
End Sub

Private Sub mnuDiv_Click()
    scmdDiv_Click
End Sub


Private Sub mnuSair_Click()
    scmdSair_Click
End Sub


Private Sub scmdAtualiza_Click()
    lblPercent.Visible = False
    SSPanel5.Visible = False
    frmGER0401.Show vbModal
End Sub

Private Sub scmdAtualiza_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Atualizar"
    lblHelp.Left = 0
    lblHelp.Visible = True
End Sub

Private Sub scmdBackup_Click()
          
    On Error GoTo Trata_Erro
          
          Dim strOrigem As String
          Dim strDestino As String
          
1         lblPercent.Visible = False
2         SSPanel5.Visible = False
          
3         strOrigem = InputBox("Indique o Caminho e o Nome do Arquivo de Origem", "Backup")
          
4         If Trim(strOrigem) <> "" Then
5             If Dir(strOrigem) = "" Then
6                 MsgBox "Caminho ou Nome do Arquivo de Origem n�o existe"
7                 Exit Sub
8             End If
9             strDestino = InputBox("Indique o Caminho e o Nome do Arquivo Destino", "Backup")
10            If Trim(strDestino) <> "" Then
11                If Dir(strDestino) <> "" Then
12                    Kill strDestino
13                End If
                  
14                Name strOrigem As strDestino
15            End If
16        End If

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub scmdBackup_Click" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub scmdBackup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Back-up"
    lblHelp.Left = 3465
    lblHelp.Visible = True
End Sub


Private Sub scmdCompacta_Click()

    On Error GoTo Trata_Erro

          Dim RESP As Integer

1         lblPercent.Visible = False
2         SSPanel5.Visible = False

3         RESP = MsgBox("Deseja realmente Compactar seu Banco de Dados ? ", vbYesNo, "Compacta��o do Banco de Dados")
          
4         If RESP = vbNo Then
5             Exit Sub
6         End If

7         Screen.MousePointer = 11

         'Fecha Banco de Dados
8         dbLocal.Close
          
         'Compacta��o de Banco de Dados
9         COMPACTA_LOCAL
          
         'Abre Banco de Dados Novamente
10        Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
         'Fecha Banco de Dados
11        dbControle.Close
          
         'Compacta��o de Banco de Dados de Controle
12        COMPACTA_CONTROLE
          
         'Abre Banco de Dados Novamente
13        Set dbControle = OpenDatabase(Path_drv & "\COM\CONTROLE.MDB", False, False)
         
14        Screen.MousePointer = 0

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub scmdCompacta" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Private Sub scmdCompacta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Compacta BD"
    lblHelp.Left = 2520
    lblHelp.Visible = True
End Sub


Private Sub scmdComunica_Click()
    lblPercent.Visible = False
    SSPanel5.Visible = False
    frmGER0403.Show vbModal
End Sub

Private Sub scmdComunica_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "LOG's Comunica��o"
    lblHelp.Left = 1680
    lblHelp.Visible = True
End Sub


Private Sub scmdDiv_Click()
    lblPercent.Visible = False
    SSPanel5.Visible = False
    frmGER0402.Show vbModal
End Sub

Private Sub scmdDiv_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "LOG's Diverg�ncias"
    lblHelp.Left = 840
    lblHelp.Visible = True
End Sub



Private Sub scmdSair_Click()
    lblPercent.Visible = False
    SSPanel5.Visible = False
    Unload Me
End Sub



Private Sub scmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Caption = "Sair"
    lblHelp.Left = 4590
    lblHelp.Visible = True
End Sub





Private Sub sspFaixa_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblHelp.Visible = False
End Sub


'Eduardo - 18/08/05
'Esta sub foi criada somente para inserir o campo VL_ICMRETIDO na tabela ITEM_PEDIDO pois nao existe uma forma de
'fazer esta altera��o pelo sistema de atualiza��o.
Sub Criar_Campo_VL_ICMRETIDO()
          
1         On Error GoTo Trata_Erro
          
          Dim Db As DAO.Database
          
2         Set Db = OpenDatabase("C:\DADOS\VENDAS.MDB")
          
3         Db.Execute "ALTER TABLE ITEM_PEDIDO ADD COLUMN VL_ICMRETIDO DOUBLE "

4         Db.Execute "UPDATE ITEM_PEDIDO SET VL_ICMRETIDO = 0 WHERE VL_ICMRETIDO IS NULL"

Trata_Erro:
5         If Err.Number = 3380 Then
6             Resume Next
7         ElseIf Err.Number <> 0 Then
8             MsgBox "Sub Criar_Campo_Vl_IcmRetido" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl
9         End If

End Sub

'************************************************************************************
'A tabela CONTROLE vai ser deletada pelo FIL050IN antes de rodar o FIL260 ou o FIL040
'************************************************************************************
Function ATU_CONTROLE()
          
1         On Error GoTo VerErro
            
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_CONTROLE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CONTROLE "
4         dbExt.Execute strSQL
                  
'5         dbLocal.Execute "DELETE * FROM CONTROLE"
                   
6         strSQL = "INSERT INTO CONTROLE SELECT * FROM AUX_CONTROLE"
7         dbLocal.Execute strSQL
8         vRegs = dbLocal.RecordsAffected
                
9         dbLocal.Execute "DROP TABLE AUX_CONTROLE"
                    
10        ATU_CONTROLE = True

VerErro:
    If Err.Number = 3010 Then
        'Tabela j� existe
        dbExt.Execute "DROP TABLE CONTROLE"
        Resume Next
    'ElseIf Err.Number = 3078 Then
    '    Resume Next
    ElseIf Err.Number <> 0 Then
        MsgBox "Sub ATU_CONTROLE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & "Linha:" & Erl
    End If
End Function

Public Function Pegar_Drive() As String
          Dim fs, d, dc, n
          Dim vArq As Integer
          
1         Pegar_Drive = Left(App.Path, 2)
2         If Pegar_Drive = "\\" Then
3             Set fs = CreateObject("Scripting.FileSystemObject")
4             Set dc = fs.Drives
5             For Each d In dc
6                 If d.DriveType = 3 Then
7                     n = d.ShareName
8                 Else
9                     n = d.Path
10                End If
11                If InStr(1, App.Path, n) > 0 Then
12                    Pegar_Drive = d.DriveLetter & ":"
13                    Exit For
14                End If
15            Next
16        End If

17        If Pegar_Drive = "\\" Then
18            vArq = FreeFile
19            If Dir("C:\DRIVE_PAAC.INI") <> "" Then
20                Open "C:\DRIVE_PAAC.INI" For Input As #vArq
21                Input #vArq, Pegar_Drive
22            Else
23                Pegar_Drive = InputBox("Informe o Drive:" & vbCrLf & "Execmplo: H:", "Aten�ao")
24                Open "C:\DRIVE_PAAC.INI" For Output As #vArq
25                Print #vArq, Pegar_Drive
26            End If
27            Close #vArq
28        End If
End Function

Sub geraIndices()
    Dim TOT_ITEM As Integer
    Dim i As Integer
    Dim vArqLog As Integer
    Dim dbLocalDAO As DAO.Database
    Dim vIndex As DAO.Index
    Dim ii As Integer
    Dim vTableDef As DAO.TableDef
    Dim vtabelas As Integer
1       Set dbLocalDAO = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
    
2       On Error GoTo Trata_Erro
3       For vtabelas = 0 To dbLocalDAO.TableDefs.Count - 1
4           Set vTableDef = dbLocalDAO.TableDefs(vtabelas)
5           If UCase(Left(vTableDef.Name, 4)) <> "MSYS" Then
6               For i = 0 To vTableDef.Indexes.Count - 1
9                   Set vIndex = vTableDef.Indexes(i)
7                   Me.Label1.Caption = "Apagando indice " & vIndex.Name
8                   Me.Label1.Refresh
10                  dbLocal.Execute "DROP INDEX " & vIndex.Name & " on " & vTableDef.Name
11              Next i
12          End If
13      Next vtabelas
14      dbLocalDAO.Close
15      Set dbLocalDAO = Nothing
    
16        Excluir_Duplicados
    
17        strSQL = "SELECT * FROM INDICES"
18        Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
19        recTab.MoveLast
20        TOT_ITEM = recTab.RecordCount
21        recTab.MoveFirst

22       For i = 1 To TOT_ITEM
23           Label1.Caption = "Criando indices da tabela: " & recTab!nome_tabela
24           DoEvents
25          If recTab!nome_indice <> "" Then
26              DoEvents
27              Me.Label1.Caption = "Criando indice " & recTab!nome_indice
                'dbLocal.Execute "DROP INDEX " & recTab!nome_indice & " on " & recTab!nome_tabela
28              dbLocal.Execute "CREATE INDEX " & recTab!nome_indice & " ON " & recTab!nome_tabela & " (" & recTab!CAMPOS_INDICE & ")"
29          End If

30          If recTab!nome_pk <> "" Then
31              DoEvents
32              Me.Label1.Caption = "Criando indice " & recTab!nome_indice
                'dbLocal.Execute "ALTER TABLE " & recTab!nome_tabela & " DROP CONSTRAINT " & recTab!NOME_PK
33              dbLocal.Execute "CREATE INDEX " & recTab!nome_pk & " ON " & recTab!nome_tabela & " (" & recTab!CAMPOS_PK & ") WITH PRIMARY"
34          End If
35          recTab.MoveNext
36          DoEvents
37          Me.Label1.Refresh
38      Next
          
Trata_Erro:
39    If Err.Number = -2147467259 Or Err.Number = -2147217887 Or Err.Number = 3022 Then
40        vArqLog = FreeFile
41        If Dir("C:\PGMS\INDICELOG.TXT") <> "" Then
42            Open "C:\PGMS\INDICELOG.TXT" For Append As #vArqLog
43        Else
44            Open "C:\PGMS\INDICELOG.TXT" For Output As #vArqLog
45        End If
46        Print #vArqLog, recTab!nome_tabela & " - " & recTab!nome_pk & " - " & Now
47        Close #vArqLog
48        Err.Number = 0
49        Resume Next
50      ElseIf Err.Number = 3377 Then 'No such relationship 'xxxxxx' on table 'xxxxx'.
51          vArqLog = FreeFile
52          If Dir("C:\PGMS\INDICELOG.TXT") <> "" Then
53              Open "C:\PGMS\INDICELOG.TXT" For Append As #vArqLog
54          Else
55              Open "C:\PGMS\INDICELOG.TXT" For Output As #vArqLog
56          End If
57          Print #vArqLog, recTab!nome_tabela & " - " & recTab!nome_pk & " - " & Err.Description & " - " & Now
58          Close #vArqLog
59          Err.Number = 0
60          Resume Next
61      ElseIf Err.Number = 3283 Or Err.Number = 3265 Or Err.Number = 3295 Or Err.Number = 3291 Or Err.Number = 3409 Or Err.Number = 3372 Then
62          vArqLog = FreeFile
63          If Dir("C:\PGMS\INDICELOG.TXT") <> "" Then
64              Open "C:\PGMS\INDICELOG.TXT" For Append As #vArqLog
65          Else
66              Open "C:\PGMS\INDICELOG.TXT" For Output As #vArqLog
67          End If
68          Print #vArqLog, recTab!nome_tabela & " - " & recTab!nome_pk & " - " & Err.Description & " - " & Now
69          Close #vArqLog
70          Err.Number = 0
71          Resume Next
72    ElseIf Err.Number <> 0 Then
73        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha:" & Erl
74    End If
End Sub

Sub Excluir_Duplicados()

1         On Error GoTo Trata_Erro

          Dim conn As ADODB.Connection
          Dim vrst As ADODB.Recordset
          Dim vrstDupl As ADODB.Recordset
          Dim vCampos As String
          Dim vRstTabela As ADODB.Recordset
          Dim vRstChave As ADODB.Recordset
          Dim vStrJoin As String
          Dim vStrChave As String
          Dim i As Integer
          Dim ii As Integer
          Dim iii As Integer
          
          Dim vWhere
          
          Dim vRegAfetados As Long
          
          'Path_drv = Pegar_Drive
          
2         Set conn = CreateObject("ADODB.Connection")
3         conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path_drv & "\Dados\Base_Dpk.mdb;User Id=admin;Password=;"
          
4         Set vrst = New ADODB.Recordset
5         vrst.Open "SELECT DISTINCT NOME_TABELA From INDICES", conn, adOpenKeyset, adLockReadOnly
          
6         For i = 1 To vrst.RecordCount
          
7             Me.Label1.Caption = "Verificando registros duplicados " & vrst!nome_tabela
          
8             Me.Label1.Refresh
          
9             Set vRstTabela = New ADODB.Recordset
              
10            vRstTabela.Open "SELECT TOP 1 * FROM " & vrst!nome_tabela, conn, adOpenKeyset
11            vCampos = ""
12            For ii = 0 To vRstTabela.Fields.Count - 1
13                If vCampos = "" Then
14                    vCampos = vRstTabela.Fields(ii).Name
15                Else
16                    vCampos = vCampos & "," & vRstTabela.Fields(ii).Name
17                End If
18            Next
19            Set vRstTabela = Nothing

20            vCamposJoin = Split(vCampos, ",")
          
21            conn.Execute "DROP TABLE DUPLICADOS"
              
Duplicados:
22            conn.Execute "SELECT " & vCampos & " INTO DUPLICADOS FROM " & vrst!nome_tabela & " GROUP BY " & vCampos & " HAVING COUNT(*) >1"
        
23            Set vrstDupl = New ADODB.Recordset
24            vrstDupl.Open "SELECT COUNT(*) FROM DUPLICADOS", conn, adOpenKeyset, adLockOptimistic

25            If vrstDupl.Fields(0) > 0 Then
                    
26               Me.Label1.Caption = "Excluindo duplicados da tabela " & vrst!nome_tabela
27               Me.Label1.Refresh
              
28               Set vRstChave = New ADODB.Recordset
                    
29               Me.Label1.Caption = "Criando tabela " & vrst!nome_tabela & "_BKP"
30               Me.Label1.Refresh
                    
31               conn.Execute "Select distinct " & vrst!nome_tabela & ".* INTO " & vrst!nome_tabela & "_BKP from " & vrst!nome_tabela
                          
32               Me.Label1.Caption = "Excluindo tabela " & vrst!nome_tabela
33               Me.Label1.Refresh
                    
34               conn.Execute "DROP TABLE " & vrst!nome_tabela
                        
35               Me.Label1.Caption = "Criando tabela " & vrst!nome_tabela
36               Me.Label1.Refresh
                        
37               conn.Execute "Select distinct " & vrst!nome_tabela & "_BKP.* INTO " & vrst!nome_tabela & " from " & vrst!nome_tabela & "_BKP"
              
38               Me.Label1.Caption = "Excluindo tabela " & vrst!nome_tabela & "_BKP"
39               Me.Label1.Refresh
              
40               conn.Execute "DROP TABLE " & vrst!nome_tabela & "_BKP"
              
41            End If
              
Proximo:
              
42            Set vrstDupl = Nothing
              
43            vrst.MoveNext
              
44        Next
45        conn.Close
46        Set conn = Nothing
          
Trata_Erro:
47        If Err.Number = -2147217904 Then
48            GoTo Proximo
49        ElseIf Err.Number = -2147217865 Then
50            GoTo Duplicados
51        ElseIf Err.Number <> 0 Then
52            MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
53        End If
End Sub

Function fMontar_Join(pNomeTabela As String) As String
    Dim i As Integer
    Dim vStr As String
    
    For i = 0 To UBound(vCamposJoin)
        If vStr = "" Then
            vStr = pNomeTabela & "." & vCamposJoin(i) & " = DUPLICADOS." & vCamposJoin(i)
        Else
            vStr = vStr & " AND " & pNomeTabela & "." & vCamposJoin(i) & " = DUPLICADOS." & vCamposJoin(i)
        End If
    Next
    fMontar_Join = vStr
End Function

Sub Verificar_PKs()

1         On Error GoTo Trata_Erro

          Dim conn As ADODB.Connection
          Dim vrst As ADODB.Recordset
          Dim i As Integer
          Dim vArq As Integer
          
2         Set conn = CreateObject("ADODB.Connection")
3         conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Path_drv & "\Dados\Base_Dpk.mdb;User Id=admin;Password=;"

4         vArq = FreeFile

5         Open "C:\PK_BASE_DPK.TXT" For Output As #vArq

6         Set vrst = New ADODB.Recordset
7         vrst.Open "SELECT * From INDICES where NOME_PK <> ''", conn, adOpenKeyset, adLockReadOnly
          
8         For i = 1 To vrst.RecordCount
9               If fExistePK(vrst!nome_tabela, vrst!nome_pk) = True Then
10                  Print #vArq, vrst!nome_tabela & Space(30 - Len(vrst!nome_tabela)) & " - " & vrst!nome_pk & Space(30 - Len(vrst!nome_pk)) & " - OK"
11              Else
12                  Print #vArq, vrst!nome_tabela & Space(30 - Len(vrst!nome_tabela)) & " - " & vrst!nome_pk & Space(30 - Len(vrst!nome_pk)) & " - ERRO"
13              End If
                    
14              vrst.MoveNext
15        Next

16        Close #vArq

Trata_Erro:
17        If Err.Number <> 0 Then
18      MsgBox "Sub Verificar_Pks" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
19        End If
End Sub

Function fExistePK(pNomeTabela As String, pnomepk As String) As Boolean

          Dim dbLocal As DAO.Database
          Dim vIndex As DAO.Index
          Dim i As Integer
          Dim ii As Integer
          Dim vTableDef As DAO.TableDef
          Dim vtabelas As Integer
          
1         fExistePK = False
          
2         Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
3         Set vTableDef = dbLocal.TableDefs(pNomeTabela)
4         For i = 0 To vTableDef.Indexes.Count - 1
5             Set vIndex = vTableDef.Indexes(i)
6             If UCase(vIndex.Name) = pnomepk Then
7                fExistePK = True
8                Exit Function
9             End If
10        Next i
          
11        dbLocal.Close
12        Set dbLocal = Nothing
13        Set vTableDef = Nothing
          
End Function

