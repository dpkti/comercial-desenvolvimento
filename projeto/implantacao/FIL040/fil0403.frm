VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmGER0403 
   Caption         =   "CONSULTA LOG DE COMUNICA��O"
   ClientHeight    =   4230
   ClientLeft      =   1095
   ClientTop       =   1800
   ClientWidth     =   6720
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4230
   ScaleWidth      =   6720
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel sspFaixa1 
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   9615
      _Version        =   65536
      _ExtentX        =   16960
      _ExtentY        =   1296
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand scmdSair 
         Height          =   735
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Picture         =   "fil0403.frx":0000
      End
   End
   Begin MSGrid.Grid grLogCom 
      Height          =   4815
      Left            =   120
      TabIndex        =   0
      Top             =   1320
      Width           =   9255
      _Version        =   65536
      _ExtentX        =   16325
      _ExtentY        =   8493
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Cols            =   7
      MouseIcon       =   "fil0403.frx":031A
   End
   Begin VB.Label lblSair 
      AutoSize        =   -1  'True
      BackColor       =   &H0000FFFF&
      Caption         =   "Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair "
   End
End
Attribute VB_Name = "frmGER0403"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    lblSair.Visible = False
    grLogCom.Row = 0
    grLogCom.ColWidth(0) = 1600
    grLogCom.Col = 0
    grLogCom.Text = "DATA / HORA"
    grLogCom.ColWidth(1) = 1700
    grLogCom.Col = 1
    grLogCom.Text = "TIPO COMUNICA��O"
    grLogCom.ColWidth(2) = 1100
    grLogCom.Col = 2
    grLogCom.Text = "N�M.DA MSG"
    grLogCom.ColWidth(3) = 1800
    grLogCom.Col = 3
    grLogCom.Text = "ASSUNTO"
    grLogCom.ColWidth(4) = 1500
    grLogCom.Col = 4
    grLogCom.Text = "TAMANHO ARQ."
    grLogCom.ColWidth(5) = 1500
    grLogCom.Col = 5
    grLogCom.Text = "TEMPO DURA��O"
    grLogCom.ColWidth(6) = 1100
    grLogCom.Col = 6
    grLogCom.Text = "SITUA��O"
    
    MONTA_GRLOGCOM
    
End Sub
Private Sub MONTA_GRLOGCOM()
      Dim strDataHora As String, strTpCom As String, _
          strNumMsg As String, strAssunto As String, _
          strTamArq As String, strTempo As String, _
          strSituacao As String
      Dim strLinha As String
      Dim contLinha As Long

1     On Error GoTo Trata_Erro

2     Open Path_drv & "\LOG\LOGCOM.TXT" For Input As #1 ' Abre arquivo de log de diverg�ncias para leitura
3     contLinha = 1
4     Do While Not EOF(1)
5         Input #1, strLinha      ' leitura da linha do arquivo
6         contLinha = contLinha + 1
7         grLogCom.Rows = contLinha
8         grLogCom.Row = contLinha - 1
9         strDataHora = Mid(strLinha, 1, 8) & " " & Mid(strLinha, 9, 8)
10        grLogCom.Col = 0
11        grLogCom.Text = strDataHora
12        strTpCom = Mid(strLinha, 17, 1)
13        grLogCom.Col = 1
14        grLogCom.Text = IIf(strTpCom = "0", "RECEP��O", "TRANSMISS�O")
15        strNumMsg = Mid(strLinha, 18, 6)
16        grLogCom.Col = 2
17        grLogCom.Text = strNumMsg
18        strAssunto = Mid(strLinha, 24, 30)
19        grLogCom.Col = 3
20        grLogCom.Text = strAssunto
21        strTamArq = Mid(strLinha, 54, 8)
22        grLogCom.Col = 4
23        grLogCom.Text = strTamArq
24        strTempo = Mid(strLinha, 62, 5)
25        grLogCom.Col = 5
26        grLogCom.Text = strTempo
27        strSituacao = Mid(strLinha, 67, 1)
28        grLogCom.Col = 6
29        grLogCom.Text = IIf(strSituacao = "0", "CONCLU�DA", "ABORTADA")
          
30    Loop
31    Close #1    ' Fecha o arquivo de log de divergencia

32    Exit Sub

Trata_Erro:

33    If Err.Number = 53 Then
34        MsgBox "Sub Monta_GrLogCom" & vbCrLf & "Erro NR.: " & Err.Number & " - " & " Arquivo n�o Encontrado " & Chr(13) & Path_drv & "\LOG\LOGCOM.TXT", vbExclamation, "ERRO NO APLICATIVO"
35    Else
36        MsgBox "Sub Monta_GrLogCom" & vbCrLf & "ERRO NR.: " & Err.Number & " - " & Err.Description
37    End If

38    Exit Sub

End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSair.Visible = False
End Sub


Private Sub mnuSair_Click()
    scmdSair_Click
End Sub

Private Sub scmdSair_Click()
    Unload Me
End Sub

Private Sub scmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSair.Left = 240
    lblSair.Caption = "Sair"
    lblSair.Visible = True
End Sub


Private Sub sspFaixa1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSair.Visible = False
End Sub


