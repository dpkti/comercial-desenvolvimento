Attribute VB_Name = "mGER0401"
Option Explicit

    'Eduardo - 28/04/2006
    Public vRegs As Long

    Public dbLocal As Database
    Public dbaccess1 As Database
    Public dbExt As Database
    Public dbControle As Database
    Public recTab As Recordset
    Public ss As Recordset
    Public recCtl As Recordset
    Public strSQL As String
    Public mDyna As Dynaset
    Public numArq As Integer
    Public contArq As Integer
    Public nomeArq As String
    Public icontrole_atual As Integer
    Public ifilial_atual As Integer
    Public controle_atual As String
    Public filial_atual As String
    Public mHoje As Date 'Data de faturamento
    
    Public dir_base As String
    Public arqmdb As String
    Public FL_TIPO As String
    
    Public STRAT_PED As String
    Public TMP_I As Integer
    Public TMP_LINHA As String
    Public POS_SEP As Integer
    Public CONTEUDO

   '************** PEDNOTA_VENDA ************
    Public Ped_Loja As Integer
    Public Ped_Num As Double
    Public Ped_Seq As Integer
    Public Ped_Loja_Nota
    Public Ped_Num_Nota
    Public Ped_Pen
    Public Ped_Fil
    Public Ped_Tp_Ped
    Public Ped_Tp_DB
    Public Ped_Tp_Tran
    Public Ped_Dt_Dig As String
    Public Ped_Dt_Ped As String
    Public Ped_Dt_Emiss As String
    Public Ped_Dt_Ssm As String
    Public Ped_Dt_Pol As String
    Public Ped_Dt_Cre As String
    Public Ped_Dt_Fre As String
    Public Ped_Nope
    Public Ped_Cli
    Public Ped_For
    Public Ped_Ent
    Public Ped_Cob
    Public Ped_Tran
    Public Ped_Rep
    Public Ped_Ven
    Public Ped_Pla
    Public Ped_Ban
    Public Ped_Dest
    Public Ped_Oper
    Public Ped_Fre
    Public Ped_Pes
    Public Ped_Qtd_Ssm
    Public Ped_Qtd_Ped
    Public Ped_Qtd_Not
    Public Ped_Vl
    Public Ped_Ipi
    Public Ped_Icm1
    Public Ped_Icm2
    Public Ped_Bas1
    Public Ped_Bas2
    Public Ped_Bas3
    Public Ped_Base
    Public Ped_Bas5
    Public Ped_Bas6
    Public Ped_Out
    Public Ped_Maj
    Public Ped_Ret
    Public Ped_Vl_Ipi
    Public Ped_Is_Ipi
    Public Ped_Ou_Ipi
    Public Ped_Vl_Fre
    Public Ped_Ace
    Public Ped_Des
    Public Ped_Suf
    Public Ped_Acr
    Public Ped_Seg
    Public Ped_Pc_Icm1
    Public Ped_Pc_Icm2
    Public Ped_Ali
    Public Ped_Can
    Public Ped_Fl_Ssm
    Public Ped_Fl_Nfis
    Public Ped_Fl_Pen
    Public Ped_Fl_Ace
    Public Ped_Fl_Icm
    Public Ped_Sit
    Public Ped_Pro
    Public Ped_Jur
    Public Ped_Che
    Public Ped_Com
    Public Ped_Dup
    Public Ped_Lim
    Public Ped_Sal
    Public Ped_Cli_Nov
    Public Ped_Desc
    Public Ped_Acre
    Public Ped_VlFat
    Public Ped_Desc1
    Public Ped_Desc2
    Public Ped_Desc3
    Public Ped_Fret
    Public Ped_MensP
    Public Ped_MensN
    Public Ped_CODVDR

   '************** ITPEDNOTA_VENDA ***************
    Public Ite_Num_It
    Public Ite_Loja_Nota
    Public Ite_Num_Nota
    Public Ite_It_Nota
    Public Ite_Dpk
    Public Ite_Sol
    Public Ite_Ate
    Public Ite_Pre
    Public Ite_Tab
    Public Ite_Desc1
    Public Ite_Desc2
    Public Ite_Desc3
    Public Ite_Icm
    Public Ite_Ipi
    Public Ite_Com
    Public Ite_Tlm
    Public Ite_Trib
    Public Ite_Trip
    Public Ite_Sit

   '**************** V_PEDLIQ_VENDA *****************
    Public V_P_Ite
    Public V_P_Sit
    Public V_P_Pr
    Public V_P_Vl
    Public V_P_Prs
    Public V_P_Vls

   '***************** ROMANEIO ********************
    Public Rom_DtC
    Public Rom_DtD
    Public Rom_Num
    Public Rom_Con
    Public Rom_Car
    Public Rom_Qtd

   '*************** R_PEDIDO_CONF
    Public R_P_Num
    Public R_P_Con
    Public R_P_Emb
    Public R_P_Exp
    Public R_P_Fl
    Public R_P_Qtd

   '*** Leitura de e-mail de retorno de pedido para
   'atualiza��o de flag de pedido
    Public dNum_Pen
    Public Chave
    
    Public Path_drv

Public Function ATU_CLIEINT() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CLIENTE_INTERNET IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIENTE_INTERNET "
4         dbExt.Execute strSQL
            
         'Efetua Update de uma tabela com base na outra .......................
5         strSQL = "UPDATE CLIENTE_INTERNET INNER JOIN AUX_CLIENTE_INTERNET ON " & _
                   " CLIENTE_INTERNET.COD_CLIENTE = AUX_CLIENTE_INTERNET.COD_CLIENTE " & _
                   " SET CLIENTE_INTERNET.E_MAIL = AUX_CLIENTE_INTERNET.E_MAIL, " & _
                   " CLIENTE_INTERNET.HOMEPAGE = AUX_CLIENTE_INTERNET.HOMEPAGE "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CLIENTE_INTERNET SELECT * FROM AUX_CLIENTE_INTERNET"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CLIENTE_INTERNET"
              
12        Controle "CLIE_INT", vRegs
              
13        ATU_CLIEINT = True

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CLIENTE_INTERNET"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CLIEINT = False
22            Screen.MousePointer = 1
'23            Call FUNCAO_ERRO("MF")
24            MsgBox "Sub ATU_CLIEINT - MF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function

Public Function ATU_VDR_CLIENTE_CATEG_VDR() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_VDR_CLIENTE_CATEG_VDR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_CLIENTE_CATEG_VDR "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE VDR_CLIENTE_CATEG_VDR INNER JOIN AUX_VDR_CLIENTE_CATEG_VDR ON " & _
                   " VDR_CLIENTE_CATEG_VDR.COD_CLIENTE = AUX_VDR_CLIENTE_CATEG_VDR.COD_CLIENTE  " & _
                   " SET VDR_CLIENTE_CATEG_VDR.CATEGORIA = AUX_VDR_CLIENTE_CATEG_VDR.CATEGORIA "
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
                
8         strSQL = "INSERT INTO VDR_CLIENTE_CATEG_VDR SELECT * FROM AUX_VDR_CLIENTE_CATEG_VDR "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_VDR_CLIENTE_CATEG_VDR "
                  
12        Controle "VDR_CLIE", vRegs
                  
13        ATU_VDR_CLIENTE_CATEG_VDR = True

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_VDR_CLIENTE_CATEG_VDR"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_VDR_CLIENTE_CATEG_VDR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function

Public Function ATU_FLAG()

      'Atualiza Flag do Pedido de Digitacao (DIGITA) ou Telemarketing (VENDAS) quando
      'o e-mail de retorno voltou indicando que ocorreu erro na finaliza��o do pedido.
      ' a Flag (FL_PEDIDO_FINALIZADO) voltar� para "N" para que o pedido possa ser
      ' enviado novamente para Campinas
          Dim Filial
          Dim vArq As Integer
          
1         On Error GoTo TRATA_ERRO_LOCAL
          
2         Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
          
3         mdiGER040.SSPanel2.Visible = True
4         mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Posi��o de Pedidos ... "
          
5         Do While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT") <> ""
6             STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT")
7             STRAT_PED = Path_drv & "\COM\ENTRADA\PEDIDOS\" & STRAT_PED
              vArq = FreeFile
8             Open STRAT_PED For Input As #vArq
9             Line Input #vArq, TMP_LINHA
10            POS_SEP = InStr(TMP_LINHA, "|")
11            Ped_Loja = Left(TMP_LINHA, POS_SEP - 1)
12            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
13            POS_SEP = InStr(TMP_LINHA, "|")
14            Ped_Num = Left(TMP_LINHA, POS_SEP - 1)
15            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
16            POS_SEP = InStr(TMP_LINHA, "|")
17            Ped_Seq = Left(TMP_LINHA, POS_SEP - 1)
18            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
19            Ped_MensP = TMP_LINHA
                
Erro_62:
20            mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Pedido n�: " & Ped_Num
21            Close #vArq
22            strSQL = ""
             
             'DEFINE A FILIAL DO PEDIDO
23            strSQL = "SELECT COD_FILIAL from DEPOSITO "
24            Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
25            If Not recTab.EOF Then
26               Filial = recTab!COD_FILIAL
27            Else
28               Filial = 0
29            End If
              
30            strSQL = ""
31            strSQL = "SELECT * from PEDNOTA_VENDA "
32            strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
33            strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
34            strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "
                 
35            Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
                
36            dbLocal.BeginTrans
37            If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
38                strSQL = ""
39                strSQL = "UPDATE "
40                strSQL = strSQL & " PEDNOTA_VENDA "
41                strSQL = strSQL & " SET "
42                strSQL = strSQL & " SITUACAO = 0 ,"
43                strSQL = strSQL & " FL_GER_NFIS = 'N' ,"
44                strSQL = strSQL & " COD_FILIAL = " & Filial & " ,"
45                strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' "
46                strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
47                strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
48                strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
49                dbLocal.Execute strSQL
50            Else ' N�o Existe o Pedido - Incluir
51                strSQL = ""
52                strSQL = "INSERT "
53                strSQL = strSQL & " into PEDNOTA_VENDA "
54                strSQL = strSQL & " (cod_loja, num_pedido, seq_pedido,Mens_pedido,fl_ger_nfis,SITUACAO,COD_FILIAL)"
55                strSQL = strSQL & " Values "
56                strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
57                strSQL = strSQL & " ' " & Ped_MensP & " ', 'N',0," & Filial & ")"
58                dbLocal.Execute strSQL
59            End If
          
60            dbLocal.CommitTrans
               
61            Kill STRAT_PED
62        Loop
          
63        dbLocal.Close
64        mdiGER040.SSPanel2.Caption = "Atualiza��o OK! "

65    Exit Function

TRATA_ERRO_LOCAL:

66    If Err = 3049 Then

          'MousePointer = 11
67        DoEvents

          ' Reparar Banco de Dados
68        RepairDatabase (Path_drv & "\DADOS\BASE_DPK.MDB")
          
69        COMPACTA_LOCAL
          
          'MousePointer = 0
          ' Ap�s corrigir o banco danificado processa novamente a linha do programa
          ' que gerou o erro.
70        Resume 0
      ElseIf Err.Number = 62 Then
          GoTo Erro_62
        
71    ElseIf Err.Number <> 0 Then
          MsgBox "Sub ATU_FLAG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
74    End If
End Function

Public Function ATU_DICAS_RETORNO(ByVal pNomeArq As String)
1         On Error GoTo Trata_Erro
          Dim vSequencia As String
          Dim vFlTipo As String
          Dim vDtAlt As String
          Dim vDtDica As String
          Dim vCodVend As String
          Dim vAno As String
          Dim vMes As String
          Dim vDia As String
          Dim vHora As String
          Dim vMinuto As String
          Dim vSegundo As String

2         Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)
3         mdiGER040.SSPanel2.Visible = True
4         mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Dicas ... "
5         While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\" & pNomeArq) <> ""
              
6             STRAT_PED = Path_drv & "\COM\ENTRADA\PEDIDOS\" & pNomeArq
7             Open STRAT_PED For Input As #1
8             Do While Not EOF(1)
9                 Line Input #1, TMP_LINHA
10                vSequencia = CLng(Mid(TMP_LINHA, 6, 9))
11                vFlTipo = Mid(TMP_LINHA, 1, 1)
                  
12                Select Case vFlTipo
                      Case "I"
13                        vFlTipo = 1
14                    Case "U"
15                        vFlTipo = 2
16                    Case "D"
17                        vFlTipo = 3
18                End Select
                  
                  'Data Dica
19                vDtDica = Mid(TMP_LINHA, 27, 14)
20                vAno = Mid(vDtDica, 1, 4)
21                vMes = Mid(vDtDica, 5, 2)
22                vDia = Mid(vDtDica, 7, 2)
23                vHora = Mid(vDtDica, 9, 2)
24                vMinuto = Mid(vDtDica, 11, 2)
25                vSegundo = Mid(vDtDica, 13, 2)
26                vDtDica = vDia & "/" & vMes & "/" & vAno & Space(1) & vHora & ":" & vMinuto & ":" & vSegundo
                  
                  'Data Altera��o
27                vDtAlt = Mid(TMP_LINHA, 41, 14)
28                vAno = Mid(vDtAlt, 1, 4)
29                vMes = Mid(vDtAlt, 5, 2)
30                vDia = Mid(vDtAlt, 7, 2)
31                vHora = Mid(vDtAlt, 9, 2)
32                vMinuto = Mid(vDtAlt, 11, 2)
33                vSegundo = Mid(vDtAlt, 13, 2)
34                vDtAlt = vDia & "/" & vMes & "/" & vAno & Space(1) & vHora & ":" & vMinuto & ":" & vSegundo
                  
                  'Formata��o data
35                If vDtAlt <> "0" Then
36                    vDtAlt = Format$(vDtAlt, "mm/dd/yy hh:nn:ss")
37                End If
38                If vDtDica <> "0" Then
39                    vDtDica = Format$(vDtDica, "mm/dd/yy hh:nn:ss")
40                End If
41                vCodVend = CInt(Mid(TMP_LINHA, 21, 6))
                  
42                dbLocal.Execute "Delete * from Controle_Dicas where sequencia = " & vSequencia & _
                  " and Dt_Dica = #" & vDtDica & "# and Dt_Alteracao = #" & vDtAlt & "#  and Cod_vend = " & vCodVend
                  
43            Loop
44            Close #1
45            Kill STRAT_PED
46        Wend
47        mdiGER040.SSPanel2.Caption = "Atualiza��o das dicas OK!"
Trata_Erro:
48        If Err.Number <> 0 Then
49            MsgBox "Sub ATU_DICAS_RETORNO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
50        End If
End Function


Public Function ATU_PEDIDOS()

    On Error GoTo Trata_Erro

1         Set dbLocal = OpenDatabase(Path_drv & "\DADOS\BASE_DPK.MDB", False, False)

2             mdiGER040.SSPanel2.Visible = True
3             mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Posi��o de Pedidos ... "
4             While Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") <> ""
5                 STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON")
6                 If Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.CON") = "" Then
7                     STRAT_PED = Dir(Path_drv & "\COM\ENTRADA\PEDIDOS\*.TXT")
8                 End If
9                 STRAT_PED = Path_drv & "\COM\ENTRADA\PEDIDOS\" & STRAT_PED
10                Open STRAT_PED For Input As #1
11                Do While Not EOF(1)
12                    Line Input #1, TMP_LINHA
13                    TMP_I = InStr(TMP_LINHA, "|")
14                    If TMP_I <> 0 Then
15                        Select Case UCase(Mid(TMP_LINHA, 1, TMP_I - 1))
                              Case "PED": Call ATU_PED
16                            Case "ITE": Call ATU_ITE
17                            Case "V_P": Call ATU_V_P
18                            Case "ROM": Call ATU_ROM
19                            Case "R_P": Call ATU_R_P
20                            Case Else: Call CHAVE_PED
21                        End Select
22                    Else
23                        TMP_LINHA = TMP_LINHA & vbCrLf
24                    End If
25                Loop
26                Close #1
              
27                Kill STRAT_PED
28            Wend
          
29            mdiGER040.SSPanel2.Caption = "Atualiza��o OK! "

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_PEDIDOS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_ITE()
      '*************************************************************
      'Separa valores dos demais campos da tabela ITPEDNOTA_VENDA fora
      ' a chave da tabela

1         On Error GoTo Trata_Erro

2         Call SEPARA
3         Ite_Num_It = CONTEUDO
4         mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Item : " & Ite_Num_It
          
5         Call SEPARA
6         Ite_Loja_Nota = CONTEUDO
7         Call SEPARA
8         Ite_Num_Nota = CONTEUDO
9         Call SEPARA
10        Ite_It_Nota = CONTEUDO
11        Call SEPARA
12        Ite_Dpk = CONTEUDO
13        Call SEPARA
14        Ite_Sol = CONTEUDO
15        Call SEPARA
16        Ite_Ate = CONTEUDO
17        Call SEPARA
18        Ite_Pre = CONTEUDO
19        Call SEPARA
20        Ite_Tab = CONTEUDO
21        Call SEPARA
22        Ite_Desc1 = CONTEUDO
23        Call SEPARA
24        Ite_Desc2 = CONTEUDO
25        Call SEPARA
26        Ite_Desc3 = CONTEUDO
27        Call SEPARA
28        Ite_Icm = CONTEUDO
29        Call SEPARA
30        Ite_Ipi = CONTEUDO
31        Call SEPARA
32        Ite_Com = CONTEUDO
33        Call SEPARA
34        Ite_Tlm = CONTEUDO
35        Call SEPARA
36        Ite_Trib = CONTEUDO
37        Call SEPARA
38        Ite_Trip = CONTEUDO
39        Call SEPARA
40        Ite_Sit = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set dbLocal = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

41    strSQL = "SELECT * from ITPEDNOTA_VENDA "
42    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
43    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
44    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
45    strSQL = strSQL & " num_item_pedido = " & Ite_Num_It

46    Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
47    strSQL = ""
48    dbLocal.BeginTrans
49    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
50        strSQL = "UPDATE "
51        strSQL = strSQL & " ITPEDNOTA_VENDA "
52        strSQL = strSQL & " SET "
53        strSQL = strSQL & " COD_LOJA_NOTA =  " & Ite_Loja_Nota & ", "
54        strSQL = strSQL & " NUM_NOTA = " & Ite_Num_Nota & ", "
55        strSQL = strSQL & " NUM_ITEM_NOTA = " & Ite_It_Nota & ", "
56        strSQL = strSQL & " COD_DPK = " & Ite_Dpk & ", "
57        strSQL = strSQL & " QTD_SOLICITADA = " & Ite_Sol & ","
58        strSQL = strSQL & " QTD_ATENDIDA = " & Ite_Ate & ","
59        strSQL = strSQL & " PRECO_UNITARIO = " & Ite_Pre & ", "
60        strSQL = strSQL & " TABELA_VENDA = ' " & Ite_Tab & "',"
61        strSQL = strSQL & " PC_DESC1 =  " & Ite_Desc1 & " , "
62        strSQL = strSQL & " PC_DESC2 =  " & Ite_Desc2 & " ,"
63        strSQL = strSQL & " PC_DESC3 =  " & Ite_Desc3 & ","
64        strSQL = strSQL & " PC_DIFICM =  " & Ite_Icm & ","
65        strSQL = strSQL & " PC_IPI =  " & Ite_Ipi & ","
66        strSQL = strSQL & " PC_COMISS =  " & Ite_Com & ","
67        strSQL = strSQL & " PC_COMISSTLMK=  " & Ite_Tlm & ","
68        strSQL = strSQL & " COD_TRIB = " & Ite_Trib & ","
69        strSQL = strSQL & " COD_TRIBIPI = " & Ite_Trip & ","
70        strSQL = strSQL & " SITUACAO = " & Ite_Sit
71        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
72        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
73        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " and "
74        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & Ite_Num_It
         
75        dbLocal.Execute strSQL

76    Else ' N�o Existe o Pedido - Incluir
77       strSQL = "INSERT "
78       strSQL = strSQL & " into ITPEDNOTA_VENDA values "
79       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
80       strSQL = strSQL & " " & Ite_Num_It & "," & Ite_Loja_Nota & " , " & Ite_Num_Nota & ", "
81       strSQL = strSQL & " " & Ite_It_Nota & ", " & Ite_Dpk & " , " & Ite_Sol & " ,"
82       strSQL = strSQL & " " & Ite_Ate & " , " & Ite_Pre & ", '" & Ite_Tab & "', "
83       strSQL = strSQL & " " & Ite_Desc1 & " , " & Ite_Desc2 & " , " & Ite_Desc3 & " , "
84       strSQL = strSQL & " " & Ite_Icm & " , " & Ite_Ipi & "," & Ite_Com & ","
85       strSQL = strSQL & " " & Ite_Tlm & " , " & Ite_Trib & ", " & Ite_Trip & " ," & Ite_Sit & ")"

      'MsgBox strSQL
         
86       dbLocal.Execute strSQL
         
87    End If
88    dbLocal.CommitTrans
      '*********************************************

Trata_Erro:
89        If Err.Number <> 0 Then
90            MsgBox "Sub ATU_ITE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
91        End If

End Function
Public Function ATU_R_CLIE_REPRES() As Boolean
1         On Error GoTo VerErro
          
2         strSQL = "SELECT * INTO AUX_R_CLIE_REPRES IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_CLIE_REPRES "
3         dbExt.Execute strSQL
                
4         strSQL = "INSERT INTO R_CLIE_REPRES SELECT * FROM AUX_R_CLIE_REPRES"
5         dbLocal.Execute strSQL
6         vRegs = dbLocal.RecordsAffected
          
7         dbLocal.Execute "DROP TABLE AUX_R_CLIE_REPRES"
          
8         ATU_R_CLIE_REPRES = True
          
9         Controle "R_CLIE_R", vRegs
          
10        Exit Function
          
VerErro:
          
11        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
12            dbLocal.Execute "DROP TABLE AUX_R_CLIE_REPRES"
13            Resume
14        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
15            Resume Next
16        ElseIf Err.Number <> 0 Then
17            ATU_R_CLIE_REPRES = False
18            Screen.MousePointer = 1
19            MsgBox "Sub ATU_R_CLIE_REPRES - JM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
20        End If

End Function

Public Function ATU_R_UF_DEPOSITO() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_R_UF_DEPOSITO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_UF_DEPOSITO "
4         dbExt.Execute strSQL
                
5         strSQL = "INSERT INTO R_UF_DEPOSITO SELECT * FROM AUX_R_UF_DEPOSITO"
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
              
8         dbLocal.Execute "DROP TABLE AUX_R_UF_DEPOSITO"
          
9         ATU_R_UF_DEPOSITO = True
          
10        Controle "R_UF_DEP", vRegs
          
11    Exit Function
          
VerErro:
          
12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_R_UF_DEPOSITO"
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        ElseIf Err.Number <> 0 Then
18            ATU_R_UF_DEPOSITO = False
19            Screen.MousePointer = 1
21            MsgBox "Sub ATU_R_UF_DEPOSITO - KJ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function


Public Function ATU_V_P()
    
    On Error GoTo Trata_Erro

         '*************************************************************
         'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
         'a chave da tabela

1         Call SEPARA
2         V_P_Ite = CONTEUDO
3         mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando V_PEDLIQ_VENDA do Item : " & V_P_Ite
4         Call SEPARA
5         V_P_Sit = CONTEUDO
6         Call SEPARA
7         V_P_Pr = CONTEUDO
8         Call SEPARA
9         V_P_Vl = CONTEUDO
10        Call SEPARA
11        V_P_Prs = CONTEUDO
12        Call SEPARA
13        V_P_Vls = CONTEUDO
         '*******************************************
          
         '*******************************************
         'Grava valores lidos na tabela PEDNOTA_VENDA
         'Set dbLocal = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14                 strSQL = "SELECT * from V_PEDLIQ_VENDA "
15        strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16        strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17        strSQL = strSQL & " seq_pedido =  " & Ped_Seq & " AND "
18        strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
          
19        Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
20        strSQL = ""
21        dbLocal.BeginTrans

22        If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
23            strSQL = "UPDATE "
24            strSQL = strSQL & " V_PEDLIQ_VENDA "
25            strSQL = strSQL & " SET "
26            strSQL = strSQL & " SITUACAO = " & V_P_Sit & ", "
27            strSQL = strSQL & " PR_LIQUIDO = " & V_P_Pr & ", "
28            strSQL = strSQL & " VL_LIQUIDO = " & V_P_Vl & ", "
29            strSQL = strSQL & " PR_LIQUIDO_SUFRAMA = " & V_P_Prs & ","
30            strSQL = strSQL & " VL_LIQUIDO_SUFRAMA = " & V_P_Vls & ""
31            strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32            strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33            strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND"
34            strSQL = strSQL & " NUM_ITEM_PEDIDO = " & V_P_Ite
             'MsgBox strSQL
35            dbLocal.Execute strSQL
36        Else    'N�o Existe o Pedido - Incluir
37            strSQL = "INSERT "
38            strSQL = strSQL & " into V_PEDLIQ_VENDA values "
39            strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
40            strSQL = strSQL & " " & V_P_Ite & ", " & V_P_Sit & ", " & V_P_Pr & " , "
41            strSQL = strSQL & " " & V_P_Vl & " , " & V_P_Prs & ", " & V_P_Vls & ")"
             'MsgBox strSQL
42            dbLocal.Execute strSQL
43        End If
          
44        dbLocal.CommitTrans
         '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_V_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function
Public Function ATU_ROM()

    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela V_PEDLIQ_VENDA fora
      ' a chave da tabela
1     mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando ROMANEIO do Pedido n�: " & Ped_Num & " - " & Ped_Seq

2         Call SEPARA
3         Rom_DtC = CONTEUDO
4         Call SEPARA
5         Rom_DtD = CONTEUDO
6         Call SEPARA
7         Rom_Num = CONTEUDO
8         Call SEPARA
9         Rom_Con = CONTEUDO
10        Call SEPARA
11        Rom_Car = CONTEUDO
12        Call SEPARA
13        Rom_Qtd = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set dbLocal = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14    strSQL = "SELECT * from ROMANEIO "
15    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "

18    Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
19    strSQL = ""
20    dbLocal.BeginTrans
21    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
22        strSQL = "UPDATE "
23        strSQL = strSQL & " ROMANEIO "
24        strSQL = strSQL & " SET "
25        strSQL = strSQL & " DT_COLETA =  '" & Rom_DtC & "', "
26        strSQL = strSQL & " DT_DESPACHO = '" & Rom_DtD & "', "
27        strSQL = strSQL & " NUM_ROMANEIO = " & Rom_Num & ", "
28        strSQL = strSQL & " NUM_CONHECIMENTO = '" & Rom_Con & "', "
29        strSQL = strSQL & " NUM_CARRO = '" & Rom_Car & "',"
30        strSQL = strSQL & " QTD_VOLUME = " & Rom_Qtd & " "
31        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
         
34        dbLocal.Execute strSQL

35    Else ' N�o Existe o Pedido - Incluir
36       strSQL = "INSERT "
37       strSQL = strSQL & " into ROMANEIO values "
38       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
39       strSQL = strSQL & " '" & Rom_DtC & "', '" & Rom_DtD & "', " & Rom_Num & " , "
40       strSQL = strSQL & " '" & Rom_Con & "' , '" & Rom_Car & "', " & Rom_Qtd & ")"
         
41       dbLocal.Execute strSQL
         
42    End If
43    dbLocal.CommitTrans
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_ROM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_R_P()
    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela R_PEDIDO_CONF fora
      ' a chave da tabela
1     mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando R_PEDIDO_CONF do Pedido n�: " & Ped_Num & " - " & Ped_Seq

2         Call SEPARA
3         R_P_Num = CONTEUDO
4         Call SEPARA
5         R_P_Con = CONTEUDO
6         Call SEPARA
7         R_P_Emb = CONTEUDO
8         Call SEPARA
9         R_P_Exp = CONTEUDO
10        Call SEPARA
11        R_P_Fl = CONTEUDO
12        Call SEPARA
13        R_P_Qtd = CONTEUDO
      '*******************************************
          
      '*******************************************
      'Grava valores lidos na tabela PEDNOTA_VENDA
      'Set dbLocal = OpenDatabase(Path_drv &"\DADOS\BASE_DPK.MDB", False, False)

14    strSQL = "SELECT * from R_PEDIDO_CONF "
15    strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
16    strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
17    strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  and "
18    strSQL = strSQL & " NUM_CAIXA =  " & R_P_Num & ""

19    Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
20    strSQL = ""
21    dbLocal.BeginTrans
22    If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
23        strSQL = "UPDATE "
24        strSQL = strSQL & " R_PEDIDO_CONF "
25        strSQL = strSQL & " SET "
26        strSQL = strSQL & " COD_CONFERENTE = " & R_P_Con & ", "
27        strSQL = strSQL & " COD_EMBALADOR = " & R_P_Emb & ", "
28        strSQL = strSQL & " COD_EXPEDIDOR = " & R_P_Exp & ", "
29        strSQL = strSQL & " FL_PEND_ETIQ = '" & R_P_Fl & "',"
30        strSQL = strSQL & " QTD_VOLUME_PARC = " & R_P_Qtd & ""
31        strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
32        strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
33        strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq & " AND "
34        strSQL = strSQL & " NUM_CAIXA= " & R_P_Num
      ' MsgBox strSQL
35        dbLocal.Execute strSQL

36    Else ' N�o Existe o Pedido - Incluir
37       strSQL = "INSERT "
38       strSQL = strSQL & " into R_PEDIDO_CONF values "
39       strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
40       strSQL = strSQL & " " & R_P_Num & ", " & R_P_Con & ", " & R_P_Emb & " , "
41       strSQL = strSQL & " " & R_P_Exp & " , '" & R_P_Fl & "', " & R_P_Qtd & ")"
      ' MsgBox strSQL
42       dbLocal.Execute strSQL
         
43    End If
44    dbLocal.CommitTrans
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_R_P" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Function

Public Function ATU_PED()

    On Error GoTo Trata_Erro

      '*************************************************************
      'Separa valores dos demais campos da tabela PEDNOTA_VENDA fora
      ' a chave da tabela
1     mdiGER040.SSPanel2.Caption = "AGUARDE. Atualizando Pedido n�: " & Ped_Num & " - " & Ped_Seq

2         Call SEPARA
3         Ped_Loja_Nota = CONTEUDO
4         Call SEPARA
5         Ped_Num_Nota = CONTEUDO
6         Call SEPARA
7         Ped_Pen = CONTEUDO
8         Call SEPARA
9         Ped_Fil = CONTEUDO
10        Call SEPARA
11        Ped_Tp_Ped = CONTEUDO
12        Call SEPARA
13        Ped_Tp_DB = CONTEUDO
14        Call SEPARA
15        Ped_Tp_Tran = CONTEUDO
16        Call SEPARA
17        Ped_Dt_Dig = CONTEUDO
18        Call SEPARA
19        Ped_Dt_Ped = CONTEUDO
20        Call SEPARA
21        Ped_Dt_Emiss = CONTEUDO
22        Call SEPARA
23        Ped_Dt_Ssm = CONTEUDO
24        Call SEPARA
25        Ped_Dt_Pol = CONTEUDO
26        Call SEPARA
27        Ped_Dt_Cre = CONTEUDO
28        Call SEPARA
29        Ped_Dt_Fre = CONTEUDO
30        Call SEPARA
31        Ped_Nope = Trim(CONTEUDO)
32        Call SEPARA
33        Ped_Cli = CONTEUDO
34        Call SEPARA
35        Ped_For = CONTEUDO
36        Call SEPARA
37        Ped_Ent = CONTEUDO
38        Call SEPARA
39        Ped_Cob = CONTEUDO
40        Call SEPARA
41        Ped_Tran = CONTEUDO
42        Call SEPARA
43        Ped_Rep = CONTEUDO
44        Call SEPARA
45        Ped_Ven = CONTEUDO
46        Call SEPARA
47        Ped_Pla = CONTEUDO
48        Call SEPARA
49        Ped_Ban = CONTEUDO
50        Call SEPARA
51        Ped_Dest = CONTEUDO
52        Call SEPARA
53        Ped_Oper = CONTEUDO
54        Call SEPARA
55        Ped_Fre = CONTEUDO
56        Call SEPARA
57        Ped_Pes = CONTEUDO
58        Call SEPARA
59        Ped_Qtd_Ssm = CONTEUDO
60        Call SEPARA
61        Ped_Qtd_Ped = CONTEUDO
62        Call SEPARA
63        Ped_Qtd_Not = CONTEUDO
64        Call SEPARA
65        Ped_Vl = CONTEUDO
66        Call SEPARA
67        Ped_Ipi = CONTEUDO
68        Call SEPARA
69        Ped_Icm1 = CONTEUDO
70        Call SEPARA
71        Ped_Icm2 = CONTEUDO
72        Call SEPARA
73        Ped_Bas1 = CONTEUDO
74        Call SEPARA
75        Ped_Bas2 = CONTEUDO
76        Call SEPARA
77        Ped_Bas3 = CONTEUDO
78        Call SEPARA
79        Ped_Base = CONTEUDO
80        Call SEPARA
81        Ped_Bas5 = CONTEUDO
82        Call SEPARA
83        Ped_Bas6 = CONTEUDO
84        Call SEPARA
85        Ped_Out = CONTEUDO
86        Call SEPARA
87        Ped_Maj = CONTEUDO
88        Call SEPARA
89        Ped_Ret = CONTEUDO
90        Call SEPARA
91        Ped_Vl_Ipi = CONTEUDO
92        Call SEPARA
93        Ped_Is_Ipi = CONTEUDO
94        Call SEPARA
95        Ped_Ou_Ipi = CONTEUDO
96        Call SEPARA
97        Ped_Vl_Fre = CONTEUDO
98        Call SEPARA
99        Ped_Ace = CONTEUDO
100       Call SEPARA
101       Ped_Des = CONTEUDO
102       Call SEPARA
103       Ped_Suf = CONTEUDO
104       Call SEPARA
105       Ped_Acr = CONTEUDO
106       Call SEPARA
107       Ped_Seg = CONTEUDO
108       Call SEPARA
109       Ped_Pc_Icm1 = CONTEUDO
110       Call SEPARA
111       Ped_Pc_Icm2 = CONTEUDO
112       Call SEPARA
113       Ped_Ali = CONTEUDO
114       Call SEPARA
115       Ped_Can = CONTEUDO
116       Call SEPARA
117       Ped_Fl_Ssm = CONTEUDO
118       Call SEPARA
119       Ped_Fl_Nfis = CONTEUDO
120       Call SEPARA
121       Ped_Fl_Pen = CONTEUDO
122       Call SEPARA
123       Ped_Fl_Ace = CONTEUDO
124       Call SEPARA
125       Ped_Fl_Icm = CONTEUDO
126       Call SEPARA
127       Ped_Sit = CONTEUDO
128       Call SEPARA
129       Ped_Pro = CONTEUDO
130       Call SEPARA
131       Ped_Jur = CONTEUDO
132       Call SEPARA
133       Ped_Che = CONTEUDO
134       Call SEPARA
135       Ped_Com = CONTEUDO
136       Call SEPARA
137       Ped_Dup = CONTEUDO
138       Call SEPARA
139       Ped_Lim = CONTEUDO
140       Call SEPARA
141       Ped_Sal = CONTEUDO
142       Call SEPARA
143       Ped_Cli_Nov = CONTEUDO
144       Call SEPARA
145       Ped_Desc = CONTEUDO
146       Call SEPARA
147       Ped_Acre = CONTEUDO
148       Call SEPARA
149       Ped_VlFat = CONTEUDO
150       Call SEPARA
151       Ped_Desc1 = CONTEUDO
152       Call SEPARA
153       Ped_Desc2 = CONTEUDO
154       Call SEPARA
155       Ped_Desc3 = CONTEUDO
156       Call SEPARA
157       Ped_Fret = CONTEUDO
158       Call SEPARA
159       Ped_MensP = CONTEUDO
160       Call SEPARA
161       Ped_MensN = CONTEUDO
162       Call SEPARA
163       Ped_CODVDR = CONTEUDO
          
         '*******************************************
             
         '*******************************************
         'Grava valores lidos na tabela PEDNOTA_VENDA
164       strSQL = ""
165       strSQL = "SELECT * from PEDNOTA_VENDA "
166       strSQL = strSQL & " where COD_LOJA=  " & Ped_Loja & " and "
167       strSQL = strSQL & " num_pedido =  " & Ped_Num & "  and "
168       strSQL = strSQL & " seq_pedido =  " & Ped_Seq & "  "


169       Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)

170       strSQL = ""
          
         'Formata��o das datas
171       If Ped_Dt_Emiss <> "0" Then
172           Ped_Dt_Emiss = Format$(Ped_Dt_Emiss, "dd/mm/yy hh:nn:ss")
173       End If
174       If Ped_Dt_Ssm <> "0" Then
175           Ped_Dt_Ssm = Format$(Ped_Dt_Ssm, "dd/mm/yy hh:nn:ss")
176       End If
177       If Ped_Dt_Pol <> "0" Then
178           Ped_Dt_Pol = Format$(Ped_Dt_Pol, "dd/mm/yy hh:nn:ss")
179       End If
180       If Ped_Dt_Cre <> "0" Then
181           Ped_Dt_Cre = Format$(Ped_Dt_Cre, "dd/mm/yy hh:nn:ss")
182       End If
183       If Ped_Dt_Fre <> "0" Then
184           Ped_Dt_Fre = Format$(Ped_Dt_Fre, "dd/mm/yy hh:nn:ss")
185       End If
          
186       dbLocal.BeginTrans
187       If Not recTab.EOF Then ' J� existe o Pedido - Atualizar
188           strSQL = "UPDATE "
189           strSQL = strSQL & " PEDNOTA_VENDA "
190           strSQL = strSQL & " SET "
191           strSQL = strSQL & " COD_LOJA_NOTA =  " & Ped_Loja_Nota & ", "
192           strSQL = strSQL & " NUM_NOTA = " & Ped_Num_Nota & ", "
193           strSQL = strSQL & " NUM_PENDENTE = " & Val(Mid(Ped_Pen, 3, 11)) & ", "
194           strSQL = strSQL & " COD_FILIAL = " & Ped_Fil & ", "
195           strSQL = strSQL & " TP_PEDIDO = " & Ped_Tp_Ped & ","
196           strSQL = strSQL & " TP_DPKBLAU = " & Ped_Tp_DB & ","
197           strSQL = strSQL & " TP_TRANSACAO = " & Ped_Tp_Tran & ", "
198           strSQL = strSQL & " DT_DIGITACAO = '" & Ped_Dt_Dig & "',"
199           strSQL = strSQL & " DT_PEDIDO = '" & Format$(Ped_Dt_Ped, "DD/MM/YY") & "', "
200           strSQL = strSQL & " DT_EMISSAO_NOTA = '" & Ped_Dt_Emiss & "' ,"
201           strSQL = strSQL & " DT_SSM = '" & Ped_Dt_Ssm & "',"
202           strSQL = strSQL & " DT_BLOQ_POLITICA = '" & Ped_Dt_Pol & "',"
203           strSQL = strSQL & " DT_BLOQ_CREDITO = '" & Ped_Dt_Cre & "',"
204           strSQL = strSQL & " DT_BLOQ_FRETE = '" & Ped_Dt_Fre & "',"
205           strSQL = strSQL & " COD_NOPE= '" & Trim(Ped_Nope) & "',"
206           strSQL = strSQL & " COD_CLIENTE = " & Ped_Cli & ","
207           strSQL = strSQL & " COD_FORNECEDOR = " & Ped_For & ","
208           strSQL = strSQL & " COD_END_ENTREGA = " & Ped_Ent & ","
209           strSQL = strSQL & " COD_END_COBRANCA = " & Ped_Cob & ","
210           strSQL = strSQL & " COD_TRANSP = " & Ped_Tran & ", "
211           strSQL = strSQL & " COD_REPRES = " & Ped_Rep & " ,"
212           strSQL = strSQL & " COD_BANCO = " & Ped_Ban & " ,"
213           strSQL = strSQL & " COD_VEND = " & Ped_Ven & ", "
214           strSQL = strSQL & " COD_PLANO = " & Ped_Pla & ", "
215           strSQL = strSQL & " COD_CFO_DEST = " & Ped_Dest & " ,"
216           strSQL = strSQL & " COD_CFO_OPER = " & Ped_Oper & ", "
217           strSQL = strSQL & " FRETE_PAGO = '" & Ped_Fre & "', "
218           strSQL = strSQL & " PESO_BRUTO = " & Ped_Pes & ", "
219           strSQL = strSQL & " QTD_SSM = " & Ped_Qtd_Ssm & ","
220           strSQL = strSQL & " QTD_ITEM_PEDIDO = " & Ped_Qtd_Ped & ","
221           strSQL = strSQL & " QTD_ITEM_NOTA = " & Ped_Qtd_Not & ","
222           strSQL = strSQL & " VL_CONTABIL = " & Ped_Vl & ","
223           strSQL = strSQL & " VL_IPI = " & Ped_Ipi & ","
224           strSQL = strSQL & " VL_BASEICM1 = " & Ped_Icm1 & ","
225           strSQL = strSQL & " VL_BASEICM2 = " & Ped_Icm2 & ","
226           strSQL = strSQL & " VL_BASE_1 = " & Ped_Bas1 & ", "
227           strSQL = strSQL & " VL_BASE_2 = " & Ped_Bas2 & ","
228           strSQL = strSQL & " VL_BASE_3 = " & Ped_Bas3 & ","
229           strSQL = strSQL & " VL_BASEISEN = " & Ped_Base & ","
230           strSQL = strSQL & " VL_BASE_5 = " & Ped_Bas5 & " ,"
231           strSQL = strSQL & " VL_BASE_6 = " & Ped_Bas6 & ","
232           strSQL = strSQL & " VL_BASEOUTR = " & Ped_Out & " ,"
233           strSQL = strSQL & " VL_BASEMAJ = " & Ped_Maj & ","
234           strSQL = strSQL & " VL_ICMRETIDO= " & Ped_Ret & " ,"
235           strSQL = strSQL & " VL_BASEIPI = " & Ped_Vl_Ipi & " ,"
236           strSQL = strSQL & " VL_BISENIPI = " & Ped_Is_Ipi & " ,"
237           strSQL = strSQL & " VL_BOUTRIPI = " & Ped_Ou_Ipi & ","
238           strSQL = strSQL & " VL_FRETE = " & Ped_Vl_Fre & ","
239           strSQL = strSQL & " VL_DESP_ACESS = " & Ped_Ace & " ,"
240           strSQL = strSQL & " PC_DESCONTO = " & Ped_Des & " ,"
241           strSQL = strSQL & " PC_DESC_SUFRAMA = " & Ped_Suf & " ,"
242           strSQL = strSQL & " PC_ACRESCIMO = " & Ped_Acr & " , "
243           strSQL = strSQL & " PC_SEGURO = " & Ped_Seg & " ,"
244           strSQL = strSQL & " PC_ICM1 = " & Ped_Pc_Icm1 & " ,"
245           strSQL = strSQL & " PC_ICM2 = " & Ped_Pc_Icm2 & ","
246           strSQL = strSQL & " PC_ALIQ_INTERNA = " & Ped_Ali & " ,"
247           strSQL = strSQL & " COD_CANCEL = " & Ped_Can & ","
248           strSQL = strSQL & " FL_GER_SSM = '" & Ped_Fl_Ssm & "' ,"
249           strSQL = strSQL & " FL_GER_NFIS = '" & Ped_Fl_Nfis & "',"
250           strSQL = strSQL & " FL_PENDENCIA = '" & Ped_Fl_Pen & "' ,"
251           strSQL = strSQL & " FL_DESP_ACESS = '" & Ped_Fl_Ace & "',"
252           strSQL = strSQL & " FL_DIF_ICM = '" & Ped_Fl_Icm & "',"
253           strSQL = strSQL & " SITUACAO = " & Ped_Sit & " ,"
254           strSQL = strSQL & " BLOQ_PROTESTO = '" & Ped_Pro & "' ,"
255           strSQL = strSQL & " BLOQ_JUROS = '" & Ped_Jur & "',"
256           strSQL = strSQL & " BLOQ_CHEQUE = '" & Ped_Che & "',"
257           strSQL = strSQL & " BLOQ_COMPRA = '" & Ped_Com & "' ,"
258           strSQL = strSQL & " BLOQ_DUPLICATA = '" & Ped_Dup & "',"
259           strSQL = strSQL & " BLOQ_LIMITE ='" & Ped_Lim & "' ,"
260           strSQL = strSQL & " BLOQ_SALDO = '" & Ped_Sal & "',"
261           strSQL = strSQL & " BLOQ_CLIE_NOVO = '" & Ped_Cli_Nov & "', "
262           strSQL = strSQL & " BLOQ_DESCONTO = '" & Ped_Desc & "', "
263           strSQL = strSQL & " BLOQ_ACRESCIMO = '" & Ped_Acre & "', "
264           strSQL = strSQL & " BLOQ_VLFATMIN = '" & Ped_VlFat & "', "
265           strSQL = strSQL & " BLOQ_ITEM_DESC1 = '" & Ped_Desc1 & "' ,"
266           strSQL = strSQL & " BLOQ_ITEM_DESC2 = '" & Ped_Desc2 & "' ,"
267           strSQL = strSQL & " BLOQ_ITEM_DESC3 = '" & Ped_Desc3 & "',"
268           strSQL = strSQL & " BLOQ_FRETE = '" & Ped_Fret & "',"
269           strSQL = strSQL & " MENS_PEDIDO = '" & Ped_MensP & "' ,"
270           strSQL = strSQL & " MENS_NOTA = '" & Ped_MensN & "' ,"
271           strSQL = strSQL & " COD_VDR = " & Ped_CODVDR & " "
272           strSQL = strSQL & " WHERE COD_LOJA = " & Ped_Loja & " AND "
273           strSQL = strSQL & " NUM_PEDIDO = " & Ped_Num & " And "
274           strSQL = strSQL & " SEQ_PEDIDO = " & Ped_Seq
          
             'MsgBox strSQL
            
275           dbLocal.Execute strSQL
          
276       Else    'N�o Existe o Pedido - Incluir
          
277           strSQL = "INSERT "
278           strSQL = strSQL & " into PEDNOTA_VENDA values "
279           strSQL = strSQL & " (" & Ped_Loja & ", " & Ped_Num & ", " & Ped_Seq & " , "
280           strSQL = strSQL & " " & Ped_Loja_Nota & " , " & Ped_Num_Nota & ", "
281           strSQL = strSQL & " " & Val(Mid(Ped_Pen, 3, 11)) & ", " & Ped_Fil & " , " & Ped_Tp_Ped & " ,"
282           strSQL = strSQL & " " & Ped_Tp_DB & " , " & Ped_Tp_Tran & ", '" & Ped_Dt_Dig & "', "
283           strSQL = strSQL & " '" & Ped_Dt_Ped & "' , '" & Ped_Dt_Emiss & "' , '" & Ped_Dt_Ssm & "' , "
284           strSQL = strSQL & " '" & Ped_Dt_Pol & "' , '" & Ped_Dt_Cre & "','" & Ped_Dt_Fre & "',"
285           strSQL = strSQL & " '" & Ped_Nope & "' , " & Ped_Cli & ", " & Ped_For & " ,"
286           strSQL = strSQL & " " & Ped_Ent & " , " & Ped_Cob & ", " & Ped_Tran & ","
287           strSQL = strSQL & " " & Ped_Rep & ", " & Ped_Ven & " , " & Ped_Pla & ", "
288           strSQL = strSQL & " " & Ped_Ban & ", " & Ped_Dest & ", " & Ped_Oper & " ,"
289           strSQL = strSQL & " '" & Ped_Fre & "', " & Ped_Pes & ", " & Ped_Qtd_Ssm & " , "
290           strSQL = strSQL & " " & Ped_Qtd_Ped & ", " & Ped_Qtd_Not & ", " & Ped_Vl & " ,"
291           strSQL = strSQL & " " & Ped_Ipi & ", " & Ped_Icm1 & ", " & Ped_Icm2 & ", "
292           strSQL = strSQL & " " & Ped_Bas1 & ", " & Ped_Bas2 & " , "
293           strSQL = strSQL & " " & Ped_Bas3 & " , " & Ped_Base & " , " & Ped_Bas5 & " ,"
294           strSQL = strSQL & " " & Ped_Bas6 & " , " & Ped_Out & " , " & Ped_Maj & ","
295           strSQL = strSQL & " " & Ped_Ret & ", " & Ped_Vl_Ipi & ", " & Ped_Is_Ipi & " ,"
296           strSQL = strSQL & " " & Ped_Ou_Ipi & ", " & Ped_Vl_Fre & " , " & Ped_Ace & " ,"
297           strSQL = strSQL & " " & Ped_Des & " , " & Ped_Suf & ", " & Ped_Acr & " ,"
298           strSQL = strSQL & " " & Ped_Seg & ", " & Ped_Pc_Icm1 & " , " & Ped_Pc_Icm2 & " , "
299           strSQL = strSQL & " " & Ped_Ali & " , " & Ped_Can & " , '" & Ped_Fl_Ssm & "' , "
300           strSQL = strSQL & " '" & Ped_Fl_Nfis & "' , '" & Ped_Fl_Pen & "' , '" & Ped_Fl_Ace & "' ,"
301           strSQL = strSQL & " '" & Ped_Fl_Icm & "' , " & Ped_Sit & " , '" & Ped_Pro & "' ,"
302           strSQL = strSQL & " '" & Ped_Jur & "', '" & Ped_Che & "',  '" & Ped_Com & "', '" & Ped_Dup & "',"
303           strSQL = strSQL & " '" & Ped_Lim & "' , '" & Ped_Sal & "' , '" & Ped_Cli_Nov & "', "
304           strSQL = strSQL & " '" & Ped_Desc & "', '" & Ped_Acre & "' , '" & Ped_VlFat & "' ,"
305           strSQL = strSQL & " '" & Ped_Desc1 & "' , '" & Ped_Desc2 & "' , '" & Ped_Desc3 & "' ,"
306           strSQL = strSQL & " '" & Ped_Fret & "' , '" & Ped_MensP & "' , '" & Ped_MensN & "', " & Ped_CODVDR & ")"
           
307           dbLocal.Execute strSQL
           
           
308       End If
309       dbLocal.CommitTrans
          
         'SE HOUVER PEDIDO DE TRANSFER�NCIA
310       strSQL = "Select * from R_COTACAO_TRANSF where COD_LOJA_COTACAO = " & Val(Mid(Ped_Pen, 1, 2)) & " AND " & _
                   "NUM_COTACAO_TEMP = " & Val(Mid(Ped_Pen, 3, 11))
          
311       Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
              
          Dim ped_cotacao
          
312       If Not recTab.EOF Then
313           dbLocal.BeginTrans
314           If Ped_Nope = "A30" Then
              
315               ped_cotacao = Mid(Ped_MensN, InStr(Ped_MensN, ":") + 2)
                  
                  
                 'n�mero do pedido de transfer�ncia
316               strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_TRANSF =" & Ped_Loja & ", " & _
                           "NUM_PEDIDO =" & Ped_Num & ", SEQ_PEDIDO =" & Ped_Seq & ", " & _
                           "NUM_COTACAO = " & ped_cotacao & " where " & _
                           "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                           "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                           
317               dbLocal.Execute strSQL
318           Else
                 'n�mero do pedido de venda
319               strSQL = "Update R_COTACAO_TRANSF set COD_LOJA_VENDA =" & Ped_Loja & ", " & _
                           "NUM_PEDIDO_VENDA =" & Ped_Num & ", SEQ_PEDIDO_VENDA =" & Ped_Seq & " where " & _
                           "COD_LOJA_COTACAO = " & Mid(Ped_Pen, 1, 2) & " AND " & _
                           "NUM_COTACAO_TEMP = " & Mid(Ped_Pen, 3, 11)
                           
320               dbLocal.Execute strSQL
321           End If
322           dbLocal.CommitTrans
323       End If
          
      '*********************************************

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub ATU_PED" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl & "StrSql:" & strSQL
    End If

End Function

Public Function ATU_VDR_CONTROLE_VDR() As Boolean
1     On Error GoTo VerErro
          
2       vRegs = 0
        
3         strSQL = "SELECT * INTO AUX_VDR_CONTROLE_VDR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_CONTROLE_VDR "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE VDR_CONTROLE_VDR INNER JOIN AUX_VDR_CONTROLE_VDR ON " & _
                   " VDR_CONTROLE_VDR.COD_FORNECEDOR = AUX_VDR_CONTROLE_VDR.COD_FORNECEDOR AND " & _
                   " VDR_CONTROLE_VDR.COD_GRUPO = AUX_VDR_CONTROLE_VDR.COD_GRUPO AND " & _
                   " VDR_CONTROLE_VDR.COD_SUBGRUPO = AUX_VDR_CONTROLE_VDR.COD_SUBGRUPO AND " & _
                   " VDR_CONTROLE_VDR.COD_TIPO_CLIENTE = AUX_VDR_CONTROLE_VDR.COD_TIPO_CLIENTE " & _
                   " SET VDR_CONTROLE_VDR.COD_VDR = AUX_VDR_CONTROLE_VDR.COD_VDR, " & _
                   " VDR_CONTROLE_VDR.LINHA_PRODUTO = AUX_VDR_CONTROLE_VDR.LINHA_PRODUTO "
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO VDR_CONTROLE_VDR SELECT * FROM AUX_VDR_CONTROLE_VDR "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
         'Deleta do Banco atual a tabela temporaria ...........................
11        dbLocal.Execute "DROP TABLE AUX_VDR_CONTROLE_VDR "
                  
12        ATU_VDR_CONTROLE_VDR = True

13        Controle "VDR_CONT", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_VDR_CONTROLE_VDR"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_VDR_CONTROLE_VDR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function

Public Function ATU_VDR_DESCONTO_CATEG() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_VDR_DESCONTO_CATEG IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_DESCONTO_CATEG "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE VDR_DESCONTO_CATEG INNER JOIN AUX_VDR_DESCONTO_CATEG ON " & _
                   " VDR_DESCONTO_CATEG.CATEGORIA = AUX_VDR_DESCONTO_CATEG.CATEGORIA  " & _
                   " SET VDR_DESCONTO_CATEG.PC_DESC = AUX_VDR_DESCONTO_CATEG.PC_DESC "
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO VDR_DESCONTO_CATEG SELECT * FROM AUX_VDR_DESCONTO_CATEG "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_VDR_DESCONTO_CATEG "
                  
12        ATU_VDR_DESCONTO_CATEG = True

13        Controle "VDR_DESC", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_VDR_DESCONTO_CATEG"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_VDR_DESCONTO_CATEG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function

Public Function ATU_VDR_ITEM_PRECO() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_VDR_ITEM_PRECO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_ITEM_PRECO "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE VDR_ITEM_PRECO INNER JOIN AUX_VDR_ITEM_PRECO ON " & _
                   " VDR_ITEM_PRECO.COD_LOJA = AUX_VDR_ITEM_PRECO.COD_LOJA AND " & _
                   " VDR_ITEM_PRECO.COD_DPK = AUX_VDR_ITEM_PRECO.COD_DPK " & _
                   " SET VDR_ITEM_PRECO.PRECO_VENDA = AUX_VDR_ITEM_PRECO.PRECO_VENDA, " & _
                   " VDR_ITEM_PRECO.PRECO_VENDA_ANT = AUX_VDR_ITEM_PRECO.PRECO_VENDA_ANT "
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO VDR_ITEM_PRECO SELECT * FROM AUX_VDR_ITEM_PRECO "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_VDR_ITEM_PRECO "
                  
12        ATU_VDR_ITEM_PRECO = True

13        Controle "VDR_ITEM", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_VDR_ITEM_PRECO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_VDR_ITEM_PRECO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If
End Function


Public Function ATU_VDR_R_CLIE_LINHA_PRODUTO() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_VDR_R_CLIE_LINHA_PRODUTO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_R_CLIE_LINHA_PRODUTO "
4         dbExt.Execute strSQL
          
5         strSQL = "delete * from VDR_R_CLIE_LINHA_PRODUTO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO VDR_R_CLIE_LINHA_PRODUTO SELECT * FROM AUX_VDR_R_CLIE_LINHA_PRODUTO "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_VDR_R_CLIE_LINHA_PRODUTO "
                  
12        ATU_VDR_R_CLIE_LINHA_PRODUTO = True

13        Controle "VDR_RCLI", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_VDR_R_CLIE_LINHA_PRODUTO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_VDR_CLIE_LINHA_PRODUTO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If
End Function


Public Function ATU_VDR_TABELA_VENDA() As Boolean
1     On Error GoTo VerErro
          
          vRegs = 0

2         strSQL = "SELECT * INTO AUX_VDR_TABELA_VENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VDR_TABELA_VENDA "
3         dbExt.Execute strSQL
          
4         strSQL = "UPDATE VDR_TABELA_VENDA INNER JOIN AUX_VDR_TABELA_VENDA ON " & _
                   " VDR_TABELA_VENDA.DIVISAO = AUX_VDR_TABELA_VENDA.DIVISAO AND " & _
                   " VDR_TABELA_VENDA.TP_TABELA = AUX_VDR_TABELA_VENDA.TP_TABELA AND " & _
                   " VDR_TABELA_VENDA.OCORR_PRECO = AUX_VDR_TABELA_VENDA.OCORR_PRECO " & _
                   " SET VDR_TABELA_VENDA.TABELA_VENDA = AUX_VDR_TABELA_VENDA.TABELA_VENDA, " & _
                   " VDR_TABELA_VENDA.DT_VIGENCIA1 = AUX_VDR_TABELA_VENDA.DT_VIGENCIA1, " & _
                   " VDR_TABELA_VENDA.DT_VIGENCIA2 = AUX_VDR_TABELA_VENDA.DT_VIGENCIA2, " & _
                   " VDR_TABELA_VENDA.DT_VIGENCIA3 = AUX_VDR_TABELA_VENDA.DT_VIGENCIA3, " & _
                   " VDR_TABELA_VENDA.DT_VIGENCIA4 = AUX_VDR_TABELA_VENDA.DT_VIGENCIA4, " & _
                   " VDR_TABELA_VENDA.DT_VIGENCIA5 = AUX_VDR_TABELA_VENDA.DT_VIGENCIA5 "
          
5         dbLocal.Execute strSQL
          vRegs = dbLocal.RecordsAffected
                
6         strSQL = "INSERT INTO VDR_TABELA_VENDA SELECT * FROM AUX_VDR_TABELA_VENDA "
7         dbLocal.Execute strSQL
          vRegs = vRegs + dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_VDR_TABELA_VENDA "
                  
9         ATU_VDR_TABELA_VENDA = True

          Controle "VDR_TABE", vRegs

10    Exit Function

VerErro:

11        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
12            dbLocal.Execute "DROP TABLE AUX_VDR_TABELA_VENDA"
13            Resume
14        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
15            Resume Next
          ElseIf Err.Number <> 0 Then
              MsgBox "Sub ATU_VDR_TABELA_VENDA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
16        End If

End Function

Public Function CHAVE_PED()
   'Primeira linha do arquivo guarda a chave da tabela
   'PEDNOTA_VENDA: COD_LOJA + NUM_PEDIDO + SEQ_PEDIDO
    
    Call SEPARA
    Ped_Loja = CONTEUDO
    Call SEPARA
    Ped_Num = CONTEUDO
    Call SEPARA
    Ped_Seq = CONTEUDO

   'MsgBox " pedido: " & Ped_Loja & "  " & Ped_Num & "-" & Ped_Seq
End Function

Public Function DEL_R_DPK_EQUIV() As Boolean
1         On Error GoTo VerErro
          
          vRegs = 0
2         strSQL = "SELECT * INTO AUX_DEL_R_DPK_EQUIV IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_R_DPK_EQUIV "
3         dbExt.Execute strSQL
          
4         strSQL = " DELETE R_DPK_EQUIV.* FROM R_DPK_EQUIV INNER JOIN " & _
                   " AUX_DEL_R_DPK_EQUIV ON " & _
                   " R_DPK_EQUIV.COD_DPK = AUX_DEL_R_DPK_EQUIV.COD_DPK AND " & _
                   " R_DPK_EQUIV.COD_DPK_EQ = AUX_DEL_R_DPK_EQUIV.COD_DPK_EQ "
5         dbLocal.Execute strSQL
          vRegs = vRegs + dbLocal.RecordsAffected
                 
         'Deleta do Banco atual a tabela temporaria ...........................
6         dbLocal.Execute "DROP TABLE AUX_DEL_R_DPK_EQUIV"
                  
7         DEL_R_DPK_EQUIV = True
          
          Controle "DEL_RDEQ", vRegs
          
8     Exit Function
          
VerErro:
          
9         If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
10            dbLocal.Execute "DROP TABLE AUX_DEL_R_DPK_EQUIV "
11            Resume
12        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
13            Resume Next
14        Else
15            DEL_R_DPK_EQUIV = False
16            Screen.MousePointer = 1
              MsgBox "Sub DEL_R_DPK_EQUIV - HF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
18        End If
End Function

Public Function DEL_R_UF_DEPOSITO() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DEL_R_UF_DEPOSITO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_R_UF_DEPOSITO "
4         dbExt.Execute strSQL
          
5         strSQL = " DELETE R_UF_DEPOSITO.* FROM R_UF_DEPOSITO INNER JOIN " & _
                   " AUX_DEL_R_UF_DEPOSITO ON " & _
                   " R_UF_DEPOSITO.COD_UF = AUX_DEL_R_UF_DEPOSITO.COD_UF AND " & _
                   " R_UF_DEPOSITO.COD_LOJA = AUX_DEL_R_UF_DEPOSITO.COD_LOJA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
                 
8         dbLocal.Execute "DROP TABLE AUX_DEL_R_UF_DEPOSITO"
                  
9         DEL_R_UF_DEPOSITO = True
          
10        Controle "DEL_UFDP", vRegs
          
11    Exit Function
          
VerErro:
          
12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_R_UF_DEPOSITO "
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_R_UF_DEPOSITO = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_R_UF_DEPOSITO - KK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If
End Function

Public Function DEL_R_CLIE_REPRES() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_DEL_R_CLIE_REPRES IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_R_CLIE_REPRES "
4         dbExt.Execute strSQL
                
5         strSQL = " DELETE R_CLIE_REPRES.* FROM R_CLIE_REPRES INNER JOIN " & _
                   " AUX_DEL_R_CLIE_REPRES ON " & _
                   " R_CLIE_REPRES.COD_CLIENTE = AUX_DEL_R_CLIE_REPRES.COD_CLIENTE AND " & _
                   " R_CLIE_REPRES.COD_REPRES = AUX_DEL_R_CLIE_REPRES.COD_REPRES "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_R_CLIE_REPRES"
                  
9         DEL_R_CLIE_REPRES = True
          
10        Controle "DEL_RCLI", vRegs
          
11    Exit Function
          
VerErro:
          
12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_R_CLIE_REPRES "
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_R_CLIE_REPRES = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_R_CLIE_REPRES - JN" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If
End Function
Public Function DEL_VDR_CLIENTE_CATEG_VDR() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_DEL_VDR_CLIENTE_CATEG_VDR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_CLIENTE_CATEG_VDR "
4         dbExt.Execute strSQL
                
5         strSQL = " DELETE VDR_CLIENTE_CATEG_VDR.* FROM VDR_CLIENTE_CATEG_VDR INNER JOIN " & _
                   " AUX_DEL_VDR_CLIENTE_CATEG_VDR ON " & _
                   " VDR_CLIENTE_CATEG_VDR.COD_CLIENTE = AUX_DEL_VDR_CLIENTE_CATEG_VDR.COD_CLIENTE "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_VDR_CLIENTE_CATEG_VDR"
                  
9         DEL_VDR_CLIENTE_CATEG_VDR = True
          
10        Controle "DEL_VDR", vRegs
          
11    Exit Function
          
VerErro:
          
12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_VDR_CLIENTE_CATEG_VDR "
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_VDR_CLIENTE_CATEG_VDR = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_VDR_CLIENTE_CATEG_VDR - GX" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function

Public Function DEL_VDR_R_CLIE_LINHA_PRODUTO() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_VDR_R_CLIE_LINHA_PRODUTO "
4         dbExt.Execute strSQL
                
5         strSQL = " DELETE VDR_R_CLIE_LINHA_PRODUTO.* FROM VDR_R_CLIE_LINHA_PRODUTO INNER JOIN " & _
                   " AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO ON " & _
                   " VDR_R_CLIE_LINHA_PRODUTO.COD_CLIENTE = AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO.COD_CLIENTE AND " & _
                   " VDR_R_CLIE_LINHA_PRODUTO.LINHA_PRODUTO = AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO.LINHA_PRODUTO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO"
                  
9         DEL_VDR_R_CLIE_LINHA_PRODUTO = True
          
10        Controle "DEL_RCLI", vRegs
          
11    Exit Function
          
VerErro:
          
12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_VDR_R_CLIE_LINHA_PRODUTO "
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_VDR_R_CLIE_LINHA_PRODUTO = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_VDR_R_CLIE_LINHA_PRODUTO - GX" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function


Public Function SEPARA()

    On Error GoTo Trata_Erro

1         POS_SEP = InStr(TMP_LINHA, "|")
         'PED_LOJA=LEFT(TMP_LINHA,POS_SEP-1)

2         CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
3         If UCase(CONTEUDO) = "PED" Then
4             TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
5             POS_SEP = InStr(TMP_LINHA, "|")
6             CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
7         End If
          
8         If UCase(CONTEUDO) = "ITE" Then
9             TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
10            POS_SEP = InStr(TMP_LINHA, "|")
11            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
12        End If
          
13        If UCase(CONTEUDO) = "ROM" Then
14            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
15            POS_SEP = InStr(TMP_LINHA, "|")
16            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
17        End If
          
18        If UCase(CONTEUDO) = "V_P" Then
19            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
20            POS_SEP = InStr(TMP_LINHA, "|")
21            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
22        End If
          
23        If UCase(CONTEUDO) = "R_P" Then
24            TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)
25            POS_SEP = InStr(TMP_LINHA, "|")
26            CONTEUDO = Left(TMP_LINHA, POS_SEP - 1)
27        End If
          
         'MsgBox TMP_LINHA
          
28        If CONTEUDO = "" Then
29            CONTEUDO = 0
30        End If
          
31        TMP_LINHA = Mid(TMP_LINHA, POS_SEP + 1)

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub SEPARA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Function
Public Sub COMPACTA_BANCO()

    On Error GoTo Trata_Erro

1         arqmdb = Dir(Path_drv & "\DADOS\BASE_DPK.MDB")
2         dir_base = Path_drv & "\DADOS\"
         'arqmdb = Path_drv &"\COM\ENTRADA\ARQUIVOS\" & arqmdb
          
3         mdiGER040.Label1.Caption = "COMPACTANDO BANCO = " & Mid(arqmdb, 1, 4)
4         DBEngine.CompactDatabase dir_base & Mid(arqmdb, 1, 8) & ".MDB", dir_base & "temp.mdb", "", dbEncrypt
5         Kill dir_base & Mid(arqmdb, 1, 8) & ".MDB"
6         Name dir_base & "temp.mdb" As dir_base & Mid(arqmdb, 1, 8) & ".MDB"
7         arqmdb = Dir

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub COMPACTA_BANCO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Function ATU_CLIENTE_ACUMULADO() As Boolean
          Dim DTFATU As String
          Dim DTINIDIA As String
          Dim CLIENTE As Long
          Dim MES1 As String
          Dim VL_1 As Double
          Dim MES2 As String
          Dim VL_2 As Double
          Dim MES3 As String
          Dim VL_3 As Double
          Dim MES4 As String
          Dim VL_4 As Double

1         On Error GoTo VerErro

         '-------------------------------------------------------------
          'Alterado por Eduardo Relvas
          'Alterado em 21/01/05
          'Objetivo:
          '    Atualizar o campo Valor1=0 quando o campo Valor1 for nulo
          '    Atualizar o campo Valor2=0 quando o campo Valor2 for nulo
          '    Atualizar o campo Valor3=0 quando o campo Valor3 for nulo
          '    Atualizar o campo Valor4=0 quando o campo Valor4 for nulo
          '-------------------------------------------------------------
          
2         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR1 = 0 " & _
                   "WHERE VALOR1 IS NULL"
3         dbLocal.Execute strSQL

4         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR2 = 0 " & _
                   "WHERE VALOR2 IS NULL"
5         dbLocal.Execute strSQL

6         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR3 = 0 " & _
                   "WHERE VALOR3 IS NULL"
7         dbLocal.Execute strSQL

8         strSQL = "UPDATE CLIENTE_ACUMULADO " & _
                   "SET VALOR4 = 0 " & _
                   "WHERE VALOR4 IS NULL"
9         dbLocal.Execute strSQL

          
10        strSQL = "SELECT dt_faturamento, dt_ini_fech_dia FROM DATAS"
          
11        Set ss = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
          
12        DTFATU = Format(ss!DT_FATURAMENTO, "dd/mm/yy")
13        DTINIDIA = Format(ss!DT_INI_FECH_DIA, "dd/mm/yy")
          
14        If Mid(DTFATU, 4, 2) <> Mid(DTINIDIA, 4, 2) Then
15            strSQL = "SELECT * FROM cliente_acumulado"
              
16            Set ss = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
              
17            Do While Not ss.EOF
18                CLIENTE = ss!cod_cliente
19                MES1 = Mid(DTFATU, 4, 2) & "-" & Mid(Format(DTFATU, "DD/MM/YYYY"), 7, 4)
20                VL_1 = 0
21                MES2 = ss!ANO_MES1
22                VL_2 = IIf(IsNull(ss!valor1), 0, ss!valor1)
23                MES3 = ss!ANO_MES2
24                VL_3 = IIf(IsNull(ss!valor2), 0, ss!valor2)
25                MES4 = ss!ANO_MES3
26                VL_4 = IIf(IsNull(ss!valor3), 0, ss!valor3)
              
27                strSQL = "UPDATE cliente_acumulado " & _
                           "SET ANO_MES1 = '" & MES1 & "', VALOR1 = " & VL_1 & ", " & _
                           "ANO_MES2 = '" & MES2 & "', VALOR2 = " & VL_2 & ", " & _
                           "ANO_MES3 = '" & MES3 & "', VALOR3 = " & VL_3 & ", " & _
                           "ANO_MES4 = '" & MES4 & "', VALOR4 = " & VL_4 & " " & _
                           "WHERE COD_CLIENTE = " & CLIENTE

28                dbLocal.Execute strSQL
              
29                ss.MoveNext
30            Loop
31        End If

32        vRegs = 0
33        strSQL = "SELECT * INTO AUX_CLIENTE_ACUMULADO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIENTE_ACUMULADO "
34        dbExt.Execute strSQL
            
35        If FL_TIPO = "D" Then
36             strSQL = "UPDATE CLIENTE_ACUMULADO INNER JOIN AUX_CLIENTE_ACUMULADO ON  " & _
                        " CLIENTE_ACUMULADO.COD_CLIENTE = AUX_CLIENTE_ACUMULADO.COD_CLIENTE " & _
                        " SET CLIENTE_ACUMULADO.ANO_MES1 = AUX_CLIENTE_ACUMULADO.ANO_MES1, " & _
                        " CLIENTE_ACUMULADO.VALOR1 = AUX_CLIENTE_ACUMULADO.VALOR1, " & _
                        " CLIENTE_ACUMULADO.ANO_MES2 = AUX_CLIENTE_ACUMULADO.ANO_MES2, " & _
                        " CLIENTE_ACUMULADO.ANO_MES3 = AUX_CLIENTE_ACUMULADO.ANO_MES3, " & _
                        " CLIENTE_ACUMULADO.ANO_MES4 = AUX_CLIENTE_ACUMULADO.ANO_MES4 "
37        Else
38             strSQL = "UPDATE CLIENTE_ACUMULADO INNER JOIN AUX_CLIENTE_ACUMULADO ON  " & _
                        " CLIENTE_ACUMULADO.COD_CLIENTE = AUX_CLIENTE_ACUMULADO.COD_CLIENTE " & _
                        " SET CLIENTE_ACUMULADO.ANO_MES1 = AUX_CLIENTE_ACUMULADO.ANO_MES1, " & _
                        " CLIENTE_ACUMULADO.VALOR1 = AUX_CLIENTE_ACUMULADO.VALOR1, " & _
                        " CLIENTE_ACUMULADO.ANO_MES2 = AUX_CLIENTE_ACUMULADO.ANO_MES2, " & _
                        " CLIENTE_ACUMULADO.VALOR2 = AUX_CLIENTE_ACUMULADO.VALOR2, " & _
                        " CLIENTE_ACUMULADO.ANO_MES3 = AUX_CLIENTE_ACUMULADO.ANO_MES3, " & _
                        " CLIENTE_ACUMULADO.VALOR3 = AUX_CLIENTE_ACUMULADO.VALOR3, " & _
                        " CLIENTE_ACUMULADO.ANO_MES4 = AUX_CLIENTE_ACUMULADO.ANO_MES4, " & _
                        " CLIENTE_ACUMULADO.VALOR4 = AUX_CLIENTE_ACUMULADO.VALOR4 "
39        End If
          
40        dbLocal.Execute strSQL
41        vRegs = dbLocal.RecordsAffected
          
42        strSQL = "INSERT INTO CLIENTE_ACUMULADO " & _
                   "SELECT COD_CLIENTE, ANO_MES1, VALOR1, ANO_MES2, ANO_MES3, ANO_MES4 " & _
                   "FROM AUX_CLIENTE_ACUMULADO "

43        dbLocal.Execute strSQL
44        vRegs = dbLocal.RecordsAffected
          
45        dbLocal.Execute "DROP TABLE AUX_CLIENTE_ACUMULADO"
              
46        ATU_CLIENTE_ACUMULADO = True

47        Controle "CLIEACUM", vRegs

48    Exit Function

VerErro:

49        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
50            dbLocal.Execute "DROP TABLE AUX_CLIENTE_ACUMULADO"
51            Resume
52        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
53            Resume Next
54        Else
55            ATU_CLIENTE_ACUMULADO = False
56            Screen.MousePointer = 1
57            MsgBox "Sub ATU_CLIENTE_ACUMULADO - BC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
58        End If

End Function
Public Function ATU_CLASS_ANTEC_ENTRADA() As Boolean

1         On Error GoTo VerErro

2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_CLASS_ANTEC IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLASS_ANTEC_ENTRADA "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE CLASS_ANTEC_ENTRADA INNER JOIN AUX_CLASS_ANTEC ON  " & _
                   " CLASS_ANTEC_ENTRADA.CLASS_FISCAL = AUX_CLASS_ANTEC.CLASS_FISCAL AND " & _
                   " CLASS_ANTEC_ENTRADA.COD_UF_ORIGEM = AUX_CLASS_ANTEC.COD_UF_ORIGEM AND " & _
                   " CLASS_ANTEC_ENTRADA.COD_UF_DESTINO = AUX_CLASS_ANTEC.COD_UF_DESTINO " & _
                   " SET CLASS_ANTEC_ENTRADA.COD_TRIB = AUX_CLASS_ANTEC.COD_TRIB, " & _
                   " CLASS_ANTEC_ENTRADA.PC_MARGEM_LUCRO = AUX_CLASS_ANTEC.PC_MARGEM_LUCRO"
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CLASS_ANTEC_ENTRADA SELECT * FROM AUX_CLASS_ANTEC"

9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CLASS_ANTEC"
              
12        ATU_CLASS_ANTEC_ENTRADA = True
            
13        Controle "CLASSANT", vRegs
          
14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CLASS_ANTEC"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CLASS_ANTEC_ENTRADA = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_CLASS_ANTEC_ENTRADA - KL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_ANTECIPACAO_TRIBUTARIA() As Boolean

1     On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_ANTECIPACAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ANTECIPACAO_TRIBUTARIA "
4         dbExt.Execute strSQL
            
5         strSQL = "delete * from ANTECIPACAO_TRIBUTARIA "
6         dbLocal.Execute strSQL

7         strSQL = "INSERT INTO ANTECIPACAO_TRIBUTARIA SELECT * FROM AUX_ANTECIPACAO"
8         dbLocal.Execute strSQL
9         vRegs = dbLocal.RecordsAffected
                    
10        dbLocal.Execute "DROP TABLE AUX_ANTECIPACAO"
              
11        ATU_ANTECIPACAO_TRIBUTARIA = True

12        Controle "ANTECIPA", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_ANTECIPACAO"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            ATU_ANTECIPACAO_TRIBUTARIA = False
21            Screen.MousePointer = 1
22            MsgBox "Sub ATU_ANTECIPACAO_TRIBUTARIA - IQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If

End Function
Public Function ATU_RESULTADO() As Boolean

1         On Error GoTo VerErro

2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_RESULTADO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM RESULTADO "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE RESULTADO INNER JOIN AUX_RESULTADO ON  " & _
                   " RESULTADO.COD_RESULTADO = AUX_RESULTADO.COD_RESULTADO  " & _
                   " SET RESULTADO.DESC_RESULTADO = AUX_RESULTADO.DESC_RESULTADO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
             
8         strSQL = "INSERT INTO RESULTADO SELECT * FROM AUX_RESULTADO"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_RESULTADO"
              
12        ATU_RESULTADO = True

13        Controle "RESULTAD", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_RESULTADO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_RESULTADO = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_RESULTADO - FN" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function
Public Function ATU_APLICACAO() As Boolean

1         On Error GoTo VerErro

2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_APLIC IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM APLICACAO "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE APLICACAO INNER JOIN AUX_APLIC ON APLICACAO.COD_DPK = AUX_APLIC.COD_DPK  " & _
                   " AND APLICACAO.COD_CATEGORIA = AUX_APLIC.COD_CATEGORIA  " & _
                   " AND APLICACAO.COD_MONTADORA = AUX_APLIC.COD_MONTADORA " & _
                   " AND APLICACAO.SEQUENCIA = AUX_APLIC.SEQUENCIA " & _
                   " SET APLICACAO.COD_ORIGINAL = AUX_APLIC.COD_ORIGINAL , APLICACAO.DESC_APLICACAO = AUX_APLIC.DESC_APLICACAO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO APLICACAO SELECT * FROM AUX_APLIC"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_APLIC"
              
12        ATU_APLICACAO = True

13        Controle "APLICACA", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_APLIC"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_APLICACAO = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_APLICACAO - AW" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function
Public Function ATU_R_DPK_EQUIV() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_R_DPK_EQUIV IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_DPK_EQUIV "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE R_DPK_EQUIV INNER JOIN AUX_R_DPK_EQUIV " & _
               " ON R_DPK_EQUIV.COD_DPK = AUX_R_DPK_EQUIV.COD_DPK " & _
               " SET R_DPK_EQUIV.COD_DPK_EQ = AUX_R_DPK_EQUIV.COD_DPK_EQ "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO R_DPK_EQUIV SELECT * FROM AUX_R_DPK_EQUIV"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
      
11    dbLocal.Execute "DROP TABLE AUX_R_DPK_EQUIV"
              
12    ATU_R_DPK_EQUIV = True

13    Controle "R_DPK_EQ", vRegs
      
14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_R_DPK_EQUIV"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_R_DPK_EQUIV = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_R_DPK_EQUIV - FK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function


Public Function ATU_PLANO() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_PLANO_PGTO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM PLANO_PGTO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE PLANO_PGTO INNER JOIN AUX_PLANO_PGTO ON PLANO_PGTO.COD_PLANO = AUX_PLANO_PGTO.COD_PLANO " & _
              " SET PLANO_PGTO.DESC_PLANO = AUX_PLANO_PGTO.DESC_PLANO, " & _
              " PLANO_PGTO.PRAZO_MEDIO = AUX_PLANO_PGTO.PRAZO_MEDIO , " & _
              " PLANO_PGTO.PC_ACRES_FIN_DPK = AUX_PLANO_PGTO.PC_ACRES_FIN_DPK, " & _
              " PLANO_PGTO.PC_DESC_FIN_DPK = AUX_PLANO_PGTO.PC_DESC_FIN_DPK, " & _
              " PLANO_PGTO.SITUACAO = AUX_PLANO_PGTO.SITUACAO, " & _
              " PLANO_PGTO.FL_AVISTA = AUX_PLANO_PGTO.FL_AVISTA, " & _
              " PLANO_PGTO.PC_DESC_FIN_VDR = AUX_PLANO_PGTO.PC_DESC_FIN_VDR, " & _
              " PLANO_PGTO.PC_ACRES_FIN_BLAU = AUX_PLANO_PGTO.PC_ACRES_FIN_BLAU "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO PLANO_PGTO SELECT * FROM AUX_PLANO_PGTO "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
      
11    dbLocal.Execute "DROP TABLE AUX_PLANO_PGTO "

12    ATU_PLANO = True

13    Controle "PLANO_PG", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_PLANO_PGTO"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_PLANO = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_PLANO - BM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function


Public Sub COMPACTA_AUXILIAR()
          
    On Error GoTo Trata_Erro
          
          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD") = Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD" Then
2             Kill Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq, Path_drv & "\COM\ENTRADA\ARQUIVOS\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq
6         VDestino = Path_drv & "\COM\ENTRADA\ARQUIVOS\OLD\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & Mid(nomeArq, 1, InStr(nomeArq, ".")) & ".NEW"
9         VDestino = Path_drv & "\COM\ENTRADA\ARQUIVOS\" & nomeArq
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub COMPACTA_AUXILIAR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub

Public Sub COMPACTA_LOCAL()

    On Error GoTo Trata_Erro

          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\DADOS\OLD\BASE_DPK.OLD") = "BASE_DPK.OLD" Then
2             Kill Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase Path_drv & "\DADOS\BASE_DPK.MDB", Path_drv & "\DADOS\BASE_DPK.NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\DADOS\BASE_DPK.MDB"
6         VDestino = Path_drv & "\DADOS\OLD\BASE_DPK.OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\DADOS\BASE_DPK.NEW"
9         VDestino = Path_drv & "\DADOS\BASE_DPK.MDB"
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
       MsgBox "Sub COMPACTA_LOCAL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If
End Sub

Public Sub COMPACTA_CONTROLE()

    On Error GoTo Trata_Erro

          Dim VOrigem As String, VDestino As String

         'Compactar Banco de Dados
1         If Dir(Path_drv & "\LOG\OLD\CONTROLE.OLD") = "CONTROLE.OLD" Then
2             Kill Path_drv & "\LOG\OLD\CONTROLE.OLD"
3         End If
          
         'Compactando o Banco de Dados, e gerando um novo Banco
4         CompactDatabase "Path_drv & \LOG\CONTROLE.MDB", Path_drv & "\LOG\CONTROLE.NEW"
          
         'Renomeando o Banco antigo
5         VOrigem = Path_drv & "\LOG\CONTROLE.MDB"
6         VDestino = Path_drv & "\LOG\OLD\CONTROLE.OLD"
7         Name VOrigem As VDestino
              
         'Tornando o Banco compaLCOMPActado com atual
8         VOrigem = Path_drv & "\LOG\CONTROLE.NEW"
9         VDestino = Path_drv & "\LOG\CONTROLE.MDB"
10        Name VOrigem As VDestino

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub COMPACTA_CONTROLE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
    End If

End Sub


Public Function DEL_PEDNOTA() As Boolean

         'DELETA PEDIDOS (PEDNOTA_VENDA) E ITENS DO PEDIDO (ITPEDNOTA_VENDA)
         'COM DATA DE PEDIDO MAIOR QUE 30 DIAS.
         'MANTEM APENAS OS PEDIDOS COM NF EMITIDAS POR UM PERIODO DE 1 MES.

1         On Error GoTo VerErro

         'PEDIDOS QUE DEVEM SER DELETADOS
         
2         strSQL = "Select dt_faturamento from datas"
          
3         Set ss = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)

          Dim MDT_AUX As Date
4         MDT_AUX = ss!DT_FATURAMENTO - 30
         
5         strSQL = "INSERT INTO  DELETA_PEDNOTA " & _
                   " SELECT COD_LOJA, NUM_PEDIDO,SEQ_PEDIDO " & _
                   " FROM PEDNOTA_VENDA " & _
                   " WHERE DT_PEDIDO <  cdate('" & CDate(MDT_AUX) & "')"
6         dbLocal.Execute strSQL
            
         'DELETA O CABECALHO DOS PEDIDOS
7                  strSQL = " DELETE PEDNOTA_VENDA.* "
8         strSQL = strSQL & " FROM PEDNOTA_VENDA INNER JOIN DELETA_PEDNOTA ON "
9         strSQL = strSQL & " PEDNOTA_VENDA.COD_LOJA=DELETA_PEDNOTA.COD_LOJA AND "
10        strSQL = strSQL & " PEDNOTA_VENDA.NUM_PEDIDO = DELETA_PEDNOTA.NUM_PEDIDO  AND "
11        strSQL = strSQL & " PEDNOTA_VENDA.SEQ_PEDIDO = DELETA_PEDNOTA.SEQ_PEDIDO  "

12        dbLocal.Execute strSQL

         'DELETA ITENS DOS PEDIDOS
13                 strSQL = "DELETE ITPEDNOTA_VENDA.* "
14        strSQL = strSQL & " FROM ITPEDNOTA_VENDA INNER JOIN DELETA_PEDNOTA ON "
15        strSQL = strSQL & " ITPEDNOTA_VENDA.COD_LOJA = DELETA_PEDNOTA.COD_LOJA AND "
16        strSQL = strSQL & " ITPEDNOTA_VENDA.NUM_PEDIDO = DELETA_PEDNOTA.NUM_PEDIDO AND "
17        strSQL = strSQL & " ITPEDNOTA_VENDA.SEQ_PEDIDO = DELETA_PEDNOTA.SEQ_PEDIDO  "
          
18        dbLocal.Execute strSQL
             
19        DEL_PEDNOTA = True

20    Exit Function

VerErro:

21        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
22            dbLocal.Execute "DROP TABLE AUX_DEL_PEDNOTA"
23            Resume
24        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
25            Resume Next
26        Else
27            DEL_PEDNOTA = False
28            Screen.MousePointer = 1
30            MsgBox "Sub DEL_PEDNOTA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
31        End If
Resume
End Function
Public Function DEL_CLIENTE() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_DEL_CLIENTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_CLIENTE "
4         dbExt.Execute strSQL
          
5         strSQL = "DELETE CLIE_CREDITO.* FROM CLIE_CREDITO INNER JOIN AUX_DEL_CLIENTE " & _
                   " ON CLIE_CREDITO.COD_CLIENTE = AUX_DEL_CLIENTE.COD_CLIENTE "
6         dbLocal.Execute strSQL
                
7         strSQL = "DELETE CLIE_ENDERECO.* FROM CLIE_ENDERECO INNER JOIN AUX_DEL_CLIENTE " & _
                   " ON CLIE_ENDERECO.COD_CLIENTE = AUX_DEL_CLIENTE.COD_CLIENTE "
8         dbLocal.Execute strSQL
          
9         strSQL = "DELETE DUPLICATAS.* FROM DUPLICATAS INNER JOIN AUX_DEL_CLIENTE " & _
                   " ON AUX_DEL_CLIENTE.COD_CLIENTE = DUPLICATAS.COD_CLIENTE"
10        dbLocal.Execute strSQL
                
11        strSQL = "DELETE CLIENTE.* FROM CLIENTE INNER JOIN " & _
                   " AUX_DEL_CLIENTE ON " & _
                   " CLIENTE.COD_CLIENTE = AUX_DEL_CLIENTE.COD_CLIENTE "
12        dbLocal.Execute strSQL
13        vRegs = dbLocal.RecordsAffected
          
14        dbLocal.Execute "DROP TABLE AUX_DEL_CLIENTE"
                  
15        DEL_CLIENTE = True

16        Controle "DEL_CLI", vRegs

17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_DEL_CLIENTE"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            DEL_CLIENTE = False
25            Screen.MousePointer = 1
26            MsgBox "Sub DEL_CLIENTE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If

End Function
Public Function DEL_APLICACAO() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DEL_APLICACAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_APLICACAO "
4         dbExt.Execute strSQL
                
5         strSQL = "DELETE APLICACAO.* FROM APLICACAO INNER JOIN " & _
                   " AUX_DEL_APLICACAO ON " & _
                   " APLICACAO.COD_DPK = AUX_DEL_APLICACAO.COD_DPK AND " & _
                   " APLICACAO.COD_CATEGORIA = AUX_DEL_APLICACAO.COD_CATEGORIA AND " & _
                   " APLICACAO.COD_MONTADORA = AUX_DEL_APLICACAO.COD_MONTADORA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_APLICACAO"
                  
9         DEL_APLICACAO = True

10        Controle "DEL_APL", vRegs

11    Exit Function

VerErro:

12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_APLICACAO"
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_APLICACAO = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_APLICACAO - EE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function
Public Function DEL_FORNECEDOR_ESPECIFICO() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_DEL_FOR_ESP IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEL_FOR_ESP "
4         dbExt.Execute strSQL
                
5         strSQL = "DELETE FORNECEDOR_ESPECIFICO.* FROM FORNECEDOR_ESPECIFICO INNER JOIN " & _
                   " AUX_DEL_FOR_ESP ON " & _
                   " FORNECEDOR_ESPECIFICO.COD_FORNECEDOR = AUX_DEL_FOR_ESP.COD_FORNECEDOR "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_FOR_ESP"
                  
9         DEL_FORNECEDOR_ESPECIFICO = True

10        Controle "DEL_FOES", vRegs

11    Exit Function

VerErro:

12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_FOR_ESP"
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_FORNECEDOR_ESPECIFICO = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_FORNECEDOR_ESPECIFICO - HE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function
Public Function DEL_REPR() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_DEL_REPR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_REPRESENTANTE "
4         dbExt.Execute strSQL
          
5         strSQL = "DELETE REPRESENTACAO.* FROM REPRESENTACAO " & _
                   " INNER JOIN R_REPCGC ON REPRESENTACAO.CGC = R_REPCGC.CGC " & _
                   " WHERE R_REPCGC.COD_REPRES IN " & _
                   " (SELECT AUX_DEL_REPR.COD_REPRES FROM AUX_DEL_REPR ) "
6         dbLocal.Execute strSQL
          
7         strSQL = "DELETE R_REPCGC.* FROM R_REPCGC INNER JOIN AUX_DEL_REPR " & _
                   " ON AUX_DEL_REPR.COD_REPRES = R_REPCGC.COD_REPRES "
8         dbLocal.Execute strSQL
          
9         strSQL = "DELETE REPR_END_CORRESP.* FROM REPR_END_CORRESP INNER JOIN AUX_DEL_REPR " & _
                   " ON AUX_DEL_REPR.COD_REPRES = REPR_END_CORRESP.COD_REPRES "
10        dbLocal.Execute strSQL
          
11        strSQL = "DELETE R_REPVEN.* FROM R_REPVEN INNER JOIN AUX_DEL_REPR " & _
                   " ON AUX_DEL_REPR.COD_REPRES = R_REPVEN.COD_REPRES OR " & _
                   "    AUX_DEL_REPR.COD_REPRES = R_REPVEN.COD_VEND "
12        dbLocal.Execute strSQL
          
13        strSQL = "DELETE CLIE_CREDITO.* FROM CLIE_CREDITO INNER JOIN CLIENTE " & _
                   " ON CLIENTE.COD_CLIENTE = CLIE_CREDITO.COD_CLIENTE " & _
                   " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                      " FROM AUX_DEL_REPR )"
14        dbLocal.Execute strSQL
          
15        strSQL = "DELETE CLIE_ENDERECO.* FROM CLIE_ENDERECO INNER JOIN CLIENTE " & _
                   " ON CLIENTE.COD_CLIENTE = CLIE_ENDERECO.COD_CLIENTE " & _
                   " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                      " FROM AUX_DEL_REPR )"
16        dbLocal.Execute strSQL
          
17        strSQL = "DELETE DUPLICATAS.* FROM DUPLICATAS INNER JOIN CLIENTE " & _
                   " ON CLIENTE.COD_CLIENTE = DUPLICATAS.COD_CLIENTE " & _
                   " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                      " FROM AUX_DEL_REPR )"
18        dbLocal.Execute strSQL
          
19        strSQL = "DELETE CLIENTE.* FROM CLIENTE INNER JOIN AUX_DEL_REPR " & _
                   " ON CLIENTE.COD_REPR_VEND = AUX_DEL_REPR.COD_REPRES "
20        dbLocal.Execute strSQL
          
21        strSQL = "DELETE REPRESENTANTE.* FROM REPRESENTANTE INNER JOIN " & _
                   " AUX_DEL_REPR ON " & _
                   " REPRESENTANTE.COD_REPRES = AUX_DEL_REPR.COD_REPRES "
22        dbLocal.Execute strSQL
23        vRegs = dbLocal.RecordsAffected
          
24        dbLocal.Execute "DROP TABLE AUX_DEL_REPR "
                  
25        DEL_REPR = True

26        Controle "DEL_REPR", vRegs

27    Exit Function

VerErro:

28        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
29            dbLocal.Execute "DROP TABLE AUX_DEL_REPR "
30            Resume
31        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
32            Resume Next
33        Else
34            DEL_REPR = False
35            Screen.MousePointer = 1
36            MsgBox "Sub DEL_REPR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
37        End If

End Function
Public Function DEL_TABDESC() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DEL_TABDESC IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DELETA_TABELA_DESCPER "
4         dbExt.Execute strSQL
                
5         strSQL = "DELETE TABELA_DESCPER.* FROM TABELA_DESCPER INNER JOIN " & _
                   " AUX_DEL_TABDESC ON " & _
                   " TABELA_DESCPER.SEQUENCIA = AUX_DEL_TABDESC.SEQUENCIA AND " & _
                   " TABELA_DESCPER.TP_TABELA = AUX_DEL_TABDESC.TP_TABELA AND " & _
                   " TABELA_DESCPER.OCORR_PRECO = AUX_DEL_TABDESC.OCORR_PRECO AND " & _
                   " TABELA_DESCPER.COD_FORNECEDOR = AUX_DEL_TABDESC.COD_FORNECEDOR AND " & _
                   " TABELA_DESCPER.COD_DPK = AUX_DEL_TABDESC.COD_DPK AND " & _
                   " TABELA_DESCPER.COD_GRUPO = AUX_DEL_TABDESC.COD_GRUPO AND " & _
                   " TABELA_DESCPER.COD_SUBGRUPO = AUX_DEL_TABDESC.COD_SUBGRUPO "
          
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         dbLocal.Execute "DROP TABLE AUX_DEL_TABDESC "
          
9         strSQL = "DELETE TABELA_DESCPER.* FROM TABELA_DESCPER WHERE SITUACAO=9"
10        dbLocal.Execute strSQL
                  
11        DEL_TABDESC = True
            
12       Controle "TABELAD", vRegs
            
13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_DEL_TABDESC "
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            DEL_TABDESC = False
21            Screen.MousePointer = 1
23            MsgBox "Sub DEL_TABDESC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function FUNCAO_ERRO(strReg As String)
    Dim byPOs As Byte
    Dim strAssunto As String    'Assunto, a que se refere
    Dim strMsg As String        'Mensagem mostrada
    Dim strResp As String       'Respons�vel
    Dim byTpProb As Byte        'Tipo de Problema
    
   'Fun��o de erro: grava na tabela LOGDIV o erro ocorrido e tamb�m
   '                envia uma mensagem pelo correio para Campinas
   '                dizendo qual foi o erro para ser dado o devido suporte
   '                ao usu�rio
    Screen.MousePointer = 0
    If Err.Number = 3265 Then
        strAssunto = "Erro de Atualiza��o"
        strMsg = "Erro NR.: " & Err.Number & " - " & " Item n�o Encontrado nesta Sele��o " & strReg
        strResp = "Dep. Sistemas - Ramal 791/793"
        MsgBox "Erro NR.: " & Err.Number & " - " & " Item n�o Encontrado nesta Sele��o ", vbExclamation, "ERRO NO APLICATIVO"
        byTpProb = 1
    ElseIf Err.Number = 53 Then
        strAssunto = "Erro Rotina de Atualiza��o"
        strMsg = "Erro NR.: " & Err.Number & " - " & " Arquivo n�o Encontrado Path_drv & \DADOS\FGER1000.DAT"
        strResp = "Dep. Opera��o Ramal 799"
        MsgBox "Erro NR.: " & Err.Number & " - " & " Arquivo n�o Encontrado " & Chr(13) & " Path_drv &\DADOS\FGER1000.DAT", vbExclamation, "ERRO NO APLICATIVO"
        byTpProb = 1
    ElseIf Err.Number = 76 Then
        strAssunto = "Erro Rotina de Atualiza��o"
        strMsg = "Erro NR.: " & Err.Number & " - " & " Diret�rio n�o Encontrado  Path_drv &\DADOS\"
        strResp = "Dep. Opera��o Ramal 799"
        MsgBox "Erro NR.: " & Err.Number & " - " & " Diret�rio n�o Encontrado " & Chr(13) & " Path_drv & \DADOS\", vbExclamation, "ERRO NO APLICATIVO"
        byTpProb = 1
    ElseIf Err.Number = 3075 Then
        strAssunto = "Erro de Atualiza��o"
        strMsg = "Erro NR.: " & Err.Number & " - ERRO DE SINTAXE " & strSQL
        strResp = "Dep. Sistemas - Ramal 791/793"
        MsgBox "Erro NR.: " & Err.Number & " - ERRO DE SINTAXE " & strSQL & " NO REGISTRO: " & strReg
        byTpProb = 1
    ElseIf Err.Number = 13 Then
        strAssunto = "Erro de Atualiza��o"
        strMsg = "Erro NR.: " & Err.Number & " - ERRO DE TIPO INCORRETO NO REG.:  " & strReg
        strResp = "Dep. Sistemas - Ramal 791/793"
        MsgBox "Erro NR.: " & Err.Number & " - ERRO DE TIPO INCORRETO NO REG.:  " & strReg
        byTpProb = 1
    ElseIf Err.Number > 0 Then
        MsgBox "Erro NR.: " & Err.Number & " - " & Err.Description
        byTpProb = 3
        strAssunto = "Erro n�o Reconhecido " & strReg
        strMsg = Err.Number & " - " & Err.Description & " - " & Err.Source
        strResp = "Depto. Suporte - Ramais 703/705/770"
    ElseIf Err.Number = 0 Then
        byTpProb = 0
        strAssunto = strReg
        strMsg = "OK"
        strResp = "Depto. Suporte - Ramais 703/705/770"
    End If
    
    byPOs = 1
    Do While True
        byPOs = InStr(byPOs, strMsg, "'")
        If byPOs = 0 Then
            Exit Do
        End If
        Mid(strMsg, byPOs) = " "
    Loop
    
    strSQL = "INSERT INTO LOGDIV " & _
             " ( DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL ) VALUES " & _
             " ( '" & Now & "' , " & byTpProb & ", '" & strAssunto & "', '" & strMsg & "', '" & strResp & "' ) "
            
    'dbControle.Execute strSQL, dbFailOnError
End Function
Public Function ATU_UF_TPCLIENTE() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF_TPCLIENTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM UF_TPCLIENTE "
4         dbExt.Execute strSQL
                
5                   strSQL = "UPDATE UF_TPCLIENTE INNER JOIN AUX_UF_TPCLIENTE ON "
6         strSQL = strSQL & " UF_TPCLIENTE.COD_UF_ORIGEM = AUX_UF_TPCLIENTE.COD_UF_ORIGEM AND "
7         strSQL = strSQL & " UF_TPCLIENTE.COD_UF_DESTINO = AUX_UF_TPCLIENTE.COD_UF_DESTINO AND "
8         strSQL = strSQL & " UF_TPCLIENTE.TP_CLIENTE = AUX_UF_TPCLIENTE.TP_CLIENTE AND"
9         strSQL = strSQL & " UF_TPCLIENTE.COD_TRIBUTACAO = AUX_UF_TPCLIENTE.COD_TRIBUTACAO "
10        strSQL = strSQL & " SET "
11        strSQL = strSQL & " UF_TPCLIENTE.PC_DESC = AUX_UF_TPCLIENTE.PC_DESC,"
12        strSQL = strSQL & " UF_TPCLIENTE.PC_DESC_DIESEL = AUX_UF_TPCLIENTE.PC_DESC_DIESEL"
13        dbLocal.Execute strSQL
14        vRegs = dbLocal.RecordsAffected
                 
15        strSQL = "INSERT INTO UF_TPCLIENTE SELECT * FROM AUX_UF_TPCLIENTE "
16        dbLocal.Execute strSQL
17        vRegs = vRegs + dbLocal.RecordsAffected
              
18        dbLocal.Execute "DROP TABLE AUX_UF_TPCLIENTE "
                  
19        ATU_UF_TPCLIENTE = True

20        Controle "UF_TPCLI", vRegs

21    Exit Function

VerErro:

22        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
23            dbLocal.Execute "DROP TABLE AUX_UF_TPCLIENTE"
24            Resume
25        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
26            Resume Next
27        Else
28            ATU_UF_TPCLIENTE = False
29            Screen.MousePointer = 1
30            MsgBox "Sub ATU_UF_TPCLIENTE - FP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
31        End If

End Function
Public Function ATU_BANCO() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_BANCO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM BANCO "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE BANCO INNER JOIN AUX_BANCO ON BANCO.COD_BANCO = AUX_BANCO.COD_BANCO  " & _
                   " SET BANCO.SIGLA = AUX_BANCO.SIGLA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
           
8         strSQL = "INSERT INTO BANCO SELECT * FROM AUX_BANCO"
9         dbLocal.Execute strSQL
10       vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_BANCO"
            
12        ATU_BANCO = True

13        Controle "BANCO", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_BANCO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_BANCO = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_BANCO - AA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function
Public Function ATU_CATEGSINAL() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CATEG_SINAL IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CATEG_SINAL "
4         dbExt.Execute strSQL
          
5         strSQL = "UPDATE CATEG_SINAL INNER JOIN AUX_CATEG_SINAL ON CATEG_SINAL.CATEGORIA = AUX_CATEG_SINAL.CATEGORIA " & _
                   " SET CATEG_SINAL.SINAL = AUX_CATEG_SINAL.SINAL "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CATEG_SINAL SELECT * FROM AUX_CATEG_SINAL"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CATEG_SINAL"
          
12        ATU_CATEGSINAL = True

13        Controle "CATEG_S", vRegs
        
14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CATEG_SINAL"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CATEGSINAL = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_CATEGSINAL - BB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_CIDADE() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_CIDADE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CIDADE "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE CIDADE INNER JOIN AUX_CIDADE ON CIDADE.COD_CIDADE = AUX_CIDADE.COD_CIDADE  " & _
                  " SET CIDADE.NOME_CIDADE = AUX_CIDADE.NOME_CIDADE, " & _
                  " CIDADE.TP_CIDADE = AUX_CIDADE.TP_CIDADE , " & _
                  " CIDADE.COD_UF = AUX_CIDADE.COD_UF, " & _
                  " CIDADE.COD_REGIAO = AUX_CIDADE.COD_REGIAO, " & _
                  " CIDADE.COD_SUBREGIAO = AUX_CIDADE.COD_SUBREGIAO , " & _
                  " CIDADE.CARACTERISTICA = AUX_CIDADE.CARACTERISTICA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CIDADE SELECT * FROM AUX_CIDADE"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CIDADE"
              
12        ATU_CIDADE = True
    
13        Controle "CIDADE", vRegs
            
14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CIDADE"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CIDADE = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_CIDADE - AC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_SALDOPED() As Boolean

1         On Error GoTo VerErro
          
2         strSQL = "SELECT * INTO AUX_SALDO_PEDIDOS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM SALDO_PEDIDOS "
3         dbExt.Execute strSQL
                
4         strSQL = "UPDATE SALDO_PEDIDOS INNER JOIN AUX_SALDO_PEDIDOS ON " & _
                   " SALDO_PEDIDOS.COD_LOJA = AUX_SALDO_PEDIDOS.COD_LOJA AND  " & _
                   " SALDO_PEDIDOS.COD_CLIENTE=AUX_SALDO_PEDIDOS.COD_CLIENTE " & _
                   " SET SALDO_PEDIDOS.SALDO_PEDIDOS = AUX_CLIE_CREDITO.SALDO_PEDIDOS "
5         dbLocal.Execute strSQL
6         vRegs = dbLocal.RecordsAffected
          
7         strSQL = "INSERT INTO SALDO_PEDIDOS SELECT * FROM AUX_SALDO_PEDIDOS"
8         dbLocal.Execute strSQL
9         vRegs = vRegs + dbLocal.RecordsAffected
          
10        dbLocal.Execute "DROP TABLE AUX_SALDOPED"
                  
11        ATU_SALDOPED = True

12        Controle "SALDO_PE", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_SALDOPED"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            ATU_SALDOPED = False
21            Screen.MousePointer = 1
22            MsgBox "Sub ATU_SALDOPED - BC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If

End Function

Public Function ATU_CLIECRED() As Boolean

1         On Error GoTo VerErro
          
2         strSQL = "SELECT * INTO AUX_CLIE_CREDITO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIE_CREDITO "
3         dbExt.Execute strSQL
            
4         strSQL = "UPDATE CLIE_CREDITO INNER JOIN AUX_CLIE_CREDITO ON CLIE_CREDITO.COD_CLIENTE = AUX_CLIE_CREDITO.COD_CLIENTE  " & _
                   " SET CLIE_CREDITO.DT_ULT_COMPRA = AUX_CLIE_CREDITO.DT_ULT_COMPRA , " & _
                   " CLIE_CREDITO.NOTA_CREDITO = AUX_CLIE_CREDITO.NOTA_CREDITO, " & _
                   " CLIE_CREDITO.LIMITE_CREDITO = AUX_CLIE_CREDITO.LIMITE_CREDITO "
5         dbLocal.Execute strSQL
6         vRegs = dbLocal.RecordsAffected
          
7         strSQL = "INSERT INTO CLIE_CREDITO SELECT * FROM AUX_CLIE_CREDITO"
8         dbLocal.Execute strSQL
9         vRegs = vRegs + dbLocal.RecordsAffected
          
10        dbLocal.Execute "DROP TABLE AUX_CLIE_CREDITO"
              
11        ATU_CLIECRED = True

12        Controle "CLIE_CRE", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_CLIE_CREDITO"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            ATU_CLIECRED = False
21            Screen.MousePointer = 1
22            MsgBox "Sub ATU_CLIECRED - BC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If

End Function
Public Function ATU_CLIEEND() As Boolean
          
1         On Error GoTo VerErro
          
2         vRegs = 0
    
3         strSQL = "SELECT * INTO AUX_CLIE_ENDERECO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIE_ENDERECO "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE CLIE_ENDERECO INNER JOIN AUX_CLIE_ENDERECO ON CLIE_ENDERECO.COD_CLIENTE = AUX_CLIE_ENDERECO.COD_CLIENTE  " & _
                   " SET CLIE_ENDERECO.TP_ENDERECO = AUX_CLIE_ENDERECO.TP_ENDERECO , " & _
                   " CLIE_ENDERECO.SEQUENCIA = AUX_CLIE_ENDERECO.SEQUENCIA , " & _
                   " CLIE_ENDERECO.ENDERECO = AUX_CLIE_ENDERECO.ENDERECO , " & _
                   " CLIE_ENDERECO.COD_CIDADE = AUX_CLIE_ENDERECO.COD_CIDADE , CLIE_ENDERECO.BAIRRO = AUX_CLIE_ENDERECO.BAIRRO, " & _
                   " CLIE_ENDERECO.CEP = AUX_CLIE_ENDERECO.CEP , CLIE_ENDERECO.DDD = AUX_CLIE_ENDERECO.DDD, " & _
                   " CLIE_ENDERECO.FONE = AUX_CLIE_ENDERECO.FONE, CLIE_ENDERECO.FAX = AUX_CLIE_ENDERECO.FAX, " & _
                   " CLIE_ENDERECO.CXPOSTAL = AUX_CLIE_ENDERECO.CXPOSTAL, CLIE_ENDERECO.TP_DOCTO = AUX_CLIE_ENDERECO.TP_DOCTO, " & _
                   " CLIE_ENDERECO.CGC = AUX_CLIE_ENDERECO.CGC, CLIE_ENDERECO.INSCR_ESTADUAL = AUX_CLIE_ENDERECO.INSCR_ESTADUAL, " & _
                   " CLIE_ENDERECO.NOME_CLIENTE = AUX_CLIE_ENDERECO.NOME_CLIENTE "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CLIE_ENDERECO SELECT * FROM AUX_CLIE_ENDERECO"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected

11        dbLocal.Execute "DROP TABLE AUX_CLIE_ENDERECO"
              
12        ATU_CLIEEND = True

13        Controle "CLIE_END", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CLIE_ENDERECO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CLIEEND = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_CLIEEND - AE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function

Public Function ATU_CLIEMSG() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CLIEMSG IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIE_MENSAGEM "
4         dbExt.Execute strSQL
            
5         strSQL = "UPDATE CLIE_MENSAGEM INNER JOIN AUX_CLIEMSG ON CLIE_MENSAGEM.COD_MENSAGEM = AUX_CLIEMSG.COD_MENSAGEM  " & _
                   " SET CLIE_MENSAGEM.DESC_MENS = AUX_CLIEMSG.DESC_MENS,  " & _
                   " CLIE_MENSAGEM.FL_BLOQUEIO = AUX_CLIEMSG.FL_BLOQUEIO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CLIE_MENSAGEM SELECT * FROM AUX_CLIEMSG"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CLIEMSG"
              
12        ATU_CLIEMSG = True

13        Controle "CLIE_MEN", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CLIEMSG"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_CLIEMSG = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_CLIEMSG - BS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_CLIENTE() As Boolean
1         On Error GoTo VerErro

2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_CLIENTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIENTE "
4         dbExt.Execute strSQL
            
5                  strSQL = " UPDATE CLIENTE INNER JOIN AUX_CLIENTE ON "
6         strSQL = strSQL & " CLIENTE.COD_CLIENTE = AUX_CLIENTE.COD_CLIENTE  "
7         strSQL = strSQL & " SET "
8         strSQL = strSQL & " CLIENTE.CGC = AUX_CLIENTE.CGC, "
9         strSQL = strSQL & " CLIENTE.NOME_CLIENTE = AUX_CLIENTE.NOME_CLIENTE, "
10        strSQL = strSQL & " CLIENTE.NOME_CONTATO = AUX_CLIENTE.NOME_CONTATO, "
11        strSQL = strSQL & " CLIENTE.CLASSIFICACAO = AUX_CLIENTE.CLASSIFICACAO , "
12        strSQL = strSQL & " CLIENTE.ENDERECO = AUX_CLIENTE.ENDERECO, "
13        strSQL = strSQL & " CLIENTE.COD_CIDADE = AUX_CLIENTE.COD_CIDADE, "
14        strSQL = strSQL & " CLIENTE.BAIRRO = AUX_CLIENTE.BAIRRO, "
15        strSQL = strSQL & " CLIENTE.DDD1 = AUX_CLIENTE.DDD1, "
16        strSQL = strSQL & " CLIENTE.FONE1 = AUX_CLIENTE.FONE1, "
17        strSQL = strSQL & " CLIENTE.DDD2 = AUX_CLIENTE.DDD2, "
18        strSQL = strSQL & " CLIENTE.FONE2 = AUX_CLIENTE.FONE2, "
19        strSQL = strSQL & " CLIENTE.CEP = AUX_CLIENTE.CEP, "
20        strSQL = strSQL & " CLIENTE.CXPOSTAL = AUX_CLIENTE.CXPOSTAL,"
21        strSQL = strSQL & " CLIENTE.TELEX = AUX_CLIENTE.TELEX, "
22        strSQL = strSQL & " CLIENTE.FAX= AUX_CLIENTE.FAX, "
23        strSQL = strSQL & " CLIENTE.INSCR_ESTADUAL = AUX_CLIENTE.INSCR_ESTADUAL, "
24        strSQL = strSQL & " CLIENTE.INSCR_SUFRAMA = AUX_CLIENTE.INSCR_SUFRAMA, "
25        strSQL = strSQL & " CLIENTE.COD_TIPO_CLIENTE = AUX_CLIENTE.COD_TIPO_CLIENTE, "
26        strSQL = strSQL & " CLIENTE.DT_CADASTR = AUX_CLIENTE.DT_CADASTR, "
27        strSQL = strSQL & " CLIENTE.COD_TRANSP = AUX_CLIENTE.COD_TRANSP, "
28        strSQL = strSQL & " CLIENTE.COD_BANCO = AUX_CLIENTE.COD_BANCO, "
29        strSQL = strSQL & " CLIENTE.COD_MENSAGEM = AUX_CLIENTE.COD_MENSAGEM, "
30        strSQL = strSQL & " CLIENTE.FL_CONS_FINAL = AUX_CLIENTE.FL_CONS_FINAL, "
31        strSQL = strSQL & " CLIENTE.CARACTERISTICA = AUX_CLIENTE.CARACTERISTICA, "
32        strSQL = strSQL & " CLIENTE.COD_MENSAGEM_FISCAL = AUX_CLIENTE.COD_MENSAGEM_FISCAL, "
33        strSQL = strSQL & " CLIENTE.TP_EMPRESA = AUX_CLIENTE.TP_EMPRESA "
34        dbLocal.Execute strSQL
35        vRegs = dbLocal.RecordsAffected
             
36        strSQL = "INSERT INTO CLIENTE SELECT * FROM AUX_CLIENTE"
37        dbLocal.Execute strSQL
38        vRegs = vRegs + dbLocal.RecordsAffected
          
39        dbLocal.Execute "DROP TABLE AUX_CLIENTE"

40        strSQL = "DELETE CLIENTE.* FROM CLIENTE WHERE SITUACAO = 9"
41        dbLocal.Execute strSQL

42        ATU_CLIENTE = True

43        Controle "CLIENTE", vRegs

44    Exit Function

VerErro:

45        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
46            dbLocal.Execute "DROP TABLE AUX_CLIENTE"
47            Resume
48        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
49            Resume Next
50        Else
51            ATU_CLIENTE = False
52            Screen.MousePointer = 1
53            MsgBox "Sub ATU_CLIENTE - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
54        End If

End Function

Public Function ATU_R_CLIENTE_QUADRANTE() As Boolean
1         On Error GoTo VerErro

2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_R_CLIENTE_QUADRANTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_CLIENTE_QUADRANTE "
4         dbExt.Execute strSQL
                  
          'Deleta tudo que esta no base_dpk
5         strSQL = "DELETE * FROM R_CLIENTE_QUADRANTE"
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
                   
8         strSQL = "INSERT INTO R_CLIENTE_QUADRANTE SELECT * FROM AUX_R_CLIENTE_QUADRANTE"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
                
11        dbLocal.Execute "DROP TABLE AUX_R_CLIENTE_QUADRANTE"
          'dbLocal.Execute strSQL

12        ATU_R_CLIENTE_QUADRANTE = True

13        Controle "R_CLIENTE_QUADRANTE", vRegs

14        Exit Function

VerErro:
15        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_R_CLIENTE_QUADRANTE"
17            Resume
18        ElseIf Err = 3078 Then 'Mandou atualiza��o de promotor mais n�o mandou a carga para criar as tabelas.
19            MsgBox "Esta mensagem � apenas para notifica-lo que seu sistema n�o esta atualizado adequadamente, " & vbCrLf & _
              "por favor envie esta mensagem para o Help Desk. " & vbCrLf & _
              "O processo de atualiza��o di�ria continuar� normalmente." & vbCrLf & _
              "Rotina: ATU_R_CLIENTE_QUADRANTE.", vbInformation, ":: Aten��o ::"
20            ATU_R_CLIENTE_QUADRANTE = True
21            Exit Function
22        ElseIf Err = 3022 Then
              'Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_R_CLIENTE_QUADRANTE = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_R_CLIENTE_QUADRANDE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If
End Function

Public Function ATU_R_ACAO_QUADRANTE() As Boolean
1         On Error GoTo VerErro

2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_R_ACAO_QUADRANTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_ACAO_QUADRANTE "
4         dbExt.Execute strSQL
                  
          'Deleta tudo que esta no base_dpk
5         strSQL = "DELETE * FROM R_ACAO_QUADRANTE"
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
                   
8         strSQL = "INSERT INTO R_ACAO_QUADRANTE SELECT * FROM AUX_R_ACAO_QUADRANTE"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
                
11        dbLocal.Execute "DROP TABLE AUX_R_ACAO_QUADRANTE"
          'dbLocal.Execute strSQL

12        ATU_R_ACAO_QUADRANTE = True

13        Controle "R_ACAO_QUADRANTE", vRegs

14        Exit Function

VerErro:
15        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_R_ACAO_QUADRANTE"
17            Resume
18        ElseIf Err = 3078 Then 'Mandou atualiza��o de promotor mais n�o mandou a carga para criar as tabelas.
19            MsgBox "Esta mensagem � apenas para notifica-lo que seu sistema n�o esta atualizado adequadamente, " & vbCrLf & _
              "por favor envie esta mensagem para o Help Desk. " & vbCrLf & _
              "O processo de atualiza��o di�ria continuar� normalmente." & vbCrLf & _
              "Rotina: ATU_R_ACAO_QUADRANTE.", vbInformation, ":: Aten��o ::"
20            ATU_R_ACAO_QUADRANTE = True
21            Exit Function
22        ElseIf Err = 3022 Then
              'Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_R_ACAO_QUADRANTE = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_R_ACAO_QUADRANDE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If
End Function

Public Function ATU_DICAS() As Boolean
1         On Error GoTo VerErro
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DICAS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DICAS "
4         dbExt.Execute strSQL

5         strSQL = "UPDATE DICAS INNER JOIN AUX_DICAS ON " & _
          " DICAS.COD_CLIENTE = AUX_DICAS.COD_CLIENTE  AND " & _
          " DICAS.DT_DICA = AUX_DICAS.DT_DICA   " & _
          " SET DICAS.DICA = AUX_DICAS.DICA"
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected

8         strSQL = "INSERT INTO DICAS SELECT * FROM AUX_DICAS"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
11        dbLocal.Execute "DROP TABLE AUX_DICAS"
12        ATU_DICAS = True

13        Controle "DICAS", vRegs
14    Exit Function

VerErro:
15        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_DICAS"
17            Resume
18        ElseIf Err = 3078 Then 'Mandou atualiza��o de promotor mais n�o mandou a carga para criar as tabelas.
19            MsgBox "Esta mensagem � apenas para notifica-lo que seu sistema n�o esta atualizado adequadamente, " & vbCrLf & _
              "por favor envie esta mensagem para o Help Desk. " & vbCrLf & _
              "O processo de atualiza��o di�ria continuar� normalmente." & vbCrLf & _
              "Rotina: ATU_DICAS.", vbInformation, ":: Aten��o ::"
20            ATU_DICAS = True
21            Exit Function
22        ElseIf Err = 3022 Then
              ' Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_DICAS = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_DICAS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If
End Function

Public Function ATU_STATUS_VISITA() As Boolean
1         On Error GoTo VerErro
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_STATUS_VISITA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM STATUS_VISITA "
4         dbExt.Execute strSQL

5         dbLocal.Execute "DELETE * FROM STATUS_VISITA"
          
6         strSQL = "INSERT INTO STATUS_VISITA SELECT * FROM AUX_STATUS_VISITA"
7         dbLocal.Execute strSQL
8         vRegs = vRegs + dbLocal.RecordsAffected

9         dbLocal.Execute "DROP TABLE AUX_STATUS_VISITA"
10        ATU_STATUS_VISITA = True

11        Controle "STATUS_VISITA", vRegs
12    Exit Function

VerErro:
13        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
14            dbLocal.Execute "DROP TABLE AUX_STATUS_VISITA"
15            Resume
16        ElseIf Err = 3078 Then 'Mandou atualiza��o de promotor mais n�o mandou a carga para criar as tabelas.
17            MsgBox "Esta mensagem � apenas para notifica-lo que seu sistema n�o esta atualizado adequadamente, " & vbCrLf & _
              "por favor envie esta mensagem para o Help Desk. " & vbCrLf & _
              "O processo de atualiza��o di�ria continuar� normalmente." & vbCrLf & _
              "Rotina: ATU_STATUS_VISITA.", vbInformation, ":: Aten��o ::"
18            ATU_STATUS_VISITA = True
19            Exit Function
20        ElseIf Err = 3022 Then
              ' Registro ja existe (Faz Update)
21            Resume Next
22        Else
23            ATU_STATUS_VISITA = False
24            Screen.MousePointer = 1
25            MsgBox "Sub ATU_STATUS_VISITA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
26        End If
End Function

Public Function ATU_VISITA_PROMOTOR() As Boolean
      Dim i As Integer
1         On Error GoTo VerErro
2         vRegs = 0

          'Insere sempre os dois ultimos registro de cada cliente da tabela visita promotor na tabela VISITA_PROMOTOR_HIST
3         strSQL = "SELECT CLIENTE.CGC"
4         strSQL = strSQL & " FROM CLIENTE INNER JOIN VISITA_PROMOTOR ON CLIENTE.CGC = VISITA_PROMOTOR.CGC"
5         strSQL = strSQL & " GROUP BY CLIENTE.CGC ORDER BY CLIENTE.CGC;"
6         Set recTab = dbLocal.OpenRecordset(strSQL, dbOpenSnapshot)
          
7         If recTab.EOF = False Then
8             dbLocal.Execute "DELETE * FROM VISITA_PROMOTOR_HIST"
9             For i = 1 To recTab.RecordCount - 1
10                strSQL = "INSERT INTO VISITA_PROMOTOR_HIST"
11                strSQL = strSQL & " SELECT TOP 2 VISITA_PROMOTOR.CGC, VISITA_PROMOTOR.DT_VISITA_AGENDA, VISITA_PROMOTOR.COD_PROMOTOR, VISITA_PROMOTOR.DT_RESULTADO, VISITA_PROMOTOR.COD_STATUS, VISITA_PROMOTOR.COMENTARIO_GERENTE, VISITA_PROMOTOR.COMENTARIO_PROMOTOR"
12                strSQL = strSQL & " From VISITA_PROMOTOR"
13                strSQL = strSQL & " Where (((VISITA_PROMOTOR.CGC) = " & recTab!cgc & "))"
14                strSQL = strSQL & " ORDER BY VISITA_PROMOTOR.DT_VISITA_AGENDA DESC;"
15                dbLocal.Execute strSQL
16                recTab.MoveNext
17            Next
18        End If

          'Antes de come�ar a atualiza��o da tabela Visita Promotor os registro devem ser todos excluidos.
19        dbLocal.Execute "DELETE * FROM VISITA_PROMOTOR"

          'Depois de excluir os registro inserir os dados da tabela agenda com data do dia na tabela visita promotor
20        strSQL = "SELECT CLIENTE.CGC, AGENDA.DT_AGENDA AS DT_VISITA_AGENDA, AGENDA.COD_VEND AS COD_PROMOTOR, """" AS DT_RESULTADO, 0 AS COD_STATUS, """" AS COMENTARIO_GERENTE, AGENDA.OBSERVACAO AS COMENTARIO_PROMOTOR into aux_Visita_Agenda in " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM "
21        strSQL = strSQL & "(AGENDA INNER JOIN CLIENTE ON AGENDA.COD_CLIENTE = CLIENTE.COD_CLIENTE) INNER JOIN DATAS ON AGENDA.DT_AGENDA = DATAS.DT_FATURAMENTO WHERE AGENDA.DT_AGENDA>=[datas].[dt_faturamento];"
22        dbLocal.Execute strSQL
          
23        strSQL = "INSERT INTO VISITA_PROMOTOR SELECT * FROM aux_Visita_Agenda"
24        dbLocal.Execute strSQL
25        dbLocal.Execute "DROP TABLE aux_Visita_Agenda"
          
          'Continuar o processo normal de atualiza��o diaria.
26        strSQL = "SELECT * INTO AUX_VISITA_PROMOTOR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VISITA_PROMOTOR "
27        dbExt.Execute strSQL
          
28        strSQL = "INSERT INTO VISITA_PROMOTOR SELECT * FROM AUX_VISITA_PROMOTOR"
29        dbLocal.Execute strSQL
30        vRegs = vRegs + dbLocal.RecordsAffected
31        dbLocal.Execute "DROP TABLE AUX_VISITA_PROMOTOR"
32        ATU_VISITA_PROMOTOR = True

33        Controle "VISITA_PROMOTOR", vRegs
34    Exit Function

VerErro:
35        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
36            dbLocal.Execute "DROP TABLE AUX_VISITA_PROMOTOR"
37            dbLocal.Execute "DROP TABLE aux_Visita_Agenda"
38            Resume
39        ElseIf Err = 3022 Then
              ' Registro ja existe (Faz Update)
40            Resume Next
41        ElseIf Err = 3078 Then 'Mandou atualiza��o de promotor mais n�o mandou a carga para criar as tabelas.
42            MsgBox "Esta mensagem � apenas para notifica-lo que seu sistema n�o esta atualizado adequadamente, " & vbCrLf & _
              "por favor envie esta mensagem para o Help Desk. " & vbCrLf & _
              "O processo de atualiza��o di�ria continuar� normalmente." & vbCrLf & _
              "Rotina: ATU_VISITA_PROMOTOR.", vbInformation, ":: Aten��o ::"
43            ATU_VISITA_PROMOTOR = True
44            Exit Function
45        Else
46            ATU_VISITA_PROMOTOR = False
47            Screen.MousePointer = 1
48            MsgBox "Sub ATU_VISITA_PROMOTOR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
49        End If
End Function

Public Function ATU_CLIENTE_CARACTERISTICA() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CLIENTE_CARACTERISTICA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLIENTE_CARACTERISTICA "
4         dbExt.Execute strSQL
                
5                  strSQL = " UPDATE CLIENTE_CARACTERISTICA INNER JOIN AUX_CLIENTE_CARACTERISTICA ON "
6         strSQL = strSQL & " CLIENTE_CARACTERISTICA.CARACTERISTICA = AUX_CLIENTE_CARACTERISTICA.CARACTERISTICA  "
7         strSQL = strSQL & " SET "
8         strSQL = strSQL & " CLIENTE_CARACTERISTICA.DESC_CARACTERISTICA = AUX_CLIENTE_CARACTERISTICA.DESC_CARACTERISTICA, "
9         strSQL = strSQL & " CLIENTE_CARACTERISTICA.FL_DESC_AUTOMATICO = AUX_CLIENTE_CARACTERISTICA.FL_DESC_AUTOMATICO, "
10        strSQL = strSQL & " CLIENTE_CARACTERISTICA.FL_RED_COMIS = AUX_CLIENTE_CARACTERISTICA.FL_RED_COMIS "
11        dbLocal.Execute strSQL
12        vRegs = dbLocal.RecordsAffected
          
13        strSQL = "INSERT INTO CLIENTE_CARACTERISTICA SELECT * FROM AUX_CLIENTE_CARACTERISTICA"
14        dbLocal.Execute strSQL
15        vRegs = vRegs + dbLocal.RecordsAffected
          
16        dbLocal.Execute "DROP TABLE AUX_CLIENTE_CARACTERISTICA"
          
17        ATU_CLIENTE_CARACTERISTICA = True

18        Controle "CLIE_CAR", vRegs

19    Exit Function

VerErro:

20        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
21            dbLocal.Execute "DROP TABLE AUX_CLIENTE_CARACTERISTICA"
22            Resume
23        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
24            Resume Next
25        Else
26            ATU_CLIENTE_CARACTERISTICA = False
27            Screen.MousePointer = 1
28            MsgBox "Sub ATU_CLIENTE_CARACTERISTICA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
29        End If
          
End Function
Public Function ATU_PEDNOTA_VENDA() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_PEDNOTA_VENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM PEDNOTA_VENDA "
4     dbExt.Execute strSQL

5     strSQL = " UPDATE PEDNOTA_VENDA INNER JOIN AUX_PEDNOTA_VENDA ON "
6     strSQL = strSQL & " PEDNOTA_VENDA.COD_LOJA = AUX_PEDNOTA_VENDA.COD_LOJA AND"
7     strSQL = strSQL & " PEDNOTA_VENDA.NUM_PEDIDO = AUX_PEDNOTA_VENDA.NUM_PEDIDO And "
8     strSQL = strSQL & " PEDNOTA_VENDA.SEQ_PEDIDO = AUX_PEDNOTA_VENDA.SEQ_PEDIDO "
9     strSQL = strSQL & " SET "
10    strSQL = strSQL & " PEDNOTA_VENDA.COD_LOJA_NOTA = AUX_PEDNOTA_VENDA.COD_LOJA_NOTA, "
11    strSQL = strSQL & " PEDNOTA_VENDA.NUM_NOTA = AUX_PEDNOTA_VENDA.NUM_NOTA, "
12    strSQL = strSQL & " PEDNOTA_VENDA.NUM_PENDENTE = AUX_PEDNOTA_VENDA.NUM_PENDENTE, "
13    strSQL = strSQL & " PEDNOTA_VENDA.COD_FILIAL = AUX_PEDNOTA_VENDA.COD_FILIAL , "
14    strSQL = strSQL & " PEDNOTA_VENDA.TP_PEDIDO = AUX_PEDNOTA_VENDA.TP_PEDIDO, "
15    strSQL = strSQL & " PEDNOTA_VENDA.TP_DPKBLAU = AUX_PEDNOTA_VENDA.TP_DPKBLAU, "
16    strSQL = strSQL & " PEDNOTA_VENDA.TP_TRANSACAO = AUX_PEDNOTA_VENDA.TP_TRANSACAO, "
17    strSQL = strSQL & " PEDNOTA_VENDA.DT_DIGITACAO = AUX_PEDNOTA_VENDA.DT_DIGITACAO, "
18    strSQL = strSQL & " PEDNOTA_VENDA.DT_PEDIDO = AUX_PEDNOTA_VENDA.DT_PEDIDO, "
19    strSQL = strSQL & " PEDNOTA_VENDA.DT_EMISSAO_NOTA = AUX_PEDNOTA_VENDA.DT_EMISSAO_NOTA, "
20    strSQL = strSQL & " PEDNOTA_VENDA.DT_SSM = AUX_PEDNOTA_VENDA.DT_SSM, "
21    strSQL = strSQL & " PEDNOTA_VENDA.DT_BLOQ_POLITICA = AUX_PEDNOTA_VENDA.DT_BLOQ_POLITICA, "
22    strSQL = strSQL & " PEDNOTA_VENDA.DT_BLOQ_CREDITO = AUX_PEDNOTA_VENDA.DT_BLOQ_CREDITO,"
23    strSQL = strSQL & " PEDNOTA_VENDA.DT_BLOQ_FRETE = AUX_PEDNOTA_VENDA.DT_BLOQ_FRETE, "
24    strSQL = strSQL & " PEDNOTA_VENDA.COD_NOPE= AUX_PEDNOTA_VENDA.COD_NOPE, "
25    strSQL = strSQL & " PEDNOTA_VENDA.COD_CLIENTE = AUX_PEDNOTA_VENDA.COD_CLIENTE, "
26    strSQL = strSQL & " PEDNOTA_VENDA.COD_FORNECEDOR = AUX_PEDNOTA_VENDA.COD_FORNECEDOR, "
27    strSQL = strSQL & " PEDNOTA_VENDA.COD_END_ENTREGA = AUX_PEDNOTA_VENDA.COD_END_ENTREGA, "
28    strSQL = strSQL & " PEDNOTA_VENDA.COD_END_COBRANCA = AUX_PEDNOTA_VENDA.COD_END_COBRANCA, "
29    strSQL = strSQL & " PEDNOTA_VENDA.COD_TRANSP = AUX_PEDNOTA_VENDA.COD_TRANSP, "
30    strSQL = strSQL & " PEDNOTA_VENDA.COD_REPRES = AUX_PEDNOTA_VENDA.COD_REPRES, "
31    strSQL = strSQL & " PEDNOTA_VENDA.COD_BANCO = AUX_PEDNOTA_VENDA.COD_BANCO, "
32    strSQL = strSQL & " PEDNOTA_VENDA.COD_VEND = AUX_PEDNOTA_VENDA.COD_VEND, "
33    strSQL = strSQL & " PEDNOTA_VENDA.COD_PLANO = AUX_PEDNOTA_VENDA.COD_PLANO, "
34    strSQL = strSQL & " PEDNOTA_VENDA.COD_CFO_DEST = AUX_PEDNOTA_VENDA.COD_CFO_DEST, "
35    strSQL = strSQL & " PEDNOTA_VENDA.COD_CFO_OPER = AUX_PEDNOTA_VENDA.COD_CFO_OPER, "
36    strSQL = strSQL & " PEDNOTA_VENDA.FRETE_PAGO = AUX_PEDNOTA_VENDA.FRETE_PAGO,"
37    strSQL = strSQL & " PEDNOTA_VENDA.PESO_BRUTO = AUX_PEDNOTA_VENDA.PESO_BRUTO, "
38    strSQL = strSQL & " PEDNOTA_VENDA.QTD_SSM = AUX_PEDNOTA_VENDA.QTD_SSM, "
39    strSQL = strSQL & " PEDNOTA_VENDA.QTD_ITEM_PEDIDO = AUX_PEDNOTA_VENDA.QTD_ITEM_PEDIDO, "
40    strSQL = strSQL & " PEDNOTA_VENDA.QTD_ITEM_NOTA = AUX_PEDNOTA_VENDA.QTD_ITEM_NOTA, "
41    strSQL = strSQL & " PEDNOTA_VENDA.VL_CONTABIL = AUX_PEDNOTA_VENDA.VL_CONTABIL, "
42    strSQL = strSQL & " PEDNOTA_VENDA.VL_IPI = AUX_PEDNOTA_VENDA.VL_IPI, "
43    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASEICM1 = AUX_PEDNOTA_VENDA.VL_BASEICM2, "
44    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASE_1 = AUX_PEDNOTA_VENDA.VL_BASE_1, "
45    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASE_2 = AUX_PEDNOTA_VENDA.VL_BASE_2, "
46    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASE_3 = AUX_PEDNOTA_VENDA.VL_BASE_3, "
47    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASEISEN = AUX_PEDNOTA_VENDA.VL_BASEISEN, "
48    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASE_5 = AUX_PEDNOTA_VENDA.VL_BASE_5, "
49    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASE_6 = AUX_PEDNOTA_VENDA.VL_BASE_6, "
50    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASEOUTR = AUX_PEDNOTA_VENDA.VL_BASEOUTR,"
51    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASEMAJ = AUX_PEDNOTA_VENDA.VL_BASEMAJ, "
52    strSQL = strSQL & " PEDNOTA_VENDA.VL_ICMRETIDO= AUX_PEDNOTA_VENDA.VL_ICMRETIDO, "
53    strSQL = strSQL & " PEDNOTA_VENDA.VL_BASEIPI = AUX_PEDNOTA_VENDA.VL_BASEIPI, "
54    strSQL = strSQL & " PEDNOTA_VENDA.VL_BISENIPI = AUX_PEDNOTA_VENDA.VL_BISENIPI, "
55    strSQL = strSQL & " PEDNOTA_VENDA.VL_BOUTRIPI = AUX_PEDNOTA_VENDA.VL_BOUTRIPI, "
56    strSQL = strSQL & " PEDNOTA_VENDA.VL_FRETE = AUX_PEDNOTA_VENDA.VL_FRETE, "
57    strSQL = strSQL & " PEDNOTA_VENDA.VL_DESP_ACESS = AUX_PEDNOTA_VENDA.VL_DESP_ACESS, "
58    strSQL = strSQL & " PEDNOTA_VENDA.PC_DESCONTO = AUX_PEDNOTA_VENDA.PC_DESCONTO, "
59    strSQL = strSQL & " PEDNOTA_VENDA.PC_DESC_SUFRAMA = AUX_PEDNOTA_VENDA.PC_DESC_SUFRAMA, "
60    strSQL = strSQL & " PEDNOTA_VENDA.PC_ACRESCIMO = AUX_PEDNOTA_VENDA.PC_ACRESCIMO, "
61    strSQL = strSQL & " PEDNOTA_VENDA.PC_SEGURO = AUX_PEDNOTA_VENDA.PC_SEGURO, "
62    strSQL = strSQL & " PEDNOTA_VENDA.PC_ICM1 = AUX_PEDNOTA_VENDA.PC_ICM1, "
63    strSQL = strSQL & " PEDNOTA_VENDA.PC_ICM2 = AUX_PEDNOTA_VENDA.PC_ICM2, "
64    strSQL = strSQL & " PEDNOTA_VENDA.PC_ALIQ_INTERNA = AUX_PEDNOTA_VENDA.PC_ALIQ_INTERNA,"
65    strSQL = strSQL & " PEDNOTA_VENDA.COD_CANCEL = AUX_PEDNOTA_VENDA.COD_CANCEL, "
66    strSQL = strSQL & " PEDNOTA_VENDA.FL_GER_SSM = AUX_PEDNOTA_VENDA.FL_GER_SSM, "
67    strSQL = strSQL & " PEDNOTA_VENDA.FL_GER_NFIS = AUX_PEDNOTA_VENDA.FL_GER_NFIS, "
68    strSQL = strSQL & " PEDNOTA_VENDA.FL_PENDENCIA = AUX_PEDNOTA_VENDA.FL_PENDENCIA, "
69    strSQL = strSQL & " PEDNOTA_VENDA.FL_DESP_ACESS = AUX_PEDNOTA_VENDA.FL_DESP_ACESS, "
70    strSQL = strSQL & " PEDNOTA_VENDA.FL_DIF_ICM = AUX_PEDNOTA_VENDA.FL_DIF_ICM, "
71    strSQL = strSQL & " PEDNOTA_VENDA.SITUACAO = AUX_PEDNOTA_VENDA.SITUACAO, "
72    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_PROTESTO = AUX_PEDNOTA_VENDA.BLOQ_PROTESTO, "
73    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_JUROS = AUX_PEDNOTA_VENDA.BLOQ_JUROS, "
74    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_CHEQUE = AUX_PEDNOTA_VENDA.BLOQ_CHEQUE, "
75    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_COMPRA = AUX_PEDNOTA_VENDA.BLOQ_COMPRA, "
76    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_DUPLICATA = AUX_PEDNOTA_VENDA.BLOQ_DUPLICATA, "
77    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_LIMITE = AUX_PEDNOTA_VENDA.BLOQ_LIMITE, "
78    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_SALDO = AUX_PEDNOTA_VENDA.BLOQ_SALDO, "
79    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_CLIE_NOVO = AUX_PEDNOTA_VENDA.BLOQ_CLIE_NOVO, "
80    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_DESCONTO = AUX_PEDNOTA_VENDA.BLOQ_DESCONTO, "
81    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_ACRESCIMO = AUX_PEDNOTA_VENDA.BLOQ_ACRESCIMO, "
82    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_VLFATMIN = AUX_PEDNOTA_VENDA.BLOQ_VLFATMIN, "
83    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_ITEM_DESC1 = AUX_PEDNOTA_VENDA.BLOQ_ITEM_DESC1, "
84    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_ITEM_DESC2 = AUX_PEDNOTA_VENDA.BLOQ_ITEM_DESC2, "
85    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_ITEM_DESC3 = AUX_PEDNOTA_VENDA.BLOQ_ITEM_DESC3, "
86    strSQL = strSQL & " PEDNOTA_VENDA.BLOQ_FRETE = AUX_PEDNOTA_VENDA.BLOQ_FRETE, "
87    strSQL = strSQL & " PEDNOTA_VENDA.MENS_PEDIDO = AUX_PEDNOTA_VENDA.MENS_PEDIDO, "
88    strSQL = strSQL & " PEDNOTA_VENDA.MENS_NOTA = AUX_PEDNOTA_VENDA.MENS_NOTA, "
89    strSQL = strSQL & " PEDNOTA_VENDA.COD_VDR = AUX_PEDNOTA_VENDA.COD_VDR "
90    dbLocal.Execute strSQL
             
91    vRegs = dbLocal.RecordsAffected
             
92    strSQL = "INSERT INTO PEDNOTA_VENDA SELECT * FROM AUX_PEDNOTA_VENDA"
93    dbLocal.Execute strSQL
94    vRegs = vRegs + dbLocal.RecordsAffected
      
95    dbLocal.Execute "DROP TABLE AUX_PEDNOTA_VENDA"

96    ATU_PEDNOTA_VENDA = True

97    Controle "PEDNOTA", vRegs

98    Exit Function

VerErro:

99    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
100       dbLocal.Execute "DROP TABLE AUX_PEDNOTA_VENDA"
101       Resume
102   ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
103       Resume Next
104   Else
105       ATU_PEDNOTA_VENDA = False
106       Screen.MousePointer = 1
108       MsgBox "Sub ATU_PEDNOTA_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
109   End If

End Function
Public Function ATU_VENDA_LIMITADA() As Boolean
1     On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_VENDA_LIMITADA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM VENDA_LIMITADA "
4         dbExt.Execute strSQL
          
5         strSQL = "delete * from VENDA_LIMITADA "
6         dbLocal.Execute strSQL
                
7         strSQL = "INSERT INTO VENDA_LIMITADA SELECT * FROM AUX_VENDA_LIMITADA "
8         dbLocal.Execute strSQL
9         vRegs = dbLocal.RecordsAffected
          
10        dbLocal.Execute "DROP TABLE AUX_VENDA_LIMITADA "
                  
11        ATU_VENDA_LIMITADA = True

12        Controle "VLIMITAD", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_VENDA_LIMITADA"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        ElseIf Err.Number <> 0 Then
20            MsgBox "Sub ATU_VENDA_LIMITADA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function
Public Function ATU_V_PEDLIQ_VENDA() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_V_PEDLIQ_VENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM V_PEDLIQ_VENDA "
4         dbExt.Execute strSQL
                
5                  strSQL = " UPDATE V_PEDLIQ_VENDA INNER JOIN AUX_V_PEDLIQ_VENDA ON "
6         strSQL = strSQL & " V_PEDLIQ_VENDA.COD_LOJA = AUX_V_PEDLIQ_VENDA.COD_LOJA AND"
7         strSQL = strSQL & " V_PEDLIQ_VENDA.NUM_PEDIDO = AUX_V_PEDLIQ_VENDA.NUM_PEDIDO And "
8         strSQL = strSQL & " V_PEDLIQ_VENDA.SEQ_PEDIDO = AUX_V_PEDLIQ_VENDA.SEQ_PEDIDO AND "
9         strSQL = strSQL & " V_PEDLIQ_VENDA.NUM_ITEM_PEDIDO = AUX_V_PEDLIQ_VENDA.NUM_ITEM_PEDIDO  "
10        strSQL = strSQL & " SET "
11        strSQL = strSQL & " V_PEDLIQ_VENDA.SITUACAO = AUX_V_PEDLIQ_VENDA.SITUACAO, "
12        strSQL = strSQL & " V_PEDLIQ_VENDA.PR_LIQUIDO = AUX_V_PEDLIQ_VENDA.PR_LIQUIDO, "
13        strSQL = strSQL & " V_PEDLIQ_VENDA.VL_LIQUIDO = AUX_V_PEDLIQ_VENDA.VL_LIQUIDO, "
14        strSQL = strSQL & " V_PEDLIQ_VENDA.PR_LIQUIDO_SUFRAMA = AUX_V_PEDLIQ_VENDA.PR_LIQUIDO_SUFRAMA , "
15        strSQL = strSQL & " V_PEDLIQ_VENDA.VL_LIQUIDO_SUFRAMA = AUX_V_PEDLIQ_VENDA.VL_LIQUIDO "
16        dbLocal.Execute strSQL
17        vRegs = dbLocal.RecordsAffected
                 
18        strSQL = "INSERT INTO V_PEDLIQ_VENDA SELECT * FROM AUX_V_PEDLIQ_VENDA"
19        dbLocal.Execute strSQL
20        vRegs = vRegs + dbLocal.RecordsAffected
              
21        dbLocal.Execute "DROP TABLE AUX_V_PEDLIQ_VENDA"
          
22        ATU_V_PEDLIQ_VENDA = True

23        Controle "V_PEDLIQ", vRegs

24    Exit Function

VerErro:

25        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
26            dbLocal.Execute "DROP TABLE AUX_V_PEDLIQ_VENDA"
27            Resume
28        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
29            Resume Next
30        Else
31            ATU_V_PEDLIQ_VENDA = False
32            Screen.MousePointer = 1
34            MsgBox "Sub ATU_V_PEDLIQ_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
35        End If

End Function
Public Function ATU_ITPEDNOTA_VENDA() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_ITPEDNOTA_VENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITPEDNOTA_VENDA "
4     dbExt.Execute strSQL
      
5     strSQL = " UPDATE ITPEDNOTA_VENDA INNER JOIN AUX_ITPEDNOTA_VENDA ON "
6     strSQL = strSQL & " ITPEDNOTA_VENDA.COD_LOJA = AUX_ITPEDNOTA_VENDA.COD_LOJA AND"
7     strSQL = strSQL & " ITPEDNOTA_VENDA.NUM_PEDIDO = AUX_ITPEDNOTA_VENDA.NUM_PEDIDO And "
8     strSQL = strSQL & " ITPEDNOTA_VENDA.SEQ_PEDIDO = AUX_ITPEDNOTA_VENDA.SEQ_PEDIDO AND"
9     strSQL = strSQL & " ITPEDNOTA_VENDA.NUM_ITEM_PEDIDO = AUX_ITPEDNOTA_VENDA.NUM_ITEM_PEDIDO "
10    strSQL = strSQL & " SET "
11    strSQL = strSQL & " ITPEDNOTA_VENDA.NUM_NOTA = AUX_ITPEDNOTA_VENDA.NUM_NOTA, "
12    strSQL = strSQL & " ITPEDNOTA_VENDA.COD_LOJA_NOTA = AUX_ITPEDNOTA_VENDA.COD_LOJA_NOTA, "
13    strSQL = strSQL & " ITPEDNOTA_VENDA.COD_DPK = AUX_ITPEDNOTA_VENDA.COD_DPK , "
14    strSQL = strSQL & " ITPEDNOTA_VENDA.QTD_SOLICITADA = AUX_ITPEDNOTA_VENDA.QTD_SOLICITADA, "
15    strSQL = strSQL & " ITPEDNOTA_VENDA.QTD_ATENDIDA = AUX_ITPEDNOTA_VENDA.QTD_ATENDIDA, "
16    strSQL = strSQL & " ITPEDNOTA_VENDA.PRECO_UNITARIO = AUX_ITPEDNOTA_VENDA.PRECO_UNITARIO, "
17    strSQL = strSQL & " ITPEDNOTA_VENDA.TABELA_VENDA = AUX_ITPEDNOTA_VENDA.TABELA_VENDA, "
18    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_DESC1 = AUX_ITPEDNOTA_VENDA.PC_DESC1, "
19    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_DESC2 = AUX_ITPEDNOTA_VENDA.PC_DESC2, "
20    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_DESC3 = AUX_ITPEDNOTA_VENDA.PC_DESC3,"
21    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_DIFICM = AUX_ITPEDNOTA_VENDA.PC_DIFICM, "
22    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_IPI= AUX_ITPEDNOTA_VENDA.PC_IPI, "
23    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_COMISS = AUX_ITPEDNOTA_VENDA.PC_COMISS, "
24    strSQL = strSQL & " ITPEDNOTA_VENDA.PC_COMISSTLMK = AUX_ITPEDNOTA_VENDA.PC_COMISSTLMK, "
25    strSQL = strSQL & " ITPEDNOTA_VENDA.COD_TRIB = AUX_ITPEDNOTA_VENDA.COD_TRIB, "
26    strSQL = strSQL & " ITPEDNOTA_VENDA.COD_TRIBIPI = AUX_ITPEDNOTA_VENDA.COD_TRIBIPI, "
27    strSQL = strSQL & " ITPEDNOTA_VENDA.SITUACAO = AUX_ITPEDNOTA_VENDA.SITUACAO "
28    dbLocal.Execute strSQL
29    vRegs = dbLocal.RecordsAffected
             
30    strSQL = "INSERT INTO ITPEDNOTA_VENDA SELECT * FROM AUX_ITPEDNOTA_VENDA"
31    dbLocal.Execute strSQL
32    vRegs = vRegs + dbLocal.RecordsAffected
              
33    dbLocal.Execute "DROP TABLE AUX_ITPEDNOTA_VENDA"

34    ATU_ITPEDNOTA_VENDA = True

35    Controle "ITPEDNOT", vRegs

36    Exit Function

VerErro:

37    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
38        dbLocal.Execute "DROP TABLE AUX_ITPEDNOTA_VENDA"
39        Resume
40    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
41        Resume Next
42    Else
43        ATU_ITPEDNOTA_VENDA = False
44        Screen.MousePointer = 1
45        MsgBox "Sub ATU_ITPEDNOTA_VENDA - AD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
46    End If
End Function


Public Function ATU_DATAS() As Boolean

          Dim MyRec As Recordset
          
1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DATAS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DATAS "
4         dbExt.Execute strSQL

5         Set MyRec = dbLocal.OpenRecordset("AUX_DATAS")
6         MyRec.MoveLast
          
7         If MyRec.RecordCount > 0 Then
8             strSQL = "DELETE FROM DATAS "
9             dbLocal.Execute strSQL
          
10            strSQL = "INSERT INTO DATAS SELECT * FROM AUX_DATAS"
11            dbLocal.Execute strSQL
12            vRegs = dbLocal.RecordsAffected
13        End If
          
14        MyRec.Close
          
15        strSQL = "DELETE DOLAR_DIARIO.* FROM DOLAR_DIARIO INNER JOIN DATAS " & _
                   " ON DOLAR_DIARIO.DATA_USS <=  " & _
                   " DATEADD('M',-3,DATAS.DT_FATURAMENTO) "
16        dbLocal.Execute strSQL
              
17        dbLocal.Execute "DROP TABLE AUX_DATAS"
          
18        ATU_DATAS = True
            
19        Controle "DATAS", vRegs
            
20    Exit Function

VerErro:

21        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
22            dbLocal.Execute "DROP TABLE AUX_DATAS"
23            Resume
24        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
25            Resume Next
26        Else
27            ATU_DATAS = False
28            Screen.MousePointer = 1
29            MsgBox "Sub ATU_DATAS - AF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If
  Resume
End Function

Public Function ATU_DEPOSITO_VISAO() As Boolean

          Dim MyRec As Recordset
          
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_DEPOSITO_VISAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEPOSITO_VISAO "
4         dbExt.Execute strSQL

5         Set MyRec = dbLocal.OpenRecordset("AUX_DEPOSITO_VISAO")
          
6         If MyRec.BOF = False Then
7             strSQL = "DELETE FROM DEPOSITO_VISAO "
8             dbLocal.Execute strSQL
          
9             strSQL = "INSERT INTO DEPOSITO_VISAO SELECT * FROM AUX_DEPOSITO_VISAO"
10            dbLocal.Execute strSQL
11            vRegs = dbLocal.RecordsAffected
12        End If
          
13        MyRec.Close
          
14        dbLocal.Execute "DROP TABLE AUX_DEPOSITO_VISAO"
          
15        ATU_DEPOSITO_VISAO = True

16        Controle "DEP_VISA", vRegs

17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_DEPOSITO_VISAO"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_DEPOSITO_VISAO = False
25            Screen.MousePointer = 1
26            MsgBox "Sub ATU_DEPOSITO_VISAO - EW" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If

End Function

Public Function ATU_DEPOSITO() As Boolean
          Dim MyRec As Recordset

1         On Error GoTo VerErro
                
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_DEPOSITO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEPOSITO"
4         dbExt.Execute strSQL

5         Set MyRec = dbLocal.OpenRecordset("AUX_DEPOSITO")
                
6         If MyRec.BOF = False Then
7             strSQL = "DELETE FROM DEPOSITO"
8             dbLocal.Execute strSQL
                
9             strSQL = "INSERT INTO DEPOSITO SELECT * FROM AUX_DEPOSITO"
10            dbLocal.Execute strSQL
11            vRegs = dbLocal.RecordsAffected
12        End If
                
13        MyRec.Close
14        dbLocal.Execute "DROP TABLE AUX_DEPOSITO"
15        ATU_DEPOSITO = True
16        Controle "DEP", vRegs

17    Exit Function
VerErro:
18        If Err = 3010 Then
              'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_DEPOSITO"
20            Resume
21        ElseIf Err = 3022 Then
              'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_DEPOSITO = False
25            Screen.MousePointer = 1
26            MsgBox "Sub ATU_DEPOSITO - EW" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If
End Function


Public Function ATU_LOJA() As Boolean

      Dim MyRec As Recordset

1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_LOJA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM LOJA "
4     dbExt.Execute strSQL
5     Set MyRec = dbLocal.OpenRecordset("AUX_LOJA")
6     If MyRec.BOF = False Then
7         strSQL = "DELETE FROM LOJA "
8         dbLocal.Execute strSQL

9         strSQL = "INSERT INTO LOJA SELECT * FROM AUX_LOJA"
10        dbLocal.Execute strSQL
11        vRegs = dbLocal.RecordsAffected
12    End If

13    MyRec.Close

14    dbLocal.Execute "DROP TABLE AUX_LOJA"
              
15    ATU_LOJA = True

16    Controle "LOJA", vRegs

17    Exit Function

VerErro:

18    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
19        dbLocal.Execute "DROP TABLE AUX_LOJA"
20        Resume
21    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
22        Resume Next
23    Else
24        ATU_LOJA = False
25        Screen.MousePointer = 1
26        MsgBox "Sub ATU_LOJA - CB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27    End If

End Function

Public Function ATU_CANCEL_PEDNOTA() As Boolean

          Dim MyRec As Recordset
          
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CANCEL_PEDNOTA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CANCEL_PEDNOTA "
4         dbExt.Execute strSQL

5         Set MyRec = dbLocal.OpenRecordset("AUX_CANCEL_PEDNOTA")
         
6         If MyRec.BOF = False Then
          
7             strSQL = "DELETE FROM CANCEL_PEDNOTA "
8             dbLocal.Execute strSQL
               
9             strSQL = "INSERT INTO CANCEL_PEDNOTA SELECT * FROM AUX_CANCEL_PEDNOTA"
10            dbLocal.Execute strSQL
11            vRegs = 0
12        End If
          
13        MyRec.Close
          
14        dbLocal.Execute "DROP TABLE AUX_CANCEL_PEDNOTA"
          
15        ATU_CANCEL_PEDNOTA = True

16        Controle "CANCEL_P", vRegs

17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_CANCEL_PEDNOTA"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_CANCEL_PEDNOTA = False
25            Screen.MousePointer = 1
26            MsgBox "Sub ATU_CANCEL_PEDNOTA - AB" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If

End Function

Public Function ATU_SALDO_PEDIDO() As Boolean

          Dim MyRec As Recordset
          
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_SALDO_PEDIDOS IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM SALDO_PEDIDOS "
4         dbExt.Execute strSQL
5         Set MyRec = dbLocal.OpenRecordset("AUX_SALDO_PEDIDOS")
          
6         If MyRec.BOF = False Then
7             strSQL = "DELETE FROM SALDO_PEDIDOS "
8             dbLocal.Execute strSQL
          
9             strSQL = "INSERT INTO SALDO_PEDIDOS SELECT * FROM AUX_SALDO_PEDIDOS"
10            dbLocal.Execute strSQL
11            vRegs = dbLocal.RecordsAffected
12        End If
          
13        MyRec.Close
              
14        dbLocal.Execute "DROP TABLE AUX_SALDO_PEDIDOS"
                  
15        ATU_SALDO_PEDIDO = True

16        Controle "SALDO_PE", vRegs

17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_SALDO_PEDIDOS"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_SALDO_PEDIDO = False
25            Screen.MousePointer = 1
27            MsgBox "Sub ATU_SALDO_PEDIDO - AF" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If

End Function
Public Function ATU_DOLARDIARIO() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_DOLAR_DIARIO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DOLAR_DIARIO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE DOLAR_DIARIO INNER JOIN AUX_DOLAR_DIARIO ON DOLAR_DIARIO.DATA_USS = AUX_DOLAR_DIARIO.DATA_USS  " & _
          " SET DOLAR_DIARIO.VALOR_USS = AUX_DOLAR_DIARIO.VALOR_USS "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
             
8     strSQL = "INSERT INTO DOLAR_DIARIO SELECT * FROM AUX_DOLAR_DIARIO"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_DOLAR_DIARIO"
              
12    ATU_DOLARDIARIO = True

13    Controle "DOLAR_DI", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_DOLAR_DIARIO"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_DOLARDIARIO = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_DOLARDIARIO - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_R_PEDIDO_CONF() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_R_PEDIDO_CONF IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_PEDIDO_CONF "
4     dbExt.Execute strSQL
          
5     strSQL = "UPDATE R_PEDIDO_CONF INNER JOIN AUX_R_PEDIDO_CONF ON " & _
               " R_PEDIDO_CONF.COD_LOJA = AUX_R_PEDIDO_CONF.COD_LOJA AND  " & _
               " R_PEDIDO_CONF.NUM_PEDIDO=AUX_R_PEDIDO_CONF.NUM_PEDIDO AND " & _
               " R_PEDIDO_CONF.SEQ_PEDIDO=AUX_R_PEDIDO_CONF.SEQ_PEDIDO AND " & _
               " R_PEDIDO_CONF.NUM_CAIXA=AUX_R_PEDIDO_CONF.NUM_CAIXA " & _
               " SET R_PEDIDO_CONF.COD_CONFERENTE = AUX_R_PEDIDO_CONF.COD_CONFERENTE,  " & _
               " R_PEDIDO_CONF.COD_EXPEDIDOR=AUX_R_PEDIDO_CONF.COD_EXPEDIDOR, " & _
               " R_PEDIDO_CONF.FL_PEND_ETIQ=AUX_R_PEDIDO_CONF.FL_PEND_ETIQ, " & _
               " R_PEDIDO_CONF.QTD_VOLUME_PARC=AUX_R_PEDIDO_CONF.QTD_VOLUME_PARC "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
             
8     strSQL = "INSERT INTO R_PEDIDO_CONF SELECT * FROM AUX_R_PEDIDO_CONF"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_R_PEDIDO_CONF"
              
12    ATU_R_PEDIDO_CONF = True

13    Controle "R_PED_CO", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_R_PEDIDO_CONF"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_R_PEDIDO_CONF = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_R_PEDIDO_CONF - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function
Public Function ATU_ROMANEIO() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0
3     strSQL = "SELECT * INTO AUX_ROMANEIO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ROMANEIO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ROMANEIO INNER JOIN AUX_ROMANEIO ON " & _
               " ROMANEIO.COD_LOJA = AUX_ROMANEIO.COD_LOJA AND  " & _
               " ROMANEIO.NUM_PEDIDO=AUX_ROMANEIO.NUM_PEDIDO AND " & _
               " ROMANEIO.SEQ_PEDIDO=AUX_ROMANEIO.SEQ_PEDIDO " & _
               " SET ROMANEIO.DT_COLETA = AUX_ROMANEIO.DT_COLETA,  " & _
               " ROMANEIO.DT_DESPACHO=AUX_ROMANEIO.DT_DESPACHO, " & _
               " ROMANEIO.NUM_ROMANEIO=AUX_ROMANEIO.NUM_ROMANEIO, " & _
               " ROMANEIO.NUM_CONHECIMENTO=AUX_ROMANEIO.NUM_CONHECIMENTO,  " & _
               " ROMANEIO.NUM_CARRO=AUX_ROMANEIO.NUM_CARRO,  " & _
               " ROMANEIO.QTD_VOLUME=AUX_ROMANEIO.QTD_VOLUME  "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO ROMANEIO SELECT * FROM AUX_ROMANEIO"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_ROMANEIO"
              
12    ATU_ROMANEIO = True

13    Controle "ROMANEIO", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_ROMANEIO"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_ROMANEIO = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_ROMANEIO - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_EMBALADOR() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_EMBALADOR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM EMBALADOR "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE EMBALADOR INNER JOIN AUX_EMBALADOR ON " & _
               " EMBALADOR.COD_EMBAL = AUX_EMBALADOR.COD_EMBAL  " & _
               " SET EMBALADOR.NOME_EMBAL = AUX_EMBALADOR.NOME_EMBAL "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO EMBALADOR SELECT * FROM AUX_EMBALADOR"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected

11    dbLocal.Execute "DROP TABLE AUX_EMBALADOR"
              
12    ATU_EMBALADOR = True

13    Controle "EMBALAD", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_EMBALADOR"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_EMBALADOR = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_EMBALADOR - AG" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_CESTAVENDA() As Boolean

1     On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_CESTAVENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CESTA_VENDA "
4         dbExt.Execute strSQL
          
5         strSQL = "delete * from CESTA_VENDA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO CESTA_VENDA SELECT * FROM AUX_CESTAVENDA "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_CESTAVENDA "
                  
12        ATU_CESTAVENDA = True

13        Controle "CESTAV", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_CESTAVENDA"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        ElseIf Err.Number <> 0 Then
21            MsgBox "Sub ATU_CESTAVENDA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function

Public Function ATU_CESTAITEM() As Boolean

1     On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_CESTAITEM IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CESTA_ITEM "
4         dbExt.Execute strSQL
          
5         strSQL = "delete * from CESTA_ITEM "
6         dbLocal.Execute strSQL
                
7         strSQL = "INSERT INTO CESTA_ITEM SELECT * FROM AUX_CESTAITEM "
8         dbLocal.Execute strSQL
9         vRegs = dbLocal.RecordsAffected
          
10        dbLocal.Execute "DROP TABLE AUX_CESTAITEM "
                  
11        ATU_CESTAITEM = True

12        Controle "CESTAI", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_CESTAITEM"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        ElseIf Err.Number <> 0 Then
20            MsgBox "Sub ATU_CESTAITEM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function

Public Function ATU_DUPLICATA() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0
3     strSQL = "SELECT * INTO AUX_DUPLICATA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DUPLICATAS "
4     dbExt.Execute strSQL

5     strSQL = "delete * from DUPLICATAS "
6     dbLocal.Execute strSQL
            
      'Efetua Update de uma tabela com base na outra .......................
      'strSQL = "UPDATE DUPLICATAS INNER JOIN AUX_DUPLICATA ON "
      'strSQL = strSQL & " DUPLICATAS.NUM_FATURA = AUX_DUPLICATA.NUM_FATURA AND "
      'strSQL = strSQL & " DUPLICATAS.NUM_ORDEM = AUX_DUPLICATA.NUM_ORDEM  AND "
      'strSQL = strSQL & " DUPLICATAS.COD_LOJA = AUX_DUPLICATA.COD_LOJA "
      'strSQL = strSQL & " SET DUPLICATAS.SITUACAO_PAGTO = AUX_DUPLICATA.SITUACAO_PAGTO, "
      'strSQL = strSQL & " DUPLICATAS.TP_DUPLICATA= AUX_DUPLICATA.TP_DUPLICATA, "
      'strSQL = strSQL & " DUPLICATAS.COD_CLIENTE = AUX_DUPLICATA.COD_CLIENTE, "
      'strSQL = strSQL & " DUPLICATAS.COD_BANCO = AUX_DUPLICATA.COD_BANCO, "
      'strSQL = strSQL & " DUPLICATAS.TP_PAGAMENTO = AUX_DUPLICATA.TP_PAGAMENTO , "
      'strSQL = strSQL & " DUPLICATAS.DT_VENCIMENTO = AUX_DUPLICATA.DT_VENCIMENTO, "
      'strSQL = strSQL & " DUPLICATAS.VL_ABERTO = AUX_DUPLICATA.VL_ABERTO, "
      'strSQL = strSQL & " DUPLICATAS.NUM_DUPL_BANCO = AUX_DUPLICATA.NUM_DUPL_BANCO "
      'dbLocal.Execute strSQL
             
      ' Com base na tabela AUX, inclui registros na tabela Original .........
7     strSQL = "INSERT INTO DUPLICATAS SELECT * FROM AUX_DUPLICATA "
8     dbLocal.Execute strSQL
9     vRegs = dbLocal.RecordsAffected

10    dbLocal.Execute "DROP TABLE AUX_DUPLICATA "
              
11    ATU_DUPLICATA = True

12    Controle "DUPLICAT", vRegs

13    Exit Function

VerErro:

14    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
15        dbLocal.Execute "DROP TABLE AUX_DUPLICATA"
16        Resume
17    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
18        Resume Next
19    Else
20        ATU_DUPLICATA = False
21        Screen.MousePointer = 1
23        MsgBox "Sub ATU_DUPLICATA - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_ALIQUOTA_ME() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ALIQUOTA_ME IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ALIQUOTA_ME "
4     dbExt.Execute strSQL

5     strSQL = "delete * from ALIQUOTA_ME "
6     dbLocal.Execute strSQL
            
7     strSQL = "INSERT INTO ALIQUOTA_ME SELECT * FROM AUX_ALIQUOTA_ME "
8     dbLocal.Execute strSQL
9     vRegs = dbLocal.RecordsAffected
      
10    dbLocal.Execute "DROP TABLE AUX_ALIQUOTA_ME "
              
11    ATU_ALIQUOTA_ME = True

12    Controle "ALIQUOTA", vRegs

13    Exit Function

VerErro:

14    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
15        dbLocal.Execute "DROP TABLE AUX_ALIQUOTA_ME"
16        Resume
17    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
18        Resume Next
19    Else
20        ATU_ALIQUOTA_ME = False
21        Screen.MousePointer = 1
22        MsgBox "Sub ATU_ALIQUOTA_ME" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23    End If

End Function
Public Function ATU_V_CLIENTE_FIEL() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_V_CLIENTE_FIEL IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM V_CLIENTE_FIEL "
4     dbExt.Execute strSQL

5     strSQL = "delete * from V_CLIENTE_FIEL "
6     dbLocal.Execute strSQL
            
7     strSQL = "INSERT INTO V_CLIENTE_FIEL SELECT * FROM AUX_V_CLIENTE_FIEL "
8     dbLocal.Execute strSQL
9     vRegs = dbLocal.RecordsAffected
      
10    dbLocal.Execute "DROP TABLE AUX_V_CLIENTE_FIEL "
              
11    ATU_V_CLIENTE_FIEL = True

12    Controle "VCLIFIEL", vRegs

13    Exit Function

VerErro:

14    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
15        dbLocal.Execute "DROP TABLE AUX_V_CLIENTE_FIEL"
16        Resume
17    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
18        Resume Next
19    Else
20        ATU_V_CLIENTE_FIEL = False
21        Screen.MousePointer = 1
22        MsgBox "Sub ATU_V_CLIENTE_FIEL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23    End If

End Function


Public Function ATU_FRETE_ENTREGA() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_FRETE_ENTREGA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FRETE_ENTREGA "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FRETE_ENTREGA INNER JOIN AUX_FRETE_ENTREGA ON "
6     strSQL = strSQL & " FRETE_ENTREGA.COD_LOJA = AUX_FRETE_ENTREGA.COD_LOJA AND "
7     strSQL = strSQL & " FRETE_ENTREGA.COD_TRANSP = AUX_FRETE_ENTREGA.COD_TRANSP  AND "
8     strSQL = strSQL & " FRETE_ENTREGA.COD_UF = AUX_FRETE_ENTREGA.COD_UF AND "
9     strSQL = strSQL & " FRETE_ENTREGA.COD_REGIAO = AUX_FRETE_ENTREGA.COD_REGIAO AND "
10    strSQL = strSQL & " FRETE_ENTREGA.COD_SUBREGIAO = AUX_FRETE_ENTREGA.COD_SUBREGIAO  AND "
11    strSQL = strSQL & " FRETE_ENTREGA.COD_CIDADE = AUX_FRETE_ENTREGA.COD_CIDADE "
12    strSQL = strSQL & " SET FRETE_ENTREGA.VL_FRETE_ENTREGA = AUX_FRETE_ENTREGA.VL_FRETE_ENTREGA, "
13    strSQL = strSQL & " FRETE_ENTREGA.SITUACAO= AUX_FRETE_ENTREGA.SITUACAO "
14    dbLocal.Execute strSQL
15    vRegs = dbLocal.RecordsAffected
             
16    strSQL = "INSERT INTO FRETE_ENTREGA SELECT * FROM AUX_FRETE_ENTREGA "
17    dbLocal.Execute strSQL
18    vRegs = vRegs + dbLocal.RecordsAffected
        
19    dbLocal.Execute "DROP TABLE AUX_FRETE_ENTREGA "
              
20    ATU_FRETE_ENTREGA = True

21    Controle "FRETE_EN", vRegs

22    Exit Function

VerErro:

23    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
24        dbLocal.Execute "DROP TABLE AUX_FRETE_ENTREGA"
25        Resume
26    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
27        Resume Next
28    Else
29        ATU_FRETE_ENTREGA = False
30        Screen.MousePointer = 1
32        MsgBox "Sub ATU_FRETE_ENTREGA - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
33    End If

End Function

Public Function ATU_FRETE_UF() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_FRETE_UF IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FRETE_UF "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FRETE_UF INNER JOIN AUX_FRETE_UF ON "
6     strSQL = strSQL & " FRETE_UF.COD_LOJA = AUX_FRETE_UF.COD_LOJA AND "
7     strSQL = strSQL & " FRETE_UF.COD_UF = AUX_FRETE_UF.COD_UF AND "
8     strSQL = strSQL & " FRETE_UF.COD_REGIAO = AUX_FRETE_UF.COD_REGIAO AND "
9     strSQL = strSQL & " FRETE_UF.COD_SUBREGIAO = AUX_FRETE_UF.COD_SUBREGIAO  AND "
10    strSQL = strSQL & " FRETE_UF.COD_CIDADE = AUX_FRETE_UF.COD_CIDADE "
11    strSQL = strSQL & " SET FRETE_UF.VL_FRETE = AUX_FRETE_UF.VL_FRETE "
12    dbLocal.Execute strSQL
13    vRegs = dbLocal.RecordsAffected
      
14    strSQL = "INSERT INTO FRETE_UF SELECT * FROM AUX_FRETE_UF "
15    dbLocal.Execute strSQL
16    vRegs = vRegs + dbLocal.RecordsAffected
      
17    dbLocal.Execute "DROP TABLE AUX_FRETE_UF "
              
18    ATU_FRETE_UF = True

19    Controle "FRETE_UF", vRegs
        
20    Exit Function

VerErro:

21    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
22        dbLocal.Execute "DROP TABLE AUX_FRETE_UF"
23        Resume
24    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
25        Resume Next
26    Else
27        ATU_FRETE_UF = False
28        Screen.MousePointer = 1
30        MsgBox "Sub ATU_FRETE_UF - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
31    End If

End Function

Public Function ATU_FRETE_UF_TRANSP() As Boolean

1     On Error GoTo VerErro

2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_FRETE_UF_TRANSP IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FRETE_UF_TRANSP "
4         dbExt.Execute strSQL
            
5                  strSQL = "UPDATE FRETE_UF_TRANSP INNER JOIN AUX_FRETE_UF_TRANSP ON "
6         strSQL = strSQL & " FRETE_UF_TRANSP.COD_LOJA = AUX_FRETE_UF_TRANSP.COD_LOJA AND "
7         strSQL = strSQL & " FRETE_UF_TRANSP.COD_TRANSP = AUX_FRETE_UF_TRANSP.COD_TRANSP AND "
8         strSQL = strSQL & " FRETE_UF_TRANSP.COD_UF = AUX_FRETE_UF_TRANSP.COD_UF "
9         strSQL = strSQL & " SET FRETE_UF_TRANSP.VL_FRETE = AUX_FRETE_UF_TRANSP.VL_FRETE "
10        dbLocal.Execute strSQL
11        vRegs = dbLocal.RecordsAffected
          
12        strSQL = "INSERT INTO FRETE_UF_TRANSP SELECT * FROM AUX_FRETE_UF_TRANSP "
13        dbLocal.Execute strSQL
14        vRegs = vRegs + dbLocal.RecordsAffected
              
15        dbLocal.Execute "DROP TABLE AUX_FRETE_UF_TRANSP "
                  
16        ATU_FRETE_UF_TRANSP = True

17        Controle "FRETEUFT", vRegs

18    Exit Function

VerErro:

19        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
20            dbLocal.Execute "DROP TABLE AUX_FRETE_UF_TRANSP"
21            Resume
22        ElseIf Err = 3022 Then
             ' Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_FRETE_UF_TRANSP = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_FRETE_UF_TRANSP - ME" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If

End Function

Public Function ATU_FRETE_UF_BLOQ() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_FRETE_UF_BLOQ IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FRETE_UF_BLOQ "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FRETE_UF_BLOQ INNER JOIN AUX_FRETE_UF_BLOQ ON "
6     strSQL = strSQL & " FRETE_UF_BLOQ.COD_UF = AUX_FRETE_UF_BLOQ.COD_UF AND "
7     strSQL = strSQL & " FRETE_UF_BLOQ.COD_TRANSP = AUX_FRETE_UF_BLOQ.COD_TRANSP "
8     strSQL = strSQL & " SET FRETE_UF_BLOQ.FL_BLOQUEIO = AUX_FRETE_UF_BLOQ.FL_BLOQUEIO "
9     dbLocal.Execute strSQL
10    vRegs = dbLocal.RecordsAffected
             
11    strSQL = "INSERT INTO FRETE_UF_BLOQ SELECT * FROM AUX_FRETE_UF_BLOQ "
12    dbLocal.Execute strSQL
13    vRegs = vRegs + dbLocal.RecordsAffected
      
14    dbLocal.Execute "DROP TABLE AUX_FRETE_UF_BLOQ "
              
15    ATU_FRETE_UF_BLOQ = True

16    Controle "FRETEUFB", vRegs

17    Exit Function

VerErro:

18    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
19        dbLocal.Execute "DROP TABLE AUX_FRETE_UF_BLOQ"
20        Resume
21    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
22        Resume Next
23    Else
24        ATU_FRETE_UF_BLOQ = False
25        Screen.MousePointer = 1
26        MsgBox "Sub ATU_FRETE_UF_BLOQ - AH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27    End If

End Function

Public Function ATU_ESTAITEM() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ESTAITEM IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ESTATISTICA_ITEM "
4     dbExt.Execute strSQL

5     If dbExt.RecordsAffected > 0 Then
6         strSQL = "DELETE ESTATISTICA_ITEM.* FROM ESTATISTICA_ITEM "
7         dbLocal.Execute strSQL
8     End If
             
9     strSQL = "INSERT INTO ESTATISTICA_ITEM SELECT * FROM AUX_ESTAITEM "
10    dbLocal.Execute strSQL
11    vRegs = dbLocal.RecordsAffected
      
12    dbLocal.Execute "DROP TABLE AUX_ESTAITEM "
              
13    ATU_ESTAITEM = True

14    Controle "ESTAT_IT", vRegs

15    Exit Function

VerErro:

16    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
17        dbLocal.Execute "DROP TABLE AUX_ESTAITEM"
18        Resume
19    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
20        Resume Next
21    Else
22        ATU_ESTAITEM = False
23        Screen.MousePointer = 1
24        MsgBox "Sub ATU_ESTAITEM - AI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function

Public Function ATU_FILIAL() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_FILIAL IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FILIAL "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FILIAL INNER JOIN AUX_FILIAL ON FILIAL.COD_FILIAL = AUX_FILIAL.COD_FILIAL  " & _
          " SET FILIAL.SIGLA = AUX_FILIAL.SIGLA, FILIAL.TP_FILIAL = AUX_FILIAL.TP_FILIAL, " & _
          " FILIAL.COD_FRANQUEADOR = AUX_FILIAL.COD_FRANQUEADOR, FILIAL.COD_REGIONAL = AUX_FILIAL.COD_REGIONAL  "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
             
8     strSQL = "INSERT INTO FILIAL SELECT * FROM AUX_FILIAL"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_FILIAL"
              
12    ATU_FILIAL = True

13    Controle "FILIAL", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_FILIAL"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_FILIAL = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_FILIAL - FI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_FILIAL_FRANQUIA() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_FILIAL_FRANQUIA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FILIAL_FRANQUIA "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FILIAL_FRANQUIA INNER JOIN AUX_FILIAL_FRANQUIA ON FILIAL_FRANQUIA.COD_FILIAL = AUX_FILIAL_FRANQUIA.COD_FILIAL  " & _
          " SET FILIAL_FRANQUIA.SEQ_BATCH = AUX_FILIAL_FRANQUIA.SEQ_BATCH"
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO FILIAL_FRANQUIA SELECT * FROM AUX_FILIAL_FRANQUIA"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_FILIAL_FRANQUIA"
              
12    ATU_FILIAL_FRANQUIA = True

13    Controle "FIL_FRA", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_FILIAL_FRANQUIA"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_FILIAL_FRANQUIA = False
22        Screen.MousePointer = 1
24        MsgBox "Sub ATU_FILIAL_FRANQUIA - FI" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function

Public Function ATU_FORNECEDOR() As Boolean

1     On Error GoTo VerErro

2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_FORNECEDOR IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FORNECEDOR "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE FORNECEDOR INNER JOIN AUX_FORNECEDOR ON FORNECEDOR.COD_FORNECEDOR = AUX_FORNECEDOR.COD_FORNECEDOR  " & _
              " SET FORNECEDOR.SIGLA = AUX_FORNECEDOR.SIGLA "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO FORNECEDOR SELECT * FROM AUX_FORNECEDOR"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
              
11        dbLocal.Execute "DROP TABLE AUX_FORNECEDOR"
          
12        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM ESTOQUE "
13        mdiGER040.Label1.Refresh
          
14        strSQL = " DELETE * FROM ITEM_ESTOQUE WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
15        dbLocal.Execute strSQL
          
16        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM ANALITICO "
17        mdiGER040.Label1.Refresh
          
18        strSQL = " DELETE * FROM ITEM_ANALITICO WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
19        dbLocal.Execute strSQL
          
20        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM CUSTO "
21        mdiGER040.Label1.Refresh
          
22        strSQL = " DELETE * FROM ITEM_CUSTO WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
23        dbLocal.Execute strSQL
          
24        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM GLOBAL "
25        mdiGER040.Label1.Refresh
          
26        strSQL = " DELETE * FROM ITEM_GLOBAL WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
27        dbLocal.Execute strSQL
          
28        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM PRECO "
29        mdiGER040.Label1.Refresh
          
30        strSQL = " DELETE * FROM ITEM_PRECO WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
31        dbLocal.Execute strSQL
          
32        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - APLICACAO "
33        mdiGER040.Label1.Refresh
          
34        strSQL = " DELETE * FROM APLICACAO WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
35        dbLocal.Execute strSQL
          
36        mdiGER040.Label1.Caption = "Tabela em Atualiza��o: " & " Fornecedor - ITEM CADASTRO "
37        mdiGER040.Label1.Refresh
          
38        strSQL = " DELETE * FROM ITEM_CADASTRO WHERE COD_DPK IN " & _
                   " (SELECT COD_DPK FROM  item_CADASTRO, FORNECEDOR " & _
                   " Where ITEM_CADASTRO.COD_FORNECEDOR = FORNECEDOR.COD_FORNECEDOR " & _
                   " AND (DIVISAO <> 'D' OR SITUACAO <> 0 OR CLASSIFICACAO <> 'A')) "
          
39        dbLocal.Execute strSQL
          
40        strSQL = " DELETE FROM FORNECEDOR WHERE FORNECEDOR.SITUACAO <> 0 OR FORNECEDOR.DIVISAO <> 'D' OR CLASSIFICACAO <> 'A' "
41        dbLocal.Execute strSQL
          
42        ATU_FORNECEDOR = True

43        Controle "FORNECED", vRegs

44    Exit Function

VerErro:

45    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
46        dbLocal.Execute "DROP TABLE AUX_FORNECEDOR"
47        Resume
48    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
49        Resume Next
50    Else
51        ATU_FORNECEDOR = False
52        Screen.MousePointer = 1
53        MsgBox "Sub ATU_FORNECEDOR - AK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
54    End If

End Function
Public Function ATU_FORNECEDOR_ESPECIFICO() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_FORNECEDOR_ESPECIFICO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM FORNECEDOR_ESPECIFICO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE FORNECEDOR_ESPECIFICO INNER JOIN AUX_FORNECEDOR_ESPECIFICO ON " & _
               " FORNECEDOR_ESPECIFICO.COD_FORNECEDOR = AUX_FORNECEDOR_ESPECIFICO.COD_FORNECEDOR  and " & _
               " FORNECEDOR_ESPECIFICO.COD_GRUPO = AUX_FORNECEDOR_ESPECIFICO.COD_GRUPO  and " & _
               " FORNECEDOR_ESPECIFICO.COD_SUBGRUPO = AUX_FORNECEDOR_ESPECIFICO.COD_SUBGRUPO  and " & _
               " FORNECEDOR_ESPECIFICO.COD_DPK = AUX_FORNECEDOR_ESPECIFICO.COD_DPK  " & _
               " SET FORNECEDOR_ESPECIFICO.fl_dif_icms = AUX_FORNECEDOR_ESPECIFICO.fl_dif_icms,   " & _
               "     FORNECEDOR_ESPECIFICO.fl_adicional = AUX_FORNECEDOR_ESPECIFICO.fl_adicional,   " & _
               "     FORNECEDOR_ESPECIFICO.TP_DIF_ICMS = AUX_FORNECEDOR_ESPECIFICO.TP_DIF_ICMS   "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO FORNECEDOR_ESPECIFICO SELECT * FROM AUX_FORNECEDOR_ESPECIFICO"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_FORNECEDOR_ESPECIFICO"

12    ATU_FORNECEDOR_ESPECIFICO = True

13    Controle "FORN_ESP", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_FORNECEDOR_ESPECIFICO"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_FORNECEDOR_ESPECIFICO = False
22        Screen.MousePointer = 1
24        MsgBox "Sub ATU_FORNECEDOR_ESPECIFICO - HD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function

Public Function ATU_GRUPO() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_GRUPO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM GRUPO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE GRUPO INNER JOIN AUX_GRUPO ON GRUPO.COD_GRUPO = AUX_GRUPO.COD_GRUPO  " & _
          " SET GRUPO.DESC_GRUPO = AUX_GRUPO.DESC_GRUPO "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO GRUPO SELECT * FROM AUX_GRUPO"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
      
11    dbLocal.Execute "DROP TABLE AUX_GRUPO"
              
12    ATU_GRUPO = True

13    Controle "GROUP", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_GRUPO"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_GRUPO = False
22        Screen.MousePointer = 1
24        MsgBox "Sub ATU_GRUPO - AL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function

Public Function ATU_ITEMANAL() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ITEM_ANALITICO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_ANALITICO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_ANALITICO INNER JOIN AUX_ITEM_ANALITICO "
6     strSQL = strSQL & " ON ITEM_ANALITICO.COD_LOJA = AUX_ITEM_ANALITICO.COD_LOJA AND "
7     strSQL = strSQL & " ITEM_ANALITICO.COD_DPK = AUX_ITEM_ANALITICO.COD_DPK  "
8     strSQL = strSQL & " SET ITEM_ANALITICO.CATEGORIA = AUX_ITEM_ANALITICO.CATEGORIA,  "
9     strSQL = strSQL & " ITEM_ANALITICO.CLASS_ABC = AUX_ITEM_ANALITICO.CLASS_ABC,  "
10    strSQL = strSQL & " ITEM_ANALITICO.CLASS_ABCF = AUX_ITEM_ANALITICO.CLASS_ABCF,  "
11    strSQL = strSQL & " ITEM_ANALITICO.CLASS_VENDA = AUX_ITEM_ANALITICO.CLASS_VENDA,  "
12    strSQL = strSQL & " ITEM_ANALITICO.QTD_MES_ANT = AUX_ITEM_ANALITICO.QTD_MES_ANT,  "
13    strSQL = strSQL & " ITEM_ANALITICO.CUSTO_MEDIO = AUX_ITEM_ANALITICO.CUSTO_MEDIO,  "
14    strSQL = strSQL & " ITEM_ANALITICO.CUSTO_MEDIO_ANT = AUX_ITEM_ANALITICO.CUSTO_MEDIO_ANT  "
15    dbLocal.Execute strSQL
16    vRegs = dbLocal.RecordsAffected
             
17    strSQL = "INSERT INTO ITEM_ANALITICO SELECT * FROM AUX_ITEM_ANALITICO "
18    dbLocal.Execute strSQL
19    vRegs = vRegs + dbLocal.RecordsAffected
      
20    dbLocal.Execute "DROP TABLE AUX_ITEM_ANALITICO "
              
21    ATU_ITEMANAL = True

22    Controle "ITEM_ANA", vRegs

23    Exit Function

VerErro:

24    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
25        dbLocal.Execute "DROP TABLE AUX_ITEM_ANALITICO "
26        Resume
27    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
28        Resume Next
29    Else
30        ATU_ITEMANAL = False
31        Screen.MousePointer = 1
32        MsgBox "Sub ATU_ITEMANAL - DK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
33    End If

End Function
Public Function ATU_ITEMCAD() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ITEM_CADASTRO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_CADASTRO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_CADASTRO INNER JOIN AUX_ITEM_CADASTRO ON ITEM_CADASTRO.COD_DPK = AUX_ITEM_CADASTRO.COD_DPK  " & _
              " SET ITEM_CADASTRO.COD_FORNECEDOR = AUX_ITEM_CADASTRO.COD_FORNECEDOR , " & _
              " ITEM_CADASTRO.COD_FABRICA = AUX_ITEM_CADASTRO.COD_FABRICA , " & _
              " ITEM_CADASTRO.DESC_ITEM = AUX_ITEM_CADASTRO.DESC_ITEM , " & _
              " ITEM_CADASTRO.COD_LINHA = AUX_ITEM_CADASTRO.COD_LINHA , " & _
              " ITEM_CADASTRO.COD_GRUPO = AUX_ITEM_CADASTRO.COD_GRUPO , " & _
              " ITEM_CADASTRO.COD_SUBGRUPO = AUX_ITEM_CADASTRO.COD_SUBGRUPO , " & _
              " ITEM_CADASTRO.DT_CADASTRAMENTO = AUX_ITEM_CADASTRO.DT_CADASTRAMENTO , "
              
6     strSQL = strSQL & " ITEM_CADASTRO.MASCARADO = AUX_ITEM_CADASTRO.MASCARADO , " & _
              " ITEM_CADASTRO.CLASS_FISCAL = AUX_ITEM_CADASTRO.CLASS_FISCAL, " & _
              " ITEM_CADASTRO.PESO = AUX_ITEM_CADASTRO.PESO, " & _
              " ITEM_CADASTRO.VOLUME = AUX_ITEM_CADASTRO.VOLUME, " & _
              " ITEM_CADASTRO.COD_UNIDADE = AUX_ITEM_CADASTRO.COD_UNIDADE , " & _
              " ITEM_CADASTRO.COD_TRIBUTACAO = AUX_ITEM_CADASTRO.COD_TRIBUTACAO, " & _
              " ITEM_CADASTRO.COD_TRIBUTACAO_IPI = AUX_ITEM_CADASTRO.COD_TRIBUTACAO_IPI, "
              
7     strSQL = strSQL & " ITEM_CADASTRO.PC_IPI = AUX_ITEM_CADASTRO.PC_IPI, " & _
          " ITEM_CADASTRO.COD_PROCEDENCIA = AUX_ITEM_CADASTRO.COD_PROCEDENCIA, " & _
          " ITEM_CADASTRO.COD_DPK_ANT = AUX_ITEM_CADASTRO.COD_DPK_ANT, " & _
          " ITEM_CADASTRO.QTD_MINFORN = AUX_ITEM_CADASTRO.QTD_MINFORN, " & _
          " ITEM_CADASTRO.QTD_MINVDA = AUX_ITEM_CADASTRO.QTD_MINVDA "
          
8     dbLocal.Execute strSQL
9     vRegs = dbLocal.RecordsAffected
      
10    strSQL = "INSERT INTO ITEM_CADASTRO SELECT * FROM AUX_ITEM_CADASTRO "
11    dbLocal.Execute strSQL
12    vRegs = vRegs + dbLocal.RecordsAffected

13    dbLocal.Execute "DROP TABLE AUX_ITEM_CADASTRO "
              
14    ATU_ITEMCAD = True

15    Controle "ITEM_CAD", vRegs

16    Exit Function

VerErro:

17    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
18        dbLocal.Execute "DROP TABLE AUX_ITEM_CADASTRO "
19        Resume
20    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
21        Resume Next
22    Else
23        ATU_ITEMCAD = False
24        Screen.MousePointer = 1
26        MsgBox "Sub ATU_ITEMCAD - AM" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27    End If

End Function
Public Function ATU_ITEMCUSTO() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ITEM_CUSTO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_CUSTO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_CUSTO INNER JOIN AUX_ITEM_CUSTO ON ITEM_CUSTO.COD_LOJA = AUX_ITEM_CUSTO.COD_LOJA AND ITEM_CUSTO.COD_DPK = AUX_ITEM_CUSTO.COD_DPK  " & _
          " SET ITEM_CUSTO.CUSTO_REPOSICAO = AUX_ITEM_CUSTO.CUSTO_REPOSICAO "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO ITEM_CUSTO SELECT * FROM AUX_ITEM_CUSTO "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_ITEM_CUSTO "
              
12    ATU_ITEMCUSTO = True

13    Controle "ITEM_CUSTO", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_ITEM_CUSTO "
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_ITEMCUSTO = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_ITEMCUSTO - AY" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function
Public Function ATU_ITEMESTOQUE() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_ITEM_ESTOQUE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_ESTOQUE "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_ESTOQUE INNER JOIN AUX_ITEM_ESTOQUE ON ITEM_ESTOQUE.COD_LOJA = AUX_ITEM_ESTOQUE.COD_LOJA AND ITEM_ESTOQUE.COD_DPK = AUX_ITEM_ESTOQUE.COD_DPK  " & _
          " SET ITEM_ESTOQUE.QTD_ATUAL = AUX_ITEM_ESTOQUE.QTD_ATUAL , " & _
          " ITEM_ESTOQUE.QTD_RESERV = AUX_ITEM_ESTOQUE.QTD_RESERV , " & _
          " ITEM_ESTOQUE.QTD_PENDENTE = AUX_ITEM_ESTOQUE.QTD_PENDENTE, " & _
          " ITEM_ESTOQUE.QTD_MAXVDA = AUX_ITEM_ESTOQUE.QTD_MAXVDA , " & _
          " ITEM_ESTOQUE.CUEMA = AUX_ITEM_ESTOQUE.CUEMA, " & _
          " ITEM_ESTOQUE.DT_CUEMA = AUX_ITEM_ESTOQUE.DT_CUEMA, " & _
          " ITEM_ESTOQUE.VL_ULT_COMPRA = AUX_ITEM_ESTOQUE.VL_ULT_COMPRA, " & _
          " ITEM_ESTOQUE.DT_ULT_COMPRA = AUX_ITEM_ESTOQUE.DT_ULT_COMPRA, " & _
          " ITEM_ESTOQUE.SITUACAO = AUX_ITEM_ESTOQUE.SITUACAO "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
             
8     strSQL = "INSERT INTO ITEM_ESTOQUE SELECT * FROM AUX_ITEM_ESTOQUE "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_ITEM_ESTOQUE "

12    strSQL = " DELETE ITEM_ESTOQUE.* FROM ITEM_ESTOQUE " & _
              " WHERE ITEM_ESTOQUE.SITUACAO = 9"
13    dbLocal.Execute strSQL
              
14    ATU_ITEMESTOQUE = True

15    Controle "ITEM_EST", vRegs

16    Exit Function

VerErro:

17    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
18        dbLocal.Execute "DROP TABLE AUX_ITEM_ESTOQUE "
19        Resume
20    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
21        Resume Next
22    Else
23        ATU_ITEMESTOQUE = False
24        Screen.MousePointer = 1
26        MsgBox "Sub ATU_ITEMESTOQUE - AZ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27    End If
End Function
Public Function ATU_ITEMGLOBAL() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_ITEM_GLOBAL IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_GLOBAL "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_GLOBAL INNER JOIN AUX_ITEM_GLOBAL ON ITEM_GLOBAL.COD_DPK = AUX_ITEM_GLOBAL.COD_DPK  " & _
          " SET ITEM_GLOBAL.SEQUENCIA_TAB = AUX_ITEM_GLOBAL.SEQUENCIA_TAB "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO ITEM_GLOBAL SELECT * FROM AUX_ITEM_GLOBAL "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_ITEM_GLOBAL "
              
12    ATU_ITEMGLOBAL = True

13    Controle "ITEM_GLO", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_ITEM_GLOBAL "
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_ITEMGLOBAL = False
22        Screen.MousePointer = 1
24        MsgBox "Sub ATU_ITEMGLOBAL - DL" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function
Public Function ATU_ITEMPRECO() As Boolean

1     On Error GoTo VerErro
2     vRegs = 0
      
3     strSQL = "SELECT * INTO AUX_ITEM_PRECO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM ITEM_PRECO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE ITEM_PRECO INNER JOIN AUX_ITEM_PRECO ON ITEM_PRECO.COD_LOJA = AUX_ITEM_PRECO.COD_LOJA AND ITEM_PRECO.COD_DPK = AUX_ITEM_PRECO.COD_DPK  " & _
          " SET ITEM_PRECO.PRECO_VENDA = AUX_ITEM_PRECO.PRECO_VENDA , " & _
          " ITEM_PRECO.PRECO_VENDA_ANT = AUX_ITEM_PRECO.PRECO_VENDA_ANT , " & _
          " ITEM_PRECO.PRECO_OF = AUX_ITEM_PRECO.PRECO_OF, " & _
          " ITEM_PRECO.PRECO_OF_ANT = AUX_ITEM_PRECO.PRECO_OF_ANT , " & _
          " ITEM_PRECO.PRECO_SP = AUX_ITEM_PRECO.PRECO_SP, " & _
          " ITEM_PRECO.PRECO_SP_ANT = AUX_ITEM_PRECO.PRECO_SP_ANT "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected

8     strSQL = "INSERT INTO ITEM_PRECO SELECT * FROM AUX_ITEM_PRECO "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
      
11    dbLocal.Execute "DROP TABLE AUX_ITEM_PRECO "
              
12    ATU_ITEMPRECO = True

13    Controle "ITEM_PRE", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_ITEM_PRECO "
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_ITEMPRECO = False
22        Screen.MousePointer = 1
24        MsgBox "Sub ATU_ITEMPRECO - BA" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function
Public Function ATU_MONTADORA() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_MONTADORA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM MONTADORA "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE MONTADORA INNER JOIN AUX_MONTADORA ON MONTADORA.COD_MONTADORA = AUX_MONTADORA.COD_MONTADORA  " & _
          " SET MONTADORA.DESC_MONTADORA = AUX_MONTADORA.DESC_MONTADORA "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO MONTADORA SELECT * FROM AUX_MONTADORA"
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_MONTADORA"
              
12    ATU_MONTADORA = True

13    Controle "MONTADORA", vRegs

14    Exit Function

VerErro:

15    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
16        dbLocal.Execute "DROP TABLE AUX_MONTADORA"
17        Resume
18    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
19        Resume Next
20    Else
21        ATU_MONTADORA = False
22        Screen.MousePointer = 1
23        MsgBox "Sub ATU_MONTADORA - CC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24    End If

End Function

Public Function ATU_NAT_OPERACAO() As Boolean
1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_NATUREZA_OPERACAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM NATUREZA_OPERACAO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE NATUREZA_OPERACAO INNER JOIN AUX_NATUREZA_OPERACAO ON "
6     strSQL = strSQL & " NATUREZA_OPERACAO.cod_natureza = AUX_NATUREZA_OPERACAO.COD_NATUREZA "
7     strSQL = strSQL & " SET NATUREZA_OPERACAO.desc_natureza = AUX_NATUREZA_OPERACAO.DESC_NATUREZA, "
8     strSQL = strSQL & " NATUREZA_OPERACAO.desc_abreviada = AUX_NATUREZA_OPERACAO.DESC_aBREVIADA,"
9     strSQL = strSQL & " NATUREZA_OPERACAO.cod_cfo_dest1=AUX_NATUREZA_OPERACAO.COD_CFO_DEST1 ,"
10    strSQL = strSQL & " NATUREZA_OPERACAO.cod_cfo_dest2=AUX_NATUREZA_OPERACAO.COD_CFO_DEST2 , "
11    strSQL = strSQL & " NATUREZA_OPERACAO.cod_cfo_dest3=AUX_NATUREZA_OPERACAO.COD_CFO_DEST3,"
12    strSQL = strSQL & " NATUREZA_OPERACAO.cod_cfo_oper=AUX_NATUREZA_OPERACAO.COD_CFO_OPER ,"
13    strSQL = strSQL & " NATUREZA_OPERACAO.cod_transac =AUX_NATUREZA_OPERACAO.COD_TRANSAC, "
14    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms1=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS1, "
15    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms2=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS2 , "
16    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms3=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS3, "
17    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms4=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS4 ,"
18    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms5=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS5 ,"
19    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_ipi1=AUX_NATUREZA_OPERACAO.COD_TRIB_IPI1, "
20    strSQL = strSQL & " NATUREZA_OPERACAO.Cod_trib_ipi2=AUX_NATUREZA_OPERACAO.COD_TRIB_IPI2, "
21    strSQL = strSQL & " NATUREZA_OPERACAO.Cod_trib_ipi3=AUX_NATUREZA_OPERACAO.COD_TRIB_IPI3 ,"
22    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_ipi4=AUX_NATUREZA_OPERACAO.COD_TRIB_IPI4 ,"
23    strSQL = strSQL & " NATUREZA_OPERACAO.cod_mensagem1=AUX_NATUREZA_OPERACAO.COD_MENSAGEM1 , "
24    strSQL = strSQL & " NATUREZA_OPERACAO.cod_mensagem2=AUX_NATUREZA_OPERACAO.COD_MENSAGEM2 , "
25    strSQL = strSQL & " NATUREZA_OPERACAO.fl_livro=AUX_NATUREZA_OPERACAO.FL_LIVRO,"
26    strSQL = strSQL & " NATUREZA_OPERACAO.fl_cuema=AUX_NATUREZA_OPERACAO.FL_CUEMA , "
27    strSQL = strSQL & " NATUREZA_OPERACAO.fl_estoque=AUX_NATUREZA_OPERACAO.FL_ESTOQUE ,"
28    strSQL = strSQL & " NATUREZA_OPERACAO.fl_estorno=AUX_NATUREZA_OPERACAO.FL_ESTORNO , "
29    strSQL = strSQL & " NATUREZA_OPERACAO.fl_tributacao=AUX_NATUREZA_OPERACAO.FL_TRIBUTACAO, "
30    strSQL = strSQL & " NATUREZA_OPERACAO.fl_fornecedor=AUX_NATUREZA_OPERACAO.FL_FORNECEDOR, "
31    strSQL = strSQL & " NATUREZA_OPERACAO.FL_vlcontabil=AUX_NATUREZA_OPERACAO.FL_VLCONTABIL, "
32    strSQL = strSQL & " NATUREZA_OPERACAO.FL_nfitem=AUX_NATUREZA_OPERACAO.FL_NFITEM, "
33    strSQL = strSQL & " NATUREZA_OPERACAO.fl_quantidade=AUX_NATUREZA_OPERACAO.FL_QUANTIDADE , "
34    strSQL = strSQL & " NATUREZA_OPERACAO.fl_cod_merc=AUX_NATUREZA_OPERACAO.FL_COD_MERC, "
35    strSQL = strSQL & " NATUREZA_OPERACAO.fl_pcdesc=AUX_NATUREZA_OPERACAO.FL_PCDESC , "
36    strSQL = strSQL & " NATUREZA_OPERACAO.fl_contas_pagar=AUX_NATUREZA_OPERACAO.FL_CONTAS_PAGAR, "
37    strSQL = strSQL & " NATUREZA_OPERACAO.fl_tipo_nota=AUX_NATUREZA_OPERACAO.FL_TIPO_NOTA, "
38    strSQL = strSQL & " NATUREZA_OPERACAO.fl_centro_custo=AUX_NATUREZA_OPERACAO.FL_CENTRO_CUSTO, "
39    strSQL = strSQL & " NATUREZA_OPERACAO.fl_contabil=AUX_NATUREZA_OPERACAO.FL_CONTABIL, "
40    strSQL = strSQL & " NATUREZA_OPERACAO.fl_base_red_icms=AUX_NATUREZA_OPERACAO.FL_BASE_RED_ICMS, "
41    strSQL = strSQL & " NATUREZA_OPERACAO.fl_consistencia=AUX_NATUREZA_OPERACAO.FL_CONSISTENCIA, "
42    strSQL = strSQL & " NATUREZA_OPERACAO.fl_ipi_incid_icm=AUX_NATUREZA_OPERACAO.FL_IPI_INCID_icm, "
43    strSQL = strSQL & " NATUREZA_OPERACAO.cod_trib_icms6=AUX_NATUREZA_OPERACAO.COD_TRIB_ICMS6, "
44    strSQL = strSQL & " NATUREZA_OPERACAO.fl_movimentacao=AUX_NATUREZA_OPERACAO.FL_MOVIMENTACAO "
45    dbLocal.Execute strSQL
46    vRegs = dbLocal.RecordsAffected

47    strSQL = "INSERT INTO NATUREZA_OPERACAO SELECT * FROM AUX_NATUREZA_OPERACAO"
48    dbLocal.Execute strSQL
49    vRegs = vRegs + dbLocal.RecordsAffected
      
50    dbLocal.Execute "DROP TABLE AUX_NATUREZA_OPERACAO"
              
51    ATU_NAT_OPERACAO = True

52    Controle "NATU_OPE", vRegs

53    Exit Function

VerErro:

54    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
55        dbLocal.Execute "DROP TABLE AUX_NATUREZA_OPERACAO"
56        Resume
57    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
58        Resume Next
59    Else
60        ATU_NAT_OPERACAO = False
61        Screen.MousePointer = 1
62        MsgBox "Sub ATU_NAT_OPERACAO - CC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
63    End If

End Function

Public Function ATU_REPCGC() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_R_REPCGC IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_REPCGC "
4     dbExt.Execute strSQL
            
5     strSQL = "INSERT INTO R_REPCGC SELECT * FROM AUX_R_REPCGC "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     dbLocal.Execute "DROP TABLE AUX_R_REPCGC "
              
9     ATU_REPCGC = True

10    Controle "R_REPCGC", vRegs

11    Exit Function

VerErro:

12    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
13        dbLocal.Execute "DROP TABLE AUX_REPCGC"
14        Resume
15    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
16        Resume Next
17    Else
18        ATU_REPCGC = False
19        Screen.MousePointer = 1
21        MsgBox "Sub ATU_REPCGC - RC" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22    End If

End Function

Public Function ATU_R_FILDEP() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_R_FILDEP IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_FILDEP "
4     dbExt.Execute strSQL
            
5     strSQL = "INSERT INTO R_FILDEP SELECT * FROM AUX_R_FILDEP "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
          
8     dbLocal.Execute "DROP TABLE AUX_R_FILDEP "
              
9     ATU_R_FILDEP = True

10    Controle "R_FILDEP", vRegs

11    Exit Function

VerErro:

12    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
13        dbLocal.Execute "DROP TABLE AUX_FILDEP"
14        Resume
15    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
16        Resume Next
17    Else
18        ATU_R_FILDEP = False
19        Screen.MousePointer = 1
20        MsgBox "Sub ATU_R_FILDEP - ES" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21    End If

End Function

Public Function ATU_REPEND() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_REPR_END_CORRESP IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM REPR_END_CORRESP "
4     dbExt.Execute strSQL
            
5     strSQL = "INSERT INTO REPR_END_CORRESP SELECT * FROM AUX_REPR_END_CORRESP "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     dbLocal.Execute "DROP TABLE AUX_REPR_END_CORRESP "
              
9     ATU_REPEND = True

10    Controle "REP_ENDC", vRegs

11    Exit Function

VerErro:

12    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
13        dbLocal.Execute "DROP TABLE AUX_REPR_END_CORRESP"
14        Resume
15    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
16        Resume Next
17    Else
18        ATU_REPEND = False
19        Screen.MousePointer = 1
20        MsgBox "Sub ATU_REPEND - RE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21    End If

End Function
Public Function ATU_REPRESENTACAO() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_REPRESENTACAO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM REPRESENTACAO "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE REPRESENTACAO INNER JOIN AUX_REPRESENTACAO ON REPRESENTACAO.CGC = AUX_REPRESENTACAO.CGC " & _
               " SET REPRESENTACAO.RAZAO_SOCIAL = AUX_REPRESENTACAO.RAZAO_SOCIAL, " & _
               " REPRESENTACAO.NUM_CORI = AUX_REPRESENTACAO.NUM_CORI, " & _
               " REPRESENTACAO.ENDERECO = AUX_REPRESENTACAO.ENDERECO, " & _
               " REPRESENTACAO.BAIRRO = AUX_REPRESENTACAO.BAIRRO, " & _
               " REPRESENTACAO.COD_CIDADE = AUX_REPRESENTACAO.COD_CIDADE, " & _
               " REPRESENTACAO.CEP = AUX_REPRESENTACAO.CEP, " & _
               " REPRESENTACAO.DDD = AUX_REPRESENTACAO.DDD, " & _
               " REPRESENTACAO.FONE = AUX_REPRESENTACAO.FONE, "
6     strSQL = strSQL & " REPRESENTACAO.FAX = AUX_REPRESENTACAO.FAX, " & _
               " REPRESENTACAO.CELULAR = AUX_REPRESENTACAO.CELULAR, " & _
               " REPRESENTACAO.CXPOSTAL = AUX_REPRESENTACAO.CXPOSTAL "

7     dbLocal.Execute strSQL
8     vRegs = dbLocal.RecordsAffected
      
9     strSQL = "INSERT INTO REPRESENTACAO SELECT * FROM AUX_REPRESENTACAO "
10    dbLocal.Execute strSQL
11    vRegs = vRegs + dbLocal.RecordsAffected
      
12    dbLocal.Execute "DROP TABLE AUX_REPRESENTACAO "
              
13    ATU_REPRESENTACAO = True

14    Controle "REPRESEN", vRegs

15    Exit Function

VerErro:

16    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
17        dbLocal.Execute "DROP TABLE AUX_REPRESENTACAO "
18        Resume
19    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
20        Resume Next
21    Else
22        ATU_REPRESENTACAO = False
23        Screen.MousePointer = 1
24        MsgBox "Sub ATU_REPRESENTACAO - RP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25    End If

End Function

Public Function ATU_REPRESENTANTE() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_REPRESENTANTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM REPRESENTANTE "
4     dbExt.Execute strSQL
            
5     strSQL = "UPDATE REPRESENTANTE INNER JOIN AUX_REPRESENTANTE ON REPRESENTANTE.COD_REPRES = AUX_REPRESENTANTE.COD_REPRES " & _
          " SET REPRESENTANTE.PSEUDONIMO = AUX_REPRESENTANTE.PSEUDONIMO, REPRESENTANTE.NOME_REPRES = AUX_REPRESENTANTE.NOME_REPRES, " & _
          " REPRESENTANTE.endereco = AUX_REPRESENTANTE.endereco, REPRESENTANTE.bairro = AUX_REPRESENTANTE.bairro, " & _
          " REPRESENTANTE.cod_cidade = AUX_REPRESENTANTE.cod_cidade, REPRESENTANTE.cep = AUX_REPRESENTANTE.cep, " & _
          " REPRESENTANTE.ddd = AUX_REPRESENTANTE.ddd, REPRESENTANTE.fone = AUX_REPRESENTANTE.fone, " & _
          " REPRESENTANTE.fax = AUX_REPRESENTANTE.fax, REPRESENTANTE.Ramal = AUX_REPRESENTANTE.Ramal, " & _
          " REPRESENTANTE.Cod_Filial = AUX_REPRESENTANTE.Cod_Filial, REPRESENTANTE.cic_gerente = AUX_REPRESENTANTE.cic_gerente, " & _
          " REPRESENTANTE.situacao = AUX_REPRESENTANTE.situacao, REPRESENTANTE.tipo = AUX_REPRESENTANTE.tipo, REPRESENTANTE.seq_franquia = AUX_REPRESENTANTE.seq_franquia, " & _
          " REPRESENTANTE.dt_cadastr = AUX_REPRESENTANTE.dt_cadastr, REPRESENTANTE.dt_desligamento = AUX_REPRESENTANTE.dt_desligamento"

6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     strSQL = "INSERT INTO REPRESENTANTE SELECT * FROM AUX_REPRESENTANTE "
9     dbLocal.Execute strSQL
10    vRegs = vRegs + dbLocal.RecordsAffected
          
11    dbLocal.Execute "DROP TABLE AUX_REPRESENTANTE"

12    strSQL = "DELETE REPRESENTACAO.* FROM REPRESENTACAO " & _
              " INNER JOIN R_REPCGC ON REPRESENTACAO.CGC = R_REPCGC.CGC " & _
              " WHERE R_REPCGC.COD_REPRES IN " & _
              " ( SELECT COD_REPRES FROM REPRESENTANTE " & _
              "   WHERE SITUACAO = 9 ) "
13    dbLocal.Execute strSQL

14    strSQL = "DELETE R_REPCGC.* FROM R_REPCGC INNER JOIN REPRESENTANTE " & _
              " ON REPRESENTANTE.COD_REPRES = R_REPCGC.COD_REPRES " & _
              " WHERE REPRESENTANTE.SITUACAO = 9 "
15    dbLocal.Execute strSQL

16    strSQL = "DELETE REPR_END_CORRESP.* FROM REPR_END_CORRESP INNER JOIN REPRESENTANTE " & _
              " ON REPRESENTANTE.COD_REPRES = REPR_END_CORRESP.COD_REPRES " & _
              " WHERE REPRESENTANTE.SITUACAO = 9 "
17    dbLocal.Execute strSQL

18    strSQL = "DELETE R_REPVEN.* FROM R_REPVEN INNER JOIN REPRESENTANTE " & _
              " ON REPRESENTANTE.COD_REPRES = R_REPVEN.COD_REPRES OR " & _
              "    REPRESENTANTE.COD_REPRES = R_REPVEN.COD_VEND " & _
              " WHERE REPRESENTANTE.SITUACAO = 9"
19    dbLocal.Execute strSQL

20    strSQL = "DELETE CLIE_CREDITO.* FROM CLIE_CREDITO INNER JOIN CLIENTE " & _
              " ON CLIENTE.COD_CLIENTE = CLIE_CREDITO.COD_CLIENTE " & _
              " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                  " FROM REPRESENTANTE WHERE SITUACAO = 9 )"
21    dbLocal.Execute strSQL

22    strSQL = "DELETE CLIE_ENDERECO.* FROM CLIE_ENDERECO INNER JOIN CLIENTE " & _
              " ON CLIENTE.COD_CLIENTE = CLIE_ENDERECO.COD_CLIENTE " & _
              " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                  " FROM REPRESENTANTE WHERE SITUACAO = 9 )"
23    dbLocal.Execute strSQL

24    strSQL = "DELETE DUPLICATAS.* FROM DUPLICATAS INNER JOIN CLIENTE " & _
              " ON CLIENTE.COD_CLIENTE = DUPLICATAS.COD_CLIENTE " & _
              " WHERE CLIENTE.COD_REPR_VEND IN ( SELECT COD_REPRES " & _
                                  " FROM REPRESENTANTE WHERE SITUACAO = 9 )"
25    dbLocal.Execute strSQL

26    strSQL = "DELETE CLIENTE.* FROM CLIENTE INNER JOIN REPRESENTANTE " & _
              " ON CLIENTE.COD_REPR_VEND = REPRESENTANTE.COD_REPRES " & _
              " WHERE REPRESENTANTE.SITUACAO = 9"
27    dbLocal.Execute strSQL

28    strSQL = "DELETE REPRESENTANTE.* FROM REPRESENTANTE WHERE SITUACAO = 9 "
29    dbLocal.Execute strSQL

30    ATU_REPRESENTANTE = True

31    Controle "REPRES", vRegs

32    Exit Function

VerErro:

33    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
34        dbLocal.Execute "DROP TABLE AUX_REPRESENTANTE"
35        Resume
36    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
37        Resume Next
38    Else
39        ATU_REPRESENTANTE = False
40        Screen.MousePointer = 1
42        MsgBox "Sub ATU_REPRESENTANTE - AO" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
43    End If

End Function
Public Function ATU_REPVEN() As Boolean

1     On Error GoTo VerErro

2     vRegs = 0

3     strSQL = "SELECT * INTO AUX_R_REPVEN IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_REPVEN "
4     dbExt.Execute strSQL
            
5     strSQL = "INSERT INTO R_REPVEN SELECT * FROM AUX_R_REPVEN "
6     dbLocal.Execute strSQL
7     vRegs = dbLocal.RecordsAffected
      
8     dbLocal.Execute "DROP TABLE AUX_R_REPVEN "
              
9     ATU_REPVEN = True

10    Controle "R_REPVEN", vRegs

11    Exit Function

VerErro:

12    If Err = 3010 Then
          'TABELA AUXILIAR J� EXISTE
13        dbLocal.Execute "DROP TABLE AUX_REPVEN"
14        Resume
15    ElseIf Err = 3022 Then
          ' Registro ja existe (Faz Update)
16        Resume Next
17    Else
18        ATU_REPVEN = False
19        Screen.MousePointer = 1
20        MsgBox "Sub ATU_REPVEN - RV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21    End If

End Function
Public Function ATU_TABVENDA() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_TABELA_VENDA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TABELA_VENDA "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE TABELA_VENDA INNER JOIN AUX_TABELA_VENDA ON " & _
                   " TABELA_VENDA.TP_TABELA = AUX_TABELA_VENDA.TP_TABELA AND " & _
                   " TABELA_VENDA.OCORR_PRECO = AUX_TABELA_VENDA.OCORR_PRECO " & _
                   " SET TABELA_VENDA.TABELA_VENDA = AUX_TABELA_VENDA.TABELA_VENDA , " & _
                   " TABELA_VENDA.DT_VIGENCIA1 = AUX_TABELA_VENDA.DT_VIGENCIA1, " & _
                   " TABELA_VENDA.DT_VIGENCIA2 = AUX_TABELA_VENDA.DT_VIGENCIA2, " & _
                   " TABELA_VENDA.DT_VIGENCIA3 = AUX_TABELA_VENDA.DT_VIGENCIA3, " & _
                   " TABELA_VENDA.DT_VIGENCIA4 = AUX_TABELA_VENDA.DT_VIGENCIA4, " & _
                   " TABELA_VENDA.DT_VIGENCIA5 = AUX_TABELA_VENDA.DT_VIGENCIA5 "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO TABELA_VENDA SELECT * FROM AUX_TABELA_VENDA "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected

11        dbLocal.Execute "DROP TABLE AUX_TABELA_VENDA"
                  
12        ATU_TABVENDA = True

13        Controle "TABELAV", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_TABELA_VENDA"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_TABVENDA = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_TABVENDA - AR" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_TAXA() As Boolean

          Dim MyRec As Recordset
          
1         vRegs = 0
          
2         On Error GoTo VerErro
          
3         strSQL = "SELECT * INTO AUX_TAXA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TAXA "
4         dbExt.Execute strSQL

5         Set MyRec = dbLocal.OpenRecordset("AUX_TAXA")
6         MyRec.MoveLast
7         If MyRec.RecordCount > 0 Then
8             strSQL = "DELETE FROM TAXA "
9             dbLocal.Execute strSQL
          
10            strSQL = "INSERT INTO TAXA SELECT * FROM AUX_TAXA"
11            dbLocal.Execute strSQL
12            vRegs = 0
13        End If
          
14        MyRec.Close
              
15        dbLocal.Execute "DROP TABLE AUX_TAXA"
                  
16        ATU_TAXA = True

17        Controle "TAXA", vRegs

18    Exit Function

VerErro:

19        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
20            dbLocal.Execute "DROP TABLE AUX_TAXA"
21            Resume
22        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
23            Resume Next
24        ElseIf Err = 3021 Then
25            Resume Next
26        Else
27            ATU_TAXA = False
28            Screen.MousePointer = 1
29            MsgBox "Sub ATU_TAXA - AS" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If

End Function
Public Function ATU_TPCLIE() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_TIPO_CLIENTE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TIPO_CLIENTE "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE TIPO_CLIENTE INNER JOIN AUX_TIPO_CLIENTE ON TIPO_CLIENTE.COD_TIPO_CLI = AUX_TIPO_CLIENTE.COD_TIPO_CLI " & _
                   " SET TIPO_CLIENTE.DESC_TIPO_CLI = AUX_TIPO_CLIENTE.DESC_TIPO_CLI , " & _
                   " TIPO_CLIENTE.COD_SEGMENTO = AUX_TIPO_CLIENTE.COD_SEGMENTO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO TIPO_CLIENTE SELECT * FROM AUX_TIPO_CLIENTE "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
          
11        dbLocal.Execute "DROP TABLE AUX_TIPO_CLIENTE "
                  
12        ATU_TPCLIE = True
          
13        Controle "TIPO_CLI", vRegs
          
14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_TIPO_CLIE "
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_TPCLIE = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_TPCLIE - AT" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If
          
End Function
Public Function ATU_TIPO_CLIENTE_BLAU() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_TIPO_CLIENTE_BLAU IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TIPO_CLIENTE_BLAU "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE TIPO_CLIENTE_BLAU INNER JOIN AUX_TIPO_CLIENTE_BLAU ON TIPO_CLIENTE_BLAU.COD_TIPO_CLI = AUX_TIPO_CLIENTE_BLAU.COD_TIPO_CLI " & _
                   " SET TIPO_CLIENTE_BLAU.DESC_TIPO_CLI = AUX_TIPO_CLIENTE_BLAU.DESC_TIPO_CLI "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO TIPO_CLIENTE_BLAU SELECT * FROM AUX_TIPO_CLIENTE_BLAU "
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected

11        dbLocal.Execute "DROP TABLE AUX_TIPO_CLIENTE_BLAU "
                  
12        ATU_TIPO_CLIENTE_BLAU = True
          
13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_TIPO_CLIENTE_BLAU "
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            ATU_TIPO_CLIENTE_BLAU = False
21            Screen.MousePointer = 1
22            MsgBox "Sub ATU_TIPO_CLIENTE_BLAU - CH" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If

End Function
Public Function ATU_TRANSPORTADORA() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_TRANSPORTADORA IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TRANSPORTADORA "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE TRANSPORTADORA INNER JOIN AUX_TRANSPORTADORA ON " & _
                   " TRANSPORTADORA.COD_TRANSP = AUX_TRANSPORTADORA.COD_TRANSP " & _
                   " SET TRANSPORTADORA.NOME_TRANSP = AUX_TRANSPORTADORA.NOME_TRANSP, " & _
                   " TRANSPORTADORA.COD_CIDADE = AUX_TRANSPORTADORA.COD_CIDADE, " & _
                   " TRANSPORTADORA.SITUACAO = AUX_TRANSPORTADORA.SITUACAO"
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO TRANSPORTADORA SELECT * FROM AUX_TRANSPORTADORA"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected

11        dbLocal.Execute "DROP TABLE AUX_TRANSPORTADORA"
                  
12        ATU_TRANSPORTADORA = True

13        Controle "TRANSPOR", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_TRANSPORTADORA"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_TRANSPORTADORA = False
22            Screen.MousePointer = 1
23            MsgBox "Sub ATU_TRANSPORTADORA - AU" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
24        End If

End Function
Public Function ATU_UF() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM UF "
4         dbExt.Execute strSQL
            
5                  strSQL = "UPDATE UF INNER JOIN AUX_UF ON "
6         strSQL = strSQL & " UF.COD_UF = AUX_UF.COD_UF "
7         strSQL = strSQL & " SET UF.DESC_UF = AUX_UF.DESC_UF,  "
8         strSQL = strSQL & " UF.DT_ALIQ_INTERNA = AUX_UF.DT_ALIQ_INTERNA "
9         dbLocal.Execute strSQL
10        vRegs = dbLocal.RecordsAffected
          
11        strSQL = "INSERT INTO UF SELECT * FROM AUX_UF"
12        dbLocal.Execute strSQL
13        vRegs = vRegs + dbLocal.RecordsAffected
          
14        dbLocal.Execute "DROP TABLE AUX_UF"
              
15        ATU_UF = True

16        Controle "UF", vRegs
    
17    Exit Function

VerErro:

18        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
19            dbLocal.Execute "DROP TABLE AUX_UF"
20            Resume
21        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
22            Resume Next
23        Else
24            ATU_UF = False
25            Screen.MousePointer = 1
26            MsgBox "Sub ATU_UF - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
27        End If
          
End Function
Public Function ATU_UF_DEPOSITO() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF_DEP IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM UF_DEPOSITO "
4         dbExt.Execute strSQL
                
5                   strSQL = "UPDATE UF_DEPOSITO INNER JOIN AUX_UF_DEP ON "
6         strSQL = strSQL & " UF_DEPOSITO.COD_LOJA = AUX_UF_DEP.COD_LOJA AND "
7         strSQL = strSQL & " UF_DEPOSITO.COD_UF_DESTINO = AUX_UF_DEP.COD_UF_DESTINO "
8         strSQL = strSQL & " SET "
9         strSQL = strSQL & " UF_DEPOSITO.PC_FRETE = AUX_UF_DEP.PC_FRETE  "
10        dbLocal.Execute strSQL
11        vRegs = dbLocal.RecordsAffected
          
12        strSQL = "INSERT INTO UF_DEPOSITO SELECT * FROM AUX_UF_DEP "
13        dbLocal.Execute strSQL
14        vRegs = vRegs + dbLocal.RecordsAffected
              
15        dbLocal.Execute "DROP TABLE AUX_UF_DEP "
                  
16        ATU_UF_DEPOSITO = True

17        Controle "UF_DEP", vRegs

18    Exit Function

VerErro:

19        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
20            dbLocal.Execute "DROP TABLE AUX_UF_DEPOSITO"
21            Resume
22        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_UF_DEPOSITO = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_UF_DEPOSITO - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If

End Function
Public Function ATU_UF_ORIGDEST() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF_ORIGDEST IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM UF_ORIGEM_DESTINO "
4         dbExt.Execute strSQL
                
5                   strSQL = "UPDATE UF_ORIGEM_DESTINO INNER JOIN AUX_UF_ORIGDEST ON "
6         strSQL = strSQL & " UF_ORIGEM_DESTINO.COD_UF_ORIGEM = AUX_UF_ORIGDEST.COD_UF_ORIGEM AND "
7         strSQL = strSQL & " UF_ORIGEM_DESTINO.COD_UF_DESTINO = AUX_UF_ORIGDEST.COD_UF_DESTINO "
8         strSQL = strSQL & " SET "
9         strSQL = strSQL & " UF_ORIGEM_DESTINO.PC_ICM = AUX_UF_ORIGDEST.PC_ICM,  "
10        strSQL = strSQL & " UF_ORIGEM_DESTINO.PC_DIFICM = AUX_UF_ORIGDEST.PC_DIFICM,  "
11        strSQL = strSQL & " UF_ORIGEM_DESTINO.PC_ICM_GERENCIAL = AUX_UF_ORIGDEST.PC_ICM_GERENCIAL  " 'CONSULTOR 30 29/05/2009
12        dbLocal.Execute strSQL
13        vRegs = dbLocal.RecordsAffected
          
14        strSQL = "INSERT INTO UF_ORIGEM_DESTINO SELECT * FROM AUX_UF_ORIGDEST "
15        dbLocal.Execute strSQL
16        vRegs = vRegs + dbLocal.RecordsAffected
          
17        dbLocal.Execute "DROP TABLE AUX_UF_ORIGDEST "
                  
18        ATU_UF_ORIGDEST = True

19        Controle "UF_OR_DE", vRegs

20    Exit Function

VerErro:

21        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
22            dbLocal.Execute "DROP TABLE AUX_UF_ORIGDEST"
23            Resume
24        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
25            Resume Next
26        Else
27            ATU_UF_ORIGDEST = False
28            Screen.MousePointer = 1
29            MsgBox "Sub ATU_UF_ORIGDEST - AV" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
30        End If

End Function
Public Function ATU_UFDPK() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF_DPK IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM UF_DPK "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE UF_DPK INNER JOIN AUX_UF_DPK ON "
6         strSQL = strSQL & " UF_DPK.COD_UF_ORIGEM = AUX_UF_DPK.COD_UF_ORIGEM AND "
7         strSQL = strSQL & " UF_DPK.COD_UF_DESTINO = AUX_UF_DPK.COD_UF_DESTINO AND "
8         strSQL = strSQL & " UF_DPK.COD_DPK = AUX_UF_DPK.COD_DPK  "
9         strSQL = strSQL & " SET UF_DPK.PC_DESC = AUX_UF_DPK.PC_DESC "
10        dbLocal.Execute strSQL
11        vRegs = dbLocal.RecordsAffected
          
12        strSQL = "INSERT INTO UF_DPK SELECT * FROM AUX_UF_DPK "
13        dbLocal.Execute strSQL
14        vRegs = vRegs + dbLocal.RecordsAffected
              
15        dbLocal.Execute "DROP TABLE AUX_UF_DPK "
                  
16        ATU_UFDPK = True
          
17        Controle "UF_DPK", vRegs
          
18    Exit Function

VerErro:

19        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
20            dbLocal.Execute "DROP TABLE AUX_UF_DPK"
21            Resume
22        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
23            Resume Next
24        Else
25            ATU_UFDPK = False
26            Screen.MousePointer = 1
27            MsgBox "Sub ATU_UFDPK - 03" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
28        End If
          
End Function
Public Function ATU_UF_CATEG() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_UF_CATEG IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM uf_categ "
4         dbExt.Execute strSQL
                
5                  strSQL = "UPDATE UF_CATEG INNER JOIN AUX_UF_CATEG ON "
6         strSQL = strSQL & " UF_CATEG.COD_UF           = AUX_UF_CATEG.COD_UF AND "
7         strSQL = strSQL & " UF_CATEG.CATEGORIA        = AUX_UF_CATEG.CATEGORIA AND "
8         strSQL = strSQL & " UF_CATEG.CARACTERISTICA   = AUX_UF_CATEG.CARACTERISTICA "
9         strSQL = strSQL & " SET "
10        strSQL = strSQL & " UF_CATEG.PC_DESC_ADIC     = AUX_UF_CATEG.PC_DESC_ADIC, "
11        strSQL = strSQL & " UF_CATEG.PC_ADIC_MAXIMO   = AUX_UF_CATEG.PC_ADIC_MAXIMO, "
12        strSQL = strSQL & " UF_CATEG.PC_DESC_COMIS    = AUX_UF_CATEG.PC_DESC_COMIS, "
13        strSQL = strSQL & " UF_CATEG.DESC_COMIS_MAXIMO= AUX_UF_CATEG.DESC_COMIS_MAXIMO "
14        dbLocal.Execute strSQL
15        vRegs = dbLocal.RecordsAffected
          
16        strSQL = "INSERT INTO UF_CATEG SELECT * FROM AUX_UF_CATEG "
17        dbLocal.Execute strSQL
18        vRegs = vRegs = dbLocal.RecordsAffected
          
19        dbLocal.Execute "DROP TABLE AUX_UF_CATEG "
                  
20        ATU_UF_CATEG = True

21        Controle "UF_CATEG", vRegs

22    Exit Function

VerErro:

23        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
24            dbLocal.Execute "DROP TABLE AUX_UF_CATEG"
25            Resume
26        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
27            Resume Next
28        Else
29            ATU_UF_CATEG = False
30            Screen.MousePointer = 1
32            MsgBox "Sub ATU_UF_CATEG - 03" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
33        End If

End Function
Public Function ATU_SUBGRUPO() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_SUBGRUPO IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM SUBGRUPO "
4         dbExt.Execute strSQL
                
5         strSQL = "UPDATE SUBGRUPO INNER JOIN AUX_SUBGRUPO ON SUBGRUPO.COD_GRUPO = AUX_SUBGRUPO.COD_GRUPO AND SUBGRUPO.COD_SUBGRUPO = AUX_SUBGRUPO.COD_SUBGRUPO " & _
                   " SET SUBGRUPO.DESC_SUBGRUPO = AUX_SUBGRUPO.DESC_SUBGRUPO "
6         dbLocal.Execute strSQL
7         vRegs = dbLocal.RecordsAffected
          
8         strSQL = "INSERT INTO SUBGRUPO SELECT * FROM AUX_SUBGRUPO"
9         dbLocal.Execute strSQL
10        vRegs = vRegs + dbLocal.RecordsAffected
              
11        dbLocal.Execute "DROP TABLE AUX_SUBGRUPO"
                  
12        ATU_SUBGRUPO = True

13        Controle "SUBGRUPO", vRegs

14    Exit Function

VerErro:

15        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
16            dbLocal.Execute "DROP TABLE AUX_SUBGRUPO"
17            Resume
18        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
19            Resume Next
20        Else
21            ATU_SUBGRUPO = False
22            Screen.MousePointer = 1
24            MsgBox "Sub ATU_SUBGRUPO - AP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
25        End If

End Function
Public Function ATU_SUBTRIB() As Boolean
1         On Error GoTo VerErro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_SUBTRIB IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM SUBST_TRIBUTARIA "
4         dbExt.Execute strSQL
                
5                  strSQL = "UPDATE SUBST_TRIBUTARIA INNER JOIN AUX_SUBTRIB ON "
6         strSQL = strSQL & " SUBST_TRIBUTARIA.CLASS_FISCAL   = AUX_SUBTRIB.CLASS_FISCAL  AND "
7         strSQL = strSQL & " SUBST_TRIBUTARIA.COD_UF_ORIGEM  = AUX_SUBTRIB.COD_UF_ORIGEM AND "
8         strSQL = strSQL & " SUBST_TRIBUTARIA.COD_UF_DESTINO = AUX_SUBTRIB.COD_UF_DESTINO "
9         strSQL = strSQL & " SET "
10        strSQL = strSQL & " SUBST_TRIBUTARIA.COD_TRIB_REVENDEDOR = AUX_SUBTRIB.COD_TRIB_REVENDEDOR, "
11        strSQL = strSQL & " SUBST_TRIBUTARIA.COD_TRIB_INSCRITO   = AUX_SUBTRIB.COD_TRIB_INSCRITO, "
12        strSQL = strSQL & " SUBST_TRIBUTARIA.COD_TRIB_ISENTO     = AUX_SUBTRIB.COD_TRIB_ISENTO, "
13        strSQL = strSQL & " SUBST_TRIBUTARIA.FATOR_REVENDEDOR    = AUX_SUBTRIB.FATOR_REVENDEDOR, "
14        strSQL = strSQL & " SUBST_TRIBUTARIA.FATOR_INSCRITO      = AUX_SUBTRIB.FATOR_INSCRITO, "
15        strSQL = strSQL & " SUBST_TRIBUTARIA.FATOR_ISENTO        = AUX_SUBTRIB.FATOR_ISENTO, "
16        strSQL = strSQL & " SUBST_TRIBUTARIA.FL_CRED_SUSPENSO    = AUX_SUBTRIB.FL_CRED_SUSPENSO, "
17        strSQL = strSQL & " SUBST_TRIBUTARIA.PC_MARGEM_LUCRO     = AUX_SUBTRIB.PC_MARGEM_LUCRO, "
18        strSQL = strSQL & " SUBST_TRIBUTARIA.FL_RED_ICMS         = AUX_SUBTRIB.FL_RED_ICMS " 'CONSULTOR 30 29/05/2009
19        dbLocal.Execute strSQL
20        vRegs = dbLocal.RecordsAffected
          
21        strSQL = "INSERT INTO SUBST_TRIBUTARIA SELECT * FROM AUX_SUBTRIB"
22        dbLocal.Execute strSQL
23        vRegs = vRegs = dbLocal.RecordsAffected
          
24        dbLocal.Execute "DROP TABLE AUX_SUBTRIB"
                  
25        ATU_SUBTRIB = True

26        Controle "SUBST_TR", vRegs

27    Exit Function

VerErro:

28        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
29            dbLocal.Execute "DROP TABLE AUX_SUBTRIB"
30            Resume
31        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
32            Resume Next
33        Else
34            ATU_SUBTRIB = False
35            Screen.MousePointer = 1
36            MsgBox "Sub ATU_SUBTRIB - CP" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
37        End If

End Function
Public Function ATU_TABDESCPER() As Boolean

1         On Error GoTo VerErro
         'Deleta todas as informa��es da Tabela_descper para garantir integridade e depois
         'inclui todos os registros novamente. - 01/11/99
          
2         vRegs = 0
            
3         strSQL = "DELETE TABELA_DESCPER.* FROM TABELA_DESCPER"
4         dbLocal.Execute strSQL
          
5         strSQL = "SELECT * INTO AUX_TABELA_DESCPER IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM TABELA_DESCPER "
6         dbExt.Execute strSQL
                
         'Efetua Update de uma tabela com base na outra .......................
         '         strSQL = "UPDATE TABELA_DESCPER INNER JOIN AUX_TABELA_DESCPER ON "
         'strSQL = strSQL & " TABELA_DESCPER.SEQUENCIA = AUX_TABELA_DESCPER.SEQUENCIA AND "
         'strSQL = strSQL & " TABELA_DESCPER.TP_TABELA = AUX_TABELA_DESCPER.TP_TABELA AND "
         'strSQL = strSQL & " TABELA_DESCPER.OCORR_PRECO = AUX_TABELA_DESCPER.OCORR_PRECO AND "
         'strSQL = strSQL & " TABELA_DESCPER.COD_FORNECEDOR = AUX_TABELA_DESCPER.COD_FORNECEDOR AND "
         'strSQL = strSQL & " TABELA_DESCPER.COD_FILIAL = AUX_TABELA_DESCPER.COD_FILIAL AND "
         'strSQL = strSQL & " TABELA_DESCPER.COD_GRUPO = AUX_TABELA_DESCPER.COD_GRUPO AND "
         'strSQL = strSQL & " TABELA_DESCPER.COD_SUBGRUPO = AUX_TABELA_DESCPER.COD_SUBGRUPO AND "
         'strSQL = strSQL & " TABELA_DESCPER.COD_DPK = AUX_TABELA_DESCPER.COD_DPK  "
         'strSQL = strSQL & " SET "
         'strSQL = strSQL & " TABELA_DESCPER.SITUACAO = AUX_TABELA_DESCPER.SITUACAO, "
         'strSQL = strSQL & " TABELA_DESCPER.PC_DESC_PERIODO1 = AUX_TABELA_DESCPER.PC_DESC_PERIODO1 , "
         'strSQL = strSQL & " TABELA_DESCPER.PC_DESC_PERIODO2 = AUX_TABELA_DESCPER.PC_DESC_PERIODO2, "
         'strSQL = strSQL & " TABELA_DESCPER.PC_DESC_PERIODO3 = AUX_TABELA_DESCPER.PC_DESC_PERIODO3 "
         'dbLocal.Execute strSQL
                 
7         strSQL = "INSERT INTO TABELA_DESCPER SELECT * FROM AUX_TABELA_DESCPER"
8         dbLocal.Execute strSQL
9         vRegs = dbLocal.RecordsAffected
              
10        dbLocal.Execute "DROP TABLE AUX_TABELA_DESCPER"
                  
11        ATU_TABDESCPER = True

12        Controle "TABELAD", vRegs

13    Exit Function

VerErro:

14        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
15            dbLocal.Execute "DROP TABLE AUX_TABELA_DESCPER"
16            Resume
17        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
18            Resume Next
19        Else
20            ATU_TABDESCPER = False
21            Screen.MousePointer = 1
22            MsgBox "Sub ATU_TABDESCPER - AQ" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
23        End If

End Function

'------------------------
'Eduardo Relvas
'18/01/2005
'------------------------
Public Function R_FABRICA_DPK() As Boolean
    
1         On Error GoTo Trata_Erro
    
2         vRegs = 0
    
3       strSQL = "SELECT * INTO AUX_R_FABRICA_DPK IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM R_FABRICA_DPK "
4       dbExt.Execute strSQL

5        dbLocal.Execute "Update R_FABRICA_DPK INNER JOIN AUX_R_FABRICA_DPK ON " & _
                    " R_FABRICA_DPK.COD_FABRICA = AUX_R_FABRICA_DPK.COD_FABRICA AND " & _
                    " R_FABRICA_DPK.COD_DPK = AUX_R_FABRICA_DPK.COD_DPK AND " & _
                    " R_FABRICA_DPK.FL_TIPO = AUX_R_FABRICA_DPK.FL_TIPO AND" & _
                    " R_FABRICA_DPK.COD_FORNECEDOR = AUX_R_FABRICA_DPK.COD_FORNECEDOR " & _
                    " SET R_FABRICA_DPK.DT_CADASTRO = AUX_R_FABRICA_DPK.DT_CADASTRO," & _
                    " R_FABRICA_DPK.SIGLA = AUX_R_FABRICA_DPK.SIGLA "

6        vRegs = dbLocal.RecordsAffected

7         dbLocal.Execute "INSERT INTO R_FABRICA_DPK SELECT * FROM AUX_R_FABRICA_DPK"
    
8        vRegs = vRegs + dbLocal.RecordsAffected
    
9         dbLocal.Execute "DROP TABLE AUX_R_FABRICA_DPK"
    
10        R_FABRICA_DPK = True

11        Controle "R_FABRIC", vRegs

12        Exit Function
    
    
Trata_Erro:
13    If Err.Number = 3010 Then
14      dbLocal.Execute "DROP TABLE AUX_R_FABRICA_DPK"
15      Resume
16    ElseIf Err = 3022 Then
       'Registro ja existe (Fez Update)
17      Resume Next
18    ElseIf Err.Number <> 0 Then
19      Screen.MousePointer = 1
20      R_FABRICA_DPK = False
21      MsgBox "Sub R_FABRICA_DPK" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22    End If
End Function

Public Function DEL_R_FABRICA_DPK() As Boolean

1         On Error GoTo VerErro
2         vRegs = 0
3         strSQL = "SELECT * INTO AUX_DEL_R_FABRICA_DPK IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEL_R_FABRICA_DPK "
4         dbExt.Execute strSQL
                
5         strSQL = "DELETE R_FABRICA_DPK.* FROM R_FABRICA_DPK INNER JOIN " & _
                   " AUX_DEL_R_FABRICA_DPK ON " & _
                   " R_FABRICA_DPK.COD_FABRICA = AUX_DEL_R_FABRICA_DPK.COD_FABRICA AND " & _
                   " R_FABRICA_DPK.COD_DPK = AUX_DEL_R_FABRICA_DPK.COD_DPK AND " & _
                   " R_FABRICA_DPK.FL_TIPO = AUX_DEL_R_FABRICA_DPK.FL_TIPO "
                   
6                  dbLocal.Execute strSQL
7          vRegs = dbLocal.RecordsAffected
           
8         dbLocal.Execute "DROP TABLE AUX_DEL_R_FABRICA_DPK"
                  
9         DEL_R_FABRICA_DPK = True

10        Controle "DEL_R_FA", vRegs

11    Exit Function

VerErro:

12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_R_FABRICA_DPK"
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_R_FABRICA_DPK = False
19            Screen.MousePointer = 1
20            MsgBox "Sub DEL_R_FABRICA_DPK - LD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
21        End If

End Function

'--------------------------------
'26/07/05
'--------------------------------
Public Function DEL_CLASS_FISCAL_RED_BASE() As Boolean

1         On Error GoTo VerErro
          
2         vRegs = 0

3         strSQL = "SELECT * INTO AUX_DEL_CLASS_FISCAL_RED_BASE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM DEL_CLASS_FISCAL_RED_BASE "
4         dbExt.Execute strSQL
                
5         strSQL = "DELETE CLASS_FISCAL_RED_BASE.* FROM CLASS_FISCAL_RED_BASE INNER JOIN " & _
                   " AUX_DEL_CLASS_FISCAL_RED_BASE ON " & _
                   " CLASS_FISCAL_RED_BASE.COD_UF_ORIGEM = AUX_DEL_CLASS_FISCAL_RED_BASE.COD_UF_ORIGEM AND " & _
                   " CLASS_FISCAL_RED_BASE.COD_UF_DESTINO = AUX_DEL_CLASS_FISCAL_RED_BASE.COD_UF_DESTINO AND " & _
                   " CLASS_FISCAL_RED_BASE.CLASS_FISCAL = AUX_DEL_CLASS_FISCAL_RED_BASE.CLASS_FISCAL "
                   
6                  dbLocal.Execute strSQL
                 
7         dbLocal.Execute "DROP TABLE AUX_DEL_CLASS_FISCAL_RED_BASE"
                  
8         vRegs = dbLocal.RecordsAffected
                  
9         DEL_CLASS_FISCAL_RED_BASE = True


10        Controle "DEL_RBAS", vRegs

11    Exit Function

VerErro:

12        If Err = 3010 Then
             'TABELA AUXILIAR J� EXISTE
13            dbLocal.Execute "DROP TABLE AUX_DEL_CLASS_FISCAL_RED_BASE"
14            Resume
15        ElseIf Err = 3022 Then
             'Registro ja existe (Faz Update)
16            Resume Next
17        Else
18            DEL_CLASS_FISCAL_RED_BASE = False
19            Screen.MousePointer = 1
21            MsgBox "Sub DEL_CLASS_FISCAL_RED_BASE - LD" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
22        End If

End Function

'--------------------------------
'26/07/05
'--------------------------------
Public Function CLASS_FISCAL_RED_BASE() As Boolean
          
1         On Error GoTo Trata_Erro
          
2         vRegs = 0
          
3         strSQL = "SELECT * INTO AUX_CLASS_FISCAL_RED_BASE IN " + Chr(34) + Path_drv & "\DADOS\BASE_DPK.MDB" + Chr(34) + " FROM CLASS_FISCAL_RED_BASE "
4         dbExt.Execute strSQL

          'dbLocal.Execute "Update CLASS_FISCAL_RED_BASE INNER JOIN AUX_CLASS_FISCAL_RED_BASE ON " & _
                          " CLASS_FISCAL_RED_BASE.cod_uf_origem = AUX_CLASS_FISCAL_RED_BASE.cod_uf_origem AND " & _
                          " CLASS_FISCAL_RED_BASE.cod_uf_destino = AUX_CLASS_FISCAL_RED_BASE.cod_uf_destino AND " & _
                          " CLASS_FISCAL_RED_BASE.class_fiscal = AUX_CLASS_FISCAL_RED_BASE.class_fiscal " & _
                          " SET CLASS_FISCAL_RED_BASE.cod_uf_origem = AUX_CLASS_FISCAL_RED_BASE.cod_uf_origem, " & _
                          " CLASS_FISCAL_RED_BASE.cod_uf_destino = AUX_CLASS_FISCAL_RED_BASE.cod_uf_destino, " & _
                          " CLASS_FISCAL_RED_BASE.class_fiscal = AUX_CLASS_FISCAL_RED_BASE.class_fiscal "

5         dbLocal.Execute "INSERT INTO CLASS_FISCAL_RED_BASE SELECT * FROM AUX_CLASS_FISCAL_RED_BASE"
          
6         vRegs = dbLocal.RecordsAffected
          
7         dbLocal.Execute "DROP TABLE AUX_CLASS_FISCAL_RED_BASE"
          
8         CLASS_FISCAL_RED_BASE = True

9         Controle "RED_BASE", vRegs

10        Exit Function
          
          
Trata_Erro:
11        If Err.Number = 3010 Then
12            dbLocal.Execute "DROP TABLE AUX_CLASS_FISCAL_RED_BASE"
13            Resume
14        ElseIf Err = 3022 Then
             'Registro ja existe (Fez Update)
15            Resume Next
16        ElseIf Err.Number <> 0 Then
17            Screen.MousePointer = 1
18            CLASS_FISCAL_RED_BASE = False
19            MsgBox "Sub CLASS_FISCAL_RED_BASE" & vbCrLf & "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha: " & Erl
20        End If
End Function

Public Sub Controle(pTabela As String, pRegs As Long)
    On Error GoTo Trata_Erro
    
    dbLocal.Execute "UPDATE CONTROLE SET REG_REC = " & pRegs & " Where NOME_TAB ='" & pTabela & "'"

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub Controle" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description
    End If
End Sub

