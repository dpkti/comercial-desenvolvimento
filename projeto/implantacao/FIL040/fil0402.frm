VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmGER0402 
   Caption         =   "CONSULTA LOG DE DIVERG�NCIA"
   ClientHeight    =   4230
   ClientLeft      =   1080
   ClientTop       =   1815
   ClientWidth     =   6720
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4230
   ScaleWidth      =   6720
   WindowState     =   2  'Maximized
   Begin Threed.SSPanel sspFaixa1 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9615
      _Version        =   65536
      _ExtentX        =   16960
      _ExtentY        =   1296
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin Threed.SSCommand scmdSair 
         Height          =   735
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Picture         =   "fil0402.frx":0000
      End
   End
   Begin MSGrid.Grid grLogDiv 
      Height          =   4695
      Left            =   120
      TabIndex        =   3
      Top             =   1440
      Width           =   9255
      _Version        =   65536
      _ExtentX        =   16325
      _ExtentY        =   8281
      _StockProps     =   77
      BackColor       =   16777215
      Cols            =   5
      MouseIcon       =   "fil0402.frx":031A
   End
   Begin VB.Label lblSair 
      AutoSize        =   -1  'True
      BackColor       =   &H0000FFFF&
      Caption         =   "Sair"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Visible         =   0   'False
      Width           =   360
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "frmGER0402"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub MONTA_GRLOGDIV()
      Dim contLinha As Long
      Dim strSQL As String

1     On Error GoTo Trata_Erro

2     strSQL = "SELECT DATA_HORA, TP_PROBLEMA, ASSUNTO, MSG_ERRO, RESPONSAVEL " & _
          " FROM LOGDIV ORDER BY DATA_HORA DESC"
          
3     Set recCtl = dbControle.OpenRecordset(strSQL, dbOpenSnapshot)

4     If recCtl.EOF Then
5         Exit Sub
6     End If

7     contLinha = 1
8     Do While Not recCtl.EOF

9         If recCtl!data_hora < DateAdd("d", -30, Date) Then
10            Exit Do
11        End If
        
12        contLinha = contLinha + 1
13        grLogDiv.Rows = contLinha
14        grLogDiv.Row = contLinha - 1
15        grLogDiv.Col = 0
16        grLogDiv.Text = recCtl!data_hora
17        grLogDiv.Col = 1
18        grLogDiv.Text = IIf(recCtl!tp_problema = "1", "ATUALIZA��O", IIf(recCtl!tp_problema = "2", "PROGRAMAS", "MENSAGENS"))
19        grLogDiv.Col = 2
20        grLogDiv.Text = recCtl!Assunto
21        grLogDiv.Col = 3
22        grLogDiv.Text = recCtl!msg_erro
23        grLogDiv.Col = 4
24        grLogDiv.Text = recCtl!responsavel
          
25        recCtl.MoveNext
          
26    Loop

27    Exit Sub

Trata_Erro:

28    If Err.Number = 53 Then
29        MsgBox "Erro NR.: " & Err.Number & " - " & " Arquivo n�o Encontrado " & Chr(13) & " Path_drv & \LOG\LOGDIV.TXT", vbExclamation, "ERRO NO APLICATIVO"
30    Else
31        MsgBox "Sub Monta_GrLogDiv" & vbCrLf & "ERRO NR.: " & Err.Number & " - " & Err.Description & vbCrLf & "Linha:" & Erl
32    End If

33    Exit Sub

End Sub

Private Sub Form_Load()
1         lblSair.Visible = False
          
2         On Error GoTo TRATA_ERRO_CTL
3         Set dbControle = OpenDatabase(Path_drv & "\LOG\CONTROLE.MDB", False, False)

4         grLogDiv.Row = 0
5         grLogDiv.ColWidth(0) = 1400
6         grLogDiv.Col = 0
7         grLogDiv.Text = "DATA / HORA"
8         grLogDiv.ColWidth(1) = 1800
9         grLogDiv.Col = 1
10        grLogDiv.Text = "TIPO DE PROBLEMA"
11        grLogDiv.ColWidth(2) = 4000
12        grLogDiv.Col = 2
13        grLogDiv.Text = "ASSUNTO"
14        grLogDiv.ColWidth(3) = 10000
15        grLogDiv.Col = 3
16        grLogDiv.Text = "MENSAGEM ERRO"
17        grLogDiv.ColWidth(4) = 8000
18        grLogDiv.Col = 4
19        grLogDiv.Text = "RESPONS�VEL"
          
20        MONTA_GRLOGDIV
          
21    Exit Sub

TRATA_ERRO_CTL:

22    If Err = 3049 Then

23        MousePointer = 11
24        DoEvents

          ' Reparar Banco de Dados
25        RepairDatabase (Path_drv & "\LOG\CONTROLE.MDB")
              
26        COMPACTA_CONTROLE
          
27        MousePointer = 0
          
          ' Ap�s corrigir o banco danificado processa novamente a linha do programa
          ' que gerou o erro.
28        Resume 0
          
29     Else
       
30        Call FUNCAO_ERRO("")
        
31        MsgBox "Sub frmGer0402_Load" & vbCrLf & "ERRO: " & Err.Number & " - " & Err.Description & vbCrLf & "Linha:" & Erl
          
32        Resume Next
          
33     End If
       
End Sub



Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblSair.Visible = False
End Sub



Private Sub mnuSair_Click()
    scmdSair_Click
End Sub

Private Sub scmdSair_Click()
    Unload Me
End Sub

Private Sub scmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblSair.Left = 240
    lblSair.Caption = "Sair"
    lblSair.Visible = True
End Sub


Private Sub sspFaixa1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
lblSair.Visible = False
End Sub


