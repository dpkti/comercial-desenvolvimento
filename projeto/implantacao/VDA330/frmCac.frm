VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmCac 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   Caption         =   "Consulta Dados CAC"
   ClientHeight    =   5520
   ClientLeft      =   30
   ClientTop       =   1605
   ClientWidth     =   9540
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5520
   ScaleWidth      =   9540
   Begin VB.TextBox txtTF 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   7155
      TabIndex        =   7
      Top             =   4815
      Visible         =   0   'False
      Width           =   1005
   End
   Begin Threed.SSFrame Frame3D1 
      Height          =   3345
      Left            =   7020
      TabIndex        =   2
      Top             =   315
      Width           =   2490
      _Version        =   65536
      _ExtentX        =   4382
      _ExtentY        =   5906
      _StockProps     =   14
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShadowStyle     =   1
      Begin Threed.SSOption Option3D1 
         Height          =   555
         Index           =   3
         Left            =   90
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1980
         Width           =   1725
         _Version        =   65536
         _ExtentX        =   3043
         _ExtentY        =   979
         _StockProps     =   78
         Caption         =   "Reclama��es"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Font3D          =   1
      End
      Begin Threed.SSOption Option3D1 
         Height          =   555
         Index           =   2
         Left            =   90
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   1440
         Width           =   2265
         _Version        =   65536
         _ExtentX        =   3995
         _ExtentY        =   979
         _StockProps     =   78
         Caption         =   "Problemas Externos"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Font3D          =   1
      End
      Begin Threed.SSOption Option3D1 
         Height          =   555
         Index           =   1
         Left            =   90
         TabIndex        =   4
         Top             =   855
         Width           =   2220
         _Version        =   65536
         _ExtentX        =   3916
         _ExtentY        =   979
         _StockProps     =   78
         Caption         =   "Problemas Internos"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Font3D          =   1
      End
      Begin Threed.SSOption Option3D1 
         Height          =   552
         Index           =   0
         Left            =   96
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   312
         Width           =   2124
         _Version        =   65536
         _ExtentX        =   3747
         _ExtentY        =   974
         _StockProps     =   78
         Caption         =   "Problemas Gerais"
         ForeColor       =   8388608
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Font3D          =   1
      End
   End
   Begin Threed.SSOption Option3D1 
      Height          =   555
      Index           =   4
      Left            =   7110
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   2700
      Width           =   2355
      _Version        =   65536
      _ExtentX        =   4154
      _ExtentY        =   979
      _StockProps     =   78
      Caption         =   "Informa��es Gerais"
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Font3D          =   1
   End
   Begin Threed.SSCommand cmdGrafico 
      Height          =   825
      Left            =   8280
      TabIndex        =   0
      Top             =   4410
      Width           =   1140
      _Version        =   65536
      _ExtentX        =   2011
      _ExtentY        =   1455
      _StockProps     =   78
      BevelWidth      =   4
      RoundedCorners  =   0   'False
      Picture         =   "FRMCAC.frx":0000
   End
   Begin GraphLib.Graph Graph1 
      Height          =   5460
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   6945
      _Version        =   65536
      _ExtentX        =   12250
      _ExtentY        =   9631
      _StockProps     =   96
      Background      =   7
      Foreground      =   0
      GridStyle       =   1
      LeftTitle       =   "n� Recl"
      PrintStyle      =   1
      RandomData      =   1
      ColorData       =   0
      ExtraData       =   0
      ExtraData[]     =   0
      FontFamily      =   4
      FontFamily[1]   =   1
      FontFamily[3]   =   1
      FontSize        =   4
      FontSize[0]     =   100
      FontSize[1]     =   100
      FontSize[2]     =   80
      FontSize[3]     =   80
      FontStyle       =   4
      GraphData       =   0
      GraphData[]     =   0
      LabelText       =   0
      LegendText      =   0
      PatternData     =   0
      SymbolData      =   0
      XPosData        =   0
      XPosData[]      =   0
   End
   Begin VB.Menu mnuImprimir 
      Caption         =   "&Imprimir"
   End
End
Attribute VB_Name = "frmCac"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ATEND As Object
Dim ds As Object

Dim DataFatur As String
Dim tipo_Graph As Integer
Dim Style_Graph As Integer
Dim RETORNO As String
Dim OP_GRAF As Integer

Public LABEL_ESCOLHA As String

Private Sub CARREGA_GRAF(Index As Integer)

    Dim SEL As String

    Screen.MousePointer = 11
    msg "Aguarde... Consultando Atendimentos!", 1

    If Index < 3 Then

        'SELECT PARA CONSULTA DE PEDIDOS
        SEL = "SELECT COUNT(CACLIE.ATENDIMENTO.NUM_NOTA), "
        SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO "
        SEL = SEL & "FROM CACLIE.ATENDIMENTO, CACLIE.ORIGEM_PROBLEMA, CACLIE.SUBGRUPO_ORIGEM "
        SEL = SEL & "WHERE CACLIE.ATENDIMENTO.TP_ATENDIMENTO = 'R' AND "
        SEL = SEL & " (CACLIE.ATENDIMENTO.DT_ATENDIMENTO >= to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD') AND "
        SEL = SEL & " CACLIE.ATENDIMENTO.DT_ATENDIMENTO < add_months(to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD'),1) ) AND "
        If Index = 1 Then
            SEL = SEL & " (CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO <> 'BANCO' and "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO <> 'CLIENTE' and "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO <> 'FORNECEDOR' and "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO <> 'TRANSPORTADORA') AND "
        ElseIf Index = 2 Then
            SEL = SEL & " (CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO = 'BANCO' or "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO = 'CLIENTE' or "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO = 'FORNECEDOR' or "
            SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO = 'TRANSPORTADORA') AND "
        End If
        SEL = SEL & " (CACLIE.ATENDIMENTO.COD_ORPROB=CACLIE.ORIGEM_PROBLEMA.COD_ORPROB AND "
        SEL = SEL & " CACLIE.ORIGEM_PROBLEMA.COD_SUBGRUPO=CACLIE.SUBGRUPO_ORIGEM.COD_SUBGRUPO) "
        SEL = SEL & "GROUP BY CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO "
        SEL = SEL & "ORDER BY CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO ASC "

    ElseIf Index = 3 Then

        'SELECT PARA CONSULTA DE PEDIDOS
        SEL = "SELECT COUNT(CACLIE.ATENDIMENTO.NUM_NOTA), "
        SEL = SEL & " TRUNC(TO_CHAR(CACLIE.ATENDIMENTO.DT_ATENDIMENTO,'RRMMDD')), "
        SEL = SEL & " SUBSTR(TO_CHAR(CACLIE.ATENDIMENTO.DT_ATENDIMENTO,'DD'),1,2) "
        SEL = SEL & "FROM CACLIE.ATENDIMENTO "
        SEL = SEL & "WHERE CACLIE.ATENDIMENTO.TP_ATENDIMENTO = 'R' AND "
        SEL = SEL & " (CACLIE.ATENDIMENTO.DT_ATENDIMENTO >= to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD') AND "
        SEL = SEL & " CACLIE.ATENDIMENTO.DT_ATENDIMENTO < add_months(to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD'),1) ) "
        SEL = SEL & "GROUP BY TRUNC(TO_CHAR(CACLIE.ATENDIMENTO.DT_ATENDIMENTO,'RRMMDD')), "
        SEL = SEL & " SUBSTR(TO_CHAR(CACLIE.ATENDIMENTO.DT_ATENDIMENTO,'DD'),1,2) "
        SEL = SEL & "ORDER BY SUBSTR(TO_CHAR(CACLIE.ATENDIMENTO.DT_ATENDIMENTO,'DD'),1,2) ASC "

        tipo_Graph = 4

    ElseIf Index = 4 Then

        'SELECT PARA CONSULTA DE PEDIDOS
        SEL = "SELECT COUNT(CACLIE.ATENDIMENTO.NUM_NOTA), "
        SEL = SEL & " CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO "
        SEL = SEL & "FROM CACLIE.ATENDIMENTO, CACLIE.ORIGEM_PROBLEMA, CACLIE.SUBGRUPO_ORIGEM "
        SEL = SEL & "WHERE CACLIE.ATENDIMENTO.TP_ATENDIMENTO = 'I' AND "
        SEL = SEL & " (CACLIE.ATENDIMENTO.DT_ATENDIMENTO >= to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD') AND "
        SEL = SEL & " CACLIE.ATENDIMENTO.DT_ATENDIMENTO < add_months(to_date('" & frmData.mskMesAno & "'||'/01','MM/RR/DD'),1) ) AND "
        SEL = SEL & " (CACLIE.ATENDIMENTO.COD_ORPROB=CACLIE.ORIGEM_PROBLEMA.COD_ORPROB AND "
        SEL = SEL & " CACLIE.ORIGEM_PROBLEMA.COD_SUBGRUPO=CACLIE.SUBGRUPO_ORIGEM.COD_SUBGRUPO) "
        SEL = SEL & "GROUP BY CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO "
        SEL = SEL & "ORDER BY CACLIE.SUBGRUPO_ORIGEM.DESC_SUBGRUPO ASC "

    End If

    'EXECUTA O SELECT
    Set ATEND = db.DbCreateDynaset(SEL, 0&)

    If ATEND.RecordCount = 0 Then
        MsgBox "N�o h� Atendimentos nesse M�s."
        Screen.MousePointer = 0
        For i = 0 To 4
            Option3D1(i).Value = False
        Next
        Form_Load
        Exit Sub
    End If

    'PARAMETROS PARA GRAFICOS
    Graph1.Visible = True
    Graph1.Enabled = True
    Graph1.NumPoints = ATEND.RecordCount
    Graph1.NumSets = 1
    Graph1.GraphTitle = "M�s " & frmData.mskMesAno
    Graph1.GraphType = tipo_Graph
    If tipo_Graph = 2 Then Graph1.GraphStyle = 6

    'CARREGA AS VARIAVEIS DE TELA E GRAFICO
    ATEND.dbMoveFirst
    For i = 1 To ATEND.RecordCount
        Graph1.ThisPoint = i           'POSICAO NO GRAFICO
        Graph1.GraphData = ATEND(0)    'CONTEUDO DA POSICAO
        ATEND.dbMoveNext
    Next

    If Index < 3 Or Index = 4 Then
        'DEFINE LEGENDAS
        ATEND.dbMoveFirst
        For i = 1 To ATEND.RecordCount
            Graph1.ThisPoint = i           'POSICAO NO GRAFICO
            Graph1.LegendText = ATEND(1)
            ATEND.dbMoveNext
        Next
        If tipo_Graph = 4 Then
            If Index < 3 Then
                Graph1.LeftTitle = "n� Recl"
            Else
                Graph1.LeftTitle = "n� Infor"
            End If
        End If
        cmdGrafico.Enabled = True
    ElseIf Index = 3 Then
        'DEFINE LABELS
        ATEND.dbMoveFirst
        For i = 1 To ATEND.RecordCount
            Graph1.ThisPoint = i           'POSICAO NO GRAFICO
            Graph1.LabelText = ATEND(2)
            ATEND.dbMoveNext
        Next
        'For i = 1 To ATEND.RecordCount
        '    Graph1.ThisPoint = i           'POSICAO NO GRAFICO
        '    Graph1.LegendText = ""
        'Next
        Graph1.DataReset = 5
        Graph1.LeftTitle = "n� Recl"
        cmdGrafico.Picture = LoadPicture("H:\ORACLE\SISTEMAS\VB\PADRAO\GR_PIZZA.ICO")
        cmdGrafico.Enabled = False
        tipo_Graph = 4
    End If

    'PARAMETROS PARA GRAFICO
    Graph1.DrawMode = 2

    For i = 0 To 4
        Option3D1(i).ForeColor = 8388608  'Azul Inicial
        Option3D1(i).Value = False
    Next

    Option3D1(Index).ForeColor = RGB(255, 0, 0)    'Vermelho

    Screen.MousePointer = 0
    msg "", 1

End Sub

Private Sub cmdGrafico_Click()

    If Graph1.Visible = True Then
        If tipo_Graph = 4 Then
            Graph1.GraphStyle = 6
            Graph1.LeftTitle = ""
            cmdGrafico.Picture = LoadPicture("H:\ORACLE\SISTEMAS\VB\PADRAO\GR_BARRA.ICO")
            tipo_Graph = 2
            CARREGA_GRAF (OP_GRAF)
        Else
            Graph1.GraphStyle = 1
            If OP_GRAF = 4 Then
                Graph1.LeftTitle = "n� Infor"
            Else
                Graph1.LeftTitle = "n� Recl"
            End If
            cmdGrafico.Picture = LoadPicture("H:\ORACLE\SISTEMAS\VB\PADRAO\GR_PIZZA.ICO")
            tipo_Graph = 4
            CARREGA_GRAF (OP_GRAF)
        End If
    End If

End Sub

Private Sub Form_Load()

    Graph1.Visible = False
    Graph1.Enabled = False
    msg "Escolha Uma Op��o.", 1

    tipo_Graph = 4
    Style_Graph = 6

End Sub

Private Sub mnuImprimir_Click()

    Frame3D1.Visible = False
    cmdGrafico.Visible = False
    Option3D1(4).Visible = False
    Graph1.Width = 9375
    Graph1.GraphTitle = LABEL_ESCOLHA & " - M�s " & frmData.mskMesAno
    Graph1.DrawMode = 2

    frmCac.PrintForm

    Graph1.Width = 5055
    Graph1.GraphTitle = "M�s " & frmData.mskMesAno
    Graph1.DrawMode = 2
    Frame3D1.Visible = True
    cmdGrafico.Visible = True

End Sub

Private Sub Option3D1_Click(Index As Integer, Value As Integer)

    frmData.Show 1      'CHAMA O FORM DE ESCOLHA DA DATA

    If txtTF = "False" Then
        For i = 0 To 4
            Option3D1(i).Value = False
        Next
        Form_Load
        Exit Sub
    End If

    OP_GRAF = Index     'SET A OPCAO P/ O BOTAO

    If OP_GRAF = 0 Then
        LABEL_ESCOLHA = "Problemas Gerais"
    ElseIf OP_GRAF = 1 Then
        LABEL_ESCOLHA = "Problemas Internos"
    ElseIf OP_GRAF = 2 Then
        LABEL_ESCOLHA = "Problemas Externos"
    ElseIf OP_GRAF = 3 Then
        LABEL_ESCOLHA = "Reclama��es/Dia"
    End If

    CARREGA_GRAF (Index)

End Sub

