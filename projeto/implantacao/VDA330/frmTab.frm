VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmTab 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "TABELA DE PEDIDOS"
   ClientHeight    =   5940
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   6690
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5940
   ScaleWidth      =   6690
   Begin Threed.SSFrame fraTabela 
      Height          =   4470
      Left            =   1320
      TabIndex        =   0
      Top             =   540
      Width           =   3915
      _Version        =   65536
      _ExtentX        =   6906
      _ExtentY        =   7885
      _StockProps     =   14
      Caption         =   "Tabela de Pedidos"
      ForeColor       =   16711680
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   15
         Picture         =   "frmTab.frx":0000
         ScaleHeight     =   390
         ScaleWidth      =   435
         TabIndex        =   9
         Top             =   855
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture2 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   15
         Picture         =   "frmTab.frx":030A
         ScaleHeight     =   390
         ScaleWidth      =   435
         TabIndex        =   8
         Top             =   1215
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture3 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   480
         Left            =   15
         Picture         =   "frmTab.frx":0614
         ScaleHeight     =   480
         ScaleWidth      =   435
         TabIndex        =   7
         Top             =   1575
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture4 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   0
         Picture         =   "frmTab.frx":091E
         ScaleHeight     =   390
         ScaleWidth      =   495
         TabIndex        =   6
         Top             =   1935
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.PictureBox Picture5 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   15
         Picture         =   "frmTab.frx":0C28
         ScaleHeight     =   495
         ScaleWidth      =   435
         TabIndex        =   5
         Top             =   2295
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture6 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   0
         Picture         =   "frmTab.frx":0F32
         ScaleHeight     =   390
         ScaleWidth      =   435
         TabIndex        =   4
         Top             =   2655
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture7 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   15
         Picture         =   "frmTab.frx":123C
         ScaleHeight     =   390
         ScaleWidth      =   495
         TabIndex        =   3
         Top             =   3015
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.PictureBox Picture8 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   510
         Left            =   15
         Picture         =   "frmTab.frx":1546
         ScaleHeight     =   510
         ScaleWidth      =   435
         TabIndex        =   2
         Top             =   3375
         Visible         =   0   'False
         Width           =   435
      End
      Begin VB.PictureBox Picture9 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   435
         Left            =   60
         Picture         =   "frmTab.frx":1850
         ScaleHeight     =   435
         ScaleWidth      =   435
         TabIndex        =   1
         Top             =   3780
         Visible         =   0   'False
         Width           =   435
      End
      Begin MSGrid.Grid Grid1 
         Height          =   3780
         Left            =   510
         TabIndex        =   10
         Top             =   435
         Width           =   3285
         _Version        =   65536
         _ExtentX        =   5794
         _ExtentY        =   6668
         _StockProps     =   77
         ForeColor       =   -2147483640
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Rows            =   9
         Cols            =   4
         FixedRows       =   2
         FixedCols       =   0
         HighLight       =   0   'False
         MouseIcon       =   "frmTab.frx":1B5A
      End
   End
End
Attribute VB_Name = "frmTab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
'/*****************************/
'/***  MONTA O GRID TABELA  ***/
'/*****************************/
    Grid1.Cols = 3
    Grid1.Rows = 10
    For i = 0 To 2
        Grid1.ColAlignment(i) = 2
    Next i
    Grid1.FillStyle = 0

    Grid1.ColWidth(0) = 600
    'Grid1.ColWidth(1) = 600
    Grid1.ColWidth(1) = 1000
    'Grid1.ColWidth(3) = 600
    Grid1.ColWidth(2) = 1000

    Grid1.RowHeight(0) = 350
    Grid1.RowHeight(1) = 350
    Grid1.RowHeight(2) = 350
    Grid1.RowHeight(3) = 350
    Grid1.RowHeight(4) = 350
    Grid1.RowHeight(5) = 350
    Grid1.RowHeight(6) = 350
    Grid1.RowHeight(7) = 350
    Grid1.RowHeight(8) = 350
    Grid1.RowHeight(9) = 350

    'LINHA 0
    Grid1.Row = 0
    Grid1.Col = 0: Grid1.Text = ""
    ' Grid1.Col = 1:    Grid1.Text = "Pedidos"
    Grid1.Col = 1: Grid1.Text = ""
    ' Grid1.Col = 3:    Grid1.Text = "Valor"
    'LINHA 1
    Grid1.Row = 1
    Grid1.Col = 0: Grid1.Text = "Horario"
    'Grid1.Col = 1:    Grid1.Text = "Normal"
    Grid1.Col = 1: Grid1.Text = "Ped. Meta"
    'Grid1.Col = 3:    Grid1.Text = "Normal"
    Grid1.Col = 2: Grid1.Text = "Valor Meta"
    'LINHA 2
    Grid1.Row = 2
    Grid1.Col = 0: Grid1.Text = "10"
    'Grid1.Col = 1:    Grid1.Text = "480"
    'Grid1.Col = 1:    Grid1.Text = "753"
    Grid1.Col = 1: Grid1.Text = "851"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "292"
    'Grid1.Col = 2:    Grid1.Text = "477"
    'Grid1.Col = 2: Grid1.Text = "592"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "665"    'William Leite - 09/08/2012
    'LINHA 3
    Grid1.Row = 3
    Grid1.Col = 0: Grid1.Text = "11"
    'Grid1.Col = 1:    Grid1.Text = "685"
    'Grid1.Col = 1:    Grid1.Text = "1074"
    Grid1.Col = 1: Grid1.Text = "1213"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "423"
    'Grid1.Col = 2:    Grid1.Text = "663"
    'Grid1.Col = 2: Grid1.Text = "822"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "920"    'William Leite - 09/08/2012
    'LINHA 4
    Grid1.Row = 4
    Grid1.Col = 0: Grid1.Text = "12"
    'Grid1.Col = 1:    Grid1.Text = "890"
    'Grid1.Col = 1:    Grid1.Text = "1425"
    Grid1.Col = 1: Grid1.Text = "1610"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "550"
    'Grid1.Col = 2:    Grid1.Text = "880"
    'Grid1.Col = 2: Grid1.Text = "1092"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "1225"    'William Leite - 09/08/2012
    'LINHA 5
    Grid1.Row = 5
    Grid1.Col = 0: Grid1.Text = "14"
    'Grid1.Col = 1:    Grid1.Text = "1100"
    'Grid1.Col = 1:    Grid1.Text = "1742"
    Grid1.Col = 1: Grid1.Text = "1968"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "671"
    'Grid1.Col = 2:    Grid1.Text = "1076"
    'Grid1.Col = 2: Grid1.Text = "1334"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "1500"    'William Leite - 09/08/2012
    'LINHA 6
    Grid1.Row = 6
    Grid1.Col = 0: Grid1.Text = "15"
    'Grid1.Col = 1:    Grid1.Text = "1300"
    'Grid1.Col = 1:    Grid1.Text = "2071"
    Grid1.Col = 1: Grid1.Text = "2340"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "803"
    'Grid1.Col = 2:    Grid1.Text = "1280"
    'Grid1.Col = 2: Grid1.Text = "1589"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "1780"    'William Leite - 09/08/2012

    'LINHA 7
    Grid1.Row = 7
    Grid1.Col = 0: Grid1.Text = "16"
    'Grid1.Col = 1:    Grid1.Text = "1510"
    'Grid1.Col = 1:    Grid1.Text = "2338"
    Grid1.Col = 1: Grid1.Text = "2642"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "924"
    'Grid1.Col = 2:     Grid1.Text = "1446"
    'Grid1.Col = 2: Grid1.Text = "1794"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "2010"    'William Leite - 09/08/2012

    'LINHA 8
    Grid1.Row = 8
    Grid1.Col = 0: Grid1.Text = "17"
    'Grid1.Col = 1:    Grid1.Text = "1715"
    'Grid1.Col = 1:    Grid1.Text = "2721"
    Grid1.Col = 1: Grid1.Text = "3075"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "1045"
    'Grid1.Col = 2:    Grid1.Text = "1680"
    'Grid1.Col = 2: Grid1.Text = "2085"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "2340"    'William Leite - 09/08/2012
    
    'linha 9
    Grid1.Row = 9
    Grid1.Col = 0: Grid1.Text = "18"
    'Grid1.Col = 1:    Grid1.Text = "2060"
    'Grid1.Col = 1:    Grid1.Text = "3253"
    Grid1.Col = 1: Grid1.Text = "3676"    'Consultor 30 - 09/08/2010
    'Grid1.Col = 3:    Grid1.Text = "1298"
    'Grid1.Col = 2:    Grid1.Text = "2012"
    'Grid1.Col = 2: Grid1.Text = "2497"    'Consultor 30 - 09/08/2010
    Grid1.Col = 2: Grid1.Text = "2800"   'William Leite - 09/08/2012
    SETA_HORARIO

End Sub

Private Sub SETA_HORARIO()

'SETA PROXIMO HORARIO
    If Val(Format(Time, "HH")) < 10 Then
        Picture1.Visible = True
    ElseIf Val(Format(Time, "HH")) = 10 Then
        Picture2.Visible = True
    ElseIf Val(Format(Time, "HH")) = 11 Then
        Picture3.Visible = True
    ElseIf Val(Format(Time, "HH")) >= 12 And Val(Format(Time, "HH")) < 14 Then
        Picture4.Visible = True
    ElseIf Val(Format(Time, "HH")) = 14 Then
        Picture5.Visible = True
    ElseIf Val(Format(Time, "HH")) = 15 Then
        Picture6.Visible = True
    ElseIf Val(Format(Time, "HH")) = 16 Then
        Picture7.Visible = True
    ElseIf Val(Format(Time, "HH")) = 17 Then
        Picture8.Visible = True
    ElseIf Val(Format(Time, "HH")) >= 18 Then
        Picture9.Visible = True
    End If

End Sub

