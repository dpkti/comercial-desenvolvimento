VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmRegional 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Faturamento por Regional"
   ClientHeight    =   4980
   ClientLeft      =   1350
   ClientTop       =   2895
   ClientWidth     =   11940
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4980
   ScaleWidth      =   11940
   Begin VB.Frame Frame1 
      Caption         =   "Ordena��o"
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   720
      TabIndex        =   3
      Top             =   0
      Width           =   6375
      Begin VB.OptionButton Option2 
         Caption         =   "PC Quota"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4440
         TabIndex        =   9
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Vl.Total"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3360
         TabIndex        =   7
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optNome 
         Caption         =   "Nome Regional"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1800
         TabIndex        =   5
         Top             =   240
         Width           =   1455
      End
      Begin VB.OptionButton optCodigo 
         Caption         =   "Cod. Regional"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   360
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   11040
      TabIndex        =   1
      Top             =   4200
      Width           =   735
   End
   Begin MSGrid.Grid grdNF 
      Height          =   3615
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   11535
      _Version        =   65536
      _ExtentX        =   20346
      _ExtentY        =   6376
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "* Valor da Quota � cadastrado pela Controladoria"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   8
      Top             =   4680
      Width           =   3465
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Intervalo de atualiza��o: a cada 15 minutos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   195
      Left            =   4680
      TabIndex        =   6
      Top             =   4320
      Width           =   3180
   End
   Begin VB.Label lblAtualizacao 
      AutoSize        =   -1  'True
      Caption         =   "Atualizado em:"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   360
      TabIndex        =   2
      Top             =   4320
      Width           =   1035
   End
End
Attribute VB_Name = "frmRegional"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub optCodigo_Click()
    Call Busca_Regional(1)
End Sub

Private Sub Option1_Click()
    Call Busca_Regional(3)
End Sub

Private Sub Option2_Click()
    Call Busca_Regional(4)
End Sub

Private Sub optNome_Click()
    Call Busca_Regional(2)
End Sub

