VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{0842D103-1E19-101B-9AAF-1A1626551E7C}#1.0#0"; "GRAPH32.OCX"
Begin VB.Form frmNotas 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   Caption         =   "Notas Fiscais Emitidas"
   ClientHeight    =   5520
   ClientLeft      =   30
   ClientTop       =   1320
   ClientWidth     =   9540
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5520
   ScaleWidth      =   9540
   Begin Threed.SSFrame Frame3D1 
      Height          =   4920
      Left            =   90
      TabIndex        =   0
      Top             =   0
      Width           =   9330
      _Version        =   65536
      _ExtentX        =   16447
      _ExtentY        =   8678
      _StockProps     =   14
      ForeColor       =   8388608
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShadowStyle     =   1
      Begin GraphLib.Graph Graph1 
         DragIcon        =   "FRMNOTAS.frx":0000
         Height          =   4695
         Left            =   45
         TabIndex        =   1
         Top             =   135
         Width           =   9195
         _Version        =   65536
         _ExtentX        =   16219
         _ExtentY        =   8281
         _StockProps     =   96
         Enabled         =   0   'False
         Foreground      =   9
         GraphType       =   6
         GridStyle       =   3
         LegendStyle     =   1
         LineStats       =   2
         PrintStyle      =   2
         RandomData      =   1
         ColorData       =   5
         ColorData[0]    =   4
         ColorData[1]    =   4
         ColorData[2]    =   4
         ColorData[3]    =   4
         ColorData[4]    =   2
         ExtraData       =   0
         ExtraData[]     =   0
         FontFamily      =   4
         FontFamily[3]   =   1
         FontSize        =   4
         FontSize[0]     =   100
         FontSize[1]     =   150
         FontSize[2]     =   70
         FontSize[3]     =   70
         FontStyle       =   4
         FontStyle[0]    =   7
         GraphData       =   0
         GraphData[]     =   0
         LabelText       =   0
         LegendText      =   0
         PatternData     =   0
         SymbolData      =   4
         SymbolData[1]   =   2
         SymbolData[2]   =   3
         SymbolData[3]   =   1
         XPosData        =   0
         XPosData[]      =   0
      End
   End
   Begin Threed.SSCommand cmdBusca 
      Height          =   468
      Left            =   8208
      TabIndex        =   2
      Top             =   4992
      Width           =   1236
      _Version        =   65536
      _ExtentX        =   2170
      _ExtentY        =   820
      _StockProps     =   78
      Caption         =   "Busca"
      ForeColor       =   16711680
   End
End
Attribute VB_Name = "frmNotas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TOTNOTA As Object
Dim ds As Object

Private Sub cmdBusca_Click()

    Dim SEL As String
    Dim HH As Date

    'HH = Time

    Screen.MousePointer = 11
    msg "Aguarde... Consultando Notas Fiscais Emitidas!", 1

    'SELECT PARA CONSULTA DE PEDIDOS
    SEL = "Select TRUNC(TO_CHAR(DT_EMISSAO_NOTA,'RRMMDD')), "
    SEL = SEL & "SUBSTR(TO_CHAR(DT_EMISSAO_NOTA,'DD/MM'),1,5) DIA_MES, "
    SEL = SEL & "COUNT(*) TOTREG "
    SEL = SEL & "From PEDNOTA_VENDA , DATAS "
    SEL = SEL & "Where FL_GER_NFIS||''='S' and "
    SEL = SEL & " SITUACAO = 0 and "
    SEL = SEL & " TP_TRANSACAO = 1 and "
    SEL = SEL & " DT_EMISSAO_NOTA >= DT_FATURAMENTO - 30 and "
    SEL = SEL & " DT_EMISSAO_NOTA < DT_FATURAMENTO +1 "
    SEL = SEL & "Group by TRUNC(TO_CHAR(DT_EMISSAO_NOTA,'RRMMDD')), "
    SEL = SEL & " SUBSTR(TO_CHAR(DT_EMISSAO_NOTA,'DD/MM'),1,5) "
    SEL = SEL & "Order by TRUNC(TO_CHAR(DT_EMISSAO_NOTA,'RRMMDD')) "

    'EXECUTA O SELECT
    Set TOTNOTA = db.DbCreateDynaset(SEL, 0&)

    'MsgBox DateDiff("s", HH, Time)

    'PARAMETROS PARA GRAFICOS
    Graph1.Visible = True
    Graph1.Enabled = True
    Graph1.NumPoints = TOTNOTA.RecordCount
    Graph1.NumSets = 2
    Graph1.ThisSet = 1
    Graph1.GraphStyle = 5

    'CARREGA VARIAVEIS DE DADOS DO GRAFICO
    TOTNOTA.dbMoveFirst
    For i = 1 To TOTNOTA.RecordCount
        Graph1.ThisPoint = i
        'Graph1.GraphData = TOTNOTA(2)
        Graph1.GraphData = TOTNOTA!TOTREG
        TOTNOTA.dbMoveNext
    Next

    'DEFINE LEGENDAS
    TOTNOTA.dbMoveFirst
    For i = 1 To TOTNOTA.RecordCount
        'Graph1.LabelText = TOTNOTA(1)
        Graph1.LabelText = TOTNOTA!DIA_MES
        TOTNOTA.dbMoveNext
    Next

    Graph1.ThisSet = 2
    Graph1.GraphStyle = 0
    Graph1.ColorData = 1
    'Graph1.SymbolData = 0

    'CARREGA VARIAVEIS DE DADOS DO GRAFICO
    TOTNOTA.dbMoveFirst
    For i = 1 To TOTNOTA.RecordCount
        Graph1.ThisPoint = i
        Graph1.GraphData = 2060
        TOTNOTA.dbMoveNext
    Next

    'DEFINE LEGENDAS
    Graph1.LegendText = "Real"
    Graph1.LegendText = "Meta 2060"

    'PARAMETROS PARA GRAFICO
    'Graph1.GraphStyle = 7
    'Graph1.IndexStyle = 1
    Graph1.BottomTitle = "Dias"
    Graph1.LeftTitle = "n� Notas"
    Graph1.DrawMode = 2

    Screen.MousePointer = 0
    msg "", 1

End Sub

Private Sub Form_Load()
    Graph1.Visible = False
    Graph1.Enabled = False
    cmdBusca_Click
End Sub

Private Sub Timer1_Timer()
End Sub

