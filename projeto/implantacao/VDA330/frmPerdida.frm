VERSION 4.00
Begin VB.Form frmPerdida 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "An�lise de Venda Perdida"
   ClientHeight    =   6750
   ClientLeft      =   1065
   ClientTop       =   1470
   ClientWidth     =   11550
   Height          =   7155
   Left            =   1005
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6750
   ScaleWidth      =   11550
   Top             =   1125
   Width           =   11670
   Begin VB.PictureBox pctAlerta 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   525
      Left            =   8280
      Picture         =   "frmPerdida.frx":0000
      ScaleHeight     =   525
      ScaleWidth      =   495
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.CommandButton cmdPesquisa 
      Caption         =   "Pesquisar"
      Height          =   375
      Left            =   5880
      TabIndex        =   6
      Top             =   240
      Width           =   1575
   End
   Begin VB.TextBox txtDT_FIM 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   3960
      TabIndex        =   5
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox txtDT_INICIO 
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   1440
      TabIndex        =   4
      Top             =   240
      Width           =   1215
   End
   Begin TabDlg.SSTab sstabPerdida 
      Height          =   5895
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   10398
      _Version        =   393216
      Tabs            =   4
      TabHeight       =   520
      TabCaption(0)   =   "CD"
      TabPicture(0)   =   "frmPerdida.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "grdDPK"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "FORNECEDOR"
      TabPicture(1)   =   "frmPerdida.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "grdForn"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "CD - FORNECEDOR"
      TabPicture(2)   =   "frmPerdida.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label4"
      Tab(2).Control(1)=   "grdLoja"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "AN�LISE POR FORNECEDOR"
      TabPicture(3)   =   "frmPerdida.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "grdAnalise"
      Tab(3).ControlCount=   1
      Begin MSGrid.Grid grdAnalise 
         Height          =   3855
         Left            =   -73560
         TabIndex        =   11
         Top             =   1320
         Visible         =   0   'False
         Width           =   7215
         _Version        =   65536
         _ExtentX        =   12726
         _ExtentY        =   6800
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Duplo Click para analisar fornecedor espec�fico"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   -74880
         TabIndex        =   10
         Top             =   5280
         Width           =   4050
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Duplo Click para visualizar por CD / Fornecedor"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   195
         Left            =   360
         TabIndex        =   9
         Top             =   5520
         Width           =   4125
      End
      Begin MSGrid.Grid grdLoja 
         Height          =   3855
         Left            =   -74880
         TabIndex        =   8
         Top             =   1260
         Visible         =   0   'False
         Width           =   10935
         _Version        =   65536
         _ExtentX        =   19288
         _ExtentY        =   6800
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSGrid.Grid grdForn 
         Height          =   3855
         Left            =   -74760
         TabIndex        =   7
         Top             =   1080
         Visible         =   0   'False
         Width           =   10215
         _Version        =   65536
         _ExtentX        =   18018
         _ExtentY        =   6800
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSGrid.Grid grdDPK 
         Height          =   4215
         Left            =   360
         TabIndex        =   1
         Top             =   960
         Visible         =   0   'False
         Width           =   10215
         _Version        =   65536
         _ExtentX        =   18018
         _ExtentY        =   7435
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Dt.Fim"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3000
      TabIndex        =   3
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Dt.Inicio"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   480
      TabIndex        =   2
      Top             =   240
      Width           =   855
   End
End
Attribute VB_Name = "frmPerdida"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit
Public lngCod_loja As Long
Public lngCod_Forn As Long




Private Sub cmdPesquisa_Click()
  grdDPK.Visible = False
  grdforn.Visible = False
  grdLoja.Visible = False
  Call PR_DPK
  Call PR_FORN
  sstabPerdida.Tab = 0
  
End Sub


Public Sub PR_DPK()
  Dim i As Long
  Dim ss As Object
  
  
  On Error GoTo Trata_erro
    
   grdDPK.Visible = False
   DoEvents
   Screen.MousePointer = 11
   OraParameters.Remove "loja"
   OraParameters.Add "loja", 0, 1
   OraParameters.Remove "ini"
   OraParameters.Add "ini", CDate(txtDT_INICIO), 1
   OraParameters.Remove "fim"
   OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
    V_SQL = "Begin producao.pck_vda_perdida.PR_loja(:vCursor,:loja,:ini,:fim,:vErro);END;"

  
    db.ExecuteSQL V_SQL
  
    Set ss = db.Parameters("vCursor").Value
  
    If ss.EOF Then
      MsgBox "N�o existe informa��o para o per�odo escolhido", vbInformation, "Aten��o"
      Screen.MousePointer = 0
      grdDPK.Visible = False
    Else
       'carrega dados
    With grdDPK
        .Cols = 10
        .Rows = ss.RecordCount + 2
        .ColWidth(0) = 500
        .ColWidth(1) = 1200
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 1000
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        .ColWidth(9) = 1000
        
        .Row = 0
        .Col = 0
        .Text = "  COD"
        .Col = 1
        .Text = "  LOJA"
        .Col = 2
        .Text = "  VL_CP"
        .Col = 3
        .Text = "VL_SP"
        .Col = 4
        .Text = "CP_ACUM"
        .Col = 5
        .Text = "SP_ACUM"
        .Col = 6
        .Text = "VL_CP_DIA"
        .Col = 7
        .Text = "VL_SP_DIA"
        .Col = 8
        .Text = "CP_DIA"
        .Col = 9
        .Text = "SP_DIA"
        
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!cod
            .Col = 1
            .Text = ss!loja
            .Col = 2
            .ColAlignment(2) = 1
            .Text = Format(ss!VL_CP, "0")
            .Col = 3
            .ColAlignment(3) = 1
            .Text = Format(ss!VL_SP, "0")
            .Col = 4
            .ColAlignment(4) = 1
            .Text = Format(ss!PC_CP_ACUM, "0.00")
             If ss!PC_CP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(ss!PC_sP_ACUM, "0.00")
             If ss!PC_sP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(ss!VL_CP_DIA, "0")
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Format(ss!VL_SP_DIA, "0")
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format(ss!PC_CP_dia, "0.00")
            If ss!PC_CP_dia > 7 Then
              .Picture = pctAlerta
            End If
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format(ss!PC_sP_dia, "0.00")
             If ss!PC_sP_dia > 7 Then
              .Picture = pctAlerta
            End If
             
            ss.MoveNext
        Next
total:
        OraParameters.Remove "ini"
        OraParameters.Add "ini", CDate(txtDT_INICIO), 1
        OraParameters.Remove "fim"
        OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
        V_SQL = "Begin producao.pck_vda_perdida.PR_DPK_TOTAL(:vCursor,:ini,:fim,:vErro);END;"

  
        db.ExecuteSQL V_SQL
  
         Set ss = db.Parameters("vCursor").Value
            '.Row = i + 1
            .Col = 0
            .Text = " "
            .Col = 1
            .Text = "TOTAL"
            .Col = 2
            .ColAlignment(2) = 1
            .Text = Format(ss!VL_CP, "0")
            .Col = 3
            .ColAlignment(3) = 1
            .Text = Format(ss!VL_SP, "0")
            .Col = 4
            .ColAlignment(4) = 1
            .Text = Format(ss!PC_CP_ACUM, "0.00")
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(ss!PC_sP_ACUM, "0.00")
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(ss!VL_CP_DIA, "0")
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Format(ss!VL_SP_DIA, "0")
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format(ss!PC_CP_dia, "0.00")
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format(ss!PC_sP_dia, "0.00")
             
        
        .Row = 1
    End With
    
   
    
    
    grdDPK.Visible = True
    Screen.MousePointer = 0
    
    
    End If
    
    Exit Sub
Trata_erro:
    If Err = 13 Then
     GoTo total
    End If

End Sub
Public Sub PR_ANALISE()
  Dim i As Long
  Dim ss As Object
  
  
  On Error GoTo Trata_erro
    
   grdAnalise.Visible = False
   DoEvents
   Screen.MousePointer = 11
   OraParameters.Remove "loja"
   OraParameters.Add "loja", lngCod_loja, 1
   OraParameters.Remove "FORN"
   OraParameters.Add "FORN", lngCod_Forn, 1
   OraParameters.Remove "ini"
   OraParameters.Add "ini", CDate(txtDT_INICIO), 1
   OraParameters.Remove "fim"
   OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
    V_SQL = "Begin producao.pck_vda_perdida.PR_ANALISE_FORN(:vCursor,:loja,:FORN,:ini,:fim,:vErro);END;"

  
    db.ExecuteSQL V_SQL
  
    Set ss = db.Parameters("vCursor").Value
  
    If ss.EOF Then
      MsgBox "N�o existe informa��o para o per�odo escolhido", vbInformation, "Aten��o"
      Screen.MousePointer = 0
      grdAnalise.Visible = False
      Exit Sub
   End If
       'carrega dados
    With grdAnalise
        .Cols = 7
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 800
        .ColWidth(1) = 800
        .ColWidth(2) = 1000
        .ColWidth(3) = 800
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 1000
        
        .Row = 0
        .Col = 0
        .Text = "DATA"
        .Col = 1
        .Text = "COD"
        .Col = 2
        .Text = "LOJA"
        .Col = 3
        .Text = "FORN"
        .Col = 4
        .Text = "SIGLA"
        .Col = 5
        .Text = "PC_CP"
        .Col = 6
        .Text = "PC_SP"
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!DATA
            .Col = 1
            .Text = ss!cod_loja
            .Col = 2
            .Text = ss!loja
            .Col = 3
            .Text = ss!forn
            .Col = 4
            .Text = ss!sigla
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(ss!PC_CP_ACUM, "0.00")
             If ss!PC_CP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(ss!PC_sP_ACUM, "0.00")
             If ss!PC_sP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
             
            ss.MoveNext
        Next
      End With
    
    grdAnalise.Visible = True
    Screen.MousePointer = 0
    
    
    
    
    Exit Sub
Trata_erro:
   
     MsgBox "Ocorreu o erro : " & Err.Description
   
End Sub

Public Sub PR_FORN()
  Dim i As Long
  Dim ss As Object
  
  
  On Error GoTo Trata_erro
    
   grdforn.Visible = False
   DoEvents
   Screen.MousePointer = 11
   OraParameters.Remove "forn"
   OraParameters.Add "forn", 0, 1
   OraParameters.Remove "ini"
   OraParameters.Add "ini", CDate(txtDT_INICIO), 1
   OraParameters.Remove "fim"
   OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
    V_SQL = "Begin producao.pck_vda_perdida.PR_forn(:vCursor,:forn,:ini,:fim,:vErro);END;"

  
    db.ExecuteSQL V_SQL
  
    Set ss = db.Parameters("vCursor").Value
  
    If ss.EOF Then
      MsgBox "N�o existe informa��o para o per�odo escolhido", vbInformation, "Aten��o"
      Screen.MousePointer = 0
      grdforn.Visible = False
    Else
       'carrega dados
    With grdforn
        .Cols = 10
        .Rows = ss.RecordCount + 2
        .ColWidth(0) = 500
        .ColWidth(1) = 1200
        .ColWidth(2) = 1000
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 1000
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        .ColWidth(9) = 1000
        
        .Row = 0
        .Col = 0
        .Text = "  COD"
        .Col = 1
        .Text = "  FORN"
        .Col = 2
        .Text = "  VL_CP"
        .Col = 3
        .Text = "VL_SP"
        .Col = 4
        .Text = "CP_ACUM"
        .Col = 5
        .Text = "SP_ACUM"
        .Col = 6
        .Text = "VL_CP_DIA"
        .Col = 7
        .Text = "VL_SP_DIA"
        .Col = 8
        .Text = "CP_DIA"
        .Col = 9
        .Text = "SP_DIA"
        
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!forn
            .Col = 1
            .Text = ss!sigla
            .Col = 2
            .ColAlignment(2) = 1
            .Text = Format(ss!VL_CP, "0")
            .Col = 3
            .ColAlignment(3) = 1
            .Text = Format(ss!VL_SP, "0")
            .Col = 4
            .ColAlignment(4) = 1
            .Text = Format(ss!PC_CP_ACUM, "0.00")
             If ss!PC_CP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(ss!PC_sP_ACUM, "0.00")
             If ss!PC_sP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(ss!VL_CP_DIA, "0")
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Format(ss!VL_SP_DIA, "0")
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format(ss!PC_CP_dia, "0.00")
             If ss!PC_CP_dia > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format(ss!PC_sP_dia, "0.00")
             If ss!PC_sP_dia > 7 Then
              .Picture = pctAlerta
             End If
            ss.MoveNext
        Next
        .Row = 1
    End With
    
    
total:
        OraParameters.Remove "ini"
        OraParameters.Add "ini", CDate(txtDT_INICIO), 1
        OraParameters.Remove "fim"
        OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
        V_SQL = "Begin producao.pck_vda_perdida.PR_FORN_TOTAL(:vCursor,:ini,:fim,:vErro);END;"

  
        db.ExecuteSQL V_SQL
  
         Set ss = db.Parameters("vCursor").Value
            '.Row = i + 1
            grdforn.Col = 0
            grdforn.Text = " "
            grdforn.Col = 1
            grdforn.Text = "TOTAL"
            grdforn.Col = 2
            grdforn.ColAlignment(2) = 1
            grdforn.Text = Format(ss!VL_CP, "0")
            grdforn.Col = 3
            grdforn.ColAlignment(3) = 1
            grdforn.Text = Format(ss!VL_SP, "0")
            grdforn.Col = 4
            grdforn.ColAlignment(4) = 1
            grdforn.Text = Format(ss!PC_CP_ACUM, "0.00")
            grdforn.Col = 5
            grdforn.ColAlignment(5) = 1
            grdforn.Text = Format(ss!PC_sP_ACUM, "0.00")
            grdforn.Col = 6
            grdforn.ColAlignment(6) = 1
            grdforn.Text = Format(ss!VL_CP_DIA, "0")
            grdforn.Col = 7
            grdforn.ColAlignment(7) = 1
            grdforn.Text = Format(ss!VL_SP_DIA, "0")
            grdforn.Col = 8
            grdforn.ColAlignment(8) = 1
            grdforn.Text = Format(ss!PC_CP_dia, "0.00")
            grdforn.Col = 9
            grdforn.ColAlignment(9) = 1
            grdforn.Text = Format(ss!PC_sP_dia, "0.00")
             
        
        grdforn.Row = 1
    
   
    
    
    frmPerdida.grdforn.Visible = True
    Screen.MousePointer = 0
    
    
    End If
    
    Exit Sub
Trata_erro:
    If Err = 13 Then
      GoTo total
    End If

End Sub
Public Sub PR_LOJA()
  Dim i As Long
  Dim ss As Object
  
  
  On Error GoTo Trata_erro
    
   grdLoja.Visible = False
   DoEvents
   Screen.MousePointer = 11
   OraParameters.Remove "loja"
   OraParameters.Add "loja", lngCod_loja, 1
   OraParameters.Remove "forn"
   OraParameters.Add "forn", 0, 1
   OraParameters.Remove "ini"
   OraParameters.Add "ini", CDate(txtDT_INICIO), 1
   OraParameters.Remove "fim"
   OraParameters.Add "fim", CDate(txtDT_FIM), 1
  
    
    V_SQL = "Begin producao.pck_vda_perdida.PR_LOJA_FORN(:vCursor,:loja,:forn,:ini,:fim,:vErro);END;"

  
    db.ExecuteSQL V_SQL
  
    Set ss = db.Parameters("vCursor").Value
  
    If ss.EOF Then
      MsgBox "N�o existe informa��o para o per�odo escolhido", vbInformation, "Aten��o"
      Screen.MousePointer = 0
      grdLoja.Visible = False
    Else
       'carrega dados
    With grdLoja
        .Cols = 12
        .Rows = ss.RecordCount + 2
        .ColWidth(0) = 500
        .ColWidth(1) = 1200
        .ColWidth(2) = 500
        .ColWidth(3) = 1000
        .ColWidth(4) = 1000
        .ColWidth(5) = 1000
        .ColWidth(6) = 1000
        .ColWidth(7) = 1000
        .ColWidth(8) = 1000
        .ColWidth(9) = 1000
        .ColWidth(10) = 1000
        .ColWidth(11) = 1000
        
        .Row = 0
        .Col = 0
        .Text = "  COD"
        .Col = 1
        .Text = "  LOJA"
        .Col = 2
        .Text = "  COD"
        .Col = 3
        .Text = "  FORN"
        .Col = 4
        .Text = "  VL_CP"
        .Col = 5
        .Text = "VL_SP"
        .Col = 6
        .Text = "CP_ACUM"
        .Col = 7
        .Text = "SP_ACUM"
        .Col = 8
        .Text = "VL_CP_DIA"
        .Col = 9
        .Text = "VL_SP_DIA"
        .Col = 10
        .Text = "CP_DIA"
        .Col = 11
        .Text = "SP_DIA"
        
        
        For i = 1 To .Rows - 1
            .Row = i
            
            .Col = 0
            .Text = ss!cod_loja
            .Col = 1
            .Text = ss!loja
            .Col = 2
            .Text = ss!forn
            .Col = 3
            .Text = ss!sigla
            .Col = 4
            .ColAlignment(4) = 1
            .Text = Format(ss!VL_CP, "0")
            .Col = 5
            .ColAlignment(5) = 1
            .Text = Format(ss!VL_SP, "0")
            .Col = 6
            .ColAlignment(6) = 1
            .Text = Format(ss!PC_CP_ACUM, "0.00")
             If ss!PC_CP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 7
            .ColAlignment(7) = 1
            .Text = Format(ss!PC_sP_ACUM, "0.00")
             If ss!PC_sP_ACUM > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 8
            .ColAlignment(8) = 1
            .Text = Format(ss!VL_CP_DIA, "0")
            .Col = 9
            .ColAlignment(9) = 1
            .Text = Format(ss!VL_SP_DIA, "0")
            .Col = 10
            .ColAlignment(10) = 1
            .Text = Format(ss!PC_CP_dia, "0.00")
             If ss!PC_CP_dia > 7 Then
              .Picture = pctAlerta
             End If
            .Col = 11
            .ColAlignment(11) = 1
            .Text = Format(ss!PC_sP_dia, "0.00")
             If ss!PC_sP_dia > 7 Then
              .Picture = pctAlerta
             End If
             
            ss.MoveNext
        Next
        .Row = 1
    End With
    
total:
        OraParameters.Remove "loja"
        OraParameters.Add "loja", lngCod_loja, 1
      '  OraParameters.Remove "forn"
      '  OraParameters.Add "forn", lngCod_Forn, 1
        OraParameters.Remove "ini"
        OraParameters.Add "ini", CDate(txtDT_INICIO), 1
        OraParameters.Remove "fim"
        OraParameters.Add "fim", CDate(txtDT_FIM), 1
        
         V_SQL = "Begin producao.pck_vda_perdida.PR_LOJA_FORN_TOTAL(:vCursor,:loja,:ini,:fim,:vErro);END;"
  
        db.ExecuteSQL V_SQL
  
         Set ss = db.Parameters("vCursor").Value
            '.Row = i + 1
            grdLoja.Col = 0
            grdLoja.Text = " "
            grdLoja.Col = 1
            grdLoja.Text = "TOTAL"
            grdLoja.Col = 4
            grdLoja.ColAlignment(4) = 1
            grdLoja.Text = Format(ss!VL_CP, "0")
            grdLoja.Col = 5
            grdLoja.ColAlignment(5) = 1
            grdLoja.Text = Format(ss!VL_SP, "0")
            grdLoja.Col = 6
            grdLoja.ColAlignment(6) = 1
            grdLoja.Text = Format(ss!PC_CP_ACUM, "0.00")
            grdLoja.Col = 7
            grdLoja.ColAlignment(7) = 1
            grdLoja.Text = Format(ss!PC_sP_ACUM, "0.00")
            grdLoja.Col = 8
            grdLoja.ColAlignment(8) = 1
            grdLoja.Text = Format(ss!VL_CP_DIA, "0")
            grdLoja.Col = 9
            grdLoja.ColAlignment(9) = 1
            grdLoja.Text = Format(ss!VL_SP_DIA, "0")
            grdLoja.Col = 10
            grdLoja.ColAlignment(10) = 1
            grdLoja.Text = Format(ss!PC_CP_dia, "0.00")
            grdLoja.Col = 11
            grdLoja.ColAlignment(11) = 1
            grdLoja.Text = Format(ss!PC_sP_dia, "0.00")
             
        
        grdLoja.Row = 1
    
    
    frmPerdida.grdLoja.Visible = True
    Screen.MousePointer = 0
    
    
    End If
    
    Exit Sub
Trata_erro:
    If Err = 13 Then
     GoTo total
    End If

End Sub


Private Sub grdDPK_DblClick()
  grdDPK.Col = 0
  lngCod_loja = grdDPK.Text
  grdDPK.Col = 2
  lngCod_Forn = grdDPK.Text
  Call PR_LOJA
  sstabPerdida.Tab = 2
End Sub

Private Sub grdLoja_DblClick()
  grdLoja.Col = 2
  lngCod_Forn = grdLoja.Text
  grdLoja.Col = 0
  lngCod_loja = grdLoja.Text
  
  Call PR_ANALISE
  sstabPerdida.Tab = 3
End Sub

Private Sub txtDT_FIM_KeyPress(KeyAscii As Integer)
  Call DATA(KeyAscii, txtDT_FIM)
End Sub


Private Sub txtDT_INICIO_KeyPress(KeyAscii As Integer)
  Call DATA(KeyAscii, txtDT_INICIO)
End Sub


