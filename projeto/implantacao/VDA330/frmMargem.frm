VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmMargem 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Margem"
   ClientHeight    =   4320
   ClientLeft      =   1140
   ClientTop       =   2310
   ClientWidth     =   8730
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4320
   ScaleWidth      =   8730
   Begin VB.TextBox Text1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   285
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   2
      Text            =   "frmMargem.frx":0000
      Top             =   3480
      Width           =   8295
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sair"
      Height          =   375
      Left            =   7680
      TabIndex        =   1
      Top             =   3840
      Width           =   735
   End
   Begin MSGrid.Grid grdMargem 
      Height          =   3015
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   8175
      _Version        =   65536
      _ExtentX        =   14420
      _ExtentY        =   5318
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMargem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdSair_Click()
    Unload Me
End Sub
Private Sub grdMargem_DblClick()
' Declarando vari�veis para passar parametros para o VDA030
Dim strparametros As String
Dim intlinha As Integer
intlinha = grdMargem.Row

' Passando Parametros para VDA030
grdMargem.Col = 0
strparametros = "VDA330;" & grdMargem.Text  'Aplicativo que est� encaminhando os dados
grdMargem.Col = 6  ' Parametro N� do Pedido
strparametros = strparametros & ";" & grdMargem.Text
grdMargem.Col = 7  ' Parametro Seq. do Pedido
strparametros = strparametros & ";" & grdMargem.Text
grdMargem.Col = 8  'Parametro N� da Loja=Dep�sito no VDA030
strparametros = strparametros & ";" & Format(CInt(grdMargem.Text), "00")
    
    On Error GoTo Trata_erro
    'Chamada do Aplicativo VDA030 em Produ��o, Identico ao caminho utilizado no VDA070
    Shell ("H:\ORACLE\SISTEMAS\VB\32bits\VDA030.EXE " & strparametros), 1
    'Chamada do Aplicativo VDA030 no Ambiente de desenvolvimento - Jairo Almeida 16-02-2012 - SDS2516
     'Shell ("C:\VDA030\VDA030.EXE " & strparametros), 1
    Exit Sub
Trata_erro:
    If Err = 53 Then
        MsgBox "Consulta pedido n�o dispon�vel", vbInformation, "Aten��o"
        Exit Sub
    End If
End Sub
