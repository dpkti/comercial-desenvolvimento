VERSION 4.00
Begin VB.MDIForm MDIForm1 
   BackColor       =   &H8000000C&
   Caption         =   "CONSULTA PEDIDO INDENT"
   ClientHeight    =   5940
   ClientLeft      =   1140
   ClientTop       =   1800
   ClientWidth     =   6690
   Height          =   6630
   Icon            =   "MDIForm1.frx":0000
   Left            =   1080
   LinkTopic       =   "MDIForm1"
   Top             =   1170
   Width           =   6810
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6690
      _ExtentX        =   11800
      _ExtentY        =   741
      Appearance      =   1
      _Version        =   327682
   End
   Begin VB.Menu mnuConsulta 
      Caption         =   "&Consulta"
   End
   Begin VB.Menu mnuSobre 
      Caption         =   "S&obre"
   End
   Begin VB.Menu mnuSair 
      Caption         =   "&Sair"
   End
End
Attribute VB_Name = "MDIForm1"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub MDIForm_Load()
      On Error GoTo TrataErro

    Dim strLogin As String
    Dim ss As Object
    Dim strMsg As String
    
    
    If App.PrevInstance Then
      MsgBox "J� EXISTE UMA INST�NCIA DO PROGRAMA NO AR"
      End
    End If
    
    Call Get_CD(Mid(App.Path, 1, 1))

    If strTp_banco = "U" Then
        strTabela_Banco = "PRODUCAO."
    Else
        strTabela_Banco = "DEP" & Format(lngCD, "00") & "."
    End If

    'posicionar tela
    MDIForm1.Top = 0
    MDIForm1.Left = 0
    MDIForm1.Width = Screen.Width
    MDIForm1.Height = Screen.Height
   
    
            
    
    'Conexao oracle
    Set orasession = CreateObject("oracleinprocserver.xorasession")
    Set oradatabase = orasession.OpenDatabase("PRODUCAO", "VDA800/PROD", 0&)
    'Set oradatabase = orasession.OpenDatabase("desenv", "PRODUCAO/des", 0&)
    
    
   
    'carregar data de faturamento,uf origem, filial origem
          SQL = "select a.dt_faturamento, a.dt_real, "
    SQL = SQL & " to_char(b.cod_filial,'0009') filial, "
    SQL = SQL & " d.cod_uf cod_uf,"
    SQL = SQL & " to_char(b.cod_loja,'09') ||'-'||e.nome_fantasia deposito_default"
    SQL = SQL & " from datas a, " & strTabela_Banco & "deposito b, filial c,  cidade d, loja e "
    SQL = SQL & " where  b.cod_filial=c.cod_filial and "
    SQL = SQL & " b.cod_loja=e.cod_loja and e.cod_cidade=d.cod_cidade "
    
    Set ss = oradatabase.dbcreatedynaset(SQL, 8&)
         
     
     
    If ss.EOF And ss.BOF Then
        Call Process_Line_Errors(SQL)
    Else
        Data_Faturamento = Format(CDate(ss!dt_faturamento), "dd/mm/yyyy")
        Filial_Ped = ss!filial
        UF_origem = ss!cod_uf
        Deposito_default = ss!Deposito_default
    End If
    
    frmFatu.Show
    
    Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Or Err = 3197 Then
      Resume
    Else
      Call Process_Line_Errors(SQL)
    End If

End Sub


Private Sub mnuConsulta_Click()
  frmFatu.Show
End Sub


Private Sub mnuSair_Click()
  End
End Sub


Private Sub mnuSobre_Click()
  Unload frmFatu
  frmSobre.Show
End Sub


