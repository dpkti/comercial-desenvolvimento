Attribute VB_Name = "Module1"
Option Explicit

Public oradatabase As Object
Public orasession As Object
Public oradynaset As Object
Public ss As Object
Public SQL As String
Public txtResposta As String
Public Data_Faturamento As Date
Public Filial_Ped As String
Public UF_origem As String * 2
Public Deposito_default As String
Public strPath As String
Public dbAccess As Database
Public lngNum_Pedido As Long
Public lngSEQ_PEDIDO As Long
Public bTipo_Indent As Byte
Public lngQtd_inf As Long
Public lngQtd_dig As Long
Public lngCod_Loja As Long
Public lngCod_Repres As Long
Global Const Branco = &HFFFFFF
Global Const Vermelho = &H8080FF
Global Const Cinza = &H8000000F

Public bTipo_Item As Byte
Public strFl_Lib As String * 1
Public strTipo As String
Public cod_cancel As Integer
Public Fl_Banco As String * 1
Public strTabela_Banco As String
Function Maiusculo(KeyAscii As Integer) As Integer
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
    Maiusculo = KeyAscii
End Function
 Sub Process_Line_Errors(ByRef SQL)
    Dim iFnum As Integer
    'On Error GoTo Handler_Process_Line_Errors
        
    If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
      Resume
    Else
      MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
     'cursor
     Screen.MousePointer = vbDefault
     If Fl_Banco = "S" Then
       dbAccess.Close
     End If
     'para a aplicacao
     End
    End If
     Exit Sub

Handler_Process_Line_Errors:
    DoEvents
    Resume Next

End Sub
Sub Data(ByRef KeyAscii, ByRef txtCampo)
    On Error GoTo TrataErro

    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub


Function FmtBR(ByVal Valor) As String

    Dim Temp As String
    Dim i As Integer
    
    Temp = Trim(Valor)
        
    For i = 1 To Len(Temp)
        If Mid$(Temp, i, 1) = "," Then
            Mid$(Temp, i, 1) = "."
        End If
    Next i

    FmtBR = Temp

End Function

Function Texto(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Texto = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
        KeyAscii = 0
        Beep
    Else
        KeyAscii = Asc(UCase(Chr$(KeyAscii)))
    End If
    
    Texto = KeyAscii
End Function


Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Valor = KeyAscii
        Exit Function
    End If
    
    If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
        If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
            KeyAscii = 0
            Beep
        End If
    Else
        If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
            KeyAscii = 0
            Beep
        End If
    End If
    
    Valor = KeyAscii
End Function
Function Tira_Caracter(Palavra)
    Dim proibir, trocar, waux, wlen, letra, wpos

    If Not IsNull(Palavra) Then
        proibir = "��������������������������������������'`^~"
        trocar = "cCaAeEiIoOuUaAeEiIoOuUaAeEiIoOuUaAoOyY    "
        wlen = Len(Palavra)
        For waux = 1 To wlen
            letra = Mid(Palavra, waux, 1)
            wpos = InStr(proibir, letra)
            If wpos > 0 Then
                Palavra = Mid(Palavra, 1, waux - 1) & Mid(trocar, wpos, 1) & Mid(Palavra, waux + 1)
            End If
        Next
    End If
    Tira_Caracter = Palavra
End Function
Function Numerico(ByVal KeyAscii As Integer) As Integer
    If KeyAscii = 8 Then    'BACKSPACE
        Numerico = KeyAscii
        Exit Function
    End If
    If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
        KeyAscii = 0
        Beep
    End If
    Numerico = KeyAscii
End Function

