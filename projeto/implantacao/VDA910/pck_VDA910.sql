--- PACKAGE DO SISTEMA VDA910
--- DATA: 22/03/2005
--- ANALISTA: FABIO GUIDOTTI
--- dir: F:\SISTEMAS\ORACLE\VDA910\PADRAO\PCK_VDA910.SQL

CREATE OR REPLACE Package PRODUCAO.PCK_VDA910 is

	Type tp_Cursor is Ref Cursor;
									
									
	PROCEDURE PR_CON_CLIENTES( PM_CODCLI  IN NUMBER,
							   PM_NOMECLI IN VARCHAR2,
							   PM_CURSOR  IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2);
							   
							   
	PROCEDURE PR_CON_FONES( PM_CODCLI  IN NUMBER,
							PM_CURSOR  IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
							
							
	PROCEDURE PR_ACT_FONES( PM_CODCLI  IN NUMBER,
							PM_DDD     IN NUMBER,
							PM_FONE    IN NUMBER,
							PM_DDDOLD  IN NUMBER,
							PM_FONEOLD IN NUMBER,
							PM_ACT     IN VARCHAR2,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2);
				   
				  					  
End PCK_VDA910;
---------------
/

CREATE OR REPLACE Package Body PRODUCAO.PCK_VDA910 is
	
	
	PROCEDURE PR_CON_CLIENTES( PM_CODCLI  IN NUMBER,
							   PM_NOMECLI IN VARCHAR2,
							   PM_CURSOR  IN OUT tp_Cursor,
							   PM_CODERRO OUT NUMBER,
							   PM_TXTERRO OUT VARCHAR2) AS	 
	
	STRSQL VARCHAR2(1000);
	
	BEGIN
		
		PM_CODERRO := 0;
		
		STRSQL := 'SELECT * FROM PRODUCAO.CLIENTE
					WHERE SITUACAO = 0';
		
		IF PM_CODCLI <> 0 THEN
			STRSQL := STRSQL || ' AND COD_CLIENTE = ' || PM_CODCLI;
		END IF;
		
		IF PM_NOMECLI IS NOT NULL THEN
			STRSQL := STRSQL || ' AND NOME_CLIENTE LIKE ''' || PM_NOMECLI || '''';
		END IF;
		
		
		OPEN PM_CURSOR FOR
		STRSQL;

		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_CLIENTES;
	
	
	PROCEDURE PR_CON_FONES( PM_CODCLI  IN NUMBER,
							PM_CURSOR  IN OUT tp_Cursor,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 
	
	STRSQL VARCHAR2(1000);
	
	BEGIN
		
		PM_CODERRO := 0;
		
		OPEN PM_CURSOR FOR
		SELECT * FROM VENDAS.R_CLIENTE_FONE
		WHERE COD_CLIENTE = PM_CODCLI;

		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_CON_FONES;
	
	
	PROCEDURE PR_ACT_FONES( PM_CODCLI  IN NUMBER,
							PM_DDD     IN NUMBER,
							PM_FONE    IN NUMBER,
							PM_DDDOLD  IN NUMBER,
							PM_FONEOLD IN NUMBER,
							PM_ACT     IN VARCHAR2,
							PM_CODERRO OUT NUMBER,
							PM_TXTERRO OUT VARCHAR2) AS	 
	
	STRSQL VARCHAR2(1000);
	
	BEGIN
		
		PM_CODERRO := 0;
		
		IF PM_ACT = 'I' THEN
			INSERT INTO VENDAS.R_CLIENTE_FONE
			VALUES (PM_CODCLI, PM_DDD, PM_FONE);
		ELSIF PM_ACT = 'A' THEN
			UPDATE VENDAS.R_CLIENTE_FONE
			SET DDD = PM_DDD,FONE = PM_FONE
			WHERE COD_CLIENTE = PM_CODCLI AND                          
			DDD = PM_DDDOLD AND
			FONE = PM_FONEOLD;
		ELSIF PM_ACT = 'D' THEN
			DELETE FROM VENDAS.R_CLIENTE_FONE
			WHERE COD_CLIENTE = PM_CODCLI AND                          
			DDD = PM_DDDOLD AND
			FONE = PM_FONEOLD;
		END IF;
		
	EXCEPTION
		WHEN OTHERS THEN
		ROLLBACK;
		PM_CODERRO := SQLCODE;
		PM_TXTERRO := SQLERRM;
		
	END PR_ACT_FONES;
	
End PCK_VDA910;
---------------
/