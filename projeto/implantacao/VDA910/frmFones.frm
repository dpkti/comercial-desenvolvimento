VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmFones 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clientes x Telefones"
   ClientHeight    =   5025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7725
   Icon            =   "frmFones.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   335
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   515
   Begin VB.Frame fra 
      Appearance      =   0  'Flat
      Caption         =   "Dados para Cadastro"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   1455
      Left            =   45
      TabIndex        =   5
      Top             =   945
      Width           =   7620
      Begin VB.TextBox txtFone 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2745
         MaxLength       =   8
         TabIndex        =   12
         Top             =   855
         Width           =   1635
      End
      Begin VB.TextBox txtDDD 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         MaxLength       =   4
         TabIndex        =   10
         Top             =   855
         Width           =   690
      End
      Begin VB.TextBox txtCodCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1350
         TabIndex        =   7
         Top             =   405
         Width           =   690
      End
      Begin VB.TextBox txtNomeCliente 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   2115
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   405
         Width           =   5325
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         Caption         =   "Fone:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   2205
         TabIndex        =   11
         Top             =   945
         Width           =   465
      End
      Begin VB.Label lbl 
         Appearance      =   0  'Flat
         Caption         =   "Cliente:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   630
         TabIndex        =   9
         Top             =   450
         Width           =   690
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         Caption         =   "DDD:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   855
         TabIndex        =   8
         Top             =   945
         Width           =   465
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      Caption         =   "Telefones Cadastrados"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   2175
      Left            =   45
      TabIndex        =   3
      Top             =   2475
      Width           =   7620
      Begin MSGrid.Grid grdFones 
         Height          =   1815
         Left            =   90
         TabIndex        =   4
         Top             =   270
         Width           =   7440
         _Version        =   65536
         _ExtentX        =   13123
         _ExtentY        =   3201
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         HighLight       =   0   'False
      End
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   4695
      Width           =   7725
      _ExtentX        =   13626
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   13573
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   7665
      _ExtentX        =   13520
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFones.frx":23D2
      PICN            =   "frmFones.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd1 
      Height          =   690
      Left            =   855
      TabIndex        =   13
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFones.frx":30C8
      PICN            =   "frmFones.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd2 
      Height          =   690
      Left            =   2295
      TabIndex        =   14
      TabStop         =   0   'False
      ToolTipText     =   "Limpar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFones.frx":3DBE
      PICN            =   "frmFones.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmd3 
      Height          =   690
      Left            =   1575
      TabIndex        =   15
      TabStop         =   0   'False
      ToolTipText     =   "Excluir"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFones.frx":46B4
      PICN            =   "frmFones.frx":46D0
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
End
Attribute VB_Name = "frmFones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim VACT
Dim vDDDNew, vFONENew


Private Sub cmd1_Click()

    If txtCodCliente.Text = "" Then
        MsgBox "Selecione um cliente", , "Aten��o"
        txtCodCliente.SetFocus
        Exit Sub
    End If
    
    If txtDDD.Text = "" Or txtFone.Text = "" Then
        MsgBox "Digite o DDD e o numero do telefone", , "Aten��o"
        txtDDD.SetFocus
        Exit Sub
    End If

    vBanco.Parameters.Remove "PM_CODCLI"
    vBanco.Parameters.Add "PM_CODCLI", txtCodCliente.Text, 1
    
    vBanco.Parameters.Remove "PM_DDD"
    vBanco.Parameters.Add "PM_DDD", txtDDD.Text, 1

    vBanco.Parameters.Remove "PM_FONE"
    vBanco.Parameters.Add "PM_FONE", txtFone.Text, 1
    
    vBanco.Parameters.Remove "PM_DDDNEW"
    vBanco.Parameters.Add "PM_DDDNEW", vDDDNew, 1

    vBanco.Parameters.Remove "PM_FONENEW"
    vBanco.Parameters.Add "PM_FONENEW", vFONENew, 1
    
    vBanco.Parameters.Remove "PM_ACT"
    vBanco.Parameters.Add "PM_ACT", VACT, 1

    vBanco.Parameters.Remove "PM_CODERRO"
    vBanco.Parameters.Add "PM_CODERRO", 0, 2
    vBanco.Parameters.Remove "PM_TXTERRO"
    vBanco.Parameters.Add "PM_TXTERRO", "", 2

    vSql = "Producao.PCK_VDA910.PR_ACT_FONES(:PM_CODCLI,:PM_DDD,:PM_FONE,:PM_DDDNEW,:PM_FONENEW,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"

    vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
    
    If vBanco.Parameters("PM_CODERRO") = "0" Then
        If VACT = "A" Then
            MsgBox "Telefone alterado com sucesso", , ""
        Else
            MsgBox "Telefone incluido com sucesso", , ""
        End If
        cmd2_Click
        txtCodCliente.Text = vBanco.Parameters("PM_CODCLI")
    Else
        MsgBox vBanco.Parameters("PM_CODERRO") & " - " & vBanco.Parameters("PM_TXTERRO"), , "Aten��o"
    End If

End Sub

Public Sub cmd2_Click()

    txtCodCliente.Text = ""
    txtNomeCliente.Text = ""
    txtDDD.Text = ""
    txtFone = ""
    VACT = ""
    vDDDNew = ""
    vFONENew = ""
    
    cmd3.Enabled = False
    
    vVB_Generica_001.LimpaGridComTitulo grdFones
    
End Sub

Private Sub cmd3_Click()

    If vVB_Generica_001.Perguntar("Confirma a exclus�o deste telefone") = 6 Then

        vBanco.Parameters.Remove "PM_CODCLI"
        vBanco.Parameters.Add "PM_CODCLI", txtCodCliente.Text, 1
        
        vBanco.Parameters.Remove "PM_DDD"
        vBanco.Parameters.Add "PM_DDD", txtDDD.Text, 1

        vBanco.Parameters.Remove "PM_FONE"
        vBanco.Parameters.Add "PM_FONE", txtFone.Text, 1
        
        vBanco.Parameters.Remove "PM_DDDNEW"
        vBanco.Parameters.Add "PM_DDDNEW", vDDDNew, 1

        vBanco.Parameters.Remove "PM_FONENEW"
        vBanco.Parameters.Add "PM_FONENEW", vFONENew, 1
        
        vBanco.Parameters.Remove "PM_ACT"
        vBanco.Parameters.Add "PM_ACT", "D", 1

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vSql = "Producao.PCK_VDA910.PR_ACT_FONES(:PM_CODCLI,:PM_DDD,:PM_FONE,:PM_DDDNEW,:PM_FONENEW,:PM_ACT,:PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        
        If vBanco.Parameters("PM_CODERRO") = "0" Then
            MsgBox "Telefone Excluido com sucesso", , ""
            cmd2_Click
            txtCodCliente.Text = vBanco.Parameters("PM_CODCLI")
        Else
            MsgBox vBanco.Parameters("PM_CODERRO") & " - " & vBanco.Parameters("PM_TXTERRO"), , "Aten��o"
        End If
        
    End If

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Me.Top = 1500
    Me.Left = 300
    
End Sub


Private Sub grdFones_DblClick()
    
    grdFones.Col = 1
    
    If grdFones.Text <> "" Then
    
        txtDDD.Text = grdFones.Text
        vDDDNew = grdFones.Text
        
        grdFones.Col = 2
        txtFone.Text = grdFones.Text
        vFONENew = grdFones.Text
        
        cmd3.Enabled = True
        
        VACT = "A"
        
    End If
    
End Sub

Private Sub txtCodCliente_Change()

    If txtCodCliente.Text <> "" And txtCodCliente.Text <> "0" Then
    
        vBanco.Parameters.Remove "PM_CODCLI"
        vBanco.Parameters.Add "PM_CODCLI", txtCodCliente.Text, 1
        
        vBanco.Parameters.Remove "PM_NOMECLI"
        vBanco.Parameters.Add "PM_NOMECLI", Null, 1

        vBanco.Parameters.Remove "PM_CURSOR1"
        vBanco.Parameters.Add "PM_CURSOR1", 0, 3
        vBanco.Parameters("PM_CURSOR1").ServerType = 102
        vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
        vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

        vBanco.Parameters.Remove "PM_CODERRO"
        vBanco.Parameters.Add "PM_CODERRO", 0, 2
        vBanco.Parameters.Remove "PM_TXTERRO"
        vBanco.Parameters.Add "PM_TXTERRO", "", 2

        vSql = "Producao.PCK_VDA910.PR_CON_CLIENTES(:PM_CODCLI,:PM_NOMECLI,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

        vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)
        Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value
        
        If Not vObjOracle.EOF Then
        
            txtNomeCliente.Text = vObjOracle(3)

            vBanco.Parameters.Remove "PM_CODCLIENTE"
            vBanco.Parameters.Add "PM_CODCLIENTE", txtCodCliente.Text, 1

            vBanco.Parameters.Remove "PM_CURSOR1"
            vBanco.Parameters.Add "PM_CURSOR1", 0, 3
            vBanco.Parameters("PM_CURSOR1").ServerType = 102
            vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
            vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0

            vBanco.Parameters.Remove "PM_CODERRO"
            vBanco.Parameters.Add "PM_CODERRO", 0, 2
            vBanco.Parameters.Remove "PM_TXTERRO"
            vBanco.Parameters.Add "PM_TXTERRO", "", 2

            vSql = "Producao.PCK_VDA910.PR_CON_FONES(:PM_CODCLIENTE,:PM_CURSOR1,:PM_CODERRO,:PM_TXTERRO)"

            vErro = vVB_Generica_001.ExecutaPl(vBanco, vSql)

            Set vObjOracle = vBanco.Parameters("PM_CURSOR1").Value

            If Not vObjOracle.EOF Then
            
                vVB_Generica_001.CarregaGridTabela grdFones, vObjOracle, 3, "1;2"

            End If
            
            VACT = "I"
        Else
            txtNomeCliente.Text = ""
            txtDDD.Text = ""
            txtFone = ""
            VACT = ""
            vDDDNew = ""
            vFONENew = ""
            
            cmd3.Enabled = False
            
            vVB_Generica_001.LimpaGridComTitulo grdFones
        End If
    Else
        cmd2_Click
    End If

End Sub

Private Sub txtCodCliente_DblClick()
    frmClientes.Show 1
End Sub

Private Sub txtCodCliente_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtCodCliente_LostFocus()
    If txtCodCliente.Text <> "" And txtNomeCliente.Text = "" Then
        MsgBox "C�digo de Cliente Inv�lido!!", , "Aten��o"
        txtCodCliente.Text = ""
    End If
End Sub


Private Sub txtDDD_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub

Private Sub txtFONE_KeyPress(KeyAscii As Integer)
    KeyAscii = vVB_Generica_001.Numero(KeyAscii)
End Sub


