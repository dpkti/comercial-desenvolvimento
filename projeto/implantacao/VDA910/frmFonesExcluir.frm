VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmFonesExcluir 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listagem de Telefones com Solicitacao de Exclus�o"
   ClientHeight    =   7470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8685
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmFonesExcluir.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   498
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   579
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ListView lsvExclusao 
      Height          =   6255
      Left            =   60
      TabIndex        =   2
      Top             =   1140
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   11033
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   4
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Usu�rio"
         Object.Width           =   1323
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Cliente"
         Object.Width           =   1587
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Alignment       =   2
         SubItemIndex    =   2
         Text            =   "Fone"
         Object.Width           =   2646
      EndProperty
      BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   3
         Text            =   "Motivo"
         Object.Width           =   7937
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   8595
      _ExtentX        =   15161
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFonesExcluir.frx":23D2
      PICN            =   "frmFonesExcluir.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   7950
      TabIndex        =   3
      TabStop         =   0   'False
      ToolTipText     =   "Aceitar Exclus�es"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFonesExcluir.frx":30C8
      PICN            =   "frmFonesExcluir.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDescartarExclusao 
      Height          =   690
      Left            =   7200
      TabIndex        =   9
      TabStop         =   0   'False
      ToolTipText     =   "Descartar Exclus�es"
      Top             =   60
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmFonesExcluir.frx":3DBE
      PICN            =   "frmFonesExcluir.frx":3DDA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblCliente 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   3810
      TabIndex        =   8
      Top             =   330
      Width           =   3345
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "Cliente:"
      Height          =   195
      Left            =   3810
      TabIndex        =   7
      Top             =   90
      Width           =   525
   End
   Begin VB.Label lblSolicitante 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   225
      Left            =   1140
      TabIndex        =   6
      Top             =   330
      Width           =   2565
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Solicitante:"
      Height          =   195
      Left            =   1140
      TabIndex        =   5
      Top             =   90
      Width           =   780
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Telefones para Exclus�o:"
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   900
      Width           =   1800
   End
End
Attribute VB_Name = "frmFonesExcluir"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDescartarExclusao_Click()
    Dim vDep As String
    Dim vParametro
    Dim ii As Integer
    
    If vTipoCD = "U" Then
        vDep = "HELPDESK."
    Else
        vDep = "DEP" & Format(vCd, "00") & "."
    End If
    
    'Excluir da tabela Vendas.R_CLIENTE_FONE os telefones que estiverem selecionados
    
    If MsgBox("Os telefones selecionados N�O ser�o exclu�dos, confirma ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
        For i = 1 To lsvExclusao.ListItems.Count
            
            If lsvExclusao.ListItems(i).Checked = True Then
                
                vBanco.Parameters.Remove "VL_PARAMETRO"
                vBanco.Parameters.Add "VL_PARAMETRO", lsvExclusao.ListItems(i).SubItems(1) & ";" & lsvExclusao.ListItems(i).SubItems(2) & ";" & lsvExclusao.ListItems(i).SubItems(3) & ";" & lsvExclusao.ListItems(i), 1
                
                vBanco.Parameters.Remove "COD_SOFT"
                vBanco.Parameters.Add "COD_SOFT", 1069, 1
                
                vBanco.Parameters.Remove "NOME_PARAMETRO"
                vBanco.Parameters.Add "NOME_PARAMETRO", "EXCLUIR_TELEFONE", 1
                
                vBanco.Parameters.Remove "DEP"
                vBanco.Parameters.Add "DEP", vDep, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA910.PR_EXCLUIR_PARAMETROS(:COD_SOFT, :NOME_PARAMETRO, :VL_PARAMETRO, :DEP)")
                
                If vErro <> "" Then
                    MsgBox "Ocorreu o seguinte erro:" & vErro, vbInformation, "Aten��o"
                End If
                
            End If
            
        Next
    
    End If
    
    Preenche_Lsv
End Sub

Private Sub cmdSalvar_Click()
    
    Dim vDep As String
    Dim vParametro
    Dim ii As Integer
    
    If vTipoCD = "U" Then
        vDep = "HELPDESK."
    Else
        vDep = "DEP" & Format(vCd, "00") & "."
    End If
    
    'Excluir da tabela Vendas.R_CLIENTE_FONE os telefones que estiverem selecionados
    
    If MsgBox("Confirma a Exclus�o dos registros selecionados ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
    
        For i = 1 To lsvExclusao.ListItems.Count
            
            If lsvExclusao.ListItems(i).Checked = True Then
                
                vBanco.Parameters.Remove "COD_CLIENTE"
                vBanco.Parameters.Add "Cod_Cliente", lsvExclusao.ListItems(i).SubItems(1), 1
                
                vBanco.Parameters.Remove "DDD"
                vBanco.Parameters.Add "DDD", 0, 1
                
                vBanco.Parameters.Remove "FONE"
                vBanco.Parameters.Add "FONE", 0, 1
                
                vBanco.Parameters.Remove "DDDOLD"
                vBanco.Parameters.Add "DDDOLD", Left(lsvExclusao.ListItems(i).SubItems(2), 2), 1
                
                vBanco.Parameters.Remove "FONEOLD"
                vBanco.Parameters.Add "FONEOLD", Mid(lsvExclusao.ListItems(i).SubItems(2), 3), 1
                
                vBanco.Parameters.Remove "ACT"
                vBanco.Parameters.Add "ACT", "D", 1
                
                vBanco.Parameters.Remove "COD_ERRO"
                vBanco.Parameters.Add "COD_ERRO", 0, 2
                
                vBanco.Parameters.Remove "TXT_ERRO"
                vBanco.Parameters.Add "TXT_ERRO", "", 2
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA910.PR_ACT_FONES(:COD_CLIENTE, :DDD, :FONE, :DDDOLD, :FONEOLD, :ACT, :COD_ERRO, :TXT_ERRO)")
                        
                If vErro <> "" Then
                    MsgBox "Ocorreu o seguinte erro:" & vErro, vbInformation, "Aten��o"
                End If
                
                vBanco.Parameters.Remove "VL_PARAMETRO"
                vBanco.Parameters.Add "VL_PARAMETRO", lsvExclusao.ListItems(i).SubItems(1) & ";" & lsvExclusao.ListItems(i).SubItems(2) & ";" & lsvExclusao.ListItems(i).SubItems(3) & ";" & lsvExclusao.ListItems(i), 1
                
                vBanco.Parameters.Remove "COD_SOFT"
                vBanco.Parameters.Add "COD_SOFT", 1069, 1
                
                vBanco.Parameters.Remove "NOME_PARAMETRO"
                vBanco.Parameters.Add "NOME_PARAMETRO", "EXCLUIR_TELEFONE", 1
                
                vBanco.Parameters.Remove "DEP"
                vBanco.Parameters.Add "DEP", vDep, 1
                
                vErro = vVB_Generica_001.ExecutaPl(vBanco, "PRODUCAO.PCK_VDA910.PR_EXCLUIR_PARAMETROS(:COD_SOFT, :NOME_PARAMETRO, :VL_PARAMETRO, :DEP)")
                
                If vErro <> "" Then
                    MsgBox "Ocorreu o seguinte erro:" & vErro, vbInformation, "Aten��o"
                End If
                
            End If
            
        Next
    
    End If
    
    Preenche_Lsv

End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    
    Preenche_Lsv
    
End Sub

Sub Preenche_Lsv()
    Dim litem As ListItem
    Dim rst As Object
    Dim vDep As String
    Dim vParametro
    Dim ii As Integer
    
    If vTipoCD = "U" Then
        vDep = "PRODUCAO."
    Else
        vDep = "DEP" & Format(vCd, "00") & "."
    End If
    
    Me.lsvExclusao.ListItems.Clear
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
    vBanco.Parameters.Remove "Cod_Sistema"
    vBanco.Parameters.Add "Cod_Sistema", 1069, 1
    
    vBanco.Parameters.Remove "Nome_Parametro"
    vBanco.Parameters.Add "Nome_Parametro", "EXCLUIR_TELEFONE", 1
    
    vBanco.Parameters.Remove "DEP"
    vBanco.Parameters.Add "DEP", vDep, 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda910.PR_SELECT_VL_PARAMETRO(:PM_CURSOR1,:Cod_Sistema, :Nome_parametro, :Dep)")
    Set rst = vBanco.Parameters("PM_CURSOR1").Value

    If rst.RecordCount <= 0 Then Exit Sub
    
    For i = 1 To rst.RecordCount
        vParametro = Split(rst!vl_parametro, ";")
        Set litem = lsvExclusao.ListItems.Add
        litem = vParametro(3)
        litem.SubItems(1) = vParametro(0)
        litem.SubItems(2) = vParametro(1)
        litem.SubItems(3) = vParametro(2)
        
        rst.MoveNext
    Next

End Sub

Private Sub lsvExclusao_Click()
    Dim rst As Object
    
    If lsvExclusao.SelectedItem Is Nothing Then Exit Sub
    
    vBanco.Parameters.Remove "PM_CURSOR1"
    vBanco.Parameters.Add "PM_CURSOR1", 0, 3
    vBanco.Parameters("PM_CURSOR1").ServerType = 102
    vBanco.Parameters("PM_CURSOR1").DynasetOption = &H2&
    vBanco.Parameters("PM_CURSOR1").DynasetCacheParams 256, 16, 20, 2000, 0
        
    vBanco.Parameters.Remove "COD_CLIENTE"
    vBanco.Parameters.Add "COD_CLIENTE", lsvExclusao.SelectedItem.SubItems(1), 1
    
    vBanco.Parameters.Remove "COD_REPRES"
    vBanco.Parameters.Add "COD_REPRES", lsvExclusao.SelectedItem, 1
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco, "Producao.pck_vda910.PR_SELECT_REPRES_CLIENTE(:PM_CURSOR1,:COD_CLIENTE,:COD_REPRES)")
    Set rst = vBanco.Parameters("PM_CURSOR1").Value

    If rst.RecordCount <= 0 Then Exit Sub
    
    Me.lblCliente = rst!NOME_CLIENTE
    rst.MoveNext
    Me.lblSolicitante = rst!NOME_CLIENTE
    
    rst.Close
    Set rst = Nothing
    
End Sub
