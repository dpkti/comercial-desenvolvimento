Attribute VB_Name = "Module1"
Public dbaccess As Database
Public SQL As String
Public path_drv As String
Public SS As Snapshot

Sub IniciaGrid2()
    
    On Error GoTo TrataErro
    
1    With frmPrincipal.grdConsPeriodo
        
2        .Cols = 10
3        .Row = 0
4        .RowHeight(0) = 400
        
5        .Col = 0: .Text = "DATA": .ColWidth(0) = 900: .FixedAlignment(0) = 2
        
6        .Col = 1: .Text = "C�D. REPRES": .ColWidth(1) = 900:  .FixedAlignment(1) = 2
7        .ColAlignment(1) = 2
        
8        .Col = 2: .Text = "DEP�SITO": .ColWidth(2) = 900:  .FixedAlignment(2) = 2
9        .ColAlignment(2) = 2
        
10        .Col = 3: .Text = "N� DO PEDIDO": .ColWidth(3) = 900:  .FixedAlignment(3) = 2
11        .ColAlignment(3) = 2
        
12        .Col = 4: .Text = "C�D. DO ITEM": .ColWidth(4) = 1000: .FixedAlignment(4) = 2
13       .ColAlignment(4) = 2
        
14        .Col = 5: .Text = "QTDE. SOLICITADA": .ColWidth(5) = 1000: .FixedAlignment(5) = 2
15        .ColAlignment(5) = 2
        
16        .Col = 6: .Text = "QTDE. ATENDIDA": .ColWidth(6) = 1000: .FixedAlignment(6) = 2
17        .ColAlignment(6) = 2
        
18       .Col = 7: .Text = "ESTOQUE DISPON�VEL": .ColWidth(7) = 1000: .FixedAlignment(7) = 2
19        .ColAlignment(7) = 2
        
20        .Col = 8: .Text = "C�D. DO CLIENTE": .ColWidth(8) = 1000: .FixedAlignment(8) = 2
21        .ColAlignment(8) = 2
        
22        .Col = 9: .Text = "CLIENTE": .ColWidth(9) = 3000: .FixedAlignment(9) = 0
23        .ColAlignment(9) = 0
24        .FixedRows = 1
        
    End With

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub IniciaGrid2" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Sub DATA(ByRef KeyAscii, ByRef txtCampo)
    
    Dim bTam As Byte    'tamanho do campo JA digitado
    Dim strData As String
    Dim bKey As Byte
    
    On Error GoTo TrataErro
    
    bTam = Len(txtCampo.Text)
    
    If KeyAscii = 8 And bTam > 0 Then 'backspace
        If Mid$(txtCampo.Text, bTam) = "/" Then
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
        Else
            txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
        End If
            
    ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
        If bTam = 1 Then
            strData = txtCampo.Text & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
            
        ElseIf bTam = 4 Then
            strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
            If CInt(strData) < 1 Or CInt(strData > 12) Then
                Beep
            Else
                txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
            End If
        
        ElseIf bTam = 7 Then
            strData = Mid$(txtCampo.Text, 1, 2)     'dia
            If CInt(strData) < 1 Or CInt(strData > 31) Then
                Beep
            Else
                strData = Mid$(txtCampo.Text, 4, 2)     'mes
                If CInt(strData) < 1 Or CInt(strData > 12) Then
                    Beep
                Else
                    strData = txtCampo.Text & Chr$(KeyAscii)
                    If Not IsDate(CDate(strData)) Then
                        Beep
                    Else
                        txtCampo.Text = strData
                    End If
                End If
            End If
            
        ElseIf bTam < 8 Then
            bKey = KeyAscii
            
        Else
            bKey = 0
        End If
    Else
        Beep
    End If
    
    SendKeys "{END}"
    KeyAscii = bKey
    Exit Sub
    
TrataErro:

    If Err.Number = 13 Then
        MsgBox strData, vbInformation, "Data Inv�lida"
        KeyAscii = 0
        Beep
        Err.Clear
    End If

End Sub




Sub IniciaGrid()
    
    On Error GoTo TrataErro
      
1    With frmPrincipal.grdConsRepres
        
2        .Cols = 9
3        .Row = 0
4        .RowHeight(0) = 400
        
5        .Col = 0: .Text = "DATA": .ColWidth(0) = 900: .FixedAlignment(0) = 2
        
6        .Col = 1: .Text = "DEP�SITO": .ColWidth(1) = 900:  .FixedAlignment(1) = 2
7        .ColAlignment(1) = 2
        
8        .Col = 2: .Text = "N� DO PEDIDO": .ColWidth(2) = 900:  .FixedAlignment(2) = 2
9        .ColAlignment(2) = 2
        
10        .Col = 3: .Text = "C�D. DO ITEM": .ColWidth(3) = 1000: .FixedAlignment(3) = 2
11        .ColAlignment(3) = 2
        
12        .Col = 4: .Text = "QTDE. SOLICITADA": .ColWidth(4) = 1000: .FixedAlignment(4) = 2
13        .ColAlignment(4) = 2
        
14        .Col = 5: .Text = "QTDE. ATENDIDA": .ColWidth(5) = 1000: .FixedAlignment(5) = 2
15        .ColAlignment(5) = 2
        
16        .Col = 6: .Text = "ESTOQUE DISPON�VEL": .ColWidth(6) = 1000: .FixedAlignment(6) = 2
17        .ColAlignment(6) = 2
        
18        .Col = 7: .Text = "C�D. DO CLIENTE": .ColWidth(7) = 1000: .FixedAlignment(7) = 2
19        .ColAlignment(7) = 2
        
20        .Col = 8: .Text = "CLIENTE": .ColWidth(8) = 3000: .FixedAlignment(8) = 0
21        .ColAlignment(8) = 0
22        .FixedRows = 1
        
    End With

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub sscmdConsulta2_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Sub LimpaGrid2()
    With frmPrincipal.grdConsPeriodo
        .Rows = 2
        .Row = 1
        For I = 0 To .Cols - 1
            .Col = I: .Text = ""
        Next I
    End With
    
    Call IniciaGrid2
    
End Sub

Sub LimpaGrid()
    With frmPrincipal.grdConsRepres
        .Rows = 2
        .Row = 1
        For I = 0 To .Cols - 1
            .Col = I: .Text = ""
        Next I
    End With
    
    Call IniciaGrid
    
End Sub

