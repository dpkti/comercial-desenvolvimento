VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrincipal 
   Caption         =   "CONSULTA VENDA PERDIDA"
   ClientHeight    =   6885
   ClientLeft      =   2115
   ClientTop       =   3285
   ClientWidth     =   9330
   Icon            =   "Periodo.frx":0000
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6885
   ScaleWidth      =   9330
   WindowState     =   2  'Maximized
   Begin TabDlg.SSTab SSTab1 
      Height          =   6465
      Left            =   90
      TabIndex        =   0
      Top             =   75
      Width           =   9420
      _ExtentX        =   16616
      _ExtentY        =   11404
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      BackColor       =   -2147483638
      ForeColor       =   128
      TabCaption(0)   =   "Consulta por REPRESENTANTE"
      TabPicture(0)   =   "Periodo.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblRepres"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "lblPeriodo"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label3"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "grdConsRepres"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "sscmdSair"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "sscmdLimpa"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "sscmdConsulta"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "sscmdImprimir"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "txtInicio"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "cmbRepres"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "txtFim"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).ControlCount=   11
      TabCaption(1)   =   "Consulta por PER�ODO"
      TabPicture(1)   =   "Periodo.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label4"
      Tab(1).Control(1)=   "txtPeriodo"
      Tab(1).Control(2)=   "grdConsPeriodo"
      Tab(1).Control(3)=   "sscmdSair2"
      Tab(1).Control(4)=   "sscmdLimpa2"
      Tab(1).Control(5)=   "sscmdConsulta2"
      Tab(1).Control(6)=   "sscmdImprimir2"
      Tab(1).Control(7)=   "txtFim2"
      Tab(1).Control(8)=   "txtInicio2"
      Tab(1).ControlCount=   9
      Begin VB.TextBox txtInicio2 
         Height          =   285
         Left            =   -74088
         TabIndex        =   13
         Top             =   492
         Width           =   1185
      End
      Begin VB.TextBox txtFim2 
         Height          =   285
         Left            =   -72588
         TabIndex        =   15
         Top             =   492
         Width           =   1185
      End
      Begin VB.TextBox txtFim 
         Height          =   285
         Left            =   8064
         TabIndex        =   6
         Top             =   540
         Width           =   1185
      End
      Begin VB.ComboBox cmbRepres 
         Height          =   315
         Left            =   1380
         TabIndex        =   2
         Top             =   540
         Width           =   2988
      End
      Begin VB.TextBox txtInicio 
         Height          =   285
         Left            =   6564
         TabIndex        =   4
         Top             =   540
         Width           =   1185
      End
      Begin Threed.SSCommand sscmdImprimir 
         Height          =   552
         Left            =   4872
         TabIndex        =   10
         Top             =   5772
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":0342
      End
      Begin Threed.SSCommand sscmdImprimir2 
         Height          =   552
         Left            =   -70104
         TabIndex        =   19
         Top             =   5808
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":065C
      End
      Begin Threed.SSCommand sscmdConsulta2 
         Height          =   552
         Left            =   -71616
         TabIndex        =   17
         Top             =   5808
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":0976
      End
      Begin Threed.SSCommand sscmdLimpa2 
         Height          =   552
         Left            =   -70872
         TabIndex        =   18
         Top             =   5808
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":0C90
      End
      Begin Threed.SSCommand sscmdSair2 
         Height          =   552
         Left            =   -69372
         TabIndex        =   20
         Top             =   5808
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":10E2
      End
      Begin Threed.SSCommand sscmdConsulta 
         Height          =   552
         Left            =   3360
         TabIndex        =   8
         Top             =   5760
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":13FC
      End
      Begin Threed.SSCommand sscmdLimpa 
         Height          =   552
         Left            =   4116
         TabIndex        =   9
         Top             =   5772
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":1716
      End
      Begin Threed.SSCommand sscmdSair 
         Height          =   552
         Left            =   5616
         TabIndex        =   11
         Top             =   5772
         Width           =   552
         _Version        =   65536
         _ExtentX        =   979
         _ExtentY        =   979
         _StockProps     =   78
         Picture         =   "Periodo.frx":1B68
      End
      Begin MSGrid.Grid grdConsPeriodo 
         Height          =   4812
         Left            =   -74832
         TabIndex        =   16
         Top             =   888
         Width           =   9060
         _Version        =   65536
         _ExtentX        =   15981
         _ExtentY        =   8488
         _StockProps     =   77
         BackColor       =   16777215
         Cols            =   9
      End
      Begin MSGrid.Grid grdConsRepres 
         Height          =   4716
         Left            =   168
         TabIndex        =   7
         Top             =   936
         Width           =   9084
         _Version        =   65536
         _ExtentX        =   16023
         _ExtentY        =   8319
         _StockProps     =   77
         BackColor       =   16777215
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.23
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Cols            =   9
      End
      Begin VB.Label txtPeriodo 
         Caption         =   "Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   228
         Left            =   -74808
         TabIndex        =   12
         Top             =   576
         Width           =   672
      End
      Begin VB.Label Label4 
         Caption         =   "�"
         ForeColor       =   &H00800000&
         Height          =   228
         Left            =   -72792
         TabIndex        =   14
         Top             =   576
         Width           =   156
      End
      Begin VB.Label Label3 
         Caption         =   "�"
         ForeColor       =   &H00800000&
         Height          =   228
         Left            =   7848
         TabIndex        =   5
         Top             =   636
         Width           =   180
      End
      Begin VB.Label lblPeriodo 
         Caption         =   "Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   228
         Left            =   5844
         TabIndex        =   3
         Top             =   636
         Width           =   672
      End
      Begin VB.Label lblRepres 
         Caption         =   "Representante:"
         ForeColor       =   &H00800000&
         Height          =   228
         Left            =   180
         TabIndex        =   1
         Top             =   660
         Width           =   1188
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   21
      Top             =   6555
      Width           =   9330
      _ExtentX        =   16457
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            Alignment       =   1
            TextSave        =   "20/09/07"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   13388
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub pImprimirCabecalho()
        
    On Error GoTo TrataErro
    
1    Printer.Print
2    lstrLinha = "                                   V E N D A    P E R D I D A                                   "
3    Printer.Print lstrLinha
4    Printer.Print
    
5    Printer.Print String(96, "-")
6    Printer.Print
    
7    lstrLinha = "C�D.REPRES: " & Left(cmbRepres.Text, 4) & "            De: "
8    lstrLinha = lstrLinha & Format(txtInicio.Text, "dd/mm/yy") & ""
9    lstrLinha = lstrLinha & " a " & Format(txtFim.Text, "dd/mm/yy") & ""
    
10    Printer.Print lstrLinha
    
11    Printer.Print
12    Printer.Print String(96, "-")
    
13    lstrLinha = ""
14    lstrLinha = "DT.PEDIDO|LOJA| NUM.PEDIDO |C�D.DPK|   QTD.   |  QTD.  |ESTOQUE|CLIENTE"
15    Printer.Print lstrLinha
16    lstrLinha = "         |    |            |       |SOLICITADA|ATENDIDA| ATUAL |       "
17    Printer.Print lstrLinha
18    Printer.Print String(96, "-")
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub pImprimirCabecalho" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
End Sub

Private Sub pImprimirCabecalho2()
        
    On Error GoTo TrataErro
    
1    Printer.Print
2    lstrLinha2 = "                                   V E N D A    P E R D I D A                                   "
3    Printer.Print lstrLinha2
4    Printer.Print
    
5    Printer.Print String(96, "-")
6    Printer.Print

7    lstrLinha2 = "PER�ODO: " & Format(txtInicio2.Text, "dd/mm/yy") & ""
8    lstrLinha2 = lstrLinha2 & " � " & Format(txtFim2.Text, "dd/mm/yy") & ""
    
9    Printer.Print lstrLinha2
    
10    Printer.Print
11    Printer.Print String(96, "-")
    
12    lstrLinha2 = ""
13    lstrLinha2 = "DT.PEDIDO| C�D. |LOJA| NUM.PEDIDO |C�D.DPK|   QTD.   |  QTD.  |ESTOQUE|CLIENTE"
14    Printer.Print lstrLinha2
15    lstrLinha2 = "         |REPRES|    |            |       |SOLICITADA|ATENDIDA| ATUAL |       "
16    Printer.Print lstrLinha2
17    Printer.Print String(96, "-")
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub pImprimirCabecalho2" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
    
    
End Sub

Private Sub Form_Load()
    
    Me.Caption = Me.Caption & " Vers�o:  " & App.Major & "." & App.Minor & "." & App.Revision
    
   '*************************
   'Define drive de execu��o
    path_drv = Left(App.Path, 2)
     
   '**************************
   'busca informacoes sobre os representantes que pertencem ao paac
    Set dbaccess = OpenDatabase(path_drv & "\DADOS\BASE_DPK.MDB")

    SQL = "Select a.cod_repres, a.nome_repres " & _
          "from representante a, deposito b " & _
          "where a.cod_filial=b.cod_filial " & _
          "order by nome_repres"
          
    Set SS = dbaccess.CreateSnapshot(SQL)
    FreeLocks
    
    Do While Not SS.EOF
        cmbRepres.AddItem Format(SS!cod_repres, "0000") & " - " & SS!nome_repres
        SS.MoveNext
    Loop
    
    SS.Close
    dbaccess.Close
   '*************************

End Sub


Private Sub grdConsPeriodo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = ""
End Sub

Private Sub grdConsRepres_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = ""
End Sub

Private Sub sscmdConsulta_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Consulta dados pelo per�odo e representante colocados acima"
End Sub

Private Sub sscmdConsulta2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Consulta dados pelo per�odo colocado acima"
End Sub

Private Sub sscmdImprimir_Click()
    
    On Error GoTo TrataErro
    
    Dim liLinhaGrid, liColunaGrid As Integer
    
1    lilinhaimpressao = 0
    
2    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
3        With grdConsRepres
        
4            Printer.Print ""
5            Printer.FontName = "Courier New"
6            Printer.FontSize = 10
            
            'GRID VAZIO
7            If .Rows = 2 Then
8                .Col = 0
9                If .Text = "" Then
                    Exit Sub
                End If
            End If
            
10            .Row = 0
11            liPagina = 0
            
12            Call pImprimirCabecalho
            
            Do
13                .Row = .Row + 1
               'IMPRIMIR LINHA DE DETALHE
14                .Col = 0:  lstrLinha = Format(.Text, "dd/mm/yy")
15                .Col = 1:  lstrLinha = lstrLinha & " | " & Format(.Text, "00") & ""
16                .Col = 2:  lstrLinha = lstrLinha & " | " & Space(10 - Len(.Text)) & .Text & ""
17                .Col = 3:  lstrLinha = lstrLinha & " | " & Space(5 - Len(.Text)) & .Text & ""
18                .Col = 4:  lstrLinha = lstrLinha & " |   " & Space(4 - Len(.Text)) & .Text & ""
19                .Col = 5:  lstrLinha = lstrLinha & "   |  " & Space(4 - Len(.Text)) & .Text & ""
20                .Col = 6:  lstrLinha = lstrLinha & "  | " & Space(4 - Len(.Text)) & .Text & ""
21                .Col = 7:  lstrLinha = lstrLinha & "  |" & Space(6 - Len(.Text)) & .Text & ""
22                .Col = 8:  lstrLinha = lstrLinha & "-" & .Text
                
23                Printer.Print lstrLinha
                
24                lilinhaimpressao = lilinhaimpressao + 1
                
                'VERIFICA QUEBRA DE PAGINA
25                If lilinhaimpressao > 59 And .Row <> .Rows - 1 Then
26                    Printer.Print String(96, "-")
27                    Printer.EndDoc
28                    Call pImprimirCabecalho
29                    lilinhaimpressao = 0
30                    liPagina = liPagina + 1
                End If
            
31            Loop Until .Row = .Rows - 1
            
32            Printer.Print String(96, "-")
33            Printer.EndDoc

            MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
        End With
    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub sscmdImprimir_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If
    
End Sub

Private Sub sscmdImprimir_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Imprime a consulta acima"
End Sub


Private Sub sscmdImprimir2_Click()
    
    On Error GoTo TrataErro
    
    Dim liLinhaGrid2, liColunaGrid2 As Integer
    
1    lilinhaimpressao2 = 0
    
2    If MsgBox("Confirma Impress�o ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
        
3        With grdConsPeriodo
        
4            Printer.Print ""
5            Printer.FontName = "Courier New"
6            Printer.FontSize = 10
            
           'GRID VAZIO
7            If .Rows = 2 Then
8                .Col = 0
9                If .Text = "" Then
                    Exit Sub
                End If
            End If
            
10            .Row = 0
11            liPagina2 = 0
            
12            Call pImprimirCabecalho2
            
            Do
13                .Row = .Row + 1
               'IMPRIMIR LINHA DE DETALHE
14                .Col = 0:  lstrLinha2 = Format(.Text, "dd/mm/yy")
15                .Col = 1:  lstrLinha2 = lstrLinha2 & " | " & Format(.Text, "0000") & ""
16                .Col = 2:  lstrLinha2 = lstrLinha2 & " | " & Format(.Text, "00") & ""
17                .Col = 3:  lstrLinha2 = lstrLinha2 & " | " & Space(10 - Len(.Text)) & .Text & ""
18                .Col = 4:  lstrLinha2 = lstrLinha2 & " | " & Space(5 - Len(.Text)) & .Text & ""
19                .Col = 5:  lstrLinha2 = lstrLinha2 & " |   " & Space(4 - Len(.Text)) & .Text & ""
20                .Col = 6:  lstrLinha2 = lstrLinha2 & "   |  " & Space(4 - Len(.Text)) & .Text & ""
21                .Col = 7:  lstrLinha2 = lstrLinha2 & "  | " & Space(4 - Len(.Text)) & .Text & ""
22                .Col = 8:  lstrLinha2 = lstrLinha2 & "  |" & Space(5 - Len(.Text)) & .Text & ""
23                .Col = 9:  lstrLinha2 = lstrLinha2 & "-" & .Text
                
24                Printer.Print lstrLinha2
                
25                lilinhaimpressao2 = lilinhaimpressao2 + 1
                
                'VERIFICA QUEBRA DE PAGINA
26                If lilinhaimpressao2 > 59 And .Row <> .Rows - 1 Then
27                    Printer.Print String(96, "-")
28                    Printer.EndDoc
29                    Call pImprimirCabecalho
30                    lilinhaimpressao2 = 0
31                    liPagina2 = liPagina2 + 1
                End If
            
32            Loop Until .Row = .Rows - 1
            
33            Printer.Print String(96, "-")
34            Printer.EndDoc

            MsgBox "Fim de Impress�o", vbInformation, "Aten��o"
        End With
    End If

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub sscmdImprimir2_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


Private Sub sscmdImprimir2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Imprime a consulta acima"
End Sub


Private Sub sscmdLimpa_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Limpa a tela para nova consulta"
End Sub

Private Sub sscmdLimpa2_Click()
    txtInicio2.Text = ""
    txtFim2.Text = ""
    Call LimpaGrid2
End Sub


Private Sub sscmdConsulta2_Click()
    Dim liLinha      As Integer
    Dim DataInicio2  As String
    Dim DataFim2     As String

    On Error GoTo TrataErro

1    If txtInicio2.Text = "" Or txtFim2.Text = "" Then
2        MsgBox "Escolha o per�odo para efetuar a consulta!"
3        txtInicio2.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = 11

4    Call IniciaGrid2
    
   '**************************
   'busca informacoes sobre os representantes que pertencem ao paac
5    Set dbaccess2 = OpenDatabase(path_drv & "\DADOS\BASE_DPK.MDB")
   
   'ESTE � O FORMATO DE DATA QUE O ACCESS ACEITA  #m/d/yy#
6    DataInicio2 = Format(txtInicio2, "m/d/yy")
7    DataFim2 = Format(txtFim2, "m/d/yy")
    
8    SQL = "Select a.dt_pedido, b.cod_loja,b.num_pedido, b.seq_pedido, b.cod_dpk, " & _
          "b.qtd_solicitada, b.qtd_atendida, c.qtd_atual, a.cod_cliente, d.nome_cliente, " & _
          "a.situacao, a.fl_ger_nfis, b.seq_pedido, a.cod_repres, a.cod_vend " & _
          "from pednota_venda a, itpednota_venda b, item_estoque c, cliente d " & _
          "where a.situacao=0 and a.fl_ger_nfis='S' and a.cod_cliente=d.cod_cliente and " & _
          "a.num_pedido=b.num_pedido and a.seq_pedido=b.seq_pedido and b.cod_dpk=c.cod_dpk and " & _
          "(dt_pedido >=#" & DataInicio2 & "# and dt_pedido <=#" & DataFim2 & "#) and " & _
          "a.cod_loja=b.cod_loja and b.cod_loja=c.cod_loja and b.qtd_solicitada > b.qtd_atendida " & _
          "order by b.num_pedido, b.cod_dpk"

9    Set ss2 = dbaccess2.CreateDynaset(SQL)

   'Enquanto houver registros para esta consulta, executa o loop
    
10    With grdConsPeriodo
11        Do While Not ss2.EOF
        
12            liLinha = liLinha + 1
            
13            .Rows = liLinha + 1
14            .Row = liLinha
                
15            If liLinha < 20 Then
16                .Refresh
            End If
            
           'Posiciona dados no grid
17            .Col = 0:  .Text = ss2.Fields("dt_pedido")
18            If ss2.Fields("cod_repres") <> "" Then
19                .Col = 1:  .Text = Format(ss2.Fields("cod_repres"), "0000")
            Else
20                .Col = 1:  .Text = Format(ss2.Fields("cod_vend"), "0000")
            End If
21            .Col = 2:  .Text = Format(ss2.Fields("cod_loja"), "00")
22            .Col = 3:  .Text = ss2.Fields("num_pedido") & "-" & ss2.Fields("seq_pedido")
23            .Col = 4:  .Text = ss2.Fields("cod_dpk")
24            .Col = 5:  .Text = ss2.Fields("qtd_solicitada")
25            .Col = 6:  .Text = ss2.Fields("qtd_atendida")
26            .Col = 7:  .Text = ss2.Fields("qtd_atual")
27            .Col = 8:  .Text = ss2.Fields("cod_cliente")
28            .Col = 9:  .Text = ss2.Fields("nome_cliente")
     
29            ss2.MoveNext
        Loop
    
    End With
    
30    ss2.Close
31    dbaccess2.Close
32    Screen.MousePointer = 0

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub sscmdConsulta2_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub sscmdConsulta_Click()
    Dim liLinha     As Integer
    Dim DataInicio  As String
    Dim DataFim     As String

1    If cmbRepres = "" Then
        MsgBox "Escolha o representante!"
2        cmbRepres.SetFocus
        Exit Sub
    End If
    
3    If txtInicio.Text = "" Or txtFim.Text = "" Then
4        MsgBox "Escolha o per�odo para efetuar a consulta!"
5        txtInicio.SetFocus
        Exit Sub
    End If
    
    Screen.MousePointer = 11

6    Call IniciaGrid
    
   '**************************
   'busca informacoes sobre os representantes que pertencem ao paac
7    Set dbaccess = OpenDatabase(path_drv & "\DADOS\BASE_DPK.MDB")
   
   'ESTE � O FORMATO DE DATA QUE O ACCESS ACEITA  #m/d/yy#
8    DataInicio = Format(txtInicio, "m/d/yy")
9    DataFim = Format(txtFim, "m/d/yy")
    
10    SQL = "Select a.dt_pedido, b.cod_loja,b.num_pedido, b.seq_pedido, b.cod_dpk, " & _
          "b.qtd_solicitada, b.qtd_atendida, c.qtd_atual, a.cod_cliente, d.nome_cliente, " & _
          "a.situacao, a.fl_ger_nfis, b.seq_pedido " & _
          "from pednota_venda a, itpednota_venda b, item_estoque c, cliente d " & _
          "where a.situacao=0 and a.fl_ger_nfis='S' and a.cod_cliente=d.cod_cliente and " & _
          "a.num_pedido=b.num_pedido and a.seq_pedido=b.seq_pedido and b.cod_dpk=c.cod_dpk and " & _
          "(dt_pedido >=#" & DataInicio & "# and dt_pedido <=#" & DataFim & "#) and " & _
          "a.cod_loja=b.cod_loja and b.cod_loja=c.cod_loja and b.qtd_solicitada > b.qtd_atendida and " & _
          "(a.cod_repres=" & Left(cmbRepres.Text, 4) & " or a.cod_vend=" & Left(cmbRepres.Text, 4) & ") " & _
          "order by b.num_pedido, b.cod_dpk"

11    Set SS = dbaccess.CreateDynaset(SQL)

   'Enquanto houver registros para esta consulta, executa o loop
    
12    With grdConsRepres
13        Do While Not SS.EOF
        
14            liLinha = liLinha + 1
            
15            .Rows = liLinha + 1
16            .Row = liLinha
                
17            If liLinha < 20 Then
18                .Refresh
            End If
            
           'Posiciona dados no grid
19            .Col = 0:  .Text = SS.Fields("dt_pedido")
20            .Col = 1:  .Text = SS.Fields("cod_loja")
21            .Col = 2:  .Text = SS.Fields("num_pedido") & "-" & SS.Fields("seq_pedido")
22            .Col = 3:  .Text = SS.Fields("cod_dpk")
23            .Col = 4:  .Text = SS.Fields("qtd_solicitada")
24            .Col = 5:  .Text = SS.Fields("qtd_atendida")
25            .Col = 6:  .Text = SS.Fields("qtd_atual")
26            .Col = 7:  .Text = SS.Fields("cod_cliente")
27            .Col = 8:  .Text = SS.Fields("nome_cliente")
     
28            SS.MoveNext
        Loop
    
    End With
    
29    SS.Close
30    dbaccess.Close
31    Screen.MousePointer = 0

TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Sub sscmdConsulta2_Click" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descricao:" & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub

Private Sub sscmdLimpa_Click()
    txtInicio.Text = ""
    txtFim.Text = ""
    Call LimpaGrid
End Sub

Private Sub sscmdLimpa2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Limpa a tela para uma nova consulta"
End Sub

Private Sub sscmdSair_Click()
    End
End Sub


Private Sub sscmdSair_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Finaliza o sistema"
End Sub


Private Sub sscmdSair2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = "Finaliza o sistema"
End Sub

Private Sub SSTab1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    StatusBar1.Panels(2) = ""
End Sub


Private Sub txtFim_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtFim)
End Sub


Private Sub txtFim2_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtFim2)
End Sub


Private Sub txtInicio_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtInicio)
End Sub


Private Sub sscmdSair2_Click()
    End
End Sub


Private Sub txtInicio2_KeyPress(KeyAscii As Integer)
    Call DATA(KeyAscii, txtInicio2)
End Sub


