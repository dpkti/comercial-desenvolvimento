VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmPreposto 
   Caption         =   "Lista de Preposto"
   ClientHeight    =   3075
   ClientLeft      =   2400
   ClientTop       =   2100
   ClientWidth     =   4875
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3075
   ScaleWidth      =   4875
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   3650
      TabIndex        =   0
      Top             =   2700
      Width           =   1095
   End
   Begin MSGrid.Grid grdPreposto 
      Height          =   1800
      Left            =   1905
      TabIndex        =   1
      Top             =   600
      Width           =   1005
      _Version        =   65536
      _ExtentX        =   1773
      _ExtentY        =   3175
      _StockProps     =   77
      BackColor       =   16777215
      FixedCols       =   0
      MouseIcon       =   "L_PREPO.frx":0000
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1920
      TabIndex        =   2
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "frmPreposto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String
Private Sub cmdSair_Click()
    txtResposta = "0"
    Unload Me
    Set frmPreposto = Nothing
End Sub


Private Sub Form_Load()

    On Error GoTo TrataErro
    
    Dim SQL As String
    Dim ss As Object
    Dim i As Integer
    
   'mouse
    Screen.MousePointer = vbHourglass
    
   'nome do form
    frmPreposto.Caption = frmPreposto.Caption & " do Representante: " & frmFimPedido.txtCOD_REPRESENTANTE.Text
    
   'montar SQL
    SQL = "select prep.SEQ_FRANQUIA "
    SQL = SQL & "from REPRESENTANTE prep,"
    SQL = SQL & "FILIAL fil "
    SQL = SQL & "where fil.COD_FILIAL = prep.COD_FILIAL and "
    SQL = SQL & "fil.COD_FRANQUEADOR = " & frmFimPedido.txtCOD_REPRESENTANTE.Text & " and "
    SQL = SQL & "prep.SITUACAO = 0 and prep.SEQ_FRANQUIA > 0 "
    SQL = SQL & "order by prep.COD_REPRES"

   'criar consulta
    Set ss = dbaccess2.CreateSnapshot(SQL)
    
    If ss.EOF And ss.BOF Then
        Screen.MousePointer = vbDefault
        MsgBox "N�o ha preposto cadastrado", vbInformation, "Aten��o"
        Exit Sub
        Unload Me
    End If
    
   'carrega dados
    With grdPreposto
        .Cols = 1
        .Rows = ss.RecordCount + 1
        .ColWidth(0) = 660
        
        .Row = 0
        .Col = 0
        .Text = "Preposto"
        
        For i = 1 To .Rows - 1
            .Row = i
            .Col = 0
            .Text = ss("SEQ_FRANQUIA")
            
            ss.MoveNext
        Next
        .Row = 1
    End With
    
   'mouse
    Screen.MousePointer = vbDefault
        
Exit Sub

TrataErro:
    If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 3197 Then
        Resume
    Else
        Call Process_Line_Errors(SQL)
    End If
End Sub
Private Sub grdPreposto_DblClick()
    grdPreposto.Col = 0
    txtResposta = grdPreposto.Text
    Unload Me
End Sub

Private Sub grdPreposto_KeyPress(KeyAscii As Integer)
    Dim iAscii As Integer
    Dim tam As Byte
    Dim i As Integer
    Dim j As Integer
    Dim iLinha As Integer
        
   'carrega variavel de pesquisa
    tam = Len(strPesquisa)
    grdPreposto.Col = 1
    If KeyAscii = 13 Then
        grdPreposto.Col = 0
        txtResposta = grdPreposto.Text
        Unload Me
    ElseIf KeyAscii = 8 Then 'backspace
       'mouse
        Screen.MousePointer = vbHourglass

        If tam > 0 Then
            strPesquisa = Mid$(strPesquisa, 1, tam - 1)
            If strPesquisa = "" Then
                lblPesquisa.Caption = strPesquisa
                lblPesquisa.Visible = False
                grdPreposto.Row = 1
                SendKeys "{LEFT}+{END}"
            Else
                lblPesquisa.Caption = strPesquisa
                DoEvents
                With grdPreposto
                    iLinha = .Row
                    j = .Row - 1
                    For i = j To 1 Step -1
                        .Row = i
                        If (.Text Like strPesquisa & "*") Then
                            iLinha = .Row
                            Exit For
                        End If
                    Next
                    .Row = iLinha
                    SendKeys "{LEFT}+{END}"
                End With
            End If
        End If
    ElseIf KeyAscii = 27 Then
        strPesquisa = ""
        lblPesquisa.Caption = strPesquisa
        lblPesquisa.Visible = False
        grdPreposto.Row = 1
        SendKeys "{LEFT}{RIGHT}"
    Else
       'mouse
        Screen.MousePointer = vbHourglass
        If tam < 33 Then
            iAscii = Texto(KeyAscii)
            If iAscii > 0 Then
               'pesquisa
                strPesquisa = strPesquisa & Chr$(iAscii)
                If tam >= 0 Then
                    lblPesquisa.Visible = True
                    lblPesquisa.Caption = strPesquisa
                    DoEvents
                    With grdPreposto
                        iLinha = .Row
                        If tam = 0 Then
                            .Row = 0
                            For i = 1 To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next i
                        Else
                            j = .Row
                            For i = j To .Rows - 1
                                .Row = i
                                If (.Text Like strPesquisa & "*") Then
                                    iLinha = .Row
                                    Exit For
                                End If
                            Next
                        End If
                        If grdPreposto.Row <> iLinha Then
                            .Row = iLinha
                            strPesquisa = Mid$(strPesquisa, 1, tam)
                            lblPesquisa.Caption = strPesquisa
                            Beep
                        End If
                        SendKeys "{LEFT}+{END}"
                    End With
                End If
            End If
        Else
            Beep
        End If
    End If
    
   'mouse
    Screen.MousePointer = vbDefault
End Sub

