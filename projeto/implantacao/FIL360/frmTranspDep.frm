VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmTranspDep 
   Caption         =   "Transportadora x UF do Dep�sito"
   ClientHeight    =   2010
   ClientLeft      =   2775
   ClientTop       =   2625
   ClientWidth     =   4710
   LinkTopic       =   "Form1"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2010
   ScaleWidth      =   4710
   Begin VB.ComboBox cboTranspDep 
      Height          =   315
      Left            =   450
      TabIndex        =   0
      Top             =   765
      Width           =   3750
   End
   Begin Threed.SSCommand sscmdOKManu 
      Height          =   510
      Left            =   2115
      TabIndex        =   3
      Top             =   1215
      Visible         =   0   'False
      Width           =   510
      _Version        =   65536
      _ExtentX        =   900
      _ExtentY        =   900
      _StockProps     =   78
      Picture         =   "frmTranspDep.frx":0000
   End
   Begin Threed.SSCommand sscmdOKFim 
      Height          =   510
      Left            =   2115
      TabIndex        =   2
      Top             =   1215
      Visible         =   0   'False
      Width           =   510
      _Version        =   65536
      _ExtentX        =   900
      _ExtentY        =   900
      _StockProps     =   78
      Picture         =   "frmTranspDep.frx":031A
   End
   Begin VB.Label Label1 
      Caption         =   "Escolha a transportadora na lista abaixo:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   240
      Left            =   450
      TabIndex        =   1
      Top             =   405
      Width           =   3750
   End
End
Attribute VB_Name = "frmTranspDep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub sscmdOKFim_Click()
    If cboTranspDep.Text = "" Then
        MsgBox "Escolha uma transportadora!"
        Exit Sub
    End If
    frmFimPedido.txtCOD_TRANSP.Text = Mid(cboTranspDep.Text, 1, 4)
    Unload Me
End Sub


Private Sub sscmdOKManu_Click()
    If cboTranspDep.Text = "" Then
        MsgBox "Escolha uma transportadora!"
        Exit Sub
    End If
    frmVisPedido.txtCOD_TRANSP.Text = Mid(cboTranspDep.Text, 1, 4)
    Unload Me
End Sub


