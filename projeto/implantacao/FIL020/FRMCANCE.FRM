VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Begin VB.Form frmCancela 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Motivos de Cancelamento"
   ClientHeight    =   2925
   ClientLeft      =   1260
   ClientTop       =   1920
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   2925
   ScaleWidth      =   6495
   StartUpPosition =   2  'CenterScreen
   Begin MSGrid.Grid grdCancela 
      Height          =   2175
      Left            =   60
      TabIndex        =   2
      Top             =   270
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "FRMCANCE.frx":0000
   End
   Begin Bot�o.cmd cmdSair 
      Height          =   405
      Left            =   5400
      TabIndex        =   3
      Top             =   2490
      Width           =   1035
      _ExtentX        =   1826
      _ExtentY        =   714
      BTYPE           =   3
      TX              =   "&Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "FRMCANCE.frx":001C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblPesquisa 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   1500
      TabIndex        =   1
      Top             =   30
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Visible         =   0   'False
      Width           =   1320
   End
End
Attribute VB_Name = "frmCancela"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim strPesquisa As String

Private Sub cmdSair_Click()
  cod_cancel = 0
  Unload Me
End Sub

Private Sub Form_Load()
        
1         On Error GoTo TrataErro
          
          Dim ss As Snapshot
          Dim i As Integer
          
          'mouse
2         Screen.MousePointer = vbHourglass
          
          'montar SQL
3         SQL = "select COD_CANCEL,DESC_CANCEL "
4         SQL = SQL & "from CANCEL_PEDNOTA "
5         SQL = SQL & "order by DESC_CANCEL"
          
          'criar consulta
6         Set ss = oradatabase.dbcreatedynaset(SQL, 0&)
          
7         If ss.EOF And ss.BOF Then
8             Screen.MousePointer = vbDefault
9             MsgBox "N�o h� Motivo de Cancelamento cadastrado", vbInformation, "Aten��o"
10            Unload Me
11            Exit Sub
12        End If

13        With frmCancela.grdCancela
14            .Cols = 2
15            .Rows = ss.RecordCount + 1
16            .ColWidth(0) = 660
17            .ColWidth(1) = 5325
              
18            .Row = 0
19            .Col = 0
20            .Text = "C�digo"
21            .Col = 1
22            .Text = "Motivo Cancelamento"
              
23            ss.MoveFirst
24            For i = 1 To .Rows - 1
25                .Row = i
                  
26                .Col = 0
27                .Text = ss("COD_CANCEL")
28                .Col = 1
29                .Text = ss("DESC_CANCEL")
                  
30                ss.MoveNext
31            Next
32            .Row = 1
33        End With
          
34        Screen.MousePointer = vbDefault
              
35        Exit Sub

TrataErro:
36      If Err = 3186 Or Err = 3188 Or Err = 3260 Then
37         Resume
38      Else
            MsgBox "Sub frmCancela_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
40      End If

End Sub

Private Sub grdCancela_DblClick()
 grdCancela.Col = 0
 cod_cancel = grdCancela.Text
 Unload Me
End Sub

Private Sub grdCancela_KeyPress(KeyAscii As Integer)
          
        On Error GoTo Trata_Erro
          
          Dim iAscii As Integer
          Dim tam As Byte
          Dim i As Integer
          Dim j As Integer
          Dim iLinha As Integer
              
          'carrega variavel de pesquisa
1         tam = Len(strPesquisa)
2         grdCancela.Col = 0
3         If KeyAscii = 13 Then
4             grdCancela.Col = 0
5             cod_cancel = grdCancela.Text
6             Unload Me
7         ElseIf KeyAscii = 8 Then 'backspace
              'mouse
8             Screen.MousePointer = vbHourglass

9             If tam > 0 Then
10                strPesquisa = Mid$(strPesquisa, 1, tam - 1)
11                If strPesquisa = "" Then
12                    lblPesquisa.Caption = strPesquisa
13                    lblPesquisa.Visible = False
14                    lblPesq.Visible = False
15                    grdCancela.Row = 1
16                    SendKeys "{LEFT}+{END}"
17                Else
18                    lblPesquisa.Caption = strPesquisa
19                    DoEvents
20                    With grdCancela
21                        iLinha = .Row
22                        j = .Row - 1
23                        For i = j To 1 Step -1
24                            .Row = i
25                            If (.Text Like strPesquisa & "*") Then
26                                iLinha = .Row
27                                Exit For
28                            End If
29                        Next
30                        .Row = iLinha
31                        SendKeys "{LEFT}+{END}"
32                    End With
33                End If
34            End If
              
35        ElseIf KeyAscii = 27 Then
36            strPesquisa = ""
37            lblPesquisa.Caption = strPesquisa
38            lblPesquisa.Visible = False
39            lblPesq.Visible = False
40            grdCancela.Row = 1
41            SendKeys "{LEFT}{RIGHT}"
          
42        Else
              'mouse
43            Screen.MousePointer = vbHourglass

44            If tam < 33 Then
45                iAscii = Texto(KeyAscii)
46                If iAscii > 0 Then
                      'pesquisa
47                    strPesquisa = strPesquisa & Chr$(iAscii)
48                    If tam >= 0 Then
49                        lblPesquisa.Visible = True
50                        lblPesq.Visible = True
51                        lblPesquisa.Caption = strPesquisa
52                        DoEvents
53                        With grdCancela
54                            iLinha = .Row
55                            If tam = 0 Then
56                                .Row = 0
57                                For i = 1 To .Rows - 1
58                                    .Row = i
59                                    If (.Text Like strPesquisa & "*") Then
60                                        iLinha = .Row
61                                        Exit For
62                                    End If
63                                Next i
64                            Else
65                                j = .Row
66                                For i = j To .Rows - 1
67                                    .Row = i
68                                    If (.Text Like strPesquisa & "*") Then
69                                        iLinha = .Row
70                                        Exit For
71                                    End If
72                                Next
73                            End If
74                            If grdCancela.Row <> iLinha Then
75                                .Row = iLinha
76                                strPesquisa = Mid$(strPesquisa, 1, tam)
77                                lblPesquisa.Caption = strPesquisa
78                                Beep
79                            End If
80                            SendKeys "{LEFT}+{END}"
81                        End With
                          
82                    End If
83                End If
84            Else
85                Beep
86            End If
87        End If
          
          'mouse
88        Screen.MousePointer = vbDefault

Trata_Erro:
    If Err.Number <> 0 Then
        MsgBox "Sub grdCancela_KeyPress" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
    End If

End Sub


