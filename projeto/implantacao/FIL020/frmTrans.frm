VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Begin VB.Form frmTransferencia 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Transfer�ncia "
   ClientHeight    =   1455
   ClientLeft      =   2730
   ClientTop       =   2985
   ClientWidth     =   3540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1455
   ScaleWidth      =   3540
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox cboDepTransfer 
      ForeColor       =   &H00800000&
      Height          =   315
      ItemData        =   "frmTrans.frx":0000
      Left            =   720
      List            =   "frmTrans.frx":0002
      TabIndex        =   1
      Top             =   390
      Width           =   2085
   End
   Begin Bot�o.cmd cmdOK 
      Height          =   555
      Left            =   1590
      TabIndex        =   2
      ToolTipText     =   "Confer�ncia/Libera��o do Pedido"
      Top             =   810
      Visible         =   0   'False
      Width           =   555
      _ExtentX        =   979
      _ExtentY        =   979
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmTrans.frx":0004
      PICN            =   "frmTrans.frx":0020
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label lblTransf 
      AutoSize        =   -1  'True
      Caption         =   "Escolha o dep�sito para finalizar a transfer�ncia:"
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3420
   End
End
Attribute VB_Name = "frmTransferencia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cboDepTransfer_Click()
    If Val(cboDepTransfer) = Val(frmVisPedido.lblDeposito(17).Caption) Then
        MsgBox "O dep�sito de transfer�ncia deve ser diferente do escolhido para o pedido."
        cboDepTransfer.SetFocus
    Else
        If Val(cboDepTransfer) <> 0 Then
            Me.cmdOK.Visible = True
        Else
            Me.cmdOK.Visible = False
        End If
    End If
End Sub

Private Sub cmdOK_Click()
    Dim vRst_Transf As Recordset
    
    If Val(cboDepTransfer) = Val(frmVisPedido.lblDeposito(17).Caption) Then
        MsgBox "O dep�sito de transfer�ncia deve ser diferente do escolhido para o pedido."
        cboDepTransfer.SetFocus
    End If
    If Val(cboDepTransfer) = 0 Then
        MsgBox "Informe o CD para transferencia.", vbInformation, "Aten��o"
        Me.cboDepTransfer.SetFocus
        Exit Sub
    End If
    
    Set vRst_Transf = dbAccess2.CreateSnapshot("Select * from transferencia where cod_dep_origem = " & Val(cboDepTransfer) & " and (cod_dep_destino=" & Val(frmVisPedido.lblDeposito(17).Caption) & " OR COD_DEP_DESTINO = 99)")
            
    If vRst_Transf.RecordCount > 0 Then
        MsgBox "Devido a restri��o Fiscal, n�o � poss�vel efetuar transfer�ncia de " & cboDepTransfer & " para " & frmVisPedido.lblDeposito(17).Caption, vbInformation, "Aten��o"
        cboDepTransfer.ListIndex = -1
        Set vRst_Transf = Nothing
        Exit Sub
    End If
    Set vRst_Transf = Nothing
    
    DepTransf = Val(cboDepTransfer.Text)
    Unload Me
End Sub


Private Sub Form_Load()
          
    Preencher_Combo_Transf
          
    If Val(frmVisPedido.lblDeposito(17).Caption) = 21 Or Val(frmVisPedido.lblDeposito(17).Caption) = 9 Then
        Me.cboDepTransfer.Enabled = False
    End If
          
          
'    On Error GoTo Trata_Erro
'
'    Dim ss2 As Snapshot
'
'    'Combo de Transfer�ncia
'    'carrega deposito default e depositos "visao"
'1   SQL2 = "Select distinct(a.cod_loja) as cod_loja,b.nome_fantasia as nome_fantasia"
'2   SQL2 = SQL2 & " from deposito_visao a, loja b, R_UF_DEPOSITO c"
'3   SQL2 = SQL2 & " where a.cod_loja=b.cod_loja and a.nome_programa = 'FIL010' and "
'4   SQL2 = SQL2 & " a.cod_loja <> c.cod_loja "
'
'5   Set ss2 = dbAccess2.CreateSnapshot(SQL2)
'6   FreeLocks
'
'7   If ss2.EOF Then
'8      MsgBox "Tabela DEPOSITO_VISAO est� vazia ligue para Depto.Sistemas", vbExclamation, "Aten��o"
'9      ss2.Close
'10     Exit Sub
'11  End If
'
'12  ss2.MoveLast
'13  ss2.MoveFirst
'
'14  For i = 1 To ss2.RecordCount
'15      cboDepTransfer.AddItem Format(ss2!COD_LOJA, "00") & "-" & ss2!NOME_FANTASIA
'16      ss2.MoveNext
'17  Next
'
'18  cboDepTransfer.Text = deposito_default
'
'Trata_Erro:
'    If Err.Number <> 0 Then
'        MsgBox "Sub frmTransferencia_Load" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o: " & Err.Description & vbCrLf & "Linha:" & Erl
'    End If


End Sub


'*****************************************************
'Eduardo Relvas - 23/01/2006
'Preencher combo de transferencia com os depositos que
'est�o na Tabela DEPOSITO_VISAO cujo nome do programa
'seja FIT030
'*****************************************************
Sub Preencher_Combo_Transf()

1         On Error GoTo Trata_Erro

          Dim Rst As Object
          Dim Deposito As Integer
          
2         If lngCod_Loja <> 0 Then
3             Deposito = lngCod_Loja
4         Else
5             Deposito = Val(deposito_default)
6         End If
          
7         Set Rst = dbAccess2.CreateSnapshot("SELECT count(*) from R_UF_DEPOSITO WHERE COD_LOJA=" & Deposito)
8         FreeLocks
          
9         If Rst.Fields(0) = 0 Then Exit Sub
          
10        Set Rst = dbAccess2.CreateDynaset("SELECT loja.NOME_FANTASIA, Deposito_visao.Cod_loja " & _
                                             " FROM Deposito_Visao INNER JOIN loja ON Deposito_Visao.COD_LOJA = loja.COD_LOJA " & _
                                             " WHERE Deposito_Visao.NOME_PROGRAMA='FIT030'")
11        FreeLocks

12        cboDepTransfer.Clear

13        If Rst.EOF Then
14            MsgBox "Tabela DEPOSITO_VISAO est� sem o FIT030 ou voc� n�o pode efetuar Transfer�ncia, ligue para Depto.Sistemas", vbExclamation, "Aten��o"
15            Rst.Close
16            Exit Sub
17        End If
          
18        Do While Rst.EOF = False
19            cboDepTransfer.AddItem Format(Rst!cod_loja, "00") & "-" & Rst!NOME_FANTASIA
20            Rst.MoveNext
21        Loop

Trata_Erro:
22        If Err.Number <> 0 Then
23            MsgBox "Sub Preencher_Combo_Transf" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & "Linha: " & Erl
24        End If
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then 'Fechou pelo botao
        frmVisPedido.fl_transf = "N"
    End If
End Sub
