VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmComis 
   BorderStyle     =   0  'None
   Caption         =   "CONTROLE DE COMISS�O"
   ClientHeight    =   6675
   ClientLeft      =   435
   ClientTop       =   1515
   ClientWidth     =   10935
   Icon            =   "FORM1.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6675
   ScaleWidth      =   10935
   ShowInTaskbar   =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   5415
      Left            =   240
      TabIndex        =   13
      Top             =   1080
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   9551
      _Version        =   393216
      Tabs            =   20
      Tab             =   15
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Checa Fechamento"
      TabPicture(0)   =   "FORM1.frx":0442
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "SSPanel1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Altera��o"
      TabPicture(1)   =   "FORM1.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SSPanel2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Lan�amento"
      TabPicture(2)   =   "FORM1.frx":047A
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "SSPanel3"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Recibo"
      TabPicture(3)   =   "FORM1.frx":0496
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "SSPanel4"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Importa��o de arquivo"
      TabPicture(4)   =   "FORM1.frx":04B2
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "sscmdProcessa"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Resumo"
      TabPicture(5)   =   "FORM1.frx":04CE
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Label25"
      Tab(5).Control(1)=   "lblPseudo_Resumo"
      Tab(5).Control(2)=   "Label26"
      Tab(5).Control(3)=   "cboAno_MesResumo"
      Tab(5).Control(4)=   "cmdOK_Resumo"
      Tab(5).Control(5)=   "cboTipo_Resumo"
      Tab(5).ControlCount=   6
      TabCaption(6)   =   "Comiss�o Bloqueada"
      TabPicture(6)   =   "FORM1.frx":04EA
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Label22"
      Tab(6).Control(1)=   "Label23"
      Tab(6).Control(2)=   "Label49"
      Tab(6).Control(3)=   "Label50"
      Tab(6).Control(4)=   "Label51"
      Tab(6).Control(5)=   "grdBloqueio"
      Tab(6).Control(6)=   "cbo_bloq_anomes"
      Tab(6).Control(7)=   "cmdVerifica"
      Tab(6).ControlCount=   8
      TabCaption(7)   =   "Gera Arq. Banco Safra"
      TabPicture(7)   =   "FORM1.frx":0506
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "cmdEncerra_Safra"
      Tab(7).Control(1)=   "txtDt_Pagamento"
      Tab(7).Control(2)=   "cboAnoMes_Arq"
      Tab(7).Control(3)=   "cmdAvulso"
      Tab(7).Control(4)=   "cmdPgto_Tlmk"
      Tab(7).Control(5)=   "cmdPgto_Repres"
      Tab(7).Control(6)=   "cmdAdiantamento"
      Tab(7).Control(7)=   "Label30"
      Tab(7).Control(8)=   "Label29"
      Tab(7).ControlCount=   9
      TabCaption(8)   =   "Adiantamento"
      TabPicture(8)   =   "FORM1.frx":0522
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Label24"
      Tab(8).Control(1)=   "Label27"
      Tab(8).Control(2)=   "Label35"
      Tab(8).Control(3)=   "lblTotal_adianta"
      Tab(8).Control(4)=   "grdAdiantamento"
      Tab(8).Control(5)=   "cboAno_Mes_Adiantamento"
      Tab(8).Control(6)=   "cmdOK_Adiantamento"
      Tab(8).ControlCount=   7
      TabCaption(9)   =   "Pgto. Avulso"
      TabPicture(9)   =   "FORM1.frx":053E
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Label31"
      Tab(9).Control(1)=   "Label32"
      Tab(9).Control(2)=   "Label33"
      Tab(9).Control(3)=   "Label34"
      Tab(9).Control(4)=   "sscmdIncluir_Avulso"
      Tab(9).Control(5)=   "lblPseudo_Avulso"
      Tab(9).Control(6)=   "cboAno_Mes_Avulso"
      Tab(9).Control(7)=   "txtRepres_Avulso"
      Tab(9).Control(8)=   "cboTp_Pagto_Avulso"
      Tab(9).Control(9)=   "txtPagto"
      Tab(9).ControlCount=   10
      TabCaption(10)  =   "Gera Contabiliza��o"
      TabPicture(10)  =   "FORM1.frx":055A
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "Label36"
      Tab(10).Control(1)=   "cboAnoMes_Contab"
      Tab(10).Control(2)=   "cmdContab"
      Tab(10).ControlCount=   3
      TabCaption(11)  =   "Gera Arq.Depto Pessoal"
      TabPicture(11)  =   "FORM1.frx":0576
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "cmdDP_Promo"
      Tab(11).Control(1)=   "cmdDP"
      Tab(11).Control(2)=   "cboAno_MesDP"
      Tab(11).Control(3)=   "Label39"
      Tab(11).ControlCount=   4
      TabCaption(12)  =   "Cadastro Chapa"
      TabPicture(12)  =   "FORM1.frx":0592
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "Label37"
      Tab(12).Control(1)=   "Label38"
      Tab(12).Control(2)=   "SSCommand1"
      Tab(12).Control(3)=   "lblPseudo_Chapa"
      Tab(12).Control(4)=   "grdChapa"
      Tab(12).Control(5)=   "txtTlmk_Chapa"
      Tab(12).Control(6)=   "txtChapa"
      Tab(12).ControlCount=   7
      TabCaption(13)  =   "Rec�lculo Fech.Cota"
      TabPicture(13)  =   "FORM1.frx":05AE
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "Label40"
      Tab(13).Control(1)=   "Label41"
      Tab(13).Control(2)=   "cmdFech_Cota"
      Tab(13).Control(3)=   "cboAnoMes_Fech"
      Tab(13).ControlCount=   4
      TabCaption(14)  =   "Gera Relat�rio Contabil"
      TabPicture(14)  =   "FORM1.frx":05CA
      Tab(14).ControlEnabled=   0   'False
      Tab(14).Control(0)=   "cmdSem_Pagto"
      Tab(14).Control(1)=   "cmdContabil"
      Tab(14).Control(2)=   "cboAno_MesRC"
      Tab(14).Control(3)=   "Label42"
      Tab(14).ControlCount=   4
      TabCaption(15)  =   "Gera Adiantamento"
      TabPicture(15)  =   "FORM1.frx":05E6
      Tab(15).ControlEnabled=   -1  'True
      Tab(15).Control(0)=   "Label43"
      Tab(15).Control(0).Enabled=   0   'False
      Tab(15).Control(1)=   "Label44"
      Tab(15).Control(1).Enabled=   0   'False
      Tab(15).Control(2)=   "txtAinicio"
      Tab(15).Control(2).Enabled=   0   'False
      Tab(15).Control(3)=   "txtAFim"
      Tab(15).Control(3).Enabled=   0   'False
      Tab(15).Control(4)=   "cmdGera_Adiantamento"
      Tab(15).Control(4).Enabled=   0   'False
      Tab(15).ControlCount=   5
      TabCaption(16)  =   "Arquivo FIS_PJ"
      TabPicture(16)  =   "FORM1.frx":0602
      Tab(16).ControlEnabled=   0   'False
      Tab(16).Control(0)=   "cmdFIS_PFJ"
      Tab(16).Control(1)=   "cboAno_MesPFJ"
      Tab(16).Control(2)=   "Label45"
      Tab(16).ControlCount=   3
      TabCaption(17)  =   "Gera Arq. Banco Bradesco"
      TabPicture(17)  =   "FORM1.frx":061E
      Tab(17).ControlEnabled=   0   'False
      Tab(17).Control(0)=   "Label46"
      Tab(17).Control(1)=   "Label47"
      Tab(17).Control(2)=   "txtPagto_Bradesco"
      Tab(17).Control(3)=   "cboAno_mes_Bradesco"
      Tab(17).Control(4)=   "cmdAvulso_Bradesco"
      Tab(17).Control(5)=   "cmdPgto_tlmk_Bradesco"
      Tab(17).Control(6)=   "cmdPgto_Repres_Bradesco"
      Tab(17).Control(7)=   "cmdAdiantamento_Bradesco"
      Tab(17).Control(8)=   "cmdEncerra_Bradesco"
      Tab(17).Control(9)=   "cmdPgto_Promo_Bradesco"
      Tab(17).Control(10)=   "cmdPgto_DtCancel"
      Tab(17).ControlCount=   11
      TabCaption(18)  =   "Posi��o Empr�stimo"
      TabPicture(18)  =   "FORM1.frx":063A
      Tab(18).ControlEnabled=   0   'False
      Tab(18).Control(0)=   "Label48"
      Tab(18).Control(1)=   "cboPos"
      Tab(18).Control(2)=   "cmdPos_Emprestimo"
      Tab(18).Control(3)=   "Command2"
      Tab(18).ControlCount=   4
      TabCaption(19)  =   "Gera Arq. Controladoria"
      TabPicture(19)  =   "FORM1.frx":0656
      Tab(19).ControlEnabled=   0   'False
      Tab(19).Control(0)=   "Command1"
      Tab(19).ControlCount=   1
      Begin VB.CommandButton cmdPgto_DtCancel 
         Caption         =   "             Pagamento Repres./Tlmk(PJ/PF)                      (com data de desligamento)"
         Height          =   495
         Left            =   -72000
         TabIndex        =   150
         Top             =   4320
         Width           =   3735
      End
      Begin VB.CommandButton cmdPgto_Promo_Bradesco 
         Caption         =   "Pagamento Promotor"
         Height          =   495
         Left            =   -72000
         TabIndex        =   141
         Top             =   4680
         Visible         =   0   'False
         Width           =   3735
      End
      Begin VB.CommandButton cmdDP_Promo 
         Caption         =   "Gera Arq. Depto Pessoal - PROMOTOR"
         Height          =   495
         Left            =   -71520
         TabIndex        =   140
         Top             =   4080
         Width           =   3015
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Gera Relat�rio Posi��o Empr�stimo Cancelados"
         Height          =   495
         Left            =   -71640
         TabIndex        =   139
         Top             =   3960
         Width           =   3015
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Gera arq. Controladoria - Posi��o parcial do Fechamento de Comiss�o"
         Height          =   495
         Left            =   -71400
         TabIndex        =   138
         Top             =   3240
         Width           =   3015
      End
      Begin VB.CommandButton cmdSem_Pagto 
         Caption         =   "Gera Relat�rio Contabil - Vls n�o Pagos"
         Height          =   495
         Left            =   -71520
         TabIndex        =   137
         Top             =   3960
         Width           =   3015
      End
      Begin VB.CommandButton cmdPos_Emprestimo 
         Caption         =   "Gera Relat�rio Posi��o Empr�stimo"
         Height          =   495
         Left            =   -71640
         TabIndex        =   133
         Top             =   3240
         Width           =   3015
      End
      Begin VB.ComboBox cboPos 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70440
         TabIndex        =   132
         Top             =   2640
         Width           =   1095
      End
      Begin VB.CommandButton cmdEncerra_Safra 
         Caption         =   "Encerra Fechamento"
         Height          =   495
         Left            =   -67800
         TabIndex        =   130
         Top             =   1920
         Width           =   2175
      End
      Begin VB.CommandButton cmdEncerra_Bradesco 
         Caption         =   "Encerra Fechamento"
         Height          =   495
         Left            =   -67920
         TabIndex        =   129
         Top             =   2040
         Width           =   2175
      End
      Begin VB.CommandButton cmdAdiantamento_Bradesco 
         Caption         =   "Adiantamento - Dia 25"
         Height          =   495
         Left            =   -72000
         TabIndex        =   128
         Top             =   2520
         Width           =   3735
      End
      Begin VB.CommandButton cmdPgto_Repres_Bradesco 
         Caption         =   "Pagamento Repres./Tlmk(PJ/PF) - Dia 10"
         Height          =   495
         Left            =   -72000
         TabIndex        =   127
         Top             =   3120
         Width           =   3735
      End
      Begin VB.CommandButton cmdPgto_tlmk_Bradesco 
         Caption         =   "Pagamento Tlmk(Funcion�rio) - 5o.Dia �til"
         Height          =   495
         Left            =   -72000
         TabIndex        =   126
         Top             =   4680
         Visible         =   0   'False
         Width           =   3735
      End
      Begin VB.CommandButton cmdAvulso_Bradesco 
         Caption         =   "Pagamento Avulso"
         Height          =   495
         Left            =   -72000
         TabIndex        =   125
         Top             =   3720
         Width           =   3735
      End
      Begin VB.ComboBox cboAno_mes_Bradesco 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70560
         TabIndex        =   122
         Top             =   1800
         Width           =   1095
      End
      Begin VB.TextBox txtPagto_Bradesco 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70560
         MaxLength       =   2
         TabIndex        =   121
         Top             =   2160
         Width           =   375
      End
      Begin VB.CommandButton cmdFIS_PFJ 
         Caption         =   "Gera FIS_PJ"
         Height          =   495
         Left            =   -71760
         TabIndex        =   120
         Top             =   3360
         Width           =   3015
      End
      Begin VB.ComboBox cboAno_MesPFJ 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70440
         TabIndex        =   119
         Top             =   2640
         Width           =   1095
      End
      Begin VB.CommandButton cmdGera_Adiantamento 
         Caption         =   "Gera Adiantamento"
         Height          =   495
         Left            =   3840
         TabIndex        =   117
         Top             =   3180
         Width           =   2415
      End
      Begin VB.TextBox txtAFim 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   5520
         TabIndex        =   116
         Top             =   2220
         Width           =   1095
      End
      Begin VB.TextBox txtAinicio 
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   3840
         TabIndex        =   114
         Top             =   2220
         Width           =   1095
      End
      Begin VB.CommandButton cmdContabil 
         Caption         =   "Gera Relat�rio Contabil"
         Height          =   495
         Left            =   -71520
         TabIndex        =   112
         Top             =   3120
         Width           =   3015
      End
      Begin VB.ComboBox cboAno_MesRC 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70320
         TabIndex        =   111
         Top             =   2340
         Width           =   1095
      End
      Begin VB.ComboBox cboAnoMes_Fech 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70200
         TabIndex        =   108
         Top             =   2340
         Width           =   1095
      End
      Begin VB.CommandButton cmdFech_Cota 
         Caption         =   "Recalcula Premio Fech Cota"
         Height          =   975
         Left            =   -71280
         TabIndex        =   106
         Top             =   2940
         Width           =   2775
      End
      Begin VB.CommandButton cmdDP 
         Caption         =   "Gera Arq. Depto Pessoal - TLMKT"
         Height          =   495
         Left            =   -71520
         TabIndex        =   104
         Top             =   3240
         Width           =   3015
      End
      Begin VB.ComboBox cboAno_MesDP 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70320
         TabIndex        =   103
         Top             =   2700
         Width           =   1095
      End
      Begin VB.TextBox txtChapa 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   -69600
         MaxLength       =   8
         TabIndex        =   99
         Top             =   2880
         Width           =   1455
      End
      Begin VB.TextBox txtTlmk_Chapa 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   -69600
         MaxLength       =   4
         TabIndex        =   97
         Top             =   2400
         Width           =   735
      End
      Begin VB.CommandButton cmdContab 
         Caption         =   "Gera Arq. Contabiliza��o M�s"
         Height          =   495
         Left            =   -71640
         TabIndex        =   95
         Top             =   3360
         Width           =   3015
      End
      Begin VB.ComboBox cboAnoMes_Contab 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70320
         TabIndex        =   94
         Top             =   2640
         Width           =   1095
      End
      Begin VB.TextBox txtPagto 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   -70080
         MaxLength       =   14
         TabIndex        =   88
         Top             =   3360
         Width           =   1455
      End
      Begin VB.ComboBox cboTp_Pagto_Avulso 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   -70080
         TabIndex        =   86
         Top             =   3000
         Width           =   4575
      End
      Begin VB.TextBox txtRepres_Avulso 
         Alignment       =   1  'Right Justify
         ForeColor       =   &H00800000&
         Height          =   285
         Left            =   -70080
         MaxLength       =   4
         TabIndex        =   82
         Top             =   2280
         Width           =   735
      End
      Begin VB.ComboBox cboAno_Mes_Avulso 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70080
         TabIndex        =   81
         Top             =   2640
         Width           =   1095
      End
      Begin VB.TextBox txtDt_Pagamento 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70080
         MaxLength       =   2
         TabIndex        =   80
         Top             =   2280
         Width           =   375
      End
      Begin VB.ComboBox cboAnoMes_Arq 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70080
         TabIndex        =   78
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CommandButton cmdAvulso 
         Caption         =   "Pagamento Avulso"
         Height          =   495
         Left            =   -71760
         TabIndex        =   76
         Top             =   4440
         Width           =   3735
      End
      Begin VB.CommandButton cmdPgto_Tlmk 
         Caption         =   "Pagamento Tlmk(Funcion�rio) - 5o.Dia �til"
         Height          =   495
         Left            =   -71760
         TabIndex        =   75
         Top             =   3840
         Width           =   3735
      End
      Begin VB.CommandButton cmdPgto_Repres 
         Caption         =   "Pagamento Repres./Tlmk(PJ/PF) - Dia 10"
         Height          =   495
         Left            =   -71760
         TabIndex        =   74
         Top             =   3240
         Width           =   3735
      End
      Begin VB.CommandButton cmdAdiantamento 
         Caption         =   "Adiantamento - Dia 25"
         Height          =   495
         Left            =   -71760
         TabIndex        =   73
         Top             =   2640
         Width           =   3735
      End
      Begin VB.ComboBox cboTipo_Resumo 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70800
         TabIndex        =   71
         Top             =   2760
         Width           =   2055
      End
      Begin VB.CommandButton cmdOK_Resumo 
         Caption         =   "Verificar"
         Height          =   375
         Left            =   -68520
         TabIndex        =   68
         Top             =   2760
         Width           =   1455
      End
      Begin VB.ComboBox cboAno_MesResumo 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70800
         TabIndex        =   67
         Top             =   2280
         Width           =   1095
      End
      Begin VB.CommandButton cmdOK_Adiantamento 
         Caption         =   "Verificar"
         Height          =   375
         Left            =   -69240
         TabIndex        =   64
         Top             =   1800
         Width           =   1455
      End
      Begin VB.ComboBox cboAno_Mes_Adiantamento 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70560
         TabIndex        =   63
         Top             =   1920
         Width           =   1095
      End
      Begin VB.CommandButton cmdVerifica 
         Caption         =   "Verificar"
         Height          =   375
         Left            =   -69240
         TabIndex        =   60
         Top             =   1860
         Width           =   1455
      End
      Begin VB.ComboBox cbo_bloq_anomes 
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   -70560
         TabIndex        =   59
         Top             =   1860
         Width           =   1095
      End
      Begin Threed.SSPanel SSPanel1 
         Height          =   3555
         Left            =   -74640
         TabIndex        =   14
         Top             =   1560
         Width           =   9615
         _Version        =   65536
         _ExtentX        =   16960
         _ExtentY        =   6271
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.PictureBox picAlteracao 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":0672
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   36
            Top             =   3000
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.PictureBox picLancamento 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":097C
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   35
            Top             =   2640
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.PictureBox picAdiantamento 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":0C86
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   34
            Top             =   2280
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.PictureBox picComissao 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":0F90
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   33
            Top             =   1920
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.PictureBox picQuota 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":129A
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   32
            Top             =   1560
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.PictureBox picVenda 
            BackColor       =   &H00C0C0C0&
            BorderStyle     =   0  'None
            Height          =   255
            Left            =   4440
            Picture         =   "FORM1.frx":15A4
            ScaleHeight     =   255
            ScaleWidth      =   375
            TabIndex        =   31
            Top             =   1200
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.ComboBox cboMes 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   4680
            TabIndex        =   30
            Top             =   240
            Width           =   855
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "Verificar"
            Height          =   375
            Left            =   5760
            TabIndex        =   16
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label lblStatus 
            Alignment       =   2  'Center
            Caption         =   "FECHAMENTO EM ABERTO"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   18
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   1455
            Left            =   5400
            TabIndex        =   37
            Top             =   1560
            Visible         =   0   'False
            Width           =   2775
         End
         Begin VB.Label Label8 
            Caption         =   "M�s"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4200
            TabIndex        =   29
            Top             =   240
            Width           =   495
         End
         Begin VB.Label lblAlteracao 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   28
            Top             =   3000
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblLancamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   27
            Top             =   2640
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblAdiantamento 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   26
            Top             =   2280
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblComissao 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   25
            Top             =   1920
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblQuota 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   24
            Top             =   1560
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label lblVenda 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BackStyle       =   0  'Transparent
            BorderStyle     =   1  'Fixed Single
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H000000FF&
            Height          =   255
            Left            =   3480
            TabIndex        =   23
            Top             =   1200
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label Label7 
            Caption         =   "TOTAL DE ALTERA��O"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   22
            Top             =   3000
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label6 
            Caption         =   "TOTAL DE LAN�AMENTO"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   21
            Top             =   2640
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label5 
            Caption         =   "TOTAL DE ADIANTAMENTO"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   20
            Top             =   2280
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label4 
            Caption         =   "TOTAL DE COMISS�O"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   19
            Top             =   1920
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label3 
            Caption         =   "TOTAL DE QUOTA"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   18
            Top             =   1560
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "TOTAL DE VENDA"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   840
            TabIndex        =   17
            Top             =   1200
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Dados para fechamento de Comiss�o."
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   720
            TabIndex        =   15
            Top             =   240
            Width           =   3255
         End
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   3435
         Left            =   -74880
         TabIndex        =   38
         Top             =   1680
         Width           =   9735
         _Version        =   65536
         _ExtentX        =   17171
         _ExtentY        =   6059
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox txtCod_Repres_Lanc 
            Alignment       =   1  'Right Justify
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   4200
            MaxLength       =   4
            TabIndex        =   6
            Top             =   2400
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.ComboBox cboEscolha 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   4200
            TabIndex        =   5
            Top             =   2040
            Width           =   855
         End
         Begin VB.TextBox txtVL_Novo 
            Alignment       =   1  'Right Justify
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   4200
            MaxLength       =   14
            TabIndex        =   4
            Top             =   1680
            Width           =   1455
         End
         Begin VB.TextBox txtVL_Atual 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   4200
            MaxLength       =   14
            TabIndex        =   3
            Top             =   1320
            Width           =   1455
         End
         Begin VB.ComboBox cboAlteracao 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   330
            Left            =   4200
            TabIndex        =   2
            Top             =   960
            Width           =   3975
         End
         Begin VB.ComboBox cboAno_mes 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   4200
            TabIndex        =   1
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox txtCod_repres 
            Alignment       =   1  'Right Justify
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   4200
            MaxLength       =   4
            TabIndex        =   0
            Top             =   240
            Width           =   735
         End
         Begin Threed.SSCommand sscmdInc_Alteracao 
            Height          =   495
            Left            =   3600
            TabIndex        =   7
            Top             =   2760
            Width           =   1695
            _Version        =   65536
            _ExtentX        =   2990
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "Incluir"
         End
         Begin VB.Label lblPseudo1 
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5040
            TabIndex        =   54
            Top             =   2400
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label lblPseudo 
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5040
            TabIndex        =   53
            Top             =   240
            Width           =   2295
         End
         Begin VB.Label Label19 
            Caption         =   "Representante/Telemarketing"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   52
            Top             =   2400
            Visible         =   0   'False
            Width           =   2295
         End
         Begin VB.Label Label18 
            Caption         =   "Gera Lan�amento ?"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   51
            Top             =   2040
            Width           =   2295
         End
         Begin VB.Label Label17 
            Caption         =   "Valor Novo"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   50
            Top             =   1680
            Width           =   2295
         End
         Begin VB.Label Label16 
            Caption         =   "Valor Atual"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   49
            Top             =   1320
            Width           =   2295
         End
         Begin VB.Label Label15 
            Caption         =   "Tipo de Alteracao"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   48
            Top             =   960
            Width           =   2295
         End
         Begin VB.Label Label14 
            Caption         =   "Ano-M�s"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   47
            Top             =   600
            Width           =   2295
         End
         Begin VB.Label Label13 
            Caption         =   "Representante/Telemarketing"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   46
            Top             =   240
            Width           =   2295
         End
      End
      Begin Threed.SSPanel SSPanel3 
         Height          =   3435
         Left            =   -74760
         TabIndex        =   39
         Top             =   1680
         Width           =   9615
         _Version        =   65536
         _ExtentX        =   16960
         _ExtentY        =   6059
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox txtValor 
            Alignment       =   1  'Right Justify
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   3960
            MaxLength       =   14
            TabIndex        =   11
            Top             =   1680
            Width           =   1455
         End
         Begin VB.ComboBox cboTP_Lancamento 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   330
            Left            =   3960
            TabIndex        =   10
            Top             =   1320
            Width           =   3975
         End
         Begin VB.ComboBox cboAnoMes 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   3960
            TabIndex        =   9
            Top             =   960
            Width           =   1095
         End
         Begin VB.TextBox txtRepres 
            Alignment       =   1  'Right Justify
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   3960
            MaxLength       =   4
            TabIndex        =   8
            Top             =   600
            Width           =   735
         End
         Begin Threed.SSCommand sscmdIncluir 
            Height          =   495
            Left            =   3600
            TabIndex        =   12
            Top             =   2400
            Width           =   1695
            _Version        =   65536
            _ExtentX        =   2990
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "Incluir"
         End
         Begin VB.Label Label12 
            Caption         =   "Valor do Lan�amento"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1440
            TabIndex        =   45
            Top             =   1680
            Width           =   2295
         End
         Begin VB.Label Label11 
            Caption         =   "Tipo de Lancamento"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1440
            TabIndex        =   44
            Top             =   1320
            Width           =   2295
         End
         Begin VB.Label Label10 
            Caption         =   "Ano-M�s"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1440
            TabIndex        =   43
            Top             =   960
            Width           =   2295
         End
         Begin VB.Label lblPseudonimo 
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4800
            TabIndex        =   42
            Top             =   600
            Width           =   2295
         End
         Begin VB.Label Label9 
            Caption         =   "Representante/Telemarketing"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1440
            TabIndex        =   41
            Top             =   600
            Width           =   2295
         End
      End
      Begin Threed.SSPanel SSPanel4 
         Height          =   3705
         Left            =   -74910
         TabIndex        =   40
         Top             =   1590
         Width           =   9525
         _Version        =   65536
         _ExtentX        =   16801
         _ExtentY        =   6535
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.TextBox txtRepres1 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            ForeColor       =   &H00800000&
            Height          =   285
            Left            =   4200
            MaxLength       =   4
            TabIndex        =   144
            Top             =   900
            Width           =   735
         End
         Begin VB.ComboBox cboAno_Mes1 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   4200
            TabIndex        =   143
            Top             =   1620
            Width           =   1095
         End
         Begin VB.ComboBox cboTipo_Pessoa 
            ForeColor       =   &H00800000&
            Height          =   315
            Left            =   4200
            TabIndex        =   142
            Top             =   1260
            Width           =   3615
         End
         Begin Threed.SSCommand sscmdVisualizar 
            Height          =   495
            Left            =   4170
            TabIndex        =   145
            Top             =   2100
            Width           =   975
            _Version        =   65536
            _ExtentX        =   1720
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "Visualizar"
         End
         Begin VB.Label Label20 
            BackStyle       =   0  'Transparent
            Caption         =   "Representante/Telemarketing"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1920
            TabIndex        =   149
            Top             =   900
            Width           =   2295
         End
         Begin VB.Label Label21 
            BackStyle       =   0  'Transparent
            Caption         =   "Ano-M�s"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1920
            TabIndex        =   148
            Top             =   1620
            Width           =   2295
         End
         Begin VB.Label lblPseudo2 
            BackStyle       =   0  'Transparent
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5010
            TabIndex        =   147
            Top             =   900
            Width           =   2805
         End
         Begin VB.Label Label28 
            BackStyle       =   0  'Transparent
            Caption         =   "Tipo"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1920
            TabIndex        =   146
            Top             =   1260
            Width           =   2295
         End
      End
      Begin MSGrid.Grid grdChapa 
         Height          =   2415
         Left            =   -74760
         TabIndex        =   105
         Top             =   1980
         Visible         =   0   'False
         Width           =   4095
         _Version        =   65536
         _ExtentX        =   7223
         _ExtentY        =   4260
         _StockProps     =   77
         BackColor       =   12648447
      End
      Begin Threed.SSCommand SSCommand1 
         Height          =   495
         Left            =   -69120
         TabIndex        =   100
         Top             =   3660
         Width           =   1695
         _Version        =   65536
         _ExtentX        =   2990
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Incluir"
      End
      Begin Threed.SSCommand sscmdIncluir_Avulso 
         Height          =   495
         Left            =   -71160
         TabIndex        =   89
         Top             =   4080
         Width           =   1695
         _Version        =   65536
         _ExtentX        =   2990
         _ExtentY        =   873
         _StockProps     =   78
         Caption         =   "Incluir"
      End
      Begin MSGrid.Grid grdAdiantamento 
         Height          =   2415
         Left            =   -73440
         TabIndex        =   65
         Top             =   2340
         Visible         =   0   'False
         Width           =   6975
         _Version        =   65536
         _ExtentX        =   12303
         _ExtentY        =   4260
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
      End
      Begin MSGrid.Grid grdBloqueio 
         Height          =   2535
         Left            =   -72480
         TabIndex        =   57
         Top             =   2400
         Visible         =   0   'False
         Width           =   6015
         _Version        =   65536
         _ExtentX        =   10610
         _ExtentY        =   4471
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
      End
      Begin Threed.SSCommand sscmdProcessa 
         Height          =   855
         Left            =   -71160
         TabIndex        =   55
         Top             =   2940
         Width           =   3015
         _Version        =   65536
         _ExtentX        =   5318
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "Processa Arquivo de Lan�amentos"
      End
      Begin VB.Label Label51 
         Caption         =   "<F3> Liberar Comiss�o sem Pagto"
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   -74880
         TabIndex        =   136
         Top             =   3240
         Width           =   2295
      End
      Begin VB.Label Label50 
         Caption         =   "<Duplo Click> Liberar Comiss�o para Pagto"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   -74880
         TabIndex        =   135
         Top             =   2400
         Width           =   2295
      End
      Begin VB.Label Label49 
         Caption         =   "<F2> Bloquear Comiss�o"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -74880
         TabIndex        =   134
         Top             =   2880
         Width           =   1815
      End
      Begin VB.Label Label48 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71280
         TabIndex        =   131
         Top             =   2760
         Width           =   855
      End
      Begin VB.Label Label47 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   -71640
         TabIndex        =   124
         Top             =   1800
         Width           =   855
      End
      Begin VB.Label Label46 
         Caption         =   "Data Pagto"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71640
         TabIndex        =   123
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label Label45 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71400
         TabIndex        =   118
         Top             =   2640
         Width           =   855
      End
      Begin VB.Label Label44 
         Alignment       =   2  'Center
         Caption         =   "A"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5040
         TabIndex        =   115
         Top             =   2220
         Width           =   375
      End
      Begin VB.Label Label43 
         Caption         =   "INTERVALO DE:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2160
         TabIndex        =   113
         Top             =   2220
         Width           =   1575
      End
      Begin VB.Label Label42 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71160
         TabIndex        =   110
         Top             =   2460
         Width           =   855
      End
      Begin VB.Label Label41 
         Caption         =   "Aguarde. Esta op��o pode demorar alguns minutos ..."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71640
         TabIndex        =   109
         Top             =   4620
         Width           =   4335
      End
      Begin VB.Label Label40 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71040
         TabIndex        =   107
         Top             =   2340
         Width           =   735
      End
      Begin VB.Label Label39 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71160
         TabIndex        =   102
         Top             =   2700
         Width           =   975
      End
      Begin VB.Label lblPseudo_Chapa 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -68640
         TabIndex        =   101
         Top             =   2400
         Width           =   2295
      End
      Begin VB.Label Label38 
         Caption         =   "Chapa"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -70560
         TabIndex        =   98
         Top             =   2940
         Width           =   1215
      End
      Begin VB.Label Label37 
         Caption         =   "Telemarketing"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -70680
         TabIndex        =   96
         Top             =   2460
         Width           =   1215
      End
      Begin VB.Label Label36 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71400
         TabIndex        =   93
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label lblTotal_adianta 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -70080
         TabIndex        =   92
         Top             =   4920
         Width           =   1455
      End
      Begin VB.Label Label35 
         Caption         =   "TOTAL"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -72360
         TabIndex        =   91
         Top             =   4920
         Width           =   1335
      End
      Begin VB.Label lblPseudo_Avulso 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -69240
         TabIndex        =   90
         Top             =   2280
         Width           =   2295
      End
      Begin VB.Label Label34 
         Caption         =   "Valor do Pagamento"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -72360
         TabIndex        =   87
         Top             =   3360
         Width           =   2295
      End
      Begin VB.Label Label33 
         Caption         =   "Tipo de Pagto Avulso"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -72360
         TabIndex        =   85
         Top             =   3000
         Width           =   2295
      End
      Begin VB.Label Label32 
         Caption         =   "Representante/Telemarketing"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -72360
         TabIndex        =   84
         Top             =   2280
         Width           =   2295
      End
      Begin VB.Label Label31 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -72360
         TabIndex        =   83
         Top             =   2640
         Width           =   2295
      End
      Begin VB.Label Label30 
         Caption         =   "Data Pagto"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71160
         TabIndex        =   79
         Top             =   2280
         Width           =   855
      End
      Begin VB.Label Label29 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   -71160
         TabIndex        =   77
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label26 
         Caption         =   "Tipo"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71520
         TabIndex        =   72
         Top             =   2760
         Width           =   495
      End
      Begin VB.Label Label27 
         Caption         =   "Duplo click exclui adiantamento"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -74880
         TabIndex        =   70
         Top             =   4860
         Width           =   2295
      End
      Begin VB.Label lblPseudo_Resumo 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -69720
         TabIndex        =   69
         Top             =   2400
         Width           =   2295
      End
      Begin VB.Label Label25 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71520
         TabIndex        =   66
         Top             =   2280
         Width           =   735
      End
      Begin VB.Label Label24 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71280
         TabIndex        =   62
         Top             =   1920
         Width           =   735
      End
      Begin VB.Label Label23 
         Caption         =   "<F9> Motivo de Bloqueio"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -74880
         TabIndex        =   61
         Top             =   3720
         Width           =   1815
      End
      Begin VB.Label Label22 
         Caption         =   "Ano-M�s"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -71520
         TabIndex        =   58
         Top             =   1860
         Width           =   735
      End
   End
   Begin Threed.SSCommand sscmdSair 
      Height          =   375
      Left            =   9720
      TabIndex        =   56
      Top             =   360
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "Sair"
   End
   Begin VB.Image Image1 
      Height          =   975
      Left            =   480
      Picture         =   "FORM1.frx":18AE
      Top             =   0
      Width           =   1200
   End
End
Attribute VB_Name = "frmComis"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : frmComis
' Author    : c.samuel.oliveira
' Date      : 02/06/2017
' Purpose   : TI-5720
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub cbo_bloq_anomes_DropDown()
          Dim i As Long

1         If cbo_bloq_anomes.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cbo_bloq_anomes.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If
End Sub

Private Sub cboAlteracao_DropDown()
          Dim i As Long
          Dim lngTotal As Long

1         If cboAlteracao.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_tp_alteracao(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAlteracao.AddItem ss!alteracao
11                ss.MoveNext
12            Next
13        End If


End Sub

Private Sub cboAlteracao_LostFocus()
      'verifica status do fechamento
1         OraParameters.Remove "ano_mes"
2         OraParameters.Add "ano_mes", cboAno_mes, 1
3         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL

5         Set ss = oradatabase.Parameters("vCursor").Value

6         If oradatabase.Parameters("vErro").Value <> 0 Then
7             Call Process_Line_Errors(SQL)
8             Exit Sub
9         End If

10        If ss!fl_status = 2 Then
11            MsgBox "Fechamento j� encerrado n�o � permitido alterar valores", vbInformation, "Aten��o"
12            cboAno_mes = ""
13            txtCod_repres = ""
14            lblPseudo1 = ""
15            Exit Sub
16        End If

17        If cboAno_mes <> "" And _
             txtCod_repres <> "" And _
             cboAlteracao <> "" Then
              'verifica valor atual
18            OraParameters.Remove "ano_mes"
19            OraParameters.Add "ano_mes", cboAno_mes, 1
20            OraParameters.Remove "repres"
21            OraParameters.Add "repres", txtCod_repres, 1
22            OraParameters.Remove "tp"
23            OraParameters.Add "tp", Mid(Trim(cboAlteracao), 1, 3), 1

24            V_SQL = "Begin producao.PCK_VDA860.pr_consulta_alteracao(:vCursor,:ano_mes,:repres,:tp,:vErro);END;"


25            oradatabase.ExecuteSQL V_SQL

26            Set ss = oradatabase.Parameters("vCursor").Value

27            If oradatabase.Parameters("vErro").Value <> 0 Then
28                Call Process_Line_Errors(SQL)
29                Exit Sub
30            End If

31            txtVL_Atual = Format(IIf(IsNull(ss!total), 0, ss!total), "0.00")
32        End If

End Sub

Private Sub cboAno_Mes_Adiantamento_DropDown()
          Dim i As Long

1         If cboAno_Mes_Adiantamento.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_Mes_Adiantamento.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

End Sub

Private Sub cboAno_Mes_Avulso_DropDown()
          Dim i As Long

1         If cboAno_Mes_Avulso.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_Mes_Avulso.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If


End Sub

Private Sub cboAno_mes_Bradesco_DropDown()
          Dim ss As Object
          Dim i As Long

1         If cboAno_mes_Bradesco.ListCount = 0 Then
2             Screen.MousePointer = 11
3             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


4             oradatabase.ExecuteSQL V_SQL

5             Set ss = oradatabase.Parameters("vCursor").Value

6             If oradatabase.Parameters("vErro").Value <> 0 Then
7                 Call Process_Line_Errors(SQL)
8                 Exit Sub
9             End If

10            For i = 1 To ss.RecordCount
11                cboAno_mes_Bradesco.AddItem ss!ano_mes
12                ss.MoveNext
13            Next
14        End If
15        Screen.MousePointer = 0

End Sub

Private Sub cboAno_mes_DropDown()
          Dim i As Long

1         If cboAno_mes.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_mes.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

End Sub

Private Sub cboAno_Mes1_DropDown()
          Dim i As Long

1         If cboAno_Mes1.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_Mes1.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

14        Screen.MousePointer = 0

End Sub

Private Sub cboAno_MesDP_DropDown()
          Dim i As Long

1         If cboAno_MesDP.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_ant(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_MesDP.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

14        Screen.MousePointer = 0

End Sub

Private Sub cboAno_MesPFJ_DropDown()
          Dim i As Long

1         Screen.MousePointer = 11
2         If cboAno_MesPFJ.ListCount = 0 Then
3             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


4             oradatabase.ExecuteSQL V_SQL

5             Set ss = oradatabase.Parameters("vCursor").Value

6             If oradatabase.Parameters("vErro").Value <> 0 Then
7                 Screen.MousePointer = 0
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If

11            For i = 1 To ss.RecordCount
12                cboAno_MesPFJ.AddItem ss!ano_mes
13                ss.MoveNext
14            Next
15        End If

16        Screen.MousePointer = 0

End Sub

Private Sub cboAno_MesRC_DropDown()
          Dim ss As Object
          Dim i As Long

1         If cboAno_MesRC.ListCount = 0 Then
2             Screen.MousePointer = 11
3             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


4             oradatabase.ExecuteSQL V_SQL

5             Set ss = oradatabase.Parameters("vCursor").Value

6             If oradatabase.Parameters("vErro").Value <> 0 Then
7                 Call Process_Line_Errors(SQL)
8                 Exit Sub
9             End If

10            For i = 1 To ss.RecordCount
11                cboAno_MesRC.AddItem ss!ano_mes
12                ss.MoveNext
13            Next
14        End If
15        Screen.MousePointer = 0

End Sub

Private Sub cboAno_MesResumo_DropDown()
          Dim i As Long

1         If cboAno_MesResumo.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAno_MesResumo.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

End Sub

Private Sub cboAnoMes_Arq_DropDown()
          Dim ss As Object
          Dim i As Long

1         If cboAnoMes_Arq.ListCount = 0 Then
2             Screen.MousePointer = 11
3             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


4             oradatabase.ExecuteSQL V_SQL

5             Set ss = oradatabase.Parameters("vCursor").Value

6             If oradatabase.Parameters("vErro").Value <> 0 Then
7                 Call Process_Line_Errors(SQL)
8                 Exit Sub
9             End If

10            For i = 1 To ss.RecordCount
11                cboAnoMes_Arq.AddItem ss!ano_mes
12                ss.MoveNext
13            Next
14        End If
15        Screen.MousePointer = 0

End Sub

Private Sub cboAnoMes_Contab_DropDown()
          Dim i As Long

1         If cboAnoMes_Contab.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_ant(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAnoMes_Contab.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

14        Screen.MousePointer = 0

End Sub

Private Sub cboAnoMes_DropDown()
          Dim i As Long

1         If cboAnoMes.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboAnoMes.AddItem ss!ano_mes
11                ss.MoveNext
12            Next
13        End If

End Sub

Private Sub cboAnoMes_Fech_DropDown()
          Dim i As Long

1         If cboAnoMes_Fech.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_mes_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             ss.MoveFirst
10            For i = 1 To ss.RecordCount
11                cboAnoMes_Fech.AddItem ss!ano_mes
12                Exit For
13                ss.MoveNext
14            Next
15        End If


End Sub

Private Sub cboEscolha_DropDown()
1         If cboEscolha.ListCount = 0 Then
2             cboEscolha.AddItem "SIM"
3             cboEscolha.AddItem "N�O"
4         End If
End Sub

Private Sub cboEscolha_LostFocus()
1         If Mid(Trim(cboEscolha), 1, 1) = "S" Then
2             Label19.Visible = True
3             txtCod_Repres_Lanc.Visible = True
4             lblPseudo1.Visible = True
5         Else
6             Label19.Visible = False
7             txtCod_Repres_Lanc = ""
8             txtCod_Repres_Lanc.Visible = False
9             lblPseudo1 = ""
10            lblPseudo1.Visible = False
11        End If
End Sub

Private Sub cboMes_DropDown()
1         Label2.Visible = False
2         Label3.Visible = False
3         Label4.Visible = False
4         Label5.Visible = False
5         Label6.Visible = False
6         Label7.Visible = False

7         lblVenda.Visible = False
8         lblQuota.Visible = False
9         lblComissao.Visible = False
10        lblAdiantamento.Visible = False
11        lblLancamento.Visible = False
12        lblAlteracao.Visible = False

13        picVenda.Visible = False
14        picQuota.Visible = False
15        picComissao.Visible = False
16        picAdiantamento.Visible = False
17        picLancamento.Visible = False
18        picAlteracao.Visible = False

19        lblStatus.Visible = False

20        DoEvents
End Sub

Private Sub cboPos_DropDown()
          Dim ss As Object
          Dim i As Long

1         If cboPos.ListCount = 0 Then
2             Screen.MousePointer = 11
3             V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


4             oradatabase.ExecuteSQL V_SQL

5             Set ss = oradatabase.Parameters("vCursor").Value

6             If oradatabase.Parameters("vErro").Value <> 0 Then
7                 Call Process_Line_Errors(SQL)
8                 Exit Sub
9             End If

10            For i = 1 To ss.RecordCount
11                cboPos.AddItem ss!ano_mes
12                ss.MoveNext
13            Next
14        End If
15        Screen.MousePointer = 0
End Sub

Private Sub cboTipo_Pessoa_DropDown()
1         If cboTipo_Pessoa.ListCount = 0 Then
2             cboTipo_Pessoa.AddItem "1-PESSOA FISICA"
3             cboTipo_Pessoa.AddItem "2-PESSOA JURIDICA"
4             cboTipo_Pessoa.AddItem "3-TELEMARKETING FUNCIONARIO"
5             cboTipo_Pessoa.AddItem "4-TELEMARKETING PAAC REPRES"
6             cboTipo_Pessoa.AddItem "5-PROMOTOR"
7             cboTipo_Pessoa.AddItem "6-TELEMARKETING PAAC TLMKT"
8         End If
End Sub

Private Sub cboTipo_Resumo_DropDown()
1         If cboTipo_Resumo.ListCount = 0 Then
2             cboTipo_Resumo.AddItem "1-PESSOA FISICA"
3             cboTipo_Resumo.AddItem "2-PESSOA JURIDICA"
4             cboTipo_Resumo.AddItem "3-TELEMARKETING"
5             cboTipo_Resumo.AddItem "4-PROMOTOR"
6         End If
End Sub

Private Sub cboTP_Lancamento_DropDown()
          Dim i As Long

1         If cboTP_Lancamento.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_tp_lancamento(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboTP_Lancamento.AddItem ss!lancamento
11                ss.MoveNext
12            Next
13        End If
End Sub

Private Sub cboTp_Pagto_Avulso_DropDown()
          Dim i As Long

1         If cboTp_Pagto_Avulso.ListCount = 0 Then
2             V_SQL = "Begin producao.PCK_VDA860.pr_consulta_tp_avulso(:vCursor,:vErro);END;"


3             oradatabase.ExecuteSQL V_SQL

4             Set ss = oradatabase.Parameters("vCursor").Value

5             If oradatabase.Parameters("vErro").Value <> 0 Then
6                 Call Process_Line_Errors(SQL)
7                 Exit Sub
8             End If

9             For i = 1 To ss.RecordCount
10                cboTp_Pagto_Avulso.AddItem ss!tp_pagto
11                ss.MoveNext
12            Next
13        End If

End Sub

Private Sub cmdAdiantamento_Bradesco_Click()
1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If

8         Call PR_ARQ_ADIANTAMENTO_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco)

End Sub

Private Sub cmdAdiantamento_Click()
1         If cboAnoMes_Arq = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtDt_Pagamento = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_ADIANTAMENTO(cboAnoMes_Arq, txtDt_Pagamento)

End Sub

Private Sub cmdAvulso_Bradesco_Click()
1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_AVULSO_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco)
End Sub

Private Sub cmdAvulso_Click()
1         If cboAnoMes_Arq = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtDt_Pagamento = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_AVULSO(cboAnoMes_Arq, txtDt_Pagamento)

End Sub

Private Sub cmdContab_Click()
1         If cboAnoMes_Contab = "" Then
2             MsgBox "Selecione um per�odo para gerar a contabiliza��o", vbInformation, "Aten��o"
3             Exit Sub
4         End If

          'MOSTRA STATUS DO FECHAMENTO
          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboAnoMes_Contab, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If (ss.EOF And ss.BOF) Or ss!fl_status = 0 Or ss!fl_status = 1 Then
15            MsgBox "O fechamento ainda n�o foi encerrado, portanto n�o � poss�vel gerar a contabiliza��o", vbCritical, "Aten��o"
16            Exit Sub
17        End If



18        Call PR_ARQ_CONTABIL_MES(Trim(frmComis.cboAnoMes_Contab))
19        Call PR_ARQ_CONTABIL_MES_AVULSO(Trim(frmComis.cboAnoMes_Contab))

20        MsgBox "ARQUIVOS GERADOS COM SUCESSO", vbInformation, "Aten��o"

End Sub

Private Sub cmdContabil_Click()
1         If cboAno_MesRC = "" Then
2             MsgBox "Selecione um per�odo para gerar o relat�rio cont�bil", vbInformation, "Aten��o"
3             Exit Sub
4         End If
5         Call PR_REL_CONTAB(Trim(cboAno_MesRC))
6         Call PR_REL_CONTAB_PF(Trim(cboAno_MesRC))
7         MsgBox "ARQUIVOS GERADOS COM SUCESSO", vbInformation, "Aten��o"
End Sub

Private Sub cmdDP_Click()
1         If cboAno_MesDP = "" Then
2             MsgBox "Selecione um per�odo para gerar a contabiliza��o", vbInformation, "Aten��o"
3             Exit Sub
4         End If
5         Call PR_ARQ_DP(Trim(frmComis.cboAno_MesDP))
6         MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"
End Sub

Private Sub cmdDP_Promo_Click()
1         If cboAno_MesDP = "" Then
2             MsgBox "Selecione um per�odo para gerar a contabiliza��o", vbInformation, "Aten��o"
3             Exit Sub
4         End If
5         Call PR_ARQ_DP_PROMO(Trim(frmComis.cboAno_MesDP))
6         MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"
End Sub

Private Sub cmdEncerra_Bradesco_Click()

1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o m�s para encerrar o fechamento", vbInformation, "Aten��o"
3             Exit Sub
4         End If


          'MOSTRA STATUS DO FECHAMENTO
          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboAno_mes_Bradesco, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss!fl_status = 2 Then
15            MsgBox "O fechamento j� foi encerrado", vbCritical, "Aten��o"
16            Exit Sub
17        End If

18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", cboAno_mes_Bradesco, 1


20        V_SQL = "Begin producao.pck_COMISSAO_APURACAO.PR_IMPOSTO_RENDA;END;"


21        oradatabase.ExecuteSQL V_SQL


22        OraParameters.Remove "ano_mes"
23        OraParameters.Add "ano_mes", cboAno_mes_Bradesco, 1


24        V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"


25        oradatabase.ExecuteSQL V_SQL

26        MsgBox "Fechamento Encerrado", vbInformation, "Aten��o"

End Sub

Private Sub cmdEncerra_Safra_Click()

1         If cboAnoMes_Arq = "" Then
2             MsgBox "Selecione o m�s para encerrar o fechamento", vbInformation, "Aten��o"
3             Exit Sub
4         End If

          'MOSTRA STATUS DO FECHAMENTO
          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboAnoMes_Arq, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss!fl_status = 2 Then
15            MsgBox "O fechamento j� foi encerrado", vbCritical, "Aten��o"
16            Exit Sub
17        End If


18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", cboAnoMes_Arq, 1


20        V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"

21        oradatabase.ExecuteSQL V_SQL

22        MsgBox "Fechamento Encerrado", vbInformation, "Aten��o"


End Sub

Private Sub cmdFech_Cota_Click()
          Dim Response As String

1         If cboAnoMes_Fech = "" Then
2             MsgBox "Selecione o ano e m�s para executar o recalculo", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         Response = MsgBox("Tem certeza que deseja recalcular os premios de fechamento de cota ", vbYesNo + vbDefaultButton2, "ATEN��O")

6         If Response = vbYes Then

7             Screen.MousePointer = 11
              'verifica status do fechamento
8             OraParameters.Remove "ano_mes"
9             OraParameters.Add "ano_mes", cboAnoMes_Fech, 1
10            V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

11            oradatabase.ExecuteSQL V_SQL

12            Set ss = oradatabase.Parameters("vCursor").Value

13            If oradatabase.Parameters("vErro").Value <> 0 Then
14                Call Process_Line_Errors(SQL)
15                Exit Sub
16            End If

17            If ss!fl_status = 2 Then
18                Screen.MousePointer = 0
19                MsgBox "Fechamento j� encerrado n�o � permitido alterar valores", vbInformation, "Aten��o"
20                Exit Sub
21            End If

22            V_SQL = "Begin producao.pck_comissao_apuracao.pr_fech_cota;END;"

23            oradatabase.ExecuteSQL V_SQL

24            Set ss = oradatabase.Parameters("vCursor").Value

25            If oradatabase.Parameters("vErro").Value <> 0 Then
26                Call Process_Line_Errors(SQL)
27                Exit Sub
28            End If


29            V_SQL = "Begin producao.pck_comissao_apuracao.pr_imposto_renda;END;"

30            oradatabase.ExecuteSQL V_SQL

31            Set ss = oradatabase.Parameters("vCursor").Value

32            If oradatabase.Parameters("vErro").Value <> 0 Then
33                Call Process_Line_Errors(SQL)
34                Exit Sub
35            End If

36            V_SQL = "Begin producao.pck_comissao_apuracao.PR_FECH_COTA_PROMOTOR;END;"

37            oradatabase.ExecuteSQL V_SQL

38            Set ss = oradatabase.Parameters("vCursor").Value

39            If oradatabase.Parameters("vErro").Value <> 0 Then
40                Call Process_Line_Errors(SQL)
41                Exit Sub
42            End If
              
              'TI-5720
43            V_SQL = "Begin producao.pck_comissao_apuracao.PR_FECH_COTA_REPRES;END;"

44            oradatabase.ExecuteSQL V_SQL

45            Set ss = oradatabase.Parameters("vCursor").Value

46            If oradatabase.Parameters("vErro").Value <> 0 Then
47                Call Process_Line_Errors(SQL)
48                Exit Sub
49            End If
              'FIM TI-5720
              
50            Screen.MousePointer = 0
51            MsgBox "Recalculo encerrado com sucesso!", vbInformation, "Aten��o"
52        End If
End Sub

Private Sub cmdFIS_PFJ_Click()
1         If cboAno_MesPFJ = "" Then
2             MsgBox "Selecione um per�odo para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         End If
5         Call PR_ARQ_PFJ(Trim(cboAno_MesPFJ))
6         MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"
End Sub

Private Sub cmdGera_Adiantamento_Click()
          Dim Response

1         If txtAinicio = "" Or txtAFim = "" Then
2             MsgBox "Para gerar o adiantamento � preciso entrar com as " & _
                   " duas datas", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         If txtAinicio > txtAFim Then
6             MsgBox "A data final n�o pode ser menor que a data inicio", vbInformation, "Aten��o"
7             Exit Sub
8         End If


9         Response = MsgBox("Deseja gerar o adiantamento ?", vbYesNo + vbDefaultButton2, "Aten��o")

10        If Response = vbYes Then

11            Screen.MousePointer = 11
12            OraParameters.Remove "ano_mes"
13            OraParameters.Add "ano_mes", "20" & Mid(txtAinicio, 7, 2) & Mid(txtAinicio, 4, 2), 1
14            V_SQL = "Begin producao.PCK_VDA860.pr_conta_adiantamento(:vCursor,:ano_mes,:vErro);END;"


15            oradatabase.ExecuteSQL V_SQL

16            Set ss = oradatabase.Parameters("vCursor").Value

17            If oradatabase.Parameters("vErro").Value <> 0 Then
18                Screen.MousePointer = 0
19                Call Process_Line_Errors(SQL)
20                Exit Sub
21            End If

22            If ss!total > 50 Then
23                Screen.MousePointer = 0
24                MsgBox "J� existe adiantamento gerado neste m�s, esta op��o n�o pode ser executada", vbCritical, "Aten��o"
25                Exit Sub
26            End If

27            OraParameters.Remove "inicio"
28            OraParameters.Add "inicio", CDate(txtAinicio), 1
29            OraParameters.Remove "fim"
30            OraParameters.Add "fim", CDate(txtAFim), 1
31            V_SQL = "Begin producao.pck_comissao_apuracao.pr_adiantamento(:inicio,:fim);END;"

32            oradatabase.ExecuteSQL V_SQL

33            Set ss = oradatabase.Parameters("vCursor").Value

34            If oradatabase.Parameters("vErro").Value <> 0 Then
35                Screen.MousePointer = 0
36                Call Process_Line_Errors(SQL)
37                Exit Sub
38            End If

39            MsgBox "Gera��o de adiantamento encerrada com sucesso"
40            Screen.MousePointer = 0
41        End If

End Sub

Private Sub cmdOK_Adiantamento_Click()
1         On Error GoTo TrataErro


          Dim ss As Object
          Dim i As Integer
          Dim dblTotal_Adianta As Double

2         If cboAno_Mes_Adiantamento = "" Then
3             MsgBox "Para fazer a consulta � necess�rio selecionar um ANO/MES", vbCritical, "Aten��o"
4             Exit Sub
5         End If

          'mouse
6         dblTotal_Adianta = 0
7         lblTotal_adianta = dblTotal_Adianta
8         Screen.MousePointer = vbHourglass
9         OraParameters.Remove "ano_mes"
10        OraParameters.Add "ano_mes", cboAno_Mes_Adiantamento, 1

          'montar SQL
11        V_SQL = "Begin producao.PCK_VDA860.pr_lista_adiantamento(:vCursor,:ano_mes,:vErro);END;"

12        oradatabase.ExecuteSQL V_SQL

13        Set ss = oradatabase.Parameters("vCursor").Value

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        If ss.EOF And ss.BOF Then
19            Screen.MousePointer = vbDefault
20            MsgBox "N�o ha adiantamento de comiss�o calculado neste m�s", vbInformation, "Aten��o"
21            grdAdiantamento.Visible = False
22            Exit Sub
23        End If

          'carrega dados
24        With frmComis.grdAdiantamento
25            .Cols = 6
26            .Rows = ss.RecordCount + 1
27            .ColWidth(0) = 660
28            .ColWidth(1) = 1500
29            .ColWidth(2) = 1500
30            .ColWidth(3) = 1000
31            .ColWidth(4) = 800
32            .ColWidth(5) = 1200

33            .Row = 0
34            .Col = 0
35            .Text = "C�d."
36            .Col = 1
37            .Text = "Repres."
38            .Col = 2
39            .Text = "Vl.Adiantamento"
40            .Col = 3
41            .Text = "Vl.Ad.Avulso"
42            .Col = 4
43            .Text = "Tipo"
44            .Col = 5
45            .Text = "Dt.Desligamento"

46            For i = 1 To .Rows - 1
47                .Row = i

48                .Col = 0
49                .Text = ss("COD_REPRES")
50                .Col = 1
51                .Text = ss("PSEUDONIMO")
52                .Col = 2
53                .ColAlignment(2) = 1
54                .Text = Format(ss("adiantamento"), "0.00")
55                dblTotal_Adianta = CDbl(dblTotal_Adianta) + IIf(IsNull(ss!adiantamento), 0, ss!adiantamento)
56                .Col = 3
57                .ColAlignment(3) = 1
58                .Text = Format(ss("pagto_avulso"), "0.00")
59                .Col = 4
60                .Text = ss("tipo")
61                .Col = 5
62                .Text = IIf(IsNull(ss("dt_desligamento")), "", ss("dt_desligamento"))

63                ss.MoveNext
64            Next
65            .Row = 1
66        End With

67        lblTotal_adianta = Format(dblTotal_Adianta, "0.00")
68        frmComis.grdAdiantamento.Visible = True
          'mouse
69        Screen.MousePointer = vbDefault

70        Exit Sub

TrataErro:
71        If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
72            Resume
73        Else
74            Call Process_Line_Errors(SQL)
75        End If

End Sub

Private Sub cmdOK_Click()
          Dim ss As Object
          Dim i As Long

1         If cboMes = "" Then
2             MsgBox "Para efetuar a consulta � preciso selecionar o m�s", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         Screen.MousePointer = 11
          'total de venda
6         OraParameters.Remove "ano_mes"
7         OraParameters.Add "ano_mes", cboMes, 1
8         V_SQL = "Begin producao.PCK_VDA860.pr_total_venda(:vCursor,:ano_mes,:vErro);END;"


9         oradatabase.ExecuteSQL V_SQL

10        Set ss = oradatabase.Parameters("vCursor").Value

11        If oradatabase.Parameters("vErro").Value <> 0 Then
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If
15        lblVenda.Caption = ss!total
16        Label2.Visible = True
17        lblVenda.Visible = True
18        picVenda.Visible = True
19        DoEvents

          '=================
          'total de quota
20        OraParameters.Remove "ano_mes"
21        OraParameters.Add "ano_mes", cboMes, 1
22        V_SQL = "Begin producao.PCK_VDA860.pr_total_quota(:vCursor,:ano_mes,:vErro);END;"


23        oradatabase.ExecuteSQL V_SQL

24        Set ss = oradatabase.Parameters("vCursor").Value

25        If oradatabase.Parameters("vErro").Value <> 0 Then
26            Call Process_Line_Errors(SQL)
27            Exit Sub
28        End If
29        lblQuota.Caption = ss!total
30        Label3.Visible = True
31        lblQuota.Visible = True
32        picQuota.Visible = True
33        DoEvents
          '=================
          'total de comissao
34        OraParameters.Remove "ano_mes"
35        OraParameters.Add "ano_mes", cboMes, 1
36        V_SQL = "Begin producao.PCK_VDA860.pr_total_comissao(:vCursor,:ano_mes,:vErro);END;"


37        oradatabase.ExecuteSQL V_SQL

38        Set ss = oradatabase.Parameters("vCursor").Value

39        If oradatabase.Parameters("vErro").Value <> 0 Then
40            Call Process_Line_Errors(SQL)
41            Exit Sub
42        End If
43        lblComissao.Caption = ss!total
44        Label4.Visible = True
45        lblComissao.Visible = True
46        picComissao.Visible = True
47        DoEvents
          '=================
          'total de adiantamento
48        OraParameters.Remove "ano_mes"
49        OraParameters.Add "ano_mes", cboMes, 1
50        V_SQL = "Begin producao.PCK_VDA860.pr_total_adiantamento(:vCursor,:ano_mes,:vErro);END;"


51        oradatabase.ExecuteSQL V_SQL

52        Set ss = oradatabase.Parameters("vCursor").Value

53        If oradatabase.Parameters("vErro").Value <> 0 Then
54            Call Process_Line_Errors(SQL)
55            Exit Sub
56        End If
57        lblAdiantamento.Caption = ss!total
58        Label5.Visible = True
59        lblAdiantamento.Visible = True
60        picAdiantamento.Visible = True
61        DoEvents
          '=================
          'total de lancamento
62        OraParameters.Remove "ano_mes"
63        OraParameters.Add "ano_mes", cboMes, 1
64        V_SQL = "Begin producao.PCK_VDA860.pr_total_lancamento(:vCursor,:ano_mes,:vErro);END;"


65        oradatabase.ExecuteSQL V_SQL

66        Set ss = oradatabase.Parameters("vCursor").Value

67        If oradatabase.Parameters("vErro").Value <> 0 Then
68            Call Process_Line_Errors(SQL)
69            Exit Sub
70        End If
71        lblLancamento.Caption = ss!total
72        Label6.Visible = True
73        lblLancamento.Visible = True
74        picLancamento.Visible = True
75        DoEvents
          '=================
          'total de alteracao
76        OraParameters.Remove "ano_mes"
77        OraParameters.Add "ano_mes", cboMes, 1
78        V_SQL = "Begin producao.PCK_VDA860.pr_total_alteracao(:vCursor,:ano_mes,:vErro);END;"


79        oradatabase.ExecuteSQL V_SQL

80        Set ss = oradatabase.Parameters("vCursor").Value

81        If oradatabase.Parameters("vErro").Value <> 0 Then
82            Call Process_Line_Errors(SQL)
83            Exit Sub
84        End If
85        lblAlteracao.Caption = ss!total
86        Label7.Visible = True
87        lblAlteracao.Visible = True
88        picAlteracao.Visible = True
89        DoEvents

          'MOSTRA STATUS DO FECHAMENTO
          'verifica status do fechamento
90        OraParameters.Remove "ano_mes"
91        OraParameters.Add "ano_mes", cboMes, 1
92        V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

93        oradatabase.ExecuteSQL V_SQL

94        Set ss = oradatabase.Parameters("vCursor").Value

95        If oradatabase.Parameters("vErro").Value <> 0 Then
96            Call Process_Line_Errors(SQL)
97            Exit Sub
98        End If
99        If (ss.EOF And ss.BOF) Or ss!fl_status = 0 Or ss!fl_status = 1 Then
100           lblStatus.Caption = "FECHAMENTO EM ABERTO"
101       Else
102           lblStatus.Caption = "FECHAMENTO ENCERRADO"
103       End If

104       lblStatus.Visible = True
105       DoEvents
106       Screen.MousePointer = 0
End Sub

Private Sub Combo1_Change()

End Sub

Private Sub cmdOK_Resumo_Click()
1         If cboAno_MesResumo = "" Then
2             MsgBox "Selecione o per�odo", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         frmResumo.Show 1

End Sub

Private Sub cmdPgto_DtCancel_Click()
1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If

8         Call PR_ARQ_PGTO_REPRES_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco, 9)
End Sub

Private Sub cmdPgto_Promo_Bradesco_Click()
1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_PGTO_PROMO_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco)
End Sub

Private Sub cmdPgto_Repres_Bradesco_Click()

1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If

8         Call PR_ARQ_PGTO_REPRES_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco, 0)

End Sub

Private Sub cmdPgto_Repres_Click()
1         If cboAnoMes_Arq = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtDt_Pagamento = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_PGTO_REPRES(cboAnoMes_Arq, txtDt_Pagamento)

End Sub

Private Sub cmdPgto_tlmk_Bradesco_Click()
1         If cboAno_mes_Bradesco = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_PGTO_TLMKT_BRADESCO(cboAno_mes_Bradesco, txtPagto_Bradesco)
End Sub

Private Sub cmdPgto_Tlmk_Click()
1         If cboAnoMes_Arq = "" Then
2             MsgBox "Selecione o Ano/Mes para gerar o arquivo", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtDt_Pagamento = "" Then
5             MsgBox "Informe o dia do pagamento para gerar o arquivo", vbInformation, "Aten��o"
6             Exit Sub

7         End If
8         Call PR_ARQ_PGTO_TLMKT(cboAnoMes_Arq, txtDt_Pagamento)

End Sub

Private Sub cmdPos_Emprestimo_Click()
1         If cboPos = "" Then
2             MsgBox "Selecione um per�odo para gerar o relat�rio de posi��o de empr�stimo", vbInformation, "Aten��o"
3             Exit Sub
4         End If

          'MOSTRA STATUS DO FECHAMENTO
          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboPos, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If (ss.EOF And ss.BOF) Or ss!fl_status = 0 Or ss!fl_status = 1 Then
15            MsgBox "O fechamento ainda n�o foi encerrado, portanto n�o � poss�vel gerar posi��o de empr�stimo", vbCritical, "Aten��o"
16            Exit Sub
17        End If

18        Call PR_REL_POSICAO_EMP(Trim(cboPos))

End Sub

Private Sub cmdSem_Pagto_Click()
1         If cboAno_MesRC = "" Then
2             MsgBox "Selecione um per�odo para gerar o relat�rio cont�bil", vbInformation, "Aten��o"
3             Exit Sub
4         End If
5         Call PR_REL_CONTAB_Sem(Trim(cboAno_MesRC))
6         Call PR_REL_CONTAB_PF_Sem(Trim(cboAno_MesRC))
7         MsgBox "ARQUIVOS GERADOS COM SUCESSO", vbInformation, "Aten��o"
End Sub

Private Sub cmdVerifica_Click()
1         On Error GoTo TrataErro


          Dim ss As Object
          Dim i As Integer

2         If cbo_bloq_anomes = "" Then
3             MsgBox "Para fazer a consulta � necess�rio selecionar um ANO/MES", vbCritical, "Aten��o"
4             Exit Sub
5         End If

          'mouse
6         Screen.MousePointer = vbHourglass
7         OraParameters.Remove "ano_mes"
8         OraParameters.Add "ano_mes", cbo_bloq_anomes, 1

          'montar SQL
9         V_SQL = "Begin producao.PCK_VDA860.pr_bloqueio_comissao(:vCursor,:ano_mes,:vErro);END;"

10        oradatabase.ExecuteSQL V_SQL

11        Set ss = oradatabase.Parameters("vCursor").Value

12        If oradatabase.Parameters("vErro").Value <> 0 Then
13            Call Process_Line_Errors(SQL)
14            Exit Sub
15        End If

16        If ss.EOF And ss.BOF Then
17            Screen.MousePointer = vbDefault
18            MsgBox "N�o ha bloqueio de comiss�o neste m�s", vbInformation, "Aten��o"
19            Exit Sub
20        End If

          'carrega dados
21        With frmComis.grdBloqueio
22            .Cols = 5
23            .Rows = ss.RecordCount + 1
24            .ColWidth(0) = 660
25            .ColWidth(1) = 1500
26            .ColWidth(2) = 1000
27            .ColWidth(3) = 1000
28            .ColWidth(4) = 1000

29            .Row = 0
30            .Col = 0
31            .Text = "C�d."
32            .Col = 1
33            .Text = "Repres."
34            .Col = 2
35            .Text = "Blq.NF"
36            .Col = 3
37            .Text = "Blq.Docto"
38            .Col = 4
39            .Text = "Dt.Desl."

40            For i = 1 To .Rows - 1
41                .Row = i

42                .Col = 0
43                .Text = ss("COD_REPRES")
44                .Col = 1
45                .Text = ss("PSEUDONIMO")
46                .Col = 2
47                .Text = ss("bloq_nf")
48                .Col = 3
49                .Text = ss("bloq_docto")
50                .Col = 4
51                .Text = IIf(IsNull(ss("dt_desligamento")), "", ss("dt_desligamento"))

52                ss.MoveNext
53            Next
54            .Row = 1
55        End With

56        frmComis.grdBloqueio.Visible = True
          'mouse
57        Screen.MousePointer = vbDefault

58        Exit Sub

TrataErro:
59        If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then
60            Resume
61        Else
62            Call Process_Line_Errors(SQL)
63        End If
End Sub

Private Sub Command1_Click()
1         Call PR_ARQ_CONTROLADORIA
End Sub

Private Sub Command2_Click()
1         If cboPos = "" Then
2             MsgBox "Selecione um per�odo para gerar o relat�rio de posi��o de empr�stimo", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         Call PR_REL_POSICAO_EMP_CANCEL(Trim(cboPos))

End Sub

Private Sub Form_Load()
          Dim ss As Object
          Dim i As Long

1         V_SQL = "Begin producao.PCK_VDA860.pr_mes(:vCursor,:vErro);END;"


2         oradatabase.ExecuteSQL V_SQL

3         Set ss = oradatabase.Parameters("vCursor").Value

4         If oradatabase.Parameters("vErro").Value <> 0 Then
5             Call Process_Line_Errors(SQL)
6             Exit Sub
7         End If

8         For i = 1 To ss.RecordCount
9             cboMes.AddItem ss!ano_mes
10            ss.MoveNext
11        Next

12        V_SQL = "Begin producao.PCK_VDA860.PR_DATA_ADIANTAMENTO(:vCursor,:vErro);END;"


13        oradatabase.ExecuteSQL V_SQL

14        Set ss = oradatabase.Parameters("vCursor").Value

15        If oradatabase.Parameters("vErro").Value <> 0 Then
16            Call Process_Line_Errors(SQL)
17            Exit Sub
18        End If

19        If ss.EOF And ss.BOF Then
20            txtAinicio = ""
21            txtAFim = ""
22        Else
23            txtAinicio = ss!dt_ini
24            txtAFim = ss!dt_fim

25        End If

26        Screen.MousePointer = 0

End Sub

Private Sub grdAdiantamento_DblClick()
          Dim Response
          Dim dblVl_Novo As Double
          Dim dblVl_Ant As Double

1         If cboAno_Mes_Adiantamento = "" Then
2             MsgBox "Para fazer a exclus�o � necess�rio escolher o ANO-MES", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         Response = MsgBox("Deseja excluir o adiantamento ?", vbYesNo, "Aten��o")

6         If Response = vbYes Then

              'verifica status do fechamento
7             OraParameters.Remove "ano_mes"
8             OraParameters.Add "ano_mes", cboAno_Mes_Adiantamento, 1
9             V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

10            oradatabase.ExecuteSQL V_SQL

11            Set ss = oradatabase.Parameters("vCursor").Value

12            If oradatabase.Parameters("vErro").Value <> 0 Then
13                Call Process_Line_Errors(SQL)
14                Exit Sub
15            End If

16            If ss!fl_status = 2 Then
17                MsgBox "Fechamento j� encerrado n�o � permitido alterar valores", vbInformation, "Aten��o"
18                Exit Sub
19            End If


20            OraParameters.Remove "ano_mes"
21            OraParameters.Add "ano_mes", cboAno_Mes_Adiantamento, 1
22            grdAdiantamento.Col = 0
23            OraParameters.Remove "repres"
24            OraParameters.Add "repres", grdAdiantamento.Text, 1
25            OraParameters.Remove "tp"
26            OraParameters.Add "tp", 13, 1
27            grdAdiantamento.Col = 2
28            OraParameters.Remove "vl_atual"
29            OraParameters.Add "vl_atual", grdAdiantamento.Text, 1
30            dblVl_Ant = grdAdiantamento.Text
31            grdAdiantamento.Col = 3
32            dblVl_Novo = (dblVl_Ant + grdAdiantamento.Text) - dblVl_Ant

33            OraParameters.Remove "vl_novo"
34            OraParameters.Add "vl_novo", dblVl_Novo, 1

35            OraParameters.Remove "repres_l"
36            OraParameters.Add "repres_l", 0, 1
37            OraParameters.Remove "vl_atual_l"
38            OraParameters.Add "vl_atual_l", 0, 1
39            OraParameters.Remove "user"
40            OraParameters.Add "user", strUsuario, 1

41            V_SQL = "Begin producao.PCK_VDA860.pr_insere_alteracao(:ano_mes,:repres,:tp,:vl_atual,:vl_novo,:repres_l,:vl_atual_l,:user,:vErro);END;"

42            oradatabase.ExecuteSQL V_SQL

43            If oradatabase.Parameters("vErro").Value <> 0 Then
44                Call Process_Line_Errors(SQL)
45                Exit Sub
46            Else
47                MsgBox "Exclus�o efetuada com sucesso", vbInformation, "Aten��o"
48                cmdOK_Adiantamento_Click
49            End If


50        End If

End Sub

Private Sub grdBloqueio_DblClick()
          Dim Resposta As String

1         grdBloqueio.Col = 0
2         strResposta = grdBloqueio.Text
3         Resposta = MsgBox("LIBERA BLOQUEIO  DE COMISS�O?", vbYesNo, "ATEN��O")

4         If Resposta = vbYes Then
5             OraParameters.Remove "ano_mes"
6             OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
7             OraParameters.Remove "repres"
8             OraParameters.Add "repres", strResposta, 1

9             V_SQL = "Begin producao.PCK_VDA860.pr_libera_bloqueio(:ano_mes,:repres,:vErro);END;"

10            oradatabase.ExecuteSQL V_SQL

11            If oradatabase.Parameters("vErro").Value <> 0 Then
12                Call Process_Line_Errors(SQL)
13                Exit Sub
14            Else
15                grdBloqueio.Visible = False
16                Call cmdVerifica_Click
17            End If
18        End If

End Sub

Private Sub grdBloqueio_KeyDown(KeyCode As Integer, Shift As Integer)
          Dim lngCod_Repres As Long
          Dim i As Long
          Dim strMotivo As String

1         If KeyCode = &H78 Then
2             frmMotivo.Label3.Visible = False
3             frmMotivo.grdDocto.Visible = False
4             frmMotivo.Label1.Visible = False
5             frmMotivo.lblMes1.Visible = False
6             frmMotivo.lblMes2.Visible = False

7             grdBloqueio.Col = 0
8             lngCod_Repres = frmComis.grdBloqueio.Text
9             frmMotivo.Caption = "MOTIVO DE BLOQUEIO REPRES.: " & frmComis.grdBloqueio.Text
10            grdBloqueio.Col = 1

11            frmMotivo.Caption = frmMotivo.Caption & " " & frmComis.grdBloqueio.Text

12            grdBloqueio.Col = 2

13            If grdBloqueio.Text <> "N" Then
14                OraParameters.Remove "ano_mes"
15                OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
16                OraParameters.Remove "repres"
17                OraParameters.Add "repres", lngCod_Repres, 1

                  'montar SQL
18                V_SQL = "Begin producao.PCK_VDA860.pr_consulta_bloqueio(:vCursor,:ano_mes,:repres,'N',:vErro);END;"

19                oradatabase.ExecuteSQL V_SQL

20                Set ss = oradatabase.Parameters("vCursor").Value

21                If ss.EOF And ss.BOF Then
22                    frmMotivo.Label1.Visible = True
23                    frmMotivo.lblMes1.Caption = ""
24                    frmMotivo.lblMes1.Visible = True
25                    frmMotivo.lblMes2.Caption = ""
26                    frmMotivo.lblMes2.Visible = True
27                End If
28                For i = 1 To ss.RecordCount
29                    If i = 1 Then
30                        frmMotivo.lblMes1 = ss!ano_mes
31                        frmMotivo.lblMes1.Visible = True
32                    Else
33                        frmMotivo.lblMes2 = ss!ano_mes
34                        frmMotivo.lblMes2.Visible = True
35                    End If
36                    ss.MoveNext
37                Next
38                frmMotivo.Label1.Visible = True
39                frmMotivo.lblMes1.Visible = True
40                frmMotivo.lblMes2.Visible = True
41            Else
42                frmMotivo.Label1.Visible = False
43                frmMotivo.lblMes1.Visible = False
44                frmMotivo.lblMes2.Visible = False
45            End If

46            grdBloqueio.Col = 3

47            If grdBloqueio.Text <> "N" Then
48                OraParameters.Remove "ano_mes"
49                OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
50                OraParameters.Remove "repres"
51                OraParameters.Add "repres", lngCod_Repres, 1

                  'montar SQL
52                V_SQL = "Begin producao.PCK_VDA860.pr_consulta_bloqueio(:vCursor,:ano_mes,:repres,'D',:vErro);END;"

53                oradatabase.ExecuteSQL V_SQL

54                Set ss = oradatabase.Parameters("vCursor").Value

55                If ss.EOF And ss.BOF Then
56                    frmMotivo.Label3.Visible = False
57                    frmMotivo.grdDocto.Visible = False
58                End If

59                With frmMotivo.grdDocto
60                    .Cols = 1
61                    .Rows = ss.RecordCount + 1
62                    .ColWidth(0) = 2500

63                    .Row = 0
64                    .Col = 0
65                    .Text = "DOCUMENTO"

66                    For i = 1 To .Rows - 1
67                        .Row = i

68                        .Col = 0
69                        .Text = ss("DESC_DOCTO")
70                        ss.MoveNext
71                    Next
72                    .Row = 1
73                End With
74                frmMotivo.grdDocto.Visible = True
75                frmMotivo.Label3.Visible = True
76            Else
77                frmMotivo.Label3.Visible = False
78                frmMotivo.grdDocto.Visible = False

79            End If


80            frmMotivo.Show 1
81        ElseIf KeyCode = vbKeyF2 Then
              Dim Resposta As String

82            grdBloqueio.Col = 0
83            strResposta = grdBloqueio.Text
84            Resposta = MsgBox("TEM CERTEZA QUE DESEJA BLOQUEAR  A COMISS�O DESTE REPRESENTANTE?", vbYesNo, "ATEN��O")

85            If Resposta = vbYes Then
86                OraParameters.Remove "ano_mes"
87                OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
88                OraParameters.Remove "repres"
89                OraParameters.Add "repres", strResposta, 1

90                V_SQL = "Begin producao.PCK_VDA860.pr_bloqueia_comis(:ano_mes,:repres,:vErro);END;"

91                oradatabase.ExecuteSQL V_SQL

92                If oradatabase.Parameters("vErro").Value <> 0 Then
93                    Call Process_Line_Errors(SQL)
94                    Exit Sub
95                Else
96                    grdBloqueio.Visible = False
97                    Call cmdVerifica_Click
98                End If
99            End If

100       ElseIf KeyCode = vbKeyF3 Then

101           grdBloqueio.Col = 0
102           strResposta = grdBloqueio.Text
103           Resposta = MsgBox("TEM CERTEZA QUE DESEJA LIBERAR A COMISS�O SEM EFETUAR PAGAMENTO PARA REPRESENTANTE?", vbYesNo, "ATEN��O")

104           If Resposta = vbYes Then
105               strMotivo = InputBox("INFORME O MOTIVO DO CANCELAMENTO", "MOTIVO")
106           Else
107               Exit Sub
108           End If
109           If strMotivo = "" Then

110               MsgBox "Para liberar uma comiss�o � preciso informar um motivo", vbCritical, "Aten��o"
111               Exit Sub
112           End If

113           OraParameters.Remove "ano_mes"
114           OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
115           OraParameters.Remove "repres"
116           OraParameters.Add "repres", strResposta, 1
117           OraParameters.Remove "motivo"
118           OraParameters.Add "motivo", UCase(strMotivo), 1

119           V_SQL = "Begin producao.PCK_VDA860.pr_nao_pagto(:ano_mes,:repres,:motivo,:vErro);END;"

120           oradatabase.ExecuteSQL V_SQL

121           If oradatabase.Parameters("vErro").Value <> 0 Then
122               Call Process_Line_Errors(SQL)
123               Exit Sub
124           Else
125               OraParameters.Remove "ano_mes"
126               OraParameters.Add "ano_mes", cbo_bloq_anomes, 1
127               OraParameters.Remove "repres"
128               OraParameters.Add "repres", strResposta, 1

129               V_SQL = "Begin producao.PCK_VDA860.pr_libera_bloqueio(:ano_mes,:repres,:vErro);END;"

130               oradatabase.ExecuteSQL V_SQL

131               grdBloqueio.Visible = False
132               Call cmdVerifica_Click
133           End If



134       End If
End Sub

Private Sub sscmdInc_Alteracao_Click()
          Dim dblVL_Atual_Lanc As Double

1         If txtCod_repres = "" Or _
             cboAno_mes = "" Or _
             cboAlteracao = "" Or _
             txtVL_Novo = "" Or _
             cboEscolha = "" Then
2             MsgBox "Para fazer a altera��o todos os campos devem sem preenchido", vbInformation, "Aten��o"
3             Exit Sub
4         End If

5         If Mid(Trim(cboEscolha), 1, 1) = "S" And _
             txtCod_Repres_Lanc = "" Then
6             MsgBox "Para gerar um novo lan�amento � necess�rio informar o representante", vbInformation, "Aten��o"
7             Exit Sub
8         End If

          'verifica status do fechamento
9         OraParameters.Remove "ano_mes"
10        OraParameters.Add "ano_mes", cboAno_mes, 1
11        V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

12        oradatabase.ExecuteSQL V_SQL

13        Set ss = oradatabase.Parameters("vCursor").Value

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        If ss!fl_status = 2 Then
19            MsgBox "Fechamento j� encerrado n�o � permitido alterar valores", vbInformation, "Aten��o"
20            Exit Sub
21        End If


22        OraParameters.Remove "ano_mes"
23        OraParameters.Add "ano_mes", cboAno_mes, 1
24        OraParameters.Remove "repres"
25        OraParameters.Add "repres", txtCod_repres, 1
26        OraParameters.Remove "tp"
27        OraParameters.Add "tp", Mid(Trim(cboAlteracao), 1, 3), 1
28        OraParameters.Remove "vl_atual"
29        OraParameters.Add "vl_atual", txtVL_Atual, 1
30        OraParameters.Remove "vl_novo"
31        OraParameters.Add "vl_novo", txtVL_Novo, 1
32        OraParameters.Remove "repres_l"
33        OraParameters.Add "repres_l", IIf(txtCod_Repres_Lanc = "", 0, txtCod_Repres_Lanc), 1
34        If IIf(txtCod_Repres_Lanc = "", 0, txtCod_Repres_Lanc) <> 0 Then
              'verifica valor atual
35            OraParameters.Remove "ano_mes"
36            OraParameters.Add "ano_mes", cboAno_mes, 1
37            OraParameters.Remove "repr"
38            OraParameters.Add "repr", txtCod_Repres_Lanc, 1
39            OraParameters.Remove "tp"
40            OraParameters.Add "tp", Mid(Trim(cboAlteracao), 1, 3), 1

41            V_SQL = "Begin producao.PCK_VDA860.pr_consulta_alteracao(:vCursor,:ano_mes,:repr,:tp,:vErro);END;"

42            oradatabase.ExecuteSQL V_SQL

43            Set ss = oradatabase.Parameters("vCursor").Value

44            If oradatabase.Parameters("vErro").Value <> 0 Then
45                Call Process_Line_Errors(SQL)
46                Exit Sub
47            End If
48            dblVL_Atual_Lanc = ss!total
49        End If
50        OraParameters.Remove "vl_atual_l"
51        OraParameters.Add "vl_atual_l", dblVL_Atual_Lanc, 1
52        OraParameters.Remove "user"
53        OraParameters.Add "user", strUsuario, 1

54        V_SQL = "Begin producao.PCK_VDA860.pr_insere_alteracao(:ano_mes,:repres,:tp,:vl_atual,:vl_novo,:repres_l,:vl_atual_l,:user,:vErro);END;"

55        oradatabase.ExecuteSQL V_SQL

56        If oradatabase.Parameters("vErro").Value <> 0 Then
57            Call Process_Line_Errors(SQL)
58            Exit Sub
59        Else
60            MsgBox "Altera��o efetuada com sucesso", vbInformation, "Aten��o"
61            txtCod_repres = ""
62            cboAno_mes = ""
63            cboAlteracao = ""
64            txtVL_Atual = ""
65            txtVL_Novo = ""
66            txtCod_Repres_Lanc = ""
67            dblVL_Atual_Lanc = 0
68            lblPseudo = ""
69            lblPseudo1 = ""
70            cboEscolha = ""
71        End If

End Sub

Private Sub sscmdInc_Alteracao_LostFocus()
1         txtCod_repres.SetFocus
End Sub

Private Sub sscmdIncluir_Avulso_Click()
          Dim strResposta As String
          Dim dblPagto_Sim As Double
          Dim dblPagto_Nao As Double
          Dim i As Long
          Dim lngSequencia As Long

1         If txtRepres_Avulso = "" Or _
             cboAno_Mes_Avulso = "" Or _
             cboTp_Pagto_Avulso = "" Or _
             txtPagto = "" Then
2             MsgBox "Para fazer a inclus�o � necess�rio preencher todos os campos", vbInformation, "Aten��o"
3             Exit Sub
4         End If


          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboAno_Mes_Avulso, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss!fl_status = 2 Then
15            MsgBox "Fechamento j� encerrado n�o � permitido incluir pagtos avulsos", vbInformation, "Aten��o"
16            Exit Sub
17        End If

18        dblPagto_Sim = 0
19        dblPagto_Nao = 0
          'verifica se lancamento ja foi feito
20        OraParameters.Remove "ano_mes"
21        OraParameters.Add "ano_mes", cboAno_Mes_Avulso, 1
22        OraParameters.Remove "repres"
23        OraParameters.Add "repres", txtRepres_Avulso, 1
24        OraParameters.Remove "tp"
25        OraParameters.Add "tp", Mid(Trim(cboTp_Pagto_Avulso), 1, 2), 1


26        V_SQL = "Begin producao.PCK_VDA860.pr_consulta_avulso_pago(:vCursor,:repres,:ano_mes,:tp,:vErro);END;"


27        oradatabase.ExecuteSQL V_SQL

28        Set ss = oradatabase.Parameters("vCursor").Value

29        If oradatabase.Parameters("vErro").Value <> 0 Then
30            Call Process_Line_Errors(SQL)
31            Exit Sub
32        End If

33        If ss.EOF And ss.BOF Then
34            dblPagto_Sim = 0
35        Else
36            dblPagto_Sim = IIf(IsNull(ss!vl_pagto), 0, ss!vl_pagto)
37        End If

38        If dblPagto_Sim <> 0 Then

39            MsgBox "Este representante j� recebeu este tipo de pagto avulso neste m�s no valor de : " & dblPagto_Sim, vbInformation, "Aten��o"
40        End If


41        dblPagto_Nao = 0
42        lngSequencia = 0
          'verifica se lancamento ja foi feito
43        OraParameters.Remove "ano_mes"
44        OraParameters.Add "ano_mes", cboAno_Mes_Avulso, 1
45        OraParameters.Remove "repres"
46        OraParameters.Add "repres", txtRepres_Avulso, 1
47        OraParameters.Remove "tp"
48        OraParameters.Add "tp", Mid(Trim(cboTp_Pagto_Avulso), 1, 2), 1


49        V_SQL = "Begin producao.PCK_VDA860.pr_consulta_avulso_aberto(:vCursor,:repres,:ano_mes,:tp,:vErro);END;"


50        oradatabase.ExecuteSQL V_SQL

51        Set ss = oradatabase.Parameters("vCursor").Value

52        If oradatabase.Parameters("vErro").Value <> 0 Then
53            Call Process_Line_Errors(SQL)
54            Exit Sub
55        End If

56        If ss.EOF And ss.BOF Then
57            dblPagto_Nao = 0
58        Else
59            dblPagto_Nao = IIf(IsNull(ss!vl_pagto), 0, ss!vl_pagto)
60            lngSequencia = IIf(IsNull(ss!sequencia), 0, ss!sequencia)
61        End If

62        If dblPagto_Nao <> 0 Then
63            MsgBox "Este representante possui este tipo de pagto avulso, ainda n�o pago, no valor de: " & dblPagto_Nao & ". Portanto ser� alterado.", vbInformation, "Aten��o"
64        End If

65        If lngSequencia = 0 Then
66            OraParameters.Remove "ano_mes"
67            OraParameters.Add "ano_mes", cboAno_Mes_Avulso, 1
68            OraParameters.Remove "repres"
69            OraParameters.Add "repres", txtRepres_Avulso, 1
70            OraParameters.Remove "tp"
71            OraParameters.Add "tp", Mid(Trim(cboTp_Pagto_Avulso), 1, 2), 1

72            V_SQL = "Begin producao.PCK_VDA860.pr_consulta_avulso_aberto(:vCursor,:repres,:ano_mes,:tp,:vErro);END;"


73            oradatabase.ExecuteSQL V_SQL

74            Set ss = oradatabase.Parameters("vCursor").Value

75            If oradatabase.Parameters("vErro").Value <> 0 Then
76                Call Process_Line_Errors(SQL)
77                Exit Sub
78            End If
79            lngSequencia = IIf(IsNull(ss!sequencia), 0, ss!sequencia) + 1
80        End If


81        OraParameters.Remove "ano_mes"
82        OraParameters.Add "ano_mes", cboAno_Mes_Avulso, 1
83        OraParameters.Remove "repres"
84        OraParameters.Add "repres", txtRepres_Avulso, 1
85        OraParameters.Remove "tp"
86        OraParameters.Add "tp", Mid(Trim(cboTp_Pagto_Avulso), 1, 2), 1
87        OraParameters.Remove "vl"
88        OraParameters.Add "vl", txtPagto, 1
89        OraParameters.Remove "seq"
90        OraParameters.Add "seq", (lngSequencia + 1), 1
91        OraParameters.Remove "vl_ant"
92        OraParameters.Add "vl_ant", dblPagto_Nao, 1
93        OraParameters.Remove "lanc"
94        OraParameters.Add "lanc", Mid(Trim(cboTp_Pagto_Avulso), 37, 2), 1


95        V_SQL = "Begin producao.PCK_VDA860.pr_insere_avulso(:repres,:ano_mes,:tp,:seq,:vl,:vl_ant,:lanc,:vErro);END;"


96        oradatabase.ExecuteSQL V_SQL

97        If oradatabase.Parameters("vErro").Value <> 0 Then
98            Call Process_Line_Errors(SQL)
99            Exit Sub
100       Else
101           MsgBox "Inclus�o efetuada com sucesso", vbInformation, "Aten��o"
102           txtRepres_Avulso = ""
103           cboAno_Mes_Avulso = ""
104           cboTp_Pagto_Avulso = ""
105           txtPagto = ""
106           lblPseudo_Avulso = ""
107       End If

End Sub

Private Sub sscmdIncluir_Click()
          Dim strResposta As String

1         If txtRepres = "" Or _
             cboAnoMes = "" Or _
             cboTP_Lancamento = "" Or _
             txtValor = "" Then
2             MsgBox "Para fazer a inclus�o � necess�rio preencher todos os campos", vbInformation, "Aten��o"
3             Exit Sub
4         End If


          'verifica status do fechamento
5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", cboAnoMes, 1
7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss!fl_status = 2 Then
15            MsgBox "Fechamento j� encerrado n�o � permitido alterar valores", vbInformation, "Aten��o"
16            Exit Sub
17        End If

          'verifica se lancamento ja foi feito
18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", cboAnoMes, 1
20        OraParameters.Remove "repres"
21        OraParameters.Add "repres", txtRepres, 1
22        OraParameters.Remove "tp"
23        OraParameters.Add "tp", Mid(Trim(cboTP_Lancamento), 1, 3), 1


24        V_SQL = "Begin producao.PCK_VDA860.pr_consulta_lancamento(:vCursor,:ano_mes,:repres,:tp,:vErro);END;"


25        oradatabase.ExecuteSQL V_SQL

26        Set ss = oradatabase.Parameters("vCursor").Value

27        If oradatabase.Parameters("vErro").Value <> 0 Then
28            Call Process_Line_Errors(SQL)
29            Exit Sub
30        End If

31        If Not ss.EOF And Not ss.BOF Then
32            strResposta = MsgBox("Lan�amento j� existe com valor de: " & Format(ss!vl_lancamento, "0.00") & ". Deseja alterar ?", vbYesNo, "Aten��o")
33            If strResposta = vbYes Then
grava:
34                OraParameters.Remove "ano_mes"
35                OraParameters.Add "ano_mes", cboAnoMes, 1
36                OraParameters.Remove "repres"
37                OraParameters.Add "repres", txtRepres, 1
38                OraParameters.Remove "tp"
39                OraParameters.Add "tp", Mid(Trim(cboTP_Lancamento), 1, 3), 1
40                OraParameters.Remove "vl"
41                OraParameters.Add "vl", txtValor, 1


42                V_SQL = "Begin producao.PCK_VDA860.pr_insere_lancamento(:ano_mes,:repres,:tp,:vl,:vErro);END;"


43                oradatabase.ExecuteSQL V_SQL

44                If oradatabase.Parameters("vErro").Value <> 0 Then
45                    Call Process_Line_Errors(SQL)
46                    Exit Sub
47                Else
48                    MsgBox "Inclus�o efetuada com sucesso", vbInformation, "Aten��o"
49                    txtRepres = ""
50                    cboAnoMes = ""
51                    cboTP_Lancamento = ""
52                    txtValor = ""
53                    lblPseudonimo = ""
54                End If

55            End If
56        Else
57            GoTo grava
58        End If

End Sub

Private Sub sscmdProcessa_Click()
          Dim Response As String
1         frmLayout.Show 1

2         Response = MsgBox("Deseja importar o arquivo ?", vbYesNo, "Aten��o")

3         If Response = vbYes Then
inicio:
4             If Dir(strPathArquivo & "lanc.con") = "" Then
5                 MsgBox "N�o h� registro para importar (" & strPathArquivo & "lanc.con)", vbInformation, "Aten��o"
6                 Exit Sub
7             Else
8                 Screen.MousePointer = 11
9                 Open strPathArquivo & "LANC.CON" For Input As #1
10                Processa_Arquivo
11                Close #1
12                Screen.MousePointer = 0
13                FileCopy strPathArquivo & "lanc.con", _
                           strPathArquivo & "salva\lanc.con"

14                Kill strPathArquivo & "lanc.con"
15                MsgBox "Arquivo processado ok", vbInformation, "Aten��o"
16            End If
17        End If

End Sub

Private Sub sscmdSair_Click()
    mFechar
End Sub

Private Sub sscmdVisualizar_Click()
1         On Error GoTo Trata_erro

2         If (txtRepres1 = "" And cboTipo_Pessoa = "") And _
             cboAno_Mes1 = "" Then
3             MsgBox "Para fazer a consulta � necess�rio preencher  os campos", vbInformation, "Aten��o"
4             Exit Sub
5         ElseIf txtRepres1 <> "" And cboAno_Mes1 <> "" Then
6             frmRecibo.Show 1
7         ElseIf cboTipo_Pessoa <> "" And cboAno_Mes1 <> "" Then
8             frmLista_Recibo.Show 1
9         End If

10        Exit Sub

Trata_erro:
11        If Err = 400 Or Err = 20 Then
12            Resume Next
13        End If


End Sub

Private Sub SSCommand1_Click()

1         Screen.MousePointer = 11

          Dim ss As Object
          Dim i As Integer


2         Screen.MousePointer = 0

3         If txtTlmk_Chapa = "" And txtChapa = "" Then
4             MsgBox "Para incluir � necess�rio preencher todos os campos", vbInformation, "Aten��o"
5             Exit Sub
6         End If
7         Screen.MousePointer = 11
8         OraParameters.Remove "repr"
9         OraParameters.Add "repr", txtTlmk_Chapa, 1
10        OraParameters.Remove "chapa"
11        OraParameters.Add "chapa", txtChapa, 1

12        V_SQL = "Begin producao.PCK_VDA860.pr_insere_chapa(:repr,:chapa,:vErro);END;"

13        oradatabase.ExecuteSQL V_SQL

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        Screen.MousePointer = 0
19        MsgBox "Inclus�o efetuada com sucesso", vbInformation, "Aten��o"

          'mouse
20        Screen.MousePointer = vbHourglass

          'montar SQL
21        V_SQL = "Begin producao.PCK_VDA860.pr_CONSULTA_CHAPA(:vCursor,:vErro);END;"

22        oradatabase.ExecuteSQL V_SQL

23        Set ss = oradatabase.Parameters("vCursor").Value

24        If oradatabase.Parameters("vErro").Value <> 0 Then
25            Call Process_Line_Errors(SQL)
26            Exit Sub
27        End If

28        If ss.EOF And ss.BOF Then
29            Screen.MousePointer = vbDefault
30            MsgBox "N�o ha chapa cadastrada", vbInformation, "Aten��o"
31            Unload Me
32            Exit Sub
33        End If

          'carrega dados
34        With frmComis.grdChapa
35            .Cols = 3
36            .Rows = ss.RecordCount + 1
37            .ColWidth(0) = 660
38            .ColWidth(1) = 1500
39            .ColWidth(2) = 1500

40            .Row = 0
41            .Col = 0
42            .Text = "C�digo"
43            .Col = 1
44            .Text = "Tlmkt"
45            .Col = 2
46            .Text = "Chapa"

47            For i = 1 To .Rows - 1
48                .Row = i

49                .Col = 0
50                .Text = ss("COD_REPRES")
51                .Col = 1
52                .Text = ss("PSEUDONIMO")
53                .Col = 2
54                .Text = IIf(IsNull(ss("chapa")), "", ss("chapa"))

55                ss.MoveNext
56            Next
57            .Row = 1
58        End With
59        grdChapa.Visible = True
60        Screen.MousePointer = 0

End Sub

Private Sub txtAFim_KeyPress(KeyAscii As Integer)
1         Call Data(KeyAscii, txtAinicio)
End Sub

Private Sub txtAinicio_KeyPress(KeyAscii As Integer)
1         Call Data(KeyAscii, txtAinicio)
End Sub

Private Sub txtChapa_KeyPress(KeyAscii As Integer)
1         KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtCod_repres_DblClick()
1         frmRepresentante.Show 1
2         txtCod_repres = strResposta
End Sub

Private Sub txtCod_repres_GotFocus()
1         If txtCod_repres <> "" Then
2             txtCod_repres_LostFocus
3         End If
End Sub

Private Sub txtCod_repres_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtCod_Repres_Lanc_DblClick()
1         frmRepresentante.Show 1
2         txtCod_Repres_Lanc = strResposta
End Sub

Private Sub txtCod_Repres_Lanc_GotFocus()
1         If txtCod_Repres_Lanc <> "" Then
2             txtCod_Repres_Lanc_LostFocus
3         End If
End Sub

Private Sub txtCod_Repres_Lanc_LostFocus()


1         If txtCod_Repres_Lanc = txtCod_repres Then
2             MsgBox "N�o � poss�vel gerar lan�amento para o mesmo representante", vbInformation, "Aten��o"
3             Exit Sub
4         End If


5         If txtCod_Repres_Lanc <> "" Then
6             OraParameters.Remove "repres"
7             OraParameters.Add "repres", txtCod_Repres_Lanc, 1
8             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


9             oradatabase.ExecuteSQL V_SQL

10            Set ss = oradatabase.Parameters("vCursor").Value

11            If oradatabase.Parameters("vErro").Value <> 0 Then
12                Call Process_Line_Errors(SQL)
13                Exit Sub
14            End If
15            If (ss!situacao <> 0) Or (ss.EOF And ss.BOF) Then
16                MsgBox "Representante desativado ou n�o cadastrado, n�o � poss�vel incluir lan�amento", vbInformation, "Aten��o"
17                txtCod_Repres_Lanc.Text = ""
18                lblPseudo1 = ""
19                Exit Sub
20            End If

21            lblPseudo1 = ss!PSEUDONIMO
22        Else
23            lblPseudo1 = ""
24        End If

End Sub

Private Sub txtCod_repres_LostFocus()
1         If txtCod_repres <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtCod_repres, 1
4             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


5             oradatabase.ExecuteSQL V_SQL

6             Set ss = oradatabase.Parameters("vCursor").Value

7             If oradatabase.Parameters("vErro").Value <> 0 Then
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If
11            If (ss.EOF And ss.BOF) Then
12                MsgBox "Representante n�o cadastrado, n�o � poss�vel incluir lan�amento", vbInformation, "Aten��o"
13                txtCod_repres.Text = ""
14                lblPseudo = ""
15                Exit Sub
16            End If

17            lblPseudo = ss!PSEUDONIMO
18        Else
19            lblPseudo = ""
20        End If

End Sub

Private Sub txtDt_Pagamento_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtDt_Pagamento_LostFocus()
1         If txtDt_Pagamento > 31 Then
2             MsgBox "Dia inv�lido por favor verifique", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtDt_Pagamento = "" Or txtDt_Pagamento = "00" Then
5             MsgBox "Dia inv�lido por favor verifique", vbInformation, "Aten��o"
6             Exit Sub
7         End If
8         txtDt_Pagamento = Format(txtDt_Pagamento, "00")
End Sub

Private Sub txtPagto_Bradesco_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtPagto_Bradesco_LostFocus()
1         If txtPagto_Bradesco > 31 Then
2             MsgBox "Dia inv�lido por favor verifique", vbInformation, "Aten��o"
3             Exit Sub
4         ElseIf txtPagto_Bradesco = "" Or txtPagto_Bradesco = "00" Then
5             MsgBox "Dia inv�lido por favor verifique", vbInformation, "Aten��o"
6             Exit Sub
7         End If
8         txtPagto_Bradesco = Format(txtPagto_Bradesco, "00")
End Sub

Private Sub txtPagto_KeyPress(KeyAscii As Integer)
1         KeyAscii = Valor(KeyAscii, txtPagto.Text)
End Sub

Private Sub txtPagto_LostFocus()
1         If txtPagto = "" Then
2             txtPagto = 0
3         End If
4         If CDbl(txtPagto) <= 0 Then
5             MsgBox "Valor inv�lido, por favor verifique", vbInformation, "Aten��o"
6             Exit Sub
7         End If
8         txtPagto = Format(FmtBR(txtPagto), "0.00")
End Sub

Private Sub txtRepres_Avulso_DblClick()
1         frmRepresentante.Show 1
2         txtRepres_Avulso.Text = strResposta
End Sub

Private Sub txtRepres_Avulso_GotFocus()
1         If txtRepres_Avulso <> "" Then
2             txtRepres_Avulso_LostFocus
3         End If
End Sub

Private Sub txtRepres_Avulso_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtRepres_Avulso_LostFocus()
1         If txtRepres_Avulso <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtRepres_Avulso, 1
4             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


5             oradatabase.ExecuteSQL V_SQL

6             Set ss = oradatabase.Parameters("vCursor").Value

7             If oradatabase.Parameters("vErro").Value <> 0 Then
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If
11            If (ss!situacao <> 0) Or (ss.EOF And ss.BOF) Then
12                MsgBox "Representante desativado ou n�o cadastrado, n�o � poss�vel incluir pgto avulso", vbInformation, "Aten��o"
13                txtRepres_Avulso.Text = ""
14                lblPseudo_Avulso = ""
15                Exit Sub
16            End If

17            lblPseudo_Avulso = ss!PSEUDONIMO
18        Else
19            lblPseudo_Avulso = ""
20        End If

End Sub

Private Sub txtRepres_DblClick()
1         frmRepresentante.Show 1
2         txtRepres.Text = strResposta
End Sub

Private Sub txtRepres_GotFocus()
1         If txtRepres <> "" Then
2             txtRepres_LostFocus
3         End If
End Sub

Private Sub txtRepres_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtRepres_LostFocus()

1         If txtRepres <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtRepres, 1
4             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


5             oradatabase.ExecuteSQL V_SQL

6             Set ss = oradatabase.Parameters("vCursor").Value

7             If oradatabase.Parameters("vErro").Value <> 0 Then
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If
11            If (ss.EOF And ss.BOF) Then
12                MsgBox "Representante n�o cadastrado, n�o � poss�vel incluir lan�amento", vbInformation, "Aten��o"
13                txtRepres.Text = ""
14                lblPseudonimo = ""
15                Exit Sub
16            End If

17            lblPseudonimo = ss!PSEUDONIMO
18        Else
19            lblPseudonimo = ""
20        End If

End Sub

Private Sub txtRepres1_DblClick()
1         frmRepresentante.Show 1
2         txtRepres1.Text = strResposta
End Sub

Private Sub txtRepres1_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtRepres1_LostFocus()

1         If txtRepres1 <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtRepres1, 1
4             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


5             oradatabase.ExecuteSQL V_SQL

6             Set ss = oradatabase.Parameters("vCursor").Value

7             If oradatabase.Parameters("vErro").Value <> 0 Then
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If
11            If ss.EOF And ss.BOF Then
12                MsgBox "Representante n�o cadastrado, n�o � poss�vel incluir lan�amento", vbInformation, "Aten��o"
13                txtRepres1.Text = ""
14                lblPseudo2 = ""
15                Exit Sub
16            End If
17            If ss!situacao = 9 Then
18                MsgBox "Representante Desativado", vbInformation, "Aten��o"
19            End If

20            lblPseudo2 = ss!PSEUDONIMO
21        Else
22            lblPseudo2 = ""
23        End If

End Sub

Private Sub txtTlmk_Chapa_DblClick()
1         frmRepresentante.Show 1
2         txtTlmk_Chapa = strResposta
End Sub

Private Sub txtTlmk_Chapa_GotFocus()
1         If txtTlmk_Chapa <> "" Then
2             txtTlmk_Chapa_LostFocus
3         End If
End Sub

Private Sub txtTlmk_Chapa_KeyPress(KeyAscii As Integer)
1         KeyAscii = Numerico(KeyAscii)
End Sub

Private Sub txtTlmk_Chapa_LostFocus()
1         If txtTlmk_Chapa <> "" Then
2             OraParameters.Remove "repres"
3             OraParameters.Add "repres", txtTlmk_Chapa, 1
4             V_SQL = "Begin producao.PCK_VDA860.pr_pseudonimo(:vCursor,:repres,:vErro);END;"


5             oradatabase.ExecuteSQL V_SQL

6             Set ss = oradatabase.Parameters("vCursor").Value

7             If oradatabase.Parameters("vErro").Value <> 0 Then
8                 Call Process_Line_Errors(SQL)
9                 Exit Sub
10            End If
11            If (ss!situacao <> 0) Or (ss.EOF And ss.BOF) Then
12                MsgBox "Representante desativado ou n�o cadastrado, n�o � poss�vel incluir lan�amento", vbInformation, "Aten��o"
13                txtTlmk_Chapa = ""
14                lblPseudo_Chapa = ""
15                Exit Sub
16            End If

17            lblPseudo_Chapa = ss!PSEUDONIMO
18        Else
19            lblPseudo_Chapa = ""
20        End If

End Sub

Private Sub txtValor_KeyPress(KeyAscii As Integer)
1         KeyAscii = Valor(KeyAscii, txtValor.Text)
End Sub

Private Sub txtValor_LostFocus()
1         If txtValor = "" Then
2             txtValor = 0
3         End If
4         If CDbl(txtValor) < 0 Then
5             MsgBox "Valor inv�lido, por favor verifique", vbInformation, "Aten��o"
6             Exit Sub
7         End If
8         txtValor = Format(FmtBR(txtValor), "0.00")
End Sub

Private Sub txtVL_Novo_KeyPress(KeyAscii As Integer)
1         KeyAscii = Valor(KeyAscii, txtVL_Novo.Text)
End Sub

Private Sub txtVL_Novo_LostFocus()
1         If txtVL_Novo <> "" Then
2             If CDbl(txtVL_Novo) < 0 Then
3                 MsgBox "Valor inv�lido, por favor verifique", vbInformation, "Aten��o"
4                 Exit Sub
5             End If
6             txtVL_Novo = Format(FmtBR(txtVL_Novo), "0.00")
7         End If
End Sub
