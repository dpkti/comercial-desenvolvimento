VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmRepresentante 
   Caption         =   "Lista de Representante"
   ClientHeight    =   3075
   ClientLeft      =   1260
   ClientTop       =   1965
   ClientWidth     =   6705
   ClipControls    =   0   'False
   ForeColor       =   &H00800000&
   Icon            =   "L_repr.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3075
   ScaleWidth      =   6705
   Begin VB.CommandButton cmdSair 
      Caption         =   "&Sair"
      Height          =   375
      Left            =   5400
      TabIndex        =   0
      Top             =   2640
      Width           =   1095
   End
   Begin MSGrid.Grid grdRepresentante 
      Height          =   2175
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   6375
      _Version        =   65536
      _ExtentX        =   11245
      _ExtentY        =   3836
      _StockProps     =   77
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FixedCols       =   0
      MouseIcon       =   "L_repr.frx":0442
   End
   Begin VB.Label lblPesq 
      AutoSize        =   -1  'True
      Caption         =   "Procurando por"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblPesquisa 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00C00000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   120
      Visible         =   0   'False
      Width           =   4935
   End
End
Attribute VB_Name = "frmRepresentante"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim strPesquisa As String

Private Sub cmdSair_Click()

1         Unload Me
2         Set frmRepresentante = Nothing
End Sub

Private Sub Form_Load()

1         On Error GoTo TrataErro


          Dim ss As Object
          Dim i As Integer

          'mouse
2         Screen.MousePointer = vbHourglass

          'montar SQL
3         V_SQL = "Begin producao.PCK_VDA860.pr_lista_Repr(:vCursor,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL

5         Set ss = oradatabase.Parameters("vCursor").Value

6         If oradatabase.Parameters("vErro").Value <> 0 Then
7             Call Process_Line_Errors(SQL)
8             Exit Sub
9         End If

10        If ss.EOF And ss.BOF Then
11            Screen.MousePointer = vbDefault
12            MsgBox "N�o ha representante cadastrado", vbInformation, "Aten��o"
13            Unload Me
14            Exit Sub
15        End If

          'carrega dados
16        With frmRepresentante.grdRepresentante
17            .Cols = 3
18            .Rows = ss.RecordCount + 1
19            .ColWidth(0) = 660
20            .ColWidth(1) = 5000
21            .ColWidth(2) = 400

22            .Row = 0
23            .Col = 0
24            .Text = "C�digo"
25            .Col = 1
26            .Text = "Nome do Representante"
27            .Col = 2
28            .Text = "Tipo"

29            For i = 1 To .Rows - 1
30                .Row = i

31                .Col = 0
32                .Text = ss("COD_REPRES")
33                .Col = 1
34                .Text = ss("PSEUDONIMO")
35                .Col = 2
36                .Text = ss("TIPO")

37                ss.MoveNext
38            Next
39            .Row = 1
40        End With


          'mouse
41        Screen.MousePointer = vbDefault

42        Exit Sub

TrataErro:
43        If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then

44            Resume
45        Else
46            Call Process_Line_Errors(SQL)
47        End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
1         Set frmRepresentante = Nothing
End Sub

Private Sub grdRepresentante_DblClick()
1         grdRepresentante.Col = 0
2         strResposta = grdRepresentante.Text
3         Unload Me
End Sub

