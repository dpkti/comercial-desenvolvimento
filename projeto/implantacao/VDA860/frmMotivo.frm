VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmMotivo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Motivo de Bloqueio"
   ClientHeight    =   3120
   ClientLeft      =   1095
   ClientTop       =   3720
   ClientWidth     =   6690
   Icon            =   "frmMotivo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3120
   ScaleWidth      =   6690
   Begin MSGrid.Grid grdDocto 
      Height          =   1575
      Left            =   3000
      TabIndex        =   4
      Top             =   1200
      Visible         =   0   'False
      Width           =   2775
      _Version        =   65536
      _ExtentX        =   4895
      _ExtentY        =   2778
      _StockProps     =   77
      ForeColor       =   8388608
      BackColor       =   16777215
   End
   Begin VB.Label Label3 
      Caption         =   "DOCUMENTOS FALTANTES"
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   360
      TabIndex        =   3
      Top             =   1080
      Visible         =   0   'False
      Width           =   2535
   End
   Begin VB.Label lblMes2 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3960
      TabIndex        =   2
      Top             =   480
      Width           =   855
   End
   Begin VB.Label lblMes1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   3000
      TabIndex        =   1
      Top             =   480
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "NOTA(s) FISCAL(IS) �LTIMOS 2 MESES"
      ForeColor       =   &H00800000&
      Height          =   495
      Left            =   360
      TabIndex        =   0
      Top             =   480
      Width           =   2655
   End
End
Attribute VB_Name = "frmMotivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

