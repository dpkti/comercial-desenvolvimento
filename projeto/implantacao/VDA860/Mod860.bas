Attribute VB_Name = "Module1"
Option Explicit
Public Nome_Arquivo_Pdf As String

'Eduardo - 14/02/2007
Public vStatus As String
Public vGerarRecibo As Boolean

Public Sub PR_ARQ_ADIANTAMENTO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         bSeq_Arquivo = 1

3         Screen.MousePointer = 11
verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                                Format(bSeq_Arquivo, "00") & "SPC" & ".txt"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
11        End If


          'montar SQL
12        OraParameters.Remove "seq"
13        OraParameters.Add "seq", 0, 2
14        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

15        oradatabase.ExecuteSQL V_SQL
16        Set ss = oradatabase.Parameters("vCursor").Value


17        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", lngAno_mes, 1
20        OraParameters.Remove "dia"
21        OraParameters.Add "dia", intDia, 1

          'montar SQL
22        V_SQL = "Begin producao.PCK_VDA860.pr_arq_adiantamento(:vCursor,:ano_mes,:dia,:vErro);END;"

23        oradatabase.ExecuteSQL V_SQL

24        Set ss = oradatabase.Parameters("vCursor").Value

25        If oradatabase.Parameters("vErro").Value <> 0 Then
26            Screen.MousePointer = 0
27            Call Process_Line_Errors(SQL)
28            Exit Sub
29        End If

30        If ss.EOF And ss.BOF Then
31            Screen.MousePointer = 0
32            MsgBox "N�o h� representantes dispon�veis para gerar Adiantamento", vbExclamation, "Aten��o"
33            Exit Sub
34        End If
35        bSeq_Registro = 1
36        dblTotal_Comissao = 0
37        Open strArquivo_Contas_Pagar For Output As #2
38        Print #2, "ARQUIVO DE COMISSAO - ADIANTAMENTO DPK "
39        Print #2,
40        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

41        Print #2,
42        Print #2, "COD."; Tab(10); "CGC/CIC"; _
                    Tab(30); "RAZ�O SOCIAL"; Tab(70); "VALOR PAGTO"; Tab(95); "DT.PAGTO"
43        Print #2,

44        Open strNome_Arquivo For Output As #1
45        Print #1, "0" & "1" & "REMESSA" & "11" & "PAGTOS FORNECED" & "01227507" & _
                    "N" & Space(2) & "0001000" & "  " & "COMERCIAL AUTOMOTIVA S.A.     " & "422" & _
                    "BANCO SAFRA S/A" & Format(Now, "DDMMYY") & Space(1) & Space(2) & Space(285) & _
                    Format(bSeq_Arquivo, "000000") & Format(bSeq_Registro, "000000")
46        lngTotal_Registros = ss.RecordCount
47        ReDim Repres(ss.RecordCount)

48        For i = 1 To ss.RecordCount

49            Repres(i) = ss!cod_repres
50            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
51            bSeq_Registro = bSeq_Registro + 1

52            Print #1, "1" & "01" & "45987005000198" & "01227507" & Space(3) & _
                        "0001000" & Space(2) & Space(25); Tab(63); Format(ss!cic_cgc, "00000000000000") & _
                        "REC" & Format(lngSeq_Repres_Banco, "0000000000") & "0" & Space(17) & "C" & _
                        "01" & Format(lngSeq_Repres_Banco, "0000000000") & Format(ss!dt_pagamento, "000000") & _
                        Format(ss!vl_comis, "0000000000000") & IIf((CDbl(ss!vl_comis) / 100) < 5000, "DOC", "TED") & _
                        "237" & Format(ss!cod_agenciA, "0000000") & "000" & _
                        Format(ss!conta_corrente, "0000000000") & Space(20) & "0001000" & Space(55) & _
                        "0000000000000000" & ss!razao_social; Tab(294); Space(54) & "0000000000000" & _
                        Format(ss!dt_pagamento, "000000") & Format(ss!vl_comis, "0000000000000") & "REAL" & _
                        Space(8) & ss!dig_agenciA & Space(2) & Format(bSeq_Registro, "000000")

53            Print #2, ss!cod_repres; Tab(10); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(30); ss!razao_social; Tab(70); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(95); Format(ss!dt_pagamento, "000000")

54            ss.MoveNext
55        Next
56        bSeq_Registro = bSeq_Registro + 1
57        Print #1, "9" & Space(123) & Format(dblTotal_Comissao, "000000000000000") & _
                    Space(109) & "000000000000000" & Space(82) & "000000000000000" & _
                    Space(4) & Format(dblTotal_Comissao, "000000000000000") & Space(15) & _
                    Format(bSeq_Registro, "000000")
58        Print #2,
59        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
60        Close #1
61        Close #2

62        For i = 1 To lngTotal_Registros
63            OraParameters.Remove "ano_mes"
64            OraParameters.Add "ano_mes", lngAno_mes, 1
65            OraParameters.Remove "repres"
66            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
67            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,1,:vErro);END;"

68            oradatabase.ExecuteSQL V_SQL

69            Set ss = oradatabase.Parameters("vCursor").Value
70        Next

71        Screen.MousePointer = 0
72        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_ADIANTAMENTO_BRADESCO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long
          Dim lngRazao As Long
          'NOVAS VARI�VEIS PARA TRATA CONTABILIZA��O
          Dim strArquivo_Contabil As String
          Dim strConta As String
          Dim lngDia_movimento As Long
          Dim dblTotal_Comissao_CP As Double
          Dim lngCC As String
          Dim dblVl_comis_CP As Double
          Dim lngSeq_pgto As Long
          Dim lngSeq_pgto_Bco As Long
          Dim strRazao As String
          Dim strEndereco As String


1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         bSeq_Arquivo = 1
3         lngSeq_pgto = 0

4         Screen.MousePointer = 11
verifica:
5         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
6         If strArquivo <> "" Then
7             bSeq_Arquivo = bSeq_Arquivo + 1
8             GoTo verifica
9         Else
              'alterado em 18/09
              'strNome_Arquivo = strPathArquivo & "FP" & Format(Now, "ddmm") & "D" & ".REM"
10            strNome_Arquivo = strPathArquivo & "PG" & Format(Now, "ddmm") & Format(bSeq_Arquivo, "00") & ".REM"

11            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
12        End If

          'CONTABILIZACAO
13        strArquivo_Contabil = strPathArquivo & "PGCOM" & Format(intDia, "00") & Format(Now, "mmyy") & "A.txt"
14        lngDia_movimento = Format(intDia, "00") & Format(Now, "MMYYYY")

          'montar SQL
15        OraParameters.Remove "seq"
16        OraParameters.Add "seq", 0, 2
17        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ_banco(:seq,:vErro);END;"

18        oradatabase.ExecuteSQL V_SQL
19        Set ss = oradatabase.Parameters("vCursor").Value


20        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


21        OraParameters.Remove "ano_mes"
22        OraParameters.Add "ano_mes", lngAno_mes, 1
23        OraParameters.Remove "dia"
24        OraParameters.Add "dia", intDia, 1

          'montar SQL
25        V_SQL = "Begin producao.PCK_VDA860.pr_arq_adiantamento(:vCursor,:ano_mes,:dia,:vErro);END;"

26        oradatabase.ExecuteSQL V_SQL

27        Set ss = oradatabase.Parameters("vCursor").Value

28        If oradatabase.Parameters("vErro").Value <> 0 Then
29            Screen.MousePointer = 0
30            Call Process_Line_Errors(SQL)
31            Exit Sub
32        End If

33        If ss.EOF And ss.BOF Then
34            Screen.MousePointer = 0
35            MsgBox "N�o h� representantes dispon�veis para gerar Adiantamento", vbExclamation, "Aten��o"
36            Exit Sub
37        End If
38        bSeq_Registro = 1
39        dblTotal_Comissao = 0

          'ARQUIVO CONTABILIZACAO
40        lngCC = "0"
41        Open strArquivo_Contabil For Output As #3


42        Open strArquivo_Contas_Pagar For Output As #2
43        Print #2, "ARQUIVO DE COMISSAO - ADIANTAMENTO DPK "
44        Print #2,
45        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

46        Print #2,
47        Print #2, "CC"; Tab(10); "COD."; Tab(20); "CGC/CIC"; _
                    Tab(40); "RAZ�O SOCIAL"; Tab(80); "VALOR PAGTO"; Tab(105); "DT.PAGTO"; _
                    Tab(115); "AG."; Tab(120); "CTA.C."; Tab(130); "DIG"


48        Print #2,
          'agencia bradesco alterada em 09/12/05 de 311 para 2002
49        Open strNome_Arquivo For Output As #1
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

          'PRODUCAO 12/09/07
          'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
          '===============================================
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

50        Print #1, "0" & "00043308" & "2" & "045987005000198" & "COMERCIAL AUTOMOTIVA S.A.               " & _
                    "20" & "1" & Format(lngSeq_Repres_Banco, "00000") & "00000" & Format(Now, "YYYYMMDD") & _
                    Format(Now, "HHMMSS") & Space(5) & Space(3) & Space(5) & Space(1) & Space(74) & Space(80) & _
                    Space(234) & Format(bSeq_Registro, "000000")



51        lngTotal_Registros = ss.RecordCount
52        ReDim Repres(ss.RecordCount)

53        For i = 1 To ss.RecordCount

54            Repres(i) = ss!cod_repres
55            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
56            bSeq_Registro = bSeq_Registro + 1
57            lngSeq_pgto = lngSeq_pgto + 1
              'montar SQL
58            OraParameters.Remove "seq"
59            OraParameters.Add "seq", 0, 2
60            V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

61            oradatabase.ExecuteSQL V_SQL
62            Set ss = oradatabase.Parameters("vCursor").Value


63            lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value

64            lngSeq_pgto_Bco = lngSeq_Repres_Banco

65            strRazao = ss!razao_social & Space(30 - Len(ss!razao_social))
66            strEndereco = ss!endereco & Space(40 - Len(ss!endereco))


              'INF.CONTABILIZA��O
67            If lngCC = "0" Then
68                lngCC = ss!cod_centro_custo
69                dblVl_comis_CP = ss!vl_comis_cp
70            Else
71                If lngCC <> ss!cod_centro_custo Then
                      'strConta = strConta & "9" & lngCC & Space(17)
72                    strConta = strConta & lngCC & Space(5) & _
                                 Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
73                    lngCC = ss!cod_centro_custo
74                    dblVl_comis_CP = ss!vl_comis_cp
75                Else
76                    dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
77                End If
78            End If

              'Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
               Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
               Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
               Space(6) & Space(44) & Format(bSeq_Registro, "000000")

              'producao 12 / 9 / 7
              'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
              '===============================================
              'Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
               Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
               Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
               Space(6) & Space(44) & Format(bSeq_Registro, "000000")

79            Print #1, "1" & "2" & Format(ss!cic_cgc, "000000000000000") & strRazao & strEndereco & _
                        Format(IIf(IsNull(ss!CEP), 0, ss!CEP), "00000000") & "237" & Format(ss!cod_agenciA, "00000") & Format(ss!dig_agenciA, "0") & _
                        Format(ss!conta_corrente, "0000000000000") & Format(ss!dig_ct_corrente, "0") & Space(1) & Format(lngSeq_pgto_Bco, "000000000000000") & "C" _
                      ; "000" & "000000000000" & Space(15) & "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        "00000000" & "00000000" & "0" & Space(4) & Space(10) & Format(ss!vl_comis, "000000000000000") & _
                        "000000000000000" & "000000000000000" & "05" & "0000000000" & Space(2) & "05" & _
                        "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        Space(3) & "01" & Space(10) & _
                        "0" & "00" & Space(4) & Space(15) & Space(15) & Space(6) & _
                        Space(40) & Space(1) & Space(1) & Space(40) & "00" & Space(35) & Space(22) & "00000" & Space(1) & _
                        "1" & "0051999" & Space(8) & Format(bSeq_Registro, "000000")




80            Print #2, ss!cod_centro_custo; Tab(10); ss!cod_repres; Tab(20); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(40); ss!razao_social; Tab(80); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(105); Format(ss!dt_pagamento, "000000"); _
                        Tab(115); ss!cod_agenciA; Tab(120); ss!conta_corrente; Tab(130); ss!dig_ct_corrente

81            ss.MoveNext
82        Next
          'strConta = strConta & "9" & lngCC & Space(17)
83        strConta = strConta & lngCC & Space(5) & _
                     Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")


84        bSeq_Registro = bSeq_Registro + 1
          ' Print #1, "9" & Format(dblTotal_Comissao, "0000000000000") & _
            Space(180) & Format(bSeq_Registro, "000000")
85        Print #1, "9" & Format(bSeq_Registro, "000000") & Format(dblTotal_Comissao, "00000000000000000") & _
                    Space(470) & Format(bSeq_Registro, "000000")

86        Print #2,
87        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")

88        dblTotal_Comissao_CP = dblTotal_Comissao / 100

          'CONTABILIZACAO
89        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21507000            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta

90        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000"); Tab(37); "11120805            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"

91        Close #1
92        Close #2
93        Close #3

94        For i = 1 To lngTotal_Registros
95            OraParameters.Remove "ano_mes"
96            OraParameters.Add "ano_mes", lngAno_mes, 1
97            OraParameters.Remove "repres"
98            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
99            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,1,:vErro);END;"

100           oradatabase.ExecuteSQL V_SQL

101           Set ss = oradatabase.Parameters("vCursor").Value
102       Next

103       Screen.MousePointer = 0
104       MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_AVULSO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1

verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                                Format(bSeq_Arquivo, "00") & "SPC" & ".txt"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
11        End If

          'montar SQL
12        OraParameters.Remove "seq"
13        OraParameters.Add "seq", 0, 2
14        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

15        oradatabase.ExecuteSQL V_SQL
16        Set ss = oradatabase.Parameters("vCursor").Value


17        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", lngAno_mes, 1
20        OraParameters.Remove "dia"
21        OraParameters.Add "dia", intDia, 1

          'montar SQL
22        V_SQL = "Begin producao.PCK_VDA860.pr_arq_avulso(:vCursor,:ano_mes,:dia,:vErro);END;"

23        oradatabase.ExecuteSQL V_SQL

24        Set ss = oradatabase.Parameters("vCursor").Value

25        If oradatabase.Parameters("vErro").Value <> 0 Then
26            Screen.MousePointer = 0
27            Call Process_Line_Errors(SQL)
28            Exit Sub
29        End If

30        If ss.EOF And ss.BOF Then
31            Screen.MousePointer = 0
32            MsgBox "N�o h� representantes dispon�veis para gerar pagto avulso", vbExclamation, "Aten��o"
33            Exit Sub
34        End If
35        bSeq_Registro = 1
36        dblTotal_Comissao = 0
37        Open strArquivo_Contas_Pagar For Output As #2
38        Print #2, "ARQUIVO DE COMISSAO - ADIANTAMENTO AVULSO DPK "
39        Print #2,
40        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

41        Print #2,
42        Print #2, "COD."; Tab(10); "CGC/CIC"; _
                    Tab(30); "RAZ�O SOCIAL"; Tab(70); "VALOR PAGTO"; Tab(95); "DT.PAGTO"
43        Print #2,

44        Open strNome_Arquivo For Output As #1
45        Print #1, "0" & "1" & "REMESSA" & "11" & "PAGTOS FORNECED" & "01227507" & _
                    "N" & Space(2) & "0001000" & "  " & "COMERCIAL AUTOMOTIVA S.A.     " & "422" & _
                    "BANCO SAFRA S/A" & Format(Now, "DDMMYY") & Space(1) & Space(2) & Space(285) & _
                    Format(bSeq_Arquivo, "000000") & Format(bSeq_Registro, "000000")

46        lngTotal_Registros = ss.RecordCount
47        ReDim Repres(ss.RecordCount)

48        For i = 1 To ss.RecordCount

49            Repres(i) = ss!cod_repres
50            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
51            bSeq_Registro = bSeq_Registro + 1

52            Print #1, "1" & "01" & "45987005000198" & "01227507" & Space(3) & _
                        "0001000" & Space(2) & Space(25); Tab(63); Format(ss!cic_cgc, "00000000000000") & _
                        "REC" & Format(lngSeq_Repres_Banco, "0000000000") & "0" & Space(17) & "C" & _
                        "01" & Format(lngSeq_Repres_Banco, "0000000000") & Format(ss!dt_pagamento, "000000") & _
                        Format(ss!vl_comis, "0000000000000") & IIf((CDbl(ss!vl_comis) / 100) < 5000, "DOC", "TED") & _
                        "237" & Format(ss!cod_agenciA, "0000000") & "000" & _
                        Format(ss!conta_corrente, "0000000000") & Space(20) & "0001000" & Space(55) & _
                        "0000000000000000" & ss!razao_social; Tab(294); Space(54) & "0000000000000" & _
                        Format(ss!dt_pagamento, "000000") & Format(ss!vl_comis, "0000000000000") & "REAL" & _
                        Space(8) & ss!dig_agenciA & Space(2) & Format(bSeq_Registro, "000000")

53            Print #2, ss!cod_repres; Tab(10); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(30); ss!razao_social; Tab(70); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(95); Format(ss!dt_pagamento, "000000")

54            ss.MoveNext
55        Next
56        bSeq_Registro = bSeq_Registro + 1
57        Print #1, "9" & Space(123) & Format(dblTotal_Comissao, "000000000000000") & _
                    Space(109) & "000000000000000" & Space(82) & "000000000000000" & _
                    Space(4) & Format(dblTotal_Comissao, "000000000000000") & Space(15) & _
                    Format(bSeq_Registro, "000000")
58        Print #2,
59        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
60        Close #1
61        Close #2
62        For i = 1 To lngTotal_Registros

63            OraParameters.Remove "ano_mes"
64            OraParameters.Add "ano_mes", lngAno_mes, 1
65            OraParameters.Remove "repres"
66            OraParameters.Add "repres", Repres(i), 1
67            OraParameters.Remove "dia"
68            OraParameters.Add "dia", intDia, 1

              'montar SQL
69            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_avulso(:ano_mes,:repres,:dia,:vErro);END;"

70            oradatabase.ExecuteSQL V_SQL

71            Set ss = oradatabase.Parameters("vCursor").Value

72            OraParameters.Remove "ano_mes"
73            OraParameters.Add "ano_mes", lngAno_mes, 1
74            OraParameters.Remove "repres"
75            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
76            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,3,:vErro);END;"

77            oradatabase.ExecuteSQL V_SQL

78            Set ss = oradatabase.Parameters("vCursor").Value


79        Next


80        Screen.MousePointer = 0

81        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_CONTROLADORIA()
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"


2         Screen.MousePointer = 11

          'arquivo de representantes
          '==========================
3         strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                            "REP" & ".txt"

          'montar SQL
4         V_SQL = "Begin producao.PCK_VDA860.PR_CONTROL_REP(:vCursor,:vErro);END;"

5         oradatabase.ExecuteSQL V_SQL

6         Set ss = oradatabase.Parameters("vCursor").Value


7         If oradatabase.Parameters("vErro").Value <> 0 Then
8             Screen.MousePointer = 0
9             Call Process_Line_Errors(SQL)
10            Exit Sub
11        End If

12        If ss.EOF And ss.BOF Then
13            Screen.MousePointer = 0
14            MsgBox "N�o h� representantes dispon�veis para gerar o arquivo", vbExclamation, "Aten��o"
15            Exit Sub
16        End If

17        Open strNome_Arquivo For Output As #1
18        Print #1, "ARQUIVO PARCIAL DE COMISS�O DE REPRESENTANTE DPK "
19        Print #1,
20        Print #1, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

21        Print #1,
22        Print #1, "CC;"; "COD.;"; "PSEUDONIMO;"; _
                    "TP;"; "VL_COMIS_NOTA;"; "VL_COMIS_LIQ;"; _
                    "IR"
23        Print #1,


24        For i = 1 To ss.RecordCount

25            Print #1, ss!cc & ";"; ss!cod&; ";"; ss!PSEUDONIMO&; ";"; _
                        ss!tp&; ";"; ss!vl_comis_nota&; ";"; ss!vl_comis_liquida&; ";"; _
                        ss!ir

26            ss.MoveNext
27        Next
28        Close #1


          'arquivo de telemarketing
          '==========================
29        strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                            "TLM" & ".txt"

          'montar SQL
30        V_SQL = "Begin producao.PCK_VDA860.PR_CONTROL_TLM(:vCursor,:vErro);END;"

31        oradatabase.ExecuteSQL V_SQL

32        Set ss = oradatabase.Parameters("vCursor").Value


33        If oradatabase.Parameters("vErro").Value <> 0 Then
34            Screen.MousePointer = 0
35            Call Process_Line_Errors(SQL)
36            Exit Sub
37        End If

38        If ss.EOF And ss.BOF Then
39            Screen.MousePointer = 0
40            MsgBox "N�o h� telemarketings dispon�veis para gerar o arquivo", vbExclamation, "Aten��o"
41            Exit Sub
42        End If

43        Open strNome_Arquivo For Output As #1
44        Print #1, "ARQUIVO PARCIAL DE COMISS�O DE TELEMARKETING DPK "
45        Print #1,
46        Print #1, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

47        Print #1,
48        Print #1, "CC;"; "COD.;"; "PSEUDONIMO;"; _
                    "TP;"; "VL_COMIS_LIQ"
49        Print #1,


50        For i = 1 To ss.RecordCount

51            Print #1, ss!cc & ";"; ss!cod & ";"; ss!PSEUDONIMO & ";"; _
                        ss!tp & ";"; ss!vl_comis_liquida

52            ss.MoveNext
53        Next
54        Close #1

          'arquivo de promotor
          '==========================
55        strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                            "PROMO" & ".txt"

          'montar SQL
56        V_SQL = "Begin producao.PCK_VDA860.PR_CONTROL_PROMO(:vCursor,:vErro);END;"

57        oradatabase.ExecuteSQL V_SQL

58        Set ss = oradatabase.Parameters("vCursor").Value


59        If oradatabase.Parameters("vErro").Value <> 0 Then
60            Screen.MousePointer = 0
61            Call Process_Line_Errors(SQL)
62            Exit Sub
63        End If

64        If ss.EOF And ss.BOF Then
65            Screen.MousePointer = 0
66            MsgBox "N�o h� promotores dispon�veis para gerar o arquivo", vbExclamation, "Aten��o"
67            Exit Sub
68        End If

69        Open strNome_Arquivo For Output As #1
70        Print #1, "ARQUIVO PARCIAL DE COMISS�O DE PROMOTOR DPK "
71        Print #1,
72        Print #1, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

73        Print #1,
74        Print #1, "CC;"; "COD.;"; "PSEUDONIMO;"; _
                    "TP;"; "VL_COMIS_LIQ"
75        Print #1,


76        For i = 1 To ss.RecordCount

77            Print #1, ss!cc & ";"; ss!cod & ";"; ss!PSEUDONIMO & ";"; _
                        ss!tp & ";"; ss!vl_comis_liquida

78            ss.MoveNext
79        Next
80        Close #1


81        Screen.MousePointer = 0

82        MsgBox "ARQUIVOS GERADOS COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_AVULSO_BRADESCO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long
          'NOVAS VARI�VEIS PARA TRATA CONTABILIZA��O
          Dim strArquivo_Contabil As String
          Dim strConta As String
          Dim lngDia_movimento As Long
          Dim dblTotal_Comissao_CP As Double
          Dim lngCC As String
          Dim dblVl_comis_CP As Double
          Dim dblTotal_Comissao_CP_tipo As Double
          Dim lngCod_Repres As Long
          Dim lngTp_Pagto As Long
          Dim lngSeq_pgto As Long
          Dim lngSeq_pgto_Bco As Long
          Dim strRazao As String
          Dim strEndereco As String

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1
4         lngSeq_pgto = 0

verifica:
5         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
6         If strArquivo <> "" Then
7             bSeq_Arquivo = bSeq_Arquivo + 1
8             GoTo verifica
9         Else
              'ALTERADO EM 18/09
              ' strNome_Arquivo = strPathArquivo & "FP" & Format(Now, "ddmm") & "A" & ".REM"
10            strNome_Arquivo = strPathArquivo & "PG" & Format(Now, "ddmm") & Format(bSeq_Arquivo, "00") & ".REM"

11            strArquivo_Contas_Pagar = strPathArquivo & intDia & Format(Now, "mmyy") & "V" & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
12        End If

          'CONTABILIZACAO
13        strArquivo_Contabil = strPathArquivo & "PGCOM" & intDia & Format(Now, "mmyy") & "V" & Format(bSeq_Arquivo, "00") & ".txt"
14        lngDia_movimento = Format(intDia, "00") & Format(Now, "MMYYYY")

          'montar SQL
15        OraParameters.Remove "seq"
16        OraParameters.Add "seq", 0, 2
17        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ_banco(:seq,:vErro);END;"

18        oradatabase.ExecuteSQL V_SQL
19        Set ss = oradatabase.Parameters("vCursor").Value


20        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


21        OraParameters.Remove "ano_mes"
22        OraParameters.Add "ano_mes", lngAno_mes, 1
23        OraParameters.Remove "dia"
24        OraParameters.Add "dia", intDia, 1

          'montar SQL
25        V_SQL = "Begin producao.PCK_VDA860.pr_arq_avulso(:vCursor,:ano_mes,:dia,:vErro);END;"

26        oradatabase.ExecuteSQL V_SQL

27        Set ss = oradatabase.Parameters("vCursor").Value

28        If oradatabase.Parameters("vErro").Value <> 0 Then
29            Screen.MousePointer = 0
30            Call Process_Line_Errors(SQL)
31            Exit Sub
32        End If

33        If ss.EOF And ss.BOF Then
34            Screen.MousePointer = 0
35            MsgBox "N�o h� representantes dispon�veis para gerar pagto avulso", vbExclamation, "Aten��o"
36            Exit Sub
37        End If
38        bSeq_Registro = 1
39        dblTotal_Comissao = 0

          'ARQUIVO CONTABILIZACAO
40        lngCC = "0"
41        Open strArquivo_Contabil For Output As #3

42        Open strArquivo_Contas_Pagar For Output As #2
43        Print #2, "ARQUIVO DE COMISSAO - ADIANTAMENTO AVULSO DPK "
44        Print #2,
45        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

46        Print #2,
47        Print #2, "CC"; Tab(5); "COD."; Tab(15); "CGC/CIC"; _
                    Tab(35); "RAZ�O SOCIAL"; Tab(75); "VALOR PAGTO"; Tab(100); "DT.PAGTO"; _
                    Tab(110); "TP.PAGTO"; _
                    Tab(125); "AG."; Tab(130); "CTA.C."; Tab(140); "DIG"

48        Print #2,
          'agencia bradesco alterada em 09/12/05 de 311 para 2002
49        Open strNome_Arquivo For Output As #1
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")


          'PRODUCAO 12/09/07
          'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
          '===============================================
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

50        Print #1, "0" & "00043308" & "2" & "045987005000198" & "COMERCIAL AUTOMOTIVA S.A.               " & _
                    "20" & "1" & Format(lngSeq_Repres_Banco, "00000") & "00000" & Format(Now, "YYYYMMDD") & _
                    Format(Now, "HHMMSS") & Space(5) & Space(3) & Space(5) & Space(1) & Space(74) & Space(80) & _
                    Space(234) & Format(bSeq_Registro, "000000")




51        lngTotal_Registros = ss.RecordCount
52        ReDim Repres(ss.RecordCount)

53        For i = 1 To ss.RecordCount

54            Repres(i) = ss!cod_repres
55            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
56            bSeq_Registro = bSeq_Registro + 1
57            lngSeq_pgto = lngSeq_pgto + 1
              'montar SQL
58            OraParameters.Remove "seq"
59            OraParameters.Add "seq", 0, 2
60            V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

61            oradatabase.ExecuteSQL V_SQL
62            Set ss = oradatabase.Parameters("vCursor").Value


63            lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


64            lngSeq_pgto_Bco = lngSeq_Repres_Banco

65            strRazao = ss!razao_social & Space(30 - Len(ss!razao_social))
66            strEndereco = ss!endereco & Space(40 - Len(ss!endereco))

              'producao 12/09/07
              'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
              '===============================================
              'Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
               Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
               Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
               Space(6) & Space(44) & Format(bSeq_Registro, "000000")

67            Print #1, "1" & "2" & Format(ss!cic_cgc, "000000000000000") & strRazao & strEndereco & _
                        Format(IIf(IsNull(ss!CEP), 0, ss!CEP), "00000000") & "237" & Format(ss!cod_agenciA, "00000") & Format(ss!dig_agenciA, "0") & _
                        Format(ss!conta_corrente, "0000000000000") & Format(ss!dig_ct_corrente, "0") & Space(1) & Format(lngSeq_pgto_Bco, "000000000000000") & "C" _
                      ; "000" & "000000000000" & Space(15) & "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        "00000000" & "00000000" & "0" & Space(4) & Space(10) & Format(ss!vl_comis, "000000000000000") & _
                        "000000000000000" & "000000000000000" & "05" & "0000000000" & Space(2) & "05" & _
                        "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        Space(3) & "01" & Space(10) & _
                        "0" & "00" & Space(4) & Space(15) & Space(15) & Space(6) & _
                        Space(40) & Space(1) & Space(1) & Space(40) & "00" & Space(35) & Space(22) & "00000" & Space(1) & _
                        "1" & "0051999" & Space(8) & Format(bSeq_Registro, "000000")



68            Print #2, ss!cod_centro_custo; Tab(5); ss!cod_repres; Tab(15); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(35); ss!razao_social; Tab(75); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(100); Format(ss!dt_pagamento, "000000"); Tab(110); IIf(ss!tp_pagto = 54301000, "TELEFONE", _
                                                                                   IIf(ss!tp_pagto = 21507000, "COMISSAO", _
                                                                                       IIf(ss!tp_pagto = 21507000, "COMISSAO", _
                                                                                           IIf(ss!tp_pagto = 12405803, "EMPRESTIMO", _
                                                                                               IIf(ss!tp_pagto = 21507000, "COMISSAO", _
                                                                                                   IIf(ss!tp_pagto = 53108000, "EQUIP.INF.", _
                                                                                                       IIf(ss!tp_pagto = 21395800, "PREMIO PRODUCAO", " "))))))); _
                                                                                                       Tab(125); ss!cod_agenciA; Tab(130); ss!conta_corrente; Tab(140); ss!dig_ct_corrente


69            ss.MoveNext
70        Next

71        bSeq_Registro = bSeq_Registro + 1
          'Print #1, "9" & Format(dblTotal_Comissao, "0000000000000") & _
           Space(180) & Format(bSeq_Registro, "000000")
72        Print #1, "9" & Format(bSeq_Registro, "000000") & Format(dblTotal_Comissao, "00000000000000000") & _
                    Space(470) & Format(bSeq_Registro, "000000")

73        Print #2,
74        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
          'total geral
75        dblTotal_Comissao_CP = dblTotal_Comissao / 100

          'pagto avulso de telefone
76        ss.MoveFirst
77        For i = 1 To ss.RecordCount
78            If ss!tp_pagto = 54301000 Or lngTp_Pagto = 54301000 Then
79                If ss!tp_pagto = 54301000 Then
80                    dblTotal_Comissao_CP_tipo = dblTotal_Comissao_CP_tipo + ss!vl_comis_cp
81                End If
                  'INF.CONTABILIZA��O
82                If lngCC = "0" Then
83                    lngCC = ss!cod_centro_custo
84                    dblVl_comis_CP = ss!vl_comis_cp
85                    lngTp_Pagto = ss!tp_pagto
86                Else
87                    If ss!tp_pagto = 54301000 Then
88                        If lngCC <> ss!cod_centro_custo Then
                              'strConta = strConta & "9" & lngCC & Space(17)
89                            strConta = strConta & lngCC & Space(5) & _
                                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
90                            lngCC = ss!cod_centro_custo
91                            dblVl_comis_CP = ss!vl_comis_cp
92                            lngTp_Pagto = ss!tp_pagto
93                        Else
94                            dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
95                        End If
96                    End If
97                End If
98            End If

99            ss.MoveNext
100       Next
101       If lngTp_Pagto = 54301000 Then
              'strConta = strConta & "9" & lngCC & Space(17)
102           strConta = strConta & lngCC & Space(5) & _
                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
103       End If


          'CONTABILIZACAO FONE
104       If dblTotal_Comissao_CP_tipo > 0 Then
105           Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "54301000            "; _
                        Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP_tipo, "0.00"))) & Format(dblTotal_Comissao_CP_tipo, "0.00") & Format(610, "00000000"); _
                        "FONE" & Space(186 - Len("FONE")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
106       End If
107       dblVl_comis_CP = 0
108       strConta = ""
109       dblTotal_Comissao_CP_tipo = 0


          'equipamento de inform�tica
110       lngCC = "0"
111       ss.MoveFirst
112       For i = 1 To ss.RecordCount
113           If ss!tp_pagto = 53108000 Or lngTp_Pagto = 53108000 Then
114               If ss!tp_pagto = 53108000 Then
115                   dblTotal_Comissao_CP_tipo = dblTotal_Comissao_CP_tipo + ss!vl_comis_cp
116               End If
                  'INF.CONTABILIZA��O
117               If lngCC = "0" Then
118                   lngCC = ss!cod_centro_custo
119                   dblVl_comis_CP = ss!vl_comis_cp
120                   lngTp_Pagto = ss!tp_pagto
121               Else
122                   If ss!tp_pagto = 53108000 Then
123                       If lngCC <> ss!cod_centro_custo Then
                              'strConta = strConta & "9" & lngCC & Space(17)
124                           strConta = strConta & lngCC & Space(5) & _
                                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
125                           lngCC = ss!cod_centro_custo
126                           dblVl_comis_CP = ss!vl_comis_cp
127                           lngTp_Pagto = ss!tp_pagto
128                       Else
129                           dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
130                       End If
131                   End If
132               End If
133           End If
134           ss.MoveNext
135       Next
136       If lngTp_Pagto = 53108000 Then
              'strConta = strConta & "9" & lngCC & Space(17)
137           strConta = strConta & lngCC & Space(5) & _
                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
138       End If

          'CONTABILIZACAO EQUIP. INFORM�TICA
139       If dblTotal_Comissao_CP_tipo > 0 Then
140           Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "53108000            "; _
                        Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP_tipo, "0.00"))) & Format(dblTotal_Comissao_CP_tipo, "0.00") & Format(610, "00000000"); _
                        "EQUIP.INF." & Space(186 - Len("EQUIP.INF.")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
141           dblVl_comis_CP = 0
142           strConta = ""
143           dblTotal_Comissao_CP_tipo = 0
144       End If

          'COMISSAO
145       lngCC = "0"
146       ss.MoveFirst
147       For i = 1 To ss.RecordCount
148           If ss!tp_pagto = 21507000 Or lngTp_Pagto = 21507000 Then
149               If ss!tp_pagto = 21507000 Then
150                   dblTotal_Comissao_CP_tipo = dblTotal_Comissao_CP_tipo + ss!vl_comis_cp
151               End If
                  'INF.CONTABILIZA��O
152               If lngCC = "0" Then
153                   lngCC = ss!cod_centro_custo
154                   dblVl_comis_CP = ss!vl_comis_cp
155                   lngTp_Pagto = ss!tp_pagto
156               Else
157                   If ss!tp_pagto = 21507000 Then
158                       If lngCC <> ss!cod_centro_custo Then
                              'strConta = strConta & "9" & lngCC & Space(17)
159                           strConta = strConta & lngCC & Space(5) & _
                                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
160                           lngCC = ss!cod_centro_custo
161                           dblVl_comis_CP = ss!vl_comis_cp
162                           lngTp_Pagto = ss!tp_pagto
163                       Else
164                           dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
165                       End If
166                   End If
167               End If

168           End If
169           ss.MoveNext
170       Next
171       If lngTp_Pagto = 21507000 Then
              ' strConta = strConta & "9" & lngCC & Space(17)
172           strConta = strConta & lngCC & Space(5) & _
                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
173       End If

          'CONTABILIZACAO COMISS�O
174       If dblTotal_Comissao_CP_tipo > 0 Then
175           Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21507000            "; _
                        Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP_tipo, "0.00"))) & Format(dblTotal_Comissao_CP_tipo, "0.00") & Format(1, "00000000"); _
                        "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
176       End If
177       dblVl_comis_CP = 0
178       strConta = ""
179       dblTotal_Comissao_CP_tipo = 0

          'EMPRESTIMO
180       lngCC = "0"
181       lngCod_Repres = 0
182       ss.MoveFirst
183       For i = 1 To ss.RecordCount
184           If ss!tp_pagto = 12405803 Then
                  'CONTABILIZACAO COMISS�O
185               Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "12405803            "; _
                            Tab(78); Space(17 - Len(Format(ss!vl_comis_cp, "0.00"))) & Format(ss!vl_comis_cp, "0.00") & Format(44, "00000000"); _
                            ss!cod_repres & "-" & ss!razao_social & Space(186 - Len(ss!cod_repres & "-" & ss!razao_social)); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"    '& strConta
186           End If
187           ss.MoveNext
188       Next
189       dblVl_comis_CP = 0
190       strConta = ""
191       dblTotal_Comissao_CP_tipo = 0


          'PREMIO PRODUCAO
192       lngCC = "0"
193       ss.MoveFirst
194       For i = 1 To ss.RecordCount
195           If ss!tp_pagto = 21395800 Or lngTp_Pagto = 21395800 Then
196               If ss!tp_pagto = 21395800 Then
197                   dblTotal_Comissao_CP_tipo = dblTotal_Comissao_CP_tipo + ss!vl_comis_cp
198               End If
                  'INF.CONTABILIZA��O
199               If lngCC = "0" Then
200                   lngCC = ss!cod_centro_custo
201                   dblVl_comis_CP = ss!vl_comis_cp
202                   lngTp_Pagto = ss!tp_pagto
203               Else
204                   If ss!tp_pagto = 21395800 Then
205                       If lngCC <> ss!cod_centro_custo Then
                              'strConta = strConta & "9" & lngCC & Space(17)
206                           strConta = strConta & lngCC & Space(5) & _
                                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
207                           lngCC = ss!cod_centro_custo
208                           dblVl_comis_CP = ss!vl_comis_cp
209                           lngTp_Pagto = ss!tp_pagto
210                       Else
211                           dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
212                       End If
213                   End If
214               End If

215           End If
216           ss.MoveNext
217       Next
218       If lngTp_Pagto = 21395800 Then
              'strConta = strConta & "9" & lngCC & Space(17)
219           strConta = strConta & lngCC & Space(5) & _
                         Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
220       End If

          'CONTABILIZACAO COMISS�O
221       If dblTotal_Comissao_CP_tipo > 0 Then
222           Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21395800            "; _
                        Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP_tipo, "0.00"))) & Format(dblTotal_Comissao_CP_tipo, "0.00") & Format(45, "00000000"); _
                      " " & Space(186 - Len(" ")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"    '& strConta
223       End If
224       dblVl_comis_CP = 0
225       strConta = ""
226       dblTotal_Comissao_CP_tipo = 0




227       Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000"); Tab(37); "11120805            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"

228       Close #1
229       Close #2
230       Close #3
231       For i = 1 To lngTotal_Registros

232           OraParameters.Remove "ano_mes"
233           OraParameters.Add "ano_mes", lngAno_mes, 1
234           OraParameters.Remove "repres"
235           OraParameters.Add "repres", Repres(i), 1
236           OraParameters.Remove "dia"
237           OraParameters.Add "dia", intDia, 1

              'montar SQL
238           V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_avulso(:ano_mes,:repres,:dia,:vErro);END;"

239           oradatabase.ExecuteSQL V_SQL

240           Set ss = oradatabase.Parameters("vCursor").Value

241           OraParameters.Remove "ano_mes"
242           OraParameters.Add "ano_mes", lngAno_mes, 1
243           OraParameters.Remove "repres"
244           OraParameters.Add "repres", Repres(i), 1

              'montar SQL
245           V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,3,:vErro);END;"

246           oradatabase.ExecuteSQL V_SQL

247           Set ss = oradatabase.Parameters("vCursor").Value


248       Next


249       Screen.MousePointer = 0

250       MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_PGTO_REPRES(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1

verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                                Format(bSeq_Arquivo, "00") & "SPC" & ".txt"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
11        End If

          'montar SQL
12        OraParameters.Remove "seq"
13        OraParameters.Add "seq", 0, 2
14        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

15        oradatabase.ExecuteSQL V_SQL
16        Set ss = oradatabase.Parameters("vCursor").Value


17        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", lngAno_mes, 1
20        OraParameters.Remove "dia"
21        OraParameters.Add "dia", intDia, 1

          'montar SQL
22        V_SQL = "Begin producao.PCK_VDA860.pr_arq_pgto_repres(:vCursor,:ano_mes,:dia,:vErro);END;"

23        oradatabase.ExecuteSQL V_SQL

24        Set ss = oradatabase.Parameters("vCursor").Value

25        If oradatabase.Parameters("vErro").Value <> 0 Then
26            Screen.MousePointer = 0
27            Call Process_Line_Errors(SQL)
28            Exit Sub
29        End If

30        If ss.EOF And ss.BOF Then
31            Screen.MousePointer = 0
32            MsgBox "N�o h� representantes dispon�veis para gerar pagto de representante", vbExclamation, "Aten��o"
33            Exit Sub
34        End If
35        bSeq_Registro = 1
36        dblTotal_Comissao = 0
37        Open strArquivo_Contas_Pagar For Output As #2
38        Print #2, "ARQUIVO DE COMISSAO - PAGAMENTO DE REPRESENTANTE DPK "
39        Print #2,
40        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

41        Print #2,
42        Print #2, "COD."; Tab(10); "CGC/CIC"; _
                    Tab(30); "RAZ�O SOCIAL"; Tab(70); "VALOR PAGTO"; Tab(95); "DT.PAGTO"
43        Print #2,

44        Open strNome_Arquivo For Output As #1
45        Print #1, "0" & "1" & "REMESSA" & "11" & "PAGTOS FORNECED" & "01227507" & _
                    "N" & Space(2) & "0001000" & "  " & "COMERCIAL AUTOMOTIVA S.A.     " & "422" & _
                    "BANCO SAFRA S/A" & Format(Now, "DDMMYY") & Space(1) & Space(2) & Space(285) & _
                    Format(bSeq_Arquivo, "000000") & Format(bSeq_Registro, "000000")

46        lngTotal_Registros = ss.RecordCount
47        ReDim Repres(ss.RecordCount)

48        For i = 1 To ss.RecordCount

49            Repres(i) = ss!cod_repres
50            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
51            bSeq_Registro = bSeq_Registro + 1

52            Print #1, "1" & "01" & "45987005000198" & "01227507" & Space(3) & _
                        "0001000" & Space(2) & Space(25); Tab(63); Format(ss!cic_cgc, "00000000000000") & _
                        "REC" & Format(lngSeq_Repres_Banco, "0000000000") & "0" & Space(17) & "C" & _
                        "01" & Format(lngSeq_Repres_Banco, "0000000000") & Format(ss!dt_pagamento, "000000") & _
                        Format(ss!vl_comis, "0000000000000") & IIf((CDbl(ss!vl_comis) / 100) < 5000, "DOC", "TED") & _
                        "237" & Format(ss!cod_agenciA, "0000000") & "000" & _
                        Format(ss!conta_corrente, "0000000000") & Space(20) & "0001000" & Space(55) & _
                        "0000000000000000" & ss!razao_social; Tab(294); Space(54) & "0000000000000" & _
                        Format(ss!dt_pagamento, "000000") & Format(ss!vl_comis, "0000000000000") & "REAL" & _
                        Space(8) & ss!dig_agenciA & Space(2) & Format(bSeq_Registro, "000000")

53            Print #2, ss!cod_repres; Tab(10); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(30); ss!razao_social; Tab(70); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(95); Format(ss!dt_pagamento, "000000")

54            ss.MoveNext
55        Next
56        bSeq_Registro = bSeq_Registro + 1
57        Print #1, "9" & Space(123) & Format(dblTotal_Comissao, "000000000000000") & _
                    Space(109) & "000000000000000" & Space(82) & "000000000000000" & _
                    Space(4) & Format(dblTotal_Comissao, "000000000000000") & Space(15) & _
                    Format(bSeq_Registro, "000000")
58        Print #2,
59        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
60        Close #1
61        Close #2
62        For i = 1 To lngTotal_Registros
63            OraParameters.Remove "ano_mes"
64            OraParameters.Add "ano_mes", lngAno_mes, 1
65            OraParameters.Remove "repres"
66            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
67            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,2,:vErro);END;"

68            oradatabase.ExecuteSQL V_SQL

69            Set ss = oradatabase.Parameters("vCursor").Value
70        Next

          ' OraParameters.Remove "ano_mes"
          ' OraParameters.Add "ano_mes", lngAno_mes, 1


          'V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"


          'oradatabase.ExecuteSQL V_SQL
71        MsgBox "Lembre-se de encerrar o fechamento", vbInformation, "Aten��o"

72        Screen.MousePointer = 0
73        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_PGTO_REPRES_BRADESCO(lngAno_mes As Long, intDia As Integer, intSituacao As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long
          'NOVAS VARI�VEIS PARA TRATA CONTABILIZA��O
          Dim strArquivo_Contabil As String
          Dim strConta As String
          Dim lngDia_movimento As Long
          Dim dblTotal_Comissao_CP As Double
          Dim lngCC As String
          Dim dblVl_comis_CP As Double
          Dim lngSeq_pgto As Long
          Dim lngSeq_pgto_Bco As Long
          Dim strRazao As String
          Dim strEndereco As String

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1
4         lngSeq_pgto = 0

verifica:
5         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPCC" & ".txt")
6         If strArquivo <> "" Then
7             bSeq_Arquivo = bSeq_Arquivo + 1
8             GoTo verifica
9         Else
              'ALTERADO EM 18/09/07
              'strNome_Arquivo = strPathArquivo & "FP" & Format(Now, "ddmm") & "D" & ".REM"
10            strNome_Arquivo = strPathArquivo & "PG" & Format(Now, "ddmm") & Format(bSeq_Arquivo, "00") & ".REM"

11            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
12        End If

          'CONTABILIZACAO
13        strArquivo_Contabil = strPathArquivo & "PGCOM" & Format(intDia, "00") & Format(Now, "mmyy") & "R.txt"
14        lngDia_movimento = Format(intDia, "00") & Format(Now, "MMYYYY")

          'montar SQL
15        OraParameters.Remove "seq"
16        OraParameters.Add "seq", 0, 2
17        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ_banco(:seq,:vErro);END;"

18        oradatabase.ExecuteSQL V_SQL
19        Set ss = oradatabase.Parameters("vCursor").Value


20        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


21        OraParameters.Remove "ano_mes"
22        OraParameters.Add "ano_mes", lngAno_mes, 1
23        OraParameters.Remove "dia"
24        OraParameters.Add "dia", intDia, 1
25        OraParameters.Remove "sit"
26        OraParameters.Add "sit", intSituacao, 1

          'montar SQL
27        V_SQL = "Begin producao.PCK_VDA860.pr_arq_pgto_repres(:vCursor,:ano_mes,:dia,:sit,:vErro);END;"

28        oradatabase.ExecuteSQL V_SQL

29        Set ss = oradatabase.Parameters("vCursor").Value

30        If oradatabase.Parameters("vErro").Value <> 0 Then
31            Screen.MousePointer = 0
32            Call Process_Line_Errors(SQL)
33            Exit Sub
34        End If

35        If ss.EOF And ss.BOF Then
36            Screen.MousePointer = 0
37            MsgBox "N�o h� representantes dispon�veis para gerar pagto de representante", vbExclamation, "Aten��o"
38            Exit Sub
39        End If
40        bSeq_Registro = 1
41        dblTotal_Comissao = 0

          'ARQUIVO CONTABILIZACAO
42        lngCC = "0"
43        Open strArquivo_Contabil For Output As #3


44        Open strArquivo_Contas_Pagar For Output As #2
45        Print #2, "ARQUIVO DE COMISSAO - PAGAMENTO DE REPRESENTANTE DPK "
46        Print #2,
47        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

48        Print #2,
          '   Print #2, "COD."; Tab(10); "CGC/CIC"; _
              Tab(30); "RAZ�O SOCIAL"; Tab(70); "VALOR PAGTO"; Tab(95); "DT.PAGTO"
49        Print #2, "CC"; Tab(10); "COD."; Tab(20); "CGC/CIC"; _
                    Tab(40); "RAZ�O SOCIAL"; Tab(80); "VALOR PAGTO"; Tab(105); "DT.PAGTO"; _
                    Tab(115); "AG."; Tab(120); "CTA.C."; Tab(130); "DIG"

50        Print #2,
          'agencia bradesco alterada em 09/12/05 de 311 para 2002
51        Open strNome_Arquivo For Output As #1
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

          'producao 12/09/07
          'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
          '===============================================
          'Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
           "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
           "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
           frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

52        Print #1, "0" & "00043308" & "2" & "045987005000198" & "COMERCIAL AUTOMOTIVA S.A.               " & _
                    "20" & "1" & Format(lngSeq_Repres_Banco, "00000") & "00000" & Format(Now, "YYYYMMDD") & _
                    Format(Now, "HHMMSS") & Space(5) & Space(3) & Space(5) & Space(1) & Space(74) & Space(80) & _
                    Space(234) & Format(bSeq_Registro, "000000")

53        lngTotal_Registros = ss.RecordCount
54        ReDim Repres(ss.RecordCount)

55        For i = 1 To ss.RecordCount

56            Repres(i) = ss!cod_repres
57            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
58            bSeq_Registro = bSeq_Registro + 1
59            lngSeq_pgto = lngSeq_pgto + 1

              'montar SQL
60            OraParameters.Remove "seq"
61            OraParameters.Add "seq", 0, 2
62            V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

63            oradatabase.ExecuteSQL V_SQL
64            Set ss = oradatabase.Parameters("vCursor").Value


65            lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value
66            lngSeq_pgto_Bco = lngSeq_Repres_Banco


              'INF.CONTABILIZA��O
67            If lngCC = "0" Then
68                lngCC = ss!cod_centro_custo
69                dblVl_comis_CP = ss!vl_comis_cp
70            Else
71                If lngCC <> ss!cod_centro_custo Then
                      'strConta = strConta & "9" & lngCC & Space(17)
72                    strConta = strConta & lngCC & Space(5) & _
                                 Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
73                    lngCC = ss!cod_centro_custo
74                    dblVl_comis_CP = ss!vl_comis_cp
75                Else
76                    dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
77                End If
78            End If

79            strRazao = ss!razao_social & Space(30 - Len(ss!razao_social))
80            strEndereco = ss!endereco & Space(40 - Len(ss!endereco))

              'Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
               Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
               Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
               Space(6) & Space(44) & Format(bSeq_Registro, "000000")

              'producao 12/09/07
              'NOVO LAY-OUT CONFO SOLIC.DEPTO A PAGAR 10/01/07
              '===============================================
              'Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
               Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
               Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
               Space(6) & Space(44) & Format(bSeq_Registro, "000000")

81            Print #1, "1" & "2" & Format(ss!cic_cgc, "000000000000000") & strRazao & strEndereco & _
                        Format(IIf(IsNull(ss!CEP), 0, ss!CEP), "00000000") & "237" & Format(ss!cod_agenciA, "00000") & Format(ss!dig_agenciA, "0") & _
                        Format(ss!conta_corrente, "0000000000000") & Format(ss!dig_ct_corrente, "0") & Space(1) & Format(lngSeq_pgto_Bco, "000000000000000") & "C" _
                      ; "000" & "000000000000" & Space(15) & "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        "00000000" & "00000000" & "0" & Space(4) & Space(10) & Format(ss!vl_comis, "000000000000000") & _
                        "000000000000000" & "000000000000000" & "05" & "0000000000" & Space(2) & "05" & _
                        "20" & Mid(Format(ss!dt_pagamento, "000000"), 5, 2) & _
                        Mid(Format(ss!dt_pagamento, "000000"), 3, 2) & Mid(Format(ss!dt_pagamento, "000000"), 1, 2) & _
                        Space(3) & "01" & Space(10) & _
                        "0" & "00" & Space(4) & Space(15) & Space(15) & Space(6) & _
                        Space(40) & Space(1) & Space(1) & Space(40) & "00" & Space(35) & Space(22) & "00000" & Space(1) & _
                        "1" & "0051999" & Space(8) & Format(bSeq_Registro, "000000")



82            Print #2, ss!cod_centro_custo; Tab(10); ss!cod_repres; Tab(20); Format(ss!cic_cgc, "00000000000000"); Tab(40); ss!razao_social; Tab(80); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(105); Format(ss!dt_pagamento, "000000"); _
                        Tab(115); ss!cod_agenciA; Tab(120); ss!conta_corrente; Tab(130); ss!dig_ct_corrente

83            ss.MoveNext
84        Next
          'strConta = strConta & "9" & lngCC & Space(17)
85        strConta = strConta & lngCC & Space(5) & _
                     Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")


86        bSeq_Registro = bSeq_Registro + 1
          'Print #1, "9" & Format(dblTotal_Comissao, "0000000000000") & _
           Space(180) & Format(bSeq_Registro, "000000")
87        Print #1, "9" & Format(bSeq_Registro, "000000") & Format(dblTotal_Comissao, "00000000000000000") & _
                    Space(470) & Format(bSeq_Registro, "000000")
88        Print #2,
89        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")



90        dblTotal_Comissao_CP = dblTotal_Comissao / 100

          'CONTABILIZACAO
91        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21507000            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta

92        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000"); Tab(37); "11120805            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"

93        Close #1
94        Close #2
95        Close #3
96        For i = 1 To lngTotal_Registros
97            OraParameters.Remove "ano_mes"
98            OraParameters.Add "ano_mes", lngAno_mes, 1
99            OraParameters.Remove "repres"
100           OraParameters.Add "repres", Repres(i), 1

              'montar SQL
101           V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,2,:vErro);END;"

102           oradatabase.ExecuteSQL V_SQL

103           Set ss = oradatabase.Parameters("vCursor").Value
104       Next



105       MsgBox "Lembre-se de encerrar o fechamento", vbInformation, "Aten��o"

106       Screen.MousePointer = 0
107       MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PrintImagem(P As IPictureDisp, Optional ByVal X, Optional ByVal Y, Optional ByVal resize)
1         If IsMissing(X) Then X = Printer.CurrentX
2         If IsMissing(Y) Then Y = Printer.CurrentY
3         If IsMissing(resize) Then resize = 1
4         Printer.PaintPicture P, X, Y, P.Width * resize, P.Height * resize
End Sub

Public Sub PR_ARQ_PFJ(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim dblVL_Comis_nota As Double
          Dim dblIR As Double
          Dim dblINSS As Double

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11


          'strArquivo = "FIS_PF_PJ" & lngAno_mes & ".TXT"
3         strArquivo = "FIS_PJ" & lngAno_mes & ".TXT"

4         OraParameters.Remove "ano_mes"
5         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
6         V_SQL = "Begin producao.PCK_VDA860.pr_arq_pfj(:vCursor,:ano_mes,:vErro);END;"

7         oradatabase.ExecuteSQL V_SQL

8         Set ss = oradatabase.Parameters("vCursor").Value

9         If oradatabase.Parameters("vErro").Value <> 0 Then
10            Screen.MousePointer = 0
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss.EOF And ss.BOF Then
15            Screen.MousePointer = 0
16            MsgBox "N�o h� informa��es dispon�veis para gerar arquivo para o per�odo escolhido", vbExclamation, "Aten��o"
17            Exit Sub
18        End If


19        dblVL_Comis_nota = 0
20        dblIR = 0
21        dblINSS = 0

22        Open strPathArquivo & strArquivo For Output As #1

23        Printer.Font = "VERDANA"
24        Printer.FontSize = 8
25        Print #1,
26        Print #1,
27        Printer.FontBold = True
28        Print #1, "COMERCIAL AUTOMOTIVA S.A."
29        Print #1,
30        Printer.FontBold = False

          'Print #1, "RELAT�RIO FIS_PF E FIS_PJ DPK "
31        Print #1, "RELAT�RIO  FIS_PJ DPK "
32        Print #1,
33        Print #1, "Referente : " & Mid(lngAno_mes, 5, 2) & "/" & Mid(lngAno_mes, 1, 4)
34        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
35        Print #1,
36        Print #1, "TP;"; "COD;"; "PSEUDONIMO;"; "RAZAO_SOCIAL;"; _
                    "CIC_CGC;"; "CC;"; "VL_COMIS_NF;"; _
                    "IR;"; "INSS;"
37        Print #1,



38        For i = 1 To ss.RecordCount

39            dblVL_Comis_nota = dblVL_Comis_nota + IIf(IsNull(ss!vl_comis_nota), 0, ss!vl_comis_nota)
40            dblIR = dblIR + IIf(IsNull(ss!ir), 0, ss!ir)
41            dblINSS = dblINSS + IIf(IsNull(ss!inss), 0, ss!inss)

42            Print #1, ss!tp & ";"; ss!cod_repres & ";"; ss!PSEUDONIMO & ";"; ss!razao_social & ";"; _
                        ss!cic_cgc & ";"; ss!cc & ";"; ss!vl_comis_nota & ";"; _
                        ss!ir & ";"; ss!inss & ";"


43            ss.MoveNext
44        Next
45        Print #1,
46        Print #1,
47        Print #1, "TOTAL COMIS NOTA: " & dblVL_Comis_nota
48        Print #1, "TOTAL IR: " & dblIR
49        Print #1, "TOTAL InSS: " & dblINSS

50        Close #1


51        Screen.MousePointer = 0


End Sub

Public Sub PR_REL_CONTAB(lngAno_mes As Long)
          Dim i As Long
          Dim ss1 As Object
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCentro_Custo As Long
          Dim dblT5110100_C As Double
          Dim dblT5110700_C As Double
          Dim dblT5120300_C As Double
          Dim dblT5310800_C As Double
          Dim dblT1240580_C As Double
          Dim dblT2140580_C As Double
          Dim dblT2140380_C As Double
          Dim dblT2150700_C As Double
          Dim dblTAdto_C As Double
          Dim dblTTotal_C As Double
          Dim dblT5110100_T As Double
          Dim dblT5110700_T As Double
          Dim dblT5120300_T As Double
          Dim dblT5310800_T As Double
          Dim dblT1240580_T As Double
          Dim dblT2140580_T As Double
          Dim dblT2140380_T As Double
          Dim dblT2150700_T As Double
          Dim dblTAdto_T As Double
          Dim dblTTotal_T As Double

          Dim dblComis_Ant As Double
          Dim dblTotal_Geral As Double
          Dim dblComis_Ant_T As Double
          Dim dblTotal_Geral_T As Double
          Dim dblComis As Double

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11

3         lngCentro_Custo = 0
4         strArquivo = "C" & lngAno_mes & "_PJ.TXT"

5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
7         V_SQL = "Begin producao.PCK_VDA860.pr_contabrel_mes(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Screen.MousePointer = 0
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If

15        If ss.EOF And ss.BOF Then
16            Screen.MousePointer = 0
17            MsgBox "N�o h� informa��es dispon�veis para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
18            Exit Sub
19        End If

          'INICIALIZANDO VARI�VEIS
20        dblT5110100_C = 0
21        dblT5110700_C = 0
22        dblT5120300_C = 0
23        dblT5310800_C = 0
24        dblT1240580_C = 0
25        dblT2140580_C = 0
26        dblT2140380_C = 0
27        dblT2150700_C = 0
28        dblTAdto_C = 0
29        dblTTotal_C = 0
30        dblT5110100_T = 0
31        dblT5110700_T = 0
32        dblT5120300_T = 0
33        dblT5310800_T = 0
34        dblT1240580_T = 0
35        dblT2140580_T = 0
36        dblT2140380_T = 0
37        dblT2150700_T = 0
38        dblTAdto_T = 0
39        dblTTotal_T = 0
40        dblComis_Ant = 0
41        dblComis_Ant_T = 0
42        dblTotal_Geral = 0
43        dblTotal_Geral_T = 0

44        Open strPathArquivo & strArquivo For Output As #1

45        Printer.Font = "VERDANA"
46        Printer.FontSize = 8
47        Print #1,
48        Print #1,
49        Printer.FontBold = True
50        Print #1, "COMERCIAL AUTOMOTIVA S.A."
51        Print #1,
52        Printer.FontBold = False

          'COTA 51107 ALTERADA PARA 51224 CONF. SOLICTA��O CONTABIL
53        Print #1, "RELAT�RIO DE COMISS�O PARA REPRESENTANTES - MENSAL - PESSOA JUR�DICA"
54        Print #1,
55        Print #1, "Referente : " & Mid(lngAno_mes, 5, 2) & "/" & Mid(lngAno_mes, 1, 4)
56        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
57        Print #1,
58        Print #1, "CC"; Tab(7); "COD"; Tab(12); "REPRES"; Tab(23); "T51101"; _
                    Tab(34); "T51224"; Tab(49); "T51203"; Tab(64); "T53108"; Tab(79); "T12405"; _
                    Tab(94); "T21405"; Tab(109); "T21405"; Tab(124); "T21507"; Tab(136); "T.ADTO"; _
                    Tab(152); "TOTAL"; Tab(164); "COMIS.ANT."; Tab(182); "TOTAL"
59        Print #1, "  "; Tab(7); "   "; Tab(12); "      "; Tab(23); "COMIS"; _
                    Tab(34); "PREMIOS"; Tab(49); "BRINDE"; Tab(64); "REEMB.EQ."; Tab(79); "COM.ANT."; _
                    Tab(94); "IRRF"; Tab(109); "INSS"; Tab(124); "INS.SALDO"; Tab(136); "      "; _
                    Tab(150); "LIQ."; Tab(164); "A PAGAR"; Tab(182); "GERAL"
60        Print #1,



61        For i = 1 To ss.RecordCount

62            OraParameters.Remove "ano_mes"
63            OraParameters.Add "ano_mes", lngAno_mes, 1
64            OraParameters.Remove "cod_repres"
65            OraParameters.Add "cod_repres", ss!cod_repres, 1
66            OraParameters.Remove "comis"
67            OraParameters.Add "comis", 0, 2


              'montar SQL
68            V_SQL = "Begin producao.PCK_VDA860.pr_comis_aberta(:ano_mes,:cod_repres,:comis,:vErro);END;"

69            oradatabase.ExecuteSQL V_SQL

70            dblComis = IIf(IsNull(oradatabase.Parameters("comis").Value), 0, oradatabase.Parameters("comis").Value)



71            If lngCentro_Custo <> 0 And _
                 ss!cod_centro_custo <> lngCentro_Custo Then

72                If dblTotal_Geral <> 0 Then
73                    Print #1, "  "; Tab(7); "   "; Tab(12); _
                                "TOTAL"; Tab(23); Format(dblT5110100_C, "0.00"); _
                                Tab(34); Format(dblT5110700_C, "0.00"); Tab(49); Format(dblT5120300_C, "0.00"); _
                                Tab(64); Format(dblT5310800_C, "0.00"); Tab(79); Format(dblT1240580_C, "0.00"); _
                                Tab(94); Format(dblT2140580_C, "0.00"); Tab(109); Format(dblT2140380_C, "0.00"); _
                                Tab(124); Format(dblT2150700_C, "0.00"); Tab(136); Format(dblTAdto_C, "0.00"); _
                                Tab(150); Format(dblTTotal_C, "0.00"); Tab(164); dblComis_Ant; Tab(182); dblTotal_Geral
74                    Print #1,
75                End If

76                dblT5110100_C = 0
77                dblT5110700_C = 0
78                dblT5120300_C = 0
79                dblT5310800_C = 0
80                dblT1240580_C = 0
81                dblT2140580_C = 0
82                dblT2140380_C = 0
83                dblT2150700_C = 0
84                dblTAdto_C = 0
85                dblTTotal_C = 0
86                dblComis_Ant = 0
87                dblTotal_Geral = 0

88            End If

89            lngCentro_Custo = ss!cod_centro_custo
90            dblT5110100_C = dblT5110100_C + ss!t51101000
91            dblT5110700_C = dblT5110700_C + ss!t51107000
92            dblT5120300_C = dblT5120300_C + ss!t51203000
93            dblT5310800_C = dblT5310800_C + ss!t53108000
94            dblT1240580_C = dblT1240580_C + ss!t12405803
95            dblT2140580_C = dblT2140580_C + ss!t21405800
96            dblT2140380_C = dblT2140380_C + ss!t21403800
97            dblT2150700_C = dblT2150700_C + ss!t21507000_ins
98            dblTAdto_C = dblTAdto_C + ss!tadto
99            dblTTotal_C = dblTTotal_C + ss!t21507_adto
100           dblComis_Ant = dblComis_Ant + dblComis
101           dblTotal_Geral = dblTTotal_C + dblComis_Ant

102           dblT5110100_T = dblT5110100_T + ss!t51101000
103           dblT5110700_T = dblT5110700_T + ss!t51107000
104           dblT5120300_T = dblT5120300_T + ss!t51203000
105           dblT5310800_T = dblT5310800_T + ss!t53108000
106           dblT1240580_T = dblT1240580_T + ss!t12405803
107           dblT2140580_T = dblT2140580_T + ss!t21405800
108           dblT2140380_T = dblT2140380_T + ss!t21403800
109           dblT2150700_T = dblT2150700_T + ss!t21507000_ins
110           dblTAdto_T = dblTAdto_T + ss!tadto
111           dblTTotal_T = dblTTotal_T + ss!t21507_adto
112           dblComis_Ant_T = dblComis_Ant_T + dblComis
113           dblTotal_Geral_T = dblTTotal_T + dblComis_Ant_T


114           If CDbl(CDbl(ss!t51101000) + CDbl(ss!t21507_adto) + dblComis) <> 0 Then
115               Print #1, ss!cod_centro_custo; Tab(7); ss!cod_repres; Tab(12); _
                            Mid(ss!PSEUDONIMO, 1, 10); Tab(23); Format(ss!t51101000, "0.00"); _
                            Tab(34); Format(ss!t51107000, "0.00"); Tab(49); Format(ss!t51203000, "0.00"); _
                            Tab(64); Format(ss!t53108000, "0.00"); Tab(79); Format(ss!t12405803, "0.00"); _
                            Tab(94); Format(ss!t21405800, "0.00"); Tab(109); Format(ss!t21403800, "0.00"); _
                            Tab(124); Format(ss!t21507000_ins, "0.00"); Tab(136); Format(ss!tadto, "0.00"); _
                            Tab(150); Format(ss!t21507_adto, "0.00"); Tab(164); dblComis; Tab(182); CDbl(ss!t21507_adto + dblComis)
116           End If

117           ss.MoveNext
118       Next

119       If dblTotal_Geral <> 0 Then
120           Print #1, "  "; Tab(7); "   "; Tab(12); _
                        "TOTAL"; Tab(23); Format(dblT5110100_C, "0.00"); _
                        Tab(34); Format(dblT5110700_C, "0.00"); Tab(49); Format(dblT5120300_C, "0.00"); _
                        Tab(64); Format(dblT5310800_C, "0.00"); Tab(79); Format(dblT1240580_C, "0.00"); _
                        Tab(94); Format(dblT2140580_C, "0.00"); Tab(109); Format(dblT2140380_C, "0.00"); _
                        Tab(124); Format(dblT2150700_C, "0.00"); Tab(136); Format(dblTAdto_C, "0.00"); _
                        Tab(150); Format(dblTTotal_C, "0.00"); Tab(164); dblComis_Ant; Tab(182); dblTotal_Geral
121           Print #1,
122       End If

123       If dblTotal_Geral_T <> 0 Then
124           Print #1, "  "; Tab(7); "   "; Tab(12); _
                        "TOTAL"; Tab(23); Format(dblT5110100_T, "0.00"); _
                        Tab(34); Format(dblT5110700_T, "0.00"); Tab(49); Format(dblT5120300_T, "0.00"); _
                        Tab(64); Format(dblT5310800_T, "0.00"); Tab(79); Format(dblT1240580_T, "0.00"); _
                        Tab(94); Format(dblT2140580_T, "0.00"); Tab(109); Format(dblT2140380_T, "0.00"); _
                        Tab(124); Format(dblT2150700_T, "0.00"); Tab(136); Format(dblTAdto_T, "0.00"); _
                        Tab(150); Format(dblTTotal_T, "0.00"); Tab(164); dblComis_Ant_T; Tab(182); dblTotal_Geral_T
125           Print #1,
126       End If
127       Close #1


128       Screen.MousePointer = 0


End Sub

Public Sub PR_REL_CONTAB_Sem(lngAno_mes As Long)
          Dim i As Long
          Dim ss1 As Object
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCentro_Custo As Long


1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11

3         lngCentro_Custo = 0
4         strArquivo = "Sem_pagto_" & lngAno_mes & "_PJ.TXT"

5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
7         V_SQL = "Begin producao.PCK_VDA860.pr_contabrel_sem_mes(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Screen.MousePointer = 0
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If

15        If ss.EOF And ss.BOF Then
16            Screen.MousePointer = 0
17            MsgBox "N�o h� informa��es dispon�veis(PJ) para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
18            Exit Sub
19        End If


20        Open strPathArquivo & strArquivo For Output As #1

21        Printer.Font = "VERDANA"
22        Printer.FontSize = 8
23        Print #1,
24        Print #1,
25        Printer.FontBold = True
26        Print #1, "COMERCIAL AUTOMOTIVA S.A."
27        Print #1,
28        Printer.FontBold = False

29        Print #1, "RELAT�RIO DE COMISS�O SEM PAGTO PARA REPRESENTANTES - MENSAL - PESSOA JUR�DICA"
30        Print #1,
31        Print #1, "Referente : " & Mid(lngAno_mes, 5, 2) & "/" & Mid(lngAno_mes, 1, 4)
32        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
33        Print #1,
34        Print #1, "CC"; Tab(5); "COD"; Tab(10); "REPRES"; Tab(30); "COMIS"; _
                    Tab(37); "MOTIVO"
35        Print #1,



36        For i = 1 To ss.RecordCount
37            Print #1, ss!CENTRO_CUSTO; Tab(5); ss!cod_repres; Tab(10); _
                        ss!PSEUDONIMO; Tab(30); Format(ss!vl_comissao, "0.00"); _
                        Tab(37); ss!DESC_MOTIVO
38            ss.MoveNext
39        Next

40        Close #1


41        Screen.MousePointer = 0


End Sub

Public Sub PR_REL_POSICAO_EMP(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCod_Repres As Long

          Dim dblVl_emprestado As Double
          Dim dblVl_parc_mensal As Double
          Dim dblTotal_recebido As Double
          Dim dblSaldo_ant As Double
          Dim dblVL_Emp_mes As Double
          Dim dblVl_parc_receb As Double
          Dim dblVl_parc_receb_AVULSO As Double
          Dim dblVl_correcao As Double
          Dim dblSaldo_atual As Double

          Dim dblVl_emprestado_t As Double
          Dim dblVl_parc_mensal_t As Double
          Dim dblTotal_recebido_t As Double
          Dim dblSaldo_ant_t As Double
          Dim dblVl_emp_mes_t As Double
          Dim dblVl_parc_receb_t As Double
          Dim dblVl_parc_receb_AVULSO_t As Double
          Dim dblVl_correcao_t As Double
          Dim dblSaldo_atual_t As Double
          Dim dblVL_Mes As Double
          Dim dblSaldo_atual_a_receber As Double
          Dim dblDiferenca As Double
          Dim dblDiferenca_total As Double
          Dim dblSaldo_ant_Final As Double
          Dim dblSaldo_ant_t_Final As Double
          Dim dblCorrecao1 As Double
          Dim dblCorrecao2 As Double




1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11


3         strArquivo = "EMP_" & lngAno_mes & ".TXT"

4         OraParameters.Remove "ano_mes"
5         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
6         V_SQL = "Begin producao.PCK_VDA860.pr_posicao_emprestimo(:vCursor,:ano_mes,:vErro);END;"

7         oradatabase.ExecuteSQL V_SQL

8         Set ss = oradatabase.Parameters("vCursor").Value

9         If oradatabase.Parameters("vErro").Value <> 0 Then
10            Screen.MousePointer = 0
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss.EOF And ss.BOF Then
15            Screen.MousePointer = 0
16            MsgBox "N�o h� informa��es dispon�veis para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
17            Exit Sub
18        End If

          'INICIALIZANDO VARI�VEIS
19        lngCod_Repres = 0



20        Open strPathArquivo & strArquivo For Output As #1

21        Printer.Font = "VERDANA"
22        Printer.FontSize = 8
23        Print #1,
24        Print #1,
25        Printer.FontBold = True
26        Print #1, "COMERCIAL AUTOMOTIVA S.A."
27        Print #1,
28        Printer.FontBold = False

29        Print #1, "POSI��O DE EMPR�STIMO A REPRESENTANTES - MENSAL"
30        Print #1,
31        Print #1,
32        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
33        Print #1,
34        Print #1, "COD"; Tab(7); "REPRES"; Tab(24); "DT.EMP."; _
                    Tab(34); "VL.EMP."; Tab(47); "VL.PARC."; Tab(62); "QTD."; Tab(77); "TOTAL"; _
                    Tab(92); "SALDO"; Tab(107); "EMP."; Tab(122); "VL.PARC."; Tab(134); "VL.PARC."; Tab(148); "VL."; _
                    Tab(162); "SD.ATUAL"
35        Print #1, "   "; Tab(7); "       "; Tab(24); " "; _
                    Tab(34); " "; Tab(47); "MENSAL"; Tab(62); "PARCS."; Tab(77); "RECEB."; _
                    Tab(92); "ANT."; Tab(107); "CONC.MES"; Tab(122); "REC.MES"; Tab(134); "REC.MES AV"; Tab(148); "CORREC."; _
                    Tab(162); "RECEB."
36        Print #1,



37        For i = 1 To ss.RecordCount


38            If lngCod_Repres <> 0 And _
                 ss!cod <> lngCod_Repres Then



39                Print #1, "  "; Tab(7); _
                            "TOTAL"; Tab(24); ; "       "; _
                            Tab(34); Format(dblVl_emprestado, "0.00"); Tab(47); Format(dblVl_parc_mensal, "0.00"); _
                            Tab(62); "          "; Tab(77); Format(dblTotal_recebido, "0.00"); _
                            Tab(92); Format(dblSaldo_ant_Final, "0.00"); Tab(107); Format(dblVL_Emp_mes, "0.00"); _
                            Tab(122); Format(dblVl_parc_receb, "0.00"); Tab(134); Format(dblVl_parc_receb_AVULSO, "0.00"); Tab(148); Format(dblVl_correcao, "0.00"); _
                            Tab(162); Format(dblSaldo_atual, "0.00")
40                Print #1,

41                dblVl_emprestado = 0
42                dblVl_parc_mensal = 0
43                dblTotal_recebido = 0
44                dblSaldo_ant = 0
45                dblSaldo_ant_Final = 0
46                dblVL_Emp_mes = 0
47                dblVl_parc_receb = 0
48                dblVl_parc_receb_AVULSO = 0
49                dblVl_correcao = 0
50                dblSaldo_atual = 0

51            End If

52            lngCod_Repres = ss!cod
53            dblVl_emprestado = dblVl_emprestado + ss!vl_emprestado
54            dblVl_parc_mensal = dblVl_parc_mensal + ss!vl_parc_mensal
55            dblTotal_recebido = dblTotal_recebido + ss!total_recebido
56            dblSaldo_ant = dblSaldo_ant + IIf(ss!situacao = 9, 0, IIf(IsNull(ss!saldo_anterior), 0, ss!saldo_anterior))
57            dblSaldo_ant_Final = dblSaldo_ant_Final + IIf(IsNull(ss!saldo_anterior), 0, ss!saldo_anterior)
58            If dblSaldo_ant > 0 And dblVl_emprestado > 0 And lngAno_mes = Format(ss!dt_empres, "yyyymm") Then
59                If lngCod_Repres = 37 And lngAno_mes = 200508 Then
60                    dblVL_Emp_mes = dblVL_Emp_mes + 5000
61                    dblVL_Mes = 5000
62                Else
                      '   dblVL_Emp_mes = dblVL_Emp_mes + (dblVl_emprestado - dblSaldo_ant)
                      '   dblVL_Mes = dblVl_emprestado - dblSaldo_ant
63                    dblVL_Emp_mes = dblVL_Emp_mes + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
64                    dblVL_Mes = IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
65                End If
66            Else

67                dblVL_Emp_mes = dblVL_Emp_mes + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
68                dblVL_Mes = IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
69            End If
70            dblVl_parc_receb = dblVl_parc_receb + ss!Vl_parc_receb_mes
71            dblVl_parc_receb_AVULSO = dblVl_parc_receb_AVULSO + ss!Vl_parc_receb_mes_AVULSO
72            dblVl_correcao = dblVl_correcao + Val(ss!Vl_correcao)



73            If lngCod_Repres = 565 And lngAno_mes = 200508 Then
74                dblSaldo_atual = dblSaldo_atual
75            Else
                  ' dblSaldo_atual = dblSaldo_atual + ss!Saldo_atual_a_receber
76                If ss!situacao = 0 Then
77                    If (dblSaldo_ant + dblVl_correcao + dblVL_Emp_mes - dblVl_parc_receb - dblVl_parc_receb_AVULSO) < 0 Then
78                        dblVl_correcao = dblVl_correcao - Val(ss!Vl_correcao) + ((dblSaldo_ant + dblVL_Emp_mes - dblVl_parc_receb - dblVl_parc_receb_AVULSO) * -1)
79                    End If
                      ' dblSaldo_atual = CDbl(IIf((dblSaldo_ant + dblVl_correcao + dblVL_Emp_mes - dblVl_parc_receb - dblVl_parc_receb_AVULSO) < 0, 0, _
                        ' (dblSaldo_ant + dblVl_correcao + dblVL_Emp_mes - dblVl_parc_receb - dblVl_parc_receb_AVULSO)))

80                    dblSaldo_atual = dblSaldo_ant + dblVl_correcao + dblVL_Emp_mes - dblVl_parc_receb - dblVl_parc_receb_AVULSO

81                Else
82                    dblSaldo_atual = 0
83                End If
84            End If




85            dblVl_emprestado_t = dblVl_emprestado_t + ss!vl_emprestado
86            dblVl_parc_mensal_t = dblVl_parc_mensal_t + ss!vl_parc_mensal
87            dblTotal_recebido_t = dblTotal_recebido_t + ss!total_recebido
88            dblSaldo_ant_t = dblSaldo_ant_t + IIf(ss!situacao = 9, 0, CDbl(IIf(IsNull(ss!saldo_anterior), 0, ss!saldo_anterior)))
89            dblSaldo_ant_t_Final = dblSaldo_ant_t_Final + CDbl(IIf(IsNull(ss!saldo_anterior), 0, ss!saldo_anterior))

90            If dblSaldo_ant_t > 0 And dblVl_emprestado_t > 0 And lngAno_mes = Format(ss!dt_empres, "yyyymm") Then
91                If lngCod_Repres = 37 And lngAno_mes = 200508 Then
92                    dblVl_emp_mes_t = dblVl_emp_mes_t + 5000
93                Else
                      ' dblVl_emp_mes_t = dblVl_emp_mes_t + (dblVl_emprestado - dblSaldo_ant)
94                    dblVl_emp_mes_t = dblVl_emp_mes_t + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
95                End If
96            Else
97                dblVl_emp_mes_t = dblVl_emp_mes_t + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), ss!vl_emprestado, 0)
98            End If
99            dblVl_parc_receb_t = dblVl_parc_receb_t + ss!Vl_parc_receb_mes
100           dblVl_parc_receb_AVULSO_t = dblVl_parc_receb_AVULSO_t + ss!Vl_parc_receb_mes_AVULSO
101           dblVl_correcao_t = dblVl_correcao_t + dblVl_correcao
              'dblVl_correcao_t = dblVl_correcao_t + Val(ss!Vl_correcao)

102           If lngCod_Repres = 565 And lngAno_mes = 200508 Then
103               dblSaldo_atual_t = dblSaldo_atual_t
104           Else
                  '  dblSaldo_atual_t = dblSaldo_atual_t + ss!Saldo_atual_a_receber
105               If ss!situacao = 0 Then
106                   If (dblSaldo_ant_t + dblVl_correcao_t + dblVl_emp_mes_t - dblVl_parc_receb_t - dblVl_parc_receb_AVULSO_t) < 0 Then
107                       dblVl_correcao_t = dblVl_correcao_t - Val(ss!Vl_correcao) + ((dblSaldo_ant_t + dblVl_emp_mes_t - dblVl_parc_receb_t - dblVl_parc_receb_AVULSO_t) * -1)
108                   End If

                      'dblSaldo_atual_t =' CDbl(IIf((dblSaldo_ant_t + dblVl_correcao_t + dblVl_emp_mes_t - dblVl_parc_receb_t - dblVl_parc_receb_AVULSO_t) < 0, 0, _
                       '(dblSaldo_ant_t + dblVl_correcao_t + dblVl_emp_mes_t - dblVl_parc_receb_t - dblVl_parc_receb_AVULSO_t)))

109                   dblSaldo_atual_t = dblSaldo_ant_t + dblVl_correcao_t + dblVl_emp_mes_t - dblVl_parc_receb_t - dblVl_parc_receb_AVULSO_t

110               Else
111                   dblSaldo_atual_t = dblSaldo_atual_t
112               End If
113           End If


114           If lngCod_Repres = 565 And lngAno_mes = 200508 Then
115               dblSaldo_atual_a_receber = 0
116           Else
                  'dblSaldo_atual_a_receber = Format(ss!Saldo_atual_a_receber, "0.00")
117               dblSaldo_atual_a_receber = Format(dblSaldo_atual_t, "0.00")
118           End If

119           dblCorrecao1 = 0
120           If (ss!saldo_anterior + Val(ss!Vl_correcao) + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO) < 0 Then
121               dblCorrecao1 = ((ss!saldo_anterior + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO) * -1)
122           Else
123               dblCorrecao1 = Val(ss!Vl_correcao)
124           End If

125           Print #1, ss!cod; Tab(7); _
                        ss!Repres; Tab(24); ; ss!dt_empres; _
                        Tab(34); Format(ss!vl_emprestado, "0.00"); Tab(47); Format(ss!vl_parc_mensal, "0.00"); _
                        Tab(62); ss!qtd_parc; Tab(77); Format(ss!total_recebido, "0.00"); _
                        Tab(92); Format(ss!saldo_anterior, "0.00"); Tab(107); Format(IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0), "0.00"); _
                        Tab(122); Format(ss!Vl_parc_receb_mes, "0.00"); Tab(134); Format(ss!Vl_parc_receb_mes_AVULSO, "0.00"); Tab(148); Format(dblCorrecao1, "0.00"); _
                        Tab(162); IIf(ss!situacao = 9, "0.00", CDbl(Format(IIf((ss!saldo_anterior + dblCorrecao1 + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO) < 0, "0.00", _
                                                                               (ss!saldo_anterior + dblCorrecao1 + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO)), "0.00")))

              'marici em 28/11/06
126           dblDiferenca = 0
127           dblDiferenca_total = 0
              '  If (ss!saldo_anterior + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO) < 0 Then
              '    dblDiferenca = (ss!saldo_anterior + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO)
              '    dblDiferenca_total = dblDiferenca_total + dblDiferenca
              '  End If


128           OraParameters.Remove "ano_mes"
129           OraParameters.Add "ano_mes", lngAno_mes, 1
130           OraParameters.Remove "emp"
131           OraParameters.Add "emp", ss!cod_emprestimo, 1
132           OraParameters.Remove "repres"
133           OraParameters.Add "repres", lngCod_Repres, 1
134           OraParameters.Remove "saldo"
135           OraParameters.Add "saldo", IIf(ss!situacao = 9, CDbl("0.00"), CDbl(Format(IIf((ss!saldo_anterior + dblCorrecao1 + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO) < 0, 0, _
                                                                                            (ss!saldo_anterior + dblCorrecao1 + IIf(lngAno_mes = Format(ss!dt_empres, "yyyymm"), dblVL_Mes, 0) - ss!Vl_parc_receb_mes - ss!Vl_parc_receb_mes_AVULSO)), "0.00"))) + dblDiferenca, 1


              'montar SQL
136           V_SQL = "Begin producao.PCK_VDA860.pr_saldo_emprestimo(:emp,:repres,:ano_mes,:saldo,:vErro);END;"

137           oradatabase.ExecuteSQL V_SQL


138           ss.MoveNext
139       Next


140       Print #1, "  "; Tab(7); _
                    "TOTAL"; Tab(24); ; "       "; _
                    Tab(34); Format(dblVl_emprestado, "0.00"); Tab(47); Format(dblVl_parc_mensal, "0.00"); _
                    Tab(62); "          "; Tab(77); Format(dblTotal_recebido, "0.00"); _
                    Tab(92); Format(dblSaldo_ant_Final, "0.00"); Tab(107); Format(dblVL_Emp_mes, "0.00"); _
                    Tab(122); Format(dblVl_parc_receb, "0.00"); Tab(134); Format(dblVl_parc_receb_AVULSO, "0.00"); Tab(148); Format(dblVl_correcao, "0.00"); _
                    Tab(162); Format(dblSaldo_atual, "0.00")
141       Print #1,

          ' dblDiferenca_total = dblDiferenca_total * -1
142       Printer.FontBold = True
143       Print #1,

144       Print #1, "  "; Tab(7); _
                    "TOTAL"; Tab(24); ; "       "; _
                    Tab(32); Format(dblVl_emprestado_t, "0.00"); Tab(47); Format(dblVl_parc_mensal_t, "0.00"); _
                    Tab(62); "          "; Tab(77); Format(dblTotal_recebido_t, "0.00"); _
                    Tab(92); Format(dblSaldo_ant_t_Final, "0.00"); Tab(107); Format(dblVl_emp_mes_t, "0.00"); _
                    Tab(122); Format(dblVl_parc_receb_t, "0.00"); Tab(134); Format(dblVl_parc_receb_AVULSO_t, "0.00"); Tab(148); Format(dblVl_correcao_t, "0.00"); _
                    Tab(162); Format(dblSaldo_atual_t + dblDiferenca_total, "0.00")
          'Print #1,

145       Print #1,
146       Print #1, "RESUMO: "
147       Print #1,
148       Print #1, "SALDO ANTERIOR : "; Tab(24); ; Format(dblSaldo_ant_t_Final, "0.00")
149       Print #1, "EMPR�STIMOS : "; Tab(24); ; Format(dblVl_emp_mes_t, "0.00")
150       Print #1, "RECEBIMENTOS : "; Tab(24); ; Format((CDbl(dblVl_parc_receb_t) + CDbl(dblVl_parc_receb_AVULSO_t)), "0.00")
151       Print #1, "CORRE��O (*) : "; Tab(24); ; Format(dblVl_correcao_t, "0.00")
152       Print #1, "SALDO ATUAL A RECEB: "; Tab(24); ; Format(dblSaldo_atual_t + dblDiferenca_total, "0.00")


          ' Print #1, "(*) CORRE��O = Valor da parcela recebida no m�s menos o valor da parcela mensal"


153       Close #1


154       MsgBox "Posi��o de empr�stimo gerada com sucesso", vbInformation, "Aten��o"

155       Screen.MousePointer = 0

End Sub

Public Sub PR_REL_POSICAO_EMP_CANCEL(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCod_Repres As Long

          Dim dblVl_emprestado As Double
          Dim dblVl_parc_mensal As Double
          Dim dblTotal_recebido As Double
          Dim dblSaldo_ant As Double
          Dim dblVL_Emp_mes As Double
          Dim dblVl_parc_receb As Double
          Dim dblVl_correcao As Double
          Dim dblSaldo_atual As Double

          Dim dblVl_emprestado_t As Double
          Dim dblVl_parc_mensal_t As Double
          Dim dblTotal_recebido_t As Double
          Dim dblSaldo_ant_t As Double
          Dim dblVl_emp_mes_t As Double
          Dim dblVl_parc_receb_t As Double
          Dim dblVl_correcao_t As Double
          Dim dblSaldo_atual_t As Double



1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11


3         strArquivo = "EMP_CANCEL_" & lngAno_mes & ".TXT"

4         OraParameters.Remove "ano_mes"
5         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
6         V_SQL = "Begin producao.pck_sim070.pr_emprest_cancel(:vCursor,:ano_mes,:vErro);END;"

7         oradatabase.ExecuteSQL V_SQL

8         Set ss = oradatabase.Parameters("vCursor").Value

9         If oradatabase.Parameters("vErro").Value <> 0 Then
10            Screen.MousePointer = 0
11            Call Process_Line_Errors(SQL)
12            Exit Sub
13        End If

14        If ss.EOF And ss.BOF Then
15            Screen.MousePointer = 0
16            MsgBox "N�o h� informa��es dispon�veis para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
17            Exit Sub
18        End If

          'INICIALIZANDO VARI�VEIS
19        lngCod_Repres = 0

20        Open strPathArquivo & strArquivo For Output As #1

21        Printer.Font = "VERDANA"
22        Printer.FontSize = 8
23        Print #1,
24        Print #1,
25        Printer.FontBold = True
26        Print #1, "COMERCIAL AUTOMOTIVA S.A."
27        Print #1,
28        Printer.FontBold = False

29        Print #1, "POSI��O DE EMPR�STIMO A REPRESENTANTES CANCELADOS - MENSAL"
30        Print #1,
31        Print #1,
32        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
33        Print #1,
34        Print #1, "COD;"; "REPRES;"; "VALOR EMP."; _
                    "MOTIVO"
35        Print #1,



36        For i = 1 To ss.RecordCount

37            Print #1, ss!cod_repres & ";"; _
                        ss!PSEUDONIMO & ";"; Format(ss!VL_EMPRESTIMO, "0.00") & ";"; _
                        ss!DESC_MOTIVO


38            ss.MoveNext
39        Next

40        Close #1


41        MsgBox "Empr�stimo cancelado gerada com sucesso", vbInformation, "Aten��o"

42        Screen.MousePointer = 0


End Sub

Public Sub PR_REL_CONTAB_PF(lngAno_mes As Long)
          Dim i As Long
          Dim ss1 As Object
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCentro_Custo As Long
          Dim dblT5110100_C As Double
          Dim dblT5110700_C As Double
          Dim dblT5120300_C As Double
          Dim dblT5310800_C As Double
          Dim dblT1240580_C As Double
          Dim dblT2140580_C As Double
          Dim dblT2140380_C As Double
          Dim dblT2150700_C As Double
          Dim dblTAdto_C As Double
          Dim dblTTotal_C As Double
          Dim dblT5110100_T As Double
          Dim dblT5110700_T As Double
          Dim dblT5120300_T As Double
          Dim dblT5310800_T As Double
          Dim dblT1240580_T As Double
          Dim dblT2140580_T As Double
          Dim dblT2140380_T As Double
          Dim dblT2150700_T As Double
          Dim dblTAdto_T As Double
          Dim dblTTotal_T As Double

          Dim dblComis_Ant As Double
          Dim dblTotal_Geral As Double
          Dim dblComis_Ant_T As Double
          Dim dblTotal_Geral_T As Double
          Dim dblComis As Double

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11

3         lngCentro_Custo = 0
4         strArquivo = "C" & lngAno_mes & "_PF.TXT"

5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
7         V_SQL = "Begin producao.PCK_VDA860.pr_contabrel_mes_pf(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Screen.MousePointer = 0
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If

15        If ss.EOF And ss.BOF Then
16            Screen.MousePointer = 0
17            MsgBox "N�o h� informa��es dispon�veis para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
18            Exit Sub
19        End If

          'INICIALIZANDO VARI�VEIS
20        dblT5110100_C = 0
21        dblT5110700_C = 0
22        dblT5120300_C = 0
23        dblT5310800_C = 0
24        dblT1240580_C = 0
25        dblT2140580_C = 0
26        dblT2140380_C = 0
27        dblT2150700_C = 0
28        dblTAdto_C = 0
29        dblTTotal_C = 0
30        dblT5110100_T = 0
31        dblT5110700_T = 0
32        dblT5120300_T = 0
33        dblT5310800_T = 0
34        dblT1240580_T = 0
35        dblT2140580_T = 0
36        dblT2140380_T = 0
37        dblT2150700_T = 0
38        dblTAdto_T = 0
39        dblTTotal_T = 0
40        dblComis_Ant = 0
41        dblComis_Ant_T = 0
42        dblTotal_Geral = 0
43        dblTotal_Geral_T = 0

44        Open strPathArquivo & strArquivo For Output As #1

45        Printer.Font = "VERDANA"
46        Printer.FontSize = 8
47        Print #1,
48        Print #1,
49        Printer.FontBold = True
50        Print #1, "COMERCIAL AUTOMOTIVA S.A."
51        Print #1,
52        Printer.FontBold = False

53        Print #1, "RELAT�RIO DE COMISS�O PARA REPRESENTANTES - MENSAL - PESSOA F�SICA"
54        Print #1,
55        Print #1, "Referente : " & Mid(lngAno_mes, 5, 2) & "/" & Mid(lngAno_mes, 1, 4)
56        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
57        Print #1,
58        Print #1, "CC"; Tab(7); "COD"; Tab(12); "REPRES"; Tab(23); "T51101"; _
                    Tab(34); "T51224"; Tab(49); "T51203"; Tab(64); "T53108"; Tab(79); "T12405"; _
                    Tab(94); "T21405"; Tab(109); "T21405"; Tab(124); "T21507"; Tab(136); "T.ADTO"; _
                    Tab(150); "TOTAL"; Tab(164); "COMIS.ANT."; Tab(182); "TOTAL"
59        Print #1, "  "; Tab(7); "   "; Tab(12); "      "; Tab(23); "COMIS"; _
                    Tab(34); "PREMIOS"; Tab(49); "BRINDE"; Tab(64); "REEMB.EQ."; Tab(79); "COM.ANT."; _
                    Tab(94); "IRRF"; Tab(109); "INSS"; Tab(124); "INS.SALDO"; Tab(136); "      "; _
                    Tab(150); "LIQ."; Tab(164); "A PAGAR"; Tab(182); "GERAL"
60        Print #1,



61        For i = 1 To ss.RecordCount
62            OraParameters.Remove "ano_mes"
63            OraParameters.Add "ano_mes", lngAno_mes, 1
64            OraParameters.Remove "cod_repres"
65            OraParameters.Add "cod_repres", ss!cod_repres, 1
66            OraParameters.Remove "comis"
67            OraParameters.Add "comis", 0, 2


              'montar SQL
68            V_SQL = "Begin producao.PCK_VDA860.pr_comis_aberta(:ano_mes,:cod_repres,:comis,:vErro);END;"

69            oradatabase.ExecuteSQL V_SQL

70            dblComis = IIf(IsNull(oradatabase.Parameters("comis").Value), 0, oradatabase.Parameters("comis").Value)



71            If lngCentro_Custo <> 0 And _
                 ss!cod_centro_custo <> lngCentro_Custo Then

72                If dblTotal_Geral <> 0 Then
73                    Print #1, "  "; Tab(7); "   "; Tab(12); _
                                "TOTAL"; Tab(23); Format(dblT5110100_C, "0.00"); _
                                Tab(34); Format(dblT5110700_C, "0.00"); Tab(49); Format(dblT5120300_C, "0.00"); _
                                Tab(64); Format(dblT5310800_C, "0.00"); Tab(79); Format(dblT1240580_C, "0.00"); _
                                Tab(94); Format(dblT2140580_C, "0.00"); Tab(109); Format(dblT2140380_C, "0.00"); _
                                Tab(124); Format(dblT2150700_C, "0.00"); Tab(136); Format(dblTAdto_C, "0.00"); _
                                Tab(150); Format(dblTTotal_C, "0.00"); Tab(164); dblComis_Ant; Tab(182); dblTotal_Geral
74                    Print #1,
75                End If

76                dblT5110100_C = 0
77                dblT5110700_C = 0
78                dblT5120300_C = 0
79                dblT5310800_C = 0
80                dblT1240580_C = 0
81                dblT2140580_C = 0
82                dblT2140380_C = 0
83                dblT2150700_C = 0
84                dblTAdto_C = 0
85                dblTTotal_C = 0
86                dblComis_Ant = 0
87                dblTotal_Geral = 0
88            End If

89            lngCentro_Custo = ss!cod_centro_custo
90            dblT5110100_C = dblT5110100_C + ss!t51101000
91            dblT5110700_C = dblT5110700_C + ss!t51107000
92            dblT5120300_C = dblT5120300_C + ss!t51203000
93            dblT5310800_C = dblT5310800_C + ss!t53108000
94            dblT1240580_C = dblT1240580_C + ss!t12405803
95            dblT2140580_C = dblT2140580_C + ss!t21405800
96            dblT2140380_C = dblT2140380_C + ss!t21403800
97            dblT2150700_C = dblT2150700_C + ss!t21507000_ins
98            dblTAdto_C = dblTAdto_C + ss!tadto
99            dblTTotal_C = dblTTotal_C + ss!t21507_adto
100           dblComis_Ant = dblComis_Ant + dblComis
101           dblTotal_Geral = dblTTotal_C + dblComis_Ant

102           dblT5110100_T = dblT5110100_T + ss!t51101000
103           dblT5110700_T = dblT5110700_T + ss!t51107000
104           dblT5120300_T = dblT5120300_T + ss!t51203000
105           dblT5310800_T = dblT5310800_T + ss!t53108000
106           dblT1240580_T = dblT1240580_T + ss!t12405803
107           dblT2140580_T = dblT2140580_T + ss!t21405800
108           dblT2140380_T = dblT2140380_T + ss!t21403800
109           dblT2150700_T = dblT2150700_T + ss!t21507000_ins
110           dblTAdto_T = dblTAdto_T + ss!tadto
111           dblTTotal_T = dblTTotal_T + ss!t21507_adto
112           dblComis_Ant_T = dblComis_Ant_T + dblComis
113           dblTotal_Geral_T = dblTTotal_T + dblComis_Ant_T

114           If CDbl(ss!t21507_adto + dblComis + ss!t51101000) <> 0 Then
115               Print #1, ss!cod_centro_custo; Tab(7); ss!cod_repres; Tab(12); _
                            Mid(ss!PSEUDONIMO, 1, 10); Tab(23); Format(ss!t51101000, "0.00"); _
                            Tab(34); Format(ss!t51107000, "0.00"); Tab(49); Format(ss!t51203000, "0.00"); _
                            Tab(64); Format(ss!t53108000, "0.00"); Tab(79); Format(ss!t12405803, "0.00"); _
                            Tab(94); Format(ss!t21405800, "0.00"); Tab(109); Format(ss!t21403800, "0.00"); _
                            Tab(124); Format(ss!t21507000_ins, "0.00"); Tab(136); Format(ss!tadto, "0.00"); _
                            Tab(150); Format(ss!t21507_adto, "0.00"); Tab(164); dblComis; Tab(182); CDbl(ss!t21507_adto + dblComis)
116           End If

117           ss.MoveNext
118       Next

119       If dblTotal_Geral <> 0 Then
120           Print #1, "  "; Tab(7); "   "; Tab(12); _
                        "TOTAL"; Tab(23); Format(dblT5110100_C, "0.00"); _
                        Tab(34); Format(dblT5110700_C, "0.00"); Tab(49); Format(dblT5120300_C, "0.00"); _
                        Tab(64); Format(dblT5310800_C, "0.00"); Tab(79); Format(dblT1240580_C, "0.00"); _
                        Tab(94); Format(dblT2140580_C, "0.00"); Tab(109); Format(dblT2140380_C, "0.00"); _
                        Tab(124); Format(dblT2150700_C, "0.00"); Tab(136); Format(dblTAdto_C, "0.00"); _
                        Tab(150); Format(dblTTotal_C, "0.00"); Tab(164); dblComis_Ant; Tab(182); dblTotal_Geral
121           Print #1,
122       End If


123       If dblTotal_Geral_T <> 0 Then
124           Print #1, "  "; Tab(7); "   "; Tab(12); _
                        "TOTAL"; Tab(23); Format(dblT5110100_T, "0.00"); _
                        Tab(34); Format(dblT5110700_T, "0.00"); Tab(49); Format(dblT5120300_T, "0.00"); _
                        Tab(64); Format(dblT5310800_T, "0.00"); Tab(79); Format(dblT1240580_T, "0.00"); _
                        Tab(94); Format(dblT2140580_T, "0.00"); Tab(109); Format(dblT2140380_T, "0.00"); _
                        Tab(124); Format(dblT2150700_T, "0.00"); Tab(136); Format(dblTAdto_T, "0.00"); _
                        Tab(150); Format(dblTTotal_T, "0.00"); Tab(164); dblComis_Ant_T; Tab(182); dblTotal_Geral_T
125           Print #1,
126       End If
127       Close #1


128       Screen.MousePointer = 0


End Sub

Public Sub PR_REL_CONTAB_PF_Sem(lngAno_mes As Long)
          Dim i As Long
          Dim ss1 As Object
          Dim strArquivo As String
          Dim strNome_Arquivo As String
          Dim lngCentro_Custo As Long


1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11

3         lngCentro_Custo = 0
4         strArquivo = "Sem_pagto_" & lngAno_mes & "_PF.TXT"

5         OraParameters.Remove "ano_mes"
6         OraParameters.Add "ano_mes", lngAno_mes, 1


          'montar SQL
7         V_SQL = "Begin producao.PCK_VDA860.pr_contabrel_sem_mes_pf(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        If oradatabase.Parameters("vErro").Value <> 0 Then
11            Screen.MousePointer = 0
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If

15        If ss.EOF And ss.BOF Then
16            Screen.MousePointer = 0
17            MsgBox "N�o h� informa��es dispon�veis(PF) para gerar relat�rio para o per�odo escolhido", vbExclamation, "Aten��o"
18            Exit Sub
19        End If


20        Open strPathArquivo & strArquivo For Output As #1

21        Printer.Font = "VERDANA"
22        Printer.FontSize = 8
23        Print #1,
24        Print #1,
25        Printer.FontBold = True
26        Print #1, "COMERCIAL AUTOMOTIVA S.A."
27        Print #1,
28        Printer.FontBold = False

29        Print #1, "RELAT�RIO DE COMISS�O SEM PAGTO PARA REPRESENTANTES - MENSAL - PESSOA F�SICA"
30        Print #1,
31        Print #1, "Referente : " & Mid(lngAno_mes, 5, 2) & "/" & Mid(lngAno_mes, 1, 4)
32        Print #1, "Gerado em : " & Format(Now, "DD/MM/YY HH:MM:SS")
33        Print #1,
34        Print #1, "CC"; Tab(5); "COD"; Tab(10); "REPRES"; Tab(30); "COMIS"; _
                    Tab(37); "MOTIVO"
35        Print #1,



36        For i = 1 To ss.RecordCount
37            Print #1, ss!CENTRO_CUSTO; Tab(5); ss!cod_repres; Tab(10); _
                        ss!PSEUDONIMO; Tab(30); Format(ss!vl_comissao, "0.00"); _
                        Tab(37); ss!DESC_MOTIVO
38            ss.MoveNext
39        Next

40        Close #1


41        Screen.MousePointer = 0



End Sub

Public Sub PR_ARQ_PGTO_TLMKT(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1

verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & Format(Now, "ddmmyy") & _
                                Format(bSeq_Arquivo, "00") & "SPC" & ".txt"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
11        End If

          'montar SQL
12        OraParameters.Remove "seq"
13        OraParameters.Add "seq", 0, 2
14        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ(:seq,:vErro);END;"

15        oradatabase.ExecuteSQL V_SQL
16        Set ss = oradatabase.Parameters("vCursor").Value


17        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


18        OraParameters.Remove "ano_mes"
19        OraParameters.Add "ano_mes", lngAno_mes, 1
20        OraParameters.Remove "dia"
21        OraParameters.Add "dia", intDia, 1

          'montar SQL
22        V_SQL = "Begin producao.PCK_VDA860.pr_arq_pgto_tlmkt(:vCursor,:ano_mes,:dia,:vErro);END;"

23        oradatabase.ExecuteSQL V_SQL

24        Set ss = oradatabase.Parameters("vCursor").Value

25        If oradatabase.Parameters("vErro").Value <> 0 Then
26            Screen.MousePointer = 0
27            Call Process_Line_Errors(SQL)
28            Exit Sub
29        End If

30        If ss.EOF And ss.BOF Then
31            Screen.MousePointer = 0
32            MsgBox "N�o h� representantes dispon�veis para gerar pagto de telemarketing", vbExclamation, "Aten��o"
33            Exit Sub
34        End If
35        bSeq_Registro = 1
36        dblTotal_Comissao = 0
37        Open strArquivo_Contas_Pagar For Output As #2
38        Print #2, "ARQUIVO DE COMISSAO - PAGAMENTO DE TELEMARKETING DPK "
39        Print #2,
40        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

41        Print #2,
42        Print #2, "COD."; Tab(10); "CGC/CIC"; _
                    Tab(30); "RAZ�O SOCIAL"; Tab(70); "VALOR PAGTO"; Tab(95); "DT.PAGTO"
43        Print #2,

44        Open strNome_Arquivo For Output As #1
45        Print #1, "0" & "1" & "REMESSA" & "11" & "PAGTOS FORNECED" & "01227507" & _
                    "N" & Space(2) & "0001000" & "  " & "COMERCIAL AUTOMOTIVA S.A.     " & "422" & _
                    "BANCO SAFRA S/A" & Format(Now, "DDMMYY") & Space(1) & Space(2) & Space(285) & _
                    Format(bSeq_Arquivo, "000000") & Format(bSeq_Registro, "000000")

46        lngTotal_Registros = ss.RecordCount
47        ReDim Repres(ss.RecordCount)

48        For i = 1 To ss.RecordCount

49            Repres(i) = ss!cod_repres
50            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
51            bSeq_Registro = bSeq_Registro + 1

52            Print #1, "1" & "01" & "45987005000198" & "01227507" & Space(3) & _
                        "0001000" & Space(2) & Space(25); Tab(63); Format(ss!cic_cgc, "00000000000000") & _
                        "REC" & Format(lngSeq_Repres_Banco, "0000000000") & "0" & Space(17) & "C" & _
                        "01" & Format(lngSeq_Repres_Banco, "0000000000") & Format(ss!dt_pagamento, "000000") & _
                        Format(ss!vl_comis, "0000000000000") & IIf((CDbl(ss!vl_comis) / 100) < 5000, "DOC", "TED") & _
                        "237" & Format(ss!cod_agenciA, "0000000") & "000" & _
                        Format(ss!conta_corrente, "0000000000") & Space(20) & "0001000" & Space(55) & _
                        "0000000000000000" & ss!razao_social; Tab(294); Space(54) & "0000000000000" & _
                        Format(ss!dt_pagamento, "000000") & Format(ss!vl_comis, "0000000000000") & "REAL" & _
                        Space(8) & ss!dig_agenciA & Space(2) & Format(bSeq_Registro, "000000")

53            Print #2, ss!cod_repres; Tab(10); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(30); ss!razao_social; Tab(70); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(95); Format(ss!dt_pagamento, "000000")

54            ss.MoveNext
55        Next
56        bSeq_Registro = bSeq_Registro + 1
57        Print #1, "9" & Space(123) & Format(dblTotal_Comissao, "000000000000000") & _
                    Space(109) & "000000000000000" & Space(82) & "000000000000000" & _
                    Space(4) & Format(dblTotal_Comissao, "000000000000000") & Space(15) & _
                    Format(bSeq_Registro, "000000")
58        Print #2,
59        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
60        Close #1
61        Close #2
62        For i = 1 To lngTotal_Registros
63            OraParameters.Remove "ano_mes"
64            OraParameters.Add "ano_mes", lngAno_mes, 1
65            OraParameters.Remove "repres"
66            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
67            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,2,:vErro);END;"

68            oradatabase.ExecuteSQL V_SQL

69            Set ss = oradatabase.Parameters("vCursor").Value
70        Next

          '  OraParameters.Remove "ano_mes"
          '  OraParameters.Add "ano_mes", lngAno_mes, 1


          ' V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"


          'oradatabase.ExecuteSQL V_SQL

71        MsgBox "Lembre-se de encerrar o fechamento", vbInformation, "Aten��o"

72        Screen.MousePointer = 0
73        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_PGTO_PROMO_BRADESCO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long
          'NOVAS VARI�VEIS PARA TRATA CONTABILIZA��O
          Dim strArquivo_Contabil As String
          Dim strConta As String
          Dim lngDia_movimento As Long
          Dim dblTotal_Comissao_CP As Double
          Dim lngCC As String
          Dim dblVl_comis_CP As Double

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1

verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & "FP" & Format(Now, "ddmm") & "P" & ".REM"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & "P.txt"
11        End If

          'CONTABILIZACAO
12        strArquivo_Contabil = strPathArquivo & "PGCOM" & Format(intDia, "00") & Format(Now, "mmyy") & "P.txt"
13        lngDia_movimento = Format(intDia, "00") & Format(Now, "MMYYYY")

          'montar SQL
14        OraParameters.Remove "seq"
15        OraParameters.Add "seq", 0, 2
16        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ_banco(:seq,:vErro);END;"

17        oradatabase.ExecuteSQL V_SQL
18        Set ss = oradatabase.Parameters("vCursor").Value


19        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


20        OraParameters.Remove "ano_mes"
21        OraParameters.Add "ano_mes", lngAno_mes, 1
22        OraParameters.Remove "dia"
23        OraParameters.Add "dia", intDia, 1

          'montar SQL
24        V_SQL = "Begin producao.PCK_VDA860.pr_arq_pgto_PROMO(:vCursor,:ano_mes,:dia,:vErro);END;"

25        oradatabase.ExecuteSQL V_SQL

26        Set ss = oradatabase.Parameters("vCursor").Value

27        If oradatabase.Parameters("vErro").Value <> 0 Then
28            Screen.MousePointer = 0
29            Call Process_Line_Errors(SQL)
30            Exit Sub
31        End If

32        If ss.EOF And ss.BOF Then
33            Screen.MousePointer = 0
34            MsgBox "N�o h� PROMOTORES dispon�veis para gerar pagto", vbExclamation, "Aten��o"
35            Exit Sub
36        End If
37        bSeq_Registro = 1
38        dblTotal_Comissao = 0

          'ARQUIVO CONTABILIZACAO
39        lngCC = "0"
40        Open strArquivo_Contabil For Output As #3


41        Open strArquivo_Contas_Pagar For Output As #2
42        Print #2, "ARQUIVO DE COMISSAO - PAGAMENTO DE PROMOTOR DPK "
43        Print #2,
44        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

45        Print #2,
46        Print #2, "CC"; Tab(10); "COD."; Tab(20); "CGC/CIC"; _
                    Tab(40); "RAZ�O SOCIAL"; Tab(80); "VALOR PAGTO"; Tab(105); "DT.PAGTO"; _
                    Tab(115); "AG."; Tab(120); "CTA.C."; Tab(130); "DIG"
47        Print #2,
          'agencia bradesco alterada em 09/12/05 de 311 para 2002
48        Open strNome_Arquivo For Output As #1
49        Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
                    "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
                    "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
                    frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

50        lngTotal_Registros = ss.RecordCount
51        ReDim Repres(ss.RecordCount)

52        For i = 1 To ss.RecordCount

53            Repres(i) = ss!cod_repres
54            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
55            bSeq_Registro = bSeq_Registro + 1

              'INF.CONTABILIZA��O
56            If lngCC = "0" Then
57                lngCC = ss!cod_centro_custo
58                dblVl_comis_CP = ss!vl_comis_cp
59            Else
60                If lngCC <> ss!cod_centro_custo Then
                      'strConta = strConta & "9" & lngCC & Space(17)
61                    strConta = strConta & lngCC & Space(5) & _
                                 Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
62                    lngCC = ss!cod_centro_custo
63                    dblVl_comis_CP = ss!vl_comis_cp
64                Else
65                    dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
66                End If
67            End If

68            Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
                        Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
                        Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
                        Space(6) & Space(44) & Format(bSeq_Registro, "000000")


69            Print #2, ss!cod_centro_custo; Tab(10); ss!cod_repres; Tab(20); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(40); ss!razao_social; Tab(80); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(105); Format(ss!dt_pagamento, "000000"); _
                        Tab(115); ss!cod_agenciA; Tab(120); ss!conta_corrente; Tab(130); ss!dig_ct_corrente

70            ss.MoveNext
71        Next
          'strConta = strConta & "9" & lngCC & Space(17)
72        strConta = strConta & lngCC & Space(5) & _
                     Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")


73        bSeq_Registro = bSeq_Registro + 1
74        Print #1, "9" & Format(dblTotal_Comissao, "0000000000000") & _
                    Space(180) & Format(bSeq_Registro, "000000")
75        Print #2,
76        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
77        dblTotal_Comissao_CP = dblTotal_Comissao / 100

          'CONTABILIZACAO
78        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21395800            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(45, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"    'N�O QUEBRA POR CENTRO DE CUSTO & strConta

79        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000"); Tab(37); "11120805            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"


80        Close #1
81        Close #2
82        Close #3
83        For i = 1 To lngTotal_Registros
84            OraParameters.Remove "ano_mes"
85            OraParameters.Add "ano_mes", lngAno_mes, 1
86            OraParameters.Remove "repres"
87            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
88            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,2,:vErro);END;"

89            oradatabase.ExecuteSQL V_SQL

90            Set ss = oradatabase.Parameters("vCursor").Value
91        Next

          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", lngAno_mes, 1


          'V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"


          'oradatabase.ExecuteSQL V_SQL

92        MsgBox "Lembre-se de encerrar o fechamento", vbInformation, "Aten��o"

93        Screen.MousePointer = 0
94        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_PGTO_TLMKT_BRADESCO(lngAno_mes As Long, intDia As Integer)
          Dim i As Long
          Dim bSeq_Arquivo As Byte
          Dim bSeq_Registro As Long
          Dim strArquivo As String
          Dim strArquivo_Contas_Pagar As String
          Dim strNome_Arquivo As String
          Dim lngSeq_Repres_Banco As Long
          Dim dblTotal_Comissao As Double
          Dim Repres() As Long
          Dim lngTotal_Registros As Long
          'NOVAS VARI�VEIS PARA TRATA CONTABILIZA��O
          Dim strArquivo_Contabil As String
          Dim strConta As String
          Dim lngDia_movimento As Long
          Dim dblTotal_Comissao_CP As Double
          Dim lngCC As String
          Dim dblVl_comis_CP As Double

1         strPathArquivo = "C:\ARQUIVO_COMISSAO\"
          'VERIFICA SEQUENCIA DO ARQUIVO

2         Screen.MousePointer = 11
3         bSeq_Arquivo = 1

verifica:
4         strArquivo = Dir(strPathArquivo & Format(Now, "ddmmyy") & _
                           Format(bSeq_Arquivo, "00") & "SPC" & ".txt")
5         If strArquivo <> "" Then
6             bSeq_Arquivo = bSeq_Arquivo + 1
7             GoTo verifica
8         Else
9             strNome_Arquivo = strPathArquivo & "FP" & Format(Now, "ddmm") & "F" & ".REM"

10            strArquivo_Contas_Pagar = strPathArquivo & Format(Now, "ddmmyy") & _
                                        Format(bSeq_Arquivo, "00") & "SPCC" & ".txt"
11        End If

          'CONTABILIZACAO
12        strArquivo_Contabil = strPathArquivo & "PGCOM" & Format(intDia, "00") & Format(Now, "mmyy") & "F.txt"
13        lngDia_movimento = Format(intDia, "00") & Format(Now, "MMYYYY")

          'montar SQL
14        OraParameters.Remove "seq"
15        OraParameters.Add "seq", 0, 2
16        V_SQL = "Begin producao.PCK_VDA860.PR_SEQ_ARQ_banco(:seq,:vErro);END;"

17        oradatabase.ExecuteSQL V_SQL
18        Set ss = oradatabase.Parameters("vCursor").Value


19        lngSeq_Repres_Banco = oradatabase.Parameters("seq").Value


20        OraParameters.Remove "ano_mes"
21        OraParameters.Add "ano_mes", lngAno_mes, 1
22        OraParameters.Remove "dia"
23        OraParameters.Add "dia", intDia, 1

          'montar SQL
24        V_SQL = "Begin producao.PCK_VDA860.pr_arq_pgto_tlmkt(:vCursor,:ano_mes,:dia,:vErro);END;"

25        oradatabase.ExecuteSQL V_SQL

26        Set ss = oradatabase.Parameters("vCursor").Value

27        If oradatabase.Parameters("vErro").Value <> 0 Then
28            Screen.MousePointer = 0
29            Call Process_Line_Errors(SQL)
30            Exit Sub
31        End If

32        If ss.EOF And ss.BOF Then
33            Screen.MousePointer = 0
34            MsgBox "N�o h� representantes dispon�veis para gerar pagto de telemarketing", vbExclamation, "Aten��o"
35            Exit Sub
36        End If
37        bSeq_Registro = 1
38        dblTotal_Comissao = 0

          'ARQUIVO CONTABILIZACAO
39        lngCC = "0"
40        Open strArquivo_Contabil For Output As #3


41        Open strArquivo_Contas_Pagar For Output As #2
42        Print #2, "ARQUIVO DE COMISSAO - PAGAMENTO DE TELEMARKETING DPK "
43        Print #2,
44        Print #2, "ARQUIVO GERADO EM : " & Format(Now, "DD/MM/YY HH:MM:SS")

45        Print #2,
46        Print #2, "CC"; Tab(10); "COD."; Tab(20); "CGC/CIC"; _
                    Tab(40); "RAZ�O SOCIAL"; Tab(80); "VALOR PAGTO"; Tab(105); "DT.PAGTO"; _
                    Tab(115); "AG."; Tab(120); "CTA.C."; Tab(130); "DIG"
47        Print #2,
          'agencia bradesco alterada em 09/12/05 de 311 para 2002
48        Open strNome_Arquivo For Output As #1
49        Print #1, "0" & "1" & "REMESSA" & "03" & "CREDITO C/C    " & "02002" & _
                    "07050" & "0051999" & "5" & Space(2); "00663" & "COMERCIAL AUTOMOTIVA S.A." & _
                    "237" & "BRADESCO       " & Format(Now, "DDMMYY") & "01600" & "BPI" & _
                    frmComis.txtPagto_Bradesco & Format(Now, "MMYY") & Space(80) & Format(bSeq_Registro, "000000")

50        lngTotal_Registros = ss.RecordCount
51        ReDim Repres(ss.RecordCount)

52        For i = 1 To ss.RecordCount

53            Repres(i) = ss!cod_repres
54            dblTotal_Comissao = CDbl(dblTotal_Comissao) + CDbl(ss!vl_comis)
55            bSeq_Registro = bSeq_Registro + 1

              'INF.CONTABILIZA��O
56            If lngCC = "0" Then
57                lngCC = ss!cod_centro_custo
58                dblVl_comis_CP = ss!vl_comis_cp
59            Else
60                If lngCC <> ss!cod_centro_custo Then
                      'strConta = strConta & "9" & lngCC & Space(17)
61                    strConta = strConta & lngCC & Space(5) & _
                                 Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")
62                    lngCC = ss!cod_centro_custo
63                    dblVl_comis_CP = ss!vl_comis_cp
64                Else
65                    dblVl_comis_CP = dblVl_comis_CP + ss!vl_comis_cp
66                End If
67            End If

68            Print #1, "1" & Space(61) & Format(ss!cod_agenciA, "00000") & "07050" & _
                        Format(ss!conta_corrente, "0000000") & ss!dig_ct_corrente & Space(2) & ss!razao_social & _
                        Space(10) & Format(ss!cod_repres, "000000") & Format(ss!vl_comis, "0000000000000") & "298" & _
                        Space(6) & Space(44) & Format(bSeq_Registro, "000000")


69            Print #2, ss!cod_centro_custo; Tab(10); ss!cod_repres; Tab(20); Format(ss!cic_cgc, "00000000000000"); _
                        Tab(40); ss!razao_social; Tab(80); Format((ss!vl_comis / 100), "0.00"); _
                        Tab(105); Format(ss!dt_pagamento, "000000"); _
                        Tab(115); ss!cod_agenciA; Tab(120); ss!conta_corrente; Tab(130); ss!dig_ct_corrente

70            ss.MoveNext
71        Next
          'strConta = strConta & "9" & lngCC & Space(17)
72        strConta = strConta & lngCC & Space(5) & _
                     Space(17 - Len(Format(dblVl_comis_CP, "0.00"))) & Format(dblVl_comis_CP, "0.00")


73        bSeq_Registro = bSeq_Registro + 1
74        Print #1, "9" & Format(dblTotal_Comissao, "0000000000000") & _
                    Space(180) & Format(bSeq_Registro, "000000")
75        Print #2,
76        Print #2, "TOTAL DE PAGAMENTOS"; Tab(70); Format((dblTotal_Comissao / 100), "0.00")
77        dblTotal_Comissao_CP = dblTotal_Comissao / 100

          'CONTABILIZACAO
78        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000") & "21395800            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(45, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"    'N�O QUEBRA POR CENTRO DE CUSTO & strConta

79        Print #3, Format(lngDia_movimento, "00000000") & Format(1, "00000000"); Tab(37); "11120805            "; _
                    Tab(78); Space(17 - Len(Format(dblTotal_Comissao_CP, "0.00"))) & Format(dblTotal_Comissao_CP, "0.00") & Format(1, "00000000"); _
                    "COMISSAO" & Space(186 - Len("COMISSAO")); "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"


80        Close #1
81        Close #2
82        Close #3
83        For i = 1 To lngTotal_Registros
84            OraParameters.Remove "ano_mes"
85            OraParameters.Add "ano_mes", lngAno_mes, 1
86            OraParameters.Remove "repres"
87            OraParameters.Add "repres", Repres(i), 1

              'montar SQL
88            V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_controle_repres(:ano_mes,:repres,2,:vErro);END;"

89            oradatabase.ExecuteSQL V_SQL

90            Set ss = oradatabase.Parameters("vCursor").Value
91        Next

          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", lngAno_mes, 1


          'V_SQL = "Begin producao.PCK_VDA860.pr_encerra_fech(:ano_mes,:vErro);END;"


          'oradatabase.ExecuteSQL V_SQL

92        MsgBox "Lembre-se de encerrar o fechamento", vbInformation, "Aten��o"

93        Screen.MousePointer = 0
94        MsgBox "ARQUIVO GERADO COM SUCESSO", vbInformation, "Aten��o"

End Sub

Public Sub PR_ARQ_CONTABIL_MES_AVULSO(lngAno_mes As Long)
          Dim i As Long
          Dim j As Long
          Dim strArquivo As String
          Dim strArquivo_Contabil As String
          Dim strNome_Arquivo As String
          Dim lngDia_movimento As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim strConta As String
          Dim strDT As Long

1         Screen.MousePointer = 11
2         strPathArquivo = "C:\ARQUIVO_COMISSAO\"

          'montar SQL
3         V_SQL = "Begin producao.PCK_VDA860.PR_ultimo_dia(:vCursor,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL
5         Set ss = oradatabase.Parameters("vCursor").Value

6         lngDia_movimento = ss!ultimo_dia

7         strArquivo_Contabil = strPathArquivo & "REP" & Mid(ss!ultimo_dia, 1, 4) & ".txt"

8         OraParameters.Remove "ano_mes"
9         OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
10        V_SQL = "Begin producao.PCK_VDA860.pr_contabilizacao_mes_avulso(:vCursor,:ano_mes,:vErro);END;"

11        oradatabase.ExecuteSQL V_SQL

12        Set ss = oradatabase.Parameters("vCursor").Value

13        If oradatabase.Parameters("vErro").Value <> 0 Then
14            Screen.MousePointer = 0
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        If ss.EOF And ss.BOF Then
19            Screen.MousePointer = 0
20            MsgBox "N�o h� representantes dispon�veis para gerar arq. contabiliza��o avulso", vbExclamation, "Aten��o"
21            Exit Sub
22        End If

23        Open strArquivo_Contabil For Output As #1

24        ss.MoveFirst
25        For j = 1 To ss.RecordCount

26            strDT = ss!dt
27            OraParameters.Remove "ano_mes"
28            OraParameters.Add "ano_mes", lngAno_mes, 1
29            OraParameters.Remove "dt"
30            OraParameters.Add "dt", CStr(Format(strDT, "00000000")), 1

              'montar SQL
31            V_SQL = "Begin producao.PCK_VDA860.pr_contabilizacao_cc_avulso(:vCursor,:ano_mes,:dt,:vErro);END;"

32            oradatabase.ExecuteSQL V_SQL

33            Set ss2 = oradatabase.Parameters("vCursor").Value

              'telefone
34            strConta = ""
35            ss2.MoveFirst
36            For i = 1 To ss2.RecordCount
37                If ss2!d_54301000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
38                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_54301000, "0.00"))) & Format(ss2!d_54301000, "0.00")
39                End If
40                ss2.MoveNext
41            Next
42            If ss!d_54301000 <> 0 Then
43                Print #1, ss!dt & Format(1, "00000000") & "54301000            "; _
                            Tab(78); Space(17 - Len(Format(ss!d_54301000, "0.00"))) & Format(ss!d_54301000, "0.00") & Format(15, "00000000"); _
                            "REPRESENTANTE" & Space(186 - Len("REPRESENTANTE")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
44            End If

45            strConta = ""
46            ss2.MoveFirst
47            For i = 1 To ss2.RecordCount
48                If ss2!c_12798001_TEL <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
49                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!c_12798001_TEL, "0.00"))) & Format(ss2!c_12798001_TEL, "0.00")
50                End If
51                ss2.MoveNext
52            Next
53            If ss!c_12798001_TEL <> 0 Then
54                Print #1, ss!dt & Format(1, "00000000"); Tab(37); "12798001            "; _
                            Tab(78); Space(17 - Len(Format(ss!c_12798001_TEL, "0.00"))) & Format(ss!c_12798001_TEL, "0.00") & Format(15, "00000000"); _
                            "REPRESENTANTE" & Space(186 - Len("REPRESENTANTE")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"
55            End If

              'manut./equip.
56            strConta = ""
57            ss2.MoveFirst
58            For i = 1 To ss2.RecordCount
59                If ss2!d_53108000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
60                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_53108000, "0.00"))) & Format(ss2!d_53108000, "0.00")
61                End If
62                ss2.MoveNext
63            Next
64            If ss!d_53108000 <> 0 Then
65                Print #1, ss!dt & Format(1, "00000000") & "53108000            "; _
                            Tab(78); Space(17 - Len(Format(ss!d_53108000, "0.00"))) & Format(ss!d_53108000, "0.00") & Format(610, "00000000"); _
                            "MANUT/EQUIP" & Space(186 - Len("MANUT/EQUIP")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
66            End If

67            strConta = ""
68            ss2.MoveFirst
69            For i = 1 To ss2.RecordCount
70                If ss2!c_12798001 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
71                    strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_12798001, "0.00"))) & Format(ss2!c_12798001, "0.00")
72                End If
73                ss2.MoveNext
74            Next
75            If ss!c_12798001 <> 0 Then
76                Print #1, ss!dt & Format(1, "00000000"); Tab(37); "12798001            "; _
                            Tab(78); Space(17 - Len(Format(ss!c_12798001, "0.00"))) & Format(ss!c_12798001, "0.00") & Format(610, "00000000"); _
                            "MANUT/EQUIP" & Space(186 - Len("MANUT/EQUIP")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
77            End If

              'pagtos avulsos.
78            strConta = ""
79            ss2.MoveFirst
80            For i = 1 To ss2.RecordCount
81                If ss2!d_21507000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
82                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_21507000, "0.00"))) & Format(ss2!d_21507000, "0.00")
83                End If
84                ss2.MoveNext
85            Next
86            If ss!d_21507000 <> 0 Then
87                Print #1, ss!dt & Format(1, "00000000") & "21507000            "; _
                            Tab(78); Space(17 - Len(Format(ss!d_21507000, "0.00"))) & Format(ss!d_21507000, "0.00") & Format(21, "00000000"); _
                            "REPRESENTANTE" & Space(186 - Len("REPRESENTANTE")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
88            End If

89            strConta = ""
90            ss2.MoveFirst
91            For i = 1 To ss2.RecordCount
92                If ss2!c_12798001_A <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
93                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!c_12798001_A, "0.00"))) & Format(ss2!c_12798001_A, "0.00")
94                End If
95                ss2.MoveNext
96            Next
97            If ss!c_12798001 <> 0 Then
98                Print #1, ss!dt & Format(1, "00000000"); Tab(37); "12798001            "; _
                            Tab(78); Space(17 - Len(Format(ss!c_12798001_A, "0.00"))) & Format(ss!c_12798001_A, "0.00") & Format(21, "00000000"); _
                            "REPRESENTANTE" & Space(186 - Len("REPRESENTANTE")); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"
99            End If



100           ss.MoveNext
101       Next

          'contabiliza��o de emprestimos
102       OraParameters.Remove "ano_mes"
103       OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
104       V_SQL = "Begin producao.PCK_VDA860.pr_contabilizacao_emprestimo(:vCursor,:ano_mes,:vErro);END;"

105       oradatabase.ExecuteSQL V_SQL

106       Set ss = oradatabase.Parameters("vCursor").Value

107       If oradatabase.Parameters("vErro").Value <> 0 Then
108           Screen.MousePointer = 0
109           Call Process_Line_Errors(SQL)
110           Exit Sub
111       End If

112       ss.MoveFirst

113       For i = 1 To ss.RecordCount
114           If ss!d_12405803 <> 0 Then
115               Print #1, ss!dt & Format(1, "00000000") & "12405803            "; _
                            Tab(78); Space(17 - Len(Format(ss!d_12405803, "0.00"))) & Format(ss!d_12405803, "0.00") & Format(312, "00000000"); _
                            ss!nome & Space(186 - Len(ss!nome)); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"

116               Print #1, ss!dt & Format(1, "00000000"); Tab(37); "12798001            "; _
                            Tab(78); Space(17 - Len(Format(ss!c_12798001_emp, "0.00"))) & Format(ss!c_12798001_emp, "0.00") & Format(318, "00000000"); _
                            ss!nome & Space(186 - Len(ss!nome)); "169"; _
                            Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"
117           End If


118           ss.MoveNext
119       Next
120       Close #1


121       Screen.MousePointer = 0

End Sub

Public Sub PR_ARQ_CONTABIL_MES(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strArquivo_Contabil As String
          Dim strNome_Arquivo As String
          Dim lngDia_movimento As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim strConta As String

1         Screen.MousePointer = 11
2         strPathArquivo = "C:\ARQUIVO_COMISSAO\"

          'montar SQL
3         V_SQL = "Begin producao.PCK_VDA860.PR_ULTIMO_DIA(:vCursor,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL
5         Set ss = oradatabase.Parameters("vCursor").Value

6         lngDia_movimento = ss!ultimo_dia

7         strArquivo_Contabil = strPathArquivo & "COM" & Mid(ss!ultimo_dia, 1, 4) & ".txt"

8         OraParameters.Remove "ano_mes"
9         OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
10        V_SQL = "Begin producao.PCK_VDA860.pr_contabilizacao_mes(:vCursor,:ano_mes,:vErro);END;"

11        oradatabase.ExecuteSQL V_SQL

12        Set ss = oradatabase.Parameters("vCursor").Value

13        If oradatabase.Parameters("vErro").Value <> 0 Then
14            Screen.MousePointer = 0
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        If ss.EOF And ss.BOF Then
19            Screen.MousePointer = 0
20            MsgBox "N�o h� representantes dispon�veis para gerar arq. contabiliza��o", vbExclamation, "Aten��o"
21            Exit Sub
22        End If

23        OraParameters.Remove "ano_mes"
24        OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
25        V_SQL = "Begin producao.PCK_VDA860.pr_contabilizacao_mes_cc(:vCursor,:ano_mes,:vErro);END;"

26        oradatabase.ExecuteSQL V_SQL

27        Set ss2 = oradatabase.Parameters("vCursor").Value

28        If oradatabase.Parameters("vErro").Value <> 0 Then
29            Screen.MousePointer = 0
30            Call Process_Line_Errors(SQL)
31            Exit Sub
32        End If


33        OraParameters.Remove "ano_mes"
34        OraParameters.Add "ano_mes", lngAno_mes, 1


35        Open strArquivo_Contabil For Output As #1

36        If ss!d_51101000 > 0 Then
              'debitos
37            strConta = ""
38            ss2.MoveFirst
39            For i = 1 To ss2.RecordCount
40                If ss2!d_51101000 <> 0 Then
                      '            strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
41                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_51101000, "0.00"))) & Format(ss2!d_51101000, "0.00")
42                End If
43                ss2.MoveNext
44            Next
45            Print #1, lngDia_movimento & Format(1, "00000000") & "51101000            "; _
                        Tab(78); Space(17 - Len(Format(ss!d_51101000, "0.00"))) & Format(ss!d_51101000, "0.00") & Format(22, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
46        End If


47        If ss!d_51107000 > 0 Then
48            strConta = ""
49            ss2.MoveFirst
50            For i = 1 To ss2.RecordCount
51                If ss2!d_51107000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
52                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_51107000, "0.00"))) & Format(ss2!d_51107000, "0.00")
53                End If
54                ss2.MoveNext
55            Next
56            Print #1, lngDia_movimento & Format(1, "00000000") & "51224000            "; _
                        Tab(78); Space(17 - Len(Format(ss!d_51107000, "0.00"))) & Format(ss!d_51107000, "0.00") & Format(614, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
57        End If


58        If ss!c_21507000 > 0 Then
              'creditos
59            strConta = ""
60            ss2.MoveFirst
61            For i = 1 To ss2.RecordCount
62                If ss2!c_21507000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
63                    strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_21507000, "0.00"))) & Format(ss2!c_21507000, "0.00")
64                End If
65                ss2.MoveNext
66            Next
67            Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "21507000            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_21507000, "0.00"))) & Format(ss!c_21507000, "0.00") & Format(22, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
68        End If

69        If ss!d_21507000 > 0 Then
              'debito
70            strConta = ""
71            ss2.MoveFirst
72            For i = 1 To ss2.RecordCount
73                If ss2!d_21507000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
74                    strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                                 Space(17 - Len(Format(ss2!d_21507000, "0.00"))) & Format(ss2!d_21507000, "0.00")
75                End If
76                ss2.MoveNext
77            Next
78            Print #1, lngDia_movimento & Format(1, "00000000") & "21507000            "; _
                        Tab(78); Space(17 - Len(Format(ss!d_21507000, "0.00"))) & Format(ss!d_21507000, "0.00") & Format(22, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
79        End If

80        If ss!c_51101000 > 0 Then
              'creditos
81            strConta = ""
82            ss2.MoveFirst
83            For i = 1 To ss2.RecordCount
84                If ss2!c_51101000 <> 0 Then
                      ' strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
85                    strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_51101000, "0.00"))) & Format(ss2!c_51101000, "0.00")
86                End If
87                ss2.MoveNext
88            Next
89            Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "51101000            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_51101000, "0.00"))) & Format(ss!c_51101000, "0.00") & Format(49, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
90        End If

91        If ss!c_51203000 > 0 Then
92            strConta = ""
93            ss2.MoveFirst
94            For i = 1 To ss2.RecordCount
95                If ss2!c_51203000 <> 0 Then
                      ' strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
96                    strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_51203000, "0.00"))) & Format(ss2!c_51203000, "0.00")
97                End If
98                ss2.MoveNext
99            Next
100           Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "51203000            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_51203000, "0.00"))) & Format(ss!c_51203000, "0.00") & Format(610, "00000000"); _
                        "BRINDE/REPRES" & Space(186 - Len("BRINDE/REPRES")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
101       End If


102       If ss!c_12405803 > 0 Then
103           strConta = ""
104           ss2.MoveFirst
105           For i = 1 To ss2.RecordCount
106               If ss2!c_12405803 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
107                   strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_12405803, "0.00"))) & Format(ss2!c_12405803, "0.00")
108               End If
109               ss2.MoveNext
110           Next
111           Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "12405803            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_12405803, "0.00"))) & Format(ss!c_12405803, "0.00") & Format(301, "00000000"); _
                        "PARCEMPREST REPRES" & Space(186 - Len("PARCEMPREST REPRES")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"
112       End If


113       If ss!c_53108000 > 0 Then
114           strConta = ""
115           ss2.MoveFirst
116           For i = 1 To ss2.RecordCount
117               If ss2!c_53108000 <> 0 Then
                      'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
118                   strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                                 Space(17 - Len(Format(ss2!c_53108000, "0.00"))) & Format(ss2!c_53108000, "0.00")
119               End If
120               ss2.MoveNext
121           Next
122           Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "53108000            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_53108000, "0.00"))) & Format(ss!c_53108000, "0.00") & Format(610, "00000000"); _
                        "MANUT/EQUIP" & Space(186 - Len("MANUT/EQUIP")); "169"; _
                        Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta
123       End If


124       strConta = ""
125       ss2.MoveFirst
126       For i = 1 To ss2.RecordCount
127           If ss2!c_21507000_ins <> 0 Then
                  'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
128               strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                             Space(17 - Len(Format(ss2!c_21507000_ins, "0.00"))) & Format(ss2!c_21507000_ins, "0.00")
129           End If
130           ss2.MoveNext
131       Next
          'conta contabil foi alterada de 21507000 para 51101000 a pedido do Luis Henrique
          'em 05/08/03
132       Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "51101000            "; _
                    Tab(78); Space(17 - Len(Format(ss!c_21507000_ins, "0.00"))) & Format(ss!c_21507000_ins, "0.00") & Format(22, "00000000"); _
                    "INSUF/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & _
                    Space(186 - Len("INSUF/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4))); _
                    "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta


133       If ss!c_21403800 > 0 Then
              'INSS
134           strConta = ""
135           Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "21403800            "; _
                        Tab(78); Space(17 - Len(Format(ss!c_21403800, "0.00"))) & Format(ss!c_21403800, "0.00") & Format(22, "00000000"); _
                        Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & Space(186 - Len(Mid(lngDia_movimento, 3, 2) & _
                                                                                                          "/" & Mid(lngDia_movimento, 5, 4))); "169"; _
                                                                                                          Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"
136       End If

          'IRRF DEBITO/CREDITO
137       strConta = ""
138       ss2.MoveFirst
139       For i = 1 To ss2.RecordCount
140           If ss2!d_21507000_IR <> 0 Then
                  'strConta = strConta & "9" & ss2!cod_centro_custo & Space(17)
141               strConta = strConta & ss2!cod_centro_custo & Space(5) & _
                             Space(17 - Len(Format(ss2!d_21507000_IR, "0.00"))) & Format(ss2!d_21507000_IR, "0.00")
142           End If
143           ss2.MoveNext
144       Next
145       Print #1, lngDia_movimento & Format(1, "00000000") & "21507000            "; _
                    Tab(78); Space(17 - Len(Format(ss!d_21507000_IR, "0.00"))) & Format(ss!d_21507000_IR, "0.00") & Format(30, "00000000"); _
                    "COM/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & _
                    Space(186 - Len("COM/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4))); _
                    "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000" & strConta


146       strConta = ""
147       ss2.MoveFirst
148       For i = 1 To ss2.RecordCount
149           If ss2!c_21405800 <> 0 Then
                  'strConta = strConta & "9" & ss2!cod_centro_custo & Space(16) & "-"
150               strConta = strConta & ss2!cod_centro_custo & Space(4) & "-" & _
                             Space(17 - Len(Format(ss2!c_21405800, "0.00"))) & Format(ss2!c_21405800, "0.00")
151           End If
152           ss2.MoveNext
153       Next
154       Print #1, lngDia_movimento & Format(1, "00000000"); Tab(37); "21405800            "; _
                    Tab(78); Space(17 - Len(Format(ss!c_21405800, "0.00"))) & Format(ss!c_21405800, "0.00") & Format(30, "00000000"); _
                    "COM/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4) & _
                    Space(186 - Len("COM/" & Mid(lngDia_movimento, 3, 2) & "/" & Mid(lngDia_movimento, 5, 4))); _
                    "169"; _
                    Tab(302); Format(0, "00000000"); Tab(338); "N" & "000000"

155       Close #1
156       Screen.MousePointer = 0

End Sub

Public Sub PR_ARQ_DP(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strArquivo_Contabil As String
          Dim strArquivo_Contabil2 As String
          Dim strArquivo_Contabil3 As String
          Dim strNome_Arquivo As String
          Dim lngDia_movimento As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim strConta As String

1         Screen.MousePointer = 11
2         strPathArquivo = "C:\ARQUIVO_COMISSAO\"

          'montar SQL
3         V_SQL = "Begin producao.PCK_VDA860.PR_DATA_DP(:vCursor,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL
5         Set ss = oradatabase.Parameters("vCursor").Value

6         lngDia_movimento = ss!DATA_DP

7         strArquivo_Contabil = strPathArquivo & "PR" & Mid(ss!DATA_DP, 1, 4) & "_1.PRN"
8         strArquivo_Contabil2 = strPathArquivo & "PR" & Mid(ss!DATA_DP, 1, 4) & "_2.PRN"
9         strArquivo_Contabil3 = strPathArquivo & "PR" & Mid(ss!DATA_DP, 1, 4) & "_3.PRN"

          'GERANDO ARQUIVO DE COMISS�O DE VENDEDORES QUE ENTRAM
          ' NA EMPRESA ANTES DE 01/05/07, IMPLEMENTADO EM 25/05/07
10        OraParameters.Remove "ano_mes"
11        OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
12        V_SQL = "Begin producao.PCK_VDA860.pr_arq_dp(:vCursor,:ano_mes,:vErro);END;"

13        oradatabase.ExecuteSQL V_SQL

14        Set ss = oradatabase.Parameters("vCursor").Value

15        If oradatabase.Parameters("vErro").Value <> 0 Then
16            Screen.MousePointer = 0
17            Call Process_Line_Errors(SQL)
18            Exit Sub
19        End If

20        If ss.EOF And ss.BOF Then
21            Screen.MousePointer = 0
22            MsgBox "N�o h� telemarketing dispon�veis para gerar arq. p/ DP", vbExclamation, "Aten��o"
              '  Exit Sub
23        End If


24        Open strArquivo_Contabil For Output As #1
25        Print #1,
26        For i = 1 To ss.RecordCount
27            Print #1, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0390" & _
                        Format(ss!vl_comis, "000000000000.00") & Format(0, "000000000000.00") & "N"
28            Print #1, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0110" & _
                        Format(ss!vl_comis, "000000000000.00") & Format(0, "000000000000.00") & "N"
29            ss.MoveNext
30        Next
31        Close #1

          'GERANDO ARQUIVO DE COMISS�O DE VENDEDORES QUE ENTRAM
          ' NA EMPRESA AP�S DE 01/05/07, IMPLEMENTADO EM 25/05/07
32        OraParameters.Remove "ano_mes"
33        OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
34        V_SQL = "Begin producao.PCK_VDA860.pr_arq_dp_maio(:vCursor,:ano_mes,:vErro);END;"

35        oradatabase.ExecuteSQL V_SQL

36        Set ss = oradatabase.Parameters("vCursor").Value

37        If oradatabase.Parameters("vErro").Value <> 0 Then
38            Screen.MousePointer = 0
39            Call Process_Line_Errors(SQL)
40            Exit Sub
41        End If

42        If ss.EOF And ss.BOF Then
43            Screen.MousePointer = 0
44            MsgBox "N�o h� telemarketing  contratado ap�s 01/05/07 com valor a receber para gerar arq. p/ DP", vbExclamation, "Aten��o"
45            Exit Sub
46        End If

          'arquivo de comiss�o
47        Open strArquivo_Contabil3 For Output As #3
48        Open strArquivo_Contabil2 For Output As #2
49        Print #2,
50        Print #3,
51        For i = 1 To ss.RecordCount
52            Print #2, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0754" & _
                        Format((CDbl(ss!vl_comis) - CDbl(ss!vl_premios)), "000000000000.00") & Format(0, "000000000000.00") & "N"

53            Print #3, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0755" & _
                        Format(CDbl(ss!vl_premios), "000000000000.00") & Format(0, "000000000000.00") & "N"
54            ss.MoveNext
55        Next
56        Close #3
57        Close #2


58        Screen.MousePointer = 0

End Sub

Public Sub PR_ARQ_DP_PROMO(lngAno_mes As Long)
          Dim i As Long
          Dim strArquivo As String
          Dim strArquivo_Contabil As String
          Dim strNome_Arquivo As String
          Dim lngDia_movimento As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim strConta As String

1         Screen.MousePointer = 11
2         strPathArquivo = "C:\ARQUIVO_COMISSAO\"

          'montar SQL
3         V_SQL = "Begin producao.PCK_VDA860.PR_DATA_DP(:vCursor,:vErro);END;"

4         oradatabase.ExecuteSQL V_SQL
5         Set ss = oradatabase.Parameters("vCursor").Value

6         lngDia_movimento = ss!DATA_DP

7         strArquivo_Contabil = strPathArquivo & "PR" & Mid(ss!DATA_DP, 1, 4) & "_P.PRN"

8         OraParameters.Remove "ano_mes"
9         OraParameters.Add "ano_mes", lngAno_mes, 1

          'montar SQL
10        V_SQL = "Begin producao.PCK_VDA860.pr_arq_dp_promo(:vCursor,:ano_mes,:vErro);END;"

11        oradatabase.ExecuteSQL V_SQL

12        Set ss = oradatabase.Parameters("vCursor").Value

13        If oradatabase.Parameters("vErro").Value <> 0 Then
14            Screen.MousePointer = 0
15            Call Process_Line_Errors(SQL)
16            Exit Sub
17        End If

18        If ss.EOF And ss.BOF Then
19            Screen.MousePointer = 0
20            MsgBox "N�o h� promotores dispon�veis para gerar arq. p/ DP", vbExclamation, "Aten��o"
21            Exit Sub
22        End If



23        Open strArquivo_Contabil For Output As #1
24        Print #1,
25        For i = 1 To ss.RecordCount
26            Print #1, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0741" & _
                        Format(ss!vl_comis, "000000000000.00") & Format(0, "000000000000.00") & "N"

27            Print #1, IIf(IsNull(ss!chapa), " ", ss!chapa) & Space(16 - Len(IIf(IsNull(ss!chapa), " ", ss!chapa))) & "0000000" & ss!mes & "0743" & _
                        Format(ss!VL_PREMIO, "000000000000.00") & Format(0, "000000000000.00") & "N"
28            ss.MoveNext
29        Next
30        Close #1
31        Screen.MousePointer = 0

End Sub

Public Function mFechar()
          
1         On Error Resume Next
          'Fechar Conexao oracle
2         ss.Close
3         Set ss = Nothing
4         oradatabase.Close
5         Set orasession = Nothing
6         Set oradatabase = Nothing
          
7         Unload frmComis
8         Unload MDIForm1
9         Unload frmImagem
10        Unload frmLayout
11        Unload frmLista_Recibo
12        Unload frmMotivo
13        Unload frmRecibo
14        Unload frmRepresentante
15        Unload frmResumo
16        Unload frmSenha
          
End Function
