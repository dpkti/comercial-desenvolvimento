VERSION 5.00
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "GRID32.OCX"
Begin VB.Form frmResumo 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7725
   ClientLeft      =   -1065
   ClientTop       =   1845
   ClientWidth     =   8025
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7725
   ScaleWidth      =   8025
   Begin VB.Frame Frame1 
      Caption         =   "DEMONSTRATIVO DE COMISS�O MENSAL"
      ForeColor       =   &H00800000&
      Height          =   7455
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.CommandButton cmdImpressao 
         Caption         =   "Imprimir"
         Height          =   315
         Left            =   5520
         TabIndex        =   1
         Top             =   120
         Width           =   1335
      End
      Begin MSGrid.Grid grdCreditos 
         Height          =   2535
         Left            =   1200
         TabIndex        =   9
         Top             =   720
         Width           =   4695
         _Version        =   65536
         _ExtentX        =   8281
         _ExtentY        =   4471
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSGrid.Grid grdDebitos 
         Height          =   2535
         Left            =   1200
         TabIndex        =   8
         Top             =   4080
         Width           =   4695
         _Version        =   65536
         _ExtentX        =   8281
         _ExtentY        =   4471
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label3 
         Caption         =   "CR�DITOS"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "D�BITOS"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "CR�DITO L�QUIDO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   7080
         Width           =   1815
      End
      Begin VB.Label lblCredito_liquido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4320
         TabIndex        =   4
         Top             =   7080
         Width           =   1215
      End
      Begin VB.Label lblDebito_Comis 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4320
         TabIndex        =   3
         Top             =   6720
         Width           =   1215
      End
      Begin VB.Label lblCredito_Comis 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4320
         TabIndex        =   2
         Top             =   3360
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmResumo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
1         Frame1.Caption = "DEMOSTRATIVO DE COMISS�O MENSAL " & Mid(frmComis.cboAno_MesResumo, 5, 2) & "/" _
                         & Mid(frmComis.cboAno_MesResumo, 1, 4)

          Dim i As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim ss3 As Object
          Dim dblVL_Comis_Direta As Double
          Dim dblVL_Comis_Indireta As Double
          Dim dblVL_Comis_Terceiros As Double
          Dim dblVL_Comis_Indent_Direta As Double
          Dim dblVL_Comis_Indent_Indireta As Double
          Dim intTP_Lancamento As Integer
          Dim dblComissao_total_Credito As Double
          Dim dblComissao_total_Debito As Double
          Dim dblTotal_Dev_Abat As Double


          'dados de comissao
2         OraParameters.Remove "ano_mes"
3         OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
4         OraParameters.Remove "rep"
5         OraParameters.Add "rep", 0, 1
6         OraParameters.Remove "tipo"
7         OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1

          'montar SQL
8         V_SQL = "Begin producao.PCK_VDA860.pr_comissao_mes(:vCursor,:ano_mes,:rep,:tipo,:vErro);END;"

9         oradatabase.ExecuteSQL V_SQL

10        Set ss2 = oradatabase.Parameters("vCursor").Value

11        If oradatabase.Parameters("vErro").Value <> 0 Then
12            Call Process_Line_Errors(SQL)
13            Exit Sub
14        End If

15        dblVL_Comis_Direta = IIf(IsNull(ss2!vl_comis_direta), 0, ss2!vl_comis_direta)
16        dblVL_Comis_Indireta = IIf(IsNull(ss2!vl_comis_indireta), 0, ss2!vl_comis_indireta)
17        dblVL_Comis_Terceiros = IIf(IsNull(ss2!vl_comis_terceiros), 0, ss2!vl_comis_terceiros)
18        dblVL_Comis_Indent_Direta = IIf(IsNull(ss2!vl_comis_indent_direta), 0, ss2!vl_comis_indent_direta)
19        dblVL_Comis_Indent_Indireta = IIf(IsNull(ss2!vl_comis_indent_indireta), 0, ss2!vl_comis_indent_indireta)
20        dblComissao_total_Credito = dblVL_Comis_Direta + dblVL_Comis_Indireta _
                                    + dblVL_Comis_Terceiros + dblVL_Comis_Indent_Direta _
                                    + dblVL_Comis_Indent_Indireta


          '===RECIBO CREDITOS
          'montar SQL
21        V_SQL = "Begin producao.PCK_VDA860.pr_lancamento(:vCursor,1,:vErro);END;"

22        oradatabase.ExecuteSQL V_SQL

23        Set ss = oradatabase.Parameters("vCursor").Value

24        If oradatabase.Parameters("vErro").Value <> 0 Then
25            Call Process_Line_Errors(SQL)
26            Exit Sub
27        End If

          'carrega dados
28        With frmResumo.grdCreditos
29            .Cols = 2
30            .Rows = ss.RecordCount + 1
31            .ColWidth(0) = 2800
32            .ColWidth(1) = 1500

33            .Row = 0
34            .Col = 0
35            .Text = ""
36            .Col = 1
37            .Text = "    COMISSAO"
38            .ColAlignment(1) = 1

39            For i = 1 To .Rows - 1
40                .Row = i

41                .Col = 0
42                .Text = Format(ss("TP_LANCAMENTO"), "00") & "-" & ss("DESC_LANCAMENTO")
43                .Col = 1
44                If ss!tp_lancamento = 1 Then
45                    .Text = Format(dblVL_Comis_Direta, "###,###,###0.00")
46                ElseIf ss!tp_lancamento = 2 Then
47                    .Text = Format(dblVL_Comis_Indireta, "###,###,###0.00")
48                ElseIf ss!tp_lancamento = 3 Then
49                    .Text = Format(dblVL_Comis_Terceiros, "###,###,###0.00")
50                ElseIf ss!tp_lancamento = 8 Then
51                    .Text = Format(dblVL_Comis_Indent_Direta, "###,###,###0.00")
52                ElseIf ss!tp_lancamento = 9 Then
53                    .Text = Format(dblVL_Comis_Indent_Indireta, "###,###,###0.00")
54                Else
55                    intTP_Lancamento = ss!tp_lancamento
56                    OraParameters.Remove "ano_mes"
57                    OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
58                    OraParameters.Remove "rep"
59                    OraParameters.Add "rep", 0, 1
60                    OraParameters.Remove "tp"
61                    OraParameters.Add "tp", intTP_Lancamento, 1
62                    OraParameters.Remove "tipo"
63                    OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1

                      'montar SQL
64                    V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,:tp,:tipo,:vErro);END;"

65                    oradatabase.ExecuteSQL V_SQL

66                    Set ss3 = oradatabase.Parameters("vCursor").Value

67                    If ss3.EOF And ss3.BOF Then
68                        .Text = "0.00"
69                    Else
70                        .Text = Format(ss3!vl_lancamento, "###,###,###0.00")
71                        dblComissao_total_Credito = dblComissao_total_Credito + ss3!vl_lancamento
72                    End If
73                End If
74                ss.MoveNext
75            Next
76            .Row = 1
77        End With
78        lblCredito_Comis = Format(dblComissao_total_Credito, "###,###,###0.00")



          '===RECIBO debitos
          'montar SQL
79        V_SQL = "Begin producao.PCK_VDA860.pr_lancamento(:vCursor,2,:vErro);END;"

80        oradatabase.ExecuteSQL V_SQL

81        Set ss = oradatabase.Parameters("vCursor").Value

82        If oradatabase.Parameters("vErro").Value <> 0 Then
83            Call Process_Line_Errors(SQL)
84            Exit Sub
85        End If

          'carrega dados
86        With frmResumo.grdDebitos
87            .Cols = 2
88            .Rows = ss.RecordCount + 1
89            .ColWidth(0) = 2800
90            .ColWidth(1) = 1500

91            .Row = 0
92            .Col = 0
93            .Text = ""
94            .Col = 1
95            .Text = "    COMISSAO"
96            .ColAlignment(1) = 1

97            For i = 1 To .Rows - 1
98                .Row = i

99                .Col = 0
100               .Text = Format(ss("TP_LANCAMENTO"), "00") & "-" & ss("DESC_LANCAMENTO")
101               .Col = 1
102               If ss!tp_lancamento = 15 Then
103                   OraParameters.Remove "ano_mes"
104                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
105                   OraParameters.Remove "rep"
106                   OraParameters.Add "rep", 0, 1
107                   OraParameters.Remove "tipo"
108                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1


                      'montar SQL
109                   V_SQL = "Begin producao.PCK_VDA860.pr_adiantamento_mes(:vCursor,:ano_mes,:rep,:tipo,:vErro);END;"

110                   oradatabase.ExecuteSQL V_SQL

111                   Set ss1 = oradatabase.Parameters("vCursor").Value
112                   If ss1.EOF And ss1.BOF Then
113                       .Text = "0.00"
114                   Else
115                       .Text = Format(ss1!vl_comis, "###,###,###0.00")
116                       dblComissao_total_Debito = dblComissao_total_Debito + ss1!vl_comis
117                   End If
118               ElseIf ss!tp_lancamento = 10 Then
119                   OraParameters.Remove "ano_mes"
120                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
121                   OraParameters.Remove "rep"
122                   OraParameters.Add "rep", 0, 1
123                   OraParameters.Remove "tipo"
124                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1


                      'montar SQL
125                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,10,:tipo,:vErro);END;"

126                   oradatabase.ExecuteSQL V_SQL

127                   Set ss3 = oradatabase.Parameters("vCursor").Value
128                   If ss3.EOF And ss3.BOF Then
129                       .Text = "0.00"
130                   Else
131                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
132                   End If

133                   OraParameters.Remove "ano_mes"
134                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
135                   OraParameters.Remove "rep"
136                   OraParameters.Add "rep", 0, 1
137                   OraParameters.Remove "tipo"
138                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1


                      'montar SQL
139                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,20,:tipo,:vErro);END;"

140                   oradatabase.ExecuteSQL V_SQL

141                   Set ss3 = oradatabase.Parameters("vCursor").Value
142                   If ss3.EOF And ss3.BOF Then
143                       .Text = "0.00"
144                   Else
145                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
146                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
147                   End If

148               ElseIf ss!tp_lancamento = 11 Then
149                   OraParameters.Remove "ano_mes"
150                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
151                   OraParameters.Remove "rep"
152                   OraParameters.Add "rep", 0, 1
153                   OraParameters.Remove "tipo"
154                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1


                      'montar SQL
155                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,11,:tipo,:vErro);END;"

156                   oradatabase.ExecuteSQL V_SQL

157                   Set ss3 = oradatabase.Parameters("vCursor").Value
158                   If ss3.EOF And ss3.BOF Then
159                       .Text = "0.00"
160                   Else
161                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
162                   End If
163                   OraParameters.Remove "ano_mes"
164                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
165                   OraParameters.Remove "rep"
166                   OraParameters.Add "rep", 0, 1
167                   OraParameters.Remove "tipo"
168                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1

                      'montar SQL
169                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,21,:tipo,:vErro);END;"

170                   oradatabase.ExecuteSQL V_SQL

171                   Set ss3 = oradatabase.Parameters("vCursor").Value
172                   If ss3.EOF And ss3.BOF Then
173                       .Text = "0.00"
174                   Else
175                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
176                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
177                   End If

178               Else
179                   intTP_Lancamento = ss!tp_lancamento
180                   OraParameters.Remove "ano_mes"
181                   OraParameters.Add "ano_mes", frmComis.cboAno_MesResumo, 1
182                   OraParameters.Remove "rep"
183                   OraParameters.Add "rep", 0, 1
184                   OraParameters.Remove "tp"
185                   OraParameters.Add "tp", intTP_Lancamento, 1
186                   OraParameters.Remove "tipo"
187                   OraParameters.Add "tipo", IIf(Mid(Trim(frmComis.cboTipo_Resumo), 1, 1) = "", 0, Mid(Trim(frmComis.cboTipo_Resumo), 1, 1)), 1

                      'montar SQL
188                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,:tp,:tipo,:vErro);END;"

189                   oradatabase.ExecuteSQL V_SQL

190                   Set ss3 = oradatabase.Parameters("vCursor").Value
191                   If ss3.EOF And ss3.BOF Then
192                       .Text = "0.00"
193                   Else
194                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
195                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
196                   End If

197               End If


198               ss.MoveNext
199           Next
200           .Row = 1
201       End With
202       lblDebito_Comis = Format(dblComissao_total_Debito, "###,###,###0.00")
203       lblCredito_liquido = Format(dblComissao_total_Credito - _
                                      dblComissao_total_Debito, "###,###,###0.00")



204       If lblCredito_liquido >= 0 Then
205           lblCredito_liquido.ForeColor = &H800000
206       Else
207           lblCredito_liquido.ForeColor = &HFF&
208       End If


209       Screen.MousePointer = 0



End Sub

