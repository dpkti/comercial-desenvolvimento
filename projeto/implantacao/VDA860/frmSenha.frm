VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmSenha 
   BackColor       =   &H80000009&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "SISTEMA DE COMISS�O DPK"
   ClientHeight    =   1680
   ClientLeft      =   1140
   ClientTop       =   1515
   ClientWidth     =   5340
   Icon            =   "frmSenha.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   1680
   ScaleWidth      =   5340
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtSenha 
      ForeColor       =   &H00800000&
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   3000
      MaxLength       =   20
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   720
      Width           =   2175
   End
   Begin VB.TextBox txtUsuario 
      ForeColor       =   &H00800000&
      Height          =   375
      Left            =   3000
      MaxLength       =   20
      TabIndex        =   0
      Top             =   240
      Width           =   2175
   End
   Begin Threed.SSCommand sscmdEntrar 
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   1200
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   661
      _StockProps     =   78
      Caption         =   "Entrar"
   End
   Begin VB.Image Image1 
      Height          =   975
      Left            =   120
      Picture         =   "frmSenha.frx":0442
      Top             =   240
      Width           =   1200
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000009&
      Caption         =   "Senha:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   3
      Top             =   720
      Width           =   855
   End
   Begin VB.Label Label1 
      BackColor       =   &H80000009&
      Caption         =   "Usu�rio:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   1560
      TabIndex        =   2
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "frmSenha"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
1         strPathArquivo = "H:\ORACLE\DADOS\32Bits\ARQUIVO\"

          'Desenv
          'strPathArquivo = "F:\ORACLE\DADOS\32Bits\ARQUIVO\"

          'Conexao oracle
2         Set orasession = CreateObject("oracleinprocserver.xorasession")
3         Set oradatabase = orasession.OpenDatabase("PRODUCAO", "vda860/prod", 0&)
4         'Set oradatabase = orasession.OpenDatabase("SDPKT", "vda860/prod", 0&)
          'Set oradatabase = orasession.OpenDatabase("PRODUCAO", "vda860/prod", 0&)

5         Set OraParameters = oradatabase.Parameters
6         OraParameters.Remove "vCursor"
7         OraParameters.Add "vCursor", 0, 2
8         OraParameters("vCursor").serverType = ORATYPE_CURSOR
9         OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
10        OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

11        OraParameters.Remove "vErro"
12        OraParameters.Add "vErro", 0, 2
13        OraParameters("vErro").serverType = ORATYPE_NUMBER

End Sub

Private Sub sscmdEntrar_Click()

1         On Error GoTo TrataErro

          '  If txtusuario = "ADRIANA" And TXTSENHA = "DPK320" Then
          '    strFL_Senha = "S"
          '  If txtUsuario = "FERNANDA" And txtSenha = "DPK860" Then
          '    strFL_Senha = "S"
          '  ElseIf txtUsuario = "BETH" And txtSenha = "DPK345" Then
          '    strFL_Senha = "S"
          '  ElseIf txtUsuario = "ROGERIOG" And txtSenha = "DPK266" Then
          '    strFL_Senha = "S"
          '  ElseIf txtUsuario = "MARICI" And txtSenha = "DPK101" Then
          '    strFL_Senha = "S"
          '  Else
          '    strFL_Senha = "N"
          '  End If
          '  strUsuario = txtUsuario
          '  Unload Me

          '  If strFL_Senha = "N" Then
          '     MsgBox "Usu�rio ou Senha inv�lido, imposs�vel logar", vbCritical, "Aten��o"
          '     End
          '  End If

          'Producao
          
2         OraParameters.Remove "vUser"
3         OraParameters.Add "vUser", txtUsuario.Text, 1
4         OraParameters.Remove "vSenha"
5         OraParameters.Add "vSenha", txtSenha.Text, 1

6         V_SQL = "Begin producao.PCK_VDA860.PR_CHECA_SENHA(:vCursor,:vUser,:vSenha);END;"

7         oradatabase.ExecuteSQL V_SQL

8         Set ss = oradatabase.Parameters("vCursor").Value

9         If UCase(ss!Senha) <> UCase(txtSenha.Text) Or IsNull(ss!Senha) Then
10            MsgBox "Usu�rio ou Senha inv�lido, entre em contato com o ServiceDesk. (Acesso)", vbCritical, "Aten��o"
11            Set ss = Nothing
12        Else
13            strUsuario = txtUsuario.Text
14            frmComis.Show
15            Unload Me
16        End If

17        Exit Sub

TrataErro:
18        If Err = 3186 Or Err = 3188 Or Err = 3260 Or Err = 75 Then
19            Resume
20        Else
21            Call Process_Line_Errors(SQL)
22        End If

End Sub

Private Sub TXTSENHA_KeyPress(KeyAscii As Integer)
1         KeyAscii = Texto(KeyAscii)
End Sub

Private Sub txtusuario_KeyPress(KeyAscii As Integer)
1         KeyAscii = Texto(KeyAscii)
End Sub

