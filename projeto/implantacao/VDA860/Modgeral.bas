Attribute VB_Name = "Module2"
Option Explicit

Public strResposta As String
Public oradatabase As Object
Public orasession As Object
Public oradynaset As Object
Public strFL_Senha As String
Public strUsuario As String
Public strPathArquivo As String

Public OraParameters As Object
Public VarRec As Object
Public VarRec1 As Object
Public V_SQL As String
Public SQL As String

'Chamado 387031
Public Const path = "\\nt_idpk2000\intranet$\Web\spc\recibos\"

Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public ss As Object


Public strArquivo As String
' GhostScript function declarations
Private Declare Function gsdll_revision Lib "gsdll32.dll" ( _
                                        lpString As String, _
                                        lpString As String, _
                                        gsrev As Long, _
                                        gsdate As Long _
                                      ) As Integer

Private Declare Function gsdll_init Lib "gsdll32.dll" ( _
                                    ByVal lpGsBack As Any, _
                                    ByVal hwnd As Long, _
                                    ByVal argc As Long, _
                                    argv As Long _
                                  ) As Integer

Private Declare Function gsdll_execute_begin Lib "gsdll32.dll" () As Integer

Private Declare Function gsdll_execute_cont Lib "gsdll32.dll" ( _
                                            ByVal pscommand As String, _
                                            ByVal commlen As Long _
                                          ) As Integer

Private Declare Function gsdll_execute_end Lib "gsdll32.dll" () As Integer

Function Texto(ByVal KeyAscii As Integer) As Integer
1         If KeyAscii = 8 Then    'BACKSPACE
2             Texto = KeyAscii
3             Exit Function
4         End If

5         If Chr$(KeyAscii) = "'" Or Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = ";" Then
6             KeyAscii = 0
7             Beep
8         Else
9             KeyAscii = Asc(UCase(Chr$(KeyAscii)))
10        End If

11        Texto = KeyAscii
End Function

Function Valor(ByVal KeyAscii As Integer, strCampo As String) As Integer
1         If KeyAscii = 8 Then    'BACKSPACE
2             Valor = KeyAscii
3             Exit Function
4         End If

5         If Chr$(KeyAscii) = "," Or Chr$(KeyAscii) = "." Then
6             If InStr(strCampo, ",") > 0 Or InStr(strCampo, ".") > 0 Then
7                 KeyAscii = 0
8                 Beep
9             End If
10        Else
11            If Chr$(KeyAscii) < "0" Or Chr$(KeyAscii) > "9" Then
12                KeyAscii = 0
13                Beep
14            End If
15        End If

16        Valor = KeyAscii
End Function

Function FmtBR(ByVal Valor) As String

          Dim Temp As String
          Dim i As Integer

1         Temp = Trim(Valor)

2         For i = 1 To Len(Temp)
3             If Mid$(Temp, i, 1) = "," Then
4                 Mid$(Temp, i, 1) = "."
5             End If
6         Next i

7         FmtBR = Temp

End Function

Sub Data(ByRef KeyAscii, ByRef txtCampo)
1         On Error GoTo TrataErro

          Dim bTam As Byte    'tamanho do campo JA digitado
          Dim strData As String
          Dim bKey As Byte
2         bTam = Len(txtCampo.Text)

3         If KeyAscii = 8 And bTam > 0 Then    'backspace
4             If Mid$(txtCampo.Text, bTam) = "/" Then
5                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 2)
6             Else
7                 txtCampo.Text = Mid$(txtCampo.Text, 1, bTam - 1)
8             End If

9         ElseIf Chr$(KeyAscii) >= "0" And Chr$(KeyAscii) <= "9" Then
10            If bTam = 1 Then
11                strData = txtCampo.Text & Chr$(KeyAscii)
12                If CInt(strData) < 1 Or CInt(strData > 31) Then
13                    Beep
14                Else
15                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
16                End If

17            ElseIf bTam = 4 Then
18                strData = Mid$(txtCampo.Text, 4) & Chr$(KeyAscii)
19                If CInt(strData) < 1 Or CInt(strData > 12) Then
20                    Beep
21                Else
22                    txtCampo.Text = txtCampo.Text & Chr$(KeyAscii) & "/"
23                End If

24            ElseIf bTam = 7 Then
25                strData = Mid$(txtCampo.Text, 1, 2)     'dia
26                If CInt(strData) < 1 Or CInt(strData > 31) Then
27                    Beep
28                Else
29                    strData = Mid$(txtCampo.Text, 4, 2)     'mes
30                    If CInt(strData) < 1 Or CInt(strData > 12) Then
31                        Beep
32                    Else
33                        strData = txtCampo.Text & Chr$(KeyAscii)
34                        If Not IsDate(CDate(strData)) Then
35                            Beep
36                        Else
37                            txtCampo.Text = strData
38                        End If
39                    End If
40                End If

41            ElseIf bTam < 8 Then
42                bKey = KeyAscii

43            Else
44                bKey = 0
45            End If
46        Else
47            Beep
48        End If

49        SendKeys "{END}"
50        KeyAscii = bKey
51        Exit Sub

TrataErro:

52        If Err.Number = 13 Then
53            MsgBox strData, vbInformation, "Data Inv�lida"
54            KeyAscii = 0
55            Beep
56            Err.Clear
57        End If

End Sub

Sub Process_Line_Errors(ByRef SQL)
          Dim iFnum As Integer
          'on error GoTo Handler_Process_Line_Errors

1         If Err.Number = 3186 Or Err.Number = 3188 Or Err.Number = 3260 Then
2             Resume
3         ElseIf Err.Number = 440 Then
4             If (Err.Description Like "*02396*" Or _
                  Err.Description Like "*01012*") Then
5                 Set oradatabase = orasession.OpenDatabase("DESENV", "producao/des", 0&)
6                 MsgBox "Programa inativo, REPITA A OPERA��O", vbInformation, "Aten��o"
7                 Set OraParameters = oradatabase.Parameters
8                 OraParameters.Remove "vCursor"
9                 OraParameters.Add "vCursor", 0, 2
10                OraParameters("vCursor").serverType = ORATYPE_CURSOR
11                OraParameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
12                OraParameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0

13                OraParameters.Remove "vErro"
14                OraParameters.Add "vErro", 0, 2
15                OraParameters("vErro").serverType = ORATYPE_NUMBER

16                Exit Sub
17            Else
18                MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"

19                End
20            End If
21        Else
22            MsgBox "Ocorreu o erro: " & Err.Number & " -" & Err.Description & ". Ligue para o departamento de sistemas"
              'cursor
23            Screen.MousePointer = vbDefault
              'para a aplicacao
24            End
25        End If
26        Exit Sub

Handler_Process_Line_Errors:
27        DoEvents
28        Resume Next

End Sub

Function Numerico(ByVal KeyAscii As Integer) As Integer
1         If KeyAscii = 8 Then    'BACKSPACE
2             Numerico = KeyAscii
3             Exit Function
4         End If
5         If Chr(KeyAscii) < "0" Or Chr(KeyAscii) > "9" Then
6             KeyAscii = 0
7             Beep
8         End If
9         Numerico = KeyAscii
End Function

Function Maiusculo(KeyAscii As Integer) As Integer
1         KeyAscii = Asc(UCase(Chr(KeyAscii)))
2         Maiusculo = KeyAscii
End Function

Public Sub Processa_Arquivo()
          Dim strChar As String
          Dim strLinha As String

1         On Error GoTo Trata_erro


2         Do While Not EOF(1)
3             strChar = Input(1, #1)
4             If strChar <> Chr(10) Then
5                 strLinha = strLinha & strChar
6             Else
itens:
                  'verifica status do fechamento
7                 OraParameters.Remove "ano_mes"
8                 OraParameters.Add "ano_mes", Mid(strLinha, 1, 6), 1
9                 V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

10                oradatabase.ExecuteSQL V_SQL

11                Set ss = oradatabase.Parameters("vCursor").Value

12                If oradatabase.Parameters("vErro").Value <> 0 Then
13                    Call Process_Line_Errors(SQL)
14                    Exit Sub
15                End If

16                If ss!fl_status = 2 Then
17                    MsgBox "Fechamento j� encerrado, n�o � permitido alterar valores", vbInformation, "Aten��o"
18                    Exit Sub
19                End If
20                OraParameters.Remove "ano_mes"
21                OraParameters.Add "ano_mes", Mid(strLinha, 1, 6), 1
22                OraParameters.Remove "repres"
23                OraParameters.Add "repres", Mid(strLinha, 7, 4), 1
24                OraParameters.Remove "tp"
25                OraParameters.Add "tp", Mid(strLinha, 11, 3), 1
26                OraParameters.Remove "vl"
27                OraParameters.Add "vl", Mid(strLinha, 14, 15), 1


28                V_SQL = "Begin producao.PCK_VDA860.pr_insere_lancamento(:ano_mes,:repres,:tp,:vl,:vErro);END;"


29                oradatabase.ExecuteSQL V_SQL

30                If oradatabase.Parameters("vErro").Value <> 0 Then
31                    Call Process_Line_Errors(SQL)
32                    Exit Sub
33                End If
34                strLinha = ""
35                Line Input #1, strLinha
36                If Mid(strLinha, 1, 1) <> "" Then
37                    GoTo itens
38                End If
39            End If
40        Loop

41        Exit Sub

Trata_erro:
42        If Err = 62 Then
43            Resume Next
44        Else
45            MsgBox Err & "-" & Err.Description
46        End If
End Sub

