VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Begin VB.Form frmLista_Recibo 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3855
   ClientLeft      =   -615
   ClientTop       =   3885
   ClientWidth     =   8295
   Icon            =   "frmLista_Recibo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3855
   ScaleWidth      =   8295
   StartUpPosition =   2  'CenterScreen
   Begin VB.FileListBox File1 
      Height          =   285
      Left            =   5640
      MultiSelect     =   2  'Extended
      Pattern         =   "*.PDF"
      TabIndex        =   6
      Top             =   90
      Visible         =   0   'False
      Width           =   1575
   End
   Begin VB.CommandButton cmdFTP 
      Caption         =   "Fazer FTP"
      Height          =   375
      Left            =   6990
      TabIndex        =   5
      ToolTipText     =   "Enviar arquivos para diretorio da Intranet"
      Top             =   3450
      Visible         =   0   'False
      Width           =   1275
   End
   Begin MSFlexGridLib.MSFlexGrid grdLista_recibo 
      Height          =   2925
      Left            =   30
      TabIndex        =   3
      Top             =   450
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   5159
      _Version        =   393216
   End
   Begin VB.CommandButton cmdGerarRecibos 
      Caption         =   "Gerar Recibos"
      Height          =   375
      Left            =   5685
      TabIndex        =   2
      ToolTipText     =   "Gerar Recibos para Itens Marcados"
      Top             =   3435
      Width           =   1275
   End
   Begin VB.CommandButton cmdDesMarcarTodos 
      Caption         =   "Desmarcar Todos"
      Height          =   375
      Left            =   1530
      TabIndex        =   1
      Top             =   30
      Width           =   1455
   End
   Begin VB.CommandButton cmdMarcarTodos 
      Caption         =   "Marcar Todos"
      Height          =   375
      Left            =   30
      TabIndex        =   0
      Top             =   30
      Width           =   1455
   End
   Begin VB.Label lblStatus 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   60
      TabIndex        =   4
      Top             =   3480
      Width           =   5415
   End
   Begin VB.Image Image3 
      Appearance      =   0  'Flat
      Height          =   240
      Left            =   7260
      Picture         =   "frmLista_Recibo.frx":0442
      Top             =   90
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.Image Image2 
      Appearance      =   0  'Flat
      Height          =   240
      Left            =   7500
      Picture         =   "frmLista_Recibo.frx":04E0
      Top             =   90
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      Height          =   150
      Left            =   7830
      Picture         =   "frmLista_Recibo.frx":057C
      Top             =   120
      Visible         =   0   'False
      Width           =   165
   End
End
Attribute VB_Name = "frmLista_Recibo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdDesMarcarTodos_Click()
          Dim i As Integer
1         For i = 1 To Me.grdLista_recibo.Rows - 1

2             grdLista_recibo.Row = i
3             grdLista_recibo.Col = 6
4             grdLista_recibo.TextMatrix(grdLista_recibo.Row, grdLista_recibo.Col) = ""
5             Set grdLista_recibo.CellPicture = Nothing

6         Next
End Sub

Private Sub cmdFTP_Click()
1         If MsgBox("Confirma o envio dos arquivos para o Diret�rio da Intranet ?", vbQuestion + vbYesNo, "Aten��o") = vbYes Then
2             Enviar_Arquivos_Intranet
3         End If
End Sub

Private Sub cmdGerarRecibos_Click()
          Dim i As Integer
          Dim vRepres As Long
          Dim vQtdRecibos As Integer
          Dim vQtdGerados As Integer

1         vStatus = "Listagem"
2         vGerarRecibo = True

3         For i = 1 To grdLista_recibo.Rows - 1
4             If grdLista_recibo.TextMatrix(i, 6) <> "" Then
5                 vQtdRecibos = vQtdRecibos + 1
6             End If
7         Next

8         For i = 1 To grdLista_recibo.Rows - 1
9             grdLista_recibo.Row = i
10            grdLista_recibo.Col = 0
11            vRepres = grdLista_recibo.Text

12            grdLista_recibo.Col = 6
13            If grdLista_recibo.TextMatrix(i, 6) <> "" Then
14                vQtdGerados = vQtdGerados + 1
15                lblStatus.Caption = "Aguarde... Gerado Recibo: " & vRepres & " - " & vQtdGerados & " de " & vQtdRecibos

16                frmComis.txtRepres1 = vRepres
17                frmRecibo.Visible = False
18                If vGerarRecibo = True Then
19                    frmRecibo.Gera_Impressao frmRecibo.lblTipo, frmRecibo.dblValor_Para_IR, "A"
20                    grdLista_recibo.TextMatrix(i, 6) = ""
21                    Set grdLista_recibo.CellPicture = Image3
22                Else
23                    grdLista_recibo.TextMatrix(i, 6) = ""
24                    Set grdLista_recibo.CellPicture = Image2
25                End If
26                Unload frmRecibo
27            End If
28        Next

29        MsgBox "Arquivo gerado com sucesso", vbInformation, "Aten��o"

30        Me.lblStatus.Caption = ""

31        vStatus = ""
End Sub

Private Sub cmdMarcarTodos_Click()
          Dim i As Integer

1         For i = 1 To grdLista_recibo.Rows - 1

2             grdLista_recibo.Row = i
3             grdLista_recibo.Col = 6
4             grdLista_recibo.CellPictureAlignment = 4
5             grdLista_recibo.TextMatrix(grdLista_recibo.Row, grdLista_recibo.Col) = " "
6             Set grdLista_recibo.CellPicture = Image1

7         Next

End Sub

Private Sub Form_Activate()
1         File1.path = "C:\"
2         Call Form_Load
End Sub

Private Sub Form_Load()

1         On Error GoTo TrataErro

          Dim ss As Object
          Dim i As Integer

2         File1.path = "C:\"

          'mouse
3         Screen.MousePointer = vbHourglass

4         frmLista_Recibo.Caption = frmComis.cboTipo_Pessoa

5         OraParameters.Remove "tipo"
6         OraParameters.Add "tipo", Mid(Trim(frmComis.cboTipo_Pessoa), 1, 1), 1
7         OraParameters.Remove "ano_mes"
8         OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1

          'montar SQL
9         V_SQL = "Begin producao.PCK_VDA860.pr_lista_Recibo(:vCursor,:tipo,:ano_mes,:vErro);END;"

10        oradatabase.ExecuteSQL V_SQL

11        Set ss = oradatabase.Parameters("vCursor").Value

12        If oradatabase.Parameters("vErro").Value <> 0 Then
13            Call Process_Line_Errors(SQL)
14            Exit Sub
15        End If

16        If ss.EOF And ss.BOF Then
17            Screen.MousePointer = vbDefault
18            MsgBox "N�o ha representante cadastrado", vbInformation, "Aten��o"
              'Unload Me
19            Exit Sub
20        End If

          'carrega dados
21        With frmLista_Recibo.grdLista_recibo
22            .Cols = 7
23            .Rows = ss.RecordCount + 1
24            .ColWidth(0) = 660
25            .ColWidth(1) = 1800
26            .ColWidth(2) = 1000
27            .ColWidth(3) = 1000
28            .ColWidth(4) = 1000
29            .ColWidth(5) = 1000
30            .ColWidth(6) = 600    'eduardo 16/02/2007

31            .TextMatrix(0, 0) = "C�digo"
32            .TextMatrix(0, 1) = "Repres."
33            .TextMatrix(0, 2) = "Arq.Adiant."
34            .TextMatrix(0, 3) = "Arq.Pgto"
35            .TextMatrix(0, 4) = "Em.Recibo"
36            .TextMatrix(0, 5) = "Qtd.Recibo"
37            .TextMatrix(0, 6) = "Recibo"

38            For i = 1 To .Rows - 1
39                .Row = i

40                .ColAlignment(2) = 4
41                .ColAlignment(3) = 4
42                .ColAlignment(4) = 4
43                .ColAlignment(5) = 4

44                .TextMatrix(i, 0) = ss("COD_REPRES")
45                .TextMatrix(i, 1) = ss("PSEUDONIMO")
46                .TextMatrix(i, 2) = ss("fl_arq_adiantamento")
47                .TextMatrix(i, 3) = ss("fl_arq_pagamento")
48                .TextMatrix(i, 4) = ss("fl_emissao_recibo")
49                .TextMatrix(i, 5) = ss("qtd_impressao")

50                .CellPictureAlignment = 4
51                .Col = 6
52                .PictureType = flexPictureColor
53                Set .CellPicture = LoadPicture("")
54                ss.MoveNext
55            Next
56            .Row = 1
57        End With


          'mouse
58        Screen.MousePointer = vbDefault

59        Exit Sub

TrataErro:
60        If Err = 3186 Or Err = 3188 Or Err = 3218 Or Err = 3260 Or Err = 3262 Or Err = 3197 Or Err = 3189 Then

61            Resume
62        Else
63            Call Process_Line_Errors(SQL)
64        End If



End Sub

Private Sub grdLista_recibo_Click()
1         If Me.grdLista_recibo.Col = 6 Then
2             If grdLista_recibo.TextMatrix(grdLista_recibo.Row, 6) = " " Then
3                 grdLista_recibo.TextMatrix(grdLista_recibo.Row, grdLista_recibo.Col) = ""
4                 Set grdLista_recibo.CellPicture = Nothing
5             Else
6                 grdLista_recibo.TextMatrix(grdLista_recibo.Row, grdLista_recibo.Col) = " "
7                 Set grdLista_recibo.CellPicture = Image1
8             End If
9         End If
End Sub

Private Sub grdLista_recibo_DblClick()
1         If grdLista_recibo.Col = 6 Then Exit Sub
2         grdLista_recibo.Col = 0
3         frmComis.txtRepres1 = grdLista_recibo.Text
4         frmRecibo.Show 1
5         frmComis.txtRepres1 = ""
End Sub

Sub Enviar_Arquivos_Intranet()
1         On Error GoTo erro

          Dim i As Double
          'CRIA ARQUIVO .BAT
          Dim Arquivo As Integer
2         Arquivo = FreeFile
3         Kill "c:\ftpPDF.bat"
4         While Dir("C:\ftpPDF.bat") <> ""
5             i = 1
6         Wend

7         Open "c:\FtpPDF.Bat" For Output As Arquivo
8         Print #Arquivo, "c:"
9         Print #Arquivo, "call ftp -i -n -s:c:\ftp.txt"

10        For i = 0 To File1.ListCount - 1
11            Print #Arquivo, "DEL C:\" & File1.List(i)    ' & ".PDF"
12        Next

13        Close #Arquivo

          'CRIA ARQUIVO EXECU��O FTP
14        Arquivo = FreeFile
15        Kill "c:\ftp.txt"
16        While Dir("C:\ftp.txt") <> ""
17            i = 1
18        Wend

19        Open "c:\ftp.txt" For Output As Arquivo
          'Print #Arquivo, "open 10.33.1.5"
20        Print #Arquivo, "open 10.33.1.11"
21        Print #Arquivo, "user atualizacao_ftp ftpdpk123"
22        Print #Arquivo, "cd InetPub\wwwroot\Producao\Intranet\Web\spc\recibos"
          '    Print #Arquivo, "cd Intranet\Web\spc\recibos"

23        For i = 0 To File1.ListCount - 1
24            Print #Arquivo, "put C:\" & File1.List(i)    '& ".PDF"
25        Next

26        Print #Arquivo, "quit"
27        Close #Arquivo

28        Me.lblStatus.Caption = "Aguarde... Enviando arquivo para o diretorio da Intranet."
29        Me.Refresh

30        Shell "C:\FtpPDF.bat", 0

31        Me.lblStatus.Caption = "Arquivo Enviados com Sucesso!"

erro:
32        If Err.Number = 53 Then
33            Resume Next
34        ElseIf Err.Number <> 0 Then
35            MsgBox "Codigo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
36        End If
End Sub

