VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{A8B3B723-0B5A-101B-B22E-00AA0037B2FC}#1.0#0"; "grid32.ocx"
Begin VB.Form frmRecibo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "RECIBO DE PAGAMENTO"
   ClientHeight    =   8010
   ClientLeft      =   1245
   ClientTop       =   2070
   ClientWidth     =   8010
   Icon            =   "frmRecibo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   8010
   ScaleWidth      =   8010
   Begin VB.Frame Frame1 
      Caption         =   "DEMONSTRATIVO DE COMISS�O MENSAL"
      ForeColor       =   &H00800000&
      Height          =   7815
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   7095
      Begin VB.CommandButton cmdArquivo 
         Caption         =   "Arquivo"
         Height          =   315
         Left            =   4560
         TabIndex        =   25
         Top             =   120
         Width           =   1095
      End
      Begin VB.Frame Frame3 
         Height          =   495
         Left            =   360
         TabIndex        =   18
         Top             =   1440
         Width           =   6495
         Begin VB.Label Label7 
            Caption         =   "%"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   6240
            TabIndex        =   24
            Top             =   120
            Width           =   135
         End
         Begin VB.Label lblRealizado 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5640
            TabIndex        =   23
            Top             =   120
            Width           =   615
         End
         Begin VB.Label lblVenda 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4560
            TabIndex        =   22
            Top             =   120
            Width           =   975
         End
         Begin VB.Label Label8 
            Caption         =   "VENDA EFETUADA R$"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   2880
            TabIndex        =   21
            Top             =   120
            Width           =   1695
         End
         Begin VB.Label lblQuota 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   1  'Fixed Single
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1800
            TabIndex        =   20
            Top             =   120
            Width           =   975
         End
         Begin VB.Label Label6 
            Caption         =   "PREVIS�O VENDA R$"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   120
            Width           =   1815
         End
      End
      Begin VB.CommandButton cmdImpressao 
         Caption         =   "Imprimir"
         Height          =   315
         Left            =   5760
         TabIndex        =   17
         Top             =   120
         Width           =   1095
      End
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   6495
         Begin VB.Label lblCGC 
            AutoSize        =   -1  'True
            Caption         =   "53037511000144"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   120
            TabIndex        =   27
            Top             =   480
            Width           =   1485
         End
         Begin VB.Label lblRegional 
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   720
            Width           =   4455
         End
         Begin VB.Label lblTipo 
            Caption         =   "PJ"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   6000
            TabIndex        =   10
            Top             =   240
            Width           =   255
         End
         Begin VB.Label lblCod_Repres 
            Caption         =   "NR.: 0004"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4680
            TabIndex        =   3
            Top             =   240
            Width           =   975
         End
         Begin VB.Label lblRazao 
            Caption         =   "NATAL SANITA COM. REP. "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   2
            Top             =   240
            Width           =   4455
         End
      End
      Begin MSGrid.Grid grdDebitos 
         Height          =   2175
         Left            =   360
         TabIndex        =   5
         Top             =   4800
         Width           =   6015
         _Version        =   65536
         _ExtentX        =   10610
         _ExtentY        =   3836
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
      End
      Begin MSGrid.Grid grdCreditos 
         Height          =   2175
         Left            =   360
         TabIndex        =   4
         Top             =   2160
         Width           =   6015
         _Version        =   65536
         _ExtentX        =   10610
         _ExtentY        =   3836
         _StockProps     =   77
         ForeColor       =   8388608
         BackColor       =   12648447
      End
      Begin VB.Label lblCredito_Comis 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   16
         Top             =   4440
         Width           =   1215
      End
      Begin VB.Label lblCredito_Venda 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2880
         TabIndex        =   15
         Top             =   4440
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "TOTAL CR�DITO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   14
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Label lblDebito_Comis 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   13
         Top             =   7080
         Width           =   1215
      End
      Begin VB.Label lblDebito_Venda 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2880
         TabIndex        =   12
         Top             =   7080
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "TOTAL D�BITO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   7080
         Width           =   1815
      End
      Begin VB.Label lblCredito_liquido 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   9
         Top             =   7440
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "CR�DITO L�QUIDO"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   7440
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "D�BITOS"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   4560
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "CR�DITOS"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   1920
         Width           =   1215
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   7560
      Top             =   4680
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "frmRecibo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public dblValor_Para_IR As Double

Sub Criar_Ftp(ArquivoPDF As String)
1         On Error GoTo erro
          Dim i As Double
          'CRIA ARQUIVO .BAT
          Dim Arquivo As Integer
2         Arquivo = FreeFile
3         Kill "c:\ftpPDF.bat"
4         While Dir("C:\ftpPDF.bat") <> ""
5             i = 1
6         Wend

7         Open "c:\FtpPDF.Bat" For Output As Arquivo
8         Print #Arquivo, "c:"
9         Print #Arquivo, "call ftp -i -n -s:c:\ftp.txt"
10        Print #Arquivo, "DEL C:\" & ArquivoPDF & ".PDF"
11        Close #Arquivo

          'CRIA ARQUIVO EXECU��O FTP
12        Arquivo = FreeFile
13        Kill "c:\ftp.txt"
14        While Dir("C:\ftp.txt") <> ""
15            i = 1
16        Wend

17        Open "c:\ftp.txt" For Output As Arquivo
          'Print #Arquivo, "open 10.33.1.5"
18        Print #Arquivo, "open 10.33.1.11"
19        Print #Arquivo, "user atualizacao_ftp ftpdpk123"
20        Print #Arquivo, "cd InetPub\wwwroot\Producao\Intranet\Web\spc\recibos"
          '    Print #Arquivo, "cd Intranet\Web\spc\recibos"
21        Print #Arquivo, "put C:\" & ArquivoPDF & ".PDF"
22        Print #Arquivo, "quit"
23        Close #Arquivo
erro:
24        If Err.Number = 53 Then
25            Resume Next
26        ElseIf Err.Number <> 0 Then
27            MsgBox "Codigo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
28        End If
End Sub

Private Sub cmdArquivo_Click()
1         Call Gera_Impressao(lblTipo, dblValor_Para_IR, "A")
2         Unload Me
End Sub

Private Sub cmdImpressao_Click()

1         Call Gera_Impressao(lblTipo, dblValor_Para_IR, "I")

End Sub

Private Sub Form_Load()

          Dim i As Long
          Dim ss1 As Object
          Dim ss2 As Object
          Dim ss3 As Object
          Dim dblVL_Venda_Direta As Double
          Dim dblVL_Venda_Indireta As Double
          Dim dblVL_Venda_Terceiros As Double
          Dim dblVL_Venda_Indent_Direta As Double
          Dim dblVL_Venda_Indent_Indireta As Double
          Dim dblVL_Comis_Direta As Double
          Dim dblVL_Comis_Indireta As Double
          Dim dblVL_Comis_Terceiros As Double
          Dim dblVL_Comis_Indent_Direta As Double
          Dim dblVL_Comis_Indent_Indireta As Double
          Dim intTP_Lancamento As Integer
          Dim dblVenda_Total_Credito As Double
          Dim dblComissao_total_Credito As Double
          Dim dblVenda_Total_Debito As Double
          Dim dblComissao_total_Debito As Double
          Dim dblRecusadas As Double
          Dim bStatus_Fechamento As Byte
          Dim dblTotal_Dev_Abat As Double
          Dim dblTotal_INSS As Double
          Dim dblDev_Est_Comis As Double

          'mouse
1         Screen.MousePointer = vbHourglass

2         dblDev_Est_Comis = 0

          'verifica status do fechamento
3         OraParameters.Remove "ano_mes"
4         OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1

5         OraParameters.Remove "rep"
6         OraParameters.Add "rep", frmComis.txtRepres1, 1

7         V_SQL = "Begin producao.PCK_VDA860.pr_controle_fech(:vCursor,:ano_mes,:vErro);END;"

8         oradatabase.ExecuteSQL V_SQL

9         Set ss = oradatabase.Parameters("vCursor").Value

10        bStatus_Fechamento = IIf(IsNull(ss!fl_status), False, ss!fl_status)

          'montar SQL
11        V_SQL = "Begin producao.PCK_VDA860.pr_dados_recibo(:vCursor,:rep,:vErro);END;"

12        oradatabase.ExecuteSQL V_SQL

13        Set ss = oradatabase.Parameters("vCursor").Value

14        If oradatabase.Parameters("vErro").Value <> 0 Then
15            Call Process_Line_Errors(SQL)
16            If vStatus = "Listagem" Then vGerarRecibo = False
17            Exit Sub
18        End If

19        If ss.EOF And ss.BOF Then
20            Screen.MousePointer = vbDefault
21            If vStatus = "Listagem" Then
22                vGerarRecibo = False
23                Exit Sub
24            End If
25            MsgBox "N�o ha cadastro para este representante", vbInformation, "Aten��o"
26            Unload Me
27        End If

28        frmRecibo.lblRazao = ss!nome
29        frmRecibo.lblCod_Repres = ss!cod_repres
30        frmRecibo.lblTipo = ss!tp
31        frmRecibo.lblRegional = "Reg.:" & ss!regional
32        If ss!CGC <> 0 Then
33            frmRecibo.lblCGC = "CNPJ: " & ss!CGC
              'frmRecibo.Visible = True
34        Else
35            frmRecibo.lblCGC = ""
              'frmRecibo.Visible = False
36        End If

          'dados de venda
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1

          'montar SQL
37        V_SQL = "Begin producao.PCK_VDA860.pr_venda_mes(:vCursor,:ano_mes,:rep,:vErro);END;"

38        oradatabase.ExecuteSQL V_SQL

39        Set ss1 = oradatabase.Parameters("vCursor").Value

40        If oradatabase.Parameters("vErro").Value <> 0 Then
41            Call Process_Line_Errors(SQL)
42            If vStatus = "Listagem" Then vGerarRecibo = False
43            Exit Sub
44        End If
45        dblVL_Venda_Direta = IIf(IsNull(ss1!vl_venda_direta), 0, ss1!vl_venda_direta)
46        dblVL_Venda_Indireta = IIf(IsNull(ss1!vl_venda_indireta), 0, ss1!vl_venda_indireta)
47        dblVL_Venda_Terceiros = IIf(IsNull(ss1!vl_venda_terceiros), 0, ss1!vl_venda_terceiros)
48        dblVL_Venda_Indent_Direta = IIf(IsNull(ss1!vl_venda_indent_direta), 0, ss1!vl_venda_indent_direta)
49        dblVL_Venda_Indent_Indireta = IIf(IsNull(ss1!vl_venda_indent_indireta), 0, ss1!vl_venda_indent_indireta)
50        dblVenda_Total_Credito = dblVL_Venda_Direta + dblVL_Venda_Indireta _
                                 + dblVL_Venda_Terceiros + dblVL_Venda_Indent_Direta _
                                 + dblVL_Venda_Indent_Indireta

          'dados de comissao
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1

          'montar SQL
51        V_SQL = "Begin producao.PCK_VDA860.pr_comissao_mes(:vCursor,:ano_mes,:rep,0,:vErro);END;"

52        oradatabase.ExecuteSQL V_SQL

53        Set ss2 = oradatabase.Parameters("vCursor").Value

54        If oradatabase.Parameters("vErro").Value <> 0 Then
55            Call Process_Line_Errors(SQL)
56            If vStatus = "Listagem" Then vGerarRecibo = False
57            Exit Sub
58        End If

59        dblVL_Comis_Direta = IIf(IsNull(ss2!vl_comis_direta), 0, ss2!vl_comis_direta)
60        dblVL_Comis_Indireta = IIf(IsNull(ss2!vl_comis_indireta), 0, ss2!vl_comis_indireta)
61        dblVL_Comis_Terceiros = IIf(IsNull(ss2!vl_comis_terceiros), 0, ss2!vl_comis_terceiros)
62        dblVL_Comis_Indent_Direta = IIf(IsNull(ss2!vl_comis_indent_direta), 0, ss2!vl_comis_indent_direta)
63        dblVL_Comis_Indent_Indireta = IIf(IsNull(ss2!vl_comis_indent_indireta), 0, ss2!vl_comis_indent_indireta)
64        dblComissao_total_Credito = dblVL_Comis_Direta + dblVL_Comis_Indireta _
                                    + dblVL_Comis_Terceiros + dblVL_Comis_Indent_Direta _
                                    + dblVL_Comis_Indent_Indireta


          '===RECIBO CREDITOS
          'montar SQL
65        V_SQL = "Begin producao.PCK_VDA860.pr_lancamento(:vCursor,1,:vErro);END;"

66        oradatabase.ExecuteSQL V_SQL

67        Set ss = oradatabase.Parameters("vCursor").Value

68        If oradatabase.Parameters("vErro").Value <> 0 Then
69            Call Process_Line_Errors(SQL)
70            If vStatus = "Listagem" Then vGerarRecibo = False
71            Exit Sub
72        End If

          'carrega dados
73        With frmRecibo.grdCreditos
74            .Cols = 4
75            .Rows = ss.RecordCount + 1
76            .ColWidth(0) = 2500
77            .ColWidth(1) = 1200
78            .ColWidth(2) = 800
79            .ColWidth(3) = 1200

80            .Row = 0
81            .Col = 0
82            .Text = ""
83            .Col = 1
84            .Text = "    VENDA"
85            .ColAlignment(1) = 1
86            .Col = 2
87            .Text = "    %"
88            .ColAlignment(2) = 1
89            .Col = 3
90            .Text = "    COMISSAO"
91            .ColAlignment(3) = 1

92            For i = 1 To .Rows - 1
93                .Row = i

94                .Col = 0
95                .Text = Format(ss("TP_LANCAMENTO"), "00") & "-" & ss("DESC_LANCAMENTO")
96                .Col = 1


97                If ss!tp_lancamento = 1 Then
98                    .Text = Format(dblVL_Venda_Direta, "###,###,###0.00")
99                    .Col = 2
100                   If dblVL_Comis_Direta = 0 Then
101                       .Text = "0.00"
102                   Else
103                       .Text = Format(dblVL_Comis_Direta / dblVL_Venda_Direta * 100, "###,###,###0.00")
104                   End If
105                   .Col = 3
106                   .Text = Format(dblVL_Comis_Direta, "###,###,###0.00")
107               ElseIf ss!tp_lancamento = 2 Then
108                   .Text = Format(dblVL_Venda_Indireta, "###,###,###0.00")
109                   .Col = 2
110                   If dblVL_Comis_Indireta = 0 Then
111                       .Text = "0.00"
112                   Else
113                       .Text = Format(dblVL_Comis_Indireta / dblVL_Venda_Indireta * 100, "###,###,###0.00")
114                   End If
115                   .Col = 3
116                   .Text = Format(dblVL_Comis_Indireta, "###,###,###0.00")
117               ElseIf ss!tp_lancamento = 3 Then
118                   .Text = Format(dblVL_Venda_Terceiros, "###,###,###0.00")
119                   .Col = 2
120                   If dblVL_Comis_Terceiros = 0 Then
121                       .Text = "0.00"
122                   Else
123                       .Text = Format(dblVL_Comis_Terceiros / dblVL_Venda_Terceiros * 100, "###,###,###0.00")
124                   End If
125                   .Col = 3
126                   .Text = Format(dblVL_Comis_Terceiros, "###,###,###0.00")
127               ElseIf ss!tp_lancamento = 8 Then
128                   .Text = Format(dblVL_Venda_Indent_Direta, "###,###,###0.00")
129                   .Col = 2
130                   .Text = "0.00"
131                   .Col = 3
132                   .Text = Format(dblVL_Comis_Indent_Direta, "###,###,###0.00")
133               ElseIf ss!tp_lancamento = 9 Then
134                   .Text = Format(dblVL_Venda_Indent_Indireta, "###,###,###0.00")
135                   .Col = 2
136                   .Text = "0.00"
137                   .Col = 3
138                   .Text = Format(dblVL_Comis_Indent_Indireta, "###,###,###0.00")
139               Else
140                   intTP_Lancamento = ss!tp_lancamento
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1
141                   OraParameters.Remove "tp"
142                   OraParameters.Add "tp", intTP_Lancamento, 1

                      'montar SQL
143                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,:tp,0,:vErro);END;"

144                   oradatabase.ExecuteSQL V_SQL

145                   Set ss3 = oradatabase.Parameters("vCursor").Value

146                   .Text = "0.00"
147                   .Col = 2
148                   .Text = "0.00"
149                   .Col = 3
150                   If ss3.EOF And ss3.BOF Then
151                       .Text = "0.00"

152                   Else
153                       .Text = Format(ss3!vl_lancamento, "###,###,###0.00")
154                       dblComissao_total_Credito = dblComissao_total_Credito + ss3!vl_lancamento

155                       If frmComis.cboAno_Mes1 >= 200612 And intTP_Lancamento = 5 Then
156                           dblDev_Est_Comis = IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
157                       End If

158                   End If
159               End If
160               ss.MoveNext
161           Next
162           .Row = 1
163       End With
164       lblCredito_Venda = Format(dblVenda_Total_Credito, "###,###,###0.00")
165       lblCredito_Comis = Format(dblComissao_total_Credito, "###,###,###0.00")
166       dblValor_Para_IR = 0
          '  If frmRecibo.lblTipo <> "TL" Then
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1
          'montar SQL
167       V_SQL = "Begin producao.PCK_VDA860.pr_total_dev_abat(:vCursor,:ano_mes,:rep,:vErro);END;"

168       oradatabase.ExecuteSQL V_SQL

169       Set ss = oradatabase.Parameters("vCursor").Value
170       If ss.EOF And ss.BOF Then
171           dblTotal_Dev_Abat = 0
172       Else
173           dblTotal_Dev_Abat = IIf(IsNull(ss!total), 0, ss!total)
174       End If

          'INSS
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1
          'montar SQL
175       V_SQL = "Begin producao.PCK_VDA860.pr_consulta_inss(:vCursor,:ano_mes,:rep,:vErro);END;"

176       oradatabase.ExecuteSQL V_SQL

177       Set ss = oradatabase.Parameters("vCursor").Value
178       If ss.EOF And ss.BOF Then
179           dblTotal_INSS = 0
180       Else
181           dblTotal_INSS = IIf(IsNull(ss!total), 0, ss!total)
182       End If

183       dblValor_Para_IR = dblComissao_total_Credito - dblTotal_Dev_Abat - dblTotal_INSS - dblDev_Est_Comis

184       If bStatus_Fechamento <> 2 Then
              'OraParameters.Remove "ano_mes"
              'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
              'OraParameters.Remove "rep"
              'OraParameters.Add "rep", frmComis.txtRepres1, 1
185           OraParameters.Remove "tp"
186           OraParameters.Add "tp", frmRecibo.lblTipo, 1
187           OraParameters.Remove "valor"
188           OraParameters.Add "valor", dblValor_Para_IR, 1

189           V_SQL = "Begin producao.PCK_VDA860.pr_CALCULA_IR(:tp,:ano_mes,:rep,:valor,:vErro);END;"

190           oradatabase.ExecuteSQL V_SQL

191           Set ss = oradatabase.Parameters("vCursor").Value

192           DoEvents
193       End If
          'End If


194       dblRecusadas = 0
          '===RECIBO debitos
          'montar SQL
195       V_SQL = "Begin producao.PCK_VDA860.pr_lancamento(:vCursor,2,:vErro);END;"

196       oradatabase.ExecuteSQL V_SQL

197       Set ss = oradatabase.Parameters("vCursor").Value

198       If oradatabase.Parameters("vErro").Value <> 0 Then
199           Call Process_Line_Errors(SQL)
200           Exit Sub
201       End If

          'carrega dados
202       With frmRecibo.grdDebitos
203           .Cols = 4
204           .Rows = ss.RecordCount + 1
205           .ColWidth(0) = 2500
206           .ColWidth(1) = 1200
207           .ColWidth(2) = 800
208           .ColWidth(3) = 1200

209           .Row = 0
210           .Col = 0
211           .Text = ""
212           .Col = 1
213           .Text = "    VENDA"
214           .ColAlignment(1) = 1
215           .Col = 2
216           .Text = "    %"
217           .ColAlignment(2) = 1
218           .Col = 3
219           .Text = "    COMISSAO"
220           .ColAlignment(3) = 1

221           For i = 1 To .Rows - 1
222               .Row = i

223               .Col = 0
224               .Text = Format(ss("TP_LANCAMENTO"), "00") & "-" & ss("DESC_LANCAMENTO")
225               .Col = 1
226               If ss!tp_lancamento = 15 Then
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1

                      'montar SQL
227                   V_SQL = "Begin producao.PCK_VDA860.pr_adiantamento_mes(:vCursor,:ano_mes,:rep,0,:vErro);END;"

228                   oradatabase.ExecuteSQL V_SQL

229                   Set ss1 = oradatabase.Parameters("vCursor").Value
230                   .Text = "0.00"
231                   .Col = 2
232                   .Text = "0.00"
233                   .Col = 3
234                   If ss1.EOF And ss1.BOF Then
235                       .Text = "0.00"
236                   Else
237                       .Text = Format(ss1!vl_comis, "###,###,###0.00")
238                       dblComissao_total_Debito = dblComissao_total_Debito + ss1!vl_comis
239                   End If
240               ElseIf ss!tp_lancamento = 10 Then
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1

                      'montar SQL
241                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,10,0,:vErro);END;"

242                   oradatabase.ExecuteSQL V_SQL

243                   Set ss3 = oradatabase.Parameters("vCursor").Value
244                   If ss3.EOF And ss3.BOF Then
245                       .Text = "0.00"
246                   Else
247                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
248                       dblVenda_Total_Debito = dblVenda_Total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
249                       dblRecusadas = dblRecusadas + IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento)
250                   End If

251                   .Col = 2
252                   .Text = "0.00"

253                   .Col = 3
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1

                      'montar SQL
254                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,20,0,:vErro);END;"

255                   oradatabase.ExecuteSQL V_SQL

256                   Set ss3 = oradatabase.Parameters("vCursor").Value
257                   If ss3.EOF And ss3.BOF Then
258                       .Text = "0.00"
259                   Else
260                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
261                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
262                   End If

263               ElseIf ss!tp_lancamento = 11 Then
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1

                      'montar SQL
264                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,11,0,:vErro);END;"

265                   oradatabase.ExecuteSQL V_SQL

266                   Set ss3 = oradatabase.Parameters("vCursor").Value
267                   If ss3.EOF And ss3.BOF Then
268                       .Text = "0.00"
269                   Else
270                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
271                       dblVenda_Total_Debito = dblVenda_Total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
272                       dblRecusadas = dblRecusadas + IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento)
273                   End If
274                   .Col = 2
275                   .Text = "0.00"

276                   .Col = 3
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1

                      'montar SQL
277                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,21,0,:vErro);END;"

278                   oradatabase.ExecuteSQL V_SQL

279                   Set ss3 = oradatabase.Parameters("vCursor").Value
280                   If ss3.EOF And ss3.BOF Then
281                       .Text = "0.00"
282                   Else
283                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
284                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
285                   End If

286               Else
287                   intTP_Lancamento = ss!tp_lancamento
                      'OraParameters.Remove "ano_mes"
                      'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
                      'OraParameters.Remove "rep"
                      'OraParameters.Add "rep", frmComis.txtRepres1, 1
288                   OraParameters.Remove "tp"
289                   OraParameters.Add "tp", intTP_Lancamento, 1

                      'montar SQL
290                   V_SQL = "Begin producao.PCK_VDA860.pr_lancamento_mes(:vCursor,:ano_mes,:rep,:tp,0,:vErro);END;"

291                   oradatabase.ExecuteSQL V_SQL

292                   Set ss3 = oradatabase.Parameters("vCursor").Value
293                   .Text = "0.00"
294                   .Col = 2
295                   .Text = "0.00"
296                   .Col = 3
297                   If ss3.EOF And ss3.BOF Then
298                       .Text = "0.00"
299                   Else
300                       .Text = Format(IIf(IsNull(ss3!vl_lancamento), "0.00", ss3!vl_lancamento), "###,###,###0.00")
301                       dblComissao_total_Debito = dblComissao_total_Debito + IIf(IsNull(ss3!vl_lancamento), 0, ss3!vl_lancamento)
302                   End If

303               End If


304               ss.MoveNext
305           Next
306           .Row = 1
307       End With
308       lblDebito_Venda = Format(dblVenda_Total_Debito, "###,###,###0.00")
309       lblDebito_Comis = Format(dblComissao_total_Debito, "###,###,###0.00")
310       lblCredito_liquido = Format(dblComissao_total_Credito - _
                                      dblComissao_total_Debito, "###,###,###0.00")

311       lblVenda = Format(lblCredito_Venda - dblRecusadas, "###,###,###0.00")
312       lblQuota = "0.00"
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1
313       OraParameters.Remove "tp"
314       OraParameters.Add "tp", frmRecibo.lblTipo, 1

          'montar SQL
315       V_SQL = "Begin producao.PCK_VDA860.pr_consulta_quota(:vCursor,:ano_mes,:rep,:tp,:vErro);END;"

316       oradatabase.ExecuteSQL V_SQL

317       Set ss3 = oradatabase.Parameters("vCursor").Value

318       If ss3.EOF And ss3.BOF Then
319           lblQuota = "0.00"
320       Else
321           lblQuota = Format(IIf(IsNull(ss3!quota), 0, ss3!quota), "###,###,###0.00")
322       End If
323       If CDbl(lblVenda) <> 0 And CDbl(lblQuota) <> 0 Then
324           lblRealizado = Format(CDbl(lblVenda) / CDbl(lblQuota) * 100, "0.00")
325       Else
326           lblRealizado = "0.00"
327       End If


328       If lblCredito_liquido >= 0 Then
329           lblCredito_liquido.ForeColor = &H800000
330       Else
331           lblCredito_liquido.ForeColor = &HFF&
332       End If

          'gera insuficiencia de saldo
          'OraParameters.Remove "ano_mes"
          'OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1
          'OraParameters.Remove "rep"
          'OraParameters.Add "rep", frmComis.txtRepres1, 1
333       OraParameters.Remove "valor"
334       If CDbl(frmRecibo.lblCredito_liquido) < 0 Then
335           OraParameters.Add "valor", (CDbl(frmRecibo.lblCredito_liquido) * -1), 1
336       Else
337           OraParameters.Add "valor", 0, 1
338       End If
339       V_SQL = "Begin producao.PCK_VDA860.pr_gera_insuficiencia_saldo(:ano_mes,:rep,:valor,:vErro);END;"

340       oradatabase.ExecuteSQL V_SQL

341       Set ss1 = Nothing
342       Set ss2 = Nothing
343       Set ss3 = Nothing

344       Screen.MousePointer = 0
End Sub

Public Sub Gera_Impressao(strTipo As String, dblValor As Double, strTP As String)
          Dim i As Long
          Dim strLinha As String
          Dim dblVenda_Recusada As Double
          Dim dblComissao_Recusada As Double
          Dim intTotal_Rec As Double
          Dim j As Long
          Dim bNr_Copias As Byte
          Dim strNomeImpressoraAtual As String
          Dim P As Object
          Dim Response As String
          Dim vRetorno As Integer

1         On Error GoTo IMPERRO

2         If strTP = "A" Then Screen.MousePointer = vbHourglass

3         Nome_Arquivo_Pdf = ""

4         If Dir("C:\Impresso.pdf") <> "" Then Kill "C:\Impresso.pdf"

5         Screen.MousePointer = 11
6         CommonDialog1.CancelError = True

7         For Each P In Printers
8             If UCase(P.DeviceName) Like UCase("*LASER*") Or _
                 UCase(P.DeviceName) Like UCase("hp*") Or _
                 UCase(P.DeviceName) Like UCase("*Kyocera*") Then
9                 Set Printer = P
10                Exit For
11            End If
12        Next
13        strNomeImpressoraAtual = Printer.DeviceName

14        If strTP = "A" Then
15            For Each P In Printers
16                If UCase(P.DeviceName) Like "*PDF*" Then
17                    Set Printer = P
18                    Nome_Arquivo_Pdf = frmComis.txtRepres1 & "_" & frmComis.cboAno_Mes1
19                    Exit For
20                End If
21            Next
22        End If

23        If strTipo = "TL" Or strTP = "A" Then
24            bNr_Copias = 1
25        Else
26            bNr_Copias = 1
27        End If

inicio:
28        For j = 1 To bNr_Copias
29            PrintImagem frmImagem.Picture1.Picture

30            Printer.Font = "VERDANA"
31            Printer.FontSize = 9
32            Printer.Print
33            Printer.Print
34            Printer.Print
35            Printer.Print
36            Printer.Print
37            Printer.Print
38            Printer.Print
39            Printer.Print
40            Printer.FontBold = True
41            Printer.Print Tab(5); "COMERCIAL AUTOMOTIVA S.A."
42            Printer.Print

43            If strTipo <> "TL" Then
44                Printer.Print Tab(5); "DEMONSTRATIVO DE COMISS�ES MENSAIS " & Mid(frmComis.cboAno_Mes1, 5, 2) & "/"; _
                                Mid(frmComis.cboAno_Mes1, 1, 4)
45            Else
46                Printer.Print Tab(5); "DEMONSTRATIVO DE PREMIA��O MENSAL PARA SIMPLES CONFER�NCIA " & Mid(frmComis.cboAno_Mes1, 5, 2) & "/"; _
                                Mid(frmComis.cboAno_Mes1, 1, 4)
47            End If
48            Printer.Print
              'Printer.Print

49            Printer.Print Tab(5); lblRazao;
50            Printer.Print Tab(80); "NR.:" & lblCod_Repres
51            Printer.Print Tab(5); lblCGC
52            Printer.Print Tab(5); lblRegional;
53            Printer.FontBold = False
54            Printer.Print
55            Printer.Print
56            Printer.Print Tab(5); Label6 & "   " & lblQuota
57            Printer.Print Tab(5); Label8 & "   " & lblVenda
58            Printer.Print Tab(5); "REALIZADO  " & " " & lblRealizado & "%"
59            Printer.Print
60            Printer.Print

61            Printer.Print Tab(5); "CR�DITOS";
62            Printer.Print Tab(40); "VENDA";
63            Printer.Print Tab(64); "%";
64            Printer.Print Tab(80); "COMISS�O"
65            Printer.Print
66            For i = 1 To grdCreditos.Rows - 1
67                grdCreditos.Row = i
68                grdCreditos.Col = 0
69                Printer.Print Tab(5); Mid(grdCreditos.Text, 4, 30);
70                grdCreditos.Col = 1
71                Printer.Print Tab(40); Format$(Format$(grdCreditos.Text, "###,###,###0.00"), "@@@@@@@@");
72                grdCreditos.Col = 2
73                Printer.Print Tab(60); Format$(Format$(grdCreditos.Text, "###,###,###0.00"), "@@@@@@@@");
74                grdCreditos.Col = 3
75                Printer.Print Tab(80); Format$(Format$(grdCreditos.Text, "###,###,###0.00"), "@@@@@@@@")
76            Next

77            Printer.Print
78            Printer.Print Tab(5); "TOTAL DE CR�DITOS";
79            Printer.Print Tab(40); lblCredito_Venda;
80            Printer.Print Tab(80); lblCredito_Comis;

81            Printer.Print
82            Printer.Print
83            Printer.Print
84            Printer.Print

85            Printer.Print Tab(5); "D�BITOS";
86            Printer.Print Tab(40); "VENDA";
87            Printer.Print Tab(64); "%";
88            Printer.Print Tab(80); "COMISS�O"
89            Printer.Print
90            dblVenda_Recusada = 0
91            dblComissao_Recusada = 0
92            intTotal_Rec = 0
93            For i = 1 To grdDebitos.Rows - 1
94                grdDebitos.Row = i
95                grdDebitos.Col = 0
96                If Mid(grdDebitos, 1, 2) = 10 Then
97                    grdDebitos.Col = 1
98                    dblVenda_Recusada = grdDebitos.Text
99                    grdDebitos.Col = 3
100                   dblComissao_Recusada = grdDebitos.Text
101                   intTotal_Rec = intTotal_Rec + 1
102               ElseIf Mid(grdDebitos, 1, 2) = 11 Then
103                   grdDebitos.Col = 1
104                   dblVenda_Recusada = dblVenda_Recusada + grdDebitos.Text
105                   grdDebitos.Col = 3
106                   dblComissao_Recusada = dblComissao_Recusada + grdDebitos.Text
107                   intTotal_Rec = intTotal_Rec + 1
108               End If
109               If intTotal_Rec = 2 Then
110                   intTotal_Rec = 0
111                   Printer.Print Tab(5); "VENDAS RECUSADAS";
112                   Printer.Print Tab(40); Format$(Format$(dblVenda_Recusada, "###,###,###0.00"), "@@@@@@@@");
113                   Printer.Print Tab(60); Format$(Format$(0, "###,###,###0.00"), "@@@@@@@@");
114                   Printer.Print Tab(80); Format$(Format$(dblComissao_Recusada, "###,###,###0.00"), "@@@@@@@@")
115               End If
116               grdDebitos.Col = 0
117               If intTotal_Rec = 0 And Mid(grdDebitos, 1, 2) <> 10 And _
                     Mid(grdDebitos, 1, 2) <> 11 Then
118                   Printer.Print Tab(5); Mid(grdDebitos.Text, 4, 30);
119                   grdDebitos.Col = 1
120                   Printer.Print Tab(40); Format$(Format$(grdDebitos.Text, "###,###,###0.00"), "@@@@@@@@");
121                   grdDebitos.Col = 2
122                   Printer.Print Tab(60); Format$(Format$(grdDebitos.Text, "###,###,###0.00"), "@@@@@@@@");
123                   grdDebitos.Col = 3
124                   Printer.Print Tab(80); Format$(Format$(grdDebitos.Text, "###,###,###0.00"), "@@@@@@@@")
125               End If
126           Next

127           Printer.Print
128           Printer.Print Tab(5); "TOTAL DE D�BITOS";
129           Printer.Print Tab(40); Format$(Format$(lblDebito_Venda, "###,###,###0.00"), "@@@@@@@@");
130           Printer.Print Tab(80); Format$(Format$(lblDebito_Comis, "###,###,###0.00"), "@@@@@@@@");

131           Printer.Print
132           Printer.Print
133           Printer.Print
134           Printer.Print Tab(5); "CR�DITO L�QUIDO";
135           Printer.Print Tab(80); Format$(Format$(lblCredito_liquido, "###,###,###0.00"), "@@@@@@@@")

'              (TI-2951 - Retirada do campo DSR do recibo de comiss�es DPK ) 04/08/2015  - c.alexandre.ferreira 
'136           If strTipo = "TL" Then
136           If strTipo <> "TL" Then
'137               Printer.Print
'138               Printer.Print Tab(5); "DSR";
'139               Printer.Print Tab(80); Format$(Format$(lblCredito_liquido * 20 / 100, "###,###,###0.00"), "@@@@@@@@")
'140           Else
141               Printer.Print
142               Printer.Print
143               Printer.Print
144               Printer.Print
145               Printer.Print
146               Printer.Print Tab(5); "RECEBEMOS O VALOR R$ " & Format(lblCredito_liquido, "###,###,###0.00") & " QUE QUITAR�";
147               Printer.Print " A NF DE VALOR R$ " & Format(dblValor, "###,###,###0.00") & " CORRESPONDENTE"
148               Printer.Print Tab(5); "A NOSSOS SERVI�OS REALIZADOS NO M�S DE " & Mid(frmComis.cboAno_Mes1, 5, 2) & "/"; _
                                Mid(frmComis.cboAno_Mes1, 1, 4)
149           End If

150           Printer.EndDoc

151       Next
152       DoEvents
          'If strTP = "A" Then
          '    Shell "c:\PDFLIV~1\mkpdf.bat " & "C:\RECIBOS\" & lblCod_Repres & ".PRN", vbMinimizedNoFocus
          '    FileCopy "C:\RECIBOS\" & lblCod_Repres & ".PRN" & ".PDF", "C:\RECIBOS\" & lblCod_Repres & ".PDF"
          '    Kill "C:\RECIBOS\" & lblCod_Repres & ".PRN" & ".PDF"
          '    DoEvents
          '    For Each P In Printers
          '      If P.DeviceName = strNomeImpressoraAtual Then
          '         Set Printer = P
          '         Exit For
          '      End If
          '    Next P
          ' End If

153       Screen.MousePointer = 0
          'atualiza flag de impress�o
154       OraParameters.Remove "rep"
155       OraParameters.Add "rep", frmComis.txtRepres1, 1
156       OraParameters.Remove "ano_mes"
157       OraParameters.Add "ano_mes", frmComis.cboAno_Mes1, 1

158       If strTP = "I" Then
              'montar SQL
159           V_SQL = "Begin producao.PCK_VDA860.pr_atualiza_impressao(:ano_mes,:rep,:vErro);END;"

160           oradatabase.ExecuteSQL V_SQL

161           Set ss = oradatabase.Parameters("vCursor").Value
162           MsgBox "Impress�o encerrada com sucesso", vbInformation, "Aten��o"
163           Response = MsgBox("Deseja gerar arquivo ? ", vbYesNo + vbDefaultButton2, "ATEN��O")

164           If Response = vbYes Then
165               Call Gera_Impressao(lblTipo, dblValor_Para_IR, "A")
166           Else
167               Unload Me
168           End If

169       Else
              Dim retorno As Integer

170           While Dir("C:\Impresso.pdf") = ""
171               i = 1
172           Wend

173           If Dir("C:\Impresso.pdf") <> "" Then
174               Name "C:\Impresso.pdf" As "C:\" & Nome_Arquivo_Pdf & ".PDF"

175               While Dir("C:\" & Nome_Arquivo_Pdf & ".pdf") = ""
176                   Name "C:\Impresso.pdf" As "C:\" & Nome_Arquivo_Pdf & ".PDF"
177                   DoEvents
178                   i = 1
179               Wend

                                  FileCopy "C:\" & Nome_Arquivo_Pdf & ".PDF", path & Nome_Arquivo_Pdf & ".pdf"
                  Kill "C:\" & Nome_Arquivo_Pdf & ".PDF"

                  'eduardo - 16/02/2007
                  'A alteracao � para que quando for feito pela listagem
                  'nao enviar o FTP do arquivo para a intranet.
                  'Sera criado um botao para enviar.

                 'Chamado 387031
                  'Comentado o bloque abaixo, pois os arquivos
                  's�o gravados direto no servidor
                  'n�o sendo necess�rio o envio via ftp
180               'If vStatus <> "Listagem" Then
181               '    Criar_Ftp Nome_Arquivo_Pdf
182               '    While Dir("C:\FtpPDF.bat") = ""
183               '        DoEvents
184               '        i = 1
185               '    Wend
186               '    While Dir("C:\Ftp.txt") = ""
187               '        DoEvents
188               '        i = 1
189               '    Wend

190               '    Shell "C:\FtpPDF.bat", 0
191               'End If
192           End If

                          'Chamado 387031
193           'If vStatus <> "Listagem" Then
194           '    MsgBox "Arquivo gerado com sucesso", vbInformation, "Aten��o"
195           'End If

196           Nome_Arquivo_Pdf = ""
197           If strTP = "A" Then Screen.MousePointer = vbNormal

198       End If

199       Exit Sub

IMPERRO:
200       If strTP = "A" Then
201           If Err = 70 Or Err = 53 Then
202               Resume
203           Else
204               Resume Next
205           End If
206       Else
207           MsgBox "Impressora n�o esta pronta ou est� com problemas , Verifique !!!"
208           Screen.MousePointer = 0
209           Printer.KillDoc
210           Exit Sub
211       End If

End Sub

Public Sub PrintImagem(P As IPictureDisp, Optional ByVal X, Optional ByVal Y, Optional ByVal resize)
          Dim retorno As Integer

1         If IsMissing(X) Then X = Printer.CurrentX
2         If IsMissing(Y) Then Y = Printer.CurrentY
3         If IsMissing(resize) Then resize = 1

4         Printer.PaintPicture P, X, Y, P.Width * resize, P.Height * resize

End Sub

