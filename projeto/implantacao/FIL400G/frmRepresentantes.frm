VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "Botoes.ocx"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "Gradient.ocx"
Begin VB.Form frmRepresentantes 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Representantes"
   ClientHeight    =   8205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   12555
   Icon            =   "frmRepresentantes.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8205
   ScaleWidth      =   12555
   Begin MSComctlLib.ListView lsvRepresAtivos 
      Height          =   6645
      Left            =   30
      TabIndex        =   3
      Top             =   1140
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   11721
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Codigo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Data Desativacao"
         Object.Width           =   2469
      EndProperty
   End
   Begin MSComctlLib.StatusBar stbBarra 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   0
      Top             =   7875
      Width           =   12555
      _ExtentX        =   22146
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   22093
            Object.ToolTipText     =   "Mensagens do sistema"
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   1
      Top             =   810
      Width           =   12465
      _ExtentX        =   21987
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   2
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentantes.frx":23D2
      PICN            =   "frmRepresentantes.frx":23EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ListView lsvRepresDesat 
      Height          =   6645
      Left            =   6330
      TabIndex        =   4
      Top             =   1140
      Width           =   6195
      _ExtentX        =   10927
      _ExtentY        =   11721
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "C�digo"
         Object.Width           =   1764
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Nome"
         Object.Width           =   5292
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "Data Desativacao"
         Object.Width           =   2469
      EndProperty
   End
   Begin Bot�o.cmd cmdExportarAtivos 
      Height          =   690
      Left            =   10230
      TabIndex        =   7
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   60
      Width           =   930
      _ExtentX        =   1640
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   "Exportar Ativos"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentantes.frx":30C8
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdDesat 
      Height          =   690
      Left            =   11220
      TabIndex        =   8
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   60
      Width           =   1230
      _ExtentX        =   2170
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   "Exportar Desativados"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRepresentantes.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "Desativados:"
      Height          =   195
      Left            =   6300
      TabIndex        =   6
      Top             =   900
      Width           =   930
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Ativos:"
      Height          =   195
      Left            =   60
      TabIndex        =   5
      Top             =   900
      Width           =   480
   End
End
Attribute VB_Name = "frmRepresentantes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDesat_Click()
    Dim vArq As Integer
    vArq = FreeFile
    
    Open "C:\Repres_Desativados.txt" For Append As #vArq
    
    For i = 1 To lsvRepresDesat.ListItems.Count
        Print #vArq, lsvRepresDesat.ListItems(i) & vbTab & lsvRepresDesat.ListItems(i).SubItems(1)
    Next
    
    Close vArq
End Sub

Private Sub cmdExportarAtivos_Click()
    
    Dim vArq As Integer
    vArq = FreeFile
    
    Open "C:\Repres_Ativos.txt" For Append As #vArq
    
    For i = 1 To Me.lsvRepresAtivos.ListItems.Count
        Print #vArq, Me.lsvRepresAtivos.ListItems(i) & vbTab & Me.lsvRepresAtivos.ListItems(i).SubItems(1)
    Next
    
    Close vArq
    
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub
Private Sub Form_Load()
    Dim vrst As Object
    Dim litem As ListItem
    
    'Ativos
    Set vrst = vBanco_Prod.CreateDynaset("Select Cod_repres, Nome_repres, Dt_Desligamento from Representante where dt_desligamento is null and situacao = 0", &O0)
    
    lsvRepresAtivos.ListItems.Clear
    
    For i = 1 To vrst.RecordCount
        Set litem = lsvRepresAtivos.ListItems.Add
        litem = vrst!cod_repres
        litem.SubItems(1) = vrst!nome_repres
        'litem.SubItems(2) = vrst!Dt_Desligamento
        vrst.MoveNext
    Next
    
    'Desativados
    Set vrst = vBanco_Prod.CreateDynaset("Select Cod_repres, Nome_repres, Dt_Desligamento from Representante where dt_desligamento is not null and situacao = 9", &O0)
    
    lsvRepresDesat.ListItems.Clear
    
    For i = 1 To vrst.RecordCount
        Set litem = lsvRepresDesat.ListItems.Add
        litem = vrst!cod_repres
        litem.SubItems(1) = vrst!nome_repres
        litem.SubItems(2) = vrst!Dt_Desligamento
        vrst.MoveNext
    Next
    
End Sub
