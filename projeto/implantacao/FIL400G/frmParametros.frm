VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{ACFAC5C6-06B6-4373-ACB8-63723C62B279}#15.0#0"; "GRADIENT.OCX"
Begin VB.Form frmParametros 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros"
   ClientHeight    =   2805
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6720
   Icon            =   "frmParametros.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   187
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   448
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtGerarLog 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   8
      Top             =   2400
      Width           =   6180
   End
   Begin VB.TextBox txtGerarBaseEm 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   5
      Top             =   1770
      Width           =   6180
   End
   Begin Bot�o.cmd cmdMdbVazio 
      Height          =   255
      Left            =   6300
      TabIndex        =   4
      Top             =   1200
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   450
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":23D2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox txtMdbVazio 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   90
      TabIndex        =   2
      Top             =   1140
      Width           =   6180
   End
   Begin pkGradientControl.pkGradient pkGradient1 
      Height          =   30
      Left            =   45
      TabIndex        =   0
      Top             =   810
      Width           =   6525
      _ExtentX        =   11509
      _ExtentY        =   53
      Color1          =   0
      Color2          =   -2147483633
      BackColor       =   -2147483633
   End
   Begin Bot�o.cmd cmdVoltar 
      Height          =   690
      Left            =   45
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Voltar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":23EE
      PICN            =   "frmParametros.frx":240A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdBase 
      Height          =   255
      Left            =   6300
      TabIndex        =   7
      Top             =   1830
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   450
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":30E4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdArquivoLog 
      Height          =   255
      Left            =   6300
      TabIndex        =   10
      Top             =   2460
      Width           =   285
      _ExtentX        =   503
      _ExtentY        =   450
      BTYPE           =   3
      TX              =   "..."
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   13160660
      BCOLO           =   13160660
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":3100
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdSalvar 
      Height          =   690
      Left            =   780
      TabIndex        =   11
      TabStop         =   0   'False
      ToolTipText     =   "Gravar"
      Top             =   45
      Width           =   690
      _ExtentX        =   1217
      _ExtentY        =   1217
      BTYPE           =   3
      TX              =   ""
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   12640511
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmParametros.frx":311C
      PICN            =   "frmParametros.frx":3138
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label2 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Gerar Arquivo de Log em:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   90
      TabIndex        =   9
      Top             =   2190
      Width           =   2115
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Gerar Base em:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   90
      TabIndex        =   6
      Top             =   1560
      Width           =   1245
   End
   Begin VB.Label lbl 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      Caption         =   "Local do MDB Vazio:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   210
      Left            =   90
      TabIndex        =   3
      Top             =   930
      Width           =   1635
   End
End
Attribute VB_Name = "frmParametros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'Janela de Pastas - Inicio
    Private Type BROWSEINFO
      hOwner          As Long
      pidlRoot        As Long
      pszDisplayName  As String
      lpszTitle       As String
      ulFlags         As Long
      lpfn            As Long
      lParam          As Long
      iImage          As Long
    End Type
        
    Private Declare Function SHGetPathFromIDList Lib _
       "shell32.dll" Alias "SHGetPathFromIDListA" _
       (ByVal pidl As Long, _
        ByVal pszPath As String) As Long
    
    Private Declare Function SHBrowseForFolder Lib _
       "shell32.dll" Alias "SHBrowseForFolderA" _
       (lpBrowseInfo As BROWSEINFO) As Long
'Janela de pastas - Fim

Dim fName As String
Dim fPath As String
Dim bi As BROWSEINFO
Dim tmpPath As String


Private Sub cmdDirBackup_Click()
    txtDirBackup = vbGetBrowseDirectory
End Sub

Private Sub cmdDirCompactar_Click()
    txtDirCompactar = vbGetBrowseDirectory
End Sub

Private Sub cmdDirFTP_Click()
    txtDirFTP = vbGetBrowseDirectory
End Sub

Private Sub cmdDirLog_Click()
    txtDirLog = vbGetBrowseDirectory
End Sub

Private Sub cmdDirPadrao_Click()
    txtDirPadrao = vbGetBrowseDirectory
End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

'Abrir tela de Pastas
Public Function vbGetBrowseDirectory() As String

    pidl = SHBrowseForFolder(bi)
    
    tmpPath = Space$(512)
    R = SHGetPathFromIDList(ByVal pidl, ByVal tmpPath)
      
    If R Then
          pos = InStr(tmpPath, Chr$(0))
          tmpPath = Left(tmpPath, pos - 1)
          vbGetBrowseDirectory = ValidateDir(tmpPath)
    Else: vbGetBrowseDirectory = ""
    End If

End Function

Private Function ValidateDir(tmpPath As String) As String

    If Right$(tmpPath, 1) = "\" Then
          ValidateDir = tmpPath
    Else: ValidateDir = tmpPath & "\"
    End If

End Function

Private Sub cmdArquivoLog_Click()
    txtGerarLog = vbGetBrowseDirectory
End Sub

Private Sub cmdBase_Click()
    txtGerarBaseEm = vbGetBrowseDirectory
End Sub

Private Sub cmdMdbVazio_Click()
    txtMdbVazio = vbGetBrowseDirectory
End Sub

Private Sub Form_Load()
    If Dir("C:\FIL400.INI") = "" Then Exit Sub

    txtMdbVazio = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "MDBVAZIO")
    txtGerarBaseEm = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "GERARBASEEM")
    txtGerarLog = GetKeyVal("C:\FIL400.INI", "PARAMETROS", "GERARLOGEM")
    'Carregar_Parametros
End Sub

Sub Carregar_Parametros()
    txtGerarBaseEm = Pegar_VL_Parametro("DIR_GERARBASEEM")
    txtGerarLog = Pegar_VL_Parametro("DIR_GERARLOGEM")
    txtMdbVazio = Pegar_VL_Parametro("DIR_MDBVAZIO")
End Sub

Private Sub CmdSalvar_Click()
    If txtGerarBaseEm = "" Then
       MsgBox "Campo Obrigat�rio.", vbInformation, "Aten��o"
       txtGerarBaseEm.SetFocus
       Exit Sub
    End If
    If txtGerarLog = "" Then
       MsgBox "Campo Obrigat�rio.", vbInformation, "Aten��o"
       txtGerarLog.SetFocus
       Exit Sub
    End If
    If txtMdbVazio = "" Then
       MsgBox "Campo Obrigat�rio.", vbInformation, "Aten��o"
       txtMdbVazio.SetFocus
       Exit Sub
    End If
    
    'Gravar Parametros
    'Gravar "DIR_GERARBASEEM", txtGerarBaseEm
    'Gravar "DIR_GERARLOGEM", txtGerarLog
    'Gravar "DIR_MDBVAZIO", txtMdbVazio
    
    AddToINI "C:\FIL400.INI", "PARAMETROS", "MDBVAZIO", txtMdbVazio
    AddToINI "C:\FIL400.INI", "PARAMETROS", "GERARLOGEM", txtGerarLog
    AddToINI "C:\FIL400.INI", "PARAMETROS", "GERARBASEEM", txtGerarBaseEm
    
End Sub


Sub Gravar(pNomeParam As String, pValorParam As String)
    
    vBanco_Prod.Parameters.Remove "Cod_Soft": vBanco_Prod.Parameters.Add "Cod_Soft", Pegar_Codigo_Sistema, 1
    vBanco_Prod.Parameters.Remove "Nome_Param": vBanco_Prod.Parameters.Add "Nome_Param", pNomeParam, 1
    vBanco_Prod.Parameters.Remove "VL_PARAM": vBanco_Prod.Parameters.Add "VL_PARAM", pValorParam, 1
    vBanco_Prod.Parameters.Remove "Txt_Erro": vBanco_Prod.Parameters.Add "Txt_Erro", "", 2
    
    vVB_Generica_001.ExecutaPl vBanco_Prod, "PRODUCAO.PKC_FIL430_PROMOTOR.Pr_Salvar_Parametros(:Nome_Param, :VL_PARAM, :Cod_Soft, :Txt_Erro)"
    
    If vBanco_Prod.Parameters("Txt_Erro") <> "" Then
        MsgBox "Descri��o do Erro:" & vBanco_Prod.Parameters("Txt_Erro")
        Exit Sub
    End If
End Sub

Private Sub cmdVoltar_Click()

    Unload Me

End Sub


