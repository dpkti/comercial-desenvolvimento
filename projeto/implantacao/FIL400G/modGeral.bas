Attribute VB_Name = "modGeral"
Option Explicit

'Trabalhar com Arquivos INI
'APIs to access INI files and retrieve data
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString& Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal APPNAME$, ByVal KeyName$, ByVal keydefault$, ByVal FileName$)


Global Const ORATYPE_CURSOR = 102
Global Const ORADYN_NO_BLANKSTRIP = &H2&
Global Const ORAPARM_INPUT = 1              'CONSTANTE DE BIND INPUT
Global Const ORAPARM_OUTPUT = 2             'CONSTANTE DE BIND OUTPUT
Global Const ORAPARM_BOTH = 3               'CONSTANTE DE BIND INPUT/OUTPUT
Global Const ORATYPE_NUMBER = 2

Public vTipoBase As String 'P-PAAC   R-Representante
Public vFl_Base As String
Public vCodFilial As Double
Public vCD_Novo As Integer

'Banco Producao
Public vSessao_Prod As Object
Public vBanco_Prod As Object
Public vObjOracle_prod As Object

'Caminho
Public vGerarBaseEm As String
Public vGerarLog As String
Public vMdbVazio As String

Public Fl_Cadastro_OK
Public FL_BASE As String
Public dbMail As Database

Public Sub Criar_Cursor(ByRef BANCO)
    BANCO.Parameters.Remove "vCursor"
    BANCO.Parameters.Add "vCursor", 0, 2
    BANCO.Parameters("vCursor").serverType = ORATYPE_CURSOR
    BANCO.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
    BANCO.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
End Sub


Sub Gerar()

    On Error GoTo TrataErro

    Dim vFL_VDR As String

    LOG
    
    Name vGerarBaseEm & "0000.MDB" As vGerarBaseEm & "0000" & frmGerar.txtCodPaacRepres & ".MDB"
    
    Deletar_Tabelas

    frmGerar.DataCompleta.DatabaseName = vGerarBaseEm & "0000" & frmGerar.txtCodPaacRepres & ".MDB"
    
    If Gerar_Genericas("APLICACAO") = True Then Call APLICACAO
    If Gerar_Genericas("banco") = True Then Call BANCO
    If Gerar_Genericas("CATEG_SINAL") = True Then Call CATEG_SINAL
    If Gerar_Genericas("CIDADE") = True Then Call CIDADE
    If Gerar_Genericas("CLIE_MENSAGEM") = True Then Call CLIE_MENSAGEM
    If Gerar_Genericas("DATAS") = True Then Call DATAS
    If Gerar_Genericas("DOLAR_DIARIO") = True Then Call DOLAR_DIARIO
    If Gerar_Genericas("Filial") = True Then Call Filial
    If Gerar_Genericas("FORNECEDOR") = True Then Call FORNECEDOR
    If Gerar_Genericas("GRUPO") = True Then Call GRUPO
    If Gerar_Genericas("ITEM_CADASTRO") = True Then Call ITEM_CADASTRO
    If Gerar_Genericas("MONTADORA") = True Then Call MONTADORA
    If Gerar_Genericas("PLANO_PGTO") = True Then Call PLANO_PGTO
    If Gerar_Genericas("SUBGRUPO") = True Then Call SUBGRUPO
    If Gerar_Genericas("SUBST_TRIBUTARIA") = True Then Call SUBST_TRIBUTARIA
    If Gerar_Genericas("TABELA_DESCPER") = True Then Call TABELA_DESCPER
    If Gerar_Genericas("TABELA_VENDA") = True Then Call TABELA_VENDA
    If Gerar_Genericas("TIPO_CLIENTE") = True Then Call TIPO_CLIENTE
    If Gerar_Genericas("TRANSPORTADORA") = True Then Call TRANSPORTADORA
    If Gerar_Genericas("UF") = True Then Call UF
    If Gerar_Genericas("UF_ORIGEM_DESTINO") = True Then Call UF_ORIGEM_DESTINO
    If Gerar_Genericas("UF_DPK") = True Then Call UF_DPK
    If Gerar_Genericas("UF_TPCLIENTE") = True Then Call UF_TPCLIENTE
    If Gerar_Genericas("FRETE_UF_BLOQ") = True Then Call FRETE_UF_BLOQ
    If Gerar_Genericas("NATUREZA_OPERACAO") = True Then Call NATUREZA_OPERACAO
    If Gerar_Genericas("TIPO_CLIENTE_BLAU") = True Then Call TIPO_CLIENTE_BLAU
    If Gerar_Genericas("CANCEL_PEDNOTA") = True Then Call CANCEL_PEDNOTA
    If Gerar_Genericas("R_FILDEP") = True Then Call R_FILDEP
    If Gerar_Genericas("EMBALADOR") = True Then Call EMBALADOR
    If Gerar_Genericas("FORNECEDOR_ESPECIFICO") = True Then Call FORNECEDOR_ESPECIFICO
    If Gerar_Genericas("R_DPK_EQUIV") = True Then Call R_DPK_EQUIV
    If Gerar_Genericas("R_UF_DEPOSITO") = True Then Call R_UF_DEPOSITO
    If Gerar_Genericas("RESULTADO") = True Then Call RESULTADO
    If Gerar_Genericas("ANTECIPACAO_TRIBUTARIA") = True Then Call ANTECIPACAO_TRIBUTARIA
    If Gerar_Genericas("CLASS_ANTEC_ENTRADA") = True Then Call CLASS_ANTEC_ENTRADA
    If Gerar_Genericas("VENDA_LIMITADA") = True Then Call VENDA_LIMITADA
    
'Alteracao Eduardo 25/07/05
'Liberar Quando dar carga
    If Gerar_Genericas("R_FABRICA_DPK") = True Then Call R_FABRICA_DPK
    If Gerar_Genericas("CLASS_FISCAL_RED_BASE") = True Then Call CLASS_FISCAL_RED_BASE
    
    '----------------------------------------------
    '-- TABELAS ESPECIFICAS POR DEPOSITO x FILIAL
    
    vBanco_Prod.Parameters.Remove "vTipoBase"
    vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
    vBanco_Prod.Parameters.Remove "TIPO_BASE"
    vBanco_Prod.Parameters.Add "TIPO_BASE", "C", 1
    vBanco_Prod.Parameters.Remove "PM_CODIGO"
    vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1
    Criar_Cursor vBanco_Prod

    '---------------> Seleciona quem ser� gerada base <-----------------
    vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_VDR(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
    vBanco_Prod.ExecuteSQL vSql
    
    Set vObjOracle = vBanco_Prod.Parameters("vCursor").Value
        
    If vTipoBase = "P" Then
       vCodFilial = vObjOracle!cod_filial
    Else
       vCodFilial = vObjOracle!cod_repres
    End If
    
    vFL_VDR = vObjOracle!FL_VDR
    
    If Gerar_Especificas_Por_Dep_Filial("DEPOSITO_VISAO") = True Then Call DEPOSITO_VISAO
    If Gerar_Especificas_Por_Dep_Filial("ITEM_ANALITICO") = True Then Call ITEM_ANALITICO
    If Gerar_Especificas_Por_Dep_Filial("ITEM_ESTOQUE") = True Then Call ITEM_ESTOQUE
    If Gerar_Especificas_Por_Dep_Filial("ITEM_GLOBAL") = True Then Call ITEM_GLOBAL
    If Gerar_Especificas_Por_Dep_Filial("ITEM_PRECO") = True Then Call ITEM_PRECO
    If Gerar_Especificas_Por_Dep_Filial("TAXA") = True Then Call TAXA
    If Gerar_Especificas_Por_Dep_Filial("UF_DEPOSITO") = True Then Call UF_DEPOSITO
    If Gerar_Especificas_Por_Dep_Filial("FRETE_UF") = True Then Call FRETE_UF
    If Gerar_Especificas_Por_Dep_Filial("FRETE_UF_TRANSP") = True Then Call FRETE_UF_TRANSP
    If Gerar_Especificas_Por_Dep_Filial("LOJA") = True Then Call LOJA
    If Gerar_Especificas_Por_Dep_Filial("FRETE_ENTREGA") = True Then Call FRETE_ENTREGA
    If Gerar_Especificas_Por_Dep_Filial("CLIENTE_CARACTERISTICA") = True Then Call CLIENTE_CARACTERISTICA
    If Gerar_Especificas_Por_Dep_Filial("CESTA_ITEM") = True Then Call CESTA_ITEM
    If Gerar_Especificas_Por_Dep_Filial("CESTA_VENDA") = True Then Call CESTA_VENDA
    If Gerar_Especificas_Por_Dep_Filial("UF_CATEG") = True Then Call UF_CATEG
    If Gerar_Especificas_Por_Dep_Filial("ALIQUOTA_ME") = True Then Call ALIQUOTA_ME


    '-----------------
    'Bases Especificas
    '-----------------
    
    'If Gerar_Especificas("DEPOSITO") = True Then Call DEPOSITO
    If Gerar_Especificas("REPRESENTANTE") = True Then Call REPRESENTANTE
    If Gerar_Especificas("FRANQUEADOR") = True Then Call FRANQUEADOR
    If Gerar_Especificas("REPR_END_CORRESP") = True Then Call REPR_END_CORRESP
    If Gerar_Especificas("REPRESENTACAO") = True Then Call REPRESENTACAO
    If Gerar_Especificas("R_REPCGC") = True Then Call R_REPCGC
    If Gerar_Especificas("R_REPVEN") = True Then Call R_REPVEN
    If Gerar_Especificas("CLIENTE") = True Then Call CLIENTE
    If Gerar_Especificas("CLIE_CREDITO") = True Then Call CLIE_CREDITO
    If Gerar_Especificas("CLIE_ENDERECO") = True Then Call CLIE_ENDERECO
    If Gerar_Especificas("CLIENTE_INTERNET") = True Then Call CLIE_INTERNET
    If Gerar_Especificas("SALDO_PEDIDOS") = True Then Call SALDO_PEDIDOS
    If Gerar_Especificas("DUPLICATAS") = True Then Call DUPLICATAS
    If Gerar_Especificas("PEDNOTA_VENDA") = True Then Call PEDNOTA_VENDA
    If Gerar_Especificas("ITPEDNOTA_VENDA") = True Then Call ITPEDNOTA_VENDA
    If Gerar_Especificas("R_PEDIDO_CONF") = True Then Call R_PEDIDO_CONF
    If Gerar_Especificas("ROMANEIO") = True Then Call ROMANEIO
    If Gerar_Especificas("V_CLIENTE_FIEL") = True Then Call V_CLIENTE_FIEL
    If Gerar_Especificas("V_PEDLIQ_VENDA") = True Then Call V_PEDLIQ_VENDA
    If Gerar_Especificas("R_CLIE_REPRES") = True Then Call R_CLIE_REPRES
    If Gerar_Especificas("CLIENTE_ACUMULADO") = True Then Call CLIENTE_ACUMULADO
    
    '---
    'VDR
    '---
    If vFL_VDR = "S" Then
       If Gerar_VDR("VDR_CLIENTE_CATEG_VDR") = True Then Call CLIENTE_CATEG_VDR
       If Gerar_VDR("VDR_CONTROLE_VDR") = True Then Call VDR_CONTROLE_VDR
       If Gerar_VDR("VDR_ITEM_PRECO") = True Then Call VDR_ITEM_PRECO
       If Gerar_VDR("VDR_R_CLIE_LINHA_PRODUTO") = True Then Call VDR_R_CLIE_LINHA_PRODUTO
       If Gerar_VDR("VDR_DESCONTO_CATEG") = True Then Call VDR_DESCONTO_CATEG
       If Gerar_VDR("VDR_TABELA_VENDA") = True Then Call VDR_TABELA_VENDA
    End If

    Screen.MousePointer = 0
         
TrataErro:
    If Err.Number = 58 Then
        If MsgBox("J� existe um arquivo com este nome, deseja substitui-lo ?", vbYesNo + vbQuestion, "Aten��o") = vbYes Then
            Kill vGerarBaseEm & "0000" & frmGerar.txtCodPaacRepres & ".MDB"
            Resume
        End If
    ElseIf Err.Number <> 0 Then
        MsgBox "C�digo: " & Err.Number & vbCrLf & "Descri��o: " & Err.Description
    End If
End Sub

Public Sub LOG()

    Dim ii As Integer

    Open "C:\Log_Paac_Repres.txt" For Output As #6
    
    If frmGerar.lstEspecificas.SelCount > 0 Then
        For ii = 0 To frmGerar.lstEspecificas.ListCount - 1
            If frmGerar.lstEspecificas.Selected(ii) = True Then
                Print #6, vTipoBase & " - " & Now & " - " & frmGerar.lstEspecificas.List(ii)
            End If
        Next
    End If
    If frmGerar.lstEspecificasPorLojaRepres.SelCount > 0 Then
        For ii = 0 To frmGerar.lstEspecificasPorLojaRepres.ListCount - 1
            If frmGerar.lstEspecificasPorLojaRepres.Selected(ii) = True Then
                Print #6, vTipoBase & " - " & Now & " - " & frmGerar.lstEspecificasPorLojaRepres.List(ii)
            End If
        Next
    End If
    If frmGerar.lstGenericas.SelCount > 0 Then
        For ii = 0 To frmGerar.lstGenericas.ListCount - 1
            If frmGerar.lstGenericas.Selected(ii) = True Then
                Print #6, vTipoBase & " - " & Now & " - " & frmGerar.lstGenericas.List(ii)
            End If
        Next
    End If
    If frmGerar.lstVDR.SelCount > 0 Then
        For ii = 0 To frmGerar.lstVDR.ListCount - 1
            If frmGerar.lstVDR.Selected(ii) = True Then
                Print #6, vTipoBase & " - " & Now & " - " & frmGerar.lstVDR.List(ii)
            End If
        Next
    End If
    
    Close #6
        
End Sub

'Public Sub VERIFICA_CADASTRO()
'
'    vBanco_Prod.Parameters_Prod.Remove "vTipoBase"
'    vBanco_Prod.Parameters_Prod.Add "vTipoBase", vTipoBase, 1
'    vBanco_Prod.Parameters_Prod.Remove "PM_CODIGO"
'    vBanco_Prod.Parameters_Prod.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1
'
'    Criar_Cursor vBanco_Prod
'    vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_VERIFICA_CADASTRO(:PM_CODIGO,:vTipoBase,:VCURSOR); END;"
'    vBanco_Prod.ExecuteSQL vSql
'
'    Set vObjOracle = vBanco_Prod.Parameters("vCursor").Value
'
'    If vObjOracle.EOF Then
'        MsgBox "Este c�digo n�o existe, por favor fa�a o Cadastro!"
'        frmCadastro.Show 1
'    Else
'
'        frmCadastro.txtCodigo.Text = frmGerar.txtCodPaacRepres
'        frmCadastro.txtEmail.Text = IIf(IsNull(vObjOracle!e_mail), "", vObjOracle!e_mail)
'        frmCadastro.cboCD.Text = vObjOracle!TIPO_LOJA
'
'        If vObjOracle!FL_VDR = "S" Then
'            frmCadastro.cbo_VDR.Text = "S - SIM"
'        Else
'            frmCadastro.cbo_VDR.Text = "N - NAO"
'        End If
'
'        If vObjOracle!FL_BASE = "S" Then
'            frmCadastro.cboBase.Text = "S - SIM"
'        Else
'            frmCadastro.cboBase.Text = "N - NAO"
'        End If
'
'        frmCadastro.Show vbModal
'
'    End If
'
'    If vFl_Base = "N" Then
'        End
'    Else
'        If MsgBox("Deseja gerar a base completa agora?", vbYesNo) = vbNo Then
'            End
'        End If
'    End If
'
'End Sub

Function Gerar_Genericas(Tabela As String) As Boolean
    For i = 0 To frmGerar.lstGenericas.ListCount - 1
        If UCase(frmGerar.lstGenericas.List(i)) = UCase(Tabela) Then
            If frmGerar.lstGenericas.Selected(i) = True Then
                Gerar_Genericas = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_Genericas_Dbf(Tabela As String) As Boolean
    For i = 0 To frmGerarDBF.lstGenericas.ListCount - 1
        If UCase(frmGerarDBF.lstGenericas.List(i)) = UCase(Tabela) Then
            If frmGerarDBF.lstGenericas.Selected(i) = True Then
                Gerar_Genericas_Dbf = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_Especificas_Por_Dep_Filial(Tabela As String) As Boolean
    For i = 0 To frmGerar.lstEspecificasPorLojaRepres.ListCount - 1
        If UCase(frmGerar.lstEspecificasPorLojaRepres.List(i)) = UCase(Tabela) Then
            If frmGerar.lstEspecificasPorLojaRepres.Selected(i) = True Then
                Gerar_Especificas_Por_Dep_Filial = True
                Exit For
            End If
        End If
    Next
End Function
Function Gerar_Especificas_Por_Dep_Filial_DBF(Tabela As String) As Boolean
    For i = 0 To frmGerarDBF.lstEspecificasPorLojaRepres.ListCount - 1
        If UCase(frmGerarDBF.lstEspecificasPorLojaRepres.List(i)) = UCase(Tabela) Then
            If frmGerarDBF.lstEspecificasPorLojaRepres.Selected(i) = True Then
                Gerar_Especificas_Por_Dep_Filial_DBF = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_Especificas(Tabela As String) As Boolean
    For i = 0 To frmGerar.lstEspecificas.ListCount - 1
        If UCase(frmGerar.lstEspecificas.List(i)) = UCase(Tabela) Then
            If frmGerar.lstEspecificas.Selected(i) = True Then
                Gerar_Especificas = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_Especificas_DBF(Tabela As String) As Boolean
    For i = 0 To frmGerarDBF.lstEspecificas.ListCount - 1
        If UCase(frmGerarDBF.lstEspecificas.List(i)) = UCase(Tabela) Then
            If frmGerarDBF.lstEspecificas.Selected(i) = True Then
                Gerar_Especificas_DBF = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_VDR(Tabela As String) As Boolean
    For i = 0 To frmGerar.lstVDR.ListCount - 1
        If UCase(frmGerar.lstVDR.List(i)) = UCase(Tabela) Then
            If frmGerar.lstVDR.Selected(i) = True Then
                Gerar_VDR = True
                Exit For
            End If
        End If
    Next
End Function

Function Gerar_VDR_DBF(Tabela As String) As Boolean
    For i = 0 To frmGerarDBF.lstVDR.ListCount - 1
        If UCase(frmGerarDBF.lstVDR.List(i)) = UCase(Tabela) Then
            If frmGerarDBF.lstVDR.Selected(i) = True Then
                Gerar_VDR_DBF = True
                Exit For
            End If
        End If
    Next
End Function


Public Function Pegar_VL_Parametro(NOME_PARAMETRO As String)
    
    vBanco_Prod.Parameters.Remove "Cod_Sistema"
    vBanco_Prod.Parameters.Add "Cod_Sistema", Pegar_Codigo_Sistema, 1
    vBanco_Prod.Parameters.Remove "Nome_Parametro"
    vBanco_Prod.Parameters.Add "Nome_Parametro", NOME_PARAMETRO, 1
    Criar_Cursor vBanco_Prod
    
    vErro = vVB_Generica_001.ExecutaPl(vBanco_Prod, "PRODUCAO.PKC_FIL430_PROMOTOR.PR_SELECT_VALOR_PARAMETRO(:vCURSOR, :Cod_Sistema, :Nome_Parametro)")
                            
    If vErro = "" Then
       Set vObjOracle = vBanco_Prod.Parameters("vCURSOR").Value
       Pegar_VL_Parametro = "" & vObjOracle("VL_PARAMETRO")
    End If

End Function

Public Function Pegar_Codigo_Sistema() As Double
   '-- Pegar o c�digo do sistema na tabela de HELPDESK.SOFTWARE usando o valor da Propriedade TITLE
    vBanco_Prod.Parameters.Remove "Nome_Software"
    vBanco_Prod.Parameters.Add "Nome_Software", UCase(App.Title), 1
    vBanco_Prod.Parameters.Remove "Cod_Software"
    vBanco_Prod.Parameters.Add "Cod_Software", 0, 2
    
    vVB_Generica_001.ExecutaPl vBanco_Prod, "PRODUCAO.PKC_FIL430_PROMOTOR.PR_SELECT_COD_SOFTWARE(:Nome_Software, :Cod_Software)"
    
    Pegar_Codigo_Sistema = vBanco_Prod.Parameters("Cod_Software").Value
        
    vBanco_Prod.Parameters.Remove "Cod_Software"

End Function


Public Function GetKeyVal(ByVal FileName As String, ByVal Section As String, ByVal Key As String)
    'Returns info from an INI file
    Dim RetVal As String, Worked As Integer
    If Dir(FileName) = "" Then MsgBox FileName & " N�o Encontrado.", vbCritical, "Aten��O": Exit Function
    RetVal = String$(255, 0)
    Worked = GetPrivateProfileString(Section, Key, "", RetVal, Len(RetVal), FileName)
    If Worked = 0 Then
        GetKeyVal = ""
    Else
        GetKeyVal = Left(RetVal, InStr(RetVal, Chr(0)) - 1)
    End If
End Function

Public Function AddToINI(ByVal FileName As String, ByVal Section As String, ByVal Key As String, ByVal KeyValue As String) As Integer
    'Add info to an INI file
    'Function returns 1 if successful and 0 if unsuccessful
    WritePrivateProfileString Section, Key, KeyValue, FileName
    AddToINI = 1
End Function


