Attribute VB_Name = "modFuncoes"
Option Explicit
'RTI-143 - Cria��o da vari�vel que armazenar� os Tipos de Loja
Dim strTipoLoja As String

Public Sub ALIQUOTA_ME()
          
1         On Error GoTo TrataErro
              
2         frmGerar.lblMensagem.Caption = " ALIQUOTA_ME - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
14        Criar_Cursor vBanco
          
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ALIQUOTA_ME(:TIPO_LOJA,:VCURSOR); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
                  
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "ALIQUOTA_ME"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "ALIQUOTA_ME - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("cgc") = vObjOracle!cgc
34           frmGerar.DataCompleta.Recordset("pc_icm") = vObjOracle!pc_icm
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
          
39
40        frmGerar.lblMensagem.Caption = "ALIQUOTA_ME - CONCLU�DO"
41        Gravar_Log
42        frmGerar.Refresh

          

TrataErro:
43        If Err.Number <> 0 Then
44            ERRO "Aliquota_ME", Err.Number, Err.Description
45            If Err.Number = 3022 Then
46                Resume Next
47            Else
48                MsgBox "Sub: Aliquota_ME" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
49            End If
50        End If
          
End Sub

Public Sub CESTA_VENDA()
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " CESTA_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1
          
8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
          
14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CESTA_VENDA(:TIPO_LOJA,:VCURSOR); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
                  
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "CESTA_VENDA"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh

26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "CESTA_VENDA - Inserindo Registros"
29        frmGerar.Refresh
  
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("tp_cesta") = vObjOracle!tp_cesta
34           frmGerar.DataCompleta.Recordset("nome_cesta") = vObjOracle!nome_cesta
35           frmGerar.DataCompleta.Recordset("dt_vigencia") = vObjOracle!dt_vigencia
36           frmGerar.DataCompleta.UpdateRecord
37           vObjOracle.MoveNext
38           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
39        Loop
          

41        frmGerar.lblMensagem.Caption = "CESTA_VENDA - CONCLU�DO"
42        Gravar_Log
43        frmGerar.Refresh
          
TrataErro:
44        If Err.Number <> 0 Then
45            ERRO "CESTA_VENDA", Err.Number, Err.Description
46            If Err.Number = 3022 Then
47                Resume Next
48            Else
49                MsgBox "Sub: CESTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
50            End If
51        End If
          
End Sub

Public Sub CESTA_ITEM()
1     On Error GoTo TrataErro


2         frmGerar.lblMensagem.Caption = " CESTA_ITEM - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
          
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
          
14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CESTA_ITEM(:TIPO_LOJA,:VCURSOR); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
                  
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           Set frmGerar.DataCompleta.Recordset = Nothing
21           frmGerar.DataCompleta.RecordSource = ""
22        Loop
23        frmGerar.DataCompleta.RecordSource = "CESTA_ITEM"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "CESTA_ITEM - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
34           frmGerar.DataCompleta.Recordset("tp_cesta") = vObjOracle!tp_cesta
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
          

40        frmGerar.lblMensagem.Caption = "CESTA_ITEM - CONCLU�DO"
41        Gravar_Log
42        frmGerar.Refresh
          
TrataErro:
         
43        If Err.Number <> 0 Then
44            ERRO "CESTA_ITEM", Err.Number, Err.Description
45            If Err.Number = 3022 Then
46               Resume Next
47            Else
48                MsgBox "Sub: CESTA_ITEM" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
49            End If
50        End If
          
End Sub

Public Sub CLIE_INTERNET()

1     On Error GoTo TrataErro
2         frmGerar.lblMensagem.Caption = " CLIENTE_INTERNET - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIE_INTERNET(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value

12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "CLIENTE_INTERNET"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "CLIENTE_INTERNET - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25            frmGerar.DataCompleta.Recordset.AddNew
26            frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
27            frmGerar.DataCompleta.Recordset("e_mail") = IIf(IsNull(vObjOracle!E_mail), "", vObjOracle!E_mail)
28            frmGerar.DataCompleta.Recordset("homepage") = vObjOracle!homepage
29            frmGerar.DataCompleta.UpdateRecord
30            vObjOracle.MoveNext
31            frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "CLIENTE_INTERNET - CONCLU�DO"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "CLIE_INTERNET", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: CLIE_INTERNET" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If

End Sub

Public Sub DELETA_R_DPK_EQUIV()
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " DELE��O R_DPK_EQUIV - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DEL_R_DPK_EQUIV(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "DELETA_R_DPK_EQUIV"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "DELETA_R_DPK_EQUIV - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
23           frmGerar.DataCompleta.Recordset("cod_dpk_eq") = vObjOracle!cod_dpk_eq
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = "DELE��O R_DPK_EQUIV - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
31        If Err.Number <> 0 Then
32            ERRO "DELETA_R_DPK_EQUIV", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: DELETA_R_DPK_EQUIV" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub DELETA_VDR_R_CLIE_LINHA_PRODUTO()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " DELETA_VDR_R_CLIE_LINHA_PRODUTO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DEL_R_CLIE_LINHA_PRODUTO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "DELETA_R_CLIE_LINHA_PRODUTO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "DELETA_VDR_R_CLIE_LINHA_PRODUTO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
23           frmGerar.DataCompleta.Recordset("linha_produto") = vObjOracle!linha_produto
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = "DELETA_VDR_R_CLIE_LINHA_PRODUTO - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
31        If Err.Number <> 0 Then
32            ERRO "DELETA_VDR_R_CLIE_LINHA_PRODUTO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: DELETA_VDR_R_CLIE_LINHA_PRODUTO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub DELETA_R_UF_DEPOSITO()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " DELETA_R_UF_DEPOSITO..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "TIPO_BASE"
5         vBanco.Parameters.Add "TIPO_BASE", "C", 1

6         Criar_Cursor vBanco
7         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DEL_R_UF_DEPOSITO(:TIPO_BASE,:VCURSOR); END;"
8         vBanco.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
10        Set frmGerar.DataCompleta.Recordset = Nothing
11        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
12           frmGerar.DataCompleta.RecordSource = ""
13           Set frmGerar.DataCompleta.Recordset = Nothing
14        Loop
15        frmGerar.DataCompleta.RecordSource = "DELETA_R_UF_DEPOSITO"
16        Deletar_Registros_Existentes
17        frmGerar.DataCompleta.Refresh

18        frmGerar.PB1.Value = 0
19        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
20        frmGerar.lblMensagem.Caption = "DELETA_R_UF_DEPOSITO - Inserindo Registros"
21        frmGerar.Refresh
          
22        Do While Not vObjOracle.EOF
23           frmGerar.DataCompleta.Recordset.AddNew
24           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
25           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
30        frmGerar.lblMensagem.Caption = "DELETA_R_UF_DEPOSITO - CONCLUIDO"
31        Gravar_Log
32        frmGerar.Refresh
          
TrataErro:
33        If Err.Number <> 0 Then
34            ERRO "DELETA_R_UF_DEPOSITO", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: DELETA_R_UF_DEPOSITO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If
          
End Sub

Public Sub R_CLIE_REPRES()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " R_CLIE_REPRES - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "COD_FILIAL"
5         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1
6         vBanco.Parameters.Remove "vTipoBase"
7         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_CLIE_REPRES(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "R_CLIE_REPRES"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "R_CLIE_REPRES - Inserindo Registros"

23        Do While Not vObjOracle.EOF
24           frmGerar.DataCompleta.Recordset.AddNew
25           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
26           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
27           frmGerar.DataCompleta.UpdateRecord
28           vObjOracle.MoveNext
29           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
30        Loop
31        frmGerar.lblMensagem.Caption = "R_CLIE_REPRES - CONCLU�DO"
32        Gravar_Log
33        frmGerar.Refresh
          
          
TrataErro:
34        If Err.Number <> 0 Then
35            ERRO "R_CLIE_REPRES", Err.Number, Err.Description
36            If Err.Number = 3022 Then
37                Resume Next
38            Else
39                MsgBox "Sub: R_CLIE_REPRES" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
40            End If
41        End If
End Sub

Public Sub R_DPK_EQUIV()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " R_DPK_EQUIV - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_DPK_EQUIV(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "R_DPK_EQUIV"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "R_DPK_EQUIV - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
23           frmGerar.DataCompleta.Recordset("cod_dpk_eq") = vObjOracle!cod_dpk_eq
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = "R_DPK_EQUIV - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
31        If Err.Number <> 0 Then
32            ERRO "R_DPK_EQUIV", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: R_DPK_EQUIV" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub
        

Public Sub VENDA_LIMITADA()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " VENDA_LIMITADA..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_VENDA_LIMITADA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VENDA_LIMITADA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VENDA_LIMITADA - Inserindo Registros"
19        frmGerar.Refresh

20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
23           frmGerar.DataCompleta.Recordset("tp_cliente") = vObjOracle!tp_cliente
24           frmGerar.DataCompleta.Recordset("cod_loja_obrigatoria") = vObjOracle!cod_loja_obrigatoria
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
          
29        frmGerar.lblMensagem.Caption = "VENDA_LIMITADA - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh
          
TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "VENDA_LIMITADA", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: VENDA_LIMITADA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub CLASS_ANTEC_ENTRADA()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " CLASS_ANTEC_ENTRADA..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLASS_ANTEC_ENTRADA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CLASS_ANTEC_ENTRADA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CLASS_ANTEC_ENTRADA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("class_fiscal") = vObjOracle!CLASS_FISCAL
23           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
24           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
25           frmGerar.DataCompleta.Recordset("cod_trib") = vObjOracle!cod_trib
26           frmGerar.DataCompleta.Recordset("PC_MARGEM_LUCRO") = vObjOracle!PC_MARGEM_LUCRO
27           frmGerar.DataCompleta.UpdateRecord
28           vObjOracle.MoveNext
29           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
30        Loop
          
31        frmGerar.lblMensagem.Caption = "CLASS_ANTEC_ENTRADA - CONCLU�DO"
32        Gravar_Log
33        frmGerar.Refresh
          
TrataErro:
         
34        If Err.Number <> 0 Then
35            ERRO "CLASS_ANTEC_ENTRADA", Err.Number, Err.Description
36            If Err.Number = 3022 Then
37                Resume Next
38            Else
39                MsgBox "Sub: CLASS_ANTEC_ENTRADA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
40            End If
41        End If

End Sub
Public Sub ANTECIPACAO_TRIBUTARIA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ANTECIPACAO_TRIBUTARIA..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ANTECIPACAO_TRIBUTARIA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "ANTECIPACAO_TRIBUTARIA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "ANTECIPACAO_TRIBUTARIA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
23           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
24           frmGerar.DataCompleta.Recordset("pc_margem_lucro") = vObjOracle!PC_MARGEM_LUCRO
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
          
29        frmGerar.lblMensagem.Caption = "ANTECIPACAO_TRIBUTARIA - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh
          
TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "ANTECIPACAO_TRIBUTARIA", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: ANTECIPACAO_TRIBUTARIA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If

End Sub

Public Sub RESULTADO()
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = "RESULTADO..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_RESULTADO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "RESULTADO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "RESULTADO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_resultado") = vObjOracle!cod_resultado
23           frmGerar.DataCompleta.Recordset("desc_resultado") = vObjOracle!desc_resultado
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = "RESULTADO - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "RESULTADO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: RESULTADO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub R_UF_DEPOSITO()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = "R_UF_DEPOSITO..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_UF_DEPOSITO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "R_UF_DEPOSITO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "R_UF_DEPOSITO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
23           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "R_UF_DEPOSITO - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "R_UF_DEPOSITO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: R_UF_DEPOSITO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub


Public Sub VDR_CONTROLE_VDR()
          
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " VDR_CONTROLE_VDR - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CONTROLE_VDR(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10            frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_CONTROLE_VDR"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_CONTROLE_VDR - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
23           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
24           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
25           frmGerar.DataCompleta.Recordset("cod_tipo_cliente") = vObjOracle!cod_tipo_cliente
26           frmGerar.DataCompleta.Recordset("cod_vdr") = vObjOracle!cod_vdr
27           frmGerar.DataCompleta.Recordset("linha_produto") = vObjOracle!linha_produto
28           frmGerar.DataCompleta.UpdateRecord
29           vObjOracle.MoveNext
30           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
31        Loop
          
32        frmGerar.lblMensagem.Caption = " VDR_CONTROLE_VDR - CONCLU�DO"
33        Gravar_Log
34        frmGerar.Refresh
          
TrataErro:
         
35        If Err.Number <> 0 Then
36            ERRO "VDR_CONTROLE_VDR", Err.Number, Err.Description
37            If Err.Number = 3022 Then
38                Resume Next
39            Else
40                MsgBox "Sub: VDR_CONTROLE_VDR" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
41            End If
42        End If
End Sub

Public Sub VDR_DESCONTO_CATEG()
          
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = "VDR_DESCONTO_CATEG - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DESCONTO_CATEG(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_DESCONTO_CATEG"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_DESCONTO_CATEG - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("categoria") = vObjOracle!categoria
23           frmGerar.DataCompleta.Recordset("pc_desc") = vObjOracle!pc_desc
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "VDR_DESCONTO_CATEG - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "VDR_DESCONTO_CATEG", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: VDR_DESCONTO_CATEG" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub VDR_ITEM_PRECO()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "VDR_ITEM_PRECO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_VDR_ITEM_PRECO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_ITEM_PRECO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_ITEM_PRECO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
23           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
24           frmGerar.DataCompleta.Recordset("preco_venda") = vObjOracle!preco_venda
25           frmGerar.DataCompleta.Recordset("preco_venda_ant") = vObjOracle!preco_venda_ant
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
30        frmGerar.lblMensagem.Caption = "VDR_ITEM_PRECO - CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh
          
TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "VDR_ITEM_PRECO", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: VDR_ITEM_PRECO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If
End Sub


Public Sub VDR_R_CLIE_LINHA_PRODUTO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " VDR_R_CLIE_LINHA_PRODUTO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_CLIE_LINHA_PRODUTO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_R_CLIE_LINHA_PRODUTO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_R_CLIE_LINHA_PRODUTO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
23           frmGerar.DataCompleta.Recordset("linha_produto") = vObjOracle!linha_produto
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = " VDR_R_CLIE_LINHA_PRODUTO - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "VDR_R_CLIE_LINHA_PRODUTO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: VDR_R_CLIE_LINHA_PRODUTO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub


Public Sub VDR_TABELA_VENDA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " VDR_TABELA_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_VDR_TABELA_VENDA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_TABELA_VENDA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_TABELA_VENDA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("divisao") = vObjOracle!divisao
23           frmGerar.DataCompleta.Recordset("tp_tabela") = vObjOracle!tp_tabela
24           frmGerar.DataCompleta.Recordset("ocorr_preco") = vObjOracle!ocorr_preco
25           frmGerar.DataCompleta.Recordset("tabela_venda") = vObjOracle!TABELA_VENDA
26           frmGerar.DataCompleta.Recordset("dt_vigencia1") = vObjOracle!dt_vigencia1
27           frmGerar.DataCompleta.Recordset("dt_vigencia2") = vObjOracle!dt_vigencia2
28           frmGerar.DataCompleta.Recordset("dt_vigencia3") = vObjOracle!dt_vigencia3
29           frmGerar.DataCompleta.Recordset("dt_vigencia4") = vObjOracle!dt_vigencia4
30           frmGerar.DataCompleta.Recordset("dt_vigencia5") = vObjOracle!dt_vigencia5
31           frmGerar.DataCompleta.UpdateRecord
32           vObjOracle.MoveNext
33           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
34        Loop
35        frmGerar.lblMensagem.Caption = " VDR_TABELA_VENDA - CONCLU�DO"
36        Gravar_Log
37        frmGerar.Refresh
TrataErro:
         
38        If Err.Number <> 0 Then
39            ERRO "VDR_TABELA_VENDA", Err.Number, Err.Description
40            If Err.Number = 3022 Then
41                Resume Next
42            Else
43                MsgBox "Sub: VDR_TABELA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
44            End If
45        End If

End Sub

'Public Sub VERIFICA_TIPO()
'   'VERIFICA QUAIS TIPOS DE BASE DEVER�O SER GERADAS
'    TIPO_FILIAL = False
'    TIPO_REPRES = False
'
'    msql = "select * from filial"
'    Set MSnp = dbcontrole.CreateSnapshot(msql)
'
'    If Not MSnp.EOF Then
'        TIPO_FILIAL = True
'    End If
'
'    msql = "select * from representante"
'    Set MSnp = dbcontrole.CreateSnapshot(msql)
'
'    If Not MSnp.EOF Then
'        TIPO_REPRES = True
'    End If
'End Sub
'

'Public Sub BUSCA_CICGER()
'   'Busca CIC do gerente para filtrar os arquivos espec�ficos
'
'    msql = "select cic_gerente from gerencia"
'    msql = msql & " where cod_gerente = " & Mid(arqmdb, 1, 4)
'
'    Set MSnp = dbcontrole.CreateSnapshot(msql)
'
'    Do While Not MSnp.EOF
'        w_cic_gerente = MSnp("cic_gerente")
'        MSnp.MoveNext
'    Loop
'End Sub

Public Sub CONEXAO_BANCO()
          
1         On Error GoTo TrataErro
          
          'Banco Batch para Gera��o da Base
2         Set vSessao = CreateObject("oracleinprocserver.xorasession")
'3         Set vBanco = vSessao.OpenDatabase("batch", "fil400/prod", 0&)
3         Set vBanco = vSessao.OpenDatabase("batch", "VDA020/PROD", 0&)

          ' // Variavel de Lista de Parametros
4         Set vBanco.Parameters = vBanco.Parameters
            
            
          'Banco Produ��o para Cadastros dos Dados do Novo Repres / PAAC
5         Set vSessao_Prod = CreateObject("oracleinprocserver.xorasession")
6         Set vBanco_Prod = vSessao_Prod.OpenDatabase("PRODUCAO", "fil400/prod", 0&)
          
          ' // Variavel de Lista de Parametros
7         Set vBanco.Parameters_Prod = vBanco_Prod.Parameters
          
          ' // Declarando o Bind de Cursor
8         vBanco.Parameters.Remove "vCursor"
9         vBanco.Parameters.Add "vCursor", 0, ORAPARM_BOTH
10        vBanco.Parameters("vCursor").serverType = ORATYPE_CURSOR
11        vBanco.Parameters("vCursor").DynasetOption = ORADYN_NO_BLANKSTRIP
12        vBanco.Parameters("vCursor").DynasetCacheParams 256, 16, 20, 2000, 0
              
          ' // Declarando o Bind de Cursor
13        vBanco.Parameters_Prod.Remove "vCursor_Prod"
14        vBanco.Parameters_Prod.Add "vCursor_Prod", 0, ORAPARM_BOTH
15        vBanco.Parameters_Prod("vCursor_Prod").serverType = ORATYPE_CURSOR
16        vBanco.Parameters_Prod("vCursor_Prod").DynasetOption = ORADYN_NO_BLANKSTRIP
17        vBanco.Parameters_Prod("vCursor_Prod").DynasetCacheParams 256, 16, 20, 2000, 0
              
          ' // Declarando o Bind de Cursor - TIPO_LOJA
18        vBanco.Parameters.Remove "vCursor2"
19        vBanco.Parameters.Add "vCursor2", 0, ORAPARM_BOTH
20        vBanco.Parameters("vCursor2").serverType = ORATYPE_CURSOR
21        vBanco.Parameters("vCursor2").DynasetOption = ORADYN_NO_BLANKSTRIP
22        vBanco.Parameters("vCursor2").DynasetCacheParams 256, 16, 20, 2000, 0
          
          ' // Declarando Bind de Erro
23        vBanco.Parameters.Remove "vErro"
24        vBanco.Parameters.Add "vErro", 0, ORAPARM_OUTPUT
25        vBanco.Parameters("vErro").serverType = ORATYPE_NUMBER
          
          ' // Declarando Bind de Erro
26        vBanco.Parameters_Prod.Remove "vErro_Prod"
27        vBanco.Parameters_Prod.Add "vErro_Prod", 0, ORAPARM_OUTPUT
28        vBanco.Parameters_Prod("vErro_Prod").serverType = ORATYPE_NUMBER
TrataErro:
         
29        If Err.Number <> 0 Then
30            ERRO "Conexao", Err.Number, Err.Description
31            If Err.Number = 3022 Then
32                Resume Next
33            Else
34                MsgBox "Sub: Conexao" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
35            End If
36        End If

End Sub

'Public Sub DEFINE_NOME_REPRESENTANTE(flag As Integer)
'    If flag = 1 Then
'       'Define nomes dos arquivos di�rios e/ou completa gen�ricos
'       'dir_extracao = ""
'        dir_extracao = dir_extracao & "EXTR" & w_num_representante & ".MDB"
'
'        If frmGerar.check2.Value = 1 Then
'            arqcompleta = dir_completa_repres & "00000000.MDB"
'        End If
'    ElseIf flag = 2 Then
'       'Define nomes dos arquivos di�rios e/ou completa espec�ficos
'        If frmGerar.check2.Value = 1 Then
'            arqcompleta = dir_completa_repres & Mid(arqmdb, 1, 4) & "0000.MDB"
'        End If
'    End If
'End Sub

'Public Sub DEFINE_BASE_COMPLETA()
'
'    Dim Arquivo_Completo
'
'    If TIPO_BASE = "C" Then
'       Nome_Base_Completa = "0000" & frmgerar.txtCodPaacRepres
'       Arquivo_Completo = dir_completa_filial & Nome_Base_Completa & ".MDB"
'       FileCopy dir_completa_filial & "0000.MDB", Arquivo_Completo
'    End If
'
'End Sub

'Public Sub BUSCA_CD_NOVO()
'
'    vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CD_NOVO(:VCURSOR); END;"
'    vBanco.ExecuteSQL vSql
'
'    ' // Jogando o Cursor da Package em uma vari�vel de ambiente
'    Set vObjOracle = vBanco.Parameters("vCursor").Value
'
'    CD_NOVO = vObjOracle!cod_Loja
'
'End Sub

Public Sub VERIFICA_CADASTRO()
        
1       On Error GoTo TrataErro
2         vBanco_Prod.Parameters.Remove "vTipoBase"
3         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
4         vBanco_Prod.Parameters.Remove "PM_CODIGO"
5         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1
          
6         Criar_Cursor vBanco_Prod
7         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_VERIFICA_CADASTRO(:PM_CODIGO,:vTipoBase,:VCURSOR); END;"
8         vBanco_Prod.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco_Prod.Parameters("vCursor").Value
          
10        If vObjOracle.EOF Then
11            MsgBox "Este c�digo n�o existe, por favor fa�a o Cadastro!"
12            Fl_Cadastro_OK = "N"
13            frmCadastro.Show 1
14        Else
15            Fl_Cadastro_OK = "S"
16            frmCadastro.txtCodigo.Text = frmGerar.txtCodPaacRepres
17            frmCadastro.txtEmail.Text = IIf(IsNull(vObjOracle!E_mail), "", vObjOracle!E_mail)
18            frmCadastro.cboCD.Text = vObjOracle!TIPO_LOJA
19            If vObjOracle!FL_VDR = "S" Then
20                frmCadastro.cbo_VDR.Text = "S - SIM"
21            Else
22                frmCadastro.cbo_VDR.Text = "N - NAO"
23            End If
24            If vObjOracle!FL_BASE = "S" Then
25                frmCadastro.cboBase.Text = "S - SIM"
26            Else
27                frmCadastro.cboBase.Text = "N - NAO"
28            End If
29            frmCadastro.Show vbModal
30        End If
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "VERIFICA_CADASTRO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: VERIFICA_CADASTRO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
          
End Sub

'Function F_BUSCA_E_MAIL(gerente As String) As String
'   'Busca e_mail do gerente para gravar no arquivo texto
'    vSql = "select e_mail from gerencia"
'    vSql = vSql & " where cod_gerente = " & gerente
'    Set MSnp = dbcontrole.CreateSnapshot(vSql)
'
'    Do While Not MSnp.EOF
'        F_BUSCA_E_MAIL = MSnp("e_mail")
'        MSnp.MoveNext
'    Loop
'End Function

Function F_BUSCA_E_MAIL_FILIAL(Filial As String, TP As String) As String
               
1         On Error GoTo TrataErro
               
2         vBanco.Parameters.Remove "COD_FILIAL"
3         vBanco.Parameters.Add "COD_FILIAL", Filial, 1
          
4         Criar_Cursor vBanco_Prod
5         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_BUSCA_EMAIL(:COD_FILIAL,:VCURSOR); END;"
6         vBanco_Prod.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         frmGerar.lblMensagem.Caption = "BUSCA E_MAIL"
9         If Not vObjOracle.EOF Then
10           F_BUSCA_E_MAIL_FILIAL = vObjOracle!E_mail
11        End If

TrataErro:
12        If Err.Number <> 0 Then
13            ERRO "F_BUSCA_E_MAIL_FILIAL", Err.Number, Err.Description
14            If Err.Number = 3022 Then
15                Resume Next
16            Else
17                MsgBox "Sub: F_BUSCA_E_MAIL_FILIAL" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
18            End If
19        End If
          
End Function

'Function F_BUSCA_E_MAIL_REPRESENTANTE(REPRESENTANTE As String) As String
'   'Busca e_mail do gerente para gravar no arquivo texto
'    vSql = "select e_mail from representante"
'    vSql = vSql & " where cic_representante = " & REPRESENTANTE
'    Set MSnp = dbcontrole.CreateSnapshot(vSql)
'    Do While Not MSnp.EOF
'
'    Loop
'End Function

Function F_NUM_MSGSAIDA() As String
          
1         On Error GoTo TrataErro
          
          Dim DYNcontrole As Dynaset
          Dim i As Integer
          
2         Set DYNcontrole = dbMail.CreateDynaset("SELECT ULTIMAMSGSAIDA FROM CONTROLE")
3         i = DYNcontrole("ULTIMAMSGSAIDA")
4         i = i + 1
5         If i = 9999 Then i = 1
6         dbMail.Execute "UPDATE CONTROLE SET ULTIMAMSGSAIDA = " & i
          
7         F_NUM_MSGSAIDA = Format(i, "000#")

TrataErro:
8         If Err.Number <> 0 Then
9             ERRO "F_NUM_MSGSAIDA", Err.Number, Err.Description
10            If Err.Number = 3022 Then
11                Resume Next
12            Else
13                MsgBox "Sub: F_NUM_MSGSAIDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
14            End If
15        End If
        
End Function

Public Sub UF_DEPOSITO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF_DEPOSITO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "PM_CODIGO"
5         vBanco_Prod.Parameters.Add "PM_CODIGO", vCodFilial, 1
6         vBanco_Prod.Parameters.Remove "vTipoBase"
7         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
8         vBanco_Prod.Parameters.Remove "PM_CODIGO"
9         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

10        Criar_Cursor vBanco_Prod
11        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
12        vBanco_Prod.ExecuteSQL vSql
13        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
14        vBanco.Parameters.Remove "TIPO_LOJA"
15        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

16        Criar_Cursor vBanco
17        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF_DEPOSITO(:TIPO_LOJA,:VCURSOR,:CD); END;"
18        vBanco.ExecuteSQL vSql
          
19        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
20        Set frmGerar.DataCompleta.Recordset = Nothing
21        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
22           frmGerar.DataCompleta.RecordSource = ""
23           Set frmGerar.DataCompleta.Recordset = Nothing
24        Loop
25        frmGerar.DataCompleta.RecordSource = "UF_DEPOSITO"
26        Deletar_Registros_Existentes
27        frmGerar.DataCompleta.Refresh

28        frmGerar.PB1.Value = 0
29        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
30        frmGerar.lblMensagem.Caption = "UF_DEPOSITO - Inserindo Registros"
31        frmGerar.Refresh
          
32        Do While Not vObjOracle.EOF
33           frmGerar.DataCompleta.Recordset.AddNew
34           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
35           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
36           frmGerar.DataCompleta.Recordset("pc_frete") = vObjOracle!pc_frete
37           frmGerar.DataCompleta.Recordset("inscr_estadual_st") = vObjOracle!inscr_estadual_st
38           frmGerar.DataCompleta.UpdateRecord
39           vObjOracle.MoveNext
40           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
41        Loop


43        frmGerar.lblMensagem.Caption = " UF_DEPOSITO - CONCLU�DO"
44        Gravar_Log
45        frmGerar.Refresh
          
TrataErro:
         
46        If Err.Number <> 0 Then
47            ERRO "UF_DEPOSITO", Err.Number, Err.Description
48            If Err.Number = 3022 Then
49                Resume Next
50            Else
51                MsgBox "Sub: UF_DEPOSITO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
52            End If
53        End If

End Sub
Public Sub UF_ORIGEM_DESTINO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF_ORIGEM_DESTINO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF_ORIGEM_DESTINO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
             
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "UF_ORIGEM_DESTINO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "UF_ORIGEM_DESTINO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
23           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
24           frmGerar.DataCompleta.Recordset("pc_icm") = vObjOracle!pc_icm
25           frmGerar.DataCompleta.Recordset("pc_dificm") = vObjOracle!pc_dificm
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
          
30        frmGerar.lblMensagem.Caption = " UF_ORIGEM_DESTINO - CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh
          
TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "UF_ORIGEM_DESTINO", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: UF_ORIGEM_DESTINO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If

End Sub

Public Sub UF_TPCLIENTE()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF_TPCLIENTE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF_TPCLIENTE(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "UF_TPCLIENTE"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "UF_TPCLIENTE - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
23           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
24           frmGerar.DataCompleta.Recordset("tp_cliente") = vObjOracle!tp_cliente
25           frmGerar.DataCompleta.Recordset("cod_tributacao") = vObjOracle!cod_tributacao
26           frmGerar.DataCompleta.Recordset("pc_desc") = vObjOracle!pc_desc
27           frmGerar.DataCompleta.Recordset("cod_tributacao_final") = vObjOracle!cod_tributacao_final
28           frmGerar.DataCompleta.Recordset("pc_desc_diesel") = vObjOracle!pc_desc_diesel
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = " UF_TPCLIENTE - CONCLU�DO"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "UF_TPCLIENTE", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: UF_TPCLIENTE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If

End Sub

Public Sub CLIENTE_CATEG_VDR()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIENTE_CATEG_VDR - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIENTE_CATEG_VDR(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "VDR_CLIENTE_CATEG_VDR"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "VDR_CLIENTE_CATEG_VDR - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
23           frmGerar.DataCompleta.Recordset("categoria") = vObjOracle!categoria
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = " CLIENTE_CATEG_VDR - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "CLIENTE_CATEG_VDR", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: CLIENTE_CATEG_VDR" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If

End Sub

'Public Sub VERIF_0000MDB()
'
'    If TIPO_FILIAL = True Then
'        arqmdb = Dir(dir_base_filial)
'        If arqmdb = "" Then
'            MsgBox "ARQUIVO " & dir_base_filial & " N�O ENCONTRADO, PROBLEMA NO PATH DESTE USU�RIO", 0, "ATEN��O!!!"
'            End
'        End If
'    End If
'
'    If TIPO_REPRES = True Then
'        arqmdb = Dir(dir_base_repres)
'        If arqmdb = "" Then
'            MsgBox "ARQUIVO " & dir_base_repres & " N�O ENCONTRADO, PROBLEMA NO PATH DESTE USU�RIO", 0, "ATEN��O!!!"
'            End
'        End If
'    End If
'
'End Sub


Public Sub R_REPVEN()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " R_REPVEN - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "PM_CODIGO"
7         vBanco.Parameters.Add "PM_CODIGO", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_REPVEN(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "R_REPVEN"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh

20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "R_REPVEN - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
27           frmGerar.DataCompleta.Recordset("cod_vend") = vObjOracle!COD_VEND
28           frmGerar.DataCompleta.UpdateRecord
29           vObjOracle.MoveNext
30           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
31        Loop
32        frmGerar.lblMensagem.Caption = " R_REPVEN - CONCLUIDO"
33        Gravar_Log
34        frmGerar.Refresh

TrataErro:
         
35        If Err.Number <> 0 Then
36            ERRO "R_REPVEN", Err.Number, Err.Description
37            If Err.Number = 3022 Then
38                Resume Next
39            Else
40                MsgBox "Sub: R_REPVEN" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
41            End If
42        End If

End Sub
Public Sub R_FILDEP()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " R_FILDEP - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "CD"
5         vBanco.Parameters.Add "CD", vCD_Novo, 1

6         Criar_Cursor vBanco
7         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_FIL_DEP(:VCURSOR); END;"
8         vBanco.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
10        Set frmGerar.DataCompleta.Recordset = Nothing
11        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
12           frmGerar.DataCompleta.RecordSource = ""
13           Set frmGerar.DataCompleta.Recordset = Nothing
14        Loop
15        frmGerar.DataCompleta.RecordSource = "R_FILDEP"
16        Deletar_Registros_Existentes
17        frmGerar.DataCompleta.Refresh

18        frmGerar.PB1.Value = 0
19        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
20        frmGerar.lblMensagem.Caption = "R_FILDEP - Inserindo Registros"

21        frmGerar.Refresh
22        Do While Not vObjOracle.EOF
23           frmGerar.DataCompleta.Recordset.AddNew
24           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
25           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
30        frmGerar.lblMensagem.Caption = " R_FILDEP - Consultando CONCLUIDO"
31        Gravar_Log
32        frmGerar.Refresh

TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "R_FILDEP", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: R_FILDEP" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If

End Sub


Public Sub CLIENTE_CARACTERISTICA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIENTE_CARACTERISTICA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIENTE_CARACTERISTICA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
              
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CLIENTE_CARACTERISTICA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CLIENTE_CARACTERISTICA - Inserindo Registros"
19        frmGerar.Refresh

20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("caracteristica") = vObjOracle!caracteristica
23           frmGerar.DataCompleta.Recordset("desc_caracteristica") = vObjOracle!desc_caracteristica
24           frmGerar.DataCompleta.Recordset("fl_desc_automatico") = vObjOracle!fl_desc_automatico
25           frmGerar.DataCompleta.Recordset("fl_red_comis") = vObjOracle!fl_red_comis
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
          
30        frmGerar.lblMensagem.Caption = " CLIENTE_CARACTERISTICA - Consultando CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh

TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "CLIENTE_CARACTERISTICA", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: CLIENTE_CARACTERISTICA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If
          
End Sub

'Public Sub DELETA_REPRESENTANTE()
'    frmGerar.lblMensagem.Caption = " DELETA_REPRESENTANTE - Consultando"
'    frmGerar.Refresh
'
'    frmGerar.lblMensagem.Caption = " DELETA_REPRESENTANTE - Consultando"
'    frmGerar.Refresh
'
'    Dim diretorio_origem As String
'    Dim diretorio_destino
'
'    diretorio_origem = "D:\DADOS\DIARIA\NOVA\"
'    diretorio_destino = "D:\DADOS\DIARIA\"
'    DoEvents
'
'    FileCopy diretorio_origem & "DEL_REP.DBF", diretorio_destino & "DEL_REP.DBF"
'    DoEvents
'
'    msql = "INSERT INTO del_rep(COD_REPRES)"
'    msql = msql & " SELECT val(chave_tab1)AS cod_repres"
'    msql = msql & " FROM producao_atuatab"
'    msql = msql & " IN '" & ARQUIVO_EXTRACAO & "'"
'    msql = msql & " WHERE val(chave_tab2) = " & w_cic_gerente & " AND"
'    msql = msql & "       cod_tabela = 'EF'"
'
'    DBF.Execute msql
'End Sub

Public Sub DELETA_R_CLIE_REPRES()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DELETA_R_CLIE_REPRES - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "COD_FILIAL"
5         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

6         Criar_Cursor vBanco
7         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DELETA_R_CLIE_REPRES(:COD_FILIAL,:VCURSOR); END;"
8         vBanco.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
10        Set frmGerar.DataCompleta.Recordset = Nothing
11        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
12           frmGerar.DataCompleta.RecordSource = ""
13           Set frmGerar.DataCompleta.Recordset = Nothing
14        Loop
15        frmGerar.DataCompleta.RecordSource = "DELETA_R_CLIE_REPRES"
16        Deletar_Registros_Existentes
17        frmGerar.DataCompleta.Refresh

18        frmGerar.PB1.Value = 0
19        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
20        frmGerar.lblMensagem.Caption = "DELETA_R_CLIE_REPRES - Inserindo Registros"
21        frmGerar.Refresh
          
22        Do While Not vObjOracle.EOF
23           frmGerar.DataCompleta.Recordset.AddNew
24           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
25           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
          
30        frmGerar.lblMensagem.Caption = " DELETA_R_CLIE_REPRES - CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh

TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "DELETA_R_CLIE_REPRES", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: DELETA_R_CLIE_REPRES" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If

End Sub

Public Sub DELETA_CLIENTE()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DELETA_CLIENTE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "COD_FILIAL"
5         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

6         Criar_Cursor vBanco
7         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DELETA_CLIENTE(:COD_FILIAL,:VCURSOR); END;"
8         vBanco.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
10        Set frmGerar.DataCompleta.Recordset = Nothing
11        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
12           frmGerar.DataCompleta.RecordSource = ""
13           Set frmGerar.DataCompleta.Recordset = Nothing
14        Loop
15        frmGerar.DataCompleta.RecordSource = "DELETA_CLIENTE"
16        Deletar_Registros_Existentes
17        frmGerar.DataCompleta.Refresh

18        frmGerar.PB1.Value = 0
19        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
20        frmGerar.lblMensagem.Caption = "DELETA_CLIENTE - Inserindo Registros"
21        frmGerar.Refresh
          
22        Do While Not vObjOracle.EOF
23           frmGerar.DataCompleta.Recordset.AddNew
24           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
          
29        frmGerar.lblMensagem.Caption = " DELETA_CLIENTE - CONCLUIDO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "DELETA_CLIENTE", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: DELETA_CLIENTE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If

End Sub
Public Sub DELETA_CLIENTE_categ_vdr()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DELETA_CLIENTE_categ_vdr - Consultando"
3         frmGerar.lblMensagem.Refresh
4         frmGerar.Refresh
          
5         Criar_Cursor vBanco
6         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DELETA_CLIENTE_CATEG_VDR(:VCURSOR); END;"
7         vBanco.ExecuteSQL vSql
          
8         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
9         Set frmGerar.DataCompleta.Recordset = Nothing
10        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
11           frmGerar.DataCompleta.RecordSource = ""
12           Set frmGerar.DataCompleta.Recordset = Nothing
13        Loop
14        frmGerar.DataCompleta.RecordSource = "DELETA_CLIENTE_CATEG_VDR"
15        Deletar_Registros_Existentes
16        frmGerar.DataCompleta.Refresh

17        frmGerar.PB1.Value = 0
18        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
19        frmGerar.lblMensagem.Caption = "DELETA_CLIENTE_CATEG_VDR - Inserindo Registros"
20        frmGerar.Refresh
          
21        Do While Not vObjOracle.EOF
22           frmGerar.DataCompleta.Recordset.AddNew
23           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = " DELETA_CLIENTE_categ_vdr - CONCLUIDO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "DELETA_CLIENTE_categ_vdr", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: DELETA_CLIENTE_categ_vdr" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub APLICACAO()

1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = "APLICACAO - Consultando "
3         frmGerar.lblMensagem.Refresh
4         frmGerar.Refresh
          
5         Criar_Cursor vBanco
6         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_APLICACAO(:VCURSOR); END;"
7         vBanco.ExecuteSQL vSql
          
8         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
9         Set frmGerar.DataCompleta.Recordset = Nothing
10        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
11           frmGerar.DataCompleta.RecordSource = ""
12           Set frmGerar.DataCompleta.Recordset = Nothing
13        Loop
14        frmGerar.DataCompleta.RecordSource = "APLICACAO"
          
15        Deletar_Registros_Existentes

16        frmGerar.DataCompleta.Refresh
          
17        frmGerar.PB1.Value = 0
18        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
19        frmGerar.lblMensagem.Caption = "APLICACAO - Inserindo Registros"
20        frmGerar.Refresh
          
21        Do While Not vObjOracle.EOF
22           frmGerar.DataCompleta.Recordset.AddNew
23           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
24           frmGerar.DataCompleta.Recordset("cod_categoria") = vObjOracle!cod_categoria
25           frmGerar.DataCompleta.Recordset("cod_montadora") = vObjOracle!cod_montadora
26           frmGerar.DataCompleta.Recordset("sequencia") = vObjOracle!sequencia
27           frmGerar.DataCompleta.Recordset("cod_original") = vObjOracle!cod_original
28           frmGerar.DataCompleta.Recordset("desc_aplicacao") = vObjOracle!desc_aplicacao
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "APLICACAO - CONCLU�DA"
34        If UCase(frmGerar.Name) = "FRMGERAR" Then Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "APLICACAO", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: APLICACAO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If

End Sub

Public Sub SALDO_PEDIDOS()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " SALDO_PEDIDOS - Consultando"
3         frmGerar.lblMensagem.Refresh
          frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_SALDO_PEDIDOS(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "SALDO_PEDIDOS"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "SALDO PEDIDOS - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
23           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
24           frmGerar.DataCompleta.Recordset("saldo_pedidos") = vObjOracle!SALDO_PEDIDOS
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = " SALDO_PEDIDOS - CONCLU�DA"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "SALDO_PEDIDOS", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: SALDO_PEDIDOS" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub


Public Sub REPR_END_CORRESP()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " REPR_END_CORRESP - Consultando"
3         frmGerar.lblMensagem.Refresh
4         frmGerar.Refresh
          
5         vBanco.Parameters.Remove "vTipoBase"
6         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
7         vBanco.Parameters.Remove "COD_FILIAL"
8         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

9         Criar_Cursor vBanco
10        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_REPR_END_CORRESP(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
11        vBanco.ExecuteSQL vSql
          
12        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
13        Set frmGerar.DataCompleta.Recordset = Nothing
14        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
15           frmGerar.DataCompleta.RecordSource = ""
16           Set frmGerar.DataCompleta.Recordset = Nothing
17        Loop
18        frmGerar.DataCompleta.RecordSource = "REPR_END_CORRESP"
19        Deletar_Registros_Existentes
20        frmGerar.DataCompleta.Refresh
          
21        frmGerar.PB1.Value = 0
22        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
23        frmGerar.lblMensagem.Caption = "REPR_END_CORRESP - Inserindo Registros"
24        frmGerar.Refresh
          
25        Do While Not vObjOracle.EOF
26           frmGerar.DataCompleta.Recordset.AddNew
27           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
28           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
29           frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
30           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
31           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
32           frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
33           frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
34           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
35           frmGerar.DataCompleta.Recordset("celular") = vObjOracle!celular
36           frmGerar.DataCompleta.Recordset("cxpostal") = vObjOracle!cxpostal
37           frmGerar.DataCompleta.UpdateRecord
38           vObjOracle.MoveNext
39           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
40        Loop
41        frmGerar.lblMensagem.Caption = " REPR_END_CORRESP - CONCLU�DA"
42        Gravar_Log
43        frmGerar.Refresh

TrataErro:
         
44        If Err.Number <> 0 Then
45            ERRO "REPR_END_CORRESP", Err.Number, Err.Description
46            If Err.Number = 3022 Then
47                Resume Next
48            Else
49                MsgBox "Sub: REPR_END_CORRESP" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
50            End If
51        End If
End Sub


Public Sub R_REPCGC()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "R_REPCGC - Consultando"
3         frmGerar.lblMensagem.Refresh
4         frmGerar.Refresh
          
5         vBanco.Parameters.Remove "vTipoBase"
6         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
7         vBanco.Parameters.Remove "COD_FILIAL"
8         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

9         Criar_Cursor vBanco
10        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_REPCGC(:COD_FILIAL,:vTipoBase,:VCURSOR); END;"
11        vBanco.ExecuteSQL vSql
          
12        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
13        Set frmGerar.DataCompleta.Recordset = Nothing
14        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
15           frmGerar.DataCompleta.RecordSource = ""
16           Set frmGerar.DataCompleta.Recordset = Nothing
17        Loop
18        frmGerar.DataCompleta.RecordSource = "R_REPCGC"
19        Deletar_Registros_Existentes
20        frmGerar.DataCompleta.Refresh
          
21        frmGerar.PB1.Value = 0
22        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
23        frmGerar.lblMensagem.Caption = "R_REPCGC - Inserindo Registros"
24        frmGerar.Refresh
          
25        Do While Not vObjOracle.EOF
26           frmGerar.DataCompleta.Recordset.AddNew
27           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
28           frmGerar.DataCompleta.Recordset("cgc") = vObjOracle!cgc
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext

31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1

32        Loop
33        frmGerar.lblMensagem.Caption = "R_REPCGC - CONCLU�DA"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "R_REPCGC", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: R_REPCGC" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If
End Sub


Public Sub BANCO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "BANCO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_BANCO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "BANCO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "BANCO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_banco") = vObjOracle!cod_banco
23           frmGerar.DataCompleta.Recordset("sigla") = vObjOracle!Sigla
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
          
28        frmGerar.lblMensagem.Caption = "BANCO - CONCLU�DA"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "BANCO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: BANCO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub LOJA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " LOJA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "PM_CODIGO"
5         vBanco_Prod.Parameters.Add "PM_CODIGO", vCodFilial, 1
6         vBanco_Prod.Parameters.Remove "vTipoBase"
7         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
8         vBanco_Prod.Parameters.Remove "PM_CODIGO"
9         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

10        Criar_Cursor vBanco_Prod
11        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
12        vBanco_Prod.ExecuteSQL vSql
          
13        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
14        vBanco.Parameters.Remove "TIPO_LOJA"
15        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
16        Criar_Cursor vBanco
17        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_LOJA(:TIPO_LOJA,:VCURSOR); END;"
18        vBanco.ExecuteSQL vSql
          
19        Set vObjOracle = vBanco.Parameters("vCursor").Value
              
20        Set frmGerar.DataCompleta.Recordset = Nothing
21        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
22           frmGerar.DataCompleta.RecordSource = ""
23           Set frmGerar.DataCompleta.Recordset = Nothing
24        Loop
25        frmGerar.DataCompleta.RecordSource = "LOJA"
26        Deletar_Registros_Existentes
27        frmGerar.DataCompleta.Refresh
        
28        frmGerar.PB1.Value = 0
29        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
30        frmGerar.lblMensagem.Caption = "LOJA - Inserindo Registros"
31        frmGerar.Refresh
          
32      Do While Not vObjOracle.EOF
33         frmGerar.DataCompleta.Recordset.AddNew
34         frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
35         frmGerar.DataCompleta.Recordset("cgc") = vObjOracle!cgc
36         frmGerar.DataCompleta.Recordset("razao_social") = vObjOracle!razao_social
37         frmGerar.DataCompleta.Recordset("nome_fantasia") = vObjOracle!nome_fantasia
38         frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
39         frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
40         frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
41         frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
42         frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
43         frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
44         frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
45         frmGerar.DataCompleta.Recordset("dt_cadastr") = vObjOracle!dt_cadastr
46         frmGerar.DataCompleta.Recordset("inscr_estadual") = vObjOracle!inscr_estadual
47         frmGerar.DataCompleta.Recordset("repart_fiscal") = vObjOracle!repart_fiscal
48         frmGerar.DataCompleta.UpdateRecord
49         vObjOracle.MoveNext
50         frmGerar.PB1.Value = frmGerar.PB1.Value + 1
51      Loop

53      frmGerar.lblMensagem.Caption = " LOJA - CONCLU�DA"
54      Gravar_Log
55      frmGerar.Refresh

TrataErro:
         
56        If Err.Number <> 0 Then
57            ERRO "LOJA", Err.Number, Err.Description
58            If Err.Number = 3022 Then
59                Resume Next
60            Else
61                MsgBox "Sub: LOJA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
62            End If
63        End If
End Sub

Public Sub DEPOSITO_VISAO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DEPOSITO_VISAO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
          
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", vCodFilial, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
          
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13          vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DEPOSITO_VISAO(:TIPO_LOJA,:VCURSOR); END;"
16        vBanco.ExecuteSQL vSql
          
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "DEPOSITO_VISAO"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "DEPOSITO_VISAO - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("nome_programa") = vObjOracle!nome_programa
33           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
34           frmGerar.DataCompleta.UpdateRecord
35           vObjOracle.MoveNext
36           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
37        Loop
38        frmGerar.lblMensagem.Caption = "DEPOSITO_VISAO - CONCLU�DA"
39        Gravar_Log
40        frmGerar.Refresh

TrataErro:
         
41        If Err.Number <> 0 Then
42            ERRO "DEPOSITO_VISAO", Err.Number, Err.Description
43            If Err.Number = 3022 Then
44                Resume Next
45            Else
46                MsgBox "Sub: DEPOSITO_VISAO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
47            End If
48        End If
End Sub

Public Sub FRETE_ENTREGA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FRETE_ENTREGA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
          
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
             strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FRETE_ENTREGA(:TIPO_LOJA,:VCURSOR,:CD); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
              
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "FRETE_ENTREGA"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "FRETE_ENTREGA - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
34           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
35           frmGerar.DataCompleta.Recordset("cod_regiao") = vObjOracle!cod_regiao
36           frmGerar.DataCompleta.Recordset("cod_subregiao") = vObjOracle!cod_subregiao
37           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
38           frmGerar.DataCompleta.Recordset("tp_frete") = vObjOracle!tp_frete
39           frmGerar.DataCompleta.Recordset("vl_frete_entrega") = vObjOracle!vl_frete_entrega
40           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
41           frmGerar.DataCompleta.UpdateRecord
42           vObjOracle.MoveNext
43           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
44        Loop

46        frmGerar.lblMensagem.Caption = "FRETE_ENTREGA - CONCLU�DA"
47        Gravar_Log
48        frmGerar.Refresh

TrataErro:
         
49        If Err.Number <> 0 Then
50            ERRO "FRETE_ENTREGA", Err.Number, Err.Description
51            If Err.Number = 3022 Then
52                Resume Next
53            Else
54                MsgBox "Sub: FRETE_ENTREGA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
55            End If
56        End If
End Sub

Public Sub FRETE_UF()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "FRETE_UF - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1
8         Criar_Cursor vBanco_Prod

9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
          
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
           strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FRETE_UF(:TIPO_LOJA,:VCURSOR,:CD); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "FRETE_UF"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "FRETE_UF - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
34           frmGerar.DataCompleta.Recordset("cod_regiao") = vObjOracle!cod_regiao
35           frmGerar.DataCompleta.Recordset("cod_subregiao") = vObjOracle!cod_subregiao
36           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
37           frmGerar.DataCompleta.Recordset("vl_frete") = vObjOracle!vl_frete
38           frmGerar.DataCompleta.UpdateRecord
39           vObjOracle.MoveNext
40           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
41        Loop

43        frmGerar.lblMensagem.Caption = "FRETE_UF - CONCLU�DA"
44        Gravar_Log
45        frmGerar.Refresh

TrataErro:
         
46        If Err.Number <> 0 Then
47            ERRO "FRETE_UF", Err.Number, Err.Description
48            If Err.Number = 3022 Then
49                Resume Next
50            Else
51                MsgBox "Sub: FRETE_UF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
52            End If
53        End If
End Sub

Public Sub FRETE_UF_TRANSP()
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FRETE_UF_TRANSP - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "TIPO_LOJA"
13        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

14        Criar_Cursor vBanco
15        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FRETE_UF_TRANSP(:TIPO_LOJA,:VCURSOR,:CD); END;"
16        vBanco.ExecuteSQL vSql
17        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
18        Set frmGerar.DataCompleta.Recordset = Nothing
19        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
20           frmGerar.DataCompleta.RecordSource = ""
21           Set frmGerar.DataCompleta.Recordset = Nothing
22        Loop
23        frmGerar.DataCompleta.RecordSource = "FRETE_UF_TRANSP"
24        Deletar_Registros_Existentes
25        frmGerar.DataCompleta.Refresh
          
26        frmGerar.PB1.Value = 0
27        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
28        frmGerar.lblMensagem.Caption = "FRETE_UF_TRANSP - Inserindo Registros"
29        frmGerar.Refresh
          
30        Do While Not vObjOracle.EOF
31           frmGerar.DataCompleta.Recordset.AddNew
32           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
33           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
34           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
35           frmGerar.DataCompleta.Recordset("vl_frete") = vObjOracle!vl_frete
36           frmGerar.DataCompleta.UpdateRecord
37           vObjOracle.MoveNext
38           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
39        Loop

41        frmGerar.lblMensagem.Caption = "FRETE_UF_TRANSP - CONCLU�DA"
42        Gravar_Log
43        frmGerar.Refresh
TrataErro:
         
44        If Err.Number <> 0 Then
45            ERRO "FRETE_UF_TRANSP", Err.Number, Err.Description
46            If Err.Number = 3022 Then
47                Resume Next
48            Else
49                MsgBox "Sub: FRETE_UF_TRANSP" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
50            End If
51        End If
End Sub

Public Sub FRETE_UF_BLOQ()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FRETE_UF_BLOQ - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FRETE_UF_BLOQ(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "FRETE_UF_BLOQ"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "FRETE_UF_BLOQ - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
23           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
24           frmGerar.DataCompleta.Recordset("fl_bloqueio") = vObjOracle!fl_bloqueio
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = "FRETE_UF_BLOQ - CONCLU�DA"
30        Gravar_Log
31        frmGerar.Refresh
TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "FRETE_UF_BLOQ", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: FRETE_UF_BLOQ" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub CATEG_SINAL()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CATEG_SINAL - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CATEG_SINAL(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CATEG_SINAL"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CATEG_SINAL - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("categoria") = vObjOracle!categoria
23           frmGerar.DataCompleta.Recordset("sinal") = vObjOracle!sinal
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "CATEG_SINAL - CONCLU�DA"
29        Gravar_Log
30        frmGerar.Refresh
TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "CATEG_SINAL", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: CATEG_SINAL" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub
'Public Sub COTACAO()
'    frmGerar.lblMensagem.Caption = " COTACAO - Consultando"
'    frmGerar.Refresh
'   'Atualiza��o da tabela COTACAO
'   'Copia somente s estrutura da tabela, por isso n`ao entra na atualizacao diaria
'
'    If frmGerar.check2.Value = 1 Then
'        msql = "insert into cotacao IN " & Chr(34) & arqcompleta & Chr(34)
'        msql = msql & " SELECT * "
'        msql = msql & " FROM producao_cotacao"
'        dbextracao.Execute msql
'    End If
'End Sub

'Public Sub ITEM_COTACAO()
'    frmGerar.lblMensagem.Caption = " ITEM_COTACAO - Consultando"
'    frmGerar.Refresh
'    'Atualiza��o da tabela ITEM_COTACAO
'    'Copia somente s estrutura da tabela, por isso n`ao entra na atualizacao diaria
'
'    If frmGerar.check2.Value = 1 Then
'        msql = "insert into item_cotacao IN " & Chr(34) & arqcompleta & Chr(34)
'        msql = msql & " SELECT * "
'        msql = msql & " FROM producao_item_cotacao"
'        dbextracao.Execute msql
'    End If
'End Sub

Public Sub CIDADE()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CIDADE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CIDADE(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
        
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CIDADE"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CIDADE - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
23           frmGerar.DataCompleta.Recordset("nome_cidade") = vObjOracle!nome_cidade
24           frmGerar.DataCompleta.Recordset("tp_cidade") = vObjOracle!tp_cidade
25           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
26           frmGerar.DataCompleta.Recordset("cod_regiao") = vObjOracle!cod_regiao
27           frmGerar.DataCompleta.Recordset("cod_subregiao") = vObjOracle!cod_subregiao
28           frmGerar.DataCompleta.Recordset("caracteristica") = vObjOracle!caracteristica
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "CIDADE - CONCLU�DA"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "CIDADE", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: CIDADE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If
End Sub

Public Sub DATAS()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "DATAS - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DATAS(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop

13        frmGerar.DataCompleta.RecordSource = "DATAS"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
         
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "DATAS - Inserindo Registros"
19        frmGerar.Refresh
         
20       Do While Not vObjOracle.EOF
21          frmGerar.DataCompleta.Recordset.AddNew
22          frmGerar.DataCompleta.Recordset("dt_real") = vObjOracle!dt_real
23          frmGerar.DataCompleta.Recordset("dt_faturamento") = vObjOracle!dt_faturamento
24          frmGerar.DataCompleta.Recordset("dt_ini_fech_mensal") = vObjOracle!dt_ini_fech_mensal
25          frmGerar.DataCompleta.Recordset("dt_fin_fech_mensal") = vObjOracle!dt_fin_fech_mensal
26          frmGerar.DataCompleta.Recordset("dt_feriado_1") = vObjOracle!dt_feriado_1
27          frmGerar.DataCompleta.Recordset("dt_feriado_2") = vObjOracle!dt_feriado_2
28          frmGerar.DataCompleta.Recordset("fl_liber") = vObjOracle!fl_liber
29          frmGerar.DataCompleta.Recordset("dt_fora_semana") = vObjOracle!dt_fora_semana
30          frmGerar.DataCompleta.Recordset("dt_ini_fech_dia") = vObjOracle!dt_ini_fech_dia
31          frmGerar.DataCompleta.UpdateRecord
32          vObjOracle.MoveNext
33          frmGerar.PB1.Value = frmGerar.PB1.Value + 1
34       Loop
35       frmGerar.lblMensagem.Caption = "DATAS - CONCLU�DA"
36        Gravar_Log
37       frmGerar.Refresh
TrataErro:
         
38        If Err.Number <> 0 Then
39            ERRO "DATAS", Err.Number, Err.Description
40            If Err.Number = 3022 Then
41                Resume Next
42            Else
43                MsgBox "Sub: DATAS" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
44            End If
45        End If

End Sub


Public Sub ITPEDNOTA_VENDA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITPEDNOTA_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITPEDNOTA_VENDA(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "ITPEDNOTA_VENDA"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "ITPEDNOTA_VENDA - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_pedido") = vObjOracle!num_pedido
28           frmGerar.DataCompleta.Recordset("seq_pedido") = vObjOracle!seq_pedido
29           frmGerar.DataCompleta.Recordset("num_item_pedido") = vObjOracle!num_item_pedido
30           frmGerar.DataCompleta.Recordset("cod_loja_nota") = vObjOracle!cod_loja_nota
31           frmGerar.DataCompleta.Recordset("num_nota") = vObjOracle!num_nota
32           frmGerar.DataCompleta.Recordset("num_item_nota") = vObjOracle!num_item_nota
33           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
34           frmGerar.DataCompleta.Recordset("qtd_solicitada") = vObjOracle!qtd_solicitada
35           frmGerar.DataCompleta.Recordset("qtd_atendida") = vObjOracle!qtd_atendida
36           frmGerar.DataCompleta.Recordset("preco_unitario") = vObjOracle!preco_unitario
37           frmGerar.DataCompleta.Recordset("tabela_venda") = vObjOracle!TABELA_VENDA
38           frmGerar.DataCompleta.Recordset("pc_desc1") = vObjOracle!pc_desc1
39           frmGerar.DataCompleta.Recordset("pc_desc2") = vObjOracle!pc_desc2
40           frmGerar.DataCompleta.Recordset("pc_desc3") = vObjOracle!pc_desc3
41           frmGerar.DataCompleta.Recordset("pc_dificm") = vObjOracle!pc_dificm
42           frmGerar.DataCompleta.Recordset("pc_ipi") = vObjOracle!pc_ipi
43           frmGerar.DataCompleta.Recordset("pc_comiss") = vObjOracle!pc_comiss
44           frmGerar.DataCompleta.Recordset("pc_comisstlmk") = vObjOracle!pc_comisstlmk
45           frmGerar.DataCompleta.Recordset("cod_trib") = vObjOracle!cod_trib
46           frmGerar.DataCompleta.Recordset("cod_tribipi") = vObjOracle!cod_tribipi
47           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
48           frmGerar.DataCompleta.UpdateRecord
49           vObjOracle.MoveNext
50           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
51        Loop
52        frmGerar.lblMensagem.Caption = "ITPEDNOTA_VENDA - CONCLU�DA"
53        Gravar_Log
54        frmGerar.Refresh

TrataErro:
         
55        If Err.Number <> 0 Then
56            ERRO "ITPEDNOTA_VENDA", Err.Number, Err.Description
57            If Err.Number = 3022 Then
58                Resume Next
59            Else
60                MsgBox "Sub: ITPEDNOTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
61            End If
62        End If
End Sub

Public Sub R_PEDIDO_CONF()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " R_PEDIDO_CONF - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_PEDIDO_CONF(:COD_FILIAL,:vTipoBase,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "R_PEDIDO_CONF"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "R_PEDIDO_CONF - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_pedido") = vObjOracle!num_pedido
28           frmGerar.DataCompleta.Recordset("seq_pedido") = vObjOracle!seq_pedido
29           frmGerar.DataCompleta.Recordset("num_caixa") = vObjOracle!num_caixa
30           frmGerar.DataCompleta.Recordset("cod_conferente") = vObjOracle!cod_conferente
31           frmGerar.DataCompleta.Recordset("cod_embalador") = vObjOracle!cod_embalador
32           frmGerar.DataCompleta.Recordset("cod_expedidor") = vObjOracle!cod_expedidor
33           frmGerar.DataCompleta.Recordset("fl_pend_etiq") = vObjOracle!fl_pend_etiq
34           frmGerar.DataCompleta.Recordset("qtd_volume_parc") = vObjOracle!qtd_volume_parc
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
39        frmGerar.lblMensagem.Caption = "R_PEDIDO_CONF - CONCLU�DA"
40        Gravar_Log
41        frmGerar.Refresh

TrataErro:
         
42        If Err.Number <> 0 Then
43            ERRO "R_PEDIDO_CONF", Err.Number, Err.Description
44            If Err.Number = 3022 Then
45                Resume Next
46            Else
47                MsgBox "Sub: R_PEDIDO_CONF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
48            End If
49        End If
End Sub

Public Sub EMBALADOR()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " EMBALADOR - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_EMBALADOR(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "EMBALADOR"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16         frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "EMBALADOR - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_embal") = vObjOracle!cod_embal
23           frmGerar.DataCompleta.Recordset("nome_embal") = vObjOracle!nome_embal
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "EMBALADOR - CONCLU�DA"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "EMBALADOR", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: EMBALADOR" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub V_PEDLIQ_VENDA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " V_PEDLIQ_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_V_PEDLIQ_VENDA(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "V_PEDLIQ_VENDA"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "V_PEDLIQ_VENDA - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_pedido") = vObjOracle!num_pedido
28           frmGerar.DataCompleta.Recordset("seq_pedido") = vObjOracle!seq_pedido
29           frmGerar.DataCompleta.Recordset("num_item_pedido") = vObjOracle!num_item_pedido
30           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
31           frmGerar.DataCompleta.Recordset("pr_liquido") = vObjOracle!pr_liquido
32           frmGerar.DataCompleta.Recordset("vl_liquido") = vObjOracle!vl_liquido
33           frmGerar.DataCompleta.UpdateRecord
34           vObjOracle.MoveNext
35           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
36        Loop
37        frmGerar.lblMensagem.Caption = "V_PEDLIQ_VENDA - CONCLU�DA"
38        Gravar_Log
39        frmGerar.Refresh

TrataErro:
         
40        If Err.Number <> 0 Then
41            ERRO "V_PEDLIQ_VENDA", Err.Number, Err.Description
42            If Err.Number = 3022 Then
43                Resume Next
44            Else
45                MsgBox "Sub: V_PEDLIQ_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
46            End If
47        End If
End Sub

Public Sub ROMANEIO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ROMANEIO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ROMANEIO(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value

12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "ROMANEIO"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "ROMANEIO - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_pedido") = vObjOracle!num_pedido
28           frmGerar.DataCompleta.Recordset("seq_pedido") = vObjOracle!seq_pedido
29           frmGerar.DataCompleta.Recordset("dt_coleta") = vObjOracle!dt_coleta
30           frmGerar.DataCompleta.Recordset("dt_despacho") = vObjOracle!dt_despacho
31           frmGerar.DataCompleta.Recordset("num_romaneio") = vObjOracle!num_romaneio
32           frmGerar.DataCompleta.Recordset("num_conhecimento") = vObjOracle!num_conhecimento
33           frmGerar.DataCompleta.Recordset("num_carro") = vObjOracle!num_carro
34           frmGerar.DataCompleta.Recordset("qtd_volume") = vObjOracle!qtd_volume
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
39        frmGerar.lblMensagem.Caption = "ROMANEIO - CONCLU�DA"
40        Gravar_Log
41        frmGerar.Refresh

TrataErro:
         
42        If Err.Number <> 0 Then
43            ERRO "ROMANEIO", Err.Number, Err.Description
44            If Err.Number = 3022 Then
45                Resume Next
46            Else
47                MsgBox "Sub: ROMANEIO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
48            End If
49        End If
End Sub

Public Sub CLIENTE_ACUMULADO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIENTE_ACUMULADO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIENTE_ACUMULADO(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
         '-- Jogando o Cursor da Package em uma vari�vel de ambiente
11        Set vObjOracle = vBanco.Parameters("vCursor").Value

12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "CLIENTE_ACUMULADO"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "CLIENTE_ACUMULADO - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
27           frmGerar.DataCompleta.Recordset("ano_mes1") = vObjOracle!mes1
28           frmGerar.DataCompleta.Recordset("valor1") = vObjOracle!vl_1
29           frmGerar.DataCompleta.Recordset("ano_mes2") = vObjOracle!mes2
30           frmGerar.DataCompleta.Recordset("valor2") = vObjOracle!vl_2
31           frmGerar.DataCompleta.Recordset("ano_mes3") = vObjOracle!mes3
32           frmGerar.DataCompleta.Recordset("valor3") = vObjOracle!vl_3
33           frmGerar.DataCompleta.Recordset("ano_mes4") = vObjOracle!mes4
34           frmGerar.DataCompleta.Recordset("valor4") = vObjOracle!vl_4
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
39        frmGerar.lblMensagem.Caption = "CLIENTE_ACUMULADO - CONCLU�DA"
40        Gravar_Log
41        frmGerar.Refresh

TrataErro:
         
42        If Err.Number <> 0 Then
43            ERRO "CLIENTE_ACUMULADO", Err.Number, Err.Description
44            If Err.Number = 3022 Then
45                Resume Next
46            Else
47                MsgBox "Sub: CLIENTE_ACUMULADO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
48            End If
49        End If
End Sub
Public Sub V_CLIENTE_FIEL()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " V_CLIENTE_FIEL - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1
          
8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_V_CLIENTE_FIEL(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value

12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "V_CLIENTE_FIEL"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20         frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "V_CLIENTE_FIEL - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
27           frmGerar.DataCompleta.Recordset("tp_fiel") = vObjOracle!TP_FIEL
28           frmGerar.DataCompleta.Recordset("desc_fiel") = vObjOracle!DESC_FIEL
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "V_CLIENTE_FIEL - CONCLU�DA"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "V_CLIENTE_FIEL", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: V_CLIENTE_FIEL" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If
End Sub


Public Sub PEDNOTA_VENDA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " PEDNOTA_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_PEDNOTA_VENDA(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "PEDNOTA_VENDA"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "PEDNOTA_VENDA - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_pedido") = vObjOracle!num_pedido
28           frmGerar.DataCompleta.Recordset("seq_pedido") = vObjOracle!seq_pedido
29           frmGerar.DataCompleta.Recordset("cod_loja_nota") = vObjOracle!cod_loja_nota
30           frmGerar.DataCompleta.Recordset("num_nota") = vObjOracle!num_nota
31           frmGerar.DataCompleta.Recordset("num_pendente") = vObjOracle!num_pendente
32           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
33           frmGerar.DataCompleta.Recordset("tp_pedido") = vObjOracle!tp_pedido
34           frmGerar.DataCompleta.Recordset("tp_dpkblau") = vObjOracle!tp_dpkblau
35           frmGerar.DataCompleta.Recordset("tp_transacao") = vObjOracle!tp_transacao
36           frmGerar.DataCompleta.Recordset("dt_digitacao") = vObjOracle!dt_digitacao
37           frmGerar.DataCompleta.Recordset("dt_pedido") = vObjOracle!dt_pedido
38           frmGerar.DataCompleta.Recordset("dt_emissao_nota") = vObjOracle!dt_emissao_nota
39           frmGerar.DataCompleta.Recordset("dt_ssm") = vObjOracle!dt_ssm
40           frmGerar.DataCompleta.Recordset("dt_bloq_politica") = vObjOracle!dt_bloq_politica
41           frmGerar.DataCompleta.Recordset("dt_bloq_credito") = vObjOracle!dt_bloq_credito
42           frmGerar.DataCompleta.Recordset("dt_bloq_frete") = vObjOracle!dt_bloq_frete
43           frmGerar.DataCompleta.Recordset("cod_nope") = vObjOracle!cod_nope
44           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
45           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
46           frmGerar.DataCompleta.Recordset("cod_end_entrega") = vObjOracle!cod_end_entrega
47           frmGerar.DataCompleta.Recordset("cod_end_cobranca") = vObjOracle!cod_end_cobranca
48           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
49           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
50           frmGerar.DataCompleta.Recordset("cod_vend") = vObjOracle!COD_VEND
51           frmGerar.DataCompleta.Recordset("cod_plano") = vObjOracle!cod_plano
52           frmGerar.DataCompleta.Recordset("cod_banco") = vObjOracle!cod_banco
53           frmGerar.DataCompleta.Recordset("cod_cfo_dest") = vObjOracle!cod_cfo_dest
54           frmGerar.DataCompleta.Recordset("cod_cfo_oper") = vObjOracle!cod_cfo_oper
55           frmGerar.DataCompleta.Recordset("frete_pago") = vObjOracle!frete_pago
56           frmGerar.DataCompleta.Recordset("peso_bruto") = vObjOracle!peso_bruto
57           frmGerar.DataCompleta.Recordset("qtd_ssm") = vObjOracle!qtd_ssm
58           frmGerar.DataCompleta.Recordset("qtd_item_pedido") = vObjOracle!qtd_item_pedido
59           frmGerar.DataCompleta.Recordset("qtd_item_nota") = vObjOracle!qtd_item_nota
60           frmGerar.DataCompleta.Recordset("vl_contabil") = vObjOracle!vl_contabil
61           frmGerar.DataCompleta.Recordset("vl_ipi") = vObjOracle!vl_ipi
62           frmGerar.DataCompleta.Recordset("vl_baseicm1") = vObjOracle!vl_baseicm1
63           frmGerar.DataCompleta.Recordset("vl_baseicm2") = vObjOracle!vl_baseicm2
64           frmGerar.DataCompleta.Recordset("vl_base_1") = vObjOracle!vl_base_1
65           frmGerar.DataCompleta.Recordset("vl_base_2") = vObjOracle!vl_base_2
66           frmGerar.DataCompleta.Recordset("vl_base_3") = vObjOracle!vl_base_3
67           frmGerar.DataCompleta.Recordset("vl_baseisen") = vObjOracle!vl_baseisen
68           frmGerar.DataCompleta.Recordset("vl_base_5") = vObjOracle!vl_base_5
69           frmGerar.DataCompleta.Recordset("vl_base_6") = vObjOracle!vl_base_6
70           frmGerar.DataCompleta.Recordset("vl_baseoutr") = vObjOracle!vl_baseoutr
71           frmGerar.DataCompleta.Recordset("vl_basemaj") = vObjOracle!vl_basemaj
72           frmGerar.DataCompleta.Recordset("vl_icmretido") = vObjOracle!vl_icmretido
73           frmGerar.DataCompleta.Recordset("vl_baseipi") = vObjOracle!vl_baseipi
74           frmGerar.DataCompleta.Recordset("vl_bisenipi") = vObjOracle!vl_bisenipi
75           frmGerar.DataCompleta.Recordset("vl_boutripi") = vObjOracle!vl_boutripi
76           frmGerar.DataCompleta.Recordset("vl_frete") = vObjOracle!vl_frete
77           frmGerar.DataCompleta.Recordset("vl_desp_acess") = vObjOracle!vl_desp_acess
78           frmGerar.DataCompleta.Recordset("pc_desconto") = vObjOracle!pc_desconto
79           frmGerar.DataCompleta.Recordset("pc_desc_suframa") = vObjOracle!pc_desc_suframa
80           frmGerar.DataCompleta.Recordset("pc_acrescimo") = vObjOracle!pc_acrescimo
81           frmGerar.DataCompleta.Recordset("pc_seguro") = vObjOracle!pc_seguro
82           frmGerar.DataCompleta.Recordset("pc_icm1") = vObjOracle!pc_icm1
83           frmGerar.DataCompleta.Recordset("pc_icm2") = vObjOracle!pc_icm2
84           frmGerar.DataCompleta.Recordset("pc_aliq_interna") = vObjOracle!pc_aliq_interna
85           frmGerar.DataCompleta.Recordset("fl_ger_ssm") = vObjOracle!fl_ger_ssm
86           frmGerar.DataCompleta.Recordset("fl_ger_nfis") = vObjOracle!fl_ger_nfis
87           frmGerar.DataCompleta.Recordset("fl_pendencia") = vObjOracle!fl_pendencia
88           frmGerar.DataCompleta.Recordset("fl_desp_acess") = vObjOracle!fl_desp_acess
89           frmGerar.DataCompleta.Recordset("fl_dif_icm") = vObjOracle!fl_dif_icm
90           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
91           frmGerar.DataCompleta.Recordset("bloq_protesto") = vObjOracle!bloq_protesto
92           frmGerar.DataCompleta.Recordset("bloq_juros") = vObjOracle!bloq_juros
93           frmGerar.DataCompleta.Recordset("bloq_cheque") = vObjOracle!bloq_cheque
94           frmGerar.DataCompleta.Recordset("bloq_compra") = vObjOracle!bloq_compra
95           frmGerar.DataCompleta.Recordset("bloq_duplicata") = vObjOracle!bloq_duplicata
96           frmGerar.DataCompleta.Recordset("bloq_limite") = vObjOracle!bloq_limite
97           frmGerar.DataCompleta.Recordset("bloq_saldo") = vObjOracle!bloq_saldo
98           frmGerar.DataCompleta.Recordset("bloq_clie_novo") = vObjOracle!bloq_clie_novo
99           frmGerar.DataCompleta.Recordset("bloq_desconto") = vObjOracle!bloq_desconto
100          frmGerar.DataCompleta.Recordset("bloq_acrescimo") = vObjOracle!bloq_acrescimo
101          frmGerar.DataCompleta.Recordset("bloq_vlfatmin") = vObjOracle!bloq_vlfatmin
102          frmGerar.DataCompleta.Recordset("bloq_item_desc1") = vObjOracle!bloq_item_desc1
103          frmGerar.DataCompleta.Recordset("bloq_item_desc2") = vObjOracle!bloq_item_desc2
104          frmGerar.DataCompleta.Recordset("bloq_item_desc3") = vObjOracle!bloq_item_desc3
105          frmGerar.DataCompleta.Recordset("bloq_frete") = vObjOracle!bloq_frete
106          frmGerar.DataCompleta.Recordset("mens_pedido") = vObjOracle!mens_pedido
107          frmGerar.DataCompleta.Recordset("mens_nota") = vObjOracle!mens_nota
108          frmGerar.DataCompleta.Recordset("cod_vdr") = vObjOracle!cod_vdr
109          frmGerar.DataCompleta.UpdateRecord
110          vObjOracle.MoveNext
111          frmGerar.PB1.Value = frmGerar.PB1.Value + 1
112       Loop
113       frmGerar.lblMensagem.Caption = "PEDNOTA_VENDA - CONCLU�DA"
114       Gravar_Log
115       frmGerar.Refresh

TrataErro:
         
116       If Err.Number <> 0 Then
117           ERRO "PEDNOTA_VENDA", Err.Number, Err.Description
118           If Err.Number = 3022 Then
119               Resume Next
120           Else
121               MsgBox "Sub: PEDNOTA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
122           End If
123       End If
End Sub

Public Sub DOLAR_DIARIO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DOLAR_DIARIO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DOLAR_DIARIO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "DOLAR_DIARIO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "DOLAR_DIARIO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("data_uss") = vObjOracle!data_uss
23           frmGerar.DataCompleta.Recordset("valor_uss") = vObjOracle!valor_uss
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "DOLAR_DIARIO - CONCLU�DA"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "DOLAR_DIARIO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: DOLAR_DIARIO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub Filial()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FILIAL - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FILIAL(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "FILIAL"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "FILIAL - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
23           frmGerar.DataCompleta.Recordset("sigla") = vObjOracle!Sigla
24           frmGerar.DataCompleta.Recordset("tp_filial") = vObjOracle!tp_filial
25           frmGerar.DataCompleta.Recordset("cod_franqueador") = vObjOracle!cod_franqueador
26           frmGerar.DataCompleta.Recordset("cod_regional") = vObjOracle!cod_regional
27           frmGerar.DataCompleta.UpdateRecord
28           vObjOracle.MoveNext
29           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
30        Loop
31        frmGerar.lblMensagem.Caption = "FILIAL - CONCLU�DA"
32        Gravar_Log
33        frmGerar.Refresh

TrataErro:
         
34        If Err.Number <> 0 Then
35            ERRO "Filial", Err.Number, Err.Description
36            If Err.Number = 3022 Then
37                Resume Next
38            Else
39                MsgBox "Sub: Filial" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
40            End If
41        End If
End Sub
'Public Sub FILIAL_FRANQUIA()
'    frmGerar.lblMensagem.Caption = " FILIAL_FRANQUIA - Consultando"
'    frmGerar.Refresh
'
'    vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FILIAL_FRANQUIA(:VCURSOR); END;"
'    vBanco.ExecuteSQL vSql
'
'    Set vObjOracle = vBanco.Parameters("vCursor").Value
'
'    frmGerar.lblMensagem.Caption = "FILIAL_FRANQUIA"
'    frmGerar.DataCompleta.DatabaseName = "D:\DADOS\DIARIA\"
'    frmGerar.DataCompleta.RecordSource = "FIL_FRA"
'    frmGerar.DataCompleta.Refresh
'    Do While Not vObjOracle.EOF
'       frmGerar.DataCompleta.Recordset.AddNew
'       frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
'       frmGerar.DataCompleta.Recordset("seq_batch") = vObjOracle!seq_batch
'       frmGerar.DataCompleta.Recordset("e_mail") = vObjOracle!e_mail
'       frmGerar.DataCompleta.UpdateRecord
'       vObjOracle.MoveNext
'    Loop
'    frmGerar.DataCompleta.Refresh
'
'    'Set vBanco = Nothing
'    'Set vObjOracle = Nothing
'
'End Sub

Public Sub FORNECEDOR()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FORNECEDOR - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FORNECEDOR(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "FORNECEDOR"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "FORNECEDOR - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
23           frmGerar.DataCompleta.Recordset("sigla") = vObjOracle!Sigla
24           frmGerar.DataCompleta.Recordset("divisao") = vObjOracle!divisao
25           frmGerar.DataCompleta.Recordset("classificacao") = vObjOracle!classificacao
26           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
27           frmGerar.DataCompleta.UpdateRecord
28           vObjOracle.MoveNext
29           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
30        Loop
31        frmGerar.lblMensagem.Caption = "FORNECEDOR - CONCLU�DA"
32        Gravar_Log
33        frmGerar.Refresh

TrataErro:
         
34        If Err.Number <> 0 Then
35            ERRO "FORNECEDOR", Err.Number, Err.Description
36            If Err.Number = 3022 Then
37                Resume Next
38            Else
39                MsgBox "Sub: FORNECEDOR" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
40            End If
41        End If
End Sub

Public Sub FORNECEDOR_ESPECIFICO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "FORNECEDOR_ESPECIFICO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FORNECEDOR_ESPECIFICO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "FORNECEDOR_ESPECIFICO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "FORNECEDOR_ESPECIFICO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
23           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
24           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
25           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
26           frmGerar.DataCompleta.Recordset("fl_dif_icms") = vObjOracle!fl_dif_icms
27           frmGerar.DataCompleta.Recordset("fl_adicional") = vObjOracle!fl_adicional
28           frmGerar.DataCompleta.Recordset("tp_dif_icms") = vObjOracle!tp_dif_icms
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "FORNECEDOR_ESPECIFICO - CONCLU�DA"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "FORNECEDOR_ESPECIFICO", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: FORNECEDOR_ESPECIFICO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If
End Sub

Public Sub DEL_FORNECEDOR_ESPECIFICO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "DEL_FORNECEDOR_ESPECIFICO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "TIPO_BASE"
5         vBanco.Parameters.Add "TIPO_BASE", vTipoBase, 1

6         Criar_Cursor vBanco
7         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DEL_FORNECEDOR_ESPECIFICO(:TIPO_BASE,:VCURSOR); END;"
8         vBanco.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
10        Set frmGerar.DataCompleta.Recordset = Nothing
11        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
12           frmGerar.DataCompleta.RecordSource = ""
13           Set frmGerar.DataCompleta.Recordset = Nothing
14        Loop
15        frmGerar.DataCompleta.RecordSource = "DELETA_FORNECEDOR_ESPECIFICO"
16        Deletar_Registros_Existentes
17        frmGerar.DataCompleta.Refresh

18        frmGerar.PB1.Value = 0
19        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
20        frmGerar.lblMensagem.Caption = "DELETA_FORNECEDOR_ESPECIFICO - Inserindo Registros"
21        frmGerar.Refresh
          
22        Do While Not vObjOracle.EOF
23           frmGerar.DataCompleta.Recordset.AddNew
24           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
25           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
26           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
27           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
28           frmGerar.DataCompleta.UpdateRecord
29           vObjOracle.MoveNext
30           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
31        Loop
32        frmGerar.lblMensagem.Caption = "DEL_FORNECEDOR_ESPECIFICO - CONCLU�DO"
33        Gravar_Log
34        frmGerar.Refresh

TrataErro:
         
35        If Err.Number <> 0 Then
36            ERRO "DEL_FORNECEDOR_ESPECIFICO", Err.Number, Err.Description
37            If Err.Number = 3022 Then
38                Resume Next
39            Else
40                MsgBox "Sub: DEL_FORNECEDOR_ESPECIFICO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
41            End If
42        End If
End Sub

Public Sub GRUPO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " GRUPO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_GRUPO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "GRUPO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "GRUPO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
23           frmGerar.DataCompleta.Recordset("desc_grupo") = vObjOracle!desc_grupo
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "GRUPO - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "GRUPO", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: GRUPO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub
Public Sub ITEM_ANALITICO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITEM_ANALITICO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "CD"
7         vBanco_Prod.Parameters.Add "CD", vCD_Novo, 1
          
8         vBanco_Prod.Parameters.Remove "PM_CODIGO"
9         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

10        Criar_Cursor vBanco_Prod
11        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
12        vBanco_Prod.ExecuteSQL vSql
          
13        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
14        vBanco.Parameters.Remove "CD"
15        vBanco.Parameters.Add "CD", vCD_Novo, 1
16        vBanco.Parameters.Remove "TIPO_LOJA"
17        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
18        Criar_Cursor vBanco
          
19        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITEM_ANALITICO(:TIPO_LOJA,:VCURSOR,:CD); END;"
20        vBanco.ExecuteSQL vSql
21        Set vObjOracle = vBanco.Parameters("vCursor").Value
                  
22        Set frmGerar.DataCompleta.Recordset = Nothing
23        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
24           frmGerar.DataCompleta.RecordSource = ""
25           Set frmGerar.DataCompleta.Recordset = Nothing
26        Loop
27        frmGerar.DataCompleta.RecordSource = "ITEM_ANALITICO"
28        Deletar_Registros_Existentes
29        frmGerar.DataCompleta.Refresh
          
30        frmGerar.PB1.Value = 0
31        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
32        frmGerar.lblMensagem.Caption = "ITEM_ANALITICO - Inserindo Registros"
33        frmGerar.Refresh
          
34        Do While Not vObjOracle.EOF
35           frmGerar.DataCompleta.Recordset.AddNew
36           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
37           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
38           frmGerar.DataCompleta.Recordset("categoria") = vObjOracle!categoria
39           frmGerar.DataCompleta.Recordset("class_abc") = vObjOracle!class_abc
40           frmGerar.DataCompleta.Recordset("class_abcf") = vObjOracle!class_abcf
41           frmGerar.DataCompleta.Recordset("class_venda") = vObjOracle!class_venda
42           frmGerar.DataCompleta.Recordset("qtd_mes_ant") = vObjOracle!qtd_mes_ant
43           frmGerar.DataCompleta.Recordset("custo_medio") = vObjOracle!custo_medio
44           frmGerar.DataCompleta.Recordset("custo_medio_ant") = vObjOracle!custo_medio_ant
45           frmGerar.DataCompleta.UpdateRecord
46           vObjOracle.MoveNext
47           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
48        Loop

50        frmGerar.lblMensagem.Caption = "ITEM_ANALITICO - CONCLU�DO"
51        Gravar_Log
52        frmGerar.Refresh

TrataErro:
         
53        If Err.Number <> 0 Then
54            ERRO "ITEM_ANALITICO", Err.Number, Err.Description
55            If Err.Number = 3022 Then
56                Resume Next
57            Else
58                MsgBox "Sub: ITEM_ANALITICO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
59            End If
60        End If
End Sub

Public Sub ITEM_CADASTRO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITEM_CADASTRO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITEM_CADASTRO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "ITEM_CADASTRO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "ITEM_CADASTRO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
23           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
24           frmGerar.DataCompleta.Recordset("cod_fabrica") = vObjOracle!COD_FABRICA
25           frmGerar.DataCompleta.Recordset("desc_item") = vObjOracle!desc_item
26           frmGerar.DataCompleta.Recordset("cod_linha") = vObjOracle!cod_linha
27           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
28           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
29           frmGerar.DataCompleta.Recordset("dt_cadastramento") = vObjOracle!dt_cadastramento
30           frmGerar.DataCompleta.Recordset("mascarado") = vObjOracle!mascarado
31           frmGerar.DataCompleta.Recordset("class_fiscal") = vObjOracle!CLASS_FISCAL
32           frmGerar.DataCompleta.Recordset("peso") = vObjOracle!peso
33           frmGerar.DataCompleta.Recordset("volume") = vObjOracle!Volume
34           frmGerar.DataCompleta.Recordset("cod_unidade") = vObjOracle!cod_unidade
35           frmGerar.DataCompleta.Recordset("cod_tributacao") = vObjOracle!cod_tributacao
36           frmGerar.DataCompleta.Recordset("cod_tributacao_ipi") = vObjOracle!cod_tributacao_ipi
37           frmGerar.DataCompleta.Recordset("pc_ipi") = vObjOracle!pc_ipi
38           frmGerar.DataCompleta.Recordset("cod_procedencia") = vObjOracle!cod_procedencia
39           frmGerar.DataCompleta.Recordset("cod_dpk_ant") = vObjOracle!cod_dpk_ant
40           frmGerar.DataCompleta.Recordset("qtd_minforn") = vObjOracle!qtd_minforn
41           frmGerar.DataCompleta.Recordset("qtd_minvda") = vObjOracle!qtd_minvda
42           frmGerar.DataCompleta.UpdateRecord
43           vObjOracle.MoveNext
44           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
45        Loop
46        frmGerar.lblMensagem.Caption = " ITEM_CADASTRO - CONCLU�DO"
47        Gravar_Log
48        frmGerar.Refresh

TrataErro:
         
49        If Err.Number <> 0 Then
50            ERRO "ITEM_CADASTRO", Err.Number, Err.Description
51            If Err.Number = 3022 Then
52                Resume Next
53            Else
54                MsgBox "Sub: ITEM_CADASTRO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
55            End If
56        End If

End Sub
 
Public Sub ITEM_ESTOQUE()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITEM_ESTOQUE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco_Prod.Parameters.Remove "vTipoBase"
5         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco_Prod.Parameters.Remove "PM_CODIGO"
7         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

8         Criar_Cursor vBanco_Prod
9         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
10        vBanco_Prod.ExecuteSQL vSql
11        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
12        vBanco.Parameters.Remove "CD"
13        vBanco.Parameters.Add "CD", vCD_Novo, 1
14        vBanco.Parameters.Remove "TIPO_LOJA"
15        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
          
16        Criar_Cursor vBanco
17        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITEM_ESTOQUE(:TIPO_LOJA,:VCURSOR,:CD); END;"
18        vBanco.ExecuteSQL vSql
19        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
20        Set frmGerar.DataCompleta.Recordset = Nothing
21        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
22           frmGerar.DataCompleta.RecordSource = ""
23           Set frmGerar.DataCompleta.Recordset = Nothing
24        Loop
25        frmGerar.DataCompleta.RecordSource = "ITEM_ESTOQUE"
26        Deletar_Registros_Existentes
27        frmGerar.DataCompleta.Refresh
28        frmGerar.PB1.Value = 0
29        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
30        frmGerar.lblMensagem.Caption = "ITEM_ESTOQUE - Inserindo Registros"
31        frmGerar.Refresh
          
32        Do While Not vObjOracle.EOF
33           frmGerar.DataCompleta.Recordset.AddNew
34           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
35           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
36           frmGerar.DataCompleta.Recordset("qtd_atual") = vObjOracle!qtd_atual
37           frmGerar.DataCompleta.Recordset("qtd_reserv") = vObjOracle!qtd_reserv
38           frmGerar.DataCompleta.Recordset("qtd_pendente") = vObjOracle!qtd_pendente
39           frmGerar.DataCompleta.Recordset("qtd_maxvda") = vObjOracle!qtd_maxvda
40           frmGerar.DataCompleta.Recordset("cuema") = vObjOracle!cuema
41           frmGerar.DataCompleta.Recordset("dt_cuema") = vObjOracle!dt_cuema
42           frmGerar.DataCompleta.Recordset("vl_ult_compra") = vObjOracle!vl_ult_compra
43           frmGerar.DataCompleta.Recordset("dt_ult_compra") = vObjOracle!dt_ult_compra
44           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
45           frmGerar.DataCompleta.UpdateRecord
46           vObjOracle.MoveNext
47           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
48        Loop

50        frmGerar.lblMensagem.Caption = "ITEM_ESTOQUE - CONCLU�DO"
51        Gravar_Log
52        frmGerar.Refresh

TrataErro:
         
53        If Err.Number <> 0 Then
54            ERRO "ITEM_ESTOQUE", Err.Number, Err.Description
55            If Err.Number = 3022 Then
56                Resume Next
57            Else
58                MsgBox "Sub: ITEM_ESTOQUE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
59            End If
60        End If
          
End Sub

Public Sub ITEM_GLOBAL()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITEM_GLOBAL - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITEM_GLOBAL(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "ITEM_GLOBAL"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "ITEM_GLOBAL - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
23           frmGerar.DataCompleta.Recordset("sequencia_tab") = vObjOracle!sequencia_tab
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "ITEM_GLOBAL - CONCLU�DO"
29        frmGerar.Refresh
30        Gravar_Log

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "ITEM_GLOBAL", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: ITEM_GLOBAL" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub ITEM_PRECO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " ITEM_PRECO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "CD"
5         vBanco.Parameters.Add "CD", vCD_Novo, 1
          
6         vBanco_Prod.Parameters.Remove "vTipoBase"
7         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
8         vBanco_Prod.Parameters.Remove "PM_CODIGO"
9         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

10        Criar_Cursor vBanco_Prod
11        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
12        vBanco_Prod.ExecuteSQL vSql
13        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
14        vBanco.Parameters.Remove "TIPO_LOJA"
15        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1
16        Criar_Cursor vBanco
17        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_ITEM_PRECO(:TIPO_LOJA,:VCURSOR,:CD); END;"
18        vBanco.ExecuteSQL vSql
19        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
20        Set frmGerar.DataCompleta.Recordset = Nothing
21        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
22           frmGerar.DataCompleta.RecordSource = ""
23           Set frmGerar.DataCompleta.Recordset = Nothing
24        Loop
25        frmGerar.DataCompleta.RecordSource = "ITEM_PRECO"
26        Deletar_Registros_Existentes
27        frmGerar.DataCompleta.Refresh
          
28        frmGerar.PB1.Value = 0
29        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
30        frmGerar.lblMensagem.Caption = "ITEM_PRECO - Inserindo Registros"
31        frmGerar.Refresh
          
32        Do While Not vObjOracle.EOF
33           frmGerar.DataCompleta.Recordset.AddNew
34           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
35           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
36           frmGerar.DataCompleta.Recordset("preco_venda") = vObjOracle!preco_venda
37           frmGerar.DataCompleta.Recordset("preco_venda_ant") = vObjOracle!preco_venda_ant
38           frmGerar.DataCompleta.Recordset("preco_of") = vObjOracle!preco_of
39           frmGerar.DataCompleta.Recordset("preco_of_ant") = vObjOracle!preco_of_ant
40           frmGerar.DataCompleta.Recordset("preco_sp") = vObjOracle!preco_sp
41           frmGerar.DataCompleta.Recordset("preco_sp_ant") = vObjOracle!preco_sp_ant
42           frmGerar.DataCompleta.UpdateRecord
43           vObjOracle.MoveNext
44           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
45        Loop

47        frmGerar.lblMensagem.Caption = "ITEM_PRECO - CONCLU�DO"
48        Gravar_Log
49        frmGerar.Refresh

TrataErro:
         
50        If Err.Number <> 0 Then
51            ERRO "ITEM_PRECO", Err.Number, Err.Description
52            If Err.Number = 3022 Then
53                Resume Next
54            Else
55                MsgBox "Sub: ITEM_PRECO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
56            End If
57        End If
End Sub

Public Sub MONTADORA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " MONTADORA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_MONTADORA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "MONTADORA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "MONTADORA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_montadora") = vObjOracle!cod_montadora
23           frmGerar.DataCompleta.Recordset("desc_montadora") = vObjOracle!desc_montadora
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "MONTADORA - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "MONTADORA", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: MONTADORA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub NATUREZA_OPERACAO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " NATUREZA_OPERACAO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_NATUREZA_OPERACAO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "NATUREZA_OPERACAO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "NATUREZA_OPERACAO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_natureza") = vObjOracle!cod_natureza
23           frmGerar.DataCompleta.Recordset("desc_natureza") = vObjOracle!desc_natureza
24           frmGerar.DataCompleta.Recordset("desc_abreviada") = vObjOracle!desc_abreviada
25           frmGerar.DataCompleta.Recordset("cod_cfo_dest1") = vObjOracle!cod_cfo_dest1
26           frmGerar.DataCompleta.Recordset("cod_cfo_dest2") = vObjOracle!cod_cfo_dest2
27           frmGerar.DataCompleta.Recordset("cod_cfo_dest3") = vObjOracle!cod_cfo_dest3
28           frmGerar.DataCompleta.Recordset("cod_cfo_oper") = vObjOracle!cod_cfo_oper
29           frmGerar.DataCompleta.Recordset("cod_transac") = vObjOracle!cod_transac
30           frmGerar.DataCompleta.Recordset("cod_trib_icms1") = vObjOracle!cod_trib_icms1
31           frmGerar.DataCompleta.Recordset("cod_trib_icms2") = vObjOracle!cod_trib_icms2
32           frmGerar.DataCompleta.Recordset("cod_trib_icms3") = vObjOracle!cod_trib_icms3
33           frmGerar.DataCompleta.Recordset("cod_trib_icms4") = vObjOracle!cod_trib_icms4
34           frmGerar.DataCompleta.Recordset("cod_trib_icms5") = vObjOracle!cod_trib_icms5
35           frmGerar.DataCompleta.Recordset("cod_trib_icms6") = vObjOracle!cod_trib_icms6
36           frmGerar.DataCompleta.Recordset("cod_trib_ipi1") = vObjOracle!cod_trib_ipi1
37           frmGerar.DataCompleta.Recordset("cod_trib_ipi2") = vObjOracle!cod_trib_ipi2
38           frmGerar.DataCompleta.Recordset("cod_trib_ipi3") = vObjOracle!cod_trib_ipi3
39           frmGerar.DataCompleta.Recordset("cod_trib_ipi4") = vObjOracle!cod_trib_ipi4
40           frmGerar.DataCompleta.Recordset("cod_mensagem1") = vObjOracle!cod_mensagem1
41           frmGerar.DataCompleta.Recordset("cod_mensagem2") = vObjOracle!cod_mensagem2
42           frmGerar.DataCompleta.Recordset("fl_livro") = vObjOracle!fl_livro
43           frmGerar.DataCompleta.Recordset("fl_cuema") = vObjOracle!fl_cuema
44           frmGerar.DataCompleta.Recordset("fl_estoque") = vObjOracle!fl_estoque
45           frmGerar.DataCompleta.Recordset("fl_estorno") = vObjOracle!fl_estorno
46           frmGerar.DataCompleta.Recordset("fl_tributacao") = vObjOracle!fl_tributacao
47           frmGerar.DataCompleta.Recordset("fl_fornecedor") = vObjOracle!fl_fornecedor
48           frmGerar.DataCompleta.Recordset("fl_vlcontabil") = vObjOracle!fl_vlcontabil
49           frmGerar.DataCompleta.Recordset("fl_nfitem") = vObjOracle!fl_nfitem
50           frmGerar.DataCompleta.Recordset("fl_quantidade") = vObjOracle!fl_quantidade
51           frmGerar.DataCompleta.Recordset("fl_cod_merc") = vObjOracle!fl_cod_merc
52           frmGerar.DataCompleta.Recordset("fl_pcdesc") = vObjOracle!fl_pcdesc
53           frmGerar.DataCompleta.Recordset("fl_contas_pagar") = vObjOracle!fl_contas_pagar
54           frmGerar.DataCompleta.Recordset("fl_tipo_nota") = vObjOracle!fl_tipo_nota
55           frmGerar.DataCompleta.Recordset("fl_centro_custo") = vObjOracle!fl_centro_custo
56           frmGerar.DataCompleta.Recordset("fl_contabil") = vObjOracle!fl_contabil
57           frmGerar.DataCompleta.Recordset("fl_base_red_icms") = vObjOracle!fl_base_red_icms
58           frmGerar.DataCompleta.Recordset("fl_consistencia") = vObjOracle!fl_consistencia
59           frmGerar.DataCompleta.Recordset("fl_ipi_incid_icm") = vObjOracle!fl_ipi_incid_icm
60           frmGerar.DataCompleta.Recordset("fl_movimentacao") = vObjOracle!fl_movimentacao
61           frmGerar.DataCompleta.UpdateRecord
62           vObjOracle.MoveNext
63           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
64        Loop
65        frmGerar.lblMensagem.Caption = " NATUREZA_OPERACAO - CONCLU�DO"
66        Gravar_Log
67        frmGerar.Refresh

TrataErro:
         
68        If Err.Number <> 0 Then
69            ERRO "NATUREZA_OPERACAO", Err.Number, Err.Description
70            If Err.Number = 3022 Then
71                Resume Next
72            Else
73                MsgBox "Sub: NATUREZA_OPERACAO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
74            End If
75        End If
End Sub

Public Sub PLANO_PGTO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " PLANO_PGTO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_PLANO_PGTO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "PLANO_PGTO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "PLANO_PGTO - Inserindo Registros"
          
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_plano") = vObjOracle!cod_plano
23           frmGerar.DataCompleta.Recordset("desc_plano") = vObjOracle!desc_plano
24           frmGerar.DataCompleta.Recordset("prazo_medio") = vObjOracle!prazo_medio
25           frmGerar.DataCompleta.Recordset("pc_acres_fin_dpk") = vObjOracle!pc_acres_fin_dpk
26           frmGerar.DataCompleta.Recordset("pc_desc_fin_dpk") = vObjOracle!pc_desc_fin_dpk
27           frmGerar.DataCompleta.Recordset("fl_avista") = vObjOracle!fl_avista
28           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
29           frmGerar.DataCompleta.Recordset("pc_desc_fin_vdr") = vObjOracle!pc_desc_fin_vdr
30           frmGerar.DataCompleta.Recordset("pc_acres_fin_blau") = vObjOracle!pc_acres_fin_blau
31           frmGerar.DataCompleta.UpdateRecord
32           vObjOracle.MoveNext
33           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
34        Loop
35        frmGerar.lblMensagem.Caption = " PLANO_PGTO - CONCLU�DO"
36        Gravar_Log
37        frmGerar.Refresh

TrataErro:
         
38        If Err.Number <> 0 Then
39            ERRO "PLANO_PGTO", Err.Number, Err.Description
40            If Err.Number = 3022 Then
41                Resume Next
42            Else
43                MsgBox "Sub: PLANO_PGTO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
44            End If
45        End If
End Sub

Public Sub SUBGRUPO()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " SUBGRUPO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_SUBGRUPO(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "SUBGRUPO"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "SUBGRUPO - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
23           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
24           frmGerar.DataCompleta.Recordset("desc_subgrupo") = vObjOracle!desc_subgrupo
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = "SUBGRUPO - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "SUBGRUPO", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: SUBGRUPO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub SUBST_TRIBUTARIA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " SUBST_TRIBUTARIA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_SUBST_TRIBUTARIA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "SUBST_TRIBUTARIA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "SUBST_TRIBUTARIA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("class_fiscal") = vObjOracle!CLASS_FISCAL
23           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
24           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
25           frmGerar.DataCompleta.Recordset("cod_trib_revendedor") = vObjOracle!cod_trib_revendedor
26           frmGerar.DataCompleta.Recordset("cod_trib_inscrito") = vObjOracle!cod_trib_inscrito
27           frmGerar.DataCompleta.Recordset("cod_trib_isento") = vObjOracle!cod_trib_isento
28           frmGerar.DataCompleta.Recordset("fator_revendedor") = vObjOracle!fator_revendedor
29           frmGerar.DataCompleta.Recordset("fator_inscrito") = vObjOracle!fator_inscrito
30           frmGerar.DataCompleta.Recordset("fator_isento") = vObjOracle!fator_isento
31           frmGerar.DataCompleta.Recordset("fl_cred_suspenso") = vObjOracle!fl_cred_suspenso
32           frmGerar.DataCompleta.Recordset("pc_margem_lucro") = vObjOracle!PC_MARGEM_LUCRO
33           frmGerar.DataCompleta.UpdateRecord
34           vObjOracle.MoveNext
35           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
36        Loop
37        frmGerar.lblMensagem.Caption = "SUBST_TRIBUTARIA - CONCLU�DO"
38        Gravar_Log
39        frmGerar.Refresh

TrataErro:
         
40        If Err.Number <> 0 Then
41            ERRO "SUBST_TRIBUTARIA", Err.Number, Err.Description
42            If Err.Number = 3022 Then
43                Resume Next
44            Else
45                MsgBox "Sub: SUBST_TRIBUTARIA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
46            End If
47        End If
End Sub

Public Sub TAXA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " TAXA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "CD"
5         vBanco.Parameters.Add "CD", vCD_Novo, 1
          
6         vBanco_Prod.Parameters.Remove "vTipoBase"
7         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
8         vBanco_Prod.Parameters.Remove "PM_CODIGO"
9         vBanco_Prod.Parameters.Add "PM_CODIGO", frmGerar.txtCodPaacRepres, 1

10        Criar_Cursor vBanco_Prod
11        vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_TIPO_LOJA_FILIAL(:vTipoBase,:PM_CODIGO,:VCURSOR); END;"
12        vBanco_Prod.ExecuteSQL vSql
13        Set vObjOracle_prod = vBanco_Prod.Parameters("vCursor").Value
          
                  'RTI-143 - Obtendo todos os tipos de loja
          strTipoLoja = ""
          Do While Not vObjOracle_prod.EOF
            strTipoLoja = strTipoLoja & vObjOracle_prod!TIPO_LOJA & ","
            vObjOracle_prod.MoveNext
          Loop
          strTipoLoja = Left(strTipoLoja, Len(strTipoLoja) - 1)
          
14        vBanco.Parameters.Remove "TIPO_LOJA"
15        vBanco.Parameters.Add "TIPO_LOJA", strTipoLoja, 1

16        Criar_Cursor vBanco
17        vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TAXA(:TIPO_LOJA,:VCURSOR,:CD); END;"
18        vBanco.ExecuteSQL vSql
19        Set vObjOracle = vBanco.Parameters("vCursor").Value
           
20        Set frmGerar.DataCompleta.Recordset = Nothing
21        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
22           frmGerar.DataCompleta.RecordSource = ""
23           Set frmGerar.DataCompleta.Recordset = Nothing
24        Loop
25         frmGerar.DataCompleta.RecordSource = "TAXA"
26        Deletar_Registros_Existentes
27         frmGerar.DataCompleta.Refresh
           
28        frmGerar.PB1.Value = 0
29        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
30        frmGerar.lblMensagem.Caption = "TAXA - Inserindo Registros"
31        frmGerar.Refresh
          
32         Do While Not vObjOracle.EOF
33            frmGerar.DataCompleta.Recordset.AddNew
34            frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
35            frmGerar.DataCompleta.Recordset("valor_2") = vObjOracle!valor_2
36            frmGerar.DataCompleta.Recordset("valor_6") = vObjOracle!valor_6
37            frmGerar.DataCompleta.Recordset("valor_7") = vObjOracle!valor_7
38            frmGerar.DataCompleta.Recordset("data_1") = vObjOracle!data_1
39            frmGerar.DataCompleta.Recordset("data_2") = vObjOracle!data_2
40            frmGerar.DataCompleta.Recordset("data_3") = vObjOracle!data_3
41            frmGerar.DataCompleta.Recordset("data_4") = vObjOracle!data_4
42            frmGerar.DataCompleta.Recordset("data_5") = vObjOracle!data_5
43            frmGerar.DataCompleta.Recordset("valor_13") = vObjOracle!valor_13
44            frmGerar.DataCompleta.UpdateRecord
45            vObjOracle.MoveNext
46            frmGerar.PB1.Value = frmGerar.PB1.Value + 1
47         Loop

49         frmGerar.lblMensagem.Caption = "TAXA - CONCLU�DO"
50        Gravar_Log
51        frmGerar.Refresh

TrataErro:
         
52        If Err.Number <> 0 Then
53            ERRO "TAXA", Err.Number, Err.Description
54            If Err.Number = 3022 Then
55                Resume Next
56            Else
57                MsgBox "Sub: TAXA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
58            End If
59        End If
End Sub

Public Sub TABELA_DESCPER()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " TABELA_DESCPER - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TABELA_DESCPER(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "TABELA_DESCPER"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
          
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "TABELA_DESCPER - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("sequencia") = vObjOracle!sequencia
23           frmGerar.DataCompleta.Recordset("tp_tabela") = vObjOracle!tp_tabela
24           frmGerar.DataCompleta.Recordset("ocorr_preco") = vObjOracle!ocorr_preco
25           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
26           frmGerar.DataCompleta.Recordset("cod_fornecedor") = vObjOracle!COD_FORNECEDOR
27           frmGerar.DataCompleta.Recordset("cod_grupo") = vObjOracle!cod_grupo
28           frmGerar.DataCompleta.Recordset("cod_subgrupo") = vObjOracle!cod_subgrupo
29           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
30           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
31           frmGerar.DataCompleta.Recordset("pc_desc_periodo1") = vObjOracle!pc_desc_periodo1
32           frmGerar.DataCompleta.Recordset("pc_desc_periodo2") = vObjOracle!pc_desc_periodo2
33           frmGerar.DataCompleta.Recordset("pc_desc_periodo3") = vObjOracle!pc_desc_periodo3
34           frmGerar.DataCompleta.UpdateRecord
35           vObjOracle.MoveNext
36           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
37        Loop
38        frmGerar.lblMensagem.Caption = " TABELA_DESCPER - CONCLU�DO"
39        Gravar_Log
40        frmGerar.Refresh

TrataErro:
         
41        If Err.Number <> 0 Then
42            ERRO "TABELA_DESCPER", Err.Number, Err.Description
43            If Err.Number = 3022 Then
44                Resume Next
45            Else
46                MsgBox "Sub: TABELA_DESCPER" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
47            End If
48        End If
End Sub

Public Sub TABELA_VENDA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " TABELA_VENDA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TABELA_VENDA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         frmGerar.lblMensagem.Caption = "TABELA_VENDA"
          
9         Set frmGerar.DataCompleta.Recordset = Nothing
10        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
11           frmGerar.DataCompleta.RecordSource = ""
12           Set frmGerar.DataCompleta.Recordset = Nothing
13        Loop
14        frmGerar.DataCompleta.RecordSource = "TABELA_VENDA"
15        Deletar_Registros_Existentes
16        frmGerar.DataCompleta.Refresh

17        frmGerar.PB1.Value = 0
18        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
19        frmGerar.lblMensagem.Caption = "TABELA_VENDA - Inserindo Registros"
20        frmGerar.Refresh
          
21        Do While Not vObjOracle.EOF
22           frmGerar.DataCompleta.Recordset.AddNew
23           frmGerar.DataCompleta.Recordset("tabela_venda") = vObjOracle!TABELA_VENDA
24           frmGerar.DataCompleta.Recordset("tp_tabela") = vObjOracle!tp_tabela
25           frmGerar.DataCompleta.Recordset("ocorr_preco") = vObjOracle!ocorr_preco
26           frmGerar.DataCompleta.Recordset("dt_vigencia1") = vObjOracle!dt_vigencia1
27           frmGerar.DataCompleta.Recordset("dt_vigencia2") = vObjOracle!dt_vigencia2
28           frmGerar.DataCompleta.Recordset("dt_vigencia3") = vObjOracle!dt_vigencia3
29           frmGerar.DataCompleta.Recordset("dt_vigencia4") = vObjOracle!dt_vigencia4
30           frmGerar.DataCompleta.Recordset("dt_vigencia5") = vObjOracle!dt_vigencia5
31           frmGerar.DataCompleta.UpdateRecord
32           vObjOracle.MoveNext
33           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
34        Loop
35        frmGerar.lblMensagem.Caption = "TABELA_VENDA - CONCLU�DO"
36        Gravar_Log
37        frmGerar.Refresh

TrataErro:
         
38        If Err.Number <> 0 Then
39            ERRO "TABELA_VENDA", Err.Number, Err.Description
40            If Err.Number = 3022 Then
41                Resume Next
42            Else
43                MsgBox "Sub: TABELA_VENDA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
44            End If
45        End If
End Sub

Public Sub CANCEL_PEDNOTA()
          
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CANCEL_PEDNOTA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CANCEL_PEDNOTA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CANCEL_PEDNOTA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CANCEL_PEDNOTA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_cancel") = vObjOracle!cod_cancel
23           frmGerar.DataCompleta.Recordset("desc_cancel") = vObjOracle!desc_cancel
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = "CANCEL_PEDNOTA - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "CANCEL_PEDNOTA", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: CANCEL_PEDNOTA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub TIPO_CLIENTE_BLAU()
          
1     On Error GoTo TrataErro

2         frmGerar.lblMensagem.Caption = " TIPO_CLIENTE_BLAU - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TIPO_CLIENTE_BLAU(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "TIPO_CLIENTE_BLAU"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "TIPO_CLIENTE_BLAU - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_tipo_cli") = vObjOracle!cod_tipo_cli
23           frmGerar.DataCompleta.Recordset("desc_tipo_cli") = vObjOracle!desc_tipo_cli
24           frmGerar.DataCompleta.UpdateRecord
25           vObjOracle.MoveNext
26           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
27        Loop
28        frmGerar.lblMensagem.Caption = " TIPO_CLIENTE_BLAU - CONCLU�DO"
29        Gravar_Log
30        frmGerar.Refresh

TrataErro:
         
31        If Err.Number <> 0 Then
32            ERRO "TIPO_CLIENTE_BLAU", Err.Number, Err.Description
33            If Err.Number = 3022 Then
34                Resume Next
35            Else
36                MsgBox "Sub: TIPO_CLIENTE_BLAU" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
37            End If
38        End If
End Sub

Public Sub TIPO_CLIENTE()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " TIPO_CLIENTE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TIPO_CLIENTE(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "TIPO_CLIENTE"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "TIPO_CLIENTE - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_tipo_cli") = vObjOracle!cod_tipo_cli
23           frmGerar.DataCompleta.Recordset("desc_tipo_cli") = vObjOracle!desc_tipo_cli
24           frmGerar.DataCompleta.Recordset("cod_segmento") = vObjOracle!cod_segmento
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = "TIPO_CLIENTE - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "TIPO_CLIENTE", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: TIPO_CLIENTE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub TRANSPORTADORA()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " TRANSPORTADORA - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_TRANSPORTADORA(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "TRANSPORTADORA"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "TRANSPORTADORA - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
23           frmGerar.DataCompleta.Recordset("nome_transp") = vObjOracle!nome_transp
24           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
25           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
30        frmGerar.lblMensagem.Caption = "TRANSPORTADORA - CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh

TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "TRANSPORTADORA", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: TRANSPORTADORA" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If
End Sub

Public Sub UF()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "UF"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "UF - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
23           frmGerar.DataCompleta.Recordset("desc_uf") = vObjOracle!desc_uf
24           frmGerar.DataCompleta.Recordset("dt_aliq_interna") = vObjOracle!dt_aliq_interna
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = "UF - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "UF", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: UF" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub UF_DPK()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF_DPK - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
         
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF_DPK(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "UF_DPK"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "UF_DPK - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf_origem") = vObjOracle!COD_UF_ORIGEM
23           frmGerar.DataCompleta.Recordset("cod_uf_destino") = vObjOracle!COD_UF_DESTINO
24           frmGerar.DataCompleta.Recordset("cod_dpk") = vObjOracle!cod_dpk
25           frmGerar.DataCompleta.Recordset("pc_desc") = vObjOracle!pc_desc
26           frmGerar.DataCompleta.UpdateRecord
27           vObjOracle.MoveNext
28           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
29        Loop
30        frmGerar.lblMensagem.Caption = "UF_DPK - CONCLU�DO"
31        Gravar_Log
32        frmGerar.Refresh

TrataErro:
         
33        If Err.Number <> 0 Then
34            ERRO "UF_DPK", Err.Number, Err.Description
35            If Err.Number = 3022 Then
36                Resume Next
37            Else
38                MsgBox "Sub: UF_DPK" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
39            End If
40        End If
End Sub

Public Sub UF_CATEG()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " UF_CATEG - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_UF_CATEG(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "UF_CATEG"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "UF_CATEG - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_uf") = vObjOracle!cod_uf
23           frmGerar.DataCompleta.Recordset("categoria") = vObjOracle!categoria
24           frmGerar.DataCompleta.Recordset("pc_desc_adic") = vObjOracle!pc_desc_adic
25           frmGerar.DataCompleta.Recordset("pc_adic_maximo") = vObjOracle!pc_adic_maximo
26           frmGerar.DataCompleta.Recordset("pc_desc_comis") = vObjOracle!pc_desc_comis
27           frmGerar.DataCompleta.Recordset("desc_comis_maximo") = vObjOracle!desc_comis_maximo
28           frmGerar.DataCompleta.Recordset("caracteristica") = vObjOracle!caracteristica
29           frmGerar.DataCompleta.UpdateRecord
30           vObjOracle.MoveNext
31           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
32        Loop
33        frmGerar.lblMensagem.Caption = "UF_CATEG - CONCLU�DO"
34        Gravar_Log
35        frmGerar.Refresh

TrataErro:
         
36        If Err.Number <> 0 Then
37            ERRO "UF_CATEG", Err.Number, Err.Description
38            If Err.Number = 3022 Then
39                Resume Next
40            Else
41                MsgBox "Sub: UF_CATEG" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
42            End If
43        End If
End Sub


Public Sub CLIE_CREDITO()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIE_CREDITO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIE_CREDITO(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        frmGerar.DataCompleta.RecordSource = ""
13        Set frmGerar.DataCompleta.Recordset = Nothing
14        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
15           frmGerar.DataCompleta.RecordSource = ""
16           Set frmGerar.DataCompleta.Recordset = Nothing
17        Loop
18        frmGerar.DataCompleta.RecordSource = "CLIE_CREDITO"
19        Deletar_Registros_Existentes
20        frmGerar.DataCompleta.Refresh

21        frmGerar.PB1.Value = 0
22        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
23        frmGerar.lblMensagem.Caption = "CLIE_CREDITO - Inserindo Registros"
24        frmGerar.Refresh
          
25        Do While Not vObjOracle.EOF
26           frmGerar.DataCompleta.Recordset.AddNew
27           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
28           frmGerar.DataCompleta.Recordset("dt_ult_compra") = vObjOracle!dt_ult_compra
29           frmGerar.DataCompleta.Recordset("nota_credito") = vObjOracle!nota_credito
30           frmGerar.DataCompleta.Recordset("limite_credito") = vObjOracle!limite_credito
31           frmGerar.DataCompleta.UpdateRecord
32           vObjOracle.MoveNext
33           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
34        Loop
35        frmGerar.lblMensagem.Caption = " CLIE_CREDITO - CONCLU�DO"
36        Gravar_Log
37        frmGerar.Refresh
          
TrataErro:
38        If Err.Number <> 0 Then
39            ERRO "CLIE_CREDITO", Err.Number, Err.Description
40            If Err.Number = 3022 Then
41                Resume Next
42            Else
43                MsgBox "Sub: CLIE_CREDITO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
44            End If
45        End If
End Sub

Public Sub CLIE_ENDERECO()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIE_ENDERECO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIE_ENDERECO(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "CLIE_ENDERECO"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh

20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "CLIE_ENDERECO - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
27           frmGerar.DataCompleta.Recordset("tp_endereco") = vObjOracle!tp_endereco
28           frmGerar.DataCompleta.Recordset("sequencia") = vObjOracle!sequencia
29           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
30           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
31           frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
32           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
33           frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
34           frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
35           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
36           frmGerar.DataCompleta.Recordset("cxpostal") = vObjOracle!cxpostal
37           frmGerar.DataCompleta.Recordset("inscr_estadual") = vObjOracle!inscr_estadual
38           frmGerar.DataCompleta.Recordset("nome_cliente") = vObjOracle!nome_cliente
39           frmGerar.DataCompleta.UpdateRecord
40           vObjOracle.MoveNext
41           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
42        Loop
43        frmGerar.lblMensagem.Caption = " CLIE_ENDERECO - CONCLU�DO"
44        Gravar_Log
45        frmGerar.Refresh

TrataErro:
46        If Err.Number <> 0 Then
47            ERRO "CLIE_ENDERECO", Err.Number, Err.Description
48            If Err.Number = 3022 Then
49                Resume Next
50            Else
51                MsgBox "Sub: CLIE_ENDERECO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
52            End If
53        End If
End Sub


Public Sub CLIE_MENSAGEM()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIE_MENSAGEM - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIE_MENSAGEM(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CLIE_MENSAGEM"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh
16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CLIE_MENSAGEM - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("cod_mensagem") = vObjOracle!cod_mensagem
23           frmGerar.DataCompleta.Recordset("desc_mens") = vObjOracle!desc_mens
24           frmGerar.DataCompleta.Recordset("fl_bloqueio") = vObjOracle!fl_bloqueio
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = " CLIE_MENSAGEM - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
32        If Err.Number <> 0 Then
33            ERRO "CLIE_MENSAGEM", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: CLIE_MENSAGEM" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If
End Sub

Public Sub CLIENTE()
1         On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " CLIENTE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1
          
8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLIENTE(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "CLIENTE"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "CLIENTE - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
27           frmGerar.DataCompleta.Recordset("cgc") = vObjOracle!cgc
28           frmGerar.DataCompleta.Recordset("nome_cliente") = vObjOracle!nome_cliente
29           frmGerar.DataCompleta.Recordset("nome_contato") = vObjOracle!nome_contato
30           frmGerar.DataCompleta.Recordset("classificacao") = vObjOracle!classificacao
31           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
32           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
33           frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
34           frmGerar.DataCompleta.Recordset("ddd1") = vObjOracle!ddd1
35           frmGerar.DataCompleta.Recordset("fone1") = vObjOracle!fone1
36           frmGerar.DataCompleta.Recordset("ddd2") = vObjOracle!ddd2
37           frmGerar.DataCompleta.Recordset("fone2") = vObjOracle!fone2
38           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
39           frmGerar.DataCompleta.Recordset("cxpostal") = vObjOracle!cxpostal
40           frmGerar.DataCompleta.Recordset("telex") = vObjOracle!telex
41           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
42           frmGerar.DataCompleta.Recordset("inscr_estadual") = vObjOracle!inscr_estadual
43           frmGerar.DataCompleta.Recordset("inscr_suframa") = vObjOracle!inscr_suframa
44           frmGerar.DataCompleta.Recordset("cod_tipo_cliente") = vObjOracle!cod_tipo_cliente
45           frmGerar.DataCompleta.Recordset("dt_cadastr") = vObjOracle!dt_cadastr
46           frmGerar.DataCompleta.Recordset("cod_transp") = vObjOracle!cod_transp
47           frmGerar.DataCompleta.Recordset("cod_repr_vend") = vObjOracle!cod_repr_vend
48           frmGerar.DataCompleta.Recordset("cod_banco") = vObjOracle!cod_banco
49           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
50           frmGerar.DataCompleta.Recordset("cod_mensagem") = vObjOracle!cod_mensagem
51           frmGerar.DataCompleta.Recordset("fl_cons_final") = vObjOracle!fl_cons_final
52           frmGerar.DataCompleta.Recordset("caracteristica") = vObjOracle!caracteristica
53           frmGerar.DataCompleta.Recordset("cod_mensagem_fiscal") = vObjOracle!cod_mensagem_fiscal
      'Alteracao Eduardo - 25/07/05
      'Liberar quando dar carga
54           frmGerar.DataCompleta.Recordset("TP_EMPRESA") = vObjOracle!tp_empresa
55           frmGerar.DataCompleta.UpdateRecord
56           vObjOracle.MoveNext
57           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
58        Loop
59        frmGerar.lblMensagem.Caption = " CLIENTE - CONCLU�DO"
60        Gravar_Log
61        frmGerar.Refresh

TrataErro:
62        If Err.Number <> 0 Then
63            ERRO "CLIENTE", Err.Number, Err.Description
64            If Err.Number = 3022 Then
65                Resume Next
66            Else
67                MsgBox "Sub: CLIENTE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
68            End If
69        End If
End Sub

Public Sub DUPLICATAS()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " DUPLICATAS - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_DUPLICATAS(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "DUPLICATAS"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh
          
20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "DUPLICATAS - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cod_loja") = vObjOracle!cod_Loja
27           frmGerar.DataCompleta.Recordset("num_fatura") = vObjOracle!num_fatura
28           frmGerar.DataCompleta.Recordset("num_ordem") = vObjOracle!num_ordem
29           frmGerar.DataCompleta.Recordset("situacao_pagto") = vObjOracle!situacao_pagto
30           frmGerar.DataCompleta.Recordset("tp_duplicata") = vObjOracle!tp_duplicata
31           frmGerar.DataCompleta.Recordset("cod_cliente") = vObjOracle!COD_CLIENTE
32           frmGerar.DataCompleta.Recordset("cod_banco") = vObjOracle!cod_banco
33           frmGerar.DataCompleta.Recordset("tp_pagamento") = vObjOracle!tp_pagamento
34           frmGerar.DataCompleta.Recordset("dt_vencimento") = vObjOracle!dt_vencimento
35           frmGerar.DataCompleta.UpdateRecord
36           vObjOracle.MoveNext
37           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
38        Loop
39        frmGerar.lblMensagem.Caption = "DUPLICATAS - CONCLU�DO"
40        Gravar_Log
41        frmGerar.Refresh

TrataErro:
42        If Err.Number <> 0 Then
43            ERRO "DUPLICATAS", Err.Number, Err.Description
44            If Err.Number = 3022 Then
45                Resume Next
46            Else
47                MsgBox "Sub: DUPLICATAS" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
48            End If
49        End If
End Sub

Public Sub REPRESENTACAO()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " REPRESENTACAO - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
       
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_REPRESENTACAO(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value

12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "REPRESENTACAO"
18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh

20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "REPRESENTACAO - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cgc") = vObjOracle!cgc
27           frmGerar.DataCompleta.Recordset("razao_social") = vObjOracle!razao_social
28           frmGerar.DataCompleta.Recordset("num_cori") = vObjOracle!num_cori
29           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
30           frmGerar.DataCompleta.Recordset("cod_Cidade") = vObjOracle!cod_cidade
31           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
32           frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
33           frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
34           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
35           frmGerar.DataCompleta.Recordset("celular") = vObjOracle!celular
36           frmGerar.DataCompleta.Recordset("cxpostal") = vObjOracle!cxpostal
37           frmGerar.DataCompleta.UpdateRecord
38           vObjOracle.MoveNext
39           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
40        Loop
41        frmGerar.lblMensagem.Caption = "REPRESENTACAO - CONCLU�DO"
42        Gravar_Log
43        frmGerar.Refresh

TrataErro:
44        If Err.Number <> 0 Then
45            ERRO "REPRESENTACAO", Err.Number, Err.Description
46            If Err.Number = 3022 Then
47                Resume Next
48            Else
49                MsgBox "Sub: REPRESENTACAO" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
50            End If
51        End If
End Sub

Public Sub REPRESENTANTE()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " REPRESENTANTE - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1

8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_REPRESENTANTE(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "REPRESENTANTE"
'18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh

20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "REPRESENTANTE - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cic") = vObjOracle!cic
27           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
28           frmGerar.DataCompleta.Recordset("nome_repres") = vObjOracle!nome_repres
29           frmGerar.DataCompleta.Recordset("pseudonimo") = vObjOracle!pseudonimo
30           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
31           frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
32           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
33           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
34           frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
35           frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
36           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
37           frmGerar.DataCompleta.Recordset("ramal") = vObjOracle!ramal
38           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
39           frmGerar.DataCompleta.Recordset("cic_gerente") = vObjOracle!Cic_Gerente
40           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
41           frmGerar.DataCompleta.Recordset("tipo") = vObjOracle!TIPO
42           frmGerar.DataCompleta.Recordset("seq_franquia") = vObjOracle!seq_franquia
43           frmGerar.DataCompleta.Recordset("dt_cadastr") = vObjOracle!dt_cadastr
44           frmGerar.DataCompleta.Recordset("dt_desligamento") = vObjOracle!Dt_Desligamento
45           frmGerar.DataCompleta.UpdateRecord
46           vObjOracle.MoveNext
47           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
48        Loop

49        frmGerar.lblMensagem.Caption = "REPRESENTANTE - CONCLU�DO"
50        Gravar_Log
51        frmGerar.Refresh
          
TrataErro:
         
52        If Err.Number <> 0 Then
53            ERRO "REPRESENTANTE", Err.Number, Err.Description
54            If Err.Number = 3022 Then
55                Resume Next
56            Else
57                MsgBox "Sub: REPRESENTANTE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
58            End If
59        End If

End Sub

Public Sub FRANQUEADOR()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = " FRANQUEADOR - Consultando"
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh
          
4         vBanco.Parameters.Remove "vTipoBase"
5         vBanco.Parameters.Add "vTipoBase", vTipoBase, 1
6         vBanco.Parameters.Remove "COD_FILIAL"
7         vBanco.Parameters.Add "COD_FILIAL", vCodFilial, 1
          
8         Criar_Cursor vBanco
9         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_FRANQUEADOR(:vTipoBase,:COD_FILIAL,:VCURSOR); END;"
10        vBanco.ExecuteSQL vSql
          
11        Set vObjOracle = vBanco.Parameters("vCursor").Value
          
12        Set frmGerar.DataCompleta.Recordset = Nothing
13        Do While Not frmGerar.DataCompleta.Recordset Is Nothing
14           frmGerar.DataCompleta.RecordSource = ""
15           Set frmGerar.DataCompleta.Recordset = Nothing
16        Loop
17        frmGerar.DataCompleta.RecordSource = "REPRESENTANTE"
'18        Deletar_Registros_Existentes
19        frmGerar.DataCompleta.Refresh

20        frmGerar.PB1.Value = 0
21        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
22        frmGerar.lblMensagem.Caption = "FRANQUEADOR - Inserindo Registros"
23        frmGerar.Refresh
          
24        Do While Not vObjOracle.EOF
25           frmGerar.DataCompleta.Recordset.AddNew
26           frmGerar.DataCompleta.Recordset("cic") = vObjOracle!cic
27           frmGerar.DataCompleta.Recordset("cod_repres") = vObjOracle!cod_repres
28           frmGerar.DataCompleta.Recordset("nome_repres") = vObjOracle!nome_repres
29           frmGerar.DataCompleta.Recordset("pseudonimo") = vObjOracle!pseudonimo
30           frmGerar.DataCompleta.Recordset("endereco") = vObjOracle!endereco
31           frmGerar.DataCompleta.Recordset("bairro") = vObjOracle!bairro
32           frmGerar.DataCompleta.Recordset("cod_cidade") = vObjOracle!cod_cidade
33           frmGerar.DataCompleta.Recordset("cep") = vObjOracle!cep
34           frmGerar.DataCompleta.Recordset("ddd") = vObjOracle!ddd
35           frmGerar.DataCompleta.Recordset("fone") = vObjOracle!fone
36           frmGerar.DataCompleta.Recordset("fax") = vObjOracle!fax
37           frmGerar.DataCompleta.Recordset("ramal") = vObjOracle!ramal
38           frmGerar.DataCompleta.Recordset("cod_filial") = vObjOracle!cod_filial
39           frmGerar.DataCompleta.Recordset("cic_gerente") = vObjOracle!Cic_Gerente
40           frmGerar.DataCompleta.Recordset("situacao") = vObjOracle!situacao
41           frmGerar.DataCompleta.Recordset("tipo") = vObjOracle!TIPO
42           frmGerar.DataCompleta.Recordset("seq_franquia") = vObjOracle!seq_franquia
43           frmGerar.DataCompleta.Recordset("dt_cadastr") = vObjOracle!dt_cadastr
44           frmGerar.DataCompleta.Recordset("dt_desligamento") = vObjOracle!Dt_Desligamento
45           frmGerar.DataCompleta.UpdateRecord
46           vObjOracle.MoveNext
47           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
48        Loop
49        frmGerar.lblMensagem.Caption = "FRANQUEADOR - CONCLU�DO"
50        Gravar_Log
51        frmGerar.Refresh

TrataErro:
         
52        If Err.Number <> 0 Then
53            ERRO "FRANQUEADOR", Err.Number, Err.Description
54            If Err.Number = 3022 Then
55                Resume Next
56            Else
57                MsgBox "Sub: FRANQUEADOR" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
58            End If
59        End If

End Sub

'--------------
'Eduardo Relvas
'19/01/2005
'--------------
Public Sub R_FABRICA_DPK()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "R_FABRICA_DPK..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_R_FABRICA_DPK(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "R_FABRICA_DPK"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "R_FABRICA_DPK - Inserindo Registros"
19        frmGerar.Refresh
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("COD_FABRICA") = vObjOracle!COD_FABRICA
23           frmGerar.DataCompleta.Recordset("COD_DPK") = vObjOracle!cod_dpk
24           frmGerar.DataCompleta.Recordset("FL_TIPO") = vObjOracle!FL_TIPO
25           frmGerar.DataCompleta.Recordset("DT_CADASTRO") = vObjOracle!DT_CADASTRO
26           frmGerar.DataCompleta.Recordset("SIGLA") = vObjOracle!Sigla
261           'frmGerar.DataCompleta.Recordset("COD_FORNEC") = vObjOracle!COD_FORNECEDOR
27           frmGerar.DataCompleta.UpdateRecord
28           vObjOracle.MoveNext
29           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
30        Loop
31        frmGerar.lblMensagem.Caption = "R_FABRICA_DPK - CONCLU�DO"
32        Gravar_Log
33        frmGerar.Refresh

TrataErro:
         
34        If Err.Number <> 0 Then
35            ERRO "R_FABRICA_DPK", Err.Number, Err.Description
36            If Err.Number = 3022 Then
37                Resume Next
38            Else
39                MsgBox "Sub: R_FABRICA_DPK" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
40            End If
41        End If

End Sub

Public Sub CLASS_FISCAL_RED_BASE()
          
1     On Error GoTo TrataErro
          
2         frmGerar.lblMensagem.Caption = "CLASS_FISCAL_RED_BASE..."
          frmGerar.lblMensagem.Refresh
3         frmGerar.Refresh

4         Criar_Cursor vBanco
5         vSql = "BEGIN PRODSIC.PCK_FIL400_PROMOTOR.PR_CLASS_FISCAL_RED_BASE(:VCURSOR); END;"
6         vBanco.ExecuteSQL vSql
          
7         Set vObjOracle = vBanco.Parameters("vCursor").Value
          
8         Set frmGerar.DataCompleta.Recordset = Nothing
9         Do While Not frmGerar.DataCompleta.Recordset Is Nothing
10           frmGerar.DataCompleta.RecordSource = ""
11           Set frmGerar.DataCompleta.Recordset = Nothing
12        Loop
13        frmGerar.DataCompleta.RecordSource = "CLASS_FISCAL_RED_BASE"
14        Deletar_Registros_Existentes
15        frmGerar.DataCompleta.Refresh

16        frmGerar.PB1.Value = 0
17        frmGerar.PB1.Max = IIf(vObjOracle.RecordCount = 0, 1, vObjOracle.RecordCount)
18        frmGerar.lblMensagem.Caption = "CLASS_FISCAL_RED_BASE - Inserindo Registros"
19        frmGerar.Refresh
          
20        Do While Not vObjOracle.EOF
21           frmGerar.DataCompleta.Recordset.AddNew
22           frmGerar.DataCompleta.Recordset("COD_UF_ORIGEM") = vObjOracle!COD_UF_ORIGEM
23           frmGerar.DataCompleta.Recordset("COD_UF_DESTINO") = vObjOracle!COD_UF_DESTINO
24           frmGerar.DataCompleta.Recordset("CLASS_FISCAL") = vObjOracle!CLASS_FISCAL
25           frmGerar.DataCompleta.UpdateRecord
26           vObjOracle.MoveNext
27           frmGerar.PB1.Value = frmGerar.PB1.Value + 1
28        Loop
29        frmGerar.lblMensagem.Caption = "CLASS_FISCAL_RED_BASE - CONCLU�DO"
30        Gravar_Log
31        frmGerar.Refresh

TrataErro:
         
32        If Err.Number <> 0 Then
33            ERRO "CLASS_FISCAL_RED_BASE", Err.Number, Err.Description
34            If Err.Number = 3022 Then
35                Resume Next
36            Else
37                MsgBox "Sub: CLASS_FISCAL_RED_BASE" & vbCrLf & "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
38            End If
39        End If

End Sub


Public Sub ERRO(pSub As String, pCodErro As String, pDescErro As String)

    frmGerar.lstErros.AddItem pSub & " - " & pCodErro & " - " & pDescErro

End Sub

Public Sub Deletar_Registros_Existentes()
          
1         On Error GoTo Trata_Erro
           
2         frmGerar.DataCompleta.Refresh

3         For i = 1 To frmGerar.DataCompleta.Recordset.RecordCount
4             frmGerar.DataCompleta.Recordset.Delete
5             frmGerar.DataCompleta.Recordset.Update
6             frmGerar.DataCompleta.Recordset.MoveNext
7             If frmGerar.DataCompleta.Recordset.EOF = True Then Exit Sub
8         Next

Trata_Erro:
          If Err.Number = 3020 Then
             Resume Next
9         ElseIf Err.Number <> 0 Then
10            MsgBox "C�digo:" & Err.Number & vbCrLf & "Descricao: " & Err.Description
11        End If

End Sub

Public Sub Deletar_Tabelas()
1     On Error GoTo TrataErro
          
          Dim conn As ADODB.Connection
2         Set conn = New ADODB.Connection
          
3         conn.Open "Provider=Microsoft.Jet.OLEDB.4.0;" & _
                    "Data Source=" & vGerarBaseEm & "0000" & frmGerar.txtCodPaacRepres & ".MDB" & ";" & _
                    "User Id=admin;" & _
                    "Password=;"
          
4         For i = 0 To frmGerar.lstEspecificas.ListCount - 1
5             If frmGerar.lstEspecificas.Selected(i) = False Then
6                  conn.BeginTrans
                      conn.Execute "Drop Table " & frmGerar.lstEspecificas.List(i)
12                 conn.CommitTrans
13            End If
14        Next
15        For i = 0 To frmGerar.lstEspecificasPorLojaRepres.ListCount - 1
16            If frmGerar.lstEspecificasPorLojaRepres.Selected(i) = False Then
17                conn.Execute "Drop Table " & frmGerar.lstEspecificasPorLojaRepres.List(i)
18            End If
19        Next
20        For i = 0 To frmGerar.lstGenericas.ListCount - 1
21            If frmGerar.lstGenericas.Selected(i) = False Then
22                conn.Execute "Drop Table " & frmGerar.lstGenericas.List(i)
23            End If
24        Next
25        For i = 0 To frmGerar.lstVDR.ListCount - 1
26            If frmGerar.lstVDR.Selected(i) = False Then
27                conn.Execute "Drop Table " & frmGerar.lstVDR.List(i)
28            End If
29        Next
          
30        If frmGerar.lstEspecificas.SelCount = frmGerar.lstEspecificas.ListCount And _
             frmGerar.lstEspecificasPorLojaRepres.SelCount = frmGerar.lstEspecificasPorLojaRepres.ListCount And _
             frmGerar.lstGenericas.SelCount = frmGerar.lstGenericas.ListCount And _
             frmGerar.lstVDR.SelCount = frmGerar.lstVDR.ListCount Then
             'N�o faz nada
31        Else
32            For i = 0 To frmGerar.lstExtras.ListCount - 1
33                conn.Execute "Drop Table " & frmGerar.lstExtras.List(i)
34            Next
35        End If
          
36        conn.Close
37        Set conn = Nothing

TrataErro:
38        If Err.Number <> 0 Then
39            If Err.Number = -2147217865 Then
40                Resume Next
41            Else
42                MsgBox "C�digo:" & Err.Number & vbCrLf & "Descri��o:" & Err.Description & vbCrLf & "Linha: " & Erl & Err.Description & vbCrLf & "Linha: " & Erl
43            End If
44        End If
End Sub

Sub Gravar_Log()
    
    If Dir(vGerarLog & "Log_Qtdes.txt") <> "" Then
        Open vGerarLog & "Log_Qtdes.txt" For Append As #9
    Else
        Open vGerarLog & "Log_Qtdes.txt" For Output As #9
    End If
    
    Print #9, frmGerar.txtCodPaacRepres & " - " & Now & " - " & frmGerar.lblMensagem.Caption & " - " & frmGerar.DataCompleta.Recordset.RecordCount
    
    Close #9
    
End Sub

'RTI-143 - Fun��o respons�vel por verificar se o usu�rio informado � do tipo gerente.
Public Function VerificaGerente(ByVal intCodRepres As Integer) As Boolean

        Dim booRetorno As Boolean
        Dim strTipoUsuario As String
        
1       On Error GoTo TrataErro
        
            booRetorno = False
            
2         vBanco_Prod.Parameters.Remove "vTipoBase"
3         vBanco_Prod.Parameters.Add "vTipoBase", vTipoBase, 1
4         vBanco_Prod.Parameters.Remove "PM_CODIGO"
5         vBanco_Prod.Parameters.Add "PM_CODIGO", intCodRepres, 1
          
6         Criar_Cursor vBanco_Prod
7         vSql = "BEGIN PRODUCAO.PCK_FIL400CAD_PROMOTOR.PR_VERIFICA_CADASTRO(:PM_CODIGO,:vTipoBase,:VCURSOR); END;"
8         vBanco_Prod.ExecuteSQL vSql
          
9         Set vObjOracle = vBanco_Prod.Parameters("vCursor").Value
          
10        If Not vObjOracle.EOF Then

17          strTipoUsuario = IIf(IsNull(vObjOracle!E_mail), "", Left(vObjOracle!E_mail, 1))
18          If (strTipoUsuario = "G") Then
                booRetorno = True
            End If
            
30        End If
          
          
TrataErro:
         
31        If Err.Number <> 0 Then
32            booRetorno = False
38        End If

VerificaGerente = booRetorno
          
End Function

