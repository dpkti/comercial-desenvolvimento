VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{E57FB72C-1106-44AF-9706-0BA099A707C7}#4.2#0"; "XPFRAME.OCX"
Begin VB.Form frmRotinaMensal 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   ".:  ROTINA MENSAL  :."
   ClientHeight    =   5145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10095
   ControlBox      =   0   'False
   Icon            =   "frmRotinaMensal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5145
   ScaleWidth      =   10095
   Begin Bot�o.cmd cmdSair 
      Height          =   960
      Left            =   9405
      TabIndex        =   1
      TabStop         =   0   'False
      ToolTipText     =   "Sair do sistema"
      Top             =   45
      Width           =   645
      _ExtentX        =   1138
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Sair"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRotinaMensal.frx":0CCA
      PICN            =   "frmRotinaMensal.frx":0CE6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin Bot�o.cmd cmdExecutar 
      Height          =   960
      Left            =   45
      TabIndex        =   0
      TabStop         =   0   'False
      ToolTipText     =   "Executar"
      Top             =   45
      Width           =   825
      _ExtentX        =   1455
      _ExtentY        =   1693
      BTYPE           =   3
      TX              =   "Executar"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   2
      FOCUSR          =   0   'False
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   0
      MCOL            =   12632256
      MPTR            =   1
      MICON           =   "frmRotinaMensal.frx":19C0
      PICN            =   "frmRotinaMensal.frx":19DC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   2
      NGREY           =   0   'False
      FX              =   3
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin CoolXPFrame.xpFrame fraLog 
      Height          =   4110
      Left            =   45
      Top             =   990
      Width           =   10005
      _ExtentX        =   17648
      _ExtentY        =   7250
      Caption         =   "Log da gera��o dos roteiros"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   13977088
      BackColor       =   14737632
      BorderColor     =   0
      ColorStyle      =   99
      Begin VB.ListBox lstLog 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3735
         Left            =   90
         TabIndex        =   2
         Top             =   270
         Width           =   9825
      End
   End
End
Attribute VB_Name = "frmRotinaMensal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdExecutar_Click()

    Dim vCodLoja As Integer
    
    If vClsDPK001.Perguntar("Confirma gera��o do roteiro de liga��o?") = vbYes Then
    
        Screen.MousePointer = vbHourglass
        
        lstLog.Clear
        
        Open "C:\RLG001\RLG001_LOG_" & Format(Now(), "ddMMyyyy_hhmmss") & ".TXT" For Output As #1
        
        Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> In�cio do processo de gera��o do roteiro de liga��o"
        lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> In�cio do processo de gera��o do roteiro de liga��o"
        lstLog.Refresh
        lstLog.Selected(lstLog.ListCount - 1) = True
        
        Call RetornaDepositos
        
        While Not vObjOracle(0).EOF
        
            vCodLoja = vObjOracle(0).Fields(0)
            
            If vCodLoja <> 1 Then
            
                Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Testando link com o dep�sito " & vObjOracle(0).Fields(1)
                lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Testando link com o dep�sito " & vObjOracle(0).Fields(1)
                lstLog.Refresh
                lstLog.Selected(lstLog.ListCount - 1) = True
                
                If TestarLink(vCodLoja) = False Then
                
                    Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Problemas no link com o dep�sito " & vObjOracle(0).Fields(1) & ". Este dep�sito n�o far� parte deste processo de gera��o."
                    lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Problemas no link com o dep�sito " & vObjOracle(0).Fields(1) & ". Este dep�sito n�o far� parte deste processo de gera��o."
                    lstLog.Refresh
                    lstLog.Selected(lstLog.ListCount - 1) = True
                    
                    GoTo Proximo_Deposito
                    
                End If
                
            End If
            
            Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Gerando m�dia de contato dos clientes do dep�sito " & vObjOracle(0).Fields(1)
            lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Gerando m�dia de contato dos clientes do dep�sito " & vObjOracle(0).Fields(1)
            lstLog.Refresh
            lstLog.Selected(lstLog.ListCount - 1) = True
        
            Call GerarMediaClientes(vCodLoja)
            
            Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Gerando roteiro para as filiais do dep�sito " & vObjOracle(0).Fields(1)
            lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Gerando roteiro para as filiais do dep�sito " & vObjOracle(0).Fields(1)
            lstLog.Refresh
            lstLog.Selected(lstLog.ListCount - 1) = True
            
            Open "C:\RLG001\RLG001.TXT" For Output As #2
            Print #2, vObjOracle(0).Fields(0)
            Close #2
            
            DoEvents
            
            vShell = Shell("H:\ORACLE\SISTEMAS\VB\32BITS\RLG002.EXE " & vCodLoja, vbNormalFocus)
            
            Call RodarRoteirizacao(vCodLoja)
            
Proximo_Deposito:
            If Dir("C:\RLG001\RLG001.TXT") <> "" Then
            
                Kill "C:\RLG001\RLG001.TXT"
            
            End If
            
            vObjOracle(0).MoveNext
            
        Wend
        
        Print #1, Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Fim do processo de gera��o do roteiro de liga��o"
        lstLog.AddItem Format(Now(), "dd/MM/yyyy hh:mm:ss") & " -> Fim do processo de gera��o do roteiro de liga��o"
        lstLog.Refresh
        lstLog.Selected(lstLog.ListCount - 1) = True
        
        Screen.MousePointer = vbDefault
        Close #1
        
        Call vClsDPK001.Informar("Processo de gera��o do roteiro de liga��o finalizado.")
    
    End If
    
    Exit Sub

End Sub
Private Sub cmdSair_Click()

    Unload Me

End Sub

Private Sub Form_Load()

    Me.Top = 0
    Me.Left = 0

End Sub
