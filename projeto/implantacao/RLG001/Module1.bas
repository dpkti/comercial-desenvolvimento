Attribute VB_Name = "Module1"
Option Explicit

Sub RetornaDepositos()

    On Error GoTo Erro

    Call vClsDPK001.ExcluiBind

    vBanco.Parameters.Add "PACAO", 1, 1
    vBanco.Parameters.Add "PCODLOJA", 0, 1
    vBanco.Parameters.Add "PDATA", "", 1
    
    vBanco.Parameters.Add "PRETORNO1", 0, 2
    vBanco.Parameters("PRETORNO1").serverType = 102
    vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
    vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "PCODERRO", 0, 2
    vBanco.Parameters.Add "PMSGERRO", "", 2
    
    vSql = _
        vOwners(1) & ".PCK_RLG001.PR_INICIO(" & _
           ":PACAO, " & _
           ":PCODLOJA, " & _
           ":PDATA, " & _
           ":PRETORNO1, " & _
           ":PCODERRO, " & _
           ":PMSGERRO)"
    
    Call vClsDPK001.ExecutaPL(vSql)

    If vBanco.Parameters("PCODERRO") <> 0 Then

        Call vClsDPK001.TratarErro(CStr(vBanco.Parameters("PCODERRO")), CStr(vBanco.Parameters("PMSGERRO")), True, "cmbDeposito_Click")
        Exit Sub

    End If
    
    Set vObjOracle(0) = vBanco.Parameters("PRETORNO1").Value
    
    Exit Sub

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "RetornaDepositos")

End Sub
Sub RodarRoteirizacao(pCodLoja As Integer, Optional pDataInicio As String, Optional pDataFim As String)

    Call vClsDPK001.ExcluiBind

    vBanco.Parameters.Add "PCODLOJA", pCodLoja, 1
    vBanco.Parameters.Add "PDATAINICIO", pDataInicio, 1
    vBanco.Parameters.Add "PDATAFIM", pDataFim, 1
    
    vBanco.Parameters.Add "PRETORNO1", 0, 2
    vBanco.Parameters("PRETORNO1").serverType = 102
    vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
    vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "PCODERRO", 0, 2
    vBanco.Parameters.Add "PMSGERRO", "", 2
    
    vSql = _
        vOwners(1) & ".PCK_RLG001.PR_ROTEIRIZACAO(" & _
           ":PCODLOJA, " & _
           ":PDATAINICIO, " & _
           ":PDATAFIM, " & _
           ":PCODERRO, " & _
           ":PMSGERRO)"
    
    Call ExecutaPL(vSql)

    If vBanco.Parameters("PCODERRO") <> 0 Then

        Exit Sub

    End If
    
    If Err.Number <> 0 Then
    
        Exit Sub
    
    End If

End Sub
Sub GerarMediaClientes(Optional pCodLoja As Integer, Optional pData As String)

    On Error GoTo Erro

    Call vClsDPK001.ExcluiBind

    vBanco.Parameters.Add "PACAO", 9, 1
    vBanco.Parameters.Add "PCODLOJA", Val(pCodLoja), 1
    vBanco.Parameters.Add "PDATA", pData, 1
    
    vBanco.Parameters.Add "PRETORNO1", 0, 2
    vBanco.Parameters("PRETORNO1").serverType = 102
    vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
    vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "PCODERRO", 0, 2
    vBanco.Parameters.Add "PMSGERRO", "", 2
    
    vSql = _
        vOwners(1) & ".PCK_RLG001.PR_INICIO(" & _
           ":PACAO, " & _
           ":PCODLOJA, " & _
           ":PDATA, " & _
           ":PRETORNO1, " & _
           ":PCODERRO, " & _
           ":PMSGERRO)"
    
    Call vClsDPK001.ExecutaPL(vSql)

    If vBanco.Parameters("PCODERRO") <> 0 Then

        Call vClsDPK001.TratarErro(CStr(vBanco.Parameters("PCODERRO")), CStr(vBanco.Parameters("PMSGERRO")), True, "cmbDeposito_Click")
        Exit Sub

    End If
    
    Exit Sub

Erro:

    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "GerarMediaClientes")

End Sub
Function TestarLink(pCodLoja As Integer) As Boolean

    TestarLink = False

    Call vClsDPK001.ExcluiBind

    vBanco.Parameters.Add "PCODLOJA", pCodLoja, 1
    
    vBanco.Parameters.Add "PRETORNO1", 0, 2
    vBanco.Parameters("PRETORNO1").serverType = 102
    vBanco.Parameters("PRETORNO1").DynasetOption = &H2&
    vBanco.Parameters("PRETORNO1").DynasetCacheParams 256, 16, 20, 2000, 0

    vBanco.Parameters.Add "PCODERRO", 0, 2
    vBanco.Parameters.Add "PMSGERRO", "", 2
    
    vSql = _
        vOwners(1) & ".PCK_RLG001.PR_TESTAR_LINK(" & _
           ":PCODLOJA, " & _
           ":PRETORNO1, " & _
           ":PCODERRO, " & _
           ":PMSGERRO)"
    
    Call ExecutaPL(vSql)
    
    If vBanco.Parameters("PCODERRO") <> 0 Then

        Exit Function

    End If
    
    If Err.Number <> 0 Then
    
        Exit Function
    
    End If
    
    Set vObjOracle(1) = vBanco.Parameters("PRETORNO1").Value
    
    If Not vObjOracle(1).EOF Then
    
        TestarLink = True
    
    End If
    
End Function
Public Sub ExecutaPL(pSQL As String)

    On Error Resume Next
    
    pSQL = "BEGIN " & pSQL & "; END;"

    vBanco.ExecuteSQL pSQL

End Sub
