VERSION 5.00
Object = "{DEF83C46-0927-11D7-924F-000103D2A28C}#4.0#0"; "BOTOES.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm mdiRLG001 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   Caption         =   ".:  MDI PADR�O (800x600)  :."
   ClientHeight    =   8160
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   Icon            =   "mdiRLG001.frx":0000
   LinkTopic       =   "MDIForm1"
   LockControls    =   -1  'True
   Begin Threed.SSPanel SSPanel1 
      Align           =   1  'Align Top
      Height          =   1770
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11880
      _Version        =   65536
      _ExtentX        =   20955
      _ExtentY        =   3122
      _StockProps     =   15
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin Bot�o.cmd cmdSair 
         Height          =   960
         Left            =   11205
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Sair do sistema"
         Top             =   180
         Width           =   645
         _ExtentX        =   1138
         _ExtentY        =   1693
         BTYPE           =   3
         TX              =   "Sair"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiRLG001.frx":0CCA
         PICN            =   "mdiRLG001.frx":0CE6
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdSobre 
         Height          =   960
         Left            =   10440
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Informa��es do sistema"
         Top             =   180
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   1693
         BTYPE           =   3
         TX              =   "Sobre"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiRLG001.frx":19C0
         PICN            =   "mdiRLG001.frx":19DC
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdMensal 
         Height          =   960
         Left            =   2115
         TabIndex        =   1
         TabStop         =   0   'False
         ToolTipText     =   "Executar"
         Top             =   180
         Width           =   1230
         _ExtentX        =   2170
         _ExtentY        =   1693
         BTYPE           =   3
         TX              =   "Rotina mensal"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiRLG001.frx":26B6
         PICN            =   "mdiRLG001.frx":26D2
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin Bot�o.cmd cmdEmergencia 
         Height          =   960
         Left            =   3375
         TabIndex        =   2
         TabStop         =   0   'False
         ToolTipText     =   "Executar"
         Top             =   180
         Width           =   1770
         _ExtentX        =   3122
         _ExtentY        =   1693
         BTYPE           =   3
         TX              =   "Rotina de emerg�ncia"
         ENAB            =   -1  'True
         BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         COLTYPE         =   2
         FOCUSR          =   0   'False
         BCOL            =   16777215
         BCOLO           =   16777215
         FCOL            =   0
         FCOLO           =   0
         MCOL            =   12632256
         MPTR            =   1
         MICON           =   "mdiRLG001.frx":33AC
         PICN            =   "mdiRLG001.frx":33C8
         UMCOL           =   -1  'True
         SOFT            =   0   'False
         PICPOS          =   2
         NGREY           =   0   'False
         FX              =   3
         HAND            =   0   'False
         CHECK           =   0   'False
         VALUE           =   0   'False
      End
      Begin VB.Label lblRazaoSocial 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   45
         TabIndex        =   5
         Top             =   1440
         Width           =   120
      End
      Begin VB.Label lblDepositoConectado 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   11685
         TabIndex        =   6
         Top             =   1440
         Width           =   120
      End
      Begin VB.Image Image4 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   0
         Picture         =   "mdiRLG001.frx":40A2
         Stretch         =   -1  'True
         Top             =   1350
         Width           =   11940
      End
      Begin VB.Image Image3 
         Height          =   1380
         Left            =   0
         Picture         =   "mdiRLG001.frx":41D8
         Top             =   0
         Width           =   2265
      End
      Begin VB.Image Image1 
         Height          =   1365
         Left            =   0
         Picture         =   "mdiRLG001.frx":5727
         Stretch         =   -1  'True
         Top             =   0
         Width           =   11895
      End
   End
End
Attribute VB_Name = "mdiRLG001"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdEmergencia_Click()

    frmRotinaEmergencia.Show

End Sub
Private Sub cmdMensal_Click()

    frmRotinaMensal.Show

End Sub
Private Sub cmdSair_Click()

    If vClsDPK001.Perguntar("Confirma sa�da do sistema?") = vbYes Then

        Call vClsDPK001.Sair

    End If

End Sub
Private Sub cmdSobre_Click()

    frmSobre.Show 1

End Sub

Private Sub MDIForm_Load()
    
    On Error GoTo Erro
    
    If Dir("C:\RLG001\RLG001.TXT") <> "" Then
    
        Kill "C:\RLG001\RLG001.TXT"
    
    End If
    
    Me.Top = 0
    Me.Left = 0
    
    lblRazaoSocial = ""
    lblDepositoConectado = ""
    
    Call vClsDPK001.ConectarOracle("PRODUCAO", "RLG001", "DPK9475", , Me)
    
    If vTipoCDConectado = "U" Then
    
        vOwners(1) = "VENDAS"
    
    ElseIf vTipoCDConectado = "M" Then
    
        vOwners(1) = "DEP" & Format(vCD, "00")
    
    End If
    
    Me.Caption = ".:  " & UCase(vObjOracle(0).Fields(0)) & " - " & UCase(vObjOracle(0).Fields(1) & "   [" & vObjOracle(0).Fields(2) & "]") & "  :."
    
    'PARA INCLUIR O FORM DE CONEX�O DA INTRANET, INCLUIR O FORM ABAIXO E
    'DESCOMENTAR A LINHA ABAIXO
    '
    'F:\SISTEMAS\Padr�es\VB6\Forms\Form de Conex�o da Intranet.frm
    'frmConexaoIntranet.Show 1
    
    If Dir("C:\RLG001", vbDirectory) = "" Then
    
        MkDir "C:\RLG001"
    
    End If
    
    Exit Sub
    
Erro:
    
    Call vClsDPK001.TratarErro(Err.Number, Err.Description, True, "MDIForm_Load")
    
End Sub
Private Sub MDIForm_Resize()

    If Me.WindowState = 2 Then
    
        Me.WindowState = vbNormal
        
        Me.Height = 8580
        Me.width = 12000
        
    End If

End Sub

Private Sub MDIForm_Unload(Cancel As Integer)

    Cancel = 1

End Sub
