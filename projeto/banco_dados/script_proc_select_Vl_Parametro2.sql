Procedure Pr_Select_Vl_Parametro2(Pm_Cursor1     Out Tp_Cursor,
                                    Pm_Cod_Sistema In Number,
                                    Pm_Nome_Param  In Varchar2,
                                    Pm_Dep         In Varchar2) Is
    Vsql Varchar2(4000) := '';
  Begin
    Vsql := 'SELECT * FROM ' || Pm_Dep || 'PARAMETROS ';
    Vsql := Vsql || 'WHERE COD_SOFTWARE = ' || Pm_Cod_Sistema;
 
    If Length(Trim(Pm_Nome_Param)) > 0 Then
      Vsql := Vsql || ' AND NOME_PARAMETRO = ' || Chr(39) || Pm_Nome_Param || Chr(39);
    End If;
 
    Open Pm_Cursor1 For Vsql;
 
  End;
