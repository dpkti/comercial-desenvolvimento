CREATE OR REPLACE PACKAGE PRODUCAO.PCK_VDA610
--------------------------------------------
--- SISTEMA	: ESTATISTICA LISTA LIGACOES P/TLMKT
--- ANALISTA    : MARICI
--- DATA	: 24/07/2003

IS
 -- variavel tipo cursor
 TYPE tp_cursor IS REF CURSOR;

 PROCEDURE PR_TLMK(p_cursor   IN OUT tp_cursor,
 	           p_tabela_banco IN VARCHAR2,
                   p_erro     OUT    NUMBER);

 PROCEDURE PR_LIGACOES(p_cursor   IN OUT tp_cursor,
 		       p_cod_vend IN     representante.cod_repres%TYPE,
              	       p_erro     OUT    NUMBER);

 PROCEDURE PR_LIGVEND(p_cursor   IN OUT tp_cursor,
 		      p_cod_vend IN     representante.cod_repres%TYPE,
 		      p_dt_contato IN   vendas.contato.dt_contato%TYPE,
                      p_erro     OUT    NUMBER);

 PROCEDURE PR_RESULTADOS(p_cursor   IN OUT tp_cursor,
  		         p_erro     OUT    NUMBER);


 PROCEDURE PR_TOTAL_PEDIDOS(p_cursor       IN OUT tp_cursor,
      			    p_cod_vend     IN     representante.cod_repres%TYPE,
      			    p_dt_inicio    IN     producao.pednota_venda.dt_pedido%TYPE,
      			    p_dt_fim       IN     producao.pednota_venda.dt_pedido%TYPE,
      			    p_erro         OUT    NUMBER);

 PROCEDURE PR_REGIONAIS(p_cursor   IN OUT tp_cursor,
 			p_tabela_banco IN VARCHAR2,
  		         p_erro     OUT    NUMBER);

  PROCEDURE PR_TOTAL_CONTATOS(p_cursor       IN OUT tp_cursor,
        	 	      p_dt_inicio    IN     producao.pednota_venda.dt_pedido%TYPE,
        		      p_dt_fim       IN     producao.pednota_venda.dt_pedido%TYPE,
        		      p_cod_regional IN     producao.filial.cod_filial%TYPE,
      			      p_erro         OUT    NUMBER);

 Procedure Pr_Select_Contatos (
                              Pm_Cursor IN OUT TP_CURSOR,
                              Pm_Tabela_Banco IN Varchar2,
                              Pm_DtIni  IN Varchar2,
                              Pm_DtFim  IN Varchar2,
                              Pm_Erro   OUT Number
                              );
 Procedure Pr_Select_Cod_Repres (
                                 Pm_Cursor IN OUT TP_CURSOR,
                                 Pm_Vend   IN R_Repven.Cod_Vend%type,
                                 Pm_Erro   OUT Number
                                );
 Procedure Pr_Select_VD (
                        Pm_Cursor   IN OUT TP_CURSOR,
                        Pm_Cod_Vend IN r_clie_repres.cod_repres%Type,
                        Pm_Erro     Out Number
                        );
 Procedure Pr_Select_Dt_Faturamento (
                                    PM_CURSOR  IN OUT Tp_Cursor,
                                    Pm_Tabela_banco IN Varchar2,
                                    Pm_CodErro OUT Number
                                    );
 Procedure Pr_Select_Datas (
                           Pm_Cursor IN OUT TP_CURSOR
                           );
  Procedure Pr_Select_Resultado (
                                Pm_Cursor IN OUT TP_CURSOR,
                                Pm_DtIni  IN Varchar2,
                                Pm_DtFim  IN Varchar2,
                                Pm_Vend   IN vendas.Contato.cod_vend%type,
                                Pm_Erro   OUT Number
                                );

Procedure Pr_Update_Agenda (
                              pm_Vend       IN  Vendas.Agenda.Cod_Vend%Type,
                              pm_DataAgenda IN  varchar2,
                              pm_CodCliente IN  Vendas.Agenda.Cod_Cliente%type,
                              pm_Cod_Erro   OUT Number,
                              pm_Txt_Erro   OUT Varchar2
                              );

   Procedure Pr_Select_Agendados(
                                 pm_Cursor1  IN OUT TP_CURSOR,
                                 pm_Cod_Vend IN Vendas.Agenda.cod_vend%Type,
                                 pm_Data1    IN Varchar2,
                                 pm_Cod_Erro OUT Number,
                                 pm_Txt_Erro OUT Varchar2
                                 );

   Procedure Pr_Select_Clientes (
                                pm_Cursor   IN OUT TP_CURSOR,
                                pm_Cod1     IN Cliente.Cod_Cliente%Type,
                                pm_Cod_Erro OUT Number,
                                pm_Txt_Erro OUT Varchar2
                                );

   Procedure Pr_Delete_Agendados (
                                 pm_Cod_Vend    IN Vendas.agenda.Cod_Vend%Type,
                                 pm_Dt          IN Varchar2,
                                 pm_Cod_Cliente IN Vendas.agenda.Cod_Cliente%type,
                                 pm_Cod_Erro    OUT Number,
                                 pm_Txt_Erro    OUT Varchar2
                                 );
   Procedure Pr_Insert_Agenda (
                               pm_Cod_Vend    IN Vendas.agenda.Cod_Vend%Type,
                               pm_Dt          IN Varchar2,
                               pm_Cod_Cliente IN Vendas.agenda.Cod_Cliente%type,
                               pm_Obs         IN Vendas.agenda.Observacao%Type,
                               pm_Cod_Erro    OUT Number,
                               pm_Txt_Erro    OUT Varchar2
                              );

PROCEDURE PR_DIAS(p_cursor   IN OUT tp_cursor,
  		  p_erro     OUT    NUMBER);


PROCEDURE PR_LISTA_CONTATOS(p_cursor   IN OUT tp_cursor,
 		            p_cod_vend IN     representante.cod_repres%TYPE,
 		            p_dt_inicio IN   vendas.contato.dt_contato%TYPE,
 		            p_dt_fim   IN   vendas.contato.dt_contato%TYPE,
                            p_erro     OUT    NUMBER);

PROCEDURE PR_TEMPO_TOTAL(p_cursor   IN OUT tp_cursor,
 		         p_cod_vend IN     representante.cod_repres%TYPE,
 		         p_dt_inicio IN   vendas.contato.dt_contato%TYPE,
 		         p_dt_fim   IN   vendas.contato.dt_contato%TYPE,
                         p_erro     OUT    NUMBER);

PROCEDURE PR_SEGMENTO_FILIAL(p_cursor   IN OUT tp_cursor,
 		             p_cod_filial IN    filial.cod_filial%TYPE,
 		             p_erro     OUT    NUMBER);

PROCEDURE PR_MOTIVOS(p_cursor   IN OUT tp_cursor,
  		     p_erro     OUT    NUMBER);



PROCEDURE PR_DESC_ADIC_MEDIO(p_cursor   IN OUT tp_cursor,
 		             p_cod_vend IN     representante.cod_repres%TYPE,
 		             p_data_inicio IN  DATE,
 		             p_data_fim    IN  DATE,
 		             p_erro     OUT    NUMBER);

PROCEDURE PR_MARGEM(p_cursor   IN OUT tp_cursor,
 		    p_cod_vend IN     representante.cod_repres%TYPE,
 		    p_data_inicio IN  DATE,
 		    p_data_fim    IN  DATE,
 		    p_erro     OUT    NUMBER);

PROCEDURE PR_TP_DESCONTO(p_cursor   IN OUT tp_cursor,
 		         p_cod_vend IN     representante.cod_repres%TYPE,
 		         p_data_inicio IN  DATE,
 		         p_data_fim    IN  DATE,
 		         p_erro     OUT    NUMBER);

Procedure Pr_Est_contatos (pm_Cursor   IN OUT tp_cursor, pcd_filial in integer,ptipo_ligacao in integer);

Procedure Pr_Select_Vl_Parametro2(Pm_Cursor1     Out Tp_Cursor,
                                    Pm_Cod_Sistema In Number,
                                    Pm_Nome_Param  In Varchar2,
                                    Pm_Dep         In Varchar2);

END PCK_VDA610;
/
CREATE OR REPLACE PACKAGE BODY PRODUCAO.PCK_vda610 IS


  PROCEDURE PR_TLMK(p_cursor   IN OUT tp_cursor,
  		    p_tabela_banco IN VARCHAR2,
                    p_erro     OUT    NUMBER)
  IS
    w_sql      varchar2(1000);
    w_tipo_A   representante.tipo%TYPE;
    w_tipo_M   representante.tipo%TYPE;
    w_tipo_V   representante.tipo%TYPE;
  BEGIN

    p_erro := 0;

    w_tipo_A := 'A';
    w_tipo_M := 'M';
    w_tipo_V := 'V';

    w_sql := 'select cod_repres,pseudonimo ';
    w_sql := w_sql ||' from representante ';
    w_sql := w_sql ||' Where tipo in ('||chr(807)||w_tipo_A||chr(807)||','||chr(807);
    w_sql := w_sql ||w_tipo_M||chr(807)||','||chr(807)||w_tipo_V||chr(807)||') AND ';
    w_sql := w_sql ||' cod_filial in (  Select a.cod_filial  ';
    w_sql := w_sql ||' From controle_bases.r_fildep a, ';
    w_sql := w_sql || p_tabela_banco||'deposito b ';
    w_sql := w_sql ||' 	Where a.cod_loja=b.cod_loja) and ';
    w_sql := w_sql ||' 	situacao=0 and dt_desligamento is null ';
    w_sql := w_sql ||' Order by cod_repres';

    --Abrindo Cursor
    OPEN p_cursor FOR w_sql;


    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;


  END;

  PROCEDURE PR_LIGACOES(p_cursor   IN OUT tp_cursor,
   		        p_cod_vend IN     representante.cod_repres%TYPE,
                	p_erro     OUT    NUMBER)


  IS

    BEGIN

      p_erro := 0;

      --Abrindo Cursor
      OPEN p_cursor FOR Select SUM(DECODE(b.situacao,0,1,0)) ativas,
		SUM(DECODE(b.situacao,9,1,0)) exec,
		SUM(DECODE(b.situacao,1,1,0)) interr,
		SUM(DECODE(b.situacao,2,1,0)) ocupado,
		SUM(DECODE(b.situacao,3,1,0)) n_atend
		From cliente a,
		vendas.ligacao b,
		r_clie_repres c
		Where  c.cod_repres in (Select x.cod_repres
		From r_repven x
		Where x.cod_vend = p_cod_vend) and
		a.cod_cliente = c.cod_cliente and
		a.cod_cliente = b.cod_cliente ;



      EXCEPTION
      WHEN OTHERS THEN
      p_erro := SQLCODE;


  END;

  PROCEDURE PR_LIGVEND(p_cursor   IN OUT tp_cursor,
   		      p_cod_vend IN     representante.cod_repres%TYPE,
   		      p_dt_contato IN   vendas.contato.dt_contato%TYPE,
                      p_erro     OUT    NUMBER)


    IS

      BEGIN

        p_erro := 0;

        --Abrindo Cursor
        OPEN p_cursor FOR Select nvl(sum(decode(tipo_ligacao,1,1,0)),0) ativas,
          nvl(sum(decode(tipo_ligacao,2,1,0)),0) receptivas
            from vendas.contato
            where
            	cod_resultado not in (15,16,19,20,22,23) and
            	cod_vend = p_cod_vend and
              dt_contato >= p_dt_contato and
              dt_contato < p_dt_contato + 1;


        EXCEPTION
        WHEN OTHERS THEN
        p_erro := SQLCODE;


  END;

  PROCEDURE PR_RESULTADOS(p_cursor   IN OUT tp_cursor,
  		          p_erro     OUT    NUMBER)



   IS

        BEGIN

          p_erro := 0;

          --Abrindo Cursor
          OPEN p_cursor FOR select cod_resultado,desc_resultado
				from(
				select
				case when cod_resultado=1 then
				1
				when cod_resultado=2 then
				2
				when cod_resultado=8 then
				3
				else
				cod_resultado + 1
				end ordem,
				cod_resultado ,
				substr(desc_resultado,1,25) desc_resultado
				from vendas.resultado
				Order by ordem,cod_resultado);



          EXCEPTION
          WHEN OTHERS THEN
          p_erro := SQLCODE;


    END;

    PROCEDURE PR_TOTAL_PEDIDOS(p_cursor       IN OUT tp_cursor,
      			       p_cod_vend     IN     representante.cod_repres%TYPE,
      			       p_dt_inicio    IN     producao.pednota_venda.dt_pedido%TYPE,
      			       p_dt_fim       IN     producao.pednota_venda.dt_pedido%TYPE,
         		       p_erro         OUT    NUMBER)


          IS

          BEGIN

            p_erro := 0;

            --Abrindo Cursor
                 OPEN p_cursor FOR select count(*) total,
                 SUM(vl_contabil) vl_medio
    		from pednota_venda,
    		datas
    		where situacao=0 and
    		cod_vend=p_cod_vend and
    		dt_digitacao >= p_dt_inicio and
    		dt_digitacao < p_dt_fim + 1;



           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;


       END;

 PROCEDURE PR_REGIONAIS(p_cursor   IN OUT tp_cursor,
 			p_tabela_banco IN VARCHAR2,
  		        p_erro     OUT    NUMBER)

IS
    w_sql      varchar2(1500);

  BEGIN

    p_erro := 0;


    w_sql := 'Select distinct(a.cod_filial) cod_filial,c.nome_filial ';
    w_sql := w_sql ||' From controle_bases.r_fildep a, ';
    w_sql := w_sql || p_tabela_banco||'deposito b,PRODUCAO.filial c ';
    w_sql := w_sql ||' 	Where a.cod_loja=b.cod_loja and ';
    w_sql := w_sql ||' 	a.cod_filial=c.cod_filial and c.tp_filial='||chr(807)||'R'||chr(807);
    w_sql := w_sql ||' and c.divisao='||chr(807)||'D'||chr(807);

    --Abrindo Cursor
    OPEN p_cursor FOR w_sql;



    EXCEPTION
    WHEN OTHERS THEN
    p_erro := SQLCODE;


  END;

  PROCEDURE PR_TOTAL_CONTATOS(p_cursor       IN OUT tp_cursor,
          	 	      p_dt_inicio    IN     producao.pednota_venda.dt_pedido%TYPE,
          		      p_dt_fim       IN     producao.pednota_venda.dt_pedido%TYPE,
          		      p_cod_regional IN     producao.filial.cod_filial%TYPE,
      			      p_erro         OUT    NUMBER)

          IS

          BEGIN

            p_erro := 0;

            --Abrindo Cursor
                 OPEN p_cursor FOR
                      SELECT a.cod_vend,
                             b.pseudonimo,
                             c.sigla,
 		                         sum(decode(a.tipo_ligacao, 1, 1, 0)) ativa,
                             sum(decode(a.tipo_ligacao, 2, 1, 0)) receptiva,
                             sum(a.tempo_ligacao) / (sum(decode(a.tipo_ligacao, 1, 1, 0)) + sum(decode(a.tipo_ligacao, 2, 1, 0))) Tempo_Medio
		                  FROM   vendas.contato a,
                             representante b,
                             filial c
		                  WHERE
		                   a.cod_resultado not in (15,16,19,20,22,23) AND
		                  a.dt_contato >= to_date(p_dt_inicio, 'DD/MM/RR')
		                  AND    a.dt_contato < to_date(p_dt_fim, 'DD/MM/RR') + 1
		                  AND    b.cod_filial IN (SELECT cod_filial
		                                          FROM filial x
		                                          WHERE DECODE(COD_REGIONAL,0,COD_FILIAL,COD_REGIONAL) = p_cod_regional
		                                          AND   x.cod_filial = c.cod_filial)
		                  AND b.cod_filial = c.cod_filial
		                  AND a.cod_vend+0 = b.cod_repres
		                  GROUP BY a.cod_vend, b.pseudonimo, c.sigla
		                  ORDER BY c.sigla, b.pseudonimo;
           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;


       END;


 Procedure Pr_Select_Contatos (
                              Pm_Cursor IN OUT TP_CURSOR,
                              Pm_Tabela_Banco IN Varchar2,
                              Pm_DtIni  IN Varchar2,
                              Pm_DtFim  IN Varchar2,
                              Pm_Erro   OUT Number
                              ) is
    vSql Varchar(4000);
    BEGIN
      Pm_Erro :=0;
      vSql := 'Select a.cod_resultado, b.desc_resultado, count(*) TOTAL ';
      vSql := vSql || 'From vendas.contato a, vendas.resultado b, representante c ';
      vSql := vSql || 'Where  a.cod_resultado not in (15,16,19,20,22,23) and a.dt_contato >= to_date(' || chr(39) || Pm_dtIni || chr(39) || ',' || chr(39) || 'DD/MM/RR' || chr(39) || ') ';
      vSql := vSql || 'and   a.dt_contato < to_date(' || chr(39) || Pm_dtFim || chr(39) || ',' || chr(39) || 'DD/MM/RR' || chr(39) || ') + 1 ';
      vSql := vSql || 'and   c.cod_filial in (Select distinct(a.cod_filial) cod_filial From controle_bases.r_fildep a, ';
      vSql := vSql || Pm_Tabela_Banco || 'deposito b ';
      vSql := vSql || 'Where a.cod_loja=b.cod_loja) ';
      vSql := vSql || 'and a.cod_vend = c.cod_repres ';
      vSql := vSql || 'and a.cod_resultado = b.cod_resultado ';
      vSql := vSql || 'Group by a.cod_resultado,b.desc_resultado ';
      vSql := vSql || 'Order by a.cod_resultado';

      OPEN Pm_Cursor for
           vSql;

      Exception
      When Others then
           Pm_Erro := Sqlcode;
 End;

 Procedure Pr_Select_Cod_Repres (
                                 Pm_Cursor IN OUT TP_CURSOR,
                                 Pm_Vend   IN R_Repven.Cod_Vend%type,
                                 Pm_Erro   OUT Number
                                ) is
   Begin
        OPEN Pm_Cursor FOR
             SELECT cod_repres
             From   r_repven
             Where  cod_vend = Pm_Vend;

        Exception
        When Others Then
             Pm_Erro := Sqlcode;
 End;
 Procedure Pr_Select_VD (
                        Pm_Cursor   IN OUT TP_CURSOR,
                        Pm_Cod_Vend IN r_clie_repres.cod_repres%Type,
                        Pm_Erro     Out Number
                        ) is
    vSql Varchar2(4000);
    BEGIN
        Pm_Erro :=0;

        vSql := 'Select a.cod_cliente,a.nome_cliente,a.nome_contato, ';
        vSql := vSql || 'a.ddd1,a.fone1,b.dt_ligacao,d.cod_repres, c.nome_cidade,c.cod_uf,ddd2,fone2 ';
        vSql := vSql || 'From cliente a, vendas.ligacao b, cidade c, r_clie_repres d ,(SELECT cod_repres From r_repven Where cod_vend =' || Pm_COd_Vend || ') E';
        vSql := vSql || ' Where d.cod_repres in (' || Pm_Cod_Vend || ', ' || ' E.COD_REPRES) and ';
        vSql := vSql || ' b.situacao <> 9 and a.cod_cliente = d.cod_cliente and ';
        vSql := vSql || ' a.cod_cidade = c.cod_cidade and a.cod_cliente = b.cod_cliente ';
        vSql := vSql || ' order by c.nome_cidade,b.dt_ligacao,a.nome_cliente';

        Open Pm_Cursor for
             vSql;

        Exception
        When Others then
             Pm_erro := SqlCode;
 End;

 Procedure Pr_Select_Dt_Faturamento (
                                    PM_CURSOR IN OUT Tp_Cursor,
                                    Pm_Tabela_banco IN Varchar2,
                                    Pm_CodErro OUT Number
                                    ) is

    vSql Varchar2(4000);
    BEGIN
         Pm_CodErro :=0;

         vSql := 'Select to_char(a.cod_loja,' || chr(39) || '09' || chr(39) || ') || ' || chr(39) || '-' || chr(39) || ' || b.nome_fantasia deposito_default, a.cod_filial ';
         vSql := vSql || ' From ' || Pm_Tabela_Banco || 'deposito a, loja b ';
         vSql := vSql || ' Where a.cod_loja=b.cod_loja';
         OPEN PM_CURSOR FOR
              vSql;
    Exception
    When Others then
         Pm_CodErro :=SqlCode;
 End;
 Procedure Pr_Select_Datas (
                           Pm_Cursor IN OUT TP_CURSOR
                           ) is

     BEGIN
          OPEN PM_CURSOR FOR
               Select dt_real
               From datas;
 End;

  Procedure Pr_Select_Resultado (
                                Pm_Cursor IN OUT TP_CURSOR,
                                Pm_DtIni  IN Varchar2,
                                Pm_DtFim  IN Varchar2,
                                Pm_Vend   IN vendas.Contato.cod_vend%type,
                                Pm_Erro   OUT Number
                                ) is
    BEGIN
       Pm_Erro :=0;
       OPEN PM_CURSOR FOR
          Select a.cod_resultado,
                 b.desc_resultado,
                 count(*) TOTAL
          From   vendas.contato a,
                 vendas.resultado b
          Where
         --  A.cod_resultado not in (19,20,22) AND
           a.dt_contato >= to_date(Pm_dtIni,'DD/MM/RR')
          and    a.dt_contato < to_date(Pm_DtFim,'DD/MM/RR') + 1
          and    a.cod_vend = Pm_vend
          and    a.cod_resultado = b.cod_resultado
          Group by a.cod_resultado,
                   b.desc_resultado
          Order by a.cod_resultado;
      Exception
      When others then
           Pm_Erro := Sqlcode;
  End;


   Procedure Pr_Update_Agenda (
                              pm_Vend       IN  Vendas.Agenda.Cod_Vend%Type,
                              pm_DataAgenda IN  Varchar2,
                              pm_CodCliente IN  Vendas.Agenda.Cod_Cliente%type,
                              pm_Cod_Erro   OUT Number,
                              pm_Txt_Erro   OUT Varchar2
                              ) is
       Begin
          Update vendas.agenda
          Set    situacao = 9
          Where  cod_vend = pm_Vend
          and    dt_agenda = to_date(pm_DataAgenda,'dd/mm/rr hh24:mi')
          and    cod_cliente = pm_CodCliente;

          COMMIT;

          EXCEPTION
             WHEN OTHERS THEN
                  ROLLBACK;
                  pm_Cod_erro := SQLCODE;
                  pm_Txt_erro := SQLERRM;

   End Pr_Update_Agenda;

   Procedure Pr_Select_Agendados(
                                 pm_Cursor1  IN OUT TP_CURSOR,
                                 pm_Cod_Vend IN Vendas.Agenda.cod_vend%Type,
                                 pm_Data1    IN Varchar2,
                                 pm_Cod_Erro OUT Number,
                                 pm_Txt_Erro OUT Varchar2
                                 ) is
          Begin
            pm_Cod_erro := 0;
            pm_Txt_erro := 0;
            OPEN PM_CURSOR1 FOR
              SELECT a.cod_cliente,
                     b.nome_cliente,
                     nvl(b.ddd1,0) ddd1, nvl(b.fone1,0) fone1,
                     to_char(a.dt_agenda,'dd/mm/yy hh24:mi') dt_agenda,
                     nvl(a.observacao,' ') observacao
              FROM   vendas.agenda a,
                     cliente b
              WHERE  a.cod_cliente = b.cod_cliente
              AND    a.cod_vend = pm_Cod_Vend
              AND    a.situacao=0
              AND    a.dt_agenda >= TO_DATE(pm_Data1,'DD/MM/RR')
              AND    a.dt_agenda < (TO_DATE(pm_Data1,'DD/MM/RR') + 1)
              ORDER BY a.dt_agenda asc;

          EXCEPTION
             WHEN OTHERS THEN
                  ROLLBACK;
                  pm_Cod_erro := SQLCODE;
                  pm_Txt_erro := SQLERRM;
   End Pr_Select_Agendados;

   Procedure Pr_Select_Clientes (
                                pm_Cursor   IN OUT TP_CURSOR,
                                pm_Cod1     IN Cliente.Cod_Cliente%Type,
                                pm_Cod_Erro OUT Number,
                                pm_Txt_Erro OUT Varchar2
                                ) is
      Begin
         pm_Cod_erro := 0;
         pm_Txt_erro := 0;

         OPEN pm_Cursor For
           SELECT cli.COD_CLIENTE,
                  cli.NOME_CLIENTE,
                  cli.SITUACAO,
                  cid.NOME_CIDADE,
                  cid.COD_UF
           FROM   CLIENTE cli,
                  CIDADE cid
           WHERE  cli.COD_CIDADE = cid.COD_CIDADE
           AND    cli.COD_CLIENTE = pm_Cod1;

         EXCEPTION
           WHEN OTHERS THEN
                ROLLBACK;
                pm_Cod_erro := SQLCODE;
                pm_Txt_erro := SQLERRM;

   End Pr_Select_Clientes;

   Procedure Pr_Delete_Agendados (
                                 pm_Cod_Vend    IN Vendas.agenda.Cod_Vend%Type,
                                 pm_Dt          IN Varchar2,
                                 pm_Cod_Cliente IN Vendas.agenda.Cod_Cliente%type,
                                 pm_Cod_Erro    OUT Number,
                                 pm_Txt_Erro    OUT Varchar2
                                 ) is
      Begin
         pm_Cod_erro := 0;
         pm_Txt_erro := 0;

         DELETE vendas.agenda
         WHERE  cod_vend    = pm_Cod_Vend
         AND    dt_agenda   = to_date(pm_Dt,'dd/mm/rr hh24')
         AND    cod_cliente = pm_Cod_Cliente;

         COMMIT;

         EXCEPTION
           WHEN OTHERS THEN
                ROLLBACK;
                pm_Cod_erro := SQLCODE;
                pm_Txt_erro := SQLERRM;
   End Pr_Delete_Agendados;

   Procedure Pr_Insert_Agenda (
                               pm_Cod_Vend    IN Vendas.agenda.Cod_Vend%Type,
                               pm_Dt          IN Varchar2,
                               pm_Cod_Cliente IN Vendas.agenda.Cod_Cliente%type,
                               pm_Obs         IN Vendas.agenda.Observacao%Type,
                               pm_Cod_Erro    OUT Number,
                               pm_Txt_Erro    OUT Varchar2
                              ) is
       BEGIN
          Pm_Cod_erro :=0;
          Pm_Txt_erro :='';

          INSERT INTO vendas.agenda
          VALUES      (pm_cod_Vend,
                       to_date(pm_dt,'dd/mm/rr hh24'),
                       pm_cod_cliente,
                       0,
                       pm_obs);

          COMMIT;

          EXCEPTION
             WHEN OTHERS THEN
                  ROLLBACK;
                  pm_cod_erro := SQLCODE;
                  pm_txt_erro := SQLERRM;
   End Pr_Insert_Agenda;

PROCEDURE PR_DIAS(p_cursor   IN OUT tp_cursor,
  		  p_erro     OUT    NUMBER)



 IS

          BEGIN

            p_erro := 0;

            --Abrindo Cursor
                 OPEN p_cursor FOR SELECT '1' NR, count(distinct cota_ideal_dia) total_dia
       		 FROM producao.ss_COTA_IDEAL A, DATAS
       		 WHERE A.DT_REAL >= DT_fin_FECH_MENSAL +1 AND
       			     A.DT_REAL < add_months(DT_ini_FECH_MENSAL,2)
       		UNION
     		SELECT '2' NR, count(distinct cota_ideal_dia) dia_uteis
     		FROM producao.ss_COTA_IDEAL A, DATAS
     		WHERE A.DT_REAL >= dt_fin_fech_mensal+1 and
     		  a.dt_real <= DT_FATURAMENTO+1
     		ORDER BY 1;


           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;


 END;


 PROCEDURE PR_LISTA_CONTATOS(p_cursor   IN OUT tp_cursor,
  		             p_cod_vend IN     representante.cod_repres%TYPE,
  		             p_dt_inicio IN   vendas.contato.dt_contato%TYPE,
  		             p_dt_fim   IN   vendas.contato.dt_contato%TYPE,
                             p_erro     OUT    NUMBER)


 IS

          BEGIN

            p_erro := 0;

            --Abrindo Cursor
                 OPEN p_cursor FOR select
		   a.cod_cliente,b.nome_cliente,
	   	  decode(a.tipo_ligacao,1,'ATIVA','RECEPTIVA') TP_LIGACAO,
		  a.dt_contato,
		  c.desc_resultado RESULTADO,
                  TRUNC(TEMPO_LIGACAO/60)||':'||TO_CHAR((ROUND(TEMPO_LIGACAO) - TRUNC(TEMPO_LIGACAO/60) * 60),'00') TEMPO_LIG,
                  NVL(A.DDD,0)||'-'||NVL(A.TELEFONE,0) FONE,
                 CASE WHEN A.TELEFONE=B.FONE1 AND FONE1 <> 0 THEN
  		  'S'
    	         ELSE
      		   CASE WHEN A.TELEFONE=B.FONE2 AND B.FONE2<> 0 THEN
      		    'S'
                   ELSE
                     CASE WHEN A.TELEFONE=NVL(E.FONE,0) AND E.FONE <> 0 THEN
          	      'S'
        	     ELSE
          	      'N'
        	     END
      		   END
                 END CADAST,
                 F.DESC_MOTIVO MOTIVO

		from vendas.contato a,
		cliente b,
		vendas.resultado c,
		REPRESENTANTE D,
    		VENDAS.R_CLIENTE_FONE E,
    		VENDAS.MOTIVO F
		where

		a.cod_vend in (p_cod_vend) and
		a.dt_contato>= to_date(p_dt_inicio,'dd/mm/rr') and
		a.dt_contato< to_date(p_dt_fim,'dd/mm/rr') + 1 and
		A.COD_MOTIVO = F.COD_MOTIVO(+) AND
    		A.COD_CLIENTE = E.COD_CLIENTE(+) AND
   		 A.TELEFONE=E.FONE(+) AND
		A.COD_VEND=D.COD_REPRES AND
		a.cod_resultado=c.cod_resultado and
		a.cod_cliente=b.cod_cliente
		ORDER BY A.COD_VEND,  A.DT_CONTATO;


           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;


 END;
PROCEDURE PR_TEMPO_TOTAL(p_cursor   IN OUT tp_cursor,
 		         p_cod_vend IN     representante.cod_repres%TYPE,
 		         p_dt_inicio IN   vendas.contato.dt_contato%TYPE,
 		         p_dt_fim   IN   vendas.contato.dt_contato%TYPE,
                         p_erro     OUT    NUMBER) is


         BEGIN

            p_erro := 0;

            --Abrindo Cursor
                 OPEN p_cursor FOR select   TRUNC(SUM(TEMPO_LIGACAO)/60)||':'||TO_CHAR((ROUND(SUM(TEMPO_LIGACAO)) - TRUNC(SUM(TEMPO_LIGACAO)/60) * 60),'00') TEMPO_LIG
		from vendas.contato a,
		cliente b,
		vendas.resultado c,
		REPRESENTANTE D,
    		VENDAS.R_CLIENTE_FONE E
		where

		a.cod_vend in (p_cod_vend) and
		a.dt_contato>= to_date(p_dt_inicio,'dd/mm/rr') and
		a.dt_contato< to_date(p_dt_fim,'dd/mm/rr') + 1 and
    		A.COD_CLIENTE = E.COD_CLIENTE(+) AND
   		 A.TELEFONE=E.FONE(+) AND
		A.COD_VEND=D.COD_REPRES AND
		a.cod_resultado=c.cod_resultado and
		a.cod_cliente=b.cod_cliente
		ORDER BY A.COD_VEND,  A.DT_CONTATO;


           EXCEPTION
           WHEN OTHERS THEN
           p_erro := SQLCODE;


 END;

 PROCEDURE PR_SEGMENTO_FILIAL(p_cursor   IN OUT tp_cursor,
  		             p_cod_filial IN    filial.cod_filial%TYPE,
 		             p_erro     OUT    NUMBER) IS

         BEGIN

             p_erro := 0;

             --Abrindo Cursor
                  OPEN p_cursor FOR select DISTINCT cod_segmento
			from filial a,
			PRODUCAO.R_TIPO_segmento_filial b
			where
			A.COD_FILIAL = p_cod_filial AND
			a.cod_tipo_segmento=b.cod_tipo_segmento;


            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;


 END;

PROCEDURE PR_MOTIVOS(p_cursor   IN OUT tp_cursor,
  		     p_erro     OUT    NUMBER) is

   BEGIN

             p_erro := 0;

             --Abrindo Cursor
                  OPEN p_cursor FOR SELECT COD_MOTIVO, DESC_MOTIVO MOTIVO
			FROM VENDAS.MOTIVO
	 		WHERE SITUACAO=0
			ORDER BY COD_MOTIVO;


            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;


 END;

PROCEDURE PR_DESC_ADIC_MEDIO(p_cursor   IN OUT tp_cursor,
 		             p_cod_vend IN     representante.cod_repres%TYPE,
 		             p_data_inicio IN  DATE,
 		             p_data_fim    IN  DATE,
 		             p_erro     OUT    NUMBER) is


  BEGIN

             p_erro := 0;

             --Abrindo Cursor
                  OPEN p_cursor FOR select
			 a.cod_vend cod_responsavel ,
			 round(avg(b.pc_desc3),2) pc_adic_md
			from pednota_venda a,
			itpednota_venda b
			where
			a.cod_vend = p_cod_vend and
			b.pc_desc3 > 0 and
			a.situacao = 0 and
			b.situacao = 0 and
			a.dt_digitacao>= to_date(p_data_inicio,'dd/mm/rr') and
			a.dt_digitacao < to_date(p_data_fim,'dd/mm/rr') + 1 and
			a.seq_pedido=b.seq_pedido and
			a.num_pedido=b.num_pedido and
			a.cod_loja=b.cod_loja
		  group by	a.cod_vend;


            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;


 END;


PROCEDURE PR_MARGEM(p_cursor   IN OUT tp_cursor,
 		    p_cod_vend IN     representante.cod_repres%TYPE,
 		    p_data_inicio IN  DATE,
 		    p_data_fim    IN  DATE,
 		    p_erro     OUT    NUMBER) is

  BEGIN

             p_erro := 0;

             --Abrindo Cursor
                  OPEN p_cursor FOR select
			a.cod_vend cod_responsavel,
			 case when sum(nvl(vl_bvista,0)) = 0 or sum(nvl(vl_lucro_bruto,0)) = 0  then
      				   0
      			 else
      				   round(sum(vl_lucro_bruto) / sum(vl_bvista) * 100,2)
      			 end mg
			from pednota_venda a,
			itpednota_venda b
			where
			a.cod_vend = p_cod_vend and
		        a.fl_ger_nfis||''='S' and
      			a.situacao = 0 and
			b.situacao = 0 and
			a.dt_emissao_nota>= to_date(p_data_inicio,'dd/mm/rr') and
			a.dt_emissao_nota < to_date(p_data_fim,'dd/mm/rr') + 1 and
			a.seq_pedido=b.seq_pedido and
			a.num_pedido=b.num_pedido and
			a.cod_loja=b.cod_loja
			group by
			  a.cod_vend;


            EXCEPTION
            WHEN OTHERS THEN
            p_erro := SQLCODE;


 END;

 PROCEDURE PR_TP_DESCONTO(p_cursor   IN OUT tp_cursor,
  		         p_cod_vend IN     representante.cod_repres%TYPE,
  		         p_data_inicio IN  DATE,
  		         p_data_fim    IN  DATE,
 		         p_erro     OUT    NUMBER) is

 BEGIN

              p_erro := 0;

              --Abrindo Cursor
                   OPEN p_cursor FOR select tp_desconto
			from
			(
			select

			     cod_vend
					 cod_responsavel ,
						 c.tp_desconto,
			       count(*) total
						from pednota_venda a,
						itpednota_venda b,
			      R_PEDIDO_COMIS_TLMK c
						where

			      a.cod_vend= p_cod_vend and
				a.situacao = 0 and
				b.situacao = 0 and
				a.dt_digitacao>= to_date(p_data_inicio,'dd/mm/rr') and
				a.dt_digitacao < to_date(p_data_fim,'dd/mm/rr') + 1 and
			      B.NUM_ITEM_PEDIDO=C.NUM_ITEM_PEDIDO AND
			      B.SEQ_PEDIDO = C.SEQ_PEDIDO AND
			      B.NUM_PEDIDO=C.NUM_PEDIDO AND
			      B.COD_LOJA=C.COD_LOJA AND
		  	      a.seq_pedido=b.seq_pedido and
			      a.num_pedido=b.num_pedido and
			      a.cod_loja=b.cod_loja
			      group by
				  a.cod_vend
				, c.tp_desconto
			      ORDER BY TOTAL DESC)
			      where rownum=1;



             EXCEPTION
             WHEN OTHERS THEN
             p_erro := SQLCODE;


  END;
  

Procedure Pr_Est_contatos (pm_Cursor   IN OUT tp_cursor, pcd_filial in integer,ptipo_ligacao in integer) is
   Begin
Declare
  param_Mes  NUMBER := 0;
  Data_      Date;
  Data_Ini   Date;
  Data_Fim   Date;
  vSql       VARCHAR2(10000);


  BEGIN
    select (VL_PARAMETRO + 1) INTO param_Mes from HELPDESK.PARAMETROS where COD_SOFTWARE = 1104 AND NOME_PARAMETRO = 'QTD_MES';

    vSQL  := 'select a.cod_cliente COD,b.nome_cliente NOME_CLIENTE,b.ddd1 DDD,b.fone1 FONE';
    LOOP
        param_Mes := param_Mes - 1;
        begin
        Data_ := ADD_MONTHS(sysdate,-param_Mes);
        
        Data_Ini := To_DATE('01' || '/' ||  to_char(Data_,'MON') || '/' || to_char(Data_,'YYYY'));
        Data_Fim := To_DATE(to_char(LAST_DAY(Data_),'DD') || '/' ||  to_char(Data_,'MON') || '/' || to_char(Data_,'YYYY'));

        vSQL := vSQL || ',sum(case when dt_contato >= ''' || Data_ini || ''' and dt_contato <= '''|| Data_Fim ||''' then 1 else 0 end) CONT_' || RTRIM(LTRIM(to_char(Data_,'month')));
        vSQL := vSQL || ',sum(case when dt_contato>=''' || Data_ini || ''' and dt_contato<=''' || Data_Fim || ''' then pednota.vl_contabil ';
        vSQL := vSQL || 'else 0 end) FAT_'|| RTRIM(LTRIM(to_char(Data_,'month')));


        end;
      EXIT WHEN param_Mes = 0;
    END LOOP;
    vSQL := vSQL || ' from vendas.contato a,cliente b,REPRESENTANTE C,TIPO_CLIENTE D, ';
    vSQL := vSQL || ' producao.pednota_venda pednota where a.tipo_ligacao=' || ptipo_ligacao ||' and C.COD_FILIAL IN (' || pcd_filial ||') AND ';
    vSQL := vSQL || ' B.COD_TIPO_CLIENTE=D.COD_TIPO_CLI AND A.COD_VEND=C.COD_REPRES AND a.cod_cliente=b.cod_cliente ';
    vSQL := vSQL || ' and pednota.cod_cliente = b.cod_cliente group by a.cod_cliente,b.nome_cliente,b.ddd1,b.fone1';
    
    OPEN pm_Cursor FOR vSQL;
  END;
  End;
  
  
  Procedure Pr_Select_Vl_Parametro2(Pm_Cursor1     Out Tp_Cursor,
                                    Pm_Cod_Sistema In Number,
                                    Pm_Nome_Param  In Varchar2,
                                    Pm_Dep         In Varchar2) Is
    Vsql Varchar2(4000) := '';
  Begin
    Vsql := 'SELECT * FROM ' || Pm_Dep || 'PARAMETROS ';
    Vsql := Vsql || 'WHERE COD_SOFTWARE = ' || Pm_Cod_Sistema;
 
    If Length(Trim(Pm_Nome_Param)) > 0 Then
      Vsql := Vsql || ' AND NOME_PARAMETRO = ' || Chr(39) || Pm_Nome_Param || Chr(39);
    End If;
 
    Open Pm_Cursor1 For Vsql;
 
  End;

END PCK_VDA610;
/
