Procedure Pr_Est_contatos (pm_Cursor   IN OUT tp_cursor, pcd_filial in integer,ptipo_ligacao in integer) is
   Begin
Declare
  param_Mes  NUMBER := 0;
  Data_      Date;
  Data_Ini   Date;
  Data_Fim   Date;
  vSql       VARCHAR2(10000);


  BEGIN
    select (VL_PARAMETRO + 1) INTO param_Mes from HELPDESK.PARAMETROS where COD_SOFTWARE = 1104 AND NOME_PARAMETRO = 'QTD_MES';

    vSQL  := 'select a.cod_cliente COD,b.nome_cliente NOME_CLIENTE,b.ddd1 DDD,b.fone1 FONE';
    LOOP
        param_Mes := param_Mes - 1;
        begin
        Data_ := ADD_MONTHS(sysdate,-param_Mes);
        
        Data_Ini := To_DATE('01' || '/' ||  to_char(Data_,'MON') || '/' || to_char(Data_,'YYYY'));
        Data_Fim := To_DATE(to_char(LAST_DAY(Data_),'DD') || '/' ||  to_char(Data_,'MON') || '/' || to_char(Data_,'YYYY'));

        vSQL := vSQL || ',sum(case when dt_contato >= ''' || Data_ini || ''' and dt_contato <= '''|| Data_Fim ||''' then 1 else 0 end) CONT_' || RTRIM(LTRIM(to_char(Data_,'month')));
        vSQL := vSQL || ',sum(case when dt_contato>=''' || Data_ini || ''' and dt_contato<=''' || Data_Fim || ''' then pednota.vl_contabil ';
        vSQL := vSQL || 'else 0 end) FAT_'|| RTRIM(LTRIM(to_char(Data_,'month')));


        end;
      EXIT WHEN param_Mes = 0;
    END LOOP;
    vSQL := vSQL || ' from vendas.contato a,cliente b,REPRESENTANTE C,TIPO_CLIENTE D, ';
    vSQL := vSQL || ' producao.pednota_venda pednota where a.tipo_ligacao=' || ptipo_ligacao ||' and C.COD_FILIAL IN (' || pcd_filial ||') AND ';
    vSQL := vSQL || ' B.COD_TIPO_CLIENTE=D.COD_TIPO_CLI AND A.COD_VEND=C.COD_REPRES AND a.cod_cliente=b.cod_cliente ';
    vSQL := vSQL || ' and pednota.cod_cliente = b.cod_cliente group by a.cod_cliente,b.nome_cliente,b.ddd1,b.fone1';
    
    OPEN pm_Cursor FOR vSQL;
  END;

   
  End;
